﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.Category" Codebehind="Category.aspx.cs" %>
<%@ Reference Control="~/Catalog/SubPanels/OrderHourPanel.ascx" %>
<%@ Reference VirtualPath="~/Catalog/SubPanels/CategoryProductPanel.ascx" %>
<%@ Reference VirtualPath="~/Catalog/SubPanels/CategoryCulturePanel.ascx" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%" EnableViewState="false">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<D:PlaceHolder runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>							
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblMenuId">Menu</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <U:UltraBoxInt runat="server" ID="ddlMenuId"></U:UltraBoxInt>								
							</td>
						</tr>
						<tr>
						    <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblVisibilityType">Visibility</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxInt runat="server" ID="ddlVisibilityType"></X:ComboBoxInt>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblParentCategoryId">Bovenliggende categorie</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <U:UltraBoxInt runat="server" ID="ddlParentCategoryId"></U:UltraBoxInt>								                                
							</td>							
						</tr>
                        <tr>
						    <td class="label">
						        <D:LabelEntityFieldInfo runat="server" id="lblCustomizeButtonText">Alteration button text</D:LabelEntityFieldInfo>
						    </td>
                            <td class="control">
						        <D:TextBoxString ID="tbCustomizeButtonText" runat="server"></D:TextBoxString>
						    </td>
							<td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblButtonText">Button text</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxString ID="tbButtonText" runat="server"></D:TextBoxString>
							</td>
						</tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDescription" />
							</td>
							<td class="control threeCol" colspan="2">
								<D:TextBoxMultiLine ID="tbDescription" runat="server" Rows="15" UseDataBinding="false" ClientIDMode="Static" onkeyup="parseMarkdown(this);"></D:TextBoxMultiLine> 
                                <small><a href="http://en.wikipedia.org/wiki/Markdown" target="_markdown">Markdown</a>-markup can be used. (bold: **bold**, italic: *italic*).</small>
							</td>
                            <td class="control">
                                <div id="markdown-output" class="markdown_preview markdown-body" />
                            </td>
						</tr>
						
                        <CC:FeatureTogglePanel ToggleType="Tagging_Cms_EnableTagEditing" runat="server">
							<tr>
								<td class="label">Tags</td>
								<td class="control">
									<CC:TagBox ID="tagBox" ClientInstanceName="TagBoxControl" TextField="Name" ValueField="Id" AllowCustomTokens="true" runat="server" />
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>
							<tr>
								<td class="label">Inherited tags</td>
								<td class="control">
									<CC:TagOverview ID="TagOverview" runat="server" />
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>
							<tr>
								<td class="label"></td>
								<td class="control token-overview-control">
									<CC:TagLegend ID="TagLegend" runat="server" />
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>
                        </CC:FeatureTogglePanel>

						<tr>
						    <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblHidePrices">Verberg prijzen</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:CheckBox runat ="server" ID="cbHidePrices" />
                            </td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblGenericcategoryId">Basiscategorie</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlGenericcategoryId" EntityName="Genericcategory" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="GenericcategoryId"></X:ComboBoxLLBLGenEntityCollection>
							</td>
						</tr>
                        <tr>
							<td class="label">
                                <D:Label runat="server" ID="lblCOlor">Color</D:Label>
							</td>
							<td class="control">
                                <dxe:ASPxColorEdit ID="ceColor" runat="server" UseDataBinding="false"></dxe:ASPxColorEdit>
							</td>                            
                        </tr>
                        <tr>
							<td class="label">
							</td>
							<td class="control">
                                <D:Button ID="btnResetColor" runat="server" Text="Reset color" PreSubmitWarning="Are you sure you want to reset the color for this product?" />
							</td>	                            
                        </tr>  
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblType" />
							</td>
							<td class="control">
								<D:RadioButtonList ID="rblType" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Inherit" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Normal" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Service items" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Directory" Value="3"></asp:ListItem>
                                </D:RadioButtonList>
							</td>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" ID="lblScheduleId"></D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<U:UltraBoxInt runat="server" ID="ddlScheduleId"></U:UltraBoxInt>	
							</td>
						</tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lblSupportNotificationType">Support notification</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt ID="ddlSupportNotificationType" runat="server" UseDataBinding="True" notdirty="true"></X:ComboBoxInt>
                            </td>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblViewLayoutType">View layout</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxInt ID="ddlViewLayoutType" runat="server" UseDataBinding="true" notdirty="true"></X:ComboBoxInt>
							</td>
                        </tr>						
                        <tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblUrl">URL</D:Label>
							</td>
							<td class="control">
                                <D:TextBoxString ID="tbUrl" runat="server" UseDataBinding="false" ReadOnly="true"></D:TextBoxString>
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lblDeliveryLocationType">Delivery location</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt ID="ddlDeliveryLocationType" runat="server" UseDataBinding="True" notdirty="true"></X:ComboBoxInt>
                            </td>
                        </tr>
						<tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lblCoversType">Ask for Covers</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <X:ComboBoxInt runat="server" ID="cbCoversType" UseDataBinding="true" DisplayEmptyItem="false" IsRequired="true"></X:ComboBoxInt>
                            </td>
						</tr>
					 </table>
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>
            <X:TabPage Text="Geavanceerd" Name="tabAdvanced">
                <Controls>
				<D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
				        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblPoscategoryId" />
							</td>
							<td class="control">
                                <U:UltraBoxInt runat="server" ID="ddlPoscategoryId"></U:UltraBoxInt>                                
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblRouteId">Route</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <U:UltraBoxInt runat="server" ID="ddlRouteId"></U:UltraBoxInt>                                                                
							</td>
						</tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblMenuItemsMustBeLinkedToExternalProduct">Items must be linked to external product</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" id="cbMenuItemsMustBeLinkedToExternalProduct" />
							</td>
							<td class="label"></td>
							<td class="control"></td>
						</tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblRateable">Rateable</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:RadioTrueFalseNull ID="rblRateable" runat="server"></D:RadioTrueFalseNull>
							</td>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblAnnouncementAction">Announcement action</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat ="server" ID="cbAnnouncementAction" />
							</td>
                        </tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblGeofencing" >Geofencing</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:RadioTrueFalseNull ID="rblGeofencing" runat="server"></D:RadioTrueFalseNull>
							</td>
                            <td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>
                        </tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblAllowFreeText" >Allow free text</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:RadioTrueFalseNull ID="rblAllowFreeText" runat="server"></D:RadioTrueFalseNull>
							</td>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>	
                        </tr>
                    </table>
                </D:PlaceHolder>
                </Controls>
            </X:TabPage>
			<X:TabPage Text="Alterations V1" name="tabAlterations1">
				<Controls>					
                    <div class="divPageLeft">
                        <D:Panel ID="pnlCategoryAlterations1" runat="server" GroupingText="Alterations V1">
                            <table class="dataformV2">
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV1_1">Alteration 1</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV1_1"></U:UltraBoxInt>                                        
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV1_2">Alteration 2</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV1_2"></U:UltraBoxInt>
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV1_3">Alteration 3</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV1_3"></U:UltraBoxInt>
							        </td>
						        </tr>	
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV1_4">Alteration 4</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV1_4"></U:UltraBoxInt>
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV1_5">Alteration 5</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV1_5"></U:UltraBoxInt>
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV1_6">Alteration 6</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV1_6"></U:UltraBoxInt>
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV1_7">Alteration 7</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV1_7"></U:UltraBoxInt>
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV1_8">Alteration 8</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV1_8"></U:UltraBoxInt>
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV1_9">Alteration 9</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV1_9"></U:UltraBoxInt>
							        </td>							        
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV1_10">Alteration 10</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV1_10"></U:UltraBoxInt>
							        </td>
						        </tr>																																																						
					        </table>
                        </D:Panel>
                    </div>
                    <div class="divPageRight">
                        <D:Panel ID="pnlInheritedAlterationV1" runat="server" GroupingText="Inherited alterations">
                            <D:PlaceHolder runat="server" ID="plhInheritedAlterationsV1"></D:PlaceHolder>
                        </D:Panel>
                    </div>
				</Controls>
			</X:TabPage>	
            <X:TabPage Text="Alterations V2" name="tabAlterations2">
				<Controls>					
                    <div class="divPageLeft">
                        <D:Panel ID="pnlCategoryAlterations2" runat="server" GroupingText="Alterations V2">
                            <table class="dataformV2">
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV2_11">Alteration 1</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV2_11"></U:UltraBoxInt>                                        
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV2_12">Alteration 2</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV2_12"></U:UltraBoxInt>
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV2_13">Alteration 3</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV2_13"></U:UltraBoxInt>
							        </td>
						        </tr>	
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV2_14">Alteration 4</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV2_14"></U:UltraBoxInt>
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV2_15">Alteration 5</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV2_15"></U:UltraBoxInt>
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV2_16">Alteration 6</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV2_16"></U:UltraBoxInt>
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV2_17">Alteration 7</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV2_17"></U:UltraBoxInt>
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV2_18">Alteration 8</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV2_18"></U:UltraBoxInt>
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV2_19">Alteration 9</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV2_19"></U:UltraBoxInt>
							        </td>							        
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategoryAlterationV2_20">Alteration 10</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <U:UltraBoxInt runat="server" ID="ddlCategoryAlterationV2_20"></U:UltraBoxInt>
							        </td>
						        </tr>																																																						
					        </table>
                        </D:Panel>
                    </div>
                    <div class="divPageRight">
                        <D:Panel ID="pnlInheritedAlterationV2" runat="server" GroupingText="Inherited alterations">
                            <D:PlaceHolder runat="server" ID="plhInheritedAlterationsV2"></D:PlaceHolder>
                        </D:Panel>
                    </div>
				</Controls>
			</X:TabPage>	            								
			<X:TabPage Text="Productsuggesties" Name="tabSuggestions">
				<Controls>					
                    <div class="divPageLeft">
                        <D:Panel ID="pnlCategorySuggestions" runat="server" GroupingText="Suggesties">
                            <table class="dataformV2">
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategorySuggestion1">Suggestie 1</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <X:ComboBoxInt runat="server" ID="ddlCategorySuggestion1" EntityName="ProductCategory" IncrementalFilteringMode="StartsWith" TextField="ProductCategoryMenuName" ValueField="ProductCategoryId"></X:ComboBoxInt>
							        </td>
						        </tr>
                                <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblSuggestionCheckout1">Checkout</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <D:CheckBox runat="server" ID="cbSuggestionCheckout1" />
							        </td>
						        </tr>
						        <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblCategorySuggestion2">Suggestie 2</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <X:ComboBoxInt runat="server" ID="ddlCategorySuggestion2" EntityName="ProductCategory" IncrementalFilteringMode="StartsWith" TextField="ProductCategoryMenuName" ValueField="ProductCategoryId" ></X:ComboBoxInt>
							        </td>
						        </tr>						
                                <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblSuggestionCheckout2">Checkout</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <D:CheckBox runat="server" ID="cbSuggestionCheckout2" />
							        </td>
						        </tr>
                                <tr>
                                    <td class="label">
								        <D:Label runat="server" id="lblCategorySuggestion3">Suggestie 3</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <X:ComboBoxInt runat="server" ID="ddlCategorySuggestion3" EntityName="ProductCategory" IncrementalFilteringMode="StartsWith" TextField="ProductCategoryMenuName" ValueField="ProductCategoryId" ></X:ComboBoxInt>
							        </td>
                                </tr>
                                <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblSuggestionCheckout3">Checkout</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <D:CheckBox runat="server" ID="cbSuggestionCheckout3" />
							        </td>
						        </tr>
                                <tr>
                                    <td class="label">
								        <D:Label runat="server" id="lblCategorySuggestion4">Suggestie 4</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <X:ComboBoxInt runat="server" ID="ddlCategorySuggestion4" EntityName="ProductCategory" IncrementalFilteringMode="StartsWith" TextField="ProductCategoryMenuName" ValueField="ProductCategoryId" ></X:ComboBoxInt>
							        </td>                                    
                                </tr>
                                <tr>
							        <td class="label">
								        <D:Label runat="server" id="lblSuggestionCheckout4">Checkout</D:Label>
							        </td>
							        <td class="control" colspan="2">
                                        <D:CheckBox runat="server" ID="cbSuggestionCheckout4" />
							        </td>
						        </tr>
					        </table>
                        </D:Panel>
                    </div>
                    <div class="divPageRight">
                        <D:Panel ID="pnlInheritedSuggestions" runat="server" GroupingText="Geërfde suggesties">
                            <D:PlaceHolder runat="server" ID="plhInheritedSuggestions"></D:PlaceHolder>
                        </D:Panel>
                    </div>
				</Controls>			
			</X:TabPage>
		</TabPages>
	</X:PageControl>
            <script type="text/javascript">
            function parseMarkdown(obj) {
                var text = obj.value;
                document.getElementById("markdown-output").innerHTML = markdown.toHTML(text);
            }

            parseMarkdown(document.getElementById("tbDescription"));
        </script>
</div>
</asp:Content>

