using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.Generic.SubPanels;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Productgroup : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields

        private OptionPanel optionPanel = null;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.Productgroup_DataSourceLoaded;
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            Generic.UserControls.CustomTextCollection translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic") as Generic.UserControls.CustomTextCollection;
            translationsPanel.FullWidth = true;

            this.optionPanel = (OptionPanel)this.LoadControl("~/Generic/SubPanels/OptionPanel.ascx");
            this.plhOptionsPanel.Controls.Add(this.optionPanel);
        }

        private void Productgroup_DataSourceLoaded(object sender)
        {
            CustomTextRenderer renderer = new CustomTextRenderer(this.DataSourceAsProductgroupEntity, CmsSessionHelper.DefaultCultureCodeForCompany);
            renderer.RenderCustomText(this.tbColumnTitle, CustomTextType.ProductgroupColumnTitle);
            renderer.RenderCustomText(this.tbColumnSubtitle, CustomTextType.ProductgroupColumnSubtitle);

            if (this.optionPanel != null && !this.DataSourceAsProductgroupEntity.IsNew)
            {
                this.optionPanel.Load(2, -1, -1, -1, this.DataSourceAsProductgroupEntity.ProductgroupId, true);
            }

            this.RenderProductsUsingProductgroup();
        }

        private void RenderProductsUsingProductgroup()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.ProductgroupId == this.EntityId);

            SortExpression sort = new SortExpression();
            sort.Add(ProductFields.Name | SortOperator.Ascending);

            IncludeFieldsList includes = new IncludeFieldsList(ProductFields.Name);
                
            ProductCollection products = new ProductCollection();
            products.GetMulti(filter, 0, null, null, null, includes, 0, 0);

            foreach (ProductEntity product in products)
            {
                this.plhProducts.AddHtml("<tr><td><a href=\"{0}\" target=\"_blank\">{1}</a></td></tr>", this.ResolveUrl("~/Catalog/Product.aspx?Id=" + product.ProductId), product.Name);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }        

	    public override bool Save()
	    {
           if (this.PageMode == Dionysos.Web.PageMode.Add)
                this.DataSourceAsProductgroupEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;

           if (base.Save())
           {
               this.SaveCustomTexts();               
               return true;
           }
           else
               return false;           
        }

        private void SaveCustomTexts()
        {
            CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsProductgroupEntity.CompanyEntity);

            CustomTextRenderer renderer = new CustomTextRenderer(this.DataSourceAsProductgroupEntity, CmsSessionHelper.DefaultCultureCodeForCompany);
            renderer.SaveCustomText(this.tbColumnTitle, CustomTextType.ProductgroupColumnTitle);
            renderer.SaveCustomText(this.tbColumnSubtitle, CustomTextType.ProductgroupColumnSubtitle);
        }

        #endregion
        
		#region Properties

		
		public ProductgroupEntity DataSourceAsProductgroupEntity
		{
			get
			{
				return this.DataSource as ProductgroupEntity;
			}
		}

		#endregion        
    }
}
