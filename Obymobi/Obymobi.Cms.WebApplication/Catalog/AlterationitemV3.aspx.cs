using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.ObymobiCms.Generic.SubPanels;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.ObymobiCms.Catalog
{
	public partial class AlterationitemV3 : Dionysos.Web.UI.PageLLBLGenEntity
	{
		#region Fields

		private OptionPanel optionPanel = null;

		#endregion

		#region Methods

		protected override void OnInit(EventArgs e)
		{
			EntityName = nameof(AlterationitemEntity);
			DataSourceLoaded += Alterationitem_DataSourceLoaded;
			base.OnInit(e);
		}

		private void Alterationitem_DataSourceLoaded(object sender)
		{
			SetFilters();
			SetGui();
		}

		private void SetFilters()
		{
			int companyId = DataSourceAsAlterationitemEntity.ParentCompanyId.GetValueOrDefault(CmsSessionHelper.CurrentCompanyId);

			PredicateExpression filter2 = new PredicateExpression();
			if (DataSourceAsAlterationitemEntity.ParentBrandId.HasValue)
			{
				filter2.Add(AlterationoptionFields.BrandId == DataSourceAsAlterationitemEntity.ParentBrandId);
			}
			else
			{
				filter2.Add(AlterationoptionFields.CompanyId == companyId);
			}
			filter2.Add(AlterationoptionFields.Version == 2);

			SortExpression sort2 = new SortExpression(new SortClause(AlterationoptionFields.Name, SortOperator.Ascending));

			AlterationoptionCollection alterationoptionCollection = new AlterationoptionCollection();
			alterationoptionCollection.GetMulti(filter2, 0, sort2);

			ddlAlterationoptionId.DataSource = alterationoptionCollection;
			ddlAlterationoptionId.DataBind();

			// GK: Code below was commented out and dropdown was removed by DK, couldn't remeber why.
			// return code below, because otherwise the AlterationId won't be set.
			PredicateExpression filter = new PredicateExpression();
			if (DataSourceAsAlterationitemEntity.ParentBrandId.HasValue)
			{
				filter.Add(AlterationFields.BrandId == DataSourceAsAlterationitemEntity.ParentBrandId);
			}
			else
			{
				filter.Add(AlterationFields.CompanyId == companyId);
			}
			filter.Add(AlterationFields.Version == 2);

			SortExpression sort = new SortExpression(new SortClause(AlterationFields.Name, SortOperator.Ascending));

			AlterationCollection alterationCollection = new AlterationCollection();
			alterationCollection.GetMulti(filter, 0, sort);

			ddlAlterationId.DataSource = alterationCollection;
			ddlAlterationId.DataBind();

            SetPosalterationitemIdFilter(companyId);
        }

		private void SetPosalterationitemIdFilter(int companyId)
        {
            PredicateExpression posalterationitemFilter = new PredicateExpression(PosalterationitemFields.CompanyId == companyId);
            SortExpression posalterationitemSort = new SortExpression(new SortClause(PosalterationitemFields.ExternalId, SortOperator.Ascending));

            PosalterationitemCollection posalterationitemCollection = new PosalterationitemCollection();
            posalterationitemCollection.GetMulti(posalterationitemFilter, 0, posalterationitemSort);

            foreach (PosalterationitemEntity posalterationitem in posalterationitemCollection)
            {
                if (!string.IsNullOrWhiteSpace(posalterationitem.RevenueCenter))
                {
                    this.ddlPosalterationitemId.Items.Add($"{posalterationitem.ExternalId} [{posalterationitem.RevenueCenter}]", posalterationitem.PosalterationitemId);
                }
                else
                {
                    this.ddlPosalterationitemId.Items.Add($"{posalterationitem.ExternalId}", posalterationitem.PosalterationitemId);
                }
            }
		}

        private void SetGui()
		{
			if (!DataSourceAsAlterationitemEntity.IsNew)
			{
				hlAlterationoptionId.Text = DataSourceAsAlterationitemEntity.AlterationoptionEntity.Name + " &#187;&#187;";				
				hlAlterationoptionId.NavigateUrl = ResolveUrl(string.Format("~/Catalog/Alterationoption.aspx?id={0}", DataSourceAsAlterationitemEntity.AlterationoptionId));

				hlAlterationId.Text = DataSourceAsAlterationitemEntity.AlterationEntity.Name + " &#187;&#187;";				
				hlAlterationId.NavigateUrl = ResolveUrl(string.Format("~/Catalog/AlterationV3.aspx?id={0}", DataSourceAsAlterationitemEntity.AlterationId));

				if (DataSourceAsAlterationitemEntity.PosalterationitemId.HasValue)
				{
					hlPosalterationitemId.Text = DataSourceAsAlterationitemEntity.PosalterationitemEntity.ExternalId + " &#187;&#187;";					
					hlPosalterationitemId.NavigateUrl = ResolveUrl(string.Format("~/Catalog/Posalterationitem.aspx?id={0}", DataSourceAsAlterationitemEntity.PosalterationitemId.Value));
				}

				tbPrice.Text = DataSourceAsAlterationitemEntity.AlterationoptionEntity.PriceIn.GetValueOrDefault(0).ToString("0.00");

				optionPanel = LoadControl<OptionPanel>("~/Generic/SubPanels/OptionPanel.ascx");
				plhOptions.Controls.Add(optionPanel);

				if (optionPanel != null)
				{
					optionPanel.Load(2, -1, -1, DataSourceAsAlterationitemEntity.AlterationitemId, -1, true);
				}
			}
		}

		public override bool Save()
		{
			bool success = base.Save();

			decimal price;
			if (success && decimal.TryParse(tbPrice.Text, out price))
			{
				DataSourceAsAlterationitemEntity.AlterationoptionEntity.PriceIn = price;
				DataSourceAsAlterationitemEntity.AlterationoptionEntity.Save();
			}

			if (cbSelectedOnDefault.Value)
			{
				AlterationEntity alterationEntity = new AlterationEntity(DataSourceAsAlterationitemEntity.AlterationId);
				if (alterationEntity.IsNew)
				{
					// Hmmmmmz
				}
				else if (alterationEntity.MaxOptions == 1)
				{
					// Check if there's another alterationitem set as default for this alteration. If so, reset to false
					PredicateExpression filter = new PredicateExpression();
					filter.Add(AlterationitemFields.AlterationId == DataSourceAsAlterationitemEntity.AlterationId);
					filter.Add(AlterationitemFields.SelectedOnDefault == true);
					filter.Add(AlterationitemFields.Version == 2);

					if (!DataSourceAsAlterationitemEntity.IsNew)
						filter.AddWithAnd(AlterationitemFields.AlterationitemId != DataSourceAsAlterationitemEntity.AlterationitemId);

					AlterationitemCollection defaultAlterationitems = new AlterationitemCollection();
					defaultAlterationitems.GetMulti(filter);

					foreach (AlterationitemEntity alterationitem in defaultAlterationitems)
					{
						alterationitem.SelectedOnDefault = false;
						alterationitem.Save();
					}
				}
			}

			return success;
		}

		#endregion

		#region Properties


		/// <summary>
		/// Return the page's datasource as a Entity
		/// </summary>
		public AlterationitemEntity DataSourceAsAlterationitemEntity
		{
			get
			{
				return DataSource as AlterationitemEntity;
			}
		}

		#endregion
	}
}
