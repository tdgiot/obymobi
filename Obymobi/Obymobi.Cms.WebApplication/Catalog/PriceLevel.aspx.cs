﻿using System;
using Dionysos;
using Dionysos.Web.UI;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.ObymobiCms.Catalog.SubPanels;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class PriceLevel : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields

        private PriceLevelItemsPanel priceLevelItemPanels;

        #endregion

        #region Methods

        private void LoadUserControls()
        {
            this.priceLevelItemPanels = (PriceLevelItemsPanel)this.LoadControl("~/Catalog/SubPanels/PriceLevelItemsPanel.ascx");
            this.plhItems.Controls.Add(this.priceLevelItemPanels);         
        }

        private void SetGui()
        {
            if (this.priceLevelItemPanels != null)
            {
                this.priceLevelItemPanels.PriceLevelId = this.DataSourceAsPriceLevelEntity.PriceLevelId;
                this.priceLevelItemPanels.Refresh();
            }            
        }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsPriceLevelEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }
            return base.Save();
        }        

        #endregion

		#region Properties
		
		public PriceLevelEntity DataSourceAsPriceLevelEntity
		{
			get
			{
                return this.DataSource as PriceLevelEntity;
			}
		}

		#endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.PriceLevel_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Page_Load(object sender, EventArgs e)
        {
            this.btCopyPriceLevel.Click += BtCopyPriceLevel_Click;
        }

        private void BtCopyPriceLevel_Click(object sender, EventArgs e)
        {
            if (this.tbCopyToName.Text.IsNullOrWhiteSpace())
            {
                this.MultiValidatorDefault.AddError("Please enter a name for the price level to be created.");
            }
            else
            {
                PriceLevelEntity newPriceLevelEntity;
                if (PriceLevelHelper.TryAndCopy(this.DataSourceAsPriceLevelEntity.PriceLevelId, this.tbCopyToName.Text, out newPriceLevelEntity))
                {
                    this.AddInformatorInfo("Price level created: <a href=\"{0}\">{1}</a>".FormatSafe(this.ResolveUrl("~/Catalog/PriceLevel.aspx?id={0}".FormatSafe(newPriceLevelEntity.PriceLevelId)), newPriceLevelEntity.Name));
                }
                else
                {
                    this.MultiValidatorDefault.AddError("Something went wrong trying to copy a price level. Contact a developer.");
                }
            }
            this.Validate();
        }

        private void PriceLevel_DataSourceLoaded(object sender)
		{
            this.SetGui();
		}

		#endregion
	}
}
