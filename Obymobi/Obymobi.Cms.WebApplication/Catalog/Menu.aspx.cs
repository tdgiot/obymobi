﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.Timestamps;
using Crave.Api.Logic.UseCases;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.Requests;
using Obymobi.Cms.Logic.UseCases;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.Catalog.SubPanels;
using Obymobi.Security;
using Obymobi.Web.HelperClasses;
using Obymobi.Web.Media;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Catalog
{
	public partial class Menu : Dionysos.Web.UI.PageLLBLGenEntity
	{
		#region Fields

		private CategoriesPanel categoriesPanel = null;
		private Transaction transaction = null;
		private EntityView<CategoryEntity> categoriesView = null;

		private int timeOut;

		#endregion

		#region Methods

		public Menu()
		{
			this.Init += Menu_Init;
		}

		void Menu_Init(object sender, EventArgs e)
		{
			timeOut = Server.ScriptTimeout;

			// Give it 1 hour = 3600 seconds
			Server.ScriptTimeout = 3600;
		}

		private void LoadUserControls()
		{
			this.categoriesPanel = (CategoriesPanel)this.LoadControl("~/Catalog/SubPanels/CategoriesPanel.ascx");
            this.categoriesPanel.ID = "ucCategoriesPanel";
			this.plhCategoriesPanel.Controls.Add(this.categoriesPanel);

			if (CmsSessionHelper.CurrentRole < Role.Reseller)
			{
				this.tabsMain.TabPages.FindByName("Copy").Visible = false;
				this.tabsMain.TabPages.FindByName("Download").Visible = false;
				this.tabsMain.TabPages.FindByName("Push").Visible = false;
				this.tabsMain.TabPages.FindByName("Brands").Visible = false;
			}
		}

		private void SetGui()
		{
			this.btnDumpEt.Visible = CmsSessionHelper.CurrentRole == Role.GodMode;

			if (this.categoriesPanel != null)
			{
				this.categoriesPanel.RefreshDataSource(this.DataSourceAsMenuEntity);
			}

			this.SetMenuWebserviceTimestamp();

			if (CmsSessionHelper.CurrentRole >= Role.Reseller)
			{
				BrandCollection brandCollection = new BrandCollection();
				foreach (CompanyBrandEntity companyBrandEntity in this.DataSourceAsMenuEntity.CompanyEntity.CompanyBrandCollection)
				{
					// Check if user has access to this brand
					if (CmsSessionHelper.GetUserRoleForBrand(companyBrandEntity.BrandId) != null)
					{
						brandCollection.Add(companyBrandEntity.BrandEntity);
					}
				}

				this.ddlBrands.DataSource = brandCollection;
				this.ddlBrands.DataBind();

				this.RenderMenuCategories(this.DataSourceAsMenuEntity.CategoryCollection);
			}

			if (CmsSessionHelper.CurrentRole != Role.GodMode)
			{
				MasterPages.MasterPageEntity masterPage = this.Master as MasterPages.MasterPageEntity;
				if (masterPage != null && masterPage.ToolBar.DeleteButton != null)
				{
					masterPage.ToolBar.DeleteButton.Visible = false;
				}
			}

			if (this.DataSourceAsMenuEntity.CompanyEntity.ApiVersion == 2)
			{
				this.tabsMain.TabPages.FindByName("Push").ClientVisible = false;
			}
		}

		private readonly List<int> renderedIds = new List<int>();
		private void RenderMenuCategories(CategoryCollection categoryCollection)
		{
			foreach (CategoryEntity categoryEntity in categoryCollection)
			{
				if (categoryEntity.ChildCategoryCollection.Count == 0)
				{
					if (!this.renderedIds.Contains(categoryEntity.CategoryId))
					{
						this.ddlCategories.Items.Add(string.Format("{0} ({1})", categoryEntity.FullCategoryName, categoryEntity.CategoryId), categoryEntity.CategoryId);
						this.renderedIds.Add(categoryEntity.CategoryId);
					}
				}
				else
				{
					this.RenderMenuCategories(categoryEntity.ChildCategoryCollection);
				}
			}
		}

		private void SetMenuWebserviceTimestamp()
		{
			this.lblTimestampMenu.Text = this.DataSourceAsMenuEntity.CompanyEntity.MenuDataLastModifiedUTC.ToString(CultureInfo.InvariantCulture);

			string cachePushKey = "MenuPush" + this.DataSourceAsMenuEntity.CompanyId;
			if (Dionysos.Web.CacheHelper.HasValue(cachePushKey, false))
			{
				this.lblTimestampMenu.Text += " (Publishing...)";
			}
		}

		private EntityView<CategoryEntity> GetCategories(bool rootOnly)
		{
			PredicateExpression filter = new PredicateExpression();
			filter.Add(CategoryFields.MenuId == this.DataSourceAsMenuEntity.MenuId);

			if (rootOnly)
			{
				filter.Add(CategoryFields.ParentCategoryId == DBNull.Value);
			}

			CategoryCollection categories = new CategoryCollection();
			categories.GetMulti(filter);

			this.categoriesView = categories.DefaultView;

			SortExpression sort = new SortExpression(CategoryFields.ParentCategoryId | SortOperator.Ascending);
			sort.Add(new SortClause(CategoryFields.SortOrder, SortOperator.Ascending));
			this.categoriesView.Sorter = sort;

			return this.categoriesView;
		}

		public override bool Save()
		{
			if (this.PageMode == Dionysos.Web.PageMode.Add)
			{
				this.DataSourceAsMenuEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
			}
			return base.Save();
		}

		public override bool Delete()
		{
			if (CmsSessionHelper.CurrentRole <= Role.Reseller)
			{
				this.AddInformator(InformatorType.Warning, "Your current role does not allow you to delete this item.");

				return false;
			}

			PredicateExpression filter = new PredicateExpression();
			filter.Add(CategoryFields.MenuId == this.DataSourceAsMenuEntity.MenuId);

			CategoryCollection categories = new CategoryCollection();
			categories.GetMulti(filter);

			foreach (CategoryEntity category in categories)
			{
				foreach (ProductCategoryEntity productCategory in category.ProductCategoryCollection)
				{
					productCategory.Delete();
				}
				category.Delete();
			}

			return base.Delete();
		}

		private void DownloadMenu()
		{
			StringBuilder builder = new StringBuilder();
			this.AddCategories(this.GetCategories(true), string.Empty, ref builder);

			this.Response.Clear();
			this.Response.Buffer = false;
			this.Response.AppendHeader("Content-Type", "text/plain");
			this.Response.AppendHeader("Content-Disposition", string.Format("attachment; filename=menu-{0}-{1}.txt", this.DataSourceAsMenuEntity.CompanyId, this.DataSourceAsMenuEntity.MenuId));
			this.Response.Write(builder.ToString());
			this.Response.Flush();
			this.Response.End();
		}

		private void AddCategories(EntityView<CategoryEntity> categoriesView, string indent, ref StringBuilder builder)
		{
			SortExpression sort = new SortExpression(CategoryFields.SortOrder | SortOperator.Ascending);
			categoriesView.Sorter = sort;

			foreach (CategoryEntity category in categoriesView)
			{
				if (category.MenuId != this.DataSourceAsMenuEntity.MenuId)
					continue;

				string mediaIds = string.Empty;

				if (this.cbIncludeCategoryMedia.Checked)
				{
					for (int i = 0; i < category.MediaCollection.Count; i++)
					{
						if (i < (category.MediaCollection.Count - 1))
							mediaIds += category.MediaCollection[i].MediaId + ",";
						else
							mediaIds += category.MediaCollection[i].MediaId;
					}
				}

				List<string> jsonParams = new List<string>();
				jsonParams.Add(string.Format("CategoryType:{0}", category.Type));
				jsonParams.Add(string.Format("Description:\"{0}\"", category.Description.Replace("\"", "\\\"")));
				jsonParams.Add(string.Format("MediaIds:[{0}]", mediaIds));

				string jsonObj = "{" + StringUtil.CombineWithCommaSpace(jsonParams.ToArray()) + "}";
				string line = string.Format("{0}#{1}{2}", indent, category.Name, jsonObj);
				builder.AppendLine(line);

				if (category.ProductCategoryCollection.Count > 0)
					this.AddProducts(category.ProductCategoryCollection, ("-" + indent), ref builder);

				if (category.ChildCategoryCollection.Count > 0)
					this.AddCategories(category.ChildCategoryCollection.DefaultView, ("-" + indent), ref builder);
			}
		}

		private void AddProducts(ProductCategoryCollection productCategories, string indent, ref StringBuilder builder)
		{
			EntityView<ProductCategoryEntity> productCategoriesView = productCategories.DefaultView;

			SortExpression sort = new SortExpression(ProductCategoryFields.SortOrder | SortOperator.Ascending);
			productCategoriesView.Sorter = sort;

			foreach (ProductCategoryEntity productCat in productCategoriesView)
			{
				ProductEntity p = productCat.ProductEntity;

				string mediaIds = string.Empty;
				string productAlterationIds = string.Empty;

				if (this.cbIncludeMedia.Checked)
				{
					for (int i = 0; i < p.MediaCollection.Count; i++)
					{
						if (i < (p.MediaCollection.Count - 1))
							mediaIds += p.MediaCollection[i].MediaId + ",";
						else
							mediaIds += p.MediaCollection[i].MediaId;
					}
				}

				if (this.cbIncludeAlterations.Checked)
				{
					for (int i = 0; i < p.ProductAlterationCollection.Count; i++)
					{
						if (i < (p.ProductAlterationCollection.Count - 1))
							productAlterationIds += p.ProductAlterationCollection[i].ProductAlterationId + ",";
						else
							productAlterationIds += p.ProductAlterationCollection[i].ProductAlterationId;
					}
				}

				List<string> jsonParams = new List<string>();
				jsonParams.Add(string.Format("Price:{0}", p.PriceIn));
				jsonParams.Add(string.Format("ProductType:{0}", p.Type));
				jsonParams.Add(string.Format("ProductSubType:{0}", p.SubType));
				jsonParams.Add(string.Format("GenericproductId:{0}", p.GenericproductId));
				jsonParams.Add(string.Format("MediaIds:[{0}]", mediaIds));
				jsonParams.Add(string.Format("ProductAlterationIds:[{0}]", productAlterationIds));
				jsonParams.Add(string.Format("Description:\"{0}\"", p.Description.Replace("\"", "\\\"")));
				jsonParams.Add(string.Format("WebTypeSmartphoneUrl:\"{0}\"", p.WebTypeSmartphoneUrl.Replace("\"", "\\\"")));
				jsonParams.Add(string.Format("WebTypeTabletUrl:\"{0}\"", p.WebTypeTabletUrl.Replace("\"", "\\\"")));

				string jsonObj = "{" + StringUtil.CombineWithCommaSpace(jsonParams.ToArray()) + "}";
				string line = string.Format("{0}{1}{2}", indent, p.Name, jsonObj);
				builder.AppendLine(line);
			}
		}

		private void CopyMenu()
		{
			if (this.tbNewMenuName.Value.Length <= 0)
			{
				this.MultiValidatorDefault.AddError("No name has been entered for the new menu");
				this.Validate();
			}
			else
			{
				Menu.StartSession();

				new Thread((param) =>
				{
					HttpContext.Current = (HttpContext)param;
					int userId = CmsSessionHelper.CurrentUser.UserId;

					if (Thread.GetNamedDataSlot("Role") == null)
						Thread.AllocateNamedDataSlot("Role");

					Thread.SetData(Thread.GetNamedDataSlot("Role"), CmsSessionHelper.CurrentRole);

					this.transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "CopyMenu");
					try
					{
						Menu.AppendLog("Copying of menu '{0}' started...", this.DataSourceAsMenuEntity.Name);

						this.Copy(this.DataSourceAsMenuEntity, this.tbNewMenuName.Value);

						this.transaction.Commit();
						Menu.AppendLog("New menu '{0}' successfully created!", this.tbNewMenuName.Value);
					}
					catch (Exception ex)
					{
						this.transaction.Rollback();
						Menu.AppendLog("Something went wrong while copying the menu. {0}.", ex.Message);
					}
					finally
					{
						this.transaction.Dispose();
						Menu.EndSessionOnGetLog();
					}
				}).Start(HttpContext.Current);

				string message = "Copying of the menu started in the background and can take a couple of minutes to complete!";
				((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, message);
			}
		}

		public void Copy(MenuEntity oldMenu, string newMenuName)
		{
			Menu.AppendLog("Copying menu... '{0}'", newMenuName);

			MenuEntity newMenu = new MenuEntity();
			newMenu.AddToTransaction(this.transaction);
			LLBLGenEntityUtil.CopyFields(oldMenu, newMenu, null);
			newMenu.Name = newMenuName;
			newMenu.Save();

			CategoryCollection newCategories = new CategoryCollection();
			Dictionary<int, int> categoryKeys = new Dictionary<int, int>();
			Dictionary<int, int> parentCategories = new Dictionary<int, int>();

			ArrayList categoryFieldsNotToBeCopied = new ArrayList();
			categoryFieldsNotToBeCopied.Add(CategoryFields.MenuId);
			categoryFieldsNotToBeCopied.Add(CategoryFields.PoscategoryId);
			categoryFieldsNotToBeCopied.Add(CategoryFields.ParentCategoryId);

			ArrayList productCategoryFieldsNotToBeCopied = new ArrayList();
			productCategoryFieldsNotToBeCopied.Add(ProductCategoryFields.CategoryId);

			ArrayList categoryAlterationFieldsNotToBeCopied = new ArrayList();
			categoryAlterationFieldsNotToBeCopied.Add(CategoryAlterationFields.CategoryId);

			ArrayList categorySuggestionFieldsNotToBeCopied = new ArrayList();
			categorySuggestionFieldsNotToBeCopied.Add(CategorySuggestionFields.CategoryId);

			foreach (CategoryEntity oldCategory in oldMenu.CategoryCollection)
			{
				Menu.AppendLog("Copying category... '{0}'", oldCategory.Name);

				CategoryEntity newCategory = new CategoryEntity();
				newCategory.AddToTransaction(this.transaction);
				LLBLGenEntityUtil.CopyFields(oldCategory, newCategory, categoryFieldsNotToBeCopied);
				newCategory.MenuId = newMenu.MenuId;
				newCategory.Save();

				if (oldCategory.ParentCategoryId.HasValue)
					parentCategories.Add(newCategory.CategoryId, oldCategory.ParentCategoryId.Value);

				categoryKeys.Add(oldCategory.CategoryId, newCategory.CategoryId);

				foreach (ProductCategoryEntity oldProductCategory in oldCategory.ProductCategoryCollection)
				{
					ProductCategoryEntity newProductCategory = new ProductCategoryEntity();
					newProductCategory.AddToTransaction(this.transaction);
					LLBLGenEntityUtil.CopyFields(oldProductCategory, newProductCategory, productCategoryFieldsNotToBeCopied);
					newCategory.ProductCategoryCollection.Add(newProductCategory);
				}

				foreach (MediaEntity oldMedia in oldCategory.MediaCollection)
				{
					CopyMediaRequest request = new CopyMediaRequest
					{
						MediaToCopy = oldMedia,
						ReferenceField = MediaFields.CategoryId,
						ReferenceValue = newCategory.CategoryId,
						CopyMediaOnCdn = MediaCloudHelper.CopyMediaOnCdn,
						Transaction = this.transaction
					};
					new CopyMediaUseCase().Execute(request);
				}

				foreach (CategoryAlterationEntity oldCategoryAlteration in oldCategory.CategoryAlterationCollection)
				{
					CategoryAlterationEntity newCategoryAlteration = new CategoryAlterationEntity();
					newCategoryAlteration.AddToTransaction(this.transaction);
					LLBLGenEntityUtil.CopyFields(oldCategoryAlteration, newCategoryAlteration, categoryAlterationFieldsNotToBeCopied);
					newCategory.CategoryAlterationCollection.Add(newCategoryAlteration);
				}

				foreach (CategorySuggestionEntity oldCategorySuggestion in oldCategory.CategorySuggestionCollection)
				{
					CategorySuggestionEntity newCategorySuggestion = new CategorySuggestionEntity();
					newCategorySuggestion.AddToTransaction(this.transaction);
					LLBLGenEntityUtil.CopyFields(oldCategorySuggestion, newCategorySuggestion, categorySuggestionFieldsNotToBeCopied);
					newCategory.CategorySuggestionCollection.Add(newCategorySuggestion);
				}

				newCategory.CustomTextCollection.AddRange(oldCategory.CustomTextCollection.Copy(CustomTextFields.CategoryId));
				newCategory.Save(true);
				newCategories.Add(newCategory);
			}

			foreach (CategoryEntity newCategory in newCategories)
			{
				if (parentCategories.ContainsKey(newCategory.CategoryId) && categoryKeys.ContainsKey(parentCategories[newCategory.CategoryId]))
				{
					Menu.AppendLog("Setting parent on category... '{0}'", newCategory.Name);
					newCategory.ParentCategoryId = categoryKeys[parentCategories[newCategory.CategoryId]];
					newCategory.Save();
				}
			}
		}

		private void HookUpEvents()
		{
			this.btnAddBatch.Click += this.btnAddBatch_Click;
			this.btnDownloadMenu.Click += this.btnDownloadMenu_Click;
			this.btnCopyMenu.Click += this.btnCopyMenu_Click;
			this.btnPush.Click += btnPush_Click;

			this.ddlBrands.SelectedIndexChanged += ddlBrands_SelectedIndexChanged;
			this.btnLinkBrandproduct.Click += btnLinkBrandproduct_Click;
		}

		private CategoryEntity FindCategoryById(int id, CategoryCollection collection)
		{
			foreach (CategoryEntity categoryEntity in collection)
			{
				if (categoryEntity.CategoryId == id)
				{
					return categoryEntity;
				}

				if (categoryEntity.ChildCategoryCollection.Count > 0)
				{
					return FindCategoryById(id, categoryEntity.ChildCategoryCollection);
				}
			}

			return null;
		}

		void btnLinkBrandproduct_Click(object sender, EventArgs e)
		{
			if (this.ddlBrandProducts.ValidId <= 0)
			{
				return;
			}
			if (this.ddlCategories.Value == null)
			{
				return;
			}

			int brandproductToLink = this.ddlBrandProducts.Value.GetValueOrDefault(0);
			int categoryToLinkInto;
			int.TryParse(this.ddlCategories.SelectedValueString, out categoryToLinkInto);

			// Fetch the genericproduct
			ProductEntity brandproductEntity = new ProductEntity(brandproductToLink);
			if (brandproductEntity.IsNew)
			{
				this.AddInformator(InformatorType.Warning, "Brand product (id '{0}') does not exists..", brandproductToLink);
				return;
			}

			PrefetchPath prefetchPath = new PrefetchPath(EntityType.CategoryEntity);
			prefetchPath.Add(CategoryEntity.PrefetchPathProductCategoryCollection);
			CategoryEntity category = new CategoryEntity(categoryToLinkInto, prefetchPath);
			if (category.IsNew)
			{
				this.AddInformator(InformatorType.Warning, "Selected import category (id '{0}') does not exists.", categoryToLinkInto);
				return;
			}

			bool success;
			using (Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "LinkBrandProduct"))
			{
				try
				{
					// Create new product
					ProductEntity product = new ProductEntity();
					product.ValidatorCreateOrUpdateDefaultCustomText = false;
					product.CompanyId = this.DataSourceAsMenuEntity.CompanyId;
					product.Name = brandproductEntity.Name;
					product.Type = (int)ProductType.Product;
					product.SubTypeAsEnum = ProductSubType.Inherit;
					product.VattariffId = 3;
					product.PriceIn = null;

					product.AddToTransaction(transaction);
					product.Save();

					int newSortOrder = 1;
					if (!category.IsNew && category.ProductCategoryCollection.Any())
					{
						newSortOrder = category.ProductCategoryCollection.Max(x => x.SortOrder) + 1;
					}

					ProductCategoryEntity productCategoryEntity = new ProductCategoryEntity();
					productCategoryEntity.ParentCompanyId = this.DataSourceAsMenuEntity.CompanyId;
					productCategoryEntity.CategoryId = categoryToLinkInto;
					productCategoryEntity.ProductId = product.ProductId;
					productCategoryEntity.SortOrder = newSortOrder;
					productCategoryEntity.AddToTransaction(transaction);
					productCategoryEntity.Save();

					product.BrandProductEntity = brandproductEntity;
					product.Save();

					transaction.Commit();
					success = true;
				}
				catch (Exception ex)
				{
					transaction.Dispose();
					throw ex;
				}
			}

			if (success)
			{
				this.AddInformator(InformatorType.Information, "A new product has been created in \"{0}\" and linked to brand product \"{1}\"", category.FullCategoryName, brandproductEntity.Name);
			}
			else
			{
				this.AddInformator(InformatorType.Warning, "An error occured while creating a new linked product.");
			}
		}

		void ddlBrands_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.ddlBrands.ValidId > 0)
			{
				PredicateExpression filter = new PredicateExpression();
				filter.Add(ProductFields.BrandId == this.ddlBrands.Value.GetValueOrDefault(0));

				SortExpression sort = new SortExpression(ProductFields.Name | SortOperator.Ascending);

				ProductCollection brandproductCollection = new ProductCollection();
				brandproductCollection.GetMulti(filter, 0, sort);

				this.ddlBrandProducts.DataSource = brandproductCollection;
				this.ddlBrandProducts.DataBind();
			}
			else
			{
				this.ddlBrandProducts.DataSource = new ProductCollection();
				this.ddlBrandProducts.DataBind();
			}
		}

		private void btnPush_Click(object sender, EventArgs e)
		{
			this.Push();
		}

		public void Upload()
		{

		}

		public void Push()
		{
			string cachePushKey = "MenuPush" + this.DataSourceAsMenuEntity.CompanyId;

			if (Dionysos.Web.CacheHelper.HasValue(cachePushKey, false))
			{
				this.AddInformator(InformatorType.Warning, "A menu is already being published for this company. Please wait until it's finished");
				this.Validate();

				return;
			}

			bool forcePush = this.btnDumpEt.Checked;

			// DK: Temp hack to make sure menu is cached on webservice
			// Can be removed, changed when we have build the Emenu to use amazon
			CompanyEntity companyEntity = this.DataSourceAsMenuEntity.CompanyEntity;

			File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2}\n", DateTime.Now, companyEntity.Name, CmsSessionHelper.CurrentUser.Username));

			if (!forcePush && ScheduledCommandTaskHelper.IsScheduledCommandTaskRunning(companyEntity.CompanyId))
			{
				File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} Unable to push menu at this moment. Scheduled command task is running for this company\n", DateTime.Now, companyEntity.Name, CmsSessionHelper.CurrentUser.Username));
				this.AddInformator(InformatorType.Warning, "Unable to push menu at this moment. Scheduled command task is running for this company");
				this.Validate();

				return;
			}

			PushMenuWebservice(companyEntity);

			companyEntity.Refetch();
			this.lblTimestampMenu.Text = companyEntity.MenuDataLastModifiedUTC.ToString(CultureInfo.InvariantCulture);

			this.AddInformator(InformatorType.Information, "Menu cache has been refreshed.");
			this.Validate();
		}

		private void PushMenuWebservice(CompanyEntity companyEntity)
		{
			string cachePushKey = "MenuPush" + this.DataSourceAsMenuEntity.CompanyId;
			Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, cachePushKey, true, 60);

			PublishHelper.Publish(companyEntity.CompanyId, (service, timestamp, mac, salt) =>
			{
				string hash = string.Empty;

				// Publish the menu
				try
				{
					File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - Start menu V1: {3}\n", DateTime.Now, companyEntity.Name, CmsSessionHelper.CurrentUser.Username, this.DataSourceAsMenuEntity.Name));

					hash = Hasher.GetHashFromParameters(salt, timestamp, mac, this.DataSourceAsMenuEntity.MenuId);
					service.PublishMenu(timestamp, mac, this.DataSourceAsMenuEntity.MenuId, hash);

					File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - End menu V1: {3}\n", DateTime.Now, companyEntity.Name, CmsSessionHelper.CurrentUser.Username, this.DataSourceAsMenuEntity.Name));
				}
				catch (Exception ex)
				{
					File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - Exception: {3}\n", DateTime.Now, companyEntity.Name, CmsSessionHelper.CurrentUser.Username, ex.ToString()));
				}

				// Publish the company
				try
				{
					File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - Start company: {3}\n", DateTime.Now, companyEntity.Name, CmsSessionHelper.CurrentUser.Username, companyEntity.Name));

					hash = Hasher.GetHashFromParameters(salt, timestamp, mac, companyEntity.CompanyId);
					service.GetCompany(timestamp, mac, companyEntity.CompanyId, hash);

					File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - End company: {3}\n", DateTime.Now, companyEntity.Name, CmsSessionHelper.CurrentUser.Username, companyEntity.Name));
				}
				catch (Exception ex)
				{
					File.AppendAllText(Server.MapPath("~/App_Data/Push.txt"), string.Format("{0} - {1} - {2} - Exception: {3}\n", DateTime.Now, companyEntity.Name, CmsSessionHelper.CurrentUser.Username, ex.ToString()));
				}
			});

			Dionysos.Web.CacheHelper.Remove(false, cachePushKey);

			this.AddInformator(InformatorType.Information, "Menu has been pushed, please give it 5 to 10 minutes before restarting clients.");
			this.Validate();

			SetMenuWebserviceTimestamp();
		}

		#endregion

		#region Properties

		public MenuEntity DataSourceAsMenuEntity
		{
			get
			{
				return this.DataSource as MenuEntity;
			}
		}

		#endregion

		#region Event handlers

		protected override void OnInit(EventArgs e)
		{
			this.LoadUserControls();
			this.DataSourceLoaded += this.Menu_DataSourceLoaded;
			base.OnInit(e);
		}

		private void Menu_DataSourceLoaded(object sender)
		{
			this.SetGui();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			this.HookUpEvents();
		}

		private void btnCopyMenu_Click(object sender, EventArgs e)
		{
			this.CopyMenu();
		}

		private void btnDownloadMenu_Click(object sender, EventArgs e)
		{
			this.DownloadMenu();
		}

		private void btnAddBatch_Click(object sender, EventArgs e)
		{
			this.Response.Redirect("~/Catalog/CategoryBatch.aspx?menuId=" + this.DataSourceAsMenuEntity.MenuId, false);
		}

		protected new void Page_Unload(object sender, EventArgs e)
		{
			//Server.ScriptTimeout = timeOut;

			base.Page_Unload(sender, e);
		}

		#endregion

		#region Logging

		private static readonly string ExistsKey = "CopyMenuExists" + CmsSessionHelper.CurrentUser.UserId;
		private static readonly string LogKey = "CopyMenuLog" + CmsSessionHelper.CurrentUser.UserId;
		private static bool endSessionOnGetLog = false;

		[WebMethod(EnableSession = true), ScriptMethod]
		public static string GetLog()
		{
			string log = string.Empty;
			if (Menu.LogExists())
			{
				log = (string)HttpContext.Current.Application.Get(Menu.LogKey);

				if (Menu.endSessionOnGetLog)
				{
					Menu.endSessionOnGetLog = false;
					Menu.EndSession();
				}
			}
			return log;
		}

		[WebMethod(EnableSession = true), ScriptMethod]
		public static bool SessionExists()
		{
			return (HttpContext.Current.Application.Get(Menu.ExistsKey) != null);
		}

		[WebMethod(EnableSession = true), ScriptMethod]
		public static bool LogExists()
		{
			return (HttpContext.Current.Application.Get(Menu.LogKey) != null);
		}

		public static void AppendLog(string log, params object[] args)
		{
			if (Menu.SessionExists())
			{
				HttpContext.Current.Application[Menu.LogKey] += string.Format(log + "\n", args);
			}
		}

		public static void StartSession()
		{
			HttpContext.Current.Application[Menu.LogKey] = string.Empty;
			HttpContext.Current.Application[Menu.ExistsKey] = true;
		}

		public static void EndSessionOnGetLog()
		{
			Menu.endSessionOnGetLog = true;
		}

		public static void EndSession()
		{
			if (Menu.SessionExists())
			{
				HttpContext.Current.Application.Remove(Menu.ExistsKey);
			}
		}

		#endregion
	}
}
