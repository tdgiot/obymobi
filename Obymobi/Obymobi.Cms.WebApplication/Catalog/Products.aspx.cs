﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using System.Drawing;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Products : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Product";
            this.EntityPageUrl = "~/Catalog/Product.aspx";
            
            base.OnInit(e);

            int visibleProducts;
            if (QueryStringHelper.TryGetValue("visibleproducts", out visibleProducts))
            {
                if (visibleProducts == 1)
                    this.cbOnlyVisibleProducts.Checked = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                filter.Add(ProductFields.Type == ProductType.Product);
                filter.Add(ProductFields.SubType != ProductSubType.SystemProduct);

                if (this.cbOnlyVisibleProducts.Checked)
                    filter.Add(ProductFields.VisibilityType != VisibilityType.Never);

                datasource.FilterToUse = filter;
            }

            if (this.MainGridView != null)
            {
                this.MainGridView.HtmlRowPrepared += new DevExpress.Web.ASPxGridViewTableRowEventHandler(MainGridView_HtmlRowPrepared);
            }
        }

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;
            bool isVisible = (bool)e.GetValue("Visible");
            bool hasCategory = (bool)e.GetValue("HasCategory");
            if (!isVisible || !hasCategory)
                e.Row.ForeColor = Color.Gray;
        }

        public void Refresh()
        {
            QueryStringHelper qs = new QueryStringHelper();

            if (this.cbOnlyVisibleProducts.Checked)
                qs.AddItem("visibleproducts", "1");
            else
                qs.AddItem("visibleproducts", "0");

            WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        #endregion
    }
}
