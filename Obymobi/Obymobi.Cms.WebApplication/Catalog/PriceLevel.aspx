﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.PriceLevel" Codebehind="PriceLevel.aspx.cs" %>
<%@ Reference VirtualPath="~/Catalog/SubPanels/PriceLevelItemsPanel.ascx" %>
<%@ Reference Control="~/Catalog/SubPanels/PriceLevelItemsPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<Controls>
                    <D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>	                              
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>
                        </tr>                        
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblItems">Products</D:Label>
                            </td>
                            <td class="control" colspan="3">
                                <div style="width: 736px;">
                                    <D:PlaceHolder runat="server" ID="plhItems"></D:PlaceHolder>
                                </div>
                            </td>
                        </tr>							                  
                    </table>
                    </D:PlaceHolder>   
                </Controls>                    
            </X:TabPage>            
            <X:TabPage Text="Copy" Name="Copy">
                <Controls>
					<table class="dataformV2">
					    <tr>
					        <td class="label">
					            <D:Label runat="server" ID="lblCopyToName">Name</D:Label>
					        </td>
                            <td class="control">
					            <D:TextBoxString runat="server" ID="tbCopyToName" />
					        </td>
                            <td class="label">
                                
                            </td>
                            <td class="control">
                                <D:Button runat="server" ID="btCopyPriceLevel" Text="Copy"/>
                            </td>
					    </tr>
                    </table>
                </Controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>