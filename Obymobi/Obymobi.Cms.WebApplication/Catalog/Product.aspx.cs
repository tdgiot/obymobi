using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;
using Dionysos;
using Dionysos.Interfaces;
using Dionysos.Reflection;
using Dionysos.Web;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Dionysos.Web.UI.UltraControls;
using Dionysos.Web.UI.WebControls;
using MarkdownSharp.Extensions;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Cms.Logic.Extensions;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.Logic.Requests;
using Obymobi.Cms.Logic.UseCases;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.Comparers;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.Media;
using SD.LLBLGen.Pro.ORMSupportClasses;
using CheckBox = System.Web.UI.WebControls.CheckBox;
using Panel = System.Web.UI.WebControls.Panel;
using TabPage = DevExpress.Web.TabPage;
using TextBox = Dionysos.Web.UI.WebControls.TextBox;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Product : PageLLBLGenEntity
    {
        // Panels
        private SubPanels.CategoryPanel categoryPanel = null;
        private Generic.UserControls.CustomTextCollection translationsPanel = null;
        private IAttachmentCollection attachmentCollection = null;

        private int ParentBrandId
        {
            get
            {
                if (this.DataSourceAsProductEntity.BrandId.HasValue)
                {
                    this.ViewState["BrandId"] = this.DataSourceAsProductEntity.BrandId.Value;
                }

                return Convert.ToInt32((this.ViewState["BrandId"] as int?) ?? (this.ViewState["BrandId"] = QueryStringHelper.GetValue<int>("BrandId")));
            }
        }

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            this.DataSourceLoaded += Product_DataSourceLoaded;
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            if (CmsSessionHelper.CurrentRole < Role.Administrator)
            {
                this.btnCreateGenericProduct.Visible = false;
            }

            if (CmsSessionHelper.CurrentRole >= Role.Reseller)
            {
                this.translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx") as Generic.UserControls.CustomTextCollection;
                this.translationsPanel.FullWidth = true;
            }
            else
            {
                this.tabsMain.TabPages.FindByName("tabAdvanced").Visible = false;
                this.tabsMain.TabPages.FindByName("Info").Visible = false;
            }

            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
            if (this.PageMode != PageMode.Add)
            {
                this.attachmentCollection = this.tabsMain.AddTabPage("Bijlagen", "Attachments", "~/Generic/SubPanels/AttachmentCollection.ascx") as IAttachmentCollection;
            }

            this.categoryPanel = this.tabsMain.AddTabPage("Categories", "Categories", "~/Catalog/SubPanels/CategoryPanel.ascx") as Catalog.SubPanels.CategoryPanel;

            this.tabsMain.AddTabPage("Suggestions", "Suggestions", "~/Catalog/SubPanels/ProductSuggestionPanel.ascx");

            // Tags permissions
            this.tagBox.ReadOnly = CmsSessionHelper.CurrentRole == Role.Console;
            this.tagBox.AllowCustomTokens = CmsSessionHelper.CurrentRole >= Role.Supervisor;

            this.cbPointOfInterestId.DataBindEntityCollection<PointOfInterestEntity>(null, PointOfInterestFields.Name);
            this.cbCoversType.DataBindEnumValues(CoversType.AlwaysAsk, CoversType.NeverAsk, CoversType.Inherit);
        }

        public override void Validate()
        {
            ValidateAlterations("ddlProductAlterationV1_");
            ValidateAlterations("ddlProductAlterationV2_");

            base.Validate();
        }

        private void ValidateAlterations(string prefix)
        {
            IEnumerable<Control> alterationControls = this.ControlList.Where(ctrl => !ctrl.ID.IsNullOrWhiteSpace() && ctrl.ID.StartsWith(prefix));
            List<int> selectedAlterationIds = new List<int>();
            foreach (Control ctrl in alterationControls)
            {
                ComboBoxInt ddl = (ComboBoxInt)ctrl;
                if (ddl.ValidId > 0)
                {
                    if (!selectedAlterationIds.Contains(ddl.ValidId))
                    {
                        selectedAlterationIds.Add(ddl.ValidId);
                    }
                    else
                    {
                        // User tries to set the same alteration multiple times, display a proper message
                        this.MultiValidatorDefault.AddError(Translate("ChosenAlterationAlreadySetForProduct", "Het is niet mogelijk om dezelfde alteration meer dan 1 keer in te stellen"));
                        break;
                    }
                }
            }
        }

        private bool ValidateAssignedPointOfInterest()
        {
            if (this.cbPointOfInterestId.Value > 0)
            {
                ProductCollection products = new ProductCollection();
                PredicateExpression filter = new PredicateExpression
                {
                    ProductFields.CompanyId == this.productEntityDataSource.CompanyId,
                    ProductFields.PointOfInterestId == this.cbPointOfInterestId.Value,
                    ProductFields.ProductId != this.productEntityDataSource.ProductId
                };
                return products.GetDbCount(filter) == 0;
            }

            return true;
        }


        private bool ValidateBrandPermissions()
        {
            if (this.ParentBrandId > 0)
            {
                BrandEntity brandEntity = new BrandEntity(this.ParentBrandId);
                if (brandEntity.IsNew)
                {
                    AddInformator(InformatorType.Warning, "You do not have permissions to add products to this brand");
                    this.DataSourceAsProductEntity.BrandId = null;
                    return false;
                }

                if (this.DataSourceAsProductEntity.IsNew)
                {
                    BrandRole? role = CmsSessionHelper.GetUserRoleForBrand(this.ParentBrandId);
                    if (role.GetValueOrDefault(BrandRole.Viewer) == BrandRole.Viewer)
                    {
                        // No permission to add product to this brand
                        AddInformator(InformatorType.Warning, "You do not have permissions to add products to this brand");
                        this.DataSourceAsProductEntity.BrandId = null;
                        return false;
                    }

                    AddInformator(InformatorType.Information, "This product will be created in brand: " + brandEntity.Name);
                    this.DataSourceAsProductEntity.BrandId = this.ParentBrandId;
                    this.DataSourceAsProductEntity.Type = (int)ProductType.BrandProduct;
                }
                else
                {
                    AddInformator(InformatorType.Information, "This product is part of the brand: " + brandEntity.Name);
                }

                return true;
            }

            return false;
        }

        private bool ValidateProductTextFields()
        {
            bool isValid = true;

            if (this.tbName.Text.Length > ProductFields.Name.MaxLength)
            {
                AddInformator(InformatorType.Warning, $"Name has a maximum length of {ProductFields.Name.MaxLength} characters.");
                isValid = false;
            }

            if (this.tbButtonText.Text.Length > ProductFields.ButtonText.MaxLength)
            {
                AddInformator(InformatorType.Warning, $"{this.lblButtonText.Text} has a maximum length of {ProductFields.ButtonText.MaxLength} characters.");
                isValid = false;
            }

            if (this.tbCustomizeButtonText.Text.Length > ProductFields.CustomizeButtonText.MaxLength)
            {
                AddInformator(InformatorType.Warning, $"{this.lblCustomizeButtonText.Text} has a maximum length of {ProductFields.CustomizeButtonText.MaxLength} characters.");
                isValid = false;
            }

            if (this.tbWebTypeTabletUrl.Text.Length > ProductFields.WebTypeTabletUrl.MaxLength)
            {
                AddInformator(InformatorType.Warning, $"{this.lblWebTypeTabletUrl.Text} has a maximum length of {ProductFields.WebTypeTabletUrl.MaxLength} characters.");
                isValid = false;
            }

            if (this.tbShortDescription.Text.Length > ProductFields.ShortDescription.MaxLength)
            {
                AddInformator(InformatorType.Warning, $"Short description has a maximum length of {ProductFields.ShortDescription.MaxLength} characters.");
                isValid = false;
            }

            return isValid;
        }

        private void RenderProductFields(ProductEntity product)
        {
            System.Drawing.Color color = product.Color > 0 ? System.Drawing.Color.FromArgb((255 << 24) | product.Color) : System.Drawing.Color.Empty;

            this.tbName.Value = product.Name;
            this.tbPriceIn.Value = product.PriceIn;
            this.tbButtonText.Value = product.ButtonText;
            this.tbCustomizeButtonText.Value = product.CustomizeButtonText;
            this.tbWebTypeTabletUrl.Value = product.WebTypeTabletUrl;
            this.tbDescription.Value = MarkdownHelper.RemoveMarkdown(product.Description);
            this.ceColor.Color = color;

            if (product.BrandProductId.HasValue)
            {
                this.tbName.Value = product.InheritName ? string.Empty : product.Name;
                this.tbPriceIn.Value = product.InheritPrice ? null : product.PriceIn;
                this.tbButtonText.Value = product.InheritButtonText ? string.Empty : product.ButtonText;
                this.tbCustomizeButtonText.Value = product.InheritCustomizeButtonText ? string.Empty : product.CustomizeButtonText;
                this.tbWebTypeTabletUrl.Value = product.InheritWebTypeTabletUrl ? string.Empty : product.WebTypeTabletUrl;
                this.tbDescription.Value = product.InheritDescription ? string.Empty : MarkdownHelper.RemoveMarkdown(product.Description);
                this.ceColor.Color = color;

                this.lblName.Style.Add("font-weight", "bold");
                this.lblSubType.Style.Add("font-weight", "bold");
                this.lblPriceIn.Style.Add("font-weight", "bold");
                this.lblButtonText.Style.Add("font-weight", "bold");
                this.lblCustomizeButtonText.Style.Add("font-weight", "bold");
                this.lblWebTypeTabletUrl.Style.Add("font-weight", "bold");
                this.lblDescription.Style.Add("font-weight", "bold");
                this.lblCOlor.Style.Add("font-weight", "bold");
                this.lblViewLayoutType.Style.Add("font-weight", "bold");
                this.lblDeliveryLocationType.Style.Add("font-weight", "bold");
            }
        }

        private void SetProductFields(ProductEntity product)
        {
            int color = this.ceColor.Color.R << 16 | this.ceColor.Color.G << 8 | this.ceColor.Color.B;

            product.Name = this.tbName.Value;
            product.PriceIn = this.tbPriceIn.Value;
            product.ButtonText = this.tbButtonText.Value;
            product.CustomizeButtonText = this.tbCustomizeButtonText.Value;
            product.WebTypeTabletUrl = this.tbWebTypeTabletUrl.Value;
            product.Description = MarkdownHelper.AddMarkdown(this.tbDescription.Text);
            product.Color = color;

            if (this.ddlBrandProductId.ValidId > 0)
            {
                product.InheritName = this.tbName.Value.IsNullOrWhiteSpace();
                product.InheritPrice = !this.tbPriceIn.Value.HasValue;
                product.InheritButtonText = this.tbButtonText.Value.IsNullOrWhiteSpace();
                product.InheritCustomizeButtonText = this.tbCustomizeButtonText.Value.IsNullOrWhiteSpace();
                product.InheritWebTypeTabletUrl = this.tbWebTypeTabletUrl.Value.IsNullOrWhiteSpace();
                product.InheritDescription = this.tbDescription.Value.IsNullOrWhiteSpace();
                product.InheritColor = color == 0;
            }
        }

        private void SetGui()
        {
            if (this.DataSourceAsProductEntity.Type == 0)
            {
                this.DataSourceAsProductEntity.Type = (int)ProductType.Product;
            }

            if (this.DataSourceAsProductEntity.SubType == (int)ProductSubType.SystemProduct)
            {
                ((MasterPages.MasterPageEntity)this.Master).ToolBar.DeleteButton.Visible = false;
                this.ddlSubType.Enabled = false;

                this.tabsMain.TabPages.FindByName("Categories").Visible = false;

                this.lblGenericproductId.Visible = false;
                this.ddlGenericproductId.Visible = false;

                this.lblBrandProductId.Visible = false;
                this.ddlBrandProductId.Visible = false;
            }

            RenderProductFields(this.DataSourceAsProductEntity);

            this.Informators.Clear();

            bool hasBrandPermission = ValidateBrandPermissions();

            if (CmsSessionHelper.CurrentRole <= Role.Reseller)
            {
                this.btUncoupleGenericProduct.Visible = false;
            }

            if (this.DataSourceAsProductEntity.Type != (int)ProductType.BrandProduct)
            {
                // Init the generic products
                GenericproductCollection genericProducts = new GenericproductCollection();
                IncludeFieldsList genericProductFields = new IncludeFieldsList
                {
                    GenericproductFields.Name
                };
                SortExpression sort = new SortExpression(GenericproductFields.Name | SortOperator.Ascending);
                PredicateExpression filter = new PredicateExpression(GenericproductFields.BrandId == DBNull.Value);
                genericProducts.GetMulti(filter, 0, sort, null, null, genericProductFields, 0, 0);
                ddlGenericproductId.Items.AddRange(genericProducts.Select(x => new ListEditItem(x.Name, x.GenericproductId)).ToList());

                List<int> brandIds = CmsSessionHelper.GetVisibleCompanyBrandsForUser(this.DataSourceAsProductEntity.CompanyId.GetValueOrDefault()).Select(x => x.BrandId).ToList();
                ProductCollection brandProducts = new ProductCollection();
                RelationCollection relationCollection = new RelationCollection(ProductEntity.Relations.BrandEntityUsingBrandId);
                PredicateExpression filter2 = new PredicateExpression(ProductFields.BrandId == brandIds);
                IncludeFieldsList brandProductFields = new IncludeFieldsList(ProductFields.ProductId, ProductFields.Name, ProductFields.BrandId, BrandFields.Name);
                SortExpression brandProductSort = new SortExpression(ProductFields.Name | SortOperator.Ascending);
                brandProducts.GetMulti(filter2, 0, brandProductSort, relationCollection, null, brandProductFields, 0, 0);
                this.ddlBrandProductId.DataSource = brandProducts;
                this.ddlBrandProductId.DataBind();
                this.ddlBrandProductId.Value = this.DataSourceAsProductEntity.BrandProductId;

                this.btUncoupleGenericProduct.Visible = this.DataSourceAsProductEntity.GenericproductId.HasValue;
                tbName.Enabled = !DataSourceAsProductEntity.IsLinkedToGeneric;

                this.btUnlinkBrandProduct.Visible = this.DataSourceAsProductEntity.BrandProductId.HasValue;

                // Init the schedules
                ScheduleCollection scheduleCollection = new ScheduleCollection();
                PredicateExpression scheduleFilter = new PredicateExpression(ScheduleFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                SortExpression scheduleSort = new SortExpression(ScheduleFields.Name | SortOperator.Ascending);
                scheduleCollection.GetMulti(scheduleFilter, 0, scheduleSort);
                ddlScheduleId.Items.AddRange(scheduleCollection.Select(x => new ListEditItem(x.Name, x.ScheduleId)).ToList());

                if (this.attachmentCollection != null)
                {
                    this.attachmentCollection.InheritFromBrand = this.DataSourceAsProductEntity.InheritAttachmentsFromBrand;
                }

                this.ddlTaxTariffId.DataBindEntityCollection<TaxTariffEntity>(
                    TaxTariffFields.CompanyId == CmsSessionHelper.CurrentCompanyId, TaxTariffFields.Name,
                    TaxTariffFields.Name);
            }
            else
            {
                BrandRole? brandRole = CmsSessionHelper.GetUserRoleForBrand(this.DataSourceAsProductEntity.BrandId.GetValueOrDefault());
                if (brandRole.GetValueOrDefault(BrandRole.Viewer) == BrandRole.Viewer)
                {
                    MasterPages.MasterPageEntity masterPage = this.Master as MasterPages.MasterPageEntity;
                    if (masterPage != null)
                    {
                        masterPage.ToolBar.SaveButton.Visible = false;
                        masterPage.ToolBar.SaveAndCloneButton.Visible = false;
                        masterPage.ToolBar.SaveAndGoButton.Visible = false;
                        masterPage.ToolBar.SaveAndNewButton.Visible = false;

                        AddInformator(InformatorType.Warning, "This brand product can not be edited due to insufficient permissions");
                    }
                }
            }

            if (this.DataSourceAsProductEntity.GenericproductId.HasValue)
            {
                this.plhGenericproduct.Visible = true;
                this.plhBrandProduct.Visible = false;

                this.tbTextColor.UseDataBinding = false;
                this.tbTextColor.Enabled = false;
                this.tbTextColor.Text = this.DataSourceAsProductEntity.GenericproductEntity.TextColor;

                this.tbBackgroundColor.UseDataBinding = false;
                this.tbBackgroundColor.Enabled = false;
                this.tbBackgroundColor.Text = this.DataSourceAsProductEntity.GenericproductEntity.BackgroundColor;

                this.tbDescriptionPlaceholder.Text = MarkdownHelper.RemoveMarkdown(this.DataSourceAsProductEntity.Description);
                this.tbGenericDescriptionPlaceholder.Text = MarkdownHelper.RemoveMarkdown(this.DataSourceAsProductEntity.GenericproductEntity.Description);

                this.ddlGenericproductId.Enabled = false;

                if (this.DataSourceAsProductEntity.IsLinkedToGeneric)
                {
                    this.ddlBrandProductId.Visible = false;
                    this.lbBrandProduct.Visible = true;

                    this.lbBrandProduct.Text = this.DataSourceAsProductEntity.GenericproductEntity.Name + " &raquo;&raquo;";

                    // Check if user has access to go to this page
                    if (CmsSessionHelper.GetUserRoleForBrand(this.DataSourceAsProductEntity.GenericproductEntity.BrandId.GetValueOrDefault()) >= BrandRole.Editor)
                    {
                        this.lbBrandProduct.NavigateUrl = ResolveUrl("~/Genericproducts/Genericproduct.aspx") + string.Format("?id={0}", this.DataSourceAsProductEntity.GenericproductId);
                    }
                }
                else
                {
                    this.ddlBrandProductId.Enabled = false;
                }

                if (this.DataSourceAsProductEntity.GenericproductEntity.GenericalterationCollectionViaGenericproductGenericalteration.Count > 0)
                {
                    this.ddlProductAlterationV1_1.Enabled = false;
                    this.ddlProductAlterationV1_2.Enabled = false;
                    this.ddlProductAlterationV1_3.Enabled = false;
                    this.ddlProductAlterationV1_4.Enabled = false;
                    this.ddlProductAlterationV1_5.Enabled = false;
                    this.ddlProductAlterationV1_6.Enabled = false;
                    this.ddlProductAlterationV1_7.Enabled = false;
                    this.ddlProductAlterationV1_8.Enabled = false;
                    this.ddlProductAlterationV1_9.Enabled = false;
                    this.ddlProductAlterationV1_10.Enabled = false;

                    this.ddlProductAlterationV2_11.Enabled = false;
                    this.ddlProductAlterationV2_12.Enabled = false;
                    this.ddlProductAlterationV2_13.Enabled = false;
                    this.ddlProductAlterationV2_14.Enabled = false;
                    this.ddlProductAlterationV2_15.Enabled = false;
                    this.ddlProductAlterationV2_16.Enabled = false;
                    this.ddlProductAlterationV2_17.Enabled = false;
                    this.ddlProductAlterationV2_18.Enabled = false;
                    this.ddlProductAlterationV2_19.Enabled = false;
                    this.ddlProductAlterationV2_20.Enabled = false;
                }

                this.tbGenericDescription.Text = this.DataSourceAsProductEntity.GenericproductEntity.Description;
                this.tabsMain.TabPages.Remove(this.tabsMain.TabPages.FindByName("Media"));
            }
            else if (this.DataSourceAsProductEntity.BrandProductId.HasValue)
            {
                this.plhGenericproduct.Visible = false;
                this.plhBrandProduct.Visible = true;

                if (this.DataSourceAsProductEntity.InheritAlterationsFromBrand)
                {
                    this.ddlProductAlterationV1_1.Enabled = false;
                    this.ddlProductAlterationV1_2.Enabled = false;
                    this.ddlProductAlterationV1_3.Enabled = false;
                    this.ddlProductAlterationV1_4.Enabled = false;
                    this.ddlProductAlterationV1_5.Enabled = false;
                    this.ddlProductAlterationV1_6.Enabled = false;
                    this.ddlProductAlterationV1_7.Enabled = false;
                    this.ddlProductAlterationV1_8.Enabled = false;
                    this.ddlProductAlterationV1_9.Enabled = false;
                    this.ddlProductAlterationV1_10.Enabled = false;

                    this.ddlProductAlterationV2_11.Enabled = false;
                    this.ddlProductAlterationV2_12.Enabled = false;
                    this.ddlProductAlterationV2_13.Enabled = false;
                    this.ddlProductAlterationV2_14.Enabled = false;
                    this.ddlProductAlterationV2_15.Enabled = false;
                    this.ddlProductAlterationV2_16.Enabled = false;
                    this.ddlProductAlterationV2_17.Enabled = false;
                    this.ddlProductAlterationV2_18.Enabled = false;
                    this.ddlProductAlterationV2_19.Enabled = false;
                    this.ddlProductAlterationV2_20.Enabled = false;
                }

                this.cbInheritAlterationsFromBrand.Visible = true;
                this.cbInheritAlterationsV3FromBrand.Visible = true;
                this.cbInheritAlterationsV3FromBrand.Checked = this.DataSourceAsProductEntity.InheritAlterationsFromBrand;

                this.lbBrandProduct.Visible = true;
                this.lbBrandProduct.Text = this.DataSourceAsProductEntity.BrandProductEntity.NameWithBrand + " &raquo;&raquo;";

                BrandRole? brandRole = CmsSessionHelper.GetUserRoleForBrand(this.DataSourceAsProductEntity.BrandProductEntity.BrandId.GetValueOrDefault());
                if (brandRole == null)
                {
                    MasterPages.MasterPageEntity masterPage = this.Master as MasterPages.MasterPageEntity;
                    if (masterPage != null)
                    {
                        masterPage.ToolBar.SaveButton.Visible = false;
                        masterPage.ToolBar.SaveAndCloneButton.Visible = false;
                        masterPage.ToolBar.SaveAndGoButton.Visible = false;
                        masterPage.ToolBar.SaveAndNewButton.Visible = false;

                        AddInformator(InformatorType.Warning, "This product can not be edited due to insufficient permissions on the linked brand");
                    }
                }
                else if (brandRole >= BrandRole.Viewer)
                {
                    this.lbBrandProduct.NavigateUrl = ResolveUrl("~/Catalog/Product.aspx") + string.Format("?id={0}", this.DataSourceAsProductEntity.BrandProductId);
                }
            }
            else
            {
                // Default to VAT 0.
                if (this.DataSourceAsProductEntity != null)
                {
                    this.DataSourceAsProductEntity.VattariffId = 3;
                }
            }

            if (CmsSessionHelper.CurrentRole >= Role.Administrator)
            {
                this.lblCreateGenericProduct.Visible = !this.DataSourceAsProductEntity.GenericproductId.HasValue && this.DataSourceAsProductEntity.Type == (int)ProductType.Product;
                this.btnCreateGenericProduct.Visible = !this.DataSourceAsProductEntity.GenericproductId.HasValue && this.DataSourceAsProductEntity.Type == (int)ProductType.Product;
            }
            this.tbGenericDescription.Visible = (this.DataSourceAsProductEntity.GenericproductId.HasValue && !this.DataSourceAsProductEntity.ManualDescriptionEnabled);
            this.lbCopyDescriptionFromGenericproduct.Visible = (this.DataSourceAsProductEntity.GenericproductId.HasValue && this.DataSourceAsProductEntity.ManualDescriptionEnabled);
            this.cbManualDescriptionEnabled.Visible = this.DataSourceAsProductEntity.GenericproductId.HasValue;
            this.lblManualDescriptionEnabled.Visible = this.DataSourceAsProductEntity.GenericproductId.HasValue;
            this.tbDescription.Placeholder = this.tbGenericDescriptionPlaceholder.Text;
            this.tbDescription.Visible = (!this.DataSourceAsProductEntity.GenericproductId.HasValue || this.DataSourceAsProductEntity.ManualDescriptionEnabled);

            this.tbShortDescription.Text = this.DataSourceAsProductEntity.ShortDescription;

            // Remove some tabs when it's not a standard product
            if (this.DataSourceAsProductEntity.IsNew || (this.DataSourceAsProductEntity.Type != (int)ProductType.Product && this.DataSourceAsProductEntity.Type != (int)ProductType.BrandProduct))
            {
                this.tabsMain.TabPages.FindByName("tabAlterations1").Visible = false;
                this.tabsMain.TabPages.FindByName("tabAlterations2").Visible = false;
                this.tabsMain.TabPages.FindByName("Suggestions").Visible = false;
                this.tabsMain.TabPages.FindByName("Media").Visible = false;

                if (this.tabsMain.TabPages.FindByName("Times") != null)
                {
                    this.tabsMain.TabPages.FindByName("Times").Visible = false;
                }

                this.lblDisplayOnHomepage.Visible = false;
                this.cbDisplayOnHomepage.Visible = false;
                this.plhNonProductFields.Visible = false;
            }

            if (this.DataSourceAsProductEntity.Type == (int)ProductType.BrandProduct)
            {
                this.plhBrandProduct.Visible = false;
                this.tabsMain.TabPages.FindByName("tabAdvanced").Visible = false;
                this.tabsMain.TabPages.FindByName("Suggestions").Visible = false;

                this.lblScheduleId.Visible = false;
                this.ddlScheduleId.Visible = false;

                this.lblSupportNotificationType.Visible = false;
                this.ddlSupportNotificationType.Visible = false;
            }

            if (CmsSessionHelper.CurrentRole < Role.Reseller)
            {
                this.tabsMain.TabPages.FindByName("tabAdvanced").ClientVisible = false;
            }

            if (this.DataSourceAsProductEntity.CompanyEntity.SystemType == SystemType.Crave || this.DataSourceAsProductEntity.Type == (int)ProductType.BrandProduct)
            {
                this.plhColors.Visible = false;
                this.lblDisplayOnHomepage.Visible = false;
                this.cbDisplayOnHomepage.Visible = false;
            }

            // Product types in ddl (GK I replaced the ints with their enum values, didn't update to the Enum DDL due to translations)
            this.ddlType.Items.Add(Translate("Product", "Product"), (int)ProductType.Product);
            if (hasBrandPermission)
            {
                this.ddlType.Items.Add(Translate("BrandProduct", "Brand Product"), (int)ProductType.BrandProduct);
            }

            if (this.DataSourceAsProductEntity.CompanyEntity.SystemType == SystemType.Otoucho)
            {
                this.ddlType.Items.Add(Translate("RequestForService", "Request for service"), (int)ProductType.Service);
                this.ddlType.Items.Add(Translate("PaymentMethod", "Payment method"), (int)ProductType.Paymentmethod);
                this.ddlType.Items.Add(Translate("Deliverypoint", "Table"), (int)ProductType.Deliverypoint);
                this.ddlType.Items.Add(Translate("Client", "Client"), (int)ProductType.Tablet);
                this.ddlType.Items.Add(Translate("PosServiceMessage", "POS service message"), (int)ProductType.PosServiceMessage);
            }

            if (this.DataSourceAsProductEntity.Type > 0)
            {
                this.ddlType.Value = this.DataSourceAsProductEntity.Type;
            }

            this.ddlType.Enabled = this.DataSourceAsProductEntity.CompanyEntity.SystemType == SystemType.Otoucho;

            // Product subtypes in ddl (GK I replaced the ints with their enum values, didn't update to the Enum DDL due to translations)
            this.ddlSubType.Items.Add(Translate("InheritFromCategory", "Inherit from category"), (int)ProductSubType.Inherit);
            this.ddlSubType.Items.Add(Translate("Normal", "Normal"), (int)ProductSubType.Normal);
            this.ddlSubType.Items.Add(Translate("ServiceItem", "Service item"), (int)ProductSubType.ServiceItems);
            this.ddlSubType.Items.Add(Translate("DirectoryItem", "Directory item"), (int)ProductSubType.Directory);
            this.ddlSubType.Items.Add(Translate("WebLink", "Web link"), (int)ProductSubType.WebLink);
            if (this.DataSourceAsProductEntity.SubTypeAsEnum == ProductSubType.SystemProduct)
            {
                this.ddlSubType.Items.Add(Translate("SystemProduct", "System product"), (int)ProductSubType.SystemProduct);
            }

            int productSubType = this.DataSourceAsProductEntity.SubType;
            if (this.DataSourceAsProductEntity.IsNew && this.DataSourceAsProductEntity.SubType > 0)
            {
                productSubType = this.DataSourceAsProductEntity.SubType;
            }
            else if (this.DataSourceAsProductEntity.GenericproductId.HasValue)
            {
                if (!this.DataSourceAsProductEntity.OverrideSubType)
                {
                    productSubType = this.DataSourceAsProductEntity.GenericproductEntity.SubType;

                    if (this.DataSourceAsProductEntity.IsLinkedToGeneric)
                    {
                        this.ddlSubType.Enabled = false;
                        this.cbOverrideSubType.Visible = false;
                    }
                    else
                    {
                        this.ddlSubType.Enabled = false;
                        this.cbOverrideSubType.Visible = true;
                    }
                }
                else
                {
                    this.cbOverrideSubType.Visible = true;
                }
            }

            this.ddlSubType.Value = productSubType;

            if (this.DataSourceAsProductEntity.BrandProductId.HasValue)
            {
                this.ddlSubType.Value = this.DataSourceAsProductEntity.BrandProductEntity.SubType;
                this.ddlSubType.Enabled = false;

                this.ddlViewLayoutType.Value = (int)this.DataSourceAsProductEntity.BrandProductEntity.ViewLayoutType;
                this.ddlViewLayoutType.Enabled = false;

                this.ddlDeliveryLocationType.Value = (int)this.DataSourceAsProductEntity.BrandProductEntity.DeliveryLocationType;
                this.ddlDeliveryLocationType.Enabled = false;
            }

            this.plhWebLink.Visible = (ProductSubType)productSubType == ProductSubType.WebLink;

            foreach (ListItem item in this.rblRateable.Items)
            {
                if (item.Text.Equals("Unknown"))
                {
                    item.Text = "Inherit";
                }
            }

            foreach (ListItem item in this.rblGeofencing.Items)
            {
                if (item.Text.Equals("Unknown"))
                {
                    item.Text = "Inherit";
                }
            }

            foreach (ListItem item in this.rblAllowFreeText.Items)
            {
                if (item.Text.Equals("Unknown"))
                {
                    item.Text = "Inherit";
                }
            }

            // Set the URL
            this.tbUrl.Text = string.Format("crave://product/{0}", this.DataSourceAsProductEntity.ProductId);

            if (this.DataSourceAsProductEntity.Type != (int)ProductType.BrandProduct)
            {
                ddlRouteId.Items.AddRange(RouteHelper.GetRouteCollection(CmsSessionHelper.CurrentCompanyId).Select(x => new ListEditItem(x.Name, x.RouteId)).ToList());
                ddlProductgroupId.Items.AddRange(ProductgroupHelper.GetProductgroups(CmsSessionHelper.CurrentCompanyId).Select(x => new ListEditItem(x.Name, x.ProductgroupId)).ToList());
            }

            // GK It's a bit strange, but make sense in a sense
            this.tbPriceIn.IsRequired = this.tbPriceIn.Visible;

            this.tbPriceIn.ReadOnly = this.DataSourceAsProductEntity.ExternalProductId.HasValue;

            // Render the product info
            RenderProductInfo();

            // Set the visibility type
            this.ddlVisibilityType.DataBindEnum<VisibilityType>();

            if (this.DataSourceAsProductEntity.GenericproductId.HasValue)
            {
                GenericproductEntity genericproductEntity = this.DataSourceAsProductEntity.GenericproductEntity;

                if (this.tbPriceIn.Visible && genericproductEntity.PriceIn.HasValue)
                {
                    this.tbPriceIn.IsRequired = false;
                    this.tbPriceIn.Placeholder = genericproductEntity.PriceIn.Value.ToString("F");
                }

                if ((ProductSubType)productSubType == ProductSubType.WebLink)
                {
                    if (!genericproductEntity.WebTypeTabletUrl.IsNullOrWhiteSpace())
                    {
                        this.tbWebTypeTabletUrl.IsRequired = false;
                        this.tbWebTypeTabletUrl.Placeholder = genericproductEntity.WebTypeTabletUrl;
                    }
                    else
                    {
                        this.tbWebTypeTabletUrl.IsRequired = true;
                    }
                }
            }
            else if (this.DataSourceAsProductEntity.BrandProductId.HasValue)
            {
                ProductEntity brandProductEntity = this.DataSourceAsProductEntity.BrandProductEntity;

                if (this.tbPriceIn.Visible && brandProductEntity.PriceIn.HasValue)
                {
                    this.tbPriceIn.IsRequired = false;
                    this.tbPriceIn.Placeholder = brandProductEntity.PriceIn.Value.ToString("F");
                }

                if ((ProductSubType)productSubType == ProductSubType.WebLink)
                {
                    if (!brandProductEntity.WebTypeTabletUrl.IsNullOrWhiteSpace())
                    {
                        this.tbWebTypeTabletUrl.IsRequired = false;
                        this.tbWebTypeTabletUrl.Placeholder = brandProductEntity.WebTypeTabletUrl;
                    }
                    else
                    {
                        this.tbWebTypeTabletUrl.IsRequired = true;
                    }
                }
            }
            else
            {
                // Determine the need for WebType Urls
                this.tbWebTypeTabletUrl.IsRequired = (ProductSubType)productSubType == ProductSubType.WebLink;
            }

            SetTags();

            this.pnlProductgroup.Visible = !this.DataSourceAsProductEntity.BrandId.HasValue;

            SetPriceLabel();

            this.ddlTaxTariffId.IsRequired = !this.DataSourceAsProductEntity.IsBrandProduct && CmsSessionHelper.CurrentCompany.IsAppLessCompany;

            this.cbIsAvailable.Enabled = !this.DataSourceAsProductEntity.ExternalProductId.HasValue;
            this.tbExternalIdentifier.Enabled = !this.DataSourceAsProductEntity.ExternalProductId.HasValue;

            SetCoversType();

            if (this.DataSourceAsProductEntity.SubTypeAsEnum == ProductSubType.WebLink)
            { SetWebLinkLabelsText(); }
        }

        private void SetCoversType()
        {
            bool isCoversVisible = CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.Covers);
            this.lblCoversType.Visible = isCoversVisible;
            this.cbCoversType.Visible = isCoversVisible;
            this.cbCoversType.IsRequired = isCoversVisible;
        }

        private void SetTags()
        {
            this.tagBox.SetSelectedTags(this.DataSourceAsProductEntity.ProductTagCollection);

            foreach (ProductCategoryEntity productCategory in this.DataSourceAsProductEntity.ProductCategoryCollection)
            {
                foreach (ProductCategoryTagEntity productCategoryTag in productCategory.ProductCategoryTagCollection)
                {
                    string path = this.DataSourceAsProductEntity.IsLinkedToMultipleMenus
                        ? productCategoryTag.CategoryEntity.FullCategoryMenuName
                        : productCategoryTag.CategoryEntity.FullCategoryName;

                    this.TagOverview.AddTagGroup(path, productCategoryTag);
                }
            }
        }

        private void SetWebLinkLabelsText()
        {
            string appLessButtonText = "AppLess button Text";
            string eMenuButtonText = "Emenu button text";
            this.lblCustomizeButtonText.Text = appLessButtonText;
            this.lblButtonText.Text = eMenuButtonText;

            if (this.translationsPanel == null)
            { return; }

            this.translationsPanel.Initialize();

            if (!this.translationsPanel.Labels.Any())
            { return; }

            foreach (ASPxLabel label in this.translationsPanel.Labels.Where(x => x.Text == "Button text"))
            { label.Text = eMenuButtonText; }

            foreach (ASPxLabel label in this.translationsPanel.Labels.Where(x => x.Text == "Alteration button text"))
            { label.Text = appLessButtonText; }
        }

        private void RenderProductInfo()
        {
            if (this.DataSourceAsProductEntity.PosproductId.HasValue)
            {
                int alterationVersion = CmsSessionHelper.AlterationDialogMode == AlterationDialogMode.v3 ? 2 : 1;

                // Render Alteration info
                ProductAlterationCollection productalterations = new ProductAlterationCollection();
                PredicateExpression filter = new PredicateExpression
                {
                    ProductAlterationFields.ProductId == this.DataSourceAsProductEntity.ProductId
                };
                productalterations.GetMulti(filter);

                AlterationCollection alterations = new AlterationCollection();
                filter = new PredicateExpression
                {
                    AlterationFields.AlterationId == productalterations.Select(x => x.AlterationId).ToList(),
                    AlterationFields.Version == alterationVersion
                };
                alterations.GetMulti(filter);

                PosalterationCollection posalterations = new PosalterationCollection();
                filter = new PredicateExpression
                {
                    PosalterationFields.PosalterationId == alterations.Select(x => x.PosalterationId).ToList()
                };
                posalterations.GetMulti(filter);

                AlterationitemCollection alterationitems = new AlterationitemCollection();
                filter = new PredicateExpression
                {
                    AlterationitemFields.AlterationId == productalterations.Select(x => x.AlterationId).ToList(),
                    AlterationitemFields.Version == alterationVersion
                };
                alterationitems.GetMulti(filter);

                AlterationoptionCollection alterationoptions = new AlterationoptionCollection();
                filter = new PredicateExpression
                {
                    AlterationoptionFields.AlterationoptionId == alterationitems.Select(x => x.AlterationoptionId).ToList(),
                    AlterationoptionFields.Version == alterationVersion
                };
                alterationoptions.GetMulti(filter);

                PosalterationoptionCollection posalterationoptions = new PosalterationoptionCollection();
                filter = new PredicateExpression
                {
                    PosalterationoptionFields.PosalterationoptionId == alterationoptions.Select(x => x.PosalterationoptionId).ToList()
                };
                posalterationoptions.GetMulti(filter);

                // Info page

                StringBuilder infoString = new StringBuilder();

                if (productalterations.Count <= 0)
                {
                    infoString.AppendFormatLine(Translate("lblNoInfo", "Er is geen kassa informatie voor dit product."));
                    this.plhAlterations.AddHtml(infoString.ToString());
                }
                else
                {
                    foreach (ProductAlterationEntity productalteration in productalterations)
                    {
                        infoString = new StringBuilder();

                        // Create table
                        infoString.AppendFormatLine("<table class=\"dataformV2\">");

                        // Create first row as header
                        infoString.AppendFormatLine("<tr>");
                        infoString.AppendFormatLine("   <td class='label'>");
                        infoString.AppendFormatLine("       <D:LabelTextOnly runat=\"server\" ID=\"lblAlterations\">{0}</D:LabelTextOnly>", Translate("lblAlterationsCaption", "Productsamenstelling / opties"));
                        infoString.AppendFormatLine("   </td>");
                        infoString.AppendFormatLine("   <td  class='label'>");
                        infoString.AppendFormatLine("      <D:LabelTextOnly runat=\"server\" ID=\"lblImported\">{0}</D:LabelTextOnly>", Translate("lblConnectedToPos", "Verbonden met kassa"));
                        infoString.AppendFormatLine("   </td>");
                        infoString.AppendFormatLine("   <td  class='label'>");
                        infoString.AppendFormatLine("       <D:LabelTextOnly runat=\"server\" ID=\"lblAlterationOrAlterationOptionExternalId\">{0}</D:LabelTextOnly>", Translate("lblExternalId", "Externe Id"));
                        infoString.AppendFormatLine("   </td>");
                        infoString.AppendFormatLine("   <td  class='label'>");
                        infoString.AppendFormatLine("   </td>");
                        infoString.AppendFormatLine("</tr>");

                        // Get the Posalteration
                        AlterationEntity alteration = alterations.FirstOrDefault(x => x.AlterationId == productalteration.AlterationId);

                        PosalterationEntity posalteration = null;
                        if (alteration != null && alteration.PosalterationId.HasValue)
                        {
                            posalteration = posalterations.FirstOrDefault(x => x.PosalterationId == alteration.PosalterationId);
                        }

                        if (posalteration == null)
                        {
                            string error = Translate("PosalterationMissing", "De Posalteration met PosalterationId: '{0}' ontbreekt");
                            infoString.AppendFormatLine("<tr><td colspan=\"3\">{0}</td></tr>", error);
                            continue;
                        }

                        string alterationText = string.Format("<a href=\"Alteration.aspx?id={0}\">{1}</a>", alteration.AlterationId, alteration.Name);

                        string importedText = "-";
                        if (posalteration != null)
                        {
                            importedText = string.Format("<a href=\"Posalteration.aspx?id={0}\">{1}</a>", posalteration.PosalterationId, posalteration.Name);
                        }

                        Panel pnlAlteration = new Panel
                        {
                            ID = "pnlAlteration-" + alteration.AlterationId,
                            GroupingText = Translate("Alteration", "Productsamenstelling") + ": " + alteration.Name
                        };
                        this.plhAlterations.Controls.Add(pnlAlteration);

                        // Render the Alteration
                        infoString.AppendFormatLine("<tr><td class='label productInfoAlteration'>{0}</td><td class='label productInfoAlteration'>{1}</td><td class='label productInfoAlteration'>{2}</td><td class='label'></td></tr>", alterationText, importedText, posalteration.ExternalId);

                        // Render it's options
                        // First get the alterationitems
                        IEnumerable<AlterationitemEntity> items = alterationitems.Where(x => x.AlterationId == alteration.AlterationId);

                        foreach (AlterationitemEntity alterationitem in items)
                        {
                            // Get the posalterationoptions based on 
                            AlterationoptionEntity alterationoption = alterationoptions.FirstOrDefault(x => x.AlterationoptionId == alterationitem.AlterationoptionId);

                            if (alterationoption == null)
                            {
                                string error = Translate("AlterationoptionMissing", "De Alterationoption met ExterneId: '{0}' ontbreekt");
                                infoString.AppendFormatLine("<tr><td colspan=\"3\">{0}</td></tr>", error);
                                continue;
                            }

                            string alterationoptionText = string.Format("<a href=\"Alterationoption.aspx?id={0}\">{1}</a>", alterationoption.AlterationoptionId, alterationoption.Name);

                            string posalterationOptionImportedText = "-";
                            string posalterationOptionImportedExternalId = "-";
                            if (alterationoption.PosalterationoptionId.HasValue)
                            {
                                PosalterationoptionEntity posalterationoption = posalterationoptions.FirstOrDefault(x => x.PosalterationoptionId == alterationoption.PosalterationoptionId);
                                if (posalterationoption != null)
                                {
                                    posalterationOptionImportedText = string.Format("<a href=\"Posalterationoption.aspx?id={0}\">{1}</a>", posalterationoption.PosalterationoptionId, posalterationoption.Name);
                                    posalterationOptionImportedExternalId = posalterationoption.ExternalId;
                                }
                            }

                            // Render the Alterationoption
                            infoString.AppendFormatLine("<tr><td class='label'>{0}</td><td class='label'>{1}</td><td class='label'>{2}</td><td></td></tr>", alterationoptionText, posalterationOptionImportedText, posalterationOptionImportedExternalId);
                        }

                        infoString.AppendFormatLine("</table>");

                        this.plhAlterations.AddHtml(infoString.ToString());
                    }
                }
            }

            if (this.DataSourceAsProductEntity.BrandId.HasValue)
            {
                this.pnlLinkedProducts.Visible = true;

                StringBuilder sb = new StringBuilder();
                if (this.DataSourceAsProductEntity.BrandProductCollection.Count == 0)
                {
                    sb.Append("No products are linked to this brand product.");
                }
                else
                {
                    // Create table
                    sb.AppendFormatLine("<table class=\"dataformV2\">");

                    // Create first row as header
                    sb.AppendFormatLine("<tr>");
                    sb.AppendFormatLine("   <td class='control'><b>{0}</b></td>", "Product Name");
                    sb.AppendFormatLine("   <td class='control'><b>{0}</b></td>", "Company");
                    sb.AppendFormatLine("   <td class='label'></td>");
                    sb.AppendFormatLine("   <td class='label'></td>");
                    sb.AppendFormatLine("</tr>");

                    foreach (ProductEntity productEntity in this.DataSourceAsProductEntity.BrandProductCollection)
                    {
                        string companyName = "<Hidden>";
                        try
                        {
                            companyName = productEntity.CompanyEntity.Name;
                        }
                        catch
                        { }

                        sb.AppendFormatLine("<tr>");
                        sb.AppendFormatLine("   <td class='control'><a href='Product.aspx?id={1}' target='_blank'>{0} ({1})</td>", productEntity.Name, productEntity.ProductId);
                        sb.AppendFormatLine("   <td class='control'>{0}</td>", companyName);
                        sb.AppendFormatLine("   <td class='label'></td>");
                        sb.AppendFormatLine("   <td class='label'></td>");
                        sb.AppendFormatLine("</tr>");
                    }

                    sb.Append("</table>");
                }

                this.plhLinkedProducts.AddHtml(sb.ToString());
            }

            RenderAlterationoptionsLinkedToProduct(this.DataSourceAsProductEntity);
        }

        private void RenderAlterationoptionsLinkedToProduct(ProductEntity product)
        {
            if (product.AlterationoptionCollection.Any())
            {
                this.plhLinkedAlterationoptions.Visible = true;

                StringBuilder sb = new StringBuilder();
                sb.AppendFormatLine("<table class=\"dataformV2\">");

                // Create first row as header 
                sb.AppendFormatLine("<tr>");
                sb.AppendFormatLine("   <td class='control'><b>{0}</b></td>", "Alterationoption Name");
                sb.AppendFormatLine("   <td class='label'></td>");
                sb.AppendFormatLine("   <td class='label'></td>");
                sb.AppendFormatLine("</tr>");

                foreach (AlterationoptionEntity alterationoption in product.AlterationoptionCollection)
                {
                    sb.AppendFormatLine("<tr>");
                    sb.AppendFormatLine("   <td class='control'><a href='Alterationoption.aspx?id={1}' target='_blank'>{0} ({1})</td>", alterationoption.Name, alterationoption.AlterationoptionId);
                    sb.AppendFormatLine("   <td class='label'></td>");
                    sb.AppendFormatLine("   <td class='label'></td>");
                    sb.AppendFormatLine("</tr>");
                }

                sb.Append("</table>");

                this.plhLinkedAlterationoptions.AddHtml(sb.ToString());
            }
        }

        private void SetFilters()
        {
            if (this.DataSourceAsProductEntity.Type != (int)ProductType.BrandProduct)
            {
                SetPosproductFilter();
                SetExternalProducts();
            }

            CategoryCollection parentCategories = new CategoryCollection();
            if (!this.DataSourceAsProductEntity.IsNew)
            {
                List<ProductCategoryEntity> productCategories = this.DataSourceAsProductEntity.ProductCategoryCollection.OrderBy(pc => pc.SortOrder).ToList();
                if (productCategories.Count > 0)
                {
                    List<int> categoryIds = productCategories.Select(pc => pc.CategoryId).ToList();
                    parentCategories = Obymobi.Cms.Logic.HelperClasses.CategoryHelper.GetCategoriesWithPrefetchedParents(categoryIds);
                }
            }

            if (this.DataSourceAsProductEntity.BrandProductId.HasValue)
            {
                RenderProductBrandAlterations(this.DataSourceAsProductEntity.BrandProductEntity, 1, this.plhInheritedAlterationsV1);
                RenderProductBrandAlterations(this.DataSourceAsProductEntity.BrandProductEntity, 2, this.plhInheritedAlterationsV2);
            }

            SetProductAlterations(parentCategories, 1, "ddlProductAlterationV1_", this.plhInheritedAlterationsV1);
            SetProductAlterations(parentCategories, 2, "ddlProductAlterationV2_", this.plhInheritedAlterationsV2);

            this.ddlViewLayoutType.DataBindEnum<ViewLayoutType>();
            this.ddlDeliveryLocationType.DataBindEnum<DeliveryLocationType>();
            this.ddlSupportNotificationType.DataBindEnum<SupportNotificationType>();
        }


        private void SetPosproductFilter()
        {
            RelationCollection relations = new RelationCollection();
            relations.Add(PosproductEntity.Relations.ProductEntityUsingPosproductId, JoinHint.Left);

            PredicateExpression filter = new PredicateExpression(PosproductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.AddWithAnd(ProductFields.ProductId == DBNull.Value);
            filter.AddWithOr(ProductFields.ProductId == DataSourceAsProductEntity.ProductId);

            SortExpression sort = new SortExpression(new SortClause(PosproductFields.Name, SortOperator.Ascending));
            IncludeFieldsList includeFields = new IncludeFieldsList { PosproductFields.Name, PosproductFields.ExternalId, PosproductFields.RevenueCenter };

            PosproductCollection posproductCollection = new PosproductCollection();

            posproductCollection.GetMulti(filter, 0, sort, relations, null, includeFields, 0, 0);

            foreach (PosproductEntity posproductEntity in posproductCollection)
            {
                AddPosproductIdComboOption(posproductEntity);
            }
        }

        private void AddPosproductIdComboOption(PosproductEntity posproduct)
        {
            if (!string.IsNullOrWhiteSpace(posproduct.RevenueCenter))
            {
                this.ddlPosproductId.Items.Add($"{posproduct.Name} ({posproduct.ExternalId}) [{posproduct.RevenueCenter}]", posproduct.PosproductId);
            }
            else
            {
                this.ddlPosproductId.Items.Add($"{posproduct.Name} ({posproduct.ExternalId})", posproduct.PosproductId);
            }
        }

        private void SetExternalProducts()
        {
            ExternalProductDataSource dataSource = new ExternalProductDataSource();
            this.ddlExternalProductId.DataSource = dataSource.GetDropdownDataSource(this.DataSourceAsProductEntity.CompanyId.GetValueOrDefault(CmsSessionHelper.CurrentCompanyId), ExternalProductType.Product);
            this.ddlExternalProductId.ValueField = nameof(ExternalProductFields.ExternalProductId);
            this.ddlExternalProductId.TextField = nameof(ExternalProductEntity.CombinedNameWithMenuAndSystemName);
            this.ddlExternalProductId.DataBind();
        }

        private void RenderProductBrandAlterations(ProductEntity brandProductEntity, int version, Dionysos.Web.UI.WebControls.PlaceHolder placeHolder)
        {
            EntityView<ProductAlterationEntity> paView = brandProductEntity.ProductAlterationCollection.DefaultView;
            paView.Filter = new PredicateExpression(ProductAlterationFields.Version == version);

            if (paView.Count == 0)
            {
                return;
            }

            StringBuilder builder = new StringBuilder();
            builder.AppendFormatLine("<table class=\"dataformV2\">");
            builder.AppendFormatLine("<tr>");
            builder.AppendFormatLine("   <td class='control'>");
            builder.AppendFormatLine("       <b>{0}</b>", Translate("lblAlterationBrandProductHeader", "Brand product alterations"));
            builder.AppendFormatLine("   </td>");
            builder.AppendFormatLine("   <td class='label'>");
            builder.AppendFormatLine("   </td>");
            builder.AppendFormatLine("</tr>");

            // Get the alterations from a parent category
            foreach (ProductAlterationEntity inheritedAlteration in paView)
            {
                string alterationLink = string.Format("<a href=\"Alteration.aspx?id={0}\">{1}</a>", inheritedAlteration.AlterationId, inheritedAlteration.AlterationEntity.Name);

                // Create row for an alteration
                builder.AppendFormatLine("<tr>");
                builder.AppendFormatLine("   <td class='control'>");
                builder.AppendFormatLine("       {0}", alterationLink);
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("   <td class='label'>");
                builder.AppendFormatLine("   </td>");
                builder.AppendFormatLine("</tr>");
            }

            builder.AppendFormatLine("</table>");
            placeHolder.AddHtml(builder.ToString());
        }

        private void SetProductAlterations(CategoryCollection parentCategories, int version, string prefix, Dionysos.Web.UI.WebControls.PlaceHolder placeHolder)
        {
            PredicateExpression filterAlterations = new PredicateExpression();
            if (this.DataSourceAsProductEntity.BrandId.HasValue)
            {
                filterAlterations.Add(AlterationFields.BrandId == this.DataSourceAsProductEntity.BrandId.Value);
            }
            else
            {
                filterAlterations.Add(AlterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            }
            filterAlterations.Add(AlterationFields.Type == AlterationHelper.GetSupportedAlterationTypes(version < 2 ? AlterationDialogMode.v2 : AlterationDialogMode.v3));
            filterAlterations.Add(AlterationFields.ParentAlterationId == DBNull.Value);
            filterAlterations.Add(AlterationFields.Version == version);

            SortExpression sort = new SortExpression(AlterationFields.Name | SortOperator.Ascending);

            RelationCollection relations = new RelationCollection();

            //Only search for Alterations which have a POS Alteration linked if the Company has no Simphony Terminals configured
            if (!this.DataSourceAsProductEntity.BrandId.HasValue && 
                this.DataSourceAsProductEntity.CompanyId.HasValue && 
                this.DataSourceAsProductEntity.CompanyEntity.TerminalCollection.Count > 0 && 
                this.DataSourceAsProductEntity.CompanyEntity.TerminalCollection.All(t => t.POSConnectorTypeEnum != POSConnectorType.SimphonyGen1) && 
                this.DataSourceAsProductEntity.PosproductId.HasValue)
            {

                filterAlterations.Add(PosalterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                filterAlterations.Add(PosproductPosalterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

                relations.Add(AlterationEntity.Relations.PosalterationEntityUsingPosalterationId);
                relations.Add(new EntityRelation(PosalterationFields.ExternalId, PosproductPosalterationFields.PosalterationExternalId, RelationType.ManyToOne));
                relations.Add(new EntityRelation(PosproductFields.ExternalId, PosproductPosalterationFields.PosproductExternalId, RelationType.ManyToOne));
            }

            IncludeFieldsList includeFields = new IncludeFieldsList
            {
                AlterationFields.AlterationId,
                AlterationFields.Name
            };

            AlterationCollection alterations = new AlterationCollection();
            alterations.GetMulti(filterAlterations, 0, sort, relations, null, includeFields, 0, 0);

            EntityView<ProductAlterationEntity> paView = this.DataSourceAsProductEntity.ProductAlterationCollection.DefaultView;

            // For backwards compatability, set SortOrders when not set			
            #region Fix for backwards compatibility
            bool fix = false;
            foreach (ProductAlterationEntity pa in paView)
            {
                if (pa.SortOrder == 0)
                {
                    fix = true;
                    break;
                }
            }

            if (fix)
            {
                int sortOrder = 1;
                foreach (ProductAlterationEntity pa in paView)
                {
                    pa.SortOrder = sortOrder;
                    pa.Save();
                    sortOrder++;
                }
            }
            #endregion

            foreach (Control control in this.ControlList)
            {
                if (!control.ID.IsNullOrWhiteSpace() && control.ID.StartsWith(prefix))
                {
                    BindProductAlterations((ComboBoxInt)control, alterations, paView);
                }
            }

            paView.Filter = null;
            if (paView.Count <= 0 || version == 2)
            {
                // Get alterations from parent categories                 
                CategoryAlterationCollection parentCategoryAlterations = GetParentCategoryAlterations(parentCategories, version);
                if (parentCategoryAlterations.Count > 0)
                {
                    System.Text.StringBuilder builder = new System.Text.StringBuilder();
                    builder.AppendFormatLine("<table class=\"dataformV2\">");
                    builder.AppendFormatLine("<tr>");
                    builder.AppendFormatLine("   <td class='label'>");
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("   <td class='control'>");
                    builder.AppendFormatLine("       {0}", Translate("lblAlterationHeader", "Alteration"));
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("   <td class='control'>");
                    builder.AppendFormatLine("      {0}", Translate("lblCategoryHeader", "Categorie"));
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("   <td class='label'>");
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("</tr>");

                    // Get the alterations from a parent category
                    for (int i = 0; i < parentCategoryAlterations.Count; i++)
                    {
                        CategoryAlterationEntity inheritedAlteration = parentCategoryAlterations[i];

                        string alterationLink = string.Format("<a href=\"{0}\">{1}</a>", inheritedAlteration.AlterationEntity.GetCmsPage(), inheritedAlteration.AlterationEntity.Name);
                        string categoryLink = string.Format("<a href=\"Category.aspx?id={0}\">{1}</a>", inheritedAlteration.CategoryId, inheritedAlteration.CategoryEntity.Name);

                        // Create row for an alteration
                        builder.AppendFormatLine("<tr>");
                        builder.AppendFormatLine("   <td class='label' style=\"padding-top:5px\">");
                        builder.AppendFormatLine("       {0}. ", (i + 1));
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("   <td class='control'>");
                        builder.AppendFormatLine("       {0}", alterationLink);
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("   <td class='control'>");
                        builder.AppendFormatLine("      {0}", categoryLink);
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("   <td class='label'>");
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("</tr>");
                    }

                    builder.AppendFormatLine("</table>");
                    placeHolder.AddHtml(builder.ToString());
                }
            }
        }

        private CategoryAlterationCollection GetParentCategoryAlterations(CategoryCollection parentCategories, int version)
        {
            CategoryAlterationCollection categoryAlterations = new CategoryAlterationCollection();
            if (!this.DataSourceAsProductEntity.IsNew)
            {
                // Get parent categories with prefetched parents
                foreach (CategoryEntity parentCategory in parentCategories)
                {
                    Obymobi.Cms.Logic.HelperClasses.CategoryHelper.AddAlterationsFromCategories(categoryAlterations, parentCategory, version);
                }
            }
            return categoryAlterations;
        }

        private void BindProductAlterations(ComboBoxInt ddl, AlterationCollection alterations, EntityView<ProductAlterationEntity> paView)
        {
            ddl.Items.AddRange(alterations.Select(x => new ListEditItem(x.Name, x.AlterationId)).ToList());

            // Check if this category has a selected alteration for this position
            int sortOrder;
            int version;

            if (ddl.ID.StartsWith("ddlProductAlterationV1_"))
            {
                sortOrder = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(ddl.ID, "ddlProductAlterationV1_"));
                version = 1;
            }
            else
            {
                sortOrder = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(ddl.ID, "ddlProductAlterationV2_"));
                version = 2;
            }

            paView.Filter = new PredicateExpression(ProductAlterationFields.SortOrder == sortOrder);

            if (paView.Count == 1 && paView[0].Version == version)
            {
                ddl.Value = paView[0].AlterationId;
            }
        }

        public CategoryCollection GetCategoriesForMenu(int menuId)
        {
            CategoryCollection cats = new CategoryCollection();

            PredicateExpression filter = new PredicateExpression
            {
                CategoryFields.MenuId == menuId,
                CategoryFields.ParentCategoryId == DBNull.Value
            };

            SortExpression sort = new SortExpression
            {
                CategoryFields.SortOrder | SortOperator.Ascending,
                CategoryFields.Name | SortOperator.Ascending
            };

            PrefetchPath path = new PrefetchPath(EntityType.CategoryEntity);
            path.Add(CategoryEntity.PrefetchPathChildCategoryCollection).SubPath.Add(CategoryEntity.PrefetchPathChildCategoryCollection).SubPath.Add(CategoryEntity.PrefetchPathChildCategoryCollection).SubPath.Add(CategoryEntity.PrefetchPathChildCategoryCollection);

            cats.GetMulti(filter, 0, sort, null, path);

            return cats;
        }

        private void SetWarnings()
        {
            if (this.DataSourceAsProductEntity.Type == (int)ProductType.Product)
            {
                if (this.DataSourceAsProductEntity.GenericproductId.HasValue)
                {
                    if (this.DataSourceAsProductEntity.IsLinkedToGeneric)
                    {
                        AddInformatorInfo("This product is linked to a brand product and therefore all fields cannot be edited.");
                    }
                    else
                    {
                        // Generic product
                        AddInformatorInfo("This is a default product and therefore has restricted input fields.");
                    }

                    if (this.DataSourceAsProductEntity.GenericproductEntity.AttachmentCollection.Count > 0)
                    {
                        AddInformatorInfo("This product inherits attachments from generic product.");
                    }
                }
                else if (this.DataSourceAsProductEntity.BrandProductId.HasValue)
                {
                    AddInformatorInfo("This product is linked to a brand product");
                }

                // POS Warning
                if (this.DataSourceAsProductEntity.CompanyEntity.GetPOSConnectorType() != POSConnectorType.Unknown
                    && !this.DataSourceAsProductEntity.BrandId.HasValue && !this.DataSourceAsProductEntity.PosproductId.HasValue
                    && !this.DataSourceAsProductEntity.CompanyEntity.IncludeMenuItemsNotExternallyLinked)
                {
                    this.MultiValidatorDefault.AddWarning("The product is not linked to a PoS product.");
                }

                // Categories
                if (this.DataSourceAsProductEntity.ProductCategoryCollection.Count == 0)
                {
                    this.MultiValidatorDefault.AddWarning("This product is not assigned to a category and therefore will not be visible. ");
                }
            }
            else if (this.DataSourceAsProductEntity.Type == (int)ProductType.Paymentmethod)
            {
                AddInformatorInfo("This product is used as a payment method and therefore has restricted input fields.");
            }
            else if (this.DataSourceAsProductEntity.Type == (int)ProductType.Service)
            {
                AddInformatorInfo("This product service item is used as a service request and therefore has restricted input fields.");
            }
            else if (this.DataSourceAsProductEntity.Type == (int)ProductType.PosServiceMessage)
            {
                AddInformatorInfo("This product service item is used as a service message for the PoS and therefore has restricted input fields.");
            }

            if ((CompanyHelper.GetPOSConnectorType(this.DataSourceAsProductEntity.CompanyId.GetValueOrDefault(0), null) != POSConnectorType.Unknown)
                && !this.DataSourceAsProductEntity.PosproductId.HasValue
                && !this.DataSourceAsProductEntity.CompanyEntity.IncludeMenuItemsNotExternallyLinked)
            {
                AddInformator(InformatorType.Warning, "The item is not linked to the PoS and therefore will not be visible to guests.");
            }

            Validate();
        }

        public override bool Save()
        {
            this.DataSourceAsProductEntity.AddToTransaction(this.SaveTransactionToUse);

            if (this.PageMode == PageMode.Add)
            {
                if (!this.DataSourceAsProductEntity.BrandId.HasValue)
                {
                    this.DataSourceAsProductEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
                }

                if (this.ddlSubType.Value.GetValueOrDefault(0).Equals(ProductSubType.WebLink))
                {
                    this.DataSourceAsProductEntity.WebLinkType = WebLinkType.Button;
                }

                if (CmsSessionHelper.CurrentRole < Role.Administrator || this.DataSourceAsProductEntity.BrandId.HasValue)
                {
                    this.DataSourceAsProductEntity.VattariffId = 3;
                }
            }

            if (this.ddlBrandProductId.ValidId > 0 && this.ddlGenericproductId.ValidId > 0)
            {
                AddInformator(InformatorType.Warning, "Can only choose either a generic product or a brand product");
                return false;
            }

            if (this.ddlBrandProductId.ValidId > 0)
            {
                this.DataSourceAsProductEntity.BrandProductId = this.ddlBrandProductId.ValidId;
            }

            if (this.attachmentCollection != null)
            {
                this.DataSourceAsProductEntity.InheritAttachmentsFromBrand = this.attachmentCollection.InheritFromBrand;
            }

            this.DataSourceAsProductEntity.SubType = this.ddlSubType.Value.GetValueOrDefault(0); // Manually set it, not using databinding

            if (!ValidateAssignedPointOfInterest())
            {
                this.MultiValidatorDefault.AddError($"Point of interest '{this.cbPointOfInterestId.Text}' is already assigned to another product");
                this.cbPointOfInterestId.Value = null;
                Validate();
                return false;
            }

            if (!ValidateProductTextFields())
            {
                Validate();
                return false;
            }

            SetProductFields(this.DataSourceAsProductEntity);

            this.DataSourceAsProductEntity.ShortDescription = this.tbShortDescription.Text;

            if (!this.DataSourceAsProductEntity.BrandId.HasValue || !this.DataSourceAsProductEntity.InheritAlterationsFromBrand)
            {
                EntityView<ProductAlterationEntity> paView = this.DataSourceAsProductEntity.ProductAlterationCollection.DefaultView;
                SaveProductAlterations(paView, 1, "ddlProductAlterationV1_");
                SaveProductAlterations(paView, 2, "ddlProductAlterationV2_");
            }

            EntityView<ProductSuggestionEntity> psView = this.DataSourceAsProductEntity.ProductSuggestionCollection.DefaultView;
            SaveProductSuggestions(psView);

            bool success = base.Save();
            DataSourceAsProductEntity.Refetch();

            if (this.tagBox.IsDirty)
            {
                try
                {
                    if (this.DataSourceAsProductEntity.AlterationoptionCollection.Any())
                    {
                        foreach (AlterationoptionEntity alterationoption in this.DataSourceAsProductEntity.AlterationoptionCollection.Where(x => x.InheritTags))
                        {
                            ICollection<Interfaces.ITag> alterationoptionTags = alterationoption.GetTags();
                            ICollection<Interfaces.ITag> alterationoptionInheritedTags = alterationoption.GetInheritedTags();

                            List<Interfaces.ITag> selectedTags = alterationoptionTags.Except(alterationoptionInheritedTags, new TagNameEqualityComparer()).ToList();

                            TagHelper.UpdateTagsForEntity<AlterationoptionEntity, AlterationoptionTagEntity, AlterationoptionTagCollection>(alterationoption, selectedTags.Union(this.tagBox.GetSelectedTags()).ToList());
                        }
                    }

                    TagHelper.UpdateTagsForEntity<ProductEntity, ProductTagEntity, ProductTagCollection>(this.DataSourceAsProductEntity, this.tagBox.GetSelectedTags());
                }
                catch (EntitySaveException ex)
                {
                    this.MultiValidatorDefault.AddError(ex.Message);
                    Validate();
                    return false;
                }
            }

            if (this.DataSourceAsProductEntity.BrandId.HasValue)
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsProductEntity.BrandEntity.CultureCode);
            }
            else
            {
                CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsProductEntity.CompanyEntity.CultureCode);
            }

            SetWarnings();

            return success;
        }

        private void SaveProductAlterations(EntityView<ProductAlterationEntity> paView, int version, string prefix)
        {
            foreach (Control control in this.ControlList)
            {
                if (!control.ID.IsNullOrWhiteSpace() && control.ID.StartsWith(prefix))
                {
                    ComboBoxInt ddl = (ComboBoxInt)control;
                    ProductAlterationEntity pa = null;

                    int sortOrder = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(ddl.ID, prefix));
                    PredicateExpression paFilter = new PredicateExpression(ProductAlterationFields.SortOrder == sortOrder)
                    {
                        ProductAlterationFields.Version == version
                    };
                    paView.Filter = paFilter;
                    if (ddl.ValidId > 0)
                    {
                        // Update / Add
                        if (paView.Count == 0)
                        {
                            // Find the PosproductPosalteration entity for this entry
                            PosproductPosalterationCollection pppa = new PosproductPosalterationCollection();
                            PredicateExpression filter = new PredicateExpression
                            {
                                PosproductPosalterationFields.PosproductExternalId == this.DataSourceAsProductEntity.PosproductEntity.ExternalId
                            };
                            AlterationEntity alteration = new AlterationEntity(ddl.ValidId);
                            filter.Add(PosproductPosalterationFields.PosalterationExternalId == alteration.PosAlterationEntity.ExternalId);
                            filter.Add(PosproductPosalterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                            pppa.GetMulti(filter);

                            pa = new ProductAlterationEntity
                            {
                                ProductEntity = this.DataSourceAsProductEntity,
                                SortOrder = sortOrder
                            };
                            if (pppa.Count > 0)
                            {
                                pa.PosproductPosalterationId = pppa[0].PosproductPosalterationId;
                            }
                        }
                        else
                        {
                            pa = paView[0];
                        }

                        pa.AlterationId = ddl.ValidId;
                        pa.Version = version;
                        pa.AddToTransaction(this.DataSourceAsProductEntity);
                        pa.Save();
                    }
                    else
                    {
                        // Delete if any
                        if (paView.Count == 1)
                        {
                            paView[0].AddToTransaction(this.DataSourceAsProductEntity);
                            paView[0].Delete();
                            paView.RelatedCollection.Remove(paView[0]);
                        }
                    }
                }
            }
        }

        private void SaveProductSuggestions(EntityView<ProductSuggestionEntity> psView)
        {
            foreach (Control control in this.ControlList)
            {
                if (!control.ID.IsNullOrWhiteSpace() && control.ID.StartsWith("ddlProductSuggestion"))
                {
                    ComboBoxInt ddl = (ComboBoxInt)control;
                    int sortOrder = Convert.ToInt32(ddl.ID.GetAllAfterFirstOccurenceOf("ddlProductSuggestion"));
                    psView.Filter = new PredicateExpression(ProductSuggestionFields.SortOrder == sortOrder);
                    if (ddl.ValidId > 0)
                    {
                        // Update / Add
                        ProductSuggestionEntity ps;
                        if (psView.Count == 0)
                        {
                            ps = new ProductSuggestionEntity
                            {
                                ProductId = this.DataSourceAsProductEntity.ProductId,
                                SortOrder = sortOrder
                            };
                        }
                        else
                        {
                            ps = psView[0];
                        }

                        ps.AddToTransaction(this.DataSourceAsProductEntity);
                        ps.SuggestedProductId = ddl.ValidId;

                        CheckBox cbCheckout = this.ControlList.FirstOrDefault(c => c.ID == string.Format("cbSuggestionCheckout{0}", sortOrder)) as CheckBox;
                        if (cbCheckout != null)
                        {
                            ps.Checkout = cbCheckout.Checked;
                        }

                        ps.Save();
                    }
                    else
                    {
                        // Delete if any
                        if (psView.Count == 1)
                        {
                            psView[0].AddToTransaction(this.DataSourceAsProductEntity);
                            psView[0].Delete();
                            psView.RelatedCollection.Remove(psView[0]);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the subcontrols from the specified control recursively
        /// </summary>
        /// <param name="control">The System.Web.UI.Control to get the subcontrols from</param>
        public override void CollectControls(Control control)
        {
            // check for null, if so an exception will be thrown
            Instance.ArgumentIsEmpty(control, "control");

            // GK 09092008
            // MDB 10092008 This is not going to work with subclasses
            if (control is DevExpress.Web.ASPxGridView ||
                control is Dionysos.Web.UI.DevExControls.GridViewEntityCollection ||
                control is ComboBoxLLBLGenEntityCollection)
            {
                this.ControlList.Add(control);

            }
            else if (control.Controls.Count == 0)
            {
                this.ControlList.Add(control);
            }
            else
            {
                this.ControlList.Add(control);

                for (int i = 0; i < control.Controls.Count; i++)
                {
                    CollectControls(control.Controls[i]);
                }
            }
        }

        private void SetPriceLabel()
        {
            if (this.DataSourceAsProductEntity.IsBrandProduct)
            {
                this.lblPriceIn.Value = "Price";

                this.lblTaxTariffId.Visible = false;
                this.ddlTaxTariffId.Visible = false;

                return;
            }

            bool pricesIncludeTaxes;

            if (this.DataSourceAsProductEntity.IsNew)
            {
                CompanyEntity company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);

                pricesIncludeTaxes = company.PricesIncludeTaxes;
            }
            else
            {
                pricesIncludeTaxes = this.DataSourceAsProductEntity.CompanyEntity.PricesIncludeTaxes;
            }

            this.lblPriceIn.Value = pricesIncludeTaxes
                ? "Price (Including tax)"
                : "Price (Excluding tax)";
        }

        private void HookUpEvents()
        {
            this.btUncoupleGenericProduct.Click += new EventHandler(btUncoupleGenericProduct_Click);
            this.btUnlinkBrandProduct.Click += btUnlinkBrandProduct_Click;
            this.btnResetColor.Click += btnResetColor_Click;
            this.lbCopyDescriptionFromGenericproduct.Click += lbCopyDescriptionFromGenericproduct_Click;
            this.btnCreateGenericProduct.Click += btnCreateGenericProduct_Click;
            this.btTreelist.Click += btTreelist_Click;
        }

        private void btTreelist_Click(object sender, EventArgs e)
        {
            int version = CmsSessionHelper.AlterationDialogMode == AlterationDialogMode.v3 ? 2 : 1;
            this.Response.Redirect(ResolveUrl(string.Format("~/Catalog/ProductV2.aspx?id={0}&Version={1}", this.DataSourceAsProductEntity.ProductId, version)));
        }

        /// <summary>
        /// Creates a generic product from a normal product and links the product to the newly created generic product
        /// </summary>
        private void CreateGenericProduct()
        {
            if (this.DataSourceAsProductEntity.IsNew)
            {
                // Do nothing
                AddInformator(Dionysos.Web.UI.InformatorType.Information, "Please save this new product first before using this feature");
            }
            else
            {
                Transaction transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "CreateGenericproduct");

                try
                {
                    // Create new generic product entity
                    GenericproductEntity genericproductEntity = new GenericproductEntity();
                    genericproductEntity.AddToTransaction(transaction);
                    genericproductEntity.Name = this.DataSourceAsProductEntity.Name;
                    genericproductEntity.Description = this.DataSourceAsProductEntity.Description;
                    genericproductEntity.VattariffId = this.DataSourceAsProductEntity.VattariffId;
                    if (!this.DataSourceAsProductEntity.TextColor.IsNullOrWhiteSpace())
                    {
                        genericproductEntity.TextColor = this.DataSourceAsProductEntity.TextColor;
                    }

                    if (!this.DataSourceAsProductEntity.BackgroundColor.IsNullOrWhiteSpace())
                    {
                        genericproductEntity.BackgroundColor = this.DataSourceAsProductEntity.BackgroundColor;
                    }

                    genericproductEntity.Save();

                    // Copy media and set media on product to null
                    if (this.DataSourceAsProductEntity.MediaCollection.Count > 0)
                    {
                        List<MediaEntity> mediaEntitiesToDelete = new List<MediaEntity>();
                        foreach (MediaEntity oldMediaEntity in this.DataSourceAsProductEntity.MediaCollection)
                        {
                            CopyMediaRequest request = new CopyMediaRequest
                            {
                                MediaToCopy = oldMediaEntity,
                                ReferenceField = MediaFields.GenericproductId,
                                ReferenceValue = genericproductEntity.GenericproductId,
                                CopyMediaOnCdn = MediaCloudHelper.CopyMediaOnCdn,
                                Transaction = transaction,
                                ToGenericEntity = true
                            };
                            new CopyMediaUseCase().Execute(request);
                            mediaEntitiesToDelete.Add(oldMediaEntity);
                        }
                        foreach (MediaEntity oldMediaEntity in mediaEntitiesToDelete)
                        {
                            this.DataSourceAsProductEntity.MediaCollection.Remove(oldMediaEntity);
                            oldMediaEntity.AddToTransaction(transaction);
                            oldMediaEntity.Delete();
                        }
                    }

                    // Link the newly created generic product to the product entity
                    this.DataSourceAsProductEntity.GenericproductId = genericproductEntity.GenericproductId;
                    this.DataSourceAsProductEntity.AddToTransaction(transaction);
                    this.DataSourceAsProductEntity.Save(true);

                    transaction.Commit();
                    WebShortcuts.ResponseRedirect(this.Request.RawUrl);
                    AddInformator(Dionysos.Web.UI.InformatorType.Information, "The generic product has been successfully created and linked to this product!");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    AddInformator(Dionysos.Web.UI.InformatorType.Warning, string.Format("Oops, something went wrong trying to create the generic product. {0}", ex.Message));
                }
                finally
                {
                    transaction.Dispose();
                }
            }
        }

        private ProductEntity productEntityDataSource;
        /// <summary>
        /// Gets the datasource as a ProductEntity instance
        /// </summary>
        public ProductEntity DataSourceAsProductEntity => this.productEntityDataSource ?? (this.productEntityDataSource = this.DataSource as ProductEntity);

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack && !this.DataSourceAsProductEntity.IsNew)
            {
                SetWarnings();
            }

            this.SaveTransactionToUse = new Transaction(IsolationLevel.ReadUncommitted, "Product-Save");

            HookUpEvents();
        }

        private void lbCopyDescriptionFromGenericproduct_Click(object sender, EventArgs e)
        {
            if (!this.DataSourceAsProductEntity.GenericproductEntity.IsNew)
            {
                this.DataSourceAsProductEntity.Description = this.DataSourceAsProductEntity.GenericproductEntity.Description;
                this.DataSourceAsProductEntity.Save();
            }

            WebShortcuts.ResponseRedirect(this.Request.RawUrl);
        }

        private void btnResetColor_Click(object sender, EventArgs e)
        {
            if (!this.DataSourceAsProductEntity.IsNew)
            {
                this.DataSourceAsProductEntity.Color = 0;
                this.DataSourceAsProductEntity.InheritColor = true;
                this.DataSourceAsProductEntity.Save();
            }

            WebShortcuts.ResponseRedirect(this.Request.RawUrl);
        }

        private void btnResetRatings_Click(object sender, EventArgs e)
        {
            if (!this.DataSourceAsProductEntity.IsNew)
            {
                foreach (RatingEntity rating in this.DataSourceAsProductEntity.RatingCollection)
                {
                    rating.Delete();
                }
            }

            WebShortcuts.ResponseRedirect(this.Request.RawUrl);
        }

        private void btUnlinkBrandProduct_Click(object sender, EventArgs e)
        {
            if (this.DataSourceAsProductEntity.BrandProductId.HasValue)
            {
                this.DataSourceAsProductEntity.BrandProductId = null;
                this.DataSourceAsProductEntity.Save();

                RedirectUsingRawUrl();
            }
        }

        private void btUncoupleGenericProduct_Click(object sender, EventArgs e)
        {
            ProductEntity p = this.DataSourceAsProductEntity;
            if (p.GenericproductEntity != null)
            {
                p.GenericproductEntity = null;
                p.GenericproductId = null;
                p.IsLinkedToGeneric = false;
                p.Save();

                RedirectUsingRawUrl();
            }
            else
            {
                this.MultiValidatorDefault.AddError(Translate("GenericNoGenericProductIsLinked", "Er is geen basisproduct gekoppeld aan dit product"));
                Validate();
            }
        }

        private void Product_DataSourceLoaded(object sender)
        {
            // Set the type if it's a new service request
            if (this.DataSourceAsProductEntity.IsNew && QueryStringHelper.TryGetValue("type", out int type) && type > 0)
            { this.DataSourceAsProductEntity.Type = type; }

            SetFilters();
            SetGui();

            if (this.DataSourceAsProductEntity.Type == (int)ProductType.Product)
            {
                this.categoryPanel.Initialize();
            }
            else
            {
                TabPage categoryTab = this.tabsMain.TabPages.FindByName("Categories");

                if (categoryTab != null)
                { categoryTab.Visible = false; }
            }

            if (this.PageMode == PageMode.Add)
            { this.DataSourceAsProductEntity.CoversType = CoversType.Inherit; }
        }

        private void btnCreateGenericProduct_Click(object sender, EventArgs e) => CreateGenericProduct();

        public override bool InitializeDataBindings()
        {
            base.InitializeDataBindings();

            if (this.DataSourceAsProductEntity.BrandProductId.HasValue)
            {
                foreach (Control control in this.tabsMain.TabPages[0].Controls)
                {
                    FunkyDataBinding(control, this.DataSource, this.DataSourceAsProductEntity.BrandProductEntity, this.IsPostBack);
                }
            }

            return true;
        }

        private void FunkyDataBinding(Control control, object datasource, object parentDatasource, bool isPostBack)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control childControl = control.Controls[i];

                // Check if a control has controls it self
                if (childControl.HasControls() || childControl.Controls.Count > 0)
                {
                    // We need special logic for SubPanels as they (might) have another datasource
                    if (!(childControl is SubPanelLLBLGenEntity))
                    {
                        FunkyDataBinding(childControl, datasource, parentDatasource, isPostBack);
                    }

                    continue;
                }

                if (childControl is IBindable)
                {
                    // Get the ID of the control without the prefix (remove prefix by removing all lowercase till the first upper)
                    // Examples: hlFirstname, tbLastname, rbGender, ddlCategory, etc.
                    string controlId = childControl.ID;
                    int upperCase = controlId.IndexOfFirstUpper();
                    string memberName = string.Empty;
                    if (upperCase > -1)
                    {
                        memberName = controlId.Substring(upperCase);
                    }

                    // Check whether a property exists on the DataSource
                    // according to the name of the control
                    try
                    {
                        if (Instance.Empty(memberName))
                        {
                            // ID of the control is empty
                        }
                        else if (Member.HasMember(parentDatasource, memberName))
                        {
                            object parentValue = Member.GetMemberValue(parentDatasource, memberName);

                            if (childControl is TextBox textBox && parentValue != null && !parentValue.ToString().IsNullOrWhiteSpace())
                            {
                                textBox.IsRequired = false;
                                textBox.Placeholder = textBox.Placeholder.IsNullOrWhiteSpace()
                                    ? parentValue.ToString()
                                    : textBox.Placeholder;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string datasourceName = datasource == null ? "null" : datasource.ToString();

                        throw new TechnicalException(ex, "Problem with binding: {0} on dataSource {1} on control {2}: {3}.\r\n If it's a Subpanel, make sure it's added with a GuiParent BEFORE the OnInit of the page, otherwise the DataSource of the Subpanel doesn't get set. If it's not on a SubPanel make sure the local database you're using is up to date with the MasterEV.", controlId, datasourceName, control.GetType().FullName, ex.Message);
                    }
                }
            }
        }
    }
}
