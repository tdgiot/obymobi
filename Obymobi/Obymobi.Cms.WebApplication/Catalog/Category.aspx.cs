using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI.DevExControls;
using Dionysos.Web.UI.UltraControls;
using MarkdownSharp.Extensions;
using Obymobi.Cms.Logic.Extensions;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.WebApplication.Catalog.SubPanels;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CategoryHelper = Obymobi.Cms.Logic.HelperClasses.CategoryHelper;
using ProductHelper = Obymobi.Cms.Logic.HelperClasses.ProductHelper;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Category : Dionysos.Web.UI.PageLLBLGenEntity
    {
        private CategoryConfigurationAppLess _categoryConfigurationAppLessControl;

        private void HookUpEvents()
        {
            this.DataSourceLoaded += this.Category_DataSourceLoaded;
        }

        private void LoadUserControls()
        {
            Generic.UserControls.CustomTextCollection translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic") as Generic.UserControls.CustomTextCollection;
            translationsPanel.FullWidth = true;

            this.tabsMain.AddTabPage("Afbeeldingen", "Media", "~/Generic/UserControls/MediaCollection.ascx");
            this.tabsMain.AddTabPage("Producten", "ProductCategoryPanel", "~/Catalog/Subpanels/CategoryProductPanel.ascx");

            this._categoryConfigurationAppLessControl = this.tabsMain.AddTabPage("AppLess", "CategoryConfigurationAppLess", "~/Catalog/Subpanels/CategoryConfigurationAppLess.ascx") as CategoryConfigurationAppLess;

            // Tags permissions
            this.tagBox.ReadOnly = CmsSessionHelper.CurrentRole == Role.Console;
            this.tagBox.AllowCustomTokens = CmsSessionHelper.CurrentRole >= Role.Supervisor;
        }

        public override bool InitializeEntity()
        {
            // GK ca. 3% improvement of page time using this.
            PrefetchPath path = new PrefetchPath(EntityType.CategoryEntity);
            path.Add(CategoryEntity.PrefetchPathProductCategoryCollection).SubPath.Add(ProductCategoryEntity.PrefetchPathProductEntity);
            CategoryEntity category = new CategoryEntity(this.EntityId, path);
            this.DataSource = category;

            return true;
        }

        private void SetFilters()
        {
            // Init the Parent Categoyr DropDownList
            PredicateExpression filter = new PredicateExpression(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                int menuId;
                if (QueryStringHelper.TryGetValue("menu", out menuId))
                {
                    filter.Add(CategoryFields.MenuId == menuId);
                }
            }
            else if (this.DataSourceAsCategoryEntity.MenuId.HasValue && this.DataSourceAsCategoryEntity.MenuId.Value > 0)
            {
                filter.Add(CategoryFields.MenuId == this.DataSourceAsCategoryEntity.MenuId.Value);
            }

            // Don't allow category to be its own parent
            filter.Add(CategoryFields.CategoryId != DataSourceAsCategoryEntity.CategoryId);

            // Don't allow categories that contain Products to be the parent Category
            RelationCollection relations = new RelationCollection();
            relations.Add(CategoryEntity.Relations.ProductCategoryEntityUsingCategoryId, JoinHint.Left);

            filter.Add(ProductCategoryFields.ProductCategoryId == DBNull.Value);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter, 0, null, relations);
            this.ddlParentCategoryId.BindEntityCollection(categoryCollection, CategoryFields.Name, CategoryFields.CategoryId);

            var parentCategories = new CategoryCollection();
            if (!this.DataSourceAsCategoryEntity.IsNew)
            {
                if (this.DataSourceAsCategoryEntity.CategoryAlterationCollection.Count == 0 || this.DataSourceAsCategoryEntity.CategorySuggestionCollection.Count == 0)
                {
                    List<int> categoryIds = new List<int>() { this.DataSourceAsCategoryEntity.CategoryId };
                    parentCategories = CategoryHelper.GetCategoriesWithPrefetchedParents(categoryIds);
                }
            }

            this.SetCategoryAlterations(parentCategories, 1, "ddlCategoryAlterationV1_", this.plhInheritedAlterationsV1);
            this.SetCategoryAlterations(parentCategories, 2, "ddlCategoryAlterationV2_", this.plhInheritedAlterationsV2);

            this.FilterSuggestions(parentCategories);

            SetPoscategoryIdFilter();

            // Init the routes            
            this.ddlRouteId.BindEntityCollection(RouteHelper.GetRouteCollection(CmsSessionHelper.CurrentCompanyId), RouteFields.Name, RouteFields.RouteId);

            // Init the menus
            MenuCollection menus = new MenuCollection();
            PredicateExpression menuFilter = new PredicateExpression(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression menuSort = new SortExpression(MenuFields.Name | SortOperator.Ascending);
            menus.GetMulti(menuFilter, 0, menuSort);
            this.ddlMenuId.BindEntityCollection(menus, MenuFields.Name, MenuFields.MenuId);

            // Init the schedules
            ScheduleCollection scheduleCollection = new ScheduleCollection();
            PredicateExpression scheduleFilter = new PredicateExpression(ScheduleFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression scheduleSort = new SortExpression(ScheduleFields.Name | SortOperator.Ascending);
            scheduleCollection.GetMulti(scheduleFilter, 0, scheduleSort);
            this.ddlScheduleId.BindEntityCollection(scheduleCollection, ScheduleFields.Name, ScheduleFields.ScheduleId);

            this.ddlViewLayoutType.DataBindEnum<ViewLayoutType>();
            this.ddlDeliveryLocationType.DataBindEnum<DeliveryLocationType>();
            this.ddlSupportNotificationType.DataBindEnum<SupportNotificationType>();
            cbCoversType.DataBindEnumValues(CoversType.AlwaysAsk, CoversType.NeverAsk, CoversType.Inherit);
        }

        private void SetPoscategoryIdFilter()
        {
            // Init the POS categories
            PredicateExpression poscategoryFilter = new PredicateExpression(PoscategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression poscategorySort = new SortExpression(new SortClause(PoscategoryFields.Name, SortOperator.Ascending));

            PoscategoryCollection poscategoryCollection = new PoscategoryCollection();
            poscategoryCollection.GetMulti(poscategoryFilter, 0, poscategorySort);

            foreach (PoscategoryEntity poscategory in poscategoryCollection)
            {
                if (!string.IsNullOrWhiteSpace(poscategory.RevenueCenter))
                {
                    this.ddlPoscategoryId.AddItem($"{poscategory.Name} ({poscategory.ExternalId}) [{poscategory.RevenueCenter}]", poscategory.PoscategoryId);
                }
                else
                {
                    this.ddlPoscategoryId.AddItem($"{poscategory.Name} ({poscategory.ExternalId})", poscategory.PoscategoryId);
                }
            }
        }

        private void SetCategoryAlterations(CategoryCollection parentCategories, int version, string prefix, Dionysos.Web.UI.WebControls.PlaceHolder placeHolder)
        {
            // Category alterations
            PredicateExpression filterAlterations = new PredicateExpression();
            filterAlterations.Add(AlterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filterAlterations.Add(AlterationFields.Type == AlterationHelper.GetSupportedAlterationTypes(version < 2 ? AlterationDialogMode.v2 : AlterationDialogMode.v3));
            filterAlterations.Add(AlterationFields.Version == version);

            SortExpression sort = new SortExpression(AlterationFields.Name | SortOperator.Ascending);

            AlterationCollection alterations = new AlterationCollection();
            alterations.GetMulti(filterAlterations, 0, sort);

            EntityView<CategoryAlterationEntity> caView = this.DataSourceAsCategoryEntity.CategoryAlterationCollection.DefaultView;

            foreach (var control in this.ControlList)
            {
                if (!control.ID.IsNullOrWhiteSpace() && control.ID.StartsWith(prefix))
                {
                    BindCategoryAlterations((UltraBoxInt)control, alterations, caView);
                }
            }

            caView.Filter = null;
            if (caView.Count <= 0)
            {
                // Get alterations from parent categories
                var parentCategoryAlterations = this.GetParentCategoryAlterations(parentCategories, version);
                if (parentCategoryAlterations.Count > 0)
                {
                    System.Text.StringBuilder builder = new System.Text.StringBuilder();
                    builder.AppendFormatLine("<table class=\"dataformV2\">");
                    builder.AppendFormatLine("<tr>");
                    builder.AppendFormatLine("   <td class='label'>");
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("   <td class='control'>");
                    builder.AppendFormatLine("       <D:LabelTextOnly runat=\"server\" ID=\"lblAlterationHeader\">{0}</D:LabelTextOnly>", this.Translate("lblAlterationHeader", "Alteration"));
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("   <td class='control'>");
                    builder.AppendFormatLine("      <D:LabelTextOnly runat=\"server\" ID=\"lblCategoryHeader\">{0}</D:LabelTextOnly>", this.Translate("lblCategoryHeader", "Categorie"));
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("   <td class='label'>");
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("</tr>");

                    // Get the alterations from a parent category
                    for (int i = 0; i < parentCategoryAlterations.Count; i++)
                    {
                        CategoryAlterationEntity categoryAlteration = parentCategoryAlterations[i];

                        string alterationLink = string.Format("<a href=\"{0}\">{1}</a>", categoryAlteration.AlterationEntity.GetCmsPage(), categoryAlteration.AlterationEntity.Name);
                        string categoryLink = string.Format("<a href=\"Category.aspx?id={0}\">{1}</a>", categoryAlteration.CategoryId, categoryAlteration.CategoryEntity.Name);

                        // Create row for an alteration
                        builder.AppendFormatLine("<tr>");
                        builder.AppendFormatLine("   <td class='label' style=\"padding-top:5px\">");
                        builder.AppendFormatLine("       <D:LabelTextOnly runat=\"server\" ID=\"lblNumber\" LocalizeText=\"false\">{0}. </D:LabelTextOnly>", (i + 1));
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("   <td class='control'>");
                        builder.AppendFormatLine("       <D:LabelTextOnly runat=\"server\" ID=\"lblAlteration\" LocalizeText=\"false\">{0}</D:LabelTextOnly>", alterationLink);
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("   <td class='control'>");
                        builder.AppendFormatLine("      <D:LabelTextOnly runat=\"server\" ID=\"lblCategory\" LocalizeText=\"false\">{0}</D:LabelTextOnly>", categoryLink);
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("   <td class='label'>");
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("</tr>");
                    }
                    builder.AppendFormatLine("</table>");
                    placeHolder.AddHtml(builder.ToString());
                }
            }
        }

        private void BindCategoryAlterations(UltraBoxInt ddl, AlterationCollection alterations, EntityView<CategoryAlterationEntity> caView)
        {
            ddl.BindEntityCollection(alterations, AlterationFields.Name, AlterationFields.AlterationId);
            ddl.SelectedIndex = 0;

            int sortOrder;

            if (ddl.ID.StartsWith("ddlCategoryAlterationV1_"))
                sortOrder = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(ddl.ID, "ddlCategoryAlterationV1_"));
            else
                sortOrder = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(ddl.ID, "ddlCategoryAlterationV2_"));

            caView.Filter = new PredicateExpression(CategoryAlterationFields.SortOrder == sortOrder);

            if (caView.Count == 1)
            {
                ddl.Value = caView[0].AlterationId;
            }
        }

        private CategoryAlterationCollection GetParentCategoryAlterations(CategoryCollection parentCategories, int version)
        {
            var categoryAlterations = new CategoryAlterationCollection();
            if (!this.DataSourceAsCategoryEntity.IsNew)
            {
                // Get parent categories with prefetched parents                
                foreach (CategoryEntity parentCategory in parentCategories)
                {
                    CategoryHelper.AddAlterationsFromCategories(categoryAlterations, parentCategory, version);
                }
            }
            return categoryAlterations;
        }

        private CategorySuggestionCollection GetParentCategorySuggestions(CategoryCollection parentCategories)
        {
            var categorySuggestions = new CategorySuggestionCollection();
            if (!this.DataSourceAsCategoryEntity.IsNew)
            {
                // Get parent categories with prefetched parents                
                foreach (CategoryEntity parentCategory in parentCategories)
                {
                    CategoryHelper.AddSuggestionsFromCategories(categorySuggestions, parentCategory);
                }
            }
            return categorySuggestions;
        }

        private void FilterSuggestions(CategoryCollection parentCategories)
        {
            ProductCategoryCollection productCategories = ProductHelper.GetProductCategoriesAvailableAsSuggestionForCompany(CmsSessionHelper.CurrentCompanyId, true, this.DataSourceAsCategoryEntity.MenuId);

            EntityView<CategorySuggestionEntity> csView = this.DataSourceAsCategoryEntity.CategorySuggestionCollection.DefaultView;

            foreach (var control in this.ControlList)
            {
                if (!control.ID.IsNullOrWhiteSpace() && control.ID.StartsWith("ddlCategorySuggestion"))
                {
                    ComboBoxInt cb = (ComboBoxInt)control;
                    cb.DataSource = productCategories;
                    cb.DataBind();

                    // Check if this category has a selected suggested for this position
                    var sortOrder = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(cb.ID, "ddlCategorySuggestion"));
                    csView.Filter = new PredicateExpression(CategorySuggestionFields.SortOrder == sortOrder);

                    if (csView.Count == 1)
                    {
                        cb.Value = csView[0].ProductCategoryId;

                        Control cbCheckout = this.ControlList.FirstOrDefault(c => c.ID == string.Format("cbSuggestionCheckout{0}", sortOrder));
                        if (cbCheckout != null)
                        {
                            (cbCheckout as CheckBox).Checked = csView[0].Checkout;
                        }
                    }
                }
            }

            // Inherited alterations
            csView.Filter = null;
            if (csView.Count <= 0)
            {
                var parentCategorySuggestions = this.GetParentCategorySuggestions(parentCategories);
                if (parentCategorySuggestions.Count > 0)
                {
                    System.Text.StringBuilder builder = new System.Text.StringBuilder();
                    builder.AppendFormatLine("<table class=\"dataformV2\">");
                    builder.AppendFormatLine("<tr>");
                    builder.AppendFormatLine("   <td class='label'>");
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("   <td class='control'>");
                    builder.AppendFormatLine("       <D:LabelTextOnly runat=\"server\" ID=\"lblSuggestionHeader\">{0}</D:LabelTextOnly>", this.Translate("lblSuggestionHeader", "Suggestie"));
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("   <td class='control'>");
                    builder.AppendFormatLine("      <D:LabelTextOnly runat=\"server\" ID=\"lblCategoryHeader\">{0}</D:LabelTextOnly>", this.Translate("lblCategoryHeader", "Categorie"));
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("   <td class='label'>");
                    builder.AppendFormatLine("   </td>");
                    builder.AppendFormatLine("</tr>");

                    // Get the suggestions from a parent category
                    for (int i = 0; i < parentCategorySuggestions.Count; i++)
                    {
                        CategorySuggestionEntity categorySuggestion = parentCategorySuggestions[i];

                        string categoryLink = string.Format("<a href=\"Category.aspx?id={0}\">{1}</a>", categorySuggestion.CategoryId, categorySuggestion.CategoryEntity.Name);
                        string suggestionLink = string.Format("<a href=\"Product.aspx?id={0}\">{1}</a>", categorySuggestion.ProductId, categorySuggestion.ProductEntity.Name);
                        if (categorySuggestion.ProductCategoryId.HasValue)
                        {
                            suggestionLink = string.Format("<a href=\"Product.aspx?id={0}\">{1}</a>", categorySuggestion.ProductCategoryEntity.ProductId, categorySuggestion.ProductCategoryEntity.ProductEntity.Name);
                        }

                        // Create row for an suggestion
                        builder.AppendFormatLine("<tr>");
                        builder.AppendFormatLine("   <td class='label' style=\"padding-top:5px\">");
                        builder.AppendFormatLine("       <D:LabelTextOnly runat=\"server\" ID=\"lblNumber\" LocalizeText=\"false\">{0}. </D:LabelTextOnly>", (i + 1));
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("   <td class='control'>");
                        builder.AppendFormatLine("       <D:LabelTextOnly runat=\"server\" ID=\"lblSuggestion\" LocalizeText=\"false\">{0}</D:LabelTextOnly>", suggestionLink);
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("   <td class='control'>");
                        builder.AppendFormatLine("      <D:LabelTextOnly runat=\"server\" ID=\"lblCategory\" LocalizeText=\"false\">{0}</D:LabelTextOnly>", categoryLink);
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("   <td class='label'>");
                        builder.AppendFormatLine("   </td>");
                        builder.AppendFormatLine("</tr>");
                    }
                    builder.AppendFormatLine("</table>");

                    this.plhInheritedSuggestions.AddHtml(builder.ToString());
                }
            }
        }

        private void SetGui()
        {
            if (CmsSessionHelper.CurrentRole < Role.Reseller)
            {
                this.tabsMain.TabPages.FindByName("tabAdvanced").ClientVisible = false;
            }

            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                DataSourceAsCategoryEntity.Visible = true;

                int menuId;
                if (QueryStringHelper.TryGetValue("menu", out menuId))
                {
                    this.ddlMenuId.Value = menuId;
                }
            }

            // Set the URL
            this.tbUrl.Text = string.Format("crave://category/{0}", this.DataSourceAsCategoryEntity.CategoryId);
            this.tbDescription.Text = MarkdownHelper.RemoveMarkdown(this.DataSourceAsCategoryEntity.Description);

            // Set the color
            if (this.DataSourceAsCategoryEntity.Color > 0)
            {
                this.ceColor.Color = System.Drawing.Color.FromArgb((255 << 24) | this.DataSourceAsCategoryEntity.Color);
            }

            foreach (ListItem item in this.rblRateable.Items)
            {
                if (item.Text.Equals("Unknown"))
                    item.Text = "Inherit";
            }

            foreach (ListItem item in this.rblGeofencing.Items)
            {
                if (item.Text.Equals("Unknown"))
                    item.Text = "Inherit";
            }

            foreach (ListItem item in this.rblAllowFreeText.Items)
            {
                if (item.Text.Equals("Unknown"))
                    item.Text = "Inherit";
            }

            // Set the visibility type
            this.ddlVisibilityType.DataBindEnum<VisibilityType>();

            if (this.DataSourceAsCategoryEntity.CompanyEntity.ApplicationConfigurationCollection.Any())
            {
                this._categoryConfigurationAppLessControl.Load(this.DataSourceAsCategoryEntity);
            }

            this.SetTags();
            SetCoversType();
        }

        private void SetCoversType()
        {
            bool isCoversVisible = CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.Covers);
            lblCoversType.Visible = isCoversVisible;
            cbCoversType.Visible = isCoversVisible;
            cbCoversType.IsRequired = isCoversVisible;
        }

        private void SetTags()
        {
            tagBox.SetSelectedTags(this.DataSourceAsCategoryEntity.CategoryTagCollection);

            // Load the inherited tags 
            foreach (CategoryEntity categoryEntity in DataSourceAsCategoryEntity.GetParentCategories())
            {
                if (!categoryEntity.CategoryTagCollection.Any())
                {
                    continue;
                }

                TagOverview.AddTagGroup(categoryEntity.Name, categoryEntity.CategoryTagCollection);
            }
        }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsCategoryEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
                this.DataSourceAsCategoryEntity.ShowName = true;
            }

            this.DataSourceAsCategoryEntity.Color = ceColor.Color.R << 16 | ceColor.Color.G << 8 | ceColor.Color.B;
            this.DataSourceAsCategoryEntity.Description = MarkdownHelper.AddMarkdown(this.tbDescription.Text);

            CategoryEntity oldCategory = new CategoryEntity(this.DataSourceAsCategoryEntity.CategoryId);

            if (tagBox.IsDirty)
            {
                try
                {
                    TagHelper.UpdateTagsForEntity<CategoryEntity, CategoryTagEntity, CategoryTagCollection>(DataSourceAsCategoryEntity, tagBox.GetSelectedTags());
                    this.DataSourceAsCategoryEntity.IsDirty = true;
                }
                catch (EntitySaveException ex)
                {
                    this.MultiValidatorDefault.AddError(ex.Message);
                    this.Validate();
                    return false;
                }
            }

            bool succes = false;
            if (base.Save())
            {
                // Update tags
                if (tagBox.IsDirty || this.DataSourceAsCategoryEntity.ParentCategoryChanged)
                {
                    CategoryTagHelper.UpdateProductCategoryTags(this.DataSourceAsCategoryEntity);
                }

                if (oldCategory.MenuId != DataSourceAsCategoryEntity.MenuId ||
                    oldCategory.MenuItemsMustBeLinkedToExternalProduct != DataSourceAsCategoryEntity.MenuItemsMustBeLinkedToExternalProduct)
                {
                    UpdateChildCategories(DataSourceAsCategoryEntity.GetChildrenCategoryIds());
                }

                // Save Alterations
                EntityView<CategoryAlterationEntity> caView = this.DataSourceAsCategoryEntity.CategoryAlterationCollection.DefaultView;

                this.SaveCategoryAlterations(caView, 1, "ddlCategoryAlterationV1_");
                this.SaveCategoryAlterations(caView, 2, "ddlCategoryAlterationV2_");

                // Save Suggestions
                EntityView<CategorySuggestionEntity> csView = this.DataSourceAsCategoryEntity.CategorySuggestionCollection.DefaultView;

                foreach (var control in this.ControlList)
                {
                    if (!control.ID.IsNullOrWhiteSpace() && control.ID.StartsWith("ddlCategorySuggestion"))
                    {
                        ComboBoxInt cb = (ComboBoxInt)control;
                        CategorySuggestionEntity cs = null;

                        int sortOrder = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(cb.ID, "ddlCategorySuggestion"));
                        csView.Filter = new PredicateExpression(CategorySuggestionFields.SortOrder == sortOrder);

                        if (cb.ValidId > 0)
                        {
                            // Update / Add
                            if (csView.Count == 0)
                            {
                                cs = new CategorySuggestionEntity();
                                cs.CategoryId = this.DataSourceAsCategoryEntity.CategoryId;
                                cs.SortOrder = sortOrder;
                            }
                            else
                                cs = csView[0];

                            cs.AddToTransaction(this.DataSourceAsCategoryEntity);
                            cs.ProductCategoryId = cb.ValidId;

                            Control cbCheckout = this.ControlList.FirstOrDefault(c => c.ID == string.Format("cbSuggestionCheckout{0}", sortOrder));
                            if (cbCheckout != null)
                            {
                                cs.Checkout = cbCheckout != null && (cbCheckout as CheckBox).Checked;
                            }

                            cs.Save();
                        }
                        else
                        {
                            // Delete if any
                            if (csView.Count == 1)
                            {
                                csView[0].AddToTransaction(this.DataSourceAsCategoryEntity);
                                csView[0].Delete();
                                csView.RelatedCollection.Remove(csView[0]);
                            }
                        }
                    }
                }
                succes = true;
            }
            else
                succes = false;

            Obymobi.Logic.HelperClasses.CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsCategoryEntity.CompanyEntity);

            return succes;
        }

        private void UpdateChildCategories(List<int> childCategoryIds)
        {
            if (!childCategoryIds.Any())
            {
                return;
            }

            CategoryEntity changes = new CategoryEntity { IsNew = false };
            changes.MenuId = DataSourceAsCategoryEntity.MenuId;
            changes.MenuItemsMustBeLinkedToExternalProduct = DataSourceAsCategoryEntity.MenuItemsMustBeLinkedToExternalProduct;

            CategoryCollection categoryCollection = new CategoryCollection();
            PredicateExpression filter = new PredicateExpression(CategoryFields.CategoryId == childCategoryIds);

            categoryCollection.UpdateMulti(changes, filter);
        }

        private void SaveCategoryAlterations(EntityView<CategoryAlterationEntity> caView, int version, string prefix)
        {
            foreach (var control in this.ControlList)
            {
                if (!control.ID.IsNullOrWhiteSpace() && control.ID.StartsWith(prefix))
                {
                    UltraBoxInt cb = (UltraBoxInt)control;
                    CategoryAlterationEntity ca = null;

                    int sortOrder = Convert.ToInt32(StringUtil.GetAllAfterFirstOccurenceOf(cb.ID, prefix));
                    PredicateExpression caFilter = new PredicateExpression(CategoryAlterationFields.SortOrder == sortOrder);
                    caFilter.Add(CategoryAlterationFields.Version == version);
                    caView.Filter = caFilter;

                    if (cb.ValidId > 0)
                    {
                        // Update / Add
                        if (caView.Count == 0)
                        {
                            ca = new CategoryAlterationEntity();
                            ca.CategoryId = this.DataSourceAsCategoryEntity.CategoryId;
                            ca.SortOrder = sortOrder;
                        }
                        else
                            ca = caView[0];

                        ca.AlterationId = cb.ValidId;
                        ca.Version = version;
                        ca.Save();
                    }
                    else
                    {
                        // Delete if any
                        if (caView.Count == 1)
                        {
                            caView[0].AddToTransaction(this.DataSourceAsCategoryEntity);
                            caView[0].Delete();
                            caView.RelatedCollection.Remove(caView[0]);
                        }
                    }
                }
            }
        }

        public override void Validate()
        {
            this.ValidateAlterations("ddlCategoryAlterationV1_");
            this.ValidateAlterations("ddlCategoryAlterationV2_");

            base.Validate();
        }

        private void ValidateAlterations(string prefix)
        {
            var alterationControls = this.ControlList.Where(ctrl => !ctrl.ID.IsNullOrWhiteSpace() && ctrl.ID.StartsWith(prefix));
            var selectedAlterationIds = new List<int>();
            foreach (var ctrl in alterationControls)
            {
                UltraBoxInt ddl = (UltraBoxInt)ctrl;
                if (ddl.ValidId > 0)
                {
                    if (!selectedAlterationIds.Contains(ddl.ValidId))
                    {
                        selectedAlterationIds.Add(ddl.ValidId);
                    }
                    else
                    {
                        // User tries to set the same alteration multiple times, display a proper message
                        this.MultiValidatorDefault.AddError(this.Translate("ChosenAlterationAlreadySetForCategory", "Het is niet mogelijk om dezelfde alteration meer dan 1 keer in te stellen"));
                        break;
                    }
                }
            }
        }

        private void SetWarnings()
        {
            if (!this.DataSourceAsCategoryEntity.HasProducts)
            {
                this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("WarningNoProductsInCategory", "Deze categorie bevat nog geen producten."));
            }

            if (!this.DataSourceAsCategoryEntity.HasCategoryButton)
            {
                this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("WarningButtonImageNotSet", "Deze categorie heeft nog geen knop afbeelding."));
            }

            if (!this.DataSourceAsCategoryEntity.Geofencing.HasValue && !Obymobi.Logic.HelperClasses.CategoryHelper.HasGeofencedParent(this.DataSourceAsCategoryEntity).HasValue)
            {
                this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, this.Translate("WarningGeofencingNotSetAtParent", "Deze categorie of een van de bovenliggende categorieen heeft nog geen waarde ingesteld voor Geofencing, waardoor Geofencing momenteel Aan staat."));
            }

            this.Validate();
        }

        /// <summary>
        /// Gets the subcontrols from the specified control recursively
        /// </summary>
        /// <param name="control">The System.Web.UI.Control to get the subcontrols from</param>
        public override void CollectControls(Control control)
        {
            // check for null, if so an exception will be thrown
            Instance.ArgumentIsEmpty(control, "control");
            //Dionysos.Diagnostics.Debug.WriteLine("Info", Dionysos.TypeUtil.GetTypeString(control));

            // GK 09092008
            // MDB 10092008 This is not going to work with subclasses
            if (control is DevExpress.Web.ASPxGridView ||
                control is Dionysos.Web.UI.DevExControls.GridViewEntityCollection ||
                control is ComboBoxLLBLGenEntityCollection)
            {
                this.ControlList.Add(control);

            }
            else if (control.Controls.Count == 0)
            {
                this.ControlList.Add(control);
            }
            else
            {
                this.ControlList.Add(control);

                for (int i = 0; i < control.Controls.Count; i++)
                {
                    this.CollectControls(control.Controls[i]);
                }
            }
        }

        /// <summary>
        /// Gets the datasource as an CategoryEntity
        /// </summary>
        public CategoryEntity DataSourceAsCategoryEntity
        {
            get
            {
                return this.DataSource as CategoryEntity;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            this.HookUpEvents();
            this.LoadUserControls();

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
                this.SetWarnings();

            this.btnResetColor.Click += btnResetColor_Click;
        }

        private void btnResetColor_Click(object sender, EventArgs e)
        {
            if (!this.DataSourceAsCategoryEntity.IsNew)
            {
                this.DataSourceAsCategoryEntity.Color = 0;
                this.DataSourceAsCategoryEntity.Save();
            }

            WebShortcuts.ResponseRedirect(Request.RawUrl);
        }

        private void Category_DataSourceLoaded(object sender)
        {
            SetFilters();
            SetGui();

            if (PageMode == PageMode.Add)
            { DataSourceAsCategoryEntity.CoversType = CoversType.Inherit; }
        }
    }
}
