﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.Posproduct" Codebehind="Posproduct.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
				    <D:Panel ID="pnlGeneric" runat="server" GroupingText="Algemeen">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblExternalId">Kassa id</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbExternalId" runat="server" IsRequired="true"></D:TextBoxString>
							</td>	
					    </tr>
					    <tr>						
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblPriceIn">Price</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxDecimal ID="tbPriceIn" runat="server" IsRequired="true"></D:TextBoxDecimal>
							</td>        
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblExternalPoscategoryId">Kassa categorie id</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbExternalPoscategoryId" runat="server" IsRequired="true"></D:TextBoxString>
							</td>							
						</tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblVatTariff">BTW-tarief</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxInt ID="tbVatTariff" runat="server" IsRequired="true"></D:TextBoxInt>
							</td>        
							<td class="label">
							</td>
							<td class="control">
							</td>						
						</tr>
					</table>
                    </D:Panel>
                    <D:Panel ID="pnlPosProductOptions" runat="server" GroupingText="POS Product Alterations">
                    <table class="dataformV2">
                         <tr>
                             <td>
                                 &nbsp;
                             </td>
                             <td>
                                 <strong><D:LabelTextOnly runat="server" ID="lblImported">Geïmporteerd</D:LabelTextOnly></strong>
                             </td>
                             <td>
                                 <strong><D:LabelTextOnly runat="server" ID="lblAlterationOrAlterationOptionExternalId">Externe Id</D:LabelTextOnly></strong>
                             </td>
                         </tr>
                         <D:PlaceHolder runat="server" ID="plhAlterations"></D:PlaceHolder>
                     </table>
                     </D:Panel>
                    <D:Panel ID="pnlLinkedProducts" runat="server" GroupingText="Gekoppelde Producten">
                    <table class="dataformV2">
                         <D:PlaceHolder runat="server" ID="phlLinkedProducts"></D:PlaceHolder>
                     </table>
                     </D:Panel>
				</Controls>
			</X:TabPage>						
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

