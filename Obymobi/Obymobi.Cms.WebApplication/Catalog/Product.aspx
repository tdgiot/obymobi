<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" EnableSessionState="ReadOnly" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.Product" Title="Product" Codebehind="Product.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/SubPanels/MediaRatioPanel.ascx" %>
<%@ Reference VirtualPath="~/Catalog/SubPanels/ProductCulturePanel.ascx" %>
<%@ Reference VirtualPath="~/Catalog/SubPanels/CategoryPanel.ascx" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>
<%@ Reference VirtualPath="~/Catalog/SubPanels/ProductSuggestionPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>

<asp:Content ID="Conent3" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="server">
    <D:Button runat="server" ID="btUncoupleGenericProduct" Text="Niet generiek" />
    <D:Button runat="server" ID="btUnlinkBrandProduct" Text="Unlink brand product" LocalizeText="False" />
    <D:Button runat="server" ID="btTreelist" Text="View treelist" LocalizeText="False" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cplhPageContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>

    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Generic" Name="Generic">
                    <controls>
					<D:PlaceHolder runat="server">
					<table class="dataformV2"> 
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true" UseDataBinding="false"></D:TextBoxString>
							</td>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlType"></X:ComboBoxInt>
							</td>
						</tr>
						<tr>
						    <td class="label">
							    <D:LabelEntityFieldInfo runat="server" id="lblVisibilityType">Visibility</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxInt runat="server" ID="ddlVisibilityType"></X:ComboBoxInt>
							</td>
                        	<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSubType">Subtype</D:LabelEntityFieldInfo>
							</td>
							<td class="control">                                
								<X:ComboBoxInt runat="server" ID="ddlSubType" UseDataBinding="False"></X:ComboBoxInt>
                                <D:CheckBox runat="server" ID="cbOverrideSubType" Text="Override generic subtype" Visible="False"/>
							</td>
						</tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblIsAvailable">Available</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" id="cbIsAvailable" />
							</td>
							<td class="label">
                                <D:Label runat="server" id="lblExternalIdentifier">PLU / SKU</D:Label>
                            </td>
							<td class="control">
                                <D:TextBoxString ID="tbExternalIdentifier" runat="server"></D:TextBoxString>
                            </td>
						</tr>
                        <tr>
      					    <td class="label">
	                            <D:Label runat="server" id="lblPriceIn" LocalizeText="False">Price</D:Label>
							</td>
							<td class="control">
							    <D:TextBoxDecimal ID="tbPriceIn" runat="server" IsRequired="true" UseValidation="true" UseDataBinding="false"></D:TextBoxDecimal>
							</td>
	                        <td class="label">
		                        <D:Label runat="server" id="lblTaxTariffId">Tax Tariff</D:Label>
	                        </td>
	                        <td class="control">
		                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlTaxTariffId" EntityName="TaxTariff" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="TaxTariffId" PreventEntityCollectionInitialization="True" IsRequired="true"></X:ComboBoxLLBLGenEntityCollection>
	                        </td>
						</tr>
                        <D:PlaceHolder ID="plhWebLink" runat="server" Visible="false">
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblWebLinkType">Web link type</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <X:ComboBoxEnum ID="cbWebLinkType"  Type="Obymobi.Enums.WebLinkType, Obymobi" runat="server"/>
							    </td>
                        	    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblWebTypeTabletUrl">Web link url</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
								    <D:TextBoxString ID="tbWebTypeTabletUrl" runat="server" IsRequired="False" UseDataBinding="false"></D:TextBoxString>
							    </td>
						    </tr>
							<tr>
								<td class="label">
									<D:Label runat="server" id="lblOpenNewWindow">Open in new window</D:Label>
                                </td>
								<td class="control">
									<D:CheckBox runat="server" id="cbOpenNewWindow" />
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                        </D:PlaceHolder>                    
						<tr>
						    <td class="label">
								<D:Label runat="server" id="lblCustomizeButtonText" LocalizeText="false">Alteration button text</D:Label>
						    </td>
                            <td class="control">
						        <D:TextBoxString ID="tbCustomizeButtonText" runat="server" UseDataBinding="false"></D:TextBoxString>
						    </td>
							<td class="label">
								<D:Label runat="server" id="lblButtonText" LocalizeText="false">Button text</D:Label>
							</td>
							<td class="control">
							    <D:TextBoxString ID="tbButtonText" runat="server" UseDataBinding="false"></D:TextBoxString>
							</td>
						</tr>
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDisplayOnHomepage" />
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbDisplayOnHomepage" />
							</td>							
                        </tr>
                        <D:PlaceHolder ID="plhNonProductFields" runat="server">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblDescription" />
							</td>
							<td class="control threeCol" colspan="2">
								<D:TextBoxMultiLine ID="tbDescription" runat="server" Rows="15" UseDataBinding="false" ClientIDMode="Static" onkeyup="parseMarkdown(this);"></D:TextBoxMultiLine>
                                <D:TextBoxMultiLine ID="tbGenericDescription" runat="server" Rows="15" UseDataBinding="false" ReadOnly="true" ClientIDMode="Static" Visible="false" onkeyup="parseMarkdown(this);"></D:TextBoxMultiLine>
							    <small><a href="http://en.wikipedia.org/wiki/Markdown" target="_markdown">Markdown</a>-markup can be used. (bold: **bold**, italic: *italic*).</small><br/>
                                <D:LinkButton ID="lbCopyDescriptionFromGenericproduct" runat="server" Text="Copy description from generic product (hover for example)" onmouseover="return setGenericDescription();" onmouseout="return setDescription();" />
							</td>
                            <td class="control">
                                <div id="markdown-output" class="markdown_preview markdown-body" />
                            </td>
						</tr>
                        </D:PlaceHolder>
                        <D:PlaceHolder ID="plhShortDescription" runat="server">
	                        <tr>
		                        <td class="label">
			                        <D:LabelEntityFieldInfo runat="server" id="lblShortDescription">Short description</D:LabelEntityFieldInfo>
		                        </td>
		                        <td class="control threeCol" colspan="2">
			                        <D:TextBoxMultiLine ID="tbShortDescription" runat="server" Rows="3" UseDataBinding="false" ClientIDMode="Static" MaxLength="255"></D:TextBoxMultiLine>
		                        </td>
		                        <td class="control">									
		                        </td>
	                        </tr>
                        </D:PlaceHolder>
        
					    <CC:FeatureTogglePanel ToggleType="Tagging_Cms_EnableTagEditing" runat="server">
							<tr>
								<td class="label">Tags</td>
								<td class="control">
									<CC:TagBox ID="tagBox" ClientInstanceName="TagBoxControl" TextField="Name" ValueField="Id" runat="server" />
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>
								<tr>
								<td class="label">Inherited tags</td>
								<td class="control" colspan="3">
									<CC:TagOverview ID="TagOverview" runat="server" />
								</td>
							</tr>
							<tr>
								<td class="label"></td>
								<td class="control token-overview-control">
									<CC:TagLegend ID="TagLegend" runat="server" />
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>
                        </CC:FeatureTogglePanel>

                        <D:PlaceHolder ID="plhGenericproduct" runat="server">
                        <tr>
      					    <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblGenericproductId" />
							</td>
							<td class="control">
							    <X:ComboBoxInt runat="server" ID="ddlGenericproductId"></X:ComboBoxInt>			 	 
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lblManualDescriptionEnabled"></D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:CheckBox runat="server" ID="cbManualDescriptionEnabled" />
							</td>
						</tr>
                        </D:PlaceHolder>
                        <D:PlaceHolder ID="plhBrandProduct" runat="server">
                        <tr>
      					    <td class="label">
								<D:Label runat="server" id="lblBrandProductId" Text="Brand product" />
							</td>
							<td class="control">
							    <X:ComboBoxInt runat="server" UseDataBinding="False" ID="ddlBrandProductId" TextField="NameWithBrand" ValueField="ProductId" />
							</td>
                            <td class="label">
								
							</td>
							<td class="control">
                                <D:HyperLink ID="lbBrandProduct" runat="server" Visible="False" LocalizeText="False" Target="_blank" />
							</td>
						</tr>
                        </D:PlaceHolder>
                        <tr>
							<td class="label">
                                <D:Label runat="server" ID="lblCOlor">Color</D:Label>
							</td>
							<td class="control">
                                <dxe:ASPxColorEdit ID="ceColor" runat="server" UseDataBinding="false"></dxe:ASPxColorEdit>
							</td>
                            <td class="label">
                                <D:Label runat="server" ID="lblIsAlcoholic">Is alcohol</D:Label>
							</td>
							<td class="control">
                                <D:CheckBox runat="server" ID="cbIsAlcoholic" />
							</td>
                        </tr>
                        <tr>
							<td class="label">
							</td>
							<td class="control">
                                <D:Button ID="btnResetColor" runat="server" Text="Reset color" PreSubmitWarning="Are you sure you want to reset the color for this product?" />
							</td>	
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lblScheduleId"></D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxInt runat="server" ID="ddlScheduleId"></X:ComboBoxInt>	
							</td>
                        </tr>
                        <D:PlaceHolder ID="plhColors" runat="server">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblTextColor" />
							</td>
							<td class="control">
								<D:TextBoxString ID="tbTextColor" runat="server"></D:TextBoxString>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblBackgroundColor" />
							</td>
							<td class="control">
								<D:TextBoxString ID="tbBackgroundColor" runat="server"></D:TextBoxString>
							</td>
						</tr>
                        </D:PlaceHolder>                                                 
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblViewLayoutType">View layout</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxInt ID="ddlViewLayoutType" runat="server" UseDataBinding="true" notdirty="true"></X:ComboBoxInt>
							</td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" ID="lblSupportNotificationType">Support notifications</D:LabelEntityFieldInfo>
                             </td>   
                            <td class="control">
                                 <X:ComboBoxInt ID="ddlSupportNotificationType" runat="server" UseDataBinding="True" notdirty="true"></X:ComboBoxInt>
                             </td>  
                        </tr>
                        <tr>
                            <td class="label">
                                 <D:LabelEntityFieldInfo runat="server" ID="lblDeliveryLocationType">Delivery location</D:LabelEntityFieldInfo>
                             </td>   
                            <td class="control">
                                 <X:ComboBoxInt ID="ddlDeliveryLocationType" runat="server" UseDataBinding="True" notdirty="true"></X:ComboBoxInt>
                             </td>   
                            <td class="label">
                                <D:Label runat="server" ID="lblUrl">URL</D:Label>
							</td>
							<td class="control">
                                <D:TextBoxString ID="tbUrl" runat="server" UseDataBinding="false" ReadOnly="true"></D:TextBoxString>
							</td>   
                        </tr>
						<tr>
                            <td class="label">
                                <D:Label runat="server" ID="lblCoversType">Ask for Covers</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="cbCoversType" UseDataBinding="true" DisplayEmptyItem="false" IsRequired="true"></X:ComboBoxInt>
							</td>
						</tr>
					</table>
					</D:PlaceHolder>
				</controls>
                </X:TabPage>
                <X:TabPage Text="Advanced" Name="tabAdvanced">
                    <controls>
					<D:PlaceHolder ID="PlaceHolder1" runat="server">
					    <table class="dataformV2">
				            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPosproductId" />
							    </td>
							    <td class="control">
                                    <X:ComboBoxInt runat="server" id="ddlPosproductId"></X:ComboBoxInt>                                   
							    </td>
					            <td class="label">
						            <D:LabelEntityFieldInfo runat="server" id="lblRouteId">Route</D:LabelEntityFieldInfo>
					            </td>
					            <td class="control">
						            <X:ComboBoxInt runat="server" ID="ddlRouteId" DisplayEmptyItem="true"></X:ComboBoxInt>                                                                        
					            </td>
						    </tr>
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblExternalProductId" Text="External product" />
							    </td>
							    <td class="control">
								    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlExternalProductId" EntityName="ExternalProduct" IncrementalFilteringMode="StartsWith" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
							    </td>
	                            <td class="label"></td>
	                            <td class="control"></td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblPointOfInterestId" Text="Point of interest" />
                                </td>
                                <td class="control">
                                    <X:ComboBoxInt runat="server" ID="cbPointOfInterestId" IncrementalFilteringMode="StartsWith" EntityName="PointOfInterest" TextField="Name" ValueField="PointOfInterestId" />
                                </td>
                                <td class="label"></td>
                                <td class="control"></td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblAnnouncementAction">Announcement action</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat ="server" ID="cbAnnouncementAction" />
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblRateable" />
							    </td>
							    <td class="control">
								    <D:RadioTrueFalseNull ID="rblRateable" runat="server"></D:RadioTrueFalseNull>
							    </td>
							    <td class="label">
								    &nbsp;
							    </td>
							    <td class="control">
								    &nbsp;
							    </td>	
                            </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblGeofencing" />
							    </td>
							    <td class="control">
								    <D:RadioTrueFalseNull ID="rblGeofencing" runat="server"></D:RadioTrueFalseNull>
							    </td>
							    <td class="label">
								    &nbsp;
							    </td>
							    <td class="control">
								    &nbsp;
							    </td>	
                            </tr>
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblAllowFreeText" />
							    </td>
							    <td class="control">
								    <D:RadioTrueFalseNull ID="rblAllowFreeText" runat="server"></D:RadioTrueFalseNull>
							    </td>
							    <td class="label">
								    &nbsp;
							    </td>
							    <td class="control">
								    &nbsp;
							    </td>	
                            </tr>
                            <tr>
							    <td class="label">
                                    <D:LabelEntityFieldInfo runat="server" id="lblHidePrice">Hide price</D:LabelEntityFieldInfo>
                                </td>
                                <td class="control">
                                    <D:CheckBox runat ="server" ID="cbHidePrice" />
                                </td>
							    <td class="label">
								    &nbsp;
							    </td>
							    <td class="control">
								    &nbsp;
							    </td>	
                            </tr>                            
                            <tr>
                                <td class="label">
                                    <D:Label ID="lblCreateGenericProduct" runat="server" Text="Create generic product" Visible="false"></D:Label>
                                </td>
                                <td class="control">
                                    <D:Button ID="btnCreateGenericProduct" runat="server" Text="Create" Visible="false" />
                                </td>
                            </tr>
                        </table>
                    </D:PlaceHolder>
                    </controls>
                </X:TabPage>
                <X:TabPage Text="Alterations V1" Name="tabAlterations1">
                    <controls>					
                        <div class="divPageLeft">
                            <D:Panel ID="pnlProductAlterations" runat="server" GroupingText="Alterations V1">
                                <table class="dataformV2">
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV1_1">Alteration 1</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV1_1"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV1_2">Alteration 2</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV1_2"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV1_3">Alteration 3</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV1_3"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV1_4">Alteration 4</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV1_4"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV1_5">Alteration 5</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV1_5"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV1_6">Alteration 6</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV1_6"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV1_7">Alteration 7</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV1_7"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV1_8">Alteration 8</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV1_8"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV1_9">Alteration 9</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV1_9"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV1_10">Alteration 10</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV1_10"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
					            </table>
                            </D:Panel>
                        </div>
                        <div class="divPageRight">
                            <D:Panel ID="pnlInheritedAlterationV1" runat="server" GroupingText="Inherited alterations">
                                <table class="dataformV2">
                                    <tr>
                                        <td class="control"><D:CheckBox ID="cbInheritAlterationsFromBrand" ClientIDMode="Static" runat="server" Text="Inherit from brand" LocalizeText="False" Visible="False"  OnClick="handleChange(this.checked);" /></td>
                                    </tr>
                                </table> 
                                <D:PlaceHolder runat="server" ID="plhInheritedAlterationsV1"></D:PlaceHolder>
                            </D:Panel>
                        </div>
				</controls>
                </X:TabPage>
                <X:TabPage Text="Alteration v2" Name="tabAlterations2">
                    <controls>					
                        <D:Panel ID="pnlProductgroup" runat="server" GroupingText="Productgroup">
                                <table class="dataformV2">
                                    <tr>
                                        <td class="label">
                                            <D:Label runat="server" id="lblProductgroup">Productgroup</D:Label>
                                        </td>
                                        <td class="control">
                                            <X:ComboBoxInt runat="server" ID="ddlProductgroupId"></X:ComboBoxInt>                                                                        
                                        </td>
                                        <td class="label">
                                        </td>
                                        <td class="control">
                                        </td>
                                    </tr>                                    
                                </table>
                            </D:Panel>
                        <div class="divPageLeft">
                            <D:Panel ID="pnlProductAlterations2" runat="server" GroupingText="Alterations V2">
   <table class="dataformV2">
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV2_11">Alteration 1</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV2_11"></X:ComboBoxInt>
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV2_12">Alteration 2</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV2_12"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV2_13">Alteration 3</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV2_13"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV2_14">Alteration 4</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV2_14"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV2_15">Alteration 5</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV2_15"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV2_16">Alteration 6</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV2_16"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV2_17">Alteration 7</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV2_17"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV2_18">Alteration 8</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV2_18"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV2_19">Alteration 9</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV2_19"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
						            <tr>
							            <td class="label">
								            <D:Label runat="server" id="lblProductAlterationV2_20">Alteration 10</D:Label>
							            </td>
							            <td class="control" colspan="2">
                                            <X:ComboBoxInt runat="server" ID="ddlProductAlterationV2_20"></X:ComboBoxInt>                                            
							            </td>
						            </tr>
					            </table>
                            </D:Panel>
                        </div>
                        <div class="divPageRight">
                            <D:Panel ID="pnlInheritedAlterationV2" runat="server" GroupingText="Inherited alterations">
                                <table class="dataformV2">
                                    <tr>
                                        <td class="control"><D:CheckBox ID="cbInheritAlterations" runat="server" Text="Inherit from categories" LocalizeText="False" /></td>
                                    </tr>
                                    <tr>
                                        <td class="control"><D:CheckBox ID="cbInheritAlterationsV3FromBrand" ClientIDMode="Static" runat="server" Text="Inherit from brand" LocalizeText="False" Visible="False" OnClick="handleChange(this.checked);" /></td>
                                    </tr>
                                </table>                                
                                <D:PlaceHolder runat="server" ID="plhInheritedAlterationsV2"></D:PlaceHolder>
                            </D:Panel>
                        </div>
				</controls>
                </X:TabPage>
                <X:TabPage Text="Info" Name="Info">
				    <Controls>
                        <D:PlaceHolder runat="server" ID="plhAlterations"></D:PlaceHolder>
					    <D:PlaceHolder runat="server" ID="plhLinkedAlterationoptions"></D:PlaceHolder> 
                        <D:Panel ID="pnlLinkedProducts" runat="server" GroupingText="Linked Products" Visible="False">
                            <D:PlaceHolder runat="server" ID="plhLinkedProducts"></D:PlaceHolder>
                        </D:Panel>
				    </Controls>
			    </X:TabPage>                		
            </TabPages>
        </X:PageControl>

        <div style="visibility:hidden">
            <D:TextBoxString ID="tbGenericDescriptionPlaceholder" runat="server" ClientIDMode="Static"></D:TextBoxString>
            <D:TextBoxString ID="tbDescriptionPlaceholder" runat="server" ClientIDMode="Static"></D:TextBoxString>
        </div>
        
        <script type="text/javascript">
            function setGenericDescription() {
                var description = document.getElementById("tbGenericDescriptionPlaceholder").value;
                document.getElementById("tbDescription").innerHTML = description;
                document.getElementById("tbDescription").scrollTop = 99999;
                document.getElementById("markdown-output").innerHTML = markdown.toHTML(description);                
            }

            function setDescription() {
                var description = document.getElementById("tbDescriptionPlaceholder").value;
                document.getElementById("tbDescription").innerHTML = description;
                document.getElementById("tbDescription").scrollTop = 99999;
                document.getElementById("markdown-output").innerHTML = markdown.toHTML(description);                
            }

            function parseMarkdown(obj) {
                var text = obj.value;
                document.getElementById("markdown-output").innerHTML = markdown.toHTML(text);
            }

            var description = document.getElementById("tbDescription");
            if (description != null) {
                parseMarkdown(description);
            } else {
                parseMarkdown(document.getElementById("tbGenericDescription"));
            }

            function handleChange(isChecked) {
                document.getElementById("cbInheritAlterationsV3FromBrand").checked = isChecked;
                document.getElementById("cbInheritAlterationsFromBrand").checked = isChecked;
            }

        </script>
    </div>
</asp:Content>
