﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Dionysos.Web;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Data.LLBLGen;

namespace Obymobi.ObymobiCms.Catalog
{
	public partial class GenericproductImport : Dionysos.Web.UI.PageQueryStringDataBinding
	{
		#region Methods

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			this.SetGui();
			if (QueryStringHelper.HasValue("gkdo"))
				this.GenerateGenericproducts();
		}

		protected override void SetDefaultValuesToControls()
		{
			this.ddlSupplierId.Value = null;
			this.ddlGenericcategoryId.Value = null;
			this.ddlCategoryId.Value = null;
			this.cbImport.Checked = true;
		}

		void SetGui()
		{			
			this.lblCompanyValue.Text = new CompanyEntity(CmsSessionHelper.CurrentCompanyId).Name;

			// Load Categories of Company
			this.ddlCategoryId.DataSource = CmsSessionHelper.GetCategoriesForCompanyFlat(true);
			this.ddlCategoryId.DataBind();

			// Load Products to Copy
			if (this.ddlSupplierId.ValidId > 0 &&
				this.ddlGenericcategoryId.ValidId > 0 &&
				this.ddlCategoryId.ValidId > 0)
			{
				if (this.cbImport.Checked)
				{
					// From Genericproduct to Product
					GenericproductCollection genericproducts = new GenericproductCollection();

					PredicateExpression filterProducts = new PredicateExpression();
					filterProducts.Add(GenericproductFields.SupplierId == this.ddlSupplierId.ValidId);
					filterProducts.Add(GenericproductFields.GenericcategoryId == this.ddlGenericcategoryId.ValidId);

					SortExpression sortProducts = new SortExpression();
					sortProducts.Add(GenericproductFields.Name | SortOperator.Ascending);

					genericproducts.GetMulti(filterProducts, 0, sortProducts);

					this.cblProductsToImport.DataTextField = "Name";
					this.cblProductsToImport.DataValueField = "GenericproductId";
					this.cblProductsToImport.DataSource = genericproducts;
					this.cblProductsToImport.DataBind();
				}
				else
				{
					// From Product to Genericproduct
					ProductCollection products = new ProductCollection();

					PredicateExpression filterProducts = new PredicateExpression();
					filterProducts.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
					filterProducts.Add(ProductCategoryFields.CategoryId == this.ddlCategoryId.ValidId);

					RelationCollection joins = new RelationCollection();
					joins.Add(ProductEntity.Relations.ProductCategoryEntityUsingProductId);

					SortExpression sortProducts = new SortExpression();
					sortProducts.Add(ProductFields.Name | SortOperator.Ascending);

					products.GetMulti(filterProducts, 0, sortProducts, joins);

					this.cblProductsToImport.DataTextField = "Name";
					this.cblProductsToImport.DataValueField = "GenericproductId";
					this.cblProductsToImport.DataSource = products;
					this.cblProductsToImport.DataBind();
				}
			}
		}

		int GetCreateGenericCategoryIdByName(string categoryName)
		{
			PredicateExpression filter = new PredicateExpression(GenericcategoryFields.Name == categoryName);
			var gcategory = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<GenericcategoryEntity>(filter);

			if (gcategory == null)
			{
				gcategory = new GenericcategoryEntity();
				gcategory.Name = categoryName;
				gcategory.Save();
			}

			return gcategory.GenericcategoryId;
		}

		void GenerateGenericproducts()
		{
			// Do all for current company
			ProductCollection products = new ProductCollection();
			PredicateExpression productfilter = new PredicateExpression();
			productfilter.Add(ProductFields.GenericproductId == DBNull.Value);

			PredicateExpression filterProductSelectionMooii = new PredicateExpression();
			filterProductSelectionMooii.Add(ProductFields.ProductId < 4769);
			filterProductSelectionMooii.Add(ProductFields.CompanyId == 37);

			PredicateExpression filterProductSelectionMoskou = new PredicateExpression();
			filterProductSelectionMoskou.Add(ProductFields.ProductId > 4878);
			filterProductSelectionMoskou.Add(ProductFields.CompanyId == 38);

			PredicateExpression filterProductSelection = new PredicateExpression();
			filterProductSelection.Add(filterProductSelectionMooii);
			filterProductSelection.AddWithOr(filterProductSelectionMoskou);

			productfilter.Add(filterProductSelection);
			
			SortExpression sort = new SortExpression();
			sort.Add(ProductFields.Name | SortOperator.Ascending);

			products.GetMulti(productfilter, 0, sort);

			foreach (var product in products)
			{
				GenericproductEntity gproduct = new GenericproductEntity();
				gproduct.SupplierId = this.ddlSupplierId.ValidId;
				if(product.ProductCategoryCollection.Count > 0)
					gproduct.GenericcategoryId = this.GetCreateGenericCategoryIdByName(product.ProductCategoryCollection[0].CategoryEntity.Name);
				gproduct.Name = product.Name + " [IMPORTED]";
				gproduct.VattariffId = product.VattariffId;
				gproduct.Description = product.Description;
				gproduct.TextColor = product.TextColor;
				gproduct.BackgroundColor = product.BackgroundColor;
				gproduct.SupplierId = 19;
				gproduct.Save(true);

				// Move the Media
				foreach (var medium in product.MediaCollection)
				{
					medium.ProductId = null;
					medium.GenericproductId = gproduct.GenericproductId;
					medium.Save();
				}

				// Make the orignal product a related to this Genericproduct
				product.GenericproductId = gproduct.GenericproductId;
				product.Description = string.Empty;
				product.TextColor = string.Empty;
				product.BackgroundColor = string.Empty;
				product.Save();
			}		
		}

		void GenerateProducts()
		{
            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(null);

            EntityView<ProductEntity> productView = productCollection.DefaultView;
            PredicateExpression filter = null;

			for (int i = 0; i < this.cblProductsToImport.Items.Count; i++)
			{
				if (this.cblProductsToImport.Items[i].Selected)
				{
					GenericproductEntity gproduct = ((GenericproductCollection)this.cblProductsToImport.DataSource)[i];

                    filter = new PredicateExpression();
                    filter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                    filter.Add(ProductFields.GenericproductId == gproduct.GenericproductId);

                    productView.Filter = filter;

                    if (productView.Count == 0)
                    {
                        ProductEntity product = new ProductEntity();
                        product.CompanyId = CmsSessionHelper.CurrentCompanyId;
                        product.GenericproductId = gproduct.GenericproductId;

                        // GK We don't want to enter the overrides, we inherit
                        product.Name = gproduct.Name;
                        product.PriceIn = 0;
                        product.VattariffId = (gproduct.VattariffId.HasValue ? gproduct.VattariffId.Value : VattariffEntity.GetDefaultVattariff().VattariffId);
                        /*
                        product.Description = gproduct.Description;
                        product.TextColor = gproduct.TextColor;
                        product.BackgroundColor = gproduct.BackgroundColor;
                        product.AgeId = gproduct.AgeId;*/

                        var productCategory = product.ProductCategoryCollection.AddNew();
                        productCategory.CategoryId = this.ddlCategoryId.ValidId;

                        // Don't copy media, "inherit" it from the Genericproduct					

                        if (product.Save(true))
                            productView.RelatedCollection.Add(product);
                    }
				}
			}	
		}

		void HookupEvents()
		{
			this.ddlSupplierId.SelectedIndexChanged += new EventHandler(ddlSupplierId_SelectedIndexChanged);
			this.ddlGenericcategoryId.SelectedIndexChanged += new EventHandler(ddlGenericcategoryId_SelectedIndexChanged);
			this.ddlCategoryId.SelectedIndexChanged += new EventHandler(ddlCategoryId_SelectedIndexChanged);
			this.cbImport.CheckedChanged += new EventHandler(cbImport_CheckedChanged);
			this.btImport.Click += new EventHandler(btImport_Click);
		}

		#endregion

		#region Event Handlers

		protected void Page_Load(object sender, EventArgs e)
		{
			this.HookupEvents();
		}

		void cbImport_CheckedChanged(object sender, EventArgs e)
		{
			QueryStringHelper qs = this.GetQueryStringInstanceWithValues();
			WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
		}

		void ddlSupplierId_SelectedIndexChanged(object sender, EventArgs e)
		{
			QueryStringHelper qs = this.GetQueryStringInstanceWithValues();
			WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
		}

		void ddlGenericcategoryId_SelectedIndexChanged(object sender, EventArgs e)
		{
			QueryStringHelper qs = this.GetQueryStringInstanceWithValues();
			WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
		}

		void ddlCategoryId_SelectedIndexChanged(object sender, EventArgs e)
		{
			QueryStringHelper qs = this.GetQueryStringInstanceWithValues();
			WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
		}

		void btImport_Click(object sender, EventArgs e)
		{
			if (this.cbImport.Checked)
			{
				// Create Products for Genericproducts
				this.GenerateProducts();
			}
			else
			{ 
				// Create Genericproducts from Products
				this.GenerateGenericproducts();
			}
		}

		#endregion
	}
}