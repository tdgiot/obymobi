using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.DevExControls;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Alterationitem : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += this.Alterationitem_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Alterationitem_DataSourceLoaded(object sender)
        {
            this.SetFilters();
            this.SetGui();
        }        

        private void SetFilters()
        {
            PredicateExpression filter2 = new PredicateExpression();
            if (this.DataSourceAsAlterationitemEntity.ParentBrandId.HasValue)
            {
                filter2.Add(AlterationoptionFields.BrandId == this.DataSourceAsAlterationitemEntity.ParentBrandId);
            }
            else
            {
                filter2.Add(AlterationoptionFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            }
            filter2.Add(AlterationoptionFields.Version == 1);

            SortExpression sort2 = new SortExpression(new SortClause(AlterationoptionFields.Name, SortOperator.Ascending));

            AlterationoptionCollection alterationoptionCollection = new AlterationoptionCollection();
            alterationoptionCollection.GetMulti(filter2, 0, sort2);

            this.ddlAlterationoptionId.DataSource = alterationoptionCollection;
            this.ddlAlterationoptionId.DataBind();

            PredicateExpression filter = new PredicateExpression();
            if (this.DataSourceAsAlterationitemEntity.ParentBrandId.HasValue)
            {
                filter.Add(AlterationFields.BrandId == this.DataSourceAsAlterationitemEntity.ParentBrandId);
            }
            else
            {
                filter.Add(AlterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            }
            filter.Add(AlterationFields.Version == 1);

            SortExpression sort = new SortExpression(new SortClause(AlterationFields.Name, SortOperator.Ascending));

            AlterationCollection alterationCollection = new AlterationCollection();
            alterationCollection.GetMulti(filter, 0, sort);

            this.ddlAlterationId.DataSource = alterationCollection;
            this.ddlAlterationId.DataBind();

            SetPosalterationitemIdFilter();
        }

        private void SetPosalterationitemIdFilter()
        {
            PredicateExpression posalterationitemFilter = new PredicateExpression(PosalterationitemFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression posalterationitemSort = new SortExpression(new SortClause(PosalterationitemFields.ExternalId, SortOperator.Ascending));

            PosalterationitemCollection posalterationitemCollection = new PosalterationitemCollection();
            if (!this.DataSourceAsAlterationitemEntity.ParentBrandId.HasValue)
            {
                posalterationitemCollection.GetMulti(posalterationitemFilter, 0, posalterationitemSort);
            }

            foreach (PosalterationitemEntity posalterationitem in posalterationitemCollection)
            {
                if (!string.IsNullOrWhiteSpace(posalterationitem.RevenueCenter))
                {
                    this.ddlPosalterationitemId.AddItem($"{posalterationitem.ExternalId} [{posalterationitem.RevenueCenter}]", posalterationitem.PosalterationitemId);
                }
                else
                {
                    this.ddlPosalterationitemId.AddItem($"{posalterationitem.ExternalId}", posalterationitem.PosalterationitemId);
                }
            }
        }

        private void SetGui()
        {
            if (!this.DataSourceAsAlterationitemEntity.IsNew)
            {
                this.hlAlterationoptionId.Text = this.DataSourceAsAlterationitemEntity.AlterationoptionEntity.Name + " &#187;&#187;";
                this.hlAlterationoptionId.NavigateUrl = ResolveUrl(string.Format("~/Catalog/Alterationoption.aspx?id={0}", this.DataSourceAsAlterationitemEntity.AlterationoptionId));

                this.hlAlterationId.Text = this.DataSourceAsAlterationitemEntity.AlterationEntity.Name + " &#187;&#187;";
                this.hlAlterationId.NavigateUrl = ResolveUrl(string.Format("~/Catalog/Alteration.aspx?id={0}", this.DataSourceAsAlterationitemEntity.AlterationId));

                if (this.DataSourceAsAlterationitemEntity.PosalterationitemId.HasValue)
                {
                    this.hlPosalterationitemId.Text = this.DataSourceAsAlterationitemEntity.PosalterationitemEntity.ExternalId + " &#187;&#187;";
                    this.hlPosalterationitemId.NavigateUrl = ResolveUrl(string.Format("~/Catalog/Posalterationitem.aspx?id={0}", this.DataSourceAsAlterationitemEntity.PosalterationitemId.Value));
                }

                this.tbPrice.Text = this.DataSourceAsAlterationitemEntity.AlterationoptionEntity.PriceIn.GetValueOrDefault(0).ToString("0.00");
            }

            if (this.DataSourceAsAlterationitemEntity.ParentBrandId.HasValue)
            {
                this.tabsMain.TabPages.FindByName("tabAdvanced").Visible = false;
            }
        }

        public override bool Save()
        {
            bool success = base.Save();

            decimal price;
            if (success && decimal.TryParse(this.tbPrice.Text, out price))
            {
                this.DataSourceAsAlterationitemEntity.AlterationoptionEntity.PriceIn = price;
                this.DataSourceAsAlterationitemEntity.AlterationoptionEntity.Save();
            }

            if (this.cbSelectedOnDefault.Value)
            {
                AlterationEntity alterationEntity = new AlterationEntity(this.DataSourceAsAlterationitemEntity.AlterationId);
                if (alterationEntity.IsNew)
                {
                    // Hmmmmmz
                }
                else if (alterationEntity.MaxOptions == 1)
                {
                    // Check if there's another alterationitem set as default for this alteration. If so, reset to false
                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(AlterationitemFields.AlterationId == this.DataSourceAsAlterationitemEntity.AlterationId);
                    filter.Add(AlterationitemFields.SelectedOnDefault == true);
                    filter.Add(AlterationitemFields.Version == 1);

                    if (!this.DataSourceAsAlterationitemEntity.IsNew)
                        filter.AddWithAnd(AlterationitemFields.AlterationitemId != this.DataSourceAsAlterationitemEntity.AlterationitemId);

                    AlterationitemCollection defaultAlterationitems = new AlterationitemCollection();
                    defaultAlterationitems.GetMulti(filter);

                    foreach (AlterationitemEntity alterationitem in defaultAlterationitems)
                    {
                        alterationitem.SelectedOnDefault = false;
                        alterationitem.Save();
                    }
                }
            }

            return success;
        }

        #endregion
        
		#region Properties

		
		/// <summary>
		/// Return the page's datasource as a Entity
		/// </summary>
		public AlterationitemEntity	DataSourceAsAlterationitemEntity
		{
			get
			{
				return this.DataSource as AlterationitemEntity;
			}
		}

		#endregion        
    }
}
