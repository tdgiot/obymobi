﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data;
using Obymobi.Enums;


namespace Obymobi.ObymobiCms.Catalog
{
    public partial class ProductMatching : Dionysos.Web.UI.PageQueryStringDataBinding
    {
        private string noneDashChooseText = Dionysos.Global.TranslationProvider.GetTranslation(ObymobiCmsTranslatables.NoneDashChoose);

        #region Methods

        protected override void SetDefaultValuesToControls()
        {
        }

        private void SetGui()
        {
        }

        private void RenderProductsToMatch()
        {
            this.plhProducts.Controls.Clear();

            // Check what to render
            ProductCollection products = new ProductCollection();
            PredicateExpression productFilter = new PredicateExpression();
            productFilter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            productFilter.Add(ProductFields.VisibilityType != VisibilityType.Never);
            productFilter.Add(ProductCategoryFields.ProductCategoryId != DBNull.Value);
            productFilter.Add(CategoryFields.ParentCategoryId != DBNull.Value);

            if (this.cbOnlyNoGenericproduct.Checked)
                productFilter.Add(ProductFields.GenericproductId == DBNull.Value);

            if (this.cbOnlyNoPosproduct.Checked)
                productFilter.Add(ProductFields.PosproductId == DBNull.Value);

            RelationCollection joins = new RelationCollection();
            joins.Add(ProductEntity.Relations.ProductCategoryEntityUsingProductId, JoinHint.Left);
            joins.Add(ProductCategoryEntity.Relations.CategoryEntityUsingCategoryId);

            PrefetchPath path = new PrefetchPath(EntityType.ProductEntity);
            path.Add(ProductEntity.PrefetchPathCategoryCollectionViaProductCategory).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity);		

            SortExpression sort = new SortExpression();
            sort.Add(ProductFields.Name | SortOperator.Ascending);

            products.GetMulti(productFilter, 0, sort, joins, path);

            // Load the generic products
            GenericproductCollection genericProducts = new GenericproductCollection();

            SortExpression sortGeneric = new SortExpression();
            sortGeneric.Add(GenericproductFields.Name | SortOperator.Ascending);
            genericProducts.GetMulti(null, 0, sortGeneric);

            // Load the POS products
            PosproductCollection posProducts = new PosproductCollection();

            PredicateExpression posFilter = new PredicateExpression();
            posFilter.Add(PosproductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            SortExpression sortPos = new SortExpression();
            sortPos.Add(PosproductFields.Name | SortOperator.Ascending);
            posProducts.GetMulti(posFilter, 0, sortPos);

            int productCount = 0;
            int treshold = 5;

            for (int i = 0; i < products.Count; i++)
            {
                ProductEntity current = products[i];

                bool isFood = false;
                bool isDrink = false;
                foreach (CategoryEntity category in current.CategoryCollectionViaProductCategory)
                {
                    if (category.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase))
                        isFood = true;
                    else if (   
                                category.ParentCategoryId.HasValue && 
                                category.ParentCategoryEntity.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase)
                            )
                        isFood = true;
                    else if (
                                category.ParentCategoryId.HasValue && 
                                category.ParentCategoryEntity.ParentCategoryId.HasValue && 
                                category.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase)
                            )
                        isFood = true;
                    else if (
                                category.ParentCategoryId.HasValue && 
                                category.ParentCategoryEntity.ParentCategoryId.HasValue && 
                                category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue && 
                                category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase)
                            )
                        isFood = true;
                    else if (   
                                category.ParentCategoryId.HasValue && 
                                category.ParentCategoryEntity.ParentCategoryId.HasValue && 
                                category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue && 
                                category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Eten", StringComparison.InvariantCultureIgnoreCase)
                            )
                        isFood = true;
                    if (category.Name.Equals("Drinken", StringComparison.InvariantCultureIgnoreCase))
                        isDrink = true;
                    else if (
                                category.ParentCategoryId.HasValue &&
                                category.ParentCategoryEntity.Name.Equals("Drinken", StringComparison.InvariantCultureIgnoreCase)
                            )
                        isDrink = true;
                    else if (
                                category.ParentCategoryId.HasValue &&
                                category.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                category.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Drinken", StringComparison.InvariantCultureIgnoreCase)
                            )
                        isDrink = true;
                    else if (
                                category.ParentCategoryId.HasValue &&
                                category.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Drinken", StringComparison.InvariantCultureIgnoreCase)
                            )
                        isDrink = true;
                    else if (
                                category.ParentCategoryId.HasValue &&
                                category.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryId.HasValue &&
                                category.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.ParentCategoryEntity.Name.Equals("Drinken", StringComparison.InvariantCultureIgnoreCase)
                            )
                        isDrink = true;
                }

                if (this.cbOnlyFood.Checked && !isFood)
                {
                    // Only food
                }
                else if (this.cbOnlyDrinks.Checked && !isDrink)
                {
                    // Only drinks
                }
                else
                {
                    productCount++;

                    // Render row per product
                    this.plhProducts.AddHtml("<tr>");
                    this.plhProducts.AddHtml("<td class=\"control\">");

                    TextBoxString tbName = new TextBoxString();
                    tbName.ID = "tb-" + current.ProductId.ToString();
                    tbName.UseDataBinding = false;
                    tbName.Value = current.Name;

                    this.plhProducts.Controls.Add(tbName);
                    this.plhProducts.AddHtml("</td>");

                    this.plhProducts.AddHtml("<td class=\"control\">");

                    Dionysos.Web.UI.WebControls.DropDownListInt ddlGenericproductId = new Dionysos.Web.UI.WebControls.DropDownListInt();
                    ddlGenericproductId.ID = "ddlGenericproductId-" + current.ProductId.ToString();
                    ddlGenericproductId.UseDataBinding = false;
                    ddlGenericproductId.DataTextField = GenericproductFields.Name.Name;
                    ddlGenericproductId.DataValueField = GenericproductFields.GenericproductId.Name;
                    ddlGenericproductId.DataSource = genericProducts;
                    ddlGenericproductId.DataBind();

                    ddlGenericproductId.Items.Insert(0, new ListItem(this.noneDashChooseText));

                    if (current.GenericproductId.HasValue)
                    {
                        ddlGenericproductId.Value = current.GenericproductId.Value;
                        ddlGenericproductId.CssClass += " valid";
                    }
                    else
                    {
                        bool found = false;
                        int genericProductId = -1;

                        if (this.cbSuggestGenericproducts.Checked)
                        {
                            
                            int distance = -1;
                            int minDistance = -1;
                            foreach (var genericProduct in genericProducts)
                            {
                                distance = genericProduct.Name.ComputeLevenshteinDistance(current.Name);
                                if (distance < treshold && (distance < minDistance || minDistance == -1))
                                {
                                    genericProductId = genericProduct.GenericproductId;
                                    minDistance = distance;
                                    found = true;
                                    break;
                                }
                            }
                        }

                        if (found)
                        {
                            ddlGenericproductId.Value = genericProductId;
                            ddlGenericproductId.CssClass += " suggestion";
                        }
                        else
                        {
                            ddlGenericproductId.SelectedIndex = 0;
                            ddlGenericproductId.CssClass += " empty";
                        }
                    }

                    this.plhProducts.Controls.Add(ddlGenericproductId);

                    this.plhProducts.AddHtml("</td>");
                    this.plhProducts.AddHtml("<td class=\"control\">");

                    Dionysos.Web.UI.WebControls.DropDownListInt ddlPosproductId = new Dionysos.Web.UI.WebControls.DropDownListInt();
                    ddlPosproductId.ID = "ddlPosproductId-" + current.ProductId.ToString();
                    ddlPosproductId.UseDataBinding = false;
                    ddlPosproductId.DataTextField = PosproductFields.Name.Name;
                    ddlPosproductId.DataValueField = PosproductFields.PosproductId.Name;
                    ddlPosproductId.DataSource = posProducts;
                    ddlPosproductId.DataBind();

                    ddlPosproductId.Items.Insert(0, new ListItem(this.noneDashChooseText));

                    if (current.PosproductId.HasValue)
                    {
                        ddlPosproductId.Value = current.PosproductId.Value;
                        ddlPosproductId.CssClass += " valid";
                    }
                    else
                    {
                        ddlPosproductId.SelectedIndex = 0;
                        ddlPosproductId.CssClass += " empty";
                    }

                    this.plhProducts.Controls.Add(ddlPosproductId);

                    this.plhProducts.AddHtml("</td>");
                    this.plhProducts.AddHtml("<td>");
                    this.plhProducts.AddHtml("</td>");
                    this.plhProducts.AddHtml("</tr>");
                }
            }

            string productCountSingular = this.Translate("ProductCountSingular", "product");
            string productCountPlural = this.Translate("ProductCountSingular", "producten");            

            this.lblProductCount.Text = string.Format("{0} {1}", productCount, (productCount == 1 ? productCountSingular : productCountPlural));
        }

        private void SaveMatches()
        {
            // Match
            foreach (var Control in this.plhProducts.Controls)
            {
                if (Control is Dionysos.Web.UI.WebControls.DropDownListInt)
                {
                    var ddl = Control as Dionysos.Web.UI.WebControls.DropDownListInt;
                    int productId = Convert.ToInt32(Dionysos.StringUtil.GetAllAfterFirstOccurenceOf(ddl.ID, '-'));

                    // Get the product
                    ProductEntity product = new ProductEntity(productId);

                    // verify company
                    if (product.CompanyId != CmsSessionHelper.CurrentCompanyId)
                        throw new Dionysos.FunctionalException("Product with ProductId '{0}' does not belong to Company '{1}'", product.ProductId, CmsSessionHelper.CurrentCompanyId);

                    if (ddl.ValidId > 0)
                    {
                        if (ddl.ID.StartsWith("ddlGenericproductId"))
                            product.GenericproductId = ddl.ValidId;
                        else if (ddl.ID.StartsWith("ddlPosproductId"))
                            product.PosproductId = ddl.ValidId;
                    }
                    else
                    {
                        if (ddl.ID.StartsWith("ddlGenericproductId"))
                            product.GenericproductId = null;
                        else if (ddl.ID.StartsWith("ddlPosproductId"))
                            product.PosproductId = null;
                    }

                    product.Save();
                }
                else if (Control is TextBoxString)
                {
                    var textbox = Control as TextBoxString;
                    int productId = Convert.ToInt32(Dionysos.StringUtil.GetAllAfterFirstOccurenceOf(textbox.ID, '-'));

                    if (!textbox.Value.IsNullOrWhiteSpace())
                    {
                        // Get the product
                        ProductEntity product = new ProductEntity(productId);

                        // verify company
                        if (product.CompanyId != CmsSessionHelper.CurrentCompanyId)
                            throw new Dionysos.FunctionalException("Product with ProductId '{0}' does not belong to Company '{1}'", product.ProductId, CmsSessionHelper.CurrentCompanyId);

                        product.Name = textbox.Value;
                        product.Save();
                    }
                }
            }
        }

        void HookupEvents()
        {
            this.btChangeCategory.Click += new EventHandler(btChangeCategory_Click);
            this.btMatch.Click += new EventHandler(btMatch_Click);
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.SetGui();
            base.OnInit(e);
            this.RenderProductsToMatch();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
        }

        private void btChangeCategory_Click(object sender, EventArgs e)
        {
            QueryStringHelper qs = this.GetQueryStringInstanceWithValues();
            WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        private void btMatch_Click(object sender, EventArgs e)
        {
            this.SaveMatches();

            QueryStringHelper qs = this.GetQueryStringInstanceWithValues();
            WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        #endregion
    }
}