﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.AlterationitemV3" CodeBehind="AlterationitemV3.aspx.cs" %>

<%@ Reference Control="~/Generic/SubPanels/OptionPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
	<div>
		<X:PageControl ID="tabsMain" runat="server" Width="100%">
			<TabPages>
				<X:TabPage Text="Algemeen" Name="Generic">
					<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblAlterationoptionId">Alteration option</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlAlterationoptionId" EntityName="Alterationoption" IncrementalFilteringMode="StartsWith" TextField="NameAndPriceIn" ValueField="AlterationoptionId" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
							</td>
                            <td class="label">
                            </td>
                            <td class="control">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlAlterationoptionId"></D:HyperLink>
                            </td>
						</tr>
                        <tr>
							<td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblAlterationId">Alteration</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlAlterationId" EntityName="Alteration" DisabledOnceSelectedFromQueryString="True" DisabledOnceSelectedAndSaved="True" IncrementalFilteringMode="StartsWith" TextField="Name" PreventEntityCollectionInitialization="True" ValueField="AlterationId"></X:ComboBoxLLBLGenEntityCollection>
							</td> 
                            <td class="label">
                            </td>
                            <td class="control">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlAlterationId"></D:HyperLink>
                            </td>
                        </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort order</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxInt ID="tbSortOrder" runat="server"></D:TextBoxInt>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSelectedOnDefault">Selected on default</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbSelectedOnDefault" />
							</td>
                        </tr>    
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblPrice">Price</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxDecimal runat="server" ID="tbPrice" />
                            </td>
                        </tr>                    
                        <tr>    
                            <td class="label">
                                <D:Label runat="server" id="lblComponents">Nested alterations</D:Label>
                            </td>
                            <td class="control" colspan="3">
                                <div style="width: 736px;">
                                    <D:PlaceHolder runat="server" ID="plhOptions"></D:PlaceHolder>
                                </div>
                            </td>
                        </tr>
					 </table>			
				</controls>
				</X:TabPage>
				<X:TabPage Text="Advanced" Name="tabAdvanced">
					<controls>
				<D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
				        <tr>
                            <td class="label">
								<D:Label runat="server" id="lblPosalterationitemId">Pos alterationitem</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxInt runat="server" ID="ddlPosalterationitemId" />
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlPosalterationitemId"></D:HyperLink>
							</td>
							<td class="control">
							</td>
						</tr>
                    </table>
                </D:PlaceHolder>
                </controls>
				</X:TabPage>
			</TabPages>
		</X:PageControl>
	</div>
</asp:Content>

