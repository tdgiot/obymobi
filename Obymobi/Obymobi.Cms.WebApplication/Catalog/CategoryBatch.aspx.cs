﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using DevExpress.Web.ASPxTreeList;
using Dionysos.Data.LLBLGen;
using Dionysos.Web;
using Dionysos.Web.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.Logic.Requests;
using Obymobi.Cms.Logic.UseCases;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.Media;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class CategoryBatch : PageDefault
    {
        #region Properties

        private const int TYPE_CATEGORY = 0;
        private const int TYPE_PRODUCT = 1;

        private int nodeIndex = 0;
        private List<BatchItem> root = new List<BatchItem>();
        private bool rootHasCategories = false;
        private bool rootHasProducts = false;

        private Dictionary<int, AlterationEntity> importedAlterations = null;
        private Transaction transaction = null;
        private Dictionary<MediaEntity, MediaEntity> oldMediaEntities = null;

        #endregion

        #region Methods

        private void HookupEvents()
        {
            this.btPreview.Click += btPreview_Click;
            this.btCreate.Click += btCreate_Click;
            this.tlCategories.HtmlRowPrepared += tlCategories_HtmlRowPrepared;
            this.ddlMenuId.ValueChanged += ddlMenuId_ValueChanged;
        }

        void ddlMenuId_ValueChanged(object sender, EventArgs e)
        {
            if (this.ddlMenuId.ValidId > 0)
            {
                this.ddlCategoryId.DataSource = CategoryHelper.GetOrderedCategories(this.ddlMenuId.ValidId);
                this.ddlCategoryId.DataBind();
            }
            else
            {
                this.ddlCategoryId.DataSource = new CategoryCollection();
                this.ddlCategoryId.DataBind();
            }
        }

        private void SetFilters()
        {
            // Menu dropdown
            MenuCollection menus = new MenuCollection();
            menus.GetMulti(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            // Create an entityView for sorting
            EntityView<MenuEntity> evMenuView = menus.DefaultView;

            // Sort by name
            SortExpression sortByName = new SortExpression(MenuFields.Name | SortOperator.Ascending);

            // Apply the sort
            evMenuView.Sorter = sortByName;

            // Fill the ddl
            this.ddlMenuId.DataSource = evMenuView;
            this.ddlMenuId.DataBind();

            // Check if a menu id was passed to this page and select it
            int menuId;
            if (QueryStringHelper.TryGetValue("menuId", out menuId))
            {
                evMenuView.Filter = new PredicateExpression(MenuFields.MenuId == menuId);
                if (evMenuView.Count > 0)
                {
                    this.ddlMenuId.Value = menuId;

                    ddlMenuId_ValueChanged(null, null);
                }
            }
        }

        private bool Validate(int companyId, bool isPreview)
        {
            this.MultiValidatorDefault.ClearErrors();
            if (this.ddlMenuId.ValidId <= 0)
            {
                this.MultiValidatorDefault.AddError("No menu has been selected");
                base.Validate();
                return false;
            }

            if (this.tbBatch.Text.Length <= 0 && !this.fuDocument.HasFile)
            {
                this.MultiValidatorDefault.AddError("No batch file has been selected, nor any batch text has been entered");
                base.Validate();
                return false;
            }
            else if (this.tbBatch.Text.Length > 0 && this.fuDocument.HasFile)
            {
                this.MultiValidatorDefault.AddError("Both a batch file and a batch text has been supplied. Please supply only one of the two options.");
                base.Validate();
                return false;
            }
            else if (this.fuDocument.HasFile)
            {
                string fileExtension = System.IO.Path.GetExtension(this.fuDocument.FileName).ToLower();
                if (fileExtension != ".txt")
                {
                    this.MultiValidatorDefault.AddError("The batch file doesn't have the right extension. Only .txt files are supported.");
                    base.Validate();
                    return false;
                }
            }

            string batchText = string.Empty;
            if (!this.fuDocument.HasFile)
                batchText = this.tbBatch.Text;
            else
            {
                using (StreamReader reader = new StreamReader(this.fuDocument.FileContent))
                {
                    batchText = reader.ReadToEnd();
                    if (isPreview)
                        this.tbBatch.Text = batchText;
                }
            }

            this.importedAlterations = this.GetImportedAlterations(companyId);

            // Validate the script
            string[] lines = batchText.Split('\n');
            int index = 0;

            CategoryBatch.AppendLog("Validating lines...");
            if (!ValidateLines(lines, ref index, 0, null, 100, companyId))
            {
                base.Validate();
                return false;
            }
            if (this.ddlCategoryId.ValidId > 0)
            {
                if (this.rootHasCategories)
                {
                    ProductCategoryCollection categoryProducts = new ProductCategoryCollection();
                    if (this.transaction != null)
                        categoryProducts.AddToTransaction(this.transaction);
                    categoryProducts.GetMulti(ProductCategoryFields.CategoryId == this.ddlCategoryId.ValidId);
                    if (categoryProducts.Count > 0)
                    {
                        this.MultiValidatorDefault.AddError("It is not possible to add sub categories to the selected category because it contains products");
                        base.Validate();
                        return false;
                    }
                }
                else if (this.rootHasProducts)
                {
                    CategoryCollection categoryChildren = new CategoryCollection();
                    if (this.transaction != null)
                        categoryChildren.AddToTransaction(this.transaction);
                    categoryChildren.GetMulti(CategoryFields.ParentCategoryId == this.ddlCategoryId.ValidId);
                    if (categoryChildren.Count > 0)
                    {
                        this.MultiValidatorDefault.AddError("It is not possible to add products to the selected category because it has sub categories");
                        base.Validate();
                        return false;
                    }
                }
            }

            return true;
        }

        private Dictionary<int, AlterationEntity> GetImportedAlterations(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationFields.CompanyId == companyId);
            filter.Add(AlterationFields.Version == 1);

            AlterationCollection alterations = new AlterationCollection();
            alterations.GetMulti(filter);

            Dictionary<int, AlterationEntity> importedAlterations = new Dictionary<int, AlterationEntity>();
            CategoryBatch.AppendLog("Retrieving imported alterations..." + alterations.Count);
            foreach (AlterationEntity alteration in alterations)
            {
                importedAlterations.Add(alteration.AlterationId, alteration);
            }

            return importedAlterations;
        }

        private void ProcessPreview()
        {
            foreach (BatchItem item in this.root)
            {
                ProcessPreviewItem(item, null);
            }
        }

        private void ProcessPreviewItem(BatchItem item, TreeListNode parentNode)
        {
            if (item.GetType() == CategoryBatch.TYPE_CATEGORY)
            {
                TreeListNode node = CreateNode(parentNode);
                node["Name"] = item.Name;
                node["SortOrder"] = item.SortOrder;
                node["TypeName"] = "Category";

                BatchCategory category = (BatchCategory)item;
                if (category.HasProducts())
                {
                    foreach (BatchProduct product in category.Products)
                    {
                        ProcessPreviewItem(product, node);
                    }
                }
                if (category.HasChildren())
                {
                    foreach (BatchCategory child in category.Children)
                    {
                        ProcessPreviewItem(child, node);
                    }
                }
            }
            else if (item.GetType() == CategoryBatch.TYPE_PRODUCT)
            {
                TreeListNode node = CreateNode(parentNode);
                node["Name"] = item.Name;
                node["SortOrder"] = item.SortOrder;
                node["TypeName"] = "Product";
            }
        }

        private bool ValidateLines(string[] lines, ref int index, int level, BatchCategory parent, int sortOrder, int companyId)
        {
            for (; index < lines.Length;)
            {
                string line = lines[index];

                // Clean up
                line = line.Replace("\r", "");

                // Skip if empty
                if (line.Length <= 0)
                {
                    index++;
                    continue;
                }

                // Extract data
                int nameIndex = this.GetNameIndex(line);// Regex.Match(line, @"[a-z,A-Z,0-9, ,()]").Index;
                string prefix = line.Substring(0, nameIndex);
                int newlevel = prefix.Count(c => c == '-');
                string name = string.Empty;
                JObject jsonObj = null;
                int tmpIncrease = 0;

                int jsonParamsIndex = line.IndexOf("{");
                if (jsonParamsIndex <= 0)
                {
                    name = line.Substring(nameIndex).Trim().Replace("\t", " ");
                }
                else
                {
                    name = line.Substring(nameIndex, (jsonParamsIndex - nameIndex)).Trim().Replace("\t", " ");

                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine(line.Substring(jsonParamsIndex));
                    if (line.IndexOf("}") <= 0)
                    {
                        int counter = index;
                        index++;
                        builder.Append(this.BuildJsonObject(lines, ref index));
                        tmpIncrease = index - counter;
                    }

                    try
                    {
                        jsonObj = JsonConvert.DeserializeObject<JObject>(builder.ToString());
                    }
                    catch (Exception e)
                    {
                        AppendLog("Deserialize json object failed... {0}", e.Message);
                    }
                }

                // Check if this is a valid prefix
                foreach (char c in prefix)
                {
                    if (c != '#' && c != '-')
                    {
                        this.MultiValidatorDefault.AddError(string.Format("Prefix for '{0}' contains illegal characters. Only dashes (-) and hashtags (#) are allowed", name));
                        return false;
                    }
                }

                // Go back a level
                if (newlevel < level)
                {
                    index = index - tmpIncrease;
                    return true;
                }

                if (newlevel > level)
                {
                    this.MultiValidatorDefault.AddError(string.Format("'{0}' could not be assigned to a parent. Too many dashes (-) or parent is not a category (prefixed with hashtag (#))", name));
                    return false;
                }

                if (prefix.Contains("#"))
                {
                    AppendLog("Validating category... '{0}'.", name);

                    if (parent != null && parent.HasProducts())
                    {
                        this.MultiValidatorDefault.AddError(string.Format("Sub category '{0}' can not be added to category '{1}' because it contains products", name, parent.Name));
                        return false;
                    }

                    BatchCategory category = this.CreateBatchCategory(name, sortOrder, companyId, jsonObj);
                    sortOrder += 100;
                    index++;

                    if (parent != null)
                    {
                        parent.Children.Add(category);
                    }
                    else if (this.rootHasProducts)
                    {
                        this.MultiValidatorDefault.AddError(string.Format("Category '{0}' can not be added to the root because it contains products", name));
                        return false;
                    }
                    else
                    {
                        this.root.Add(category);
                        this.rootHasCategories = true;
                    }

                    if (!ValidateLines(lines, ref index, level + 1, category, 100, companyId))
                    {
                        return false;
                    }
                }
                else
                {
                    AppendLog("Validating product... '{0}'.", name);

                    if (parent != null && parent.HasChildren())
                    {
                        this.MultiValidatorDefault.AddError(string.Format("Product '{0}' can not be added to category '{1}' because it contains sub categories", name, parent.Name));
                        return false;
                    }

                    var product = this.CreateBatchProduct(name, sortOrder, companyId, jsonObj);
                    sortOrder += 100;
                    index++;

                    if (parent != null)
                    {
                        parent.Products.Add(product);
                    }
                    else if (this.rootHasCategories)
                    {
                        this.MultiValidatorDefault.AddError(string.Format("Product '{0}' can not be added to the root because it contains categories", name));
                        return false;
                    }
                    else
                    {
                        this.root.Add(product);
                        this.rootHasProducts = true;
                    }
                }
            }
            return true;
        }

        private int GetNameIndex(string line)
        {
            int index = 0;
            foreach (char c in line)
            {
                if (c != '#' && c != '-')
                    break;

                index++;
            }
            return index;
        }

        private BatchCategory CreateBatchCategory(string name, int sortOrder, int companyId, JObject jsonObj)
        {
            BatchCategory category = new BatchCategory();
            category.Name = name;
            category.SortOrder = sortOrder;
            category.CompanyId = companyId;

            if (jsonObj != null)
            {
                try
                {
                    JValue descriptionObj = (JValue)jsonObj["Description"];
                    JValue categoryTypeObj = (JValue)jsonObj["CategoryType"];
                    JArray mediaIdArray = (JArray)jsonObj["MediaIds"];

                    category.Description = descriptionObj.ToString();

                    int categoryType;
                    if (int.TryParse(categoryTypeObj.ToString(), out categoryType))
                        category.Type = categoryType;

                    // Media ids
                    for (int i = 0; i < mediaIdArray.Count; i++)
                    {
                        JValue mediaIdObj = (JValue)mediaIdArray[i];

                        int mediaId;
                        if (int.TryParse(mediaIdObj.ToString(), out mediaId))
                        {
                            MediaEntity oldMediaEntity = new MediaEntity(mediaId);
                            if (oldMediaEntity.IsNew)
                            {
                                CategoryBatch.AppendLog("Creating batch category. MediaEntity with mediaId '{0}' doesn't exist anymore", mediaId);
                            }
                            else
                            {
                                CopyMediaRequest request = new CopyMediaRequest
                                {
                                    MediaToCopy = oldMediaEntity,
                                    CopyMediaOnCdn = MediaCloudHelper.CopyMediaOnCdn,
                                    Transaction = transaction,
                                    SkipSave = true
                                };
                                MediaEntity newMedia = new CopyMediaUseCase().Execute(request);
                                category.MediaCollection.Add(newMedia);
                                this.oldMediaEntities.Add(newMedia, oldMediaEntity);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    CategoryBatch.AppendLog("Reading data of the json object for category '{0}' failed with exception: '{1}'", name, e.Message);
                }
            }

            return category;
        }

        private BatchProduct CreateBatchProduct(string name, int sortOrder, int companyId, JObject jsonObj)
        {
            BatchProduct product = new BatchProduct();
            product.Name = name;
            product.SortOrder = sortOrder;
            product.CompanyId = companyId;

            if (jsonObj != null)
            {
                try
                {
                    JValue descriptionObj = (JValue)jsonObj["Description"];
                    JValue WebTypeSmartphoneUrlObj = (JValue)jsonObj["WebTypeSmartphoneUrl"];
                    JValue WebTypeTabletUrlObj = (JValue)jsonObj["WebTypeTabletUrl"];
                    JValue priceObj = (JValue)jsonObj["Price"];
                    JValue productTypeObj = (JValue)jsonObj["ProductType"];
                    JValue productSubTypeObj = (JValue)jsonObj["ProductSubType"];
                    JValue genericProductIdObj = (JValue)jsonObj["GenericproductId"];
                    JArray mediaIdArray = (JArray)jsonObj["MediaIds"];
                    JArray productAlterationIdArray = (JArray)jsonObj["ProductAlterationIds"];

                    decimal price;
                    int productType;
                    int subType;
                    int genericproductId;

                    product.Description = descriptionObj.ToString();
                    product.WebTypeSmartphoneUrl = WebTypeSmartphoneUrlObj.ToString();
                    product.WebTypeTabletUrl = WebTypeTabletUrlObj.ToString();
                    if (decimal.TryParse(priceObj.ToString(), out price))
                        product.Price = price;
                    if (int.TryParse(productTypeObj.ToString(), out productType))
                        product.Type = productType;
                    if (int.TryParse(productSubTypeObj.ToString(), out subType))
                        product.SubType = subType;
                    if (int.TryParse(genericProductIdObj.ToString(), out genericproductId))
                        product.GenericproductId = genericproductId;

                    // Media ids
                    for (int i = 0; i < mediaIdArray.Count; i++)
                    {
                        JValue mediaIdObj = (JValue)mediaIdArray[i];

                        int mediaId;
                        if (int.TryParse(mediaIdObj.ToString(), out mediaId))
                        {
                            MediaEntity oldMediaEntity = new MediaEntity(mediaId);
                            if (oldMediaEntity.IsNew)
                            {
                                CategoryBatch.AppendLog("Creating batch category. MediaEntity with mediaId '{0}' doesn't exist anymore", mediaId);
                            }
                            else
                            {
                                CopyMediaRequest request = new CopyMediaRequest
                                {
                                    MediaToCopy = oldMediaEntity,
                                    CopyMediaOnCdn = MediaCloudHelper.CopyMediaOnCdn,
                                    Transaction = transaction,
                                    SkipSave = true
                                };
                                MediaEntity newMedia = new CopyMediaUseCase().Execute(request);
                                product.MediaCollection.Add(newMedia);
                                this.oldMediaEntities.Add(newMedia, oldMediaEntity);
                            }
                        }
                    }

                    // Alteration ids
                    for (int i = 0; i < productAlterationIdArray.Count; i++)
                    {
                        JValue productAlterationIdObj = (JValue)productAlterationIdArray[i];

                        int productAlterationId;
                        if (int.TryParse(productAlterationIdObj.ToString(), out productAlterationId))
                        {
                            var oldProductAlterationEntity = new ProductAlterationEntity(productAlterationId);
                            if (oldProductAlterationEntity != null)
                            {
                                if (!this.importedAlterations.ContainsKey(oldProductAlterationEntity.AlterationId))
                                {
                                    // Alteration doesnt exist for this company
                                    var newAlterationEntity = AlterationHelper.CopyAlteration(oldProductAlterationEntity.AlterationEntity, companyId, this.transaction);
                                    if (newAlterationEntity != null)
                                    {
                                        this.importedAlterations.Add(oldProductAlterationEntity.AlterationId, newAlterationEntity);
                                    }
                                }

                                if (this.importedAlterations.ContainsKey(oldProductAlterationEntity.AlterationId))
                                {
                                    ProductAlterationEntity newProductAlterationEntity = ProductAlterationHelper.CopyProductAlteration(oldProductAlterationEntity, this.transaction);
                                    if (newProductAlterationEntity != null)
                                    {
                                        newProductAlterationEntity.AlterationId = oldProductAlterationEntity.AlterationId;
                                        product.ProductAlterationCollection.Add(newProductAlterationEntity);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    AppendLog("Creating a batch product failed for product: '{0}'. Exception: {1}", name, e.Message);
                }
            }

            return product;
        }

        private string BuildJsonObject(string[] lines, ref int index)
        {
            StringBuilder builder = new StringBuilder();
            for (; index < lines.Length;)
            {
                string line = lines[index];
                builder.Append(line);

                if (line.IndexOf("}") <= 0)
                {
                    index++;
                }
                else
                {
                    break;
                }
            }
            return builder.ToString();
        }

        private bool Create(BatchItem item, CategoryEntity parentCategoryEntity, string defaultCultureCode)
        {
            if (item.GetType() == CategoryBatch.TYPE_CATEGORY)
            {
                CategoryBatch.AppendLog("Creating category... '{0}'", item.Name);

                BatchCategory batchCategory = (BatchCategory)item;

                CategoryEntity category = new CategoryEntity();
                if (this.transaction != null) category.AddToTransaction(this.transaction);
                category.Name = item.Name;
                category.Description = batchCategory.Description;
                category.SortOrder = item.SortOrder;
                category.CompanyId = item.CompanyId;
                category.MenuId = this.ddlMenuId.ValidId;
                category.Type = batchCategory.Type;
                category.Save();

                foreach (MediaEntity media in item.MediaCollection)
                {
                    MediaEntity oldMedia = this.oldMediaEntities[media];
                    if (this.transaction != null) media.AddToTransaction(this.transaction);
                    media.CategoryId = category.CategoryId;
                    if (media.Save(true) && oldMedia != null)
                    {
                        MediaCloudHelper.CopyMediaOnCdn(oldMedia, media);
                    }
                }

                if (parentCategoryEntity != null)
                {
                    category.ParentCategoryEntity = parentCategoryEntity;
                }

                if (!category.Save(true))
                    return false;

                Obymobi.Logic.HelperClasses.CustomTextHelper.UpdateCustomTexts(category, defaultCultureCode, this.transaction);

                if (batchCategory.HasProducts())
                {
                    foreach (BatchProduct product in batchCategory.Products)
                    {
                        Create(product, category, defaultCultureCode);
                    }
                }
                if (batchCategory.HasChildren())
                {
                    foreach (BatchCategory child in batchCategory.Children)
                    {
                        Create(child, category, defaultCultureCode);
                    }
                }
            }
            else if (item.GetType() == CategoryBatch.TYPE_PRODUCT)
            {
                CategoryBatch.AppendLog("Creating product... '{0}'", item.Name);

                BatchProduct batchProduct = (BatchProduct)item;

                ProductEntity product = new ProductEntity();
                if (this.transaction != null) product.AddToTransaction(this.transaction);
                product.Name = item.Name;
                product.SortOrder = item.SortOrder;
                product.CompanyId = item.CompanyId;
                product.PriceIn = batchProduct.Price;
                product.Description = batchProduct.Description;
                product.Type = batchProduct.Type;
                product.SubType = batchProduct.SubType;
                product.GenericproductId = batchProduct.GenericproductId;
                product.WebTypeSmartphoneUrl = batchProduct.WebTypeSmartphoneUrl;
                product.WebTypeTabletUrl = batchProduct.WebTypeTabletUrl;
                product.VattariffId = 3;
                product.Save();

                foreach (MediaEntity media in batchProduct.MediaCollection)
                {
                    MediaEntity oldMedia = this.oldMediaEntities[media];
                    if (this.transaction != null) media.AddToTransaction(this.transaction);
                    media.ProductId = product.ProductId;
                    bool saved = media.Save(true);
                    if (media.Save(true) && oldMedia != null)
                    {
                        MediaCloudHelper.CopyMediaOnCdn(oldMedia, media);
                    }
                }

                if (!product.Save(true))
                    return false;

                Obymobi.Logic.HelperClasses.CustomTextHelper.UpdateCustomTexts(product, defaultCultureCode, this.transaction);

                if (parentCategoryEntity != null)
                {
                    ProductCategoryEntity productCategory = new ProductCategoryEntity();
                    if (this.transaction != null)
                        productCategory.AddToTransaction(this.transaction);
                    productCategory.ProductId = product.ProductId;
                    productCategory.CategoryId = parentCategoryEntity.CategoryId;
                    if (!productCategory.Save())
                        return false;
                }

                foreach (ProductAlterationEntity productAlterationEntity in batchProduct.ProductAlterationCollection)
                {
                    if (this.importedAlterations.ContainsKey(productAlterationEntity.AlterationId))
                    {
                        ProductAlterationEntity productAlteration = new ProductAlterationEntity();
                        if (this.transaction != null)
                            productAlteration.AddToTransaction(this.transaction);
                        LLBLGenEntityUtil.CopyFields(productAlterationEntity, productAlteration); // source entity is already cleaned up, no need to skip fields
                        productAlteration.ProductId = product.ProductId;
                        productAlteration.AlterationId = this.importedAlterations[productAlterationEntity.AlterationId].AlterationId;
                        if (!productAlteration.Save())
                            return false;
                    }
                }
            }
            return true;
        }

        private bool Create()
        {
            bool success = true;
            CategoryEntity parent = null;
            if (this.ddlCategoryId.ValidId > 0)
            {
                parent = new CategoryEntity(this.ddlCategoryId.ValidId);
            }

            // Create new alterations
            foreach (AlterationEntity alteration in this.importedAlterations.Values)
            {
                if (alteration.IsNew && this.transaction != null)
                {
                    alteration.AddToTransaction(this.transaction);
                    alteration.Save(true);
                }
            }

            string defaultCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            foreach (BatchItem item in this.root)
            {
                if (!Create(item, parent, defaultCultureCode))
                {
                    success = false;
                    break;
                }
            }
            if (success)
            {
                return true;
            }
            return false;
        }


        private TreeListNode CreateNode(TreeListNode parentNode)
        {
            TreeListNode node = this.tlCategories.AppendNode(nodeIndex, parentNode);
            node.Expanded = true;
            nodeIndex++;
            return node;
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.SetFilters();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
        }

        void btCreate_Click(object sender, EventArgs e)
        {
            this.tabsMain.ActiveTabIndex = 1;
            StartSession();

            new Thread((param) =>
            {
                HttpContext.Current = (HttpContext)param;
                int userId = CmsSessionHelper.CurrentUser.UserId;

                if (Thread.GetNamedDataSlot("Role") == null)
                    Thread.AllocateNamedDataSlot("Role");

                Thread.SetData(Thread.GetNamedDataSlot("Role"), CmsSessionHelper.CurrentRole);

                int companyId = CmsSessionHelper.CurrentCompanyId;

                this.transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "CreateMenu");
                bool success = false;

                try
                {
                    AppendLog("Import of menu started...");

                    this.oldMediaEntities = new Dictionary<MediaEntity, MediaEntity>();

                    if (Validate(companyId, false))
                    {
                        success = this.Create();
                    }

                    if (success)
                    {
                        transaction.Commit();
                        AppendLog("Import finished successfully.");
                        //int menuId = this.ddlMenuId.ValidId;
                        //this.Response.Redirect("~/Catalog/Menu.aspx?id=" + menuId, false);
                    }
                    else
                    {
                        transaction.Rollback();
                        AppendLog("Import finished unsuccessfully.");
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    AppendLog("Something went wrong while processing the batch. Message: {0}.", ex.Message);
                }
                finally
                {
                    transaction.Dispose();
                    EndSessionOnGetLog();
                }
            }).Start(HttpContext.Current);

            string message = "Importing of the menu started and can take a couple of minutes to complete!";
            ((Dionysos.Web.UI.PageDefault)this.Page).AddInformator(Dionysos.Web.UI.InformatorType.Information, message);
        }

        void btPreview_Click(object sender, EventArgs e)
        {
            // Reset the transaction
            this.transaction = null;

            // Clear the current log
            ClearLog();

            // Validate data
            Validate(CmsSessionHelper.CurrentCompanyId, true);

            // Fill the preview tree
            ProcessPreview();
        }

        void tlCategories_HtmlRowPrepared(object sender, TreeListHtmlRowEventArgs e)
        {
            if (e.GetValue("TypeName") != null)
            {
                string type = e.GetValue("TypeName").ToString();
                if (type == "Category")
                {
                    e.Row.Font.Bold = true;
                }
            }
        }

        #endregion

        #region Logging

        private static string existsKey = "CategoryBatchExists" + CmsSessionHelper.CurrentUser.UserId;
        private static string logKey = "CategoryBatchLog" + CmsSessionHelper.CurrentUser.UserId;
        private static bool endSessionOnGetLog = false;

        [WebMethod(EnableSession = true), ScriptMethod]
        public static string GetLog()
        {
            string log = string.Empty;
            if (LogExists())
            {
                log = (string)HttpContext.Current.Application.Get(logKey);

                if (endSessionOnGetLog)
                {
                    endSessionOnGetLog = false;
                    EndSession();
                }
            }
            return log;
        }

        [WebMethod(EnableSession = true), ScriptMethod]
        public static bool SessionExists()
        {
            return (HttpContext.Current.Application.Get(existsKey) != null);
        }

        [WebMethod(EnableSession = true), ScriptMethod]
        public static bool LogExists()
        {
            return (HttpContext.Current.Application.Get(logKey) != null);
        }

        public static void AppendLog(string log, params object[] args)
        {
            if (SessionExists())
                HttpContext.Current.Application[logKey] += string.Format(log + "\n", args);
        }

        public static void StartSession()
        {
            HttpContext.Current.Application[logKey] = string.Empty;
            HttpContext.Current.Application[existsKey] = true;
        }

        public static void ClearLog()
        {
            if (LogExists())
                HttpContext.Current.Application[logKey] = string.Empty;
        }

        public static void EndSessionOnGetLog()
        {
            endSessionOnGetLog = true;
        }

        public static void EndSession()
        {
            if (SessionExists())
                HttpContext.Current.Application.Remove(existsKey);
        }

        #endregion

        #region classes

        abstract class BatchItem
        {
            public string Name;
            public int SortOrder;
            public int CompanyId;
            public MediaCollection MediaCollection = new MediaCollection();

            abstract public int GetType();
        }

        class BatchCategory : BatchItem
        {
            public string Description = string.Empty;
            public int Type = (int)CategoryType.Inherit;
            public List<BatchProduct> Products = new List<BatchProduct>();
            public List<BatchCategory> Children = new List<BatchCategory>();

            public override int GetType()
            {
                return CategoryBatch.TYPE_CATEGORY;
            }

            public bool HasProducts()
            {
                return this.Products.Count > 0;
            }

            public bool HasChildren()
            {
                return this.Children.Count > 0;
            }
        }

        class BatchProduct : BatchItem
        {
            public string Description = string.Empty;
            public decimal Price = decimal.Zero;
            public int Type = (int)ProductType.Product;
            public int SubType = (int)ProductSubType.Inherit;
            public int? GenericproductId = null;
            public string WebTypeSmartphoneUrl = string.Empty;
            public string WebTypeTabletUrl = string.Empty;
            public ProductAlterationCollection ProductAlterationCollection = new ProductAlterationCollection();

            public override int GetType()
            {
                return CategoryBatch.TYPE_PRODUCT;
            }
        }

        #endregion                
    }
}
