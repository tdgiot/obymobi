﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.Alterationoption" CodeBehind="Alterationoption.aspx.cs" %>

<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
	<D:Button runat="server" ID="btUnlinkProduct" Text="Unlink product" OnClick="UnlinkProduct" LocalizeText="False" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
	<div>
		<X:PageControl ID="tabsMain" runat="server" Width="100%">
			<TabPages>
				<X:TabPage Text="Algemeen" Name="Generic">
					<controls>
                    <D:PlaceHolder runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true" UseDataBinding="false"></D:TextBoxString>
							</td>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxInt ID="cbType" runat="server" IsRequired="true" UseDataBinding="true"></X:ComboBoxInt> 
							</td>      
						</tr>			
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFriendlyName">Zichtbare Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbFriendlyName" runat="server" IsRequired="false"></D:TextBoxString>
							</td>
                            <td class="label">
                                <D:Label runat="server" id="lblExternalIdentifier">PLU / SKU</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbExternalIdentifier" runat="server"></D:TextBoxString>
                            </td>
                        </tr>
						<tr>
                            <td class="label">
                                <D:Label runat="server" id="lblPriceIn" LocalizeText="False">Prijs toevoeging</D:Label>
                            </td>
                            <td class="control">
                                <D:TextBoxDecimal ID="tbPriceIn" runat="server" UseDataBinding="false"></D:TextBoxDecimal>
                            </td>
                            <td class="label">
								<D:Label runat="server" id="lblTaxTariffId">Tax Tariff</D:Label>
							</td>
							<td class="control">                                    
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlTaxTariffId" EntityName="TaxTariff" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="TaxTariffId" PreventEntityCollectionInitialization="true" EmptyItemText="Inherit from Product" LocalizeEmptyItemText="false" UseDataBinding="false"></X:ComboBoxLLBLGenEntityCollection>
							</td>
						</tr>
						<tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblIsAvailable">Available</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" id="cbIsAvailable" />
                            </td>
		                    <CC:FeatureTogglePanel ToggleType="Alterations_Cms_EnableProductInherit" runat="server">
		                    <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblProductId">Link to Product</D:LabelEntityFieldInfo>
		                    </td>
		                    <td class="control">                                    
			                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlProductId" EntityName="Product" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="ProductId" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
		                    </td>
		                    </CC:FeatureTogglePanel>
	                    </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblIsAlcoholic">Is Alcohol</D:Label>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbIsAlcoholic" UseDataBinding="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblDescription">Description</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control" colspan="3">
                                <D:TextBoxMultiLine ID="tbDescription" runat="server" Rows="10" UseDataBinding="true" ClientIDMode="Static"></D:TextBoxMultiLine>
                            </td>
                        </tr>
                        <CC:FeatureTogglePanel ToggleType="Tagging_Cms_EnableTagEditing" runat="server">
							<tr>
								<td class="label">
									<D:Label ID="lblTags" runat="server">Tags</D:Label>
								</td>
								<td class="control">
									<CC:TagBox ID="tagBox" ClientInstanceName="TagBoxControl" IsRequired="false" runat="server"></CC:TagBox>
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>
	                        <D:PlaceHolder ID="plhInherittedTags" Visible="false" runat="server">
		                        <tr>
			                        <td class="label">Inherited tags</td>
			                        <td class="control" colspan="3">
				                        <CC:TagOverview ID="TagOverview" runat="server" />
			                        </td>
		                        </tr>
	                        </D:PlaceHolder>
							<tr>
								<td class="label"></td>
								<td class="control token-overview-control">
									<CC:TagLegend ID="TagLegend" runat="server" />
								</td>
								<td class="label"></td>
								<td class="control"></td>
							</tr>
						</CC:FeatureTogglePanel>
                        
					 </table>		
					</D:PlaceHolder>
				</controls>
				</X:TabPage>
				<X:TabPage Text="Technisch" Name="tabAdvanced">
					<controls>
                        <D:PlaceHolder ID="PlaceHolder1" runat="server">
					    <table class="dataformV2">
				            <D:PlaceHolder ID="plhAdvancedPos" runat="server">
                            <tr>
                                <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPosalterationoptionId">Kassa optie</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlPosalterationoptionId" />
							    </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                    <D:HyperLink runat="server" LocalizeText="false" ID="hlPosalterationoptionId"  Target="_blank"></D:HyperLink>
                                </td>
                            </tr>
                            
                            <tr>
							    <td class="label">
								    <D:LabelEntityFieldInfo runat="server" id="lblPosproductId">Kassa product</D:LabelEntityFieldInfo>
							    </td>                            
							    <td class="control">
                                    <X:ComboBoxInt runat="server" ID="ddlPosproductId"/>
							    </td>
                                <td class="label">
                                </td>
                                <td class="control">
                                    <D:HyperLink runat="server" LocalizeText="false" ID="hlPosproductId"  Target="_blank"></D:HyperLink>
                                </td>
						    </tr>
                            </D:PlaceHolder>
                            <D:PlaceHolder ID="plhExternalProduct" Visible="false" runat="server">
                                <tr>
                                    <td class="label">
                                        <D:LabelEntityFieldInfo runat="server" id="lblExternalProductId" LocalizeText="False">External product</D:LabelEntityFieldInfo>
                                    </td>
                                    <td class="control">
                                        <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlExternalProductId" EntityName="ExternalProduct" IncrementalFilteringMode="StartsWith" PreventEntityCollectionInitialization="true"></X:ComboBoxLLBLGenEntityCollection>
                                    </td>
                                    <td class="label"></td>
                                    <td class="control"></td>
                                </tr>
                            </D:PlaceHolder>
                            <tr>
							    <td class="label">
							        <D:LabelEntityFieldInfo runat="server" id="lblIsProductRelated">Is gerelateerd aan product</D:LabelEntityFieldInfo>
							    </td>
							    <td class="control">
							        <D:CheckBox runat="server" ID="cbIsProductRelated" />
							    </td>
                                <td></td>                                
                                <td></td>
                            </tr>
                        </table>
                            </D:PlaceHolder>
                    </controls>
				</X:TabPage>
			</TabPages>
		</X:PageControl>
	</div>
</asp:Content>

