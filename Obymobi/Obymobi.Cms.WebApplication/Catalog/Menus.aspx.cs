﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Menus : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Fields

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Menu";
            this.EntityPageUrl = "~/Catalog/Menu.aspx";
            base.OnInit(e);
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                datasource.FilterToUse = filter;

                PrefetchPath prefetch = new PrefetchPath(EntityType.MenuEntity);
                prefetch.Add(MenuEntity.PrefetchPathDeliverypointgroupCollection);

                datasource.PrefetchPathToUse = prefetch;
            }

            if (CmsSessionHelper.CurrentRole != Role.GodMode)
            {
                this.MainGridView.ShowDeleteHyperlinkColumn = false;
                this.MainGridView.ShowDeleteColumn = false;
            }
        }

        #endregion

        #region Properties
        
        public MenuCollection DataSourceAsMenuCollection
        {
            get
            {
                return this.DataSource as MenuCollection;
            }
        }

        #endregion

    }
}
