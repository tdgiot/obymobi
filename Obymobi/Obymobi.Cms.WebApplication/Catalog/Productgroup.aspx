﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.Productgroup" Codebehind="Productgroup.aspx.cs" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>
<%@ Reference Control="~/Generic/SubPanels/OptionPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>                            
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>
						</tr>			                        
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblColumnTitle">Column title</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbColumnTitle" runat="server"/>
                            </td>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblColumnSubtitle">Column subtitle</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxString ID="tbColumnSubtitle" runat="server"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblOptions">Items</D:Label>
                            </td>
                            <td class="control" colspan="3">
                                <div style="width: 736px;">
                                    <D:PlaceHolder runat="server" ID="plhOptionsPanel"></D:PlaceHolder>
                                </div>
                            </td>
                        </tr>
					 </table>			
				</Controls>
			</X:TabPage>	
            <X:TabPage Text="Products" Name="tabForProducts">
                <controls>
                    <D:PlaceHolder runat="server">
                        <D:LabelTextOnly runat="server" ID="lblProductsUsingProductgroup">The following products are using this productgroup:</D:LabelTextOnly>
                        &nbsp;
                        <table>
                            <D:PlaceHolder runat="server" ID="plhProducts"></D:PlaceHolder>
                        </table>
                    </D:PlaceHolder>
                </controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>