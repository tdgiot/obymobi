﻿<%@ Page Title="POS product import" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.PosProductImport" Codebehind="PosProductImport.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" runat="Server">
    <span class="toolbar">
        <X:ToolBarButton runat="server" ID="btImport" CommandName="Import" Text="Importeren"
            Image-Url="~/Images/Icons/table_add.png" />
        <X:ToolBarButton runat="server" ID="btChangeSettings" CommandName="RefreshSettings"
            Text="Verversen" Image-Url="~/Images/Icons/table_refresh.png" />
        <X:ToolBarButton runat="server" ID="btRefreshSyncedProducts" CommandName="RefreshSyncedProducts"
            Text="Geïmporteerde producten updaten" Image-Url="~/Images/Icons/table_refresh.png" />
        <X:ToolBarButton runat="server" ID="btImportAllProducts" CommandName="ImportAllProducts"
            Text="Alle producten importeren"  />
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" runat="Server">
    <X:PageControl ID="tabsMain" runat="server" Width="100%">
        <TabPages>
            <X:TabPage Text="Producten koppelen" Name="ProductMatching">
                <controls>
					<D:PlaceHolder runat="server">
                        <table cellpadding="0" cellspacing="0" style="margin-bottom: 14px">
                        <tr>
                            <td>
                                <D:Label runat="server" ID="lblHideImportedProducts">Verberg reeds geïmporteerde producten</D:Label>
                            </td>
                            <td style="padding-left: 4px">
                                <D:CheckBox runat="server" ID="cbHideImportedProducts" />
                            </td>
                        </tr>
                        <D:PlaceHolder runat="server" ID="plhChangePageSize" Visible="false">                        
                        <tr>
                            <td style="padding-top:4px;">
                                <D:Label runat="server" ID="lblPagingSize">Producten per pagina</D:Label>
                            </td>
                            <td style="padding:4px 0 0 4px;">
                                <D:TextBoxInt runat="server" ID="tbPagingSize"  /> - This functionality should not be used unless the code behind is checked for errors.
                            </td>
                        </tr>
                        </D:PlaceHolder>
                        <tr>
                                <td style="padding-top:4px;">
                                    <D:LabelAssociated runat="server" ID="lblCategoryToImportTo">Importeren in categorie:</D:LabelAssociated>
                                </td>
                                <td style="padding:4px 0 0 4px;">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="cbCategoryId" IncrementalFilteringMode="StartsWith" EntityName="Category" TextField="Name" ValueField="CategoryId" />
                                </td>
                            </tr>
                        </table>
                        <D:PlaceHolder runat="server" ID="plhSelectedProducts"></D:PlaceHolder>
                       <table cellpadding="0" cellspacing="0" style="margin-bottom: 14px">
                            <tr>
                                <td>
                                    <X:Button ID="btnSelectAll" runat="server" AutoPostBack="false" UseSubmitBehavior="false"
                                        Text="Alles selecteren">
                                        <ClientSideEvents Click="function() { grid.SelectAllRowsOnPage() }" />
                                    </X:Button>
                                </td>
                                <td style="padding-left: 4px">
                                    <X:Button ID="btnSelectNone" runat="server" AutoPostBack="false" UseSubmitBehavior="false"
                                        Text="Niets selecteren">
                                        <ClientSideEvents Click="function() { grid.UnselectAllRowsOnPage() }" />
                                    </X:Button>
                                </td>
                                <td style="padding-left: 4px">
                                </td>
                            </tr>
                            </table>                      
                        <X:GridView ID="grid" ClientInstanceName="grid" runat="server" KeyFieldName="PosproductId" 
                            Width="100%">          
                            <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                            <SettingsPager PageSize="25"></SettingsPager>
                            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                            <Columns>
                                <dxwgv:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0">                                    
                                    <HeaderTemplate>
                                        <input type="checkbox" onclick="grid.SelectAllRowsOnPage(this.checked);" title="Selecteer/deselecteer alle rijen op de pagina" />
                                    </HeaderTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </dxwgv:GridViewCommandColumn>
                                <dxwgv:GridViewDataColumn FieldName="ExternalId" VisibleIndex="1">
                                <Settings AutoFilterCondition="Contains" />                                  
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="Name" VisibleIndex="2" >
                                <Settings AutoFilterCondition="Contains" />
                                <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="PriceIn" VisibleIndex="4" >
                                <Settings AutoFilterCondition="Contains" />
                                <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="PosCategoryName" VisibleIndex="3" >
                                <Settings AutoFilterCondition="Contains" />
                                </dxwgv:GridViewDataColumn>
                            </Columns>
                            <Settings ShowFilterRow="True" />
                        </X:GridView>

                     </D:PlaceHolder>
				</controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>
