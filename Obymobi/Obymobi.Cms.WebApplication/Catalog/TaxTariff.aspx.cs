﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class TaxTariff : Dionysos.Web.UI.PageLLBLGenEntity
    {
        public override bool Save()
        {
            if (PageMode == Dionysos.Web.PageMode.Add)
            {
                DataSourceAsTaxTariffEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
                DataSourceAsTaxTariffEntity.CreatedBy = CmsSessionHelper.CurrentUser.UserId;
                DataSourceAsTaxTariffEntity.UpdatedBy = 0;
            }
            else
            {
                DataSourceAsTaxTariffEntity.UpdatedBy = CmsSessionHelper.CurrentUser.UserId;
            }

            return base.Save();
        }

        public TaxTariffEntity DataSourceAsTaxTariffEntity => DataSource as TaxTariffEntity;
	}
}
