﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.Alteration" Codebehind="Alteration.aspx.cs" %>
<%@ Reference VirtualPath="~/Catalog/SubPanels/AlterationitemCollection.ascx" %>
<%@ Reference VirtualPath="~/Catalog/SubPanels/AlterationComponentsPanel.ascx" %>
<%@ Reference Control="~/Catalog/SubPanels/AlterationComponentsPanel.ascx" %>
<%@ Reference VirtualPath="~/Generic/UserControls/CustomTextCollection.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<controls>
                    <D:PlaceHolder runat="server" ID="plhGeneric">
                    <table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Name</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>	  
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblType">Type</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxInt ID="cbType" runat="server" IsRequired="true" UseDataBinding="true"></X:ComboBoxInt> 
							</td>      
						</tr>                        
                        <tr>
                            <td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblFriendlyName">Friendly name</D:LabelEntityFieldInfo>
							</td>
                            <td class="control">
								<D:TextBoxString ID="tbFriendlyName" runat="server"></D:TextBoxString>                                
							</td>                                                        
                        </tr>
                        <D:PlaceHolder ID="plhMinMaxOptions" runat="server">
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblMinOptions">Minimale opties</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxInt runat="server" ID="tbMinOptions" AllowZero="true" AllowNegative="false"></D:TextBoxInt>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblMaxOptions">Maximale opties</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxInt runat="server" ID="tbMaxOptions" AllowZero="true" AllowNegative="false"></D:TextBoxInt>
							</td>
						</tr>		
                        </D:PlaceHolder>
					    <D:PlaceHolder ID="plhTimeOptions" runat="server">
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblStartTime">Start time</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:TimeEdit runat="server" ID="teStartTime"></X:TimeEdit>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblEndTime">End time</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:TimeEdit runat="server" ID="teEndTime"></X:TimeEdit>
							</td>
						</tr>	
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblMinLeadMinutes">Minimum lead time</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:TimeEdit runat="server" ID="teMinLeadMinutes" UseDataBinding="false"></X:TimeEdit>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblMaxLeadHours">Maximum lead hours</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxInt runat="server" ID="teMaxLeadHours" UseDataBinding="false"></D:TextBoxInt>
							</td>
						</tr>	
                            <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblIntervalMinutes">Interval time</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:TimeEdit runat="server" ID="teIntervalMinutes" UseDataBinding="false"></X:TimeEdit>
							</td>
                            <td class="label">

                            </td>                                    
                            <td class="control">

                            </td>                                			    
						</tr>
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblShowDatePicker">Show date picker</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbShowDatePicker" />
							</td>
                            <td class="label">
							</td>
							<td class="control">
							</td>
						</tr>	                        
					    </D:PlaceHolder>
					    <D:PlaceHolder ID="plhMaxDays" runat="server">
                        <tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lvlMaxLeadDays">Maximum lead days</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <D:TextBoxInt runat="server" ID="teMaxLeadDays" UseDataBinding="false"></D:TextBoxInt>
							</td>							
					    </tr>	
                        </D:PlaceHolder>		 
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblOrderLevelEnabled">Order level enabled</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbOrderLevelEnabled" />
						    </td>				                
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblVisible">Visible</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:CheckBox runat="server" ID="cbVisible" />
						    </td>				                            
                        </tr>                        
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblDescription" />
                            </td>
                            <td class="control" colspan="2">
                                <D:TextBoxMultiLine ID="tbDescription" runat="server" Rows="15" UseDataBinding="true" ClientIDMode="Static"></D:TextBoxMultiLine>                             
                                <small runat="server" id="lblDescriptionNote" Visible="false"><a href="http://en.wikipedia.org/wiki/Markdown" target="_markdown">Markdown</a>-markup can be used. (bold: **bold**, italic: *italic*).</small>
                            </td>
                            <td class="control">
                                <D:PlaceHolder runat="server" ID="plhMarkdown"><div id="markdown-output" class="markdown_preview markdown-body" /></D:PlaceHolder>
                            </td>
                        </tr>
                        <D:PlaceHolder runat="server" ID="plhAlterationComponents">
                        <tr>    
                            <td class="label">
                                <D:Label runat="server" id="lblComponents">Components</D:Label>
                            </td>
                            <td class="control" colspan="3">
                                <div style="width: 736px;">
                                    <D:PlaceHolder runat="server" ID="plhAlterationComponentsPanel"></D:PlaceHolder>
                                </div>
                            </td>
                        </tr>
                        </D:PlaceHolder> 
                    </table> 
                    </D:PlaceHolder>
				</controls>
			</X:TabPage>														
            <X:TabPage Text="Advanced" Name="tabAdvanced">
                <controls>
				<D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
					    <D:PlaceHolder ID="plhPos" runat="server">
				        <tr>
                            <td class="label">POS Alteration</td>
							<td class="control">
                                <U:UltraBoxInt ID="ddlPosalterationId" runat="server" />
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlPosalterationId"  Target="_blank"></D:HyperLink>
							</td>
							<td class="control">
							</td>
						</tr>
                        </D:PlaceHolder>
				        <tr>
                            <td class="label">
								<D:LabelTextOnly id="lblActions" Text="Actions" LocalizeText="false" runat="server" />
							</td>
							<td class="control">
							    <D:Button ID="btRemoveAlterationFromProducts" LocalizeText="false" Text="Remove this option from all products" PreSubmitWarning="Are you sure you want to remove this alteration from all products?" runat="server" /><br />                                
							</td>
							<td class="label">
							</td>
							<td class="control">
							</td>
						</tr>
                    </table>
                </D:PlaceHolder>
                </controls>
            </X:TabPage>
            <X:TabPage Text="Products" Name="tabForProducts">
                <controls>
                    <D:PlaceHolder runat="server">
                        <D:LabelTextOnly runat="server" ID="lblProductsUsingThisAlteration">De volgende producten gebruiken deze optie:</D:LabelTextOnly>
                        &nbsp;
                        <table>
                            <D:PlaceHolder runat="server" ID="plhProducts"></D:PlaceHolder>
                        </table>
                    </D:PlaceHolder>
                </controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
            <script type="text/javascript">
                function parseMarkdown(obj) {
                    var output = document.getElementById("markdown-output");
                    if (output != null && obj != null) {
                        var text = obj.value;
                        output.innerHTML = markdown.toHTML(text);
                    }
                }

                parseMarkdown(document.getElementById("tbDescription"));
        </script>
</div>
</asp:Content>