﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.PriceSchedule" Codebehind="PriceSchedule.aspx.cs" %>
<%@ Register TagPrefix="uc" TagName="PriceScheduledItemsPanel" Src="~/Catalog/SubPanels/PriceScheduledItemsPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Schedule" Name="Schedules">
				<Controls>
                    <D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblName">Naam</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:TextBoxString ID="tbName" runat="server" IsRequired="true"></D:TextBoxString>
							</td>	                              
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <D:PlaceHolder runat="server" ID="plhSchedule" />
                            </td>
                        </tr>							                  
                    </table>
                    </D:PlaceHolder>   
                </Controls>                    
            </X:TabPage>            
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>