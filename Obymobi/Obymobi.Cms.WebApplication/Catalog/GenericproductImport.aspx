﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.GenericproductImport" Codebehind="GenericproductImport.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
	<D:Button runat="server" ID="btImport" Text="Inlezen" PreSubmitWarning="Weet u zeker dat u de geselecteerde producten wilt inlezen" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Producten inlezen" Name="ImportProducts">
				<Controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblSupplierId">Leverancier</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSupplierId" CssClass="input" UseDataBinding="true" EntityName="Supplier" AutoPostBack="true"></X:ComboBoxLLBLGenEntityCollection>            
							</td>
							<td class="label">
								<D:Label runat="server" id="lblGenericcategoryId">Van categorie</D:Label>
							</td>
							<td class="control">
							    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlGenericcategoryId" CssClass="input" UseDataBinding="true" EntityName="Genericcategory" AutoPostBack="true"></X:ComboBoxLLBLGenEntityCollection>            
							</td>        
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblCompany">Bedrijf</D:Label>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblCompanyValue" LocalizeText="false"></D:Label>
							</td>
							<td class="label">
								<D:Label runat="server" id="lblCategoryId">Naar categorie</D:Label>
							</td>
							<td class="control">
								<X:ComboBoxInt runat="server" ID="ddlCategoryId" IncrementalFilteringMode="StartsWith" TextField="FullCategoryName" ValueField="CategoryId" AutoPostBack="true" ></X:ComboBoxInt>
							</td>
						</tr>
						<tr>
							<td class="label">
								&nbsp;
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbImport" AutoPostBack="true" Visible="false" />
							</td>							
						</tr>
						<tr>
							<td class="label">
								<D:Label runat="server" id="lblProductsToImport">Producten selecteren</D:Label>
							</td>
							<td class="control">
								<D:CheckBoxList runat="server" ID="cblProductsToImport" RepeatDirection=Horizontal RepeatColumns=2></D:CheckBoxList>
							</td>							
						</tr>						
					 </table>			
				</Controls>
			</X:TabPage>			
		</TabPages>
	</X:PageControl>
</asp:Content>

