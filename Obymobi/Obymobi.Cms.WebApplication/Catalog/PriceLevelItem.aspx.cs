﻿using System;
using Obymobi.Data.EntityClasses;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class PriceLevelItem : Dionysos.Web.UI.PageLLBLGenEntity
    {

        #region Methods
        
        private void LoadUserControls()
        {

        }

        private void HookUpEvents()
        {
            
        }

        private void SetGui()
        {
            
        }

        #endregion

		#region Properties

		
		public PriceLevelItemEntity DataSourceAsPriceLevelItemEntity
		{
			get
			{
                return this.DataSource as PriceLevelItemEntity;
			}
		}

		#endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.PriceLevelItem_DataSourceLoaded;
            base.OnInit(e);
        }

        private void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();
        }

        private void PriceLevelItem_DataSourceLoaded(object sender)
		{
            this.SetGui();
		}

		#endregion
	}
}
