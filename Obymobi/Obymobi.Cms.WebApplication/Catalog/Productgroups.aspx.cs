﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using System.Drawing;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Productgroups : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Productgroup";
            this.EntityPageUrl = "~/Catalog/Productgroup.aspx";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ProductgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);                

                datasource.FilterToUse = filter;
            }
        }        

        #endregion
    }
}
