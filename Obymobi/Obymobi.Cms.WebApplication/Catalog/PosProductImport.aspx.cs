﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Logic.HelperClasses;
using Dionysos.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System.Data;
using Obymobi.Data.DaoClasses;
using Obymobi.Data;
using Obymobi.Integrations.POS;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class PosProductImport : Dionysos.Web.UI.PageQueryStringDataBinding
    {
        #region Fields

        private PoscategoryCollection posCategories;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.SetGui();
            base.OnInit(e);
            this.LoadPosproducts();
        }

        protected override void SetDefaultValuesToControls()
        {
            this.cbCategoryId.Value = null;
            this.tbPagingSize.Value = Dionysos.ConfigurationManager.GetInt(DionysosWebConfigurationConstants.PageSize);
            this.cbHideImportedProducts.Checked = true;
        }

        public void RefreshSyncedProducts()
        {
            PosDataSynchronizer syncer = new PosDataSynchronizer(CmsSessionHelper.CurrentCompanyId, CmsSessionHelper.AlterationDialogMode);
            syncer.SynchronizeExistingProducts(CmsSessionHelper.AlterationDialogMode);
        }

        public void Import()
        {
            if (this.cbCategoryId.SelectedIndex == 0)
            {
                this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, "Please select a category to import the products in.");
                this.Validate();
            }
            else
            {
                // Do Import
                List<object> selection = grid.GetSelectedFieldValues(grid.KeyFieldName);

                List<int> productIds = new List<int>();

                foreach (var selectedId in selection)
                {
                    productIds.Add(Convert.ToInt32(selectedId));
                }

                PosDataSynchronizer sync = new PosDataSynchronizer(CmsSessionHelper.CurrentCompanyId, CmsSessionHelper.AlterationDialogMode);

                if (this.cbCategoryId.Text.Equals("Service items"))
                {
                    sync.SynchronizeSpecialProducts(productIds, ProductType.Service, true);
                }
                else if (this.cbCategoryId.Text.Equals("Payment methods"))
                {
                    sync.SynchronizeSpecialProducts(productIds, ProductType.Paymentmethod, true);
                }
                else if (this.cbCategoryId.Text.Equals("POS Messages"))
                {
                    sync.SynchronizeSpecialProducts(productIds, ProductType.PosServiceMessage, true);
                }
                else
                {
                    sync.SynchronizeProducts(productIds, this.cbCategoryId.Value, false, true, true, false, CmsSessionHelper.AlterationDialogMode);
                }
                

                // Refresh page
                QueryStringHelper qs = new QueryStringHelper();
                qs.AddItem("imported", productIds.Count);
                qs.AddItem("importedto", this.cbCategoryId.SelectedItem.Text);
                this.Response.Redirect(qs.MergeQuerystringWithRawUrl(), true);
            }
       }

        public void ImportAllProducts()
        {
            if (this.cbCategoryId.SelectedIndex == 0)
            {
                this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, "Please select a category to import the products in.");
                this.Validate();
            }
            else
            {
                // Do Import
                var posproducts = PosHelperWrapper.RetrieveAllImportablePosproducts(new CompanyEntity(CmsSessionHelper.CurrentCompanyId), false, true);

                List<int> productIds = new List<int>();
                foreach (var posproduct in posproducts)
                {
                    productIds.Add(Convert.ToInt32(posproduct.PosproductId));
                }

                PosDataSynchronizer sync = new PosDataSynchronizer(CmsSessionHelper.CurrentCompanyId, CmsSessionHelper.AlterationDialogMode);

                if (this.cbCategoryId.Text.Equals("Service items"))
                {
                    sync.SynchronizeSpecialProducts(productIds, ProductType.Service, true);
                }
                else if (this.cbCategoryId.Text.Equals("Payment methods"))
                {
                    sync.SynchronizeSpecialProducts(productIds, ProductType.Paymentmethod, true);
                }
                else if (this.cbCategoryId.Text.Equals("POS Messages"))
                {
                    sync.SynchronizeSpecialProducts(productIds, ProductType.PosServiceMessage, true);
                }
                else
                {
                    sync.SynchronizeProducts(productIds, this.cbCategoryId.Value, false, true, true, false, CmsSessionHelper.AlterationDialogMode);
                }

                // Refresh page
                QueryStringHelper qs = new QueryStringHelper();
                qs.AddItem("imported", productIds.Count);
                qs.AddItem("importedto", this.cbCategoryId.SelectedItem.Text);
                this.Response.Redirect(qs.MergeQuerystringWithRawUrl(), true);
            }
        }

        public void RefreshSettings()
        {
            QueryStringHelper qs = this.GetQueryStringInstanceWithValues();
            qs.AddItem("imported", string.Empty);
            qs.AddItem("importedto", string.Empty);
            WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        void SetGui()
        {
            ResultsetFields fields = new ResultsetFields(2);
            fields.DefineField(CategoryFields.CategoryId, 0, "CategoryId");
            fields.DefineField(CategoryFields.Name, 1, "Name");

            fields[1].ExpressionToApply = new DbFunctionCall("{0} + ': ' + {1}", new object[] 
                                          { 
                                              MenuFields.Name, CategoryFields.Name 
                                          });

            RelationCollection relations = new RelationCollection();
            relations.Add(CategoryEntity.Relations.MenuEntityUsingMenuId, JoinHint.Inner);
            relations.Add(CategoryEntity.Relations.CategoryEntityUsingParentCategoryId, "child", JoinHint.Left);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(new FieldCompareValuePredicate(CategoryFields.CategoryId, ComparisonOperator.Equal, DBNull.Value, "child"));

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(fields[1], SortOperator.Ascending));

            DataTable categoryTable = new DataTable();
            TypedListDAO dao = new TypedListDAO();
            dao.GetMultiAsDataTable(fields, categoryTable, 0, sort, filter, relations, false, null, null, 0, 0);

            // - Add dummy categories
            List<CategoryEntity> dummyList = new List<CategoryEntity>();

            DataRow dummyServiceitem = categoryTable.NewRow();
            dummyServiceitem[0] = -1;
            dummyServiceitem[1] = "Service items";
            categoryTable.Rows.Add(dummyServiceitem);

            DataRow dummyPaymentmethod = categoryTable.NewRow();
            dummyPaymentmethod[0] = -2;
            dummyPaymentmethod[1] = "Payment methods";
            categoryTable.Rows.Add(dummyPaymentmethod);

            DataRow dummyPOSMessage = categoryTable.NewRow();
            dummyPOSMessage[0] = -3;
            dummyPOSMessage[1] = "POS Messages";
            categoryTable.Rows.Add(dummyPOSMessage);
            // -

            this.cbCategoryId.DataSource = categoryTable;
            this.cbCategoryId.DataBind();

            // Import All Products, only for developers (might later also include Administrator)
            this.btImportAllProducts.Visible = TestUtil.IsPcDeveloper;

            if (!this.IsPostBack)
            {
                int importedProducts;
                if (QueryStringHelper.TryGetValue("imported", out importedProducts))
                {
                    string importedTo; 
                    if(QueryStringHelper.TryGetValue("importedto", out importedTo))
                        this.AddInformatorInfo("{0} products have been imported to {1}.", importedProducts, importedTo);
                    else
                        this.AddInformatorInfo("{0} products have been imported.", importedProducts);
                    this.Validate();
                }
            }
        }

        void LoadPosproducts()
        {
            // GK Disabled because this would give strange behavoir when using others pages than the first
            // it would select wrong items or not select them at all.
            //this.grid.SettingsPager.PageSize = this.tbPagingSize.Value.Value;
            this.grid.SettingsBehavior.EnableRowHotTrack = true;

            this.posCategories = PoscategoryHelper.GetPoscategoryCollection(CmsSessionHelper.CurrentCompanyId);
           
            // Retrieve Pos Products
            var posproductCollection = PosHelperWrapper.RetrieveAllImportablePosproducts(new CompanyEntity(CmsSessionHelper.CurrentCompanyId), false, this.cbHideImportedProducts.Checked);

            // Set all PosCategoryNames                        
            foreach (var posproduct in posproductCollection)
            {
                PoscategoryEntity poscategory = this.posCategories.FirstOrDefault(pc => pc.ExternalId == posproduct.ExternalPoscategoryId);

                if(poscategory != null)
                    posproduct.PosCategoryName = poscategory.Name;
                else
                    posproduct.PosCategoryName = string.Empty;
            }

            // Translations
            if (this.grid.Columns["ExternalId"] != null)
                this.grid.Columns["ExternalId"].Caption = this.Translate("clmnExternalId", "Extern Id");

            if (this.grid.Columns["Name"] != null)
                this.grid.Columns["Name"].Caption = this.Translate("clmnName", "Naam");

            if (this.grid.Columns["PriceIn"] != null)
                this.grid.Columns["PriceIn"].Caption = this.Translate("clmnPriceIn", "Prijs");

            if (this.grid.Columns["PosCategoryName"] != null)
                this.grid.Columns["PosCategoryName"].Caption = this.Translate("clmnPosCategoryName", "POS categorie");
            
            this.grid.DataSource = posproductCollection;
            this.grid.DataBind();
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.grid.CustomColumnSort += MainGridView_CustomColumnSort;
        }

        void MainGridView_CustomColumnSort(object sender, DevExpress.Web.CustomColumnSortEventArgs e)
        {
            if (e.Column.FieldName == "ExternalId")
            {
                int numVal1 = 0, numVal2 = 0;
                try
                {
                    numVal1 = Convert.ToInt32(e.Value1);
                }
                catch { }

                try
                {
                    numVal2 = Convert.ToInt32(e.Value2);
                }
                catch { }

                if (numVal1 < numVal2)
                    e.Result = -1;
                else if (numVal1 > numVal2)
                    e.Result = 1;
                else
                    e.Result = 0;

                e.Handled = true;
            }
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            DevExpress.Web.GridViewDataColumn column = this.grid.Columns["ExternalId"] as DevExpress.Web.GridViewDataColumn;
            if (column != null)
            {
                column.Settings.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            }
        }

        #endregion
    }
}