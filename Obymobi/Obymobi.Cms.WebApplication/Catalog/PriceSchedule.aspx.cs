﻿using Obymobi.Data.EntityClasses;
using System;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class PriceSchedule : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields

        private PriceScheduledItemsPanel scheduledItemsPanel;

        #endregion

        #region Methods

        private void SetGui()
        {
            if (!this.DataSourceAsPriceScheduleEntity.IsNew)
            {
                this.scheduledItemsPanel = (PriceScheduledItemsPanel)this.LoadControl("~/Catalog/SubPanels/PriceScheduledItemsPanel.ascx");
                this.scheduledItemsPanel.ID = "ucPriceScheduleItemsPanel";
                this.scheduledItemsPanel.PriceScheduleId = this.DataSourceAsPriceScheduleEntity.PriceScheduleId;
                this.plhSchedule.Controls.Add(this.scheduledItemsPanel);
            }
        }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                this.DataSourceAsPriceScheduleEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }
            return base.Save();
        }

        #endregion

		#region Properties
		
		public PriceScheduleEntity DataSourceAsPriceScheduleEntity
		{
			get
			{
                return this.DataSource as PriceScheduleEntity;
			}
		}

		#endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.DataSourceLoaded += this.PriceSchedule_DataSourceLoaded;
            base.OnInit(e);
        }

        private void PriceSchedule_DataSourceLoaded(object sender)
		{
            this.SetGui();
		}

		#endregion
	}
}
