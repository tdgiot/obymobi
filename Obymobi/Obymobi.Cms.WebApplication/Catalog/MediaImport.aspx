﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.MediaImport" Codebehind="MediaImport.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
		<D:Button runat="server" ID="btMatch" Text="Koppelen" />	
		<D:Button runat="server" ID="btLoadPhotos" Text="Foto's inladen" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Foto's importeren" Name="PictureImport">
				<Controls>
					<D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
						<tr>
							<td class="control">
								<D:Label runat="server" id="lblProductCount"></D:Label>
							</td>
							<td class="control">
                                &nbsp;
							</td>
						</tr>
						<tr>
							<td class="control">
								<D:Label runat="server" id="lblFileName">Product</D:Label>
							</td>
							<td class="control">
                                <D:Label runat="server" id="lblProductName">Bestand</D:Label>
							</td>
						</tr>	
						<D:PlaceHolder runat="server" ID="plhProducts"></D:PlaceHolder>
					 </table>			
					 </D:PlaceHolder>
				</Controls>
			</X:TabPage>			
		</TabPages>
	</X:PageControl>
</asp:Content>

