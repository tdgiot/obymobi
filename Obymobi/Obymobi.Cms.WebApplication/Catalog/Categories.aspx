﻿<%@ Page Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.Categories" Title="Categorieen" Codebehind="Categories.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<%@ Register Assembly="Dionysos.Web" Namespace="Dionysos.Web.UI.WebControls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
    <cc1:Label runat="server" ID="lblTitleProducts">Categorieen</cc1:Label> -
<cc1:Label runat="server" ID="lblTitleOverview">Overzicht</cc1:Label>    
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cplhToolbarHolder">
	<X:ToolBarPageOverviewDataSourceCollection runat="server"  ID="ToolBarPageOverviewDataSourceCollection" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
    <X:TreeList ID="tlCategories" runat="server" AutoGenerateColumns="False" KeyFieldName="CategoryId" ParentFieldName="ParentCategoryId" Width="100%">            
        <settingsbehavior autoexpandallnodes="True" />
        <Settings GridLines="Horizontal" />
        <Columns>                    
            <dxwtl:TreeListHyperLinkColumn caption="Naam" fieldname="CategoryId" visibleindex="0" CellStyle-HorizontalAlign="Left">
                <PropertiesHyperLink TextField="Name" NavigateUrlFormatString="Category.aspx?id={0}" >
                </PropertiesHyperLink>           
            </dxwtl:TreeListHyperLinkColumn>            
            <dxwtl:TreeListDataColumn caption="Volgorde" fieldname="SortOrder" visibleindex="1" width="60px" CellStyle-HorizontalAlign="Left">
            </dxwtl:TreeListDataColumn>                        
        </Columns>        
    </X:TreeList>    
</asp:Content>

