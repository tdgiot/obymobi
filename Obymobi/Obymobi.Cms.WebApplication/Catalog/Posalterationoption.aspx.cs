﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Posalterationoption : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
                this.DataSourceAsPosalterationoptionEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;

            return base.Save();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the datasource as a PosalterationEntity instance
        /// </summary>
        public PosalterationoptionEntity DataSourceAsPosalterationoptionEntity
        {
            get
            {
                return this.DataSource as PosalterationoptionEntity;
            }
        }

        #endregion
    }
}
