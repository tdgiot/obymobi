﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class GenericProductMatching : Dionysos.Web.UI.PageQueryStringDataBinding
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.SetGui();
            base.OnInit(e);
            this.RenderProductsToMatch();
        }

        protected override void SetDefaultValuesToControls()
        {
            this.ddlCategoryId.Value = -1;
            this.cbImport.Checked = true;
        }

        void SetGui()
        {
            // Load Categories of Company
            this.ddlCategoryId.DataSource = CmsSessionHelper.GetCategoriesForCompanyFlat(true);
            this.ddlCategoryId.DataBind();

            // Insert custom items
            this.ddlCategoryId.Items.Insert(1, new DevExpress.Web.ListEditItem("Alle producten", -2));
            this.ddlCategoryId.Items.Insert(1, new DevExpress.Web.ListEditItem("Niet gekoppelde producten", -3));
        }

        void RenderProductsToMatch()
        {
            // Check what to render
            ProductCollection products = new ProductCollection();
            PredicateExpression productFilter = new PredicateExpression();
            productFilter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            productFilter.Add(ProductFields.VisibilityType != VisibilityType.Never);

            if (!this.ddlCategoryId.Value.HasValue)
            {
                // None
                productFilter.Add(ProductFields.ProductId == -1);
            }
            else if (this.ddlCategoryId.Value == -2)
            {
                // All
            }
            else if (this.ddlCategoryId.Value == -3)
            {
                // Non-matched
                productFilter.Add(ProductFields.ProductId == DBNull.Value);
            }
            else
            {
                // Selected category
                productFilter.Add(ProductCategoryFields.CategoryId == this.ddlCategoryId.Value.Value);
            }

            RelationCollection joins = new RelationCollection();
            joins.Add(ProductEntity.Relations.ProductCategoryEntityUsingProductId, JoinHint.Left);

            SortExpression sort = new SortExpression();
            sort.Add(ProductFields.Name | SortOperator.Ascending);

            products.GetMulti(productFilter, 0, sort, joins);

            // Load the generic products
            GenericproductCollection genericProducts = new GenericproductCollection();

            SortExpression sortGeneric = new SortExpression();
            sortGeneric.Add(GenericproductFields.Name | SortOperator.Ascending);
            genericProducts.GetMulti(null, 0, sortGeneric);


            for (int i = 0; i < products.Count; i++)
            {
                ProductEntity current = products[i];

                // Render row per product
                this.plhProducts.AddHtml("<tr><td class=\"label\">{0}</td><td class=\"control\">", current.Name);

                /*
                ComboBox cbi = new ComboBox();
                cbi.ID = "cbi" + current.ProductId.ToString();
                cbi.UseDataBinding = false;
                cbi.TextField = PosproductFields.Name.Name;
                cbi.ValueField = PosproductFields.PosproductId.Name;
                cbi.DataSource = posProducts;
                cbi.DataBind();
                this.plhProducts.Controls.Add(cbi);*/

                Dionysos.Web.UI.WebControls.DropDownListInt ddl = new Dionysos.Web.UI.WebControls.DropDownListInt();
                ddl.ID = "ddl-" + current.ProductId.ToString();
                ddl.UseDataBinding = false;
                ddl.DataTextField = GenericproductFields.Name.Name;
                ddl.DataValueField = GenericproductFields.GenericproductId.Name;
                ddl.DataSource = genericProducts;
                ddl.DataBind();

                ddl.Items.Insert(0, new ListItem("Geen - Maak uw keuze"));

                if (current.GenericproductId.HasValue)
                    ddl.Value = current.GenericproductId.Value;
                else
                    ddl.SelectedIndex = 0;

                this.plhProducts.Controls.Add(ddl);

                this.plhProducts.AddHtml("</td></tr>");
            }
        }

        void SaveMatches()
        {
            // Match
            foreach (var Control in this.plhProducts.Controls)
            {
                if (Control is Dionysos.Web.UI.WebControls.DropDownListInt)
                {
                    var ddl = Control as Dionysos.Web.UI.WebControls.DropDownListInt;
                    int productId = Convert.ToInt32(Dionysos.StringUtil.GetAllAfterFirstOccurenceOf(ddl.ID, '-'));
                    ProductEntity product = new ProductEntity(productId);

                    // verify company
                    if (product.CompanyId != CmsSessionHelper.CurrentCompanyId)
                        throw new Exception("Ongeldigde product selectie in kassakoppeling");

                    if (ddl.ValidId > 0)
                        product.GenericproductId = ddl.ValidId;
                    else
                        product.GenericproductId = null;

                    product.Save();
                }
            }
        }

        void HookupEvents()
        {
            this.btChangeCategory.Click += new EventHandler(btChangeCategory_Click);
            this.btMatch.Click += new EventHandler(btMatch_Click);
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookupEvents();
        }

        void btChangeCategory_Click(object sender, EventArgs e)
        {
            this.SaveMatches();
            QueryStringHelper qs = this.GetQueryStringInstanceWithValues();
            WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        void btMatch_Click(object sender, EventArgs e)
        {
            this.SaveMatches();
        }


        #endregion
    }
}