﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using System.Drawing;
using Dionysos.Web;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class PaymentMethods : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Product";
            this.EntityPageUrl = "~/Catalog/Product.aspx";
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LLBLGenProDataSource datasource = this.DataSource as LLBLGenProDataSource;
            if (datasource != null)
            {
                PredicateExpression filter = new PredicateExpression(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
                filter.Add(ProductFields.Type == ProductType.Paymentmethod);
                datasource.FilterToUse = filter;
            }

            if (this.MainGridView != null)
                this.MainGridView.HtmlRowPrepared += new DevExpress.Web.ASPxGridViewTableRowEventHandler(MainGridView_HtmlRowPrepared);
        }

        private void MainGridView_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType != DevExpress.Web.GridViewRowType.Data) return;
            bool isVisible = (bool)e.GetValue("Visible");
            if (!isVisible)
                e.Row.ForeColor = Color.Gray;
        }

        public override bool Add()
        {
            string url = string.Format("{0}?mode={1}&entity={2}&id=&type={3}", this.EntityPageUrl, "add", this.EntityName, (int)ProductType.Paymentmethod);
            WebShortcuts.ResponseRedirect(url, true);

            return true;
        }

        #endregion
    }
}
