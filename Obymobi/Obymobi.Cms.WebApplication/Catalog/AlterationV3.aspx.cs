﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.Generic.SubPanels;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class AlterationV3 : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields

        private OptionPanel optionPanel = null;

        #endregion

        #region Methods

        void HookUpEvents()
        {
            this.btRemoveAlterationFromProducts.Click += new EventHandler(this.btRemoveAlterationFromProducts_Click);

            if (this.PageMode == PageMode.Add)
            {
                this.cbIsAvailable.Checked = true;
                this.cbVisible.Checked = true;
            }
        }

        private void LoadUserControls()
        {
            Generic.UserControls.CustomTextCollection translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx") as Generic.UserControls.CustomTextCollection;
            translationsPanel.FullWidth = true;

            this.optionPanel = (OptionPanel)this.LoadControl("~/Generic/SubPanels/OptionPanel.ascx");
            this.optionPanel.ID = "ucOptionPanel";
            this.plhOptionsPanel.Controls.Add(this.optionPanel);
        }

        private void DataBindAlterationTypes()
        {
            List<AlterationType> supportedTypes = AlterationHelper.GetSupportedAlterationTypes(AlterationDialogMode.v3);

            if (this.DataSourceAsAlterationEntity.IsNew)
            {
                foreach (AlterationType type in supportedTypes)
                {
                    this.cbType.Items.Add(new ListEditItem(type.ToString(), (int)type));
                }
            }
            else if (supportedTypes.Any(at => at == this.DataSourceAsAlterationEntity.Type))
            {
                foreach (AlterationType type in supportedTypes)
                {
                    this.cbType.Items.Add(new ListEditItem(type.ToString(), (int)type));
                }
            }
            else
            {
                this.cbType.Items.Add(new ListEditItem(this.DataSourceAsAlterationEntity.Type.ToString(), (int)this.DataSourceAsAlterationEntity.Type));
                this.cbType.Enabled = false;
            }
        }

        private void DataBindExternalProductDropdown()
        {
            ExternalProductDataSource dataSource = new ExternalProductDataSource();
            ddlExternalProductId.DataSource = dataSource.GetDropdownDataSource(CompanyId, ExternalProductType.ModifierGroup);
            ddlExternalProductId.ValueField = nameof(ExternalProductFields.ExternalProductId);
            ddlExternalProductId.TextField = nameof(ExternalProductEntity.CombinedNameWithMenuAndSystemName);
            ddlExternalProductId.DataBind();
        }

        private void SetGui()
        {
            this.Informators.Clear();
            ValidateBrandPermissions();

            CustomTextRenderer renderer;
            if (this.DataSourceAsAlterationEntity.BrandId.HasValue)
            {
                this.plhPos.Visible = false;
                renderer = new CustomTextRenderer(this.DataSourceAsAlterationEntity, this.DataSourceAsAlterationEntity.BrandEntity.CultureCode);
            }
            else
            {
                renderer = new CustomTextRenderer(this.DataSourceAsAlterationEntity, CmsSessionHelper.DefaultCultureCodeForCompany);
            }

            DataBindAlterationTypes();
            DataBindExternalProductDropdown();

            if (this.optionPanel != null && !this.DataSourceAsAlterationEntity.IsNew)
            {
                if (this.DataSourceAsAlterationEntity.Type != AlterationType.Options && this.DataSourceAsAlterationEntity.Type != AlterationType.Form)
                {
                    this.optionPanel.Visible = false;
                    this.lblComponents.Visible = false;
                }
                else
                {
                    this.optionPanel.Load(2, -1, this.DataSourceAsAlterationEntity.AlterationId, -1, -1, true);
                    this.optionPanel.SetParentDataSource(this.DataSourceAsAlterationEntity);
                }
            }

            if (this.DataSourceAsAlterationEntity.PosalterationId.HasValue)
            {
                this.hlPosalterationId.Text = this.DataSourceAsAlterationEntity.PosAlterationEntity.Name + " &#187;&#187;";
                this.hlPosalterationId.Target = "_Blank";
                this.hlPosalterationId.NavigateUrl = this.ResolveUrl(string.Format("~/Catalog/Posalteration.aspx?id={0}", this.DataSourceAsAlterationEntity.PosalterationId.Value));
            }

            if (CmsSessionHelper.CurrentRole < Role.Administrator)
            {
                this.tabsMain.TabPages.FindByName("tabAdvanced").ClientVisible = false;
            }

            DateTime midnight = DateTime.Now;
            midnight = midnight.AddHours(-midnight.Hour);
            midnight = midnight.AddMinutes(-midnight.Minute);
            if (this.DataSourceAsAlterationEntity.MinLeadMinutes > 0)
            {
                DateTime minLeadMinutes = midnight.AddMinutes(this.DataSourceAsAlterationEntity.MinLeadMinutes);
                this.teMinLeadMinutes.Value = minLeadMinutes;
            }

            if (this.DataSourceAsAlterationEntity.IntervalMinutes > 0)
            {
                DateTime intervalMinutes = midnight.AddMinutes(this.DataSourceAsAlterationEntity.IntervalMinutes);
                this.teIntervalMinutes.Value = intervalMinutes;
            }

            if (this.DataSourceAsAlterationEntity.Type == AlterationType.Date)
            {
                this.teMaxLeadDays.Value = this.DataSourceAsAlterationEntity.MaxLeadHours / 24;
            }
            else if (this.DataSourceAsAlterationEntity.Type == AlterationType.DateTime)
            {
                this.teMaxLeadHours.Value = this.DataSourceAsAlterationEntity.MaxLeadHours;
            }

            if (this.DataSourceAsAlterationEntity.Type == AlterationType.DateTime)
            {
                this.plhMinMaxOptions.Visible = false;
                this.plhTimeOptions.Visible = true;
                this.plhMaxDays.Visible = false;
            }
            else if (this.DataSourceAsAlterationEntity.Type == AlterationType.Date)
            {
                this.plhMinMaxOptions.Visible = false;
                this.plhTimeOptions.Visible = false;
                this.plhMaxDays.Visible = true;
            }
            else if (this.DataSourceAsAlterationEntity.Type == AlterationType.Category)
            {
                this.plhMinMaxOptions.Visible = true;
                this.plhTimeOptions.Visible = false;
                this.plhMaxDays.Visible = false;
            }
            else if (this.DataSourceAsAlterationEntity.Type == AlterationType.Package)
            {
                this.plhMinMaxOptions.Visible = false;
                this.plhTimeOptions.Visible = false;
                this.plhMaxDays.Visible = false;
            }
            else
            {
                this.plhMinMaxOptions.Visible = true;
                this.plhTimeOptions.Visible = false;
                this.plhMaxDays.Visible = false;
            }

            tbMinOptions.ReadOnly = DataSourceAsAlterationEntity.ExternalProductId.HasValue;
            tbMaxOptions.ReadOnly = DataSourceAsAlterationEntity.ExternalProductId.HasValue;
            cbAllowDuplicates.Enabled = !DataSourceAsAlterationEntity.ExternalProductId.HasValue;

            this.lblDescriptionNote.Visible = this.DataSourceAsAlterationEntity.Type == AlterationType.Instruction;
            this.RenderProducts();

            renderer.RenderCustomText(this.tbOverwriteNoThanks, CustomTextType.AlterationNoThanksOption);
            renderer.RenderCustomText(this.tbColumnTitle, CustomTextType.AlterationColumnTitle);
            renderer.RenderCustomText(this.tbColumnSubtitle, CustomTextType.AlterationColumnSubtitle);

            MasterPages.MasterPageEntity masterPage = this.Master as MasterPages.MasterPageEntity;
            if (masterPage != null)
            {
                masterPage.SetPageTitle(string.Format("Alterations V3 - {0}", this.DataSourceAsAlterationEntity.Name));
            }

            this.ShowWarningIfTaxMissingOnOrderLevelAlterationptionsForAppLessCompany();

            cbIsAvailable.Enabled = !DataSourceAsAlterationEntity.ExternalProductId.HasValue;
        }

        private void ShowWarningIfTaxMissingOnOrderLevelAlterationptionsForAppLessCompany()
        {
            if (!this.DataSourceAsAlterationEntity.OrderLevelEnabled ||
                !this.DataSourceAsAlterationEntity.CompanyEntity.ApplicationConfigurationCollection.Any())
            {
                return;
            }

            IEnumerable<AlterationitemEntity> alterationitemsMissingTaxTariffs = this.DataSourceAsAlterationEntity.AlterationitemCollection
                .Where(alterationitem => alterationitem.AlterationoptionEntity.PriceIn.GetValueOrDefault(0) > 0 &&
                !alterationitem.AlterationoptionEntity.TaxTariffId.HasValue);

            if (alterationitemsMissingTaxTariffs.Any())
            {
                string alterationoptionNames = string.Join(", ", alterationitemsMissingTaxTariffs.Select(x => x.AlterationoptionName));

                this.AddInformator(InformatorType.Warning, $"There are alteration options which are missing tax tariffs: {alterationoptionNames}");
            }
        }

        void RenderProducts()
        {
            ProductCollection products = new ProductCollection();
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductAlterationFields.AlterationId == this.EntityId);

            RelationCollection joins = new RelationCollection();
            joins.Add(ProductEntityBase.Relations.ProductAlterationEntityUsingProductId);

            SortExpression sort = new SortExpression();
            sort.Add(ProductFields.Name | SortOperator.Ascending);

            IncludeFieldsList includes = new IncludeFieldsList(ProductFields.Name);

            products.GetMulti(filter, 0, null, joins, null, includes, 0, 0);

            foreach (ProductEntity product in products)
            {
                this.plhProducts.AddHtml("<tr><td><a href=\"{0}\" target=\"_blank\">{1}</a></td></tr>", this.ResolveUrl("~/Catalog/Product.aspx?Id=" + product.ProductId), product.Name);
            }
        }

        private void SetFilters()
        {
            if (DataSourceAsAlterationEntity.BrandId.HasValue)
            {
                return;
            }

            // Init AlterationItems
            // Init the AlterationSelection DropDownLitss
            EntityView<AlterationitemEntity> aiView = DataSourceAsAlterationEntity.AlterationitemCollection.DefaultView;

            // For backwards compatability, set SortOrders when not set		
            // Also fix SortOrder > 14

            #region Fix for backwards compatibility

            // Create items for this company if required:
            for (int i = aiView.Count - 1; i >= 0; i--)
            {
                AlterationitemEntity ai = aiView[i];
                if (ai.AlterationoptionEntity.CompanyId != CompanyId)
                {
                    AlterationoptionEntity newOption = new AlterationoptionEntity();
                    Dionysos.Data.LLBLGen.LLBLGenEntityUtil.CopyFields(ai.AlterationoptionEntity, newOption, null);
                    newOption.CompanyId = CompanyId;
                    newOption.Save();

                    ai.AlterationoptionId = newOption.AlterationoptionId;
                }
            }

            #endregion

            SetPosalterationIdFilter();
        }

        private void SetPosalterationIdFilter()
        {
            PredicateExpression posalterationFilter = new PredicateExpression(PosalterationFields.CompanyId == CompanyId);
            SortExpression posalterationSort = new SortExpression(new SortClause(PosalterationFields.Name, SortOperator.Ascending));

            PosalterationCollection posalterationCollection = new PosalterationCollection();
            posalterationCollection.GetMulti(posalterationFilter, 0, posalterationSort);

            foreach (PosalterationEntity posalteration in posalterationCollection)
            {
                if (!string.IsNullOrWhiteSpace(posalteration.RevenueCenter))
                {
                    this.ddlPosalterationId.Items.Add($"{posalteration.Name} ({posalteration.ExternalId}) [{posalteration.RevenueCenter}]", posalteration.PosalterationId);
                }
                else
                {
                    this.ddlPosalterationId.Items.Add($"{posalteration.Name} ({posalteration.ExternalId})", posalteration.PosalterationId);
                }
            }
        }

        private void SetWarnings()
        {
            if ((CompanyHelper.GetPOSConnectorType(CompanyId, null) != POSConnectorType.Unknown) && !DataSourceAsAlterationEntity.PosalterationId.HasValue)
            {
                AddInformator(InformatorType.Warning, "The item is not linked to the PoS and therefore will not be visible to guests.");
            }

            Validate();
        }

        public override void Validate()
        {
            if (!this.DataSourceAsAlterationEntity.IsNew && this.DataSourceAsAlterationEntity.Type == AlterationType.DateTime &&
               (!this.teIntervalMinutes.Value.HasValue || (this.teIntervalMinutes.Value.Value.Hour == 0 && this.teIntervalMinutes.Value.Value.Minute == 0)))
            {
                this.MultiValidatorDefault.AddError("Interval time must be at least 1 minute.");
            }

            base.Validate();
        }

        public override bool Save()
        {
            if (PageMode == PageMode.Add && ParentBrandId == 0)
            {
                DataSourceAsAlterationEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            if (teStartTime.Value.HasValue)
            {
                DataSourceAsAlterationEntity.StartTime = DateTimeUtil.CombineDateAndTime(teStartTime.Value.Value, DateTime.Now);
            }

            if (teEndTime.Value.HasValue)
            {
                DataSourceAsAlterationEntity.EndTime = DateTimeUtil.CombineDateAndTime(teEndTime.Value.Value, DateTime.Now);
            }

            if (teMinLeadMinutes.Value.HasValue)
            {
                DataSourceAsAlterationEntity.MinLeadMinutes = (teMinLeadMinutes.Value.Value.Hour * 60) + teMinLeadMinutes.Value.Value.Minute;
            }

            if (teIntervalMinutes.Value.HasValue)
            {
                DataSourceAsAlterationEntity.IntervalMinutes = (teIntervalMinutes.Value.Value.Hour * 60) + teIntervalMinutes.Value.Value.Minute;
            }

            bool clearOptionFields = false;
            if (this.DataSourceAsAlterationEntity.Type == AlterationType.Date)
            {
                if (this.teMaxLeadDays.Value.HasValue)
                {
                    this.DataSourceAsAlterationEntity.MaxLeadHours = this.teMaxLeadDays.Value.Value * 24;
                }
                clearOptionFields = true;
            }
            else if (this.DataSourceAsAlterationEntity.Type == AlterationType.DateTime)
            {
                if (this.teMaxLeadHours.Value.HasValue)
                {
                    this.DataSourceAsAlterationEntity.MaxLeadHours = this.teMaxLeadHours.Value.Value;
                }
                clearOptionFields = true;
            }

            if (clearOptionFields)
            {
                this.DataSourceAsAlterationEntity.MinOptions = 0;
                this.DataSourceAsAlterationEntity.MaxOptions = 1;
            }

            this.DataSourceAsAlterationEntity.Version = 2;
            this.DataSourceAsAlterationEntity.StartTimeUTC = this.DataSourceAsAlterationEntity.StartTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
            this.DataSourceAsAlterationEntity.EndTimeUTC = this.DataSourceAsAlterationEntity.EndTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);

            if (base.Save())
            {
                this.SaveCustomTexts();
                return true;
            }

            return false;
        }

        private void SaveCustomTexts()
        {
            CustomTextRenderer renderer;
            if (this.DataSourceAsAlterationEntity.BrandId.HasValue)
            {
                Obymobi.Logic.HelperClasses.CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsAlterationEntity.BrandEntity.CultureCode);
                renderer = new CustomTextRenderer(this.DataSourceAsAlterationEntity, this.DataSourceAsAlterationEntity.BrandEntity.CultureCode);
            }
            else
            {
                Obymobi.Logic.HelperClasses.CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsAlterationEntity.CompanyEntity);
                renderer = new CustomTextRenderer(this.DataSourceAsAlterationEntity, CmsSessionHelper.DefaultCultureCodeForCompany);
            }

            renderer.SaveCustomText(this.tbOverwriteNoThanks, CustomTextType.AlterationNoThanksOption);
            renderer.SaveCustomText(this.tbColumnTitle, CustomTextType.AlterationColumnTitle);
            renderer.SaveCustomText(this.tbColumnSubtitle, CustomTextType.AlterationColumnSubtitle);
        }

        private void ValidateBrandPermissions()
        {
            if (ParentBrandId > 0)
            {
                BrandEntity brandEntity = new BrandEntity(ParentBrandId);
                if (brandEntity.IsNew)
                {
                    this.AddInformator(InformatorType.Warning, "You do not have permissions to add alterations to this brand");
                    this.DataSourceAsAlterationEntity.BrandId = null;
                }

                if (this.DataSourceAsAlterationEntity.IsNew)
                {
                    BrandRole? role = CmsSessionHelper.GetUserRoleForBrand(ParentBrandId);
                    if (role.GetValueOrDefault(BrandRole.Viewer) == BrandRole.Viewer)
                    {
                        // No permission to add product to this brand
                        this.AddInformator(InformatorType.Warning, "You do not have permissions to add alterations to this brand");
                        this.DataSourceAsAlterationEntity.BrandId = null;
                    }

                    this.AddInformator(InformatorType.Information, "This alteration will be created in brand: " + brandEntity.Name);
                    this.DataSourceAsAlterationEntity.BrandId = ParentBrandId;
                }
                else
                {
                    this.AddInformator(InformatorType.Information, "This alteration is part of the brand: " + brandEntity.Name);
                }
            }
        }

        #endregion

        #region Properties

        private int ParentBrandId
        {
            get
            {
                if (this.DataSourceAsAlterationEntity.BrandId.HasValue)
                {
                    this.ViewState["BrandId"] = this.DataSourceAsAlterationEntity.BrandId.Value;
                }

                return Convert.ToInt32((this.ViewState["BrandId"] as int?) ?? (this.ViewState["BrandId"] = QueryStringHelper.GetValue<int>("BrandId")));
            }
        }

        private int CompanyId => DataSourceAsAlterationEntity.CompanyId.GetValueOrDefault(CmsSessionHelper.CurrentCompanyId);

        public AlterationEntity DataSourceAsAlterationEntity => DataSource as AlterationEntity;

        #endregion

        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "Alteration";
            this.LoadUserControls();
            this.DataSourceLoaded += this.Alteration_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.SetWarnings();
            }

            this.HookUpEvents();
        }

        private void Alteration_DataSourceLoaded(object sender)
        {
            if (this.DataSourceAsAlterationEntity.IsNew)
            {
                this.DataSourceAsAlterationEntity.Visible = true;
            }

            this.SetFilters();
            this.SetGui();
        }

        void btRemoveAlterationFromProducts_Click(object sender, EventArgs e)
        {
            AlterationHelper.RemoveAlterationFromProducts(this.DataSourceAsAlterationEntity.AlterationId, out int removedFromXProducts);

            this.AddInformatorInfo(string.Format(this.Translate("OrderFailedProductNotSpecified", "This alteration has been removed from {0} products."), removedFromXProducts));
        }

        #endregion
    }
}
