﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.Catalog.SubPanels;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Alteration : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Fields

        private AlterationComponentsPanel componentsPanel = null;
        private Subpanels.AlterationitemCollection alterationitemPanel = null;

        #endregion

        #region Methods

        bool ValidateBrandPermissions()
        {
            if (ParentBrandId > 0)
            {
                BrandEntity brandEntity = new BrandEntity(ParentBrandId);
                if (brandEntity.IsNew)
                {
                    this.AddInformator(InformatorType.Warning, "You do not have permissions to add alterations to this brand");
                    this.DataSourceAsAlterationEntity.BrandId = null;
                    return false;
                }

                if (this.DataSourceAsAlterationEntity.IsNew)
                {
                    BrandRole? role = CmsSessionHelper.GetUserRoleForBrand(ParentBrandId);
                    if (role.GetValueOrDefault(BrandRole.Viewer) == BrandRole.Viewer)
                    {
                        // No permission to add product to this brand
                        this.AddInformator(InformatorType.Warning, "You do not have permissions to add alterations to this brand");
                        this.DataSourceAsAlterationEntity.BrandId = null;
                        return false;
                    }

                    this.AddInformator(InformatorType.Information, "This alteration will be created in brand: " + brandEntity.Name);
                    this.DataSourceAsAlterationEntity.BrandId = ParentBrandId;
                }
                else
                {
                    this.AddInformator(InformatorType.Information, "This product is part of the brand: " + brandEntity.Name);
                }

                return true;
            }

            return false;
        }

        void HookUpEvents()
        {
            this.btRemoveAlterationFromProducts.Click += new EventHandler(this.btRemoveAlterationFromProducts_Click);
        }

        private void LoadUserControls()
        {
            Generic.UserControls.CustomTextCollection translationsPanel = this.tabsMain.AddTabPage("Translations", "CustomTextCollection", "~/Generic/UserControls/CustomTextCollection.ascx", "Generic") as Generic.UserControls.CustomTextCollection;
            translationsPanel.FullWidth = true;

            this.alterationitemPanel = this.tabsMain.AddTabPage("Options", "Options", "~/Catalog/SubPanels/AlterationitemCollection.ascx", "tabAdvanced", true) as Subpanels.AlterationitemCollection;
            this.tabsMain.AddTabPage("Images", "Media", "~/Generic/UserControls/MediaCollection.ascx");

            this.componentsPanel = (AlterationComponentsPanel)this.LoadControl("~/Catalog/SubPanels/AlterationComponentsPanel.ascx");
            this.plhAlterationComponentsPanel.Controls.Add(this.componentsPanel);
        }

        private void DataBindAlterationTypes()
        {
            List<AlterationType> supportedTypes = AlterationHelper.GetSupportedAlterationTypes(AlterationDialogMode.v2);
            
            if (this.DataSourceAsAlterationEntity.IsNew)
            {
                foreach (AlterationType type in supportedTypes)
                {
                    this.cbType.Items.Add(new ListEditItem(type.ToString(), (int)type));
                }
            }
            else if (supportedTypes.Any(at => at == this.DataSourceAsAlterationEntity.Type))
            {
                foreach (AlterationType type in supportedTypes)
                {
                    this.cbType.Items.Add(new ListEditItem(type.ToString(), (int)type));
                }
            }
            else
            {                
                this.cbType.Items.Add(new ListEditItem(this.DataSourceAsAlterationEntity.Type.ToString(), (int)this.DataSourceAsAlterationEntity.Type));
                this.cbType.Enabled = false;
            }
        }

        private void SetGui()
        {
            this.Informators.Clear();
            bool hasBrandPermission = ValidateBrandPermissions();

            if (this.DataSourceAsAlterationEntity.BrandId.HasValue)
            {
                this.plhPos.Visible = false;
            }

            this.DataBindAlterationTypes();

            if (this.DataSourceAsAlterationEntity.Type == AlterationType.Package && this.componentsPanel != null)
            {
                this.componentsPanel.RefreshDataSource(this.DataSourceAsAlterationEntity);
            }

            if (this.DataSourceAsAlterationEntity.PosalterationId.HasValue)
            {
                this.hlPosalterationId.Text = this.DataSourceAsAlterationEntity.PosAlterationEntity.Name + " &#187;&#187;";
                this.hlPosalterationId.Target = "_Blank";
                this.hlPosalterationId.NavigateUrl = this.ResolveUrl(string.Format("~/Catalog/Posalteration.aspx?id={0}", this.DataSourceAsAlterationEntity.PosalterationId.Value));
            }

            if (CmsSessionHelper.CurrentRole < Role.Administrator)
            {
                this.tabsMain.TabPages.FindByName("tabAdvanced").ClientVisible = false;
            }

            DateTime midnight = DateTime.Now;
            midnight = midnight.AddHours(-midnight.Hour);
            midnight = midnight.AddMinutes(-midnight.Minute);
            if (this.DataSourceAsAlterationEntity.MinLeadMinutes > 0)
            {
                DateTime minLeadMinutes = midnight.AddMinutes(this.DataSourceAsAlterationEntity.MinLeadMinutes);
                this.teMinLeadMinutes.Value = minLeadMinutes;
            }

            if (this.DataSourceAsAlterationEntity.IntervalMinutes > 0)
            {
                DateTime intervalMinutes = midnight.AddMinutes(this.DataSourceAsAlterationEntity.IntervalMinutes);
                this.teIntervalMinutes.Value = intervalMinutes;
            }

            if (this.DataSourceAsAlterationEntity.Type == AlterationType.Date)
            {
                this.teMaxLeadDays.Value = this.DataSourceAsAlterationEntity.MaxLeadHours / 24;
            }
            else if (this.DataSourceAsAlterationEntity.Type == AlterationType.DateTime)
            {
                this.teMaxLeadHours.Value = this.DataSourceAsAlterationEntity.MaxLeadHours;
            }

            if (this.DataSourceAsAlterationEntity.Type == AlterationType.DateTime)
            {
                this.plhMinMaxOptions.Visible = false;
                this.plhTimeOptions.Visible = true;
                this.plhMaxDays.Visible = false;
                this.plhAlterationComponents.Visible = false;
            }
            else if(this.DataSourceAsAlterationEntity.Type == AlterationType.Date)
            {
                this.plhMinMaxOptions.Visible = false;
                this.plhTimeOptions.Visible = false;
                this.plhMaxDays.Visible = true;
                this.plhAlterationComponents.Visible = false;
            }
            else if (this.DataSourceAsAlterationEntity.Type == AlterationType.Category)
            {
                this.plhMinMaxOptions.Visible = true;
                this.plhTimeOptions.Visible = false;
                this.plhMaxDays.Visible = false;
                this.plhAlterationComponents.Visible = false;
            }
            else if (this.DataSourceAsAlterationEntity.Type == AlterationType.Package)
            {
                this.plhMinMaxOptions.Visible = false;
                this.plhTimeOptions.Visible = false;
                this.plhMaxDays.Visible = false;
                this.plhAlterationComponents.Visible = true;
            }
            else
            {
                this.plhMinMaxOptions.Visible = true;
                this.plhTimeOptions.Visible = false;
                this.plhMaxDays.Visible = false;
                this.plhAlterationComponents.Visible = false;
            }

            if (this.alterationitemPanel != null)
            {
                this.alterationitemPanel.AlterationEntity = this.DataSourceAsAlterationEntity;
            }

            this.lblDescriptionNote.Visible = this.DataSourceAsAlterationEntity.Type == AlterationType.Instruction;
            this.plhMarkdown.Visible = this.DataSourceAsAlterationEntity.Type == AlterationType.Instruction;
            
            this.RenderProducts();

            MasterPages.MasterPageEntity masterPage = this.Master as MasterPages.MasterPageEntity;
            if (masterPage != null)
            {
                masterPage.SetPageTitle(string.Format("Alterations V1 & V2 - {0}", this.DataSourceAsAlterationEntity.Name));
            }
        }

        void RenderProducts()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductAlterationFields.AlterationId == this.EntityId);

            RelationCollection joins = new RelationCollection();
            joins.Add(ProductEntityBase.Relations.ProductAlterationEntityUsingProductId);

            SortExpression sort = new SortExpression();
            sort.Add(ProductFields.Name | SortOperator.Ascending);

            ProductCollection products = new ProductCollection();

            products.GetMulti(filter, 0, null, joins);

            foreach (ProductEntity product in products)
            {
                this.plhProducts.AddHtml("<tr><td><a href=\"{0}\" target=\"_blank\">{1}</a></td></tr>", this.ResolveUrl("~/Catalog/Product.aspx?Id=" + product.ProductId), product.Name);
            }
        }

        private void SetFilters()
        {
            if (!this.DataSourceAsAlterationEntity.BrandId.HasValue)
            {
                // For backwards compatability, set SortOrders when not set		
                // Also fix SortOrder > 14

                #region Fix for backwards compatibility

                EntityView<AlterationitemEntity> aiView = this.DataSourceAsAlterationEntity.AlterationitemCollection.DefaultView;

                // Create items for this company if required:
                for (int i = aiView.Count - 1; i >= 0; i--)
                {
                    AlterationitemEntity ai = aiView[i];
                    if (ai.AlterationoptionEntity.CompanyId != CmsSessionHelper.CurrentCompanyId)
                    {
                        AlterationoptionEntity newOption = new AlterationoptionEntity();
                        Dionysos.Data.LLBLGen.LLBLGenEntityUtil.CopyFields(ai.AlterationoptionEntity, newOption, null);
                        newOption.CompanyId = CmsSessionHelper.CurrentCompanyId;
                        newOption.Save();

                        ai.AlterationoptionId = newOption.AlterationoptionId;
                    }
                }

                #endregion

                SetPosalterationIdFilter();
            }
        }

        private void SetPosalterationIdFilter()
        {
            PredicateExpression posalterationFilter = new PredicateExpression(PosalterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression posalterationSort = new SortExpression(new SortClause(PosalterationFields.Name, SortOperator.Ascending));

            PosalterationCollection posalterationCollection = new PosalterationCollection();
            posalterationCollection.GetMulti(posalterationFilter, 0, posalterationSort);

            foreach (PosalterationEntity posalteration in posalterationCollection)
            {
                if (!string.IsNullOrWhiteSpace(posalteration.RevenueCenter))
                {
                    this.ddlPosalterationId.AddItem($"{posalteration.Name} ({posalteration.ExternalId}) [{posalteration.RevenueCenter}]", posalteration.PosalterationId);
                }
                else
                {
                    this.ddlPosalterationId.AddItem($"{posalteration.Name} ({posalteration.ExternalId})", posalteration.PosalterationId);
                }
            }
        }

        private void SetWarnings()
        {
            if ((DataSourceAsAlterationEntity.Type == AlterationType.Options && this.DataSourceAsAlterationEntity.AlterationitemCollection.Count == 0))
            {
                AddInformator(InformatorType.Warning, "The product does not have any options assigned to it. ");
            }

            if ((CompanyHelper.GetPOSConnectorType(CmsSessionHelper.CurrentCompanyId, null) != POSConnectorType.Unknown) && !this.DataSourceAsAlterationEntity.PosalterationId.HasValue)
            {
                AddInformator(InformatorType.Warning, "The item is not linked to the PoS and therefore will not be visible to guests.");
            }

            Validate();
        }

        public override void Validate()
        {
            if (!this.DataSourceAsAlterationEntity.IsNew && this.DataSourceAsAlterationEntity.Type == AlterationType.DateTime && 
               (!this.teIntervalMinutes.Value.HasValue || (this.teIntervalMinutes.Value.Value.Hour == 0 && this.teIntervalMinutes.Value.Value.Minute == 0)))
            {
                this.MultiValidatorDefault.AddError("Interval time must be at least 1 minute.");
            }

            base.Validate();
        }

		public override bool Save()
		{
		    if (this.PageMode == Dionysos.Web.PageMode.Add)
		    {
		        if (ParentBrandId == 0)
		        {
		            this.DataSourceAsAlterationEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
		        }
		    }

		    if (this.teStartTime.Value.HasValue)
               this.DataSourceAsAlterationEntity.StartTime = DateTimeUtil.CombineDateAndTime(this.teStartTime.Value.Value, DateTime.Now);

           if (this.teEndTime.Value.HasValue)
               this.DataSourceAsAlterationEntity.EndTime = DateTimeUtil.CombineDateAndTime(this.teEndTime.Value.Value, DateTime.Now);

           if (this.teMinLeadMinutes.Value.HasValue)
               this.DataSourceAsAlterationEntity.MinLeadMinutes = (this.teMinLeadMinutes.Value.Value.Hour * 60) + this.teMinLeadMinutes.Value.Value.Minute;

           if (this.teIntervalMinutes.Value.HasValue)
               this.DataSourceAsAlterationEntity.IntervalMinutes = (this.teIntervalMinutes.Value.Value.Hour * 60) + this.teIntervalMinutes.Value.Value.Minute;

           bool clearOptionFields = false;
           if (this.DataSourceAsAlterationEntity.Type == AlterationType.Date)
           {
               if (this.teMaxLeadDays.Value.HasValue)
               {
                   this.DataSourceAsAlterationEntity.MaxLeadHours = this.teMaxLeadDays.Value.Value * 24;
               }
               clearOptionFields = true;
           }
           else if (this.DataSourceAsAlterationEntity.Type == AlterationType.DateTime)
           {
               if (this.teMaxLeadHours.Value.HasValue)
               {
                   this.DataSourceAsAlterationEntity.MaxLeadHours = this.teMaxLeadHours.Value.Value;
               }
               clearOptionFields = true;
           }

           if (clearOptionFields)
           {
               this.DataSourceAsAlterationEntity.MinOptions = 0;
               this.DataSourceAsAlterationEntity.MaxOptions = 1;
           }

		    this.DataSourceAsAlterationEntity.StartTimeUTC = this.DataSourceAsAlterationEntity.StartTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
		    this.DataSourceAsAlterationEntity.EndTimeUTC = this.DataSourceAsAlterationEntity.EndTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);

           if (base.Save())
           {
               if (this.DataSourceAsAlterationEntity.BrandId.HasValue)
               {
                   Obymobi.Logic.HelperClasses.CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsAlterationEntity.BrandEntity.CultureCode);
               }
               else
               {
                   Obymobi.Logic.HelperClasses.CustomTextHelper.UpdateCustomTexts(this.DataSource, this.DataSourceAsAlterationEntity.CompanyEntity.CultureCode);
               }

               return true;
            }

            return false;
        }

        #endregion

		#region Properties

        private int ParentBrandId
        {
            get
            {
                if (this.DataSourceAsAlterationEntity.BrandId.HasValue)
                {
                    this.ViewState["BrandId"] = this.DataSourceAsAlterationEntity.BrandId.Value;
                }

                return Convert.ToInt32((this.ViewState["BrandId"] as int?) ?? (this.ViewState["BrandId"] = QueryStringHelper.GetValue<int>("BrandId")));
            }
            set { this.ViewState["BrandId"] = value; }
        }

		public AlterationEntity	DataSourceAsAlterationEntity
		{
			get
			{
				return this.DataSource as AlterationEntity;
			}
		}

		#endregion

		#region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.Alteration_DataSourceLoaded;
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
                this.SetWarnings();
            this.HookUpEvents();
        }

		private void Alteration_DataSourceLoaded(object sender)
		{
            if (this.DataSourceAsAlterationEntity.IsNew)
                this.DataSourceAsAlterationEntity.Visible = true;

			this.SetFilters();
            this.SetGui();
		}

        void btRemoveAlterationFromProducts_Click(object sender, EventArgs e)
        {
            int removedFromXProducts = 0;
            Obymobi.Cms.Logic.HelperClasses.AlterationHelper.RemoveAlterationFromProducts(this.DataSourceAsAlterationEntity.AlterationId, out removedFromXProducts);

            this.AddInformatorInfo(string.Format(this.Translate("OrderFailedProductNotSpecified", "This alteration has been removed from {0} products."), removedFromXProducts));
        }

		#endregion
	}
}
