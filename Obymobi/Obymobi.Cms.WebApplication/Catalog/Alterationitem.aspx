﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.Alterationitem" Codebehind="Alterationitem.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblAlterationoptionId">Alteration option</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlAlterationoptionId" EntityName="Alterationoption" IncrementalFilteringMode="StartsWith" TextField="NameAndPriceIn" ValueField="AlterationoptionId" PreventEntityCollectionInitialization="true" IsRequired="true"></X:ComboBoxLLBLGenEntityCollection>
							</td>
                            <td class="label">
                            </td>
                            <td class="control">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlAlterationoptionId"></D:HyperLink>
                            </td>
						</tr>
                        <tr>
							<td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblAlterationId">Alteration</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlAlterationId" EntityName="Alteration" DisabledOnceSelectedFromQueryString="True" DisabledOnceSelectedAndSaved="True" IncrementalFilteringMode="StartsWith" TextField="Name" PreventEntityCollectionInitialization="True" ValueField="AlterationId" IsRequired="true"></X:ComboBoxLLBLGenEntityCollection>
							</td> 
                            <td class="label">
                            </td>
                            <td class="control">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlAlterationId"></D:HyperLink>
                            </td>
                        </tr>
						<tr>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSortOrder">Sort order</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
							    <D:TextBoxInt ID="tbSortOrder" runat="server"></D:TextBoxInt>
							</td>
							<td class="label">
								<D:LabelEntityFieldInfo runat="server" id="lblSelectedOnDefault">Selected on default</D:LabelEntityFieldInfo>
							</td>
							<td class="control">
								<D:CheckBox runat="server" ID="cbSelectedOnDefault" />
							</td>
                        </tr>    
                        <tr>
                            <td class="label">
                                <D:LabelEntityFieldInfo runat="server" id="lblPrice">Price</D:LabelEntityFieldInfo>
                            </td>
                            <td class="control">
                                <D:TextBoxDecimal runat="server" ID="tbPrice" />
                            </td>
                        </tr>                                        
					 </table>			
				</controls>
			</X:TabPage>	
            <X:TabPage Text="Technical" Name="tabAdvanced">
                <controls>
				<D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
				        <tr>
                            <td class="label">POS Alteration item</td>
							<td class="control">
                                <U:UltraBoxInt runat="server" ID="ddlPosalterationitemId" />
							</td>
							<td class="label">
                                <D:HyperLink runat="server" LocalizeText="false" ID="hlPosalterationitemId"></D:HyperLink>
							</td>
							<td class="control">
							</td>
						</tr>
                    </table>
                </D:PlaceHolder>
                </controls>
            </X:TabPage>
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

