﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Dionysos.Web;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Integrations.POS;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class PosDeliverypointImport : Dionysos.Web.UI.PageQueryStringDataBinding
    {
        #region Fields

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.SetGui();
            base.OnInit(e);
            this.LoadPosDeliverypoints();
        }

        protected override void SetDefaultValuesToControls()
        {
            this.cbDeliverypointgroupId.Value = null;
            this.tbPagingSize.Value = Dionysos.ConfigurationManager.GetInt(DionysosWebConfigurationConstants.PageSize);
            this.cbHideImportedDeliverypoints.Checked = true;
        }

        public void RefreshSyncedDeliverypoints()
        {
            PosDataSynchronizer syncer = new PosDataSynchronizer(CmsSessionHelper.CurrentCompanyId, CmsSessionHelper.AlterationDialogMode);
            syncer.SynchronizeDeliverypoints(false, true);
        }

        public void Import()
        {
            if (this.cbDeliverypointgroupId.SelectedIndex == 0)
            {
                this.AddInformator(Dionysos.Web.UI.InformatorType.Warning, "Please select a deliverypointgroup to import the deliverypoints in.");
                this.Validate();
            }
            else
            {
                // Do Import
                try
                {
                    List<object> selection = grid.GetSelectedFieldValues(grid.KeyFieldName);

                    List<int> deliverypointIds = new List<int>();

                    foreach (var selectedId in selection)
                    {
                        deliverypointIds.Add(Convert.ToInt32(selectedId));
                    }

                    PosDataSynchronizer sync = new PosDataSynchronizer(CmsSessionHelper.CurrentCompanyId, CmsSessionHelper.AlterationDialogMode);

                    if (this.cbDeliverypointgroupId.SelectedIndex != 0)
                    {
                        sync.SynchronizeDeliverypoints(deliverypointIds, this.cbDeliverypointgroupId.Value);
                    }
                    else
                    {
                        sync.SynchronizeDeliverypoints(deliverypointIds, null);
                    }

                    // Refresh page
                    QueryStringHelper qs = new QueryStringHelper();
                    qs.AddItem("imported", deliverypointIds.Count);
                    qs.AddItem("importedto", this.cbDeliverypointgroupId.SelectedItem.Text);
                    this.Response.Redirect(qs.MergeQuerystringWithRawUrl(), true);
                }
                catch (ObymobiException oex)
                {
                    if (oex.ErrorEnumValue is DeliverypointSaveResult &&
                        (DeliverypointSaveResult)oex.ErrorEnumValue == DeliverypointSaveResult.MultipleDeliverypointsWithSameNumber)
                    {
                        string message = this.Translate("DeliverypointAlreadyImported", "Eén of meer kamers is al in gebruik: {0}");
                        this.MultiValidatorDefault.AddError(message.FormatSafe(oex.Message));
                        this.Validate();
                    }
                    else
                        throw;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
       }

        public void RefreshSettings()
        {
            QueryStringHelper qs = this.GetQueryStringInstanceWithValues();
            qs.AddItem("imported", string.Empty);
            qs.AddItem("importedto", string.Empty);
            WebShortcuts.Redirect(qs.MergeQuerystringWithRawUrl());
        }

        void SetGui()
        {
            IEnumerable<CategoryEntity> categories = CmsSessionHelper.GetCategoriesForCompanyFlat(true).OrderBy(c => c.Name);

            DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();
            deliverypointgroupCollection.GetMulti(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            this.cbDeliverypointgroupId.DataSource = deliverypointgroupCollection;
            this.cbDeliverypointgroupId.DataBind();

            if (!this.IsPostBack)
            {
                int importedDeliverypoints;
                if (QueryStringHelper.TryGetValue("imported", out importedDeliverypoints))
                {
                    string importedTo; 
                    if(QueryStringHelper.TryGetValue("importedto", out importedTo))
                        this.AddInformatorInfo("{0} deliverypoints have been imported to {1}.", importedDeliverypoints, importedTo);
                    else
                        this.AddInformatorInfo("{0} deliverypoints have been imported.", importedDeliverypoints);
                    this.Validate();
                }
            }
        }

        void LoadPosDeliverypoints()
        {
            this.grid.SettingsBehavior.EnableRowHotTrack = true;

            RelationCollection joins = new RelationCollection();

            // Retrieve Pos Products
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosdeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

            if (this.cbHideImportedDeliverypoints.Checked)
            {
                joins.Add(PosdeliverypointEntity.Relations.DeliverypointEntityUsingPosdeliverypointId, JoinHint.Left);
                filter.Add(DeliverypointFields.DeliverypointId == DBNull.Value);
            }

            PosdeliverypointCollection posdeliverypointCollection = new PosdeliverypointCollection();
            posdeliverypointCollection.GetMulti(filter, joins);

            // Check for non-integer Table Names


            // Translations
            if (this.grid.Columns["ExternalId"] != null)
                this.grid.Columns["ExternalId"].Caption = this.Translate("clmnExternalId", "Extern Id");

            if (this.grid.Columns["Name"] != null)
                this.grid.Columns["Name"].Caption = this.Translate("clmnName", "Naam");

            if (this.grid.Columns["ExternalPosdeliverypointgroupId"] != null)
                this.grid.Columns["ExternalPosdeliverypointgroupId"].Caption = this.Translate("clmnExternalPosDeliverypointgroupId", "Extern POS tafelgroep Id");

            this.grid.DataSource = posdeliverypointCollection;
            this.grid.DataBind();
        }

        #endregion

        #region Event Handlers

        #endregion
    }
}