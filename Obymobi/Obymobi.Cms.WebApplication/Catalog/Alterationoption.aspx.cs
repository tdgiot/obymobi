using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Web;
using Dionysos;
using Dionysos.Data.Extensions;
using Dionysos.Web;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.WebApplication.AppLess;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.Comparers;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.MasterPages;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Alterationoption : Dionysos.Web.UI.PageLLBLGenEntity
    {
        private Generic.UserControls.CustomTextCollection translationsTab;

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.LoadUserControls();
            this.DataSourceLoaded += this.Alterationoption_DataSourceLoaded;
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            translationsTab = this.LoadTranslationsTab(fullWidth: true);
            
            // Tags permissions
            this.tagBox.ReadOnly = CmsSessionHelper.CurrentRole == Role.Console;
            this.tagBox.AllowCustomTokens = CmsSessionHelper.CurrentRole >= Role.Supervisor;
        }

        private void Alterationoption_DataSourceLoaded(object sender)
        {
            this.SetGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
                this.SetWarnings();

            if (this.DataSourceAsAlterationoptionEntity.PosalterationoptionId.HasValue)
            {
                this.hlPosalterationoptionId.Text = this.DataSourceAsAlterationoptionEntity.PosalterationoptionEntity.Name + " &#187;&#187;";
                this.hlPosalterationoptionId.Target = "_Blank";
                this.hlPosalterationoptionId.NavigateUrl = ResolveUrl(string.Format("~/Catalog/Posalterationoption.aspx?id={0}", this.DataSourceAsAlterationoptionEntity.PosalterationoptionId.Value));
            }

            if (this.DataSourceAsAlterationoptionEntity.PosproductId.HasValue)
            {
                this.hlPosproductId.Text = this.DataSourceAsAlterationoptionEntity.PosproductEntity.Name + " &#187;&#187;";
                this.hlPosproductId.Target = "_Blank";
                this.hlPosproductId.NavigateUrl = ResolveUrl(string.Format("~/Catalog/Posproduct.aspx?id={0}", this.DataSourceAsAlterationoptionEntity.PosproductId.Value));
            }
        }

        private void SetGui()
        {
            if (PageMode != PageMode.Add)
            {
                plhAdvancedPos.Visible = !DataSourceAsAlterationoptionEntity.BrandId.HasValue;
                btUnlinkProduct.Visible = DataSourceAsAlterationoptionEntity.ProductId.HasValue;

                SetPosAlterationoptionIdFilter();
                SetPosProductIdFilter();
                BindTagBox();
                BindExternalProductDropdown();
            }
            
            plhExternalProduct.Visible = PageMode != PageMode.Add;

            tbPriceIn.ReadOnly = DataSourceAsAlterationoptionEntity.ExternalProductId.HasValue;

            SetVersion();
            SetPriceLabel();

            BindAlterationTypeDropdown();
            BindTaxTariffDropdown();
            BindLinkProductDropdown();

            if (DataSourceAsAlterationoptionEntity.ProductId.HasValue)
            {
                translationsTab.DisableCustomTextTypes(CustomTextType.AlterationoptionName);
            }

            RenderAlterationoptionFields(DataSourceAsAlterationoptionEntity);

            cbIsAvailable.Enabled = !DataSourceAsAlterationoptionEntity.ExternalProductId.HasValue;
        }

        public void UnlinkProduct(object sender, EventArgs e)
        {
            if (!DataSourceAsAlterationoptionEntity.ProductId.HasValue)
            {
                return;
            }

            RemoveInheritedTags();

            DataSourceAsAlterationoptionEntity.ProductId = null;
            DataSourceAsAlterationoptionEntity.Save();

            RedirectUsingRawUrl();
        }

        private void RemoveInheritedTags()
        {
            var alterationoptionTags = DataSourceAsAlterationoptionEntity.GetTags();
            var inheritedTags = DataSourceAsAlterationoptionEntity.GetInheritedTags();

            var selectedTags = alterationoptionTags.Except(inheritedTags, new TagNameEqualityComparer()).ToArray();

            TagHelper.UpdateTagsForEntity<AlterationoptionEntity, AlterationoptionTagEntity, AlterationoptionTagCollection>(DataSourceAsAlterationoptionEntity, selectedTags);
        }

        private void BindTagBox()
        { 
            tagBox.SetSelectedTags(DataSourceAsAlterationoptionEntity.AlterationoptionTagCollection);

            if (DataSourceAsAlterationoptionEntity.ProductId.HasValue)
            {
                var product = DataSourceAsAlterationoptionEntity.ProductEntity;
                var productTags = product.ProductTagCollection;

                if (productTags.Any())
                {
                    plhInherittedTags.Visible = true;
                    
                    tagBox.SetInheritedTags(productTags);
                    TagOverview.AddTagGroup(string.Empty, productTags);
                }
            }
        }

        private void BindAlterationTypeDropdown()
        {
            List<AlterationType> supportedTypes = AlterationoptionHelper.GetSupportedAlterationoptionTypes(CmsSessionHelper.AlterationDialogMode);

            if (this.DataSourceAsAlterationoptionEntity.IsNew)
            {
                foreach (AlterationType type in supportedTypes)
                {
                    this.cbType.Items.Add(new ListEditItem(type.ToString(), (int) type));
                }
            }
            else if (supportedTypes.Any(at => at == this.DataSourceAsAlterationoptionEntity.Type.ToEnum<AlterationType>()))
            {
                foreach (AlterationType type in supportedTypes)
                {
                    this.cbType.Items.Add(new ListEditItem(type.ToString(), (int) type));
                }
            }
            else
            {
                this.cbType.Items.Add(new ListEditItem(
                    this.DataSourceAsAlterationoptionEntity.Type.ToEnum<AlterationType>().ToString(),
                    (int) this.DataSourceAsAlterationoptionEntity.Type));
                this.cbType.Enabled = false;
            }
        }

        private void RenderAlterationoptionFields(AlterationoptionEntity alterationoption)
        {
            tbName.Value = alterationoption.Name;
            tbPriceIn.Value = alterationoption.PriceIn;
            ddlTaxTariffId.Value = alterationoption.TaxTariffId;
            cbIsAlcoholic.Value = alterationoption.IsAlcoholic;

            if (alterationoption.ProductId.HasValue)
            {
                if (alterationoption.InheritName)
                {
                    lblName.Style.Add("font-weight", "bold");

                    tbName.Value = string.Empty;
                    tbName.Placeholder = alterationoption.Name;
                }

                lblIsAlcoholic.Style.Add("font-weight", "bold");
                cbIsAlcoholic.Enabled = false;

                lblTaxTariffId.Style.Add("font-weight", "bold");
                ddlTaxTariffId.Enabled = false;

                if (alterationoption.InheritPrice)
                {
                    lblPriceIn.Style.Add("font-weight", "bold");

                    tbPriceIn.Value = (decimal?)null;
                    tbPriceIn.Placeholder = alterationoption.PriceIn?.ToString("F");
                }
                
                tbName.IsRequired = false;
                tbPriceIn.IsRequired = false;
            }
        }

        private void SetAlterationoptionFields(AlterationoptionEntity alterationoption)
        {
            alterationoption.Name = tbName.Value;
            alterationoption.PriceIn = tbPriceIn.Value;
            alterationoption.TaxTariffId = ddlTaxTariffId.Value;
            alterationoption.IsAlcoholic = cbIsAlcoholic.Value;

            if (alterationoption.ProductId.HasValue)
            {
                alterationoption.InheritName = tbName.Value.IsNullOrWhiteSpace();
                alterationoption.InheritPrice = !tbPriceIn.Value.HasValue;
            }
        }

        private void SetPriceLabel()
        {
            bool pricesIncludeTaxes;

            if (this.DataSourceAsAlterationoptionEntity.IsNew)
            {
                CompanyEntity company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);

                pricesIncludeTaxes = company.PricesIncludeTaxes;
            }
            else
            {
                pricesIncludeTaxes = this.DataSourceAsAlterationoptionEntity.CompanyEntity.PricesIncludeTaxes;
            }

            this.lblPriceIn.Value = pricesIncludeTaxes
                ? "Price (Including tax)"
                : "Price (Excluding tax)";
        }

        private void BindTaxTariffDropdown()
        {
	        PredicateExpression filter = new PredicateExpression(TaxTariffFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
	        SortExpression sort = new SortExpression(new SortClause(TaxTariffFields.Name, SortOperator.Ascending));
            TaxTariffCollection taxTariffCollection = new TaxTariffCollection();

            taxTariffCollection.GetMulti(filter, 0, sort);

	        ddlTaxTariffId.DataSource = taxTariffCollection;
	        ddlTaxTariffId.DataBind();
        }

        private void BindLinkProductDropdown()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.Add(ProductFields.Type == ProductType.Product);
            filter.Add(ProductFields.SubType.In(ProductSubType.Inherit, ProductSubType.Normal, ProductSubType.ServiceItems));

            SortExpression sort = new SortExpression(ProductFields.Name.Ascending());
            IncludeFieldsList includes = new IncludeFieldsList(ProductFields.ProductId, ProductFields.Name);

            ProductCollection collection = new ProductCollection();
            collection.GetMulti(filter, 0, sort, null, null, includes, 0, 0);

            ddlProductId.DisplayEmptyItem = !DataSourceAsAlterationoptionEntity.ProductId.HasValue;
            ddlProductId.DataSource = collection;
            ddlProductId.DataBind();
        }

        private void SetPosAlterationoptionIdFilter()
        {
            PredicateExpression filter = new PredicateExpression(PosalterationoptionFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression sort = new SortExpression(new SortClause(PosalterationoptionFields.Name, SortOperator.Ascending));

            PosalterationoptionCollection posalterationoptionCollection = new PosalterationoptionCollection();
            posalterationoptionCollection.GetMulti(filter, 0, sort);

            foreach (PosalterationoptionEntity posalterationoption in posalterationoptionCollection)
            {
                if (!string.IsNullOrWhiteSpace(posalterationoption.RevenueCenter))
                {
                    this.ddlPosalterationoptionId.Items.Add($"{posalterationoption.Name} ({posalterationoption.ExternalId}) [{posalterationoption.RevenueCenter}]", posalterationoption.PosalterationoptionId);
                }
                else
                {
                    this.ddlPosalterationoptionId.Items.Add($"{posalterationoption.Name} ({posalterationoption.ExternalId})", posalterationoption.PosalterationoptionId);
                }
            }
        }

        private void SetPosProductIdFilter()
        {
            PredicateExpression filter = new PredicateExpression(PosproductFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            SortExpression sort = new SortExpression(new SortClause(PosproductFields.Name, SortOperator.Ascending));

            PosproductCollection posproductCollection = new PosproductCollection();
            posproductCollection.GetMulti(filter, 0, sort);

            foreach (PosproductEntity posproduct in posproductCollection)
            {
                if (!string.IsNullOrWhiteSpace(posproduct.RevenueCenter))
                {
                    this.ddlPosproductId.Items.Add($"{posproduct.Name} ({posproduct.ExternalId}) [{posproduct.RevenueCenter}]", posproduct.PosproductId);
                }
                else
                {
                    this.ddlPosproductId.Items.Add($"{posproduct.Name} ({posproduct.ExternalId})", posproduct.PosproductId);
                }
            }

        }

        private void BindExternalProductDropdown()
        {
            ExternalProductDataSource dataSource = new ExternalProductDataSource();
            ddlExternalProductId.DataSource = dataSource.GetDropdownDataSource(DataSourceAsAlterationoptionEntity.CompanyId.GetValueOrDefault(CmsSessionHelper.CurrentCompanyId), ExternalProductType.Modifier);
            ddlExternalProductId.ValueField = nameof(ExternalProductFields.ExternalProductId);
            ddlExternalProductId.TextField = nameof(ExternalProductEntity.CombinedNameWithMenuAndSystemName);
            ddlExternalProductId.DataBind();
        }

        private void SetVersion()
        {
            if (QueryStringHelper.HasValue<int>("version"))
            {
                this.DataSourceAsAlterationoptionEntity.Version = QueryStringHelper.GetInt("version");
            }

            if (this.Master is MasterPageEntity masterPage)
            {
                string version = this.DataSourceAsAlterationoptionEntity.Version == 2 ? "V3" : "V1 & V2";
                masterPage.SetPageTitle(string.Format("Alteration options {0} - {1}", version, this.DataSourceAsAlterationoptionEntity.Name));
            }
        }

        private void SetWarnings()
        {
            if (!DataSourceAsAlterationoptionEntity.BrandId.HasValue)
            {
                if ((CompanyHelper.GetPOSConnectorType(CmsSessionHelper.CurrentCompanyId, null) != POSConnectorType.Unknown)
                    && !DataSourceAsAlterationoptionEntity.PosalterationoptionId.HasValue
                    && !DataSourceAsAlterationoptionEntity.PosproductId.HasValue)
                {
                    AddInformator(Dionysos.Web.UI.InformatorType.Warning, "The item is not linked to the PoS and therefore will not be visible to guests.");
                }
            }

            Validate();
        }
        
	    public override bool Save()
	    {
	        if (PageMode == PageMode.Add)
	        {
	            DataSourceAsAlterationoptionEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            DataSourceAsAlterationoptionEntity.StartTimeUTC = DataSourceAsAlterationoptionEntity.StartTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
            DataSourceAsAlterationoptionEntity.EndTimeUTC = DataSourceAsAlterationoptionEntity.EndTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);

            SetAlterationoptionFields(DataSourceAsAlterationoptionEntity);

            if (tagBox.IsDirty)
            {
                try
                {
                    TagHelper.UpdateTagsForEntity<AlterationoptionEntity, AlterationoptionTagEntity, AlterationoptionTagCollection>(DataSourceAsAlterationoptionEntity, tagBox.GetSelectedTags());
                }
                catch (EntitySaveException ex)
                {
                    this.MultiValidatorDefault.AddError(ex.Message);
                    this.Validate();
                    return false;
                }
            }

            if (!DataSourceAsAlterationoptionEntity.InheritTags && DataSourceAsAlterationoptionEntity.IsChanged(AlterationoptionFields.InheritTags))
            {
                RemoveInheritedTags();
            }

            if (ddlProductId.Value.HasValue)
            {
                translationsTab.DisableCustomTextTypes(CustomTextType.AlterationoptionName);
            }

            if (base.Save())
            {
                UpdateCustomTexts();

                return true;
            }

            return false;           
        }

        private void UpdateCustomTexts()
        {
            if (DataSourceAsAlterationoptionEntity.BrandId.HasValue)
            {
                CustomTextHelper.UpdateCustomTexts(DataSource, DataSourceAsAlterationoptionEntity.BrandEntity.CultureCode);
            }
            else
            {
                CustomTextHelper.UpdateCustomTexts(DataSource, DataSourceAsAlterationoptionEntity.CompanyEntity.CultureCode);
            }
        }

        #endregion

        #region Properties


        /// <summary>
        /// Return the page's datasource as a Entity
        /// </summary>
        public AlterationoptionEntity DataSourceAsAlterationoptionEntity => this.DataSource as AlterationoptionEntity;

        #endregion        
    }
}
