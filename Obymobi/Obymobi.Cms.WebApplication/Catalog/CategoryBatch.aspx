﻿<%@ Page Title="Add category batch" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.CategoryBatch" ValidateRequest="false" Codebehind="CategoryBatch.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxTreeList.v20.1" Namespace="DevExpress.Web.ASPxTreeList" TagPrefix="dxwtl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<controls>
					<table class="dataformV2">
						<tr>
							<td class="label">
					            <D:Label runat="server" id="lblMenu">Menu</D:Label>
				            </td>
				            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlMenuId" IncrementalFilteringMode="StartsWith" TextField="Name" ValueField="MenuId" PreventEntityCollectionInitialization="true" AutoPostBack="true" />
                            </td>
							<td class="label">
                                 <D:Label runat="server" id="lblCategory">Category</D:Label>
							</td>
							<td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlCategoryId" IncrementalFilteringMode="StartsWith" TextField="FullCategoryName" ValueField="CategoryId" PreventEntityCollectionInitialization="true" AutoPostback="true" />
							</td>
                        </tr>
                        <tr>
                            <td class="label">
							</td>
							<td colspan="3">
							     <X:TreeList ID="tlCategories" ClientInstanceName="treelist" runat="server" AutoGenerateColumns="False" Width="100%" >            
                                    <settingsbehavior autoexpandallnodes="True" />
                                    <Settings GridLines="Horizontal" />
                                     <Images>
                                         <ExpandedButton Height="0px" />
                                         <CollapsedButton Height="0px" Width="0px" />
                                     </Images>
                                    <Columns>                    
                                        <dxwtl:TreeListDataColumn caption="Naam" fieldname="Name" visibleindex="0" CellStyle-HorizontalAlign="Left">
                                        </dxwtl:TreeListDataColumn>
                                        <dxwtl:TreeListDataColumn caption="SortOrder" fieldname="SortOrder" visibleindex="0" CellStyle-HorizontalAlign="Center">
                                        </dxwtl:TreeListDataColumn>
                                        <dxwtl:TreeListDataColumn caption="Type" fieldname="TypeName" visibleindex="1" CellStyle-HorizontalAlign="Right">
                                        </dxwtl:TreeListDataColumn>            
                                    </Columns>
                                </X:TreeList>
						    </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblBatchFile">Batch file</D:Label>
                            </td>
                            <td class="control">
                                <D:FileUpload runat="server" ID="fuDocument" />
                            </td>
                        </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblBatchText">Batch text</D:Label>
							</td>
							<td class="control threeCol" colspan="3">
							    <D:TextBoxMultiLine ID="tbBatch" runat="server" Rows="40"></D:TextBoxMultiLine>
						    </td>
                        </tr>
                        <tr>
                            <td class="label">
							</td>
							<td class="control">
                                <D:Button ID="btCreate" runat="server" Text="Create" ClientIDMode="static" Style="float: left" />&nbsp
                                <D:Button ID="btPreview" runat="server" Text="Preview" ClientIDMode="static" Style="float: left; margin-left:2px;" />
							</td>
                            <td class="label">
							</td>
							<td class="control">
							</td>
                        </tr>
                    </table>
                </controls>
            </X:TabPage>            
            <X:TabPage Text="Progress" Name="Progress">
	            <controls>
		            <table class="dataformV2">
			            <tr>
                            <td class="control" colspan="4">
                                <strong><D:Label ID="lblProgress" runat="server" Text="Progress"></D:Label></strong>
                            </td>
                        </tr>
                        <tr>
                            <td class="control" colspan="4">
                                <D:TextBoxMultiLine ID="tbProgress" runat="server" Rows="40" ClientIDMode="Static" ReadOnly="true"></D:TextBoxMultiLine>                                
                            </td>
                        </tr>
                    </table>
                </controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>    
</div>
 <script type="text/javascript">
    var progressVisible = false;

    InitProgress();

    function InitProgress() {
        PageMethods.SessionExists(function (exists) {
            progressVisible = exists;
        });

        UpdateProgress();
    }

    function UpdateProgress() {
        PageMethods.SessionExists(function (exists) {
            if (exists) {
                progressVisible = true;
            } else {
                progressVisible = false;
            }

            if (progressVisible) {
                document.getElementById("btCreate").style.display = 'none';
                document.getElementById("btPreview").style.display = 'none';

                PageMethods.GetLog(function (log) {
                    document.getElementById("tbProgress").innerHTML = log;
                    document.getElementById("tbProgress").scrollTop = 99999;
                });
            }
            else {
                document.getElementById("btCreate").style.display = 'block';
                document.getElementById("btPreview").style.display = 'block';
            }
        });

        setTimeout(function () { UpdateProgress(); }, 500);
    }
</script>
</asp:Content>

