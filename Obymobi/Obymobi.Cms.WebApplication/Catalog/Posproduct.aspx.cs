﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data;

namespace Obymobi.ObymobiCms.Catalog
{
    public partial class Posproduct : Dionysos.Web.UI.PageLLBLGenEntity
    {
        #region Methods

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.SetGui();
        }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
                this.DataSourceAsPosproductEntity.CompanyId = CmsSessionHelper.CurrentCompanyId;

            return base.Save();
        }

        private void SetGui()
        { 
            // Render Alteration info
            PosproductPosalterationCollection pppas = new PosproductPosalterationCollection();
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosproductPosalterationFields.PosproductExternalId == this.DataSourceAsPosproductEntity.ExternalId);
            filter.Add(PosproductPosalterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            pppas.GetMulti(filter);

            PosalterationCollection pas = new PosalterationCollection();
            filter = new PredicateExpression();
            filter.Add(PosalterationFields.ExternalId == pppas.Select(x => x.PosalterationExternalId).ToList());
            filter.Add(PosalterationFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            pas.GetMulti(filter);

            PosalterationitemCollection pais = new PosalterationitemCollection();
            filter = new PredicateExpression();
            filter.Add(PosalterationitemFields.ExternalPosalterationId == pppas.Select(x => x.PosalterationExternalId).ToList());
            filter.Add(PosalterationitemFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            pais.GetMulti(filter);

            PosalterationoptionCollection paos = new PosalterationoptionCollection();
            filter = new PredicateExpression();
            filter.Add(PosalterationoptionFields.ExternalId == pais.Select(x => x.ExternalPosalterationoptionId).ToList());
            filter.Add(PosalterationoptionFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            paos.GetMulti(filter);

            ProductAlterationCollection productalterations = new ProductAlterationCollection();
            filter = new PredicateExpression();
            filter.Add(ProductAlterationFields.PosproductPosalterationId == pppas.Select(x => x.PosproductPosalterationId).ToList());
            PrefetchPath path = new PrefetchPath(EntityType.ProductAlterationEntity);
            path.Add(ProductAlterationEntity.PrefetchPathAlterationEntity)
                .SubPath.Add(AlterationEntity.PrefetchPathAlterationitemCollection)
                .SubPath.Add(AlterationitemEntity.PrefetchPathAlterationoptionEntity);
            productalterations.GetMulti(filter, path);

            if (pppas.Count == 0)
            {
                this.plhAlterations.AddHtml("<tr><td></td><td colspan='2'>{0}</td></tr>", this.Translate("NoPosAlterations", "This pos product does not have any pos alterations."));
            }

            foreach (var posproductalteration in pppas)
            { 
                // Check if it's been used
                var productalterationsImported = productalterations.Where(x => x.PosproductPosalterationId == posproductalteration.PosproductPosalterationId);

                // Get the Posalteration
                var posalteration = pas.FirstOrDefault(x => x.ExternalId == posproductalteration.PosalterationExternalId);

                if(posalteration == null)
                {
                    string error = this.Translate("PosalterationMissing", "De Posalteration met ExterneId: '{0}' ontbreekt");
                    this.plhAlterations.AddHtml("<tr><td colspan=\"3\">{0}</td></tr>", error);
                    continue;
                }

                string importedText = "-";
                ProductAlterationEntity productalteration = null;
                if(productalterationsImported != null && productalterationsImported.Count() > 0)
                {
                    productalteration = productalterationsImported.ToList()[0];
                    importedText = string.Format("<a href=\"Alteration.aspx?id={0}\">{1}</a>", productalteration.AlterationId, productalteration.AlterationEntity.Name);
                }

                // Render the Alteration
                this.plhAlterations.AddHtml("<tr><td><strong>{0}</strong></td><td>{1}</td><td>{2}</td></tr>",
                    posalteration.Name, importedText, posalteration.ExternalId); 

                // Render it's options
                // First get the posalteratiomitems
                var posalterationitems = pais.Where(x => x.ExternalPosalterationId == posalteration.ExternalId);

                foreach (var posalterationitem in posalterationitems)
                {
                    // Get the posalterationoptions based on 
                    var posalterationoption = paos.FirstOrDefault(x => x.ExternalId == posalterationitem.ExternalPosalterationoptionId);

                    if (posalterationoption == null)                    
                    {
                        string error = this.Translate("PosalterationoptionMissing", "De Posalterationoption met ExterneId: '{0}' ontbreekt");
                        this.plhAlterations.AddHtml("<tr><td colspan=\"3\">{0}</td></tr>", error);
                        continue;
                    }

                    string alterationOptionImportedText = "-";
                    if (alterationOptionImportedText != null && productalteration!= null)
                    {
                        var alterationItem = productalteration.AlterationEntity.AlterationitemCollection.FirstOrDefault(x => x.PosalterationitemId == posalterationitem.PosalterationitemId);
                        if(alterationItem != null)
                            alterationOptionImportedText = string.Format("<a href=\"Alterationoption.aspx?id={0}\">{1}</a>", alterationItem.AlterationoptionId, alterationItem.AlterationoptionEntity.Name);
                    }

                    // Render the Alterationoption
                    this.plhAlterations.AddHtml("<tr><td>&nbsp;&nbsp;&nbsp;{0}</td><td>{1}</td><td>{2}</td></tr>",
                        posalterationoption.Name, alterationOptionImportedText, posalterationoption.ExternalId); 
                    
                }  
              
                // Empty line
                this.plhAlterations.AddHtml("<tr><td>&nbsp;</td></tr>");
            }

            var products = this.DataSourceAsPosproductEntity.ProductCollection;
            if (products.Count == 0)
            {
                this.phlLinkedProducts.AddHtml("<tr><td>{0}</td></tr>", this.Translate("NoLinkedProducts", "There are no products linked to this pos product.")); 
            }

            foreach (ProductEntity product in products)
            {
                this.phlLinkedProducts.AddHtml("<tr><td><a href='Product.aspx?id={0}'><strong>{1} &raquo;</strong></a></td></tr>",
                    product.ProductId, product.Name); 
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the datasource as a PosproductEntity instance
        /// </summary>
        public PosproductEntity DataSourceAsPosproductEntity
        {
            get
            {
                return this.DataSource as PosproductEntity;
            }
        }

        #endregion
    }
}
