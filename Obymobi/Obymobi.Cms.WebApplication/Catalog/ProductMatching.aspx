﻿<%@ Page Title="Product matching" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Catalog.ProductMatching" Codebehind="ProductMatching.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
		<D:Button runat="server" ID="btMatch" Text="Koppelen" />	
		<D:Button runat="server" ID="btChangeCategory" Text="Verversen" />	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Producten koppelen" Name="ProductMatching">
				<Controls>
					<D:PlaceHolder ID="PlaceHolder1" runat="server">
					<table class="dataformV2">
						<tr>
							<td class="control">
								<D:Label runat="server" id="lblProductCount" LocalizeText="false"></D:Label>
							</td>
							<td class="control">
                                &nbsp;
							</td>							
							<td class="control">
                                &nbsp;
							</td>
							<td class="control">
								&nbsp;
							</td>				
						</tr>
                        <tr>
							<td class="control">
                                <D:CheckBox ID="cbOnlyFood" runat="server" Text="Alleen eten tonen" /><br />
                                <D:CheckBox ID="cbOnlyDrinks" runat="server" Text="Alleen drinken tonen" Checked="true" />
							</td>
							<td class="control">
								<D:CheckBox ID="cbOnlyNoGenericproduct" runat="server" Text="Alleen producten zonder basisproduct" Checked="true" /><br />
                                <D:CheckBox ID="cbSuggestGenericproducts" runat="server" Text="Doe suggesties voor basisproducten" Checked="true" />
							</td>							
							<td class="control">
								<D:CheckBox ID="cbOnlyNoPosproduct" runat="server" Text="Alleen producten zonder kassaproduct" />
							</td>
							<td class="control">
								&nbsp;
							</td>
                        </tr>
						<tr>
							<td class="control">
								<D:Label runat="server" id="lblProductTitle">Menukaart product</D:Label>
							</td>
							<td class="control">
								<D:Label runat="server" id="lblGenericProductTitle">Basis product</D:Label>
							</td>							
							<td class="control">
								<D:Label runat="server" id="lblPosproductTitle">Kassa product</D:Label>
							</td>
							<td class="control">
								&nbsp;
							</td>				
						</tr>	
						<D:PlaceHolder runat="server" ID="plhProducts"></D:PlaceHolder>
					 </table>			
					 </D:PlaceHolder>
				</Controls>
			</X:TabPage>			
		</TabPages>
	</X:PageControl>
</asp:Content>

