﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using DevExpress.Web;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;
using Dionysos;
using Google.Apis.Analytics.v3.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Google;

namespace Obymobi.ObymobiCms.Analytics
{
    public partial class TopLists : Dionysos.Web.UI.PageQueryStringDataBinding
    {
        private enum ReportingType : int
        { 
            PageViews = 1,
            Entertainment = 2,
            Orders = 3,
            BestSellers = 4,
            PopularCategories = 5,
            AdvertisementByAdvertisedItem = 6,
            AdvertisementByAdvertisement = 7,
            MicrositePageViews = 8
        }

        #region Fields
        private WebChartControl chart = null;

        Subpanels.CategoryPanel CategoryPanel;
        Subpanels.DeliverypointPanel DeliverypointPanel;

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            this.ddlReportType.Items.Add(this.Translate("ReportTypePageViews", "Pagina vertoningen"), (int)ReportingType.PageViews);
            this.ddlReportType.Items.Add(this.Translate("ReportTypeEntertainment", "Entertainment gebruik"), (int)ReportingType.Entertainment);
            //this.ddlReportType.Items.Add(this.Translate("ReportTypeOrders", "Bestellingen"), (int)ReportingType.Orders);
            this.ddlReportType.Items.Add(this.Translate("ReportTypeBestSellers", "Bestsellers"), (int)ReportingType.BestSellers);
            this.ddlReportType.Items.Add(this.Translate("ReportTypeCategories", "Populaire Categoriën"), (int)ReportingType.PopularCategories);
            this.ddlReportType.Items.Add(this.Translate("ReportTypeAdvertisementByAdvertisedItem", "Geadverteerd item hits"), (int)ReportingType.AdvertisementByAdvertisedItem);
            this.ddlReportType.Items.Add(this.Translate("ReportTypeAdvertisementByAdvertisement", "Advertentie hits"), (int)ReportingType.AdvertisementByAdvertisement);
            this.ddlReportType.Items.Add(this.Translate("ReportTypeMicrositePageViews", "Microsite hits"), (int)ReportingType.MicrositePageViews);

            base.OnInit(e);

            //this.tabsMain.AddTabPage("Categories", "Categories", "~/Analytics/Subpanels/CategoryPanel.ascx");
            this.CategoryPanel = (Obymobi.ObymobiCms.Analytics.Subpanels.CategoryPanel)this.LoadControl("~/Analytics/Subpanels/CategoryPanel.ascx");
            this.plhCategoryPanel.Controls.Add(this.CategoryPanel);

            this.DeliverypointPanel = (Obymobi.ObymobiCms.Analytics.Subpanels.DeliverypointPanel)this.LoadControl("~/Analytics/Subpanels/DeliverypointPanel.ascx");
            this.plhDeliverypointPanel.Controls.Add(this.DeliverypointPanel);
        }

        protected override void SetDefaultValuesToControls()
        {
            this.ddlReportType.Value = (int)ReportingType.PageViews;

            if (CmsSessionHelper.CurrentCompanyId > 0)
                this.ddlCompanyId.Value = CmsSessionHelper.CurrentCompanyId;

            this.deDateFrom.Value = DateTimeUtil.MakeBeginOfDay(DateTime.Now.AddMonths(-1));
            this.deDateTo.Value = DateTimeUtil.MakeBeginOfDay(DateTime.Now);
        }                

        public WebChartControl getPieChart(string name, int width, int height, KeyValuePair<string, double>[] points)
        {
            // Prepare the basics
            chart = new WebChartControl();
            chart.EnableViewState = false;
            chart.SaveStateOnCallbacks = false;
            chart.Legend.Visible = true;
            chart.Legend.AlignmentVertical = LegendAlignmentVertical.Center;
            chart.Legend.AlignmentHorizontal = LegendAlignmentHorizontal.Right;
            chart.BackColor = System.Drawing.Color.Transparent;            
            chart.Width = width;
            chart.Height = height;            
            chart.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False;
            ChartTitle title = new ChartTitle();
            title.Text = name;
            chart.Titles.Add(title);

            Series series = new Series(name, ViewType.Pie);

            foreach (var point in points)
            {
                SeriesPoint newPoint = new SeriesPoint();
                newPoint.Values = new double[1] { point.Value };
                newPoint.Argument = point.Key;
                series.Points.Add(newPoint);
            }

            PieSeriesView view = new PieSeriesView();
            view.Rotation = 90;
            view.RuntimeExploding = false;
            series.View = view;

            PiePointOptions legendPointOptions = new PiePointOptions();
            legendPointOptions.PointView = PointView.ArgumentAndValues;
            legendPointOptions.ValueNumericOptions.Format = NumericFormat.Percent;
            legendPointOptions.ValueNumericOptions.Precision = 0;
            series.LegendPointOptions = legendPointOptions;

            series.Label.PointOptions.ValueNumericOptions.Format = NumericFormat.Percent;
            series.Label.PointOptions.ValueNumericOptions.Precision = 0;

            chart.CustomDrawSeriesPoint += new CustomDrawSeriesPointEventHandler(chart_CustomDrawSeriesPoint);

            chart.Series.Add(series);
            return chart;
        }

        private string GetDeliverypointFilter()
        {
            string filter = string.Empty;
            List<String> selectedDeliverypoints = this.DeliverypointPanel.GetSelectedDeliverypoints();
            foreach (string deliverypoint in selectedDeliverypoints)
            {
                if (filter.Length > 0)
                    filter += ",";

                filter += "ga:dimension2==" + deliverypoint;
            }
            return filter;
        }

        private string GetCategoryFilter(string categoryDimension)
        {
            string filter = string.Empty;
            List<String> selectedCategories = this.CategoryPanel.GetSelectedCategories();
            foreach (string category in selectedCategories)
            {
                if (filter.Length > 0)
                    filter += ",";

                filter += categoryDimension + "=@[" + category + "]";
            }
            return filter;
        }

        private void ShowPageViewsReport()
        {
            // Columns unique to this report
            Dionysos.Collections.OrderedDictionary<string, Type> columns = new Dionysos.Collections.OrderedDictionary<string,Type>();
            columns.Add(this.Translate("ColumnHeadingPage", "Pagina"), typeof(string));
            columns.Add(this.Translate("ColumnHeadingViews", "Vertoningen"), typeof(int));

            string filter = GetDeliverypointFilter();

            var dataTable = this.GetReportData(columns, "ga:screenviews", "ga:screenName", filter, "-ga:screenViews");

            if (dataTable != null)
            {
                this.DataBindGridAndChart(columns, dataTable);
                this.viewsGrid.Visible = true;            
            }
            else
                this.viewsGrid.Visible = false;            
        }
    
        private void ShowEntertainmentReport()
        {
            // Columns unique to this report
            Dionysos.Collections.OrderedDictionary<string, Type> columns = new Dionysos.Collections.OrderedDictionary<string, Type>();
            columns.Add(this.Translate("ColumnHeadingEntertainment", "Entertainment"), typeof(string));
            columns.Add(this.Translate("ColumnHeadingViews", "Vertoningen"), typeof(int));

            string filter = GetDeliverypointFilter();

            var dataTable = this.GetReportData(columns, "ga:metric8", "ga:dimension10", filter, "-ga:metric8");

            if (dataTable != null)
            {
                this.DataBindGridAndChart(columns, dataTable);
                this.viewsGrid.Visible = true;
            }
            else
                this.viewsGrid.Visible = false;            
        }

        private void ShowBestSellersReport()
        {
            // Columns unique to this report
            Dionysos.Collections.OrderedDictionary<string, Type> columns = new Dionysos.Collections.OrderedDictionary<string, Type>();
            columns.Add(this.Translate("ColumnHeadingProduct", "Product"), typeof(string));
            columns.Add(this.Translate("ColumnHeadingAmount", "Aantal"), typeof(int));
            columns.Add(this.Translate("ColumnHeadingRevenue", "Omzet"), typeof(string));

            string filter = GetDeliverypointFilter();
            string categoryFilter = GetCategoryFilter("ga:productCategory");
            if (categoryFilter.Length > 0)
            {
                if (filter.Length > 0)
                    filter += ";";

                filter += categoryFilter;
            }

            var dataTable = this.GetReportData(columns, "ga:itemQuantity,ga:itemRevenue", "ga:productName", filter, "-ga:itemQuantity");

            if (dataTable != null)
            {
                this.DataBindGridAndChart(columns, dataTable);
                this.viewsGrid.Visible = true;
            }
            else
                this.viewsGrid.Visible = false;            
        }

        private void ShowPopularCategoriesReport()
        {
            // Columns unique to this report
            Dionysos.Collections.OrderedDictionary<string, Type> columns = new Dionysos.Collections.OrderedDictionary<string, Type>();
            columns.Add(this.Translate("ColumnHeadingCategory", "Categorie"), typeof(string));
            columns.Add(this.Translate("ColumnHeadingViews", "Vertoningen"), typeof(int));
            columns.Add(this.Translate("ColumnHeadingOrdered", "Besteld"), typeof(int));

            string filter = GetDeliverypointFilter();
            string categoryFilter = GetCategoryFilter("ga:dimension9");
            if (categoryFilter.Length > 0)
            {
                if (filter.Length > 0)
                    filter += ";";

                filter += categoryFilter;
            }

            var dataTable = this.GetReportData(columns, "ga:metric7,ga:metric9", "ga:dimension9", filter, "-ga:metric7,-ga:metric9");

            if (dataTable != null)
            {
                this.DataBindGridAndChart(columns, dataTable);
                this.viewsGrid.Visible = true;
            }
            else
                this.viewsGrid.Visible = false;            
        }

        private void ShowAdvertisementByAdvertisedItemReport()
        {
            // Columns unique to this report
            Dionysos.Collections.OrderedDictionary<string, Type> columns = new Dionysos.Collections.OrderedDictionary<string, Type>();
            columns.Add("AdvertisedItem", typeof(string));
            columns.Add("SlideshowHits", typeof(int));
            columns.Add("HomeHits", typeof(int));
            columns.Add("CategoryHits", typeof(int));
            columns.Add("TotalHits", typeof(int));

            DataTable datatable = new DataTable();
            datatable.Columns.Add(new DataColumn("AdvertisedItem", typeof(string)));
            datatable.Columns.Add(new DataColumn("SlideshowHits", typeof(int)));
            datatable.Columns.Add(new DataColumn("HomeHits", typeof(int)));
            datatable.Columns.Add(new DataColumn("CategoryHits", typeof(int)));
            datatable.Columns.Add(new DataColumn("TotalHits", typeof(int)));

            string filter = GetDeliverypointFilter();
            var advertisementData = this.GetGoogleData("ga:metric12", "ga:dimension12", filter, "-ga:metric12");

            Dictionary<string, AdvertisementHits> advertisementHits = new Dictionary<string, AdvertisementHits>();

            for (int i = 0; i < advertisementData.Rows.Count; i++)
            {
                AdvertisementEvent advertisementEvent = new AdvertisementEvent((string)advertisementData.Rows[i][0]);

                AdvertisementHits hits = null;
                if (advertisementHits.ContainsKey(advertisementEvent.AdvertisedItemFull))
                {
                    hits = advertisementHits[advertisementEvent.AdvertisedItemFull];
                }
                else
                {
                    hits = new AdvertisementHits(advertisementEvent.AdvertisedItemName);
                    advertisementHits[advertisementEvent.AdvertisedItemFull] = hits;
                }
                
                if(advertisementEvent.Location.Equals("Slideshow"))
                {
                    hits.SlideshowHits++;
                }
                else if(advertisementEvent.Location.Equals("Home"))
                {
                    hits.HomeHits++;
                }
                else if(advertisementEvent.Location.Equals("Category"))
                {
                    hits.CategoryHits++;
                }
            }

            List<KeyValuePair<string, AdvertisementHits>> advertisementHitList = advertisementHits.ToList();

            advertisementHitList.Sort((pairA, pairB) =>
            {
                if (pairA.Value.TotalHits < pairB.Value.TotalHits)
                    return 1;
                else
                    return 0;
            });

            foreach (KeyValuePair<string, AdvertisementHits> entry in advertisementHitList)
            {
                DataRow row = datatable.NewRow();
                row[0] = entry.Value.ItemName;
                row[1] = entry.Value.SlideshowHits;
                row[2] = entry.Value.HomeHits;
                row[3] = entry.Value.CategoryHits;
                row[4] = entry.Value.SlideshowHits + entry.Value.HomeHits + entry.Value.CategoryHits;
                datatable.Rows.Add(row);
            }

            if (datatable != null)
            {
                this.viewsGrid.SettingsPager.PageSize = this.tbItemsPerPage.Value.Value;

                this.viewsGrid.Columns.Clear();
                foreach (var column in columns)
                {
                    this.viewsGrid.Columns.Add(new GridViewDataColumn(column.Key));
                }

                // Bind Data to Grid
                this.viewsGrid.DataSource = datatable;
                this.viewsGrid.DataBind();

                this.viewsGrid.Visible = true;
            }
            else
                this.viewsGrid.Visible = false;
        }

        private void ShowAdvertisementByAdvertisementReport()
        {
            // Columns unique to this report
            Dionysos.Collections.OrderedDictionary<string, Type> columns = new Dionysos.Collections.OrderedDictionary<string, Type>();
            columns.Add("Advertisement", typeof(string));
            columns.Add("SlideshowHits", typeof(int));
            columns.Add("HomeHits", typeof(int));
            columns.Add("CategoryHits", typeof(int));
            columns.Add("TotalHits", typeof(int));

            DataTable datatable = new DataTable();
            datatable.Columns.Add(new DataColumn("Advertisement", typeof(string)));
            datatable.Columns.Add(new DataColumn("SlideshowHits", typeof(int)));
            datatable.Columns.Add(new DataColumn("HomeHits", typeof(int)));
            datatable.Columns.Add(new DataColumn("CategoryHits", typeof(int)));
            datatable.Columns.Add(new DataColumn("TotalHits", typeof(int)));

            string filter = GetDeliverypointFilter();
            var advertisementData = this.GetGoogleData("ga:metric12", "ga:dimension12", filter, "-ga:metric12");

            Dictionary<string, AdvertisementHits> advertisementHits = new Dictionary<string, AdvertisementHits>();

            for (int i = 0; i < advertisementData.Rows.Count; i++)
            {
                AdvertisementEvent advertisementEvent = new AdvertisementEvent((string)advertisementData.Rows[i][0]);

                AdvertisementHits hits = null;
                if (advertisementHits.ContainsKey(advertisementEvent.Advertisement))
                {
                    hits = advertisementHits[advertisementEvent.Advertisement];
                }
                else
                {
                    hits = new AdvertisementHits(advertisementEvent.Advertisement);
                    advertisementHits[advertisementEvent.Advertisement] = hits;
                }

                if (advertisementEvent.Location.Equals("Slideshow"))
                {
                    hits.SlideshowHits++;
                }
                else if (advertisementEvent.Location.Equals("Home"))
                {
                    hits.HomeHits++;
                }
                else if (advertisementEvent.Location.Equals("Category"))
                {
                    hits.CategoryHits++;
                }
            }

            List<KeyValuePair<string, AdvertisementHits>> advertisementHitList = advertisementHits.ToList();

            advertisementHitList.Sort((pairA, pairB) =>
            {
                if (pairA.Value.TotalHits < pairB.Value.TotalHits)
                    return 1;
                else
                    return 0;
            });

            foreach (KeyValuePair<string, AdvertisementHits> entry in advertisementHitList)
            {
                DataRow row = datatable.NewRow();
                row[0] = entry.Value.ItemName;
                row[1] = entry.Value.SlideshowHits;
                row[2] = entry.Value.HomeHits;
                row[3] = entry.Value.CategoryHits;
                row[4] = entry.Value.SlideshowHits + entry.Value.HomeHits + entry.Value.CategoryHits;
                datatable.Rows.Add(row);
            }

            if (datatable != null)
            {
                this.viewsGrid.SettingsPager.PageSize = this.tbItemsPerPage.Value.Value;

                this.viewsGrid.Columns.Clear();
                foreach (var column in columns)
                {
                    this.viewsGrid.Columns.Add(new GridViewDataColumn(column.Key));
                }

                // Bind Data to Grid
                this.viewsGrid.DataSource = datatable;
                this.viewsGrid.DataBind();

                this.viewsGrid.Visible = true;
            }
            else
                this.viewsGrid.Visible = false;
        }

        private void ShowMicrositeReport()
        {
            // Columns unique to this report
            Dionysos.Collections.OrderedDictionary<string, Type> columns = new Dionysos.Collections.OrderedDictionary<string, Type>();
            columns.Add("PageTitle", typeof(string));
            columns.Add("PagePath", typeof(string));
            columns.Add("Views", typeof(int));

            DataTable datatable = new DataTable();
            datatable.Columns.Add(new DataColumn("PageTitle", typeof(string)));
            datatable.Columns.Add(new DataColumn("PagePath", typeof(string)));
            datatable.Columns.Add(new DataColumn("Views", typeof(int)));

            var micrositeData = this.GetMicrositesGoogleData("ga:pageviews", "ga:pageTitle,ga:pagePath", string.Empty, "-ga:pageviews");
            if (micrositeData != null && micrositeData.Rows != null)
            {
                List<MicrositeHits> micrositeHitsList = new List<MicrositeHits>();
                for (int i = 0; i < micrositeData.Rows.Count; i++)
                {
                    MicrositeHits micrositeHits = new MicrositeHits();
                    micrositeHits.PageTitle = micrositeData.Rows[i][0];
                    micrositeHits.PagePath = micrositeData.Rows[i][1];
                    micrositeHits.Hits = int.Parse(micrositeData.Rows[i][2]);
                    micrositeHitsList.Add(micrositeHits);
                }

                micrositeHitsList.Sort((pairA, pairB) => pairB.Hits.CompareTo(pairA.Hits));

                // Prepare the pie chart value container
                KeyValuePair<string, double>[] pieValues;
                double otherValue = 0;
                pieValues = new KeyValuePair<string, double>[System.Math.Min(this.tbItemsOnChart.Value.Value + 1, micrositeHitsList.Count)];

                for(int i = 0; i < micrositeHitsList.Count; i++)
                {
                    DataRow row = datatable.NewRow();
                    row[0] = micrositeHitsList[i].PageTitle;
                    row[1] = micrositeHitsList[i].PagePath;
                    row[2] = micrositeHitsList[i].Hits;
                    datatable.Rows.Add(row);

                    if (i < this.tbItemsOnChart.Value.Value)
                        pieValues[i] = new KeyValuePair<string, double>(row[0].ToString(), micrositeHitsList[i].Hits);
                    else
                        otherValue += micrositeHitsList[i].Hits;
                }

                if (datatable != null)
                {
                    this.viewsGrid.SettingsPager.PageSize = this.tbItemsPerPage.Value.Value;

                    this.viewsGrid.Columns.Clear();
                    foreach (var column in columns)
                    {
                        this.viewsGrid.Columns.Add(new GridViewDataColumn(column.Key));
                    }

                    // Bind Data to Grid
                    this.viewsGrid.DataSource = datatable;
                    this.viewsGrid.DataBind();

                    this.viewsGrid.Visible = true;

                    // Create the pie chart

                    // Add the 'Other value' of the accumlated items 10+
                    if (otherValue > 0)
                        pieValues[this.tbItemsOnChart.Value.Value] = new KeyValuePair<string, double>("Other", otherValue);

                    this.chart = getPieChart("Microsites", 1000, 500, pieValues);
                    this.plhOverview.Controls.Add(chart); 
                }
                else
                    this.viewsGrid.Visible = false;        
            }
            else
                this.viewsGrid.Visible = false;
        }

        private DataTable GetReportData(Dionysos.Collections.OrderedDictionary<string, Type> columnNames, string metrics, string dimension, string filter, string sorter)
        {
            // Retrieve Data
            var googleData = this.GetGoogleData(metrics, dimension, filter, sorter);

            // Return when we have no results
            if (googleData == null || googleData.TotalResults == 0)
            {
                this.AddInformatorInfo(this.Translate("WithinThePeriodAndFiltersThereAreNoResults", "Binnen door u gekozen periode en/of filters zijn geen gegevens gevonden"));                
                return null;
            }

            // Create Datatable & Chart Data            
            DataTable dt = new DataTable();

            // Create Columns in DataTable                      
            foreach (var column in columnNames)
            {
                var dataColumn = dt.Columns.Add(column.Key);
                dataColumn.DataType = column.Value;
            }            

            // Prepare a DataTable for the DataGrid & Set of PieValues for the Chart            
            for (int i = 0; i < googleData.TotalResults; i++)
            {
                // Add row to data tablse
                DataRow row = dt.NewRow();

                // The first data is always 'special'
                string valuePrettiedUp;
                if (googleData.Rows[i][0].IndexOf("]") > 0)
                    valuePrettiedUp = googleData.Rows[i][0].GetAllAfterFirstOccurenceOf("]").Trim();
                else
                    valuePrettiedUp = googleData.Rows[i][0];
                row[0] = valuePrettiedUp;

                // Copy all other values after the first from Google Data to DataTable
                for (int j = 1; j < columnNames.Count; j++)
                {
                    // Always try to parse it to an int, for sorting in the grid
                    int value;
                    double doubleValue;
                    if (int.TryParse(googleData.Rows[i][j], out value))
                        row[j] = value;
                    else if (double.TryParse(googleData.Rows[i][j], out doubleValue))
                        row[j] = string.Format("{0:0.00}", doubleValue);
                    else
                        row[j] = googleData.Rows[i][j];
                }

                dt.Rows.Add(row);
            }

            return dt;
        }

        private void DataBindGridAndChart(Dionysos.Collections.OrderedDictionary<string, Type> columns, DataTable dt)
        {
            KeyValuePair<string, double>[] pieValues;
            // Create the PieValues (first column as identifier & second column as value)
            double otherValue = 0;
            pieValues = new KeyValuePair<string, double>[System.Math.Min(this.tbItemsOnChart.Value.Value + 1, dt.Rows.Count)];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                double value = double.Parse(row[1].ToString());
                // Add Pie values
                if (i < this.tbItemsOnChart.Value.Value)
                    pieValues[i] = new KeyValuePair<string, double>(row[0].ToString(), value);
                else
                    otherValue += value;
            }

            // Add the 'Other value' of the accumlated items 10+
            if (otherValue > 0)
                pieValues[this.tbItemsOnChart.Value.Value] = new KeyValuePair<string, double>("Other", otherValue);

            // Set up the colums of the Gridview
            this.viewsGrid.Columns.Clear();
            foreach (var column in columns)
            {
                this.viewsGrid.Columns.Add(new GridViewDataColumn(column.Key));
            }

            this.viewsGrid.SettingsPager.PageSize = this.tbItemsPerPage.Value.Value;

            // Bind Data to Grid
            this.viewsGrid.DataSource = dt;
            this.viewsGrid.DataBind();

            // Prepare to create the Chart
            string name = this.Translate("AllDeliverypoints", "Alle tafels / kamers");
            if (this.DeliverypointPanel.GetSelectedDeliverypoints().Count > 0)
            {
                name = this.Translate("SelectedDeliverypoints", "Selected deliverypoints");
            }

            this.chart = getPieChart(name, 1000, 500, pieValues);
            this.plhOverview.Controls.Add(chart);            
        }

        private Google.Apis.Manual.Analytics.v3.Data.GaData GetGoogleData(string metrics, string dimensions, string additionalFilter, string sorter)
        {
            StringBuilder builder = new StringBuilder();
            Google.Apis.Manual.Analytics.v3.Data.GaData toReturn = null;
            CompanyEntity company = new CompanyEntity(this.ddlCompanyId.ValidId);

            string cacheKey = string.Format("Views.aspx.cs.{0}.{1}.{2}.{3}.{4}.{5}.{6}", metrics, dimensions, additionalFilter, sorter, 
                this.ddlCompanyId.ValidId, this.deDateFrom.Value.Value, this.deDateTo.Value.Value);

            // Cache for speed             
            if (!company.IsNew)
            {
                if (Dionysos.Web.CacheHelper.TryGetValue(cacheKey, false, out toReturn))
                { 
                    // From cache
                }
                else if (company.GoogleAnalyticsId.Length > 0)
                {
                    GoogleAPI google = new GoogleAPI();
                    if (google.SetProfile(company.GoogleAnalyticsId))
                    {
                        DateTime dateFrom = this.deDateFrom.Value.Value;
                        DateTime dateTo = this.deDateTo.Value.Value;

                        toReturn = google.Get(metrics, dimensions, additionalFilter, sorter, dateFrom, dateTo);
                        Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, cacheKey, toReturn, 30);
                    }
                    else
                    {
                        string message = this.Translate("GoogleAnalyticsProfileCouldNotBeOpened", "Google Analytics profile '{0}' could not be opened.");
                        this.MultiValidatorDefault.AddError(string.Format(message, company.GoogleAnalyticsId));
                    }
                }
                else
                {
                    this.MultiValidatorDefault.AddError(this.Translate("NoGoogleAnalyticsIdConfigured", "Er is geen Google Analytics ID ingesteld voor dit bedrijf."));
                }
            }
            else
            {
                this.MultiValidatorDefault.AddError(this.Translate("NoCompanySelected", "Er is geen bedrijf gekozen."));
            }

            return toReturn;
        }

        private GaData GetMicrositesGoogleData(string metrics, string dimensions, string additionalFilter, string sorter)
        {
            StringBuilder builder = new StringBuilder();
            GaData toReturn = null;
            CompanyEntity company = new CompanyEntity(this.ddlCompanyId.ValidId);

            string cacheKey = string.Format("Views.aspx.cs.{0}.{1}.{2}.{3}.{4}.{5}.{6}", metrics, dimensions, additionalFilter, sorter,
                this.ddlCompanyId.ValidId, this.deDateFrom.Value.Value, this.deDateTo.Value.Value);

            // Cache for speed             
            if (!company.IsNew)
            {
                /*if (Dionysos.Web.CacheHelper.TryGetValue(cacheKey, false, out toReturn))
                {
                    // From cache
                }
                else
                {*/
                    //PredicateExpression filter = new PredicateExpression();
                    //filter.Add(CompanyAnalyticsprofileFields.CompanyId == this.ddlCompanyId.ValidId);

                    //List<int> selectedProfiles = this.CompanyAnalyticsprofilePanel.GetSelectedProfiles();
                    //if (selectedProfiles.Count > 0)
                    //{
                    //    filter.Add(new FieldCompareRangePredicate(CompanyAnalyticsprofileFields.CompanyAnalyticsprofileId, selectedProfiles));
                    //}
                    //else
                    //{
                    //    filter.Add(CompanyAnalyticsprofileFields.Type == (int)AnalyticsprofileType.Microsite);
                    //}

                    //CompanyAnalyticsprofileCollection micrositeProfiles = new CompanyAnalyticsprofileCollection();
                    //micrositeProfiles.GetMulti(filter);

                    //if(micrositeProfiles.Count > 0)
                    //{
                    //    foreach(CompanyAnalyticsprofileEntity micrositeProfile in micrositeProfiles)
                    //    {
                    //        GoogleAPI google = new GoogleAPI();
                    //        if (google.SetProfile(micrositeProfile.GoogleAnalyticsId))
                    //        {
                    //            DateTime dateFrom = this.deDateFrom.Value.Value;
                    //            DateTime dateTo = this.deDateTo.Value.Value;

                    //            if(toReturn == null || toReturn.Rows == null)
                    //            {
                    //                toReturn = google.Get(metrics, dimensions, additionalFilter, sorter, dateFrom, dateTo);
                    //            }
                    //            else
                    //            {
                    //                GaData data = google.Get(metrics, dimensions, additionalFilter, sorter, dateFrom, dateTo);
                    //                for (int i = 0; i < data.Rows.Count; i++)
                    //                    toReturn.Rows.Add(data.Rows[i]);
                    //            }
                    //        }
                    //        else
                    //        {
                    //            string message = this.Translate("GoogleAnalyticsProfileCouldNotBeOpened", "Google Analytics profile '{0}' could not be opened.");
                    //            this.MultiValidatorDefault.AddError(string.Format(message, company.GoogleAnalyticsId));
                    //        }
                    //    }
                    //    if(toReturn != null)
                    //        Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, cacheKey, toReturn, 30);
                    //}
                    //else
                    //{
                    //    string message = this.Translate("NoMicrositeGoogleAnalyticsIdsConfigured", "This company does not have any microsite type Analytics profiles");
                    //    this.MultiValidatorDefault.AddError(message);
                    //}
               // }
            }
            else
            {
                this.MultiValidatorDefault.AddError(this.Translate("NoCompanySelected", "Er is geen bedrijf gekozen."));
            }

            return toReturn;
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnRefresh.Click += btnRefresh_Click;

            // GK Removed, reasons this commented method btnExport_Click
            // this.btnExport.Click += new EventHandler(btnExport_Click);

            this.Validate();
            if (!this.IsValid)
            {
                this.plhOverview.Visible = false;
                this.viewsGrid.Visible = false;
            }
            else if (this.ddlCompanyId.Value.HasValue && this.ddlReportType.ValidId > 0)
            {
                var reportType = this.ddlReportType.ValidId.ToEnum<ReportingType>();

                switch (reportType)
                {
                    case ReportingType.PageViews:
                        this.ShowPageViewsReport();
                        break;
                    case ReportingType.Entertainment:
                        this.ShowEntertainmentReport();
                        break;
                    case ReportingType.BestSellers:
                        this.ShowBestSellersReport();
                        break;
                    case ReportingType.PopularCategories:
                        this.ShowPopularCategoriesReport();
                        break;
                    case ReportingType.AdvertisementByAdvertisedItem:
                        this.ShowAdvertisementByAdvertisedItemReport();
                        break;
                    case ReportingType.AdvertisementByAdvertisement:
                        this.ShowAdvertisementByAdvertisementReport();
                        break;
                    case ReportingType.MicrositePageViews:
                        this.ShowMicrositeReport();
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }

           
        }

        void btnRefresh_Click(object sender, EventArgs e)
        {
            //this.RefreshWithControlValues();
            this.tabsMain.ActiveTabPage = this.tabsMain.GetTabPageByName("Report");
            
        }

        // GK Removed, worked like crap. If suggest: Go to Google Analytics for reports
        // and if the feature gets requested again we should redo it using SpreadsheetLight
        // that can also use Charts.
        //
        //void btnExport_Click(object sender, EventArgs e)
        //{
        //    PrintingSystem ps = new PrintingSystem();

        //    ASPxGridViewExporter gridExporter = new ASPxGridViewExporter();
        //    gridExporter.GridViewID = this.viewsGrid.ID;
        //    Form.Controls.Add(gridExporter);

        //    //PrintableComponentLink link1 = new PrintableComponentLink();
        //    //link1.Component = ((IChartContainer)this.chart).Chart;
        //    //link1.PrintingSystem = ps;

        //    PrintableComponentLink link2 = new PrintableComponentLink();
        //    link2.Component = gridExporter;
        //    link2.PrintingSystem = ps;

        //    CompositeLink compositeLink = new CompositeLink();
        //    compositeLink.Links.AddRange(new object[] { link2 });
        //    compositeLink.PrintingSystem = ps;

        //    compositeLink.CreateDocument();
        //    compositeLink.PrintingSystem.ExportOptions.Pdf.DocumentOptions.Author = "Crave";
        //    using (MemoryStream stream = new MemoryStream())
        //    {
        //        compositeLink.PrintingSystem.ExportToXls(stream);
        //        Response.Clear();
        //        Response.Buffer = false;
        //        Response.AppendHeader("Content-Type", "application/vnd.ms-excel");
        //        Response.AppendHeader("Content-Transfer-Encoding", "binary");
        //        Response.AppendHeader("Content-Disposition", "attachment; filename=test.xls");
        //        Response.BinaryWrite(stream.GetBuffer());
        //        Response.End();
        //    }

        //    ps.Dispose();
        //}

        void chart_CustomDrawSeriesPoint(object sender, CustomDrawSeriesPointEventArgs e)
        {
            e.LegendText = e.SeriesPoint.Argument;// +e.SeriesPoint.Values[0];
        }

        class AdvertisementEvent
        {
            public AdvertisementEvent(String data)
            {
                Location = string.Empty;
                Advertisement = string.Empty;
                AdvertisedItemFull = string.Empty;
                AdvertisedItemId = 0;
                AdvertisedItemName = string.Empty;

                Regex adEventRegex = new Regex(@"\[[^\[\]]*\]\[[^\[\]]*\]\s.+");
                if (adEventRegex.IsMatch(data))
                {
                    int locationStart = 1;
                    int locationEnd = data.IndexOf(']');
                    int advertisementStart = locationEnd + 2;
                    int advertisementEnd = data.IndexOf(']', advertisementStart);
                    int itemStart = advertisementEnd + 2;

                    Location = data.Substring(locationStart, locationEnd - locationStart);
                    Advertisement = data.Substring(advertisementStart, advertisementEnd - advertisementStart);
                    AdvertisedItemFull = data.Substring(itemStart);
                }

                Regex itemIdRegex = new Regex(@"\[.*\]\s.+");
                if (itemIdRegex.IsMatch(AdvertisedItemFull))
                {
                    int idStart = 1;
                    int idEnd = AdvertisedItemFull.IndexOf(']');
                    int nameStart = idEnd + 2;

                    try
                    {
                        AdvertisedItemId = int.Parse(AdvertisedItemFull.Substring(idStart, idEnd - idStart));
                        AdvertisedItemName = AdvertisedItemFull.Substring(nameStart);
                    }
                    catch
                    {
                        AdvertisedItemName = AdvertisedItemFull;
                    }
                }
                else
                {
                    AdvertisedItemName = AdvertisedItemFull;
                }
            }

            public String Location;
            public String Advertisement;
            public String AdvertisedItemFull;
            public int AdvertisedItemId;
            public String AdvertisedItemName;
        }

        class AdvertisementHits
        {
            public string ItemName;
            public int SlideshowHits;
            public int HomeHits;
            public int CategoryHits;
            public int TotalHits
            {
                get { return this.SlideshowHits + this.HomeHits + this.CategoryHits; }
            }

            public AdvertisementHits(string itemName)
            {
                this.ItemName = itemName;
                this.SlideshowHits = 0;
                this.HomeHits = 0;
                this.CategoryHits = 0;
            }
        }

        class MicrositeHits
        {
            public string PageTitle;
            public string PagePath;
            public int Hits;

            public MicrositeHits()
            {
                this.PageTitle = string.Empty;
                this.PagePath = string.Empty;
                this.Hits = 0;
            }
        }

        #endregion
    }
}