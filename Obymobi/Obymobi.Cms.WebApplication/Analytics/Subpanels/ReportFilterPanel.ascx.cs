﻿using System.Data.SqlTypes;
using DevExpress.Web;
using Dionysos;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using System;
using Obymobi.Logic.Analytics;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using System.Collections.Generic;
using System.Linq;
using Dionysos.Data.LLBLGen;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.Analytics.Requests;

namespace Obymobi.ObymobiCms.Analytics.Subpanels
{
    public partial class ReportFilterPanel : Dionysos.Web.UI.UserControlDefault
    {
        private CategoryPanel CategoryPanel = null;

        private SortOrder currentSortOrder = SortOrder.DeliverypointgroupAsc;
        private bool isRefresh = false;

        public enum ReportingPeriod
        {
            Last7days,
            Last14days,
            Last28days,
            Last30days,
            Last60days,
            Last90days,
            Last180days,
            Last365days,
            CurrentMonth,
            CurrentQuarter,
            CurrentYear,
            LastMonth,
            LastQuarter,
            LastYear,
            Manual
        }

        private enum SortOrder
        {
            DeliverypointgroupAsc,
            DeliverypointAsc
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.InitGui();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnLoadDeliverypoints.Click += this.btnLoadDeliverypoints_Click;
            this.btnOrderByDeliverypointgroup.Click += this.btnOrderByDeliverypointgroup_Click;
            this.btnOrderByDeliverypoint.Click += this.btnOrderByDeliverypoint_Click;
        }

        private void btnOrderByDeliverypointgroup_Click(object sender, EventArgs e)
        {
            this.LoadDeliverypoints(SortOrder.DeliverypointgroupAsc);
        }

        private void btnOrderByDeliverypoint_Click(object sender, EventArgs e)
        {
            this.LoadDeliverypoints(SortOrder.DeliverypointAsc);
        }

        private void btnLoadDeliverypoints_Click(object sender, EventArgs e)
        {
            this.isRefresh = true;
            this.LoadDeliverypoints(SortOrder.DeliverypointgroupAsc);
            //this.lbDeliverypoints.SelectAll();
        }

        private void LoadDeliverypoints(SortOrder sortOrder)
        {
            this.currentSortOrder = sortOrder;

            List<DeliverypointEntity> deliverypoints = this.GetSortedDeliverypoints(sortOrder);

            this.lbDeliverypoints.DataSource = deliverypoints;
            this.lbDeliverypoints.DataBind();
        }

        private List<DeliverypointEntity> GetSortedDeliverypoints(SortOrder sortOrder)
        {
            DeliverypointCollection deliverypoints = this.LoadDeliverypoints(this.lbDeliverypointgroups.SelectedItemsValuesAsIntList);

            List<DeliverypointEntity> orderedDeliverypoints = new List<DeliverypointEntity>();
            switch (sortOrder)
            {
                case SortOrder.DeliverypointgroupAsc:
                    orderedDeliverypoints = deliverypoints.OrderBy(d => d.DeliverypointgroupEntity.Name).ToList();
                    break;
                case SortOrder.DeliverypointAsc:
                    orderedDeliverypoints = deliverypoints.OrderBy(d => int.Parse(d.Number)).ToList();
                    break;
            }
            return orderedDeliverypoints;
        }

        #region Properties

        public int CompanyId
        {
            get { return CmsSessionHelper.CurrentCompanyId; }
        }

        // Due to timezoning - use GetPeriod
        public DateTime? DateFrom
        {
            // get { return this.deDateFrom.Value; }
            set { this.deDateFrom.Value = value; }
        }

        public DateTime? DateUntil
        {
            // get { return this.deDateUntil.Value; }
            set { this.deDateUntil.Value = value; }
        }

        public ReportingPeriod Period
        {
            get { return (ReportingPeriod)this.cbPeriod.Value; }
            set
            {
                this.cbPeriod.Value = (int)value;
                this.PeriodRelatedValueChanged(null, null);
            }
        }

        public ReportCategoryFilter CategoriesFilter
        {
            get { return (ReportCategoryFilter)this.cbCategories.Value; }
            set
            {
                this.cbCategories.Value = (int)value;
                this.CategoriesFilterIndexChanged(null, null);
            }
        }

        public bool IncludeInRoomTablets
        {
            get { return this.cbIncludeInRoomTablets.Checked; }
            set { this.cbIncludeInRoomTablets.Checked = value; }
        }

        public bool IncludeByod
        {
            get { return this.cbIncludeByodApps.Checked; }
            set { this.cbIncludeByodApps.Checked = value; }
        }

        public bool IncludeFailedOrders
        {
            get { return this.cbIncludeFailedOrders.Checked; }
            set { this.cbIncludeFailedOrders.Checked = value; }
        }

        public bool IncludeOrderitems
        {
            get { return this.cbIncludeOrderitems.Checked; }
            set { this.cbIncludeOrderitems.Checked = value; } 
        }

        public ListBox Deliverypointgroups
        {
            get { return this.lbDeliverypointgroups; }
        }

        public ListBox Deliverypoints
        {
            get { return this.Deliverypoints; }
        }

        public string TimeZoneOlsonId
        {
            get { return this.ddlTimeZoneOlsonId.SelectedValueString; }
            set { this.ddlTimeZoneOlsonId.Value = value; }
        }

        public bool ShowFilterDevices
        {
            set
            {
                this.phlRoomFilter.Visible = value;
            }
        }

        public bool ShowDeliverypointgroups
        {
            set
            {
                this.plhDeliverypointgroups.Visible = value;
            }
        }

        public bool ShowFilterOrders
        {
            set { this.phlOrders.Visible = value; }
        }

        public bool ShowLoadDeliverypointsButton
        {
            set { this.btnLoadDeliverypoints.Visible = value; }
        }

        public bool ShowFilterDeliverypoints
        {
            set 
            { 
                this.lblDeliverypoints.Visible = value;
                this.lbDeliverypoints.Visible = value;
                this.btnOrderByDeliverypointgroup.Visible = value;
                this.btnOrderByDeliverypoint.Visible = value;
                this.cbExcludeDeliverypoints.Visible = value;
            }
            get { return this.lblDeliverypoints.Visible; }
        }

        public bool ShowFilterCategories
        {
            set
            {
                this.plhCategories.Visible = value;
                this.lblCategories.Visible = value;
                this.cbCategories.Visible = value;
            }
        }

        public bool ShowFilterProduct
        {
            set { this.plhProduct.Visible = value; }
        }

        #endregion

        void InitGui()
        {
            this.CategoryPanel = (CategoryPanel)this.LoadControl("~/Analytics/Subpanels/CategoryPanel.ascx");
            this.plhCategories.Controls.Add(this.CategoryPanel);

            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.Last7days, "Afgelopen 7 dagen", true), (int)ReportingPeriod.Last7days);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.Last14days, "Afgelopen 14 dagen", true), (int)ReportingPeriod.Last14days);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.Last28days, "Afgelopen 28 dagen", true), (int)ReportingPeriod.Last28days);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.Last30days, "Afgelopen 30 dagen", true), (int)ReportingPeriod.Last30days);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.Last60days, "Afgelopen 60 dagen", true), (int)ReportingPeriod.Last60days);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.Last90days, "Afgelopen 90 dagen", true), (int)ReportingPeriod.Last90days);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.Last180days, "Afgelopen 180 dagen", true), (int)ReportingPeriod.Last180days);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.Last365days, "Afgelopen 365 dagen", true), (int)ReportingPeriod.Last365days);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.CurrentMonth, "Huidige maand", true), (int)ReportingPeriod.CurrentMonth);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.CurrentQuarter, "Huidige kwartaal", true), (int)ReportingPeriod.CurrentQuarter);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.CurrentYear, "Huidige jaar", true), (int)ReportingPeriod.CurrentYear);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.LastMonth, "Vorige maand", true), (int)ReportingPeriod.LastMonth);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.LastQuarter, "Vorige kwartaal", true), (int)ReportingPeriod.LastQuarter);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.LastYear, "Vorige jaar", true), (int)ReportingPeriod.LastYear);
            this.cbPeriod.Items.Add(this.Translate("ReportingPeriod." + ReportingPeriod.Manual, "Handmatig", true), (int)ReportingPeriod.Manual);

            this.cbCategories.Items.Add("All categories", (int)ReportCategoryFilter.AllCategories);
            this.cbCategories.Items.Add("Include categories", (int)ReportCategoryFilter.IncludeCategories);
            this.cbCategories.Items.Add("Exclude categories", (int)ReportCategoryFilter.ExcludeCategories);
            this.cbCategories.SelectedIndex = 0;

            IEnumerable<Obymobi.TimeZone> timezones = Obymobi.TimeZone.Mappings.Values.OrderBy(x => x.ToString());

            this.ddlTimeZoneOlsonId.DataSource = timezones;
            this.ddlTimeZoneOlsonId.DataBind();

            CompanyEntity companyEntity = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            this.lblCompanyValue.Text = companyEntity.Name;

            this.ddlTimeZoneOlsonId.SelectedItem = this.ddlTimeZoneOlsonId.Items.FindByValue(companyEntity.TimeZoneOlsonId);

            this.cbOrderType.DataBindEnum<OrderType>();

            this.DataBindProducts();                       

            this.cbPeriod.ValueChanged += this.PeriodRelatedValueChanged;
            this.ddlTimeZoneOlsonId.ValueChanged += this.PeriodRelatedValueChanged;
            this.cbCategories.ValueChanged += this.CategoriesFilterIndexChanged;
        }

        private void DataBindProducts()
        {
            this.cbProductCategoryId.DataSource = ProductHelper.GetProductCategories(CmsSessionHelper.CurrentCompanyId, true, 0);
            this.cbProductCategoryId.DataBind();
        }

        public void UpdateCompanyId()
        {
            int companyId = CmsSessionHelper.CurrentCompanyId;
            if (companyId > 0)
            {
                DeliverypointgroupCollection deliverypointgroups = this.GetDeliverypointgroups(companyId);                

                this.lbDeliverypointgroups.DataSource = deliverypointgroups;
                this.lbDeliverypointgroups.DataBind();                
                this.lbDeliverypointgroups.EnableCheckAll = true;

                this.LoadDeliverypoints(this.currentSortOrder);
                this.lbDeliverypoints.EnableCheckAll = true;
            }
        }

        private DeliverypointgroupCollection GetDeliverypointgroups(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(DeliverypointgroupFields.CompanyId == companyId);

            SortExpression sort = new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending);

            IncludeFieldsList includes = new IncludeFieldsList();
            includes.Add(DeliverypointgroupFields.Name);

            DeliverypointgroupCollection dpgs = new DeliverypointgroupCollection();
            dpgs.GetMulti(filter, 0, sort, null, null, includes, 0, 0);

            return dpgs;
        }

        private DeliverypointCollection LoadDeliverypoints(List<int> selectedDeliverypointgroups = null)
        {
            PredicateExpression dpFilter = new PredicateExpression(DeliverypointFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            RelationCollection dpRelation = new RelationCollection(DeliverypointEntityBase.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, JoinHint.Left);

            if (selectedDeliverypointgroups != null)
            {
                dpFilter.Add(DeliverypointFields.DeliverypointgroupId == selectedDeliverypointgroups);
            }

            SortExpression dpSort = new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending);
            EntityField field = DeliverypointFields.Number;
            field.ExpressionToApply = new DbFunctionCall("CAST({0} as INT)", new object[] { DeliverypointFields.Number });
            dpSort.Add(field | SortOperator.Ascending);

            PrefetchPath dpPrefetch = new PrefetchPath(EntityType.DeliverypointEntity);
            dpPrefetch.Add(DeliverypointEntityBase.PrefetchPathDeliverypointgroupEntity);

            IncludeFieldsList includes = new IncludeFieldsList();
            includes.Add(DeliverypointFields.Name);
            includes.Add(DeliverypointFields.Number);
            includes.Add(DeliverypointgroupFields.Name);

            DeliverypointCollection dps = new DeliverypointCollection();
            dps.GetMulti(dpFilter, 0, dpSort, dpRelation, dpPrefetch, includes, 0, 0);

            return dps;
        }

        public void Validate()
        {
            if (isRefresh)
            {
                isRefresh = false;
                return;
            }

            if (this.cbPeriod.Value == (int)ReportingPeriod.Manual)
            {
                if (!this.deDateFrom.Value.HasValue || !this.deDateUntil.Value.HasValue)
                {
                    Page.MultiValidatorDefault.AddError(this.Translate("AFromAndUntilDateAreRequiredForAManualPeriod", "Er moet een 'Van' en 'Tot en met' datum worden gekozen voor een handmatige periode."));
                }
                else if (this.deDateFrom.Value.Value > this.deDateUntil.Value.Value)
                {
                    Page.MultiValidatorDefault.AddError(this.Translate("UntilDateMustBeEqualToOrLaterThanFromDate", "De 'Tot en met' datum moet gelijk zijn of later dan de 'Van' datum."));
                }
                else if (this.deDateFrom.Value.Value > DateTime.Today)
                {
                    Page.MultiValidatorDefault.AddError(this.Translate("FromDateCanNotBeLaterThanToday", "De 'Van' datum kan niet later zijn dan vandaag."));
                }
                else if (this.deDateUntil.Value.Value > DateTime.Today)
                {
                    Page.MultiValidatorDefault.AddError(this.Translate("UntilDateCanNotBeLaterThanToday", "De 'Tot en met' datum kan niet later zijn dan vandaag."));
                }
            }

            // At least Byod OR Irt
            if (this.phlRoomFilter.Visible && 
                (!this.cbIncludeByodApps.Checked && !this.cbIncludeInRoomTablets.Checked))
            {
                Page.MultiValidatorDefault.AddError(this.Translate("AtLeastOnOfIrtAndByodHasToBeChosen", "Er moet tenminste één van de twee 'In Kamer Tablets' and 'BYOD Apps' worden gekozen."));
            }

            // At least one Deliverypointgroup
            if (this.plhDeliverypointgroups.Visible && this.lbDeliverypointgroups.SelectedItemsCount < 1)
            {
                Page.MultiValidatorDefault.AddError(this.Translate("AtLeastOneDeliverypointgroupMustBeSelected", "Er moet tenminste één kamer/tafelgroep worden gekozen."));
            }

            // At least one Deliverypoint
            if (this.lbDeliverypoints.SelectedItemsCount < 1 && this.ShowFilterDeliverypoints)
            {
                Page.MultiValidatorDefault.AddError(this.Translate("AtLeastOneDeliverypointMustBeSelected", "Er moet tenminste één kamer/tafel worden gekozen."));
            }

            if (this.cbProductCategoryId.ValidId <= 0 && this.plhProduct.Visible)
            {
                Page.MultiValidatorDefault.AddError("Please select a product first.");
            }

            // Validate that all chosen Deliverypoints are part of selected Deliverypointgroups
            if (this.lbDeliverypoints.Visible)
            {
                if (this.lbDeliverypoints.ItemsCount != this.lbDeliverypoints.SelectedItemsCount && this.lbDeliverypoints.SelectedItemsCount >= 1800)
                {
                    Page.MultiValidatorDefault.AddError("Too many deliverypoints seleceted, use the exclude deliverypoint filter instead");
                }

                List<int> selectedDeliverypointgroupIds = this.lbDeliverypointgroups.SelectedItemsValuesAsIntList;
                List<string> missingDeliverypointgroupNames = new List<string>();
                if (this.lbDeliverypointgroups.SelectedItemsCount > 0 && this.lbDeliverypoints.SelectedItemsCount > 0)
                {

                    List<int> deliverypointsList = this.lbDeliverypoints.SelectedItemsValuesAsIntList;
                    List<List<int>> deliverypointsMasterList = deliverypointsList.Select((x, i) => new {Index = i, Value = x})
                                                                                 .GroupBy(x => x.Index / 1000)
                                                                                 .Select(x => x.Select(v => v.Value).ToList())
                                                                                 .ToList();

                    foreach (var deliverypointList in deliverypointsMasterList)
                    {
                        DeliverypointCollection dps = new DeliverypointCollection();
                        var dpgFilter = new PredicateExpression();
                        if (this.cbExcludeDeliverypoints.Checked)
                        {
                            dpgFilter.Add(DeliverypointFields.DeliverypointgroupId != deliverypointList);
                            dpgFilter.Add(DeliverypointFields.CompanyId == this.CompanyId);
                            dpgFilter.Add(DeliverypointFields.DeliverypointgroupId == selectedDeliverypointgroupIds);
                        }
                        else
                        {
                            dpgFilter.Add(DeliverypointFields.DeliverypointgroupId == deliverypointList);
                        }

                        PrefetchPath path = new PrefetchPath(EntityType.DeliverypointEntity);
                        path.Add(DeliverypointEntityBase.PrefetchPathDeliverypointgroupEntity);
                        dps.GetMulti(dpgFilter, path);

                        foreach (DeliverypointEntity dp in dps)
                        {
                            // If it's available, no worries.
                            if (selectedDeliverypointgroupIds.Contains(dp.DeliverypointgroupId)) 
                                continue;

                            // It's not available, add to the list of missing DPGs if not already in it.                    
                            if (!missingDeliverypointgroupNames.Contains(dp.DeliverypointgroupEntity.Name))
                            {
                                missingDeliverypointgroupNames.Add(dp.DeliverypointgroupEntity.Name);
                            }
                        }
                    }
                }

                if (missingDeliverypointgroupNames.Count > 0)
                {
                    this.Page.AddInformator(Dionysos.Web.UI.InformatorType.Information, 
                        this.Translate("DeliverypointsSelectedOfNotSelectedDeliverypointgroups", "Er zijn tafels/kamers gekozen voor tafel/kamergroepen die niet zijn geselecteerd: ") + string.Join(", ", missingDeliverypointgroupNames));                    
                }
            }
        }

        public Filter GetFilter()
        {
            DateTime from, until;
            GetPeriod(out from, out until);
            Filter filter = new Filter(from, until, this.CompanyId);
            filter.TimeZoneOlsonId = this.TimeZoneOlsonId;
            filter.DeliverypointsExclude = this.cbExcludeDeliverypoints.Checked;

            if (this.phlRoomFilter.Visible)
            {
                filter.IncludeByodApps = this.cbIncludeByodApps.Checked;
                filter.IncludeInRoomTablets = this.cbIncludeInRoomTablets.Checked;
            }
            if (this.phlOrders.Visible)
            {
                filter.IncludeFailedOrders = this.cbIncludeFailedOrders.Checked;
            }

            // Check if any of the items are deselected (don't check 0, as that's the 'All/None toggle'
            if (this.lbDeliverypointgroups.ItemsCount != this.lbDeliverypointgroups.SelectedItemsCount)
            {
                // Create a filter
                filter.DeliverypointgroupIds = this.lbDeliverypointgroups.SelectedItemsValuesAsIntList;
            }

            if (this.lbDeliverypoints.Visible)
            {
                if (this.lbDeliverypoints.ItemsCount != this.lbDeliverypoints.SelectedItemsCount)
                {
                    // Create a filter
                    filter.DeliverypointIds = this.lbDeliverypoints.SelectedItemsValuesAsIntList;
                }
            }

            if (this.cbProductCategoryId.ValidId > 0)
            {
                int productId = new ProductCategoryEntity(this.cbProductCategoryId.ValidId).ProductId;
                filter.ProductId = productId;
                filter.ProductIdsFromAlterationProducts = this.GetProductIdsFromAlterationProductsRecursive(productId);
            }

            return filter;
        }

        public void GetPeriod(out DateTime fromUtc, out DateTime untilUtc)
        {
            DateTime now = DateTime.UtcNow;

            // To display the correct period to the user would have needed to know the User's time zone.
            // We don't know the User it's time zone and we also can't use the current Company Time Zone 
            // because users can work with companies that are not their time zones. Therefore we just work
            // based on Utc
            fromUtc = now;
            untilUtc = now; 

            switch (this.Period)
            {
                case ReportingPeriod.Last7days:
                    fromUtc = now.AddDays(-7);
                    untilUtc = now;
                    break;
                case ReportingPeriod.Last14days:
                    fromUtc = now.AddDays(-14);
                    untilUtc = now;
                    break;
                case ReportingPeriod.Last28days:
                    fromUtc = now.AddDays(-28);
                    untilUtc = now;
                    break;
                case ReportingPeriod.Last30days:
                    fromUtc = now.AddDays(-30);
                    untilUtc = now;
                    break;
                case ReportingPeriod.Last60days:
                    fromUtc = now.AddDays(-60);
                    untilUtc = now;
                    break;
                case ReportingPeriod.Last90days:
                    fromUtc = now.AddDays(-90);
                    untilUtc = now;
                    break;
                case ReportingPeriod.Last180days:
                    fromUtc = now.AddDays(-180);
                    untilUtc = now;
                    break;
                case ReportingPeriod.Last365days:
                    fromUtc = now.AddDays(-365);
                    untilUtc = now;
                    break;
                case ReportingPeriod.CurrentMonth:
                    fromUtc = now.GetFirstDayOfMonth();
                    untilUtc = now.GetLastDayOfMonth();
                    break;
                case ReportingPeriod.CurrentQuarter:
                    fromUtc = now.GetFirstDayOfQuarter();
                    untilUtc = now.GetLastDayOfQuarter();
                    break;
                case ReportingPeriod.CurrentYear:
                    fromUtc = now.GetFirstDayOfYear();
                    untilUtc = now.GetLastDayOfYear();
                    break;
                case ReportingPeriod.LastMonth:
                    fromUtc = now.AddMonths(-1).GetFirstDayOfMonth();
                    untilUtc = fromUtc.GetLastDayOfMonth();
                    break;
                case ReportingPeriod.LastQuarter:
                    fromUtc = now.AddMonths(-3).GetFirstDayOfQuarter();
                    untilUtc = fromUtc.GetLastDayOfQuarter();
                    break;
                case ReportingPeriod.LastYear:
                    fromUtc = now.AddYears(-1).GetFirstDayOfYear();
                    untilUtc = fromUtc.GetLastDayOfYear();
                    break;
                default:
                    fromUtc = this.deDateFrom.Value.GetValueOrDefault(fromUtc);
                    untilUtc = this.deDateUntil.Value.GetValueOrDefault(untilUtc);
                    break;
            }

            fromUtc = fromUtc.MakeBeginOfDay();
            untilUtc = untilUtc.MakeEndOfDay();

            this.deDateFrom.Value = fromUtc;
            this.deDateUntil.Value = untilUtc;

            // User Picks 01-05-2015 till 05-05-2015 in combination with Pacific Time Zone (UTC-08:00)
            // This means our 'source' date times are 01-05-2015 00:00:00 - 05-05-2015 23:59:59 PDT (It's daylight savings time)
            // To query this correctly we must convert this to Utc
            // Which would mean: 01-05-2015 07:00:00 - 06-05-2015 06:59:59
            if (!this.ddlTimeZoneOlsonId.SelectedValueString.IsNullOrWhiteSpace())
            {
                TimeZoneInfo selectedTimeZone = TimeZoneHelper.GetTimeZoneInfo(this.ddlTimeZoneOlsonId.SelectedValueString);
                fromUtc = TimeZoneInfo.ConvertTimeToUtc(fromUtc, selectedTimeZone);
                untilUtc = TimeZoneInfo.ConvertTimeToUtc(untilUtc, selectedTimeZone);
            }
        }

        public new Dionysos.Web.UI.PageQueryStringDataBinding Page
        {
            get
            {
                return base.Page as Dionysos.Web.UI.PageQueryStringDataBinding;
            }
        }        

        protected void PeriodRelatedValueChanged(object sender, EventArgs e)
        {
            DateTime from, until;
            GetPeriod(out from, out until);

            this.deDateFrom.Enabled = (this.Period == ReportingPeriod.Manual);
            this.deDateUntil.Enabled = (this.Period == ReportingPeriod.Manual);
        }

        protected void CategoriesFilterIndexChanged(object sender, EventArgs e)
        {
            this.plhCategories.Visible = this.CategoriesFilter != ReportCategoryFilter.AllCategories;
        }        

        public TransactionsRequest GetTransactionsRequest()
        {
            DateTime from, until;
            this.GetPeriod(out from, out until);
            TransactionsRequest request = new TransactionsRequest(from, until, this.CompanyId);

            if (this.phlRoomFilter.Visible)
            {
                request.IncludeByodApps = this.cbIncludeByodApps.Checked;
                request.IncludeInRoomTablets = this.cbIncludeInRoomTablets.Checked;
            }
            if (this.phlOrders.Visible)
            {
                request.IncludeFailedOrders = this.cbIncludeFailedOrders.Checked;
                request.IncludeOrderitems = this.cbIncludeOrderitems.Checked;

                if (this.cbOrderType.SelectedIndex > 0)
                {                    
                    request.OrderType = (OrderType)this.cbOrderType.ValidId;
                }
            }

            // Check if any of the items are deselected (don't check 0, as that's the 'All/None toggle'
            if (this.lbDeliverypointgroups.ItemsCount != this.lbDeliverypointgroups.SelectedItemsCount)
            {
                // Create a filter
                request.DeliverypointgroupIds = this.lbDeliverypointgroups.SelectedItemsValuesAsIntList;
            }

            if (this.lbDeliverypoints.Visible)
            {
                if (this.lbDeliverypoints.ItemsCount != this.lbDeliverypoints.SelectedItemsCount)
                {
                    // Create a filter
                    request.DeliverypointIds = this.lbDeliverypoints.SelectedItemsValuesAsIntList;
                }
            }

            request.DeliverypointsExclude = this.cbExcludeDeliverypoints.Checked;
            request.ReportCategoryFilter = this.CategoriesFilter;
            request.CategoryIds = this.CategoryPanel.GetSelectedCategoryIds();
            request.TimeZoneOlsonId = this.TimeZoneOlsonId;

            return request;
        }        

        private List<int> GetProductIdsFromAlterationProductsRecursive(int productId)
        {
            List<int> productIds = new List<int>();

            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductAlterationEntity);
            prefetch.Add(ProductAlterationEntity.PrefetchPathAlterationEntity);

            ProductAlterationCollection productAlterations = new ProductAlterationCollection();
            productAlterations.GetMulti(ProductAlterationFields.ProductId == productId, prefetch);

            foreach (ProductAlterationEntity productAlteration in productAlterations)
            {
                productIds.AddRange(this.GetProductIdsFromAlterationProductsRecursive(productAlteration.AlterationEntity.AlterationCollection));
            }

            return productIds;
        }

        private List<int> GetProductIdsFromAlterationProductsRecursive(IEntityCollection entities)
        {
            List<int> productIds = new List<int>();
            foreach (IEntity entity in entities)
            {
                if (entity is AlterationEntity)
                {                    
                    productIds.AddRange(this.GetProductIdsFromAlterationProductsRecursive(((AlterationEntity)entity).AlterationCollection));
                    productIds.AddRange(this.GetProductIdsFromAlterationProductsRecursive(((AlterationEntity)entity).AlterationProductCollection));
                }
                else if (entity is AlterationProductEntity)
                {
                    productIds.Add(((AlterationProductEntity)entity).ProductId);
                    productIds.AddRange(this.GetProductIdsFromAlterationProductsRecursive(((AlterationProductEntity)entity).ProductEntity.ProductAlterationCollection));
                }
                else if (entity is ProductAlterationEntity)
                {
                    AlterationCollection collection = new AlterationCollection();
                    collection.Add(((ProductAlterationEntity)entity).AlterationEntity);
                    productIds.AddRange(this.GetProductIdsFromAlterationProductsRecursive(collection));
                }
            }
            return productIds;
        }
    }
}