﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.Subpanels.ReportFilterPanel" Codebehind="ReportFilterPanel.ascx.cs" %>
<%@ Reference Control="~/Analytics/Subpanels/CategoryPanel.ascx" %>
<table class="dataformV2">
    <tr>
	    <td class="label">
		    <D:Label runat="server" ID="lblCompany">Bedrijf</D:Label>
	    </td>
	    <td class="control">
		    <D:Label runat="server" ID="lblCompanyValue" LocalizeText="False"></D:Label>
	    </td>	  
        <td class="label">
		    <D:Label runat="server" ID="lblDeliverypointgroup">Periode</D:Label>
	    </td>
	    <td class="control">
		    <X:ComboBoxInt runat="server" ID="cbPeriod" AutoPostBack="True"></X:ComboBoxInt>
	    </td>
    </tr>
    <tr>
	    <td class="label">
		    <D:Label runat="server" ID="lblDateFrom">Van</D:Label>
	    </td>
	    <td class="control">                                    
		    <X:DateEdit runat="server" ID="deDateFrom" AllowNull="false"></X:DateEdit>
	    </td>	  
        <td class="label">
		    <D:Label runat="server" ID="lblDateUntil">Tot en met</D:Label>
	    </td>
	    <td class="control">
		    <X:DateEdit runat="server" ID="deDateUntil" AllowNull="false"></X:DateEdit>
	    </td>      
    </tr>
    <tr>
	    <td class="label">
		    <D:Label runat="server" id="lblTimeZoneId">Timezone</D:Label>
	    </td>
	    <td class="control">
	        <X:ComboBox runat="server" ID="ddlTimeZoneOlsonId" IsRequired="true" TextField="Name" ValueField="OlsonTimeZoneId"></X:ComboBox>		    
	    </td>	  
    </tr>
    <D:PlaceHolder ID="phlRoomFilter" runat="server">
    <tr>
	    <td class="label">
		    &nbsp;
	    </td>
	    <td class="control">
		    <D:CheckBox runat="server" ID="cbIncludeInRoomTablets" Text="Include In Room Tablets" />
	    </td>	  
        <td class="label">								    
	    </td>
	    <td class="control">								    
            <D:CheckBox runat="server" ID="cbIncludeByodApps" Text="Include Byod Apps" />
	    </td>      
    </tr>
    </D:PlaceHolder>
    <D:PlaceHolder ID="phlOrders" runat="server">
    <tr>
	    <td class="label">
		    &nbsp;
	    </td>
	    <td class="control">
		    <D:CheckBox runat="server" ID="cbIncludeFailedOrders" Text="Include Failed Orders" />
	    </td>	  
        <td class="label">			
            <D:Label runat="server" ID="lblOrderType">Order type</D:Label>					    
	    </td>
	    <td class="control">	
            <X:ComboBoxInt runat="server" ID="cbOrderType"></X:ComboBoxInt>
	    </td>      
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>   
        <td class="control">
            <D:CheckBox runat="server" ID="cbIncludeOrderitems" Text="Include Orderitems" />
        </td>
        <td class="label">
            <D:Label runat="server" ID="lblCategories">Categories</D:Label>
        </td>
        <td class="control">
		    <X:ComboBoxInt runat="server" ID="cbCategories" AutoPostBack="True"></X:ComboBoxInt>
	    </td>
    </tr>
    </D:PlaceHolder>
    <D:PlaceHolder ID="plhProduct" runat="server">
    <tr>
         <td class="label">
            <D:Label runat="server" ID="lblProduct">Product</D:Label>
         </td>   
        <td class="control">
            <X:ComboBoxLLBLGenEntityCollection ID="cbProductCategoryId" runat="server" IncrementalFilteringMode="StartsWith" EntityName="ProductCategory" ValueField="ProductCategoryId" TextField="ProductCategoryMenuName" PreventEntityCollectionInitialization="true" UseDataBinding="true"/>
        </td>        
    </tr>    
    </D:PlaceHolder>
    <D:PlaceHolder runat="server" ID="plhDeliverypointgroups"> 
    <tr>
	    <td class="label">
		    <D:Label runat="server" ID="lblDeliverypointgroups" LocalizeText="true">Tafel/kamergroepen</D:Label>
	    </td>
	    <td class="control">                                    
		    <X:ListBox runat="server" ID="lbDeliverypointgroups" SelectionMode="CheckColumn" ClientInstanceName="lbDeliverypointgroups" TextField="Name" ValueField="DeliverypointgroupId"></X:ListBox>
	    </td>	          
        <td class="label">
		    <D:Label runat="server" ID="lblDeliverypoints" LocalizeText="true">Tafels/Kamers</D:Label><br/><br/>
            <D:Button runat="server" ID="btnLoadDeliverypoints" Text="Load Deliverypoints" /><br/><br/><br/><br/><br/><br/><br/><br/>
            <D:Button runat="server" ID="btnOrderByDeliverypointgroup" Text="Order by deliverypointgroup" /><br/><br/>
            <D:Button runat="server" ID="btnOrderByDeliverypoint" Text="Order by deliverypoint" />
	    </td>
	    <td class="control">
		    <X:ListBox runat="server" ID="lbDeliverypoints" SelectionMode="CheckColumn" ClientInstanceName="lbDeliverypoints" TextField="DeliverypointgroupNameDeliverypointNumber" ValueField="DeliverypointId"></X:ListBox>
            <D:CheckBox runat="server" ID="cbExcludeDeliverypoints" Text="Exclude deliverypoints" />
	    </td>      
    </tr>    
    </D:PlaceHolder>
</table>
<D:PlaceHolder runat="server" ID="plhCategories" Visible="false">
</D:PlaceHolder>