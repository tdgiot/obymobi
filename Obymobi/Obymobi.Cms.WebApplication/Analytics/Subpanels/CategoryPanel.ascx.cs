﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxTreeList;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Analytics.Subpanels
{
    public partial class CategoryPanel : UserControl
    {
        #region Fields

        readonly List<int> categoryIds = new List<int>();
        readonly List<ASPxTreeList> categoryLists = new List<ASPxTreeList>();

        #endregion

        #region Event Handlers

        private void CreateMenuTree()
        {
            MenuCollection menus = new MenuCollection();
            menus.GetMulti(new PredicateExpression(MenuFields.CompanyId == CmsSessionHelper.CurrentCompanyId));

            if (menus.Count > 0)
            {
                if (menus.Count > 1)
                {
                    PageControl tabsMenus = new PageControl();
                    tabsMenus.Width = Unit.Percentage(100);
                    foreach (MenuEntity menu in menus)
                    {
                        TabPage page = new TabPage();
                        page.Text = menu.Name;
                        page.Name = string.Format("menu-{0}", menu.MenuId);

                        ASPxTreeList tlMenu = this.RenderTreeMenu(menu);

                        page.Controls.Add(tlMenu);
                        tabsMenus.TabPages.Add(page);
                    }

                    this.plhMenus.Controls.Add(tabsMenus);
                }
                else
                {
                    ASPxTreeList tlMenu = this.RenderTreeMenu(menus[0]);
                    this.plhMenus.Controls.Add(tlMenu);
                }
            }
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            this.CreateMenuTree();
        }

        private ASPxTreeList RenderTreeMenu(MenuEntity menu)
        {
            ASPxTreeList tlMenu = new ASPxTreeList();

            // Treelist settings
            tlMenu.KeyFieldName = "CategoryId";
            tlMenu.ParentFieldName = "ParentCategoryId";
            tlMenu.AutoGenerateColumns = false;
            tlMenu.SettingsBehavior.AutoExpandAllNodes = true;
            tlMenu.Settings.GridLines = GridLines.Horizontal;
            tlMenu.Width = Unit.Percentage(100);
            tlMenu.SettingsEditing.Mode = TreeListEditMode.EditFormAndDisplayNode;
            tlMenu.SettingsSelection.Enabled = true;
            tlMenu.ID = "tlMenu-" + menu.MenuId.ToString();
            tlMenu.EnableCallbacks = true;
            tlMenu.ClientInstanceName = "tlMenu-" + menu.MenuId.ToString();
            
            // Name Column
            TreeListHyperLinkColumn tlcName = new TreeListHyperLinkColumn();
            tlcName.Caption = "Name";
            tlcName.FieldName = "CategoryId";
            tlcName.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            tlcName.PropertiesHyperLink.TextField = "Name";
            tlcName.PropertiesHyperLink.NavigateUrlFormatString = "~/Catalog/Category.aspx?id={0}";
            tlMenu.Columns.Add(tlcName);
            
            // Bind the data
            tlMenu.DataBound += this.Menu_DataBound;
            tlMenu.DataSource = menu.CategoryCollection;

            if (!this.IsPostBack)
            {
                tlMenu.DataBind();
            }

            this.categoryLists.Add(tlMenu);

            return tlMenu;
        }

        private void Menu_DataBound(object sender, EventArgs e)
        {
            ASPxTreeList tlMenu = (ASPxTreeList)sender;
            
            foreach (TreeListNode node in tlMenu.Nodes)
            {
                this.CheckCategoryNode(node);
            }
        }

        private void CheckCategoryNode(TreeListNode node)
        {
            int categoryId = int.Parse(node.Key);

            if (categoryId > 0 && this.categoryIds.Contains(categoryId))
            {
                // Found it!
                node.Selected = true;
            }

            // Check his child nodes
            foreach (TreeListNode childNode in node.ChildNodes)
            {
                this.CheckCategoryNode(childNode);
            }
        }

        public List<String> GetSelectedCategories()
        {
            List<String> selectedCategories = new List<String>();

            foreach (ASPxTreeList categoryList in this.categoryLists)
            {
                foreach (TreeListNode node in categoryList.GetSelectedNodes())
                {
                    selectedCategories.Add(node.Key);
                }
            }

            return selectedCategories;
        }

        public List<int> GetSelectedCategoryIds()
        {
            List<int> selectedCategoryIds = new List<int>();

            foreach (string selectedCategory in this.GetSelectedCategories())
            {
                selectedCategoryIds.Add(int.Parse(selectedCategory));
            }

            return selectedCategoryIds;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region Properties

        public new PageLLBLGenEntity Page
        {
            get
            {
                return base.Page as PageLLBLGenEntity;
            }
        }

        #endregion

    }
}