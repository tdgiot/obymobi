﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos.Web.UI;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Logic.HelperClasses;
using System.Data;
using Obymobi.Data.DaoClasses;
using Obymobi.Enums;
using DevExpress.Web.ASPxTreeList;
using Obymobi.Data;
using DevExpress.Web;

namespace Obymobi.ObymobiCms.Analytics.Subpanels
{
    public partial class DeliverypointPanel : System.Web.UI.UserControl
    {
        #region Fields

        private List<ASPxCheckBoxList> DeliverypointLists = new List<ASPxCheckBoxList>();

        #endregion

        #region Methods

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);           
        }

        #endregion

        #region Event Handlers

        private void createCheckboxList()
        {
            Table table = new Table();
            table.CellSpacing = 10;

            DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
            deliverypointgroups.GetMulti(new PredicateExpression(DeliverypointgroupFields.CompanyId == CmsSessionHelper.CurrentCompanyId));
            foreach(DeliverypointgroupEntity deliverypointgroup in deliverypointgroups)
            {
                TableRow row = new TableRow();

                TableCell cellLabel = new TableCell();
            
                ASPxCheckBoxList cblDeliverypointgroup = new ASPxCheckBoxList();
                cblDeliverypointgroup.ID = "cblDeliverypointgroup" + deliverypointgroup.DeliverypointgroupId;
                cblDeliverypointgroup.ClientInstanceName = "cblDeliverypointgroup" + deliverypointgroup.DeliverypointgroupId;
                cblDeliverypointgroup.Items.Add(deliverypointgroup.Name);
                cblDeliverypointgroup.Border.BorderWidth = 0;
                cblDeliverypointgroup.ClientSideEvents.SelectedIndexChanged = "function(s, e) { if(cblDeliverypointgroup" + deliverypointgroup.DeliverypointgroupId + ".GetSelectedItems().length == 0){ cblDeliverypoints" + deliverypointgroup.DeliverypointgroupId + ".UnselectAll(); } else {cblDeliverypoints" + deliverypointgroup.DeliverypointgroupId + ".SelectAll(); } }";
                cellLabel.VerticalAlign = VerticalAlign.Top;
                cellLabel.Controls.Add(cblDeliverypointgroup);
                row.Cells.Add(cellLabel);

                TableCell cellList = new TableCell();
                ASPxCheckBoxList cblDeliverypoints = this.renderCheckboxlist(deliverypointgroup);
                cblDeliverypoints.Border.BorderWidth = 0;
                this.DeliverypointLists.Add(cblDeliverypoints);
                cellList.Controls.Add(cblDeliverypoints);
                row.Cells.Add(cellList);

                table.Rows.Add(row);
            }

            this.plhControls.Controls.Add(table);
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            this.createCheckboxList();
        }

        private ASPxCheckBoxList renderCheckboxlist(DeliverypointgroupEntity deliverypointgroup)
        {
            ASPxCheckBoxList checkboxList = new ASPxCheckBoxList();
            checkboxList.ID = "cblDeliverypoints" + deliverypointgroup.DeliverypointgroupId;
            checkboxList.ClientInstanceName = "cblDeliverypoints" + deliverypointgroup.DeliverypointgroupId;
            checkboxList.RepeatLayout = RepeatLayout.Table;
            checkboxList.RepeatDirection = RepeatDirection.Vertical;
            checkboxList.RepeatColumns = 10;
            checkboxList.TextField = "Number";

            DeliverypointCollection deliverypoints = new DeliverypointCollection();
            deliverypoints.GetMulti(new PredicateExpression(DeliverypointFields.DeliverypointgroupId == deliverypointgroup.DeliverypointgroupId));

            checkboxList.DataSource = deliverypoints;

            if (!IsPostBack)
                checkboxList.DataBind();

            return checkboxList;
        }

        public List<String> GetSelectedDeliverypoints()
        {
            List<String> selectedDeliverypoints = new List<String>();
            foreach (ASPxCheckBoxList deliverypointList in this.DeliverypointLists)
            {
                foreach (ListEditItem item in deliverypointList.SelectedItems)
                {
                    selectedDeliverypoints.Add(item.Text);
                }
            }
            return selectedDeliverypoints;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region Properties

        public new PageLLBLGenEntity Page
        {
            get
            {
                return base.Page as PageLLBLGenEntity;
            }
        }

        #endregion

    }
}