﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data;
using Dionysos.Diagnostics;
using Obymobi.Web.Google;
using System.Text;
using System.Data;
using DevExpress.Web;
using Obymobi.Logic.HelperClasses;
using Dionysos.Web.UI;
using Dionysos;

namespace Obymobi.ObymobiCms.Analytics
{
    public partial class Orders : Dionysos.Web.UI.PageQueryStringDataBinding
    {
        Dictionary<int, string> sessionIds = new Dictionary<int, string>();

        #region Methods

        protected override void SetDefaultValuesToControls()
        {
            if (CmsSessionHelper.CurrentCompanyId > 0)
                this.ddlCompanyId.Value = CmsSessionHelper.CurrentCompanyId;

            this.deDateFrom.Value = DateTimeUtil.MakeBeginOfDay(DateTime.Now.AddMonths(-1));
            this.deDateTo.Value = DateTimeUtil.MakeBeginOfDay(DateTime.Now);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (this.ddlCompanyId.ValidId > 0)
            {
                DeliverypointCollection deliverypoints = new DeliverypointCollection();
                deliverypoints.GetMulti(DeliverypointFields.CompanyId == this.ddlCompanyId.ValidId);

                this.ddlDeliverypointId.DataSource = deliverypoints;
                this.ddlDeliverypointId.DataBind();

                DeliverypointgroupCollection dpgs = new DeliverypointgroupCollection();
                dpgs.GetMulti(DeliverypointgroupFields.CompanyId == this.ddlCompanyId.ValidId);

                this.ddlDeliverypointgroupId.DataSource = dpgs;
                this.ddlDeliverypointgroupId.DataBind();
            } 
        }

        private void ProcessOrders()
        {
            StringBuilder builder = new StringBuilder();
            CompanyEntity company = new CompanyEntity(this.ddlCompanyId.ValidId);
            if (!company.IsNew)
            {
                if (company.GoogleAnalyticsId.Length > 0)
                {
                    string cacheKey = string.Format("Orders.aspx.cs.{0}.{1}.{2}.{3}.{4}",
                        company.CompanyId, this.ddlDeliverypointgroupId.ValidId, this.ddlDeliverypointId.ValidId, this.deDateFrom.Value, this.deDateTo.Value);

                    Google.Apis.Manual.Analytics.v3.Data.GaData orders;
                    if(!Dionysos.Web.CacheHelper.TryGetValue(cacheKey,false,out orders))
                    {
                        GoogleAPI google = new GoogleAPI();
                        if (google.SetProfile(company.GoogleAnalyticsId))
                        {
                            DateTime dateFrom = this.deDateFrom.Value.Value;
                            DateTime dateTo = this.deDateTo.Value.Value;

                            int deliverypointNumber = 0;
                            if (this.ddlDeliverypointId.ValidId > 0)
                                deliverypointNumber = int.Parse(this.ddlDeliverypointId.Text);

                            // Orders tab
                            if (!Dionysos.Web.CacheHelper.TryGetValue(cacheKey, false, out orders))
                            {
                                orders = google.Get("ga:totalEvents", "ga:eventLabel,ga:customVarValue5", "ga:eventAction==Place Order", null, dateFrom, dateTo);
                                Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, cacheKey, orders, 60);
                            }
                        }                        
                        else
                        {
                            builder.AppendFormat("Google Analytics profile '{0}' could not be opened.", company.GoogleAnalyticsId);
                        }
                    }

                    if (orders != null)
                    {
                        for (int i = 0; i < orders.TotalResults; i++)
                        {
                            string orderIdStr = orders.Rows[i][0];
                            int orderId;
                            if (!orderIdStr.Contains(" ") && orderIdStr != "0" && int.TryParse(orderIdStr, out orderId)) // Check if it has the old format (timestamp<space>orderGuid)
                            {
                                sessionIds.Add(orderId, orders.Rows[i][1]);
                            }
                        }

                        OrderCollection orderCollection = new OrderCollection();
                        PredicateExpression filter = new PredicateExpression();
                        filter.Add(OrderFields.CompanyId == this.ddlCompanyId.ValidId);
                        filter.Add(OrderFields.CreatedUTC >=  this.deDateFrom.Value.Value.MakeBeginOfDay().LocalTimeToUtc());
                        filter.Add(OrderFields.CreatedUTC <= this.deDateTo.Value.Value.MakeEndOfDay().LocalTimeToUtc());

                        if (this.ddlDeliverypointgroupId.ValidId > 0)
                        {
                            DeliverypointgroupEntity dpg = new DeliverypointgroupEntity(this.ddlDeliverypointgroupId.ValidId);
                            if (dpg.DeliverypointCollection.Count > 0)
                            {
                                var deliverypointIds = dpg.DeliverypointCollection.Select(dp => dp.DeliverypointId).ToList();
                                filter.Add(OrderFields.DeliverypointId == deliverypointIds);
                            }
                            else
                                filter.Add(OrderFields.DeliverypointNumber == "-1");
                        }
                        else if (this.ddlDeliverypointId.ValidId > 0)
                        {
                            filter.Add(OrderFields.DeliverypointId == this.ddlDeliverypointId.ValidId);
                        }

                        PrefetchPath prefetch = new PrefetchPath(EntityType.OrderEntity);
                        prefetch.Add(OrderEntity.PrefetchPathDeliverypointEntity);

                        SortExpression sort = new SortExpression();
                        sort.Add(OrderFields.CreatedUTC | SortOperator.Descending);

                        orderCollection.GetMulti(filter, 0, sort, null, prefetch);

                        if (orderCollection.Count > 0)
                        {
                            DataTable orderList = new DataTable();
                            bool isCoversVisible = CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.Covers);

                            // Create columns
                            DataColumn orderIdColumn = new DataColumn("OrderId");
                            DataColumn deliverypointNumberColumn = new DataColumn("DeliverypointNumber");
                            DataColumn createdColumn = new DataColumn("Created");
                            DataColumn sessionColumn = new DataColumn("Session");
                            DataColumn viewSessionColumn = new DataColumn("ViewSession");
                            DataColumn coversCountColumn = new DataColumn("NumberOfCovers");

                            // Translations
                            orderIdColumn.Caption = this.Translate("clmnOrderId", "Orderid");
                            deliverypointNumberColumn.Caption = this.Translate("clmnDeliverypointNumber", "Tafelnummer");
                            createdColumn.Caption = this.Translate("clmnCreated", "Aangemaakt");
                            sessionColumn.Caption = this.Translate("clmnSession", "Sessie");
                            viewSessionColumn.Caption = this.Translate("clmViewSession", "Bekijk sessie");
                            coversCountColumn.Caption = this.Translate("clmNumberOfCovers", "Aantal klanten");

                            // Add colums
                            orderList.Columns.Add(orderIdColumn);
                            orderList.Columns.Add(deliverypointNumberColumn);
                            orderList.Columns.Add(createdColumn);
                            orderList.Columns.Add(sessionColumn);
                            orderList.Columns.Add(viewSessionColumn);

                            if (isCoversVisible)
                            { orderList.Columns.Add(coversCountColumn); }

                            foreach (OrderEntity orderEntity in orderCollection)
                            {
                                DataRow row = orderList.NewRow();
                                row["OrderId"] = orderEntity.OrderId;
                                row["DeliverypointNumber"] = orderEntity.DeliverypointEntity.Number;
                                row["Created"] = orderEntity.CreatedUTC.Value.UtcToLocalTime();
                                if (sessionIds.ContainsKey(orderEntity.OrderId))
                                {
                                    row["ViewSession"] = "View";
                                    row["Session"] = string.Format("Session.aspx?profile={0}&session={1}", company.GoogleAnalyticsId, sessionIds[orderEntity.OrderId]);
                                }

                                if (isCoversVisible)
                                { row["NumberOfCovers"] = orderEntity.CoversCount; }

                                orderList.Rows.Add(row);
                            }

                            this.orderGrid.Visible = true;
                            this.orderGrid.DataSource = orderList;
                            this.orderGrid.DataBind();
                        }
                        else
                        { 
                            this.AddInformatorInfo(this.Translate("NoOrdersInSelectedPeriod", "Er zijn geen bestellingen binnen de geselecteerde periode"));
                            this.orderGrid.Visible = false;
                            this.Validate();    
                        }
                    }
                    else
                    {
                        builder.AppendFormat("Could not get data from Google Analytics.", company.GoogleAnalyticsId);
                    }
                }
                else
                {
                    builder.Append(this.Translate("lblNoGoogleAnalyticsConfigured", "Dit bedrijf heeft geen Google Analytics ID geconfigureerd."));                    
                }
            }
            else
            {
                builder.Append("No company selected.");
            }
            this.plhOverview.AddHtml(builder.ToString());
        }

        #endregion


        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnRefresh.Click += btnRefresh_Click;
            
            if (!this.IsPostBack || this.IsCallback)
            {
                this.ProcessOrders();
            }
        }

        void btnRefresh_Click(object sender, EventArgs e)
        {
            this.RedirectWithControlValues();
        }

        #endregion
    }
}