﻿<%@ Page Title="Orders" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.Orders" Codebehind="Orders.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <D:Button runat="server" ID="btnRefresh" Text="Verversen" CausesValidation="true"/>	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
            <X:TabPage Text="Overzicht" Name="Overview">
				    <Controls>
	                    <table class="dataformV2">
		                    <tr>
			                    <td class="label">
				                    <D:Label runat="server" id="lblCompanyId">Bedrijf</D:Label>
			                    </td>
			                    <td class="control">
				                    <X:ComboBoxLLBLGenEntityCollection ID="ddlCompanyId" runat="server" EntityName="Company" IsRequired="true"></X:ComboBoxLLBLGenEntityCollection>
			                    </td>																	
		                    </tr>
                            <tr>
                                <td class="label">
                                    <D:Label runat="server" id="lblDeliverypointgroup">Tafelgroep</D:Label>
			                    </td>
			                    <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlDeliverypointgroupId" IncrementalFilteringMode="StartsWith" EntityName="Deliverypointgroup" TextField="Name" ValueField="DeliverypointgroupId" />
			                    </td>				
                                <td class="label">
                                    <D:Label runat="server" id="lblDeliverypoint">Tafel</D:Label>
			                    </td>
			                    <td class="control">
                                    <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlDeliverypointId" IncrementalFilteringMode="StartsWith" EntityName="Deliverypoint" TextField="Number" ValueField="DeliverypointId" />
			                    </td>											
		                    </tr>
		                    <tr>
			                    <td class="label">
				                    <D:Label runat="server" id="lblDateFrom">Datum van</D:Label>
			                    </td>
			                    <td class="control">
                                    <X:DateEdit runat="server" ID="deDateFrom"></X:DateEdit>
			                    </td>							
			                    <td class="label">
				                    <D:Label runat="server" id="lblDateTo">Datum tot</D:Label>
			                    </td>
			                    <td class="control">
                                    <X:DateEdit runat="server" ID="deDateTo"></X:DateEdit>
			                    </td>							
		                    </tr>
                        </table>
                        <D:PlaceHolder runat="server" ID="plhOverview"></D:PlaceHolder>
                        <X:GridView ID="orderGrid" ClientInstanceName="orderGrid" runat="server" Width="100%" Visible="false">        
                            <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                            <SettingsPager PageSize="25"></SettingsPager>
                            <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                            <Columns>
                                <dxwgv:GridViewDataColumn FieldName="OrderId" VisibleIndex="0" >
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="DeliverypointNumber" VisibleIndex="1" >
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataColumn FieldName="Created" VisibleIndex="2" >
                                </dxwgv:GridViewDataColumn>
                                <dxwgv:GridViewDataHyperLinkColumn FieldName="Session" VisibleIndex="3" >
                                    <PropertiesHyperLinkEdit NavigateUrlFormatString="{0}" TextField="ViewSession"  />
                                </dxwgv:GridViewDataHyperLinkColumn>
                                <dxwgv:GridViewDataColumn FieldName="NumberOfCovers" VisibleIndex="5" >
                                </dxwgv:GridViewDataColumn>
                            </Columns>
                        </X:GridView>                         
                     </Controls>
            </X:TabPage>		
        </TabPages>
    </X:PageControl>
</asp:Content>

