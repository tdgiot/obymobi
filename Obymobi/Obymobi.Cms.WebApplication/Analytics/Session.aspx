﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.Session" Codebehind="Session.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Overzicht" Name="Overview">
				<Controls>
                    <D:PlaceHolder ID="PlaceHolder1" runat="server">
                        <table class="dataformV2">
					    	<D:PlaceHolder runat="server" ID="plhOverview"></D:PlaceHolder>
					    </table>			
                     </D:PlaceHolder>
                     <X:GridView ID="actionGrid" ClientInstanceName="actionGrid" runat="server" Width="100%">        
                        <SettingsBehavior AutoFilterRowInputDelay="350" />
                        <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                        <SettingsPager PageSize="100"></SettingsPager>
                        <Columns>
                            <dxwgv:GridViewDataColumn FieldName="Time" VisibleIndex="0" >
							    <Settings AutoFilterCondition="Contains" />
							    <Settings AutoFilterCondition="Contains" />
                            </dxwgv:GridViewDataColumn>
                            <dxwgv:GridViewDataColumn FieldName="Action" VisibleIndex="1" >
							    <Settings AutoFilterCondition="Contains" />
							    <Settings AutoFilterCondition="Contains" />
                            </dxwgv:GridViewDataColumn>
                            <dxwgv:GridViewDataColumn FieldName="Value" VisibleIndex="2" >
							    <Settings AutoFilterCondition="Contains" />
                            </dxwgv:GridViewDataColumn>
                        </Columns>
                    </X:GridView>      
				</Controls>
			</X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>

