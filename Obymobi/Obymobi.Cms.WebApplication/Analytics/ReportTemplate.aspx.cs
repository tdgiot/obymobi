using DevExpress.Web.ASPxTreeList;
using DevExpress.Web.Internal;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.ObymobiCms.Analytics.DataSources;
using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Requests.RequestBase;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Web.Analytics.Factories;

namespace Obymobi.ObymobiCms.Analytics
{
	public partial class ReportTemplate : PageLLBLGenEntityGeneric<ReportProcessingTaskTemplateEntity>
	{
		private readonly ReportRequestBridgeFactory reportRequestBridgeFactory = new ReportRequestBridgeFactory();
		private CompanyEntity companyEntity;

		private ReportRequest filter;
		private SheetFilterDataSource filterDataSource;

		private string TreelistId => $"{ID}_treelist";
		private ReportRequest Filter => filter ?? (filter = DeserializeOrCreateFilter(DataSource.ReportType));

		private string EndDragNodeMethod =>
			@" function(s, e) {                                                                
							e.cancel = true;
							var key = s.GetNodeKeyByRow(e.targetElement);
							" + TreelistId + @".PerformCustomCallback(e.nodeKey + ':' + key);
							}";

		protected override void OnInit(EventArgs e)
		{
            ReturnUrl = "~/Analytics/ReportTemplates.aspx"; 
            
            LoadUserControls();
			CreateSheetFilterTreeList();
			DataSourceLoaded += ReportTemplate_DataSourceLoaded;
			base.OnInit(e);
		}

		private void LoadUserControls()
		{
			cbPeriod.DisplayEmptyItem = false;
			cbPeriod.DataBindEnum<ReportingPeriod>();

			DataBindReportTypes();
		}

		private void DataBindReportTypes()
		{
			if (PageMode == PageMode.Add)
			{
				List<AnalyticsReportType> reportTypes = new List<AnalyticsReportType>
				{
					AnalyticsReportType.TransactionsAppLess
				};

				if (CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.Reporting_Cms_ProductSalesReportTemplate))
				{
					reportTypes.Add(AnalyticsReportType.ProductSales);
				}

				if (CmsSessionHelper.CurrentUser.HasFeature(FeatureToggle.Reporting_Cms_InventoryReportTemplate))
				{
					reportTypes.Add(AnalyticsReportType.Inventory);
				}

				cbReportType.DataBindEnumValues(reportTypes.ToArray());
				cbReportType.Enabled = reportTypes.Count > 1;
			}
			else
			{
				cbReportType.DataBindEnum<AnalyticsReportType>();
				cbReportType.Enabled = false;
			}
		}

		private void ReportTemplate_DataSourceLoaded(object sender)
		{
			if (DataSource.IsNew)
			{
				DataSource.CompanyId = CmsSessionHelper.CurrentCompanyId;
				DataSource.ReportType = AnalyticsReportType.TransactionsAppLess;
			}

			if (!DataSource.IsNew && !DataSource.Filter.IsNullOrWhiteSpace())
			{
				tbMetadata.Text = JToken.Parse(DataSource.Filter).ToString(Formatting.Indented);
			}

			ApplyFilter();
			SetGui();
		}

		private void SetGui()
		{
			lblCompanyValue.Text = GetCompany().Name;
			lblCompanyTimeZone.Text = GetCompany().TimeZoneOlsonId;

			pnlSheets.Visible = PageMode != PageMode.Add;
		}

		private void CreateSheetFilterTreeList()
		{
			tlSheetFilter.AutoGenerateColumns = false;
			tlSheetFilter.Settings.GridLines = GridLines.Horizontal;
			tlSheetFilter.Width = Unit.Percentage(100);
			tlSheetFilter.SettingsEditing.Mode = TreeListEditMode.Inline;
			tlSheetFilter.SettingsSelection.Enabled = false;
			tlSheetFilter.ClientInstanceName = TreelistId;
			tlSheetFilter.SettingsSelection.AllowSelectAll = false;
			tlSheetFilter.SettingsEditing.AllowNodeDragDrop = true;
			tlSheetFilter.ClientSideEvents.EndDragNode = EndDragNodeMethod;

			tlSheetFilter.KeyFieldName = "Id";

			TreeListTextColumn tlcFilterName = new TreeListTextColumn
			{
				Caption = "Name",
				Name = "Name",
				FieldName = "Name"
			};

			tlcFilterName.CellStyle.HorizontalAlign = HorizontalAlign.Left;
			tlSheetFilter.Columns.Add(tlcFilterName);

			TreeListCommandColumn editCommandColumn = new TreeListCommandColumn
			{
				VisibleIndex = 4,
				Width = Unit.Pixel(50),
				ShowNewButtonInHeader = true,
				ButtonType = ButtonType.Image
			};

			editCommandColumn.EditButton.Visible = false;
			editCommandColumn.NewButton.Visible = false;
			editCommandColumn.DeleteButton.Visible = true;
			editCommandColumn.CellStyle.HorizontalAlign = HorizontalAlign.Left;
			editCommandColumn.DeleteButton.Image.Url = ResolveUrl("~/images/icons/delete.png");
			editCommandColumn.CancelButton.Image.Url = ResolveUrl("~/images/icons/cross_grey.png");
			editCommandColumn.NewButton.Image.Url = ResolveUrl("~/images/icons/add2.png");
			editCommandColumn.UpdateButton.Image.Url = ResolveUrl("~/images/icons/disk.png");
			tlSheetFilter.Columns.Add(editCommandColumn);

			TreeListHyperLinkColumn editPageColumn = new TreeListHyperLinkColumn
			{
				Name = "EditPage",
				Caption = " ",
				VisibleIndex = 5,
				Width = Unit.Pixel(15),
				FieldName = "Id"
			};

			editPageColumn.CellStyle.HorizontalAlign = HorizontalAlign.Left;
			editPageColumn.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
			editPageColumn.EditCellStyle.CssClass = "hidden";
			editPageColumn.EditFormSettings.VisibleIndex = 5;
			editPageColumn.EditFormCaptionStyle.CssClass = "hidden";
			tlSheetFilter.Columns.Add(editPageColumn);

			tlSheetFilter.NodeInserted += Save;
			tlSheetFilter.NodeDeleted += Save;
			tlSheetFilter.HtmlDataCellPrepared += TlSheetFilter_HtmlDataCellPrepared;
			tlSheetFilter.CustomCallback += TlSheetFilter_CustomCallback;
		}

		private void Save<TEventArgs>(object sender, TEventArgs e) where TEventArgs : EventArgs => Save();

		private void TlSheetFilter_CustomCallback(object sender, TreeListCustomCallbackEventArgs e)
		{
			// Node dragged callback
			string[] nodes = e.Argument.Split(':');

			string draggedId = nodes[0];
			string draggedUponId = nodes[1];

			TreeListNode node = tlSheetFilter.FindNodeByKeyValue(draggedId);
			if (!(node.DataItem is SpreadSheetFilter draggedOption))
			{
				return;
			}

			TreeListNode parentNode = tlSheetFilter.FindNodeByKeyValue(draggedUponId);
			if (!(parentNode.DataItem is SpreadSheetFilter draggedUponOption))
			{
				return;
			}

			filterDataSource.DraggedToSort(draggedOption.Id, draggedUponOption.Id);
			tlSheetFilter.DataBind();

			Save();
		}

		private void TlSheetFilter_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
		{
			if (e.NodeKey == null)
			{
				return;
			}

			if (e.Column.Name == "EditPage")
			{
				SpreadSheetFilter option = GetOptionByNodeKey(e.NodeKey);
				if (option == null)
				{
					return;
				}

				string url = $"~/Analytics/ReportTemplateSheet.aspx?id={DataSource.ReportProcessingTaskTemplateId}&filter={option.Id}";

				foreach (Control item in e.Cell.Controls)
				{
					if (!(item is HyperLinkDisplayControl link))
					{
						continue;
					}

					link.NavigateUrl = url;
					break;
				}
			}
			else if (e.Column.Name == "Name")
			{
				SpreadSheetFilter option = GetOptionByNodeKey(e.NodeKey);
				if (option == null || option.Active)
				{
					return;
				}

				e.Cell.ForeColor = Color.Gray;
				e.Cell.Font.Strikeout = true;
			}
		}

		private SpreadSheetFilter GetOptionByNodeKey(string nodeKey)
		{
			TreeListNode currentNode = tlSheetFilter.FindNodeByKeyValue(nodeKey);
			if (currentNode?.DataItem is SpreadSheetFilter option)
			{
				return option;
			}

			return null;
		}

		protected void Page_Load(object s, EventArgs e)
		{
			if (!IsPostBack)
			{
				SetDateTimePeriod();
			}

			cbPeriod.ValueChanged += OnCbPeriodOnValueChanged;
		}

		private void OnCbPeriodOnValueChanged(object sender, EventArgs args)
		{
			ReportingPeriod reportingPeriod = (ReportingPeriod) cbPeriod.Value.GetValueOrDefault(0);
			if (reportingPeriod == ReportingPeriod.Manual)
			{
				DateTime currentTime = DateTime.UtcNow.UtcToLocalTime(CmsSessionHelper.CompanyTimeZone);
				deDateTimeUntil.Value = currentTime;
				deDateTimeFrom.Value = currentTime.MakeBeginOfDay();
			}

			SetDateTimePeriod();
		}

		public override bool Save()
		{
			Validate();

			if (!IsValid)
			{
				return false;
			}

			DataSource.ReportingPeriod = (ReportingPeriod) cbPeriod.Value.GetValueOrDefault(0);
			DataSource.Filter = SetMetadata();

			return base.Save();
		}

		public override void Validate()
		{
			if (tbFriendlyName.Text.IsNullOrWhiteSpace())
			{
				MultiValidatorDefault.AddError("Please enter the name of this report");
			}

			if (cbPeriod.Value == (int) ReportingPeriod.Manual)
			{
				if (!deDateTimeFrom.Value.HasValue || !deDateTimeUntil.Value.HasValue)
				{
					MultiValidatorDefault.AddError("A valid from and until date have to be selected");
				}
				else if (deDateTimeFrom.Value.Value > deDateTimeUntil.Value.Value)
				{
					MultiValidatorDefault.AddError("The 'from' date can not be later than the 'until' date");
				}
				else if (deDateTimeFrom.Value.Value > DateTime.UtcNow.Date.UtcToLocalTime(GetCompany().TimeZoneInfo))
				{
					MultiValidatorDefault.AddError("The 'from' date had to be before today");
				}
			}

			base.Validate();
		}

        public void ShowMetadata()
        {
            GenerateReport();
        }

		public void GenerateReport() => CreateManualReportingTask();

		private void SetDateTimePeriod()
		{
			DateTime now = DateTime.Now;

			DateTime fromDate = now.MakeBeginOfDay();
			DateTime untilDate = now.MakeEndOfDay();

			ReportingPeriod reportingPeriod = (ReportingPeriod) cbPeriod.Value.GetValueOrDefault(0);
			if (reportingPeriod == ReportingPeriod.Manual)
			{
				fromDate = deDateTimeFrom.Value.GetValueOrDefault(fromDate);
				untilDate = deDateTimeUntil.Value.GetValueOrDefault(untilDate);

				deDateTimeFrom.Value = fromDate;
				deDateTimeFrom.Enabled = true;
				deDateTimeUntil.Value = untilDate;
				deDateTimeUntil.Enabled = true;
				deDateTimeUntil.VisibleTime = true;
			}
			else
			{
				(fromDate, untilDate) = ReportingPeriodHelper.GetDateTimeForPeriod(reportingPeriod, GetCompany().TimeZoneInfo);
				deDateTimeFrom.ValueDate = fromDate;
				deDateTimeFrom.Enabled = false;
				deDateTimeUntil.ValueDate = untilDate;
				deDateTimeUntil.Enabled = false;
				deDateTimeUntil.VisibleTime = false;
			}
		}

		private void ApplyFilter()
		{
			if (DataSource.IsNew)
			{
				cbPeriod.SelectedIndex = (int) ReportingPeriod.Today;
				return;
			}

			// Set default date/time with ticks from filter
			if (Filter.ReportingPeriod == ReportingPeriod.Manual)
			{
				deDateTimeUntil.Value = Filter.UntilDateTimeUtc.UtcToLocalTime(GetCompany().TimeZoneInfo);
				deDateTimeFrom.Value = Filter.UntilDateTimeUtc.Add(Filter.LeadTimeOffset).UtcToLocalTime(GetCompany().TimeZoneInfo);
			}
			else
			{
				DateTime untilUtcDateTime = DateTime.UtcNow.Date.Add(Filter.UntilDateTimeUtc.TimeOfDay);
				deDateTimeUntil.Value = untilUtcDateTime.UtcToLocalTime(GetCompany().TimeZoneInfo);
				deDateTimeFrom.Value = untilUtcDateTime.Add(Filter.LeadTimeOffset).UtcToLocalTime(GetCompany().TimeZoneInfo);
			}

			cbPeriod.SelectedIndex = (int) Filter.ReportingPeriod;
			DataBindSheetFilters();
			SetDateTimePeriod();
		}

		private void DataBindSheetFilters()
		{
			filterDataSource = new SheetFilterDataSource(Filter, CreateSheetFilterFactory(DataSource.ReportType));

			tlSheetFilter.DataSource = filterDataSource;
			tlSheetFilter.DataBind();
		}

		private ISheetFilterFactory CreateSheetFilterFactory(AnalyticsReportType reportType) =>
			reportRequestBridgeFactory
				.GetSheetFilterRefinedAbstraction(reportType)
				.CreateSheetFilterFactory();

		private ReportRequest CreateReportRequest(AnalyticsReportType reportType) =>
			reportRequestBridgeFactory
				.GetReportRequestRefinedAbstraction(reportType)
				.CreateReportRequest();

		private string SetMetadata()
		{
			string metadata = JsonConvert.SerializeObject(CreateRequestFilter(), Formatting.Indented);
			tbMetadata.Text = metadata;

			return metadata;
		}

		private CompanyEntity GetCompany() => companyEntity ?? (companyEntity = PageMode == PageMode.Add ? new CompanyEntity(CmsSessionHelper.CurrentCompanyId) : DataSource.CompanyEntity);

		private ReportRequest CreateRequestFilter()
		{
			ReportingPeriod reportingPeriod = (ReportingPeriod) cbPeriod.Value.GetValueOrDefault(0);

			TimeZoneInfo timeZoneInfo = GetCompany().TimeZoneInfo;

			DateTime fromDateUtc = TimeZoneInfo.ConvertTimeToUtc(deDateTimeFrom.Value.GetValueOrDefault(), timeZoneInfo);
			DateTime untilDateUtc = TimeZoneInfo.ConvertTimeToUtc(deDateTimeUntil.Value.GetValueOrDefault(), timeZoneInfo);

			if (reportingPeriod != ReportingPeriod.Manual)
			{
				untilDateUtc = untilDateUtc.Date;
			}

			AnalyticsReportType reportType = (AnalyticsReportType) cbReportType.Value.GetValueOrDefault((int) AnalyticsReportType.TransactionsAppLess);

			IList<SpreadSheetFilter> sheetFilters = DataSource.IsNew
				? new[] {CreateFirstSpreadSheetFilter(reportType)}
				: filterDataSource.GetSheetFilters();

			ReportRequest reportRequest = CreateReportRequest(reportType);

			reportRequest.CompanyId = DataSource.CompanyId;
			reportRequest.CompanyTimeZone = GetCompany().TimeZoneOlsonId;
			reportRequest.ReportingPeriod = reportingPeriod;
			reportRequest.UntilDateTimeUtc = untilDateUtc;
			reportRequest.LeadTimeOffset = fromDateUtc.Subtract(untilDateUtc);
			reportRequest.SheetFilters = sheetFilters;

			return reportRequest;
		}

		private SpreadSheetFilter CreateFirstSpreadSheetFilter(AnalyticsReportType reportType)
		{
			ISheetFilterFactory factory = CreateSheetFilterFactory(reportType);
			SpreadSheetFilter spreadSheetFilter = factory.CreateSpreadSheetFilter();
			spreadSheetFilter.Name = "Sheet1";
			return spreadSheetFilter;
		}

		private void CreateManualReportingTask()
		{
			ReportRequest request = CreateRequestFilter();

			if (request.ReportingPeriod != ReportingPeriod.Manual)
			{
				Tuple<DateTime, DateTime> reportingPeriod = ReportingPeriodHelper.GetDateTimeForPeriod(request.ReportingPeriod, GetCompany().TimeZoneInfo);
				request.UntilDateTimeUtc = TimeZoneInfo.ConvertTimeToUtc(reportingPeriod.Item2, GetCompany().TimeZoneInfo);
			}

			if (!cbReportType.Value.HasValue)
			{
				AddInformatorInfo("Please select a report type");
				Validate();
				return;
			}

			AnalyticsReportType reportType = (AnalyticsReportType) cbReportType.Value.Value;

			ReportProcessingTaskEntity task = new ReportProcessingTaskEntity
			{
				CompanyId = request.CompanyId,
				TillUTC = request.UntilDateTimeUtc,
				FromUTC = request.FromDateTimeUtc,
				Filter = JsonConvert.SerializeObject(request, Formatting.Indented),
				AnalyticsReportType = reportType,
				TimeZoneOlsonId = request.CompanyTimeZone
			};

			string friendlyName = !tbFriendlyName.Text.IsNullOrWhiteSpace() ? tbFriendlyName.Text : $"Manual triggered: {task.AnalyticsReportTypeAndPeriod}";
			task.ReportName = $"{friendlyName} ({task.PeriodText})";
			task.Save();

			AddInformatorInfo("Your report is ready to be processed. Click <a href=\"{0}\">here</a> to view its progress.",
				ResolveUrl($"~/Analytics/ReportProcessingTask.aspx?id={task.ReportProcessingTaskId}"));
		}

		private ReportRequest DeserializeOrCreateFilter(AnalyticsReportType reportType) =>
			reportRequestBridgeFactory
				.GetReportRequestRefinedAbstraction(reportType)
				.CreateReportRequest(DataSource.Filter);

		public override void Dispose()
		{
			cbPeriod.ValueChanged -= OnCbPeriodOnValueChanged;
			tlSheetFilter.NodeInserted -= Save;
			tlSheetFilter.NodeDeleted -= Save;
			tlSheetFilter.HtmlDataCellPrepared -= TlSheetFilter_HtmlDataCellPrepared;
			tlSheetFilter.CustomCallback -= TlSheetFilter_CustomCallback;
			DataSourceLoaded -= ReportTemplate_DataSourceLoaded;

			base.Dispose();
		}
	}
}
