﻿<%@ Page Title="Transactions" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.ReportTemplate" Codebehind="ReportTemplate.aspx.cs" %>
<%@ Register TagPrefix="dxwtl" Namespace="DevExpress.Web.ASPxTreeList" Assembly="DevExpress.Web.ASPxTreeList.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">Report Template</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server">
	<span class="toolbar">       
		<X:ToolBarButton runat="server" ID="btGenerateReport" CommandName="GenerateReport" Text="Generate Report" Image-Url="~/Images/Icons/report.png" />
		<X:ToolBarButton runat="server" ID="btGenerateMetadata" CommandName="ShowMetadata" Text="Generate Metadata" Image-Url="~/Images/Icons/cog.png" />
	</span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
	<D:PlaceHolder runat="server">
        <X:PopupControl runat="server" PopupAction="MouseOver" CloseAction="MouseOut" PopupElementID="untilDateHelp" ShowHeader="False" Width="500px">
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContent" runat="server" SupportsDisabledAttribute="True">
                    <div>
                        Depending on the selected reporting period the until time can differ:<br/>
                        * Manual: You manually set the end time<br/>
                        * Today, TradingDay and LastXDays: The end time is the time at which is report is generated (scheduled or manual)<br/>
                        * For all the other periods, the end time will 23:59 of the until day<br/>
                    </div>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </X:PopupControl>
		<div>
			<X:PageControl ID="tabsMain" runat="server" Width="100%">
				<TabPages>
					<X:TabPage Text="Algemeen" Name="Generic">
						<controls>
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">Company</td>
                                    <td class="control"><D:Label runat="server" ID="lblCompanyValue" LocalizeText="False" /></td>
                                    <td class="label">TimeZone</td>
                                    <td class="control"><D:Label runat="server" ID="lblCompanyTimeZone" LocalizeText="False"></D:Label></td>
                                </tr>
                                <tr>
                                    <td class="label">Report name</td>
                                    <td class="control"><D:TextBox runat="server" ID="tbFriendlyName" IsRequired="True"/></td>
                                    <td class="label">Type</td>
                                    <td class="control"><X:ComboBoxInt runat="server" ID="cbReportType" IsRequired="true" DisplayEmptyItem="False"></X:ComboBoxInt></td>
                                </tr>
                                <tr>
                                    <td class="label">Email addresses</td>
                                    <td class="control">
                                        <D:TextBoxMultiLine runat="server" ID="tbEmail" Rows="2"/><br/>
                                        <small>Email addresses to which the scheduled report will be delivered</small>
                                    </td>
                                    <td class="label">&nbsp;</td>
                                    <td class="control">&nbsp;</td>
                                </tr>
                            </table>

                            <D:Panel runat="server" ID="plhReportPeriod" GroupingText="Reporting Period">
                            <table class="dataformV2">
                                <tr>
                                    <td class="label">Period</td>
                                    <td class="control"><X:ComboBoxInt runat="server" ID="cbPeriod" AutoPostBack="True" Width="200" notdirty="true" /></td>
                                    <td class="label">From</td>
                                    <td class="control"><X:DateTimeEdit runat="server" ID="deDateTimeFrom" AllowNull="False"/></td>
                                </tr>
                                    <tr>
                                    <td class="label">&nbsp;</td>
                                    <td class="control">&nbsp;</td>
                                    <td class="label">Until</td>
                                    <td class="control" style="padding-top: 0">
                                        <table>
                                            <td><X:DateTimeEdit runat="server" ID="deDateTimeUntil" AllowNull="false"/></td>
                                            <td style="padding-left: 8px"><a id="untilDateHelp" href="#"><img src="../Images/Icons/information.png" alt="info"/></a></td>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            </D:Panel>
                            
                            <D:Panel runat="server" GroupingText="Report Sheets" ID="pnlSheets">
                                <table class="dataformV2">
                                    <tr>
                                        <td class="label" colspan="3">
                                            <dxwtl:ASPxTreeList ID="tlSheetFilter" ClientInstanceName="tlSheetFilters" runat="server" EnableViewState="False" />
                                        </td>
                                        <td class="control">&nbsp;</td>
                                    </tr>
                                </table>
                                

                            </D:Panel>

					    </controls>
					</X:TabPage>
					<X:TabPage Text="Metadata" Name="Metadata"> 
						<controls>
							<table class="dataformV2">
								<tr>
									<td class="label"><D:Label runat="server" LocalizeText="false">Metadata</D:Label></td>
									<td class="control" colspan="3"><D:TextBoxMultiLine runat="server" ID="tbMetadata" Rows="20"></D:TextBoxMultiLine></td>	  
								</tr>
								<tr>
									<td class="label">&nbsp;</td>
									<td class="control">&nbsp;</td>
									<td class="label">&nbsp;</td>
									<td class="control">&nbsp;</td>
								</tr>
							</table>                        
					</controls>
					</X:TabPage>
				</TabPages>
			</X:PageControl>
		</div>
	</D:PlaceHolder>
</asp:Content>
