﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data;
using Dionysos.Diagnostics;
using Obymobi.Web.Google;
using System.Text;
using System.Data;
using DevExpress.Web;
using Dionysos.Web.UI;
using DevExpress.XtraCharts.Web;
using DevExpress.XtraCharts;
using DevExpress.XtraPrinting;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraPrintingLinks;
using System.IO;
using Dionysos;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Analytics
{
    public partial class Views : PageDefault
    {
        //private enum ReportingType : int
        //{ 
        //    PageViews = 1,
        //    Entertainment = 2,
        //    Orders = 3,
        //    BestSellers = 4,
        //    PopularCategories = 5
        //}

        //#region Fields

        //string columnHeadingPage;
        //string columnHeadingViews;
        //string columnHeadingUnique;
        //WebChartControl chart = null;

        //#endregion

        //#region Methods

        //protected override void OnInit(EventArgs e)
        //{
        //    this.ddlReportType.Items.Add(this.Translate("ReportTypePageViews", "Pagina vertoningen"), (int)ReportingType.PageViews);
        //    this.ddlReportType.Items.Add(this.Translate("ReportTypeEntertainment", "Entertainment gebruik"), (int)ReportingType.Entertainment);
        //    //this.ddlReportType.Items.Add(this.Translate("ReportTypeOrders", "Bestellingen"), (int)ReportingType.Orders);
        //    this.ddlReportType.Items.Add(this.Translate("ReportTypeBestSellers", "Bestsellers"), (int)ReportingType.BestSellers);
        //    this.ddlReportType.Items.Add(this.Translate("ReportTypeCategories", "Populaire Categoriën"), (int)ReportingType.PopularCategories);

        //    base.OnInit(e);
            
        //    if (this.ddlCompanyId.ValidId > 0)
        //    {
        //        DeliverypointCollection deliverypoints = new DeliverypointCollection();
        //        deliverypoints.GetMulti(DeliverypointFields.CompanyId == this.ddlCompanyId.ValidId);

        //        this.ddlDeliverypointId.DataSource = deliverypoints;
        //        this.ddlDeliverypointId.DataBind();

        //        DeliverypointgroupCollection dpgs = new DeliverypointgroupCollection();
        //        dpgs.GetMulti(DeliverypointgroupFields.CompanyId == this.ddlCompanyId.ValidId);

        //        this.ddlDeliverypointgroupId.DataSource = dpgs;
        //        this.ddlDeliverypointgroupId.DataBind();
        //    }            
        //}

        //protected override void SetDefaultValuesToControls()
        //{
        //    this.ddlReportType.Value = (int)ReportingType.PageViews;

        //    if (CmsSessionHelper.CurrentCompanyId > 0)
        //        this.ddlCompanyId.Value = CmsSessionHelper.CurrentCompanyId;

        //    this.deDateFrom.Value = DateTimeUtil.MakeBeginOfDay(DateTime.Now.AddMonths(-1));
        //    this.deDateTo.Value = DateTimeUtil.MakeBeginOfDay(DateTime.Now);
        //}                

        //public WebChartControl getPieChart(string name, int width, int height, KeyValuePair<string, double>[] points)
        //{
        //    // Prepare the basics
        //    chart = new WebChartControl();
        //    chart.EnableViewState = false;
        //    chart.SaveStateOnCallbacks = false;
        //    chart.Legend.Visible = true;
        //    chart.Legend.AlignmentVertical = LegendAlignmentVertical.Center;
        //    chart.Legend.AlignmentHorizontal = LegendAlignmentHorizontal.Right;
        //    chart.BackColor = System.Drawing.Color.Transparent;            
        //    chart.Width = width;
        //    chart.Height = height;            
        //    chart.BorderOptions.Visible = false;
        //    ChartTitle title = new ChartTitle();
        //    title.Text = name;
        //    chart.Titles.Add(title);

        //    Series series = new Series(name, ViewType.Pie);

        //    foreach (var point in points)
        //    {
        //        SeriesPoint newPoint = new SeriesPoint();
        //        newPoint.Values = new double[1] { point.Value };
        //        newPoint.Argument = point.Key;
        //        series.Points.Add(newPoint);
        //    }

        //    PieSeriesView view = new PieSeriesView();
        //    view.Rotation = 90;
        //    view.RuntimeExploding = false;
        //    series.View = view;

        //    PiePointOptions legendPointOptions = new PiePointOptions();
        //    legendPointOptions.PointView = PointView.ArgumentAndValues;
        //    legendPointOptions.ValueNumericOptions.Format = NumericFormat.Percent;
        //    legendPointOptions.ValueNumericOptions.Precision = 0;
        //    series.LegendPointOptions = legendPointOptions;

        //    series.Label.PointOptions.ValueNumericOptions.Format = NumericFormat.Percent;
        //    series.Label.PointOptions.ValueNumericOptions.Precision = 0;

        //    chart.CustomDrawSeriesPoint += new CustomDrawSeriesPointEventHandler(chart_CustomDrawSeriesPoint);

        //    chart.Series.Add(series);
        //    return chart;
        //}

        //private void ShowPageViewsReport()
        //{
        //    // Columns unique to this report
        //    Dionysos.Collections.OrderedDictionary<string, Type> columns = new Dionysos.Collections.OrderedDictionary<string,Type>();
        //    columns.Add(this.Translate("ColumnHeadingPage", "Pagina"), typeof(string));
        //    columns.Add(this.Translate("ColumnHeadingViews", "Vertoningen"), typeof(int));
        //    columns.Add(this.Translate("ColumnHeadingUnique", "Unieke Vertoningen"), typeof(int));            

        //    var dataTable = this.GetReportData(columns, "ga:pageviews,ga:uniquePageviews", "ga:pagePath", string.Empty, "-ga:pageviews");

        //    if (dataTable != null)
        //        this.DataBindGridAndChart(columns, dataTable);
        //    else            
        //        this.viewsGrid.Visible = false;            
        //}
    
        //private void ShowEntertainmentReport()
        //{
        //    // Columns unique to this report
        //    Dionysos.Collections.OrderedDictionary<string, Type> columns = new Dionysos.Collections.OrderedDictionary<string, Type>();
        //    columns.Add(this.Translate("ColumnHeadingEntertainment", "Entertainment"), typeof(string));
        //    columns.Add(this.Translate("ColumnHeadingViews", "Vertoningen"), typeof(int));

        //    var dataTable = this.GetReportData(columns, "ga:totalEvents", "ga:eventLabel", "ga:eventAction==View Entertainment", "-ga:totalEvents");

        //    if(dataTable != null)
        //        this.DataBindGridAndChart(columns, dataTable);
        //    else
        //        this.viewsGrid.Visible = false;
        //}

        //private void ShowBestSellersReport()
        //{
        //    // Columns unique to this report
        //    Dionysos.Collections.OrderedDictionary<string, Type> columns = new Dionysos.Collections.OrderedDictionary<string, Type>();
        //    columns.Add(this.Translate("ColumnHeadingProduct", "Product"), typeof(string));
        //    columns.Add(this.Translate("ColumnHeadingAmount", "Aantal"), typeof(int));

        //    // Bestsellers
        //    DataTable bestsellers = AnalyticsHelper.getBestsellers(this.ddlCompanyId.ValidId, this.GetSelectedDeliverypointIds(), 10, this.deDateFrom.Value.Value, this.deDateTo.Value.Value);

        //    // Pretty up column names
        //    int columnI = 0;
        //    foreach(var column in columns)
        //    {
        //        bestsellers.Columns[columnI].ColumnName = column.Key;
        //        columnI++;
        //    }

        //    if(bestsellers.Rows.Count > 0)                
        //        this.DataBindGridAndChart(columns, bestsellers);
        //    else                            
        //        this.viewsGrid.Visible = false;
        //}

        //private void ShowPopulairCategoriesReport()
        //{
        //    Dionysos.Collections.OrderedDictionary<string, Type> columns = new Dionysos.Collections.OrderedDictionary<string, Type>();
        //    columns.Add(this.Translate("ColumnHeadingProduct", "Categorie"), typeof(string));
        //    columns.Add(this.Translate("ColumnHeadingAmount", "Bestellingen"), typeof(int));
        //    columns.Add(this.Translate("ColumnHeadingViews", "Weergaves"), typeof(int));

        //    // Most popular categories
        //    DataTable topCategories = AnalyticsHelper.getTopCategories(this.ddlCompanyId.ValidId, this.GetSelectedDeliverypointIds(), 10, this.deDateFrom.Value.Value, this.deDateTo.Value.Value);
        //    DataColumn viewsColumn = new DataColumn("Views", typeof(int));
        //    topCategories.Columns.Add(viewsColumn);
            

        //    if (topCategories.Rows.Count > 0)
        //    {
        //        // Search the categories in Google Analytics, filter:
        //        string categoryFilter = string.Empty;
        //        for (int i = 0; i < topCategories.Rows.Count; i++)
        //        {
        //            if (categoryFilter.Length > 0)
        //                categoryFilter += ",";

        //            categoryFilter += string.Format("ga:eventLabel=@[{0}]", topCategories.Rows[i][0]);
        //        }

        //        // Get data from Google
        //        var categoryViews = this.GetGoogleData("ga:totalEvents", "ga:eventAction,ga:eventLabel", string.Format("ga:eventAction==Select Category;{0}", categoryFilter), string.Empty);
                
        //        // Match Google data with our Database data
        //        for (int i = 0; i < topCategories.Rows.Count; i++)
        //        {               
        //            string categoryId = string.Format("[{0}]", topCategories.Rows[i][0]);
        //            var googleResult = categoryViews.Rows.FirstOrDefault(r => r[1].Contains(categoryId));

        //            if (googleResult != null)
        //                topCategories.Rows[i][3] = Convert.ToInt32(googleResult[2]);
        //        }
        //    }

        //    // Remove first column, == CategoryId
        //    topCategories.Columns.RemoveAt(0);

        //    // Pretty up the remaining column names
        //    int columnI = 0;
        //    foreach (var column in columns)
        //    {
        //        topCategories.Columns[columnI].ColumnName = column.Key;
        //        columnI++;
        //    }

        //    // Render the Chart & Grid
        //    if (topCategories.Rows.Count > 0)
        //        this.DataBindGridAndChart(columns, topCategories);
        //    else
        //        this.viewsGrid.Visible = false;
        //}

        //private DataTable GetReportData(Dionysos.Collections.OrderedDictionary<string, Type> columnNames, string metrics, string dimension, string filter, string sorter)
        //{
        //    // Retrieve Data
        //    var googleData = this.GetGoogleData(metrics, dimension, filter, sorter);

        //    // Return when we have no results
        //    if (googleData == null || googleData.TotalResults == 0)
        //    {
        //        this.AddInformatorInfo(this.Translate("WithinThePeriodAndFiltersThereAreNoResults", "Binnen door u gekozen periode en/of filters zijn geen gegevens gevonden"));                
        //        return null;
        //    }

        //    // Create Datatable & Chart Data            
        //    DataTable dt = new DataTable();
        //    KeyValuePair<string, double>[] pieValues;

        //    // Create Columns in DataTable                      
        //    foreach (var column in columnNames)
        //    {
        //        var dataColumn = dt.Columns.Add(column.Key);
        //        dataColumn.DataType = column.Value;
        //    }            

        //    // Prepare a DataTable for the DataGrid & Set of PieValues for the Chart            
        //    for (int i = 0; i < googleData.TotalResults; i++)
        //    {
        //        // Add row to data tablse
        //        DataRow row = dt.NewRow();

        //        // The first data is always 'special'
        //        string valuePrettiedUp;
        //        if (googleData.Rows[i][0].IndexOf("]") > 0)
        //            valuePrettiedUp = googleData.Rows[i][0].GetAllAfterFirstOccurenceOf("]").Trim();
        //        else
        //            valuePrettiedUp = googleData.Rows[i][0];
        //        row[0] = valuePrettiedUp;

        //        // Copy all other values after the first from Google Data to DataTable
        //        for (int j = 1; j < columnNames.Count; j++)
        //        {
        //            // Always try to parse it to an int, for sorting in the grid
        //            int value;
        //            if (int.TryParse(googleData.Rows[i][j], out value))
        //                row[j] = value;
        //            else
        //                row[j] = googleData.Rows[i][j];
        //        }

        //        dt.Rows.Add(row);
        //    }

        //    return dt;
        //}

        //private void DataBindGridAndChart(Dionysos.Collections.OrderedDictionary<string, Type> columns, DataTable dt)
        //{
        //    KeyValuePair<string, double>[] pieValues;
        //    // Create the PieValues (first column as identifier & second column as value)
        //    double otherValue = 0;
        //    pieValues = new KeyValuePair<string, double>[System.Math.Min(11, dt.Rows.Count)];
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        DataRow row = dt.Rows[i];
        //        double value = double.Parse(row[1].ToString());
        //        // Add Pie values (only show 10 items, rest will be accumulated under Other
        //        if (i < 10)
        //            pieValues[i] = new KeyValuePair<string, double>(row[0].ToString(), value);
        //        else
        //            otherValue += value;
        //    }

        //    // Add the 'Other value' of the accumlated items 10+
        //    if (otherValue > 0)
        //        pieValues[10] = new KeyValuePair<string, double>("Other", otherValue);

        //    // Set up the colums of the Gridview
        //    this.viewsGrid.Columns.Clear();
        //    foreach (var column in columns)
        //    {
        //        this.viewsGrid.Columns.Add(new GridViewDataColumn(column.Key));
        //    }

        //    // Bind Data to Grid
        //    this.viewsGrid.DataSource = dt;
        //    this.viewsGrid.DataBind();

        //    // Prepare to create the Chart
        //    string name = this.Translate("AllDeliverypoints", "Alle tafels / kamers");
        //    if (this.ddlDeliverypointgroupId.ValidId > 0)
        //    {
        //        string deliverypointgroupText = this.Translate("Deliverypoint", "Tafel / kamer groep '{0}'");
        //        name = string.Format(deliverypointgroupText, this.ddlDeliverypointgroupId.Text);
        //    }
        //    else if (this.ddlDeliverypointId.ValidId > 0)
        //    {
        //        string deliverypointText = this.Translate("Deliverypoint", "Tafel / kamer '{0}'");
        //        name = string.Format(deliverypointText, this.ddlDeliverypointId.Text);
        //    }

        //    this.chart = getPieChart(name, 1000, 500, pieValues);
        //    this.plhOverview.Controls.Add(chart);            
        //}

        //private GaData GetGoogleData(string metrics, string dimensions, string additionalFilter, string sorter)
        //{
        //    StringBuilder builder = new StringBuilder();
        //    GaData toReturn = null;
        //    CompanyEntity company = new CompanyEntity(this.ddlCompanyId.ValidId);

        //    string cacheKey = string.Format("Views.aspx.cs.{0}.{1}.{2}.{3}.{4}", metrics, dimensions, additionalFilter, sorter, this.ddlCompanyId.ValidId);

        //    // Cache for speed             
        //    if (!company.IsNew)
        //    {
        //        if (Dionysos.Web.CacheHelper.TryGetValue(cacheKey, false, out toReturn))
        //        { 
        //            // From cache
        //        }
        //        else if (company.GoogleAnalyticsId.Length > 0)
        //        {
        //            GoogleAPI google = new GoogleAPI();
        //            if (google.SetProfile(company.GoogleAnalyticsId))
        //            {
        //                DateTime dateFrom = this.deDateFrom.Value.Value;
        //                DateTime dateTo = this.deDateTo.Value.Value;

        //                string deliverypointsFilter = string.Empty;
        //                if (this.ddlDeliverypointgroupId.ValidId > 0)
        //                {
        //                    DeliverypointgroupEntity dpg = new DeliverypointgroupEntity(this.ddlDeliverypointgroupId.ValidId);
        //                    foreach (var deliverypoint in dpg.DeliverypointCollection)
        //                    {
        //                        deliverypointsFilter = StringUtil.CombineWithSeperator(",", deliverypointsFilter, string.Format("ga:customVarValue2=={0}", deliverypoint.Number));
        //                    }
        //                }
        //                else if (this.ddlDeliverypointId.ValidId > 0)
        //                {
        //                    deliverypointsFilter = string.Format("ga:customVarValue2=={0}", this.ddlDeliverypointId.Text);
        //                }

        //                // , = OR | ; = AND
        //                string filter = StringUtil.CombineWithSemicolon(deliverypointsFilter, additionalFilter);

        //                toReturn = google.Get(metrics, dimensions, filter, sorter, dateFrom, dateTo);
        //                Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, cacheKey, toReturn, 30);
        //            }
        //            else
        //            {
        //                string message = this.Translate("GoogleAnalyticsProfileCouldNotBeOpened", "Google Analytics profile '{0}' could not be opened.");
        //                this.MultiValidatorDefault.AddError(string.Format(message, company.GoogleAnalyticsId));
        //            }
        //        }
        //        else
        //        {
        //            this.MultiValidatorDefault.AddError(this.Translate("NoGoogleAnalyticsIdConfigured", "Er is geen Google Analytics ID ingesteld voor dit bedrijf."));
        //        }
        //    }
        //    else
        //    {
        //        this.MultiValidatorDefault.AddError(this.Translate("NoCompanySelected", "Er is geen bedrijf gekozen."));
        //    }

        //    return toReturn;
        //}

        //private List<int> GetSelectedDeliverypointIds()
        //{
        //    List<int> deliverypointIds = null;
        //    if (this.ddlDeliverypointgroupId.ValidId > 0)
        //    {
        //        DeliverypointgroupEntity dpg = new DeliverypointgroupEntity(this.ddlDeliverypointgroupId.ValidId);
        //        if (dpg.DeliverypointCollection.Count > 0)
        //            deliverypointIds = dpg.DeliverypointCollection.Select(dp => dp.DeliverypointId).ToList();
        //    }
        //    else if (this.ddlDeliverypointId.ValidId > 0)
        //    {
        //        deliverypointIds = new List<int> { this.ddlDeliverypointId.ValidId };
        //    }

        //    return deliverypointIds;
        //}

        //#endregion

        //#region Event Handlers

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    this.btnRefresh.Click += btnRefresh_Click;

        //    // GK Removed, reasons this commented method btnExport_Click
        //    // this.btnExport.Click += new EventHandler(btnExport_Click);

        //    this.Validate();
        //    if (!this.IsValid)
        //    {
        //        this.plhOverview.Visible = false;
        //        this.viewsGrid.Visible = false;
        //    }
        //    else if (this.ddlCompanyId.Value.HasValue && this.ddlReportType.ValidId > 0)
        //    {
        //        var reportType = this.ddlReportType.ValidId.ToEnum<ReportingType>();

        //        switch (reportType)
        //        {
        //            case ReportingType.PageViews:
        //                this.ShowPageViewsReport();
        //                break;
        //            case ReportingType.Entertainment:
        //                this.ShowEntertainmentReport();
        //                break;
        //            case ReportingType.BestSellers:
        //                this.ShowBestSellersReport();
        //                break;
        //            case ReportingType.PopularCategories:
        //                this.ShowPopulairCategoriesReport();
        //                break;
        //            default:
        //                throw new NotImplementedException();                        
        //        }                
        //    }
        //}

        //void btnRefresh_Click(object sender, EventArgs e)
        //{
        //    this.RefreshWithControlValues();
        //}

        //// GK Removed, worked like crap. If suggest: Go to Google Analytics for reports
        //// and if the feature gets requested again we should redo it using SpreadsheetLight
        //// that can also use Charts.
        ////
        ////void btnExport_Click(object sender, EventArgs e)
        ////{
        ////    PrintingSystem ps = new PrintingSystem();

        ////    ASPxGridViewExporter gridExporter = new ASPxGridViewExporter();
        ////    gridExporter.GridViewID = this.viewsGrid.ID;
        ////    Form.Controls.Add(gridExporter);

        ////    //PrintableComponentLink link1 = new PrintableComponentLink();
        ////    //link1.Component = ((IChartContainer)this.chart).Chart;
        ////    //link1.PrintingSystem = ps;

        ////    PrintableComponentLink link2 = new PrintableComponentLink();
        ////    link2.Component = gridExporter;
        ////    link2.PrintingSystem = ps;

        ////    CompositeLink compositeLink = new CompositeLink();
        ////    compositeLink.Links.AddRange(new object[] { link2 });
        ////    compositeLink.PrintingSystem = ps;

        ////    compositeLink.CreateDocument();
        ////    compositeLink.PrintingSystem.ExportOptions.Pdf.DocumentOptions.Author = "Crave";
        ////    using (MemoryStream stream = new MemoryStream())
        ////    {
        ////        compositeLink.PrintingSystem.ExportToXls(stream);
        ////        Response.Clear();
        ////        Response.Buffer = false;
        ////        Response.AppendHeader("Content-Type", "application/vnd.ms-excel");
        ////        Response.AppendHeader("Content-Transfer-Encoding", "binary");
        ////        Response.AppendHeader("Content-Disposition", "attachment; filename=test.xls");
        ////        Response.BinaryWrite(stream.GetBuffer());
        ////        Response.End();
        ////    }

        ////    ps.Dispose();
        ////}

        //void chart_CustomDrawSeriesPoint(object sender, CustomDrawSeriesPointEventArgs e)
        //{
        //    e.LegendText = e.SeriesPoint.Argument;// +e.SeriesPoint.Values[0];
        //}

        //#endregion
    }
}