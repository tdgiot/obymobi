﻿using System;
using System.Linq;
using Dionysos;
using Obymobi.Enums;
using Obymobi.ObymobiCms.Analytics.Subpanels;
using Newtonsoft.Json;
using Obymobi.Data.EntityClasses;
using Dionysos.Web;
using System.IO;
using System.Text;
using Obymobi.Analytics;
using Obymobi.Analytics.Google;
using Google.Apis.Auth.OAuth2;
using System.Threading.Tasks;
using System.Collections.Generic;
using Obymobi.Analytics.Reports;
using Obymobi.Analytics.KpiFunctions;

namespace Obymobi.ObymobiCms.Analytics
{
    public partial class KpiReport : BaseReportingPage
    {
        private ReportFilterPanel filterPanel;
        private ReportProcessingTaskEntity copyFromTask = null;

        protected override void OnInit(EventArgs e)
        {
            this.tbName.Text = "KPI - {0} - {1}".FormatSafe(CmsSessionHelper.CurrentCompanyName, DateTime.Now.Ticks);

            LoadGoogleViews();

            base.OnInit(e);
        }

        private void LoadGoogleViews()
        {
            var views = GoogleManagementApiCache.GetFlattenedViewInfos();
            if (views != null)
            {
                if (views.Count == 0)
                {
                    this.AddInformatorInfo("Page can't be used - No Analytics Accounts are linked to your token");
                }
                else
                {
                    // Great
                    foreach (var view in views.OrderBy(x => x.Name))
                    {
                        this.ddlGoogleViewId.Items.Add(view.Name, view.ViewId);
                    }

                    this.ddlGoogleViewId.Value = AccountConfiguration.GetByCloudEnvironment(WebEnvironmentHelper.CloudEnvironment).ViewId;
                }

            }
            else if (GoogleManagementApiCache.Status == GoogleManagementApiCacheStatus.Failed)
            {
                this.AddInformatorInfo("Page can't be used - Analytics Accounts can't be loaded: " + GoogleManagementApiCache.LoadingErrorText);
            }
            else if (GoogleManagementApiCache.Status == GoogleManagementApiCacheStatus.Loading)
            {
                this.AddInformatorInfo("Page can't be used yet - The Analytics Accounts are still being loaded.");
            }
            else
            {
                this.AddInformatorInfo("Page can't be used yet - The Analytics Accounts can't be loaded: " + GoogleManagementApiCache.LoadingErrorText);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.filterPanel != null)
            {
                this.filterPanel.UpdateCompanyId();
            }            

            if (!this.IsPostBack)
            {
                this.MultiValidatorDefault.ValidationGroup = "Informators";
                this.Validate("Informators");
            }

            this.btGenerateDoIt.Visible = (TestUtil.IsPcDeveloper && WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.Manual);

            // For development purposes
            if (QueryStringHelper.HasValue("dashboard"))
            {
                this.btShowDashboard.Visible = true;
                this.btGenerateDoIt.Visible = false;
                this.btQueueReport.Visible = false;
                this.fuReportTemplate.IsRequired = false;
            }

            int copyFromId;
            if (QueryStringHelper.TryGetValue("copyFrom", out copyFromId) && copyFromId > 0)
            {
                this.copyFromTask = new ReportProcessingTaskEntity(copyFromId);
                if (this.copyFromTask.IsNew)
                    throw new Exception("Invalid ReportProcessingTaskEntity");

                this.fuReportTemplate.Visible = false;
                this.fuReportTemplate.IsRequired = false;
                this.lblReportTemplateCopied.Visible = true;
                                                             
                var metadata = JsonConvert.DeserializeObject<Obymobi.Analytics.Reports.KpiReportMetadata>(this.copyFromTask.Filter);

                this.tbName.Text = metadata.Name;
                this.cbIncludeActualFilters.Checked = metadata.IncludeActualFilters;                
                this.cbIncludeStackTracesWithErrors.Checked = metadata.IncludeStackTracesWithErrors;
                this.cbAllowAllCompanies.Checked = !metadata.CompanyId.HasValue;
            }            
        }

        protected override void SetDefaultValuesToControls()
        {
            this.filterPanel = this.LoadControl<ReportFilterPanel>("~/Analytics/Subpanels/ReportFilterPanel.ascx");
            this.filterPanel.ShowFilterCategories = false;
            this.filterPanel.ShowFilterDeliverypoints = false;
            this.filterPanel.ShowFilterDevices = false;
            this.filterPanel.ShowFilterOrders = false;
            this.filterPanel.ShowFilterProduct = false;
            this.filterPanel.ShowLoadDeliverypointsButton = false;
            this.filterPanel.ShowDeliverypointgroups = false;
            this.filterPanel.Period = ReportFilterPanel.ReportingPeriod.LastMonth;
            this.phlFilter.Controls.Add(this.filterPanel);

            this.plhAdvancedFeatures.Visible = (CmsSessionHelper.CurrentRole >= Role.Administrator);
            this.cbIncludeActualFilters.Checked = !WebEnvironmentHelper.CloudEnvironmentIsProduction;
            this.cbIncludeStackTracesWithErrors.Checked = !WebEnvironmentHelper.CloudEnvironmentIsProduction;            
        }

        protected override void BindQueryStringToControls()
        {
            base.BindQueryStringToControls();
            this.filterPanel.UpdateCompanyId();

            int taskId;
            if (QueryStringHelper.TryGetValue("copyFrom", out taskId))
            {
                ReportProcessingTaskEntity task = new ReportProcessingTaskEntity(taskId);
            }
        }

        public void ValidateSelf()
        {
            this.filterPanel.Validate();
            base.Validate();
        }

        public void GenerateReportDoIt()
        {
            this.GenerateReport(true);
        }

        public void GenerateReport()
        {
            this.GenerateReport(false);
        }

        public void GenerateReport(bool doIt, bool metaDataOnly = false)
        {
            this.Validate();
            if (!this.IsValid)
                return;

            // Default normal production case
            using (MemoryStream ms = new MemoryStream())
            {
                if (this.copyFromTask != null)
                {
                    if (this.copyFromTask.ReportProcessingTaskTemplateCollection.Count != 1)
                        throw new Exception("Template to copy from doesn't contain a template file");
                    byte[] templateFile = this.copyFromTask.ReportProcessingTaskTemplateCollection[0].TemplateFile;
                    ms.Write(templateFile, 0, templateFile.Length);
                }
                else
                    this.fuReportTemplate.FileContent.CopyTo(ms);

                try
                {
                    var metadata = new Obymobi.Analytics.Reports.KpiReportMetadata();
                    if ((CmsSessionHelper.CurrentRole >= Role.Administrator) && this.cbAllowAllCompanies.Checked)
                    {
                        // No need to mention a CompanyId
                    }
                    else
                        metadata.CompanyId = CmsSessionHelper.CurrentCompanyId;                    
                    metadata.IncludeActualFilters = this.cbIncludeActualFilters.Checked;
                    metadata.IncludeStackTracesWithErrors = this.cbIncludeStackTracesWithErrors.Checked;
                    metadata.GoogleAnalyticsViewId = this.ddlGoogleViewId.SelectedValueString;
                    metadata.Name = this.tbName.Text;                    

                    ReportProcessingTaskEntity task = new ReportProcessingTaskEntity();
                    DateTime fromUtc, tillUtc;
                    this.filterPanel.GetPeriod(out fromUtc, out tillUtc);
                    task.FromUTC = fromUtc;
                    task.TillUTC = tillUtc;
                    task.TimeZoneOlsonId = this.filterPanel.TimeZoneOlsonId;
                    task.CompanyId = this.filterPanel.CompanyId;
                    task.AnalyticsReportType = AnalyticsReportType.Kpi;
                    task.Filter = JsonConvert.SerializeObject(metadata);

                    ReportProcessingTaskTemplateEntity template = new ReportProcessingTaskTemplateEntity();
                    template.TemplateFile = ms.ToArray();
                    template.CreatedBy = CmsSessionHelper.CurrentUser.UserId;
                    template.UpdatedBy = CmsSessionHelper.CurrentUser.UserId;
                    task.ReportProcessingTaskTemplateCollection.Add(template);

                    var kpireport = new Obymobi.Analytics.Reports.KpiReport(task, WebEnvironmentHelper.GoogleApisJwt);

                    if (doIt)
                    {
                        // Direct processing
                        var output = kpireport.RunReport(false);
                        Response.Clear();
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("Content-Disposition", "attachment; filename=KpiReport-{0}-{1}-{2}.xlsx".FormatSafe(new CompanyEntity(CmsSessionHelper.CurrentCompanyId).Name, this.tbName.Text, DateTime.Now.DateTimeToSimpleDateTimeStamp()));
                        output.SaveAs(Response.OutputStream);
                        Response.End();
                    }
                    else
                    {
                        // Queue for processing by /Services/ 
                        var output = kpireport.RunReport(true);
                        task.CreatedBy = CmsSessionHelper.CurrentUser.UserId;
                        task.UpdatedBy = CmsSessionHelper.CurrentUser.UserId;
                        task.Save(true);
                        this.Response.Redirect("~/Analytics/ReportProcessingTask.aspx?id=" + task.ReportProcessingTaskId);
                    }
                }                
                catch (Exception ex)
                {
                    this.MultiValidatorDefault.AddError("Invalid report: " + ex.Message);
                    this.Validate();
                    return;
                }
            }
        }


        /// <summary>
        /// GAKR I've implemented this method as an example for Robin to build a Client-Side Dashboard.
        /// </summary>
        public void ShowDashboard()
        {
            this.plhEmbedApi.Visible = true;

            // ----------------------------------------
            // AUTHORIZATION - Server Side - ClientId  
            // ----------------------------------------
            // There's a security issue with the current approach (which I couldn't fix in one night), if someone steals the ClientId from the Source they can retrieve
            // anything that that account has access too. So this is a wide open security whole
            // It would be prettier to have users to use their own Google Accounts to acces GA data, but for the short term I think that's too much hassle.
            // also in the case that we would want to continue to use Server-side tokens than each company should upload their own JSON token and we should
            // use that for the signed on user that belongs to that company.           
            // https://ga-dev-tools.appspot.com/embed-api/server-side-authorization/
            // http://stackoverflow.com/questions/32954236/get-token-from-client-email-and-private-key-service-account-in-google                                    
            string token;

            // 
            if (!CacheHelper.TryGetValue("kpireport.googletoken", false, out token))
            {
                GoogleCredential credential;
                using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(WebEnvironmentHelper.GoogleApisJwt)))
                {
                    // https://cloud.google.com/storage/docs/json_api/v1/json-api-dotnet-samples
                    credential = GoogleCredential.FromStream(stream);
                    if (credential.IsCreateScopedRequired)
                        credential = credential.CreateScoped("https://www.googleapis.com/auth/analytics.readonly");

                    Task<string> task = ((ITokenAccess)credential).GetAccessTokenForRequestAsync();
                    task.Wait();
                    token = task.Result;
                }

                CacheHelper.AddAbsoluteExpire(false, "kpireport.googletoken", token, 60); // I don't exactly know how long you can (re)use a token.
            }
                
            string authorizeJs = @"
                                    gapi.analytics.auth.authorize({
                                        'serverAuth': {
                                                    'access_token': '[[TOKEN]]'
                                        }
                                    });".Replace("[[TOKEN]]", token);
            this.plhEmbedApiAuthorize.AddJavascript(authorizeJs, false);

            // ----------------------------------------
            // Render the Components
            // ----------------------------------------
            // For now I've limited myself to ga:totalEvents queries, another one could be screenViews, in that case you must use metric: ga:screenviews
            // For each requested query I will render a 'KPI', i.e. a single total for the full interval and the chart

            // Setup a stack of Kpi solving things, mainly to just conver the function format to a filter class.
            var categoryFetcher = new ChildCategoryFetcher();
            // The class that can retrieve Kpi's
            var googleAnalyticsKpiRetriever = new GoogleAnalyticsKpiRetriever(WebEnvironmentHelper.GoogleApisJwt, this.ddlGoogleViewId.Value);
            // The class that translates my 'function format' to method calls
            var googleKpiProcessor = new Obymobi.Analytics.Google.GoogleAnalyticsKpiFunctionProcessor(googleAnalyticsKpiRetriever, categoryFetcher);
            // A generic KpiFunctionHelper (as we also have Transactions functions)
            KpiFunctionHelper kpiFunctionHelper = new KpiFunctionHelper(new List<IKpiFunctionProcessor> { googleKpiProcessor });
            
            DateTime fromUtc, tillUtc;
            this.filterPanel.GetPeriod(out fromUtc, out tillUtc);
            var functions = this.getFunctions();
            int sequence = 0;
            foreach (var function in functions)
            {
                // Get a Filter
               var filter = kpiFunctionHelper.GetFilter(function, fromUtc, tillUtc) as Obymobi.Analytics.Google.Filter;
                string filterExpression = filter.GetFilterExpression();

                this.plhEmbedApiContainers.AddHtml("<strong>{0}</strong>", function);

                // Render HTML/Javascript to fetch and show the 'KPI' (the simple total number)
                this.plhEmbedApiContainers.AddHtml("<div id=\"kpicontainer-report{0}\"></div>", sequence);
                this.plhRetrievalScript.AddJavascript(this.getKpiJavascript(sequence, filterExpression, fromUtc, tillUtc), false);

                // Render HTML/Javascript to fetch and show the chart
                this.plhEmbedApiContainers.AddHtml("<div id=\"chartcontainer-chart{0}\"></div>", sequence);
                this.plhRetrievalScript.AddJavascript(this.getChartJavascript(sequence, filterExpression, fromUtc, tillUtc), false);

               sequence++;
            }
        }

        private string getKpiJavascript(int sequence, string filterExpression, DateTime from, DateTime till)
        {
            string script = @"
            var [[REPORTNAME]] = new gapi.analytics.report.Data({
	            query: {	
		            [[QUERY]]
	            }
            });

            [[REPORTNAME]].on('success', function handleCoreAPIResponse(resultsAsObject) {
				                        
	            var total = 0;				                        
	            if (resultsAsObject.rows.length > 0) {

		            resultsAsObject.rows.forEach(function calculateTotal(row) {
			            total += parseInt(row[1]);
		            });					                        
	            }
                                        
	            var container = document.getElementById('kpicontainer-[[REPORTNAME]]');
	            var strongElement = document.createElement('strong');
	            strongElement.appendChild(document.createTextNode('Total: ' + total));
	            container.appendChild(strongElement);
            })		  

            [[REPORTNAME]].execute();
            ";

            script = script.Replace("[[REPORTNAME]]", "report" + sequence);
            script = script.Replace("[[QUERY]]", this.getQueryForFunction(from, till, filterExpression));
            return script;
        }

        private string getChartJavascript(int sequence, string filterExpression, DateTime from, DateTime till)
        {
            string script = @"
		        var [[CHARTNAME]] = new gapi.analytics.googleCharts.DataChart({
		            query: {
		            [[QUERY]]
			    },
			    chart: {
			        container: 'chartcontainer-[[CHARTNAME]]',
			        type: 'LINE',
			        options: {
				    width: '100%'
			        }
			    }
		        });
                        
                [[CHARTNAME]].execute();
            ";

            script = script.Replace("[[CHARTNAME]]", "chart" + sequence);
            script = script.Replace("[[QUERY]]", this.getQueryForFunction(from, till, filterExpression));

            return script;
        }

        private string getQueryForFunction(DateTime from, DateTime till, string filterExpression)
        {
            StringBuilder sb = new StringBuilder();

            if (filterExpression.EndsWith(";"))
                filterExpression = filterExpression.Substring(0, filterExpression.Length - 1);

            sb.AppendFormatLine("ids: 'ga:{0}',", this.ddlGoogleViewId.Value);
            sb.AppendFormatLine("metrics: 'ga:totalEvents',");
            sb.AppendFormatLine("filters: '{0}',", filterExpression.Replace("'", "\\'"));
            sb.AppendFormatLine("dimensions: 'ga:date',");
            sb.AppendFormatLine("'start-date': '{0:yyyy-MM-dd}',", from);
            sb.AppendFormatLine("'end-date': '{0:yyyy-MM-dd}'", till);

            return sb.ToString();
        }

        private List<string> getFunctions()
        {
            List<string> kpiFunctions = new List<string>();
            //kpiFunctions.Add("EventCount(EventActions[Company tab viewed, Product viewed, Category Details Shown, Attachment viewed, Native page viewed, Entertainment viewed]; CompanyIds[343]);");
            //kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Room Controls];)");
            //kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Widgets]; EventActions[Scene clicked]; EventLabels[Scene: 'All Off'])");
            //kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Features]; EventActions[Estimated delivery time viewed];)");
            //kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Features]; EventActions[Disable privacy dialog shown];)");
            //kpiFunctions.Add("EventCount(EventActions[Section Viewed, Area changed]; CompanyIds[343]);");
            kpiFunctions.Add("EventCount(EventActions[Product Viewed, Category Details shown]; CategoryIds[0, 22586]; CompanyIds[343])");
            kpiFunctions.Add("EventCount(EventActions[Company tab viewed]; EventLabels[Home, Cart]; CompanyIds[343])");
            kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Widgets]; EventActions[Link clicked, Scene clicked];)");
            kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Advertising]; EventActions[Advertisement clicked];)");
            kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Room Controls]; EventActions[Wake up timer changed]; EventLabels[Enabled])");
            kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Room Controls]; EventActions[Wake up timer changed]; EventLabels[Disabled])");
            //kpiFunctions.Add("EventCount(CompanyIds[343]; EventCategories[Room Controls]; EventActions[Wake up timer ran];)");
            //kpiFunctions.Add("EventCount(EventActions[Company tab viewed]; CompanyIds[343])");
            //kpiFunctions.Add("EventCount(EventActions[Product Viewed, Category Details Shown]; CompanyIds[343])");
            //kpiFunctions.Add("EventCount(EventActions[Section Viewed, Area changed]; CompanyIds[343])");
            //kpiFunctions.Add("EventCount(EventActions[Advertisement viewed]; CompanyIds[343])");
            //kpiFunctions.Add("EventCount(EventActions[Attachment viewed]; CompanyIds[343])");
            //kpiFunctions.Add("EventCount(EventActions[Message viewed]; CompanyIds[343])");
            //kpiFunctions.Add("EventCount(EventActions[Browser view]; CompanyIds[343])");
            //kpiFunctions.Add("EventCount(EventActions[Native page viewed]; CompanyIds[343]);");
            //kpiFunctions.Add("EventCount(EventActions[Entertainment viewed]; CompanyIds[343]);");
            //kpiFunctions.Add("EventCount(EventActions[Company tab viewed, Product viewed, Category Details Shown, Section Viewed, Area changed, Advertisement viewed, Attachment viewed, Message viewed, Browser view, Native page viewed, Entertainment viewed]; CompanyIds[343]);");
            return kpiFunctions;
        }
    }

}