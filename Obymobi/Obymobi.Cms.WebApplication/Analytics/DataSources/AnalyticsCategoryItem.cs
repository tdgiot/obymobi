﻿namespace Obymobi.ObymobiCms.Analytics.DataSources
{
    public class AnalyticsCategoryItem
    {
        public string CategoryId { get; set; }
        public string ParentCategoryId { get; set; }
        public string Name { get; set; }
    }
}