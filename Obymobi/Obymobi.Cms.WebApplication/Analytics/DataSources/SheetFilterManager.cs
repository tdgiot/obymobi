﻿using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Requests.RequestBase;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Obymobi.Web.Analytics.Factories;

namespace Obymobi.ObymobiCms.Analytics.DataSources
{
	public class SheetFilterManager
	{
		private readonly IReportRequest request;
		private readonly ISheetFilterFactory sheetFilterFactory;

		public SheetFilterManager(IReportRequest request, ISheetFilterFactory sheetFilterFactory)
		{
			this.request = request;
			this.sheetFilterFactory = sheetFilterFactory;
		}

		[DataObjectMethod(DataObjectMethodType.Select, true)]
		public IEnumerable Select()
		{
			return this.DataSource.OrderBy(x => x.SortOrder);
		}

		[DataObjectMethod(DataObjectMethodType.Insert, true)]
		public void Insert(string name, int id)
		{
			int index = this.DataSource.Count == 0 ? 1 : this.DataSource.Max(x => x.Id) + 1;
			int sortOrder = this.DataSource.Count == 0 ? 1 : this.DataSource.Max(x => x.SortOrder) + 1;

			this.DataSource.Add(sheetFilterFactory.CreateSpreadSheetFilter(index, name, sortOrder));
		}

		[DataObjectMethod(DataObjectMethodType.Update, true)]
		public void Update(string name, int id)
		{
			SpreadSheetFilter filter = this.DataSource.FirstOrDefault(x => x.Id == id);
			if (filter != null)
			{
				filter.Name = name;
			}
		}

		[DataObjectMethod(DataObjectMethodType.Delete, true)]
		public void Delete(int id)
		{
			SpreadSheetFilter filter = this.DataSource.FirstOrDefault(x => x.Id == id);
			if (filter != null)
			{
				int sortOrder = filter.SortOrder;
				this.DataSource.Remove(filter);

				// Fix sort order
				foreach (SpreadSheetFilter f in this.DataSource)
				{
					if (f.SortOrder > sortOrder)
					{
						f.SortOrder--;
					}
				}
			}
		}

		public void DraggedToSort(int draggedId, int draggedUponId)
		{
			SpreadSheetFilter dragged = this.DataSource.FirstOrDefault(x => x.Id == draggedId);
			SpreadSheetFilter draggedUpon = this.DataSource.FirstOrDefault(x => x.Id == draggedUponId);

			if (dragged == null || draggedUpon == null)
			{
				return;
			}

			bool draggedDown = dragged.SortOrder < draggedUpon.SortOrder;

			int originalSortOrder = draggedUpon.SortOrder;
			
			foreach (SpreadSheetFilter filter in this.DataSource)
			{
				if (filter.Id == draggedId)
				{
					filter.SortOrder = originalSortOrder;
					Trace.WriteLine($"Filter: {filter.Name}, SortOrder: {filter.SortOrder}");
					continue;
				}

				if (draggedDown)
				{
					if (filter.SortOrder > dragged.SortOrder && filter.SortOrder <= draggedUpon.SortOrder)
					{
						filter.SortOrder--;
					}
				}
				else
				{
					if (filter.SortOrder >= draggedUpon.SortOrder && filter.SortOrder < dragged.SortOrder)
					{
						filter.SortOrder++;
					}
				}

				Trace.WriteLine($"Filter: {filter.Name}, SortOrder: {filter.SortOrder}");
			}
		}

		public IList<SpreadSheetFilter> DataSource => this.request.SheetFilters;
	}
}