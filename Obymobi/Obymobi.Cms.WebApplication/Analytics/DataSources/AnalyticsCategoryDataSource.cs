﻿using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Analytics.DataSources
{
    public class AnalyticsCategoryDataSource : ObjectDataSource
    {
        private readonly AnalyticsCategoryManager Manager;

        public AnalyticsCategoryDataSource(int companyId)
        {
            this.TypeName = typeof(AnalyticsCategoryManager).FullName;
            this.Manager = new AnalyticsCategoryManager(companyId);

            this.SelectMethod = "Select";

            this.ObjectCreating += this.PageDataSource_ObjectCreating;
        }

        private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = this.Manager;
        }

        public List<AnalyticsCategoryItem> DataSource => this.Manager.DataSource;
    }


}