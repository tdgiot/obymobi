﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Analytics.DataSources
{
    public class AnalyticsCategoryManager
    {
        private readonly int companyId;

        public AnalyticsCategoryManager(int companyId)
        {
            this.companyId = companyId;
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public IEnumerable Select()
        {
            return this.DataSource;
        }

        private List<AnalyticsCategoryItem> dataSource;
        public List<AnalyticsCategoryItem> DataSource
        {
            get
            {
                if (this.dataSource == null)
                {
                    this.dataSource = this.CreateNodes();
                }

                return this.dataSource;
            }
        }

        private List<AnalyticsCategoryItem> CreateNodes()
        {
            PredicateExpression filter = new PredicateExpression(); 
            filter.Add(CategoryFields.CompanyId == companyId);

            IncludeFieldsList includeFieldsList = new IncludeFieldsList(CategoryFields.Name);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter,0, null, null, null, includeFieldsList, 0, 0);

            Dictionary<int, AnalyticsCategoryItem> nodes = new Dictionary<int, AnalyticsCategoryItem>();
            LoopCategories(categoryCollection, ref nodes);

            return nodes.Values.ToList();
        }
        
        private void LoopCategories(CategoryCollection collection, ref Dictionary<int, AnalyticsCategoryItem> nodes)
        {
            foreach (CategoryEntity categoryEntity in collection)
            {
                if (!categoryEntity.ParentCategoryId.HasValue)
                {
                    int parentId = 10000000 + categoryEntity.MenuId.GetValueOrDefault(0);

                    if (!nodes.ContainsKey(parentId))
                    {
                        AnalyticsCategoryItem item = new AnalyticsCategoryItem
                        {
                            CategoryId = parentId.ToString(),
                            ParentCategoryId = string.Empty,
                            Name = categoryEntity.MenuEntity.Name
                        };

                        nodes.Add(parentId, item);
                    }

                    categoryEntity.ParentCategoryId = parentId;
                }

                if (!nodes.ContainsKey(categoryEntity.CategoryId))
                {
                    nodes.Add(categoryEntity.CategoryId, new AnalyticsCategoryItem
                    {
                        CategoryId = categoryEntity.CategoryId.ToString(),
                        ParentCategoryId = categoryEntity.ParentCategoryId?.ToString(),
                        Name = categoryEntity.Name
                    });
                }

                if (categoryEntity.ChildCategoryCollection.Count > 0)
                {
                    LoopCategories(categoryEntity.ChildCategoryCollection, ref nodes);
                }
            }
        }
    }
}