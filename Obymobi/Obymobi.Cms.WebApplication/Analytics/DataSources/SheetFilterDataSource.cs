﻿using System.Collections.Generic;
using System.Web.UI.WebControls;
using Obymobi.Web.Analytics.Factories;
using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Requests.RequestBase;

namespace Obymobi.ObymobiCms.Analytics.DataSources
{
    public class SheetFilterDataSource : ObjectDataSource
    {
        private readonly SheetFilterManager Manager;

        public SheetFilterDataSource(IReportRequest request, ISheetFilterFactory sheetFilterFactory)
        {
            this.TypeName = typeof(SheetFilterManager).FullName;
            this.Manager = new SheetFilterManager(request, sheetFilterFactory);

            this.SelectMethod = "Select";
            this.UpdateMethod = "Update";
            this.DeleteMethod = "Delete";
            this.InsertMethod = "Insert";

            this.ObjectCreating += this.PageDataSource_ObjectCreating;
        }

        private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = this.Manager;
        }

        public IList<SpreadSheetFilter> GetSheetFilters()
        {
            return this.Manager.DataSource;
        }

        public void DraggedToSort(int draggedId, int draggedUponId)
        {
            this.Manager.DraggedToSort(draggedId, draggedUponId);
        }
    }
}