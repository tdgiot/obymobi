﻿<%@ Page Title="KPI Report" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.KpiReport" Codebehind="KpiReport.aspx.cs" %>

<%@ Reference VirtualPath="~/Analytics/SubPanels/ReportFilterPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" runat="Server">KPI Report</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" runat="Server">
    <span class="toolbar">
        <X:ToolBarButton runat="server" ID="btQueueReport" CommandName="GenerateReport" Text="Queue Report for Processing" Image-Url="~/Images/Icons/report.png" />
        <X:ToolBarButton runat="server" ID="btGenerateDoIt" CommandName="GenerateReportDoIt" Text="Rapport genereren - Do it!" Image-Url="~/Images/Icons/report.png" />
        <X:ToolBarButton runat="server" ID="btShowDashboard" CommandName="ShowDashboard" Text="Show Dashboard" Image-Url="~/Images/Icons/report.png" Visible="false" />
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" runat="Server">
    <D:PlaceHolder ID="PlaceHolder1" runat="server">
        <div>
            <X:PageControl ID="tabsMain" runat="server" Width="100%">
                <TabPages>
                    <X:TabPage Text="Advanced" Name="Advanced">
                        <controls>
                            <D:PlaceHolder runat="server">
                            <D:PlaceHolder ID="phlFilter" runat="server"></D:PlaceHolder>                            
							<table class="dataformV2">								
                                <tr>
									<td class="label">
										<D:Label runat="server" ID="lblName" LocalizeText="false">Report Name</D:Label>
									</td>
									<td class="control">
										<D:TextBoxString runat="server" ID="tbName" IsRequired="true"></D:TextBoxString>
									</td>	
									<td class="label">
										<D:Label runat="server" id="lblReportTemplate" LocalizeText="false">Excel Report Template</D:Label>
									</td>
									<td class="control">
										<D:FileUpload runat="server" ID="fuReportTemplate" IsRequired="true" UseValidation="true" />
                                        <D:Label runat="server" ID="lblReportTemplateCopied" Visible="false">Report template will be copied.</D:Label>
 									</td>	                                      
								</tr> 
                                <tr>
									<td class="label">
										<D:Label runat="server" id="lblGoogleViewId" LocalizeText="false">Google View</D:Label>
									</td>
									<td class="control">
                                        <X:ComboBoxString runat="server" ID="ddlGoogleViewId" IsRequired="true"></X:ComboBoxString>
  									</td>	                                      
								</tr> 
                                <D:PlaceHolder runat="server" ID="plhAdvancedFeatures">
			                    <tr>
									<td class="label">
                                        <D:Label runat="server" ID="lblIncludeActualFilters">Include actual filter in output</D:Label>
									</td>
									<td class="control">
                                        <D:CheckBox runat="server" ID="cbIncludeActualFilters" />
									</td>
									<td class="label">
                                        <D:Label runat="server" ID="lblIncludeStackTracesWithErrors">Include stack traces with errors</D:Label>
									</td>
									<td class="control">
                                        <D:CheckBox runat="server" ID="cbIncludeStackTracesWithErrors" />
									</td>
								</tr>
			                    <tr>
									<td class="label">
                                        <D:Label runat="server" ID="lblAllowAllCompanies">Disable 'Current Company Only'-restriction</D:Label>
									</td>
									<td class="control">
                                        <D:CheckBox runat="server" ID="cbAllowAllCompanies" />
									</td>
									<td class="label">
                                        &nbsp;
									</td>
									<td class="control">
                                        &nbsp;
									</td>
								</tr>
                                </D:PlaceHolder> 
			                    <tr>
									<td class="label">&nbsp;</td>
									<td class="control">&nbsp;</td>
									<td class="label">&nbsp;</td>
									<td class="control">&nbsp;</td>
								</tr>
							</table>    
                            <D:PlaceHolder runat="server" ID="plhEmbedApi" Visible="false">
		                        <script>
		                        (function(w,d,s,g,js,fs){
		                          g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
		                          js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
		                          js.src='https://apis.google.com/js/platform.js';
		                          fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
		                        }(window,document,'script'));
		                        </script>

		                        <%--<div id="embed-api-auth-container"></div>--%>

                                <!-- Output is rendered here -->
		                        <D:PlaceHolder runat="server" ID="plhEmbedApiContainers"></D:PlaceHolder>

		                        <script>

		                        gapi.analytics.ready(function() {

		                            <D:PlaceHolder runat="server" ID="plhEmbedApiAuthorize"></D:PlaceHolder>

                                    <D:PlaceHolder runat="server" ID="plhRetrievalScript"></D:PlaceHolder>			                       

		                        });
		                        </script>  

                            </D:PlaceHolder>
                        </D:PlaceHolder>                    

					</controls>
                    </X:TabPage>

                </TabPages>
            </X:PageControl>
        </div>
    </D:PlaceHolder>
</asp:Content>
