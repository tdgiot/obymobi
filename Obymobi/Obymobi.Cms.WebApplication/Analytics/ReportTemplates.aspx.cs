﻿using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.ObymobiCms.Analytics
{
    public partial class ReportTemplates : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
    {
        protected override void OnInit(EventArgs e)
        {
            this.EntityName = "ReportProcessingTaskTemplate";
            this.EntityPageUrl = "~/Analytics/ReportTemplate.aspx";

            this.Filter = new PredicateExpression();
            this.Filter.Add(ReportProcessingTaskTemplateFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
            
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
    }
}