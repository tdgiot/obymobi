﻿<%@ Page Title="Batch Reports" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.BatchReports" Codebehind="BatchReports.aspx.cs" %>
<%@ Reference VirtualPath="~/Analytics/SubPanels/ReportFilterPanel.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">Batch Reports</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <span class="toolbar">
        <X:ToolBarButton runat="server" ID="btQueueReport" CommandName="GenerateReport" Text="Queue Report for Processing" Image-Url="~/Images/Icons/report.png" />     
        <X:ToolBarButton runat="server" ID="btGenerateDoIt" CommandName="GenerateReportDoIt" Text="Rapport genereren - Do it!" Image-Url="~/Images/Icons/report.png" />   
        <X:ToolBarButton runat="server" ID="btGenerateMetadata" CommandName="ShowMetadata" Text="Generate Metadata" Image-Url="~/Images/Icons/cog.png" />
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<D:PlaceHolder ID="PlaceHolder1" runat="server">    
    <div>
	    <X:PageControl Id="tabsMain" runat="server" Width="100%">
		    <TabPages>
					<X:TabPage Text="Advanced" Name="Advanced">
						<controls>
                            <D:PlaceHolder ID="phlFilter" runat="server"></D:PlaceHolder>                            
							<table class="dataformV2">
								<!-- Transactions -->
                                <tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Report Collection Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbReportCollectionName"></D:TextBox>
									</td>	  
								</tr>                                
								<tr>
									<td colspan="4"><strong>Transactions Reports</strong></td>
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#1 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbTransactionsReportName1"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbTransactionsReportJson1"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#2 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbTransactionsReportName2"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbTransactionsReportJson2"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#3 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbTransactionsReportName3"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbTransactionsReportJson3"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#4 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbTransactionsReportName4"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbTransactionsReportJson4"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#5 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbTransactionsReportName5"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbTransactionsReportJson5"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#6 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbTransactionsReportName6"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbTransactionsReportJson6"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#7 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbTransactionsReportName7"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbTransactionsReportJson7"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#8 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbTransactionsReportName8"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbTransactionsReportJson8"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#9 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbTransactionsReportName9"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbTransactionsReportJson9"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<!-- Product Overviews -->
								<tr>
									<td colspan="4"><strong>Product Overview Reports</strong></td>
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#1 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbProductOverviewsReportName1"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbProductOverviewsReportJson1"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#2 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbProductOverviewsReportName2"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbProductOverviewsReportJson2"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#3 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbProductOverviewsReportName3"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbProductOverviewsReportJson3"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#4 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbProductOverviewsReportName4"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbProductOverviewsReportJson4"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<!-- Wake Up Calls -->
								<tr>
									<td colspan="4"><strong>Wake Up Call Reports</strong></td>
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#1 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbWakeupReportName1"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbWakeupReportJson1"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#2 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbWakeupReportName2"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbWakeupReportJson2"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#3 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbWakeupReportName3"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbWakeupReportJson3"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#4 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbWakeupReportName4"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbWakeupReportJson4"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<!-- Page Views -->
								<tr>
									<td colspan="4"><strong>Page Views Reports</strong></td>
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#1 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbPageViewsReportName1"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbPageViewsReportJson1"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#2 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbPageViewsReportName2"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbPageViewsReportJson2"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#3 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbPageViewsReportName3"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbPageViewsReportJson3"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">
										<D:Label runat="server" LocalizeText="false">#4 Name</D:Label>
									</td>
									<td class="control">
										<D:TextBox runat="server" ID="tbPageViewsReportName4"></D:TextBox>
									</td>	  
									<td class="label">
										<D:Label runat="server" LocalizeText="false">Metadata</D:Label>
									</td>
									<td class="control">
										<D:TextBoxMultiLine runat="server" ID="tbPageViewsReportJson4"></D:TextBoxMultiLine>
									</td>	  
								</tr>
								<tr>
									<td class="label">&nbsp;</td>
									<td class="control">&nbsp;</td>
									<td class="label">&nbsp;</td>
									<td class="control">&nbsp;</td>
								</tr>
							</table>                        
					</controls>
					</X:TabPage>
				                <X:TabPage Text="Metadata" Name="Metadata">
                    <controls>
                        <table class="dataformV2">
                            <tr>
	                            <td class="label">
		                            <D:Label runat="server" LocalizeText="false">Metadata</D:Label>
	                            </td>
	                            <td class="control" colspan="3">
		                            <D:TextBoxMultiLine runat="server" ID="tbMetadata" Rows="20"></D:TextBoxMultiLine>
	                            </td>	  
                            </tr>
                            <tr>
                                <td class="label">&nbsp;</td>
	                            <td class="control">&nbsp;</td>
                                <td class="label">&nbsp;</td>
	                            <td class="control">&nbsp;</td>
                            </tr>
                        </table>                        
                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</D:PlaceHolder>
</asp:Content>