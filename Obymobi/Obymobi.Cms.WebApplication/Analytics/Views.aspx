﻿<%@ Page Title="Views" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.Views" Codebehind="Views.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <D:Button runat="server" ID="btnRefresh" Text="Verversen" CausesValidation="true"/>	
    <D:Button runat="server" ID="btnExport" Text="Export" Visible="false"/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Overzicht" Name="Overview">
				<Controls>
                    <p>This page has been replaced by <a href="TopLists.aspx">Top lists</a> (remove in next version).</p>
				</Controls>
			</X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>

