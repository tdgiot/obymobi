﻿<%@ Page Title="Transactions" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.ReportTemplateSheet" Codebehind="ReportTemplateSheet.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" runat="Server">Transactions AppLess Filter</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" runat="Server"></asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" runat="Server">
	<D:PlaceHolder runat="server">
		<div>
			<X:PageControl ID="tabsMain" runat="server" Width="100%">
				<TabPages>
					<X:TabPage Text="Algemeen" Name="Generic">
						<controls>
		                    <table class="dataformV2">
		                        <tr>
		                            <td class="label">Report</td>
		                            <td class="control"><D:HyperLink runat="server" ID="hlParentReportName" LocalizeText="false" /></td>
		                            <td class="label"></td>
		                            <td class="control"></td>
		                        </tr>
		                        <tr>
		                            <td class="label">Sheet Name</td>
		                            <td class="control"><D:TextBox runat="server" ID="tbName" MaxLength="31" /></td>
		                            <td class="label">Active</td>
		                            <td class="control"><D:CheckBox runat="server" ID="cbActive"/></td>
		                        </tr>
		                    </table>
							<D:PlaceHolder ID="plhReportSheetFilter" runat="server" />
						</controls>
					</X:TabPage>
				</TabPages>
			</X:PageControl>
		</div>
	</D:PlaceHolder>
</asp:Content>
