﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxScheduler;
using DevExpress.XtraScheduler;
using Dionysos.Web.UI;
using Obymobi.Cms.WebApplication.Old_App_Code.Scheduling;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using MenuItem = DevExpress.Web.MenuItem;

namespace Obymobi.ObymobiCms.Analytics
{
    public partial class ReportScheduling : PageDefault
    {
        private SchedulingDataSource objectInstance;

        protected override void OnInit(EventArgs e)
        {
            LoadUserControls();
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            this.scheduler.PopupMenuShowing += Scheduler_PopupMenuShowing;

            this.scheduler.InitClientAppointment += Scheduler_InitClientAppointment;
            this.scheduler.AppointmentFormShowing += Scheduler_AppointmentFormShowing;
            this.scheduler.BeforeExecuteCallbackCommand += Scheduler_BeforeExecuteCallbackCommand;
            this.scheduler.AppointmentViewInfoCustomizing += Scheduler_AppointmentViewInfoCustomizing;
            this.scheduler.InitAppointmentDisplayText += Scheduler_InitAppointmentDisplayText;
            this.scheduler.AppointmentRowInserted += Scheduler_OnAppointmentRowInserted;

            this.scheduler.TimelineView.IntervalCount = 24;
            this.scheduler.TimelineView.ShowMoreButtons = false;
            this.scheduler.TimelineView.NavigationButtonVisibility = NavigationButtonVisibility.Never;

            this.scheduler.WorkWeekView.ShowAllDayArea = false;
            this.scheduler.WorkWeekView.ShowFullWeek = true;
            this.scheduler.WorkWeekView.ShowMoreButtons = false;

            this.scheduler.DayView.ShowAllDayArea = false;
            this.scheduler.DayView.ShowMoreButtons = false;

            this.scheduler.MonthView.ShowWeekend = true;

            this.scheduler.OptionsBehavior.ShowViewNavigator = true;
            this.scheduler.OptionsBehavior.ShowViewVisibleInterval = true;

            this.scheduler.DayView.TimeMarkerVisibility = TimeMarkerVisibility.Never;

            this.scheduler.OptionsCustomization.AllowInplaceEditor = UsedAppointmentType.None;
            this.scheduler.OptionsCustomization.AllowAppointmentResize = UsedAppointmentType.None;
            this.scheduler.OptionsCustomization.AllowAppointmentDrag = UsedAppointmentType.None;

            this.scheduler.OptionsBehavior.RecurrentAppointmentDeleteAction = RecurrentAppointmentAction.Series;
            this.scheduler.OptionsBehavior.RecurrentAppointmentEditAction = RecurrentAppointmentAction.Series;
            this.scheduler.OptionsForms.RecurrentAppointmentDeleteFormVisibility = SchedulerFormVisibility.None;
            this.scheduler.OptionsForms.RecurrentAppointmentEditFormVisibility = SchedulerFormVisibility.PopupWindow;
            
            this.scheduler.OptionsToolTips.ShowAppointmentToolTip = true;

            this.scheduler.WeekView.Enabled = true;
            this.scheduler.DayView.Enabled = true;
            this.scheduler.WeekView.Enabled = false;
            this.scheduler.WorkWeekView.Enabled = true;
            this.scheduler.MonthView.Enabled = true;

            this.scheduler.WorkWeekView.MenuCaption = "Full Week View";

            this.scheduler.Images.SmartTag.Url = this.ResolveUrl("~/images/icons/add2.png");

            this.scheduler.WorkDays.Add(WeekDays.Saturday);
            this.scheduler.WorkDays.Add(WeekDays.Sunday);
            this.scheduler.TimelineView.WorkTime = new TimeOfDayInterval(TimeSpan.FromHours(0), TimeSpan.FromHours(24));
            this.scheduler.DayView.WorkTime = new TimeOfDayInterval(TimeSpan.FromHours(0), TimeSpan.FromHours(24));
            this.scheduler.WorkWeekView.WorkTime = new TimeOfDayInterval(TimeSpan.FromHours(0), TimeSpan.FromHours(24));

            this.scheduler.Start = DateTime.Now.Date;
            this.scheduler.ActiveViewType = SchedulerViewType.WorkWeek;
        }

        private static UIScheduleEntity GetOrCreateReportingSchedule(int companyId)
        {
            return GetReportingSchedule(companyId) ?? CreateAndSaveReportingSchedule(companyId);
        }

        private static UIScheduleEntity GetReportingSchedule(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIScheduleFields.CompanyId == companyId);
            filter.Add(UIScheduleFields.Type == UIScheduleType.Reporting);

            UIScheduleCollection scheduleCollection = new UIScheduleCollection();
            scheduleCollection.GetMulti(filter);

            return scheduleCollection.FirstOrDefault();
        }

        private static UIScheduleEntity CreateAndSaveReportingSchedule(int companyId)
        {
            UIScheduleEntity schedule = new UIScheduleEntity();
            schedule.CompanyId = companyId;
            schedule.Type = UIScheduleType.Reporting;
            schedule.Name = "Reporting schedule";
            schedule.Save();

            return schedule;
        }

        private void Page_Load(object sender, EventArgs e)
        {
            this.scheduler.Storage.Appointments.AutoRetrieveId = true;
        }

        private void Scheduler_AppointmentViewInfoCustomizing(object sender, DevExpress.Web.ASPxScheduler.AppointmentViewInfoCustomizingEventArgs e)
        {
            int backgroundColor = (int)e.ViewInfo.Appointment.CustomFields["BackgroundColor"];
            e.ViewInfo.AppointmentStyle.BackColor = System.Drawing.Color.FromArgb(backgroundColor);

            int textColor = (int)e.ViewInfo.Appointment.CustomFields["TextColor"];
            e.ViewInfo.AppointmentStyle.ForeColor = System.Drawing.Color.FromArgb(textColor);

            e.ViewInfo.ShowStartTime = false;
            e.ViewInfo.ShowEndTime = false;
            e.ViewInfo.StatusDisplayType = AppointmentStatusDisplayType.Never;
        }

        private void Scheduler_InitAppointmentDisplayText(object sender, AppointmentDisplayTextEventArgs e)
        {
            e.Text = e.ViewInfo.Appointment.Start.ToString("t") + " - " + e.ViewInfo.Appointment.Subject;
        }

        private void Scheduler_OnAppointmentRowInserted(object sender, ASPxSchedulerDataInsertedEventArgs e)
        {
            e.KeyFieldValue = objectInstance.ObtainLastInsertedId();
        }

        private void Scheduler_InitClientAppointment(object sender, InitClientAppointmentEventArgs args)
        {
            args.Properties["cpSubject"] = args.Appointment.Subject;
            args.Properties["cpLocation"] = args.Appointment.Location;
        }

        private void Scheduler_BeforeExecuteCallbackCommand(object sender, SchedulerCallbackCommandEventArgs e)
        {
            if (e.CommandId == SchedulerCallbackCommandId.AppointmentSave)
            {
                e.Command = new SchedulingAppointmentSaveCallbackCommand((ASPxScheduler)sender);
            }
            else if (e.CommandId == SchedulingAppointmentDeleteCallbackCommand.CommandId)
            {
                e.Command = new SchedulingAppointmentDeleteCallbackCommand((ASPxScheduler)sender);
            }
        }

        private void Scheduler_AppointmentFormShowing(object sender, DevExpress.Web.ASPxScheduler.AppointmentFormEventArgs e)
        {
            e.Container = new SchedulingAppointmentForm((ASPxScheduler)sender);
            e.Container.Caption = "Add Item";
        }

        private void Scheduler_PopupMenuShowing(object sender, DevExpress.Web.ASPxScheduler.PopupMenuShowingEventArgs e)
        {
            if (e.Menu.Id == SchedulerMenuItemId.DefaultMenu)
            {
                MenuItem newAppointment = e.Menu.Items.FindByName("NewAppointment");
                newAppointment.Image.Url = this.ResolveUrl("~/images/icons/add2.png");
                newAppointment.Text = "Add Item";

                MenuItem timeScaleEnable = e.Menu.Items.FindByName("TimeScaleEnable");
                MenuItem switchViewMenu = e.Menu.Items.FindByName("SwitchViewMenu");

                e.Menu.Items.Clear();

                e.Menu.Items.Add(newAppointment);
                if (timeScaleEnable != null)
                {
                    e.Menu.Items.Add(timeScaleEnable);
                }
                e.Menu.Items.Add(switchViewMenu);
            }
            else if (e.Menu.Id == SchedulerMenuItemId.AppointmentMenu)
            {
                e.Menu.ClientSideEvents.ItemClick = String.Format("function(s, e) {{ DefaultAppointmentMenuHandler({0}, s, e); }}", this.scheduler.ClientID);
                e.Menu.Items.Clear();

                MenuItem editItem = new MenuItem("Edit Item", "EditItem");
                editItem.BeginGroup = true;
                e.Menu.Items.Add(editItem);

                MenuItem editPattern = new MenuItem("Edit Series", "EditPattern");
                editPattern.BeginGroup = true;
                e.Menu.Items.Add(editPattern);

                MenuItem restoreItem = new MenuItem("Restore Default State", "RestoreItem");
                restoreItem.BeginGroup = true;
                e.Menu.Items.Add(restoreItem);

                MenuItem deleteItem = new MenuItem("Delete", "DeleteAppointment");
                deleteItem.BeginGroup = true;
                deleteItem.Image.Url = this.ResolveUrl("~/images/icons/delete.png");
                e.Menu.Items.Add(deleteItem);
            }
        }

        protected void scheduleDataSource_ObjectCreated(object sender, ObjectDataSourceEventArgs e)
        {
            UIScheduleEntity schedule = GetOrCreateReportingSchedule(CmsSessionHelper.CurrentCompanyId);

            SchedulingAppointmentConverter converter = new SchedulingAppointmentConverter(schedule.CompanyEntity.TimeZoneInfo);

            SchedulingAppointmentHandler handler = new SchedulingAppointmentHandler(schedule.UIScheduleId, 
                                                                                    converter);

            SchedulingAppointmentList appointments = converter.Convert(schedule.UIScheduleItemOccurrenceCollection);

            this.objectInstance = new SchedulingDataSource(handler, 
                                                           appointments);
            e.ObjectInstance = this.objectInstance;
        }
    }
}
