﻿<%@ Page Title="Views" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.TopLists" Codebehind="TopLists.aspx.cs" %>
<%@ Reference Control="~/Analytics/Subpanels/CategoryPanel.ascx" %>
<%@ Reference Control="~/Analytics/Subpanels/DeliverypointPanel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <D:Button runat="server" ID="btnRefresh" Text="Verversen" CausesValidation="true"/>	
    <D:Button runat="server" ID="btnExport" Text="Export" Visible="false"/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Filters" Name="Filter">
				<Controls>
                    <X:PageControl Id="tabsFilters" runat="server" Width="100%">
		                <TabPages>
			                <X:TabPage Text="Generic" Name="Generic">
                                <Controls>
                                    <D:PlaceHolder ID="PlaceHolder1" runat="server">
	                                    <table class="dataformV2">
		                                    <tr>
			                                    <td class="label">
				                                    <D:Label runat="server" id="lblCompanyId">Bedrijf</D:Label>
			                                    </td>
			                                    <td class="control">
				                                    <X:ComboBoxLLBLGenEntityCollection ID="ddlCompanyId" runat="server" EntityName="Company" IsRequired="true"></X:ComboBoxLLBLGenEntityCollection>
			                                    </td>
			                                    <td class="label">
				                                    <D:Label runat="server" id="lblReportType">Rapportage</D:Label>
			                                    </td>
			                                    <td class="control">
				                                    <X:ComboBoxInt runat="server" ID="ddlReportType" IncrementalFilteringMode="StartsWith" IsRequired="true"></X:ComboBoxInt>
			                                    </td>                                							
                                            </tr>
		                                    <tr>
			                                    <td class="label">
				                                    <D:Label runat="server" id="lblDateFrom">Datum van</D:Label>
			                                    </td>
			                                    <td class="control">
                                                    <X:DateEdit runat="server" ID="deDateFrom"></X:DateEdit>
			                                    </td>							
			                                    <td class="label">
				                                    <D:Label runat="server" id="lblDateTo">Datum tot</D:Label>
			                                    </td>
			                                    <td class="control">
                                                    <X:DateEdit runat="server" ID="deDateTo"></X:DateEdit>
			                                    </td>							
		                                    </tr>
                                             <tr>
                                                <td class="label">
                                                    <D:Label runat="server" id="lblItemsPerPage">Items per page</D:Label>
			                                    </td>
			                                    <td class="control">
                                                    <D:TextBoxInt id="tbItemsPerPage" runat="server" DecimalValue="10" DefaultValue="10" UseDataBinding="false" />
			                                    </td>				
                                                <td class="label">
                                                    <D:Label runat="server" id="lblItemsOnChart">Items on chart</D:Label>
			                                    </td>
			                                    <td class="control">
                                                    <D:TextBoxInt id="tbItemsOnChart" runat="server" DecimalValue="10" DefaultValue="10" UseDataBinding="false"/>
			                                    </td>											
		                                    </tr>
                                        </table>
                                     </D:PlaceHolder>
                                 </Controls>
                            </X:TabPage>
                            <X:TabPage Text="Categories" Name="Categories">
				                <Controls>
                                    <D:PlaceHolder ID="plhCategoryPanel" runat="server" />
                                </Controls>
                            </X:TabPage>
                             <X:TabPage Text="Deliverypoints" Name="Deliverypoints">
				                <Controls>
                                    <D:PlaceHolder ID="plhDeliverypointPanel" runat="server" />
                                </Controls>
                            </X:TabPage>
                             <X:TabPage Text="Analytics Profiles" Name="AnalyticsProfiles">
				                <Controls>
                                    <D:PlaceHolder ID="plhAnalyticsprofilePanel" runat="server" />
                                </Controls>
                            </X:TabPage>
                        </TabPages>
                    </X:PageControl>
				</Controls>
			</X:TabPage>
            <X:TabPage Text="Report" Name="Report">
				<Controls>
                    <D:Placeholder ID="Placeholder2" runat="server">
                        <table class="dataformV2">
                            <table style="width:100%;">
                                <tr>
					    	        <td style="text-align:center">
                                        <div style="width:1000px;margin:0 auto;">
                                            <D:PlaceHolder runat="server" ID="plhOverview"></D:PlaceHolder>
                                        </div>
					    	        </td>
                                </tr>
                            </table>
                            <X:GridView ID="viewsGrid" ClientInstanceName="viewsGrid" runat="server" Width="100%">        
                                <SettingsBehavior AutoFilterRowInputDelay="350" />                            
                                <SettingsPager PageSize="10"></SettingsPager>
                                <SettingsBehavior AllowGroup="false" AllowDragDrop="false" />
                                <Columns />                                
                            </X:GridView>  
					    </table>
                    </D:Placeholder>
                </Controls>
            </X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>

