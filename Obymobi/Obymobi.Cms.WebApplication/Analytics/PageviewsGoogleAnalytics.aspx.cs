﻿using System;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Analytics;
using Obymobi.ObymobiCms.Analytics.Subpanels;
using Dionysos.Web;
using Newtonsoft.Json;

namespace Obymobi.ObymobiCms.Analytics
{
    public partial class PageviewsGoogleAnalytics : BaseReportingPage
    {
        private ReportFilterPanel filterPanel;

        protected override void OnInit(EventArgs e)
        {
            this.InitGui();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void SetDefaultValuesToControls()
        {
            this.filterPanel.Period = ReportFilterPanel.ReportingPeriod.LastMonth;
        }

        protected override void BindQueryStringToControls()
        {
            base.BindQueryStringToControls();
            this.filterPanel.UpdateCompanyId();
        }

        void InitGui()
        {
            this.filterPanel = this.LoadControl<ReportFilterPanel>("~/Analytics/Subpanels/ReportFilterPanel.ascx");
            this.filterPanel.ShowFilterDeliverypoints = false;
            this.filterPanel.IncludeByod = true;
            this.filterPanel.IncludeInRoomTablets = true;
            this.filterPanel.ShowFilterOrders = false;
            this.filterPanel.ShowLoadDeliverypointsButton = false;
            this.filterPanel.ShowFilterCategories = false;
            this.filterPanel.ShowFilterProduct = false;

            this.phlFilter.Controls.Add(this.filterPanel);

            this.btGenerateDoIt.Visible = (TestUtil.IsPcDeveloper && WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.Manual) || TestUtil.IsPcGabriel;
        }

        public void ValidateSelf()
        {
            this.filterPanel.Validate();

            base.Validate();
        }

        public void ShowMetadata()
        {
            this.GenerateReport(true, true);
        }


        public void GenerateReportDoIt()
        {
            this.GenerateReport(true);
        }

        public void GenerateReport()
        {
            this.GenerateReport(false);
        }

        public void GenerateReport(bool doIt, bool metaDataOnly = false)
        {
            this.ValidateSelf();
            if (this.IsValid)
            {
                Filter filter = this.filterPanel.GetFilter();

                if (metaDataOnly)
                {
                    this.tbMetadata.Text = JsonConvert.SerializeObject(filter);

                    // Ensure it's the valid serialize/deserialize
                    var validationObject = JsonConvert.DeserializeObject<Filter>(this.tbMetadata.Text);
                    if (!JsonConvert.SerializeObject(validationObject).Equals(this.tbMetadata.Text))
                        throw new Exception("The current report selection can't be serialized.");

                    this.tabsMain.TabPages.FindByName("Metadata").Visible = true;
                    return;
                }
                
                var report = new PageviewsReport(filter, WebEnvironmentHelper.CloudEnvironment);
                GenerateReport(doIt, filter, report, AnalyticsReportType.PageViews);
            }
        }
    }
}