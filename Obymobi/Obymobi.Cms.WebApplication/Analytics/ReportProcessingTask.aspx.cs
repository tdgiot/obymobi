﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using Dionysos;
using Dionysos.Web.UI;
using Newtonsoft.Json;
using Obymobi.Analytics.Reports;
using Obymobi.Web.Analytics.Converters;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.MasterPages;
using Obymobi.Web.Analytics.Requests;
using AnalyticsLogic = Obymobi.Logic.Analytics;
using LogicHelpers = Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Analytics
{
	public partial class ReportProcessingTask : PageLLBLGenEntity
	{
		public new ReportProcessingTaskEntity DataSource
		{
			get => base.DataSource as ReportProcessingTaskEntity;
			set => base.DataSource = value;
		}

		private void SetGui()
		{
			lblReportTypeValue.Text = DataSource.AnalyticsReportTypeAndPeriod;

			if (DataSource.TimeZoneOlsonId.IsNullOrWhiteSpace())
			{
				lblTimeZoneValue.Text = "No time zone information.";
			}
			else
			{
				TimeZone timeZone = TimeZone.Mappings[DataSource.TimeZoneOlsonId];

				lblTimeZoneValue.Text = timeZone.ToString();
			}

			((MasterPageEntity) Master).ToolBar.DeleteButton.Visible = DataSource.Processed && CmsSessionHelper.CurrentRole != Role.Reseller;
			((MasterPageEntity) Master).ToolBar.SaveButton.Visible = false;
			((MasterPageEntity) Master).ToolBar.SaveAndGoButton.Visible = false;
			((MasterPageEntity) Master).ToolBar.SaveAndNewButton.Visible = false;

			hlStatusReadyForDownload.Visible = DataSource.Processed && !DataSource.Failed;
			lblFailed.Visible = DataSource.Processed && DataSource.Failed;
			lblStatusQueued.Visible = !DataSource.Processed;

			lblCreatedValue.Text = DataSource.CreatedLocalTime.ToString(CultureInfo.InvariantCulture);

			btRerunCopy.Visible = DataSource.AnalyticsReportType == AnalyticsReportType.Batch || DataSource.AnalyticsReportType == AnalyticsReportType.Kpi;

			if (DataSource.AnalyticsReportType == AnalyticsReportType.BrokenLinks)
			{
				// No further details
			}
			else if (DataSource.AnalyticsReportType == AnalyticsReportType.Batch)
			{
				BatchReportRequest request = JsonConvert.DeserializeObject<BatchReportRequest>(DataSource.Filter);
				StringBuilder sb = new StringBuilder();

				sb.AppendLine(request.Name);

				foreach (TransactionsRequest r in request.TransactionReports)
				{
					sb.AppendFormatLine("Transactions Report: " + r.Name);
				}

				foreach (AnalyticsLogic.Filter r in request.ProductOverviewReports)
				{
					sb.AppendFormatLine("Product Overview Report: " + r.Name);
				}

				foreach (AnalyticsLogic.Filter r in request.WakeUpCallReports)
				{
					sb.AppendFormatLine("Wake Up Calls Report: " + r.Name);
				}

				foreach (AnalyticsLogic.Filter r in request.PageViewReports)
				{
					sb.AppendFormatLine("Page View Report: " + r.Name);
				}

				lblReportConfigurationValue.Text = sb.ToString();
				plhFilter.Visible = true;
			}
			else if (DataSource.AnalyticsReportType == AnalyticsReportType.Kpi)
			{
				StringBuilder sb = new StringBuilder();
				KpiReportMetadata metaData = JsonConvert.DeserializeObject<KpiReportMetadata>(DataSource.Filter);

				// Metadata
				sb.AppendFormatLine("Name: \t\t\t\t{0}", metaData.Name);
				sb.AppendFormatLine("Include Actual Filters:\t\t{0}", metaData.IncludeActualFilters);
				sb.AppendFormatLine("Include Stack Traces:\t\t{0}", metaData.IncludeStackTracesWithErrors);
				sb.AppendFormatLine("\n", metaData.IncludeStackTracesWithErrors);

				// Link to download file
				lblReportConfigurationValue.Text = sb.ToString();
				plhFilter.Visible = true;

				btDownloadTemplate.Click += BtDownloadTemplate_Click;
				btDownloadTemplate.Visible = true;
			}
			else if (DataSource.AnalyticsReportType == AnalyticsReportType.Transactions)
			{
				TransactionsRequest request = LogicHelpers.XmlHelper.Deserialize<TransactionsRequest>(DataSource.Filter);

				AnalyticsLogic.Filter filter = new AnalyticsLogic.Filter();
				filter.TillUtc = request.TillUtc;
				filter.FromUtc = request.FromUtc;
				filter.CompanyId = request.CompanyId;
				filter.TimeZoneOlsonId = request.TimeZoneOlsonId;
				filter.DeliverypointIds = request.DeliverypointIds;
				filter.DeliverypointgroupIds = request.DeliverypointgroupIds;
				filter.Name = request.Name;
				filter.DeliverypointsExclude = request.DeliverypointsExclude;
				filter.IncludeByodApps = request.IncludeByodApps;
				filter.IncludeFailedOrders = request.IncludeFailedOrders;
				filter.IncludeInRoomTablets = request.IncludeInRoomTablets;
				filter.OrderStatuses = request.OrderStatuses;
				lblReportConfigurationValue.Text = filter.ToString();
				plhFilter.Visible = true;
			}
			else if (DataSource.AnalyticsReportType == AnalyticsReportType.TransactionsAppLess)
			{
				TransactionsAppLessRequest filter = JsonConvert.DeserializeObject<TransactionsAppLessRequest>(DataSource.Filter, new TransactionsAppLessSpreadSheetFilterConverter());
				lblReportConfigurationValue.Text = filter.ToString();
				plhFilter.Visible = true;
			}
			else if (DataSource.AnalyticsReportType == AnalyticsReportType.ProductSales)
			{
				ProductReportRequest filter = JsonConvert.DeserializeObject<ProductReportRequest>(DataSource.Filter, new ProductReportSpreadSheetFilterConverter());
				lblReportConfigurationValue.Text = filter.ToString();
				plhFilter.Visible = true;
			}
			else if (DataSource.AnalyticsReportType == AnalyticsReportType.Inventory)
			{
				InventoryReportRequest filter = JsonConvert.DeserializeObject<InventoryReportRequest>(DataSource.Filter, new InventoryReportSpreadSheetFilterConverter());
				lblReportConfigurationValue.Text = filter.ToString();
				plhFilter.Visible = true;
			}
			else
			{
				AnalyticsLogic.Filter filter = LogicHelpers.XmlHelper.Deserialize<AnalyticsLogic.Filter>(DataSource.Filter, false);
				lblReportConfigurationValue.Text = filter.ToString();
				plhFilter.Visible = true;
			}

			// Check if we had issues
			if (DataSource.Processed && DataSource.Failed)
			{
				AddInformator(InformatorType.Warning, Translate("GeneratingTheReportFailedTryAgain", "Generating the report failed. Try to generate a new report."));
				lblExceptionTextValue.Text = DataSource.ExceptionText;
			}

			plhException.Visible = lblFailed.Visible;
		}

		private void BtDownloadTemplate_Click(object sender, EventArgs e)
		{
			ReportProcessingTaskTemplateEntity template = DataSource.ReportProcessingTaskTemplateCollection.FirstOrDefault();
			if (template == null)
			{
				return;
			}

			Response.Clear();
			Response.Clear();
			Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			Response.AddHeader("Content-Disposition", "attachment; filename=KpiReport-Template-{0}.xlsx".FormatSafe(template.ReportProcessingTaskId));
			Response.OutputStream.Write(template.TemplateFile, 0, template.TemplateFile.Length);
			Response.End();
		}

		public override bool Save() => throw new Exception("This should never happen.");

		protected override void OnInit(EventArgs e)
		{
			DataSourceLoaded += ReportProcessingTask_DataSourceLoaded;
			base.OnInit(e);
		}

		private void ReportProcessingTask_DataSourceLoaded(object sender)
		{
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			SetGui();
			hlStatusReadyForDownload.Click += hlStatusReadyForDownload_Click;
			Validate();
		}

		public void RerunCopy()
		{
			if (DataSource.AnalyticsReportType == AnalyticsReportType.Batch)
			{
				Response.Redirect("~/Analytics/BatchReports.aspx?copyFrom=" + DataSource.ReportProcessingTaskId);
			}
			else
			{
				Response.Redirect("~/Analytics/KpiReport.aspx?copyFrom=" + EntityId);
			}
		}

		public void RefreshPage()
		{
			// Do nothing, just let the page refresh
		}

		private void hlStatusReadyForDownload_Click(object sender, EventArgs e)
		{
			if (!DataSource.ReportProcessingTaskFileEntity.IsNew && DataSource.ReportProcessingTaskFileEntity.File.Length > 0)
			{
				string filename;
				if (DataSource.AnalyticsReportType == AnalyticsReportType.BrokenLinks)
				{
					filename = "{0}-{1}-{2}".FormatSafe(DataSource.AnalyticsReportType,
						DataSource.ReportProcessingTaskId,
						DataSource.CreatedUTC.GetValueOrDefault().DateTimeToSimpleDateStamp());
				}
				else
				{
					filename = "{0}-{1}-{2}-{3}".FormatSafe(DataSource.AnalyticsReportType,
						DataSource.ReportProcessingTaskId,
						DataSource.FromTimeZoned.GetValueOrDefault().DateTimeToSimpleDateStamp(),
						DataSource.TillTimeZoned.GetValueOrDefault().DateTimeToSimpleDateStamp());
				}

				Response.Clear();
				Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
				if (DataSource.AnalyticsReportType == AnalyticsReportType.Batch)
				{
					Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.zip", filename));
				}
				else
				{
					Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.xlsx", filename));
				}

				Response.BinaryWrite(DataSource.ReportProcessingTaskFileEntity.File);
				Response.End();
			}
			else
			{
				AddInformator(InformatorType.Warning, Translate("ReportUnavailable", "Dit rapport is onbeschikbaar, probeer het opnieuw aan te maken"));
			}
		}
	}
}
