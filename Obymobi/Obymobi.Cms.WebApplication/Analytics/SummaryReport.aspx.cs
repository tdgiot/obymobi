﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Analytics;
using Obymobi.ObymobiCms.Analytics.Subpanels;

namespace Obymobi.ObymobiCms.Analytics
{
    public partial class SummaryReport : BaseReportingPage
    {
        private ReportFilterPanel filterPanel;

        protected override void OnInit(EventArgs e)
        {
            this.InitGui();
            base.OnInit(e);
        }

        void InitGui()
        {
            filterPanel = this.LoadControl<ReportFilterPanel>("~/Analytics/Subpanels/ReportFilterPanel.ascx");
            this.phlFilter.Controls.Add(filterPanel);

            this.btGenerateDoIt.Visible = (TestUtil.IsPcDeveloper && WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.Manual);
        }

        public override void Validate()
        {
            filterPanel.Validate();

            base.Validate();
        }

        public void GenerateReportDoIt()
        {
            this.GenerateReport(true);
        }

        public void GenerateReport()
        {
            this.GenerateReport(false);
        }

        public void GenerateReport(bool doIt)
        {
            this.Validate();
            if (this.IsValid)
            {
                //Filter filter = this.filterPanel.GetFilter();
                //var report = new SummaryReport(filter);
                //GenerateReport(doIt, filter, report, AnalyticsReportType.Transactions);
            }
        }

        protected override void SetDefaultValuesToControls()
        {
            filterPanel.IncludeInRoomTablets = true;
            filterPanel.IncludeByod = true;
            filterPanel.Period = ReportFilterPanel.ReportingPeriod.LastMonth;
        }

        protected override void BindQueryStringToControls()
        {
            base.BindQueryStringToControls();
            this.filterPanel.UpdateCompanyId();
        }
    }
}