using DevExpress.Web.ASPxTreeList;
using Obymobi.Cms.WebApplication.Old_App_Code.Extensions;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.Analytics.DataSources;
using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Analytics.ReportTemplateSheets
{
    public interface IReportSheetFilterControl
    {
        void Load(SpreadSheetFilter reportRequest, int sheetId);
        SpreadSheetFilter GetSheetFilter();
    }

    public partial class TransactionsAppLessReportSheetFilter : UserControl, IReportSheetFilterControl
    {
        private TransactionsAppLessSheetFilter SheetFilter { get; set; }

        protected override void OnInit(EventArgs e)
        {
            rbCategoryFilterType.DataBindEnum<IncludeExcludeFilter>();

            lbSystemProducts.DataBindEnum<OrderitemType>();
            lbSystemProducts.Items.RemoveAt(0);
            lbSystemProducts.DisplayEmptyItem = true;

            tlCategories.Load += (sender, args) =>
            {
                tlCategories.SettingsSelection.Recursive = true;
            };

            CreateCategoryTreeList();

            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Events
            cbOutletFilter.SelectedIndexChanged += (s, args) =>
            {
                UpdateOutlet();
                lbOutletServiceMethods.SelectAll();
                lbOutletCheckoutMethods.SelectAll();
            };
            cbShowProducts.CheckedChanged += (s, args) => UpdateProductFilter();

            UpdateOutlet();
            UpdateProductFilter();
            UpdateSystemProducts();
        }

        public new void Load(SpreadSheetFilter sheetFilter, int companyId)
        {
            SheetFilter = sheetFilter as TransactionsAppLessSheetFilter;

            DataBindOutlets(companyId);
            DataBindCategories(companyId);
            DataBindTaxTariffs(companyId);

            SetSheetFilterValues();
        }

        private void UpdateOutlet()
        {
            int outletId = cbOutletFilter.Value.GetValueOrDefault();
            if (outletId <= 0)
            {
                plhOutletMethods.Visible = false;
            }
            else
            {
                plhOutletMethods.Visible = true;

                DataBindServiceMethods(outletId);
                DataBindCheckoutMethods(outletId);
            }
        }

        private void UpdateProductFilter()
        {
            cbShowProductQuantity.Enabled = cbShowProducts.Checked;
            cbShowProductPrice.Enabled = cbShowProducts.Checked;
            cbShowAlterations.Enabled = cbShowProducts.Checked;
            cbShowProductCategory.Enabled = cbShowProducts.Checked;
        }

        private void UpdateSystemProducts()
        {
            lbSystemProducts.Enabled = cbShowSystemProducts.Checked;
            if (!lbSystemProducts.Enabled)
            {
                return;
            }

            cbShowProductPrice.Checked = true;

            if (!IsPostBack)
            {
                if (SheetFilter.IncludedSystemProductTypes.Count == 0)
                {
                    lbSystemProducts.SelectAll();
                }
                else
                {
                    lbSystemProducts.SelectValues(SheetFilter.IncludedSystemProductTypes);
                }
            }
        }

        private void DataBindServiceMethods(int outletId)
        {
            IEntityFieldCore[] serviceMethodIncludeFieldsList = {
                ServiceMethodFields.ServiceMethodId,
                ServiceMethodFields.Name
            };

            lbOutletServiceMethods.DataBindEntityCollection<ServiceMethodEntity>(ServiceMethodFields.OutletId == outletId, ServiceMethodFields.Name, serviceMethodIncludeFieldsList);

            if (!IsPostBack)
            {
                if (SheetFilter.ServiceMethods.Count > 0)
                {
                    lbOutletServiceMethods.SelectValues(SheetFilter.ServiceMethods);
                }
                else
                {
                    lbOutletServiceMethods.SelectAll();
                }
            }
        }

        private void DataBindCheckoutMethods(int outletId)
        {
            IEntityFieldCore[] checkoutMethodIncludeFieldsList = {
                CheckoutMethodFields.CheckoutMethodId,
                CheckoutMethodFields.Name
            };

            lbOutletCheckoutMethods.DataBindEntityCollection<CheckoutMethodEntity>(CheckoutMethodFields.OutletId == outletId, CheckoutMethodFields.Name, checkoutMethodIncludeFieldsList);

            if (!IsPostBack)
            {
                if (SheetFilter.CheckoutMethods.Count > 0)
                {
                    lbOutletCheckoutMethods.SelectValues(SheetFilter.CheckoutMethods);
                }
                else
                {
                    lbOutletCheckoutMethods.SelectAll();
                }
            }
        }

        private void DataBindOutlets(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(OutletFields.CompanyId == companyId);

            IEntityFieldCore[] includeFieldsList = {
                OutletFields.OutletId,
                OutletFields.Name
            };

            cbOutletFilter.DataBindEntityCollection<OutletEntity>(filter, OutletFields.Name, includeFieldsList);
            cbOutletFilter.SelectedIndex = cbOutletFilter.Items.FindByValue(SheetFilter.OutletId).Index;
        }

        private void DataBindCategories(int companyId)
        {
            tlCategories.DataSource = new AnalyticsCategoryDataSource(companyId);
            tlCategories.DataBind();
            tlCategories.ExpandToLevel(3);
        }

        private void DataBindTaxTariffs(int companyId)
        {
            TaxTariffCollection taxTariffCollection = new TaxTariffCollection();
            taxTariffCollection.GetMulti(TaxTariffFields.CompanyId == companyId, new IncludeFieldsList(TaxTariffFields.Name), null);

            lbTaxTariffs.DataSource = taxTariffCollection;
            lbTaxTariffs.ValueField = nameof(TaxTariffEntity.TaxTariffId);
            lbTaxTariffs.ValueType = typeof(int);
            lbTaxTariffs.TextField = nameof(TaxTariffEntity.Name);
            lbTaxTariffs.DataBind();
        }

        private void CreateCategoryTreeList()
        {
            tlCategories.AutoGenerateColumns = false;
            tlCategories.Settings.GridLines = GridLines.Horizontal;
            tlCategories.Width = Unit.Percentage(100);
            tlCategories.SettingsEditing.Mode = TreeListEditMode.EditFormAndDisplayNode;
            tlCategories.SettingsBehavior.AllowSort = false;
            tlCategories.SettingsSelection.Enabled = true;
            tlCategories.SettingsSelection.Recursive = true;
            tlCategories.SettingsSelection.AllowSelectAll = false;

            tlCategories.KeyFieldName = "CategoryId";
            tlCategories.ParentFieldName = "ParentCategoryId";

            TreeListTextColumn tlcCategoryName = new TreeListTextColumn();
            tlcCategoryName.Caption = "Category";
            tlcCategoryName.Name = "Name";
            tlcCategoryName.FieldName = "Name";
            tlcCategoryName.CellStyle.HorizontalAlign = HorizontalAlign.Left;

            tlCategories.Columns.Add(tlcCategoryName);
        }

        private void SetSheetFilterValues()
        {
            cbShowProducts.Checked = SheetFilter.ShowProducts;
            cbShowProductQuantity.Checked = SheetFilter.ShowProductQuantity;
            cbShowProductPrice.Checked = SheetFilter.ShowProductPrice;
            cbShowAlterations.Checked = SheetFilter.ShowAlterations;
            cbShowServiceMethod.Checked = SheetFilter.ShowServiceMethod;
            cbShowCheckoutMethod.Checked = SheetFilter.ShowCheckoutMethod;
            cbShowCustomerInfo.Checked = SheetFilter.ShowCustomerInfo;
            cbShowOrderNotes.Checked = SheetFilter.ShowOrderNotes;
            cbShowSystemProducts.Checked = SheetFilter.ShowSystemProducts;
            cbShowProductCategory.Checked = SheetFilter.ShowProductCategory;
            cbShowPaymentMethod.Checked = SheetFilter.ShowPaymentMethod;
            cbShowCardSummary.Checked = SheetFilter.ShowCardSummary;
            cbShowDeliveryPoint.Checked = SheetFilter.ShowDeliveryPoint;
            cbShowCoversCount.Checked = SheetFilter.ShowCoversCount;

            rbCategoryFilterType.Value = (int)SheetFilter.CategoryFilterType;

            // Set selected nodes
            if (!IsPostBack)
            {
                foreach (int excludedCategoryId in SheetFilter.CategoryFilterIds)
                {
                    TreeListNode node = tlCategories.FindNodeByKeyValue(excludedCategoryId.ToString());

                    if (node == null)
                    {
                        continue;
                    }

                    node.Selected = true;
                }

                if (SheetFilter.IncludeTaxTariffIds.Count > 0)
                {
                    lbTaxTariffs.SelectValues(SheetFilter.IncludeTaxTariffIds);
                }
            }
        }

        public SpreadSheetFilter GetSheetFilter()
        {
            List<int> selectedSystemProducts = new List<int>();
            if (lbSystemProducts.SelectedItemsCount != lbSystemProducts.Items.Count)
            {
                selectedSystemProducts = lbSystemProducts.SelectedItemsValuesAsIntList;
            }

            SheetFilter.OutletId = cbOutletFilter.Value;
            SheetFilter.ServiceMethods = lbOutletServiceMethods.SelectedItemsValuesAsIntList;
            SheetFilter.CheckoutMethods = lbOutletCheckoutMethods.SelectedItemsValuesAsIntList;
            SheetFilter.ShowProducts = cbShowProducts.Checked;
            SheetFilter.ShowProductQuantity = cbShowProductQuantity.Enabled && cbShowProductQuantity.Checked;
            SheetFilter.ShowProductPrice = cbShowProductPrice.Checked; // Can also be checked when system products are active
            SheetFilter.ShowProductCategory = cbShowProductCategory.Enabled && cbShowProductCategory.Checked;
            SheetFilter.ShowAlterations = cbShowAlterations.Enabled && cbShowAlterations.Checked;
            SheetFilter.ShowServiceMethod = cbShowServiceMethod.Checked;
            SheetFilter.ShowCheckoutMethod = cbShowCheckoutMethod.Checked;
            SheetFilter.ShowCustomerInfo = cbShowCustomerInfo.Checked;
            SheetFilter.ShowOrderNotes = cbShowOrderNotes.Checked;
            SheetFilter.ShowPaymentMethod = cbShowPaymentMethod.Checked;
            SheetFilter.ShowCardSummary = cbShowCardSummary.Checked;
            SheetFilter.ShowDeliveryPoint = cbShowDeliveryPoint.Checked;
            SheetFilter.ShowSystemProducts = cbShowSystemProducts.Checked;
            SheetFilter.ShowCoversCount = cbShowCoversCount.Checked;
            SheetFilter.CategoryFilterType = rbCategoryFilterType.Value.ToEnum<IncludeExcludeFilter>();
            SheetFilter.CategoryFilterIds = tlCategories.GetSelectedNodes(false).Select(x => int.Parse(x.Key)).ToList();
            SheetFilter.IncludedSystemProductTypes = selectedSystemProducts;
            SheetFilter.IncludeTaxTariffIds = lbTaxTariffs.SelectedItemsValuesAsIntList;

            return SheetFilter;
        }
    }
}