<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.ReportTemplateSheets.TransactionsAppLessReportSheetFilter" Codebehind="TransactionsAppLessReportSheetFilter.ascx.cs" %>
<%@ Register TagPrefix="dxwtl" Namespace="DevExpress.Web.ASPxTreeList" Assembly="DevExpress.Web.ASPxTreeList.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<D:Panel runat="server" GroupingText="Outlet filter" ID="pnlOutletFilter">
	<table class="dataformV2">
		<tr>
			<td class="label">Outlet</td>
			<td class="control">
				<X:ComboBoxInt ID="cbOutletFilter" runat="server" ValueField="OutletId" TextField="Name" AutoPostBack="True" notdirty="true" />
				<small>Optional filter: When no outlet is selected, all outlets will be used in the report</small>
			</td>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
		</tr>

		<D:PlaceHolder runat="server" ID="plhOutletMethods" Visible="False">
			<tr>
				<td class="label">Service Methods</td>
				<td class="control">
					<X:ListBox runat="server" ID="lbOutletServiceMethods" SelectionMode="CheckColumn" TextField="Name" ValueField="ServiceMethodId" Height="150" />
				</td>
				<td class="label">Checkout Methods</td>
				<td class="control">
					<X:ListBox runat="server" ID="lbOutletCheckoutMethods" SelectionMode="CheckColumn" TextField="Name" ValueField="CheckoutMethodId" Height="150" />
				</td>
			</tr>
		</D:PlaceHolder>
	</table>
</D:Panel>

<D:Panel runat="server" GroupingText="Report Configuration" ID="pnlReportTemplateConfig">
	<table class="dataformV2">
		<tr>
			<td class="label">Include products</td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbShowProducts" AutoPostBack="True" notdirty="true" /></td>
			<td class="label">Show product quantity</td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbShowProductQuantity" /></td>
		</tr>
		<tr>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
			<td class="label">Show product price</td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbShowProductPrice" /></td>
		</tr>
		<tr>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
			<td class="label">Show alterations</td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbShowAlterations" /></td>
		</tr>
		<tr>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
			<td class="label">Show category</td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbShowProductCategory" /></td>
		</tr>
		<tr>
			<td class="label">Include service method</td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbShowServiceMethod" /></td>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
		</tr>
		<tr>
			<td class="label">Include checkout method</td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbShowCheckoutMethod" /></td>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
		</tr>
		<tr>
			<td class="label">Include customer information</td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbShowCustomerInfo" /></td>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
		</tr>
		<tr>
			<td class="label">Include order notes</td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbShowOrderNotes" /></td>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
		</tr>
        <tr>
            <td class="label">Include payment method</td>
            <td class="control">
                <D:CheckBox runat="server" ID="cbShowPaymentMethod"/>
            </td>
            <td class="label">&nbsp;</td>
            <td class="control">&nbsp;</td>
        </tr>
        <tr>
            <td class="label">Include last 4 card digits</td>
            <td class="control">
                <D:CheckBox runat="server" ID="cbShowCardSummary"/>
            </td>
            <td class="label">&nbsp;</td>
            <td class="control">&nbsp;</td>
        </tr>
		<tr>
			<td class="label">
				Include delivery point
            </td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbShowDeliveryPoint" />
            </td>
            <td class="label"></td>
            <td class="control"></td>
        </tr>
		<tr>
			<td class="label">
				Include covers count
            </td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbShowCoversCount" />
            </td>
            <td class="label"></td>
            <td class="control"></td>
        </tr>
		<tr>
			<td class="label">Include system products</td>
			<td class="control">
				<D:CheckBox runat="server" ID="cbShowSystemProducts" AutoPostBack="True" notdirty="true" /></td>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
		</tr>
		<tr>
			<td class="label">&nbsp;</td>
			<td class="control">
				<X:ListBoxInt runat="server" ID="lbSystemProducts" SelectionMode="CheckColumn" Height="100" />
			</td>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
		</tr>
		<tr>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
		</tr>
		<tr>
			<td class="label">Category filter</td>
			<td class="control">
				<D:RadioButtonList runat="server" ID="rbCategoryFilterType" RepeatDirection="Horizontal" />
			</td>
			<td class="label">Tax tariff filter</td>
			<td class="control" rowspan="2">
				<X:ListBoxInt runat="server" ID="lbTaxTariffs" SelectionMode="CheckColumn" Height="100" />
				<small>When no tariffs are selected, the tax tariff filter will not be applied to the report</small>
			</td>
		</tr>
		<tr>
			<td class="label">&nbsp;</td>
			<td class="control">
				<dxwtl:ASPxTreeList ID="tlCategories" ClientInstanceName="tlCategories" runat="server" EnableViewState="False" />
			</td>
			<td class="label">&nbsp;</td>
		</tr>
	</table>
</D:Panel>
