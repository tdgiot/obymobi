﻿using DevExpress.Web.ASPxTreeList;
using Obymobi.Cms.WebApplication.Old_App_Code.Extensions;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.Analytics.DataSources;
using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Model.ProductReport;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Obymobi.ObymobiCms.Analytics.ReportTemplateSheets
{
	public partial class ProductSalesReportSheetFilter : UserControl, IReportSheetFilterControl
	{
		private ProductReportSheetFilter SheetFilter { get; set; }

		public new void Load(SpreadSheetFilter sheetFilter, int companyId)
		{
			SheetFilter = sheetFilter as ProductReportSheetFilter;

			DataBindOutlets(companyId);
			DataBindCategories(companyId);

			SetSheetFilterValues();
		}

		public SpreadSheetFilter GetSheetFilter()
		{
			SheetFilter.OutletId = cbOutletFilter.Value;
			SheetFilter.ServiceMethods = lbOutletServiceMethods.SelectedItemsValuesAsIntList;
			SheetFilter.CheckoutMethods = lbOutletCheckoutMethods.SelectedItemsValuesAsIntList;
			SheetFilter.CategoryFilterType = rbCategoryFilterType.Value.ToEnum<IncludeExcludeFilter>();
			SheetFilter.CategoryFilterIds = tlCategories.GetSelectedNodes(false).Select(x => int.Parse(x.Key)).ToList();
			SheetFilter.IncludedTagIds = tagBox.GetSelectedTags().Select(tag => tag.TagId).ToArray();
            SheetFilter.ShowExternalIdentifier = cbShowExternalIdentifier.Checked;

			return SheetFilter;
		}

		protected override void OnInit(EventArgs e)
		{
			rbCategoryFilterType.DataBindEnum<IncludeExcludeFilter>();
			
			tlCategories.Load += OnCategoriesLoad;

			CreateCategoryTreeList();

			base.OnInit(e);
		}

		private void OnCategoriesLoad(object sender, EventArgs args) => tlCategories.SettingsSelection.Recursive = true;

		protected void Page_Load(object sender, EventArgs e)
		{
			// Events
			cbOutletFilter.SelectedIndexChanged += (s, args) =>
			{
				UpdateOutlet();
				lbOutletServiceMethods.SelectAll();
				lbOutletCheckoutMethods.SelectAll();
			};

			UpdateOutlet();
        }

		private void UpdateOutlet()
		{
			int outletId = cbOutletFilter.Value.GetValueOrDefault();
			if (outletId <= 0)
			{
				plhOutletMethods.Visible = false;
			}
			else
			{
				plhOutletMethods.Visible = true;

				DataBindServiceMethods(outletId);
				DataBindCheckoutMethods(outletId);
			}
		}

        private void DataBindServiceMethods(int outletId)
		{
			IEntityFieldCore[] serviceMethodIncludeFieldsList =
			{
				ServiceMethodFields.ServiceMethodId,
				ServiceMethodFields.Name
			};

			lbOutletServiceMethods.DataBindEntityCollection<ServiceMethodEntity>(ServiceMethodFields.OutletId == outletId, ServiceMethodFields.Name, serviceMethodIncludeFieldsList);

			if (!IsPostBack)
			{
				if (SheetFilter.ServiceMethods.Count > 0)
				{
					lbOutletServiceMethods.SelectValues(SheetFilter.ServiceMethods);
				}
				else
				{
					lbOutletServiceMethods.SelectAll();
				}
			}
		}

		private void DataBindCheckoutMethods(int outletId)
		{
			IEntityFieldCore[] checkoutMethodIncludeFieldsList =
			{
				CheckoutMethodFields.CheckoutMethodId,
				CheckoutMethodFields.Name
			};

			lbOutletCheckoutMethods.DataBindEntityCollection<CheckoutMethodEntity>(CheckoutMethodFields.OutletId == outletId, CheckoutMethodFields.Name, checkoutMethodIncludeFieldsList);

			if (!IsPostBack)
			{
				if (SheetFilter.CheckoutMethods.Count > 0)
				{
					lbOutletCheckoutMethods.SelectValues(SheetFilter.CheckoutMethods);
				}
				else
				{
					lbOutletCheckoutMethods.SelectAll();
				}
			}
		}

		private void DataBindOutlets(int companyId)
		{
			PredicateExpression filter = new PredicateExpression(OutletFields.CompanyId == companyId);

			IEntityFieldCore[] includeFieldsList =
			{
				OutletFields.OutletId,
				OutletFields.Name
			};

			cbOutletFilter.DataBindEntityCollection<OutletEntity>(filter, OutletFields.Name, includeFieldsList);
			cbOutletFilter.SelectedIndex = cbOutletFilter.Items.FindByValue(SheetFilter.OutletId).Index;
		}

		private void DataBindCategories(int companyId)
		{
			tlCategories.DataSource = new AnalyticsCategoryDataSource(companyId);
			tlCategories.DataBind();
			tlCategories.ExpandToLevel(3);
		}

        private void CreateCategoryTreeList()
		{
			tlCategories.AutoGenerateColumns = false;
			tlCategories.Settings.GridLines = GridLines.Horizontal;
			tlCategories.Width = Unit.Percentage(100);
			tlCategories.SettingsEditing.Mode = TreeListEditMode.EditFormAndDisplayNode;
			tlCategories.SettingsBehavior.AllowSort = false;
			tlCategories.SettingsSelection.Enabled = true;
			tlCategories.SettingsSelection.Recursive = true;
			tlCategories.SettingsSelection.AllowSelectAll = false;

			tlCategories.KeyFieldName = "CategoryId";
			tlCategories.ParentFieldName = "ParentCategoryId";

			TreeListTextColumn tlcCategoryName = new TreeListTextColumn();
			tlcCategoryName.Caption = "Category";
			tlcCategoryName.Name = "Name";
			tlcCategoryName.FieldName = "Name";
			tlcCategoryName.CellStyle.HorizontalAlign = HorizontalAlign.Left;

			tlCategories.Columns.Add(tlcCategoryName);
		}

		private void SetSheetFilterValues()
		{
            cbShowExternalIdentifier.Checked = SheetFilter.ShowExternalIdentifier;
			rbCategoryFilterType.Value = (int) SheetFilter.CategoryFilterType;

			// Set selected nodes
			if (!IsPostBack)
			{
				foreach (int excludedCategoryId in SheetFilter.CategoryFilterIds)
				{
					TreeListNode node = tlCategories.FindNodeByKeyValue(excludedCategoryId.ToString());

					if (node == null)
					{
						continue;
					}

					node.Selected = true;
				}
			}

			tagBox.SetSelectedTags(SheetFilter.IncludedTagIds);
		}

		public override void Dispose()
		{
			tlCategories.Load -= OnCategoriesLoad;

			base.Dispose();
		}
	}
}
