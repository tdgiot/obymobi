﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.ReportTemplateSheets.InventoryReportSheetFilterControl" Codebehind="InventoryReportSheetFilterControl.ascx.cs" %>
<%@ Register TagPrefix="dxwtl" Namespace="DevExpress.Web.ASPxTreeList" Assembly="DevExpress.Web.ASPxTreeList.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<D:Panel runat="server" GroupingText="Outlet filter" ID="pnlOutletFilter">
	<table class="dataformV2">
		<tr>
			<td class="label">Outlet</td>
			<td class="control">
				<X:ComboBoxInt ID="cbOutletFilter" runat="server" ValueField="OutletId" TextField="Name" AutoPostBack="True" notdirty="true" />
				<small>Optional filter: When no outlet is selected, all outlets will be used in the report</small>
			</td>
			<td class="label">&nbsp;</td>
			<td class="control">&nbsp;</td>
		</tr>

		<D:PlaceHolder runat="server" ID="plhOutletMethods" Visible="False">
			<tr>
				<td class="label">Service Methods</td>
				<td class="control">
					<X:ListBox runat="server" ID="lbOutletServiceMethods" SelectionMode="CheckColumn" TextField="Name" ValueField="ServiceMethodId" Height="150" />
				</td>
				<td class="label">Checkout Methods</td>
				<td class="control">
					<X:ListBox runat="server" ID="lbOutletCheckoutMethods" SelectionMode="CheckColumn" TextField="Name" ValueField="CheckoutMethodId" Height="150" />
				</td>
			</tr>
		</D:PlaceHolder>
	</table>
</D:Panel>

<D:Panel runat="server" GroupingText="Report Configuration" ID="pnlReportTemplateConfig">
	<table class="dataformV2">
		<tr>
            <td class="label">Include PLU / SKU</td>
            <td class="control"><D:CheckBox runat="server" ID="cbShowExternalIdentifier" /></td>
            <td class="label">&nbsp;</td>
            <td class="control">&nbsp;</td>
        </tr>
		<CC:FeatureTogglePanel ToggleType="Tagging_Cms_EnableTagEditing" runat="server">
			<tr>
				<td class="label">Tags</td>
				<td class="control">
					<CC:TagBox ID="tagBox" ClientInstanceName="TagBoxControl" TextField="Name" ValueField="Id" runat="server" AllowCustomTokens="false" />
				</td>
				<td class="label"></td>
				<td class="control"></td>
			</tr>
			<tr>
				<td class="label"></td>
				<td class="control token-overview-control">
					<CC:TagLegend ID="TagLegend" runat="server" />
				</td>
				<td class="label"></td>
				<td class="control"></td>
			</tr>
		</CC:FeatureTogglePanel>
		<tr>
			<td class="label">Category filter</td>
			<td class="control">
				<D:RadioButtonList runat="server" ID="rbCategoryFilterType" RepeatDirection="Horizontal" />
			</td>
			<td class="label"></td>
			<td class="control"></td>
		</tr>
		<tr>
			<td class="label">&nbsp;</td>
			<td class="control">
				<dxwtl:ASPxTreeList ID="tlCategories" ClientInstanceName="tlCategories" runat="server" EnableViewState="False" />
			</td>
			<td class="label">&nbsp;</td>
		</tr>
	</table>
</D:Panel>
