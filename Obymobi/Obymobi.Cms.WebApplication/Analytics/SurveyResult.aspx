﻿<%@ Page Title="Survey results" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.SurveyResult" Codebehind="SurveyResult.aspx.cs" %>
<%@ Register Assembly="DevExpress.XtraCharts.v20.1.Web" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v20.1" Namespace="DevExpress.XtraCharts" TagPrefix="dxcharts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <D:Button runat="server" ID="btnRefresh" Text="Verversen" CausesValidation="true"/>	
    <D:Button runat="server" ID="btnExportToExcel" Text="Resultaten exporteren" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">	
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Overzicht" Name="Overview">
				<Controls>
                    <table class="dataformV2">
		                <tr>
			                <td class="label">
				                <D:Label runat="server" id="lblCompanyId">Bedrijf</D:Label>
			                </td>
			                <td class="control">
				                <X:ComboBoxLLBLGenEntityCollection ID="ddlCompanyId" runat="server" EntityName="Company" IsRequired="true"></X:ComboBoxLLBLGenEntityCollection>
			                </td>							
                            <td class="label">
                            </td>
                            <td class="control">
                            </td>		
		                </tr>
                        <tr>
                            <td class="label">
                                <D:Label runat="server" id="lblSurveyId">Enquête</D:Label>
			                </td>
			                <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSurveyId" IncrementalFilteringMode="StartsWith" EntityName="Survey" TextField="Name" ValueField="SurveyId" IsRequired="true"/>
			                </td>				
                            <td class="label">
                                <D:Label runat="server" id="lblSubmitted">Gesubmit</D:Label>
                            </td>
                            <td class="control">
                                <X:ComboBoxLLBLGenEntityCollection runat="server" ID="ddlSurveyResultId" IncrementalFilteringMode="StartsWith" EntityName="SurveyResult" TextField="SubmittedAndTableString" ValueField="SurveyResultId" PreventEntityCollectionInitialization="true" />
                            </td>	
                        </tr>
		                <tr>
			                <td class="label">
				                <D:Label runat="server" id="lblDateFrom">Datum van</D:Label>
			                </td>
			                <td class="control">
                                <X:DateEdit runat="server" ID="deDateFrom"></X:DateEdit>
			                </td>							
			                <td class="label">
				                <D:Label runat="server" id="lblDateTo">Datum tot</D:Label>
			                </td>
			                <td class="control">
                                <X:DateEdit runat="server" ID="deDateTo"></X:DateEdit>
			                </td>							
		                </tr>
                    </table>
                    <D:Panel ID="pnlSurveyResults" GroupingText="Survey results" CssClass="pnlSurveyResults"></D:Panel>                
                    <D:PlaceHolder ID="plhValues" runat="server"></D:PlaceHolder>
                    <div class="survey-bottom-bar"></div>
				</Controls>
			</X:TabPage>
        </TabPages>
    </X:PageControl>	
    
</asp:Content>

