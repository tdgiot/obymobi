﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageEntity.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.ReportProcessingTask" Codebehind="ReportProcessingTask.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhAdditionalToolBarButtons" Runat="Server">
    &nbsp;<X:ToolBarButton runat="server" ID="btRerunCopy" CommandName="RerunCopy" Text="Copy to Re-run" Image-Url="~/Images/Icons/table_refresh.png" />
    &nbsp;<X:ToolBarButton runat="server" ID="btnRefresh" CommandName="RefreshPage" Text="Refresh" Image-Url="~/Images/Icons/table_refresh.png" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhPageContent" Runat="Server">
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Algemeen" Name="Generic">
				<controls>
                    <D:PlaceHolder runat="server">
					    <table class="dataformV2">
						    <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" id="lblReportType" LocalizeText="False">Report</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <span>
                                        <D:LabelTextOnly runat="server" id="lblReportTypeValue" LocalizeText="false"></D:LabelTextOnly>
                                    </span>
                                </td>
                                <td class="label">
                                    <D:Label runat="server" id="lblCreatedCaption">Created</D:Label>
                                </td>
                                <td class="control">
                                    <D:Label runat="server" id="lblCreatedValue" LocalizeText="false"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" id="lblTimeZone" LocalizeText="False">Time zone</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <span><D:LabelTextOnly runat="server" id="lblTimeZoneValue" LocalizeText="false"></D:LabelTextOnly></span>
                                </td>
                                <td class="label">
                                    <D:LabelTextOnly runat="server" id="lblStatus">Status</D:LabelTextOnly>
                                </td>
                                <td class="control">
                                    <span>
                                        <D:LabelTextOnly runat="server" id="lblStatusQueued">In wachtrij</D:LabelTextOnly>
                                        <D:LabelTextOnly runat="server" id="lblFailed">Mislukt</D:LabelTextOnly>
                                        <D:LinkButton runat="server" ID="hlStatusReadyForDownload">Gereed om te downloaden</D:LinkButton>
                                    </span>
                                </td>
                            </tr>
                            <D:PlaceHolder runat="server" ID="plhFilter">
					        <tr>						
							    <td class="label">
								    <D:Label runat="server" id="lblReportConfiguration" LocalizeText="False">Configuration</D:Label>
							    </td>
							    <td class="control" colspan="3">
                                    <span>
                                        <!-- Pre, so keep this way! -->
                                        <pre><D:LabelTextOnly runat="server" id="lblReportConfigurationValue" LocalizeText="false"></D:LabelTextOnly></pre>
                                        <D:LinkButton runat="server" ID="btDownloadTemplate" Visible="false">Download Template</D:LinkButton>
                                    </span>
							    </td>        		
						    </tr>
                            </D:PlaceHolder>
                            <D:PlaceHolder runat="server" ID="plhException">
					        <tr>						
							    <td class="label">
								    <D:LabelTextOnly runat="server" id="lblExceptionTextLabel">Foutmelding</D:LabelTextOnly>
							    </td>
							    <td class="control" colspan="3">
                                    <span>
                                        <!-- Pre, so keep this way! -->
                                        <pre><D:LabelTextOnly runat="server" id="lblExceptionTextValue" LocalizeText="false"></D:LabelTextOnly></pre>
                                    </span>
							    </td>        		
						    </tr>
                            </D:PlaceHolder>
					    </table>
                    </D:PlaceHolder>
				</Controls>
			</X:TabPage>                    				
		</TabPages>
	</X:PageControl>
</div>
</asp:Content>

