﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.ReportScheduling" Codebehind="ReportScheduling.aspx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v20.1" Namespace="DevExpress.Web.ASPxScheduler" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">Report scheduling
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">    
<div>
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Generic" Name="Generic">
				<Controls>
                    <dx:ASPxScheduler ID="scheduler" runat="server" Width="100%" ActiveViewType="Timeline" ClientIDMode="static" AppointmentDataSourceID="scheduleDataSource">
                        <OptionsToolTips AppointmentToolTipUrl="~/Analytics/Scheduling/SchedulingAppointmentTooltip.ascx" AppointmentToolTipCornerType="None" />
                        <OptionsBehavior ShowViewSelector="true" ShowFloatingActionButton="False"/>
                        <Storage>
                        <Appointments>
                            <Mappings 
                                AppointmentId="Id" 
                                Start="StartTime" 
                                End="EndTime" 
                                Subject="Subject" 
                                AllDay="AllDay" 
                                Description="Description" 
                                Label="Label" 
                                Location="Location" 
                                RecurrenceInfo="RecurrenceInfo" 
                                ReminderInfo="ReminderInfo" 
                                Status="Status"                 
                                Type="Type" />
                            <CustomFieldMappings>
                                <dx:ASPxAppointmentCustomFieldMapping Member="ReportProcessingTaskTemplateId" Name="ReportProcessingTaskTemplateId" ValueType="Integer" />
                                <dx:ASPxAppointmentCustomFieldMapping Member="BackgroundColor" Name="BackgroundColor" ValueType="Integer" />
                                <dx:ASPxAppointmentCustomFieldMapping Member="TextColor" Name="TextColor" ValueType="Integer" />                
                            </CustomFieldMappings>
                        </Appointments>
                        </Storage>
                        <OptionsForms AppointmentFormTemplateUrl="~/Analytics/Scheduling/SchedulingAppointmentModal.ascx" />
                    </dx:ASPxScheduler>
                    <asp:ObjectDataSource ID="scheduleDataSource" runat="server" 
                        DataObjectTypeName="SchedulingAppointment"
                        DeleteMethod="DeleteMethodHandler"
                        InsertMethod="InsertMethodHandler" 
                        SelectMethod="SelectMethodHandler" 
                        UpdateMethod="UpdateMethodHandler" 
                        TypeName="SchedulingDataSource" 
                        onobjectcreated="scheduleDataSource_ObjectCreated"> 
                    </asp:ObjectDataSource> 
                    <script type="text/javascript">
                        // <![CDATA[
                        scheduler.menuManager.UpdateAppointmentMenuItems = function (appointmentViewInfo, menu)
                        {
                            var apt = this.scheduler.GetAppointment(appointmentViewInfo.appointmentId);
                            if (apt == null)
                                return;
                            var aptType = apt.appointmentType;
                            var isRecurring = aptType == ASPxAppointmentType.Pattern || aptType == ASPxAppointmentType.Occurrence;
                            
                            menu.GetItemByName("EditItem").SetVisible(!isRecurring);
                            menu.GetItemByName("EditPattern").SetVisible(isRecurring);
                            menu.GetItemByName("RestoreItem").SetVisible(false);
                            return this.constructor.prototype.UpdateAppointmentMenuItems.call(this, appointmentViewInfo, menu);
                        }

                        function DefaultAppointmentMenuHandler(control, s, args)
                        {
                            if (args.item.name == "AddItem") {
                                control.RaiseCallback('MNUVIEW|NewAppointment');
                            }
                            else if (args.item.name == "EditItem") {
                                control.RaiseCallback('MNUAPT|OpenAppointment');
                            }
                            else if (args.item.name == "EditPattern") {
                                control.RaiseCallback('MNUAPT|EditSeries');
                            }
                            else if (args.item.name == "RestoreItem") {
                                control.RaiseCallback('MNUAPT|RestoreOccurrence');
                            }
                            else if (args.item.name == "DeleteAppointment") {
                                control.RaiseCallback("MNUAPT|DeleteAppointment");
                            }
                        }
                        // ]]> 
                    </script>
                </Controls>                    
            </X:TabPage>
        </TabPages>
	</X:PageControl>
</div>
</asp:Content>