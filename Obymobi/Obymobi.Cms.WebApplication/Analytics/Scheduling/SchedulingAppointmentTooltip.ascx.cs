﻿using DevExpress.Web.ASPxScheduler;
using System.Web.UI;

public partial class SchedulingAppointmentTooltip : ASPxSchedulerToolTipBase
{
    public override bool ToolTipShowStem { get { return false; } }
    public override string ClassName { get { return "ASPxClientAppointmentToolTip"; } }
    
    protected override Control[] GetChildControls()
    {
        Control[] controls = new Control[] { lblSubject, lblLocation, lblInterval, lblShowMenu, btnDelete, btnEdit };
        return controls;
    }
}