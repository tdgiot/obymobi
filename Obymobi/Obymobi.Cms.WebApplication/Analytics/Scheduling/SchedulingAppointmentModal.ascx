﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="SchedulingAppointmentModal" Codebehind="SchedulingAppointmentModal.ascx.cs" %>
<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v20.1" Namespace="DevExpress.Web.ASPxScheduler" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.XtraScheduler.v20.1.Core" Namespace="DevExpress.XtraScheduler" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxScheduler.v20.1" Namespace="DevExpress.Web.ASPxScheduler.Controls" TagPrefix="dx" %>
<table id="tbScheduleAppointment" class="dataformV2">
    <tr>
		<td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblReportProcessingTaskTemplateId">Report template</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control" colspan="3">
            <X:ComboBoxInt runat="server" ID="cbReportProcessingTaskTemplateId" ClientIDMode="Static" CssClass="input" TextField="FriendlyName" ValueField="ReportProcessingTaskTemplateId" IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000" DisplayEmptyItem="False">
                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                <ClientSideEvents Validation="function(s, e) { OnReportProcessingTaskTemplateValidate(s, e); }" ValueChanged="function(s, e) { OnUpdateControlValue(s, e); }" KeyUp="function(s,e) { OnUpdateControlValue(s,e); }" />
            </X:ComboBoxInt>
        </td>		
	</tr>
    <tr>
		<td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblBackgroundColor">Background Color</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
             <dxe:ASPxColorEdit runat="server" ID="ceBackgroundColor" UseDataBinding="false" EnableCustomColors="true" />
	    </td>
        <td class="label">
		    <D:LabelEntityFieldInfo runat="server" id="lblTextColor">Text Color</D:LabelEntityFieldInfo>
	    </td>
	    <td class="control">
             <dxe:ASPxColorEdit runat="server" ID="ceTextColor" UseDataBinding="false" EnableCustomColors="true" />
	    </td>
	</tr>
    <tr>
		<td class="label">
            <D:Label runat="server" id="lblStartTime">Time</D:Label>
        </td>
        <td class="control" colspan="3">
            <dxe:ASPxDateEdit ID="edtStartDate" ClientIDMode="Static" runat="server" EditFormat="DateTime" DateOnError="Undo" AllowNull="false" Date='<%# ((AppointmentFormTemplateContainer)Container).Start %>' />
        </td>
    </tr>
    <tr>
        <td class="label">
	    </td>
	    <td class="control" colspan="3">
            <dx:AppointmentRecurrenceForm ID="AppointmentRecurrenceForm1" runat="server"
                IsRecurring='<%# ((AppointmentFormTemplateContainer)Container).Appointment.IsRecurring %>' 
                DayNumber='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceDayNumber %>' 
                End='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceEnd %>' 
                Month='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceMonth %>' 
                OccurrenceCount='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceOccurrenceCount %>' 
                Periodicity='<%# ((AppointmentFormTemplateContainer)Container).RecurrencePeriodicity %>' 
                RecurrenceRange='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceRange %>' 
                Start='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceStart %>' 
                WeekDays='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceWeekDays %>' 
                WeekOfMonth='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceWeekOfMonth %>' 
                RecurrenceType='<%# ((AppointmentFormTemplateContainer)Container).RecurrenceType %>'
                IsFormRecreated='<%# ((AppointmentFormTemplateContainer)Container).IsFormRecreated %>'>
            </dx:AppointmentRecurrenceForm>
        </td>
    </tr>
</table>
<table style="width: 100%; height: 35px;">
    <tr>
        <td style="width: 100%; height: 100%;" align="center">
            <table style="height: 100%;">
                <tr>
                    <td colspan="4">
                        <D:Label ID="lblError" ClientIDMode="Static" runat="server" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <dxe:ASPxButton runat="server" ID="btnOk" Text="OK" UseSubmitBehavior="False" AutoPostBack="False" ClientInstanceName="btnOk" EnableViewState="False" Width="91px" />
                    </td>
                    <td style="padding: 0 4px;">
                        <dxe:ASPxButton runat="server" ID="btnCancel" Text="Cancel" UseSubmitBehavior="false" AutoPostBack="false" EnableViewState="false" Width="91px" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td style="width: 100%;" align="left">
            <dx:ASPxSchedulerStatusInfo runat="server" ID="schedulerStatusInfo" Priority="1" MasterControlId='<%# ((AppointmentFormTemplateContainer)Container).ControlId %>' />
        </td>
    </tr>
</table>
<script id="dxss_myAppoinmentForm007" type="text/javascript">
    // <![CDATA[
    function OnReportProcessingTaskTemplateValidate(s, e) {
        if (!e.isValid)
            return;
        e.isValid = SelectedItemCount() == 1;
        e.errorText = "Please select one item";
        btnOk.SetEnabled(e.isValid);
    }
    function SelectedItemCount() {
        var count = 0;
        if (cbReportProcessingTaskTemplateId.GetValue() != null) {
            count++;
        }
        return count;
    }
    function OnUpdateControlValue(s, e) {
        var isValid = ASPxClientEdit.ValidateEditorsInContainer(null);
        btnOk.SetEnabled(isValid);
    }

    window.setTimeout("ASPxClientEdit.ValidateEditorsInContainer(null)", 0);
    // ]]> 
</script>