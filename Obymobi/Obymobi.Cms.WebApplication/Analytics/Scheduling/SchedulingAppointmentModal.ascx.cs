﻿using System;
using DevExpress.Web;
using DevExpress.Web.ASPxScheduler;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;

public partial class SchedulingAppointmentModal : SchedulerFormControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public override void DataBind()
    {
        base.DataBind();

        SchedulingAppointmentForm container = (SchedulingAppointmentForm)Parent;
        this.DataBindReportProcessingTaskTemplate(container);

        if (container.IsNewAppointment)
        {
            this.ceBackgroundColor.Color = System.Drawing.Color.White;
            this.ceTextColor.Color = System.Drawing.Color.Black;
        }
        else
        {
            this.ceBackgroundColor.Color = System.Drawing.Color.FromArgb(container.BackgroundColor);
            this.ceTextColor.Color = System.Drawing.Color.FromArgb(container.TextColor);
        }

        this.btnOk.ClientSideEvents.Click = container.SaveHandler;
        this.btnCancel.ClientSideEvents.Click = container.CancelHandler;
    }

    private void DataBindReportProcessingTaskTemplate(SchedulingAppointmentForm container)
    {
        this.cbReportProcessingTaskTemplateId.DataBindEntityCollection<ReportProcessingTaskTemplateEntity>(ReportProcessingTaskTemplateFields.CompanyId == CmsSessionHelper.CurrentCompanyId, ReportProcessingTaskTemplateFields.FriendlyName, ReportProcessingTaskTemplateFields.FriendlyName);
        
        if (container.ReportProcessingTaskTemplateId <= 0)
        {
            this.cbReportProcessingTaskTemplateId.SelectedIndex = 0;
        }
        else
        {
            this.cbReportProcessingTaskTemplateId.SelectedItem = this.cbReportProcessingTaskTemplateId.Items.FindByValue(container.ReportProcessingTaskTemplateId);
        }
    }

    protected override ASPxEditBase[] GetChildEditors()
    {
        return new ASPxEditBase[] { this.cbReportProcessingTaskTemplateId, this.ceBackgroundColor, this.ceTextColor };
    }

    protected override ASPxButton[] GetChildButtons()
    {
        ASPxButton[] buttons = new ASPxButton[] { btnOk, btnCancel };
        return buttons;
    }
}