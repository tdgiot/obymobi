﻿using System;
using System.Diagnostics;
using Dionysos;
using Obymobi.Enums;
using Obymobi.ObymobiCms.Analytics.Subpanels;
using Obymobi.Web.Analytics.Requests;
using SpreadsheetLight;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.EntityClasses;
using Newtonsoft.Json;

namespace Obymobi.ObymobiCms.Analytics
{
    public partial class Transactions : BaseReportingPage
    {
        private ReportFilterPanel filterPanel;

        protected override void OnInit(EventArgs e)
        {
            this.InitGui();
            base.OnInit(e);
        }

        private void Page_Load(object sender, EventArgs e)
        {
            if (this.filterPanel != null)
            {
                this.filterPanel.UpdateCompanyId();
            }
        }

        void InitGui()
        {
            this.filterPanel = this.LoadControl<ReportFilterPanel>("~/Analytics/Subpanels/ReportFilterPanel.ascx");
            this.phlFilter.Controls.Add(this.filterPanel);

            this.btGenerateDoIt.Visible = (TestUtil.IsPcDeveloper && WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.Manual);
        }

        public void ValidateSelf()
        {
            this.filterPanel.Validate();

            base.Validate();
        }

        public void ShowMetadata()
        {
            this.GenerateReport(true, true);
        }

        public void GenerateReportDoIt()
        {
            this.GenerateReport(true);
        }

        public void GenerateReport()
        {
            this.GenerateReport(false);
        }

        public void GenerateReport(bool doIt, bool metaDataOnly = false)
        {
            this.ValidateSelf();
            if (this.IsValid)
            {
                TransactionsRequest request = this.filterPanel.GetTransactionsRequest();

                if (metaDataOnly)
                {
                    this.tbMetadata.Text = JsonConvert.SerializeObject(request);

                    // Ensure it's the valid serialize/deserialize
                    var validationObject = JsonConvert.DeserializeObject<TransactionsRequest>(this.tbMetadata.Text);
                    if (!JsonConvert.SerializeObject(validationObject).Equals(this.tbMetadata.Text))
                        throw new Exception("The current report selection can't be serialized.");

                    this.tabsMain.TabPages.FindByName("Metadata").Visible = true;
                    return;
                }

                Obymobi.Web.Analytics.Reports.TransactionsReport report = new Obymobi.Web.Analytics.Reports.TransactionsReport(request);
                this.GenerateReport(doIt, request, report);
            }
        }

        public void GenerateReport(bool doIt, TransactionsRequest request, Obymobi.Web.Analytics.Reports.TransactionsReport report)
        {
            if (doIt)
            {
                // Never allow this on the non development environment as it might grind the cms to a hold
                if (WebEnvironmentHelper.CloudEnvironment != CloudEnvironment.Manual && !TestUtil.IsPcGabriel)
                    throw new NotImplementedException("Do It is not implemented for non development environments.");

                Stopwatch sw = Stopwatch.StartNew();

                SLDocument spreadsheet;
                report.CreateReport(out spreadsheet);

                sw.Stop();

                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=WebStreamDownload-{0}-{1}.xlsx".FormatSafe(DateTime.Now.DateTimeToSimpleDateTimeStamp()));
                spreadsheet.SaveAs(Response.OutputStream);
                Response.End();
            }
            else
            {
                ReportProcessingTaskEntity task = new ReportProcessingTaskEntity();
                task.CompanyId = request.CompanyId;
                task.FromUTC = request.FromUtc;
                task.TillUTC = request.TillUtc;
                task.Filter = XmlHelper.Serialize(request, true);
                task.AnalyticsReportType = AnalyticsReportType.Transactions;
                task.TimeZoneOlsonId = request.TimeZoneOlsonId;
                task.Save();

                string infoMessage = this.Translate("ReportQueuedForProcessing", "Your report is ready to be processed. Click <a href=\"{0}\">here</a> to view its progress.");

                this.AddInformatorInfo(infoMessage, this.ResolveUrl("~/Analytics/ReportProcessingTask.aspx?id=" + task.ReportProcessingTaskId));
            }
        }

        //public void GenerateReportMagically()
        //{
        //    // Feeling like a dirty pig - sleazy hard coding ids
        //    int companyId = 343;
        //    List<int> dpgs = new List<int> { 644, 665, 556, 599 };
        //    List<int> dpsToExclude = new List<int> { 46074, 25395, 28156, 28174, 28197 };
        //    List<int> categoriesToExclude = new List<int> { 22463, 25314, 25479, 25661 };  // These are just the parents, any childeren will be added automagically (a few lines below)
        //    DateTime from = DateTimeUtil.MakeBeginOfDay(new DateTime(2016, 3, 7));
        //    DateTime till = DateTimeUtil.MakeEndOfDay(new DateTime(2016, 3, 13));
        //    //DateTime from = DateTime.Now.Date.AddDays(-7).MakeBeginOfDay();
        //    //DateTime till = DateTime.Now.Date.MakeEndOfDay();
        //    TransactionsRequest parametersRequest;

        //    List<int> categoriesToExcludeIncludingChilderen = new List<int>();
        //    foreach (int categoryId in categoriesToExclude)
        //    {
        //        CategoryEntity c = new CategoryEntity(categoryId);
        //        var ids = c.GetAllUnderlyingCategoryIds(c, null); // Why tha fuuck did we choose an ArrayList ?
        //        foreach(var id in ids)
        //            categoriesToExcludeIncludingChilderen.Add((int)id);
        //    }

        //    // -Orders & service requests(Settings: http://i.imgur.com/G0scavA.png) Code: Transactions.aspx.cs
        //    // Take out categories are excluded from this report(Menu: Aria Live Menu)
        //    parametersRequest = new TransactionsRequest(from, till, companyId);
        //    parametersRequest.IncludeInRoomTablets = true;
        //    parametersRequest.IncludeByodApps = false;
        //    parametersRequest.IncludeFailedOrders = false;
        //    parametersRequest.IncludeDeletedDeliverypoints = true; // ?
        //    parametersRequest.DeliverypointIds = dpsToExclude;
        //    parametersRequest.DeliverypointsExclude = true;
        //    parametersRequest.DeliverypointgroupIds = dpgs;
        //    parametersRequest.OrderStatuses = null;
        //    parametersRequest.OrderTypeSelected = false;
        //    parametersRequest.OrderType = 0;
        //    parametersRequest.IncludeOrderitems = true;
        //    parametersRequest.ReportCategoryFilter = ReportCategoryFilter.ExcludeCategories;
        //    parametersRequest.CategoryIds = categoriesToExcludeIncludingChilderen;

        //    //Obymobi.Web.Analytics.Model.Transactions.Transactions transactions = new Obymobi.Web.Analytics.Model.Transactions.Transactions(parametersRequest);
        //    //transactions.FetchAndProcessData();

        //    SLDocument spreadsheet;
            
        //    this.plhGkTest.AddHtml("<pre>{0}</pre>", JsonConvert.SerializeObject(parametersRequest));

        //    //Obymobi.Web.Analytics.Reports.TransactionsReport report = new Obymobi.Web.Analytics.Reports.TransactionsReport(transactions);
        //    //report.CreateReport(out spreadsheet);

        //    //string fileName = "Transactions_{0}_{1}_Magic.xlsx".FormatSafe(from.ToString("yyyyMMdd"), till.ToString("yyyyMMdd"));

        //    //this.Response.Clear();
        //    //this.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //    //this.Response.AddHeader("Content-Disposition", "attachment; filename={0}".FormatSafe(fileName));
        //    //spreadsheet.SaveAs(this.Response.OutputStream);
        //    //this.Response.End();

        //    // - Orders(Settings: http://i.imgur.com/kekfubD.png) Code: Transactions.aspx.cs
        //    // Take out categories are excluded from this report(Menu: Aria Live Menu)

        //    // - Orders including order items(Settings: http://i.imgur.com/yecM4kW.png) Code: Transactions.aspx.cs
        //    // Take out categories are excluded from this report(Menu: Aria Live Menu)

        //    // - Service requests(Settings: http://i.imgur.com/kNCLB6K.png) Code: Transactions.aspx.cs
        //    // Take out categories are excluded from this report(Menu: Aria Live Menu)

        //    // - Service requests including order items(Settings: http://i.imgur.com/ynAqoHc.png) Code: Transactions.aspx.cs
        //    // Take out categories are excluded from this report(Menu: Aria Live Menu)

        //    // - Take out: five 50(Settings: http://i.imgur.com/VOnRueo.png) Code: Transactions.aspx.cs
        //    // The subcategories of five 50 are included in this report. (Menu: Aria Live Menu)

        //    // - Take out: other categories(Settings: http://i.imgur.com/VOnRueo.png) Code: Transactions.aspx.cs
        //    // All the take out categories without five 50 are included in this report. (Menu: Aria Live Menu)
        //}





        protected override void SetDefaultValuesToControls()
        {
            this.filterPanel.IncludeInRoomTablets = true;
            this.filterPanel.IncludeByod = true;
            this.filterPanel.IncludeOrderitems = true;
            this.filterPanel.Period = ReportFilterPanel.ReportingPeriod.LastMonth;
            this.filterPanel.ShowFilterProduct = false;
        }
    }
}