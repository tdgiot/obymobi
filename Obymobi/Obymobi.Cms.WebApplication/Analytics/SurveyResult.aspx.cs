using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data;
using Dionysos.Diagnostics;
using Obymobi.Web.Google;
using System.Text;
using System.Data;
using Dionysos.Web.UI;
using DevExpress.XtraCharts.Web;
using DevExpress.XtraCharts;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using System.IO;
using DevExpress.XtraCharts.Native;
using Obymobi.Logic.HelperClasses;
using Dionysos;
using Dionysos.Web;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using System.Collections;
using DevExpress.Web;

namespace Obymobi.ObymobiCms.Analytics
{
    public partial class SurveyResult : PageQueryStringDataBinding
    {
        List<DataTable> datatables = new List<DataTable>();
        List<ASPxGridView> gridviews = new List<ASPxGridView>();
        SurveyQuestionCollection surveyQuestions = new SurveyQuestionCollection();
        SurveyResultCollection distinctSurveyResults = new SurveyResultCollection();
        DateTime? submitted = null;
        Dictionary<int, long> submittedResults = new Dictionary<int, long>();

        #region Methods

        private void ExportResults()
        {
            this.SetFilter();
            this.SetGui();

            PrintingSystem ps = new PrintingSystem();

            CompositeLink compositelink = new CompositeLink();
            compositelink.PrintingSystem = ps;
            
            List<PrintableComponentLink> links = new List<PrintableComponentLink>();
            
            foreach (ASPxGridView gridview in this.gridviews)
            {
                ASPxGridViewExporter gridExport = new ASPxGridViewExporter();
                gridExport.GridViewID = gridview.ID;
                gridExport.Landscape = true;
                this.Controls.Add(gridExport);

                PrintableComponentLink link = new PrintableComponentLink();
                link.Component = gridExport;
                link.PrintingSystem = ps;

                links.Add(link);
            }

            compositelink.Links.AddRange(links.ToArray());
            compositelink.BreakSpace = 100;
            compositelink.CreateDocument();
            compositelink.PrintingSystem.ExportOptions.Pdf.DocumentOptions.Author = "Crave";

            CompanyEntity company = new CompanyEntity(CmsSessionHelper.CurrentCompanyId);
            string fileName = string.Format("SurveyResults-{0}-from-{1}-to-{2}", company.Name.Replace(" ", ""),
                this.deDateFrom.Value.Value.ToString("dd-MM-yy"), this.deDateTo.Value.Value.ToString("dd-MM-yy"));

            using (MemoryStream stream = new MemoryStream())
            {
                compositelink.PrintingSystem.ExportToXls(stream);
                Response.Clear();
                Response.Buffer = false;
                Response.AppendHeader("Content-Type", "application/vnd.ms-excel");
                Response.AppendHeader("Content-Transfer-Encoding", "binary");
                Response.AppendHeader("Content-Disposition", string.Format("attachment; filename={0}.xls", fileName));
                Response.BinaryWrite(stream.GetBuffer());
                Response.End();
            }

            ps.Dispose();
        }

        protected override void SetDefaultValuesToControls()
        {            
            if (CmsSessionHelper.CurrentCompanyId > 0)
                this.ddlCompanyId.Value = CmsSessionHelper.CurrentCompanyId;

            if (CmsSessionHelper.CurrentRole == Role.Console)
                this.ddlCompanyId.Enabled = false;

            this.deDateFrom.Value = DateTimeUtil.MakeBeginOfDay(DateTime.Now);
            this.deDateTo.Value = DateTimeUtil.MakeEndOfDay(DateTime.Now);
        }    

        protected override void OnInit(EventArgs e)
        {            
            base.OnInit(e);

            if (this.ddlCompanyId.ValidId > 0)
            {
                SurveyCollection surveys = new SurveyCollection();
                surveys.GetMulti(SurveyFields.CompanyId == this.ddlCompanyId.ValidId);

                this.ddlSurveyId.DataSource = surveys;
                this.ddlSurveyId.DataBind();

                if (!this.ddlSurveyId.Value.HasValue && surveys.Count > 0)
                    this.ddlSurveyId.Value = surveys[0].SurveyId;

                PrefetchPath parentPrefetch = new PrefetchPath(EntityType.SurveyResultEntity);
                PrefetchPath childPrefetch = new PrefetchPath(EntityType.SurveyResultEntity);

                RelationCollection parentRelations = new RelationCollection();
                RelationCollection childRelations = new RelationCollection();

                // Filters
                PredicateExpression filter = new PredicateExpression();
                filter.Add(SurveyResultFields.SubmittedUTC > deDateFrom.Value);
                filter.Add(SurveyResultFields.SubmittedUTC <= deDateTo.Value.Value.AddDays(1));
                
                if (this.ddlSurveyId.Value.HasValue)
                {
                    parentRelations.Add(SurveyResultEntity.Relations.SurveyQuestionEntityUsingSurveyQuestionId);
                    parentRelations.Add(SurveyQuestionEntity.Relations.SurveyPageEntityUsingSurveyPageId, "SurveyPageThroughParentQuestion");
                    parentPrefetch.Add(SurveyResultEntity.PrefetchPathSurveyQuestionEntity).SubPath.Add(SurveyQuestionEntity.PrefetchPathSurveyPageEntity);

                    childRelations.Add(SurveyResultEntity.Relations.SurveyQuestionEntityUsingSurveyQuestionId);
                    childRelations.Add(SurveyQuestionEntity.Relations.SurveyQuestionEntityUsingSurveyQuestionIdParentQuestionId, "ParentQuestions", JoinHint.Inner);
                    childRelations.Add(SurveyQuestionEntity.Relations.SurveyPageEntityUsingSurveyPageId, "ParentQuestions", "SurveyPageThroughParentQuestion", JoinHint.Inner);
                    childPrefetch.Add(SurveyResultEntity.PrefetchPathSurveyQuestionEntity).SubPath.Add(SurveyQuestionEntity.PrefetchPathParentSurveyQuestionEntity).SubPath.Add(SurveyQuestionEntity.PrefetchPathSurveyPageEntity);
                }

                var parentResults = this.GetSurveyResults(parentRelations, parentPrefetch);
                var childResults = this.GetSurveyResults(childRelations, childPrefetch);

                parentResults.AddRange(childResults);

                SurveyResultCollection results = parentResults;

                //List<DateTime> submittedResults = new List<DateTime>();
                this.submittedResults = new Dictionary<int, long>();
                this.distinctSurveyResults = new SurveyResultCollection();

                // Get distinctive surveyResults based on the submitted datetime
                foreach (SurveyResultEntity surveyResult in results)
                {
                    if (surveyResult.SubmittedUTC.HasValue)
                    {
                        if (!this.submittedResults.Values.Any(x => x == surveyResult.SubmittedUTC.Value.ToUnixTime()))
                        {
                            this.submittedResults.Add(surveyResult.SurveyResultId, surveyResult.SubmittedUTC.Value.ToUnixTime());
                            this.distinctSurveyResults.Add(surveyResult);
                        }
                    }
                    
                }

                this.ddlSurveyResultId.DataSource = this.distinctSurveyResults;
                this.ddlSurveyResultId.DataBind();
            }
        }

        private SurveyResultCollection GetSurveyResults(RelationCollection relations, PrefetchPath prefetch)
        {
            // Filters
            PredicateExpression filter = new PredicateExpression();
            filter.Add(SurveyResultFields.SubmittedUTC > deDateFrom.Value);
            filter.Add(SurveyResultFields.SubmittedUTC < deDateTo.Value);

            if (this.ddlSurveyId.Value.HasValue)
            {
                // Extra survey filter 
                PredicateExpression subFilter = new PredicateExpression();
                subFilter.Add(SurveyPageFields.SurveyId.SetObjectAlias("SurveyPageThroughParentQuestion") == this.ddlSurveyId.Value);
                
                filter.Add(subFilter);
            }

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(SurveyResultFields.SubmittedUTC, SortOperator.Descending));

            SurveyResultCollection results = new SurveyResultCollection();
            results.GetMulti(filter, 0, null, relations, prefetch);

            return results;
        }

        private void SetFilter()
        {
            if (this.ddlSurveyId.Value > 0)
            {
                SortExpression sort = new SortExpression();
                sort.Add(new SortClause(SurveyQuestionFields.SortOrder, SortOperator.Ascending));

                var parentRelations = new RelationCollection();
                parentRelations.Add(SurveyQuestionEntity.Relations.SurveyPageEntityUsingSurveyPageId);
                parentRelations.Add(SurveyPageEntity.Relations.SurveyEntityUsingSurveyId, "SurveyBySurveyPage", JoinHint.Inner);
                parentRelations.Add(SurveyQuestionEntity.Relations.SurveyResultEntityUsingSurveyQuestionId, "SurveyResults", JoinHint.Inner);

                var allQuestions = this.GetSurveyQuestions(parentRelations, sort);

                //var childRelations = new RelationCollection();
                //childRelations.Add(SurveyQuestionEntity.Relations.SurveyQuestionEntityUsingSurveyQuestionIdParentQuestionId, "ParentQuestions", JoinHint.Inner);
                //childRelations.Add(SurveyQuestionEntity.Relations.SurveyPageEntityUsingSurveyPageId, "ParentQuestions", "SurveyPageByParentQuestion", JoinHint.Inner);
                //childRelations.Add(SurveyPageEntity.Relations.SurveyEntityUsingSurveyId, "SurveyPageByParentQuestion", "SurveyBySurveyPage", JoinHint.Inner);
                //childRelations.Add(SurveyQuestionEntity.Relations.SurveyResultEntityUsingSurveyQuestionId, "SurveyResults", JoinHint.Inner);

                //sort.Add(new SortClause(SurveyPageFields.SortOrder, SortOperator.Ascending, "SurveyPageByParentQuestion"));

                //var childQuestions = this.GetSurveyQuestions(childRelations, sort);

                //// Combine the results
                //allQuestions.AddRange(childQuestions);

                // Filter the duplicate records
                List<int> questionIds = new List<int>();
                var filteredQuestions = new SurveyQuestionCollection();
                foreach (var question in allQuestions)
                {
                    if (!questionIds.Contains(question.SurveyQuestionId))
                    {
                        questionIds.Add(question.SurveyQuestionId);
                        filteredQuestions.Add(question);
                    }
                }

                this.surveyQuestions = filteredQuestions;
            }
        }

        private SurveyQuestionCollection GetSurveyQuestions(RelationCollection relations, SortExpression sort)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(SurveyFields.SurveyId.SetObjectAlias("SurveyBySurveyPage") == this.ddlSurveyId.Value);
            filter.Add(SurveyResultFields.SubmittedUTC.SetObjectAlias("SurveyResults") > deDateFrom.Value);
            filter.Add(SurveyResultFields.SubmittedUTC.SetObjectAlias("SurveyResults") < deDateTo.Value);
            
            if (this.ddlSurveyResultId.Value > 0)
            {
                // Extra filter on submitted datetime

                var surveyResult = distinctSurveyResults.SingleOrDefault(sr => sr.SurveyResultId == this.ddlSurveyResultId.Value);

                if (surveyResult != null)
                {
                    filter.Add(SurveyResultFields.SubmittedUTC.SetObjectAlias("SurveyResults") == surveyResult.SubmittedUTC);
                    this.submitted = surveyResult.SubmittedUTC.Value;
                }
            }

            PrefetchPath prefetch = new PrefetchPath(EntityType.SurveyQuestionEntity);
            prefetch.Add(SurveyQuestionEntity.PrefetchPathSurveyResultCollection);

            var questions = new SurveyQuestionCollection();
            questions.GetMulti(filter, 0, sort, relations, prefetch);

            return questions;
        }

        private void SetGui()
        {
            if (this.surveyQuestions.Count > 0)
            {
                this.gridviews = new List<ASPxGridView>();
                this.datatables = new List<DataTable>();

                foreach (SurveyQuestionEntity surveyQuestion in this.surveyQuestions)
                {
                    switch (surveyQuestion.TypeAsEnum)
                    {
                        case SurveyQuestionType.SingleTextbox:
                            this.plhValues.Controls.Add(this.GetSingleTextboxGridview(surveyQuestion));
                            break;
                        case SurveyQuestionType.MultipleChoice:
                            this.plhValues.Controls.Add(this.GetSingleQuestionGridview(surveyQuestion));
                            break;
                        case SurveyQuestionType.Form:
                            this.plhValues.Controls.Add(this.GetFormGridview(surveyQuestion));
                            break;
                        case SurveyQuestionType.Matrix:
                            this.plhValues.Controls.Add(this.GetMultiQuestionGridview(surveyQuestion));
                            break;
                        case SurveyQuestionType.Ranking:
                            this.plhValues.Controls.Add(this.GetMultiQuestionGridview(surveyQuestion));
                            break;
                        case SurveyQuestionType.Rating:
                            this.plhValues.Controls.Add(this.GetRatingGridview(surveyQuestion));
                            break;
                        case SurveyQuestionType.YesNo:
                            this.plhValues.Controls.Add(this.GetSingleQuestionGridview(surveyQuestion));
                            break;
                        default:
                            
                            break;
                    }

                    this.plhValues.AddHtml("<br />");

                    // Add some comments depending on the question
                    ASPxGridView gvComment = this.GetCommentsGridview(surveyQuestion);
                    if (gvComment != null)
                    {
                        this.plhValues.Controls.Add(gvComment);
                        this.plhValues.AddHtml("<br />");
                    }
                }
            }
        }

        #region Gridviews

        private ASPxGridView GetSingleTextboxGridview(SurveyQuestionEntity question)
        {
            ASPxGridView grid = this.CreateGridview(question.SurveyQuestionId.ToString());

            GridViewDataTextColumn clmAnswer = this.CreateTextColumn("Answer", 100, question.Question, null);

            grid.Columns.Add(clmAnswer);

            grid.DataSource = this.GetSingleTextboxTable(question);
            grid.DataBind();

            return grid;
        }

        private ASPxGridView GetFormGridview(SurveyQuestionEntity question)
        {
            ASPxGridView grid = this.CreateGridview(question.SurveyQuestionId.ToString());
            
            GridViewBandColumn bandColumn = new GridViewBandColumn(question.Question);
            grid.Columns.Add(bandColumn);

            GridViewDataTextColumn clmQuestion = this.CreateTextColumn("Question", 25, "Question", null, false);
            GridViewDataTextColumn clmAnswer = this.CreateTextColumn("Answer", 25, "Answer", null, false);
            
            bandColumn.Columns.Add(clmQuestion);
            bandColumn.Columns.Add(clmAnswer);

            grid.DataSource = this.GetFormTable(question);
            grid.DataBind();

            return grid;
        }

        private ASPxGridView GetRatingGridview(SurveyQuestionEntity question)
        {
            ASPxGridView grid = this.CreateGridview(question.SurveyQuestionId.ToString());
            
            GridViewBandColumn headerColumn = new GridViewBandColumn(question.FieldValue2);
            grid.Columns.Add(headerColumn);

            GridViewDataTextColumn clmQuestion = this.CreateTextColumn("Question", 50, " ", null);
            GridViewDataTextColumn clmAverage = this.CreateTextColumn("Average", 25, "Average", HorizontalAlign.Center);
            GridViewDataTextColumn clmCount = this.CreateTextColumn("Count", 25, "Count", HorizontalAlign.Center);

            headerColumn.Columns.Add(clmQuestion);
            headerColumn.Columns.Add(clmAverage);
            headerColumn.Columns.Add(clmCount);

            grid.DataSource = this.GetRatingTable(question);
            grid.DataBind();

            return grid;
        }

        private ASPxGridView GetSingleQuestionGridview(SurveyQuestionEntity question)
        {
            ASPxGridView grid = this.CreateGridview(question.SurveyQuestionId.ToString());
            
            GridViewBandColumn headerColumn = new GridViewBandColumn(question.Question);
            grid.Columns.Add(headerColumn);

            GridViewDataTextColumn clmAnswer = this.CreateTextColumn("Answer", 50, " ", null);
            GridViewDataProgressBarColumn clmPercentage = this.CreateProgressColumn("Percentage", 25, "Percentage", HorizontalAlign.Center);
            GridViewDataTextColumn clmCount = this.CreateTextColumn("Count", 25, "Count", HorizontalAlign.Center);

            headerColumn.Columns.Add(clmAnswer);
            headerColumn.Columns.Add(clmPercentage);
            headerColumn.Columns.Add(clmCount);

            grid.DataSource = this.GetSingleQuestionTable(question);
            grid.DataBind();

            return grid;
        }

        private ASPxGridView GetMultiQuestionGridview(SurveyQuestionEntity question)
        {
            ASPxGridView grid = this.CreateGridview(question.SurveyQuestionId.ToString());
            
            GridViewBandColumn headerColumn = new GridViewBandColumn(question.FieldValue3);
            grid.Columns.Add(headerColumn);

            GridViewDataTextColumn clmQuestion = this.CreateTextColumn("Question", 50, " ", null);
            headerColumn.Columns.Add(clmQuestion);

            double width = 50 / question.SurveyAnswerCollection.Count;
            foreach (SurveyAnswerEntity answer in question.SurveyAnswerCollection)
            {
                GridViewDataProgressBarColumn clmAnswer = this.CreateProgressColumn(answer.Answer, width, answer.Answer, HorizontalAlign.Center);
                headerColumn.Columns.Add(clmAnswer);
            }

            grid.DataSource = this.GetMultiQuestionTable(question);
            grid.DataBind();

            return grid;
        }

        private ASPxGridView GetCommentsGridview(SurveyQuestionEntity question)
        { 
            ASPxGridView grid = null;

            // Get the comments datatable
            DataTable commentsTable = this.GetCommentsTable(question);

            if (commentsTable != null)
            {
                grid = this.CreateGridview("comments-" + question.SurveyQuestionId);

                GridViewDataTextColumn clmComments = this.CreateTextColumn("Comments", 100, "Latest comments(10)", null);
                grid.Columns.Add(clmComments);

                grid.DataSource = commentsTable;
                grid.DataBind();
            }
            
            return grid;
        }

        #endregion

        #region DataTables

        private DataTable GetSingleTextboxTable(SurveyQuestionEntity question)
        {
            DataTable table = this.CreateTable("Answer");

            // Order results based on submitted datetime and render the results in groups
            EntityView<SurveyResultEntity> surveyResultsView = question.SurveyResultCollection.DefaultView;

            if (this.submitted != null)
                surveyResultsView.Filter = new PredicateExpression(SurveyResultFields.SubmittedUTC == this.submitted);
            else
                surveyResultsView.Filter = this.GetBetweenDatesFilter();
                
            foreach (SurveyResultEntity surveyResult in surveyResultsView)
            {
                if (!String.IsNullOrEmpty(surveyResult.Comment))
                {
                    DataRow row = table.NewRow();
                    row[0] = surveyResult.Comment;

                    table.Rows.Add(row);
                }
            }

            return table;
        }

        private DataTable GetFormTable(SurveyQuestionEntity question)
        {
            DataTable table = this.CreateTable("Question", "Answer");

            // Order results based on submitted datetime and render the results in groups
            EntityView<SurveyResultEntity> parentSurveyResultsView = question.SurveyResultCollection.DefaultView;

            if (this.submitted != null)
                parentSurveyResultsView.Filter = new PredicateExpression(SurveyResultFields.SubmittedUTC == this.submitted);
            else
                parentSurveyResultsView.Filter = this.GetBetweenDatesFilter();

            foreach (SurveyResultEntity parentSurveyResult in parentSurveyResultsView)
            {
                DateTime parentSubmitted = parentSurveyResult.SubmittedUTC.Value;

                // Child questions (form fields)
                foreach (SurveyQuestionEntity child in question.SurveyQuestionCollection)
                {
                    // Get the result of this child question
                    EntityView<SurveyResultEntity> childSurveyResultsView = child.SurveyResultCollection.DefaultView;

                    // Filter based on parent submitted datetime
                    childSurveyResultsView.Filter = new PredicateExpression(SurveyResultFields.SubmittedUTC == parentSubmitted);

                    foreach (SurveyResultEntity childSurveyResult in childSurveyResultsView)
                    {
                        DataRow row = table.NewRow();
                        row[0] = child.Question;
                        row[1] = childSurveyResult.Comment;

                        table.Rows.Add(row);
                    }
                }
            }

            return table;
        }

        private DataTable GetRatingTable(SurveyQuestionEntity question)
        {
            DataTable table = this.CreateTable("Question", "Average", "Count");

            StringBuilder builder = new StringBuilder();

            // Render child questions
            foreach (SurveyQuestionEntity child in question.AllChildSurveyQuestions())
            {
                EntityView<SurveyResultEntity> surveyResultsView = child.SurveyResultCollection.DefaultView;

                if (this.submitted != null)
                    surveyResultsView.Filter = new PredicateExpression(SurveyResultFields.SubmittedUTC == this.submitted);
                else
                    surveyResultsView.Filter = this.GetBetweenDatesFilter();

                if (surveyResultsView.Count > 0)
                {
                    int average = 0;
                    int totalRating = 0;
                    foreach (SurveyResultEntity result in surveyResultsView)
                    {
                        int rating;
                        if (Int32.TryParse(result.SurveyAnswerEntity.Answer, out rating))
                            totalRating += rating;
                    }


                    average = (int)System.Math.Round((double)totalRating / surveyResultsView.Count, 1);

                    DataRow row = table.NewRow();
                    row[0] = child.Question;
                    row[1] = average;
                    row[2] = surveyResultsView.Count;

                    table.Rows.Add(row);
                }
            }

            return table;
        }

        private DataTable GetSingleQuestionTable(SurveyQuestionEntity question)
        {
            DataTable table = this.CreateTable("Answer", "Percentage", "Count");

            // Order results based on submitted datetime and render the results in groups
            EntityView<SurveyResultEntity> surveyResultsView = question.SurveyResultCollection.DefaultView;

            if (this.submitted != null)
                surveyResultsView.Filter = new PredicateExpression(SurveyResultFields.SubmittedUTC == this.submitted);
            else
                surveyResultsView.Filter = this.GetBetweenDatesFilter();

            SurveyAnswerCollection answers = question.SurveyAnswerCollection;
            int totalResults = surveyResultsView.Count;

            foreach (SurveyAnswerEntity surveyAnswer in question.SurveyAnswerCollection)
            {
                PredicateExpression answerFilter = new PredicateExpression();
                answerFilter.Add(SurveyResultFields.SurveyAnswerId == surveyAnswer.SurveyAnswerId);

                if (this.submitted != null)
                    answerFilter.Add(SurveyResultFields.SubmittedUTC == this.submitted);
                else
                    answerFilter.Add(this.GetBetweenDatesFilter());
                
                surveyResultsView.Filter = answerFilter;

                int percentage = 0;
                if (totalResults > 0)
                    percentage = (int)System.Math.Round(((double)surveyResultsView.Count / (double)totalResults) * 100.0);

                DataRow row = table.NewRow();
                row[0] = surveyAnswer.Answer;
                row[1] = percentage;
                row[2] = surveyResultsView.Count;

                table.Rows.Add(row);
            }

            return table;
        }

        private DataTable GetMultiQuestionTable(SurveyQuestionEntity question)
        {
            // Create columns
            List<string> columns = new List<string>();
            columns.Add("Question");

            foreach (SurveyAnswerEntity answer in question.SurveyAnswerCollection)
                columns.Add(answer.Answer);

            // Create table
            DataTable table = this.CreateTable(columns.ToArray());

            // Render child questions
            foreach (SurveyQuestionEntity child in question.AllChildSurveyQuestions())
            {
                DataRow row = table.NewRow();
                row[0] = child.Question;

                EntityView<SurveyResultEntity> surveyResultsView = child.SurveyResultCollection.DefaultView;

                if (this.submitted != null)
                    surveyResultsView.Filter = new PredicateExpression(SurveyResultFields.SubmittedUTC == this.submitted);
                else
                    surveyResultsView.Filter = this.GetBetweenDatesFilter();

                SurveyAnswerCollection answers = child.SurveyAnswerCollection;
                int totalResults = surveyResultsView.Count;

                foreach (SurveyAnswerEntity surveyAnswer in child.SurveyAnswerCollection)
                {
                    if (surveyAnswer.Answer.IsNullOrWhiteSpace())
                        continue;

                    PredicateExpression answerFilter = new PredicateExpression();
                    answerFilter.Add(SurveyResultFields.SurveyAnswerId == surveyAnswer.SurveyAnswerId);

                    if (this.submitted != null)
                        answerFilter.Add(SurveyResultFields.SubmittedUTC == this.submitted);
                    else
                        answerFilter.Add(this.GetBetweenDatesFilter());

                    surveyResultsView.Filter = answerFilter;

                    int percentage = 0;
                    if (totalResults > 0)
                        percentage = (int)System.Math.Round(((double)surveyResultsView.Count / (double)totalResults) * 100.0);

                    row[surveyAnswer.Answer] = percentage;
                }

                table.Rows.Add(row);
            }

            return table;
        }

        private DataTable GetCommentsTable(SurveyQuestionEntity question)
        { 
            DataTable table = null;

            if (question.TypeAsEnum == SurveyQuestionType.MultipleChoice ||
                question.TypeAsEnum == SurveyQuestionType.Form ||
                question.TypeAsEnum == SurveyQuestionType.Matrix ||
                question.TypeAsEnum == SurveyQuestionType.Ranking ||
                question.TypeAsEnum == SurveyQuestionType.Rating)
            {
                EntityView<SurveyResultEntity> surveyResultsView = question.SurveyResultCollection.DefaultView;

                // Reset filter
                surveyResultsView.Filter = null;

                if (this.submitted != null)
                    surveyResultsView.Filter = new PredicateExpression(SurveyResultFields.SubmittedUTC == this.submitted);
                else
                    surveyResultsView.Filter = this.GetBetweenDatesFilter();

                table = this.GetCommentsList(surveyResultsView);
            }

            // Check if this child is the parent
            //if (child.ParentQuestionId == null || child.ParentQuestionId <= 0)
            //    comments = this.GetCommentsList(surveyResultsView);

            return table;
        }

        private DataTable GetCommentsList(EntityView<SurveyResultEntity> surveyResultsView)
        {
            DataTable table = new DataTable();

            if (surveyResultsView != null && surveyResultsView.Count > 0)
            {
                table = this.CreateTable("Comments");

                int numComments = System.Math.Min(10, surveyResultsView.Count);

                for (int i = 0; i < numComments; i++)
                {
                    if (!String.IsNullOrEmpty(surveyResultsView[i].Comment))
                    {
                        DataRow row = table.NewRow();
                        row[0] = surveyResultsView[i].Comment;
                        table.Rows.Add(row);
                    }
                }
            }

            return table;
        }

        #endregion

        private ASPxGridView CreateGridview(string id)
        { 
            ASPxGridView gridview = new ASPxGridView();
            gridview.Width = Unit.Percentage(100);
            gridview.AutoGenerateColumns = false;
            gridview.ID = id;

            this.gridviews.Add(gridview);

            return gridview;
        }

        private DataTable CreateTable(params string[] columnNames)
        {
            DataTable table = new DataTable();
            foreach (string columnName in columnNames)
            {
                table.Columns.Add(new DataColumn(columnName));
            }
            this.datatables.Add(table);
            return table;
        }

        private GridViewDataProgressBarColumn CreateProgressColumn(string fieldname, double width, string caption, HorizontalAlign? alignment)
        {
            GridViewDataProgressBarColumn column = new GridViewDataProgressBarColumn();
            column.FieldName = fieldname;
            column.Width = Unit.Percentage(width);
            column.Caption = caption;

            if (alignment.HasValue)
            {
                column.HeaderStyle.HorizontalAlign = alignment.Value;
                column.CellStyle.HorizontalAlign = alignment.Value;
            }

            return column;
        }

        private GridViewDataTextColumn CreateTextColumn(string fieldname, double width, string caption, HorizontalAlign? alignment)
        {
            GridViewDataTextColumn column = new GridViewDataTextColumn();
            column.FieldName = fieldname;
            column.Width = Unit.Percentage(width);
            column.Caption = caption;

            if (alignment.HasValue)
            {
                column.HeaderStyle.HorizontalAlign = alignment.Value;
                column.CellStyle.HorizontalAlign = alignment.Value;
            }

            return column;
        }

        private GridViewDataTextColumn CreateTextColumn(string fieldname, double width, string caption, HorizontalAlign? alignment, bool visible)
        {
            GridViewDataTextColumn column = this.CreateTextColumn(fieldname, width, caption, alignment);

            if (!visible)
                column.HeaderStyle.CssClass = "hidden";

            return column;
        }

        private PredicateExpression GetBetweenDatesFilter()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(SurveyResultFields.SubmittedUTC > this.deDateFrom.Value);
            filter.Add(SurveyResultFields.SubmittedUTC < this.deDateTo.Value);

            return filter;
        }

        #endregion

        #region Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            if (QueryStringHelper.HasValue("t"))
            {
                long submitted = QueryStringHelper.GetValue<long>("t").Value;

                foreach (long ts in this.submittedResults.Values)
                {
                    if (ts.Equals(submitted))
                    {
                        // Get the key (SurveyResultId)
                        int surveyResultId = submittedResults.FirstOrDefault(x => x.Value == ts).Key;
                        this.ddlSurveyResultId.SelectByItemValue(surveyResultId);

                        break;
                    }
                }
            }

            if (!this.IsPostBack || this.IsCallback)
            {
                this.SetFilter();
                this.SetGui();
            }

            this.btnRefresh.Click += btnRefresh_Click;
            this.btnExportToExcel.Click += new EventHandler(btnExportToExcel_Click);
        }

        void btnRefresh_Click(object sender, EventArgs e)
        {
            this.RedirectWithControlValues();
        }

        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            // Valid customer
            this.Validate();

            if (this.IsValid)
            {
                this.ExportResults();
            }
        }

        #endregion
    }
}