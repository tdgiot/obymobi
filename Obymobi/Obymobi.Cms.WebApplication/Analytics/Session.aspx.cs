﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data;
using Dionysos.Diagnostics;
using Obymobi.Web.Google;
using System.Text;
using Dionysos.Web;
using System.Data;

namespace Obymobi.ObymobiCms.Analytics
{
    public partial class Session : Dionysos.Web.UI.Page
    {
        private struct Action
        {
            public string action;
            public string value;

            public Action(string action, string value)
            {
                this.action = action;
                this.value = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string profileId, sessionId;
            if (QueryStringHelper.TryGetValue("profile", out profileId) && QueryStringHelper.TryGetValue("session", out sessionId))
            {
                ProcessSession(profileId, sessionId);
            }
        }

        private void ProcessSession(string profileId, string sessionId)
        {
            StringBuilder builder = new StringBuilder();
           
            GoogleAPI google = new GoogleAPI();
            if (google.SetProfile(profileId))
            {
                DateTime dateFrom = new DateTime(2012, 1, 1);
                DateTime dateTo = DateTime.Now;

                List<KeyValuePair<long, Action>> actions = new List<KeyValuePair<long, Action>>();

                var events = google.Get("ga:totalEvents", "ga:customVarValue5,ga:eventAction,ga:eventLabel,ga:customVarValue4", string.Format("ga:customVarValue5=={0};ga:eventAction!=Select Category", sessionId), "ga:customVarValue4", dateFrom, dateTo);
                for (int i = 0; i < events.TotalResults; i++)
                {
                    long ticks = long.Parse(events.Rows[i][3]);
                    Action action = new Action(events.Rows[i][1], events.Rows[i][2]);

                    actions.Add(new KeyValuePair<long, Action>(ticks, action));
                }

                var pageviews = google.Get("ga:pageviews", "ga:customVarValue5,ga:pagePath,ga:customVarValue4", string.Format("ga:customVarValue5=={0}", sessionId), "ga:customVarValue4", dateFrom, dateTo);
                for (int i = 0; i < pageviews.TotalResults; i++)
                {
                    long ticks = long.Parse(pageviews.Rows[i][2]);
                    Action action = new Action("Change view", pageviews.Rows[i][1]);

                    actions.Add(new KeyValuePair<long, Action>(ticks, action));
                }

                actions.Sort(delegate(KeyValuePair<long, Action> lhs, KeyValuePair<long, Action> rhs)
                {
                    return Comparer<long>.Default.Compare(lhs.Key, rhs.Key);
                });
                
                DataTable orderList = new DataTable();
                orderList.Columns.Add(new DataColumn("Time"));
                orderList.Columns.Add(new DataColumn("Action"));
                orderList.Columns.Add(new DataColumn("Value"));

                foreach (KeyValuePair<long, Action> action in actions)
                {
                    DateTime UTCBaseTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    DateTime time = UTCBaseTime.Add(new TimeSpan(action.Key * TimeSpan.TicksPerMillisecond)).ToLocalTime();

                    DataRow row = orderList.NewRow();
                    row["Time"] = time;
                    row["Action"] = action.Value.action;
                    row["Value"] = action.Value.value;

                    orderList.Rows.Add(row);
                }

                this.actionGrid.DataSource = orderList;
                this.actionGrid.DataBind();
            }
            else
            {
                builder.AppendFormat("Google Analytics profile ID '{0}' could not be opened.", profileId);
            } this.plhOverview.AddHtml(builder.ToString());
        }
    }
}