﻿using System;
using System.Linq;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Analytics;
using Obymobi.ObymobiCms.Analytics.Subpanels;
using Newtonsoft.Json;
using Obymobi.Web.Analytics.Requests;
using Obymobi.Data.EntityClasses;
using Dionysos.Web;
using Dionysos.Web.UI.WebControls;
using Obymobi.Web.Analytics.Reports;

namespace Obymobi.ObymobiCms.Analytics
{
    public partial class BatchReports : BaseReportingPage
    {
        private ReportFilterPanel filterPanel;

        protected override void OnInit(EventArgs e)
        {
            this.InitGui();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.filterPanel != null)
            {
                this.filterPanel.UpdateCompanyId();
            }
        }

        protected override void SetDefaultValuesToControls()
        {
            this.filterPanel = this.LoadControl<ReportFilterPanel>("~/Analytics/Subpanels/ReportFilterPanel.ascx");
            this.filterPanel.ShowFilterCategories = false;
            this.filterPanel.ShowFilterDeliverypoints = false;
            this.filterPanel.ShowFilterDevices = false;
            this.filterPanel.ShowFilterOrders = false;
            this.filterPanel.ShowFilterProduct = false;
            this.filterPanel.ShowLoadDeliverypointsButton = false;
            this.filterPanel.ShowDeliverypointgroups = false;
            this.filterPanel.Period = ReportFilterPanel.ReportingPeriod.LastMonth;
            this.phlFilter.Controls.Add(this.filterPanel);
        }

        protected override void BindQueryStringToControls()
        {
            base.BindQueryStringToControls();
            this.filterPanel.UpdateCompanyId();

            int taskId;
            if (QueryStringHelper.TryGetValue("copyFrom", out taskId))
            {
                ReportProcessingTaskEntity task = new ReportProcessingTaskEntity(taskId);
                BatchReportRequest request = JsonConvert.DeserializeObject<BatchReportRequest>(task.Filter);
                this.tbReportCollectionName.Text = request.Name;

                for (int i = 0; i < request.TransactionReports.Count; i++)
                {
                    var report = request.TransactionReports[i];
                    TextBox tbName = this.ControlList.FindControl("tbTransactionsReportName" + (i + 1)) as TextBox;
                    TextBoxMultiLine tbJson = this.ControlList.FindControl("tbTransactionsReportJson" + (i + 1)) as TextBoxMultiLine;

                    tbName.Text = report.Name;
                    tbJson.Text = JsonConvert.SerializeObject(report);
                }

                for (int i = 0; i < request.ProductOverviewReports.Count; i++)
                {
                    var report = request.ProductOverviewReports[i];
                    TextBox tbName = this.ControlList.FindControl("tbProductOverviewsReportName" + (i + 1)) as TextBox;
                    TextBoxMultiLine tbJson = this.ControlList.FindControl("tbProductOverviewsReportJson" + (i + 1)) as TextBoxMultiLine;

                    tbName.Text = report.Name;
                    tbJson.Text = JsonConvert.SerializeObject(report);
                }

                for (int i = 0; i < request.WakeUpCallReports.Count; i++)
                {
                    var report = request.WakeUpCallReports[i];
                    TextBox tbName = this.ControlList.FindControl("tbWakeupReportName" + (i + 1)) as TextBox;
                    TextBoxMultiLine tbJson = this.ControlList.FindControl("tbWakeupReportJson" + (i + 1)) as TextBoxMultiLine;

                    tbName.Text = report.Name;
                    tbJson.Text = JsonConvert.SerializeObject(report);
                }

                for (int i = 0; i < request.PageViewReports.Count; i++)
                {
                    var report = request.PageViewReports[i];
                    TextBox tbName = this.ControlList.FindControl("tbPageViewsReportName" + (i + 1)) as TextBox;
                    TextBoxMultiLine tbJson = this.ControlList.FindControl("tbPageViewsReportJson" + (i + 1)) as TextBoxMultiLine;

                    tbName.Text = report.Name;
                    tbJson.Text = JsonConvert.SerializeObject(report);
                }
            }
        }

        void InitGui()
        {            
            this.btGenerateDoIt.Visible = (TestUtil.IsPcDeveloper && WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.Manual);           
        }

        public void ValidateSelf()
        {
            this.filterPanel.Validate();

            base.Validate();
        }

        public void ShowMetadata()
        {
            this.GenerateReport(true, true);
        }

        public void GenerateReportDoIt()
        {
            this.GenerateReport(true);
        }

        public void GenerateReport()
        {
            this.GenerateReport(false);
        }

        public void GenerateReport(bool doIt, bool metaDataOnly = false)
        {
            Validate();
            if (!IsValid)
            {
                return;
            }

            if (metaDataOnly)
            {
                tbMetadata.Text = JsonConvert.SerializeObject(filterPanel.GetFilter());

                // Ensure it's the valid serialize/deserialize
                var validationObject = JsonConvert.DeserializeObject<Filter>(this.tbMetadata.Text);
                if (!JsonConvert.SerializeObject(validationObject).Equals(this.tbMetadata.Text))
                {
                    throw new JsonSerializationException("The current report selection can't be serialized.");
                }

                tabsMain.TabPages.FindByName("Metadata").Visible = true;
                return;
            }

            BatchReportRequest request = new BatchReportRequest();
            request.Name = this.tbReportCollectionName.Text;            

            // GK Jep, lame code. But this feature is End-of-life. So didn't want to spend to much brain-cycles and time on this.
            if (!this.tbTransactionsReportJson1.Value.IsNullOrWhiteSpace())
                this.AddTransactionsReport(this.tbTransactionsReportJson1.Text, this.tbTransactionsReportName1.Text, request);

            if (!this.tbTransactionsReportJson2.Value.IsNullOrWhiteSpace())
                this.AddTransactionsReport(this.tbTransactionsReportJson2.Text, this.tbTransactionsReportName2.Text, request);

            if (!this.tbTransactionsReportJson3.Value.IsNullOrWhiteSpace())
                this.AddTransactionsReport(this.tbTransactionsReportJson3.Text, this.tbTransactionsReportName3.Text, request);

            if (!this.tbTransactionsReportJson4.Value.IsNullOrWhiteSpace())
                this.AddTransactionsReport(this.tbTransactionsReportJson4.Text, this.tbTransactionsReportName4.Text, request);

            if (!this.tbTransactionsReportJson5.Value.IsNullOrWhiteSpace())
                this.AddTransactionsReport(this.tbTransactionsReportJson5.Text, this.tbTransactionsReportName5.Text, request);

            if (!this.tbTransactionsReportJson6.Value.IsNullOrWhiteSpace())
                this.AddTransactionsReport(this.tbTransactionsReportJson6.Text, this.tbTransactionsReportName6.Text, request);

            if (!this.tbTransactionsReportJson7.Value.IsNullOrWhiteSpace())
                this.AddTransactionsReport(this.tbTransactionsReportJson7.Text, this.tbTransactionsReportName7.Text, request);

            if (!this.tbTransactionsReportJson8.Value.IsNullOrWhiteSpace())
                this.AddTransactionsReport(this.tbTransactionsReportJson8.Text, this.tbTransactionsReportName8.Text, request);

            if (!this.tbTransactionsReportJson9.Value.IsNullOrWhiteSpace())
                this.AddTransactionsReport(this.tbTransactionsReportJson9.Text, this.tbTransactionsReportName9.Text, request);

            // Wakeups
            if (!this.tbWakeupReportJson1.Value.IsNullOrWhiteSpace())
                this.AddWakeupCallsReport(this.tbWakeupReportJson1.Text, this.tbWakeupReportName1.Text, request);

            if (!this.tbWakeupReportJson2.Value.IsNullOrWhiteSpace())
                this.AddWakeupCallsReport(this.tbWakeupReportJson2.Text, this.tbWakeupReportName2.Text, request);

            if (!this.tbWakeupReportJson3.Value.IsNullOrWhiteSpace())
                this.AddWakeupCallsReport(this.tbWakeupReportJson3.Text, this.tbWakeupReportName3.Text, request);

            if (!this.tbWakeupReportJson4.Value.IsNullOrWhiteSpace())
                this.AddWakeupCallsReport(this.tbWakeupReportJson4.Text, this.tbWakeupReportName4.Text, request);

            // Product Overviews
            if (!this.tbProductOverviewsReportJson1.Value.IsNullOrWhiteSpace())
                this.AddProductOverviewReport(this.tbProductOverviewsReportJson1.Text, this.tbProductOverviewsReportName1.Text, request);

            if (!this.tbProductOverviewsReportJson2.Value.IsNullOrWhiteSpace())
                this.AddProductOverviewReport(this.tbProductOverviewsReportJson2.Text, this.tbProductOverviewsReportName2.Text, request);

            if (!this.tbProductOverviewsReportJson3.Value.IsNullOrWhiteSpace())
                this.AddProductOverviewReport(this.tbProductOverviewsReportJson3.Text, this.tbProductOverviewsReportName3.Text, request);

            if (!this.tbProductOverviewsReportJson4.Value.IsNullOrWhiteSpace())
                this.AddProductOverviewReport(this.tbProductOverviewsReportJson4.Text, this.tbProductOverviewsReportName4.Text, request);

            // PageView Reports
            if (!this.tbPageViewsReportJson1.Value.IsNullOrWhiteSpace())
                this.AddPageViewsReport(this.tbPageViewsReportJson1.Text, this.tbPageViewsReportName1.Text, request);

            if (!this.tbPageViewsReportJson2.Value.IsNullOrWhiteSpace())
                this.AddPageViewsReport(this.tbPageViewsReportJson2.Text, this.tbPageViewsReportName2.Text, request);

            if (!this.tbPageViewsReportJson3.Value.IsNullOrWhiteSpace())
                this.AddPageViewsReport(this.tbPageViewsReportJson3.Text, this.tbPageViewsReportName3.Text, request);

            if (!this.tbPageViewsReportJson4.Value.IsNullOrWhiteSpace())
                this.AddPageViewsReport(this.tbPageViewsReportJson4.Text, this.tbPageViewsReportName4.Text, request);

            if (request.PageViewReports.Any(x => x.CompanyId != CmsSessionHelper.CurrentCompanyId) ||
                request.ProductOverviewReports.Any(x => x.CompanyId != CmsSessionHelper.CurrentCompanyId) ||
                request.TransactionReports.Any(x => x.CompanyId != CmsSessionHelper.CurrentCompanyId) ||
                request.WakeUpCallReports.Any(x => x.CompanyId != CmsSessionHelper.CurrentCompanyId))
            {
                this.MultiValidatorDefault.AddError("One or more reports are for another company than the current active company.");
                this.Validate();
                return;
            }

            if (doIt)
            {
                DateTime fromUtc, tillUtc;
                this.filterPanel.GetPeriod(out fromUtc, out tillUtc);
                BatchReport report = new BatchReport(fromUtc, tillUtc, request);

                while (report.RunAReport())
                {
                    // Do it.
                }

                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=BatchReport-{0}-{1}-{2}.zip".FormatSafe(new CompanyEntity(CmsSessionHelper.CurrentCompanyId).Name, request.Name, DateTime.Now.DateTimeToSimpleDateTimeStamp()));
                report.WriteZipFile(Response.OutputStream);
                Response.End();                
            }
            else
            {
                ReportProcessingTaskEntity task = new ReportProcessingTaskEntity();
                DateTime fromUtc, tillUtc;
                this.filterPanel.GetPeriod(out fromUtc, out tillUtc);
                task.FromUTC = fromUtc;
                task.TillUTC = tillUtc;
                task.CompanyId = this.filterPanel.CompanyId;
                task.AnalyticsReportType = AnalyticsReportType.Batch;
                task.Filter = JsonConvert.SerializeObject(request);
                task.TimeZoneOlsonId = this.filterPanel.TimeZoneOlsonId;
                task.Save();
                this.Response.Redirect("~/Analytics/ReportProcessingTask.aspx?id=" + task.ReportProcessingTaskId);
            }                        
        }

        private void AddPageViewsReport(string json, string name, BatchReportRequest batchRequest)
        {
            Filter filter = JsonConvert.DeserializeObject<Filter>(json);
            filter.Name = name;
            batchRequest.PageViewReports.Add(filter);
        }

        private void AddWakeupCallsReport(string json, string name, BatchReportRequest batchRequest)
        {
            Filter filter = JsonConvert.DeserializeObject<Filter>(json);
            filter.Name = name;
            batchRequest.WakeUpCallReports.Add(filter);
        }

        private void AddProductOverviewReport(string json, string name, BatchReportRequest batchRequest)
        {
            Filter filter = JsonConvert.DeserializeObject<Filter>(json);
            filter.Name = name;
            batchRequest.ProductOverviewReports.Add(filter);
        }

        private void AddTransactionsReport(string json, string name, BatchReportRequest batchRequest)
        {
            TransactionsRequest transactionsRequest = JsonConvert.DeserializeObject<TransactionsRequest>(json);
            transactionsRequest.Name = name;
            batchRequest.TransactionReports.Add(transactionsRequest);
        }
    }

}