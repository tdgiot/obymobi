﻿<%@ Page Title="Entertainment" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.Entertainment" Codebehind="Entertainment.aspx.cs" %>
<%@ Register Assembly="DevExpress.XtraCharts.v20.1.Web" Namespace="DevExpress.XtraCharts.Web" TagPrefix="dxchartsui" %>
<%@ Register Assembly="DevExpress.XtraCharts.v20.1" Namespace="DevExpress.XtraCharts" TagPrefix="dxcharts" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
	<X:PageControl Id="tabsMain" runat="server" Width="100%">
		<TabPages>
			<X:TabPage Text="Overzicht" Name="Overview">
				<controls>
                    <p>This page has been replaced by <a href="TopLists.aspx">Top lists</a> (remove in next version).</p>
                </controls>
			</X:TabPage>
        </TabPages>
    </X:PageControl>
</asp:Content>

