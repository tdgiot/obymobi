﻿using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using DevExpress.Data;
using DevExpress.Web;
using DevExpress.XtraGrid;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.ObymobiCms.Analytics
{
	public partial class ReportProcessingTasks : Dionysos.Web.UI.PageLLBLOverviewDataSourceCollection
	{
		private const string COLUMN_NAME_CREATED = nameof(ReportProcessingTaskEntity.CreatedUTC);
		private const string COLUMN_NAME_FROM = nameof(ReportProcessingTaskEntity.FromUTC);
		private const string COLUMN_NAME_TILL = nameof(ReportProcessingTaskEntity.TillUTC);
		private const string COLUMN_REPORT_TYPE = nameof(ReportProcessingTaskEntity.AnalyticsReportType);
		
		private static readonly Dictionary<string, string> ColumnDisplayTextFieldMapping = new Dictionary<string, string>
		{
			{ COLUMN_NAME_CREATED, nameof(ReportProcessingTaskEntity.CreatedLocalTime) },
			{ COLUMN_NAME_FROM, nameof(ReportProcessingTaskEntity.FromLocalTime) },
			{ COLUMN_NAME_TILL, nameof(ReportProcessingTaskEntity.TillLocalTime) }
		};

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			PreRender += ReportProcessingTasks_PreRender;
		}

		private void ReportProcessingTasks_PreRender(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				SortByCreatedColumn();
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			this.Filter = new PredicateExpression
			{
				ReportProcessingTaskFields.CompanyId == CmsSessionHelper.CurrentCompanyId
			};

			if (CmsSessionHelper.CurrentRole == Role.Reseller)
			{
				this.Filter.Add(ReportProcessingTaskFields.AnalyticsReportType == AnalyticsReportType.Transactions);
				this.MainGridView.ShowDeleteHyperlinkColumn = false;
			}

			this.IncludeFieldsList = new IncludeFieldsList
			{
				ReportProcessingTaskFields.CreatedUTC,
				ReportProcessingTaskFields.FromUTC,
				ReportProcessingTaskFields.TillUTC,
				ReportProcessingTaskFields.TimeZoneOlsonId,
				ReportProcessingTaskFields.Processed,
				ReportProcessingTaskFields.Failed
			};

			((MasterPages.MasterPageEntityCollection)this.Master).ToolBar.AddButton.Visible = false;

			base.OnLoad(e);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			MainGridView.HtmlDataCellPrepared += OnMainGridViewOnHtmlDataCellPrepared;
			MainGridView.DataBound += OnMainGridViewOnDataBound;
		}

		private static void OnMainGridViewOnHtmlDataCellPrepared(object s, ASPxGridViewTableDataCellEventArgs args) => SetCellDisplayText(args);

		private void OnMainGridViewOnDataBound(object s, EventArgs args)
		{
			SetColumnSortMode(COLUMN_NAME_CREATED, ColumnSortMode.Value);
			SetColumnSortMode(COLUMN_NAME_FROM, ColumnSortMode.Value);
			SetColumnSortMode(COLUMN_NAME_TILL, ColumnSortMode.Value);
		}

		private static void SetCellDisplayText(ASPxGridViewTableDataCellEventArgs e)
		{
			if (ColumnDisplayTextFieldMapping.TryGetValue(e.DataColumn.FieldName, out string displayTextField))
			{
				string displayText = e.GetValue(displayTextField)?.ToString();
				if (displayText != null)
				{
					e.Cell.Text = displayText;
				}
			}
			else if (e.DataColumn.FieldName == COLUMN_REPORT_TYPE)
			{
				if (EnumUtil.TryParse(e.GetValue(COLUMN_REPORT_TYPE).ToString(), out AnalyticsReportType reportType))
				{
					e.Cell.Text = reportType.GetStringValue();
				}
			}
		}

		private void SortByCreatedColumn()
		{
			if (GetColumnByName(COLUMN_NAME_CREATED) is GridViewDataColumn column)
			{
				MainGridView.SortBy(column, ColumnSortOrder.Descending);
			}
		}

		public override void Dispose()
		{
			PreRender -= ReportProcessingTasks_PreRender;
			MainGridView.DataBound -= OnMainGridViewOnDataBound;
			MainGridView.HtmlDataCellPrepared -= OnMainGridViewOnHtmlDataCellPrepared;
			base.Dispose();
		}
	}
}
