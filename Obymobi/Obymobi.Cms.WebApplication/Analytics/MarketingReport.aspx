﻿<%@ Page Title="Transactions" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.MarketingReport" Codebehind="MarketingReport.aspx.cs" %>
<%@ Reference VirtualPath="~/Analytics/SubPanels/ReportFilterPanel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">Marketing Report</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <span class="toolbar">
        <X:ToolBarButton runat="server" ID="btGenerate" CommandName="GenerateReport" Text="Rapport genereren" Image-Url="~/Images/Icons/report.png" />
        <X:ToolBarButton runat="server" ID="btGenerateDoIt" CommandName="GenerateReportDoIt" Text="Rapport genereren - Do it!" Image-Url="~/Images/Icons/report.png" />
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<D:PlaceHolder ID="PlaceHolder1" runat="server">    
    <div>
	    <X:PageControl Id="tabsMain" runat="server" Width="100%">
		    <TabPages>
			    <X:TabPage Text="Algemeen" Name="Generic">
				    <controls>
					    <D:PlaceHolder ID="phlFilter" runat="server"></D:PlaceHolder>
                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</D:PlaceHolder>
</asp:Content>