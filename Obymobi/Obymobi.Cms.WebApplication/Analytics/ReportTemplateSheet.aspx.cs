﻿using Dionysos.Web;
using Dionysos.Web.UI;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.ObymobiCms.Analytics.ReportTemplateSheets;
using Obymobi.ObymobiCms.MasterPages;
using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Requests.RequestBase;
using System;
using System.Web.UI;
using Obymobi.Web.Analytics.Factories;

namespace Obymobi.ObymobiCms.Analytics
{
	public partial class ReportTemplateSheet : PageLLBLGenEntityGeneric<ReportProcessingTaskTemplateEntity>
	{
		private int sheetFilterId = -1;

		private string ReportUrl { get; set; }
		private IReportRequest ReportRequest { get; set; }
		private SpreadSheetFilter SheetFilter { get; set; }
		private UserControl SheetFilterControl { get; set; }
		private readonly ReportRequestBridgeFactory reportRequestBridgeFactory = new ReportRequestBridgeFactory();

		protected void Page_Load(object s, EventArgs e)
		{
			if (Master == null)
			{
				return;
			}

			ToolBarPageEntity toolbar = ((MasterPageEntity) Master).ToolBar;
			toolbar.SaveAndCloneButton.Visible = false;
			toolbar.SaveAndNewButton.Visible = false;
		}

		protected override void OnInit(EventArgs e)
		{
			if (!QueryStringHelper.GetInt("filter", out int filterId))
			{
				throw new InvalidOperationException("Filter id has not been passed");
			}

			sheetFilterId = filterId;

			DataSourceLoaded += ReportTemplateSheet_DataSourceLoaded;

			base.OnInit(e);
		}

		private void ReportTemplateSheet_DataSourceLoaded(object sender)
		{
			ReportRequest = reportRequestBridgeFactory
				.GetReportRequestRefinedAbstraction(DataSource.ReportType)
				.CreateReportRequest(DataSource.Filter);

			SheetFilter = ReportRequest.GetSheet(sheetFilterId);

			tbName.Text = SheetFilter.Name;
			cbActive.Value = SheetFilter.Active;

			SetGui();
		}

		private void SetGui()
		{
			ConfigureReportHyperLink();

			RenderReportSheetFilter(DataSource.ReportType);

			((IReportSheetFilterControl) SheetFilterControl).Load(SheetFilter, DataSource.CompanyId);
		}

		private void RenderReportSheetFilter(AnalyticsReportType reportType)
		{
			plhReportSheetFilter.Controls.Clear();

			SheetFilterControl = reportRequestBridgeFactory.GetSheetFilterControlRefinedAbstraction(reportType).CreateControl(this);

			if (SheetFilterControl != null)
			{
				plhReportSheetFilter.Controls.Add(SheetFilterControl);
			}
		}

		private void ConfigureReportHyperLink()
		{
			ReportUrl = $"~/Analytics/ReportTemplate.aspx?id={DataSource.ReportProcessingTaskTemplateId}";

			hlParentReportName.Text = DataSource.FriendlyName;
			hlParentReportName.NavigateUrl = ReportUrl;
		}

		public override bool Save()
		{
			if (IsValid)
			{
				SheetFilter = ((IReportSheetFilterControl) SheetFilterControl).GetSheetFilter();
				SheetFilter.Name = tbName.Text;
				SheetFilter.Active = cbActive.Value;

				ReportRequest.UpdateSheet(SheetFilter);

				DataSource.Filter = ReportRequest.Serialize();

				return DataSource.Save();
			}

			return false;
		}

		public override bool Delete()
		{
			ReportRequest.RemoveSheet(sheetFilterId);

			DataSource.Filter = ReportRequest.Serialize();

			if (DataSource.Save())
			{
				Response.Redirect(ResolveUrl(ReportUrl));

				return true;
			}

			return false;
		}

		public override void Dispose()
		{
			DataSourceLoaded -= ReportTemplateSheet_DataSourceLoaded;
			
			base.Dispose();
		}
	}
}
