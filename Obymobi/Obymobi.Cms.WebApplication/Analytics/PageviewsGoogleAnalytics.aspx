﻿<%@ Page Title="Page Views" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Analytics.PageviewsGoogleAnalytics" Codebehind="PageviewsGoogleAnalytics.aspx.cs" %>
<%@ Reference VirtualPath="~/Analytics/SubPanels/ReportFilterPanel.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" Runat="Server">Pageviews (Google Analytics)</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" Runat="Server">
    <span class="toolbar">
        <X:ToolBarButton runat="server" ID="btGenerate" CommandName="GenerateReport" Text="Rapport genereren" Image-Url="~/Images/Icons/report.png" />
        <X:ToolBarButton runat="server" ID="btGenerateDoIt" CommandName="GenerateReportDoIt" Text="Rapport genereren - Do it!" Image-Url="~/Images/Icons/report.png" />
        <X:ToolBarButton runat="server" ID="btGenerateMetadata" CommandName="ShowMetadata" Text="Generate Metadata" Image-Url="~/Images/Icons/cog.png" />
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" Runat="Server">
<D:PlaceHolder ID="PlaceHolder1" runat="server">    
    <div>
	    <X:PageControl Id="tabsMain" runat="server" Width="100%">
		    <TabPages>
			    <X:TabPage Text="Algemeen" Name="Generic">
				    <controls>
					    <D:PlaceHolder ID="phlFilter" runat="server"></D:PlaceHolder>
                    </controls>
                </X:TabPage>
                <X:TabPage Text="Metadata" Name="Metadata">
                    <controls>
                        <table class="dataformV2">
                            <tr>
	                            <td class="label">
		                            <D:Label runat="server" LocalizeText="false">Metadata</D:Label>
	                            </td>
	                            <td class="control" colspan="3">
		                            <D:TextBoxMultiLine runat="server" ID="tbMetadata" Rows="20"></D:TextBoxMultiLine>
	                            </td>	  
                            </tr>
                            <tr>
                                <td class="label">&nbsp;</td>
	                            <td class="control">&nbsp;</td>
                                <td class="label">&nbsp;</td>
	                            <td class="control">&nbsp;</td>
                            </tr>
                        </table>                        
                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>
</D:PlaceHolder>
</asp:Content>