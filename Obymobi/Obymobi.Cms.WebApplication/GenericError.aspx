﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.GenericError" CodeBehind="GenericError.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" id="HtmlHeader">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Obymobi Content Management System</title>
    <meta http-equiv="imagetoolbar" content="no" />
    <link href="~/favicon.ico" type="image/x-icon" link rel="shortcut icon" />

    <link href="~/css/default.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/default-hide-left-column.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/base.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/eWorldUiControls.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/mediacollection.css" type="text/css" link rel="Stylesheet" />
    <link href="~/css/mediabox.css" type="text/css" link rel="Stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="head">
            <div class="mainmenu">
                <ul class="nav">
                </ul>
            </div>

            <div class="titles">
                <div class="module">
                </div>
                <div class="seperator">&nbsp;</div>
                <div class="pagetitle">
                    <div style="padding-left: 6px;">
                        Oops, something went wrong. It shouldn't, but it just did. A notification has been sent to the development team.
                    </div>
                </div>
                <div class="company">
                </div>
                <div class="user">
                    <strong></strong>
                </div>
            </div>
        </div>
        <div id="content">
            <D:Button runat="server" ID="btnBack" Text="Go back" />
        </div>
    </form>
</body>
</html>
