﻿using System;
using System.Web;

namespace Obymobi.ObymobiCms
{
    public partial class _500 : Dionysos.Web.UI.PageDefault
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnBack.Attributes.Add("onClick", "javascript:history.back(); return false;");
            lblException.Text = (HttpContext.Current.Application.Get("Exception") as Exception)?.Message;
        }
    }
}
