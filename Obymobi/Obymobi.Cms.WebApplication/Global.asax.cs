using System;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.SessionState;
using Dionysos;
using Dionysos.Data;
using Dionysos.Logging;
using Dionysos.Verification;
using Dionysos.Web;
using Dionysos.Web.Security;
using Dionysos.Web.Sessions;
using Obymobi.Cms.WebApplication.Api;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Verifiers;
using Obymobi.Web;
using Obymobi.Web.Azure.ServiceBus.Notifications;
using Obymobi.Web.CloudStorage;
using Obymobi.Web.GoogleApis;
using Obymobi.Web.Slack;
using Obymobi.Web.Slack.Clients;
using Obymobi.Web.Verifiers;
using SD.LLBLGen.Pro.DQE.SqlServer;

namespace Obymobi.ObymobiCms
{
    /// <summary>
    /// Summary description for Global
    /// </summary>
    public class Global : HttpApplicationDionysosBased
    {
        public Global() : base("ObymobiCms")
        { }

        protected override void Application_Start(object sender, EventArgs e)
        {
            base.Application_Start(sender, e);

            ApiConfiguration.Register(GlobalConfiguration.Configuration);
            ApiConfiguration.SetupFormatters();
        }

        protected void Application_PostAuthorizeRequest()
        {
            if (IsWebApiRequest())
            {
                HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
            }
        }

        private bool IsWebApiRequest() => HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.StartsWith(ApiConfiguration.UrlPrefixRelative);

        public override void DatabaseIndependentPreInitialization()
        {
            // Code that runs on application startup 

            // Set SessionStore
            SessionHelper.SetSessionStore(new CacheSessionStore());

            // Initialize the factory classes
            DataFactory.EntityCollectionFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityCollectionFactory();
            DataFactory.EntityFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityFactory();
            DataFactory.EntityUtil = new Dionysos.Data.LLBLGen.LLBLGenEntityUtil();
            DataFactory.EntityPrefetchPathFactory = new Dionysos.Data.LLBLGen.LLBLGenEntityPrefetchPathFactory();

            // Initialize the assembly information
            Dionysos.Global.AssemblyInfo.Add(Server.MapPath("~/Bin/Obymobi.Data.dll"), "Data");

            // Initialize the configuration information
            Dionysos.Global.ConfigurationInfo.Add(new ObymobiDataConfigInfo());
            Dionysos.Global.ConfigurationProvider = new Dionysos.Configuration.LLBLGenConfigurationProvider();
            Dionysos.Global.ConfigurationInfo.Add(new DionysosWebConfigurationInfo());
            Dionysos.Global.ConfigurationInfo.Add(new DionysosConfigurationInfo());
            Dionysos.Global.ConfigurationInfo.Add(new DionysosWebDevExConfigInfo());
            Dionysos.Global.ConfigurationInfo.Add(new ObymobiConfigInfo());

            // Initialize the application information
            Dionysos.Global.ApplicationInfo = new WebApplicationInfo
            {
                ApplicationName = "ObymobiCms",
                ApplicationVersion = "1.2019103101"
            };

            // Set translation provider (now only used for translation purposes
            Dionysos.Global.TranslationProvider = new Dionysos.Globalization.LLBLGenTranslationProvider();
            Dionysos.Global.GlobalizationHelper = new Obymobi.Logic.Cms.GlobalizationHelper
            {
                AutoLearnNewControls = TestUtil.IsPcDeveloper
            };

            // Comet Helper
            CometHelper.SetCometProvider(new CometDesktopProvider(ClientCommunicationMethod.SignalR, CometConstants.CometIdentifierCms, NetmessageClientType.Cms));
        }

        public override void DatabaseDependentInitialization()
        {
            // Initialize the user manager            
            UserManager.Instance.FieldMappings.Username = Data.HelperClasses.UserFields.Username.Name;
            UserManager.Instance.UserEntityName = "UserEntity";

            // Set locking method
            DynamicQueryEngine.UseNoLockHintOnSelects = ConfigurationManager.GetBool(ObymobiDataConfigConstants.UseNoLockHintOnSelects);

            DatabaseDependentConfigurationValidation();

            // Setup Azure Notification Provider
            new AzureNotificationProvider(new AsyncWebAppLoggingProvider(HostingEnvironment.MapPath("~/App_Data/Logs/"), "Webservice", 7));

            // Update the version
            if (!TestUtil.IsPcDeveloper)
            {
                EnableDatabaseProfiler();
            }

            PreventAppDomainRecycleOnFileChange();
        }

        protected override void Application_Error(object sender, EventArgs e)
        {
            try
            {
                Exception error = HttpContext.Current.Server.GetLastError().GetBaseException();
                HttpContext.Current.Application.Add("Exception", error);

                if (error is AuthorizationException)
                {
                    Redirect(WebShortcuts.ResolveUrl("~/401.aspx"), true);
                }
                else if (error is EntityNotFoundException || (error is HttpException && (error as HttpException).GetHttpCode() == 404))
                {
                    Redirect(WebShortcuts.ResolveUrl("~/404.aspx"), false);
                }
                else
                {
                    Redirect(WebShortcuts.ResolveUrl("~/500.aspx"), true);
                }
            }
            catch
            {
                // If the custom logging fails, use the framework logging
                base.Application_Error(sender, e);
            }
        }

        private void Redirect(string url, bool logError)
        {
            if (logError)
            {
                LogToSlack(HttpContext.Current);
            }

            Server.ClearError();
            Server.Transfer(url, true);
        }

        private static void LogToSlack(HttpContext context)
        {
            UserEntity currentUser = CmsSessionHelper.CurrentUser;
            SlackMessage slackMessage = new SlackMessage(CreateMessageText(context), currentUser.Username, null, null);
            SlackClient slackClient = new SlackClient(SlackWebhookEndpoints.DevSupportNotifications);
            slackClient.SendMessage(slackMessage);
        }

        private static string CreateMessageText(HttpContext httpContext)
        {
            MessageFromExceptionFactory messageFactory = new MessageFromExceptionFactory();
            return messageFactory.Create(httpContext);
        }

        protected override void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if (!HttpApplicationDionysosBased.applicationInitialized)
            {
                if (this.Request.IsLocal)
                {
                    throw new Exception(string.Format("Application_Start did not complete: Database was unavailable ({0} - {1}). ConnectionStringPoller will try to get a working ConnectionString from the NocService",
                        HttpApplicationDionysosBased.lastDatabaseStatus, Obymobi.Data.DaoClasses.CommonDaoBase.ActualConnectionString));
                }
                else
                {
                    throw new Exception(string.Format("Application_Start did not complete: Database was unavailable ({0}). ConnectionStringPoller will try to get a working ConnectionString from the NocService",
                                        HttpApplicationDionysosBased.lastDatabaseStatus));
                }
            }

            base.Application_AuthenticateRequest(sender, e);
        }

        override protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            DevExpress.Web.ASPxWebControl.GlobalTheme = "Glass";
            base.Application_PreRequestHandlerExecute(sender, e);
            if (!this.Request.RawUrl.Contains("SystemInfo"))
            {
                this.Application["LastRequest"] = DateTime.Now;
            }

            // Set the User manager
            // First set the FieldMappings, because the entity is checked upon insteriion
            if (UserManager.Instance.UserEntityName == string.Empty)
            {
                UserManager.Instance.UsePasswordSalting = false;
                UserManager.Instance.OverrideIsApproved = true;
                UserManager.Instance.UserEntityName = "UserEntity";
            }

            // Set Culture
            if (CmsSessionHelper.CurrentCulture != null)
            {
                Thread.CurrentThread.CurrentUICulture = CmsSessionHelper.CurrentCulture;
            }
        }

        private void DatabaseDependentConfigurationValidation()
        {
            VerifierCollection verifiers = new VerifierCollection
            {
                new DependencyInjectionVerifier(),
                new EntityVerifier(),
                new UserManagementVerifier(),
                new CountryVerifier(),

                new WritePermissionVerifier(HostingEnvironment.MapPath("~/App_Data/Logs/")),
                new WritePermissionVerifier(HostingEnvironment.MapPath("~/App_Data/UpdateLogs/")),
                new WritePermissionVerifier(HostingEnvironment.MapPath("~/Downloads/")),
                new WritePermissionVerifier(HostingEnvironment.MapPath("~/temp/")),
                new WritePermissionVerifier(HostingEnvironment.MapPath("~/temp/ThumbnailGenerator/")),
                new WritePermissionVerifier(HostingEnvironment.MapPath("~/Media")),

                new AppSettingsVerifier(DionysosConfigurationConstants.BaseUrl),
                new AppSettingsVerifier(DionysosConfigurationConstants.CloudEnvironment),

                new AppSettingsVerifier(DionysosConfigurationConstants.CraveGatewayPublicUrl),
                new AppSettingsVerifier(DionysosConfigurationConstants.RemoteControlUrl),
                new AppSettingsVerifier(DionysosConfigurationConstants.AuditServiceUrl),

                new AppSettingsVerifier(DionysosConfigurationConstants.SendGridApiKey),
                new AppSettingsVerifier(DionysosConfigurationConstants.ServiceStackLicense),

                new AmazonCloudStorageVerifier(),
                new AmazonCognitoVerifier()
            };

            if (!TestUtil.IsPcDeveloper)
            {
                verifiers.Add(new GoogleApiKeyVerifier());
                // How to get GoogleApisJsonWebTokenBase64 token? https://docs.google.com/document/d/1mxh2qw_Mp3Z_aJf8W9cLQi1LLY2vOwgdZOYwW84nqh4/edit
                verifiers.Add(new AppSettingsVerifier("GoogleApisJsonWebTokenBase64"));
            }

            if (!verifiers.Verify())
            {
                throw new VerificationException(verifiers.ErrorMessage);
            }
        }

        private void PreventAppDomainRecycleOnFileChange()
        {
            // ES: The following piece of code stops the appdomain recycle (and therefore the sign-out) that happens when a folder gets removed (e.g. after generating configuration files)
            PropertyInfo p = typeof(System.Web.HttpRuntime).GetProperty("FileChangesMonitor", BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
            object o = p.GetValue(null, null);
            FieldInfo f = o.GetType().GetField("_dirMonSubdirs", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.IgnoreCase);
            object monitor = f.GetValue(o);
            MethodInfo m = monitor.GetType().GetMethod("StopMonitoring", BindingFlags.Instance | BindingFlags.NonPublic);
            m.Invoke(monitor, new object[] { });
        }
    }
}
