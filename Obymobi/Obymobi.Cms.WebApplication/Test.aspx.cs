﻿using System;
using DevExpress.Web;
using Dionysos.Web.UI;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms
{
    public partial class Test : PageDefault
    {
        protected override void OnInit(EventArgs e)
        {
            if (CmsSessionHelper.CurrentRole != Role.GodMode)
            {
                Response.Redirect("~/");

                return;
            }

            LoadUserControls();
            base.OnInit(e);
        }

        private void LoadUserControls()
        {
            SetGui();
            PrepareTokenBox();
        }

        protected void Page_Load(object sender, EventArgs args)
        {
            if (IsPostBack)
            {
                TagBox.AllowCustomTokens = chkAllowCustomTokens.Checked;
                TagBox.ShowDropDownOnFocus = (ShowDropDownOnFocusMode)ddlShowDropdownOnFocus.Value;
                TagBox.IncrementalFilteringMode = (IncrementalFilteringMode)ddlIncrementalFilteringMode.Value;
                TagBox.ReadOnly = cbReadOnly.Checked;
            }
        }

        private void SetGui()
        {
            ddlShowDropdownOnFocus.DataBindEnum<ShowDropDownOnFocusMode>();
            ddlIncrementalFilteringMode.DataBindEnum<IncrementalFilteringMode>();
        }

        private void PrepareTokenBox()
        {
            chkAllowCustomTokens.Value = TagBox.AllowCustomTokens;
            ddlShowDropdownOnFocus.Value = (int)TagBox.ShowDropDownOnFocus;
            ddlIncrementalFilteringMode.Value = (int)TagBox.IncrementalFilteringMode;
            cbReadOnly.Value = TagBox.ReadOnly;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductTagFields.ParentCompanyId == CmsSessionHelper.CurrentCompanyId);
            filter.AddWithAnd(ProductTagFields.ProductId == 262083);

            ProductTagCollection productTagCollection = new ProductTagCollection();
            productTagCollection.GetMulti(filter);

            TagBox.SetSelectedTags(productTagCollection);
        }
    }
}
