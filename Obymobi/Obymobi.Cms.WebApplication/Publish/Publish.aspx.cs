﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI.WebControls;
using Crave.Api.Logic.Requests.Publishing;
using Crave.Api.Logic.UseCases;
using DevExpress.Web;
using DevExpress.Web.ASPxTreeList;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.Slack;
using Obymobi.Web.Slack.Clients;
using Obymobi.Web.Slack.Interfaces;
using LogicHelpers = Obymobi.Logic.HelperClasses;

namespace Obymobi.ObymobiCms.Publish
{
    public partial class Publish : PageDefault
    {
        #region Fields

        private bool showAllContent = false;

        private readonly ISlackClient _slackClient;

        #endregion

        public Publish()
        {
            _slackClient = new SlackClient(SlackWebhookEndpoints.DevSupportNotifications);
        }

        #region Methods

        public void PublishContent()
        {
            this.PublishContent(false);
        }

        public void ShowForcePublishConfirmationDialog()
        {
            this.pcPopup.ShowOnPageLoad = true;
        }

        public void Refresh()
        {
            WebShortcuts.Redirect(new QueryStringHelper().MergeQuerystringWithRawUrl());
        }

        public void ShowAllContent()
        {
            WebShortcuts.Redirect(PathHelper.UrlWithoutQueryString + "?showAll=true");
        }

        public void ShowModifiedContent()
        {
            WebShortcuts.Redirect(PathHelper.UrlWithoutQueryString);
        }

        private void SetButtonsForRole(Role role)
        {
            switch (role)
            {
                case Role.GodMode:
                    btnPublish.Enabled = true;
                    btnPublish.Visible = true;
                    btnPublishForced.Enabled = true;
                    btnPublishForced.Visible = true;
                    btnRefresh.Visible = true;
                    btnShowAllContent.Visible = !showAllContent;
                    btnShowModifiedContent.Visible = showAllContent;
                    break;
                case Role.Crave:
                    btnPublish.Visible = true;
                    btnPublishForced.Visible = true;
                    btnRefresh.Visible = true;
                    btnShowAllContent.Visible = false;
                    btnShowModifiedContent.Visible = false;
                    break;
                default:
                    btnPublish.Visible = true;
                    btnPublishForced.Visible = false;
                    btnRefresh.Visible = true;
                    btnShowAllContent.Visible = false;
                    btnShowModifiedContent.Visible = false;
                    break;
            }
        }

        private void CreateTreeList()
        {
            this.tlPublishing.AutoGenerateColumns = false;
            this.tlPublishing.Settings.GridLines = GridLines.Horizontal;
            this.tlPublishing.Width = Unit.Percentage(100);
            this.tlPublishing.SettingsEditing.Mode = TreeListEditMode.EditFormAndDisplayNode;
            this.tlPublishing.SettingsSelection.Enabled = true;
            this.tlPublishing.SettingsSelection.Recursive = true;
            this.tlPublishing.SettingsSelection.AllowSelectAll = true;
            this.tlPublishing.SettingsBehavior.ProcessSelectionChangedOnServer = true;

            this.tlPublishing.KeyFieldName = "ContentId";
            this.tlPublishing.ParentFieldName = "ContentParentId";

            TreeListHyperLinkColumn tlcName = new TreeListHyperLinkColumn();
            tlcName.Caption = "Content";
            tlcName.Name = "Content";
            tlcName.FieldName = "CmsUrl";
            tlcName.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            tlcName.PropertiesHyperLink.TextField = "Text";
            tlcName.PropertiesHyperLink.NavigateUrlFormatString = "{0}";
            this.tlPublishing.Columns.Add(tlcName);

            TreeListTextColumn tlcLastModified = new TreeListTextColumn();
            tlcLastModified.Caption = "Last modified";
            tlcLastModified.Name = "LastModified";
            tlcLastModified.FieldName = "LastModified";
            tlcLastModified.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            this.tlPublishing.Columns.Add(tlcLastModified);

            TreeListTextColumn tlcLastPublished = new TreeListTextColumn();
            tlcLastPublished.Caption = "Last published";
            tlcLastPublished.Name = "LastPublished";
            tlcLastPublished.FieldName = "LastPublished";
            tlcLastPublished.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            this.tlPublishing.Columns.Add(tlcLastPublished);

            // Published by
            TreeListTextColumn tlcPublishedBy = new TreeListTextColumn();
            tlcPublishedBy.Caption = "Published by";
            tlcPublishedBy.Name = "PublishedBy";
            tlcPublishedBy.FieldName = "PublishedBy";
            tlcPublishedBy.VisibleIndex = 5;
            tlcPublishedBy.CellStyle.HorizontalAlign = HorizontalAlign.Left;
            this.tlPublishing.Columns.Add(tlcPublishedBy);

            if (this.showAllContent && CmsSessionHelper.CurrentRole == Role.GodMode)
            {
                // Edit page
                TreeListHyperLinkColumn tlcEdit = new TreeListHyperLinkColumn();
                tlcEdit.Name = "EditPage";
                tlcEdit.VisibleIndex = 6;
                tlcEdit.Caption = "&nbsp;";
                tlcEdit.FieldName = "PublishingId";
                tlcEdit.CellStyle.HorizontalAlign = HorizontalAlign.Center;
                tlcEdit.EditCellStyle.CssClass = "hidden";
                tlcEdit.Width = Unit.Pixel(25);
                tlcEdit.PropertiesHyperLink.ImageUrl = "~/images/icons/page_edit.png";
                tlcEdit.PropertiesHyperLink.NavigateUrlFormatString = "~/Generic/Publishing.aspx?id={0}";
                tlcEdit.PropertiesHyperLink.Target = "_blank";
                this.tlPublishing.Columns.Add(tlcEdit);
            }

            this.tlPublishing.HtmlDataCellPrepared += treeList_HtmlDataCellPrepared;
            this.tlPublishing.DataBound += treeList_OnDataBound;

            this.tlPublishing.DataSource = new PublishingDataSource(CmsSessionHelper.CurrentCompanyId, CmsSessionHelper.CurrentUser.TimeZoneInfo, this.showAllContent);
        }

        private void RemoveSelectionFromLeafs()
        {
            TreeListNodeIterator iterator = this.tlPublishing.CreateNodeIterator();
            TreeListNode node = iterator.GetNext();
            while (node != null)
            {
                node.AllowSelect = node.HasChildren;
                node = iterator.GetNext();
            }
        }

        private void PublishContent(bool force, string reasonText = default)
        {
            string restApiUrl = WebEnvironmentHelper.GetRestApiBaseUrl();
            int currentCompanyId = CmsSessionHelper.CurrentCompanyId;
            UserEntity currentUser = CmsSessionHelper.CurrentUser;

            List<PublishRequestBase> selectedPublishRequests = new List<PublishRequestBase>();

            List<TreeListNode> selectedNodes = this.tlPublishing.GetSelectedNodes();
            foreach (TreeListNode node in selectedNodes)
            {
                PublishRequestBase publishRequest = node["PublishRequest"] as PublishRequestBase;
                if (publishRequest != null)
                {
                    publishRequest.CompanyId = currentCompanyId;
                    publishRequest.User = currentUser;
                    publishRequest.BaseUrl = restApiUrl;
                    publishRequest.Force = force;

                    selectedPublishRequests.Add(publishRequest);
                }
            }

            if (selectedPublishRequests.Any())
            {
                if (force)
                {
                    _slackClient.SendMessage(new SlackMessage(reasonText, currentUser.Fullname));
                }

                foreach (PublishRequestBase publishRequest in selectedPublishRequests)
                {
                    new PublishUseCase().Execute(publishRequest);
                }
            }

            WebShortcuts.Redirect(new QueryStringHelper().MergeQuerystringWithRawUrl());
        }

        #endregion

        #region Event handlers

        protected override void OnInit(EventArgs e)
        {
            this.showAllContent = QueryStringHelper.HasValue<bool>("showAll");

            base.OnInit(e);
            this.CreateTreeList();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.showAllContent = QueryStringHelper.HasValue<bool>("showAll");

            if (!this.IsPostBack)
            {
                this.tlPublishing.DataBind();
            }

            this.tlPublishing.ExpandAll();

            this.SetPageTitle();
            this.SetButtonsForRole(CmsSessionHelper.CurrentRole);
            this.UpdateButtonStates();
            this.SetNotifications();

            this.btnPublishForced.Click += ShowForcePublishReasonPopup;
            this.btnConfirmPublishForced.Click += ConfirmForcePublish;
        }

        private void SetPageTitle()
        {
            TabPage contentTab = this.tabsMain.TabPages.FindByName("PublishContent");
            if (contentTab != null)
            {
                contentTab.Text = this.showAllContent ? "All content" : "Modified content";
            }
        }

        private void UpdateButtonStates()
        {
            this.btnPublish.Enabled = this.DeterminePublishEnabled();

            if (CmsSessionHelper.CurrentRole == Role.GodMode)
            {
                this.btnPublish.Enabled = true;
            }
        }

        private void treeList_HtmlDataCellPrepared(object sender, TreeListHtmlDataCellEventArgs e)
        {
            try
            {
                if (e.Column.Name.Equals("LastPublished"))
                {
                    bool hasUnpublishedChanges = (bool)e.GetValue("HasUnpublishedChanges");
                    if (hasUnpublishedChanges)
                        e.Cell.ForeColor = Color.Red;
                    else
                        e.Cell.ForeColor = Color.Green;
                }
                else if (e.Column.Name.Equals("EditPage") && this.showAllContent)
                {
                    int publishingId = (int)e.GetValue("PublishingId");
                    if (publishingId <= 0)
                    {
                        e.Cell.Text = "";
                    }
                }
            }
            catch { }
        }

        private void ShowForcePublishReasonPopup(object sender, EventArgs e)
        {
            this.pcPopup.ShowOnPageLoad = true;
        }

        private void treeList_OnDataBound(object sender, EventArgs e)
        {
            if (!this.showAllContent)
            {
                if (this.tlPublishing.DataSource is PublishingDataSource)
                {
                    bool hasContentToPublish = ((PublishingDataSource)this.tlPublishing.DataSource).DataSource.Count > 0;

                    this.tlPublishing.Visible = hasContentToPublish;
                    this.lblContentPublished.Visible = !hasContentToPublish;

                }
                return;
            }

            this.RemoveSelectionFromLeafs();
        }

        private void SetNotifications()
        {
            if (!this.DeterminePublishEnabled())
            {
                this.AddInformator(InformatorType.Information, "Unable to publish at this moment. Scheduled command task is running for this company");

                this.btnPublish.Enabled = false;
                this.btnPublishForced.Enabled = false;

                this.Validate();
            }
        }

        private bool DeterminePublishEnabled()
        {
            return !LogicHelpers.ScheduledCommandTaskHelper.IsScheduledCommandTaskRunning(CmsSessionHelper.CurrentCompanyId);
        }



        private void ConfirmForcePublish(object sender, EventArgs e)
        {
            string reasonText = this.tbReason.Text;
            if (string.IsNullOrWhiteSpace(reasonText))
            {
                this.MultiValidatorDefault.AddError("Please specifiy the reason why the regular publish didn’t work.");
                this.Validate();

                return;
            }

            this.PublishContent(true, reasonText);
        }

        #endregion
    }
}
