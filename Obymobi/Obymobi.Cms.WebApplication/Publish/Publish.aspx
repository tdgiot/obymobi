﻿<%@ Page Title="Publish content" Language="C#" MasterPageFile="~/MasterPages/MasterPageBase.master" AutoEventWireup="true" Inherits="Obymobi.ObymobiCms.Publish.Publish" CodeBehind="Publish.aspx.cs" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxTreeList" Assembly="DevExpress.Web.ASPxTreeList.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"  %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v20.1, Version=20.1.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cplhTitleHolder" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cplhToolbarHolder" runat="Server">
    <span class="toolbar">
        <X:ToolBarButton runat="server" ID="btnPublish" Text="Publish" Image-Url="~/Images/Icons/table_save.png" CommandName="PublishContent" LocalizeText="false" />
        <X:ToolBarButton runat="server" ID="btnPublishForced" Text="Publish (forced)" Image-Url="~/Images/Icons/table_save.png" CommandName="ShowForcePublishConfirmationDialog" LocalizeText="false" />
        <X:ToolBarButton runat="server" ID="btnRefresh" Text="Refresh" Image-Url="~/Images/Icons/arrow_refresh.png" CommandName="Refresh" LocalizeText="false" />
        <X:ToolBarButton runat="server" ID="btnShowAllContent" Text="Show all content" Image-Url="~/Images/Icons/cog.png" CommandName="ShowAllContent" LocalizeText="false" Visible="false" />
        <X:ToolBarButton runat="server" ID="btnShowModifiedContent" Text="Show modified content" Image-Url="~/Images/Icons/cog.png" CommandName="ShowModifiedContent" LocalizeText="false" Visible="false" />
    </span>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cplhContentHolder" runat="Server">
    <style type="text/css">
        a.dxeHyperlink_Glass {
            color: #2a769d;
        }

        a:hover.dxeHyperlink_Glass {
            color: #2a769d;
        }

        a:visited.dxeHyperlink_Glass {
            color: #2a769d;
        }
    </style>

    <div>
        <X:PageControl ID="tabsMain" runat="server" Width="100%">
            <TabPages>
                <X:TabPage Text="Publish content" Name="PublishContent" LocalizeText="false">
                    <controls>
                        <D:Label ID="lblContentPublished" runat="server" LocalizeText="False" Style="width:100%; text-align: center; float:left" Visible="False">All content has been published!</D:Label>
                        <dx:ASPxTreeList ID="tlPublishing" ClientInstanceName="tlPublishing" runat="server" EnableViewState="False" />
                    </controls>
                </X:TabPage>
            </TabPages>
        </X:PageControl>
    </div>

    <dx:ASPxPopupControl id="pcPopup" runat="server" width="460" closeaction="CloseButton" closeonescape="true" modal="True" showonpageload="False"
        popuphorizontalalign="WindowCenter" popupverticalalign="WindowCenter" clientinstancename="pcPopup"
        headertext="Force publish reason confirmation" allowdragging="True" popupanimationtype="None" enableviewstate="False" autoupdateposition="true">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <dx:ASPxPanel ID="ASPxPanel1" runat="server" DefaultButton="btOK">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                            <table class="dataformV2" width="460px">
                                <tr>
                                    <td class="label">
                                        <D:Label runat="server" ID="lblNotes">Reason</D:Label>
                                    </td>
                                    <td class="control">
                                        <D:TextBoxMultiLine runat="server" ID="tbReason" ClientIDMode="Static" Rows="5" IsRequired="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label"></td>
                                    <td class="control">
                                        <D:Button ID="btnConfirmPublishForced" runat="server" Text="Submit" />
                                    </td>
                                </tr>
                            </table>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>

        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>
