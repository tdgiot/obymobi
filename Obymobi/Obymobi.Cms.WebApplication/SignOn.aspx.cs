﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.StoredProcedureCallerClasses;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.LinqSupportClasses;
using System.Linq;
using Dionysos;
using Dionysos.Web;
using Dionysos.Web.UI;
using Obymobi.Data.Linq;
using System.Diagnostics;
using Obymobi.Enums;
using System.Collections.Generic;

namespace Obymobi.ObymobiCms
{
    public partial class SignOn : PageDefault
    {
        void HookUpEvents()
        {
            this.btEn.Click += new EventHandler(btEn_Click);
            this.btNl.Click += new EventHandler(btNl_Click);
        }

        void btNl_Click(object sender, EventArgs e)
        {
            CmsSessionHelper.CurrentCulture = new System.Globalization.CultureInfo("nl-NL");
            WebShortcuts.RedirectUsingRawUrl();
        }

        void btEn_Click(object sender, EventArgs e)
        {
            CmsSessionHelper.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
            WebShortcuts.RedirectUsingRawUrl();
        }

        protected new void Page_Unload(object sender, EventArgs e)
        {
            if (QueryStringHelper.HasValue("SignOut"))
            {
                var response = HttpContext.Current.Response;

                if (response.Cookies["currentUser"] != null)
                {
                    response.Cookies["currentUser"].Expires = DateTime.Now.AddDays(-1);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HookUpEvents();

            // Set name and logo
            this.hlPoweredBy.Text = Dionysos.Global.ApplicationInfo.ApplicationVersion + " - Crave Interactive";
            this.hlPoweredBy.NavigateUrl = "http://www.crave-emenu.com";
            this.lblApplicationName.Text = "Crave Content Management System";

            if (QueryStringHelper.HasValue("SuperHero") && TestUtil.IsPcGabriel)
            {
                LinqMetaData metaData = new LinqMetaData();
                //var items = metaData.Orderitem.Where(oi => oi.ProductEntity.ProductCategoryCollection.Any(x => x.CategoryId == 1));
                //foreach (var item in items)
                //{
                //	Debug.WriteLine(item.ProductName);
                //}

                DateTime from = new DateTime(2016, 3, 7);
                DateTime till = new DateTime(2016, 3, 14);
                List<int> categoryIds = new List<int> { 24529, 22468, 22470 };


                // Retrieve Revenue with filtering on OrderType, Dates, Company & Categories
                /*
                SELECT sum((ISNULL(AlterationsPriceIn, 0) + ProductPriceIn) * Quantity) AS TotalRevenue
                FROM
                    (SELECT
                        (SELECT sum(OrderitemAlterationitem.AlterationoptionPriceIn)
                        FROM OrderitemAlterationitem
                        WHERE OrderitemAlterationitem.OrderitemId = Orderitem.OrderitemId) AS AlterationsPriceIn, Quantity, ProductPriceIn
                    FROM Orderitem
                    LEFT JOIN [Order] ON Orderitem.OrderId = [Order].OrderId    
                    WHERE 
                        [order].Created > '2016-03-07' and [order].Created < '2016-03-14' and [order].Type = 200 and [Order].CompanyId = 343
		                and 
		                Orderitem.ProductId in (select ProductId from ProductCategory where ProductCategory.CategoryId in (24529, 22468, 22470))
		                ) oi
                */
                //var query5 = metaData.Order
                //    .Where(order => order.Type == (int)OrderType.Standard &&
                //                    order.Created > from &&
                //                    order.Created < till &&
                //                    order.CompanyId == 343)
                //    .Sum(order => order.OrderitemCollection
                //                                        .Where(oi => oi.ProductEntity.ProductCategoryCollection.Any(pc => categoryIds.Contains(pc.CategoryId)))
                //                                        .Sum(oi => (oi.ProductPriceIn * oi.Quantity) + 
                //                                                    (((decimal?)(oi.OrderitemAlterationitemCollection.Sum(oiai => oiai.AlterationoptionPriceIn)) ?? 0) * oi.Quantity)
                //                                            )
                //                                        );

                // Retrieve Quantity of Products Ordered with filtering on OrderType, Dates, Company & Categories
                /*
                SELECT sum(Quantity) AS TotalRevenue
                FROM
                    (SELECT
                        (SELECT sum(OrderitemAlterationitem.AlterationoptionPriceIn)
                        FROM OrderitemAlterationitem
                        WHERE OrderitemAlterationitem.OrderitemId = Orderitem.OrderitemId) AS AlterationsPriceIn, Quantity, ProductPriceIn
                    FROM Orderitem
                    LEFT JOIN [Order] ON Orderitem.OrderId = [Order].OrderId    
                    WHERE 
                        [order].Created > '2016-03-07' and [order].Created < '2016-03-14' and [order].Type = 200 and [Order].CompanyId = 343
		                and 
		                Orderitem.ProductId in (select ProductId from ProductCategory where ProductCategory.CategoryId in (24529, 22468, 22470))
		                ) oi
                */
                //var query6 = metaData.Order
                //    .Where(order => order.Type == (int)OrderType.Standard &&
                //                    order.Created > from &&
                //                    order.Created < till &&
                //                    order.CompanyId == 343)
                //    .Sum(order => order.OrderitemCollection
                //                                        .Where(oi => oi.ProductEntity.ProductCategoryCollection.Any(pc => categoryIds.Contains(pc.CategoryId)))
                //                                        .Sum(oi => oi.Quantity)                                                            
                //                                        );

                // Order Quantities for Orders containing 1 or more Orderitems qualifying what we're looking for
                /*
                SELECT 
	                COUNT(*) 
                FROM 
	                [Order]
                WHERE 
	                [order].Created > '2016-03-07' 
	                and [order].Created < '2016-03-14' 
	                and [order].Type = 200 
	                and [Order].CompanyId = 343
	                and [Order].OrderId in (SELECT OrderId FROM Orderitem WHERE Orderitem.ProductId in (select ProductId from ProductCategory where ProductCategory.CategoryId in (24529, 22468, 22470)))
                */
                //var query7 = metaData.Order
                //    .Where(order => order.Type == (int)OrderType.Standard &&
                //                    order.Created > from &&
                //                    order.Created < till &&
                //                    order.CompanyId == 343 &&
                //                    order.OrderitemCollection.Any(oi => oi.ProductEntity.ProductCategoryCollection.Any(pc => categoryIds.Contains(pc.CategoryId)))).Count();

                //.Sum(order => order.OrderId);
                // this.plhNormalMessage.AddHtml(query7.ToString());






                //TranslationCollection translations = new TranslationCollection();
                //PredicateExpression filter = new PredicateExpression();
                //filter.Add(TranslationFields.TranslationKey % "EntityFieldInformation.%.FriendlyName");

                //translations.GetMulti(filter);

                //foreach (var translation in translations)
                //{
                //	string[] elements = translation.TranslationKey.Split('.');

                //	if (translation.TranslationValue.IsNullOrWhiteSpace())
                //	{
                //		translation.TranslationValue = elements[2];                        
                //	}

                //	if (translation.TranslationValue.EndsWith("id"))
                //		translation.TranslationValue = translation.TranslationValue.Substring(0, translation.TranslationValue.Length - 2);

                //	translation.TranslationValue = StringUtil.TurnFirstToUpper(StringUtil.FormatPascalCase(translation.TranslationValue), true);

                //	translation.Save();
                //}
            }

            if (GlobalizationHelper.CurrentUICultureLanguageCode == "en")
                this.btEn.CssClass = "flag-en active";
            else
                this.btNl.CssClass = "flag-nl active";

            // MDB TODO
            this.Title = this.lblApplicationName.Text + " - "; // +Resources.InCodeMessages.Generic_SignOn_SignOn;

            if (QueryStringHelper.HasValue("msg"))
            {
                string validationMessage = QueryStringHelper.GetValue("msg");

                this.plhNormalMessage.Visible = false;
                this.plhSignInError.Visible = true;

                lblErrorMessage.Text = validationMessage;
            }

            if (QueryStringHelper.HasValue("NoRightsToAccesCompanyData"))
            {
                this.plhNormalMessage.Visible = false;
                this.plhSignInError.Visible = true;

                if (GlobalizationHelper.CurrentUICultureLanguageCode == "en")
                {
                    this.lblErrorMessage.Text = this.Translate("NoRightsToAccesCompanyData", "The chosen data is not available to you.");
                }

                else
                {
                    this.lblErrorMessage.Text = this.Translate("NoRightsToAccesCompanyData", "De gekozen gegevens zijn niet voor u beschikbaar.");
                }
            }

            if (QueryStringHelper.HasValue("SignOut"))
            {
                var response = HttpContext.Current.Response;

                if (response.Cookies["currentUser"] != null)
                {
                    response.Cookies["currentUser"].Expires = DateTime.Now.AddDays(-1);
                }
            }
        }
    }
}
