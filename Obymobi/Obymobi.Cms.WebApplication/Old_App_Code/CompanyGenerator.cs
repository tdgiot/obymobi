﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Routing;
using Obymobi.Web.CloudStorage;
using Obymobi.ObymobiCms.Catalog;

public class CompanyGenerator
{
    #region Fields

    private readonly CompanyGeneratorSettings settings;
    private Transaction transaction;

    private CompanyOwnerEntity companyOwner;
    private CompanyEntity company;
    private TerminalEntity terminal;
    private TerminalConfigurationEntity terminalConfiguration;
    private UIModeEntity terminalUIMode;
    private MenuEntity menu;
    private DeliverypointgroupEntity deliverypointgroup;
    private UIModeEntity deliverypointgroupUIMode;
    private RouteEntity route;
    private DeviceEntity device;
    private ClientEntity client;
    private TaxTariffEntity taxTariff;

    #endregion

    #region Constructor

    public CompanyGenerator(CompanyGeneratorSettings settings)
    {
        this.settings = settings;
    }

    #endregion

    #region Public methods

    public bool Generate(out string errorMsg, out int companyId)
    {
        bool success = false;

        this.transaction = new Transaction(IsolationLevel.ReadCommitted, "CompanyGenerator");

        errorMsg = string.Empty;
        companyId = -1;

        try
        {
            this.GenerateInternal();

            this.transaction.Commit();

            this.CreateAmazonUser();

            companyId = this.company.CompanyId;
            success = true;
        }
        catch (Exception ex)
        {
            this.transaction.Rollback();
            errorMsg = ex.Message;
        }
        finally
        {
            this.transaction.Dispose();            
        }

        return success;
    }

    #endregion

    #region Private methods

    private void GenerateInternal()
    {
        this.CreateCompanyOwner();
        this.CreateCompany();
        this.CreateCompanyCultures();
        this.CreateBusinesshours();
        this.CreateTerminalUIMode();
        this.CreateTerminalConfiguration();
        this.CreateTerminal();
        this.CreateRoute();
        this.CreateMenu();
        this.CreateDeliverypointgroupUIMode();
        this.CreateDeliverypointgroup();    
        this.CreateDeliverypoints();
        this.CreateDevice();
        this.CreateClient();
        this.CreateZeroPercentageTaxTariff();
    }

    private void CreateAmazonUser()
    {
        string username, accessKey, accessKeySecret;
        if (AmazonCloudStorageHelper.CreateAmazonUser(this.company, out username, out accessKey, out accessKeySecret))
        {
            CloudStorageAccountEntity storageAccount = AmazonCloudStorageHelper.CreateCloudStorageAccount(CloudStorageAccountType.Amazon, company.CompanyId, username, accessKey, accessKeySecret);
            storageAccount.Save();
        }
    }

    private void CreateCompanyOwner()
    {
        this.companyOwner = new CompanyOwnerEntity();
        this.companyOwner.AddToTransaction(this.transaction);
        this.companyOwner.Username = this.settings.Username;
        this.companyOwner.Password = Dionysos.Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(this.settings.Password);
        this.companyOwner.IsPasswordEncrypted = true;        
        this.companyOwner.Save();
    }

    private void CreateCompany()
    {
        this.company = new CompanyEntity();
        this.company.AddToTransaction(this.transaction);
        this.company.CompanyOwnerEntity = this.companyOwner;
        this.company.SupportpoolId = this.settings.SupportpoolId;
        this.company.Name = this.settings.CompanyName;
        this.company.NameWithoutDiacritics = this.settings.CompanyName.ReplaceDiacritics();        
        this.company.Zipcode = this.settings.Zipcode;
        this.company.CountryCode = this.settings.CountryCode;
        this.company.CountryId = 1;
        this.company.CurrencyCode = this.settings.CurrencyCode;                
        this.company.CurrencyId = 1;
        this.company.CultureCode = this.settings.CultureCode;
        this.company.TimeZoneOlsonId = this.settings.TimeZoneOlsonId;
        this.company.StandardReceiptTypes = string.Empty;
        this.company.StandardCheckInterval = 30;
        this.company.ApiVersion = this.settings.ApiVersion;
        this.company.MessagingVersion = this.settings.MessagingVersion;
        this.company.Save();
    }

    private void CreateCompanyCultures()
    {
        foreach (string cultureCode in this.settings.CompanyCultures)
        {
            CompanyCultureEntity companyCulture = new CompanyCultureEntity();
            companyCulture.AddToTransaction(this.transaction);
            companyCulture.CompanyEntity = this.company;
            companyCulture.CultureCode = cultureCode;
            companyCulture.Save();
        }
    }

    private void CreateBusinesshours()
    {
        for (int i = 0; i < 7; i++)
        {
            BusinesshoursEntity start = new BusinesshoursEntity();
            start.AddToTransaction(this.transaction);            
            start.CompanyEntity = this.company;
            start.DayOfWeekAndTime = string.Format("{0}0000", i);
            start.Opening = true;
            start.Save();

            BusinesshoursEntity end = new BusinesshoursEntity();
            end.AddToTransaction(this.transaction);            
            end.CompanyEntity = this.company;
            end.DayOfWeekAndTime = string.Format("{0}2359", i);
            end.Opening = false;            
            end.Save();
        }
    }

    private void CreateTerminalUIMode()
    {
        if (!this.settings.CreateTerminal)
            return;

        this.terminalUIMode = new UIModeEntity();
        this.terminalUIMode.AddToTransaction(this.transaction);
        this.terminalUIMode.CompanyEntity = this.company;
        this.terminalUIMode.Name = string.Format("{0} ui mode", this.settings.TerminalName);
        this.terminalUIMode.Type = UIModeType.VenueOwnedStaffDevices;
        this.terminalUIMode.Save();

        UITabEntity ordersTab = new UITabEntity();
        ordersTab.AddToTransaction(this.transaction);
        ordersTab.UIModeEntity = this.terminalUIMode;
        ordersTab.Type = (int)UITabType.Orders;
        ordersTab.SortOrder = 1000;
        ordersTab.Save();

        UITabEntity messageTab = new UITabEntity();
        messageTab.AddToTransaction(this.transaction);
        messageTab.UIModeEntity = this.terminalUIMode;
        messageTab.Type = (int)UITabType.Message;
        messageTab.SortOrder = 3000;
        messageTab.Save();

        UITabEntity managementTab = new UITabEntity();
        managementTab.AddToTransaction(this.transaction);
        managementTab.UIModeEntity = this.terminalUIMode;
        managementTab.Type = (int)UITabType.Management;
        managementTab.SortOrder = 4000;
        managementTab.Save();

        UITabEntity companyNameTab = new UITabEntity();
        companyNameTab.AddToTransaction(this.transaction);
        companyNameTab.UIModeEntity = this.terminalUIMode;
        companyNameTab.Type = (int)UITabType.TerminalStatus;
        companyNameTab.Width = 360;
        companyNameTab.SortOrder = 6000;
        companyNameTab.Save();

        UITabEntity orderCountTab = new UITabEntity();
        orderCountTab.AddToTransaction(this.transaction);
        orderCountTab.UIModeEntity = this.terminalUIMode;
        orderCountTab.Type = (int)UITabType.OrderCount;
        orderCountTab.Width = 100;
        orderCountTab.SortOrder = 7000;
        orderCountTab.Save();
    }

    private void CreateTerminalConfiguration()
    {
        if (!this.settings.CreateTerminal)
            return;

        this.terminalConfiguration = new TerminalConfigurationEntity();
        this.terminalConfiguration.AddToTransaction(this.transaction);
        this.terminalConfiguration.CompanyEntity = this.company;
        this.terminalConfiguration.UIModeEntity = this.terminalUIMode;        
        this.terminalConfiguration.Name = string.Format("{0} configuration", this.settings.TerminalName);
        this.terminalConfiguration.Save();
    }

    private void CreateTerminal()
    {
        if (!this.settings.CreateTerminal)
            return;

        this.terminal = new TerminalEntity();
        this.terminal.AddToTransaction(this.transaction);
        this.terminal.CompanyEntity = this.company;
        this.terminal.TerminalConfigurationEntity = this.terminalConfiguration;
        this.terminal.UIModeEntity = this.terminalUIMode;
        this.terminal.Name = this.settings.TerminalName;
        this.terminal.TerminalType = this.settings.TerminalType;        
        this.terminal.Save();

        this.terminalConfiguration.TerminalEntity = this.terminal;
        this.terminalConfiguration.Save();
    }    

    private void CreateRoute()
    {
        if (!this.settings.CreateDeliverypointgroup || !this.settings.CreateTerminal || !this.settings.CreateRoute)
            return;

        this.route = new RouteEntity();
        this.route.AddToTransaction(this.transaction);
        this.route.CompanyEntity = this.company;
        this.route.Name = string.Format("{0} route", this.settings.DeliverypointgroupName);
        this.route.StepTimeoutMinutes = 60;
        this.route.ValidatorOverrideCreateDefaultStep = false;
        this.route.Save();

        RoutestepEntity routestep = new RoutestepEntity();
        routestep.AddToTransaction(this.transaction);
        routestep.ValidatorOverrideSequenceChecking = true;
        routestep.RouteEntity = this.route;
        routestep.Number = RoutingHelper.FIRST_STEP_NUMBER;
        routestep.Save();

        RoutestephandlerEntity routestephandler = new RoutestephandlerEntity();
        routestephandler.AddToTransaction(this.transaction);
        routestephandler.RoutestepEntity = routestep;
        routestephandler.TerminalEntity = this.terminal;
        routestephandler.HandlerType = (int) RoutestephandlerType.Console;
        routestephandler.Save();

        this.company.RouteEntity = this.route;
        this.company.Save();
    }

    private void CreateMenu()
    {
        this.menu = new MenuEntity();
        this.menu.AddToTransaction(this.transaction);
        this.menu.CompanyEntity = this.company;
        this.menu.Name = string.Format("{0} menu", this.settings.CompanyName);
        this.menu.Save();
    }

    private void CreateDeliverypointgroupUIMode()
    {
        if (!this.settings.CreateDeliverypointgroup)
            return;

        this.deliverypointgroupUIMode = new UIModeEntity();
        this.deliverypointgroupUIMode.AddToTransaction(this.transaction);
        this.deliverypointgroupUIMode.CompanyEntity = this.company;        
        this.deliverypointgroupUIMode.Name = string.Format("{0} ui mode", this.settings.DeliverypointgroupName);
        this.deliverypointgroupUIMode.ShowBrightness = true;
        this.deliverypointgroupUIMode.ShowDeliverypoint = true;
        this.deliverypointgroupUIMode.ShowBattery = true;
        this.deliverypointgroupUIMode.ShowClock = true;
        this.deliverypointgroupUIMode.Save();

        UITabEntity homeTab = new UITabEntity();
        homeTab.AddToTransaction(this.transaction);
        homeTab.UIModeEntity = this.deliverypointgroupUIMode;
        homeTab.Type = (int)UITabType.Home;
        homeTab.Caption = "Home";
        homeTab.Save();

        // TODO: Categories

        UITabEntity moreTab = new UITabEntity();
        moreTab.AddToTransaction(this.transaction);
        moreTab.UIModeEntity = this.deliverypointgroupUIMode;        
        moreTab.Type = (int)UITabType.More;
        moreTab.Caption = "More";        
        moreTab.Save();

        UITabEntity placeholderTab = new UITabEntity();
        placeholderTab.AddToTransaction(this.transaction);
        placeholderTab.UIModeEntity = this.deliverypointgroupUIMode;        
        placeholderTab.Type = (int)UITabType.Placeholder;
        placeholderTab.Width = 60;
        placeholderTab.Caption = "empty";        
        placeholderTab.Save();

        UITabEntity basketTab = new UITabEntity();
        basketTab.AddToTransaction(this.transaction);
        basketTab.UIModeEntity = this.deliverypointgroupUIMode;
        basketTab.Type = (int)UITabType.Basket;
        basketTab.Caption = "Order Basket";        
        basketTab.Save();
    }    

    private void CreateDeliverypointgroup()
    {
        if (!this.settings.CreateDeliverypointgroup)
            return;

        this.deliverypointgroup = new DeliverypointgroupEntity();
        this.deliverypointgroup.AddToTransaction(this.transaction);
        this.deliverypointgroup.CompanyEntity = this.company;
        this.deliverypointgroup.UIModeEntity = this.deliverypointgroupUIMode;
        this.deliverypointgroup.RouteEntity = this.route;        
        this.deliverypointgroup.UIThemeId = this.settings.UIThemeId;
        this.deliverypointgroup.MenuEntity = this.menu;
        this.deliverypointgroup.Name = this.settings.DeliverypointgroupName;
        this.deliverypointgroup.Pincode = this.settings.Pincode;
        this.deliverypointgroup.PincodeGM = this.settings.PincodeGodmode;
        this.deliverypointgroup.PincodeSU = this.settings.PincodeSuperuser;
        this.deliverypointgroup.Save();
    }

    private void CreateDeliverypoints()
    {
        if (!this.settings.CreateDeliverypointgroup || !this.settings.CreateDeliverypoints)
            return;

        for (int i = this.settings.RangeStart; i <= this.settings.RangeEnd; i++)
        {
            DeliverypointEntity deliverypoint = new DeliverypointEntity();
            deliverypoint.AddToTransaction(this.transaction);
            deliverypoint.CompanyEntity = this.company;
            deliverypoint.DeliverypointgroupEntity = this.deliverypointgroup;
            deliverypoint.Number = i.ToString();
            deliverypoint.Name = i.ToString();
            deliverypoint.Save();
        }
    }

    private void CreateDevice()
    {
        this.device = new DeviceEntity();
        this.device.AddToTransaction(this.transaction);
        this.device.Identifier = string.Format("dummy@{0}.com", this.settings.CompanyName);
        this.device.DeviceModel = DeviceModel.SamsungP5110;
        this.device.Save();
    }

    private void CreateClient()
    {
        this.client = new ClientEntity();        
        this.client.AddToTransaction(this.transaction);
        this.client.CompanyEntity = this.company;
        this.client.DeviceEntity = this.device;
        this.client.Save();
    }

    private void CreateZeroPercentageTaxTariff()
    {
        this.taxTariff = new TaxTariffEntity();
        this.taxTariff.AddToTransaction(this.transaction);
        this.taxTariff.Name = "Tax free";
        this.taxTariff.Percentage = 0;
        this.taxTariff.CompanyEntity = this.company;
        this.taxTariff.Save();
    }

    #endregion
}