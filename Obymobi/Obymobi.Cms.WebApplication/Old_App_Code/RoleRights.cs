using Obymobi.Enums;

namespace Obymobi.Cms.WebApplication.Old_App_Code
{
    public static class RoleRights
    {
        public static bool AllowedToDeleteOutlet(Role role) => role >= Role.Administrator;
        public static bool AllowedToRemoteControl(Role role) => role >= Role.Reseller;
        public static bool AllowedToViewLogCat(Role role) => role >= Role.Crave;
    }
}