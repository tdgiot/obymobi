﻿using System;
using System.Linq;
using System.ComponentModel;

[Serializable]
public class SchedulingAppointmentList : BindingList<SchedulingAppointment>
{
    public void AddRange(SchedulingAppointmentList events)
    {
        Array.ForEach(events.ToArray(), e => this.Add(e));
    }

    public int GetEventIndex(int eventId)
    {
        SchedulingAppointment result = this.FirstOrDefault(item => item.Id == eventId);
        if (result != null)
        {
            return this.IndexOf(result);
        }

        return -1;
    }    
}