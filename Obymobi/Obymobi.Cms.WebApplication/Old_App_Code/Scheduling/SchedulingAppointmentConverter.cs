﻿using System;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Xml;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Dionysos;
using RecurrenceRange = DevExpress.XtraScheduler.RecurrenceRange;
using RecurrenceType = DevExpress.XtraScheduler.RecurrenceType;

namespace Obymobi.Cms.WebApplication.Old_App_Code.Scheduling
{
    public class SchedulingAppointmentConverter
    {
        private readonly TimeZoneInfo timeZoneInfo;

        public SchedulingAppointmentConverter(TimeZoneInfo timeZoneInfo)
        {
            this.timeZoneInfo = timeZoneInfo;
        }

        public SchedulingAppointmentList Convert(UIScheduleItemOccurrenceCollection occurrences)
        {
            SchedulingAppointmentList appointments = new SchedulingAppointmentList();

            foreach (UIScheduleItemOccurrenceEntity occurrence in occurrences)
            {
                appointments.Add(Convert(occurrence));
            }

            return appointments;
        }

        public SchedulingAppointment Convert(UIScheduleItemOccurrenceEntity occurrence)
        {
            SchedulingAppointment appointment = new SchedulingAppointment();
            appointment.Id = occurrence.UIScheduleItemOccurrenceId;
            appointment.ReportProcessingTaskTemplateId = occurrence.ReportProcessingTaskTemplateId.GetValueOrDefault();
            appointment.Type = occurrence.Type;
            appointment.BackgroundColor = occurrence.BackgroundColor;
            appointment.TextColor = occurrence.TextColor;
            appointment.StartTime = occurrence.StartTime.GetValueOrDefault();
            appointment.EndTime = occurrence.EndTime.GetValueOrDefault();

            if (occurrence.Recurring)
            {
                RecurrenceInfo recurrenceInfo = new RecurrenceInfo();
                recurrenceInfo.Type = (RecurrenceType)occurrence.RecurrenceType;
                recurrenceInfo.Range = (RecurrenceRange)occurrence.RecurrenceRange;
                recurrenceInfo.Start = occurrence.RecurrenceStart.GetValueOrDefault();
                recurrenceInfo.End = occurrence.RecurrenceEnd.GetValueOrDefault();
                recurrenceInfo.OccurrenceCount = occurrence.RecurrenceOccurrenceCount;
                recurrenceInfo.Periodicity = occurrence.RecurrencePeriodicity;
                recurrenceInfo.DayNumber = occurrence.RecurrenceDayNumber;
                recurrenceInfo.WeekDays = (WeekDays)occurrence.RecurrenceWeekDays;
                recurrenceInfo.WeekOfMonth = (WeekOfMonth)occurrence.RecurrenceWeekOfMonth;
                recurrenceInfo.Month = occurrence.RecurrenceMonth;

                appointment.RecurrenceId = (Guid)recurrenceInfo.Id;
                appointment.RecurrenceInfo = recurrenceInfo.ToXml();
                appointment.ReminderInfo = string.Empty;
            }

            if (occurrence.ReportProcessingTaskTemplateId.HasValue)
            {
                appointment.Subject = occurrence.ReportProcessingTaskTemplateEntity.FriendlyName;
            }

            return appointment;
        }

        public UIScheduleItemOccurrenceEntity Convert(SchedulingAppointment appointment)
        {
            DateTime startTime = DateTime.SpecifyKind(appointment.StartTime, DateTimeKind.Unspecified);
            DateTime endTime = DateTime.SpecifyKind(appointment.EndTime, DateTimeKind.Unspecified);

            UIScheduleItemOccurrenceEntity occurrence = new UIScheduleItemOccurrenceEntity();
            occurrence.IsNew = appointment.Id <= 0;
            occurrence.UIScheduleItemOccurrenceId = appointment.Id;
            occurrence.ReportProcessingTaskTemplateId = appointment.ReportProcessingTaskTemplateId;
            occurrence.StartTime = startTime;
            occurrence.EndTime = endTime;
            occurrence.StartTimeUTC = startTime.LocalTimeToUtc(this.timeZoneInfo);
            occurrence.EndTimeUTC = endTime.LocalTimeToUtc(this.timeZoneInfo);
            occurrence.BackgroundColor = appointment.BackgroundColor;
            occurrence.TextColor = appointment.TextColor;
            occurrence.Type = appointment.Type;

            if (appointment.Type == (int)AppointmentType.Normal)
            {
                return occurrence;
            }
            
            IRecurrenceInfo recurrenceInfo = RecurrenceInfoXmlPersistenceHelper.ObjectFromXml(appointment.RecurrenceInfo);
            
            TimeSpan difference = endTime - startTime;
            if (recurrenceInfo.Range == 0 && difference.TotalMinutes > 1440) // Forever recurring with difference longer than a day
            {
                occurrence.EndTime = startTime.AddDays(1);
                occurrence.EndTimeUTC = startTime.LocalTimeToUtc(this.timeZoneInfo).AddDays(1);
            }

            DateTime recurrenceStartTime = DateTime.SpecifyKind(recurrenceInfo.Start, DateTimeKind.Unspecified);
            DateTime recurrenceEndTime = DateTime.SpecifyKind(recurrenceInfo.End, DateTimeKind.Unspecified);

            occurrence.Recurring = true;
            occurrence.RecurrenceType = (int)recurrenceInfo.Type;
            occurrence.RecurrenceRange = (int)recurrenceInfo.Range;
            occurrence.RecurrenceStart = recurrenceStartTime;
            occurrence.RecurrenceEnd = recurrenceEndTime;
            occurrence.RecurrenceStartUTC = recurrenceStartTime.LocalTimeToUtc(this.timeZoneInfo);
            occurrence.RecurrenceEndUTC = recurrenceEndTime.LocalTimeToUtc(this.timeZoneInfo);
            occurrence.RecurrenceOccurrenceCount = recurrenceInfo.OccurrenceCount;
            occurrence.RecurrencePeriodicity = recurrenceInfo.Periodicity;
            occurrence.RecurrenceDayNumber = recurrenceInfo.DayNumber;
            occurrence.RecurrenceWeekDays = (int)recurrenceInfo.WeekDays;
            occurrence.RecurrenceWeekOfMonth = (int)recurrenceInfo.WeekOfMonth;
            occurrence.RecurrenceMonth = recurrenceInfo.Month;

            return occurrence;
        }
    }
}
