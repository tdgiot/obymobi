﻿using System;

[Serializable]
public class SchedulingAppointment
{
    public SchedulingAppointment()
    {

    }

    public int Id { get; set; }
    public int ReportProcessingTaskTemplateId { get; set; }
    public DateTime StartTime { get; set; }
    public DateTime EndTime { get; set; }
    public string Subject { get; set; }
    public int Status { get; set; }
    public string Description { get; set; }
    public long Label { get; set; }
    public string Location { get; set; }
    public bool AllDay { get; set; }
    public int Type { get; set; }
    public Guid RecurrenceId { get; set; }
    public string RecurrenceInfo { get; set; }
    public string ReminderInfo { get; set; }
    public int BackgroundColor { get; set; }
    public int TextColor { get; set; }
}