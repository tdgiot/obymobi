﻿using System;
using System.Collections;
using Obymobi.Cms.WebApplication.Old_App_Code.Scheduling;

public class SchedulingDataSource
{
    public SchedulingAppointmentHandler Handler { get; set; }
    public SchedulingAppointmentList Appointments { get; set; }
    public int Count => Appointments.Count;

    public SchedulingDataSource(SchedulingAppointmentHandler handler, 
                                SchedulingAppointmentList appointments)
    {
        this.Handler = handler;
        this.Appointments = appointments;
    }

    public SchedulingDataSource() : this(null, new SchedulingAppointmentList())
    {
        
    }

    public object InsertMethodHandler(SchedulingAppointment appointment)
    {
        appointment.Id = this.Handler.InsertAppointment(appointment);
        Appointments.Add(appointment);
        return appointment.Id;
    }

    public void UpdateMethodHandler(SchedulingAppointment appointment)
    {
        int eventIndex = Appointments.GetEventIndex(appointment.Id);
        if (eventIndex < 0)
            return;

        this.Handler.UpdateAppointment(appointment);
        this.Appointments.RemoveAt(eventIndex);
        this.Appointments.Insert(eventIndex, appointment);
    }

    public void DeleteMethodHandler(SchedulingAppointment appointment)
    {
        int eventIndex = Appointments.GetEventIndex(appointment.Id);
        if (eventIndex < 0)
            return;

        this.Handler.DeleteAppointment(appointment);
        this.Appointments.RemoveAt(eventIndex);
    }

    public IEnumerable SelectMethodHandler()
    {
        SchedulingAppointmentList result = new SchedulingAppointmentList();
        result.AddRange(this.Appointments);
        return result;
    }

    public object ObtainLastInsertedId()
    {
        if (Count < 1)
        {
            return null;
        }
            
        return Appointments[Count - 1].Id;
    }
}