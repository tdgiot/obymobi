﻿using DevExpress.Web.ASPxScheduler;
using DevExpress.Web.ASPxScheduler.Internal;
using DevExpress.XtraScheduler;
using System;

public class SchedulingAppointmentDeleteCallbackCommand : SchedulerCallbackCommand
{
    public const string CommandId = "USRAPTMENU";

    private string menuItemId = String.Empty;

    public SchedulingAppointmentDeleteCallbackCommand(ASPxScheduler control)
        : base(control)
    {
    }

    public override string Id => CommandId;
    public string MenuItemId => menuItemId;

    protected override void ParseParameters(string parameters)
    {
        this.menuItemId = parameters;
    }

    protected override void ExecuteCore()
    {
        if (this.MenuItemId != "DeleteId")
            return;

        Appointment apt = this.Control.SelectedAppointments[0];
        if (apt.RecurrencePattern != null)
        {
            apt.RecurrencePattern.Delete();
        }
        else
        {
            apt.Delete();
        }
    }
}