﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Cms.WebApplication.Old_App_Code.Scheduling
{
    public class SchedulingAppointmentHandler
    {
        private readonly int uiScheduleId;
        private readonly SchedulingAppointmentConverter converter;

        public SchedulingAppointmentHandler(int uiScheduleId,
                                            SchedulingAppointmentConverter converter)
        {
            this.uiScheduleId = uiScheduleId;
            this.converter = converter;
        }

        public int InsertAppointment(SchedulingAppointment appointment)
        {
            UIScheduleItemOccurrenceEntity occurrence = this.converter.Convert(appointment);
            occurrence.UIScheduleId = this.uiScheduleId;
            occurrence.Save();

            return occurrence.UIScheduleItemOccurrenceId;
        }

        public void DeleteAppointment(SchedulingAppointment appointment)
        {
            UIScheduleItemOccurrenceEntity occurrence = new UIScheduleItemOccurrenceEntity(appointment.Id);
            occurrence.Delete();
        }

        public void UpdateAppointment(SchedulingAppointment appointment)
        {
            UIScheduleItemOccurrenceEntity occurrence = this.converter.Convert(appointment);
            occurrence.Save();
        }
    }
}