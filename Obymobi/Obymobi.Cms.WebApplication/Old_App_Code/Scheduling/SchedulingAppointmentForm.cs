﻿using DevExpress.Web.ASPxScheduler;
using DevExpress.Web.ASPxScheduler.Internal;
using DevExpress.XtraScheduler;

public class SchedulingAppointmentForm : AppointmentFormTemplateContainer
{
    public SchedulingAppointmentForm(ASPxScheduler control)
        : base(control)
    {

    }

    public int ReportProcessingTaskTemplateId
    {
        get
        {
            object val = Appointment.CustomFields["ReportProcessingTaskTemplateId"];
            return (val == System.DBNull.Value) ? 0 : System.Convert.ToInt32(val);
        }
    }
    
    public int BackgroundColor
    {
        get
        {
            object val = Appointment.CustomFields["BackgroundColor"];
            return (val == System.DBNull.Value) ? 0 : System.Convert.ToInt32(val);
        }
    }

    public int TextColor
    {
        get
        {
            object val = Appointment.CustomFields["TextColor"];
            return (val == System.DBNull.Value) ? 0 : System.Convert.ToInt32(val);
        }
    }
}

public class SchedulingAppointmentFormController : AppointmentFormController
{
    public SchedulingAppointmentFormController(ASPxScheduler control, Appointment apt)
        : base(control, apt) 
    { 
    }

    public int ReportProcessingTaskTemplateId
    {
        get => (int)EditedAppointmentCopy.CustomFields["ReportProcessingTaskTemplateId"];
        set => this.EditedAppointmentCopy.CustomFields["ReportProcessingTaskTemplateId"] = value;
    }
    
    public int BackgroundColor
    {
        get => (int)EditedAppointmentCopy.CustomFields["BackgroundColor"];
        set => this.EditedAppointmentCopy.CustomFields["BackgroundColor"] = value;
    }

    public int TextColor
    {
        get => (int)EditedAppointmentCopy.CustomFields["TextColor"];
        set => this.EditedAppointmentCopy.CustomFields["TextColor"] = value;
    }

    public int SourceReportProcessingTaskTemplateId
    {
        get => (int)SourceAppointment.CustomFields["ReportProcessingTaskTemplateId"];
        set => SourceAppointment.CustomFields["ReportProcessingTaskTemplateId"] = value;
    }

    public int SourceBackgroundColor
    {
        get => (int)SourceAppointment.CustomFields["BackgroundColor"];
        set => SourceAppointment.CustomFields["BackgroundColor"] = value;
    }

    public int SourceTextColor
    {
        get => (int)SourceAppointment.CustomFields["TextColor"];
        set => SourceAppointment.CustomFields["TextColor"] = value;
    }
}