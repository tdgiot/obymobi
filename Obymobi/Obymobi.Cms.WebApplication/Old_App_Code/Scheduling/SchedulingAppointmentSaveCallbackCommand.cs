﻿using System;
using DevExpress.Web;
using DevExpress.Web.ASPxScheduler.Internal;
using DevExpress.Web.ASPxScheduler;
using DevExpress.XtraScheduler;

public class SchedulingAppointmentSaveCallbackCommand : AppointmentFormSaveCallbackCommand
{
    private const int DEFAULT_APPOINTMENT_MINUTES = 60;

    public SchedulingAppointmentSaveCallbackCommand(ASPxScheduler control) : base(control)
    {
    }

    protected internal new SchedulingAppointmentFormController Controller
    {
        get { return (SchedulingAppointmentFormController)base.Controller; }
    }

    protected override void AssignControllerValues()
    {
        ASPxComboBox cbReportProcessingTaskTemplateId = (ASPxComboBox)this.FindControlByID("cbReportProcessingTaskTemplateId");
        ASPxColorEdit ceBackgroundColor = (ASPxColorEdit)this.FindControlByID("ceBackgroundColor");
        ASPxColorEdit ceTextColor = (ASPxColorEdit)this.FindControlByID("ceTextColor");
        ASPxDateEdit deStart = (ASPxDateEdit)this.FindControlByID("edtStartDate");

        this.Controller.ReportProcessingTaskTemplateId = System.Convert.ToInt32(cbReportProcessingTaskTemplateId.Value);
        this.Controller.BackgroundColor = ceBackgroundColor.Color.ToArgb();
        this.Controller.TextColor = ceTextColor.Color.ToArgb();
        this.Controller.Subject = cbReportProcessingTaskTemplateId.Text;
        this.Controller.Start = deStart.Date;
        this.Controller.End = deStart.Date.AddMinutes(DEFAULT_APPOINTMENT_MINUTES);

        base.AssignControllerValues();
    }

    protected override AppointmentFormController CreateAppointmentFormController(Appointment apt)
    {
        return new SchedulingAppointmentFormController(this.Control, apt);
    }
}