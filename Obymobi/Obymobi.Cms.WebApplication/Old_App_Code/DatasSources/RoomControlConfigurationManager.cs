﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Data;
using Obymobi.Data;
using Obymobi.Logic.RoomControl;
using Obymobi.Enums;

/// <summary>
/// Summary description for RoomControlConfigurationManager
/// </summary>
public class RoomControlConfigurationManager
{
    private readonly int roomControlConfigurationId;
    private RoomControlConfigurationEntity configurationEntity;

    public RoomControlConfigurationManager(int roomControlConfigurationId)
    {
        this.roomControlConfigurationId = roomControlConfigurationId;
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource.OrderBy(x => x.SortOrder).ToList();
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string name, string nameSystem, string itemType, int type, string itemId)
    {
        if (itemType == RoomControlItemType.Area)
        {
            IEnumerable<IRoomControlItem> existingAreas = this.DataSource.Where(x => x.ItemType == RoomControlItemType.Area);

            RoomControlAreaEntity area = new RoomControlAreaEntity();
            area.ValidatorCreateOrUpdateDefaultAreaLanguage = true;
            area.RoomControlConfigurationId = this.roomControlConfigurationId;
            area.Name = name;
            area.Type = (RoomControlAreaType)type;
            area.NameSystem = nameSystem;
            area.SortOrder = existingAreas.Any() ? (existingAreas.Max(x => x.SortOrder) + 1) : 1;
            area.Save();

            this.DataSource.Add(area);
        }
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string name, string nameSystem, string itemType, int type, string itemId, string parentItemId)
    {
        IEnumerable<IRoomControlItem> existingSiblingItems = this.DataSource.Where(x => x.ParentItemId == parentItemId);
        if (parentItemId.Length == 0)
        {
            IEnumerable<IRoomControlItem> existingAreas = this.DataSource.Where(x => x.ItemType == RoomControlItemType.Area);

            RoomControlAreaEntity area = new RoomControlAreaEntity();
            area.ValidatorCreateOrUpdateDefaultAreaLanguage = true;
            area.RoomControlConfigurationId = this.roomControlConfigurationId;
            area.Name = name;
            area.Type = (RoomControlAreaType)type;
            area.NameSystem = nameSystem;
            area.SortOrder = existingAreas.Any() ? (existingAreas.Max(x => x.SortOrder) + 1) : 1;
            area.Save();

            this.DataSource.Add(area);
        }
        else
        {
            IRoomControlItem parentItem = this.DataSource.FirstOrDefault(x => x.ItemId == parentItemId);

            int newSortOrder = 1;
            if (existingSiblingItems.Count() > 0)
            {
                newSortOrder = existingSiblingItems.Max(x => x.SortOrder) + 1;
            }

            if (parentItem is RoomControlAreaEntity)
            {
                RoomControlSectionEntity section = new RoomControlSectionEntity();
                section.ValidatorCreateOrUpdateDefaultSectionLanguage = true;
                section.RoomControlAreaId = (parentItem as RoomControlAreaEntity).RoomControlAreaId;
                section.Name = name;
                section.Type = (RoomControlSectionType)type;
                section.SortOrder = newSortOrder;
                section.Save();

                this.DataSource.Add(section);
            }
            else if (parentItem is RoomControlSectionEntity)
            {
                if (itemType == RoomControlItemType.Component)
                {
                    RoomControlComponentEntity component = new RoomControlComponentEntity();
                    component.ValidatorCreateOrUpdateDefaultComponentLanguage = true;
                    component.RoomControlSectionId = (parentItem as RoomControlSectionEntity).RoomControlSectionId;
                    component.Name = name;
                    component.NameSystem = nameSystem;
                    component.Type = (RoomControlComponentType)type;
                    component.SortOrder = newSortOrder;
                    component.Save();

                    this.DataSource.Add(component);
                }
                else if (itemType == RoomControlItemType.SectionItem)
                {
                    RoomControlSectionItemEntity sectionItem = new RoomControlSectionItemEntity();
                    sectionItem.ValidatorCreateOrUpdateDefaultSectionItemLanguage = true;
                    sectionItem.RoomControlSectionId = (parentItem as RoomControlSectionEntity).RoomControlSectionId;
                    sectionItem.Name = name;
                    sectionItem.Type = (RoomControlSectionItemType)type;
                    sectionItem.SortOrder = newSortOrder;
                    sectionItem.Save();

                    this.DataSource.Add(sectionItem);
                }
                else if (itemType == RoomControlItemType.Widget)
                {
                    RoomControlWidgetEntity widget = new RoomControlWidgetEntity();
                    widget.ValidatorCreateOrUpdateDefaultWidgetLanguage = true;
                    widget.RoomControlSectionId = (parentItem as RoomControlSectionEntity).RoomControlSectionId;
                    widget.Caption = name;
                    widget.Type = (RoomControlWidgetType)type;
                    widget.SortOrder = newSortOrder;
                    widget.Save();

                    this.DataSource.Add(widget);
                }
            }
            else if (parentItem is RoomControlSectionItemEntity)
            {
                if (itemType == RoomControlItemType.Widget)
                {
                    RoomControlWidgetEntity widget = new RoomControlWidgetEntity();
                    widget.ValidatorCreateOrUpdateDefaultWidgetLanguage = true;
                    widget.RoomControlSectionItemId = (parentItem as RoomControlSectionItemEntity).RoomControlSectionItemId;
                    widget.Caption = name;
                    widget.Type = (RoomControlWidgetType)type;
                    widget.SortOrder = newSortOrder;
                    widget.Save();

                    this.DataSource.Add(widget);
                }
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string name, string nameSystem, string itemType, int type, string itemId)
    {
        IRoomControlItem item = this.DataSource.FirstOrDefault(x => x.ItemId == itemId);
        if (itemType == RoomControlItemType.Area)
        {
            RoomControlAreaEntity area = (RoomControlAreaEntity)item;
            if (area != null)
            {
                area.ValidatorCreateOrUpdateDefaultAreaLanguage = true;
                area.Name = name;
                area.NameSystem = nameSystem;
                area.Type = (RoomControlAreaType)type;
                area.Save();
            }
        }
        else if (itemType == RoomControlItemType.Section)
        {
            RoomControlSectionEntity section = (RoomControlSectionEntity)item;
            if (section != null)
            {
                section.ValidatorCreateOrUpdateDefaultSectionLanguage = true;
                section.Name = name;
                section.NameSystem = nameSystem;
                section.Type = (RoomControlSectionType)type;
                section.Save();
            }
        }
        else if (itemType == RoomControlItemType.Component)
        {
            RoomControlComponentEntity component = (RoomControlComponentEntity)item;
            if (component != null)
            {
                component.ValidatorCreateOrUpdateDefaultComponentLanguage = true;
                component.Name = name;
                component.NameSystem = nameSystem;
                component.Type = (RoomControlComponentType)type;
                component.Save();
            }
        }
        else if (itemType == RoomControlItemType.SectionItem)
        {
            RoomControlSectionItemEntity sectionItem = (RoomControlSectionItemEntity)item;
            if (sectionItem != null)
            {
                sectionItem.ValidatorCreateOrUpdateDefaultSectionItemLanguage = true;
                sectionItem.Name = name;
                sectionItem.Type = (RoomControlSectionItemType)type;
                sectionItem.Save();
            }
        }
        else if (itemType == RoomControlItemType.Widget)
        {
            RoomControlWidgetEntity widget = (RoomControlWidgetEntity)item;
            if (widget != null)
            {
                widget.ValidatorCreateOrUpdateDefaultWidgetLanguage = true;
                widget.Caption = name;
                widget.Type = (RoomControlWidgetType)type;
                widget.Save();
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string parentItemId, string itemId)
    {
        IRoomControlItem item = this.DataSource.FirstOrDefault(x => x.ItemId == itemId);
        IRoomControlItem parentItem = this.DataSource.FirstOrDefault(x => x.ItemId == parentItemId);
        if(item.ItemType == parentItem.ItemType)
        {
            List<IRoomControlItem> sortedItems = this.DataSource.Where(x => x.ParentItemId == parentItem.ParentItemId).OrderBy(x => x.SortOrder).ToList();

            sortedItems.Remove(item);
            int parentItemIndex = sortedItems.IndexOf(parentItem);
            if(item.SortOrder < parentItem.SortOrder)
            {
                sortedItems.Insert(parentItemIndex + 1, item);
            }
            else
            {
                sortedItems.Insert(parentItemIndex, item);
            }

            int orderNo = 1;
            foreach (IRoomControlItem sortedItem in sortedItems)
            {
                sortedItem.SortOrder = orderNo++;
                sortedItem.Save();
            }
        }
        else
        {
            if (item.ItemType == RoomControlItemType.Section && parentItem.ItemType == RoomControlItemType.Area)
            {
                RoomControlSectionEntity section = (RoomControlSectionEntity)item;
                section.RoomControlAreaId = (parentItem as RoomControlAreaEntity).RoomControlAreaId;
                section.Save();
            }
            else if (item.ItemType == RoomControlItemType.Component && parentItem.ItemType == RoomControlItemType.Section)
            {
                RoomControlComponentEntity component = (RoomControlComponentEntity)item;
                component.RoomControlSectionId = (parentItem as RoomControlSectionEntity).RoomControlSectionId;
                component.Save();
            }
            else if (item.ItemType == RoomControlItemType.SectionItem && parentItem.ItemType == RoomControlItemType.Section)
            {
                RoomControlSectionItemEntity sectionItem = (RoomControlSectionItemEntity)item;
                sectionItem.RoomControlSectionId = (parentItem as RoomControlSectionEntity).RoomControlSectionId;
                sectionItem.Save();
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(string itemId)
    {
        IRoomControlItem item = this.DataSource.FirstOrDefault(x => x.ItemId == itemId);
        if (item != null)
        {
            this.DataSource.Remove(item);
            item.Delete();
        }
    }

    private List<IRoomControlItem> dataSource;
    private List<IRoomControlItem> DataSource
    {
        get
        {
            if (this.dataSource == null)
            {
                this.dataSource = new List<IRoomControlItem>();

                PrefetchPath path = new PrefetchPath(EntityType.RoomControlConfigurationEntity);
                IPrefetchPathElement roomAreasPath = path.Add(RoomControlConfigurationEntityBase.PrefetchPathRoomControlAreaCollection);
                IPrefetchPathElement roomSectionsPath = roomAreasPath.SubPath.Add(RoomControlAreaEntityBase.PrefetchPathRoomControlSectionCollection);
                IPrefetchPathElement roomComponentsPath = roomSectionsPath.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathRoomControlComponentCollection);
                IPrefetchPathElement roomSectionItemsPath = roomSectionsPath.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathRoomControlSectionItemCollection);
                IPrefetchPathElement sectionWidgetsPath = roomSectionsPath.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathRoomControlWidgetCollection);                

                this.configurationEntity = new RoomControlConfigurationEntity(this.roomControlConfigurationId, path);
                if (!this.configurationEntity.IsNew)
                {
                    this.dataSource.AddRange(this.configurationEntity.RoomControlAreaCollection);
                    foreach (RoomControlAreaEntity areaEntity in this.configurationEntity.RoomControlAreaCollection)
                    {
                        this.dataSource.AddRange(areaEntity.RoomControlSectionCollection);
                        foreach (RoomControlSectionEntity sectionEntity in areaEntity.RoomControlSectionCollection)
                        {
                            this.dataSource.AddRange(sectionEntity.RoomControlComponentCollection);
                            this.dataSource.AddRange(sectionEntity.RoomControlSectionItemCollection);
                            this.dataSource.AddRange(sectionEntity.RoomControlWidgetCollection);                            
                        }
                    }
                }
            }

            return this.dataSource;
        }
    }
}