﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;

public class InfraredCommandManager
{
    public int InfraredConfigurationId { get; set; }
    public InfraredConfigurationEntity InfraredConfigurationEntity { get; set; }
    public List<InfraredCommandEntity> DataSource { get; set; } = new List<InfraredCommandEntity>();

    private InfraredCommandManager(int infraredConfigurationId)
    {
        this.InfraredConfigurationId = infraredConfigurationId;
        this.RefreshDataSource(); 
    }

    public static InfraredCommandManager Create(int infraredConfigurationId) 
    {
        return new InfraredCommandManager(infraredConfigurationId);
    }    

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource.OrderBy(x => x.Type);
    }       

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(int infraredCommandId, string typeStringValue, string name, string hex)
    {
        InfraredCommandEntity command = this.DataSource.FirstOrDefault(x => x.InfraredCommandId == infraredCommandId);
        if (command != null)
        {
            command.Name = name;
            command.Hex = hex;
            command.Save();
            this.RefreshDataSource();
        }
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(int infraredCommandId)
    {
        InfraredCommandEntity entity = this.DataSource.FirstOrDefault(x => x.InfraredCommandId == infraredCommandId);
        if (entity == null)
        {
            return;
        }

        this.DataSource.Remove(entity);
        entity.Delete();
    }

    public void RefreshDataSource()
    {
        if (this.InfraredConfigurationId > 0)
        {
            PredicateExpression filter = new PredicateExpression(InfraredCommandFields.InfraredConfigurationId == this.InfraredConfigurationId);

            InfraredCommandCollection commands = new InfraredCommandCollection();
            commands.GetMulti(filter);

            this.DataSource = commands.ToList();
        }        
    }
}