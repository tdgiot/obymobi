﻿using Obymobi.Data.EntityClasses;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public class StationDataSource : ObjectDataSource
{
    public readonly StationManager Manager;    

    public StationDataSource(int stationListId)
    {
        this.TypeName = typeof(StationManager).FullName;
        this.Manager = StationManager.Create(stationListId);

        this.UpdateMethod = "Update";
        this.SelectMethod = "Select";
        this.InsertMethod = "Insert";
        this.DeleteMethod = "Delete";

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.Manager;
    }

    public List<StationEntity> DataSource { get { return this.Manager != null ? this.Manager.DataSource : new List<StationEntity>(); } }
}