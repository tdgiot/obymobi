﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Google.Apis.Manual.Util;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;

public class CarouselItemManager
{
    public WidgetCarouselEntity WidgetCarouselEntity { get; set; }
    public List<CarouselItemEntity> DataSource { get; set; } = new List<CarouselItemEntity>();

    private CarouselItemManager(int widgetCarouselId)
    {
        this.WidgetCarouselEntity = new WidgetCarouselEntity(widgetCarouselId);
        if (this.WidgetCarouselEntity.IsNew)
        {
            throw new System.Exception("Unknown widget carousel");
        }

        this.RefreshDataSource();
    }

    public static CarouselItemManager Create(int widgetCarouselId)
    {
        return new CarouselItemManager(widgetCarouselId);
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource.OrderBy(x => x.SortOrder);
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(int carouselItemId, string name, string message)
    {
        if (name.IsNullOrEmpty())
        {
            throw new System.Exception("Please enter a name");
        }

        CarouselItemEntity entity = new CarouselItemEntity();
        entity.WidgetCarouselId = this.WidgetCarouselEntity.WidgetId;
        entity.ParentCompanyId = this.WidgetCarouselEntity.ParentCompanyId;
        entity.Name = name;
        entity.Message = message;
        entity.SortOrder = this.DataSource.Any() ? (this.DataSource.Max(x => x.SortOrder) + 1) : 1;
        entity.Save();        

        this.DataSource.Add(entity);
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(int carouselItemId, string name, string message)
    {
        if (name.IsNullOrEmpty())
        {
            throw new System.Exception("Please enter a name");
        }

        CarouselItemEntity entity = this.DataSource.FirstOrDefault(x => x.CarouselItemId == carouselItemId);
        if (entity == null)
        {
            return;
        }

        entity.Name = name;
        entity.Message = message;
        entity.Save();
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(int carouselItemId)
    {
        CarouselItemEntity entity = this.DataSource.FirstOrDefault(x => x.CarouselItemId == carouselItemId);
        if (entity == null)
        {
            return;
        }

        this.DataSource.Remove(entity);
        entity.Delete();
    }

    public void DraggedToSort(int draggedCarouselItemId, int draggedUponCarouselItemId)
    {
        CarouselItemEntity draggedEntity = this.DataSource.FirstOrDefault(x => x.CarouselItemId == draggedCarouselItemId);
        CarouselItemEntity draggedUponEntity = this.DataSource.FirstOrDefault(x => x.CarouselItemId == draggedUponCarouselItemId);

        if (draggedEntity == null || draggedUponEntity == null)
        {
            return;
        }

        bool draggedDown = false;
        if (draggedEntity.SortOrder < draggedUponEntity.SortOrder)
        {
            // It was above it, so it's dragged down
            draggedDown = true;
        }

        // Put the items in the correct order - then renumber the whole lot
        List<CarouselItemEntity> items = this.DataSource.OrderBy(x => x.SortOrder).ToList();
        items.Remove(draggedEntity);

        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] == draggedUponEntity)
            {
                if (draggedDown)
                {
                    // Place it below this item
                    items.Insert(i + 1, draggedEntity);
                }
                else
                {
                    // Place it in front of this item.
                    items.Insert(i, draggedEntity);
                }
                break;
            }
        }

        int orderNo = 1;
        foreach (CarouselItemEntity entity in items)
        {
            entity.SortOrder = orderNo;
            entity.Save();
            orderNo++;
        }
    }

    public void RefreshDataSource()
    {
        PredicateExpression filter = new PredicateExpression(CarouselItemFields.WidgetCarouselId == this.WidgetCarouselEntity.WidgetId);

        CarouselItemCollection carouselItemCollection = new CarouselItemCollection();
        carouselItemCollection.GetMulti(filter);

        this.DataSource = carouselItemCollection.ToList();
    }        
}