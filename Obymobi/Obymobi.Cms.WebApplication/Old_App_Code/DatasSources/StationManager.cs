﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Google.Apis.Manual.Util;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;

public class StationManager
{
    public int StationListId { get; set; }
    public List<StationEntity> DataSource { get; set; } = new List<StationEntity>();

    private StationManager(int stationListId)
    {
        this.StationListId = stationListId;
        this.RefreshDataSource();
    }

    public static StationManager Create(int stationListId)
    {
        return new StationManager(stationListId);
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource.OrderBy(x => x.SortOrder);
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(int stationId, string name, string scene, string channel)
    {
        if (name.IsNullOrEmpty() || channel.IsNullOrEmpty())
        {
            throw new System.Exception("Please enter a name and channel");
        }

        StationEntity entity = new StationEntity();
        entity.StationListId = this.StationListId;
        entity.Name = name;
        entity.Scene = scene;
        entity.Channel = channel;
        entity.SortOrder = this.DataSource.Any() ? (this.DataSource.Max(x => x.SortOrder) + 1) : 1;
        entity.Save();            

        this.DataSource.Add(entity);
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(int stationId, string name, string scene, string channel)
    {
        if (name.IsNullOrEmpty() || channel.IsNullOrEmpty())
        {
            throw new System.Exception("Please enter a name and channel");
        }

        StationEntity entity = this.DataSource.FirstOrDefault(x => x.StationId == stationId);
        if (entity == null)
        {
            return;
        }

        entity.Name = name;
        entity.Scene = scene;
        entity.Channel = channel;        
        entity.Save();
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(int stationId)
    {
        StationEntity entity = this.DataSource.FirstOrDefault(x => x.StationId == stationId);
        if (entity == null)
        {
            return;
        }

        this.DataSource.Remove(entity);
        entity.Delete();
    }

    public void DraggedToSort(int draggedStationId, int draggedUponStationId)
    {
        StationEntity draggedEntity = this.DataSource.FirstOrDefault(x => x.StationId == draggedStationId);
        StationEntity draggedUponEntity = this.DataSource.FirstOrDefault(x => x.StationId == draggedUponStationId);

        if (draggedEntity == null || draggedUponEntity == null)
        {
            return;
        }

        bool draggedDown = false;
        if (draggedEntity.SortOrder < draggedUponEntity.SortOrder)
        {
            // It was above it, so it's dragged down
            draggedDown = true;
        }

        // Put the items in the correct order - then renumber the whole lot
        List<StationEntity> stations = this.DataSource.OrderBy(x => x.SortOrder).ToList();
        stations.Remove(draggedEntity);

        for (int i = 0; i < stations.Count; i++)
        {
            if (stations[i] == draggedUponEntity)
            {
                if (draggedDown)
                {
                    // Place it below this item
                    stations.Insert(i + 1, draggedEntity);
                }
                else
                {
                    // Place it in front of this item.
                    stations.Insert(i, draggedEntity);
                }
                break;
            }
        }

        int orderNo = 1;
        foreach (StationEntity entity in stations)
        {
            entity.SortOrder = orderNo;
            entity.Save();
            orderNo++;
        }
    }

    public void RefreshDataSource()
    {
        if (this.StationListId <= 0)
            return;
        
        PredicateExpression filter = new PredicateExpression(StationFields.StationListId == this.StationListId);

        StationCollection stationCollection = new StationCollection();
        stationCollection.GetMulti(filter);

        this.DataSource = stationCollection.ToList();
    }        
}