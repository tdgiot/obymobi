﻿using Obymobi.Data.EntityClasses;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public class InfraredCommandDataSource : ObjectDataSource
{
    private readonly InfraredCommandManager Manager;    

    public InfraredCommandDataSource(int infraredConfigurationId)
    {
        this.TypeName = typeof(InfraredCommandManager).FullName;
        this.Manager = InfraredCommandManager.Create(infraredConfigurationId);

        this.UpdateMethod = "Update";
        this.SelectMethod = "Select";
        this.DeleteMethod = "Delete";

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.Manager;
    }

    public InfraredConfigurationEntity InfraredConfigurationEntity { get { return this.Manager.InfraredConfigurationEntity; } }
    public List<InfraredCommandEntity> DataSource { get { return this.Manager != null ? this.Manager.DataSource : new List<InfraredCommandEntity>(); } }
}