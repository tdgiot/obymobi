﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Summary description for OptionConverter
/// </summary>
public class OptionConverter
{
    private int Version = 1; 
     
    public List<Option> Options;

    public OptionConverter(int version)
    {
        this.Version = version;
        this.Options = new List<Option>();
    }    

    public void ConvertChildrenFromProduct(ProductEntity product, Option parentOption)
    {
        if (product.ProductgroupId > 0)
        {
            this.ConvertProductgroup(product.ProductgroupEntity, parentOption);
        }

        if (product.ProductAlterationCollection.Count > 0)
        {
            this.ConvertProductAlterations(product.ProductAlterationCollection, parentOption);
        }        
    }

    public void ConvertProductgroup(ProductgroupEntity productgroup, Option parentOption)
    {
        if (this.Version > 1)
        {
            string parentOptionId = parentOption != null ? parentOption.OptionId : string.Empty;
            bool parentLinked = parentOption != null ? parentOption.LinkedMultipleEntities : false;

            Option option = new Option
            {
                OptionId = string.Format("{0}-productgroup-{1}", parentOptionId, productgroup.ProductgroupId),
                ParentOptionId = parentOptionId,
                Name = productgroup.Name,
                SortOrder = 0,
                Entity = productgroup,
                LinkedMultipleEntities = parentLinked ? parentLinked : this.IsProductgroupLinkedMultipleTimes(productgroup)
            };
            this.AddOption(option, parentOption);            
            this.ConvertProductgroupItems(productgroup.ProductgroupItemCollection, option);
        }        
    }

    public void ConvertProductgroupItems(ProductgroupItemCollection items, Option parentOption)
    {
        foreach (ProductgroupItemEntity item in items.OrderBy(x => x.SortOrder))
        {
            this.ConvertProductgroupItem(item, parentOption);
        }
    }

    public void ConvertProductgroupItem(ProductgroupItemEntity item, Option parentOption)
    {
        string parentOptionId = parentOption != null ? parentOption.OptionId : string.Empty;
        bool parentLinked = parentOption != null ? parentOption.LinkedMultipleEntities : false;

        if (item.ProductId.GetValueOrDefault(0) > 0)
        {
            Option option = new Option
            {
                OptionId = string.Format("{0}-ProductgroupItem-{1}", parentOptionId, item.ProductgroupItemId),
                ParentOptionId = parentOptionId,
                Name = item.ProductEntity.Name,
                SortOrder = item.SortOrder,
                Entity = item.ProductEntity,
                LinkedEntity = item,
                SubType = item.ProductEntity.Type,
                LinkedMultipleEntities = parentLinked ? parentLinked : this.IsProductLinkedMultipleTimes(item.ProductEntity)
            };
            this.AddOption(option, parentOption);            
            this.ConvertChildrenFromProduct(item.ProductEntity, option);
        }
        else if (item.NestedProductgroupId.GetValueOrDefault(0) > 0)
        {
            Option option = new Option
            {
                OptionId = string.Format("{0}-ProductgroupItem-{1}", parentOptionId, item.ProductgroupItemId),
                ParentOptionId = parentOptionId,
                Name = item.ProductgroupEntity1.Name,
                SortOrder = item.SortOrder,
                Entity = item.ProductgroupEntity1,
                LinkedEntity = item,
                LinkedMultipleEntities = parentLinked ? parentLinked : this.IsProductgroupLinkedMultipleTimes(item.ProductgroupEntity1)
            };
            this.AddOption(option, parentOption);
            this.ConvertProductgroupItems(item.ProductgroupEntity1.ProductgroupItemCollection, option);
        }        
    }    

    private void ConvertProductAlterations(ProductAlterationCollection productAlterations, Option parentOption)
    {
        foreach (ProductAlterationEntity productAlteration in productAlterations.Where(x => x.Version == this.Version).OrderBy(x => x.SortOrder))
        {
            this.ConvertProductAlteration(productAlteration, parentOption);
        }
    }    

    public void ConvertProductAlteration(ProductAlterationEntity productAlteration, Option parentOption)
    {
        string parentOptionId = parentOption != null ? parentOption.OptionId : string.Empty;
        bool parentLinked = parentOption != null ? parentOption.LinkedMultipleEntities : false;

        Option option = new Option
        {
            OptionId = string.Format("{0}-ProductAlteration-{1}", parentOptionId, productAlteration.ProductAlterationId),
            ParentOptionId = parentOptionId,
            Name = productAlteration.AlterationEntity.Name,
            SortOrder = productAlteration.SortOrder,
            Entity = productAlteration.AlterationEntity,
            LinkedEntity = productAlteration,
            SubType = (int)productAlteration.AlterationEntity.Type,
            LinkedMultipleEntities = parentLinked ? parentLinked : this.IsAlterationLinkedMultipleTimes(productAlteration.AlterationEntity)
        };
        this.AddOption(option, parentOption);
        this.ConvertAlterationitems(productAlteration.AlterationEntity.AlterationitemCollection, option);
        if (this.Version == 1)
        {
            this.ConvertAlterations(productAlteration.AlterationEntity.AlterationCollection, option);
        }
    }    

    public void ConvertAlterationitems(AlterationitemCollection items, Option parentOption)
    {
        foreach (AlterationitemEntity item in items.Where(x => x.Version == this.Version).OrderBy(x => x.SortOrder))
        {
            this.ConvertAlterationitem(item, parentOption);
        }
    }

    public void ConvertAlterationitem(AlterationitemEntity alterationitem, Option parentOption)
    {
        if (alterationitem.AlterationoptionEntity != null)
        {
            string parentOptionId = parentOption != null ? parentOption.OptionId : string.Empty;
            bool parentLinked = parentOption != null ? parentOption.LinkedMultipleEntities : false;

            Option option = new Option
            {
                OptionId = string.Format("{0}-Alterationitem-{1}", parentOptionId, alterationitem.AlterationitemId),
                ParentOptionId = parentOptionId,
                Name = alterationitem.AlterationoptionEntity.Name,
                SortOrder = alterationitem.SortOrder,
                Entity = alterationitem.AlterationoptionEntity,
                LinkedEntity = alterationitem,
                SubType = alterationitem.AlterationoptionEntity.Type,
                LinkedMultipleEntities = parentLinked ? parentLinked : this.IsAlterationoptionLinkedMultipleTimes(alterationitem.AlterationoptionEntity)
            };
            this.AddOption(option, parentOption);
            if (this.Version > 1)
            {
                this.ConvertAlterationitemAlterations(alterationitem.AlterationitemAlterationCollection, option);
            }
        }
    }

    public void ConvertAlterations(AlterationCollection alterations, Option parentOption)
    {
        foreach (AlterationEntity alteration in alterations.OrderBy(x => x.SortOrder))
        {
            this.ConvertAlteration(alteration, parentOption);
        }
    }

    public void ConvertAlteration(AlterationEntity alteration, Option parentOption)
    {
        string parentOptionId = parentOption != null ? parentOption.OptionId : string.Empty;
        bool parentLinked = parentOption != null ? parentOption.LinkedMultipleEntities : false;

        Option option = new Option
        {
            OptionId = string.Format("{0}-Alteration-{1}", parentOptionId, alteration.AlterationId),
            ParentOptionId = parentOptionId,
            Name = alteration.Name,
            SortOrder = alteration.SortOrder,
            Entity = alteration,            
            SubType = (int)alteration.Type,
            LinkedMultipleEntities = parentLinked ? parentLinked : this.IsAlterationLinkedMultipleTimes(alteration)
        };
        this.AddOption(option, parentOption);
        this.ConvertAlterations(alteration.AlterationCollection, option);
        this.ConvertAlterationProducts(alteration.AlterationProductCollection, option);
    }

    public void ConvertAlterationProducts(AlterationProductCollection alterationProducts, Option parentOption)
    {
        foreach (AlterationProductEntity alterationProduct in alterationProducts.OrderBy(x => x.SortOrder))
        {
            this.ConvertAlterationProduct(alterationProduct, parentOption);
        }
    }

    public void ConvertAlterationProduct(AlterationProductEntity alterationProduct, Option parentOption)
    {
        string parentOptionId = parentOption != null ? parentOption.OptionId : string.Empty;
        bool parentLinked = parentOption != null ? parentOption.LinkedMultipleEntities : false;

        Option option = new Option
        {
            OptionId = string.Format("{0}-AlterationProduct-{1}", parentOptionId, alterationProduct.AlterationProductId),
            ParentOptionId = parentOptionId,
            Name = alterationProduct.ProductEntity.Name,
            SortOrder = alterationProduct.SortOrder,
            Entity = alterationProduct.ProductEntity,
            LinkedEntity = alterationProduct,
            SubType = alterationProduct.ProductEntity.Type,
            LinkedMultipleEntities = parentLinked ? parentLinked : this.IsProductLinkedMultipleTimes(alterationProduct.ProductEntity)
        };
        this.AddOption(option, parentOption);
        this.ConvertChildrenFromProduct(alterationProduct.ProductEntity, option);
    }

    public void ConvertAlterationitemAlterations(AlterationitemAlterationCollection alterationitemAlterations, Option parentOption)
    {
        foreach (AlterationitemAlterationEntity alterationitemAlteration in alterationitemAlterations.OrderBy(x => x.SortOrder))
        {
            this.ConvertAlterationitemAlteration(alterationitemAlteration, parentOption);
        }
    }

    public void ConvertAlterationitemAlteration(AlterationitemAlterationEntity alterationitemAlteration, Option parentOption)
    {
        if (alterationitemAlteration.AlterationEntity != null)
        {
            string parentOptionId = parentOption != null ? parentOption.OptionId : string.Empty;
            bool parentLinked = parentOption != null ? parentOption.LinkedMultipleEntities : false;

            Option option = new Option
            {
                OptionId = string.Format("{0}-AlterationitemAlterationId-{1}", parentOptionId, alterationitemAlteration.AlterationitemAlterationId),
                ParentOptionId = parentOptionId,
                Name = alterationitemAlteration.AlterationEntity.Name,
                SortOrder = alterationitemAlteration.SortOrder,
                Entity = alterationitemAlteration.AlterationEntity,
                LinkedEntity = alterationitemAlteration,
                SubType = (int)alterationitemAlteration.AlterationEntity.Type,
                LinkedMultipleEntities = parentLinked ? parentLinked : this.IsAlterationLinkedMultipleTimes(alterationitemAlteration.AlterationEntity)
            };
            this.AddOption(option, parentOption);
            this.ConvertAlterationitems(alterationitemAlteration.AlterationEntity.AlterationitemCollection, option);
        }
    }

    private void ConvertProductSuggestions(ProductSuggestionCollection productSuggestions, Option parentOption)
    {
        foreach (ProductSuggestionEntity productSuggestion in productSuggestions.OrderBy(x => x.SortOrder))
        {
            this.ConvertProductSuggestion(productSuggestion, parentOption);
        }
    }

    private void ConvertProductSuggestion(ProductSuggestionEntity productSuggestion, Option parentOption)
    {
        string parentOptionId = parentOption != null ? parentOption.OptionId : string.Empty;        

        Option option = new Option
        {
            OptionId = string.Format("{0}-ProductSuggestion-{1}", parentOptionId, productSuggestion.ProductSuggestionId),
            ParentOptionId = parentOptionId,
            Name = productSuggestion.SuggestedProductEntity.Name,
            SortOrder = productSuggestion.SortOrder,
            Entity = productSuggestion.SuggestedProductEntity,
            LinkedEntity = productSuggestion,
            SubType = productSuggestion.SuggestedProductEntity.Type
        };
        this.AddOption(option, parentOption);
        this.ConvertChildrenFromProduct(productSuggestion.SuggestedProductEntity, option);
    }

    private void AddOption(Option option, Option parentOption)
    {
        this.Options.Add(option);
        if (parentOption != null)
        {
            parentOption.HasChildren = true;
        }
    }

    private bool IsProductgroupLinkedMultipleTimes(ProductgroupEntity productgroup)
    {
        int count = 0;
        count += productgroup.ProductgroupItemCollection1.Count;
        count += productgroup.ProductCollection.Count;
        return count > 1;
    }

    private bool IsProductLinkedMultipleTimes(ProductEntity product)
    {
        int count = 0;
        count += product.AlterationProductCollection.Count;
        count += product.ProductgroupItemCollection.Count;                
        return count > 1;
    }

    private bool IsAlterationLinkedMultipleTimes(AlterationEntity alteration)
    {
        int count = 0;
        count += alteration.ProductAlterationCollection.Count;
        count += alteration.AlterationitemAlterationCollection.Count;
        if (alteration.ParentAlterationId > 0)
            count += 1;
        return count > 1;
    }

    private bool IsAlterationoptionLinkedMultipleTimes(AlterationoptionEntity alterationoption)
    {
        int count = 0;
        count += alterationoption.AlterationitemCollection.Count;
        return count > 1;
    }
}