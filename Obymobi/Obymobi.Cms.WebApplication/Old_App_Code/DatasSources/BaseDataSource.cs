﻿using System.Web.UI.WebControls;

/// <summary>
/// Summary description for BaseDataSource
/// </summary>
public class BaseDataSource<T> : ObjectDataSource
{
    private readonly T dataSourceManager;

    public BaseDataSource(T manager)
    {              
        this.TypeName = typeof(T).FullName;
        this.dataSourceManager = manager;

        // Create interface to get sort of typed safety
        this.InsertMethod = "Insert";
        this.UpdateMethod = "Update";
        this.DeleteMethod = "Delete";
        this.SelectMethod = "Select";
        this.OldValuesParameterFormatString = "sheit_{0}";

        this.ObjectCreating += PageDataSource_ObjectCreating;     
    }

    void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = dataSourceManager;
    }
}