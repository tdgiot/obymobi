﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

/// <summary>
/// This navigation menu manager belong to AppLess.
/// </summary>
public class NavigationMenuManager
{
    #region Constructors

    private NavigationMenuManager(int navigationMenuId)
    {
        this.NavigationMenuId = navigationMenuId;
        this.RefreshDataSource();
    }

    #endregion

    #region Properties

    public int NavigationMenuId { get; set; }

    public List<NavigationMenuItemEntity> DataSource { get; set; } = new List<NavigationMenuItemEntity>();

    #endregion

    #region Methods

    public static NavigationMenuManager Create(int navigationMenuId)
    {
        return new NavigationMenuManager(navigationMenuId);
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource.OrderBy(x => x.SortOrder);
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(int navigationMenuItemId, string text)
    {
        if (text.IsNullOrWhiteSpace())
        {
            throw new System.Exception("Please enter a text value for the navigation item.");
        }

        NavigationMenuItemEntity entity = new NavigationMenuItemEntity();
        entity.NavigationMenuId = this.NavigationMenuId;
        entity.Text = text;
        entity.SortOrder = this.DataSource.Any() 
            ? (this.DataSource.Max(x => x.SortOrder) + 1) 
            : 1;
        entity.Save();

        this.DataSource.Add(entity);
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(int navigationMenuItemId, string text)
    {
        if (text.IsNullOrWhiteSpace())
        {
            throw new System.Exception("Please enter a text value for the navigation item.");
        }

        NavigationMenuItemEntity entity = this.DataSource.FirstOrDefault(x => x.NavigationMenuItemId == navigationMenuItemId);
        if (entity == null)
        {
            return;
        }

        entity.Text = text;
        entity.Save();
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(int navigationMenuItemId)
    {
        NavigationMenuItemEntity entity = this.DataSource.FirstOrDefault(x => x.NavigationMenuItemId == navigationMenuItemId);
        if (entity == null)
        {
            return;
        }

        this.DataSource.Remove(entity);
        entity.Delete();
    }

    public void DraggedToSort(int draggedNavigationMenuItemId, int draggedUponNavigationMenuItemId)
    {
        NavigationMenuItemEntity draggedEntity = this.DataSource.FirstOrDefault(x => x.NavigationMenuItemId == draggedNavigationMenuItemId);
        NavigationMenuItemEntity draggedUponEntity = this.DataSource.FirstOrDefault(x => x.NavigationMenuItemId == draggedUponNavigationMenuItemId);

        if (draggedEntity == null || draggedUponEntity == null)
        {
            return;
        }

        // It was above it, so it's dragged down
        bool draggedDown = draggedEntity.SortOrder < draggedUponEntity.SortOrder;

        // Put the items in the correct order - then renumber the whole lot
        List<NavigationMenuItemEntity> navigationMenuItems = this.DataSource.OrderBy(x => x.SortOrder).ToList();
        navigationMenuItems.Remove(draggedEntity);

        for (int i = 0; i < navigationMenuItems.Count; i++)
        {
            if (navigationMenuItems[i] == draggedUponEntity)
            {
                if (draggedDown)
                {
                    // Place it below this item
                    navigationMenuItems.Insert(i + 1, draggedEntity);
                }
                else
                {
                    // Place it in front of this item.
                    navigationMenuItems.Insert(i, draggedEntity);
                }
                break;
            }
        }

        int orderNo = 1;
        foreach (NavigationMenuItemEntity entity in navigationMenuItems)
        {
            entity.SortOrder = orderNo;
            entity.Save();
            orderNo++;
        }
    }

    public void RefreshDataSource()
    {
        if (this.NavigationMenuId <= 0)
            return;

        PredicateExpression filter = new PredicateExpression(NavigationMenuItemFields.NavigationMenuId == this.NavigationMenuId);

        NavigationMenuItemCollection navigationMenuItemCollection = new NavigationMenuItemCollection();
        navigationMenuItemCollection.GetMulti(filter);

        this.DataSource = navigationMenuItemCollection.ToList();
    }

    #endregion

}
