﻿using System.Collections.Generic;
using System.Web.UI.WebControls;
using Obymobi.Logic.Cms;

public class MenuDataSource : ObjectDataSource
{
    private readonly MenuManager menuManager;

    public MenuDataSource(int menuId)
    {
        this.TypeName = typeof(MenuManager).FullName;
        this.menuManager = new MenuManager(menuId);

        this.InsertMethod = "Insert";
        this.UpdateMethod = "Update";
        this.DeleteMethod = "Delete";
        this.SelectMethod = "Select";

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.menuManager;
    }

    public List<IMenuItem> DataSource
    {
        get { return this.menuManager != null ? this.menuManager.DataSource : new List<IMenuItem>(); }
    }

    public MenuManager MenuManager
    {
        get
        {
            return this.menuManager;
        }
    }
}