﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Google.Apis.Manual.Util;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Enums;

public class LandingPageWidgetManager
{
    private LandingPageDataSource landingPageDataSource = new LandingPageDataSource();
    private WidgetDataSource widgetDataSource = new WidgetDataSource();

    private LandingPageEntity LandingPageEntity;

    public List<LandingPageWidgetEntity> DataSource { get; set; } = new List<LandingPageWidgetEntity>();    

    private LandingPageWidgetManager(int landingPageId)
    {
        LandingPageEntity entity = this.landingPageDataSource.GetLandingPage(landingPageId);
        if (entity.IsNew)
        {
            throw new System.Exception("Unknown landing page");
        }

        this.LandingPageEntity = entity;
        this.RefreshDataSource();
    }

    public static LandingPageWidgetManager Create(int landingPageId)
    {
        return new LandingPageWidgetManager(landingPageId);
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource.OrderBy(x => x.SortOrder);
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string name, ApplessWidgetType type)
    {
        if (name.IsNullOrEmpty())
        {
            throw new System.Exception("Please enter a name");
        }

        WidgetEntity widgetEntity = this.widgetDataSource.CreateWidget(type, name, this.LandingPageEntity.ParentCompanyId, this.LandingPageEntity.ApplicationConfigurationId);
        widgetEntity.Name = name;

        LandingPageWidgetEntity entity = new LandingPageWidgetEntity();
        entity.ParentCompanyId = this.LandingPageEntity.ParentCompanyId;
        entity.LandingPageId = this.LandingPageEntity.LandingPageId;
        entity.WidgetEntity = widgetEntity;
        entity.SortOrder = this.DataSource.Any() ? (this.DataSource.Max(x => x.SortOrder) + 1) : 1;
        entity.Save(true);

        this.DataSource.Add(entity);
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(int foreignKey)
    {
        var widgetEntity = this.widgetDataSource.GetWidget(foreignKey);

        LandingPageWidgetEntity entity = new LandingPageWidgetEntity();
        entity.ParentCompanyId = this.LandingPageEntity.ParentCompanyId;
        entity.LandingPageId = this.LandingPageEntity.LandingPageId;
        entity.WidgetEntity = widgetEntity;
        entity.SortOrder = this.DataSource.Any() ? (this.DataSource.Max(x => x.SortOrder) + 1) : 1;
        entity.Save(true);

        this.DataSource.Add(entity);
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(int widgetId, string name)
    {
        if (name.IsNullOrEmpty())
        {
            throw new System.Exception("Please enter a name");
        }

        LandingPageWidgetEntity entity = this.DataSource.FirstOrDefault(x => x.WidgetId == widgetId);
        if (entity == null)
        {
            return;
        }

        entity.WidgetEntity.Name = name;
        entity.Save(true);
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(int widgetId)
    {
        LandingPageWidgetEntity entity = this.DataSource.FirstOrDefault(x => x.WidgetId == widgetId);
        if (entity == null)
        {
            return;
        }

        if (entity.Delete())
        {
            this.DataSource.Remove(entity);
        }
    }

    public void DraggedToSort(int draggedWidgetId, int draggedUponWidgetId)
    {
        LandingPageWidgetEntity draggedEntity = this.DataSource.FirstOrDefault(x => x.WidgetId == draggedWidgetId);
        LandingPageWidgetEntity draggedUponEntity = this.DataSource.FirstOrDefault(x => x.WidgetId == draggedUponWidgetId);

        if (draggedEntity == null || draggedUponEntity == null)
        {
            return;
        }

        bool draggedDown = draggedEntity.SortOrder < draggedUponEntity.SortOrder;

        // Put the items in the correct order - then renumber the whole lot
        List<LandingPageWidgetEntity> widgets = this.DataSource.OrderBy(x => x.SortOrder).ToList();
        widgets.Remove(draggedEntity);

        for (int i = 0; i < widgets.Count; i++)
        {
            if (widgets[i] == draggedUponEntity)
            {
                if (draggedDown)
                {
                    // Place it below this item
                    widgets.Insert(i + 1, draggedEntity);
                }
                else
                {
                    // Place it in front of this item.
                    widgets.Insert(i, draggedEntity);
                }
                break;
            }
        }

        int orderNo = 1;
        foreach (LandingPageWidgetEntity entity in widgets)
        {
            entity.SortOrder = orderNo;
            entity.Save();
            orderNo++;
        }
    }

    public void RefreshDataSource()
    {
        PredicateExpression filter = new PredicateExpression(LandingPageWidgetFields.LandingPageId == this.LandingPageEntity.LandingPageId);

        LandingPageWidgetCollection landingPageWidgetCollection = new LandingPageWidgetCollection();
        landingPageWidgetCollection.GetMulti(filter);

        this.DataSource = landingPageWidgetCollection.ToList();
    }        
}