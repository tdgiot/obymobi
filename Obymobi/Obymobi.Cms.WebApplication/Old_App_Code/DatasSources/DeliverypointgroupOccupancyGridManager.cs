﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using Obymobi;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.Injectables.Validators;
using SD.LLBLGen.Pro.ORMSupportClasses;

/// <summary>
/// Summary description for DeliverypointgroupOccupancyGridManager
/// </summary>
public class DeliverypointgroupOccupancyGridManager
{
    public class OccupancyRow
    {
        public int DeliverypointgroupId { get; set; }
        public string Deliverypointgroup { get; set; }

        // Is it good looking? Fuck no! Does it work? Hell yes!
        public int d1 { set { Days.Add(1, value); } get { return Days[1]; } }
        public int d2 { set { Days.Add(2, value); } get { return Days[2]; } }
        public int d3 { set { Days.Add(3, value); } get { return Days[3]; } }
        public int d4 { set { Days.Add(4, value); } get { return Days[4]; } }
        public int d5 { set { Days.Add(5, value); } get { return Days[5]; } }
        public int d6 { set { Days.Add(6, value); } get { return Days[6]; } }
        public int d7 { set { Days.Add(7, value); } get { return Days[7]; } }
        public int d8 { set { Days.Add(8, value); } get { return Days[8]; } }
        public int d9 { set { Days.Add(9, value); } get { return Days[9]; } }
        public int d10 { set { Days.Add(10, value); } get { return Days[10]; } }
        public int d11 { set { Days.Add(11, value); } get { return Days[11]; } }
        public int d12 { set { Days.Add(12, value); } get { return Days[12]; } }
        public int d13 { set { Days.Add(13, value); } get { return Days[13]; } }
        public int d14 { set { Days.Add(14, value); } get { return Days[14]; } }
        public int d15 { set { Days.Add(15, value); } get { return Days[15]; } }
        public int d16 { set { Days.Add(16, value); } get { return Days[16]; } }
        public int d17 { set { Days.Add(17, value); } get { return Days[17]; } }
        public int d18 { set { Days.Add(18, value); } get { return Days[18]; } }
        public int d19 { set { Days.Add(19, value); } get { return Days[19]; } }
        public int d20 { set { Days.Add(20, value); } get { return Days[20]; } }
        public int d21 { set { Days.Add(21, value); } get { return Days[21]; } }
        public int d22 { set { Days.Add(22, value); } get { return Days[22]; } }
        public int d23 { set { Days.Add(23, value); } get { return Days[23]; } }
        public int d24 { set { Days.Add(24, value); } get { return Days[24]; } }
        public int d25 { set { Days.Add(25, value); } get { return Days[25]; } }
        public int d26 { set { Days.Add(26, value); } get { return Days[26]; } }
        public int d27 { set { Days.Add(27, value); } get { return Days[27]; } }
        public int d28 { set { Days.Add(28, value); } get { return Days[28]; } }
        public int d29 { set { Days.Add(29, value); } get { return Days[29]; } }
        public int d30 { set { Days.Add(30, value); } get { return Days[30]; } }
        public int d31 { set { Days.Add(31, value); } get { return Days[31]; } }
        
        public long sheit_DeliverypointgroupId { get; set; }

        public FastDictionary<int, int> Days = new FastDictionary<int, int>(31);
    }

    private readonly int companyId;
    private DateTime occupancyDate;

	public DeliverypointgroupOccupancyGridManager(int companyId, DateTime date)
	{
	    this.companyId = companyId;

	    SetOccupancyDate(date);
	}

    public void SetOccupancyDate(DateTime date)
    {
        this.occupancyDate = new DateTime(date.Year, date.Month, 1);
        dataSource = null;
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public DataTable Select()
    {
        return DataSource;
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert()
    {

    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(OccupancyRow row)
    {
        var dataRow = this.DataSource.Select(string.Format("DeliverypointgroupId='{0}'", row.DeliverypointgroupId));
        if (dataRow.Length == 0)
        {
            throw new Exception("Unable to find Deliverypointgroup with id: " + row.DeliverypointgroupId + " in DataSource");
        }

        foreach (var day in row.Days)
        {
            if (day.Key > DateTime.DaysInMonth(occupancyDate.Year, occupancyDate.Month))
            {
                continue;
            }

            int value = day.Value;
            if (day.Value < 0)
            {
                value = 0;
            }

            bool isSaved;
            try
            {
                var occupancy = new DeliverypointgroupOccupancyEntity();
                occupancy.Date = new DateTime(occupancyDate.Year, occupancyDate.Month, day.Key);
                occupancy.Amount = value;
                occupancy.DeliverypointgroupId = row.DeliverypointgroupId;
                occupancy.UpdateIfExists = true;

                isSaved = occupancy.Save();
            }
            catch (ObymobiException ex)
            {
                if (ex.ErrorEnumValue.Equals(DeliverypointgroupOccupancyValidator.DeliverypointgroupOccupancyError.StopSaving))
                {
                    isSaved = true;
                }
                else
                {
                    throw;
                }
            }

            if (isSaved)
            {
                dataRow[0]["d" + day.Key] = value;
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(long sheit_DeliverypointgroupOccupancyId)
    {

    }

    private DataTable dataSource;
    private DataTable DataSource
    {
        get
        {
            if (this.dataSource == null)
            {
                this.dataSource = new DataTable();
                this.dataSource.Columns.Add(new DataColumn("DeliverypointgroupId", typeof(int)));
                this.dataSource.Columns.Add(new DataColumn("Deliverypointgroup", typeof(string)));
                for (int i = 1; i <= 31; i++)
                {
                    var dayColumn = new DataColumn("d" + i, typeof(int));
                    dayColumn.DefaultValue = 0;
                    this.dataSource.Columns.Add(dayColumn);
                }

                var filter = new PredicateExpression();
                filter.Add(DeliverypointgroupFields.CompanyId == this.companyId);

                var relation = new RelationCollection(DeliverypointgroupEntityBase.Relations.DeliverypointgroupOccupancyEntityUsingDeliverypointgroupId, JoinHint.Left);

                var occupancyFilter = new PredicateExpression();
                occupancyFilter.Add(DeliverypointgroupOccupancyFields.Date >= this.occupancyDate);
                occupancyFilter.Add(DeliverypointgroupOccupancyFields.Date < this.occupancyDate.AddMonths(1));

                var prefetch = new PrefetchPath(EntityType.DeliverypointgroupEntity);
                prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathDeliverypointgroupOccupancyCollection, 0, occupancyFilter);

                var sort = new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending);

                var deliverypointgroupCollection = new DeliverypointgroupCollection();
                deliverypointgroupCollection.GetMulti(filter, 0, sort, relation, prefetch);

                foreach (var deliverypointgroupEntity in deliverypointgroupCollection)
                {
                    var row = this.dataSource.NewRow();
                    row["DeliverypointgroupId"] = deliverypointgroupEntity.DeliverypointgroupId;
                    row["Deliverypointgroup"] = deliverypointgroupEntity.Name;

                    foreach (var occupancyEntity in deliverypointgroupEntity.DeliverypointgroupOccupancyCollection)
                    {
                        row["d" + occupancyEntity.Date.Day] = occupancyEntity.Amount;
                    }

                    this.dataSource.Rows.Add(row);
                }
            }

            return this.dataSource;
        }
    }
}