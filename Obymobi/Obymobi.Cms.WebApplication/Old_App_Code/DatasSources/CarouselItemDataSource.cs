﻿using Obymobi.Data.EntityClasses;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public class CarouselItemDataSource : ObjectDataSource
{
    public readonly CarouselItemManager Manager;    

    public CarouselItemDataSource(int widgetCarouselId)
    {
        this.TypeName = typeof(CarouselItemManager).FullName;
        this.Manager = CarouselItemManager.Create(widgetCarouselId);

        this.UpdateMethod = "Update";
        this.SelectMethod = "Select";
        this.InsertMethod = "Insert";
        this.DeleteMethod = "Delete";

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.Manager;
    }

    public List<CarouselItemEntity> DataSource { get { return this.Manager != null ? this.Manager.DataSource : new List<CarouselItemEntity>(); } }
}