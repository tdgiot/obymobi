﻿using System.Collections.Generic;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;

public class NavigationMenuTreeListDataSource : ObjectDataSource
{
    public readonly NavigationMenuManager Manager;

    public NavigationMenuTreeListDataSource(int navigationMenuId)
    {
        this.TypeName = typeof(NavigationMenuManager).FullName;
        this.Manager = NavigationMenuManager.Create(navigationMenuId);

        this.UpdateMethod = "Update";
        this.SelectMethod = "Select";
        this.InsertMethod = "Insert";
        this.DeleteMethod = "Delete";

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.Manager;
    }

    public List<NavigationMenuItemEntity> DataSource => this.Manager != null 
            ? this.Manager.DataSource
            : new List<NavigationMenuItemEntity>();
}