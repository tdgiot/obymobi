﻿using Obymobi.Data.EntityClasses;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public class WidgetGroupWidgetDataSource : ObjectDataSource
{
    public readonly WidgetGroupWidgetManager Manager;    

    public WidgetGroupWidgetDataSource(int widgetGroupId)
    {
        this.TypeName = typeof(WidgetGroupWidgetManager).FullName;
        this.Manager = WidgetGroupWidgetManager.Create(widgetGroupId);

        this.UpdateMethod = "Update";
        this.SelectMethod = "Select";
        this.InsertMethod = "Insert";
        this.DeleteMethod = "Delete";

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.Manager;
    }

    public List<WidgetGroupWidgetEntity> DataSource { get { return this.Manager != null ? this.Manager.DataSource : new List<WidgetGroupWidgetEntity>(); } }
}