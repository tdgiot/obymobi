﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Google.Apis.Manual.Util;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Cms.Logic.DataSources;
using Obymobi.Enums;

public class WidgetGroupWidgetManager
{
    private WidgetDataSource widgetDataSource = new WidgetDataSource();

    private WidgetGroupEntity WidgetGroupEntity;

    public List<WidgetGroupWidgetEntity> DataSource { get; set; } = new List<WidgetGroupWidgetEntity>();

    private WidgetGroupWidgetManager(int widgetGroupId)
    {
        this.WidgetGroupEntity = this.widgetDataSource.GetWidgetGroup(widgetGroupId);
        if (this.WidgetGroupEntity.IsNew)
        {
            throw new System.Exception("Unknown widget group");
        }
        
        this.RefreshDataSource();
    }

    public static WidgetGroupWidgetManager Create(int widgetGroupId)
    {
        return new WidgetGroupWidgetManager(widgetGroupId);
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource.OrderBy(x => x.SortOrder);
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(int widgetId, string name, string text, ActionButtonIconType iconType)
    {
        if (name.IsNullOrEmpty())
        {
            throw new System.Exception("Please enter a name");
        }

        WidgetActionButtonEntity widgetEntity = this.widgetDataSource.CreateWidget(ApplessWidgetType.ActionButton, name, this.WidgetGroupEntity.ParentCompanyId, this.WidgetGroupEntity.ApplicationConfigurationId) as WidgetActionButtonEntity;

        WidgetGroupWidgetEntity entity = new WidgetGroupWidgetEntity();
        entity.ParentCompanyId = this.WidgetGroupEntity.ParentCompanyId;
        entity.WidgetGroupId = this.WidgetGroupEntity.WidgetId;
        entity.ParentWidgetEntity = widgetEntity;
        entity.Name = name;
        entity.Text = text;
        entity.IconType = iconType;
        entity.SortOrder = this.DataSource.Any() ? (this.DataSource.Max(x => x.SortOrder) + 1) : 1;
        entity.Save(true);

        this.DataSource.Add(entity);
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(int widgetId, string name, string text, ActionButtonIconType iconType)
    {
        if (name.IsNullOrEmpty())
        {
            throw new System.Exception("Please enter a name");
        }

        WidgetGroupWidgetEntity entity = this.DataSource.FirstOrDefault(x => x.WidgetId == widgetId);
        if (entity == null)
        {
            return;
        }

        entity.Name = name;
        entity.Text = text;
        entity.IconType = iconType;
        entity.Save(true);
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(int widgetId)
    {
        WidgetGroupWidgetEntity entity = this.DataSource.FirstOrDefault(x => x.WidgetId == widgetId);
        if (entity == null)
        {
            return;
        }

        this.DataSource.Remove(entity);
        entity.Delete();
    }

    public void DraggedToSort(int draggedWidgetId, int draggedUponWidgetId)
    {
        WidgetGroupWidgetEntity draggedEntity = this.DataSource.FirstOrDefault(x => x.WidgetId == draggedWidgetId);
        WidgetGroupWidgetEntity draggedUponEntity = this.DataSource.FirstOrDefault(x => x.WidgetId == draggedUponWidgetId);

        if (draggedEntity == null || draggedUponEntity == null)
        {
            return;
        }

        bool draggedDown = false;
        if (draggedEntity.SortOrder < draggedUponEntity.SortOrder)
        {
            // It was above it, so it's dragged down
            draggedDown = true;
        }

        // Put the items in the correct order - then renumber the whole lot
        List<WidgetGroupWidgetEntity> widgets = this.DataSource.OrderBy(x => x.SortOrder).ToList();
        widgets.Remove(draggedEntity);

        for (int i = 0; i < widgets.Count; i++)
        {
            if (widgets[i] == draggedUponEntity)
            {
                if (draggedDown)
                {
                    // Place it below this item
                    widgets.Insert(i + 1, draggedEntity);
                }
                else
                {
                    // Place it in front of this item.
                    widgets.Insert(i, draggedEntity);
                }
                break;
            }
        }

        int orderNo = 1;
        foreach (WidgetGroupWidgetEntity entity in widgets)
        {
            entity.SortOrder = orderNo;
            entity.Save();
            orderNo++;
        }
    }

    public void RefreshDataSource()
    {
        PredicateExpression filter = new PredicateExpression(WidgetGroupWidgetFields.WidgetGroupId == this.WidgetGroupEntity.WidgetId);

        WidgetGroupWidgetCollection widgetGroupWidgetCollection = new WidgetGroupWidgetCollection();
        widgetGroupWidgetCollection.GetMulti(filter);

        this.DataSource = widgetGroupWidgetCollection.ToList();
    }        
}