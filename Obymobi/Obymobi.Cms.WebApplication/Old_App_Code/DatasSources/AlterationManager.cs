﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Enums;
using Obymobi.Logic.Cms;

/// <summary>
/// Summary description for AlterationManager
/// </summary>
public class AlterationManager
{
    private readonly int alterationId;
    private AlterationEntity alterationEntity;

    public AlterationManager(int alterationId)
    {
        this.alterationId = alterationId;
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource.OrderBy(x => x.SortOrder).ToList();
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string itemType, string name, int type, string itemId)
    {
        if (itemType == AlterationComponentType.Alteration)
        {
            IEnumerable<IAlterationComponent> existingAlterations = this.DataSource.Where(x => x.ItemType == AlterationComponentType.Alteration);

            AlterationEntity alteration = new AlterationEntity();
            alteration.ValidatorCreateOrUpdateDefaultCustomText = true;
            alteration.CompanyId = this.alterationEntity.CompanyId;
            alteration.BrandId = this.alterationEntity.BrandId;
            alteration.ParentAlterationId = this.alterationId;
            alteration.Name = name;
            alteration.Type = (AlterationType)type;
            alteration.SortOrder = existingAlterations.Any() ? (existingAlterations.Max(x => x.SortOrder) + 1) : 1;
            alteration.Save();

            this.DataSource.Add(alteration);
        }
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string itemType, string name, int type, string itemId, string parentItemId)
    {
        IEnumerable<IAlterationComponent> existingSiblingItems = this.DataSource.Where(x => x.ParentItemId == parentItemId);
        if (parentItemId.Length == 0)
        {
            IEnumerable<IAlterationComponent> existingAlterations = this.DataSource.Where(x => x.ItemType == AlterationComponentType.Alteration);

            AlterationEntity alteration = new AlterationEntity();
            alteration.ValidatorCreateOrUpdateDefaultCustomText = true;
            alteration.CompanyId = this.alterationEntity.CompanyId;
            alteration.BrandId = this.alterationEntity.BrandId;
            alteration.ParentAlterationId = this.alterationId;
            alteration.Name = name;
            alteration.Type = (AlterationType)type;
            alteration.SortOrder = existingAlterations.Any() ? (existingAlterations.Max(x => x.SortOrder) + 1) : 1;
            alteration.Save();

            this.DataSource.Add(alteration);
        }
        else
        {
            IAlterationComponent parentItem = this.DataSource.FirstOrDefault(x => x.ItemId == parentItemId);

            int newSortOrder = 1;
            if (existingSiblingItems.Any())
            {
                newSortOrder = existingSiblingItems.Max(x => x.SortOrder) + 1;
            }

            if (parentItem is AlterationEntity)
            {
                if (itemType == AlterationComponentType.Alteration)
                {
                    AlterationEntity alteration = new AlterationEntity();
                    alteration.ValidatorCreateOrUpdateDefaultCustomText = true;
                    alteration.CompanyId = this.alterationEntity.CompanyId;
                    alteration.BrandId = this.alterationEntity.BrandId;
                    alteration.ParentAlterationId = (parentItem as AlterationEntity).AlterationId;
                    alteration.Name = name;
                    alteration.Type = (AlterationType)type;
                    alteration.SortOrder = newSortOrder;
                    alteration.Save();

                    this.DataSource.Add(alteration);
                }
                else if (itemType == AlterationComponentType.Product)
                {
                    ProductEntity product = new ProductEntity();
                    product.ValidatorCreateOrUpdateDefaultCustomText = true;
                    product.CompanyId = this.alterationEntity.CompanyId;
                    product.BrandId = this.alterationEntity.BrandId;
                    product.Type = (int)ProductType.Product;
                    product.SubType = (int)ProductSubType.Inherit;
                    product.VattariffId = 3;
                    product.Name = name;
                    product.Save();

                    AlterationProductEntity alterationProduct = new AlterationProductEntity();
                    alterationProduct.ParentCompanyId = this.alterationEntity.CompanyId;
                    alterationProduct.AlterationId = (parentItem as AlterationEntity).AlterationId;
                    alterationProduct.ProductId = product.ProductId;
                    alterationProduct.SortOrder = newSortOrder;
                    alterationProduct.Save();

                    this.DataSource.Add(alterationProduct);
                }                
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string itemType, string itemId, string parentItemId, string foreignKey)
    {
        IEnumerable<IAlterationComponent> existingSiblingItems = this.DataSource.Where(x => x.ParentItemId == parentItemId);
        if (parentItemId.Length == 0)
        {
            IEnumerable<IAlterationComponent> existingAlterations = this.DataSource.Where(x => x.ItemType == AlterationComponentType.Alteration);

            if (itemType == AlterationComponentType.Alteration)
            {
                AlterationEntity alteration = new AlterationEntity(int.Parse(foreignKey));
                alteration.ParentAlterationId = this.alterationId;
                alteration.SortOrder = existingAlterations.Any() ? (existingAlterations.Max(x => x.SortOrder) + 1) : 1;
                alteration.Save();

                this.DataSource.Add(alteration);
            }
            else if (itemType == AlterationComponentType.Product)
            {
                AlterationProductEntity alterationProduct = new AlterationProductEntity();
                alterationProduct.ParentCompanyId = this.alterationEntity.CompanyId;
                alterationProduct.AlterationId = this.alterationId;
                alterationProduct.ProductId = int.Parse(foreignKey);
                alterationProduct.SortOrder = existingAlterations.Any() ? (existingAlterations.Max(x => x.SortOrder) + 1) : 1;
                alterationProduct.Save();

                this.DataSource.Add(alterationProduct);
            }
        }
        else
        {
            IAlterationComponent parentItem = this.DataSource.FirstOrDefault(x => x.ItemId == parentItemId);

            int newSortOrder = 1;
            if (existingSiblingItems.Any())
            {
                newSortOrder = existingSiblingItems.Max(x => x.SortOrder) + 1;
            }

            if (parentItem is AlterationEntity)
            {
                if (itemType == AlterationComponentType.Alteration)
                {
                    AlterationEntity alteration = new AlterationEntity(int.Parse(foreignKey));
                    alteration.ParentAlterationId = (parentItem as AlterationEntity).AlterationId;
                    alteration.SortOrder = newSortOrder;
                    alteration.Save();

                    this.DataSource.Add(alteration);
                }
                else if (itemType == AlterationComponentType.Product)
                {
                    AlterationProductEntity alterationProduct = new AlterationProductEntity();
                    alterationProduct.ParentCompanyId = this.alterationEntity.CompanyId;
                    alterationProduct.AlterationId = (parentItem as AlterationEntity).AlterationId;
                    alterationProduct.ProductId = int.Parse(foreignKey);
                    alterationProduct.SortOrder = newSortOrder;
                    alterationProduct.Save();

                    this.DataSource.Add(alterationProduct);
                }
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string itemType, string itemId, string foreignKey)
    {
        if (itemType == AlterationComponentType.Alteration)
        {
            IEnumerable<IAlterationComponent> existingAlterations = this.DataSource.Where(x => x.ItemType == AlterationComponentType.Alteration);

            AlterationEntity alteration = new AlterationEntity(int.Parse(foreignKey));
            alteration.ParentAlterationId = this.alterationId;
            alteration.SortOrder = existingAlterations.Any() ? (existingAlterations.Max(x => x.SortOrder) + 1) : 1;
            alteration.Save();

            this.DataSource.Add(alteration);
        }        
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string name, string itemType, int type, string itemId)
    {
        IAlterationComponent item = this.DataSource.FirstOrDefault(x => x.ItemId == itemId);
        if (itemType == AlterationComponentType.Alteration)
        {
            if (item is AlterationEntity)
            {
                AlterationEntity alteration = (AlterationEntity)item;
                if (alteration != null)
                {
                    alteration.ValidatorCreateOrUpdateDefaultCustomText = true;
                    alteration.Name = name;
                    alteration.Type = (AlterationType)type;
                    alteration.Save();
                }
            }
        }
        else if (itemType == AlterationComponentType.Product)
        {
            AlterationProductEntity alterationProduct = (AlterationProductEntity)item;
            if (alterationProduct != null)
            {
                alterationProduct.ProductEntity.ValidatorCreateOrUpdateDefaultCustomText = true;
                alterationProduct.ProductEntity.Name = name;
                alterationProduct.ProductEntity.Type = type;
                alterationProduct.ProductEntity.Save();
                
                alterationProduct.Save();
            }
        }                     
    }

   [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string itemType, string itemId, string foreignKey)
    {
        IAlterationComponent item = this.DataSource.FirstOrDefault(x => x.ItemId == itemId);
        if (item is AlterationEntity)
        {
            AlterationEntity newAlteration = new AlterationEntity(int.Parse(foreignKey));
            if (!newAlteration.IsNew)
            {
                AlterationEntity oldAlteration = (AlterationEntity)item;

                newAlteration.ParentAlterationId = oldAlteration.ParentAlterationId;
                newAlteration.SortOrder = oldAlteration.SortOrder;
                newAlteration.Save();

                oldAlteration.ParentAlterationId = null;
                oldAlteration.Save();

                this.DataSource.Remove(item);
                this.DataSource.Add(newAlteration);
            }                        
        }
        else if (item is AlterationProductEntity)
        {
            AlterationProductEntity oldAlterationProduct = (AlterationProductEntity)item;

            AlterationProductEntity newAlterationProduct = new AlterationProductEntity();
            newAlterationProduct.ParentCompanyId = this.alterationEntity.CompanyId;
            newAlterationProduct.AlterationId = oldAlterationProduct.AlterationId;
            newAlterationProduct.ProductId = int.Parse(foreignKey);
            newAlterationProduct.SortOrder = oldAlterationProduct.SortOrder;
            newAlterationProduct.Save();

            this.DataSource.Remove(item);
            oldAlterationProduct.Delete();
            this.DataSource.Add(newAlterationProduct);
        }
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string parentItemId, string itemId)
    {
        IAlterationComponent item = this.DataSource.FirstOrDefault(x => x.ItemId == itemId);
        IAlterationComponent parentItem = this.DataSource.FirstOrDefault(x => x.ItemId == parentItemId);
        if (parentItem == null)
        {
            if (item is AlterationEntity && (((AlterationEntity)item).Type == AlterationType.Page || ((AlterationEntity)item).Type == AlterationType.Summary))
            {
                ((AlterationEntity)item).ParentAlterationId = null;
                ((AlterationEntity)item).Save();
            }
        }
        else
        {
            if (item.ItemType == parentItem.ItemType)
            {
                if (item is AlterationEntity && parentItem is AlterationEntity)
                {
                    AlterationEntity alterationItem = (AlterationEntity)item;
                    AlterationEntity parentAlterationItem = (AlterationEntity)parentItem;

                    if (alterationItem.Type == AlterationType.Category && parentAlterationItem.Type == AlterationType.Category && parentAlterationItem.AlterationProductCollection.Count > 0)
                    {
                        // Destination parent already has products configured
                        throw new Exception("Alterations of type 'Category' can't be added to the same category as products.");
                    }
                    else if (alterationItem.Type == AlterationType.Page || alterationItem.Type == AlterationType.Summary)
                    {
                        // Can't set a page or summary alteration as child
                    }
                    else if (alterationItem.Type == AlterationType.Category && (parentAlterationItem.Type != AlterationType.Category && parentAlterationItem.Type != AlterationType.Page))
                    {
                        // No can do, can only attach a category alteration to another category, or a page
                    }
                    else
                    {
                        // Change parent
                        AlterationEntity alteration = (AlterationEntity)item;
                        alteration.ParentAlterationId = (parentItem as AlterationEntity).AlterationId;
                        alteration.Save();
                    }
                }
                else
                {
                    List<IAlterationComponent> sortedItems = this.DataSource.Where(x => x.ParentItemId == parentItem.ParentItemId).OrderBy(x => x.SortOrder).ToList();

                    sortedItems.Remove(item);
                    int parentItemIndex = sortedItems.IndexOf(parentItem);
                    if (item.SortOrder < parentItem.SortOrder)
                    {
                        sortedItems.Insert(parentItemIndex + 1, item);
                    }
                    else
                    {
                        sortedItems.Insert(parentItemIndex, item);
                    }

                    int orderNo = 1;
                    foreach (IAlterationComponent sortedItem in sortedItems)
                    {
                        sortedItem.SortOrder = orderNo++;
                        sortedItem.Save();
                    }    
                }
            }
            else
            {
                if (item.ItemType == AlterationComponentType.Alteration && parentItem.ItemType == AlterationComponentType.Alteration)
                {
                    if (item is AlterationEntity)
                    {
                        AlterationEntity alteration = (AlterationEntity)item;
                        alteration.ParentAlterationId = (parentItem as AlterationEntity).AlterationId;
                        alteration.Save();
                    }
                }
                else if (item.ItemType == AlterationComponentType.Alteration && parentItem.ItemType == AlterationComponentType.Product)
                {
                    AlterationEntity alteration = (AlterationEntity)item;
                    alteration.ParentAlterationId = (parentItem as ProductAlterationEntity).AlterationId;
                    alteration.Save();
                }
                else if (item.ItemType == AlterationComponentType.Product && parentItem.ItemType == AlterationComponentType.Alteration)
                {
                    if (parentItem is AlterationEntity && ((AlterationEntity)parentItem).Type == AlterationType.Category)
                    {
                        AlterationEntity parentAlteration = (AlterationEntity)parentItem;

                        if (parentAlteration.AlterationCollection.Count > 0)
                        {
                            throw new Exception("Products can't be added to a 'category' type alteration which already has child alterations set up.");
                        }
                        else if (item is AlterationProductEntity)
                        {
                            AlterationProductEntity alterationProduct = (AlterationProductEntity)item;

                            AlterationProductEntity existingProduct = parentAlteration.AlterationProductCollection.SingleOrDefault(x => x.ProductId == alterationProduct.ProductId);
                            if (existingProduct != null)
                            {
                                throw new Exception("Products can't be added twice to the same 'Category' alteration.");
                            }
                            else
                            {
                                alterationProduct.AlterationId = parentAlteration.AlterationId;
                                alterationProduct.Save();    
                            }
                        }
                    }
                }
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(string itemId)
    {
        IAlterationComponent item = this.DataSource.FirstOrDefault(x => x.ItemId == itemId);
        if (item != null)
        {
            this.DataSource.Remove(item);
            item.Delete();
        }
    }

    public void DraggedToSort(string draggedComponentId, string draggedUponComponentId)
    {
        IAlterationComponent dragged = this.DataSource.FirstOrDefault(x => x.ItemId == draggedComponentId);
        IAlterationComponent draggedUpon = this.DataSource.FirstOrDefault(x => x.ItemId == draggedUponComponentId);

        bool draggedDown = false;
        if (dragged.ParentItemId == draggedUpon.ParentItemId)
        {
            if (dragged.SortOrder < draggedUpon.SortOrder)
            {
                // It was above it, so it's dragged down
                draggedDown = true;
            }

            // Put the items in the correct order - then renumber the whole lot
            List<IAlterationComponent> components = this.DataSource.Where(x => x.ParentItemId == dragged.ParentItemId).OrderBy(x => x.SortOrder).ToList();

            // Remove the dragged page from the list (we're going to insert that again on it's 
            // correct new place, and then re-number all sort orders.
            components.Remove(dragged);

            for (int i = 0; i < components.Count; i++)
            {
                if (components[i] == draggedUpon)
                {
                    if (draggedDown)
                    {
                        // Place it below this item
                        components.Insert(i + 1, dragged);
                    }
                    else
                    {
                        // Place it in front of this item.
                        components.Insert(i, dragged);
                    }
                    break;
                }
            }

            int orderNo = 1;
            foreach (IAlterationComponent component in components)
            {
                component.SortOrder = orderNo;
                component.Save();
                orderNo++;
            }
        }
        else
        {
            if (draggedUpon is AlterationEntityBase)
            {
                AlterationEntityBase draggedUponAlteration = (AlterationEntityBase)draggedUpon;

                if (dragged is AlterationEntityBase)
                {
                    AlterationEntityBase draggedAlteration = (AlterationEntityBase)dragged;
                    draggedAlteration.ParentAlterationId = draggedUponAlteration.ParentAlterationId;
                    draggedAlteration.SortOrder = draggedUpon.SortOrder;
                    draggedAlteration.Save();
                }
                else if (dragged is AlterationProductEntityBase)
                {
                    AlterationProductEntityBase draggedAlterationProduct = (AlterationProductEntityBase)dragged;
                    draggedAlterationProduct.AlterationId = draggedUponAlteration.ParentAlterationId.Value;
                    draggedAlterationProduct.SortOrder = draggedUpon.SortOrder;
                    draggedAlterationProduct.Save();
                }

                // Get other childeren to sort
                List<IAlterationComponent> childerenToSort = this.DataSource.Where(x => x.ParentItemId == draggedUpon.ParentItemId && x.SortOrder >= dragged.SortOrder && x.ItemId != draggedComponentId).OrderBy(x => x.SortOrder).ToList();
                int currentSortOrder = dragged.SortOrder + 1;
                for (int i = 0; i < childerenToSort.Count; i++)
                {
                    if (i == 0 && draggedDown)
                    {
                        childerenToSort[i].SortOrder = currentSortOrder - 2; // Dragged down with same parent, needs to be placed above it.
                    }
                    else
                    {
                        childerenToSort[i].SortOrder = currentSortOrder;
                    }
                    childerenToSort[i].Save();
                    currentSortOrder++;
                }
            }
        }
    }

    public void SortCategory(string componentId)
    {
        IAlterationComponent dragged = this.DataSource.FirstOrDefault(x => x.ItemId == componentId);

        AlterationEntity entity = dragged as AlterationEntity;
        if (entity == null || entity.Type != AlterationType.Category)
        {
            return;
        }

        List<IAlterationComponent> components = this.DataSource.Where(x => x.ParentItemId == dragged.ItemId).ToList();
        components.Sort((one, two) => SmartCompare(one.Name, two.Name));

        int orderNo = 1;
        foreach (IAlterationComponent component in components)
        {
            component.SortOrder = orderNo;
            component.Save();
            orderNo++;
        }
    }

    private static readonly Regex smartCompareExpression = new Regex(@"^(?:A |The)\s*", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

    static Int32 SmartCompare(String x, String y)
    {
        x = smartCompareExpression.Replace(x, "");
        y = smartCompareExpression.Replace(y, "");

        return String.Compare(x, y, StringComparison.Ordinal);
    }

    private List<IAlterationComponent> dataSource;
    public List<IAlterationComponent> DataSource
    {
        get
        {
            if (this.dataSource == null)
            {
                this.dataSource = new List<IAlterationComponent>();

                PrefetchPath path = new PrefetchPath(EntityType.AlterationEntity);

                List<IPrefetchPathElement> alterationPaths = new List<IPrefetchPathElement>();
                for (int i = 0; i < 4; i++)
                {
                    IPrefetchPathElement alterationPath = AlterationEntityBase.PrefetchPathAlterationCollection;

                    // Alteration > AlterationProduct
                    // Alteration > AlterationProduct > Product
                    IPrefetchPathElement prefetchAlterationAlterationProduct = alterationPath.SubPath.Add(AlterationEntityBase.PrefetchPathAlterationProductCollection);
                    IPrefetchPathElement prefetchAlterationAlterationProductProduct = prefetchAlterationAlterationProduct.SubPath.Add(AlterationProductEntityBase.PrefetchPathProductEntity);                    

                    alterationPaths.Add(alterationPath);
                }
                path.Add(alterationPaths[0]).SubPath.Add(alterationPaths[1]).SubPath.Add(alterationPaths[2]).SubPath.Add(alterationPaths[3]);

                this.alterationEntity = new AlterationEntity(this.alterationId, path);
                if (!this.alterationEntity.IsNew)
                {
                    this.AddToDataSource(this.alterationEntity.AlterationCollection);
                }
            }

            return this.dataSource;
        }
    }

    private void AddToDataSource(AlterationCollection alterations)
    {
        this.dataSource.AddRange(alterations);

        foreach (AlterationEntity alteration in alterations)
        {
            this.dataSource.AddRange(alteration.AlterationProductCollection);

            if (alteration.AlterationCollection.Count > 0)
            {
                this.AddToDataSource(alteration.AlterationCollection);
            }
        }
    }
}