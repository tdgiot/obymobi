﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

public class SmsKeywordManager
{
    public enum SmsKeywordErrors
    {
        SmsKeywordAlreadyExists
    }

    private readonly int smsInformationId;

	public SmsKeywordManager(int smsInformationId)
	{
	    this.smsInformationId = smsInformationId;
	}

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource.OrderBy(x => x.Keyword);
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string keyword, int companyId)
    {
        // Save to database
        var smsKeyword = new SmsKeywordEntity();
        smsKeyword.Keyword = keyword;
        smsKeyword.SmsInformationId = this.smsInformationId;
        smsKeyword.CompanyId = companyId;
        if (smsKeyword.Save())
        {
            this.DataSource.Add(smsKeyword);
        }
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string keyword, int companyId, int sheit_SmsKeywordId)
    {
        var keywordInDataSource = this.DataSource.FirstOrDefault(x => x.SmsKeywordId == sheit_SmsKeywordId);

        if (keywordInDataSource != null)
        {
            var oldKeyword = keywordInDataSource.Keyword;
            keywordInDataSource.Keyword = keyword;
            keywordInDataSource.CompanyId = companyId;
            if (!keywordInDataSource.Save())
            {
                keywordInDataSource.Keyword = oldKeyword;
            }
        }
        else
            throw new Exception("Keyword isn't available with id: " + sheit_SmsKeywordId);
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(int sheit_SmsKeywordId)
    {
        var keywordInDataSource = this.DataSource.FirstOrDefault(x => x.SmsKeywordId == sheit_SmsKeywordId);

        if (keywordInDataSource != null)
        {
            this.DataSource.Remove(keywordInDataSource);
            keywordInDataSource.Delete();
        }
        else
            throw new Exception("Keyword isn't available with id: " + sheit_SmsKeywordId);
    }

    private List<SmsKeywordEntity> dataSource;
    private List<SmsKeywordEntity> DataSource
    {
        get
        {
            if (this.dataSource == null)
            {
                this.dataSource = new List<SmsKeywordEntity>();

                var filter = new PredicateExpression();
                filter.Add(SmsKeywordFields.SmsInformationId == this.smsInformationId);

                var keywordCollection = new SmsKeywordCollection();
                keywordCollection.GetMulti(filter);

                this.dataSource.AddRange(keywordCollection);
            }

            return this.dataSource;
        }
    }
}