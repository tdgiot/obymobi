﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections;
using System.Linq;

/// <summary>
/// Summary description for Option
/// </summary>
public class Option
{
    public Option()
    {
        
    }

    public string OptionId { get; set; }
    public string ParentOptionId { get; set; }
    public string Name { get; set; }
    public IEntity Entity { get; set; }
    public IEntity LinkedEntity { get; set; }
    public int SubType { get; set; }
    public int SortOrder { get; set; }
    public bool LinkedMultipleEntities { get; set; }
    public bool HasChildren { get; set; }

    public string Type
    {
        get
        {
            return this.Entity != null ? this.Entity.PrimaryKeyFields[0].Name.ToString().Replace("Id", "") : string.Empty;
        }
    }

    public int EntityId => (int?)this.Entity?.PrimaryKeyFields[0].DbValue ?? 0;

    public string SubTypeAsString
    {
        get
        {
            if (this.Entity is ProductEntity)
                return this.SubType.ToEnum<ProductType>().ToString();
            else if (this.Entity is AlterationEntity)
                return this.SubType.ToEnum<AlterationType>().ToString();
            else if (this.Entity is AlterationoptionEntity)
                return this.SubType.ToEnum<AlterationType>().ToString();

            return string.Empty;
        }        
    }
}