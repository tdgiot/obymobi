﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Cms.WebApplication.Old_App_Code.DatasSources.MenuExternalItems
{
    public class MenuExternalItemConverter
    {
        private readonly MenuEntity menuEntity;
        private readonly List<MenuExternalItem> menuExternalItems;

        public MenuExternalItemConverter(MenuEntity menuEntity)
        {
            this.menuEntity = menuEntity;
            this.menuExternalItems = new List<MenuExternalItem>();
        }

        public IEnumerable<MenuExternalItem> Convert()
        {
            this.menuExternalItems.Clear();
            this.ConvertCategories(this.menuEntity.CategoryCollection);

            return this.menuExternalItems;
        }

        private void ConvertCategories(CategoryCollection categoryCollection)
        {
            foreach (CategoryEntity categoryEntity in categoryCollection)
            {
                MenuExternalItem menuExternalItem = this.ConvertCategory(categoryEntity);
                this.menuExternalItems.Add(menuExternalItem);

                this.ConvertProducts(categoryEntity.ProductCategoryCollection, menuExternalItem);
            }
        }

        private void ConvertProducts(ProductCategoryCollection productCategoryCollection, MenuExternalItem menuExternalParentItem)
        {
            foreach (ProductCategoryEntity productCategoryEntity in productCategoryCollection)
            {
                MenuExternalItem menuExternalItem = this.ConvertProduct(productCategoryEntity, menuExternalParentItem);
                this.menuExternalItems.Add(menuExternalItem);

                this.ConvertAlterations(productCategoryEntity.ProductEntity.ProductAlterationCollection, menuExternalItem);
            }
        }

        private void ConvertAlterations(ProductAlterationCollection productAlterationCollection, MenuExternalItem menuExternalParentItem)
        {
            foreach (ProductAlterationEntity productAlterationEntity in productAlterationCollection)
            {
                MenuExternalItem menuExternalItem = this.ConvertAlteration(productAlterationEntity, menuExternalParentItem);
                this.menuExternalItems.Add(menuExternalItem);
        
                this.ConvertAlterationoptions(productAlterationEntity.AlterationEntity.AlterationitemCollection, menuExternalItem);
            }
        }
        
        private void ConvertAlterationoptions(AlterationitemCollection alterationitemCollection, MenuExternalItem menuExternalParentItem)
        {
            this.menuExternalItems.AddRange(alterationitemCollection.Select(x => ConvertAlterationoption(x, menuExternalParentItem)));    
        }

        private MenuExternalItem ConvertCategory(CategoryEntity categoryEntity)
        {
            return new MenuExternalItem
            {
                MenuExternalItemId = $"category-{categoryEntity.CategoryId}",
                MenuExternalParentItemId = $"category-{categoryEntity.ParentCategoryId}",
                Name = categoryEntity.Name,
                ExternalItemName = string.Empty,
                SortOrder = categoryEntity.SortOrder,
                EntityType = nameof(CategoryEntity),
                PrimaryKey = categoryEntity.CategoryId
            };
        }

        private MenuExternalItem ConvertProduct(ProductCategoryEntity productCategoryEntity, MenuExternalItem menuExternalParentItem)
        {
            string menuExternalParentItemId = this.GetMenuExternalItemId(menuExternalParentItem);

            return new MenuExternalItem
            {
                MenuExternalItemId = menuExternalParentItemId + $"productcategory-{productCategoryEntity.ProductCategoryId}",
                MenuExternalParentItemId = menuExternalParentItemId,
                Name = productCategoryEntity.ProductEntity.Name,
                ExternalItemName = productCategoryEntity.ProductEntity.ExternalProductId.HasValue
                    ? productCategoryEntity.ProductEntity.ExternalProductEntity.Name
                    : string.Empty,
                SortOrder = productCategoryEntity.SortOrder,
                EntityType = nameof(ProductEntity),
                PrimaryKey = productCategoryEntity.ProductId,
                IsVisible = productCategoryEntity.ProductEntity.VisibilityType != Enums.VisibilityType.Never
            };
        }

        private MenuExternalItem ConvertAlteration(ProductAlterationEntity productAlterationEntity, MenuExternalItem menuExternalItem)
        {
            string menuExternalParentItemId = this.GetMenuExternalItemId(menuExternalItem);
        
            return new MenuExternalItem
            {
                MenuExternalItemId = menuExternalParentItemId + $"productalteration-{productAlterationEntity.ProductAlterationId}",
                MenuExternalParentItemId = menuExternalParentItemId,
                Name = productAlterationEntity.AlterationEntity.Name,
                ExternalItemName = productAlterationEntity.AlterationEntity.ExternalProductId.HasValue
                    ? productAlterationEntity.AlterationEntity.ExternalProductEntity.Name
                    : string.Empty,
                SortOrder = productAlterationEntity.SortOrder,
                EntityType = nameof(AlterationEntity),
                PrimaryKey = productAlterationEntity.AlterationId,
                IsVisible = true
            };
        }
        
        private MenuExternalItem ConvertAlterationoption(AlterationitemEntity alterationitemEntity, MenuExternalItem menuPosParentItem)
        {
            string menuExternalParentItemId = this.GetMenuExternalItemId(menuPosParentItem);

            return new MenuExternalItem
            {
                MenuExternalItemId = menuExternalParentItemId + $"alterationitem-{alterationitemEntity.AlterationitemId}",
                MenuExternalParentItemId = menuExternalParentItemId,
                Name = alterationitemEntity.AlterationoptionEntity.Name,
                ExternalItemName = alterationitemEntity.AlterationoptionEntity.ExternalProductId.HasValue
                    ? alterationitemEntity.AlterationoptionEntity.ExternalProductEntity.Name
                    : string.Empty,
                SortOrder = alterationitemEntity.SortOrder,
                EntityType = nameof(AlterationoptionEntity),
                PrimaryKey = alterationitemEntity.AlterationoptionId,
                IsVisible = true
            };
        }

        private string GetMenuExternalItemId(MenuExternalItem menuExternalItem) => menuExternalItem != null ? menuExternalItem.MenuExternalItemId : string.Empty;
    }
}