﻿using System.Collections.Generic;
using System.Web.UI.WebControls;
using Obymobi.Cms.WebApplication.Old_App_Code.DatasSources.MenuExternalItems;

public class MenuExternalItemDataSource : ObjectDataSource
{
    private readonly MenuExternalItemManager menuExternalItemManager;

    public MenuExternalItemDataSource(int menuId)
    {
        this.TypeName = typeof(MenuExternalItemManager).FullName;
        this.menuExternalItemManager = new MenuExternalItemManager(menuId);

        this.SelectMethod = nameof(MenuExternalItemManager.Select);
        this.UpdateMethod = nameof(MenuExternalItemManager.Update);

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.menuExternalItemManager;
    }

    public List<MenuExternalItem> DataSource => this.menuExternalItemManager.DataSource;

    public MenuExternalItemManager MenuExternalItemManager => this.menuExternalItemManager;
}