using Obymobi.Cms.WebApplication.Old_App_Code.DatasSources.MenuExternalItems;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

public class MenuExternalItemManager
{
    private readonly int MenuId;

    public MenuExternalItemManager(int menuId)
    {
        MenuId = menuId;
        RefreshDataSource();
    }

    public List<MenuExternalItem> DataSource { get; private set; } = new List<MenuExternalItem>();

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return DataSource.OrderBy(x => x.SortOrder).ToList();
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string menuExternalItemId, string typeText, string name, string externalItemName)
    {
        MenuExternalItem item = DataSource.FirstOrDefault(x => x.MenuExternalItemId.Equals(menuExternalItemId, System.StringComparison.InvariantCultureIgnoreCase));
        if (item == null)
        {
            return;
        }

        int? externalProductId = null;
        if (int.TryParse(externalItemName, out int parsedExternalProductId) && parsedExternalProductId > 0)
        {
            externalProductId = parsedExternalProductId;
        }

        if (item.EntityType.Equals(nameof(ProductEntity)))
        {
            ProductEntity entity = new ProductEntity(item.PrimaryKey);
            entity.ExternalProductId = externalProductId;
            entity.Save();
        }
        else if (item.EntityType.Equals(nameof(AlterationEntity)))
        {
            AlterationEntity entity = new AlterationEntity(item.PrimaryKey);
            entity.ExternalProductId = externalProductId;
            entity.Save();
        }
        else if (item.EntityType.Equals(nameof(AlterationoptionEntity)))
        {
            AlterationoptionEntity entity = new AlterationoptionEntity(item.PrimaryKey);
            entity.ExternalProductId = externalProductId;
            entity.Save();
        }

        RefreshDataSource();
    }

    public void RefreshDataSource()
    {
        MenuEntity menuEntity = FetchMenu(MenuId);
        DataSource = new MenuExternalItemConverter(menuEntity).Convert().ToList();
    }

    private MenuEntity FetchMenu(int menuId)
    {
        PrefetchPath prefetchPath = new PrefetchPath(EntityType.MenuEntity);

        // Root categories
        IPrefetchPathElement prefetchCategory = prefetchPath.Add(MenuEntity.PrefetchPathCategoryCollection, new IncludeFieldsList(CategoryFields.Name, CategoryFields.SortOrder));

        // Products
        IPrefetchPathElement prefetchProductCategory = prefetchCategory.SubPath.Add(CategoryEntity.PrefetchPathProductCategoryCollection, new IncludeFieldsList(ProductCategoryFields.ProductId, ProductCategoryFields.SortOrder));
        IPrefetchPathElement prefetchProduct = prefetchProductCategory.SubPath.Add(ProductCategoryEntity.PrefetchPathProductEntity, new IncludeFieldsList(ProductFields.Name, ProductFields.ExternalProductId, ProductFields.VisibilityType));
        IPrefetchPathElement prefetchExternalproduct = prefetchProduct.SubPath.Add(ProductEntity.PrefetchPathExternalProductEntity, new IncludeFieldsList(ExternalProductFields.Name, ExternalProductFields.ParentCompanyId));

        // Alterations
        IPrefetchPathElement prefetchProductAlteration = prefetchProduct.SubPath.Add(ProductEntity.PrefetchPathProductAlterationCollection, new IncludeFieldsList(ProductAlterationFields.ProductId, ProductAlterationFields.SortOrder));
        IPrefetchPathElement prefetchAlteration = prefetchProductAlteration.SubPath.Add(ProductAlterationEntity.PrefetchPathAlterationEntity, new IncludeFieldsList(AlterationFields.Name));
        IPrefetchPathElement prefetchAlterationExternalproduct = prefetchAlteration.SubPath.Add(AlterationEntity.PrefetchPathExternalProductEntity, new IncludeFieldsList(ExternalProductFields.Name, ExternalProductFields.ParentCompanyId));

        // Alterationoptions
        IPrefetchPathElement prefetchAlterationitem = prefetchAlteration.SubPath.Add(AlterationEntity.PrefetchPathAlterationitemCollection, new IncludeFieldsList(AlterationitemFields.AlterationoptionId, AlterationitemFields.SortOrder));
        IPrefetchPathElement prefetchAlterationoption = prefetchAlterationitem.SubPath.Add(AlterationitemEntity.PrefetchPathAlterationoptionEntity, new IncludeFieldsList(AlterationoptionFields.Name));
        IPrefetchPathElement prefetchAlterationoptionExternalproduct = prefetchAlterationoption.SubPath.Add(AlterationoptionEntity.PrefetchPathExternalProductEntity, new IncludeFieldsList(ExternalProductFields.Name, ExternalProductFields.ParentCompanyId));

        return new MenuEntity(menuId, prefetchPath);
    }
}