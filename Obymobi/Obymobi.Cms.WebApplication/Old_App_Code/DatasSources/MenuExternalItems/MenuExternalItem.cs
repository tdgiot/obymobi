﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Cms.WebApplication.Old_App_Code.DatasSources.MenuExternalItems
{
    public class MenuExternalItem
    {
        public string MenuExternalItemId { get; set; }
        public string MenuExternalParentItemId { get; set; }
        public string Name { get; set; }
        public string ExternalItemName { get; set; }
        public int SortOrder { get; set; }
        public string EntityType { get; set; }
        public int PrimaryKey { get; set; }

        public string TypeText
        {
            get
            {
                switch (EntityType)
                {
                    case nameof(CategoryEntity): return "Category";
                    case nameof(ProductEntity): return "Product";
                    case nameof(AlterationEntity): return "Alteration";
                    case nameof(AlterationoptionEntity): return "Alteration option";
                    default: return EntityType;
                }
            }
        }

        public bool? IsVisible { get; internal set; }
    }
}