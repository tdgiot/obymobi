﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

/// <summary>
/// Summary description for DeliverypointgroupOccupancyManager
/// </summary>
public class DeliverypointgroupOccupancyManager
{
    private readonly int deliverypointgroupId;

	public DeliverypointgroupOccupancyManager(int deliverypointgroupId)
	{
        this.deliverypointgroupId = deliverypointgroupId;
	}

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return DataSource.OrderByDescending(i => i.Date);
        //DataSource.Sort((a, b) => -1 * a.Date.CompareTo(b.Date));
        //return DataSource.ToList();
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(DateTime date, int amount)
    {
        var occupancy = new DeliverypointgroupOccupancyEntity();
        occupancy.Date = date;
        occupancy.Amount = amount;
        occupancy.DeliverypointgroupId = this.deliverypointgroupId;
        occupancy.ParentCompanyId = CmsSessionHelper.CurrentCompanyId;
        if (occupancy.Save())
        {
            DataSource.Add(occupancy);
        }
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(DateTime date, int amount, long sheit_DeliverypointgroupOccupancyId)
    {
        var occupancyInDataSource = this.DataSource.FirstOrDefault(x => x.DeliverypointgroupOccupancyId == sheit_DeliverypointgroupOccupancyId);

        if (occupancyInDataSource != null)
        {
            var oldDate = occupancyInDataSource.Date;
            var oldAmount = occupancyInDataSource.Amount;

            occupancyInDataSource.Date = date;
            occupancyInDataSource.Amount = amount;
            if (!occupancyInDataSource.Save())
            {
                occupancyInDataSource.Date = oldDate;
                occupancyInDataSource.Amount = oldAmount;
            }
        }
        else
        {
            throw new Exception("Occupancy isn't available with id: " + sheit_DeliverypointgroupOccupancyId);
        }
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(long sheit_DeliverypointgroupOccupancyId)
    {
        var occupancyInDataSource = this.DataSource.FirstOrDefault(x => x.DeliverypointgroupOccupancyId == sheit_DeliverypointgroupOccupancyId);

        if (occupancyInDataSource != null)
        {
            this.DataSource.Remove(occupancyInDataSource);
            occupancyInDataSource.Delete();
        }
        else
        {
            throw new Exception("Keyword isn't available with id: " + sheit_DeliverypointgroupOccupancyId);
        }
    }

    private List<DeliverypointgroupOccupancyEntity> dataSource;
    private List<DeliverypointgroupOccupancyEntity> DataSource
    {
        get
        {
            if (this.dataSource == null)
            {
                this.dataSource = new List<DeliverypointgroupOccupancyEntity>();

                var filter = new PredicateExpression();
                filter.Add(DeliverypointgroupOccupancyFields.DeliverypointgroupId == this.deliverypointgroupId);

                var occupancyCollection = new DeliverypointgroupOccupancyCollection();
                occupancyCollection.GetMulti(filter);

                this.dataSource.AddRange(occupancyCollection);
            }

            return this.dataSource;
        }
    }
}