﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;
using Dionysos;
using System;
using Obymobi;

public class OptionManager
{
    public int Version;
    public int ProductId;
    public int AlterationId;
    public int AlterationitemId;
    public int ProductgroupId;

    public int BrandId = 0;

    public ProductEntity ProductEntity;
    public AlterationEntity AlterationEntity;
    public AlterationitemEntity AlterationitemEntity;
    public ProductgroupEntity ProductgroupEntity;
    
    private OptionConverter optionConverter;

    private OptionManager(int version, int productId, int alterationId, int alterationitemId, int productgroupId)
    {
        this.Version = version;
        this.ProductId = productId;
        this.AlterationId = alterationId;
        this.AlterationitemId = alterationitemId;
        this.ProductgroupId = productgroupId;
        this.RefreshDataSource(); 
    }

    public static OptionManager Empty(int version) 
    {
        return new OptionManager(version, -1, -1, -1, -1);
    }

    public static OptionManager FromProduct(int version, int productId)
    {
        return new OptionManager(version, productId, -1, -1, -1);
    }

    public static OptionManager FromAlteration(int version, int alterationId)
    {
        return new OptionManager(version, -1, alterationId, -1, -1);
    }

    public static OptionManager FromAlterationitem(int version, int alterationitemId)
    {
        return new OptionManager(version, -1, -1, alterationitemId, -1);
    }

    public static OptionManager FromProductgroup(int version, int productgroupId)
    {
        return new OptionManager(version, -1, -1, -1, productgroupId);
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource;
    }       

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string name, string type, int subType)
    { 
        IEnumerable<Option> existingOptions = this.DataSource.Where(x => x.ParentOptionId.IsNullOrWhiteSpace());

        if (type.Equals(OptionType.Product))
        {
            if (this.ProductgroupId > 0)
            {
                ProductEntity product = new ProductEntity();
                product.ValidatorCreateOrUpdateDefaultCustomText = true;
                if (this.BrandId > 0)
                {
                    product.BrandId = this.BrandId;
                }
                else
                {
                    product.CompanyId = CmsSessionHelper.CurrentCompanyId;
                }
                product.VattariffId = 3;
                product.Type = subType;
                product.Name = name;
                product.Save();

                ProductgroupItemEntity productgroupItem = new ProductgroupItemEntity();
                productgroupItem.ProductgroupId = this.ProductgroupId;
                productgroupItem.ProductId = product.ProductId;
                productgroupItem.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                productgroupItem.Save();

                this.optionConverter.ConvertProductgroupItem(productgroupItem, null);
                this.RefreshDataSource();
            }
        }
        else if (type.Equals(OptionType.Productgroup))
        {
            ProductgroupEntity productgroup = new ProductgroupEntity();
            productgroup.ValidatorCreateOrUpdateDefaultCustomText = true;
            productgroup.CompanyId = CmsSessionHelper.CurrentCompanyId;
            productgroup.Name = name;
            productgroup.Save();

            if (this.ProductId > 0)
            {
                this.ProductEntity.ProductgroupId = productgroup.ProductgroupId;
                this.ProductEntity.Save();

                this.optionConverter.ConvertProductgroup(productgroup, null);
                this.RefreshDataSource();
            }
            else if (this.ProductgroupId > 0)
            {
                ProductgroupItemEntity productgroupItem = new ProductgroupItemEntity();
                productgroupItem.ProductgroupId = this.ProductgroupId;
                productgroupItem.NestedProductgroupId = productgroup.ProductgroupId;
                productgroupItem.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                productgroupItem.Save();

                this.optionConverter.ConvertProductgroupItem(productgroupItem, null);
                this.RefreshDataSource();
            }
        }
        else if (type.Equals(OptionType.Alteration))
        {
            AlterationEntity alteration = new AlterationEntity();
            alteration.ValidatorCreateOrUpdateDefaultCustomText = true;
            if (this.BrandId > 0)
            {
                alteration.BrandId = this.BrandId;
            }
            else
            {
                alteration.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }
            alteration.Version = this.Version;
            alteration.Type = subType.ToEnum<AlterationType>();
            alteration.Name = name;
            alteration.Save();

            if (this.ProductId > 0)
            {
                ProductAlterationEntity productAlteration = new ProductAlterationEntity();
                if (alteration.CompanyId.HasValue)
                {
                    productAlteration.ParentCompanyId = alteration.CompanyId;
                }
                productAlteration.Version = this.Version;
                productAlteration.ProductId = this.ProductId;
                productAlteration.AlterationId = alteration.AlterationId;
                productAlteration.SortOrder = this.GetSortOrder(existingOptions);
                productAlteration.Save();

                this.optionConverter.ConvertProductAlteration(productAlteration, null);
                this.RefreshDataSource();
            }
            else if (this.AlterationitemId > 0)
            {
                AlterationitemAlterationEntity alterationitemAlteration = new AlterationitemAlterationEntity();
                if (alteration.CompanyId.HasValue)
                {
                    alterationitemAlteration.ParentCompanyId = alteration.CompanyId;
                }
                alterationitemAlteration.AlterationitemId = this.AlterationitemId;
                alterationitemAlteration.AlterationId = alteration.AlterationId;
                alterationitemAlteration.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                alterationitemAlteration.Save();

                this.optionConverter.ConvertAlterationitemAlteration(alterationitemAlteration, null);
                this.RefreshDataSource();
            }
        }
        else if (type.Equals(OptionType.Alterationoption))
        {
            if (subType == -1)
            {
                throw new ObymobiException(GenericResult.EntitySaveException, "Please select a sub type.");
            }

            AlterationoptionEntity alterationoption = new AlterationoptionEntity();
            alterationoption.ValidatorCreateOrUpdateDefaultCustomText = true;
            if (this.BrandId > 0)
            {
                alterationoption.BrandId = this.BrandId;
            }
            else
            {
                alterationoption.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }
            alterationoption.Version = this.Version;
            alterationoption.Type = subType;
            alterationoption.Name = name;
            alterationoption.Save();

            AlterationitemEntity alterationitem = new AlterationitemEntity();
            if (this.BrandId <= 0)
            {
                alterationitem.ParentCompanyId = CmsSessionHelper.CurrentCompanyId;
            }
            alterationitem.Version = this.Version;
            alterationitem.AlterationId = this.AlterationId;
            alterationitem.AlterationoptionId = alterationoption.AlterationoptionId;
            alterationitem.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
            alterationitem.Save();

            this.optionConverter.ConvertAlterationitem(alterationitem, null);
            this.RefreshDataSource();
        }
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string name, string type, int subType, string parentOptionId)
    {
        Option parent = this.DataSource.SingleOrDefault(x => x.OptionId.Equals(parentOptionId, System.StringComparison.InvariantCultureIgnoreCase));
        IEnumerable<Option> existingOptions = this.DataSource.Where(x => x.ParentOptionId.Equals(parent.OptionId, StringComparison.InvariantCultureIgnoreCase));

        if (type.Equals(OptionType.Product))
        {
            ProductEntity product = new ProductEntity();
            product.ValidatorCreateOrUpdateDefaultCustomText = true;
            if (this.BrandId > 0)
            {
                product.BrandId = this.BrandId;
            }
            else
            {
                product.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }

            product.VattariffId = 3;
            product.Type = subType;
            product.Name = name;
            product.Save();

            if (parent.Entity is ProductgroupEntity)
            {
                ProductgroupItemEntity productgroupItem = new ProductgroupItemEntity();
                productgroupItem.ParentCompanyId = CmsSessionHelper.CurrentCompanyId;
                productgroupItem.ProductgroupId = ((ProductgroupEntity)parent.Entity).ProductgroupId;
                productgroupItem.ProductId = product.ProductId;
                productgroupItem.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                productgroupItem.Save();

                this.optionConverter.ConvertProductgroupItem(productgroupItem, parent);
                this.RefreshDataSource();
            }
            else if (parent.Entity is AlterationEntity)
            {
                AlterationEntity entity = (AlterationEntity)parent.Entity;

                AlterationProductEntity alterationProduct = new AlterationProductEntity();
                if (entity.CompanyId.HasValue)
                {
                    alterationProduct.ParentCompanyId = entity.CompanyId;
                }
                alterationProduct.AlterationId = entity.AlterationId;
                alterationProduct.ProductId = product.ProductId;
                alterationProduct.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                alterationProduct.Save();

                this.optionConverter.ConvertAlterationProduct(alterationProduct, parent);
                this.RefreshDataSource();
            }
        }
        else if (type.Equals(OptionType.Productgroup))
        {
            if (parent.Entity is ProductEntity)
            {
                ProductEntity parentProduct = (ProductEntity)parent.Entity;

                ProductgroupEntity productgroup = new ProductgroupEntity();
                productgroup.ValidatorCreateOrUpdateDefaultCustomText = true;
                productgroup.CompanyId = CmsSessionHelper.CurrentCompanyId;
                productgroup.Name = name;
                productgroup.Save();

                parentProduct.ProductgroupId = productgroup.ProductgroupId;
                parentProduct.Save();

                this.optionConverter.ConvertProductgroup(productgroup, parent);
                this.RefreshDataSource();
            }
            else if (parent.Entity is ProductgroupEntity)
            {
                ProductgroupEntity productgroup = new ProductgroupEntity();
                productgroup.ValidatorCreateOrUpdateDefaultCustomText = true;
                productgroup.CompanyId = CmsSessionHelper.CurrentCompanyId;
                productgroup.Name = name;
                productgroup.Save();

                ProductgroupItemEntity productgroupItem = new ProductgroupItemEntity();
                productgroupItem.ParentCompanyId = CmsSessionHelper.CurrentCompanyId;
                productgroupItem.ProductgroupId = ((ProductgroupEntity)parent.Entity).ProductgroupId;
                productgroupItem.NestedProductgroupId = productgroup.ProductgroupId;
                productgroupItem.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                productgroupItem.Save();

                this.optionConverter.ConvertProductgroupItem(productgroupItem, parent);
                this.RefreshDataSource();
            }
        }
        else if (type.Equals(OptionType.Alteration))
        {
            AlterationEntity alteration = new AlterationEntity();
            alteration.ValidatorCreateOrUpdateDefaultCustomText = true;
            if (this.BrandId > 0)
            {
                alteration.BrandId = this.BrandId;
            }
            else
            { 
                alteration.CompanyId = CmsSessionHelper.CurrentCompanyId;
            }
            alteration.Version = this.Version;
            alteration.Type = subType.ToEnum<AlterationType>();
            alteration.Name = name;
            alteration.Save();

            if (parent.Entity is ProductEntity)
            {
                ProductAlterationEntity productAlteration = new ProductAlterationEntity();
                if (alteration.CompanyId.HasValue)
                {
                    productAlteration.ParentCompanyId = alteration.CompanyId;
                }
                productAlteration.Version = this.Version;
                productAlteration.ProductId = ((ProductEntity)parent.Entity).ProductId;
                productAlteration.AlterationId = alteration.AlterationId;
                productAlteration.SortOrder = this.GetSortOrder(existingOptions);
                productAlteration.Save();

                this.optionConverter.ConvertProductAlteration(productAlteration, parent);
                this.RefreshDataSource();
            }
            else if (parent.LinkedEntity is AlterationitemEntity)
            {
                AlterationitemAlterationEntity alterationitemAlteration = new AlterationitemAlterationEntity();
                if (alteration.CompanyId.HasValue)
                {
                    alterationitemAlteration.ParentCompanyId = alteration.CompanyId;
                }
                alterationitemAlteration.AlterationitemId = ((AlterationitemEntity)parent.LinkedEntity).AlterationitemId;
                alterationitemAlteration.AlterationId = alteration.AlterationId;
                alterationitemAlteration.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                alterationitemAlteration.Save();

                this.optionConverter.ConvertAlterationitemAlteration(alterationitemAlteration, parent);
                this.RefreshDataSource();
            }
            else if (parent.Entity is AlterationEntity)
            {
                alteration.ParentAlterationId = ((AlterationEntity)parent.Entity).AlterationId;
                alteration.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                alteration.Save();

                this.optionConverter.ConvertAlteration(alteration, parent);
                this.RefreshDataSource();
            }
        }
        else if (type.Equals(OptionType.Alterationoption))
        {
            if (parent.Entity is AlterationEntity)
            {
                AlterationEntity entity = (AlterationEntity)parent.Entity;

                AlterationoptionEntity alterationoption = new AlterationoptionEntity();
                alterationoption.ValidatorCreateOrUpdateDefaultCustomText = true;
                if (entity.BrandId.HasValue)
                {
                    alterationoption.BrandId = entity.BrandId;
                }
                else
                {
                    alterationoption.CompanyId = entity.CompanyId;
                }
                
                alterationoption.Version = this.Version;
                alterationoption.Type = subType;
                alterationoption.Name = name;
                alterationoption.Save();
                
                AlterationitemEntity alterationitem = new AlterationitemEntity();
                if (entity.CompanyId.HasValue)
                {
                    alterationitem.ParentCompanyId = entity.CompanyId;
                }
                alterationitem.Version = this.Version;
                alterationitem.AlterationId = entity.AlterationId;
                alterationitem.AlterationoptionId = alterationoption.AlterationoptionId;
                alterationitem.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                alterationitem.Save();

                this.optionConverter.ConvertAlterationitem(alterationitem, parent);                
                this.RefreshDataSource();
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(int foreignKey, string type)
    {
        ICollection<Option> existingOptions = this.DataSource.Where(x => x.ParentOptionId.IsNullOrWhiteSpace()).ToList();

        Option parent = null;

        if (this.ProductId > 0)
        {
            parent = new Option {Entity = this.ProductEntity};            
        }        
        else if (this.ProductgroupId > 0)
        {
            parent = new Option { Entity = this.ProductgroupEntity };
        }
        else if (this.AlterationId > 0)
        {
            parent = new Option { Entity = this.AlterationEntity };            
        }        
        else if (this.AlterationitemId > 0)
        {
            parent = new Option { Entity = this.AlterationitemEntity };            
        }

        if (parent != null)
        {
            this.Insert(foreignKey, type, parent, existingOptions);
        }        
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(int foreignKey, string type, string parentOptionId)
    {
        Option parent = this.DataSource.SingleOrDefault(x => x.OptionId.Equals(parentOptionId, System.StringComparison.InvariantCultureIgnoreCase));
        ICollection<Option> existingOptions = this.DataSource.Where(x => x.ParentOptionId.Equals(parent.OptionId, StringComparison.InvariantCultureIgnoreCase)).ToList();
        this.Insert(foreignKey, type, parent, existingOptions); 
    }

    private void Insert(int foreignKey, string type, Option parent, ICollection<Option> existingOptions)
    {
        if (type.Equals(OptionType.Productgroup))
        {
            if (parent.Entity is ProductgroupEntity)
            {
                ProductgroupItemEntity productgroupItem = new ProductgroupItemEntity();
                productgroupItem.ParentCompanyId = CmsSessionHelper.CurrentCompanyId;
                productgroupItem.ProductgroupId = ((ProductgroupEntity)parent.Entity).ProductgroupId;
                productgroupItem.NestedProductgroupId = foreignKey;
                productgroupItem.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                productgroupItem.Save();
            }
            else if (parent.Entity is ProductEntity)
            {
                ((ProductEntity)parent.Entity).ProductgroupId = foreignKey;
                ((ProductEntity)parent.Entity).Save();                
            }
        }
        else if (type.Equals(OptionType.Product))
        {
            if (parent.Entity is ProductgroupEntity)
            {
                ProductgroupItemEntity productgroupItem = new ProductgroupItemEntity();
                productgroupItem.ParentCompanyId = CmsSessionHelper.CurrentCompanyId;
                productgroupItem.ProductgroupId = ((ProductgroupEntity)parent.Entity).ProductgroupId;
                productgroupItem.ProductId = foreignKey;
                productgroupItem.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                productgroupItem.Save();
            }
            else if (parent.Entity is AlterationEntity)
            {
                AlterationEntity entity = (AlterationEntity)parent.Entity;

                AlterationProductEntity alterationProduct = new AlterationProductEntity();
                if (entity.CompanyId.HasValue)
                {
                    alterationProduct.ParentCompanyId = entity.CompanyId;
                }
                alterationProduct.AlterationId = entity.AlterationId;
                alterationProduct.ProductId = foreignKey;
                alterationProduct.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                alterationProduct.Save();                
            }
        }
        else if (type.Equals(OptionType.Alteration))
        {
            if (parent.Entity is ProductEntity)
            {
                ProductAlterationEntity productAlteration = new ProductAlterationEntity();
                productAlteration.ParentCompanyId = CmsSessionHelper.CurrentCompanyId;
                productAlteration.Version = this.Version;
                productAlteration.ProductId = ((ProductEntity)parent.Entity).ProductId;
                productAlteration.AlterationId = foreignKey;
                productAlteration.SortOrder = this.GetSortOrder(existingOptions);
                productAlteration.Save();
            }
            else if (parent.Entity is AlterationitemEntity)
            {
                AlterationitemEntity entity = (AlterationitemEntity)parent.Entity;

                AlterationitemAlterationEntity alterationitemAlteration = new AlterationitemAlterationEntity();
                if (entity.ParentCompanyId.HasValue)
                {
                    alterationitemAlteration.ParentCompanyId = entity.ParentCompanyId;
                }
                alterationitemAlteration.AlterationitemId = entity.AlterationitemId;
                alterationitemAlteration.AlterationId = foreignKey;
                alterationitemAlteration.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                alterationitemAlteration.Save();
            }
            else if (parent.Entity is AlterationEntity)
            {
                AlterationEntity alteration = new AlterationEntity(foreignKey);
                if (!alteration.IsNew)
                {
                    alteration.ParentAlterationId = ((AlterationEntity)parent.Entity).AlterationId;
                    alteration.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                    alteration.Save();
                }
            }                
            else if (parent.LinkedEntity is AlterationitemEntity)
            {
                AlterationitemEntity entity = (AlterationitemEntity)parent.LinkedEntity;

                AlterationitemAlterationEntity alterationitemAlteration = new AlterationitemAlterationEntity();
                if (entity.ParentCompanyId.HasValue)
                {
                    alterationitemAlteration.ParentCompanyId = entity.ParentCompanyId;
                }
                alterationitemAlteration.AlterationitemId = entity.AlterationitemId;
                alterationitemAlteration.AlterationId = foreignKey;
                alterationitemAlteration.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                alterationitemAlteration.Save();
            }
        }
        else if (type.Equals(OptionType.Alterationoption))
        {
            if (parent.Entity is AlterationEntity)
            {
                AlterationEntity entity = (AlterationEntity)parent.Entity;

                AlterationitemEntity alterationitem = new AlterationitemEntity();
                if (!entity.BrandId.HasValue)
                {
                    alterationitem.ParentCompanyId = CmsSessionHelper.CurrentCompanyId;
                }
                alterationitem.Version = this.Version;
                alterationitem.AlterationId = entity.AlterationId;
                alterationitem.AlterationoptionId = foreignKey;
                alterationitem.SortOrder = existingOptions.Any() ? (existingOptions.Max(x => x.SortOrder) + 1) : 1;
                alterationitem.Save();
            }
        }

        this.RefreshDataSource();
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string name, int subType, string optionId)
    {
        Option option = this.DataSource.FirstOrDefault(x => x.OptionId.Equals(optionId, System.StringComparison.InvariantCultureIgnoreCase));
        if (option != null)
        {
            if (option.Entity is ProductEntity)
            {
                ((ProductEntity)option.Entity).ValidatorCreateOrUpdateDefaultCustomText = true;                
                ((ProductEntity)option.Entity).Type = subType > -1 ? subType : ((ProductEntity)option.Entity).Type;
            }
            else if (option.Entity is ProductgroupEntity)
            {
                ((ProductgroupEntity)option.Entity).ValidatorCreateOrUpdateDefaultCustomText = true;
            }
            else if (option.Entity is AlterationEntity)
            {
                ((AlterationEntity)option.Entity).ValidatorCreateOrUpdateDefaultCustomText = true;
                ((AlterationEntity)option.Entity).Type = subType > -1 ? subType.ToEnum<AlterationType>() : ((AlterationEntity)option.Entity).Type;
            }
            else if (option.Entity is AlterationoptionEntity)
            {
                ((AlterationoptionEntity)option.Entity).ValidatorCreateOrUpdateDefaultCustomText = true;
                ((AlterationoptionEntity)option.Entity).Type = subType > -1 ? subType : ((AlterationoptionEntity)option.Entity).Type;
            }                
            
            option.Entity.SetNewFieldValue("Name", name);
            option.Entity.Save();
            this.RefreshDataSource();
        }
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(string optionId)
    {
        Option option = this.DataSource.FirstOrDefault(x => x.OptionId.Equals(optionId, System.StringComparison.InvariantCultureIgnoreCase));
        if (option != null)
        {
            this.DataSource.Remove(option);

            Option parentOption = this.DataSource.FirstOrDefault(x => x.OptionId.Equals(option.ParentOptionId, System.StringComparison.InvariantCultureIgnoreCase));
            if (option.LinkedEntity != null)
            {
                option.LinkedEntity.Delete();
            }
            else if (option.Entity is ProductgroupEntity)
            {
                if (this.ProductEntity != null && this.ProductEntity.ProductgroupId.Equals(((ProductgroupEntity)option.Entity).ProductgroupId))
                {
                    this.ProductEntity.ProductgroupId = null;
                    this.ProductEntity.Save();
                }
                else if (parentOption != null && parentOption.Entity is ProductEntity)
                {
                    ((ProductEntity)parentOption.Entity).ProductgroupId = null;
                    ((ProductEntity)parentOption.Entity).Save();
                }
            }
            else if (option.Entity is AlterationEntity && parentOption != null && parentOption.Entity is AlterationEntity)
            {
                ((AlterationEntity)option.Entity).ParentAlterationId = null;
                ((AlterationEntity)option.Entity).Save();
            }
            else
            {
                option.Entity.Delete();
            }
        }        
    }

    public void RefreshDataSource()
    {
        this.optionConverter = new OptionConverter(this.Version);
        if (this.ProductId > 0)
        {
            this.ProductEntity = new ProductEntity(this.ProductId);
            this.optionConverter.ConvertChildrenFromProduct(this.ProductEntity, null);
        }
        else if (this.AlterationId > 0)
        {
            this.AlterationEntity = new AlterationEntity(this.AlterationId);
            this.optionConverter.ConvertAlterationitems(this.AlterationEntity.AlterationitemCollection, null);

            this.BrandId = this.AlterationEntity.BrandId.GetValueOrDefault(0);
        }
        else if (this.AlterationitemId > 0)
        {
            this.AlterationitemEntity = new AlterationitemEntity(this.AlterationitemId);
            this.optionConverter.ConvertAlterationitemAlterations(this.AlterationitemEntity.AlterationitemAlterationCollection, null);
        }
        else if (this.ProductgroupId > 0)
        {
            this.ProductgroupEntity = new ProductgroupEntity(this.ProductgroupId);
            this.optionConverter.ConvertProductgroupItems(this.ProductgroupEntity.ProductgroupItemCollection, null);
        }
        this.DataSource = this.optionConverter.Options;
    }

    public void DraggedToSort(string draggedId, string draggedUponId)
    {
        Option dragged = this.DataSource.FirstOrDefault(x => x.OptionId.Equals(draggedId, System.StringComparison.InvariantCultureIgnoreCase));
        Option draggedUpon = this.DataSource.FirstOrDefault(x => x.OptionId.Equals(draggedUponId, System.StringComparison.InvariantCultureIgnoreCase));

        bool draggedDown = false;
        if (dragged.ParentOptionId == draggedUpon.ParentOptionId)
        {
            if (dragged.SortOrder < draggedUpon.SortOrder)
            {
                // It was above it, so it's dragged down
                draggedDown = true;
            }

            // Put the items in the correct order - then renumber the whole lot
            List<Option> components = this.DataSource.Where(x => x.ParentOptionId == dragged.ParentOptionId).OrderBy(x => x.SortOrder).ToList();

            // Remove the dragged page from the list (we're going to insert that again on it's 
            // correct new place, and then re-number all sort orders.
            components.Remove(dragged);

            for (int i = 0; i < components.Count; i++)
            {
                if (components[i] == draggedUpon)
                {
                    if (draggedDown)
                    {
                        // Place it below this item
                        components.Insert(i + 1, dragged);
                    }
                    else
                    {
                        // Place it in front of this item.
                        components.Insert(i, dragged);
                    }
                    break;
                }
            }

            int orderNo = 1;
            foreach (Option component in components)
            {
                if (component.LinkedEntity != null)
                {
                    component.LinkedEntity.SetNewFieldValue("SortOrder", orderNo);
                    component.LinkedEntity.Save();
                    orderNo++;
                }
            }
        }
        else
        {
            if (draggedUpon.Entity is ProductEntity)
            {
                ProductEntity product = (ProductEntity)draggedUpon.Entity;

                if (dragged.LinkedEntity is ProductAlterationEntity)
                {
                    ProductAlterationEntity productAlteration = (ProductAlterationEntity)dragged.LinkedEntity;
                    productAlteration.ProductId = product.ProductId;
                    productAlteration.SortOrder = draggedUpon.SortOrder;
                    productAlteration.Save();
                    dragged.SortOrder = draggedUpon.SortOrder;
                }
                else if (dragged.LinkedEntity is AlterationitemAlterationEntity)
                {
                    AlterationitemAlterationEntity alterationitemAlteration = (AlterationitemAlterationEntity)dragged.LinkedEntity;

                    ProductAlterationEntity productAlteration = new ProductAlterationEntity();
                    productAlteration.Version = this.Version;
                    productAlteration.ProductId = product.ProductId;
                    productAlteration.AlterationId = alterationitemAlteration.AlterationId;
                    productAlteration.SortOrder = draggedUpon.SortOrder;
                    productAlteration.Save();

                    dragged.LinkedEntity.Delete();
                    dragged.LinkedEntity = productAlteration;
                    dragged.SortOrder = draggedUpon.SortOrder;
                }
            }
            else if (draggedUpon.Entity is AlterationEntity)
            {
                AlterationEntity alteration = (AlterationEntity)draggedUpon.Entity;

                if (dragged.LinkedEntity is AlterationitemEntity)
                {
                    AlterationitemEntity alterationitem = (AlterationitemEntity)dragged.LinkedEntity;
                    alterationitem.AlterationId = alteration.AlterationId;
                    alterationitem.SortOrder = draggedUpon.SortOrder;
                    alterationitem.Save();
                    dragged.SortOrder = draggedUpon.SortOrder;
                }
            }
            else if (draggedUpon.LinkedEntity is AlterationitemEntity)
            {
                AlterationitemEntity draggedUponAlterationitem = (AlterationitemEntity)draggedUpon.LinkedEntity;

                if (dragged.LinkedEntity is AlterationitemAlterationEntity)
                {
                    AlterationitemAlterationEntity alterationitemAlteration = (AlterationitemAlterationEntity)dragged.LinkedEntity;
                    alterationitemAlteration.AlterationitemId = draggedUponAlterationitem.AlterationitemId;
                    alterationitemAlteration.SortOrder = draggedUpon.SortOrder;
                    alterationitemAlteration.Save();
                    dragged.SortOrder = draggedUpon.SortOrder;
                }
                else if (dragged.LinkedEntity is ProductAlterationEntity)
                {
                    ProductAlterationEntity productAlteration = (ProductAlterationEntity)dragged.LinkedEntity;

                    AlterationitemAlterationEntity alterationitemAlteration = new AlterationitemAlterationEntity();
                    alterationitemAlteration.AlterationitemId = draggedUponAlterationitem.AlterationitemId;
                    alterationitemAlteration.AlterationId = productAlteration.AlterationId;
                    alterationitemAlteration.SortOrder = draggedUpon.SortOrder;
                    alterationitemAlteration.Save();

                    dragged.LinkedEntity.Delete();
                    dragged.LinkedEntity = alterationitemAlteration;
                    dragged.SortOrder = draggedUpon.SortOrder;
                }
            }

            // Get other childeren to sort
            List<Option> childerenToSort = this.DataSource.Where(x => x.ParentOptionId.Equals(draggedUpon.OptionId, System.StringComparison.InvariantCultureIgnoreCase) && x.SortOrder >= dragged.SortOrder && !x.OptionId.Equals(draggedId, System.StringComparison.InvariantCultureIgnoreCase)).OrderBy(x => x.SortOrder).ToList();
            int currentSortOrder = dragged.SortOrder + 1;
            for (int i = 0; i < childerenToSort.Count; i++)
            {
                if (childerenToSort[i].LinkedEntity != null)
                {
                    if (i == 0 && draggedDown)
                    {
                        childerenToSort[i].LinkedEntity.SetNewFieldValue("SortOrder", currentSortOrder - 2); // Dragged down with same parent, needs to be placed above it.
                    }
                    else
                    {
                        childerenToSort[i].LinkedEntity.SetNewFieldValue("SortOrder", currentSortOrder);
                    }

                    childerenToSort[i].LinkedEntity.Save();
                    currentSortOrder++;
                }
            }
        }
        this.RefreshDataSource();
    }

    private int GetSortOrder(IEnumerable<Option> existingOptions)
    {
        if (existingOptions.Any())
        {
            return existingOptions.Max(x => x.SortOrder) + 1;
        }

        return this.Version == 2 ? 11 : 1;
    }

    public List<Option> DataSource { get; set; }
}