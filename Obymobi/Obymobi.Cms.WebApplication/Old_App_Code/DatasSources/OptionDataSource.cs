﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

public class OptionDataSource : ObjectDataSource
{
    private readonly OptionManager Manager;    

    public OptionDataSource(int version, int productId, int alterationId, int alterationitemId, int productgroupId)
    {
        this.TypeName = typeof(OptionManager).FullName;

        if (productId > 0)
        {
            this.Manager = OptionManager.FromProduct(version, productId);
        }
        else if (alterationId > 0)
        {
            this.Manager = OptionManager.FromAlteration(version, alterationId);
        }
        else if (alterationitemId > 0)
        {
            this.Manager = OptionManager.FromAlterationitem(version, alterationitemId);
        }
        else if (productgroupId > 0)
        {
            this.Manager = OptionManager.FromProductgroup(version, productgroupId);
        }
        else
        {
            this.Manager = OptionManager.Empty(version);
        }

        this.InsertMethod = "Insert";
        this.UpdateMethod = "Update";
        this.DeleteMethod = "Delete";
        this.SelectMethod = "Select";

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.Manager;
    }

    public void DraggedToSort(string draggedId, string draggedUponId)
    {
        this.Manager.DraggedToSort(draggedId, draggedUponId);
    }

    public int Version { get { return this.Manager.Version; } }
    public int ProductId { get { return this.Manager.ProductId; } }    
    public int AlterationId { get { return this.Manager.AlterationId; } }
    public int AlterationitemId { get { return this.Manager.AlterationitemId; } }
    public int ProductgroupId { get { return this.Manager.ProductgroupId; } }
    public ProductgroupEntity ProductgroupEntity { get { return this.Manager.ProductgroupEntity; } }
    public ProductEntity ProductEntity { get { return this.Manager.ProductEntity; } }
    public AlterationEntity AlterationEntity { get { return this.Manager.AlterationEntity; } }
    public AlterationitemEntity AlterationitemEntity { get { return this.Manager.AlterationitemEntity; } }
    public T GetEntitiesOfType<T>(string type) where T : IEntityCollection, new() { return this.DataSource.Where(x => x.Type.Equals(type, System.StringComparison.InvariantCultureIgnoreCase)).Select(x => x.Entity).ToEntityCollection<T>(); }
    public List<Option> DataSource { get { return this.Manager != null ? this.Manager.DataSource : new List<Option>(); } }    
}