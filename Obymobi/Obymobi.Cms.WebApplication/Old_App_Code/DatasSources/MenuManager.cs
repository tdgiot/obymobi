﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;

public class MenuManager
{
    private readonly int menuId;
    private MenuEntity menuEntity;

    public MenuManager(int menuId)
    {
        this.menuId = menuId;
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource.OrderBy(x => x.SortOrder).ToList();
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string name, string itemType, string itemId)
    {
        if (itemType == MenuItemType.Category)
        {
            IEnumerable<IMenuItem> existingCategories = this.DataSource.Where(x => x.ItemType == MenuItemType.Category);

            CategoryEntity category = new CategoryEntity();
            category.ValidatorCreateOrUpdateDefaultCategoryLanguage = true;
            category.CompanyId = this.menuEntity.CompanyId;
            category.MenuId = this.menuId;
            category.Name = name;
            category.Type = (int)CategoryType.Inherit;
            category.SortOrder = existingCategories.Any() ? (existingCategories.Max(x => x.SortOrder) + 1) : 1;
            category.Save();

            this.DataSource.Add(category);
        }
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string name, string itemType, string itemId, string parentItemId)
    {
        IEnumerable<IMenuItem> existingSiblingItems = this.DataSource.Where(x => x.ParentItemId == parentItemId);
        if (parentItemId.Length == 0)
        {
            IEnumerable<IMenuItem> existingCategories = this.DataSource.Where(x => x.ItemType == MenuItemType.Category);

            CategoryEntity category = new CategoryEntity();
            category.ValidatorCreateOrUpdateDefaultCategoryLanguage = true;
            category.CompanyId = this.menuEntity.CompanyId;
            category.MenuId = this.menuId;
            category.Name = name;
            category.Type = (int)CategoryType.Inherit;
            category.SortOrder = existingCategories.Any() ? (existingCategories.Max(x => x.SortOrder) + 1) : 1;
            category.Save();

            this.DataSource.Add(category);
        }
        else
        {
            IMenuItem parentItem = this.DataSource.FirstOrDefault(x => x.ItemId == parentItemId);

            int newSortOrder = 1;
            if (existingSiblingItems.Any())
            {
                newSortOrder = existingSiblingItems.Max(x => x.SortOrder) + 1;
            }

            if (parentItem is CategoryEntity)
            {
                if (itemType == MenuItemType.Category)
                {
                    CategoryEntity category = new CategoryEntity();
                    category.ValidatorCreateOrUpdateDefaultCategoryLanguage = true;
                    category.CompanyId = this.menuEntity.CompanyId;
                    category.MenuId = this.menuId;
                    category.ParentCategoryId = (parentItem as CategoryEntity).CategoryId;
                    category.Name = name;
                    category.SortOrder = newSortOrder;
                    category.Save();

                    this.DataSource.Add(category);
                }
                else if (itemType == MenuItemType.Product)
                {
                    ProductEntity product = new ProductEntity();
                    product.ValidatorCreateOrUpdateDefaultCustomText = true;
                    product.CompanyId = this.menuEntity.CompanyId;
                    product.Type = (int)ProductType.Product;
                    product.SubType = (int)ProductSubType.Inherit;
                    product.VattariffId = 3;
                    product.Name = name;
                    product.Save();

                    ProductCategoryEntity productCategory = new ProductCategoryEntity();
                    productCategory.ParentCompanyId = this.menuEntity.CompanyId;
                    productCategory.CategoryId = (parentItem as CategoryEntity).CategoryId;
                    productCategory.ProductId = product.ProductId;
                    productCategory.SortOrder = newSortOrder;
                    productCategory.Save();

                    this.DataSource.Add(productCategory);
                }
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string name, string itemType, string itemId)
    {
        IMenuItem item = this.DataSource.FirstOrDefault(x => x.ItemId == itemId);
        if (itemType == MenuItemType.Category)
        {
            CategoryEntity category = (CategoryEntity)item;
            if (category != null)
            {
                category.ValidatorCreateOrUpdateDefaultCategoryLanguage = true;
                category.Name = name;
                category.Save();
            }
        }
        else if (itemType == MenuItemType.Product)
        {
            ProductCategoryEntity productCategory = (ProductCategoryEntity)item;
            if (productCategory != null)
            {
                productCategory.ProductEntity.ValidatorCreateOrUpdateDefaultCustomText = true;
                productCategory.ProductEntity.Name = name;
                productCategory.ProductEntity.Save(); 
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string parentItemId, string itemId)
    {
        IMenuItem item = this.DataSource.FirstOrDefault(x => x.ItemId == itemId);
        IMenuItem parentItem = this.DataSource.FirstOrDefault(x => x.ItemId == parentItemId);
        if(item != null && item.ItemType == parentItem.ItemType)
        {
            List<IMenuItem> sortedItems = this.DataSource.Where(x => x.ParentItemId == parentItem.ParentItemId).OrderBy(x => x.SortOrder).ToList();

            sortedItems.Remove(item);
            int parentItemIndex = sortedItems.IndexOf(parentItem);
            if(item.SortOrder < parentItem.SortOrder)
            {
                sortedItems.Insert(parentItemIndex + 1, item);
            }
            else
            {
                sortedItems.Insert(parentItemIndex, item);
            }

            int orderNo = 1;
            foreach (IMenuItem sortedItem in sortedItems)
            {
                sortedItem.SortOrder = orderNo++;
                sortedItem.Save();
            }
        }
        else
        {
            if (item.ItemType == MenuItemType.Category && parentItem.ItemType == MenuItemType.Category)
            {
                CategoryEntity category = (CategoryEntity)item;
                category.ParentCategoryId = (parentItem as CategoryEntity).CategoryId;
                category.Save();
            }
            else if (item.ItemType == MenuItemType.Product && parentItem.ItemType == MenuItemType.Category)
            {
                ProductCategoryEntity productCategory = (ProductCategoryEntity)item;
                productCategory.CategoryId = (parentItem as CategoryEntity).CategoryId;
                productCategory.Save();
            }
        }
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(string itemId)
    {
        IMenuItem item = this.DataSource.FirstOrDefault(x => x.ItemId == itemId);
        if (item != null)
        {
            this.DataSource.Remove(item);
            item.Delete();
        }
    }

    private List<IMenuItem> dataSource;
    public List<IMenuItem> DataSource
    {
        get
        {
            if (this.dataSource == null)
            {
                this.dataSource = new List<IMenuItem>();

                PrefetchPath path = new PrefetchPath(EntityType.MenuEntity);

                List<IPrefetchPathElement> categoryPaths = new List<IPrefetchPathElement>();
                for (int i = 0; i < 8; i++)
                {
                    IPrefetchPathElement categoryPath;
                    if (i == 0)
                        categoryPath = MenuEntityBase.PrefetchPathCategoryCollection;
                    else
                        categoryPath = CategoryEntityBase.PrefetchPathChildCategoryCollection;

                    // Category > ProductCategory
                    // Category > ProductCategory > Product
                    IPrefetchPathElement prefetchCategoryProductCategory = categoryPath.SubPath.Add(CategoryEntityBase.PrefetchPathProductCategoryCollection);
                    IPrefetchPathElement prefetchCategoryProductCategoryProduct = prefetchCategoryProductCategory.SubPath.Add(ProductCategoryEntityBase.PrefetchPathProductEntity);

                    categoryPaths.Add(categoryPath);
                }
                path.Add(categoryPaths[0]).SubPath.Add(categoryPaths[1]).SubPath.Add(categoryPaths[2]).SubPath.Add(categoryPaths[3]).SubPath.Add(categoryPaths[4]).SubPath.Add(categoryPaths[5]).SubPath.Add(categoryPaths[6]).SubPath.Add(categoryPaths[7]);

                this.menuEntity = new MenuEntity(this.menuId, path);
                if (!this.menuEntity.IsNew)
                {
                    this.dataSource.AddRange(this.menuEntity.CategoryCollection);
                    foreach (CategoryEntity category in this.menuEntity.CategoryCollection)
                    {
                        this.dataSource.AddRange(category.ProductCategoryCollection);
                    }
                }
            }

            return this.dataSource;
        }
    }

    public void DraggedToSort(string draggedPageId, string draggedUponPageId)
    {
        // Find both items
        IMenuItem itemDragged = this.DataSource.FirstOrDefault(x => x.ItemId == draggedPageId);
        IMenuItem itemDraggedUpon = this.DataSource.FirstOrDefault(x => x.ItemId == draggedUponPageId);

        bool draggedDown = false;
        if (itemDraggedUpon == null && itemDragged is CategoryEntity)
        {
            CategoryEntity category = itemDragged as CategoryEntity;            
            category.ParentCategoryId = null;
            category.SortOrder = category.MenuEntity.CategoryCollection.Count > 0 ? category.MenuEntity.CategoryCollection.Max(cat => cat.SortOrder) + 1 : 1;
            category.Save();
        }
        else if (itemDraggedUpon != null && itemDragged.ParentItemId == itemDraggedUpon.ParentItemId)
        {
            // Sorting within a group of childeren is a bit more advanced, so 
            // that both up and down dragging can be used. 
            // (Instead of always putting the dragged item above the item it was dragged upon)
            if (itemDragged.SortOrder < itemDraggedUpon.SortOrder)
            {
                // It was above it, so it's dragged down
                draggedDown = true;
            }

            // Put the items in the correct order - then renumber the whole lot
            // Get all pages for this parent page
            List<IMenuItem> pages = this.DataSource.Where(x => x.ParentItemId == itemDragged.ParentItemId).OrderBy(x => x.SortOrder).ToList();

            // Remove the dragged page from the list (we're going to insert that again on it's 
            // correct new place, and then re-number all sort orders.
            pages.Remove(itemDragged);

            for (int i = 0; i < pages.Count; i++)
            {
                if (pages[i] == itemDraggedUpon)
                {
                    if (draggedDown)
                    {
                        // Place it below this item
                        pages.Insert(i + 1, itemDragged);
                    }
                    else
                    {
                        // Place it in front of this item.
                        pages.Insert(i, itemDragged);
                    }
                    break;
                }
            }

            int orderNo = 1;
            foreach (IMenuItem page in pages)
            {
                page.SortOrder = orderNo;
                page.Save();
                orderNo++;
            }
        }
        else if (itemDraggedUpon is CategoryEntity)
        {
            // Use the same parent as the one thats dragged upon 
            // If that's possible (can't have a category in a category which has products setup)
            CategoryEntity parentCategory = itemDraggedUpon as CategoryEntity;
                
            if (itemDragged is CategoryEntity && parentCategory.ProductCategoryCollection.Count == 0)
            {
                // Category is dragged within a category without products
                CategoryEntity draggedCategory = itemDragged as CategoryEntity;
                draggedCategory.ParentCategoryId = parentCategory.CategoryId;
                draggedCategory.SortOrder = parentCategory.ChildCategoryCollection.Count > 0 ? parentCategory.ChildCategoryCollection.Max(cat => cat.SortOrder) + 1 : 1;
                draggedCategory.Save();

                // Update tags
                CategoryTagHelper.UpdateProductCategoryTags(draggedCategory);
            }
            else if (itemDragged is ProductCategoryEntity && parentCategory.ChildCategoryCollection.Count == 0)
            {
                // Product is dragged within a category without categories
                ProductCategoryEntity draggedProductCategory = itemDragged as ProductCategoryEntity;
                draggedProductCategory.CategoryId = parentCategory.CategoryId;
                draggedProductCategory.SortOrder = parentCategory.ProductCategoryCollection.Count > 0 ? parentCategory.ProductCategoryCollection.Max(pc => pc.SortOrder) + 1 : 1;
                draggedProductCategory.Save();
            }
        }        
    }
}