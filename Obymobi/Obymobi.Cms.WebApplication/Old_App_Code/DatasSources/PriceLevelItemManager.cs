﻿using System.Collections;
using System.ComponentModel;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using System;
using System.Collections.Generic;
using Dionysos;
using Dionysos.Data.LLBLGen;

public class PriceLevelItemManager
{
    private const string CATEGORY_KEY = "CATEGORY_{0}";
    private const string PRODUCT_KEY = "PRODUCT_{0}";
    private const string ALTERATION_KEY = "ALTERATION_{0}";
    private const string ALTERATIONOPTION_KEY = "ALTERATIONOPTION_{0}";

    private readonly int priceLevelId;    

    public PriceLevelItemManager(int priceLevelId)
    {
        this.priceLevelId = priceLevelId;
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public PriceLevelItemModelCollection Select()
    {
        return this.DataSource;
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string name, decimal price, string itemId)
    {
        if (name.StartsWith("category", StringComparison.InvariantCultureIgnoreCase))
        {
            int categoryId;
            if (int.TryParse(name.Replace("CATEGORY_", ""), out categoryId))
            {
                PrefetchPath prefetch = new PrefetchPath(EntityType.CategoryEntity);
                prefetch.Add(CategoryEntityBase.PrefetchPathProductCategoryCollection).SubPath.Add(ProductCategoryEntityBase.PrefetchPathProductEntity);
                CategoryEntity category = new CategoryEntity(categoryId, prefetch);
                foreach (ProductCategoryEntity productCategory in category.ProductCategoryCollection)
                {
                    this.InsertProductCategory(productCategory.ProductCategoryId, productCategory.ProductEntity.PriceIn.Value);
                }
            }
        }
        else if (name.StartsWith("product", StringComparison.InvariantCultureIgnoreCase))
        {
            int productCategoryId;
            if (itemId.IsNullOrWhiteSpace() && int.TryParse(name.Replace("PRODUCT_", ""), out productCategoryId))
            {
                this.InsertProductCategory(productCategoryId, price);
                
            }
        }
        this.dataSource = null;
    }

    private void InsertProductCategory(int productCategoryId, decimal price)
    {
        ProductCategoryEntity productCategory = new ProductCategoryEntity(productCategoryId);
        if (productCategory != null && !this.PriceLevelItemExistsForProduct(productCategory.ProductId))
        {
            PriceLevelItemEntity priceLevelItemEntity = new PriceLevelItemEntity();
            priceLevelItemEntity.PriceLevelId = this.priceLevelId;
            priceLevelItemEntity.ProductId = productCategory.ProductId;
            priceLevelItemEntity.Price = price;
            priceLevelItemEntity.Save();
        }
    }

    private bool PriceLevelItemExistsForProduct(int productId)
    {
        PredicateExpression filter = new PredicateExpression(PriceLevelItemFields.PriceLevelId == this.priceLevelId);
        filter.Add(PriceLevelItemFields.ProductId == productId);
        PriceLevelItemCollection items = new PriceLevelItemCollection();
        return items.GetDbCount(filter) > 0;
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string itemId, string name, decimal price)
    {
        PriceLevelItemModel model = this.DataSource.GetByItemId(itemId);
        if (model != null && model.Type != PriceLevelItemModelType.Alteration)
        {
            PriceLevelItemEntity priceLevelItemEntity = model.Entity.PriceLevelItemCollection.FirstOrDefault(x => x.PriceLevelId == this.priceLevelId);
            if (priceLevelItemEntity == null)
            {
                priceLevelItemEntity = new PriceLevelItemEntity();
                priceLevelItemEntity.PriceLevelId = this.priceLevelId;

                if (model.Type == PriceLevelItemModelType.Product)
                    priceLevelItemEntity.ProductId = int.Parse(model.Entity.PrimaryKeyFields[0].CurrentValue.ToString());
                else if (model.Type == PriceLevelItemModelType.Alteration)
                    priceLevelItemEntity.AlterationId = int.Parse(model.Entity.PrimaryKeyFields[0].CurrentValue.ToString());
                else if (model.Type == PriceLevelItemModelType.Alterationoption)
                    priceLevelItemEntity.AlterationoptionId = int.Parse(model.Entity.PrimaryKeyFields[0].CurrentValue.ToString());
            }
            priceLevelItemEntity.Price = price;
            priceLevelItemEntity.Save();
            model.Price = price.ToString("N2");
        }        
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(string itemId)
    {
        PriceLevelItemModel model = this.DataSource.GetByItemId(itemId);
        if (model != null)
        {
            this.DataSource.Remove(model);

            EntityView<PriceLevelItemEntity> priceView = model.Entity.PriceLevelItemCollection.DefaultView;
            if (priceView.Count > 0)
            {
                priceView[0].Delete();
                priceView.RelatedCollection.Remove(priceView[0]);                
            }
        }
    }

    private PriceLevelItemModelCollection dataSource;
    public PriceLevelItemModelCollection DataSource
    {
        get
        {
            if (this.dataSource == null)
            {                
                PredicateExpression filter = new PredicateExpression();
                filter.Add(PriceLevelItemFields.PriceLevelId == this.priceLevelId);
                filter.Add(PriceLevelItemFields.ProductId != DBNull.Value);
                                
                ISortExpression sorter = new SortExpression(new SortClause(ProductFields.Name, SortOperator.Ascending));

                RelationCollection relations = new RelationCollection();
                relations.Add(PriceLevelItemEntityBase.Relations.ProductEntityUsingProductId);

                PrefetchPath prefetch = new PrefetchPath(EntityType.PriceLevelItemEntity);
                IPrefetchPathElement prefetchProduct = prefetch.Add(PriceLevelItemEntityBase.PrefetchPathProductEntity, 0, filter, relations, sorter);
                IPrefetchPathElement prefetchProductPriceLevelItems = prefetchProduct.SubPath.Add(ProductEntityBase.PrefetchPathPriceLevelItemCollection);
                IPrefetchPathElement prefetchProductAlterationProducts = prefetchProduct.SubPath.Add(ProductEntityBase.PrefetchPathAlterationProductCollection);
                IPrefetchPathElement prefetchProductAlterations = prefetchProduct.SubPath.Add(ProductEntityBase.PrefetchPathProductAlterationCollection);
                IPrefetchPathElement prefetchProductAlterationsAlteration = prefetchProductAlterations.SubPath.Add(ProductAlterationEntityBase.PrefetchPathAlterationEntity);

                List<IPrefetchPathElement> alterationPaths = new List<IPrefetchPathElement>();
                for (int i = 0; i < 4; i++)
                {
                    IPrefetchPathElement alterationPath = AlterationEntityBase.PrefetchPathAlterationCollection;

                    // Alteration > Alterationitem
                    // Alteration > Alterationitem > Alterationoption
                    // Alteration > Alterationitem > Alterationoption > PriceLevelItem
                    IPrefetchPathElement prefetchProductAlterationsAlterationItems = alterationPath.SubPath.Add(AlterationEntityBase.PrefetchPathAlterationitemCollection);
                    IPrefetchPathElement prefetchProductAlterationsAlterationItemsOptions = prefetchProductAlterationsAlterationItems.SubPath.Add(AlterationitemEntityBase.PrefetchPathAlterationoptionEntity);
                    IPrefetchPathElement prefetchProductAlterationsAlterationItemsOptionsPriceLevelItems = prefetchProductAlterationsAlterationItemsOptions.SubPath.Add(AlterationoptionEntityBase.PrefetchPathPriceLevelItemCollection);

                    // Alteration > AlterationProduct
                    // Alteration > AlterationProduct > Product
                    // Alteration > AlterationProduct > Product > PriceLevelItem
                    // Alteration > AlterationProduct > Product > ProductAlterations
                    // Alteration > AlterationProduct > Product > ProductAlterations > Alteration
                    // Alteration > AlterationProduct > Product > ProductAlterations > Alteration > Alterationitem
                    // Alteration > AlterationProduct > Product > ProductAlterations > Alteration > Alterationitem > Alterationoption
                    // Alteration > AlterationProduct > Product > ProductAlterations > Alteration > Alterationitem > Alterationoption > PriceLevelItem
                    IPrefetchPathElement alterationAlterationProduct = alterationPath.SubPath.Add(AlterationEntityBase.PrefetchPathAlterationProductCollection);
                    IPrefetchPathElement alterationAlterationProductProduct = alterationAlterationProduct.SubPath.Add(AlterationProductEntityBase.PrefetchPathProductEntity);
                    IPrefetchPathElement alterationAlterationProductProductPriceLevelItems = alterationAlterationProductProduct.SubPath.Add(ProductEntityBase.PrefetchPathPriceLevelItemCollection);
                    IPrefetchPathElement alterationAlterationProductProductProductAlterations = alterationAlterationProductProduct.SubPath.Add(ProductEntityBase.PrefetchPathProductAlterationCollection);
                    IPrefetchPathElement alterationAlterationProductProductProductAlterationsAlteration = alterationAlterationProductProductProductAlterations.SubPath.Add(ProductAlterationEntityBase.PrefetchPathAlterationEntity);
                    IPrefetchPathElement alterationAlterationProductProductProductAlterationsAlterationAlterationitem = alterationAlterationProductProductProductAlterationsAlteration.SubPath.Add(AlterationEntityBase.PrefetchPathAlterationitemCollection);
                    IPrefetchPathElement alterationAlterationProductProductProductAlterationsAlterationAlterationitemAlterationoption = alterationAlterationProductProductProductAlterationsAlterationAlterationitem.SubPath.Add(AlterationitemEntityBase.PrefetchPathAlterationoptionEntity);
                    IPrefetchPathElement alterationAlterationProductProductProductAlterationsAlterationAlterationitemAlterationoptionPriceLevelItems = alterationAlterationProductProductProductAlterationsAlterationAlterationitemAlterationoption.SubPath.Add(AlterationoptionEntityBase.PrefetchPathPriceLevelItemCollection);

                    alterationPaths.Add(alterationPath);
                }
                prefetchProductAlterationsAlteration.SubPath.Add(alterationPaths[0]).SubPath.Add(alterationPaths[1]).SubPath.Add(alterationPaths[2]).SubPath.Add(alterationPaths[3]);

                PriceLevelItemCollection items = new PriceLevelItemCollection();
                items.GetMulti(filter, 0, null, relations, prefetch);

                this.dataSource = this.CreatePriceLevelItemModelCollection(items);
            }

            return this.dataSource;
        }
    }

    private PriceLevelItemModelCollection CreatePriceLevelItemModelCollection(PriceLevelItemCollection entityCollection)
    {
        PriceLevelItemModelCollection modelCollection = new PriceLevelItemModelCollection();

        foreach (PriceLevelItemEntity priceLevelItemEntity in entityCollection)
        {
            ProductEntity productEntity = priceLevelItemEntity.ProductEntity;
            if (productEntity != null)
            {
                // Product
                this.AddProduct(string.Empty, productEntity, ref modelCollection);
            }
        }

        return modelCollection;
    }    

    private void AddProduct(string parentItemId, ProductEntity product, ref PriceLevelItemModelCollection modelCollection)
    {
        // Product
        PriceLevelItemEntity priceLevelItemEntity = product.PriceLevelItemCollection.FirstOrDefault(x => x.PriceLevelId == this.priceLevelId);
        string itemId = PriceLevelItemManager.GetItemId(product.ProductId, PriceLevelItemModelType.Product);
        PriceLevelItemModel model = new PriceLevelItemModel(product)
        {
            ItemId = string.Format("{0}-{1}", itemId, parentItemId),
            ParentItemId = parentItemId,
            Name = product.Name,
            Price = priceLevelItemEntity != null ? priceLevelItemEntity.Price.ToString("N2") : product.PriceIn.Value.ToString("N2"),
            Type = PriceLevelItemModelType.Product
        };
        modelCollection.Add(model);

        // Alterations
        foreach (ProductAlterationEntity productAlteration in product.ProductAlterationCollection)
        {
            AlterationCollection alterations = this.GetChildAlterationsWithChildren(productAlteration.AlterationEntity);
            if (alterations.Count > 0)
            {
                foreach (AlterationEntity alteration in alterations)
                {
                    this.AddAlteration(model.ItemId, alteration, ref modelCollection);
                }
            }
        }        
    }

    private void AddAlteration(string parentItemId, AlterationEntity alteration, ref PriceLevelItemModelCollection modelCollection)
    {
        // Alteration
        string itemId = PriceLevelItemManager.GetItemId(alteration.AlterationId, PriceLevelItemModelType.Alteration);
        PriceLevelItemModel model = new PriceLevelItemModel(alteration)
        {
            ItemId = string.Format("{0}-{1}", itemId, parentItemId),
            ParentItemId = parentItemId,
            Name = alteration.Name,
            Price = string.Empty,
            Type = PriceLevelItemModelType.Alteration
        };
        modelCollection.Add(model);

        if (alteration.AlterationitemCollection.Count > 0)
        {
            // Alteration options
            foreach (AlterationitemEntity alterationitem in alteration.AlterationitemCollection.OrderBy(x => x.SortOrder))
            {
                this.AddAlterationoption(model.ItemId, alterationitem, ref modelCollection);                
            }
        }
        else if (alteration.AlterationProductCollection.Count > 0)
        {
            // Alteration products
            foreach (AlterationProductEntity alterationProduct in alteration.AlterationProductCollection.OrderBy(x => x.SortOrder))
            {
                this.AddProduct(model.ItemId, alterationProduct.ProductEntity, ref modelCollection);
            }
        }
        else if (alteration.AlterationCollection.Count > 0)
        {
            // Child alterations
            foreach (AlterationEntity childAlteration in alteration.AlterationCollection)
            {
                this.AddAlteration(model.ItemId, childAlteration, ref modelCollection);
            }
        }
    }

    private void AddAlterationoption(string parentItemId, AlterationitemEntity alterationitemEntity, ref PriceLevelItemModelCollection modelCollection)
    {
        // Alteration option
        AlterationoptionEntity alterationoption = alterationitemEntity.AlterationoptionEntity;
        PriceLevelItemEntity priceLevelItemEntity = alterationoption.PriceLevelItemCollection.FirstOrDefault(x => x.PriceLevelId == this.priceLevelId);
        string itemId = PriceLevelItemManager.GetItemId(alterationoption.AlterationoptionId, PriceLevelItemModelType.Alterationoption);
        PriceLevelItemModel model = new PriceLevelItemModel(alterationoption)
        {
            ItemId = string.Format("{0}-{1}", itemId, parentItemId),
            ParentItemId = parentItemId,
            Name = alterationoption.Name,
            Price = priceLevelItemEntity != null ? priceLevelItemEntity.Price.ToString("N2") : alterationoption.PriceIn.GetValueOrDefault(0).ToString("N2"),
            Type = PriceLevelItemModelType.Alterationoption
        };
        modelCollection.Add(model);

        foreach (AlterationitemAlterationEntity alterationitemAlteration in alterationitemEntity.AlterationitemAlterationCollection)
        {
            this.AddAlteration(model.ItemId, alterationitemAlteration.AlterationEntity, ref modelCollection);
        }
    }

    private AlterationCollection GetChildAlterationsWithChildren(AlterationEntity alterationEntity)
    {
        AlterationCollection alterations = new AlterationCollection();

        if (alterationEntity.AlterationitemCollection.Count > 0)
        {
            alterations.Add(alterationEntity);
        }
        else if (alterationEntity.AlterationProductCollection.Count > 0)
        {
            alterations.Add(alterationEntity);
        }
        else if (alterationEntity.AlterationCollection.Count > 0)
        {
            foreach (AlterationEntity childAlteration in alterationEntity.AlterationCollection)
            {
                alterations.AddRange(this.GetChildAlterationsWithChildren(childAlteration));
            }
        }

        return alterations;
    }

    public static string GetItemId(int primaryId, PriceLevelItemModelType type)
    {
        switch (type)
        {
            case PriceLevelItemModelType.Category:
                return string.Format(PriceLevelItemManager.CATEGORY_KEY, primaryId);
            case PriceLevelItemModelType.Product:
                return string.Format(PriceLevelItemManager.PRODUCT_KEY, primaryId);
            case PriceLevelItemModelType.Alteration:
                return string.Format(PriceLevelItemManager.ALTERATION_KEY, primaryId);
            case PriceLevelItemModelType.Alterationoption:
                return string.Format(PriceLevelItemManager.ALTERATIONOPTION_KEY, primaryId);
            default:
                throw new Exception("Unknown type");
        }
    }

    public class PriceLevelItemModelCollection : List<PriceLevelItemModel>
    {
        public PriceLevelItemModel GetByItemId(string itemId)
        {
            return this.SingleOrDefault(x => x.ItemId == itemId);
        }
    }

    public class PriceLevelItemModel
    {
        public PriceLevelItemModel(IPriceContainingEntity entity)
        {
            this.Entity = entity;
        }

        public IPriceContainingEntity Entity { get; set; }
        public string ItemId { get; set; }
        public string ParentItemId { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public PriceLevelItemModelType Type { get; set; }

        public bool Save()
        {
            return this.Entity.Save();
        }

        public bool Delete()
        {
            return this.Entity.Delete();
        }
    }
    
    public enum PriceLevelItemModelType : int
    {
        Category = 0,
        Product = 1,
        Alteration = 2,
        Alterationoption = 3
    }
}