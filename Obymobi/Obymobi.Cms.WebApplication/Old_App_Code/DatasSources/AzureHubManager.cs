﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

/// <summary>
/// Summary description for AzureHubManager
/// </summary>
public class AzureHubManager
{
    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource.OrderBy(x => x.HubPath);
    }

    [DataObjectMethod(DataObjectMethodType.Insert, true)]
    public void Insert(string connectionString, string hubPath)
    {
        // Save to database
        var hub = new AzureNotificationHubEntity();
        hub.ConnectionString = connectionString.Trim();
        hub.HubPath = hubPath.Trim();
        if (hub.Save())
        {
            this.DataSource.Add(hub);
        }
    }

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string connectionString, string hubPath, int sheit_NotificationHubId)
    {
        var hubInDataSource = this.DataSource.FirstOrDefault(x => x.NotificationHubId == sheit_NotificationHubId);

        if (hubInDataSource != null)
        {
            var oldConnectionString = hubInDataSource.ConnectionString;
            var oldHubPath = hubInDataSource.HubPath;

            hubInDataSource.ConnectionString = connectionString.Trim();
            hubInDataSource.HubPath = hubPath.Trim();
            if (!hubInDataSource.Save())
            {
                hubInDataSource.ConnectionString = oldConnectionString;
                hubInDataSource.HubPath = oldHubPath;
            }
        }
        else
            throw new Exception(string.Format("Hub with id '{0}' is not available.", sheit_NotificationHubId));
    }

    [DataObjectMethod(DataObjectMethodType.Delete, true)]
    public void Delete(int sheit_NotificationHubId)
    {
        var hubInDataSource = this.DataSource.FirstOrDefault(x => x.NotificationHubId == sheit_NotificationHubId);

        if (hubInDataSource != null)
        {
            this.DataSource.Remove(hubInDataSource);
            hubInDataSource.Delete();
        }
        else
            throw new Exception(string.Format("Hub with id '{0}' is not available.", sheit_NotificationHubId));
    }

    private List<AzureNotificationHubEntity> dataSource;
    private List<AzureNotificationHubEntity> DataSource
    {
        get
        {
            if (this.dataSource == null)
            {
                this.dataSource = new List<AzureNotificationHubEntity>();

                var hubCollection = new AzureNotificationHubCollection();
                hubCollection.GetMulti(null);

                this.dataSource.AddRange(hubCollection);
            }

            return this.dataSource;
        }
    }
}