﻿using System.Collections.Generic;
using System.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Cms.WebApplication
{
    public class DeliveryDistanceDataSource : ObjectDataSource
    {
        #region Fields

        public DeliveryDistanceManager Manager;

        #endregion

        #region Constructors

        public DeliveryDistanceDataSource(int serviceMethodId)
        {
            this.TypeName = typeof(DeliveryDistanceManager).FullName;
            this.Manager = DeliveryDistanceManager.Create(serviceMethodId);

            this.DeleteMethod = "Delete";
            this.InsertMethod = "Insert";
            this.SelectMethod = "Select";
            this.UpdateMethod = "Update";

            this.ObjectCreating += this.PageDataSource_ObjectCreating;
        }

        #endregion

        #region Events

        private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e) => e.ObjectInstance = this.Manager;

        #endregion

        #region Properties

        public List<DeliveryDistanceEntity> DataSource => this.Manager != null ? this.Manager.DataSource : new List<DeliveryDistanceEntity>();

        #endregion
    }
}