﻿using System.Collections.Generic;
using System.Web.UI.WebControls;
using Obymobi.Logic.Cms;

public class AlterationDataSource : ObjectDataSource
{
    public readonly AlterationManager AlterationManager;

    public AlterationDataSource(int alterationId)
    {
        this.TypeName = typeof(AlterationManager).FullName;
        this.AlterationManager = new AlterationManager(alterationId);

        this.InsertMethod = "Insert";
        this.UpdateMethod = "Update";
        this.DeleteMethod = "Delete";
        this.SelectMethod = "Select";

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.AlterationManager;
    }

    public List<IAlterationComponent> DataSource
    {
        get { return this.AlterationManager != null ? this.AlterationManager.DataSource : new List<IAlterationComponent>(); }
    }
}