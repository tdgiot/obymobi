﻿public sealed class OptionType
{
    public static readonly string Product = "Product";
    public static readonly string Productgroup = "Productgroup";
    public static readonly string Alteration = "Alteration";    
    public static readonly string Alterationoption = "Alterationoption";    
}