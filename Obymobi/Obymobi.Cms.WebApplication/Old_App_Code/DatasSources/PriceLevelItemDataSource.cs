﻿using System.Web.UI.WebControls;

public class PriceLevelItemDataSource : ObjectDataSource
{
    public readonly PriceLevelItemManager Manager;

    public PriceLevelItemDataSource(int priceLevelId)
    {
        this.TypeName = typeof(PriceLevelItemManager).FullName;
        this.Manager = new PriceLevelItemManager(priceLevelId);

        this.InsertMethod = "Insert";
        this.UpdateMethod = "Update";        
        this.SelectMethod = "Select";
        this.DeleteMethod = "Delete";

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.Manager;
    }

    public PriceLevelItemManager.PriceLevelItemModelCollection DataSource
    {
        get { return this.Manager != null ? this.Manager.DataSource : new PriceLevelItemManager.PriceLevelItemModelCollection(); }
    }
}