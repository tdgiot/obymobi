﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Cms.WebApplication.Old_App_Code.DatasSources.MenuPosItems;
using Obymobi.Data.HelperClasses;

public class MenuPosItemManager
{
    private int menuId { get; set; }
    public List<MenuPosItem> DataSource { get; set; } = new List<MenuPosItem>();

    public MenuPosItemManager(int menuId)
    {
        this.menuId = menuId;
        this.RefreshDataSource();
    }

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource.OrderBy(x => x.SortOrder).ToList();
    }    

    [DataObjectMethod(DataObjectMethodType.Update, true)]
    public void Update(string menuPosItemId, string name, string posItemName)
    {
        MenuPosItem item = this.DataSource.FirstOrDefault(x => x.MenuPosItemId.Equals(menuPosItemId, System.StringComparison.InvariantCultureIgnoreCase));
        if (item != null)
        {
            if (item.EntityType.Equals(nameof(CategoryEntity)))
            {
                CategoryEntity entity = new CategoryEntity(item.PrimaryKey);
                entity.PoscategoryId = int.Parse(posItemName);
                entity.Save();
            }
            else if (item.EntityType.Equals(nameof(ProductEntity)))
            {
                ProductEntity entity = new ProductEntity(item.PrimaryKey);
                entity.PosproductId = int.Parse(posItemName);
                entity.Save();
            }
            else if (item.EntityType.Equals(nameof(AlterationEntity)))
            {
                AlterationEntity entity = new AlterationEntity(item.PrimaryKey);
                entity.PosalterationId = int.Parse(posItemName);
                entity.Save();
            }
            else if (item.EntityType.Equals(nameof(AlterationoptionEntity)))
            {
                AlterationoptionEntity entity = new AlterationoptionEntity(item.PrimaryKey);
                entity.PosalterationoptionId = int.Parse(posItemName);
                entity.Save();
            }

            this.RefreshDataSource();
        }
    }   

    public void RefreshDataSource()
    {
        MenuEntity menuEntity = this.FetchMenu(this.menuId);
        this.DataSource = new MenuPosItemConverter(menuEntity).Convert().ToList();
    }

    private MenuEntity FetchMenu(int menuId)
    {
        PrefetchPath prefetchPath = new PrefetchPath(EntityType.MenuEntity);

        // Root categories
        IPrefetchPathElement prefetchCategory = prefetchPath.Add(MenuEntity.PrefetchPathCategoryCollection, new IncludeFieldsList(CategoryFields.Name, CategoryFields.PoscategoryId, CategoryFields.SortOrder));
        IPrefetchPathElement prefetchCategoryPoscategory = prefetchCategory.SubPath.Add(CategoryEntity.PrefetchPathPoscategoryEntity, new IncludeFieldsList(PoscategoryFields.Name));

        // Products
        IPrefetchPathElement prefetchProductCategory = prefetchCategory.SubPath.Add(CategoryEntity.PrefetchPathProductCategoryCollection, new IncludeFieldsList(ProductCategoryFields.ProductId, ProductCategoryFields.SortOrder));
        IPrefetchPathElement prefetchProduct = prefetchProductCategory.SubPath.Add(ProductCategoryEntity.PrefetchPathProductEntity, new IncludeFieldsList(ProductFields.Name, ProductFields.PosproductId));
        IPrefetchPathElement prefetchPosproduct = prefetchProduct.SubPath.Add(ProductEntity.PrefetchPathPosproductEntity, new IncludeFieldsList(PosproductFields.Name, PosproductFields.ExternalId, PosproductFields.RevenueCenter));

        // Alterations
        IPrefetchPathElement prefetchProductAlteration = prefetchProduct.SubPath.Add(ProductEntity.PrefetchPathProductAlterationCollection, new IncludeFieldsList(ProductAlterationFields.ProductId, ProductAlterationFields.SortOrder));
        IPrefetchPathElement prefetchAlteration = prefetchProductAlteration.SubPath.Add(ProductAlterationEntity.PrefetchPathAlterationEntity, new IncludeFieldsList(AlterationFields.Name, AlterationFields.PosalterationId));
        IPrefetchPathElement prefetchPosalteration = prefetchAlteration.SubPath.Add(AlterationEntity.PrefetchPathPosAlterationEntity, new IncludeFieldsList(PosalterationFields.Name, PosalterationFields.ExternalId, PosalterationFields.RevenueCenter));

        // Alterationoptions
        IPrefetchPathElement prefetchAlterationitem = prefetchAlteration.SubPath.Add(AlterationEntity.PrefetchPathAlterationitemCollection, new IncludeFieldsList(AlterationitemFields.AlterationoptionId, AlterationitemFields.SortOrder));
        IPrefetchPathElement prefetchAlterationoption = prefetchAlterationitem.SubPath.Add(AlterationitemEntity.PrefetchPathAlterationoptionEntity, new IncludeFieldsList(AlterationoptionFields.Name));
        IPrefetchPathElement prefetchAlterationoptionPosAlterationoption = prefetchAlterationoption.SubPath.Add(AlterationoptionEntity.PrefetchPathPosalterationoptionEntity, new IncludeFieldsList(PosalterationoptionFields.Name, PosalterationoptionFields.ExternalId, PosalterationoptionFields.RevenueCenter));
        IPrefetchPathElement prefetchAlterationoptionPosproduct = prefetchAlterationoption.SubPath.Add(AlterationoptionEntity.PrefetchPathPosproductEntity, new IncludeFieldsList(PosproductFields.Name));

        return new MenuEntity(this.menuId, prefetchPath);
    }
}