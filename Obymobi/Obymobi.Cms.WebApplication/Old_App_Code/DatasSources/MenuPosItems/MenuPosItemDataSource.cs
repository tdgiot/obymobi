﻿using System.Collections.Generic;
using System.Web.UI.WebControls;
using Obymobi.Cms.WebApplication.Old_App_Code.DatasSources.MenuPosItems;

public class MenuPosItemDataSource : ObjectDataSource
{
    private readonly MenuPosItemManager menuPosItemManager;

    public MenuPosItemDataSource(int menuId)
    {
        this.TypeName = typeof(MenuPosItemManager).FullName;
        this.menuPosItemManager = new MenuPosItemManager(menuId);

        this.UpdateMethod = "Update";
        this.SelectMethod = "Select";

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.menuPosItemManager;
    }

    public List<MenuPosItem> DataSource => this.menuPosItemManager.DataSource;

    public MenuPosItemManager MenuPosItemManager => this.menuPosItemManager;
}