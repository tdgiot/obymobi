﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Cms.WebApplication.Old_App_Code.DatasSources.MenuPosItems
{
    public class MenuPosItemConverter
    {
        private readonly MenuEntity menuEntity;
        private readonly List<MenuPosItem> menuPosItems;

        public MenuPosItemConverter(MenuEntity menuEntity)
        {
            this.menuEntity = menuEntity;
            this.menuPosItems = new List<MenuPosItem>();
        }

        public IEnumerable<MenuPosItem> Convert()
        {
            this.menuPosItems.Clear();
            this.ConvertCategories(this.menuEntity.CategoryCollection);
            return this.menuPosItems;
        }

        private void ConvertCategories(CategoryCollection categoryCollection)
        {
            foreach (CategoryEntity categoryEntity in categoryCollection)
            {
                MenuPosItem menuPosItem = this.ConvertCategory(categoryEntity);
                this.menuPosItems.Add(menuPosItem);

                this.ConvertProducts(categoryEntity.ProductCategoryCollection, menuPosItem);
            }            
        }

        private void ConvertProducts(ProductCategoryCollection productCategoryCollection, MenuPosItem menuPosParentItem)
        {
            foreach (ProductCategoryEntity productCategoryEntity in productCategoryCollection)
            {
                MenuPosItem menuPosItem = this.ConvertProduct(productCategoryEntity, menuPosParentItem);
                this.menuPosItems.Add(menuPosItem);

                this.ConvertAlterations(productCategoryEntity.ProductEntity.ProductAlterationCollection, menuPosItem);
            }
        }

        private void ConvertAlterations(ProductAlterationCollection productAlterationCollection, MenuPosItem menuPosParentItem)
        {
            foreach (ProductAlterationEntity productAlterationEntity in productAlterationCollection)
            {
                MenuPosItem menuPosItem = this.ConvertAlteration(productAlterationEntity, menuPosParentItem);
                this.menuPosItems.Add(menuPosItem);

                this.ConvertAlterationoptions(productAlterationEntity.AlterationEntity.AlterationitemCollection, menuPosItem);
            }
        }

        private void ConvertAlterationoptions(AlterationitemCollection alterationitemCollection, MenuPosItem menuPosParentItem)
        {
            this.menuPosItems.AddRange(alterationitemCollection.Select(x => ConvertAlterationoption(x, menuPosParentItem)));    
        }

        private MenuPosItem ConvertCategory(CategoryEntity categoryEntity)
        {
            return new MenuPosItem
            {
                MenuPosItemId = $"category-{categoryEntity.CategoryId}",
                MenuPosParentItemId = $"category-{categoryEntity.ParentCategoryId}",
                Name = categoryEntity.Name,
                PosItemName = categoryEntity.PoscategoryId.HasValue ? categoryEntity.PoscategoryEntity.Name : string.Empty,
                SortOrder = categoryEntity.SortOrder,
                EntityType = nameof(CategoryEntity),
                PrimaryKey = categoryEntity.CategoryId
            };
        }

        private MenuPosItem ConvertProduct(ProductCategoryEntity productCategoryEntity, MenuPosItem menuPosParentItem)
        {
            string menuPosParentItemId = this.GetMenuPosItemId(menuPosParentItem);

            return new MenuPosItem
            {
                MenuPosItemId = menuPosParentItemId + $"productcategory-{productCategoryEntity.ProductCategoryId}",
                MenuPosParentItemId = menuPosParentItemId,
                Name = productCategoryEntity.ProductEntity.Name,
                PosItemName = productCategoryEntity.ProductEntity.PosproductId.HasValue ? GetMenuPosProductName(productCategoryEntity.ProductEntity.PosproductEntity) : string.Empty,
                SortOrder = productCategoryEntity.SortOrder,
                EntityType = nameof(ProductEntity),
                PrimaryKey = productCategoryEntity.ProductId
            };
        }

        private MenuPosItem ConvertAlteration(ProductAlterationEntity productAlterationEntity, MenuPosItem menuPosParentItem)
        {
            string menuPosParentItemId = this.GetMenuPosItemId(menuPosParentItem);

            return new MenuPosItem
            {
                MenuPosItemId = menuPosParentItemId + $"productalteration-{productAlterationEntity.ProductAlterationId}",
                MenuPosParentItemId = menuPosParentItemId,
                Name = productAlterationEntity.AlterationEntity.Name,
                PosItemName = productAlterationEntity.AlterationEntity.PosalterationId.HasValue ? GetMenuPosalterationName(productAlterationEntity.AlterationEntity.PosAlterationEntity) : string.Empty,
                SortOrder = productAlterationEntity.SortOrder,
                EntityType = nameof(AlterationEntity),
                PrimaryKey = productAlterationEntity.AlterationId
            };
        }

        private MenuPosItem ConvertAlterationoption(AlterationitemEntity alterationitemEntity, MenuPosItem menuPosParentItem)
        {
            string menuPosParentItemId = this.GetMenuPosItemId(menuPosParentItem);

            return new MenuPosItem
            {
                MenuPosItemId = menuPosParentItemId + $"alterationitem-{alterationitemEntity.AlterationitemId}",
                MenuPosParentItemId = menuPosParentItemId,
                Name = alterationitemEntity.AlterationoptionEntity.Name,
                PosItemName = alterationitemEntity.AlterationoptionEntity.PosalterationoptionId.HasValue ? 
                              GetMenuPosAlterationoptionName(alterationitemEntity.AlterationoptionEntity.PosalterationoptionEntity) :
                              string.Empty,
                SortOrder = alterationitemEntity.SortOrder,
                EntityType = nameof(AlterationoptionEntity),
                PrimaryKey = alterationitemEntity.AlterationoptionId
            };
        }

        private string GetMenuPosItemId(MenuPosItem menuPosItem)
        {
            return menuPosItem != null ? menuPosItem.MenuPosItemId : string.Empty;
        }

        private string GetMenuPosProductName(PosproductEntity posProductEntity)
        {
            if (!string.IsNullOrEmpty(posProductEntity.RevenueCenter))
            {
                return $"{posProductEntity.Name} " + $"({posProductEntity.ExternalId}) " + $"[{posProductEntity.RevenueCenter}]";
            }

            return $"{posProductEntity.Name} " + $"({posProductEntity.ExternalId})";
        }

        private string GetMenuPosalterationName(PosalterationEntity posalterationEntity)
        {
            if (!string.IsNullOrEmpty(posalterationEntity.RevenueCenter))
            {
                return $"{posalterationEntity.Name} " + $"({posalterationEntity.ExternalId}) " + $"[{posalterationEntity.RevenueCenter}]";
            }

            return $"{posalterationEntity.Name} " + $"({posalterationEntity.ExternalId})";
        }

        private string GetMenuPosAlterationoptionName(PosalterationoptionEntity posAlterationoptionEntity)
        {
            if (!string.IsNullOrEmpty(posAlterationoptionEntity.RevenueCenter))
            {
                return $"{posAlterationoptionEntity.Name} " + $"({posAlterationoptionEntity.ExternalId}) " + $"[{posAlterationoptionEntity.RevenueCenter}]";
            }

            return $"{posAlterationoptionEntity.Name} " + $"({posAlterationoptionEntity.ExternalId})";
        }
    }
}