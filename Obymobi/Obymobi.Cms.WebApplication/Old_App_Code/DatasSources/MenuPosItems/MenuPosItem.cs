﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Obymobi.Cms.WebApplication.Old_App_Code.DatasSources.MenuPosItems
{
    public class MenuPosItem
    {
        public string MenuPosItemId { get; set; }
        public string MenuPosParentItemId { get; set; }
        public string Name { get; set; }
        public string PosItemName { get; set; }
        public int SortOrder { get; set; }
        public string EntityType { get; set; }
        public int PrimaryKey { get; set; }
    }
}