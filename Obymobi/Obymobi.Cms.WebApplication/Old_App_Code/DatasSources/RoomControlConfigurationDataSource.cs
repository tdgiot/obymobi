﻿using Obymobi.Logic.RoomControl;
using System.Web.UI.WebControls;

public class RoomControlConfigurationDataSource : ObjectDataSource
{
    private readonly RoomControlConfigurationManager roomControlConfigurationManager;

    public RoomControlConfigurationDataSource(int roomControlConfigurationId)
    {
        this.TypeName = typeof(RoomControlConfigurationManager).FullName;
        this.roomControlConfigurationManager = new RoomControlConfigurationManager(roomControlConfigurationId);

        this.InsertMethod = "Insert";
        this.UpdateMethod = "Update";
        this.DeleteMethod = "Delete";
        this.SelectMethod = "Select";

        this.ObjectCreating += PageDataSource_ObjectCreating;
    }

    void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.roomControlConfigurationManager;
    }
}