﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.WebApplication
{
    public class DeliveryDistanceManager
    {
        #region Fields

        private readonly ServiceMethodEntity ServiceMethodEntity;

        #endregion

        #region Constructors

        private DeliveryDistanceManager(int serviceMethodId)
        {
            ServiceMethodEntity serviceMethodEntity = new ServiceMethodEntity(serviceMethodId);
            if (serviceMethodEntity.IsNew)
            {
                throw new KeyNotFoundException("The service method doesn't exist.");
            }

            this.ServiceMethodEntity = serviceMethodEntity;

            this.RefreshDataSource();
        }

        #endregion

        #region Properties

        public List<DeliveryDistanceEntity> DataSource { get; set; } = new List<DeliveryDistanceEntity>();

        #endregion

        #region Statics

        public static DeliveryDistanceManager Create(int serviceMethodId) => new DeliveryDistanceManager(serviceMethodId);

        #endregion

        #region Methods

        public void RefreshDataSource()
        {
            PredicateExpression filter = new PredicateExpression(DeliveryDistanceFields.ServiceMethodId == this.ServiceMethodEntity.ServiceMethodId);

            DeliveryDistanceCollection deliveryDistanceCollection = new DeliveryDistanceCollection();
            deliveryDistanceCollection.GetMulti(filter);

            this.DataSource = deliveryDistanceCollection.ToList();
        }

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public IEnumerable Select() => this.DataSource.OrderBy(deliveryDistance => deliveryDistance.MaximumInMetres);

        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public void Insert(int? maximumInMetres, decimal? price)
        {
            this.ValidateSubmittedFields(maximumInMetres, price);

            DeliveryDistanceEntity deliveryDistanceEntity = new DeliveryDistanceEntity();
            deliveryDistanceEntity.ServiceMethodId = this.ServiceMethodEntity.ServiceMethodId;
            deliveryDistanceEntity.MaximumInMetres = maximumInMetres.Value;
            deliveryDistanceEntity.Price = price.Value;
            deliveryDistanceEntity.Save(false);

            this.DataSource.Add(deliveryDistanceEntity);
        }

        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public void Update(int deliveryDistanceId, int? maximumInMetres, decimal? price)
        {
            this.ValidateSubmittedFields(maximumInMetres, price);

            DeliveryDistanceEntity deliveryDistanceEntity = this.DataSource.FirstOrDefault(x => x.DeliveryDistanceId == deliveryDistanceId);
            if (deliveryDistanceEntity == null)
            {
                return;
            }

            deliveryDistanceEntity.MaximumInMetres = maximumInMetres.Value;
            deliveryDistanceEntity.Price = price.Value;
            deliveryDistanceEntity.Save(false);
        }

        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public void Delete(int deliveryDistanceId)
        {
            DeliveryDistanceEntity deliveryDistanceEntity = this.DataSource
                .FirstOrDefault(deliveryDistance => deliveryDistance.DeliveryDistanceId == deliveryDistanceId);

            if (deliveryDistanceEntity == null)
            {
                return;
            }

            if (deliveryDistanceEntity.Delete())
            {
                this.DataSource.Remove(deliveryDistanceEntity);
            }
        }

        private void ValidateSubmittedFields(int? maximumInMetres, decimal? price)
        {
            if (!maximumInMetres.HasValue || maximumInMetres.Value <= 0)
            {
                throw new ArgumentException($"The specified argument {nameof(maximumInMetres)} cannot be null or less or equals to 0.");
            }

            if (!price.HasValue || price.Value < decimal.Zero)
            {
                throw new ArgumentException($"The specified argument {nameof(price)} cannot be null or less than zero (0).");
            }
        }

        #endregion
    }
}