﻿using Obymobi.Data.EntityClasses;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public class LandingPageWidgetDataSource : ObjectDataSource
{
    public readonly LandingPageWidgetManager Manager;    

    public LandingPageWidgetDataSource(int landingPageId)
    {
        this.TypeName = typeof(LandingPageWidgetManager).FullName;
        this.Manager = LandingPageWidgetManager.Create(landingPageId);

        this.UpdateMethod = "Update";
        this.SelectMethod = "Select";
        this.InsertMethod = "Insert";
        this.DeleteMethod = "Delete";

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.Manager;
    }

    public List<LandingPageWidgetEntity> DataSource { get { return this.Manager != null ? this.Manager.DataSource : new List<LandingPageWidgetEntity>(); } }
}