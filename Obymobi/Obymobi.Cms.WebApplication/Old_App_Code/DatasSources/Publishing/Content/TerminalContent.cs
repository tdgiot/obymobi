﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class TerminalContent : ContentBase<TerminalEntity>
{
    private readonly Crave.Api.Logic.DataSources.TerminalDataSource terminalDataSource;

    public TerminalContent(Crave.Api.Logic.DataSources.TerminalDataSource terminalDataSource)
    {
        this.terminalDataSource = terminalDataSource;
    }

    public override IEnumerable<TerminalEntity> GetData(int companyId)
    {
        return this.terminalDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "Terminals",
            Text = "Terminals",
            CmsUrl = "~/Company/Terminals.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(TerminalEntity entity)
    {
        return new[]
        {
            new Content
            {
                ContentId = string.Format("Terminal-{0}", entity.TerminalId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Company/Terminal.aspx?id={0}", entity.TerminalId),
                PublishRequest = new PublishTerminal { TerminalId = entity.TerminalId }
            }
        };
    }
}