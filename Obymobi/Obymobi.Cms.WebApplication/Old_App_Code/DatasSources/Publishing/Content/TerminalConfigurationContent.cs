﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class TerminalConfigurationContent : ContentBase<TerminalConfigurationEntity>
{
    private readonly Crave.Api.Logic.DataSources.TerminalConfigurationDataSource menuDataSource;

    public TerminalConfigurationContent(Crave.Api.Logic.DataSources.TerminalConfigurationDataSource menuDataSource)
    {
        this.menuDataSource = menuDataSource;
    }

    public override IEnumerable<TerminalConfigurationEntity> GetData(int companyId)
    {
        return this.menuDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "TerminalConfigurations",
            Text = "Terminal configurations",
            CmsUrl = "~/Company/TerminalConfigurations.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(TerminalConfigurationEntity entity)
    {
        PublishTerminalConfiguration publishRequest = new PublishTerminalConfiguration { TerminalConfigurationId = entity.TerminalConfigurationId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("TerminalConfiguration-{0}", entity.TerminalConfigurationId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Company/TerminalConfiguration.aspx?id={0}", entity.TerminalConfigurationId),
                PublishRequest = publishRequest
            }
        };
    }
}