﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;
using Crave.Api.Logic.DataSources;

public class DeliveryTimeContent : ContentBase<DeliverypointgroupEntity>
{
    private readonly DeliverypointgroupDataSource deliverypointgroupDataSource;

    public DeliveryTimeContent(DeliverypointgroupDataSource dataSource)
    {
        this.deliverypointgroupDataSource = dataSource;
    }

    public override IEnumerable<DeliverypointgroupEntity> GetData(int companyId)
    {
        return this.deliverypointgroupDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "DeliveryTime",
            Text = "Delivery Time",
            CmsUrl = "~/Company/Deliverypointgroups.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(DeliverypointgroupEntity entity)
    {
        PublishDeliveryTime publishRequest = new PublishDeliveryTime { DeliverypointgroupId = entity.DeliverypointgroupId };

        return new[]
        {
            new Content
            {
                ContentId = string.Format("DeliveryTime-{0}", entity.DeliverypointgroupId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Company/Deliverypointgroup.aspx?id={0}", entity.DeliverypointgroupId),
                PublishRequest = publishRequest
            }
        };
    }
}