﻿using System.Collections.Generic;
using Crave.Api.Logic.DataSources;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;

public class MessageTemplateContent : ContentBase<TerminalEntity>
{
    private readonly TerminalDataSource terminalDataSource;

    public MessageTemplateContent(TerminalDataSource terminalDataSource)
    {
        this.terminalDataSource = terminalDataSource;
    }

    public override IEnumerable<TerminalEntity> GetData(int companyId)
    {
        return this.terminalDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int terminalId)
    {
        return new Content()
        {
            ContentId = "MessageTemplates",
            Text = "Message templates",
            CmsUrl = "~/Company/MessageTemplates.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(TerminalEntity entity)
    {
        PublishMessageTemplates publishRequest = new PublishMessageTemplates { TerminalId = entity.TerminalId };

        return new[]
        {
            new Content
            {
                ContentId = string.Format("MessageTemplates-{0}", entity.TerminalId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Company/Terminal.aspx?id={0}", entity.TerminalId),
                PublishRequest = publishRequest
            }
        };
    }
}
