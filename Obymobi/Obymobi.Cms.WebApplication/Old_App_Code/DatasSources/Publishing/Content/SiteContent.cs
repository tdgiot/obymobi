﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;
using System.Linq;

public class SiteContent : ContentBase<SiteEntity>
{
    private readonly Crave.Api.Logic.DataSources.SiteDataSource siteDataSource;

    public SiteContent(Crave.Api.Logic.DataSources.SiteDataSource siteDataSource)
    {
        this.siteDataSource = siteDataSource;
    }

    public override IEnumerable<SiteEntity> GetData(int companyId)
    {
        return this.siteDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "Sites",
            Text = "Sites",
            CmsUrl = "~/Cms/Sites.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(SiteEntity entity)
    {
        if (!entity.SiteCultureCollection.Any())
            return new List<Content>();

        PublishSite publishRequest = new PublishSite
        {
            SiteId = entity.SiteId,
            CultureCode = entity.SiteCultureCollection[0].CultureCode
        };

        return new []
        {
            new Content
            {
                ContentId = string.Format("Site-{0}", entity.SiteId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Cms/Site.aspx?id={0}", entity.SiteId),
                PublishRequest = publishRequest
            }
        };
    }
}