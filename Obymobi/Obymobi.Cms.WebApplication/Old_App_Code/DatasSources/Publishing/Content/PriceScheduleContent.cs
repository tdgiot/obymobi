﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class PriceScheduleContent : ContentBase<PriceScheduleEntity>
{
    private readonly Crave.Api.Logic.DataSources.PriceScheduleDataSource priceScheduleDataSource;

    public PriceScheduleContent(Crave.Api.Logic.DataSources.PriceScheduleDataSource priceScheduleDataSource)
    {
        this.priceScheduleDataSource = priceScheduleDataSource;
    }

    public override IEnumerable<PriceScheduleEntity> GetData(int companyId)
    {
        return this.priceScheduleDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "PriceSchedules",
            Text = "Price schedules",
            CmsUrl = "~/Catalog/PriceSchedules.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(PriceScheduleEntity entity)
    {
        PublishPriceSchedule publishRequest = new PublishPriceSchedule { PriceScheduleId = entity.PriceScheduleId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("PriceSchedule-{0}", entity.PriceScheduleId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Catalog/PriceSchedule.aspx?id={0}", entity.PriceScheduleId),
                PublishRequest = publishRequest
            }
        };
    }
}