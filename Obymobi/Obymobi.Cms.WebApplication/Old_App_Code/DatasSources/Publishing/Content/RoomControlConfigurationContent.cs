﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class RoomControlConfigurationContent : ContentBase<RoomControlConfigurationEntity>
{
    private readonly Crave.Api.Logic.DataSources.RoomControlConfigurationDataSource roomControlConfigurationDataSource;

    public RoomControlConfigurationContent(Crave.Api.Logic.DataSources.RoomControlConfigurationDataSource roomControlConfigurationDataSource)
    {
        this.roomControlConfigurationDataSource = roomControlConfigurationDataSource;
    }

    public override IEnumerable<RoomControlConfigurationEntity> GetData(int companyId)
    {
        return this.roomControlConfigurationDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "RoomControlConfigurations",
            Text = "Room control configurations",
            CmsUrl = "~/Company/RoomControlConfigurations.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(RoomControlConfigurationEntity entity)
    {
        PublishRoomControlConfiguration publishRequest = new PublishRoomControlConfiguration { RoomControlConfigurationId = entity.RoomControlConfigurationId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("RoomControlConfiguration-{0}", entity.RoomControlConfigurationId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Company/RoomControlConfiguration.aspx?id={0}", entity.RoomControlConfigurationId),
                PublishRequest = publishRequest
            }
        };
    }
}