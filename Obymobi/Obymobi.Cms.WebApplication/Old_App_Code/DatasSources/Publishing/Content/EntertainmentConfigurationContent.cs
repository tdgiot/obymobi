﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class EntertainmentConfigurationContent : ContentBase<EntertainmentConfigurationEntity>
{
    private readonly Crave.Api.Logic.DataSources.EntertainmentConfigurationDataSource menuDataSource;

    public EntertainmentConfigurationContent(Crave.Api.Logic.DataSources.EntertainmentConfigurationDataSource menuDataSource)
    {
        this.menuDataSource = menuDataSource;
    }

    public override IEnumerable<EntertainmentConfigurationEntity> GetData(int companyId)
    {
        return this.menuDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "EntertainmentConfigurations",
            Text = "Entertainments",
            CmsUrl = "~/Generic/EntertainmentConfigurations.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(EntertainmentConfigurationEntity entity)
    {
        PublishEntertainment publishRequest = new PublishEntertainment { EntertainmentConfigurationId = entity.EntertainmentConfigurationId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("EntertainmentConfiguration-{0}", entity.EntertainmentConfigurationId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Generic/EntertainmentConfiguration.aspx?id={0}", entity.EntertainmentConfigurationId),
                PublishRequest = publishRequest
            }
        };
    }
}