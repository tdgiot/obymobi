﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class DeliverypointgroupContent : ContentBase<DeliverypointgroupEntity>
{
    private readonly Crave.Api.Logic.DataSources.DeliverypointgroupDataSource deliverypointgroupDataSource;

    public DeliverypointgroupContent(Crave.Api.Logic.DataSources.DeliverypointgroupDataSource deliverypointgroupDataSource)
    {
        this.deliverypointgroupDataSource = deliverypointgroupDataSource;
    }

    public override IEnumerable<DeliverypointgroupEntity> GetData(int companyId)
    {
        return this.deliverypointgroupDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "Deliverypointgroups",
            Text = "Deliverypoints",
            CmsUrl = "~/Company/Deliverypointgroups.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(DeliverypointgroupEntity entity)
    {
        PublishDeliverypoints publishRequest = new PublishDeliverypoints { DeliverypointgroupId = entity.DeliverypointgroupId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("Deliverypointgroup-{0}", entity.DeliverypointgroupId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Company/Deliverypointgroup.aspx?id={0}", entity.DeliverypointgroupId),
                PublishRequest = publishRequest
            }
        };
    }
}