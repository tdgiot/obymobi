﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class ClientConfigurationContent : ContentBase<ClientConfigurationEntity>
{
    private readonly Crave.Api.Logic.DataSources.ClientConfigurationDataSource clientConfigurationDataSource;

    public ClientConfigurationContent(Crave.Api.Logic.DataSources.ClientConfigurationDataSource clientConfigurationDataSource)
    {
        this.clientConfigurationDataSource = clientConfigurationDataSource;
    }

    public override IEnumerable<ClientConfigurationEntity> GetData(int companyId)
    {
        return this.clientConfigurationDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "ClientConfigurations",
            Text = "Deliverypoint groups",
            CmsUrl = "~/Company/ClientConfigurations.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(ClientConfigurationEntity entity)
    {
        PublishClientConfiguration publishRequest = new PublishClientConfiguration { ClientConfigurationId = entity.ClientConfigurationId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("ClientConfiguration-{0}", entity.ClientConfigurationId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Company/ClientConfiguration.aspx?id={0}", entity.ClientConfigurationId),
                PublishRequest = publishRequest
            }
        };
    }
}