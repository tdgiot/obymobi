﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class MenuContent : ContentBase<MenuEntity>
{
    private readonly Crave.Api.Logic.DataSources.MenuDataSource menuDataSource;

    public MenuContent(Crave.Api.Logic.DataSources.MenuDataSource menuDataSource)
    {
        this.menuDataSource = menuDataSource;
    }

    public override IEnumerable<MenuEntity> GetData(int companyId)
    {
        return this.menuDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "Menus",
            Text = "Menus",
            CmsUrl = "~/Catalog/Menus.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(MenuEntity entity)
    {
        PublishMenu publishMenu = new PublishMenu { MenuId = entity.MenuId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("Menu-{0}", entity.MenuId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Catalog/Menu.aspx?id={0}", entity.MenuId),
                PublishRequest = publishMenu
            }
        };
    }
}