﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public interface IContent
{
    IEnumerable<Content> CreateContent(int companyId, TimeZoneInfo timeZoneInfo, bool showAllContent);
}