﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class MapContent : ContentBase<MapEntity>
{
    private readonly Crave.Api.Logic.DataSources.MapDataSource mapDataSource;

    public MapContent(Crave.Api.Logic.DataSources.MapDataSource mapDataSource)
    {
        this.mapDataSource = mapDataSource;
    }

    public override IEnumerable<MapEntity> GetData(int companyId)
    {
        return this.mapDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "Maps",
            Text = "Maps",
            CmsUrl = "~/Generic/Maps.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(MapEntity entity)
    {
        PublishMap publishRequest = new PublishMap { MapId = entity.MapId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("Map-{0}", entity.MapId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Generic/Map.aspx?id={0}", entity.MapId),
                PublishRequest = publishRequest
            }
        };
    }
}