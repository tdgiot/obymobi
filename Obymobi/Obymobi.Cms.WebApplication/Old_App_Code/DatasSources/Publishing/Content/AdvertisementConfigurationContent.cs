﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class AdvertisementConfigurationContent : ContentBase<AdvertisementConfigurationEntity>
{
    private readonly Crave.Api.Logic.DataSources.AdvertisementConfigurationDataSource advertisementConfigurationDataSource;

    public AdvertisementConfigurationContent(Crave.Api.Logic.DataSources.AdvertisementConfigurationDataSource advertisementConfigurationDataSource)
    {
        this.advertisementConfigurationDataSource = advertisementConfigurationDataSource;
    }

    public override IEnumerable<AdvertisementConfigurationEntity> GetData(int companyId)
    {
        return this.advertisementConfigurationDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "AdvertisementConfigurations",
            Text = "Advertisements",
            CmsUrl = "~/Advertising/AdvertisementConfigurations.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(AdvertisementConfigurationEntity entity)
    {
        PublishAdvertisements publishRequest = new PublishAdvertisements { AdvertisementConfigurationId = entity.AdvertisementConfigurationId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("AdvertisementConfiguration-{0}", entity.AdvertisementConfigurationId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Advertising/AdvertisementConfiguration.aspx?id={0}", entity.AdvertisementConfigurationId),
                PublishRequest = publishRequest
            }
        };
    }
}