﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Api.Logic;
using Crave.Api.Logic.Requests.Publishing;
using Dionysos;
using Obymobi;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

public abstract class ContentBase<TEntity> : IContent
{
    private readonly Crave.Api.Logic.DataSources.PublishingDataSource publishingDataSource;

    private bool showAllContent;
    private TimeZoneInfo timeZoneInfo;

    public ContentBase()
    {
        this.publishingDataSource = new Crave.Api.Logic.DataSources.PublishingDataSource();
    }

    public abstract IEnumerable<TEntity> GetData(int companyId);
    public abstract Content CreateRootContent(int companyId);
    public abstract IEnumerable<Content> CreateEntityContent(TEntity entity);

    public IEnumerable<Content> CreateContent(int companyId, TimeZoneInfo timeZoneInfo, bool showAllContent)
    {
        this.timeZoneInfo = timeZoneInfo;
        this.showAllContent = showAllContent;

        IEnumerable<TEntity> entities = this.GetData(companyId);

        List<Content> contentCollection = new List<Content>();

        Content rootContent = this.AddRootContent(companyId, ref contentCollection); // i.e. Menus

        foreach (TEntity entity in entities)
        {
            List<Content> entityContent = this.AddEntityContent(entity, ref contentCollection); // i.e. Main menu
            this.SetTimestampsOnParent(rootContent, entityContent);
            this.SetParentOnChildren(rootContent, entityContent);
        }

        return contentCollection;
    }

    private Content AddRootContent(int companyId, ref List<Content> contentCollection)
    {
        Content rootContent = this.CreateRootContent(companyId);
        contentCollection.Add(rootContent);
        return rootContent;
    }

    private List<Content> AddEntityContent(TEntity entity, ref List<Content> contentCollection)
    {
        List<Content> entityContentCollection = this.CreateEntityContent(entity).ToList();

        foreach (Content entityContent in entityContentCollection)
        {
            PublishRequestBase publishRequest = entityContent.PublishRequest;
            if (publishRequest == null)
                continue;

            if (this.showAllContent)
            {
                PublishingEntity publishingEntity = this.publishingDataSource.GetEntity(publishRequest.GetType().ToString(), publishRequest.ParameterNamesAndValues);
                this.SetPublishingInfo(entityContent, publishingEntity);
            }

            List<Content> apiOperationContent = publishRequest.ApiOperations
                                                            .Where(x => x.TimestampFieldIndexes != null)
                                                            .Select(x => this.CreateApiOperationContent(x, publishRequest, entityContent))
                                                            .ToList();

            this.SetTimestampsOnParent(entityContent, apiOperationContent);
            this.SetParentOnChildren(entityContent, apiOperationContent);
            contentCollection.AddRange(apiOperationContent);
        }

        contentCollection.AddRange(entityContentCollection);
        return entityContentCollection;
    }

    private Content CreateApiOperationContent(ApiOperation apiOperation, PublishRequestBase publishRequest, Content parentContent)
    {
        TimestampFieldIndex modifiedField = apiOperation.TimestampFieldIndexes.ModifiedField;
        TimestampFieldIndex publishedField = apiOperation.TimestampFieldIndexes.PublishedField;

        DateTime? lastModifiedUtc = publishRequest.Timestamp.GetTimestampUtc(modifiedField);
        DateTime? lastPublishedUtc = publishRequest.Timestamp.GetTimestampUtc(publishedField);

        string timestampName = apiOperation.RequestName.Replace("Get", string.Empty).FormatPascalCase();
        bool hasUnpublishedChanges = publishRequest.Timestamp.IsFieldDirty(modifiedField, publishedField);

        return new Content
        {
            ContentId = string.Format("{0}-{1}", parentContent.ContentId, apiOperation.RequestName),
            IsApiOperation = true,
            Text = timestampName,
            CmsUrl = string.Empty,
            LastModifiedUtc = lastModifiedUtc,
            LastModified = this.FormatDateTime(lastModifiedUtc),
            LastPublishedUtc = lastPublishedUtc,
            LastPublished = this.FormatDateTime(lastPublishedUtc),
            HasUnpublishedChanges = hasUnpublishedChanges
        };
    }

    private string FormatDateTime(DateTime? dateTime)
    {
        if (dateTime.HasValue)
            return string.Format("{0} ({1} ago)", dateTime.UtcToLocalTime(this.timeZoneInfo), dateTime.Value.GetElapsedTimeString(DateTime.UtcNow));

        return "Never";
    }

    private void SetTimestampsOnParent(Content parentContent, List<Content> childContent)
    {
        Content lastModifiedContent = childContent.Where(x => x.LastModifiedUtc.HasValue)
                                                  .OrderByDescending(x => x.LastModifiedUtc.GetValueOrDefault())
                                                  .FirstOrDefault();

        if (lastModifiedContent == null)
        {
            parentContent.LastModified = "Never";
        }
        else
        {
            parentContent.LastModifiedUtc = lastModifiedContent.LastModifiedUtc;
            parentContent.LastModified = lastModifiedContent.LastModified;
        }

        Content lastPublishedContent = childContent.Where(x => x.LastPublishedUtc.HasValue)
                                                  .OrderByDescending(x => x.LastPublishedUtc.GetValueOrDefault())
                                                  .FirstOrDefault();

        if (lastPublishedContent == null)
        {
            parentContent.LastPublished = "Never";
        }
        else
        {
            parentContent.LastPublishedUtc = lastPublishedContent.LastPublishedUtc;
            parentContent.LastPublished = lastPublishedContent.LastPublished;
        }

        if (childContent.Any(x => x.HasUnpublishedChanges))
        {
            parentContent.HasUnpublishedChanges = true;
        }
    }

    private void SetParentOnChildren(Content parentContent, List<Content> childContentCollection)
    {
        foreach (Content child in childContentCollection)
        {
            child.ContentParentId = parentContent.ContentId;
            child.PublishingId = parentContent.PublishingId;
            child.PublishedBy = parentContent.PublishedBy;
        }
    }

    private void SetPublishingInfo(Content content, PublishingEntity publishingEntity)
    {
        if (publishingEntity == null)
            return;

        content.PublishingId = publishingEntity.PublishingId;
        content.PublishedBy = publishingEntity.Username;
    }
}