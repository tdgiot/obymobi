﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class CompanyContent : ContentBase<CompanyEntity>
{
    private readonly Crave.Api.Logic.DataSources.CompanyDataSource companyDataSource;

    public CompanyContent(Crave.Api.Logic.DataSources.CompanyDataSource companyDataSource)
    {
        this.companyDataSource = companyDataSource;
    }

    public override IEnumerable<CompanyEntity> GetData(int companyId)
    {
        return this.companyDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "Companies",
            Text = "Company",
            CmsUrl = string.Format("~/Company/Company.aspx?id={0}", companyId)
        };
    }

    public override IEnumerable<Content> CreateEntityContent(CompanyEntity entity)
    {
        return new[]
        {
            new Content
            {
                ContentId = "CompanyDetails",
                Text = string.Format("Company details '{0}'", entity.Name),
                CmsUrl = string.Format("~/Company/Company.aspx?id={0}", entity.CompanyId),
                PublishRequest = new PublishCompany { CompanyId = entity.CompanyId }
            },
            new Content
            {
                ContentId = "CompanyAvailability",
                Text = "Availabilities",
                CmsUrl = "~/Company/Availabilities.aspx",
                PublishRequest = new PublishAvailabilities { CompanyId = entity.CompanyId }
            },
            new Content
            {
                ContentId = "CloudStorageAccounts",
                Text = "Cloud storage accounts",
                CmsUrl = "~/Company/CloudStorageAccounts.aspx",
                PublishRequest = new PublishCloudStorageAccounts { CompanyId = entity.CompanyId }
            },
            new Content
            {
                ContentId = "CompanyAmenities",
                Text = "Amenities",
                CmsUrl = "~/Generic/Amenities.aspx",
                PublishRequest = new PublishCompanyAmenities { CompanyId = entity.CompanyId }
            },
            new Content
            {
                ContentId = "CompanyVenueCategories",
                Text = "Venue categories",
                CmsUrl = "~/Generic/VenueCategories.aspx",
                PublishRequest = new PublishCompanyVenueCategories { CompanyId = entity.CompanyId }
            },
            new Content
            {
                ContentId = "CompanyReleases",
                Text = "Releases",
                CmsUrl = string.Format("~/Company/Company.aspx?id={0}", entity.CompanyId),
                PublishRequest = new PublishCompanyReleases { CompanyId = entity.CompanyId }
            },
            new Content
            {
                ContentId = "Messagegroups",
                Text = "Messagegroups",
                CmsUrl = "~/Company/Messagegroups.aspx",
                PublishRequest = new PublishMessagegroups { CompanyId = entity.CompanyId }
            },
            new Content
            {
                ContentId = "AnnouncementActions",
                Text = "Announcement actions",
                CmsUrl = "~/Company/Announcements.aspx",
                PublishRequest = new PublishAnnouncementActions { CompanyId = entity.CompanyId }
            },
        };
    }
}