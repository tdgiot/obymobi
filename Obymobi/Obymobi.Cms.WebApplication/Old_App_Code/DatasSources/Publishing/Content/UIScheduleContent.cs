﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class UIScheduleContent : ContentBase<UIScheduleEntity>
{
    private readonly Crave.Api.Logic.DataSources.UIScheduleDataSource uiScheduleDataSource;

    public UIScheduleContent(Crave.Api.Logic.DataSources.UIScheduleDataSource uiScheduleDataSource)
    {
        this.uiScheduleDataSource = uiScheduleDataSource;
    }

    public override IEnumerable<UIScheduleEntity> GetData(int companyId)
    {
        return this.uiScheduleDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "UISchedules",
            Text = "UI schedules",
            CmsUrl = "~/Company/UISchedules.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(UIScheduleEntity entity)
    {
        PublishUISchedule publishRequest = new PublishUISchedule { UIScheduleId = entity.UIScheduleId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("UISchedule-{0}", entity.UIScheduleId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Company/UISchedule.aspx?id={0}", entity.UIScheduleId),
                PublishRequest = publishRequest
            }
        };
    }
}