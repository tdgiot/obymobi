﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class UIModeContent : ContentBase<UIModeEntity>
{
    private readonly Crave.Api.Logic.DataSources.UIModeDataSource uiModeDataSource;

    public UIModeContent(Crave.Api.Logic.DataSources.UIModeDataSource uiModeDataSource)
    {
        this.uiModeDataSource = uiModeDataSource;
    }

    public override IEnumerable<UIModeEntity> GetData(int companyId)
    {
        return this.uiModeDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "UIModes",
            Text = "UI modes",
            CmsUrl = "~/Company/UIModes.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(UIModeEntity entity)
    {
        PublishUIMode publishRequest = new PublishUIMode { UIModeId = entity.UIModeId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("UIMode-{0}", entity.UIModeId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Company/UIMode.aspx?id={0}", entity.UIModeId),
                PublishRequest = publishRequest
            }
        };
    }
}