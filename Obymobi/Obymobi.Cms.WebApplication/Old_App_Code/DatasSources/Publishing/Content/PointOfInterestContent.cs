﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;

public class PointOfInterestContent : ContentBase<PointOfInterestEntity>
{
    private readonly Crave.Api.Logic.DataSources.PointOfInterestMapDataSource pointOfInterestMapDataSource;
    private readonly Crave.Api.Logic.DataSources.PointOfInterestSiteDataSource pointOfInterestSiteDataSource;

    public PointOfInterestContent(Crave.Api.Logic.DataSources.PointOfInterestMapDataSource pointOfInterestMapDataSource,
                                  Crave.Api.Logic.DataSources.PointOfInterestSiteDataSource pointOfInterestSiteDataSource)
    {
        this.pointOfInterestMapDataSource = pointOfInterestMapDataSource;
        this.pointOfInterestSiteDataSource = pointOfInterestSiteDataSource;
    }

    public override IEnumerable<PointOfInterestEntity> GetData(int companyId)
    {
        PointOfInterestCollection mapPointOfInterestCollection = this.pointOfInterestMapDataSource.GetEntities(companyId);
        PointOfInterestCollection sitePointOfInterestCollection = this.pointOfInterestSiteDataSource.GetEntities(companyId);
        return mapPointOfInterestCollection.Union(sitePointOfInterestCollection).Distinct();
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "PointOfInterests",
            Text = "Point of interests",
            CmsUrl = "~/Generic/PointOfInterests.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(PointOfInterestEntity entity)
    {
        PublishPointOfInterest publishPointOfInterest = new PublishPointOfInterest { PointOfInterestId = entity.PointOfInterestId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("PointOfInterest-{0}", entity.PointOfInterestId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Generic/PointOfInterest.aspx?id={0}", entity.PointOfInterestId),
                PublishRequest = publishPointOfInterest
            }
        };
    }
}