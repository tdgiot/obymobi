﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class InfraredConfigurationContent : ContentBase<InfraredConfigurationEntity>
{
    private readonly Crave.Api.Logic.DataSources.InfraredConfigurationDataSource infraredConfigurationDataSource;

    public InfraredConfigurationContent(Crave.Api.Logic.DataSources.InfraredConfigurationDataSource infraredConfigurationDataSource)
    {
        this.infraredConfigurationDataSource = infraredConfigurationDataSource;
    }

    public override IEnumerable<InfraredConfigurationEntity> GetData(int companyId)
    {
        return this.infraredConfigurationDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "InfraredConfigurations",
            Text = "Infrared configurations",
            CmsUrl = "~/Generic/InfraredConfiguration.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(InfraredConfigurationEntity entity)
    {
        return new []
        {
            new Content
            {
                ContentId = string.Format("InfraredConfiguration-{0}", entity.InfraredConfigurationId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Generic/InfraredConfiguration.aspx?id={0}", entity.InfraredConfigurationId),
                PublishRequest = new PublishInfraredConfiguration { InfraredConfigurationId = entity.InfraredConfigurationId }
            }
        };
    }
}