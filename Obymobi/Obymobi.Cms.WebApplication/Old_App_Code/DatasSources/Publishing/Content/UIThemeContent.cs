﻿using System;
using Crave.Api.Logic.Requests.Publishing;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

public class UIThemeContent : ContentBase<UIThemeEntity>
{
    private readonly Crave.Api.Logic.DataSources.UIThemeDataSource uiThemeDataSource;

    public UIThemeContent(Crave.Api.Logic.DataSources.UIThemeDataSource uiThemeDataSource)
    {
        this.uiThemeDataSource = uiThemeDataSource;
    }

    public override IEnumerable<UIThemeEntity> GetData(int companyId)
    {
        return this.uiThemeDataSource.GetEntities(companyId);
    }

    public override Content CreateRootContent(int companyId)
    {
        return new Content
        {
            ContentId = "UIThemes",
            Text = "UI themes",
            CmsUrl = "~/Generic/UIThemes.aspx"
        };
    }

    public override IEnumerable<Content> CreateEntityContent(UIThemeEntity entity)
    {
        PublishUITheme publishRequest = new PublishUITheme { UIThemeId = entity.UIThemeId };

        return new []
        {
            new Content
            {
                ContentId = string.Format("UITheme-{0}", entity.UIThemeId),
                Text = entity.Name,
                CmsUrl = string.Format("~/Generic/UITheme.aspx?id={0}", entity.UIThemeId),
                PublishRequest = publishRequest
            }
        };
    }
}