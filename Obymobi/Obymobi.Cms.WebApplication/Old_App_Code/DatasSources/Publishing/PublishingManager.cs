﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Crave.Api.Logic.DataSources;

public class PublishingManager
{
    private readonly int companyId;
    private readonly TimeZoneInfo timeZoneInfo;
    private readonly IEnumerable<IContent> contentSections;
    private readonly bool showAllContent;

    public PublishingManager(int companyId,
                             TimeZoneInfo timeZoneInfo,
                             bool showAllContent)
    {
        this.companyId = companyId;
        this.timeZoneInfo = timeZoneInfo;
        this.showAllContent = showAllContent;
        this.contentSections = new IContent[]
        {
            new CompanyContent(new CompanyDataSource()),
            new MenuContent(new Crave.Api.Logic.DataSources.MenuDataSource()),
            new SiteContent(new SiteDataSource()),
            new MapContent(new MapDataSource()),
            new DeliverypointgroupContent(new DeliverypointgroupDataSource()),
            new DeliveryTimeContent(new DeliverypointgroupDataSource()),
            new ClientConfigurationContent(new ClientConfigurationDataSource()),
            new UIModeContent(new UIModeDataSource()),
            new PriceScheduleContent(new Crave.Api.Logic.DataSources.PriceScheduleDataSource()),
            new UIScheduleContent(new UIScheduleDataSource()),
            new UIThemeContent(new UIThemeDataSource()),
            new AdvertisementConfigurationContent(new AdvertisementConfigurationDataSource()),
            new EntertainmentConfigurationContent(new EntertainmentConfigurationDataSource()),
            new RoomControlConfigurationContent(new Crave.Api.Logic.DataSources.RoomControlConfigurationDataSource()),
            new InfraredConfigurationContent(new InfraredConfigurationDataSource()), 
            new TerminalContent(new TerminalDataSource()),
            new TerminalConfigurationContent(new TerminalConfigurationDataSource()),
            new MessageTemplateContent(new TerminalDataSource()),
            new PointOfInterestContent(new PointOfInterestMapDataSource(), new PointOfInterestSiteDataSource())
        };
    }    

    [DataObjectMethod(DataObjectMethodType.Select, true)]
    public IEnumerable Select()
    {
        return this.DataSource;
    }               

    private List<Content> dataSource;
    public List<Content> DataSource
    {
        get
        {
            if (this.dataSource == null)
            {
                this.dataSource = this.CreateContentNodes();
            }

            return this.dataSource;
        }
    }

    private List<Content> CreateContentNodes()
    {
        List<Content> contentCollection = new List<Content>();

        foreach (IContent content in this.contentSections)
        {
            contentCollection.AddRange(content.CreateContent(this.companyId, this.timeZoneInfo, this.showAllContent));
        }

        if (this.showAllContent)
        {
            return contentCollection;
        }
        else
        {
            return contentCollection.Where(x => !x.IsApiOperation && x.HasUnpublishedChanges).ToList();
        }
    }
}