﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Crave.Api.Logic.Requests.Publishing;

public class Content
{
    public string ContentId { get; set; }
    public string ContentParentId { get; set; }
    public string Text { get; set; }
    public string CmsUrl { get; set; }
    public bool IsApiOperation { get; set; }
    public int PublishingId { get; set; }
    public string PublishedBy { get; set; }
    public DateTime? LastModifiedUtc { get; set; }
    public string LastModified { get; set; }
    public DateTime? LastPublishedUtc { get; set; }
    public string LastPublished { get; set; }
    public bool HasUnpublishedChanges { get; set; }
    public PublishRequestBase PublishRequest { get; set; }
}