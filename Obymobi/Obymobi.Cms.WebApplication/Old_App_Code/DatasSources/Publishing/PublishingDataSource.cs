﻿using System;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;
using System.Web.UI.WebControls;

public class PublishingDataSource : ObjectDataSource
{
    private readonly PublishingManager Manager;    

    public PublishingDataSource(int companyId, TimeZoneInfo timeZoneInfo, bool showAllContent)
    {
        this.TypeName = typeof(PublishingManager).FullName;
        this.Manager = new PublishingManager(companyId, timeZoneInfo, showAllContent);
        
        this.SelectMethod = "Select";

        this.ObjectCreating += this.PageDataSource_ObjectCreating;
    }

    private void PageDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
    {
        e.ObjectInstance = this.Manager;
    }
    
    public List<Content> DataSource { get { return this.Manager.DataSource; } }
}