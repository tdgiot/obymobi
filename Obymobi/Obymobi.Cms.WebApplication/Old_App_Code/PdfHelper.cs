﻿using Dionysos.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

/// <summary>
/// Summary description for PdfConverter
/// </summary>
public class PdfConverter
{
    private const string SessionKey = "PdfConverter.SessionKey";
    public bool Processing {get;set;}
    public object processingLock;

	private PdfConverter()
	{
        this.Processing = false;
        this.processingLock = new object();
	}

    /// <summary>
    /// This is not a Singleton, but single instance per Session class
    /// </summary>
    public PdfConverter Instance
    {
        get
        {
            PdfConverter instance = SessionHelper.GetValue<PdfConverter>(PdfConverter.SessionKey);
            if(instance == null)
            {
                instance = new PdfConverter();                   
            }

            return instance;
        }
    }

    public void AttachPdfAsMedia(IEntity entity, Stream pdfDocument)
    {
        lock(processingLock)
        {
            if(this.Processing)
            {
                // Already processing (this should be prevented by the CMS pages themselves by checking
                // the Processing property before trying to attach a PDF.
                throw new Exception("It's not possible to proces more than one PDF at a time.");
            }
            else
                this.Processing = true;
        }

        try
        {
            // Retrieve the Entity PK and PK Fieldname, so the entity can go out of scope during the processing.
            // these are used on the MediaEntity as FK.
            string fkFieldName = entity.PrimaryKeyFields[0].Name;
            int fkId = (int)entity.PrimaryKeyFields[0].CurrentValue;
            
            // Run in background.
            Task.Factory.StartNew(() => this.ProcessPdf(fkFieldName, fkId, pdfDocument));
        }
        finally
        {
            lock(processingLock)
            {
                this.Processing = false;
            }
        }
    }

    private void ProcessPdf(string fkFieldName, int fkValue, Stream pdfDocument)
    {
        try
        {
            // Do PDF Magic.

        }
        finally
        {
            lock(processingLock)
            {
                this.Processing = false;
            }
        }
    }
 
}