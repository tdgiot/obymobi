﻿using System;
using Obymobi.Enums;
using Obymobi.Integrations.Payment;
using Obymobi.Integrations.Payment.Adyen;
using Obymobi.Integrations.Payment.Omise;

namespace Obymobi.Cms.WebApplication.Old_App_Code.Payments
{
    public class PaymentFactory
    {
        public IClient CreateClient(IPaymentProviderConfiguration configuration)
        {
            return this.GetClientFactory(configuration).Create();
        }

        private IClientFactory GetClientFactory(IPaymentProviderConfiguration configuration)
        {
            switch (configuration.PaymentProviderType)
            {
                case PaymentProviderType.Adyen:
                case PaymentProviderType.AdyenForPlatforms:
                    return new AdyenClientFactory(configuration);
                case PaymentProviderType.Omise:
                    return new OmiseClientFactory(configuration);
                default:
                    throw new Exception($"PaymentProviderType {configuration.PaymentProviderType} not implemented");
            }
        }
    }
}