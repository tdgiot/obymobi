﻿using SpreadsheetLight;
using Obymobi.Logic.Analytics;

namespace Obymobi.ObymobiCms.Reports
{
    public class GuestInformationReport : IReport
    {
        #region Constructors

        public GuestInformationReport(GuestInformationReportFilter filter)
        {
            this.GuestInformationData = new GuestInformationData(filter);
            this.GuestInformationData.FetchAndProcessData();
        }

        #endregion

        #region Properties

        private GuestInformationData GuestInformationData { get; set; }

        #endregion

        #region Methods

        public bool RunReport(out SLDocument spreadsheet)
        {
            spreadsheet = new SLDocument();
            string summaryWorkSheetName = "Guest Information";

            // Prepare the Summary worksheet
            spreadsheet.RenameWorksheet(SLDocument.DefaultFirstSheetName, summaryWorkSheetName);
            
            this.RenderGuestInformationWorksheet(spreadsheet);
            
            // Scale 1x1 Page
            spreadsheet.SelectWorksheet(summaryWorkSheetName);
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 1);
            spreadsheet.SetPageSettings(pageSettings);

            // Set all print sizes to A4
            spreadsheet.SetPageSizeOfWorksheets(SLPaperSizeValues.A4Paper);

            return true;
        }

        private void RenderGuestInformationWorksheet(SLDocument spreadsheet)
        {
            // Create and select worksheet
            spreadsheet.AddWorksheet("Guest Information");
            spreadsheet.SelectWorksheet("Guest Information");

            // Set Headings + styling
            spreadsheet.SetCellValue("A1", "Id");
            spreadsheet.SetCellValue("B1", "External Id");
            spreadsheet.SetCellValue("C1", "Number");
            spreadsheet.SetCellValue("D1", "Reservation #");
            spreadsheet.SetCellValue("E1", "Group");
            spreadsheet.SetCellValue("F1", "Check-in");
            spreadsheet.SetCellValue("G1", "Check-out");
            spreadsheet.SetCellValue("H1", "DND Active");
            spreadsheet.SetCellValue("I1", "Message Waiting");
            spreadsheet.SetCellValue("J1", "Allowed to View Bill");
            spreadsheet.SetCellValue("K1", "Express Check-out");
            spreadsheet.SetStyle("A1", "K1", borders: Borders.Bottom, backgroundColor: SpreadsheetHelper.Green10);
            spreadsheet.SetStyle("A1", "K1", DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Left);

            int currentRow = 2;
            foreach (GuestInformation guestInformation in this.GuestInformationData.GuestInformation)
            {
                this.RenderRowForGuestInformation(spreadsheet, guestInformation, ref currentRow);
            }

            // Set column widths
            spreadsheet.SetColumnWidth("A", 10);
            spreadsheet.SetColumnWidth("B", 16);
            spreadsheet.SetColumnWidth("C", 16);
            spreadsheet.SetColumnWidth("D", 16);
            spreadsheet.SetColumnWidth("E", 16);
            spreadsheet.SetColumnWidth("F", 20);
            spreadsheet.SetColumnWidth("G", 20);
            spreadsheet.SetColumnWidth("H", 16);
            spreadsheet.SetColumnWidth("I", 16);
            spreadsheet.SetColumnWidth("J", 20);
            spreadsheet.SetColumnWidth("K", 20);

            // Keep first line frozen
            spreadsheet.FreezePanes(1, 0);

            // Set printing to 1 page width
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 500);
            spreadsheet.SetPageSettings(pageSettings);
        }

        private void RenderRowForGuestInformation(SLDocument spreadsheet, GuestInformation guestInformation, ref int currentRow)
        {
            int productRow = currentRow;
            currentRow++;

            spreadsheet.SetCellValue("A" + productRow, guestInformation.GuestInformationId);
            spreadsheet.SetCellValue("B" + productRow, guestInformation.ExternalGuestId);
            spreadsheet.SetCellValue("C" + productRow, guestInformation.DeliverypointNumber);
            spreadsheet.SetCellValue("D" + productRow, guestInformation.ReservationNumber);
            spreadsheet.SetCellValue("E" + productRow, guestInformation.GroupName);
            spreadsheet.SetCellValue("F" + productRow, guestInformation.CheckInAsString);
            spreadsheet.SetCellValue("G" + productRow, guestInformation.CheckOutAsString);
            spreadsheet.SetCellValue("H" + productRow, guestInformation.DoNotDisturb);
            spreadsheet.SetCellValue("I" + productRow, guestInformation.MessageWaiting);
            spreadsheet.SetCellValue("J" + productRow, guestInformation.ViewBill);
            spreadsheet.SetCellValue("K" + productRow, guestInformation.ExpressCheckout);

            // Only background when including categories, otherwise borders
            spreadsheet.SetStyle("A" + productRow, "K" + productRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green15);
            spreadsheet.SetStyle("A" + productRow, "K" + productRow, DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Left);
        }

        #endregion
    }
} 
