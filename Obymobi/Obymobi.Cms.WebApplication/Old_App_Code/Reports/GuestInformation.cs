﻿using Obymobi.Web.Analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GuestInformation
/// </summary>
public class GuestInformation
{
    private readonly ReportSettings settings;

    public GuestInformation(ReportSettings settings)
    {
        this.settings = settings;
    }

    public int GuestInformationId { get; set; }
    public string ExternalGuestId { get; set; }
    public string DeliverypointNumber { get; set; }
    public string ReservationNumber { get; set; }
    public string GroupName { get; set; }
    public string LanguageCode { get; set; }
    public DateTime CheckIn { get; set; }
    public DateTime? CheckOut { get; set; }
    public bool Vip { get; set; }
    public bool DoNotDisturb { get; set; }
    public bool MessageWaiting { get; set; }
    public bool ViewBill { get; set; }
    public bool ExpressCheckout { get; set; }

    public string CheckInAsString { get { return this.settings.ToClockModeString(this.CheckIn); } }
    public string CheckOutAsString { get { return this.CheckOut.HasValue ? this.settings.ToClockModeString(this.CheckOut.Value) : string.Empty; } }
}