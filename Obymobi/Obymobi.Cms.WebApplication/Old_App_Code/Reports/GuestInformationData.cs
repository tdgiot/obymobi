﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using Dionysos;
using Obymobi.Web.Analytics;

namespace Obymobi.ObymobiCms.Reports
{
    public class GuestInformationData
    {
        #region Fields

        private readonly GuestInformationReportFilter filter;
        private GuestInformationCollection guestInformationCollection;

        #endregion

        #region Constructors

        public GuestInformationData(GuestInformationReportFilter filter)
        {
            if (filter == null)
                throw new ArgumentNullException("filter");

            this.filter = filter;
        }

        #endregion

        #region Properties

        public List<GuestInformation> GuestInformation { get; set; }

        #endregion

        #region Methods

        public void FetchAndProcessData()
        {
            this.FetchGuestInformation();
            this.ProcessData();
        }

        private void FetchGuestInformation()
        {         
            PredicateExpression filter = new PredicateExpression();
            filter.Add(GuestInformationFields.CompanyId == this.filter.CompanyId);

            if (this.filter.CheckInUtc.HasValue)
            {
                PredicateExpression checkInFilter = new PredicateExpression();
                checkInFilter.Add(GuestInformationFields.CheckInUTC >= this.filter.CheckInUtc.Value.MakeBeginOfDay());
                checkInFilter.Add(GuestInformationFields.CheckInUTC <= this.filter.CheckInUtc.Value.MakeEndOfDay());
                filter.Add(checkInFilter);
            }

            if (this.filter.CheckOutUtc.HasValue)
            {
                PredicateExpression checkOutFilter = new PredicateExpression();
                checkOutFilter.Add(GuestInformationFields.CheckOutUTC >= this.filter.CheckOutUtc.Value.MakeBeginOfDay());
                checkOutFilter.Add(GuestInformationFields.CheckOutUTC <= this.filter.CheckOutUtc.Value.MakeEndOfDay());
                filter.Add(checkOutFilter);
            }

            if (!this.filter.GroupName.IsNullOrWhiteSpace())
            {
                filter.Add(GuestInformationFields.GroupName == this.filter.GroupName);
            }
                        
            SortExpression sort = new SortExpression(new SortClause(GuestInformationFields.DeliverypointNumber, SortOperator.Ascending));

            this.guestInformationCollection = new GuestInformationCollection();
            this.guestInformationCollection.GetMulti(filter, 0, sort);            
        }

        private void ProcessData()
        {
            this.GuestInformation = new List<GuestInformation>();
            this.Populate();
        }

        private void Populate()
        {
            ReportSettings settings = new ReportSettings(this.filter.CompanyId);

            foreach (GuestInformationEntity entity in this.guestInformationCollection)
            {
                GuestInformation model = new GuestInformation(settings);
                model.GuestInformationId = entity.GuestInformationId;
                model.ExternalGuestId = entity.ExternalGuestId;
                model.DeliverypointNumber = entity.DeliverypointNumber;
                model.ReservationNumber = entity.ReservationNumber;
                model.GroupName = entity.GroupName;
                model.LanguageCode = entity.LanguageCode;
                model.CheckIn = entity.CheckInUTC.HasValue ? entity.CheckInUTC.Value.ToTimeZone(TimeZoneInfo.Utc, settings.CompanyTimeZoneInfo) : new DateTime();
                model.CheckOut = entity.CheckOutUTC.HasValue ? entity.CheckOutUTC.Value.ToTimeZone(TimeZoneInfo.Utc, settings.CompanyTimeZoneInfo) : new DateTime();
                model.Vip = entity.Vip;
                model.DoNotDisturb = entity.DoNotDisturb;
                model.MessageWaiting = entity.MessageWaiting;
                model.ViewBill = entity.ViewBill;
                model.ExpressCheckout = entity.ExpressCheckOut;
                this.GuestInformation.Add(model);
            }
        }
        
        #endregion
    }
}
