﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GuestInformationReportFilter
/// </summary>
public class GuestInformationReportFilter
{
    public int CompanyId { get; set; }
    public DateTime? CheckInUtc { get; set; }
    public DateTime? CheckOutUtc { get; set; }
    public string GroupName { get; set; }

    public GuestInformationReportFilter()
    {
        this.CompanyId = -1;
        this.CheckInUtc = null;
        this.CheckOutUtc = null;
        this.GroupName = string.Empty;
    }
}