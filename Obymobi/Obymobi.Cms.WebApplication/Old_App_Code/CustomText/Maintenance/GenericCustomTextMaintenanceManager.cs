﻿using System.Linq;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.HelperClasses;
using System;

/// <summary>
/// Summary description for GenericCustomTextMaintenanceManager
/// </summary>
public class GenericCustomTextMaintenanceManager : CustomTextMaintanceManager
{
    private ActionButtonCollection actionButtons;
    private AdvertisementCollection advertisements;
    private AmenityCollection amenities;
    private AnnouncementCollection announcements;
    private EntertainmentcategoryCollection entertainmentcategories;
    private GenericcategoryCollection genericCategories;
    private GenericproductCollection genericProducts;
    private PointOfInterestCollection pointOfInterests;
    private UIModeCollection uiModes;
    private UITabCollection uiTabs;
    private VenueCategoryCollection venueCategories;

    public override void FetchData()
    {
        PrefetchPath prefetchActionButton = new PrefetchPath(EntityType.ActionButtonEntity);
        prefetchActionButton.Add(ActionButtonEntityBase.PrefetchPathCustomTextCollection);
        this.actionButtons = new ActionButtonCollection();
        this.actionButtons.GetMulti(null, prefetchActionButton);

        PredicateExpression filterAdvertisement = new PredicateExpression();
        filterAdvertisement.Add(AdvertisementFields.CompanyId == DBNull.Value);
        PrefetchPath prefetchAdvertisement = new PrefetchPath(EntityType.AdvertisementEntity);
        prefetchAdvertisement.Add(AdvertisementEntityBase.PrefetchPathCustomTextCollection);
        this.advertisements = new AdvertisementCollection();
        this.advertisements.GetMulti(filterAdvertisement, prefetchAdvertisement);

        PrefetchPath prefetchAmenity = new PrefetchPath(EntityType.AmenityEntity);
        prefetchAmenity.Add(AmenityEntityBase.PrefetchPathCustomTextCollection);
        this.amenities = new AmenityCollection();
        this.amenities.GetMulti(null, prefetchAmenity);

        PredicateExpression filterAnnouncement = new PredicateExpression();
        filterAnnouncement.Add(AnnouncementFields.CompanyId == DBNull.Value);
        PrefetchPath prefetchAnnouncement = new PrefetchPath(EntityType.AnnouncementEntity);
        prefetchAnnouncement.Add(AnnouncementEntityBase.PrefetchPathCustomTextCollection);
        this.announcements = new AnnouncementCollection();
        this.announcements.GetMulti(filterAnnouncement, prefetchAnnouncement);

        PrefetchPath prefetchEntertainmentCategory = new PrefetchPath(EntityType.EntertainmentcategoryEntity);
        prefetchEntertainmentCategory.Add(EntertainmentcategoryEntityBase.PrefetchPathCustomTextCollection);
        this.entertainmentcategories = new EntertainmentcategoryCollection();
        this.entertainmentcategories.GetMulti(null, prefetchEntertainmentCategory);

        PrefetchPath prefetchGenericcategory = new PrefetchPath(EntityType.GenericcategoryEntity);
        prefetchGenericcategory.Add(GenericcategoryEntityBase.PrefetchPathCustomTextCollection);
        this.genericCategories = new GenericcategoryCollection();
        this.genericCategories.GetMulti(null, prefetchGenericcategory);

        PrefetchPath prefetchGenericproduct = new PrefetchPath(EntityType.GenericproductEntity);
        prefetchGenericproduct.Add(GenericproductEntityBase.PrefetchPathCustomTextCollection);
        this.genericProducts = new GenericproductCollection();
        this.genericProducts.GetMulti(null, prefetchGenericproduct);

        PrefetchPath prefetchPointOfInterest = new PrefetchPath(EntityType.PointOfInterestEntity);
        prefetchPointOfInterest.Add(PointOfInterestEntityBase.PrefetchPathCustomTextCollection);
        this.pointOfInterests = new PointOfInterestCollection();
        this.pointOfInterests.GetMulti(null, prefetchPointOfInterest);

        PredicateExpression filterUiMode = new PredicateExpression();
        filterUiMode.Add(UIModeFields.PointOfInterestId != DBNull.Value);
        this.uiModes = new UIModeCollection();
        this.uiModes.GetMulti(filterUiMode);

        PrefetchPath prefetchUiTab = new PrefetchPath(EntityType.UITabEntity);
        prefetchUiTab.Add(UITabEntityBase.PrefetchPathCustomTextCollection);
        this.uiTabs = new UITabCollection();
        this.uiTabs.GetMulti(null, prefetchUiTab);

        PrefetchPath prefetchVenueCategory = new PrefetchPath(EntityType.VenueCategoryEntity);
        prefetchVenueCategory.Add(VenueCategoryEntityBase.PrefetchPathCustomTextCollection);
        this.venueCategories = new VenueCategoryCollection();
        this.venueCategories.GetMulti(null, prefetchVenueCategory);
    }

    public override void CreateCustomTexts()
    {
        foreach (ActionButtonEntity actionButton in this.actionButtons)
        {
            this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, actionButton);
        }

        foreach (AdvertisementEntity advertisement in this.advertisements)
        {
            if (advertisement.CompanyId.GetValueOrDefault(0) <= 0)
            {
                this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, advertisement);
            }            
        }

        foreach (AmenityEntity amenity in this.amenities)
        {
            this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, amenity);
        }

        foreach (AnnouncementEntity announcement in this.announcements)
        {
            if (announcement.CompanyId.GetValueOrDefault(0) <= 0)
            {
                this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, announcement);
            }            
        }

        foreach (EntertainmentcategoryEntity entertainmentcategory in this.entertainmentcategories)
        {
            this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, entertainmentcategory);
        }

        foreach (GenericcategoryEntity genericcategory in this.genericCategories)
        {
            this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, genericcategory);
        }

        foreach (GenericproductEntity genericproduct in this.genericProducts)
        {
            this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, genericproduct);
        }

        foreach (PointOfInterestEntity pointOfInterest in this.pointOfInterests)
        {
            this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, pointOfInterest);
        }

        foreach (UITabEntity uiTab in this.uiTabs)
        {
            UIModeEntity uiMode = this.uiModes.SingleOrDefault(x => x.UIModeId == uiTab.UIModeId);
            if (uiMode != null && uiMode.PointOfInterestId.GetValueOrDefault(0) > 0)
            {
                this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, uiTab);
            }
        }

        foreach (VenueCategoryEntity venueCategory in this.venueCategories)
        {
            this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, venueCategory);
        }
    }    
}