﻿using System.Linq;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using System;

/// <summary>
/// Summary description for MenuCustomTextMaintenanceManager
/// </summary>
public class MenuCustomTextMaintenanceManager : CustomTextMaintanceManager
{
    private CategoryCollection categories;
    private ProductCollection products;
    private AlterationCollection alterations;
    private AlterationoptionCollection alterationoptions;
    private AttachmentCollection attachments;    

    public override void FetchData()
    {
        PrefetchPath prefetchCategory = new PrefetchPath(EntityType.CategoryEntity);
        prefetchCategory.Add(CategoryEntityBase.PrefetchPathCustomTextCollection);
        this.categories = new CategoryCollection();
        this.categories.GetMulti(null, prefetchCategory);

        PrefetchPath prefetchProduct = new PrefetchPath(EntityType.ProductEntity);
        prefetchProduct.Add(ProductEntityBase.PrefetchPathCustomTextCollection);        
        this.products = new ProductCollection();
        this.products.GetMulti(null, prefetchProduct);

        PrefetchPath prefetchAlteration = new PrefetchPath(EntityType.AlterationEntity);
        prefetchAlteration.Add(AlterationEntityBase.PrefetchPathCustomTextCollection);
        this.alterations = new AlterationCollection();
        this.alterations.GetMulti(new PredicateExpression(AlterationFields.Version == 1), prefetchAlteration);

        PrefetchPath prefetchAlterationoption = new PrefetchPath(EntityType.AlterationoptionEntity);
        prefetchAlterationoption.Add(AlterationoptionEntityBase.PrefetchPathCustomTextCollection);
        this.alterationoptions = new AlterationoptionCollection();
        this.alterationoptions.GetMulti(new PredicateExpression(AlterationoptionFields.Version == 1), prefetchAlterationoption);

        PredicateExpression filterAttachment = new PredicateExpression();
        filterAttachment.Add(AttachmentFields.ProductId != DBNull.Value);
        PrefetchPath prefetchAttachment = new PrefetchPath(EntityType.AttachmentEntity);
        this.attachments = new AttachmentCollection();
        this.attachments.GetMulti(filterAttachment, prefetchAttachment);
    }

    public override void CreateCustomTexts()
    {
        foreach (CategoryEntity category in this.categories)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForCategory(category), category);
        }

        foreach (ProductEntity product in this.products)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForProduct(product), product);
        }

        foreach (AlterationEntity alteration in this.alterations)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForAlteration(alteration), alteration);
        }

        foreach (AlterationoptionEntity alterationoption in this.alterationoptions)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForAlterationoption(alterationoption), alterationoption);
        }

        foreach (AttachmentEntity attachment in this.attachments)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForAttachment(attachment), attachment);
        }
    }

    private int GetCompanyIdForCategory(CategoryEntity category)
    {
        return category.CompanyId;
    }    

    private int GetCompanyIdForProduct(ProductEntity product)
    {
        return product.CompanyId.GetValueOrDefault();
    }

    private int GetCompanyIdForAlteration(AlterationEntity alteration)
    {
        return alteration.CompanyId ?? -1;
    }

    private int GetCompanyIdForAlterationoption(AlterationoptionEntity alterationoption)
    {
        return alterationoption.CompanyId ?? -1;
    }

    private int GetCompanyIdForAttachment(AttachmentEntity attachment)
    {
        if (attachment.ProductId.GetValueOrDefault(0) > 0)
        {
            ProductEntity product = this.products.SingleOrDefault(x => x.ProductId == attachment.ProductId);
            return this.GetCompanyIdForProduct(product);
        }
        return -1;
    }
}