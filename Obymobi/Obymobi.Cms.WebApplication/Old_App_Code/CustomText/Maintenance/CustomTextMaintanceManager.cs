﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Collections.Generic;
using System.Linq;
using System;
using Obymobi.Data;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.EntityClasses;
using Dionysos;

/// <summary>
/// Summary description for CustomTextMaintanceManager
/// </summary>
public abstract class CustomTextMaintanceManager
{
    #region Properties

    private Dictionary<int, string> CompanyCultureCodes { get; set; }
    public CustomTextCollection CustomTextCollection { get; set; }
    
    #endregion

    #region Constructors

    public CustomTextMaintanceManager()
    {
        this.CustomTextCollection = new CustomTextCollection();
        this.CompanyCultureCodes = this.GetCompanyCultureCodes();        
    }

    #endregion

    #region Abstract methods

    public abstract void FetchData();
    public abstract void CreateCustomTexts();

    #endregion

    #region Protected methods

    protected void CreateDefaultCustomTextsIfNotExist(int companyId, ICustomTextContainingEntity entity)
    {
        if (companyId <= 0 || entity == null || !this.CompanyCultureCodes.ContainsKey(companyId))
            return;

        this.CreateDefaultCustomTextsIfNotExist(this.CompanyCultureCodes[companyId], entity);
    }

    protected void CreateDefaultCustomTextsIfNotExist(string defaultCompanyCultureCode, ICustomTextContainingEntity entity)
    {
        if (defaultCompanyCultureCode.IsNullOrWhiteSpace() || entity == null)
            return;

        Dictionary<int, CustomTextType> customTextTypesForEntity = CustomTextHelper.GetMappedDictionaryForEntity(entity); // Key: FieldIndex, Value: CustomTextType
        foreach (KeyValuePair<int, CustomTextType> entry in customTextTypesForEntity)
        {
            int fieldIndex = entry.Key;
            CustomTextType type = entry.Value;
            
            if (entity.Fields[fieldIndex].CurrentValue != null && !entity.Fields[fieldIndex].CurrentValue.ToString().IsNullOrWhiteSpace())
            {
                CustomTextEntity customTextEntity = entity.CustomTextCollection.SingleOrDefault(x => x.Type == type && x.CultureCode.Equals(defaultCompanyCultureCode, StringComparison.InvariantCultureIgnoreCase));
                if (customTextEntity == null)
                {
                    customTextEntity = new CustomTextEntity();
                    customTextEntity.CultureCode = defaultCompanyCultureCode;
                    customTextEntity.Type = type;
                    customTextEntity.Text = entity.Fields[fieldIndex].CurrentValue.ToString();
                    customTextEntity.SetNewFieldValue(entity.PrimaryKeyFields[0].Name.ToString(), entity.PrimaryKeyFields[0].CurrentValue);

                    this.CustomTextCollection.Add(customTextEntity);
                }
            }
        }        
    }

    #endregion

    #region Public methods

    public void SaveChanges(Transaction transaction, bool isTest)
    {
        if (this.CustomTextCollection.Count > 0 && !isTest)
        {
            foreach (CustomTextEntity customTextEntity in this.CustomTextCollection)
            {
                customTextEntity.Save();
            }

            //this.CustomTextCollection.AddToTransaction(transaction);
            //this.CustomTextCollection.SaveMulti();
        }        
    }    

    #endregion

    #region Private methods

    private Dictionary<int, string> GetCompanyCultureCodes()
    {
        IncludeFieldsList includes = new IncludeFieldsList();
        includes.Add(CompanyFields.CompanyId);
        includes.Add(CompanyFields.CultureCode);

        CompanyCollection companies = new CompanyCollection();
        companies.GetMulti(null, includes, null);        

        return companies.ToDictionary(x => x.CompanyId, x => x.CultureCode);
    }

    #endregion
}