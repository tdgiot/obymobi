﻿using System.Linq;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;

/// <summary>
/// Summary description for RoomControlCustomTextMaintenanceManager
/// </summary>
public class RoomControlCustomTextMaintenanceManager : CustomTextMaintanceManager
{
    private RoomControlConfigurationCollection roomControlConfigurations;
    private RoomControlAreaCollection roomControlAreas;
    private RoomControlSectionCollection roomControlSections;
    private RoomControlSectionItemCollection roomControlSectionItems;
    private RoomControlComponentCollection roomControlComponents;
    private RoomControlWidgetCollection roomControlWidgets;
    private StationListCollection stationLists;
    private StationCollection stations;

    public override void FetchData()
    {
        this.roomControlConfigurations = new RoomControlConfigurationCollection();
        this.roomControlConfigurations.GetMulti(null);

        PrefetchPath prefetchArea = new PrefetchPath(EntityType.RoomControlAreaEntity);
        prefetchArea.Add(RoomControlAreaEntityBase.PrefetchPathCustomTextCollection);
        this.roomControlAreas = new RoomControlAreaCollection();
        this.roomControlAreas.GetMulti(null, prefetchArea);

        PrefetchPath prefetchSection = new PrefetchPath(EntityType.RoomControlSectionEntity);
        prefetchSection.Add(RoomControlSectionEntityBase.PrefetchPathCustomTextCollection);
        this.roomControlSections = new RoomControlSectionCollection();
        this.roomControlSections.GetMulti(null, prefetchSection);

        PrefetchPath prefetchSectionItem = new PrefetchPath(EntityType.RoomControlSectionItemEntity);
        prefetchSectionItem.Add(RoomControlSectionItemEntityBase.PrefetchPathCustomTextCollection);
        this.roomControlSectionItems = new RoomControlSectionItemCollection();
        this.roomControlSectionItems.GetMulti(null, prefetchSectionItem);

        PrefetchPath prefetchComponent = new PrefetchPath(EntityType.RoomControlComponentEntity);
        prefetchComponent.Add(RoomControlComponentEntityBase.PrefetchPathCustomTextCollection);
        this.roomControlComponents = new RoomControlComponentCollection();
        this.roomControlComponents.GetMulti(null, prefetchComponent);

        PrefetchPath prefetchWidget = new PrefetchPath(EntityType.RoomControlWidgetEntity);
        prefetchWidget.Add(RoomControlWidgetEntityBase.PrefetchPathCustomTextCollection);
        this.roomControlWidgets = new RoomControlWidgetCollection();
        this.roomControlWidgets.GetMulti(null, prefetchWidget);

        this.stationLists = new StationListCollection();
        this.stationLists.GetMulti(null);

        PrefetchPath prefetchStation = new PrefetchPath(EntityType.StationEntity);
        prefetchStation.Add(StationEntityBase.PrefetchPathCustomTextCollection);
        prefetchStation.Add(StationEntityBase.PrefetchPathStationListEntity);
        this.stations = new StationCollection();
        this.stations.GetMulti(null, prefetchStation);
    }

    public override void CreateCustomTexts()
    {
        foreach (RoomControlAreaEntity roomControlArea in this.roomControlAreas)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForRoomControlArea(roomControlArea), roomControlArea);
        }

        foreach (RoomControlSectionEntity roomControlSection in this.roomControlSections)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForRoomControlSection(roomControlSection), roomControlSection);
        }

        foreach (RoomControlSectionItemEntity roomControlSectionItem in this.roomControlSectionItems)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForRoomControlSectionItem(roomControlSectionItem), roomControlSectionItem);
        }

        foreach (RoomControlComponentEntity roomControlComponent in this.roomControlComponents)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForRoomControlComponent(roomControlComponent), roomControlComponent);
        }

        foreach (RoomControlWidgetEntity roomControlWidget in this.roomControlWidgets)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForRoomControlWidget(roomControlWidget), roomControlWidget);
        }

        foreach (StationEntity station in this.stations)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForStation(station), station);
        }
    }

    private int GetCompanyIdForRoomControlConfiguration(RoomControlConfigurationEntity configuration)
    {
        return configuration.CompanyId;
    }

    private int GetCompanyIdForRoomControlArea(RoomControlAreaEntity area)
    {
        RoomControlConfigurationEntity configuration = this.roomControlConfigurations.SingleOrDefault(x => x.RoomControlConfigurationId == area.RoomControlConfigurationId);
        return this.GetCompanyIdForRoomControlConfiguration(configuration);
    }

    private int GetCompanyIdForRoomControlSection(RoomControlSectionEntity section)
    {
        RoomControlAreaEntity area = this.roomControlAreas.SingleOrDefault(x => x.RoomControlAreaId == section.RoomControlAreaId);
        return this.GetCompanyIdForRoomControlArea(area);
    }

    private int GetCompanyIdForRoomControlSectionItem(RoomControlSectionItemEntity sectionItem)
    {
        RoomControlSectionEntity section = this.roomControlSections.SingleOrDefault(x => x.RoomControlSectionId == sectionItem.RoomControlSectionId);
        return this.GetCompanyIdForRoomControlSection(section);
    }

    private int GetCompanyIdForRoomControlComponent(RoomControlComponentEntity component)
    {
        if (component.RoomControlSectionId.GetValueOrDefault(0) > 0)
        {
            RoomControlSectionEntity section = this.roomControlSections.SingleOrDefault(x => x.RoomControlSectionId == component.RoomControlSectionId);
            return this.GetCompanyIdForRoomControlSection(section);
        }        
        return -1;
    }

    private int GetCompanyIdForRoomControlWidget(RoomControlWidgetEntity widget)
    {
        if (widget.RoomControlSectionId.GetValueOrDefault(0) > 0)
        {
            RoomControlSectionEntity section = this.roomControlSections.SingleOrDefault(x => x.RoomControlSectionId == widget.RoomControlSectionId);
            return this.GetCompanyIdForRoomControlSection(section);
        }
        return -1;
    }

    private int GetCompanyIdForStationList(StationListEntity stationList)
    {
        return stationList.CompanyId;
    }

    private int GetCompanyIdForStation(StationEntity station)
    {
        StationListEntity stationList = this.stationLists.SingleOrDefault(x => x.StationListId == station.StationListId);
        return this.GetCompanyIdForStationList(stationList);
    }
}