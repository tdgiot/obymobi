﻿using System.Linq;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Data.HelperClasses;
using System;

/// <summary>
/// Summary description for SitesCustomTextMaintenanceManager
/// </summary>
public class SitesCustomTextMaintenanceManager : CustomTextMaintanceManager
{
    private SiteCollection sites;
    private SiteTemplateCollection siteTemplates;
    private PageCollection pages;
    private PageTemplateCollection pageTemplates;
    private AttachmentCollection attachments;    

    public override void FetchData()
    {
        PrefetchPath prefetchSite = new PrefetchPath(EntityType.SiteEntity);
        prefetchSite.Add(SiteEntityBase.PrefetchPathCustomTextCollection);
        this.sites = new SiteCollection();
        this.sites.GetMulti(null, prefetchSite);

        PrefetchPath prefetchSiteTemplate = new PrefetchPath(EntityType.SiteTemplateEntity);
        prefetchSiteTemplate.Add(SiteTemplateEntityBase.PrefetchPathCustomTextCollection);
        this.siteTemplates = new SiteTemplateCollection();
        this.siteTemplates.GetMulti(null, prefetchSiteTemplate);

        PrefetchPath prefetchPage = new PrefetchPath(EntityType.PageEntity);
        prefetchPage.Add(PageEntityBase.PrefetchPathCustomTextCollection);
        this.pages = new PageCollection();
        this.pages.GetMulti(null, prefetchPage);

        PrefetchPath prefetchPageTemplate = new PrefetchPath(EntityType.PageTemplateEntity);
        prefetchPageTemplate.Add(PageTemplateEntityBase.PrefetchPathCustomTextCollection);
        this.pageTemplates = new PageTemplateCollection();
        this.pageTemplates.GetMulti(null, prefetchPageTemplate);

        PredicateExpression filterAttachment = new PredicateExpression();
        filterAttachment.AddWithOr(AttachmentFields.PageId != DBNull.Value);
        filterAttachment.AddWithOr(AttachmentFields.PageTemplateId != DBNull.Value);
        PrefetchPath prefetchAttachment = new PrefetchPath(EntityType.AttachmentEntity);
        prefetchAttachment.Add(AttachmentEntityBase.PrefetchPathCustomTextCollection);
        this.attachments = new AttachmentCollection();
        this.attachments.GetMulti(filterAttachment, prefetchAttachment);
    }

    public override void CreateCustomTexts()
    {
        foreach (SiteEntity site in this.sites)
        {
            this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, site);
        }

        foreach (SiteTemplateEntity siteTemplate in this.siteTemplates)
        {
            this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, siteTemplate);
        }

        foreach (PageEntity page in this.pages)
        {
            this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, page);
        }

        foreach (PageTemplateEntity pageTemplate in this.pageTemplates)
        {
            this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, pageTemplate);
        }

        foreach (AttachmentEntity attachment in this.attachments)
        {
            this.CreateDefaultCustomTextsIfNotExist(CustomTextHelper.DefaultCulture.Code, attachment);
        }
    }    
}