﻿using System.Linq;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using System;

/// <summary>
/// Summary description for CompanyCustomTextMaintenanceManager
/// </summary>
public class CompanyCustomTextMaintenanceManager : CustomTextMaintanceManager
{
    private AdvertisementCollection advertisements;
    private AnnouncementCollection announcements;
    private AvailabilityCollection availabilities;
    private CompanyCollection companies;
    private DeliverypointgroupCollection deliverypointgroups;
    private UIModeCollection uiModes;
    private UIFooterItemCollection uiFooterItems;
    private UITabCollection uiTabs;
    private UIWidgetCollection uiWidgets;

    public override void FetchData()
    {
        PredicateExpression filterAdvertisement = new PredicateExpression();
        filterAdvertisement.Add(AdvertisementFields.CompanyId != DBNull.Value);
        PrefetchPath prefetchAdvertisement = new PrefetchPath(EntityType.AdvertisementEntity);
        prefetchAdvertisement.Add(AdvertisementEntityBase.PrefetchPathCustomTextCollection);
        this.advertisements = new AdvertisementCollection();
        this.advertisements.GetMulti(filterAdvertisement, prefetchAdvertisement);

        PredicateExpression filterAnnouncement = new PredicateExpression();
        filterAnnouncement.Add(AnnouncementFields.CompanyId != DBNull.Value);
        PrefetchPath prefetchAnnouncement = new PrefetchPath(EntityType.AnnouncementEntity);
        prefetchAnnouncement.Add(AnnouncementEntityBase.PrefetchPathCustomTextCollection);
        this.announcements = new AnnouncementCollection();
        this.announcements.GetMulti(filterAnnouncement, prefetchAnnouncement);

        PrefetchPath prefetchAvailability = new PrefetchPath(EntityType.AvailabilityEntity);
        prefetchAvailability.Add(AvailabilityEntityBase.PrefetchPathCustomTextCollection);
        this.availabilities = new AvailabilityCollection();
        this.availabilities.GetMulti(null, prefetchAvailability);

        PrefetchPath prefetchCompany = new PrefetchPath(EntityType.CompanyEntity);
        prefetchCompany.Add(CompanyEntityBase.PrefetchPathCustomTextCollection);
        this.companies = new CompanyCollection();
        this.companies.GetMulti(null, prefetchCompany);

        PrefetchPath prefetchDeliverypointgroup = new PrefetchPath(EntityType.DeliverypointgroupEntity);
        prefetchDeliverypointgroup.Add(DeliverypointgroupEntityBase.PrefetchPathCustomTextCollection);
        this.deliverypointgroups = new DeliverypointgroupCollection();
        this.deliverypointgroups.GetMulti(null, prefetchDeliverypointgroup);

        PredicateExpression filterUiModes = new PredicateExpression();
        filterUiModes.Add(UIModeFields.CompanyId != DBNull.Value);
        this.uiModes = new UIModeCollection();
        this.uiModes.GetMulti(filterUiModes);

        PrefetchPath prefetchUiFooterItem = new PrefetchPath(EntityType.UIFooterItemEntity);
        prefetchUiFooterItem.Add(UIFooterItemEntityBase.PrefetchPathCustomTextCollection);
        this.uiFooterItems = new UIFooterItemCollection();
        this.uiFooterItems.GetMulti(null, prefetchUiFooterItem);

        PrefetchPath prefetchUiTab = new PrefetchPath(EntityType.UITabEntity);
        prefetchUiTab.Add(UITabEntityBase.PrefetchPathCustomTextCollection);
        this.uiTabs = new UITabCollection();
        this.uiTabs.GetMulti(null, prefetchUiTab);

        PrefetchPath prefetchUiWidget = new PrefetchPath(EntityType.UIWidgetEntity);
        prefetchUiWidget.Add(UIWidgetEntityBase.PrefetchPathCustomTextCollection);
        this.uiWidgets = new UIWidgetCollection();
        this.uiWidgets.GetMulti(null, prefetchUiWidget);        
    }

    public override void CreateCustomTexts()
    {
        foreach (AdvertisementEntity advertisement in this.advertisements)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForAdvertisement(advertisement), advertisement);
        }

        foreach (AnnouncementEntity announcement in this.announcements)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForAnnouncement(announcement), announcement);
        }

        foreach (AvailabilityEntity availability in this.availabilities)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForAvailability(availability), availability);
        }

        foreach (CompanyEntity company in this.companies)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForCompany(company), company);
        }

        foreach (DeliverypointgroupEntity deliverypointgroup in this.deliverypointgroups)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForDeliverypointgroup(deliverypointgroup), deliverypointgroup);
        }

        foreach (UIFooterItemEntity uiFooterItem in this.uiFooterItems)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForUIFooterItem(uiFooterItem), uiFooterItem);
        }

        foreach (UITabEntity uiTab in this.uiTabs)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForUITab(uiTab), uiTab);
        }

        foreach (UIWidgetEntity uiWidget in this.uiWidgets)
        {
            this.CreateDefaultCustomTextsIfNotExist(this.GetCompanyIdForUIWidget(uiWidget), uiWidget);
        }
    }

    private int GetCompanyIdForAdvertisement(AdvertisementEntity advertisement)
    {
        return advertisement.CompanyId ?? -1;
    }

    private int GetCompanyIdForAnnouncement(AnnouncementEntity announcement)
    {
        return announcement.CompanyId ?? -1;
    }

    private int GetCompanyIdForAvailability(AvailabilityEntity availability)
    {
        return availability.CompanyId;
    }

    private int GetCompanyIdForCompany(CompanyEntity company)
    {
        return company.CompanyId;
    }

    private int GetCompanyIdForDeliverypointgroup(DeliverypointgroupEntity deliverypointgroup)
    {
        return deliverypointgroup.CompanyId;
    }

    private int GetCompanyIdForUIMode(UIModeEntity uiMode)
    {
        return uiMode.CompanyId ?? -1;
    }

    private int GetCompanyIdForUIFooterItem(UIFooterItemEntity uiFooterItem)
    {
        UIModeEntity uiMode = this.uiModes.SingleOrDefault(x => x.UIModeId == uiFooterItem.UIModeId);
        if (uiMode != null)
        {
            return this.GetCompanyIdForUIMode(uiMode);
        }
        return -1;        
    }

    private int GetCompanyIdForUITab(UITabEntity uiTab)
    {
        UIModeEntity uiMode = this.uiModes.SingleOrDefault(x => x.UIModeId == uiTab.UIModeId);
        if (uiMode != null)
        {
            return this.GetCompanyIdForUIMode(uiMode);
        }
        return -1;
    }

    private int GetCompanyIdForUIWidget(UIWidgetEntity uiWidget)
    {
        UITabEntity uiTab = this.uiTabs.SingleOrDefault(x => x.UITabId == uiWidget.UITabId);
        if (uiTab != null)
        {
            return this.GetCompanyIdForUITab(uiTab);
        }
        return -1;
    }
}