﻿using System.Web;
using Dionysos;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data;
using Obymobi.Enums;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using MarkdownSharp.Extensions;

/// <summary>
/// The CustomTextRenderer can be used to render CustomTextEntities on a page and save the entered values.
/// </summary>
public class CustomTextRenderer
{
    #region Properties

    private readonly ICustomTextContainingEntity entity;
    private readonly EntityView<CustomTextEntity> entityView;
    private readonly string cultureCode;

    #endregion

    #region Constructors

    public CustomTextRenderer(ICustomTextContainingEntity entity, string cultureCode)
    {
        this.entity = entity;
        this.entityView = entity.CustomTextCollection.DefaultView;
        this.cultureCode = cultureCode;
    }

    #endregion

    #region Methods
    
    public void RenderCustomText(TextBox tb, CustomTextType type, bool removeMarkdown = false)
    {
        PredicateExpression filter = this.CreateDefaultFilter();
        filter.Add(CustomTextFields.Type == type);

        this.entityView.Filter = filter;
        if (this.entityView.Count > 0)
        {
            if (!removeMarkdown)
            {
                tb.Text = this.entityView[0].Text;
            }
            else
            {
                MarkdownHelper.RemoveMarkdown(this.entityView[0].Text);
            }            
        }        
	}

    public void SaveCustomText(TextBox tb, CustomTextType type, bool addMarkdown = false)
    {
        if (!tb.HasTextChanged)
        {
            return;
        }

        PredicateExpression filter = this.CreateDefaultFilter();
        filter.Add(CustomTextFields.Type == type);

        this.entityView.Filter = filter;
        if (tb.Text.IsNullOrWhiteSpace() && this.entityView.Count > 0)
        {
            // No value
            this.entityView[0].Delete();
            this.entityView.RelatedCollection.Remove(this.entityView[0]);
        }
        else if (!tb.Text.IsNullOrWhiteSpace())
        {
            // Value
            CustomTextEntity entity;
            if (this.entityView.Count > 0)
            {
                entity = this.entityView[0];
            }
            else
            {
                entity = this.CreateCustomTextEntity(type);
            }

            entity.Text = addMarkdown ? MarkdownHelper.AddMarkdown(tb.Text) : tb.Text;
            entity.Save();
        }
    }

    private CustomTextEntity CreateCustomTextEntity(CustomTextType type)
    {
        string foreignKey = this.entity.LLBLGenProEntityName.Replace("Entity", "Id");
        int foreignKeyValue = int.Parse(this.entity.PrimaryKeyFields[0].CurrentValue.ToString());

        CustomTextEntity customTextEntity = new CustomTextEntity();
        customTextEntity.CultureCode = this.cultureCode;
        customTextEntity.Type = type;
        customTextEntity.SetNewFieldValue(foreignKey, foreignKeyValue);

        return customTextEntity;
    }

    private PredicateExpression CreateDefaultFilter()
    {
        return new PredicateExpression(CustomTextFields.CultureCode == this.cultureCode);
    }

    #endregion   
}