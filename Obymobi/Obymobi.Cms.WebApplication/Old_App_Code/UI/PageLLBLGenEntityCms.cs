﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Obymobi.ObymobiCms.UI
{
    /// <summary>
    /// Summary description for PageLLBLGenEntityCms
    /// </summary>
    public class PageLLBLGenEntityCms : Dionysos.Web.UI.PageLLBLGenEntity
    {
        public PageLLBLGenEntityCms()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public override bool Save()
        {
            if (this.PageMode == Dionysos.Web.PageMode.Add)
            {
                if (this.DataSource.Fields["CompanyId"] != null &&                    
                    !(this.DataSource is SiteEntity) &&
                    !(this.DataSource is UIThemeEntity))
                {
                    this.DataSource.SetNewFieldValue("CompanyId", CmsSessionHelper.CurrentCompanyId);
                }
            }

            return base.Save();
        }
    }
}