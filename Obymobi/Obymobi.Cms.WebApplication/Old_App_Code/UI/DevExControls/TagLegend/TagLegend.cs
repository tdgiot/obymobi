﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Obymobi.Enums;

namespace Obymobi.Cms.WebApplication.Old_App_Code.UI.DevExControls
{
    public class TagLegend : WebControl
    {
        protected override void RenderContents(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "tags-legend");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);

            RenderLegendItem(writer, TagType.System);
            RenderLegendItem(writer, TagType.Company);

            writer.RenderEndTag();
        }

        private void RenderLegendItem(HtmlTextWriter writer, TagType tagType)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, $"tag-legend-item tag-legend-{tagType.GetStringValue()?.ToLower()}");
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
            writer.Write(tagType);
            writer.RenderEndTag();
        }
    }
}