﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Dionysos.Web.UI.DevExControls;
using Obymobi.Cms.WebApplication.Old_App_Code.Extensions;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Interfaces;
using Obymobi.Web.Analytics.Repositories;

namespace Obymobi.Cms.WebApplication.Old_App_Code.UI.DevExControls
{
	public class TagBox : TokenBox
	{
		private readonly ITagRepository tagRepository;

		public TagBox()
		{
			tagRepository = new TagRepository();
			LoadAvailableTags();

			ClientSideEvents.Init = "onInit";
			ClientSideEvents.TokensChanged = "onTokensChanged";

			PreRender += TagBox_PreRender;
		}

		private void TagBox_PreRender(object sender, EventArgs args)
		{
			string tagBoxJavaScriptPath = HttpContext.Current.Server.MapPath($"~/{nameof(Old_App_Code)}/{nameof(UI)}/{nameof(DevExControls)}/{nameof(TagBox)}/TagBox.js");
			string tagBoxJavaScript = File.ReadAllText(tagBoxJavaScriptPath);

			Page.ClientScript.RegisterClientScriptBlock(GetType(), "TagBoxScript", tagBoxJavaScript, true);
		}

		public void SetSelectedTags(IEnumerable<ITaggableEntity> taggableEntities) =>
			SetSelectedTokens(taggableEntities.Select(x => x.TagEntity)
				.OrderBy(tag => tag.CompanyId)
				.ThenBy(tag => tag.Name)
				.Select((tag, index) => tag.ToToken(index)));

		public void SetSelectedTags(IEnumerable<int> tagIds) => SetSelectedTokens(TokenCollection.Where(token => tagIds.Contains(token.Id)));

		public void SetInheritedTags(IEnumerable<ITaggableEntity> inheritedTags) =>
			HideTokens(inheritedTags.Select(t => t.ToToken()));

		public ICollection<ITag> GetSelectedTags() =>
			SelectedTokens
				.Union(SelectedHiddenTokens)
				.Select(token => token.ToTag())
				.ToArray();

		private void LoadAvailableTags()
		{
			TagCollection tagCollection = tagRepository.GetByCompanyId(CmsSessionHelper.CurrentCompanyId);

			TokenCollection.Clear();
			TokenCollection.AddRange(tagCollection.ToTokens());
		}

		public override void Dispose()
		{
			PreRender -= TagBox_PreRender;
			base.Dispose();
		}
	}
}
