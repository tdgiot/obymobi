﻿function setReadonlyTokens(sender, ev) {
	var readOnlyTokens = sender.cpReadOnlyTokens || [];
	for (var i = 0; i < readOnlyTokens.length; i++) {
		var index = sender.GetTokenIndexByText(readOnlyTokens[i]);
		if (index != -1) {
			var tokenElement = sender.GetTokenHtmlElement(index);
			var removeBtn = tokenElement.querySelector(".removeButton");
			if (removeBtn) {
				removeBtn.style.display = 'none';
			}
		}
	}
}

function setTokenColor(sender, token) {
	var index = sender.GetTokenIndexByText(token.Name);
	if (index != -1) {
		var tokenElement = sender.GetTokenHtmlElement(index);
		if (!tokenElement) {
			return;
		}

		tokenElement.className = 'token ' + token.Color;
	}
}

function setInitialTokens(sender, ev) {
	var tokens = sender.cpInitialTokens || [];
	for (var token of tokens) {
		setTokenColor(sender, token);
	}
}

function onInit(sender, ev) {
	setInitialTokens(sender, ev);
	setReadonlyTokens(sender, ev);
}

function onTokensChanged(sender, ev) {
	makePageDirty();

	var newTokens = sender.GetTokenCollection();
	var lastToken = newTokens[newTokens.length - 1];

	var initialTokens = sender.cpInitialTokens || [];
	var token = initialTokens.find(t => t.Name === lastToken);
	if (!token) {
		var itemsManager = sender.listBox.itemsManager;
		var listBoxItems = itemsManager.deserializedItems;

		var item = listBoxItems.find(i => i.text === lastToken);
		if (item) {
			token = {
				Name: item.text,
				Color: item.cssClass
			};
		}
	}

	if (token) {
		setTokenColor(sender, token);
	}
}
