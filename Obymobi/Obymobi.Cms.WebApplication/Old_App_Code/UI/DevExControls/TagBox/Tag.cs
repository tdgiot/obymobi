﻿using Obymobi.Interfaces;

namespace Obymobi.Cms.WebApplication.Old_App_Code.UI.DevExControls
{

	public class Tag : ITag
	{
		public Tag()
		{ }

		public Tag(int id, string name)
		{
			this.TagId = id;
			this.Name = name;
		}

		public int TagId { get; set; }
		public string Name { get; set; }
		public int? CompanyId { get; set; }
	}
}
