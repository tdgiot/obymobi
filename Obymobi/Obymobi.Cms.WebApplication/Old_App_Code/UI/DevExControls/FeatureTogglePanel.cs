﻿using System;
using System.ComponentModel;
using Dionysos.Web.UI.WebControls;
using Obymobi.Cms.Logic.UseCases;
using Obymobi.Enums;

namespace Obymobi.Cms.WebApplication.Old_App_Code.UI.DevExControls
{
    public class FeatureTogglePanel : PlaceHolder
    {
        private readonly DetermineFeatureAvailabilityUseCase determineFeatureAvailabilityUseCase = new DetermineFeatureAvailabilityUseCase();

        [Browsable(true)]
        public FeatureToggle ToggleType { get; set; }

        protected override void OnInit(EventArgs e) => Visible = determineFeatureAvailabilityUseCase.Execute(CmsSessionHelper.CurrentRole, CmsSessionHelper.CurrentCompanyReleaseGroup, ToggleType);
    }
}