﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dionysos;
using Obymobi.Data;
using Obymobi.Enums;
using Obymobi.Extensions;
using Obymobi.Interfaces;
using Label = Dionysos.Web.UI.WebControls.Label;

namespace Obymobi.Cms.WebApplication.Old_App_Code.UI.DevExControls
{
    [DefaultProperty("Tokens")]
    [ToolboxData("<{0}:TagOverview runat=server></{0}:TagOverview>")]
    public class TagOverview : WebControl
    {
        private Dictionary<string, List<ITag>> TagGroups { get; } = new Dictionary<string, List<ITag>>();

        public void AddTagGroup(string label, IEnumerable<ITaggableEntity> taggableEntities)
        {
            if (label == null)
            {
                return;
            }

            var tags = taggableEntities.Select(x => x.TagEntity).ToList();
            if (!tags.Any())
            {
                return;
            }

            if (!TagGroups.TryGetValue(label, out var tagList))
            {
                TagGroups.Add(label, new List<ITag>(tags));
            }
            else
            {
                tagList.AddRange(tags);
            }
        }

        public void AddTagGroup(string label, ITaggableEntity taggableEntity)
        {
            AddTagGroup(label, new[] { taggableEntity });
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (TagGroups.Any())
            {
                RenderTagTable(writer);
            }
            else
            {
                Label l = new Label("no-inherit-tags", "No inherited tags");
                l.RenderControl(writer);
            }
        }

        private void RenderTagTable(HtmlTextWriter writer)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            foreach (KeyValuePair<string, List<ITag>> kv in TagGroups)
            {
                if (!kv.Value.Any())
                {
                    continue;
                }

                this.RenderTagTableRow(writer, kv);
            }

            writer.RenderEndTag();
        }

        private void RenderTagTableRow(HtmlTextWriter writer, KeyValuePair<string, List<ITag>> kv)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);

            if (!kv.Key.IsNullOrWhiteSpace())
            {
                RenderTableRowLabel(writer, kv.Key);
            }
            RenderTableRowTags(writer, kv.Value);

            writer.RenderEndTag();
        }

        private void RenderTableRowLabel(HtmlTextWriter writer, string label)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Class, "inherited-tag-row");
            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            writer.Write(label);
            
            writer.RenderEndTag();
        }

        private void RenderTableRowTags(HtmlTextWriter writer, List<ITag> tags)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.Td);

            HashSet<string> seenTags = new HashSet<string>();
            foreach (ITag tag in tags.OrderByDescending(tag => tag.GetTagType()).ThenBy(tag => tag.Name))
            {
                if (!seenTags.Add($"[{tag.GetTagType()}]{tag.Name}"))
                {
                    continue;
                }

                writer.AddAttribute(HtmlTextWriterAttribute.Class, tag.GetTagType() == TagType.Company
                    ? "company-tag-label"
                    : "system-tag-label"
                );
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.Write(tag.Name);
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
        }
    }
}
