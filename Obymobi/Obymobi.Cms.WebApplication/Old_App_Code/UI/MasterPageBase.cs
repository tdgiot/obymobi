﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Dionysos;
using Dionysos.Data;
using Dionysos.Data.LLBLGen;
using Dionysos.Interfaces;
using Dionysos.Interfaces.Data;
using Dionysos.Reflection;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.ObymobiCms.Web.UI
{
    /// <summary>
    /// Summary description for MasterPageBase
    /// </summary>
    public class MasterPageBase : Dionysos.Web.UI.MasterPage
    {
        public MasterPageBase()
        {
        }

        private List<string[]> GetBreadCrumbElements(IEntity entity, IEntityInformation info)
        {
            // All elements for the breadcrumbs are stored to be rendered later in the format:
            // [EntityEditPage, EntityId, EntityDisplayValue]
            List<string[]> breadCrumbElements = new List<string[]>();

            // Regex for parsing TitleAsBreadCrumbHierarchy on EntityInfo
            Regex breadcrumbFormat = new Regex(@"\[(?<Value>[a-zA-Z,]+)\]");
            MatchCollection matches;
            if (info.EntityName == LLBLGenUtil.GetEntityName("Media"))
            {
                // the Media Entity has a custom implementation, as long as it's only
                // one customer implmentation, or maybe 2, we don't build a full generic
                // solution
                // It's simple, we just need to retrieve the BreadCrumb for the related entity and attach the media information
                MediaEntity media = entity as MediaEntity;
                if (media.RelatedEntity != null)
                {
                    IEntityInformation relatedEntityInfo;
                    string customBreadCrumb;
                    if (media.RelatedEntity.LLBLGenProEntityTypeValue == (int)Obymobi.Data.EntityType.PageElementEntity)
                    {
                        // Needs an exception at the PageElement doesn't have a editing aspx page itself, it's only the PageEntity that has that.
                        _ = EntityInformationUtil.GetEntityInformation("PageEntity");
                        entity = ((MediaEntity)entity).RelatedEntity;
                        customBreadCrumb = "[SiteEntity][PageEntity]";
                    }
                    else if (media.RelatedEntity.LLBLGenProEntityTypeValue == (int)Obymobi.Data.EntityType.UIThemeEntity)
                    {
                        customBreadCrumb = ((Obymobi.Data.EntityClasses.UIThemeEntity)((MediaEntity)entity).RelatedEntity).Name;
                    }
                    else
                    {
                        relatedEntityInfo = EntityInformationUtil.GetEntityInformation(media.RelatedEntity);
                        customBreadCrumb = relatedEntityInfo.TitleAsBreadCrumbHierarchy + string.Format("[{0}Entity]", LLBLGenUtil.GetEntityName(media.RelatedEntity));
                    }
                    

                    // Add the Related Entity to the String of BreadCrumb Items
                    

                    matches = breadcrumbFormat.Matches(customBreadCrumb);
                }
                else
                    matches = breadcrumbFormat.Matches("NONE");
            }
            else
            {
                matches = breadcrumbFormat.Matches(info.TitleAsBreadCrumbHierarchy);
            }


            if (matches.Count > 0)
            {
                // Here we have matches, so we can build a breadcrumb


                // Last Used Entity in the loop, we initially start with the entity of the page
                // as that's the lowest child. Since we iterate reversely we constantly can find 
                // the parent on the last used entity for building the bread crumb
                // 1st: Order, 2nd: Customer, 3rd: Debtor
                IEntity lastUsedEntity = entity;

                // Loop through all elements reversed
                for (int i = matches.Count - 1; i >= 0; i--) // We reverse, since we start at the lowest entity in the hierarchy
                {
                    // Init variables needed per iteration
                    Match match = matches[i];
                    string relatedEntityPropertyName = string.Empty;
                    string relatedEntityShowField = string.Empty;

                    // Get the PropertyName where to find the related parent entity
                    relatedEntityPropertyName = match.Groups["Value"].Value;

                    // Now try to invoke the property or retrieve by getting PK vlaue of related entity from querystring
                    try
                    {
                        IEntity currentEntity = Member.InvokeProperty(lastUsedEntity, relatedEntityPropertyName) as IEntity;
                        if (currentEntity == null)
                            throw new Dionysos.TechnicalException("RelatedEntityPropertyName: {0} on Entity of Type: {1} as IEntity == null",
                                relatedEntityPropertyName, entity.LLBLGenProEntityName);
                        else if (currentEntity.PrimaryKeyFields[0].CurrentValue == null)
                        {
                            // If page is in Add mode, try if we have the FK value on the QueryString if not set
                            int pkValue;
                            if (QueryStringHelper.GetInt(currentEntity.PrimaryKeyFields[0].Name, out pkValue))
                                currentEntity = Dionysos.DataFactory.EntityFactory.GetEntity(currentEntity.LLBLGenProEntityName, pkValue) as IEntity;
                        }

                        // Retrieve Entity Info of this Current (parent) entity
                        IEntityInformation currentEntityInfo = EntityInformationUtil.GetEntityInformation(currentEntity.LLBLGenProEntityName);

                        // Create the element
                        // [EntityEditPage, EntityId, EntityDisplayValue]
                        if (currentEntity.PrimaryKeyFields[0].CurrentValue != null)
                        {
                            string[] element = new string[3];
                            element[0] = this.ResolveUrl(currentEntityInfo.DefaultEntityEditPage);
                            element[1] = currentEntity.PrimaryKeyFields[0].CurrentValue.ToString();
                            element[2] = EntityInformationUtil.GetShowFieldNameBreadCrumbValue(currentEntity);

                            if (lastUsedEntity is PageElementEntity)
                            {
                                var pageElement = (PageElementEntity)lastUsedEntity;
                                // Activate the correct tab:
                                string tabname = "editor-agnostic";
                                if (!pageElement.CultureCode.IsNullOrWhiteSpace())
                                    tabname = "editor_" + pageElement.CultureCode;

                                element[0] += "?{0}={1}".FormatSafe(Dionysos.Web.UI.DevExControls.PageControl.queryStringElementToActiveTab, tabname);
                            }

                            breadCrumbElements.Add(element);
                        }

                        // Set current entity as last used
                        lastUsedEntity = currentEntity;
                    }
                    catch (Exception ex)
                    {
                        throw new Dionysos.TechnicalException(ex, "RelatedEntityPropertyName: {0} on Entity of Type: {1} not available or other error",
                            relatedEntityPropertyName, entity.LLBLGenProEntityName);
                    }
                }
            }

            return breadCrumbElements;
        }

        public bool GetHtmlForBreadCrumbTitle(IEntity entity, IEntityInformation info, out string html)
        {
            bool toReturn = true;
            try
            {
                // MDB TODO
                //if (!Dionysos.ConfigurationManager.GetBool(EvAdminConfigurationConstants.ShowTitlesAsBreadCrumbsFromEntityInfo))
                /*
                if (false)
                {
                    html = string.Empty;
                    return false;
                }
                else
                    toReturn = true;
                 */

                html = string.Empty;

                List<string[]> breadCrumbElements = this.GetBreadCrumbElements(entity, info);
                if (breadCrumbElements.Count == 0)
                {
                    html = string.Empty;
                    return false;
                }

                // Now we have all elements, render HTML				
                html += "<span class=\"breadcrumb\">";

                // Render this entities type name
                html += info.FriendlyName + ": ";

                // Render links
                for (int i = breadCrumbElements.Count - 1; i >= 0; i--)
                {
                    // <a href="{EntityEditPage}?id={EntityId}">{EntityDisplayValue}</a>
                    string[] element = breadCrumbElements[i];
                    string cssClass = string.Empty;
                    if (breadCrumbElements.Count == 1)
                        cssClass = "single";
                    else if (i == breadCrumbElements.Count - 1)
                        cssClass = "first";
                    else if (i == 0)
                        cssClass = "last";

                    string linkHtml;
                    if(element[0].Contains("?")) // a custom querystring is provided
                        linkHtml = "<a href=\"{0}&id={1}&mode=edit\" class=\"{2}\">{3}</a> &gt; ";
                    else
                        linkHtml = "<a href=\"{0}?id={1}&mode=edit\" class=\"{2}\">{3}</a> &gt; ";

                    html += string.Format(linkHtml, element[0], element[1], cssClass, element[2]);
                }

                // Render name of current entity, OR Toevoegen if new
                try
                {
                    if (this.PageAsPageEntity.PageMode == PageMode.Add)
                    {
                        // MDB TODO
                        //html += InCodeMessages.ExtraVestiging_EVAdmin_Web_UI_MasterPageBase_Breadcrumb_Add;
                    }
                    else
                    {
                        string displayValue = EntityInformationUtil.GetShowFieldNameBreadCrumbValue(entity);
                        if (displayValue.Length == 0)
                            displayValue = "#";
                        html += displayValue;
                    }
                }
                catch
                {
                    html += "#";
                }


                html += "</span>";
            }
            catch 
            {
                if (TestUtil.IsPcDeveloper)
                    throw;
                html = string.Empty;
                toReturn = false;
            }


            return toReturn;
        }
        
        public Dionysos.Web.UI.PageEntity PageAsPageEntity
        {
            get
            {
                return this.Page as Dionysos.Web.UI.PageEntity;
            }
        }

        public IGuiEntityCollectionSimple PageAsPageEntityCollection
        {
            get
            {
                return this.Page as IGuiEntityCollectionSimple;
            }
        }
    }
}

