﻿using System;
using DevExpress.Web;
using Dionysos;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.WebApplication.Old_App_Code.Extensions
{
    public static class ASPxListBoxExtensions
    {
        public static ASPxListBox DataBindEntityCollection<TEntity>(this ASPxListBox listbox, IPredicate predicate, IEntityField sortField, params IEntityFieldCore[] fields) 
            where TEntity : EntityBase, new()
        {
            string entityName = typeof(TEntity).Name.Replace("Entity", "");
            if (Instance.ArgumentIsEmpty(entityName, "entityName"))
            {
                throw new ArgumentException("Entity name is empty");
            }

            if (!(DataFactory.EntityCollectionFactory.GetEntityCollection(entityName) is IEntityCollection entityCollection) ||
                Instance.Empty(entityCollection))
            {
                throw new EmptyException("Variable 'entityCollection' is empty.");
            }

            PredicateExpression filter = new PredicateExpression(predicate);
            SortExpression sort = new SortExpression(new SortClause(sortField, SortOperator.Ascending));
            IncludeFieldsList includes = new IncludeFieldsList(fields);

            entityCollection.GetMulti(filter, 0, sort, null, null, includes, 0, 0);

            listbox.DataSource = entityCollection;
            listbox.DataBind();

            return listbox;
        }
    }
}