﻿using Obymobi.Cms.Logic.UseCases;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

public static class UserEntityExtensions
{
    private static readonly DetermineFeatureAvailabilityUseCase DetermineFeatureAvailabilityUseCase = new DetermineFeatureAvailabilityUseCase();

    public static bool HasFeature(this UserEntity user, FeatureToggle toggleType) =>
        DetermineFeatureAvailabilityUseCase.Execute(user.Role, CmsSessionHelper.CurrentCompanyReleaseGroup, toggleType);
}