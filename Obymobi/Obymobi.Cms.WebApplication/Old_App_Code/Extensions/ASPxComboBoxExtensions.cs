﻿using System;
using DevExpress.Web;
using Dionysos;
using SD.LLBLGen.Pro.ORMSupportClasses;

/// <summary>
/// Summary description for ASPxComboBoxExtensions
/// </summary>
public static class ASPxComboBoxExtensions
{
    public static void DataBindEnum<T>(this ASPxComboBox combobox, T[] types)
    {
        combobox.Items.Clear();
        foreach (T value in types)
        {
            string enumText = EnumUtil.GetStringValue(value as Enum, null);
            int enumValue = Convert.ToInt32(value);

            combobox.Items.Add(enumText, enumValue);
        }        
    }

    public static void DataBindEntityCollection<TEntity>(this ASPxComboBox combobox, IPredicate predicate, IEntityField sortField, params IEntityFieldCore[] fields) where TEntity : EntityBase, new()
    {
        DataBindEntityCollection<TEntity>(combobox, predicate, null, sortField, fields);
    }

    public static void DataBindEntityCollection<TEntity>(this ASPxComboBox combobox, IPredicate predicate, IRelationCollection relations, IEntityField sortField, params IEntityFieldCore[] fields) where TEntity : EntityBase, new()
    {
        string entityName = typeof(TEntity).Name.Replace("Entity", "");
        if (Instance.ArgumentIsEmpty(entityName, "entityName"))
        {
            // Parameter 'entityName' is empty
        }
        else
        {
            IEntityCollection entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(entityName) as IEntityCollection;
            if (entityCollection == null || Instance.Empty(entityCollection))
            {
                throw new EmptyException("Variable 'entityCollection' is empty.");
            }
            else
            {
                PredicateExpression filter = new PredicateExpression(predicate);
                SortExpression sort = new SortExpression(new SortClause(sortField, SortOperator.Ascending));
                IncludeFieldsList includes = new IncludeFieldsList(fields);

                entityCollection.GetMulti(filter, 0, sort, relations, null, includes, 0, 0);

                combobox.DataSource = entityCollection;
                combobox.DataBind();
            }
        }
    }
}