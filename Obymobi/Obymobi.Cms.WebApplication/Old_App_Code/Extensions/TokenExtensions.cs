﻿using Dionysos.Web.DevExpress.UI.DevExControls.TokenBox.Models;
using Obymobi.Cms.WebApplication.Old_App_Code.UI.DevExControls;

namespace Obymobi.Cms.WebApplication.Old_App_Code.Extensions
{
	public static class TokenExtensions
	{
		public static Tag ToTag(this Token token) => new Tag(token.Id, token.Name);
	}
}
