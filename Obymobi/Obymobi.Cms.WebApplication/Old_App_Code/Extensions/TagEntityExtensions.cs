﻿using Dionysos.Web.DevExpress.UI.DevExControls.TokenBox.Models;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Cms.WebApplication.Old_App_Code.Extensions
{
	public static class TagEntityExtensions
	{
		public static Token ToToken(this TagEntity tagEntity, int index) =>
			new Token
			{
				Id = tagEntity.TagId,
				Name = tagEntity.Name,
				Color = tagEntity.CompanyId.GetValueOrDefault() > 0 ? "blue" : "red",
				SortOrder = index
			};

		public static Token ToToken(this ITaggableEntity tag) =>
			new Token
			{
				Id = tag.TagId,
				Name = tag.TagEntity.Name
			};
	}
}
