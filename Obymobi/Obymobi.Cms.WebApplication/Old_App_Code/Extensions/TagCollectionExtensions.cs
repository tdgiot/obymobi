﻿using System.Collections.Generic;
using System.Linq;
using Dionysos.Web.DevExpress.UI.DevExControls.TokenBox.Models;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Cms.WebApplication.Old_App_Code.Extensions
{
	public static class TagCollectionExtensions
	{
		public static IEnumerable<Token> ToTokens(this TagCollection tagCollection) =>
			tagCollection.Select((tagEntity, index) => TagEntityExtensions.ToToken(tagEntity, index)).ToArray();
	}
}
