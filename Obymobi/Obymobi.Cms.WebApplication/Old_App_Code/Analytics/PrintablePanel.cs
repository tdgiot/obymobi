﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevExpress.XtraPrinting;

public interface PrintablePanel
{
    PrintableComponentLink[] GetPrintableComponentLinks(PrintingSystem printingSystem);
}