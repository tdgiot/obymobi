﻿using Dionysos;
using Dionysos.Diagnostics;
using Dionysos.Web;
using Obymobi;
using Obymobi.Analytics.Google;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

/// <summary>
/// Summary description for GoogleAccountsCache
/// </summary>
public static class GoogleManagementApiCache
{    
    private const string cachedFlattenedViewInfosKey = "NocServiceStatusBeingRefreshed.cachedFlattenedViewInfosKey";    
    private static object statusLock = new object();

    public static IList<FlattenedViewInfo> GetFlattenedViewInfos()
    {
        lock(statusLock)
        {
            IList<FlattenedViewInfo> toReturn = GoogleManagementApiCache.FlattenedViewInfos;

            // Perfect world:
            if (Status == GoogleManagementApiCacheStatus.Loaded && toReturn != null)
            {
                return toReturn;
            }

            // Ok - we need to load
            if (Status != GoogleManagementApiCacheStatus.Loading)
            {
                Debug.WriteLine("GoogleManagementApiCache.LoadFlattenedViewInfosInBackground - Start loading");
                GoogleManagementApiCache.Status = GoogleManagementApiCacheStatus.Loading;
                GoogleManagementApiCache.LoadFlattenedViewInfos();
                Debug.WriteLine("GoogleManagementApiCache.LoadFlattenedViewInfosInBackground - Loading fired on other thread");
            }

            return toReturn;
        }
    }

    private static void LoadFlattenedViewInfos()
    {
        var t = new Thread(() =>
        {
            try
            {
                Debug.WriteLine("GoogleManagementApiCache.LoadFlattenedViewInfos - Threaded started");

                ManagementApiHelper mah = new ManagementApiHelper(WebEnvironmentHelper.GoogleApisJwt);
                GoogleManagementApiCache.FlattenedViewInfos = mah.GetAllViewsFlattened();

                lock (statusLock)
                {
                    GoogleManagementApiCache.Status = GoogleManagementApiCacheStatus.Loaded;
                }
                Debug.WriteLine("GoogleManagementApiCache.LoadFlattenedViewInfos - Threaded completed");
            }
            catch (Exception ex)
            {
                lock (statusLock)
                {
                    GoogleManagementApiCache.Status = GoogleManagementApiCacheStatus.Failed;
                    GoogleManagementApiCache.LoadingErrorText = "Issue with loading Google Accounts: " + ex.GetAllMessages();
                }
            }
            finally
            {
                Debug.WriteLine("GoogleManagementApiCache.LoadFlattenedViewInfos - Threaded finalizing");
                if (GoogleManagementApiCache.Status == GoogleManagementApiCacheStatus.Loading)
                {
                    // This should never happen.
                    Debug.WriteLine("GoogleManagementApiCache.LoadFlattenedViewInfos - Wrong state.");
                    GoogleManagementApiCache.Status = GoogleManagementApiCacheStatus.Failed;
                    GoogleManagementApiCache.LoadingErrorText = "Unknown issue with loading Google Accounts";
                }

                Debug.WriteLine("GoogleManagementApiCache.LoadFlattenedViewInfos - Threaded finalized");
            }
        });
        t.Start();
    }

    private static IList<FlattenedViewInfo> flattenedViewInfos;
    private static IList<FlattenedViewInfo> FlattenedViewInfos
    {
        get
        {            
            if (flattenedViewInfos != null)
                return flattenedViewInfos;
            else
            {
                lock (statusLock)
                {
                    if (GoogleManagementApiCache.Status == GoogleManagementApiCacheStatus.Loaded)
                    {
                        GoogleManagementApiCache.Status = GoogleManagementApiCacheStatus.NotLoaded;
                    }
                }
                return null;
            }
        }
        set
        {
            flattenedViewInfos = value;
        }
    }

    // CACHE BASED FAIELD
    //private static IList<FlattenedViewInfo> FlattenedViewInfos
    //{
    //    get
    //    {
    //        List<FlattenedViewInfo> toReturn;
    //        if (CacheHelper.TryGetValue(cachedFlattenedViewInfosKey, false, out toReturn))
    //            return toReturn;
    //        else
    //        {
    //            lock (statusLock)
    //            {
    //                if (GoogleManagementApiCache.Status == GoogleManagementApiCacheStatus.Loaded)
    //                {
    //                    GoogleManagementApiCache.Status = GoogleManagementApiCacheStatus.NotLoaded;
    //                }                    
    //            }
    //            return null;
    //        }
    //    }
    //    set
    //    {
    //        CacheHelper.AddSlidingExpire(false, cachedFlattenedViewInfosKey, value, 360);
    //    }        
    //}

    public static GoogleManagementApiCacheStatus Status { get; private set; }
    public static string LoadingErrorText { get; private set; }
}