﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GoogleManagementApiCacheStatus
/// </summary>
public enum GoogleManagementApiCacheStatus
{
    NotLoaded,
    Loading,
    Loaded,
    Failed
}