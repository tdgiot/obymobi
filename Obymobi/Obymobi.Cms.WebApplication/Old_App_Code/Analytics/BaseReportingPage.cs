﻿using System;
using Dionysos;
using Obymobi;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Analytics;
using SpreadsheetLight;
using System.Diagnostics;

public abstract class BaseReportingPage : Dionysos.Web.UI.PageQueryStringDataBinding
{
    public void GenerateReport(bool doIt, Filter filter, IReport report, AnalyticsReportType type)
    {
        if (doIt)
        {
            // Never allow this on the non development environment as it might grind the cms to a hold
            if (WebEnvironmentHelper.CloudEnvironment != CloudEnvironment.Manual && !TestUtil.IsPcGabriel) 
                throw new NotImplementedException("Do It is not implemented for non development environments.");

            Stopwatch sw = Stopwatch.StartNew();

            SLDocument spreadsheet;
            report.RunReport(out spreadsheet);

            sw.Stop();

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=WebStreamDownload-{0}-{1}.xlsx".FormatSafe(filter.FromTimeZoned.DateTimeToSimpleDateTimeStamp(), filter.TillTimeZoned.DateTimeToSimpleDateTimeStamp()));
            spreadsheet.SaveAs(Response.OutputStream);
            Response.End();
        }
        else
        {
            ReportProcessingTaskEntity task = filter.CreateReportProcessingTask();
            task.AnalyticsReportType = type;
            task.TimeZoneOlsonId = filter.TimeZoneOlsonId;
            task.Save();

            string infoMessage = this.Translate("ReportQueuedForProcessing", "Your report is ready to be processed. Click <a href=\"{0}\">here</a> to view its progress.");

            this.AddInformatorInfo(infoMessage, this.ResolveUrl("~/Analytics/ReportProcessingTask.aspx?id=" + task.ReportProcessingTaskId));
        }
    }

    protected override void SetDefaultValuesToControls()
    {
        
    }
}