﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

public static class RoomControlDemoManager
{
    public static void CreateRoomControlConfigurationDemo(int companyId)
    {
        RoomControlConfigurationEntity roomControlConfigurationEntity = new RoomControlConfigurationEntity();
        roomControlConfigurationEntity.CompanyId = companyId;
        roomControlConfigurationEntity.Name = "Demo configuration";

        // Create watch & listen stations
        StationListEntity stationListWatch = RoomControlDemoManager.CreateStationListEntityAndStations(StationListType.Watch, companyId);
        StationListEntity stationListListen = RoomControlDemoManager.CreateStationListEntityAndStations(StationListType.Listen, companyId);

        // Living
        RoomControlAreaEntity living = RoomControlDemoManager.CreateRoomControlAreaEntity(RoomControlAreaType.Normal, "Living", 1);
        roomControlConfigurationEntity.RoomControlAreaCollection.Add(living);

        // Living sections
        living.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlDashboardSection(0));
        living.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlLightingSection(1));
        living.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlComfortSection(2));
        living.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlBlindsSection(3));
        living.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlTvChannelsSection(stationListWatch, 4));
        living.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlRadioStationsSection(stationListListen, 5));
        living.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.ScreenSleep, "Screen sleep", 6));
        living.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlTimerSection(7));
        living.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Service, "Privacy", 8));
        living.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Service, "Service", 9));

        // Bedroom
        RoomControlAreaEntity bedroom = RoomControlDemoManager.CreateRoomControlAreaEntity(RoomControlAreaType.Normal, "Bedroom", 2);
        roomControlConfigurationEntity.RoomControlAreaCollection.Add(bedroom);

        bedroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlDashboardSection(0));
        bedroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlLightingSection(1));
        bedroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlComfortSection(2));
        bedroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlBlindsSection(3));
        bedroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlTvChannelsSection(stationListWatch, 4));
        bedroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlRadioStationsSection(stationListListen, 5));
        bedroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.ScreenSleep, "Screen sleep", 6));
        bedroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlTimerSection(7));
        bedroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Service, "Privacy", 8));
        bedroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Service, "Service", 9));

        // Bathroom
        RoomControlAreaEntity bathroom = RoomControlDemoManager.CreateRoomControlAreaEntity(RoomControlAreaType.Normal, "Bathroom", 3);
        roomControlConfigurationEntity.RoomControlAreaCollection.Add(bathroom);

        bathroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlLightingSection(0));
        bathroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Service, "Privacy", 8));
        bathroom.RoomControlSectionCollection.Add(RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Service, "Service", 9));

        roomControlConfigurationEntity.Save(true);
    }

    private static RoomControlSectionEntity CreateRoomControlDashboardSection(int sortOrder)
    {
        RoomControlSectionEntity dashboardSection = RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Dashboard, "Basics", sortOrder);
        dashboardSection.RoomControlWidgetCollection.Add(RoomControlDemoManager.CreateRoomControlWidgetEntity(RoomControlWidgetType.Placeholder1x1, "", 1));
        dashboardSection.RoomControlWidgetCollection.Add(RoomControlDemoManager.CreateRoomControlWidgetEntity(RoomControlWidgetType.Scene1x1, "All Off", 2));
        dashboardSection.RoomControlWidgetCollection.Add(RoomControlDemoManager.CreateRoomControlWidgetEntity(RoomControlWidgetType.Scene1x1, "All On", 3));
        dashboardSection.RoomControlWidgetCollection.Add(RoomControlDemoManager.CreateRoomControlWidgetEntity(RoomControlWidgetType.Placeholder1x1, "", 4));
        dashboardSection.RoomControlWidgetCollection.Add(RoomControlDemoManager.CreateRoomControlWidgetEntity(RoomControlWidgetType.Thermostat4x2, "", 5));
        dashboardSection.RoomControlWidgetCollection.Add(RoomControlDemoManager.CreateRoomControlWidgetEntity(RoomControlWidgetType.Volume1x2, "TV Volume", 6));
        dashboardSection.RoomControlWidgetCollection.Add(RoomControlDemoManager.CreateRoomControlWidgetEntity(RoomControlWidgetType.Channel1x2, "TV Channels", 7));
        dashboardSection.RoomControlWidgetCollection.Add(RoomControlDemoManager.CreateRoomControlWidgetEntity(RoomControlWidgetType.Blind1x2, "Drapes", 8));
        dashboardSection.RoomControlWidgetCollection.Add(RoomControlDemoManager.CreateRoomControlWidgetEntity(RoomControlWidgetType.Blind1x2, "Sheers", 9));

        return dashboardSection;
    }

    private static RoomControlSectionEntity CreateRoomControlLightingSection(int sortOrder)
    {
        RoomControlSectionEntity lightingSection = RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Lighting, "Lighting", sortOrder);

        int i;
        for (i = 0; i < 7; i++)
        {
            lightingSection.RoomControlComponentCollection.Add(RoomControlDemoManager.CreateRoomControlComponentEntity(RoomControlComponentType.Light, string.Format("Light {0}", i), i));
        }
     
        for (int j = 0; j < 5; j++)
        {
            i++;
            lightingSection.RoomControlComponentCollection.Add(RoomControlDemoManager.CreateRoomControlComponentEntity(RoomControlComponentType.LightingScene, string.Format("Scene {0}", j), i));
        }

        return lightingSection;
    }

    private static RoomControlSectionEntity CreateRoomControlComfortSection(int sortOrder)
    {
        RoomControlSectionEntity comfortSection = RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Comfort, "Comfort", sortOrder);
        
        for (int i = 0; i < 2; i++)
        {
            comfortSection.RoomControlComponentCollection.Add(RoomControlDemoManager.CreateRoomControlComponentEntity(RoomControlComponentType.Thermostat, string.Format("Thermostat {0}", i), i));
        }

        return comfortSection;
    }

    private static RoomControlSectionEntity CreateRoomControlBlindsSection(int sortOrder)
    {
        RoomControlSectionEntity blindsSection = RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Blinds, "Blinds", sortOrder);

        for (int i = 0; i < 8; i++)
        {
            blindsSection.RoomControlComponentCollection.Add(RoomControlDemoManager.CreateRoomControlComponentEntity(RoomControlComponentType.Blind, string.Format("Blind {0}", i), i));
        }

        return blindsSection;
    }

    private static RoomControlSectionEntity CreateRoomControlTvChannelsSection(StationListEntity stationList, int sortOrder)
    {
        RoomControlSectionEntity tvChannelsSection = RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Custom, "Watch", sortOrder);
        tvChannelsSection.RoomControlSectionItemCollection.Add(RoomControlDemoManager.CreateRoomControlSectionItemEntity(RoomControlSectionItemType.StationListing, "Popular Channels", 1, stationList));
        tvChannelsSection.RoomControlSectionItemCollection.Add(RoomControlDemoManager.CreateRoomControlSectionItemEntity(RoomControlSectionItemType.RemoteControl, "Remote control", 2));
        tvChannelsSection.RoomControlSectionItemCollection.Add(RoomControlDemoManager.CreateRoomControlSectionItemEntity(RoomControlSectionItemType.Scene, "Watch TV", 3, null, "Turning TV on"));
        tvChannelsSection.RoomControlSectionItemCollection.Add(RoomControlDemoManager.CreateRoomControlSectionItemEntity(RoomControlSectionItemType.Scene, "Guide", 4, null, "Opening TV guide"));
        tvChannelsSection.RoomControlSectionItemCollection.Add(RoomControlDemoManager.CreateRoomControlSectionItemEntity(RoomControlSectionItemType.Scene, "Movies", 5, null, "Opening movies channel"));
        tvChannelsSection.RoomControlSectionItemCollection.Add(RoomControlDemoManager.CreateRoomControlSectionItemEntity(RoomControlSectionItemType.Scene, "Pay Per View", 6, null, "Opening Pay Per View Channel"));
        tvChannelsSection.RoomControlSectionItemCollection.Add(RoomControlDemoManager.CreateRoomControlSectionItemEntity(RoomControlSectionItemType.Scene, "Pay Per Day", 7, null, "Opening Pay Per Day Channel"));
        tvChannelsSection.RoomControlSectionItemCollection.Add(RoomControlDemoManager.CreateRoomControlSectionItemEntity(RoomControlSectionItemType.Scene, "Connectivity Panel", 8, null, "Opening connectivity panel"));

        return tvChannelsSection;
    }

    private static RoomControlSectionEntity CreateRoomControlRadioStationsSection(StationListEntity stationList, int sortOrder)
    {
        RoomControlSectionEntity tvChannelsSection = RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Custom, "Listen", sortOrder);
        tvChannelsSection.RoomControlSectionItemCollection.Add(RoomControlDemoManager.CreateRoomControlSectionItemEntity(RoomControlSectionItemType.StationListing, "Popular Stations", 1, stationList));        

        return tvChannelsSection;
    }

    private static RoomControlSectionEntity CreateRoomControlTimerSection(int sortOrder)
    {
        RoomControlSectionEntity timerSection = RoomControlDemoManager.CreateRoomControlSectionEntity(RoomControlSectionType.Timers, "Set alarm", sortOrder);
        timerSection.RoomControlSectionItemCollection.Add(RoomControlDemoManager.CreateRoomControlSectionItemEntity(RoomControlSectionItemType.WakeUpTimer, "Wake up", 1));
        timerSection.RoomControlSectionItemCollection.Add(RoomControlDemoManager.CreateRoomControlSectionItemEntity(RoomControlSectionItemType.SleepTimer, "Sleep", 2));

        return timerSection;
    }

    private static RoomControlAreaEntity CreateRoomControlAreaEntity(RoomControlAreaType type, string name, int sortOrder)
    {
        RoomControlAreaEntity roomControlAreaEntity = new RoomControlAreaEntity();
        roomControlAreaEntity.Type = type;
        roomControlAreaEntity.Name = name;
        roomControlAreaEntity.NameSystem = name;
        roomControlAreaEntity.SortOrder = sortOrder;

        return roomControlAreaEntity;
    }

    private static RoomControlSectionEntity CreateRoomControlSectionEntity(RoomControlSectionType type, string name, int sortOrder)
    {
        RoomControlSectionEntity roomControlSectionEntity = new RoomControlSectionEntity();
        roomControlSectionEntity.Type = type;
        roomControlSectionEntity.Name = name;
        roomControlSectionEntity.NameSystem = name;
        roomControlSectionEntity.SortOrder = sortOrder;        

        return roomControlSectionEntity;
    }

    private static RoomControlSectionItemEntity CreateRoomControlSectionItemEntity(RoomControlSectionItemType type, string name, int sortOrder)
    {
        return RoomControlDemoManager.CreateRoomControlSectionItemEntity(type, name, sortOrder, null);
    }

    private static RoomControlSectionItemEntity CreateRoomControlSectionItemEntity(RoomControlSectionItemType type, string name, int sortOrder, StationListEntity stationListEntity)
    {
        return RoomControlDemoManager.CreateRoomControlSectionItemEntity(type, name, sortOrder, stationListEntity, string.Empty);
    }

    private static RoomControlSectionItemEntity CreateRoomControlSectionItemEntity(RoomControlSectionItemType type, string name, int sortOrder, StationListEntity stationListEntity, string successMessage)
    {
        RoomControlSectionItemEntity roomControlSectionItemEntity = new RoomControlSectionItemEntity();
        roomControlSectionItemEntity.Type = type;
        roomControlSectionItemEntity.Name = name;        
        roomControlSectionItemEntity.SortOrder = sortOrder;
        roomControlSectionItemEntity.StationListEntity = stationListEntity;
        roomControlSectionItemEntity.FieldValue2 = successMessage;

        return roomControlSectionItemEntity;
    }

    private static RoomControlWidgetEntity CreateRoomControlWidgetEntity(RoomControlWidgetType type, string name, int sortOrder)
    {
        RoomControlWidgetEntity roomControlWidgetEntity = new RoomControlWidgetEntity();
        roomControlWidgetEntity.Type = type;
        roomControlWidgetEntity.Name = name;
        roomControlWidgetEntity.SortOrder = sortOrder;

        return roomControlWidgetEntity;
    }

    private static RoomControlComponentEntity CreateRoomControlComponentEntity(RoomControlComponentType type, string name, int sortOrder)
    {
        RoomControlComponentEntity roomControlComponentEntity = new RoomControlComponentEntity();
        roomControlComponentEntity.Type = type;
        roomControlComponentEntity.Name = name;
        roomControlComponentEntity.SortOrder = sortOrder;

        return roomControlComponentEntity;
    }

    private static StationListEntity CreateStationListEntity(int companyId, string name)
    {
        StationListEntity stationListEntity = new StationListEntity();
        stationListEntity.CompanyId = companyId;
        stationListEntity.Name = name;

        return stationListEntity;
    }

    private static StationEntity CreateStationEntity(string name, int sortOrder)
    {
        StationEntity stationEntity = new StationEntity();
        stationEntity.Name = name;
        stationEntity.SortOrder = sortOrder;

        return stationEntity;
    }

    private static StationListEntity CreateStationListEntityAndStations(StationListType type, int companyId)
    {
        StationListEntity stationListEntity = null;

        switch (type)
        {
            case StationListType.Watch:
                stationListEntity = RoomControlDemoManager.CreateStationListEntity(companyId, "Watch demo");

                stationListEntity.StationCollection.Add(RoomControlDemoManager.CreateStationEntity("NBC", 1));
                stationListEntity.StationCollection.Add(RoomControlDemoManager.CreateStationEntity("FOX", 5));
                stationListEntity.StationCollection.Add(RoomControlDemoManager.CreateStationEntity("CBS", 8));
                stationListEntity.StationCollection.Add(RoomControlDemoManager.CreateStationEntity("ABC", 13));
                stationListEntity.StationCollection.Add(RoomControlDemoManager.CreateStationEntity("CNN", 101));
                stationListEntity.StationCollection.Add(RoomControlDemoManager.CreateStationEntity("FOX NEWS", 108));
                break;
            case StationListType.Listen:
                stationListEntity = RoomControlDemoManager.CreateStationListEntity(companyId, "Listen demo");

                stationListEntity.StationCollection.Add(RoomControlDemoManager.CreateStationEntity("TOP POP", 601));
                stationListEntity.StationCollection.Add(RoomControlDemoManager.CreateStationEntity("MUSIC FOR LOVERS", 604));
                stationListEntity.StationCollection.Add(RoomControlDemoManager.CreateStationEntity("DANCE BEATS", 605));
                stationListEntity.StationCollection.Add(RoomControlDemoManager.CreateStationEntity("ROCK HITS", 609));
                stationListEntity.StationCollection.Add(RoomControlDemoManager.CreateStationEntity("TODAY'S COUNTRY", 612));
                stationListEntity.StationCollection.Add(RoomControlDemoManager.CreateStationEntity("SMOOTH JAZZ", 618));
                break;
        }

        return stationListEntity;
    }

    private enum StationListType
    {
        Watch,
        Listen
    }
}