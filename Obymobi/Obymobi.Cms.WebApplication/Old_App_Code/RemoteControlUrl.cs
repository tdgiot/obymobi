﻿using System;
using System.IO;
using Dionysos;
using Obymobi.Constants;
using Obymobi.Security;

namespace Obymobi.Cms.WebApplication.Old_App_Code
{
    public class RemoteControlUrl
    {
        public RemoteControlUrl(string identifier)
        {
            Identifier = identifier;
        }

        private string RemoteControlBaseUrl { get; } = System.Configuration.ConfigurationManager.AppSettings[DionysosConfigurationConstants.RemoteControlUrl];
        private long Timestamp { get; } = DateTime.UtcNow.ToUnixTime();
        private string Identifier { get; }

        public static implicit operator string(RemoteControlUrl remoteControlUrl) => BuildUrl(remoteControlUrl);

        private static string BuildUrl(RemoteControlUrl remoteControlUrl)
        {
            string hash = Hasher.GetHashFromParameters(ObymobiConstants.GenericSalt, remoteControlUrl.Timestamp, remoteControlUrl.Identifier);
            return $"{remoteControlUrl.RemoteControlBaseUrl}/control?timestamp={remoteControlUrl.Timestamp}&identifier={remoteControlUrl.Identifier}&hash={hash}";
        }
    }
}