﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class CompanyGeneratorSettings
{
    #region Company owner

    public string Username { get; set; }
    public string Password { get; set; }

    #endregion

    #region Company

    public string CompanyName { get; set; }
    public string Zipcode { get; set; }
    public string CountryCode { get; set; }
    public string CultureCode { get; set; }
    public string CurrencyCode { get; set; }
    public string TimeZoneOlsonId { get; set; }
    public int? SupportpoolId { get; set; }
    public int ApiVersion { get; set; }
    public int MessagingVersion { get; set; }

    #endregion

    #region Company cultures

    public List<string> CompanyCultures { get; set; }

    #endregion

    #region Terminal

    public bool CreateTerminal { get; set; }
    public string TerminalName { get; set; }
    public int TerminalType { get; set; }

    #endregion

    #region Deliverypointgroup

    public bool CreateDeliverypointgroup { get; set; }
    public string DeliverypointgroupName { get; set; }
    public string Pincode { get; set; }
    public string PincodeGodmode { get; set; }
    public string PincodeSuperuser { get; set; }
    public int? UIThemeId { get; set; }

    #endregion

    #region Route

    public bool CreateRoute { get; set; }
    public int RoutestephandlerType { get; set; }

    #endregion

    #region Deliverypoints

    public bool CreateDeliverypoints { get; set; }
    public int RangeStart { get; set; }
    public int RangeEnd { get; set; }

    #endregion
}