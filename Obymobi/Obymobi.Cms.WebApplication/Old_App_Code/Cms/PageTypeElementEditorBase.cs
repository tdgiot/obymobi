﻿using Dionysos.Interfaces;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Obymobi.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

/// <summary>
/// Summary description for PageTypeElementEditorBase
/// </summary>
public abstract class PageTypeElementEditorBase : System.Web.UI.UserControl, ISaveableControl
{
	public PageTypeElementEditorBase()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public bool Save()
    {
        this.DataBindPageTypeElement();
        return this.PageTypeElement.SaveDataSource();
    }

    protected override void OnLoad(EventArgs e)
    {
        if(!this.IsPostBack)
            this.DataBindControls();

        base.OnLoad(e);
    }

    protected abstract void DataBindControls();

    protected abstract void DataBindPageTypeElement();

    protected virtual void LoadUserControls() { }

    protected PageTypeElement pageTypeElement;

    public virtual PageTypeElement PageTypeElement
    {
        get
        {
            return this.pageTypeElement;
        }
        set
        {
            this.pageTypeElement = value;            
        }
    }

    protected PageElementEntity PageTypeElementDataSourceAsPageElementEntity
    {
        get
        {
            return (PageElementEntity)this.PageTypeElement.DataSource;
        }
    }

    protected PageTemplateElementEntity PageTypeElementDataSourceAsPageTemplateElementEntity
    {
        get
        {
            return (PageTemplateElementEntity)this.PageTypeElement.DataSource;
        }
    }

    protected IMediaContainingEntity PageTypeElementDataSourceAsMediaContainingEntity
    {
        get
        {
            return (IMediaContainingEntity)this.PageTypeElement.DataSource;            
        } 
    }

    protected Dionysos.Web.UI.PageEntity PageAsPageEntity
    {
        get
        {
            return (Dionysos.Web.UI.PageEntity)this.Page;
        }
    }    
}