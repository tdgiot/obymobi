﻿using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PageTypeElementEditorBase
/// </summary>
public abstract class PageTypeElementEditorBase<PageTypeElementType> : PageTypeElementEditorBase where PageTypeElementType : PageTypeElement
{
    public PageTypeElementEditorBase()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public new PageTypeElementType PageTypeElement
    {
        get
        {
            return base.PageTypeElement as PageTypeElementType;
        }
        set
        {
            base.pageTypeElement = value;
        }
    }
}