﻿using System;
using System.Collections;
using DevExpress.XtraScheduler;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using DevExpress.XtraScheduler.Xml;
using Dionysos;
using RecurrenceRange = DevExpress.XtraScheduler.RecurrenceRange;
using RecurrenceType = DevExpress.XtraScheduler.RecurrenceType;

public class PriceScheduleDataSource
{
    private int priceScheduleId;

    public PriceScheduleDataSource(int priceScheduleId)
    {
        this.priceScheduleId = priceScheduleId;
    }

    public PriceScheduleDataSource()
    {
    }

    private PriceScheduleAppointmentList appointments;
    public PriceScheduleAppointmentList Appointments
    {
        get
        {
            if (this.appointments == null)
            {
                this.appointments = new PriceScheduleAppointmentList();

                PredicateExpression filter = new PredicateExpression();
                filter.Add(PriceScheduleItemFields.PriceScheduleId == this.priceScheduleId);

                PrefetchPath prefetch = new PrefetchPath(EntityType.PriceScheduleItemEntity);
                prefetch.Add(PriceScheduleItemEntityBase.PrefetchPathPriceScheduleItemOccurrenceCollection);
                prefetch.Add(PriceScheduleItemEntityBase.PrefetchPathPriceLevelEntity);

                PriceScheduleItemCollection priceScheduleItems = new PriceScheduleItemCollection();
                priceScheduleItems.GetMulti(filter, prefetch);

                foreach (PriceScheduleItemEntity priceScheduleItem in priceScheduleItems)
                {
                    foreach (PriceScheduleItemOccurrenceEntity priceScheduleItemOccurrence in priceScheduleItem.PriceScheduleItemOccurrenceCollection)
                    {
                        PriceScheduleAppointment appointment = new PriceScheduleAppointment();
                        appointment.PriceScheduleItemId = priceScheduleItemOccurrence.PriceScheduleItemOccurrenceId;

                        PriceLevelEntity priceLevel = priceScheduleItem.PriceLevelEntity;
                        appointment.PriceLevelId = priceLevel.PriceLevelId;
                        appointment.Subject = string.Format("Price level: {0}", priceLevel.Name);;
                        appointment.BackgroundColor = priceScheduleItemOccurrence.BackgroundColor;
                        appointment.TextColor = priceScheduleItemOccurrence.TextColor;
                        appointment.StartTime = priceScheduleItemOccurrence.StartTime.Value;
                        appointment.EndTime = priceScheduleItemOccurrence.EndTime.Value;

                        if (priceScheduleItemOccurrence.Recurring)
                        {
                            RecurrenceInfo recurrenceInfo = new RecurrenceInfo();
                            recurrenceInfo.Type = (RecurrenceType)priceScheduleItemOccurrence.RecurrenceType;
                            recurrenceInfo.Range = (RecurrenceRange)priceScheduleItemOccurrence.RecurrenceRange;
                            recurrenceInfo.Start = priceScheduleItemOccurrence.RecurrenceStart.Value;
                            recurrenceInfo.End = priceScheduleItemOccurrence.RecurrenceEnd.Value;
                            recurrenceInfo.OccurrenceCount = priceScheduleItemOccurrence.RecurrenceOccurrenceCount;
                            recurrenceInfo.Periodicity = priceScheduleItemOccurrence.RecurrencePeriodicity;
                            recurrenceInfo.DayNumber = priceScheduleItemOccurrence.RecurrenceDayNumber;
                            recurrenceInfo.WeekDays = (WeekDays)priceScheduleItemOccurrence.RecurrenceWeekDays;
                            recurrenceInfo.WeekOfMonth = (WeekOfMonth)priceScheduleItemOccurrence.RecurrenceWeekOfMonth;
                            recurrenceInfo.Month = priceScheduleItemOccurrence.RecurrenceMonth;
                            appointment.Type = (int)AppointmentType.Pattern;
                            appointment.RecurrenceInfo = recurrenceInfo.ToXml();
                            appointment.ReminderInfo = string.Empty;
                        }
                        
                        this.appointments.Add(appointment);
                    }
                }
            }

            return this.appointments;
        }
    }    

    #region ObjectDataSource methods
    public void InsertMethodHandler(PriceScheduleAppointment appointment)
    {
        PriceLevelEntity priceLevel = null;

        if (appointment.Type == (int)AppointmentType.ChangedOccurrence)
        {
            return;
        }

        if (appointment.PriceLevelId > 0)
        {
            priceLevel = new PriceLevelEntity(appointment.PriceLevelId);
        }        
        else
        {
            return;
        }

        PredicateExpression filter = new PredicateExpression();
        filter.Add(PriceScheduleItemFields.PriceScheduleId == this.priceScheduleId);
        filter.Add(PriceScheduleItemFields.PriceLevelId == appointment.PriceLevelId);
        
        PriceScheduleItemCollection priceScheduleItemCollection = new PriceScheduleItemCollection();
        priceScheduleItemCollection.GetMulti(filter);

        PriceScheduleItemEntity priceScheduleItem = null;
        if (priceScheduleItemCollection.Count > 0)
        {            
            priceScheduleItem = priceScheduleItemCollection[0];
        }
        else
        {
            priceScheduleItem = new PriceScheduleItemEntity();
            priceScheduleItem.PriceScheduleId = this.priceScheduleId;
            priceScheduleItem.PriceLevelId = priceLevel.PriceLevelId;
            priceScheduleItem.Save();
        }

        DateTime startTime = DateTime.SpecifyKind(appointment.StartTime, DateTimeKind.Unspecified);
        DateTime endTime = DateTime.SpecifyKind(appointment.EndTime, DateTimeKind.Unspecified);

        PriceScheduleItemOccurrenceEntity priceScheduleItemOccurrence = new PriceScheduleItemOccurrenceEntity();
        priceScheduleItemOccurrence.PriceScheduleItemId = priceScheduleItem.PriceScheduleItemId;
        priceScheduleItemOccurrence.StartTime = startTime;
        priceScheduleItemOccurrence.EndTime = endTime;
        priceScheduleItemOccurrence.StartTimeUTC = startTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
        priceScheduleItemOccurrence.EndTimeUTC = endTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);

        priceScheduleItemOccurrence.BackgroundColor = appointment.BackgroundColor;
        priceScheduleItemOccurrence.TextColor = appointment.TextColor;        
        
        if (!string.IsNullOrWhiteSpace(appointment.RecurrenceInfo))
        {
            IRecurrenceInfo recurrenceInfo = RecurrenceInfoXmlPersistenceHelper.ObjectFromXml(appointment.RecurrenceInfo);
            
            DateTime recurrenceStartTime = DateTime.SpecifyKind(recurrenceInfo.Start, DateTimeKind.Unspecified);
            DateTime recurrenceEndTime = DateTime.SpecifyKind(recurrenceInfo.End, DateTimeKind.Unspecified);

            priceScheduleItemOccurrence.Recurring = true;
            priceScheduleItemOccurrence.RecurrenceType = (int)recurrenceInfo.Type;
            priceScheduleItemOccurrence.RecurrenceRange = (int)recurrenceInfo.Range;
            priceScheduleItemOccurrence.RecurrenceStart = recurrenceStartTime;
            priceScheduleItemOccurrence.RecurrenceEnd = recurrenceEndTime;
            priceScheduleItemOccurrence.RecurrenceStartUTC = recurrenceStartTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
            priceScheduleItemOccurrence.RecurrenceEndUTC = recurrenceEndTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
            priceScheduleItemOccurrence.RecurrenceOccurrenceCount = recurrenceInfo.OccurrenceCount;
            priceScheduleItemOccurrence.RecurrencePeriodicity = recurrenceInfo.Periodicity;
            priceScheduleItemOccurrence.RecurrenceDayNumber = recurrenceInfo.DayNumber;
            priceScheduleItemOccurrence.RecurrenceWeekDays = (int)recurrenceInfo.WeekDays;
            priceScheduleItemOccurrence.RecurrenceWeekOfMonth = (int)recurrenceInfo.WeekOfMonth;
            priceScheduleItemOccurrence.RecurrenceMonth = recurrenceInfo.Month;
        };

        priceScheduleItemOccurrence.Save();

        appointment.PriceScheduleItemId = priceScheduleItemOccurrence.PriceScheduleItemOccurrenceId;
        appointment.Subject = string.Format("Price level: {0}", priceLevel.Name);
        
        this.Appointments.Add(appointment);
    }

    public void DeleteMethodHandler(PriceScheduleAppointment appointment)
    {
        PriceScheduleItemOccurrenceEntity priceScheduleItemOccurrence = new PriceScheduleItemOccurrenceEntity((int)appointment.PriceScheduleItemId);
        if (!priceScheduleItemOccurrence.IsNew)
        {
            PriceScheduleItemEntity priceScheduleItem = priceScheduleItemOccurrence.PriceScheduleItemEntity;

            priceScheduleItem.PriceScheduleItemOccurrenceCollection.Remove(priceScheduleItemOccurrence);
            priceScheduleItemOccurrence.Delete();

            if (priceScheduleItem.PriceScheduleItemOccurrenceCollection.Count == 0)
            {
                priceScheduleItem.Delete();
            }
        }
        this.Appointments.Remove(appointment);
    }

    public void UpdateMethodHandler(PriceScheduleAppointment appointment)
    {
        int eventIndex = this.Appointments.GetEventIndex((int)appointment.PriceScheduleItemId);
        if (eventIndex >= 0)
        {
            PriceScheduleItemOccurrenceEntity priceScheduleItemOccurrence = new PriceScheduleItemOccurrenceEntity((int)appointment.PriceScheduleItemId);
            if (!priceScheduleItemOccurrence.IsNew)
            {
                PriceScheduleItemEntity scheduledItem = priceScheduleItemOccurrence.PriceScheduleItemEntity;
                scheduledItem.PriceLevelId = appointment.PriceLevelId;

                DateTime startTime = DateTime.SpecifyKind(appointment.StartTime, DateTimeKind.Unspecified);
                DateTime endTime = DateTime.SpecifyKind(appointment.EndTime, DateTimeKind.Unspecified);

                priceScheduleItemOccurrence.StartTime = startTime;
                priceScheduleItemOccurrence.EndTime = endTime;
                priceScheduleItemOccurrence.StartTimeUTC = startTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
                priceScheduleItemOccurrence.EndTimeUTC = endTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
                priceScheduleItemOccurrence.BackgroundColor = appointment.BackgroundColor;
                priceScheduleItemOccurrence.TextColor = appointment.TextColor;
                
                if (!string.IsNullOrWhiteSpace(appointment.RecurrenceInfo))
                {
                    IRecurrenceInfo recurrenceInfo = RecurrenceInfoXmlPersistenceHelper.ObjectFromXml(appointment.RecurrenceInfo);

                    DateTime recurrenceStartTime = DateTime.SpecifyKind(recurrenceInfo.Start, DateTimeKind.Unspecified);
                    DateTime recurrenceEndTime = DateTime.SpecifyKind(recurrenceInfo.End, DateTimeKind.Unspecified);

                    priceScheduleItemOccurrence.Recurring = true;
                    priceScheduleItemOccurrence.RecurrenceType = (int)recurrenceInfo.Type;
                    priceScheduleItemOccurrence.RecurrenceRange = (int)recurrenceInfo.Range;
                    priceScheduleItemOccurrence.RecurrenceStart = recurrenceStartTime;
                    priceScheduleItemOccurrence.RecurrenceEnd = recurrenceEndTime;
                    priceScheduleItemOccurrence.RecurrenceStartUTC = recurrenceStartTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
                    priceScheduleItemOccurrence.RecurrenceEndUTC = recurrenceEndTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
                    priceScheduleItemOccurrence.RecurrenceOccurrenceCount = recurrenceInfo.OccurrenceCount;
                    priceScheduleItemOccurrence.RecurrencePeriodicity = recurrenceInfo.Periodicity;
                    priceScheduleItemOccurrence.RecurrenceDayNumber = recurrenceInfo.DayNumber;
                    priceScheduleItemOccurrence.RecurrenceWeekDays = (int)recurrenceInfo.WeekDays;
                    priceScheduleItemOccurrence.RecurrenceWeekOfMonth = (int)recurrenceInfo.WeekOfMonth;
                    priceScheduleItemOccurrence.RecurrenceMonth = recurrenceInfo.Month;
                }
                else
                {
                    priceScheduleItemOccurrence.Recurring = false;
                    priceScheduleItemOccurrence.RecurrenceType = 0;
                    priceScheduleItemOccurrence.RecurrenceRange = 0;
                    priceScheduleItemOccurrence.RecurrenceStart = null;
                    priceScheduleItemOccurrence.RecurrenceEnd = null;
                    priceScheduleItemOccurrence.RecurrenceStartUTC = null;
                    priceScheduleItemOccurrence.RecurrenceEndUTC = null;
                    priceScheduleItemOccurrence.RecurrenceOccurrenceCount = 0;
                    priceScheduleItemOccurrence.RecurrencePeriodicity = 0;
                    priceScheduleItemOccurrence.RecurrenceDayNumber = 0;
                    priceScheduleItemOccurrence.RecurrenceWeekDays = 0;
                    priceScheduleItemOccurrence.RecurrenceWeekOfMonth = 0;
                    priceScheduleItemOccurrence.RecurrenceMonth = 0;
                }

                scheduledItem.Save(true);
            }

            this.Appointments.RemoveAt(eventIndex);
            this.Appointments.Insert(eventIndex, appointment);
        }
    }

    public IEnumerable SelectMethodHandler()
    {
        PriceScheduleAppointmentList result = new PriceScheduleAppointmentList();
        result.AddRange(this.Appointments);
        return result;
    }

    #endregion
}