﻿using DevExpress.Web.ASPxScheduler;
using DevExpress.Web.ASPxScheduler.Internal;
using DevExpress.XtraScheduler;

public class PriceScheduleAppointmentForm : AppointmentFormTemplateContainer
{
    public PriceScheduleAppointmentForm(ASPxScheduler control)
        : base(control)
    {
        
    }

    public int PriceLevelId
    {
        get
        {
            object val = Appointment.CustomFields["PriceLevelId"];
            return (val == System.DBNull.Value) ? 0 : System.Convert.ToInt32(val);
        }
    }

    public int BackgroundColor
    {
        get
        {
            object val = Appointment.CustomFields["BackgroundColor"];
            return (val == System.DBNull.Value) ? 0 : System.Convert.ToInt32(val);
        }
    }

    public int TextColor
    {
        get
        {
            object val = Appointment.CustomFields["TextColor"];
            return (val == System.DBNull.Value) ? 0 : System.Convert.ToInt32(val);
        }
    }
}

public class PriceScheduleAppointmentFormController : AppointmentFormController
{
    public PriceScheduleAppointmentFormController(ASPxScheduler control, Appointment apt)
        : base(control, apt) 
    { 
    }

    public int PriceLevelId
    {
        get 
        {
            return (int)EditedAppointmentCopy.CustomFields["PriceLevelId"]; 
        }
        set
        {
            this.EditedAppointmentCopy.CustomFields["PriceLevelId"] = value; 
        }
    }

    public int BackgroundColor
    {
        get
        {
            return (int)EditedAppointmentCopy.CustomFields["BackgroundColor"];
        }
        set
        {
            this.EditedAppointmentCopy.CustomFields["BackgroundColor"] = value;
        }
    }

    public int TextColor
    {
        get
        {
            return (int)EditedAppointmentCopy.CustomFields["TextColor"];
        }
        set
        {
            this.EditedAppointmentCopy.CustomFields["TextColor"] = value;
        }
    }

    int SourcePriceLevelId
    {
        get 
        {
            return (int)SourceAppointment.CustomFields["PriceLevelId"]; 
        }
        set 
        {
            SourceAppointment.CustomFields["PriceLevelId"] = value; 
        }
    }

    int SourceBackgroundColor
    {
        get
        {
            return (int)SourceAppointment.CustomFields["BackgroundColor"];
        }
        set
        {
            SourceAppointment.CustomFields["BackgroundColor"] = value;
        }
    }

    int SourceTextColor
    {
        get
        {
            return (int)SourceAppointment.CustomFields["TextColor"];
        }
        set
        {
            SourceAppointment.CustomFields["TextColor"] = value;
        }
    }
}