﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.ComponentModel;
using DevExpress.XtraScheduler;

[Serializable]
public class PriceScheduleAppointmentList : BindingList<PriceScheduleAppointment>
{
    public void AddRange(PriceScheduleAppointmentList events)
    {
        Array.ForEach(events.ToArray(), e => this.Add(e));
    }

    public int GetEventIndex(int eventId)
    {
        var result = this.FirstOrDefault(item => (int)item.PriceScheduleItemId == eventId);
        if (result != null)
            return this.IndexOf(result);

        return -1;
    }

}