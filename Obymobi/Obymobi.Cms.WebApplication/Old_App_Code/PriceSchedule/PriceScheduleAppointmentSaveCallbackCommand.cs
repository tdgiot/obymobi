﻿using DevExpress.Web;
using DevExpress.Web.ASPxScheduler.Internal;
using DevExpress.Web.ASPxScheduler;
using DevExpress.XtraScheduler;

public class PriceScheduleAppointmentSaveCallbackCommand : AppointmentFormSaveCallbackCommand
{
    public PriceScheduleAppointmentSaveCallbackCommand(ASPxScheduler control) : base(control)
    {
    }

    protected internal new PriceScheduleAppointmentFormController Controller
    {
        get { return (PriceScheduleAppointmentFormController)base.Controller; }
    }

    protected override void AssignControllerValues()
    {
        ASPxComboBox ddlPriceLevelId = (ASPxComboBox)this.FindControlByID("ddlPriceLevelId");
        ASPxColorEdit ceBackgroundColor = (ASPxColorEdit)this.FindControlByID("ceBackgroundColor");
        ASPxColorEdit ceTextColor = (ASPxColorEdit)this.FindControlByID("ceTextColor");

        this.Controller.PriceLevelId = System.Convert.ToInt32(ddlPriceLevelId.Value);
        this.Controller.BackgroundColor = ceBackgroundColor.Color.ToArgb();
        this.Controller.TextColor = ceTextColor.Color.ToArgb();

        base.AssignControllerValues();
    }
    protected override AppointmentFormController CreateAppointmentFormController(Appointment apt)
    {
        return new PriceScheduleAppointmentFormController(this.Control, apt);
    }
}