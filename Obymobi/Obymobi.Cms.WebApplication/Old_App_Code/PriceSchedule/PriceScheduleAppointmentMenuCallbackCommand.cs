﻿using DevExpress.Web.ASPxScheduler;
using DevExpress.Web.ASPxScheduler.Internal;
using DevExpress.XtraScheduler;
using System;

public class PriceScheduleAppointmentMenuCallbackCommand : SchedulerCallbackCommand
{
    public const string CommandId = "USRAPTMENU";

    private string menuItemId = String.Empty;

    public PriceScheduleAppointmentMenuCallbackCommand(ASPxScheduler control)
        : base(control)
    {
    }

    public override string Id { get { return PriceScheduleAppointmentMenuCallbackCommand.CommandId; } }
    public string MenuItemId { get { return menuItemId; } }

    protected override void ParseParameters(string parameters)
    {
        this.menuItemId = parameters;
    }

    protected override void ExecuteCore()
    {
        if (MenuItemId == "DeleteId")
        {
            Appointment apt = this.Control.SelectedAppointments[0];
            if (apt.RecurrencePattern != null)
            {
                apt.RecurrencePattern.Delete();
            }
            else
            {
                apt.Delete();
            }
        }
    }
}