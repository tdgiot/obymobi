﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Globalization;

namespace Obymobi.ObymobiCms
{
    public class ObymobiCmsTranslatables
    {        
        public static string prefix = "Obymobi.ObymobiCms.";
        public static Translatable NotSet = new Translatable(prefix + "NotSet", "Niet ingesteld");
        public static Translatable NoneDashChoose = new Translatable(prefix + "NoneDashChoose", "Geen - Maak uw keuze");        
    }
}
