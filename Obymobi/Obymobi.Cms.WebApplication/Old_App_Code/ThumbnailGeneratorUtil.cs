﻿using Obymobi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Dionysos;
using System.Web.Hosting;

/// <summary>
/// Summary description for ThumbnailGeneratorUtil
/// </summary>
public static class ThumbnailGeneratorUtil
{
    // Also it's not place in /App_Data/ because we want it to be accesible by users.
    static string tempFolder = "temp"; // GK I was not aware of a constant for this, if you know it, please replace!    
    static string thumbnailFolder = "ThumbnailGenerator";
    static object cleanUpLock = new object();
    public static DateTime lastTempCleanUp = DateTime.MinValue;
    static bool checkedFolder = false;

    private static string GetThumbnailsPath()
    {
        string path = HostingEnvironment.MapPath("~/temp/ThumbnailGenerator/");

        if (!checkedFolder)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        return path;
    }

    public static void CleanUpOldTempFiles(bool force = false)
    {
        lock (ThumbnailGeneratorUtil.cleanUpLock)
        {
            if (force || (DateTime.Now - ThumbnailGeneratorUtil.lastTempCleanUp).TotalHours > 1)
            {
                ThumbnailGeneratorUtil.lastTempCleanUp = DateTime.Now;

                Task.Factory.StartNew(() => ThumbnailGeneratorUtil.CleanUpOldTempFilesDoWork());
            }
        }
    }

    private static void CleanUpOldTempFilesDoWork()
    {
        // Get the phyiscal path
        string path = ThumbnailGeneratorUtil.GetThumbnailsPath();

        var files = Directory.GetFiles(path);

        DateTime now = DateTime.Now;
        foreach (var file in files)
        {
            FileInfo f = new FileInfo(file);
            if ((now - f.CreationTime).TotalDays > 2)
            {
                File.Delete(file);
            }
        }
    }

    public static void CleanUpTempFilesForMedia(int mediaId)
    {
        Task.Factory.StartNew(() => ThumbnailGeneratorUtil.CleanUpTempFilesForMediaDoWork(mediaId));
    }

    private static void CleanUpTempFilesForMediaDoWork(int mediaId)
    {
        string path = ThumbnailGeneratorUtil.GetThumbnailsPath();

        var files = Directory.GetFiles(path, "{0}-*.*".FormatSafe(mediaId));

        foreach (var file in files)
            File.Delete(file);
    }

    public static void SaveFileToCache(string cacheKey, byte[] file)
    {
        string path = ThumbnailGeneratorUtil.GetThumbnailsPath();
        path = Path.Combine(path, cacheKey);
        File.WriteAllBytes(path, file);
    }

    public static bool TryGetFileFromCache(string cacheKey, out byte[] file)
    {
        // GK Might get slow one day with a lot files, need to do sub dirs then.
        file = new byte[0];
        string path = ThumbnailGeneratorUtil.GetThumbnailsPath();
        path = Path.Combine(path, cacheKey);
        if (File.Exists(path))
        {
            file = File.ReadAllBytes(path);
            return true;
        }
        else
            return false;
    }
}