using Dionysos;
using Obymobi.Constants;
using Obymobi.Security;
using System;

namespace Obymobi.Cms.WebApplication.Old_App_Code
{
    public class LogCatUrl
    {
        public LogCatUrl(string identifier)
        {
            this.Identifier = identifier;
        }

        private string RemoteControlBaseUrl { get; } = System.Configuration.ConfigurationManager.AppSettings[DionysosConfigurationConstants.RemoteControlUrl];
        private long Timestamp { get; } = DateTime.UtcNow.ToUnixTime();
        private string Identifier { get; }

        public static implicit operator string(LogCatUrl logCatUrl) => BuildUrl(logCatUrl);

        private static string BuildUrl(LogCatUrl logCatUrl)
        {
            string hash = Hasher.GetHashFromParameters(ObymobiConstants.GenericSalt, logCatUrl.Timestamp, logCatUrl.Identifier);
            return $"{logCatUrl.RemoteControlBaseUrl}/read?timestamp={logCatUrl.Timestamp}&identifier={logCatUrl.Identifier}&hash={hash}";
        }
    }
}