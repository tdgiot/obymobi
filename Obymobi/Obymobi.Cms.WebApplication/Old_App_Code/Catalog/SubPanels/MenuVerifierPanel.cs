﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Obymobi.Logic.Model;
using Obymobi.Enums;
using Dionysos.Web.UI.WebControls;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;

/// <summary>
/// Summary description for Class1
/// </summary>
public abstract class MenuVerifierPanel : System.Web.UI.UserControl
{
	#region Fields

	private ProductmenuItem productmenuItem = null;
	private bool onlyWarnings = false;
	private bool showCategoryLayouts = false;
	private bool showProducts = false;
	private bool showSuggestionAlterations = false;
	private bool showAlterationOptions = false;
	private bool showDescriptions = false;
	private bool showAdvertisementTags = false;
	private ProductmenuItem[] productmenuItems = null;
	private List<string> errors = null;
	private List<string> warnings = null;
	private bool hasWarnings = false;
	private bool hasErrors = false;
	private bool usePos = false;	
	private EntityView<ProductEntity> productView = null;
	private EntityView<CategoryEntity> categoryView = null;
	private bool isFoodProduct = false;

	#endregion

	#region Methods

	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		this.PreRender += new EventHandler(MenuVerifierPanel_PreRender);
	}

	public abstract void RenderInformation();

	public string GetProductNameById(int id)
	{
		var pitem = this.productmenuItems.First(pi => pi.Id == id);

		if (pitem != null)
			return pitem.Name;
		else
		{
			this.AddError("Suggestie met Id '{0}' niet gevonden.", id);
			return "Niet gevonden";
		}
	}

	private void AddWarningOrError(bool error, string message, params object[] args)
	{
		message = string.Format(message, args);

		// Prepend info: 
		string prefix = string.Empty;
		if (this.ProductmenuItem.ItemType == 100)
		{
			prefix = "Categorie";
		}
		else
		{
			switch (this.ProductmenuItem.ItemType.ToEnum<ProductType>())
			{
				case ProductType.Product:
					prefix = "Product";
					break;
				case ProductType.Service:
					prefix = "Service";
					break;
				case ProductType.Paymentmethod:
					prefix = "Betaalmethode";
					break;
				case ProductType.Deliverypoint:
					prefix = "Afleverpunt";
					break;
				case ProductType.Tablet:
					prefix = "Tablet";
					break;
				default:
					break;
			}
		}

		if (error)
			this.Warnings.Add(string.Format("{0} - {1} ({2}): {3}", prefix, this.ProductmenuItem.Name, this.ProductmenuItem.Id, message));
		else
			this.Errors.Add(string.Format("{0} - {1} ({2}): {3}", prefix, this.ProductmenuItem.Name, this.ProductmenuItem.Id, message));
	}

	public void AddWarning(string message, params object[] args)
	{
		this.AddWarningOrError(false, message, args);
		this.HasWarnings = true;
	}

	public void AddError(string message, params object[] args)
	{
		this.AddWarningOrError(true, message, args);
		this.HasErrors = true;
	}

	protected void SetAlterations()
	{
		if (this.ProductmenuItem.Alterations == null)
			this.ProductmenuItem.Alterations = new Alteration[0];

		int alterationCount = this.ProductmenuItem.Alterations.Count();
		this.lblAlterationsControl.Text += string.Format(" ({0})", alterationCount);

		this.plhAlterationsControl.AddHtml("<ul class=\"alterations\">");
		foreach (var alteration in this.ProductmenuItem.Alterations)
		{
			int alterationOptionCount = alteration.Alterationoptions.Count();

			if (alterationOptionCount == 0)
				this.AddError("Productsamenstelling '{0}' heeft geen keuzeopties.", alteration.Name);
			else if (alterationOptionCount == 1)
				this.AddWarning("Productsamenstelling '{0}' heeft maar 1 keuzeoptie.", alteration.Name);

			// Render alteration
			string specification = string.Empty;

			if (alteration.MaxOptions > 1)
				specification = "Multi";
			else
				specification = "Enkel";

			if (alteration.MinOptions > 0)
				specification += "*";

			this.plhAlterationsControl.AddHtml("<li><a href=\"Alteration.aspx?id={0}\">{1}</a> ({2})", alteration.AlterationId,  alteration.Name, specification);

			// Render options if required
			if (this.ShowAlterationOptions)
			{
				this.plhAlterationsControl.AddHtml("<ul>");
				foreach (var option in alteration.Alterationoptions)
				{
					this.plhAlterationsControl.AddHtml("<li><a href=\"Alterationoption.aspx?id={0}\">{1}</a> - {2}", option.AlterationoptionId, option.Name, option.PriceIn);					
				}
				this.plhAlterationsControl.AddHtml("</ul>");
			}

			this.plhAlterationsControl.AddHtml("</li>");
		}
		this.plhAlterationsControl.AddHtml("</ul>");
	}

	protected void SetSuggestions()
	{
		if (this.ProductmenuItem.ProductSuggestions == null)
			this.ProductmenuItem.ProductSuggestions = new ProductSuggestion[0];

		int suggestionCount = this.ProductmenuItem.ProductSuggestions.Count();

		this.lblSuggestionsControl.Text += string.Format(" ({0})", suggestionCount);

		if (suggestionCount == 0)
		{
			this.AddWarning("Geen suggesties");
			this.lblSuggestionsControl.CssClass = "warning";
		}
		else if (suggestionCount < 4)
		{
			this.AddWarning("Slechts {0} van de 4 suggesties ingevuld.", suggestionCount);
			this.lblSuggestionsControl.CssClass = "warning";
		}

		string suggestion1 = "N/A";
		string suggestion2 = "N/A";
		string suggestion3 = "N/A";
		string suggestion4 = "N/A";

		if (suggestionCount > 0)
		{
			int suggestedProductId = this.ProductmenuItem.ProductSuggestions[0].SuggestedProductId;
			suggestion1 = string.Format("<a href=\"Product.aspx?id={0}\">{1}</a>", suggestedProductId, this.GetProductNameById(suggestedProductId));
		}

		if (suggestionCount > 1)
		{
			int suggestedProductId = this.ProductmenuItem.ProductSuggestions[1].SuggestedProductId;
			suggestion2 = string.Format("<a href=\"Product.aspx?id={0}\">{1}</a>", suggestedProductId, this.GetProductNameById(suggestedProductId));
		}

		if (suggestionCount > 2)
		{
			int suggestedProductId = this.ProductmenuItem.ProductSuggestions[2].SuggestedProductId;
			suggestion3 = string.Format("<a href=\"Product.aspx?id={0}\">{1}</a>", suggestedProductId, this.GetProductNameById(suggestedProductId));
		}

		if (suggestionCount > 3)
		{
			int suggestedProductId = this.ProductmenuItem.ProductSuggestions[3].SuggestedProductId;
			suggestion4 = string.Format("<a href=\"Product.aspx?id={0}\">{1}</a>", suggestedProductId, this.GetProductNameById(suggestedProductId));
		}

		this.plhSuggestionsControl.AddHtml("<table class=\"suggestions\"><tr><td>{0}</td><td>{1}</td></tr><tr><td>{2}</td><td>{3}</td></tr></table>",
			suggestion1, suggestion2, suggestion3, suggestion4);
	}

	public CategoryEntity GetCategoryById(int categoryId)
	{
		this.CategoryView.Filter = new PredicateExpression(CategoryFields.CategoryId == categoryId);

		if (this.CategoryView.Count == 1)
			return this.CategoryView[0];
		else
			return null;			
	}

	public ProductEntity GetProductById(int productId)
	{
		this.ProductView.Filter = new PredicateExpression(ProductFields.ProductId == productId);

		if (this.ProductView.Count == 1)
			return this.ProductView[0];
		else
			return null;
	}

	#endregion

	#region Event Handlers

	void MenuVerifierPanel_PreRender(object sender, EventArgs e)
	{
		if (this.OnlyWarnings && !this.HasErrors)
			this.Visible = false;
	}

	#endregion

	#region Properties

	public abstract Label lblSuggestionsControl { get; }
	public abstract Label lblAlterationsControl { get; }
	public abstract PlaceHolder plhAlterationsControl { get; }
	public abstract PlaceHolder plhSuggestionsControl { get; }

	/// <summary>
	/// Gets or sets the PropertyName
	/// </summary>
	public EntityView<CategoryEntity> CategoryView
	{
		get
		{
			return this.categoryView;
		}
		set
		{
			this.categoryView = value;
		}
	}


	/// <summary>
	/// Gets or sets the productsView
	/// </summary>
	public EntityView<ProductEntity> ProductView
	{
		get
		{
			return this.productView;
		}
		set
		{
			this.productView = value;
		}
	}

	/// <summary>
	/// Gets or sets the usePos
	/// </summary>
	public bool UsePos
	{
		get
		{
			return this.usePos;
		}
		set
		{
			this.usePos = value;
		}
	}


	/// <summary>
	/// Gets or sets the hasErrors
	/// </summary>
	public bool HasErrors
	{
		get
		{
			return this.hasErrors;
		}
		set
		{
			this.hasErrors = value;
		}
	}


	/// <summary>
	/// Gets or sets the hasWarnings
	/// </summary>
	public bool HasWarnings
	{
		get
		{
			return this.hasWarnings;
		}
		set
		{
			this.hasWarnings = value;
		}
	}

	/// <summary>
	/// Gets or sets the warnings
	/// </summary>
	public List<string> Warnings
	{
		get
		{
			return this.warnings;
		}
		set
		{
			this.warnings = value;
		}
	}


	/// <summary>
	/// Gets or sets the errors
	/// </summary>
	public List<string> Errors
	{
		get
		{
			return this.errors;
		}
		set
		{
			this.errors = value;
		}
	}


	/// <summary>
	/// Gets or sets the productmenuItems
	/// </summary>
	public ProductmenuItem[] ProductmenuItems
	{
		get
		{
			return this.productmenuItems;
		}
		set
		{
			this.productmenuItems = value;
		}
	}


	/// <summary>
	/// Gets or sets the showAdvertisementTags
	/// </summary>
	public bool ShowAdvertisementTags
	{
		get
		{
			return this.showAdvertisementTags;
		}
		set
		{
			this.showAdvertisementTags = value;
		}
	}



	/// <summary>
	/// Gets or sets the showDescriptions
	/// </summary>
	public bool ShowDescriptions
	{
		get
		{
			return this.showDescriptions;
		}
		set
		{
			this.showDescriptions = value;
		}
	}



	/// <summary>
	/// Gets or sets the showAlterationOptions
	/// </summary>
	public bool ShowAlterationOptions
	{
		get
		{
			return this.showAlterationOptions;
		}
		set
		{
			this.showAlterationOptions = value;
		}
	}


	/// <summary>
	/// Gets or sets the showSuggestionAlterations
	/// </summary>
	public bool ShowSuggestionAlterations
	{
		get
		{
			return this.showSuggestionAlterations;
		}
		set
		{
			this.showSuggestionAlterations = value;
		}
	}



	/// <summary>
	/// Gets or sets the showProducts
	/// </summary>
	public bool ShowProducts
	{
		get
		{
			return this.showProducts;
		}
		set
		{
			this.showProducts = value;
		}
	}



	/// <summary>
	/// Gets or sets the showCategoryLayouts
	/// </summary>
	public bool ShowCategoryLayouts
	{
		get
		{
			return this.showCategoryLayouts;
		}
		set
		{
			this.showCategoryLayouts = value;
		}
	}	

	/// <summary>
	/// Gets or sets the IsFoodProduct
	/// </summary>
	public bool IsFoodProduct
	{
		get
		{
			return this.isFoodProduct;
		}
		set
		{
			this.isFoodProduct = value;
		}
	}

	/// <summary>
	/// Gets or sets the onlyWarnings
	/// </summary>
	public bool OnlyWarnings
	{
		get
		{
			return this.onlyWarnings;
		}
		set
		{
			this.onlyWarnings = value;
		}
	}


	/// <summary>
	/// Gets or sets the productmenuItem
	/// </summary>
	public ProductmenuItem ProductmenuItem
	{
		get
		{
			return this.productmenuItem;
		}
		set
		{
			this.productmenuItem = value;

			if (this.productmenuItem.Media == null)
				this.productmenuItem.Media = new Media[0];
		}
	}

	#endregion

}