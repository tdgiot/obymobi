﻿using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Xml;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Xml;

public static class MarketingScheduleHelper
{
    public static UIScheduleItemEntity GetExistingUIScheduleItemByAppointment(MarketingAppointment appointment, int uiScheduleId)
    {
        PredicateExpression filter = new PredicateExpression();
        filter.Add(UIScheduleItemFields.UIScheduleId == uiScheduleId);
        
        PredicateExpression itemFilter = new PredicateExpression();
        itemFilter.Add(UIScheduleItemFields.UIWidgetId == appointment.UIWidgetId);
        itemFilter.AddWithOr(UIScheduleItemFields.MediaId == appointment.MediaId);
        itemFilter.AddWithOr(UIScheduleItemFields.ScheduledMessageId == appointment.ScheduledMessageId);
        filter.Add(itemFilter);

        UIScheduleItemCollection uiScheduleItemCollection = new UIScheduleItemCollection();
        uiScheduleItemCollection.GetMulti(filter);

        return uiScheduleItemCollection.Count > 0 ? uiScheduleItemCollection[0] : null;
    }

    public static UIScheduleItemEntity CreateUIScheduleItemByAppointment(MarketingAppointment appointment, int uiScheduleId)
    {
        UIScheduleItemEntity uiScheduleItem = new UIScheduleItemEntity();
        uiScheduleItem.UIScheduleId = uiScheduleId;        

        if (appointment.UIWidgetId > 0)
        {
            uiScheduleItem.UIWidgetId = appointment.UIWidgetId;
        }
        else if (appointment.MediaId > 0)
        {
            uiScheduleItem.MediaId = appointment.MediaId;
        }
        else if (appointment.ScheduledMessageId > 0)
        {
            uiScheduleItem.ScheduledMessageId = appointment.ScheduledMessageId;
        }
        
        return uiScheduleItem;
    }
}