﻿using System;
using System.Collections;
using System.IO;
using DevExpress.XtraScheduler;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Dionysos;
using RecurrenceRange = DevExpress.XtraScheduler.RecurrenceRange;
using RecurrenceType = DevExpress.XtraScheduler.RecurrenceType;
using DevExpress.XtraScheduler.Xml;
using System.Linq;
using System.Xml;

public class MarketingScheduleDataSource
{
    private int uiScheduleId;
    private MarketingScheduleAppointmentList appointments;

    public MarketingScheduleDataSource(int uiScheduleId)
    {
        this.uiScheduleId = uiScheduleId;
    }

    public MarketingScheduleDataSource()
    {
        
    } 

    #region ObjectDataSource methods

    public object InsertMethodHandler(MarketingAppointment appointment)
    {
        if (appointment.UIWidgetId <= 0 && appointment.MediaId <= 0 && appointment.ScheduledMessageId <= 0)
        {
            return 0;
        }

        UIScheduleItemEntity uiScheduleItem = MarketingScheduleHelper.GetExistingUIScheduleItemByAppointment(appointment, this.uiScheduleId);
        if (uiScheduleItem == null)
        {
            uiScheduleItem = MarketingScheduleHelper.CreateUIScheduleItemByAppointment(appointment, this.uiScheduleId);
            uiScheduleItem.Save();
        }

        AppointmentType type = appointment.Type.ToEnum<AppointmentType>();
        if (type == AppointmentType.DeletedOccurrence || type == AppointmentType.ChangedOccurrence)
        {
            return this.CreateExceptionOccurrence(appointment, uiScheduleItem).UIScheduleItemOccurrenceId;
        }
        else
        {
            return this.CreateOccurrence(appointment, uiScheduleItem).UIScheduleItemOccurrenceId;
        }              
    }

    public void DeleteMethodHandler(MarketingAppointment app)
    {
        UIScheduleItemOccurrenceEntity uiScheduleItemOccurrence = new UIScheduleItemOccurrenceEntity((int)app.UIScheduleItemOccurrenceId);
        if (!uiScheduleItemOccurrence.IsNew)
        {
            UIScheduleItemEntity uiScheduleItem = uiScheduleItemOccurrence.UIScheduleItemEntity;
            uiScheduleItemOccurrence.Delete();
            uiScheduleItem.UIScheduleItemOccurrenceCollection.Remove(uiScheduleItemOccurrence);

            if (uiScheduleItem.UIScheduleItemOccurrenceCollection.Count == 0)
            {
                uiScheduleItem.Delete();
            }
        }

        this.Appointments.Remove(app);
    }

    public void UpdateMethodHandler(MarketingAppointment app)
    {
        if (app.UIScheduleItemOccurrenceId == null)
            return;

        int eventIndex = this.Appointments.GetEventIndex((int)app.UIScheduleItemOccurrenceId);
        if (eventIndex >= 0)
        {
            if (app.Type == (int)AppointmentType.DeletedOccurrence)
            {
                this.DeleteMethodHandler(app);
            }
            else
            {
                UIScheduleItemOccurrenceEntity uiScheduleItemOccurrence = new UIScheduleItemOccurrenceEntity((int)app.UIScheduleItemOccurrenceId);
                if (!uiScheduleItemOccurrence.IsNew)
                {
                    UIScheduleItemEntity scheduledItem = this.UpdateUIScheduleItem(app, uiScheduleItemOccurrence);
                    if (scheduledItem == null)
                    {
                        return;
                    }

                    this.UpdateUIScheduleItemOccurrence(app, uiScheduleItemOccurrence);
                    scheduledItem.Save(true);
                }

                this.Appointments.RemoveAt(eventIndex);
                this.Appointments.Insert(eventIndex, app);
            }            
        }
    }    

    public IEnumerable SelectMethodHandler()
    {
        MarketingScheduleAppointmentList result = new MarketingScheduleAppointmentList();
        result.AddRange(this.Appointments);
        return result;
    }

    private AppointmentType GetAppointmentType(UIScheduleItemOccurrenceEntity uiScheduleItemOccurrence)
    {
        if (uiScheduleItemOccurrence.Type > 0)
        {
            return uiScheduleItemOccurrence.Type.ToEnum<AppointmentType>();
        }
        else if (uiScheduleItemOccurrence.Recurring)
        {
            return AppointmentType.Pattern;
        }
        else
        {
            return AppointmentType.Normal;
        }
    }

    private void SetAppointmentInfo(MarketingAppointment app, UIScheduleItemEntity uiScheduleItem, UIScheduleItemOccurrenceEntity uiScheduleItemOccurrence)
    {
        app.UIScheduleItemOccurrenceId = uiScheduleItemOccurrence.UIScheduleItemOccurrenceId;
        app.BackgroundColor = uiScheduleItemOccurrence.BackgroundColor;
        app.TextColor = uiScheduleItemOccurrence.TextColor;
        app.StartTime = uiScheduleItemOccurrence.StartTime.Value;
        app.EndTime = uiScheduleItemOccurrence.EndTime.Value;

        if (uiScheduleItemOccurrence.Recurring)
        {
            RecurrenceInfo recurrenceInfo = new RecurrenceInfo(); 
            recurrenceInfo.Type = (RecurrenceType)uiScheduleItemOccurrence.RecurrenceType;
            recurrenceInfo.Range = (RecurrenceRange)uiScheduleItemOccurrence.RecurrenceRange;
            recurrenceInfo.Start = uiScheduleItemOccurrence.RecurrenceStart.Value;
            recurrenceInfo.End = uiScheduleItemOccurrence.RecurrenceEnd.Value;
            recurrenceInfo.OccurrenceCount = uiScheduleItemOccurrence.RecurrenceOccurrenceCount;
            recurrenceInfo.Periodicity = uiScheduleItemOccurrence.RecurrencePeriodicity;
            recurrenceInfo.DayNumber = uiScheduleItemOccurrence.RecurrenceDayNumber;
            recurrenceInfo.WeekDays = (WeekDays)uiScheduleItemOccurrence.RecurrenceWeekDays;
            recurrenceInfo.WeekOfMonth = (WeekOfMonth)uiScheduleItemOccurrence.RecurrenceWeekOfMonth;
            recurrenceInfo.Month = uiScheduleItemOccurrence.RecurrenceMonth;            

            app.RecurrenceId = (Guid)recurrenceInfo.Id;
            app.RecurrenceInfo = recurrenceInfo.ToXml();
            app.ReminderInfo = string.Empty;
        }

        if (uiScheduleItem.UIWidgetId.HasValue)
        {
            string linkedItemDescription = uiScheduleItem.UIWidgetEntity.LinkedItemDescription;
            app.UIWidgetId = uiScheduleItem.UIWidgetEntity.UIWidgetId;
            app.Subject = "Widget: " + uiScheduleItem.UIWidgetEntity.NameOrTypeName;
            if (linkedItemDescription.Length > 0)
            {
                app.Location = linkedItemDescription;
            }
        }
        else if (uiScheduleItem.MediaId.HasValue)
        {
            app.MediaId = uiScheduleItem.MediaEntity.MediaId;
            app.Subject = "Slide: " + uiScheduleItem.MediaEntity.Name;
        }
        else if (uiScheduleItem.ScheduledMessageId.HasValue)
        {
            app.ScheduledMessageId = uiScheduleItem.ScheduledMessageEntity.ScheduledMessageId;
            app.MessagegroupId = uiScheduleItemOccurrence.MessagegroupId ?? 0;
            app.Subject = "Message: " + uiScheduleItem.ScheduledMessageEntity.Title;
            if (uiScheduleItemOccurrence.MessagegroupId.HasValue)
            {
                app.Location = "Messagegroup: " + uiScheduleItemOccurrence.MessagegroupEntity.Name;
            }
        }     
    }

    private UIScheduleItemOccurrenceEntity CreateOccurrence(MarketingAppointment appointment, UIScheduleItemEntity uiScheduleItem)
    {
        UIScheduleItemOccurrenceEntity uiScheduleItemOccurence = this.CreateUIScheduleItemOccurrenceByAppointment(appointment, uiScheduleItem);
        uiScheduleItemOccurence.Save();

        this.SetAppointmentInfo(appointment, uiScheduleItem, uiScheduleItemOccurence);

        this.Appointments.Add(appointment);

        return uiScheduleItemOccurence;
    }

    public UIScheduleItemOccurrenceEntity CreateExceptionOccurrence(MarketingAppointment appointment, UIScheduleItemEntity uiScheduleItem)
    {
        UIScheduleItemOccurrenceEntity uiScheduleItemOccurence = this.CreateUIScheduleItemOccurrenceByAppointment(appointment, uiScheduleItem);
        uiScheduleItemOccurence.Save();

        this.SetAppointmentInfo(appointment, uiScheduleItem, uiScheduleItemOccurence);

        return uiScheduleItemOccurence;
    }

    private UIScheduleItemOccurrenceEntity CreateUIScheduleItemOccurrenceByAppointment(MarketingAppointment appointment, UIScheduleItemEntity uiScheduleItem)
    {
        DateTime startTime = DateTime.SpecifyKind(appointment.StartTime, DateTimeKind.Unspecified);
        DateTime endTime = DateTime.SpecifyKind(appointment.EndTime, DateTimeKind.Unspecified);

        DateTime startTimeUtc = startTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
        DateTime endTimeUtc = endTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);

        TimeSpan difference = endTime - startTime;

        UIScheduleItemOccurrenceEntity uiScheduleItemOccurrence = new UIScheduleItemOccurrenceEntity();
        uiScheduleItemOccurrence.UIScheduleItemId = uiScheduleItem.UIScheduleItemId;
        uiScheduleItemOccurrence.StartTime = startTime;
        uiScheduleItemOccurrence.EndTime = endTime;
        uiScheduleItemOccurrence.StartTimeUTC = startTimeUtc;
        uiScheduleItemOccurrence.EndTimeUTC = endTimeUtc;
        uiScheduleItemOccurrence.BackgroundColor = appointment.BackgroundColor;
        uiScheduleItemOccurrence.TextColor = appointment.TextColor;
        uiScheduleItemOccurrence.Type = appointment.Type;

        if (appointment.MessagegroupId > 0)
        {
            uiScheduleItemOccurrence.MessagegroupId = appointment.MessagegroupId;
        }

        if (appointment.Type == (int)AppointmentType.DeletedOccurrence || appointment.Type == (int)AppointmentType.ChangedOccurrence)
        {
            IRecurrenceInfo recurrenceInfo = RecurrenceInfoXmlPersistenceHelper.ObjectFromXml(appointment.RecurrenceInfo);

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(appointment.RecurrenceInfo);
            XmlNode indexNode = xml.DocumentElement.Attributes.GetNamedItem("Index");
            if (indexNode != null && !indexNode.Value.IsNullOrWhiteSpace())
            {
                uiScheduleItemOccurrence.RecurrenceIndex = int.Parse(indexNode.Value);
            }

            MarketingAppointment parentAppointment = this.CachedAppointments.FirstOrDefault(x => x.RecurrenceId == (Guid)recurrenceInfo.Id);
            if (parentAppointment != null && parentAppointment.UIScheduleItemOccurrenceId != null)
            {
                uiScheduleItemOccurrence.ParentUIScheduleItemOccurrenceId = (int?)parentAppointment.UIScheduleItemOccurrenceId;
            }
        }
        else if (appointment.Type == (int)AppointmentType.Pattern)
        {
            IRecurrenceInfo recurrenceInfo = RecurrenceInfoXmlPersistenceHelper.ObjectFromXml(appointment.RecurrenceInfo);
            
            if (recurrenceInfo.Range == 0 && difference.TotalMinutes > 1440) // Forever recurring with difference longer than a day
            {
                uiScheduleItemOccurrence.EndTime = startTime.AddDays(1);
                uiScheduleItemOccurrence.EndTimeUTC = startTimeUtc.AddDays(1);
            }

            startTime = recurrenceInfo.Start;
            endTime = recurrenceInfo.End;

            uiScheduleItemOccurrence.Recurring = true;
            uiScheduleItemOccurrence.RecurrenceType = (int)recurrenceInfo.Type;
            uiScheduleItemOccurrence.RecurrenceRange = (int)recurrenceInfo.Range;
            uiScheduleItemOccurrence.RecurrenceStart = startTime;
            uiScheduleItemOccurrence.RecurrenceEnd = endTime;
            uiScheduleItemOccurrence.RecurrenceStartUTC = startTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
            uiScheduleItemOccurrence.RecurrenceEndUTC = endTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
            uiScheduleItemOccurrence.RecurrenceOccurrenceCount = recurrenceInfo.OccurrenceCount;
            uiScheduleItemOccurrence.RecurrencePeriodicity = recurrenceInfo.Periodicity;
            uiScheduleItemOccurrence.RecurrenceDayNumber = recurrenceInfo.DayNumber;
            uiScheduleItemOccurrence.RecurrenceWeekDays = (int)recurrenceInfo.WeekDays;
            uiScheduleItemOccurrence.RecurrenceWeekOfMonth = (int)recurrenceInfo.WeekOfMonth;
            uiScheduleItemOccurrence.RecurrenceMonth = recurrenceInfo.Month;
        };

        return uiScheduleItemOccurrence;
    }

    private UIScheduleItemEntity UpdateUIScheduleItem(MarketingAppointment app, UIScheduleItemOccurrenceEntity uiScheduleItemOccurrence)
    {
        UIScheduleItemEntity scheduledItem = uiScheduleItemOccurrence.UIScheduleItemEntity;

        UIWidgetEntity uiWidget = null;
        MediaEntity media = null;
        ScheduledMessageEntity scheduledMessage = null;

        if (app.UIWidgetId > 0)
        {
            uiWidget = new UIWidgetEntity(app.UIWidgetId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIScheduleItemFields.UIScheduleId == scheduledItem.UIScheduleId);
            filter.Add(UIScheduleItemFields.UIWidgetId == app.UIWidgetId);
            filter.Add(UIScheduleItemFields.UIScheduleItemId != scheduledItem.UIScheduleItemId);

            UIScheduleItemCollection uiScheduleItemCollection = new UIScheduleItemCollection();
            uiScheduleItemCollection.GetMulti(filter);

            if (uiScheduleItemCollection.Count > 0)
            {
                uiScheduleItemOccurrence.UIScheduleItemId = uiScheduleItemCollection[0].UIScheduleItemId;
                uiScheduleItemOccurrence.Save();

                scheduledItem.Delete();
                scheduledItem = uiScheduleItemCollection[0];
            }
        }
        else if (app.MediaId > 0)
        {
            media = new MediaEntity(app.MediaId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIScheduleItemFields.UIScheduleId == scheduledItem.UIScheduleId);
            filter.Add(UIScheduleItemFields.MediaId == app.MediaId);
            filter.Add(UIScheduleItemFields.UIScheduleItemId != scheduledItem.UIScheduleItemId);

            UIScheduleItemCollection uiScheduleItemCollection = new UIScheduleItemCollection();
            uiScheduleItemCollection.GetMulti(filter);

            if (uiScheduleItemCollection.Count > 0)
            {
                uiScheduleItemOccurrence.UIScheduleItemId = uiScheduleItemCollection[0].UIScheduleItemId;
                uiScheduleItemOccurrence.Save();

                scheduledItem.Delete();
                scheduledItem = uiScheduleItemCollection[0];
            }
        }
        else if (app.ScheduledMessageId > 0)
        {
            scheduledMessage = new ScheduledMessageEntity(app.ScheduledMessageId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(UIScheduleItemFields.UIScheduleId == scheduledItem.UIScheduleId);
            filter.Add(UIScheduleItemFields.ScheduledMessageId == app.ScheduledMessageId);
            filter.Add(UIScheduleItemFields.UIScheduleItemId != scheduledItem.UIScheduleItemId);

            UIScheduleItemCollection uiScheduleItemCollection = new UIScheduleItemCollection();
            uiScheduleItemCollection.GetMulti(filter);

            if (uiScheduleItemCollection.Count > 0)
            {
                uiScheduleItemOccurrence.UIScheduleItemId = uiScheduleItemCollection[0].UIScheduleItemId;
                uiScheduleItemOccurrence.Save();

                scheduledItem.Delete();
                scheduledItem = uiScheduleItemCollection[0];
            }
        }
        else
        {
            scheduledItem = null;
        }

        if (scheduledItem != null)
        {
            scheduledItem.UIWidgetId = null;
            scheduledItem.MediaId = null;
            scheduledItem.ScheduledMessageId = null;

            if (uiWidget != null)
            {
                scheduledItem.UIWidgetId = uiWidget.UIWidgetId;
            }
            else if (media != null)
            {
                scheduledItem.MediaId = media.MediaId;
            }
            else if (scheduledMessage != null)
            {
                scheduledItem.ScheduledMessageId = scheduledMessage.ScheduledMessageId;
            }            
        }

        return scheduledItem;
    }

    private void UpdateUIScheduleItemOccurrence(MarketingAppointment app, UIScheduleItemOccurrenceEntity uiScheduleItemOccurrence)
    {
        DateTime startTime = DateTime.SpecifyKind(app.StartTime, DateTimeKind.Unspecified);
        DateTime endTime = DateTime.SpecifyKind(app.EndTime, DateTimeKind.Unspecified);

        DateTime startTimeUtc = startTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
        DateTime endTimeUtc = endTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);

        TimeSpan difference = endTime - startTime;

        uiScheduleItemOccurrence.StartTime = startTime;
        uiScheduleItemOccurrence.EndTime = endTime;
        uiScheduleItemOccurrence.StartTimeUTC = startTimeUtc;
        uiScheduleItemOccurrence.EndTimeUTC = endTimeUtc;
        uiScheduleItemOccurrence.BackgroundColor = app.BackgroundColor;
        uiScheduleItemOccurrence.TextColor = app.TextColor;
        uiScheduleItemOccurrence.MessagegroupId = app.MessagegroupId > 0 ? app.MessagegroupId : (int?)null;
        uiScheduleItemOccurrence.Type = app.Type;

        if (app.Type == (int)AppointmentType.Pattern)
        {
            IRecurrenceInfo recurrenceInfo = RecurrenceInfoXmlPersistenceHelper.ObjectFromXml(app.RecurrenceInfo);

            if (recurrenceInfo.Range == 0 && difference.TotalMinutes > 1440) // Forever recurring with difference longer than a day
            {
                uiScheduleItemOccurrence.EndTime = startTime.AddDays(1);
                uiScheduleItemOccurrence.EndTimeUTC = startTimeUtc.AddDays(1);
            }

            startTime = recurrenceInfo.Start;
            endTime = recurrenceInfo.End;

            uiScheduleItemOccurrence.Recurring = true;
            uiScheduleItemOccurrence.RecurrenceType = (int)recurrenceInfo.Type;
            uiScheduleItemOccurrence.RecurrenceRange = (int)recurrenceInfo.Range;
            uiScheduleItemOccurrence.RecurrenceStart = startTime;
            uiScheduleItemOccurrence.RecurrenceEnd = endTime;
            uiScheduleItemOccurrence.RecurrenceStartUTC = startTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
            uiScheduleItemOccurrence.RecurrenceEndUTC = endTime.LocalTimeToUtc(CmsSessionHelper.CompanyTimeZone);
            uiScheduleItemOccurrence.RecurrenceOccurrenceCount = recurrenceInfo.OccurrenceCount;
            uiScheduleItemOccurrence.RecurrencePeriodicity = recurrenceInfo.Periodicity;
            uiScheduleItemOccurrence.RecurrenceDayNumber = recurrenceInfo.DayNumber;
            uiScheduleItemOccurrence.RecurrenceWeekDays = (int)recurrenceInfo.WeekDays;
            uiScheduleItemOccurrence.RecurrenceWeekOfMonth = (int)recurrenceInfo.WeekOfMonth;
            uiScheduleItemOccurrence.RecurrenceMonth = recurrenceInfo.Month;
        }
        else
        {
            uiScheduleItemOccurrence.Recurring = false;
            uiScheduleItemOccurrence.RecurrenceType = 0;
            uiScheduleItemOccurrence.RecurrenceRange = 0;
            uiScheduleItemOccurrence.RecurrenceStart = null;
            uiScheduleItemOccurrence.RecurrenceEnd = null;
            uiScheduleItemOccurrence.RecurrenceStartUTC = null;
            uiScheduleItemOccurrence.RecurrenceEndUTC = null;
            uiScheduleItemOccurrence.RecurrenceOccurrenceCount = 0;
            uiScheduleItemOccurrence.RecurrencePeriodicity = 0;
            uiScheduleItemOccurrence.RecurrenceDayNumber = 0;
            uiScheduleItemOccurrence.RecurrenceWeekDays = 0;
            uiScheduleItemOccurrence.RecurrenceWeekOfMonth = 0;
            uiScheduleItemOccurrence.RecurrenceMonth = 0;
        }
    }

    public MarketingScheduleAppointmentList Appointments
    {
        get
        {
            if (this.appointments == null)
            {
                this.appointments = new MarketingScheduleAppointmentList();

                PredicateExpression filter = new PredicateExpression();
                filter.Add(UIScheduleItemFields.UIScheduleId == this.uiScheduleId);

                PrefetchPath prefetch = new PrefetchPath(EntityType.UIScheduleItemEntity);
                
                var occurrencePrefetch = prefetch.Add(UIScheduleItemEntityBase.PrefetchPathUIScheduleItemOccurrenceCollection);
                occurrencePrefetch.Filter = new PredicateExpression(UIScheduleItemOccurrenceFields.ParentUIScheduleItemOccurrenceId == DBNull.Value);
                occurrencePrefetch.SubPath.Add(UIScheduleItemOccurrenceEntityBase.PrefetchPathUIScheduleItemOccurrenceCollection);

                prefetch.Add(UIScheduleItemEntityBase.PrefetchPathUIWidgetEntity).SubPath.Add(UIWidgetEntityBase.PrefetchPathAdvertisementEntity);
                prefetch.Add(UIScheduleItemEntityBase.PrefetchPathMediaEntity);
                prefetch.Add(UIScheduleItemEntityBase.PrefetchPathScheduledMessageEntity);

                UIScheduleItemCollection uiScheduleItems = new UIScheduleItemCollection();
                uiScheduleItems.GetMulti(filter, prefetch);

                foreach (UIScheduleItemEntity uiScheduleItem in uiScheduleItems)
                {
                    foreach (UIScheduleItemOccurrenceEntity uiScheduleItemOccurrence in uiScheduleItem.UIScheduleItemOccurrenceCollection)
                    {
                        AppointmentType type = this.GetAppointmentType(uiScheduleItemOccurrence);
                        MarketingAppointment app = new MarketingAppointment(type);
                        this.SetAppointmentInfo(app, uiScheduleItem, uiScheduleItemOccurrence);

                        this.appointments.Add(app);
                        
                        foreach (UIScheduleItemOccurrenceEntity exceptionUIScheduleItemOccurrence in uiScheduleItemOccurrence.UIScheduleItemOccurrenceCollection)
                        {
                            AppointmentType exceptionType = exceptionUIScheduleItemOccurrence.Type.ToEnum<AppointmentType>();
                            MarketingAppointment exception = app.CreateException(exceptionType, exceptionUIScheduleItemOccurrence.RecurrenceIndex);
                            exception.RecurrenceInfo = string.Format(@"<RecurrenceInfo Id=""{0}"" Index=""{1}"" />", app.RecurrenceId, exceptionUIScheduleItemOccurrence.RecurrenceIndex);

                            this.SetAppointmentInfo(exception, uiScheduleItem, exceptionUIScheduleItemOccurrence);

                            this.appointments.Add(exception);
                        }
                    }
                }

                Dionysos.Web.CacheHelper.AddSlidingExpire(true, MarketingScheduleKey, this.appointments, 60);                
            }

            return this.appointments;
        }
    }    

    private MarketingScheduleAppointmentList CachedAppointments
    {
        get
        {
            MarketingScheduleAppointmentList appointments = new MarketingScheduleAppointmentList();
            Dionysos.Web.CacheHelper.TryGetValue(this.MarketingScheduleKey, true, out appointments);            
            return appointments;
        }
    }

    private string MarketingScheduleKey
    {
        get { return string.Format("MarketingSchedule-{0}", this.uiScheduleId); }
    }

    #endregion
}