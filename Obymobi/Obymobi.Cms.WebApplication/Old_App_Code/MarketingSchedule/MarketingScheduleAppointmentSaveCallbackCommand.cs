﻿using DevExpress.Web;
using DevExpress.Web.ASPxScheduler.Internal;
using DevExpress.Web.ASPxScheduler;
using DevExpress.XtraScheduler;

public class MarketingScheduleAppointmentSaveCallbackCommand : AppointmentFormSaveCallbackCommand
{
    public MarketingScheduleAppointmentSaveCallbackCommand(ASPxScheduler control) : base(control)
    {
    }

    protected internal new ScheduleAppointmentFormController Controller
    {
        get { return (ScheduleAppointmentFormController)base.Controller; }
    }

    protected override void AssignControllerValues()
    {
        ASPxComboBox ddlUIWidgetId = (ASPxComboBox)this.FindControlByID("ddlUIWidgetId");
        ASPxComboBox ddlMediaId = (ASPxComboBox)this.FindControlByID("ddlMediaId");
        ASPxComboBox ddlScheduledMessageId = (ASPxComboBox)this.FindControlByID("ddlScheduledMessageId");
        ASPxComboBox ddlMessagegroupId = (ASPxComboBox)this.FindControlByID("ddlMessagegroupId");
        ASPxColorEdit ceBackgroundColor = (ASPxColorEdit)this.FindControlByID("ceBackgroundColor");
        ASPxColorEdit ceTextColor = (ASPxColorEdit)this.FindControlByID("ceTextColor");

        this.Controller.UIWidgetId = System.Convert.ToInt32(ddlUIWidgetId.Value);
        this.Controller.MediaId = System.Convert.ToInt32(ddlMediaId.Value);
        this.Controller.ScheduledMessageId = System.Convert.ToInt32(ddlScheduledMessageId.Value);
        this.Controller.MessagegroupId = System.Convert.ToInt32(ddlMessagegroupId.Value);
        this.Controller.BackgroundColor = ceBackgroundColor.Color.ToArgb();
        this.Controller.TextColor = ceTextColor.Color.ToArgb();

        if (this.Controller.UIWidgetId > 0)
        {
            this.Controller.Subject = "Widget: " + ddlUIWidgetId.Text;
        }
        else if (this.Controller.MediaId > 0)
        {
            this.Controller.Subject = "Slide: " + ddlMediaId.Text;
        }
        else if (this.Controller.ScheduledMessageId > 0)
        {
            this.Controller.Subject = "Message: " + ddlScheduledMessageId.Text;
        }

        base.AssignControllerValues();
    }
    protected override AppointmentFormController CreateAppointmentFormController(Appointment apt)
    {
        return new ScheduleAppointmentFormController(this.Control, apt);
    }
}