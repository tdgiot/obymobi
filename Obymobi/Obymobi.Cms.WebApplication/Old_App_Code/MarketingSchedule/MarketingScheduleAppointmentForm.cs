﻿using DevExpress.Web.ASPxScheduler;
using DevExpress.Web.ASPxScheduler.Internal;
using DevExpress.XtraScheduler;

public class MarketingScheduleAppointmentForm : AppointmentFormTemplateContainer
{
    private int deliverypointgroupId;

    public MarketingScheduleAppointmentForm(ASPxScheduler control, int deliverypointgroupId)
        : base(control)
    {
        this.deliverypointgroupId = deliverypointgroupId;
    }

    public int DeliverypointgroupId
    {
        get
        {
            return this.deliverypointgroupId;
        }
    }

    public int UIWidgetId
    {
        get
        {
            object val = Appointment.CustomFields["UIWidgetId"];
            return (val == System.DBNull.Value) ? 0 : System.Convert.ToInt32(val);
        }
    }

    public int MediaId
    {
        get
        {
            object val = Appointment.CustomFields["MediaId"];
            return (val == System.DBNull.Value) ? 0 : System.Convert.ToInt32(val);
        }
    }

    public int ScheduledMessageId
    {
        get
        {
            object val = Appointment.CustomFields["ScheduledMessageId"];
            return (val == System.DBNull.Value) ? 0 : System.Convert.ToInt32(val);
        }
    }

    public int MessagegroupId
    {
        get
        {
            object val = Appointment.CustomFields["MessagegroupId"];
            return (val == System.DBNull.Value) ? 0 : System.Convert.ToInt32(val);
        }
    }

    public int BackgroundColor
    {
        get
        {
            object val = Appointment.CustomFields["BackgroundColor"];
            return (val == System.DBNull.Value) ? 0 : System.Convert.ToInt32(val);
        }
    }

    public int TextColor
    {
        get
        {
            object val = Appointment.CustomFields["TextColor"];
            return (val == System.DBNull.Value) ? 0 : System.Convert.ToInt32(val);
        }
    }
}

public class ScheduleAppointmentFormController : AppointmentFormController
{
    public ScheduleAppointmentFormController(ASPxScheduler control, Appointment apt)
        : base(control, apt) 
    { 
    }

    public int UIWidgetId
    {
        get 
        {
            return (int)EditedAppointmentCopy.CustomFields["UIWidgetId"]; 
        }
        set
        {
            this.EditedAppointmentCopy.CustomFields["UIWidgetId"] = value; 
        }
    }

    public int MediaId
    {
        get
        {
            return (int)EditedAppointmentCopy.CustomFields["MediaId"];
        }
        set
        {
            this.EditedAppointmentCopy.CustomFields["MediaId"] = value;
        }
    }

    public int ScheduledMessageId
    {
        get
        {
            return (int)EditedAppointmentCopy.CustomFields["ScheduledMessageId"];
        }
        set
        {
            this.EditedAppointmentCopy.CustomFields["ScheduledMessageId"] = value;
        }
    }

    public int MessagegroupId
    {
        get
        {
            return (int)EditedAppointmentCopy.CustomFields["MessagegroupId"];
        }
        set
        {
            this.EditedAppointmentCopy.CustomFields["MessagegroupId"] = value;
        }
    }

    public int BackgroundColor
    {
        get
        {
            return (int)EditedAppointmentCopy.CustomFields["BackgroundColor"]; 
        }
        set
        {
            this.EditedAppointmentCopy.CustomFields["BackgroundColor"] = value;
        }
    }

    public int TextColor
    {
        get
        {
            return (int)EditedAppointmentCopy.CustomFields["TextColor"];
        }
        set
        {
            this.EditedAppointmentCopy.CustomFields["TextColor"] = value;
        }
    }

    int SourceUIWidgetId
    {
        get 
        { 
            return (int)SourceAppointment.CustomFields["UIWidgetId"]; 
        }
        set 
        { 
            SourceAppointment.CustomFields["UIWidgetId"] = value; 
        }
    }

    int SourceMediaId
    {
        get
        {
            return (int)SourceAppointment.CustomFields["MediaId"];
        }
        set
        {
            SourceAppointment.CustomFields["MediaId"] = value;
        }
    }

    int SourceScheduledMessageId
    {
        get
        {
            return (int)SourceAppointment.CustomFields["ScheduledMessageId"];
        }
        set
        {
            SourceAppointment.CustomFields["ScheduledMessageId"] = value;
        }
    }

    int SourceMessagegroupId
    {
        get
        {
            return (int)SourceAppointment.CustomFields["MessagegroupId"];
        }
        set
        {
            SourceAppointment.CustomFields["MessagegroupId"] = value;
        }
    }

    int SourceBackgroundColor
    {
        get
        {
            return (int)SourceAppointment.CustomFields["BackgroundColor"];
        }
        set
        {
            SourceAppointment.CustomFields["BackgroundColor"] = value;
        }
    }

    int SourceTextColor
    {
        get
        {
            return (int)SourceAppointment.CustomFields["TextColor"];
        }
        set
        {
            SourceAppointment.CustomFields["TextColor"] = value;
        }
    }
}