﻿using DevExpress.XtraScheduler;
using System;
using Obymobi.Logic;

public class MarketingAppointment
{
    

    public MarketingAppointment()
    {
        
    }

    public MarketingAppointment(AppointmentType type)
    {
        this.Appointment = new DevExpressAppointment(type);
        this.Type = (int)Appointment.Type;
    }

    public MarketingAppointment(Appointment appointment)
    {
        this.Appointment = appointment;
        this.Type = (int)appointment.Type;
    }

    public MarketingAppointment CreateException(AppointmentType type, int recurrenceIndex)
    {
        return new MarketingAppointment(this.Appointment.CreateException(type, recurrenceIndex));
    }

    public object UIScheduleItemOccurrenceId { get; set; }
    public int UIWidgetId { get; set; }
    public int MediaId { get; set; }
    public int ScheduledMessageId { get; set; }
    public int MessagegroupId { get; set; }
    public int BackgroundColor { get; set; }
    public int TextColor { get; set; }
    public DateTime StartTime { get; set; }
    public DateTime EndTime { get; set; }
    public string ReminderInfo { get; set; }
    public string RecurrenceInfo { get; set; }
    public int Type { get; set; }
    public string Subject { get; set; }
    public string Location { get; set; }
    public bool AllDay { get; set; }
    public int Status { get; set; }
    public int Label { get; set; }
    public string Description { get; set; }    
    public Guid RecurrenceId { get; set; }    
    public Appointment Appointment { get; set; }    
}