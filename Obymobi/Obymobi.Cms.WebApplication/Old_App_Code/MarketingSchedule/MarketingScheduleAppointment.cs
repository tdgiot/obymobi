﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class MarketingScheduleAppointment
{
    public object ScheduleitemId { get; set; }
    public int UIWidgetId { get; set; }
    public int MediaId { get; set; }
    public int ScheduledMessageId { get; set; }
    public int MessagegroupId { get; set; }
    public string Subject { get; set; }
    public DateTime StartTime { get; set; }
    public DateTime EndTime { get; set; }
    public int BackgroundColor { get; set; }
    public int TextColor { get; set; }

    public bool AllDay { get; set; }
    public string Description { get; set; }
    public long Label { get; set; }
    public string Location { get; set; }
    public string RecurrenceInfo { get; set; }
    public string ReminderInfo { get; set; }
    public int Status { get; set; }
    public int Type { get; set; }
}