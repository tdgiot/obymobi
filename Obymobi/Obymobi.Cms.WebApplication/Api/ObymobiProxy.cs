﻿using System;
using System.Security.Principal;
using System.Web;

namespace Obymobi.Cms.WebApplication.Api
{
    public static class ObymobiProxy
    {
        private const int UserId = 166; // floris.otto

        public static ActionResult<T> TryExecuteAsGodMode<T>(Func<T> action)
        {
            ActionResult<T> actionResult;

            try
            {
                HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(UserId.ToString()), new string[0]);

                T result = action.Invoke();

                actionResult = ActionResult<T>.FromSuccess(result);
            }
            catch (Exception ex)
            {
                actionResult = ActionResult<T>.FromError($"Unexcepted exception occurred: {ex}");
            }

            return actionResult;
        }
    }
}