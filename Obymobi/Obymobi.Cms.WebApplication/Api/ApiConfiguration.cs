﻿using System.Web.Http;
using System.Web.Http.Cors;
using Newtonsoft.Json.Serialization;

namespace Obymobi.Cms.WebApplication.Api
{
    public static class ApiConfiguration
    {
        public static string UrlPrefix { get { return "api"; } }
        public static string UrlPrefixRelative { get { return "~/api"; } }

        public static void Register(HttpConfiguration config)
        {
            EnableCrossSiteRequests(config);
            AddRoutes(config);
        }

        private static void EnableCrossSiteRequests(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute(
                origins: "*",
                headers: "*",
                methods: "*");
            config.EnableCors(cors);
        }

        public static void AddRoutes(HttpConfiguration config)
        {
           config.Routes.MapHttpRoute(
                            name: "DefaultApi",
                            routeTemplate: UrlPrefix + "/{controller}/{action}/{id}",
                            defaults: new { id = RouteParameter.Optional });
        }

        public static void SetupFormatters()
        {
            GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }
}