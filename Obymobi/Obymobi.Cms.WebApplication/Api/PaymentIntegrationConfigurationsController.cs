﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Obymobi.Cms.WebApplication.Api
{
    [Route("api/paymentintegrationconfigurations")]
    [AllowAnonymous]
    public class PaymentIntegrationConfigurationsController : ApiController
    {
        [HttpGet]
        [ActionName("getByAccountCode")]
        public ActionResult<IEnumerable<string>> GetByAccountCode(string accountCode)
        {
            return ObymobiProxy.TryExecuteAsGodMode(() =>
            {
                IPredicate filter = new PredicateExpression(PaymentIntegrationConfigurationFields.AdyenAccountCode == accountCode);
                IncludeFieldsList entityFields = new IncludeFieldsList(PaymentIntegrationConfigurationFields.DisplayName);

                PaymentIntegrationConfigurationCollection paymentIntegrationConfigurations = new PaymentIntegrationConfigurationCollection();
                paymentIntegrationConfigurations.GetMulti(filter, entityFields, null);

                return paymentIntegrationConfigurations.Select(p => p.DisplayName);
            });
        }
    }
}