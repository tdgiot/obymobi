﻿namespace Obymobi.Cms.WebApplication.Api
{
    public class ActionResult<T>
    {
        private ActionResult(bool success, string error = null, T response = default)
        {
            this.Success = success;
            this.Error = error;
            this.Response = response;
        }

        public bool Success { get; }
        public string Error { get; }
        public T Response { get; }

        public static ActionResult<T> FromSuccess(T response = default)
        {
            return new ActionResult<T>(true, null, response);
        }

        public static ActionResult<T> FromError(string error)
        {
            return new ActionResult<T>(false, error);
        }
    }
}