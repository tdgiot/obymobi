﻿using System.Web.Http;
using Obymobi.Data.Master;
using Obymobi.Logic.Cms;
using Obymobi.ObymobiCms.Configuration.Sys;

namespace Obymobi.Cms.WebApplication.Api
{
    [Route("api/databaseactions")]
    [AllowAnonymous]
    public class DatabaseActionsController : ApiController
    {
        private const string SecretKey = "supersecretkey";

        [ActionName("anonymise")]
        public ActionResult<bool> Anonymise([FromBody] string secret)
        {
            if (!TryValidateSecret(secret, out ActionResult<bool> actionResult))
            {
                return actionResult;
            }

            if (Dionysos.ConfigurationManager.GetBoolRefreshed(ObymobiDataConfigConstants.DatabaseClearedForNonLiveUsage))
            {
                return ActionResult<bool>.FromError("Database was already anonymised");
            }

            return ObymobiProxy.TryExecuteAsGodMode(() =>
            {
                DatabaseClearer clearer = new DatabaseClearer();
                clearer.ClearDatabase();
                return true;
            });
        }

        [ActionName("mastermaintenance")]
        public ActionResult<bool> MasterMaintenance([FromBody] string secret)
        {
            if (!TryValidateSecret(secret, out ActionResult<bool> actionResult))
            {
                return actionResult;
            }

            return ObymobiProxy.TryExecuteAsGodMode(() => 
            {
                Modules.RefreshModules(true);

                UIElements.RefreshUIElements(true);

                UIElementSubPanels.RefreshUIElementSubPanels(true);
                UIElementSubPanels.RefreshUIElementSubPanelUIElements(true);

                EntityInformation.RefreshEntityInformationEntities(true);
                EntityInformation.RefreshEntityFieldInformationEntities(true);

                Views.CreateDefaultViews();
                Views.CreateDefaultViewItems(true);
                Maintenance.CreateDefaultClientViewItems();

                ReferentialConstraints.RefreshReferentialConstraints(true);
                return true;
            });
        }

        private static bool TryValidateSecret(string secret, out ActionResult<bool> actionResult)
        {
            actionResult = null;

            if (string.IsNullOrEmpty(secret) || secret != SecretKey)
            {
                actionResult = ActionResult<bool>.FromError("Invalid credentials provided");
                return false;
            }

            return true;
        }       
    }
}