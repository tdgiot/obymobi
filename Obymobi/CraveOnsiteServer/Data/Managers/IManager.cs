﻿using CraveOnsiteServer.Logic.Enum;

namespace CraveOnsiteServer.Data.Managers
{
    interface IManager
    {
        bool Load();
        LoadResult Reload();
        void ReloadRevert();
    }
}
