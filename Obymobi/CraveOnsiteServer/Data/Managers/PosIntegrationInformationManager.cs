﻿using System;
using System.Linq;
using System.IO;

using CraveOnsiteServer.Logic.Enum;

using Obymobi.Logic.Model;
using Obymobi.Logic.HelperClasses;
using Dionysos;
using Obymobi.Configuration;
using Obymobi.Logging;

namespace CraveOnsiteServer.Data.Managers
{
    public class PosIntegrationInformationManager : IManager
    {
        private const string FILENAME = "GetPosIntegrationInformation.data";
        private PosIntegrationInformation posIntegrationInformation;
        private PosIntegrationInformation oldData;

        public PosIntegrationInformation GetPosIntegrationInformation()
        {
            return posIntegrationInformation;
        }

        public Deliverypoint GetPosDeliverypointById(int deliverypointId)
        {
            return posIntegrationInformation.Deliverypoints.SingleOrDefault(dp => dp.DeliverypointId == deliverypointId);
        }

        public bool Load()
        {
            bool ret;
            
            ConsoleLogger.WriteToLog(" * Loading PosIntegrationInformation");

            if (Global.Status.IsWebserviceAvailable)
                ret = LoadFromWebservice();
            else
                ret = LoadFromDisk();

            if (this.posIntegrationInformation.Categories != null)
                ConsoleLogger.WriteToLog("    - Pos categories loaded: {0}", this.posIntegrationInformation.Categories.Length);

            if (this.posIntegrationInformation.Products != null)
                ConsoleLogger.WriteToLog("    - Pos products loaded: {0}", this.posIntegrationInformation.Products.Length);

            ConsoleLogger.WriteToLog("    - System messages deliverypoint external id: {0}", this.posIntegrationInformation.SystemMessagesDeliverypointId);
            ConsoleLogger.WriteToLog("    - Alternative system messages deliverypoint external id: {0}", this.posIntegrationInformation.AltSystemMessagesDeliverypointId);
            ConsoleLogger.WriteToLog("    - Unlock deliverypoint product external id: {0}", this.posIntegrationInformation.UnlockDeliverypointProductId);
            ConsoleLogger.WriteToLog("    - Battery low product external id: {0}", this.posIntegrationInformation.BatteryLowProductId);
            ConsoleLogger.WriteToLog("    - Client disconnected product external id: {0}", this.posIntegrationInformation.ClientDisconnectedProductId);

            if (this.posIntegrationInformation.SystemMessagesDeliverypointId.IsNullOrWhiteSpace() && this.posIntegrationInformation.AltSystemMessagesDeliverypointId.IsNullOrWhiteSpace())
            {
                ConsoleLogger.WriteToLog("************************************************");
                ConsoleLogger.WriteToLog("WARNING: THERE ARE NO SYSTEM MESSAGES DELIVERYPOINTS SPECIFIED");
                ConsoleLogger.WriteToLog("************************************************");
            }
            else if (this.posIntegrationInformation.UnlockDeliverypointProductId.IsNullOrWhiteSpace() || this.posIntegrationInformation.BatteryLowProductId.IsNullOrWhiteSpace() || this.posIntegrationInformation.ClientDisconnectedProductId.IsNullOrWhiteSpace())
            {
                ConsoleLogger.WriteToLog("************************************************");

                if (this.posIntegrationInformation.UnlockDeliverypointProductId.IsNullOrWhiteSpace())
                    ConsoleLogger.WriteToLog("WARNING: THERE IS NO UNLOCK DELIVERYPOINT PRODUCT SPECIFIED");

                if (this.posIntegrationInformation.BatteryLowProductId.IsNullOrWhiteSpace())
                    ConsoleLogger.WriteToLog("WARNING: THERE IS NO BATTERY LOW PRODUCT SPECIFIED");

                if (this.posIntegrationInformation.ClientDisconnectedProductId.IsNullOrWhiteSpace())
                    ConsoleLogger.WriteToLog("WARNING: THERE IS NO CLIENT DISCONNECTED PRODUCT SPECIFIED");

                ConsoleLogger.WriteToLog("************************************************");
            }

            return ret;
        }

        public LoadResult Reload()
        {
            this.oldData = this.posIntegrationInformation;
            LoadResult toReturn = LoadResult.ERROR;

            if (LoadFromWebservice())
            {
                toReturn = LoadResult.NEW_DATA;
                if (DataManager.PublicPropertiesEqual(this.oldData, this.posIntegrationInformation))
                {
                    toReturn = LoadResult.LOADED;
                    this.oldData = null;
                }
            }

            return toReturn;
        }

        public void ReloadRevert()
        {
            if (this.oldData != null)
            {
                this.posIntegrationInformation = this.oldData;
                this.oldData = null;
            }
        }

        private string GetDataFilePath()
        {
            return Path.Combine(Global.RootDirectory, "Data", FILENAME);
        }

        public bool LoadFromWebservice()
        {

            bool ret = false;

            string path = GetDataFilePath();

            // Only retrieve from webservice when local version is outdated
            if (ConfigurationManager.GetLong(CraveOnSiteServerConfigConstants.PosIntegrationInformationLastModifiedTicks) < DataManager.Instance.CompanyManager.Company.PosIntegrationInformationLastModifiedTicks || !File.Exists(path))
            {
                ConsoleLogger.WriteToLog(" * Pos Integration Information is outdated or does not exist on disk. Getting from webservice.");

                this.posIntegrationInformation = WebserviceManager.GetPosIntegrationInformation();

                if (this.posIntegrationInformation != null)
                {
                    string posIntegrationInformationSerialized = XmlHelper.Serialize(posIntegrationInformation);
                    posIntegrationInformationSerialized = posIntegrationInformationSerialized.Replace("&lt;", "").Replace("&gt;", "");

                    DataManager.Instance.WriteToFile(FILENAME, posIntegrationInformationSerialized);
                    ret = true;
                }

                if (ret)
                {
                    ConfigurationManager.SetValue(CraveOnSiteServerConfigConstants.PosIntegrationInformationLastModifiedTicks, DataManager.Instance.CompanyManager.Company.PosIntegrationInformationLastModifiedTicks);
                }
            }
            else
            {
                ConsoleLogger.WriteToLog(" * Loading Pos Integration Information from webservice skipped. Local version is up-to-date.");

                // Local version is up-to-date, load from disk
                ret = LoadFromDisk();
            }

            return ret;
        }

        public bool LoadFromDisk()
        {
            bool ret = false;
            string path = GetDataFilePath();
            if (File.Exists(path))
            {
                try
                {
                    string data = File.ReadAllText(path);
                    data = data.Replace("&lt;", "").Replace("&gt;", "");

                    this.posIntegrationInformation = XmlHelper.Deserialize<PosIntegrationInformation>(data);
                    ret = true;
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteToLog("ERROR: Exception while deserializing cached GetPosIntegrationInformation data. Exception: {0}", ex.Message);
                }
            }
            else
            {
                ConsoleLogger.WriteToLog("ERROR: Loading PosIntegrationInformation info from Disk. File not found: {0}", path);
            }

            return ret;
        }
    }
}
