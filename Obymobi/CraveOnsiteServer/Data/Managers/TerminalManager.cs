﻿using System;
using System.IO;
using CraveOnsiteServer.Logic.Enum;
using Dionysos;
using Obymobi.Configuration;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Logging;

namespace CraveOnsiteServer.Data.Managers
{
    public class TerminalManager : IManager
    {
        #region  Fields

        private const string FILENAME = "GetTerminal.data";
        private Terminal oldData;
        private Terminal terminal;

        #endregion

        #region Methods

        public Terminal GetTerminal()
        {
            return this.terminal;
        }

        public bool SynchronizeWithPos()
        {
            return this.terminal.SynchronizeWithPOSOnStart;
        }

        public bool SynchronizeWithPms()
        {
            return this.terminal.SynchronizeWithPMSOnStart;
        }

        public bool LoadFromWebservice()
        {
            bool ret = false;
            this.terminal = WebserviceManager.GetTerminal();

            if (this.terminal != null)
            {
                string terminalSerialized = XmlHelper.Serialize(this.terminal);
                DataManager.Instance.WriteToFile(TerminalManager.FILENAME, terminalSerialized);

                // Write value to configuration file
                ConfigurationManager.SetValue(CraveOnSiteServerConfigConstants.RequestInterval, this.terminal.RequestInterval);
                if (Global.RequestInterval != this.terminal.RequestInterval)
                {
                    ConsoleLogger.WriteToLog("Request Interval updated to: {0}", this.terminal.RequestInterval);
                    Global.RequestInterval = this.terminal.RequestInterval;
                }

                ret = true;
            }

            return ret;
        }

        public bool LoadFromDisk()
        {
            bool ret = false;
            string path = Path.Combine(Global.RootDirectory, "data/" + TerminalManager.FILENAME);
            if (File.Exists(path))
            {
                try
                {
                    string data = File.ReadAllText(path);
                    this.terminal = XmlHelper.Deserialize<Terminal>(data);
                    ret = true;
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteToLog("ERROR: Exception while deserializing cached GetTerminal data. Exception: {0}", ex.Message);
                }
            }
            else
            {
                ConsoleLogger.WriteToLog("ERROR: Loading Terminal info from Disk. File not found: {0}", path);
            }

            return ret;
        }

        public bool Load()
        {
            bool ret;

            ConsoleLogger.WriteToLog(" * Loading Terminal");

            if (Global.Status.IsWebserviceAvailable)
            {
                ret = this.LoadFromWebservice();
            }
            else
            {
                ret = this.LoadFromDisk();
            }

            return ret;
        }

        public LoadResult Reload()
        {
            this.oldData = this.terminal;
            LoadResult toReturn = LoadResult.ERROR;

            if (this.LoadFromWebservice())
            {
                toReturn = LoadResult.NEW_DATA;
                if (DataManager.PublicPropertiesEqual(this.oldData, this.terminal))
                {
                    toReturn = LoadResult.LOADED;
                    this.oldData = null;
                }
            }
            return toReturn;
        }

        public void ReloadRevert()
        {
            if (this.oldData != null)
            {
                this.terminal = this.oldData;
                this.oldData = null;
            }
        }

        #endregion
    }
}