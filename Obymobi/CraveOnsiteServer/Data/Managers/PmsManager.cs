﻿using System;
using CraveOnsiteServer.Data.Queue;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Interfaces;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;
using Obymobi.Logging;
using Obymobi.Integrations.PMS;
using Obymobi.Integrations.PMS.Tigertms;
using Obymobi.Integrations.PMS.Innsist;
using Obymobi.Integrations.PMS.Exceptions;

namespace CraveOnsiteServer.Data.Managers
{
	public class PmsManager
    {
        #region Fields

	    private static PmsManager sInstance;

        #endregion

        #region Constructors

        private PmsManager()
        {
            
        }

        public static PmsManager Instance
        {
            get { return PmsManager.sInstance ?? (PmsManager.sInstance = new PmsManager()); }
        }
        
        #endregion

        #region Public methods

	    public void Initialize()
	    {
	        try
	        {
                Terminal terminal = DataManager.Instance.TerminalManager.GetTerminal();
	            Global.PmsConnector = this.CreateConnector(terminal);

                if (Global.PmsConnector != null && !CdnManager.Instance.IsProviderAvailable() )
                {
                    throw new Exception("A PMS connection requires a connection to CDN. The connection to CDN isn't setup");
                }
	        }
	        catch (Exception ex)
	        {
                WriteToLog log = new WriteToLog(TerminalLogType.Start, "PmsManager - Initialize - Exception: " + ex.Message);
                DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(WriteToLog), log), true);
                ConsoleLogger.WriteToLog("PMS Exception: {0}", ex.Message);
	            throw ex;
	        }
	    }

        public void SynchronizeWithPms()
        {
            if (Global.PmsConnector == null)
                return;

            if (Global.PmsConnector is TCAPosConnector)
            {
                // Innsist doesn't allow us to fetch all rooms in one go :(
                foreach (Deliverypoint dp in DataManager.Instance.DeliverypointManager.GetDeliverypoints())
                {
                    PmsJob job = new PmsJob();
                    job.JobType = PmsJob.PmsJobType.GetGuestInformation;
                    job.DeliverypointNumber = dp.Number.ToString("D");

                    PmsQueue.Enqueue(job);    
                }
            }
            else
            {
                PmsJob job = new PmsJob();
                job.JobType = PmsJob.PmsJobType.GetGuestInformation;
                job.DeliverypointNumber = "ALL";

                PmsQueue.Enqueue(job);    
            }
        }

        public void SavePmsModel(IPmsModel model, bool forceUpdate = false)
        {
            try
            {
                this.CheckForVipStatus(model);

                string jsonFromDisk;
                if (!PmsManager.Instance.LoadPmsModel(model.DeliverypointNumber, model.Name, out jsonFromDisk))
                {
                    ConsoleLogger.WriteToLog("PmsManager - SavePmsModel - {0} not cached yet for deliverypointNumber {1}", model.Name, model.DeliverypointNumber);
                }

                bool updated = jsonFromDisk.IsNullOrWhiteSpace() || !jsonFromDisk.Equals(model.ToJson(), StringComparison.InvariantCultureIgnoreCase);

                ConsoleLogger.WriteToLog("PmsManager - SavePmsModel - {0} {1} for deliverypointNumber {2}", model.Name, updated ? "modified" : "unmodified", model.DeliverypointNumber);

                if (forceUpdate || updated)
                {
                    CacheManager.Instance.Save(model);
                }
                else if (model is Folio)
                {
                    // Always save folio to the API so a netmessage is send to the client
                    // to make it stop showing the loading dialog.
                    // When the messaging is properly set up with groups and shit we can 
                    // change this so the OSS sends out a message directly. For now 
                    // this will do... (DK - 2019-03-18 @ 23:13)
                    DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(Folio), (Folio)model));
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("PmsManager - SavePmsModel - Exception: " + ex.Message);
            }
        }

        public bool LoadPmsModel(string deliverypointNumber, string modelName, out string json)
        {
            bool success = false;
            string tmpJson = string.Empty;

            try
            {
                success = CacheManager.Instance.Load(deliverypointNumber, modelName, out tmpJson);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("PmsManager - LoadPmsModel - Exception: " + ex.Message);
            }

            json = tmpJson;
            return success;
        }

        public void ClearFolio(string deliverypointNumber)
        {
            Folio model = new Folio();
            model.DeliverypointNumber = deliverypointNumber;

            PmsManager.Instance.SavePmsModel(model);
        }

        #endregion

        #region Private methods

        private PmsConnector CreateConnector(Terminal terminal)
        {
            PMSConnectorType pmsConnectorType = terminal.PMSConnectorType.ToEnum<PMSConnectorType>();

            PmsConnector connector = null;
            switch (pmsConnectorType)
            {
                case PMSConnectorType.Unknown:
                    ConsoleLogger.WriteToLog("No PMS Connector configured.");
                    break;
                case PMSConnectorType.Tigertms:
                    ConsoleLogger.WriteToLog("Initializing TigerTMS PMS connector");
                    TigertmsConfigurationAdapter tigerAdapter = new TigertmsConfigurationAdapter();
                    tigerAdapter.ReadFromTerminalModel(terminal);
                    tigerAdapter.WriteToConfigurationManager();
                    connector = new TigertmsConnector(tigerAdapter, (sender, level, log) => ConsoleLogger.WriteToLog(log, level));
                    break;
                case PMSConnectorType.Innsist:
                    TCAPosConfgurationAdapter tcaposAdapter = new TCAPosConfgurationAdapter();
                    tcaposAdapter.ReadFromTerminalModel(terminal);
                    tcaposAdapter.WriteToConfigurationManager();
                    connector = new TCAPosConnector(tcaposAdapter, (sender, level, log) => ConsoleLogger.WriteToLog(log, level));
                    break;
                default:
                    PmsException.ThrowTyped(PmsError.ConfigurationError, null, "PMS Connector is not yet implemented (in the OSS)");
                    break;
            }
            return connector;
        }

        private void CheckForVipStatus(IPmsModel model)
	    {
            // FO-TEMP: Haxor for Aria hotel, uppercase == vip, lowercase = normal customer

            if (model is GuestInformation)
            {
                GuestInformation guestInformation = (GuestInformation)model;
                if ((Global.CompanyId == 343 || Global.CompanyId == 283) && !guestInformation.Vip)
                {
                    string customerName = string.Concat(guestInformation.CustomerFirstname, guestInformation.CustomerLastname);
                    if (!customerName.IsNullOrWhiteSpace())
                    {
                        bool customerIsVip = customerName.IsUpper();
                        if (guestInformation.Vip != customerIsVip)
                        {
                            ConsoleLogger.WriteToLog("PmsManager - Setting VIP status");
                            guestInformation.Vip = customerIsVip;
                        }
                    }
                }
            }            
	    }

	    public void CheckForFolioUpdate(GuestInformation guestInformation)
	    {
	        if (guestInformation.Occupied && guestInformation.AllowViewFolio)
	        {
                ConsoleLogger.WriteToLog("PmsManager - CheckForFolioUpdate - Requesting folio for deliverypointNumber {0}", guestInformation.DeliverypointNumber);

                PmsJob getFolioJob = new PmsJob();
                getFolioJob.JobType = PmsJob.PmsJobType.GetFolio;
                getFolioJob.DeliverypointNumber = guestInformation.DeliverypointNumber;

                PmsQueue.Enqueue(getFolioJob);
	        }
	        else
	        {
                PmsManager.Instance.ClearFolio(guestInformation.DeliverypointNumber);
	        }
	    }

	    #endregion
    }
}
