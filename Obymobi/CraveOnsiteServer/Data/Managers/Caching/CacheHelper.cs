﻿using Dionysos.Security.Cryptography;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;
using Obymobi.Logging;

namespace CraveOnsiteServer.Data.Managers.Caching
{
    public static class CacheHelper
    {
        public static string Encrypt(string text, string salt)
        {
            string encryptedText = string.Empty;
            try
            {
                if (!salt.IsNullOrWhiteSpace())
                {
                    encryptedText = Cryptographer.EncryptUsingCBC(text, salt);
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("CacheHelper - Encrypt - Error: {0}", ex.Message);
            }
            return encryptedText;
        }

        public static string Decrypt(string text, string salt)
        {
            string decryptedText = string.Empty;
            try
            {
                if (!salt.IsNullOrWhiteSpace())
                {
                    decryptedText = Cryptographer.DecryptUsingCBC(text, salt);
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("CacheHelper - Decrypt - Error: {0}", ex.Message);
            }
            return decryptedText;
        }
    }
}
