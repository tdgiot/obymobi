﻿using CraveOnsiteServer.Data.Managers.Caching;
using CraveOnsiteServer.Logic.Interfaces;
using Obymobi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;
using Obymobi.Logging;

namespace CraveOnsiteServer.Data.Managers
{
    public class CacheManager
    {
        #region Fields

        public const string JSON_FILENAME_FORMAT = "{0}-{1}.json";
        public const bool ENCRYPT = true;

        private static CacheManager sInstance { get; set; }
        
        public string Salt { get; set; }
        private List<ICacheManager> managers { get; set; }

        #endregion

        #region Constructors

        private CacheManager(string salt)
        {
            this.Salt = salt;
            this.managers = new List<ICacheManager>();
            this.managers.Add(new DiskCacheManager());
            this.managers.Add(new CdnCacheManager());
            this.managers.Add(new ApiCacheManager());
        }

        public static CacheManager Instance
        {
            get { return CacheManager.sInstance ?? (CacheManager.sInstance = new CacheManager(Global.SaltPms)); }
        }

        #endregion

        #region Methods        

        public void Save(IPmsModel model)
        {
            try
            {
                string filename = string.Format(CacheManager.JSON_FILENAME_FORMAT, model.Name.ToLowerInvariant(), model.DeliverypointNumber);
                
                foreach (ICacheManager manager in this.managers)
                {
                    manager.Save(filename, model);
                }                                
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("CacheManager - Exception: " + ex.Message);
            }
        }

        public bool Load(string deliverypointNumber, string modelName, out string json)
        {
            bool success = false;
            string tmpJson = string.Empty;

            DiskCacheManager manager = (DiskCacheManager)this.managers.SingleOrDefault(x => x is DiskCacheManager);
            if (manager != null)
            {
                string text;
                if (manager.Load(deliverypointNumber, modelName, out text))
                {
                    tmpJson = CacheHelper.Decrypt(text, this.Salt);
                    success = true;
                }
            }

            json = tmpJson;
            return success;
        }

        #endregion
    }
}
