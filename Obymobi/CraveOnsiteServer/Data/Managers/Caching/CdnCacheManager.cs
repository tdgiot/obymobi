﻿using CraveOnsiteServer.CraveService;
using CraveOnsiteServer.Data.Managers.Caching;
using CraveOnsiteServer.Logic.Interfaces;
using Obymobi;
using Obymobi.Constants;
using Obymobi.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;

namespace CraveOnsiteServer.Data.Managers
{
    public class CdnCacheManager : ICacheManager
    {
        #region Fields

        private const string CDN_PMS_DIR = "pms";

        private string container { get; set; }

        #endregion

        #region Constructors

        public CdnCacheManager()
        {            
            this.container = this.GetCompanyContainer();
        }

        #endregion

        #region Public methods

        public void Save(string filename, IPmsModel model)
        {
            string encryptedJson = CacheHelper.Encrypt(model.ToJson(), CacheManager.Instance.Salt);
            string key = string.Format("{0}/{1}", CdnCacheManager.CDN_PMS_DIR, filename);
            CdnManager.Instance.UploadAsync(this.container, key, encryptedJson, true);
        }

        #endregion

        #region Private methods

        private string GetCompanyContainer()
        {
            if (Global.CdnRootContainer.IsNullOrWhiteSpace() && Global.Status.CloudEnvironment == Obymobi.Enums.CloudEnvironment.Manual)
                throw new Exception("Global.CdnRootContainer is empty");

            string environmentPrefix = Global.CdnRootContainer.Replace("cravecloud-", "");
            if (Global.Status.CloudEnvironment != Obymobi.Enums.CloudEnvironment.Manual)
            {
                environmentPrefix = WebEnvironmentHelper.GetEnvironmentPrefixForCdn(Global.Status.CloudEnvironment);
            }
            return string.Format(ObymobiConstants.AmazonS3AwsCompanyContainer, environmentPrefix, Global.CompanyId);
        }

        #endregion
    }
}