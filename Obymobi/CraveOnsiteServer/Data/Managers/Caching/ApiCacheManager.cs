﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq.Expressions;
using CraveOnsiteServer.Data.Queue;
using CraveOnsiteServer.Logic.Interfaces;
using Dionysos;
using Obymobi.Interfaces;
using Obymobi.Logic.Model;
using Obymobi.Logging;

namespace CraveOnsiteServer.Data.Managers
{
    public class ApiCacheManager : ICacheManager
    {
        #region Fields

        private const int LOOPER_INTERVAL = 30000; // 30 seconds

        private readonly Queue<GuestInformation> guestInformations = new Queue<GuestInformation>();
        private readonly Queue<Folio> folios = new Queue<Folio>();

        private readonly object guestInformationsLocker = new object();
        private readonly object foliosLocker = new object();

        private readonly InfiniteLooper uploadLooper;

        #endregion

        #region Constructors

        public ApiCacheManager()
        {
            this.uploadLooper = new InfiniteLooper(() =>
                                                   {
                                                       this.UploadGuestInformations();                                                       
                                                       this.UploadFolios();
                                                   }, ApiCacheManager.LOOPER_INTERVAL, -1, OnExceptionHandler);

            this.StartUploadLooper();
        }

        #endregion

        #region Private methods

        private void UploadGuestInformations()
        {
            List<GuestInformation> guestInformationsToUpload = new List<GuestInformation>();

            lock (this.guestInformationsLocker)
            {
                while (this.guestInformations.Count > 0)
                {
                    guestInformationsToUpload.Add(this.guestInformations.Dequeue());
                }
            }

            if (guestInformationsToUpload.Count > 0)
            {
                ConsoleLogger.WriteToLog("ApiCacheManager - UploadGuestInformations - Uploading {0} guest informations", guestInformationsToUpload.Count);
                DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(GuestInformation[]), guestInformationsToUpload.ToArray()));
            }
        }

        private void UploadFolios()
        {
            List<Folio> foliosToUpload = new List<Folio>();

            lock(this.foliosLocker)
            {
                while (this.folios.Count > 0)
                {
                    foliosToUpload.Add(this.folios.Dequeue());
                }
            }

            if (foliosToUpload.Count > 0)
            {
                ConsoleLogger.WriteToLog("ApiCacheManager - UploadFolios - Uploading {0} folios", foliosToUpload.Count);
                DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(Folio[]), foliosToUpload.ToArray()));
            }
        }

        private void OnExceptionHandler(Exception exception)
        {
            ConsoleLogger.WriteToLog("ApiCacheManager - OnExceptionHandler - Exception: '{0}'.", exception.Message);            

            this.StopUploadLooper();
            this.StartUploadLooper();
        }

        private void StartUploadLooper()
        {
            if (this.uploadLooper != null && !this.uploadLooper.IsRunning)
            {
                ConsoleLogger.WriteToLog("ApiCacheManager - StartUploadLooper - Starting uploadLooper");
                this.uploadLooper.Start();
            }
        }

        private void StopUploadLooper()
        {
            if (this.uploadLooper != null && this.uploadLooper.IsRunning)
            {
                ConsoleLogger.WriteToLog("ApiCacheManager - StopUploadLooper - Stopping uploadLooper");
                this.uploadLooper.Stop();
            }
        }

        #endregion

        #region Public methods

        public void Save(string filename, IPmsModel model)
        {
            if (model is GuestInformation)
            {
                lock(this.guestInformationsLocker)
                {
                    this.guestInformations.Enqueue((GuestInformation)model);
                }                
            }
            else if (model is Folio)
            {
                lock(this.foliosLocker)
                {
                    this.folios.Enqueue((Folio)model);
                }                
            }
        }

        public bool Load(string deliverypointNumber, string modelName, out string json)
        {
            json = string.Empty;
            return false;
        }

        #endregion                
    }
}
