﻿using CraveOnsiteServer.Logic.Interfaces;
using System.IO;
using CraveOnsiteServer.Data.Managers.Caching;
using Obymobi.Interfaces;

namespace CraveOnsiteServer.Data.Managers
{
    public class DiskCacheManager : ICacheManager
    {
        #region Fields
        
        private const string LOCAL_CACHE_DIR = "Data\\Cache\\";

        private readonly string localCachePath = string.Empty;

        #endregion

        #region Constructors

        public DiskCacheManager()
        {
            this.localCachePath = DataManager.CreateDirectory(DiskCacheManager.LOCAL_CACHE_DIR);
        }

        #endregion

        #region Public methods

        public void Save(string filename, IPmsModel model)
        {
            string filepath = Path.Combine(this.localCachePath, filename);
            string encryptedJson = CacheHelper.Encrypt(model.ToJson(), CacheManager.Instance.Salt);
            File.WriteAllText(filepath, encryptedJson);
        }

        public bool Load(string deliverypointNumber, string modelName, out string json)
        {
            string filename = string.Format(CacheManager.JSON_FILENAME_FORMAT, modelName.ToLowerInvariant(), deliverypointNumber);
            string filepath = Path.Combine(this.localCachePath, filename);

            if (!File.Exists(filepath))
            {
                json = string.Empty;
                return false;                
            }
            else
            {
                json = File.ReadAllText(filepath);
                return true;
            }
        }

        #endregion                
    }
}
