﻿using System;
using System.IO;
using CraveOnsiteServer.Logic.Enum;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Logging;

namespace CraveOnsiteServer.Data.Managers
{
	public class CompanyManager : IManager
	{
        private const string FILENAME = "GetCompany.data";
		private Company company;
	    private Company oldData;

		public CompanyManager()
		{
		    this.company = null;
		}

		public bool Load()
		{
			bool ret = false;

            ConsoleLogger.WriteToLog(" * Loading Company");

            if (Global.Status.IsWebserviceAvailable)
				ret = this.LoadFromWebservice();
			
            if (!Global.Status.IsWebserviceAvailable || !ret)
				ret = this.LoadFromDisk();

            if (ret)
            {                
                DataManager.Instance.DeliverypointManager.SetDeliverypointGroups(this.company.Deliverypointgroups);                
            }

			return ret;
		}

        public LoadResult Reload()
        {
            this.oldData = this.company;

            LoadResult toReturn = LoadResult.ERROR;
            if (this.LoadFromWebservice())
            {
                toReturn = LoadResult.NEW_DATA;

                // Compare data
                if(DataManager.PublicPropertiesEqual(this.oldData, this.company))
                {
                    toReturn = LoadResult.LOADED;
                    this.oldData = null;
                }
            }

    	    return toReturn;
        }

        public void ReloadRevert()
        {
            if (this.oldData != null)
            {
                this.company = this.oldData;
                this.oldData = null;
            }
        }

        public bool LoadFromWebservice()
		{
			bool ret = false;
            this.company = WebserviceManager.GetCompany();

			if (this.company != null)
			{
                string companySerialized = XmlHelper.Serialize(this.company);
				DataManager.Instance.WriteToFile(CompanyManager.FILENAME, companySerialized);

				ret = true;
			}

			return ret;
		}

        public bool LoadFromDisk()
		{
			bool ret = false;
			string path = Path.Combine(Global.RootDirectory, "data/" + CompanyManager.FILENAME);
			if (File.Exists(path))
			{
				try
				{
					string data = File.ReadAllText(path);
				    this.company = XmlHelper.Deserialize<Company>(data);
					ret = true;
				}
				catch (Exception ex)
				{
					ConsoleLogger.WriteToLog("ERROR: Exception while deserializing cached GetCompany data. Exception: {0}", ex.Message);
				}
			}
			else
			{
				ConsoleLogger.WriteToLog("ERROR: Loading Company info from Disk. File not found: {0}", path);
			}

			return ret;
		}

        public Company Company
        {
            get
            {
                return this.company;
            }
        }
	}
}
