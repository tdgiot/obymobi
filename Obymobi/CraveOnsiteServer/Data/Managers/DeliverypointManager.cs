﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CraveOnsiteServer.Logic.Enum;
using Dionysos;
using Obymobi.Configuration;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Logging;

namespace CraveOnsiteServer.Data.Managers
{
	public class DeliverypointManager : IManager
	{
	    private const string FILENAME = "GetDeliverypoints-{0}.data";

	    private readonly List<Deliverypointgroup> deliverypointgroups;
	    private readonly Dictionary<int, List<Deliverypoint>> deliverypointCollection;

		public DeliverypointManager()
		{
            this.deliverypointgroups = new List<Deliverypointgroup>();
            this.deliverypointCollection = new Dictionary<int, List<Deliverypoint>>();
		}

		public IEnumerable<Deliverypoint> GetDeliverypoints()
		{
		    List<Deliverypoint> deliverypoints = new List<Deliverypoint>();
		    foreach (var deliverypointList in this.deliverypointCollection.Values)
		    {
		        deliverypoints.AddRange(deliverypointList);
		    }

            return deliverypoints.OrderBy(d => d.Number);
		}

        public void SetDeliverypointGroups(Deliverypointgroup[] groups)
        {
            if (groups != null && groups.Length > 0)
            {
                this.deliverypointgroups.Clear();
                this.deliverypointgroups.AddRange(groups);

                foreach (Deliverypointgroup deliverypointgroup in this.deliverypointgroups)
                {
                    if (!this.deliverypointCollection.ContainsKey(deliverypointgroup.DeliverypointgroupId))
                    {
                        this.deliverypointCollection.Add(deliverypointgroup.DeliverypointgroupId, new List<Deliverypoint>());
                    }
                }
            }
        }

	    public bool Load()
	    {
	        bool ret = true;

	        foreach (Deliverypointgroup deliverypointgroup in deliverypointgroups)
	        {
	            if (Global.Status.IsWebserviceAvailable)
	            {
	                ret = LoadDeliverypointsFromWebservice(deliverypointgroup.DeliverypointgroupId);
	            }
	            else
	            {
	                ret = LoadDeliverypointsFromDisk(deliverypointgroup.DeliverypointgroupId);
	            }

                ConsoleLogger.WriteToLog("    - DPG: {0} ({1}). Deliverypoints loaded: {2}", deliverypointgroup.Name, deliverypointgroup.DeliverypointgroupId, this.deliverypointCollection[deliverypointgroup.DeliverypointgroupId].Count);    
	        }

			return ret;
		}

        public LoadResult Reload()
        {
            LoadResult toReturn = LoadResult.LOADED;

            foreach (Deliverypointgroup deliverypointgroup in deliverypointgroups)
            {
                List<Deliverypoint> oldData;

                if (!this.deliverypointCollection.TryGetValue(deliverypointgroup.DeliverypointgroupId, out oldData))
                {
                    continue;
                }
                
                if (LoadDeliverypointsFromWebservice(deliverypointgroup.DeliverypointgroupId))
                {
                    List<Deliverypoint> deliverypoints = this.deliverypointCollection[deliverypointgroup.DeliverypointgroupId];

                    toReturn = LoadResult.NEW_DATA;
                    if (oldData.Count == deliverypoints.Count)
                    {
                        toReturn = LoadResult.LOADED;
                        for (int i = 0; i < oldData.Count; i++)
                        {
                            if (!DataManager.PublicPropertiesEqual(oldData[i], deliverypoints[i]))
                            {
                                toReturn = LoadResult.NEW_DATA;
                                break;
                            }
                        }
                    }
                }
            }

            return toReturn;
        }

        public void ReloadRevert()
        {
        }

        public bool LoadDeliverypointsFromWebservice(int deliverypointgroupId)
		{
			bool ret = false;
            Deliverypoint[] dpResult = WebserviceManager.GetDeliverypoints(deliverypointgroupId);

			if (dpResult != null)
			{
				this.deliverypointCollection[deliverypointgroupId] = new List<Deliverypoint>(dpResult);

                string deliverypointSerialized = XmlHelper.Serialize(this.deliverypointCollection[deliverypointgroupId]);
				DataManager.Instance.WriteToFile(string.Format(FILENAME, deliverypointgroupId), deliverypointSerialized);

				ret = true;
			}

			return ret;
		}

        public bool LoadDeliverypointsFromDisk(int deliverypointgroupId)
		{
			bool ret = false;
            string path = Path.Combine(Global.RootDirectory, "data/" + string.Format(FILENAME, deliverypointgroupId));
			if (File.Exists(path))
			{
				try
				{
					string data = File.ReadAllText(path);
                    var dpObject = XmlHelper.Deserialize<Deliverypoint[]>(data);

                    this.deliverypointCollection[deliverypointgroupId] = new List<Deliverypoint>(dpObject);
					
					ret = true;
				}
				catch (Exception ex)
				{
					ConsoleLogger.WriteToLog("ERROR: Exception while deserializing cached GetDeliverypoint data. Exception: {0}", ex.Message);
				}
			}
			else
			{
				ConsoleLogger.WriteToLog("ERROR: Loading Deliverypoint info from Disk. File not found: {0}", path);
			}

			return ret;
		}
	}
}
