﻿using System;
using System.Collections.Generic;
using System.IO;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Logic.Routing;
using Obymobi.Logging;

namespace CraveOnsiteServer.Data.Managers
{
    public class OrderManager
    {
        private static readonly object _sAutoIdLock = new object();
        private static readonly object _sOrderLock = new object();
        private static UInt32 _mCount = 1;

        private readonly SortedDictionary<UInt32, Order> orders = new SortedDictionary<UInt32, Order>();
        private readonly string directory = DataManager.CreateDirectory("Data\\Orders");

        public Order GetOrderByOrderRoutestephandlerGuid(string guid)
        {
            Order ret = null;
            lock (_sOrderLock)
            {
                if (this.orders != null && this.orders.Count > 0)
                {
                    foreach (var item in this.orders)
                    {
                        if (item.Value != null)
                        {
                            Order order = item.Value;
                            if (order.OrderRoutestephandler != null && order.OrderRoutestephandler.Guid.Equals(guid))
                            {
                                ret = order;
                                break;
                            }
                        }
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// Set the status of an orderroutestephandler to Completed
        /// </summary>
		/// <param name="orderroutestephandlerGuid">The GUID of the order which the status need to be set to Processed</param>
        /// <param name="isProcessed">Set to true if an order status update needs to be send to the webservice. Default is false.</param>
        public void SetOrderRoutestephandlerCompleted(string orderroutestephandlerGuid, bool isProcessed = false)
        {
            OrderRoutestephandlerSaveStatus result = null;
            lock (_sOrderLock)
            {
                Order order = GetOrderByOrderRoutestephandlerGuid(orderroutestephandlerGuid);
                if (order != null && order.OrderRoutestephandler.Status != (int)OrderRoutestephandlerStatus.Completed)
                {
                    order.OrderRoutestephandler.Status = (int)OrderRoutestephandlerStatus.Completed;
                    order.OrderRoutestephandler.Error = (int)OrderProcessingError.ManuallyProcessed;
                    SaveOrderToFile(order);

                    result = OrderRoutestephandlerHelper.CreateRoutestephandlerSaveStatusModelFromOrderModel(order);
                }
            }

            if (result != null && !isProcessed)
                DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(OrderRoutestephandlerSaveStatus), result));
        }

        public bool HasOrderOrderRoutestepHandlerGuid(string guid)
        {
            bool ret = false;
            lock (_sOrderLock)
            {
                if (this.orders != null && this.orders.Count > 0)
                {
                    foreach (var item in this.orders)
                    {
                        if (item.Value != null)
                        {
                            Order order = item.Value;
                            if (order.OrderRoutestephandler != null && order.OrderRoutestephandler.Guid.Equals(guid))
                            {
                                ret = true;
                                break;
                            }
                        }
                    }
                }
            }
            return ret;
        }

        public bool HasOrderWithOrderRoutestepHandlerId(int orderRoutestephandlerId)
        {
            bool ret = false;
            lock (_sOrderLock)
            {
                if (this.orders != null && this.orders.Count > 0)
                {
                    foreach (var item in this.orders)
                    {
                        if (item.Value != null)
                        {
                            Order order = item.Value;
                            if (order.OrderRoutestephandler != null && order.OrderRoutestephandler.OrderRoutestephandlerId.Equals(orderRoutestephandlerId))
                            {
                                ret = true;
                                break;
                            }
                        }
                    }
                }
            }
            return ret;
        }

		public bool HasOrderWithOrderGuid(string guid)
		{
            bool ret = false;
            lock (_sOrderLock)
            {
                if (this.orders != null && this.orders.Count > 0)
                {
                    foreach (var item in this.orders)
                    {
                        if (item.Value != null)
                        {
                            Order order = item.Value;
                            if (order.OrderRoutestephandler != null && order.Guid.Equals(guid))
                            {
                                ret = true;
                                break;
                            }
                        }
                    }
                }
            }
            return ret;
		}

        public void AddOrder(Order order)
        {
            lock (_sOrderLock)
            {
                order.AutoId = GetUniqueOrderId();
                order.CreatedOnServer = DateTime.Now;

                this.orders.Add(order.AutoId, order);

                ConsoleLogger.WriteToLog("OrderManager - AddOrder - Id: {0} - Guid: {1}", order.AutoId, order.Guid);

                SaveOrderToFile(order);
            }
        }

        public void UpdateOrder(Order order)
        {
            lock (_sOrderLock)
            {
                Order orderGet;
                if (this.orders.TryGetValue(order.AutoId, out orderGet)) // GK-ROUTING: Will AutoId keep working?
                {
                    this.orders[order.AutoId] = order;
                    SaveOrderToFile(order);
                }
                else
                {
                    AddOrder(order);
                }
            }
        }

        public void SetOrderSavedOnWebservice(Order order)
        {
            lock (_sOrderLock)
            {
                Order orderGet;
                if (this.orders.TryGetValue(order.AutoId, out orderGet)) // GK-ROUTING: Will AutoId keep working?
                {
                    orderGet.IsSavedOnWebservice = order.IsSavedOnWebservice;
                    SaveOrderToFile(orderGet);
                }
                else
                {
                    ConsoleLogger.WriteToLog("OrderManager - SetOrderSavedOnWebservice - Can't find order.. WTF?");
                }
            }
        }

        public List<Order> GetUnhandledOrders()
        {
            List<Order> list = new List<Order>();
            lock (_sOrderLock)
            {
                if (this.orders != null && this.orders.Count > 0)
                {
                    foreach (var item in this.orders)
                    {
                        if (item.Value != null)
                        {
                            Order order = item.Value;
                            if (order.OrderRoutestephandler != null && !order.OrderRoutestephandler.IsFinished())
                                list.Add(order);
                        }
                    }
                }
            }

            return list;
        }

        public List<Order> GetUnprocessableOrders()
        {
            var list = new List<Order>();
            lock (_sOrderLock)
            {
                if (this.orders != null && this.orders.Count > 0)
                {
                    foreach (var item in this.orders)
                    {
                        if (item.Value != null)
                        {
                            Order order = item.Value;
                            if (order.OrderRoutestephandler != null && order.OrderRoutestephandler.IsFailed())
                                list.Add(order);
                        }
                    }
                }
            }

            return list;
        }

        private void SaveOrderToFile(Order order)
        {
            try
            {
                var orderSerialized = XmlHelper.Serialize(order);
                string filePath = Path.Combine(directory, order.OrderRoutestephandler.Guid + ".order");
                File.WriteAllText(filePath, orderSerialized);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("OrderManager::SaveOrderToFile - {0}", ex.Message);
            }
        }

        private UInt32 GetUniqueOrderId()
        {
            lock (_sAutoIdLock)
            {
                _mCount++;

                if (_mCount >= UInt32.MaxValue)
                {
                    _mCount = 1;
                }

                while (this.orders.ContainsKey(_mCount))
                {
                    if (_mCount >= UInt32.MaxValue)
                    {
                        _mCount = 1;
                    }
                    else
                    {
                        _mCount++;
                    }
                }
            }

            return _mCount;
        }
    }
}