﻿using System;
using CraveOnsiteServer.Server;
using Obymobi.Enums;
using Obymobi.Integrations.Interfaces;
using Obymobi.Integrations.POS.Agilysys;
using Obymobi.Integrations.POS.Interfaces;
using Obymobi.Integrations.POS.Simphony;
using Obymobi.Logging;
using Obymobi.Logic.Model;

namespace CraveOnsiteServer.Data.Managers
{
    public class PosManager
    {
        #region Fields

        private static PosManager sInstance;

        #endregion

        #region Constructors

        public static PosManager Instance => sInstance ?? (sInstance = new PosManager());

        #endregion

        #region Methods

        public void Initialize()
        {
            try
            {
                PosIntegrationInformation posInfo = DataManager.Instance.PosIntegrationInformationManager.GetPosIntegrationInformation();
                Global.PosConnector = CreateConnector(posInfo.Terminal);

                new OnsiteOrderConverter(posInfo);

                // Synchronize with POS
                if (DataManager.Instance.TerminalManager.SynchronizeWithPos())
                {
                    ConsoleLogger.WriteToLog("Synchronizing with POS...");
                    OnSiteServerPosHelper.SynchronizeWithPos();
                }
            }
            catch (Exception ex)
            {
                WriteToLog log = new WriteToLog(TerminalLogType.Start, "PosManager - Initialize - Exception: " + ex.Message);
                DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(WriteToLog), log), true);
                ConsoleLogger.WriteToLog("POS Exception: {0}", ex.Message);
                throw ex;
            }
        }

        private IPOSConnector CreateConnector(Terminal terminal)
        {
            POSConnectorType posConnectorType = terminal.POSConnectorType.ToEnum<POSConnectorType>();

            IConfigurationAdapter configAdapter;
            IPOSConnector connector = null;
            switch (posConnectorType)
            {
                case POSConnectorType.Unknown:
                    ConsoleLogger.WriteToLog("Unkown POS connector");
                    break;
                case POSConnectorType.Agilysys:
                    ConsoleLogger.WriteToLog("Initializing Agilysys POS connector");
                    configAdapter = new AgilysysConfigurationAdapter();
                    configAdapter.ReadFromTerminalModel(terminal);
                    configAdapter.WriteToConfigurationManager();
                    connector = new AgilysysConnector();
                    break;
                case POSConnectorType.SimphonyGen1:
                    ConsoleLogger.WriteToLog("Initializing Simphony POS connector");
                    configAdapter = new SimphonyConfigurationAdapter();
                    configAdapter.ReadFromTerminalModel(terminal);
                    configAdapter.WriteToConfigurationManager();
                    connector = new SimphonyConnector((SimphonyConfigurationAdapter)configAdapter);
                    break;
            }
            return connector;
        }

        #endregion
    }
}
