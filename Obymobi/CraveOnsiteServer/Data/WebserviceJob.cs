﻿using System;
using System.Runtime.Serialization;

namespace CraveOnsiteServer.Data
{
    [Serializable]
    public class WebserviceJob : ISerializable
	{
        /// <summary>
        /// Type of job
        /// </summary>
		public Type JobType { get; private set; }

        /// <summary>
        /// Data to send
        /// </summary>
		public object Data { get; private set; }

        /// <summary>
        /// Retry count
        /// </summary>
		public int NumberOfRetry { get; set; }

        /// <summary>
        /// How many times a job can be reposted in the queue
        /// Default is -1 where it will stay in the queue forever (alone)
        /// </summary>
        public int NumberOfMaxRetry { get; set; }

        /// <summary>
        /// Number of miliseconds before job gets posted back in queue (default is 30 seconds)
        /// -1 if job can be removed if failed
        /// </summary>
        public int RetryDelay { get; set; }

        /// <summary>
        /// Global Unique Identifier of this webservice job
        /// </summary>
        public long Guid { get; set; }

        /// <summary>
        /// Method which will be called when webservice call is succesful
        /// </summary>
        public Action<object> ResultMethod { get; set; }

		public WebserviceJob(Type type, object data)
		{
			this.JobType = type;
			this.Data = data;
			this.NumberOfRetry = 1;
		    this.NumberOfMaxRetry = -1;
		    this.RetryDelay = (30 * 1000);
		    this.Guid = DateTime.Now.Ticks;
		    this.ResultMethod = null;
		}

        public WebserviceJob(SerializationInfo info, StreamingContext context)
        {
            this.Data = info.GetValue("Data", typeof(object));
            this.JobType = Type.GetType((string)info.GetValue("JobType", typeof(string)));
            this.NumberOfRetry = (int)info.GetValue("NumberOfRetry", typeof(int));
            this.RetryDelay = (int)info.GetValue("RetryDelay", typeof(int));
            this.Guid = (long)info.GetValue("Guid", typeof(long));
            this.ResultMethod = (Action<object>)info.GetValue("ResultMethod", typeof(Action<object>));
        }

	    public void GetObjectData(SerializationInfo info, StreamingContext context)
	    {
	        info.AddValue("Data", this.Data);
            info.AddValue("JobType", this.JobType.AssemblyQualifiedName);
            info.AddValue("NumberOfRetry", this.NumberOfRetry);
            info.AddValue("RetryDelay", this.RetryDelay);
            info.AddValue("Guid", this.Guid);
            info.AddValue("ResultMethod", this.ResultMethod);
	    }        
	}
}
