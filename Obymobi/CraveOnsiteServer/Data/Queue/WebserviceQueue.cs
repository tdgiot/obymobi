﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CraveOnsiteServer.Logic;
using Dionysos;
using Obymobi.Logic.Model;
using Obymobi.Enums;
using Obymobi.Logging;

namespace CraveOnsiteServer.Data.Queue
{
    public class WebserviceQueue
    {
        #region Fields

        private readonly string serializeDirectory;
        private readonly string serializeDirectoryFailed;

        private Queue<WebserviceJob> queue = new Queue<WebserviceJob>();
        private readonly List<WebserviceJob> delayedTasks = new List<WebserviceJob>();
        private readonly object queueLocker = new object();

        private Thread consumerThread;

        private bool isRunning = true;

        #endregion

        #region Constructor

        public WebserviceQueue()
        {
            consumerThread = null;
            DataManager.CreateDirectory("Data");
            serializeDirectory = DataManager.CreateDirectory("Data\\Queue");
            serializeDirectoryFailed = DataManager.CreateDirectory("Data\\Queue\\Failed");
            LoadQueueFromFile();
        }

        #endregion

        #region Serializing

        private void SaveQueueItemToFile(WebserviceJob item)
        {
            if (item != null)
            {
                string filename = Path.Combine(serializeDirectory, item.Guid + ".job");
                string str = JsonHelper.Serialize(item);
                File.WriteAllText(filename, str);
            }
        }

        private void RemoveSavedQueueItem(WebserviceJob item)
        {
            if (item != null)
            {
                string fileName = Path.Combine(serializeDirectory, item.Guid + ".job");
                if (File.Exists(fileName))
                    File.Delete(fileName);
            }
        }

        private void LoadQueueFromFile()
        {
            if (this.queue == null)
            {
                this.queue = new Queue<WebserviceJob>();
            }

            string[] files = Directory.GetFiles(serializeDirectory, "*.job");
            foreach (var fileStr in files)
            {
                try
                {
                    var fileSerialized = File.ReadAllText(fileStr);
                    var job = JsonHelper.Deserialize<WebserviceJob>(fileSerialized);

                    if (job != null)
                        this.queue.Enqueue(job);
                    else
                        File.Delete(fileStr);
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteToLog("WebserviceQueue::LoadQueueFromFile - {0} - {1}", fileStr, ex.Message);
                }
            }
        }

        #endregion

        #region Public Methods

        public void StartConsumer()
        {
            LoadQueueFromFile();

            isRunning = true;

            if (consumerThread == null)
            {
                consumerThread = new Thread(this.ConsumeItems) { IsBackground = true };
                consumerThread.Start();
            }
        }

        public void StopConsumer()
        {
            if (consumerThread != null)
            {
                // Add any delayed tasks to queue
                foreach (WebserviceJob webserviceJob in delayedTasks)
                {
                    queue.Enqueue(webserviceJob);
                }

                Enqueue(null);

                isRunning = false;

                ConsoleLogger.WriteToLog("Waiting for Webservice queue to stop. Could take a minute.");
                consumerThread.Join(new TimeSpan(0, 1, 0));
                consumerThread = null;
            }
        }

        public void EnqueueDelayed(WebserviceJob item, int delay)
        {
            this.delayedTasks.Add(item);

            Task.Factory.StartNew(() =>
            {
                Thread.Sleep(delay);
                lock(this.queueLocker)
                {
                    this.delayedTasks.Remove(item);
                }
                Enqueue(item);
            });
        }        

        public void Enqueue(WebserviceJob item)
        {
            this.Enqueue(item, false);
        }

        public void Enqueue(WebserviceJob item, bool validateWebservice)
        {
            if (!isRunning)
            {
                // Queue not dequeueing
            }
            else if (validateWebservice && !Global.Status.IsWebserviceAvailable)
            {
                // Webservice not available
            }
            else
            {
                lock (this.queueLocker)
                {
                    this.queue.Enqueue(item);
                    
                    // After enqueuing the item, signal the consumer thread.
                    Monitor.PulseAll(this.queueLocker);
                }    
            }
        }

        public void ManualProcessAfterNoInternet()
        {
            lock (this.queueLocker)
            {
                // After enqueuing the item, signal the consumer thread.
                Monitor.PulseAll(this.queueLocker);
            }
        }

        #endregion

        #region Private Methods

        private void ConsumeItems()
        {
            while (true)
            {
                // Check if webservice is online
                if (!Global.Status.IsWebserviceAvailable)
                {
                    Thread.Sleep(1000);
                    continue;
                }

                try
                {
                    lock (this.queueLocker)
                    {
                        while (this.queue.Count == 0)
                        {
                            // ...otherwise, wait for manual process
                            Monitor.Wait(this.queueLocker);
                        }

                        if (Global.Status.IsWebserviceAvailable)
                        {
                            bool doesItemExist = (this.queue.Count > 0);
                            if (doesItemExist)
                            {
                                WebserviceJob nextItem = this.queue.Dequeue();
                                if (nextItem == null)
                                    break;

                                RemoveSavedQueueItem(nextItem);

                                // If there was an item in the queue, process it
                                ProcessItem(nextItem);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    ConsoleLogger.WriteToLog("WebserviceQueue - Uncaught exception: {0}\n{1}", ex.Message, ex.ProcessStackTrace());
                }
            }
        }

        private void ProcessItem(WebserviceJob job)
        {
            if (job == null)
                return;

            ConsoleLogger.WriteToLog("WebserviceQueue - Processing job of type '{0}'", job.JobType.ToString());

            // GK-ROUTING: After merge: Extract methods for each case.
            bool result = true;
            bool showError = true;
            if (job.JobType == typeof(Order))
            {
                var order = job.Data as Order;
                if (order != null)
                {
                    // Get original order from memory to check if it's already saved on webservice
                    // This is cheaper than looping over all delayed tasks
                    var originalOrder = DataManager.Instance.OrderManager.GetOrderByOrderRoutestephandlerGuid(order.OrderRoutestephandler.Guid);
                    if (originalOrder != null && !originalOrder.IsSavedOnWebservice)
                    {
                        ConsoleLogger.WriteToLog("WebserviceQueue - Order.Guid: '{0}'", order.Guid);
                        result = WebserviceQueueHandler.HandleSaveOrder(order);

                        if (result)
                        {
                            // Update order saved property so it's known the order saved on the webservice
                            order.IsSavedOnWebservice = true;
                            DataManager.Instance.OrderManager.SetOrderSavedOnWebservice(order);
                        }
                        else
                        {
                            ConsoleLogger.WriteToLog("WebserviceQueue - Failed to save order to webservice.");
                        }
                    }
                    else
                    {
                        ConsoleLogger.WriteToLog("WebserviceQueue - Order.Guid: '{0}' - Already marked as saved on webservice", order.Guid);
                    }
                }
                else
                    ConsoleLogger.WriteToLog("WebserviceQueue - WebserviceJob.Data could not be casted to type 'Order'!");
            }
            else if (job.JobType == typeof(OrderRoutestephandlerSaveStatus))
            {
                var routestephandlerSaveStatus = job.Data as OrderRoutestephandlerSaveStatus;
                if (routestephandlerSaveStatus != null)
                {
                    ConsoleLogger.WriteToLog("WebserviceQueue - OrderRouteStepHandlerGuid: '{0}', Status: '{1}'",
                        routestephandlerSaveStatus.OrderRoutestephandlerGuid, routestephandlerSaveStatus.Status.ToEnum<OrderRoutestephandlerStatus>());

                    // Get order
                    Order order = DataManager.Instance.OrderManager.GetOrderByOrderRoutestephandlerGuid(routestephandlerSaveStatus.OrderRoutestephandlerGuid);
                    if ((order == null && routestephandlerSaveStatus.Error == (int)OrderProcessingError.NoTableForDeliverypointAtOss) ||
                        (order != null && order.IsSavedOnWebservice))
                    {
                        job.NumberOfMaxRetry = 3;
                        result = WebserviceQueueHandler.HandleSetRoutestephandlerSaveStatuses(routestephandlerSaveStatus);                        
                    }
                    else if (order != null)
                    {
                        ConsoleLogger.WriteToLog("WebserviceQueue - Can't upate order status. Order is not yet processed by the webservice.");
                        showError = false;

                        // Push order to webservice
                        ConsoleLogger.WriteToLog("WebserviceQueue - Pushing save order in webservice queue (again)...");

                        // Set routestep
                        if (order.OrderRoutestephandler != null)
                        {
                            order.OrderRoutestephandler.Status = routestephandlerSaveStatus.Status;
                            order.OrderRoutestephandler.Error = routestephandlerSaveStatus.Error;
                        }

                        // Update order in memory
                        DataManager.Instance.OrderManager.UpdateOrder(order);

                        // Enqueue order again
                        var webserviceJob = new WebserviceJob(typeof(Order), order.Clone());
                        webserviceJob.NumberOfMaxRetry = 3;
                        Enqueue(webserviceJob);
                    }
                }
                else
                    ConsoleLogger.WriteToLog("WebserviceQueue - WebserviceJob.Data could not be casted to type 'OrderSaveStatus'!");
            }
            else if (job.JobType == typeof(WriteToLog))
            {
                var log = job.Data as WriteToLog;
                if (log != null)
                {
                    //ConsoleLogger.WriteToLog("WebserviceQueue - Log.Type: '{0}'\n\tLog.Message: '{1}'\n\tLog.Status: '{2}'\n\tLog.Log: '{3}'", log.TerminalLogType.ToString(), log.Message, log.Status, log.Log);
                    result = WebserviceQueueHandler.HandleWriteToLog(log);
                }
                else
                    ConsoleLogger.WriteToLog("WebserviceQueue - WebserviceJob.Data could not be casted to type 'WriteToLog'!");
            }
            else if (job.JobType == typeof(TerminalOperationMode))
            {
                var tmp = String.Format("{0}", job.Data);
                var tmpInt = int.Parse(tmp);
                result = WebserviceQueueHandler.HandleSetTerminalOperationMode(tmpInt.ToEnum<TerminalOperationMode>());
            }
            else if (job.JobType == typeof(TerminalOperationState))
            {
                var tmp = String.Format("{0}", job.Data);
                result = WebserviceQueueHandler.HandleSetTerminalOperationState(tmp.ToEnum<TerminalOperationState>());
            }
            //else if (job.JobType == typeof(Folio))
            //{
            //    var actionData = job.Data as Folio;
            //    if (actionData != null)
            //        result = WebserviceQueueHandler.HandleSetPmsFolio(actionData);
            //}
            //else if (job.JobType == typeof(GuestInformation))
            //{
            //    var actionData = job.Data as GuestInformation;
            //    if (actionData != null)
            //    {
            //        ConsoleLogger.WriteToLog("WebserviceQueue - Processing job of type '{0}' - SubType: ", actionData.OnSiteServerWebserviceJobType);
            //        ConsoleLogger.WriteToLog("WebserviceQueue - Action Data: DeliverypointNumber '{0}', CustomerFirstname: '{1}', CustomerLastname: '{2}'", actionData.DeliverypointNumber, actionData.CustomerFirstname, actionData.CustomerLastname);                    

            //        if (actionData.OnSiteServerWebserviceJobType == OnSiteServerWebserviceJobType.PmsGetGuestInformation)
            //            result = WebserviceQueueHandler.HandleSetPmsGuestInformation(actionData);
            //        else if (actionData.OnSiteServerWebserviceJobType == OnSiteServerWebserviceJobType.PmsCheckedIn)
            //            result = WebserviceQueueHandler.HandleSetPmsCheckedIn(actionData);
            //        else
            //        {
            //            ConsoleLogger.WriteToLog("WebserviceQueue - Job of Type: {0} - Has non implemented JobType: '{1}'", job.JobType.ToString(), actionData.OnSiteServerWebserviceJobType.ToString());
            //            job.NumberOfMaxRetry = 0;
            //            result = false;
            //        }
            //    }
            //}
            else if (job.JobType == typeof(WakeUpStatus))
            {
                var actionData = job.Data as WakeUpStatus;
                if (actionData != null)
                {
                    ConsoleLogger.WriteToLog("WebserviceQueue - Processing job of type '{0}' - SubType: ", actionData.OnSiteServerWebserviceJobType);
                    ConsoleLogger.WriteToLog("WebserviceQueue - Action Data: DeliverypointNumber '{0}', WakeUp date: '{1}', WakeUp time: '{2}'", actionData.DeliverypointNumber, actionData.WakeUpDate.ToShortDateString(), actionData.WakeUpTime.ToShortTimeString());
                    if (actionData.OnSiteServerWebserviceJobType == OnSiteServerWebserviceJobType.PmsWakeUpSet)
                        result = WebserviceQueueHandler.HandleSetPmsWakeUp(actionData);
                    else if (actionData.OnSiteServerWebserviceJobType == OnSiteServerWebserviceJobType.PmsWakeUpCleared)
                        result = WebserviceQueueHandler.HandleClearPmsWakeUp(actionData);
                    else
                    {
                        ConsoleLogger.WriteToLog("WebserviceQueue - Job of Type: {0} - Has non implemented JobType: '{1}'", job.JobType.ToString(), actionData.OnSiteServerWebserviceJobType.ToString());
                        job.NumberOfMaxRetry = 0;
                        result = false;
                    }
                }
            }
            else if (job.JobType == typeof(Checkout))
            {
                var actionData = job.Data as Checkout;
                if (actionData != null)
                    result = WebserviceQueueHandler.HandleSetPmsExpressCheckedOut(actionData);
            }
            else if (job.JobType == typeof(NonModelWebserviceJob))
            {
                var actionData = job.Data as NonModelWebserviceJob;
                if (actionData != null)
                {
                    ConsoleLogger.WriteToLog("WebserviceQueue - Processing job of type '{0}' - SubType: ", actionData.JobType);
                    if (actionData.JobType == NonModelWebserviceJob.NonModelWebserviceJobType.SetPmsCheckedOut)
                        result = WebserviceQueueHandler.HandleSetPmsCheckedOut(actionData.DeliverypointNumber);
                    else
                    {
                        ConsoleLogger.WriteToLog("WebserviceQueue - Job of Type: {0} - Has non implemented JobType: '{1}'",
                                                 job.JobType.ToString(), actionData.JobType.ToString());
                        job.NumberOfMaxRetry = 0;
                        result = false;
                    }
                }
            }
            else if (job.JobType == typeof(GuestInformation[]))
            {
                var actionData = job.Data as GuestInformation[];
                if (actionData != null)
                {
                    ConsoleLogger.WriteToLog("WebserviceQueue - Processing a SaveGuestInformations request with '{0}' models", actionData.Length);
                    result = WebserviceQueueHandler.HandleSaveGuestInformations(actionData);
                }
            }
            else if (job.JobType == typeof(Folio[]))
            {
                var actionData = job.Data as Folio[];
                if (actionData != null)
                {
                    ConsoleLogger.WriteToLog("WebserviceQueue - Processing a SaveFolios request with '{0}' models", actionData.Length);
                    result = WebserviceQueueHandler.HandleSaveFolios(actionData);
                }
            }
            else if (job.JobType == typeof(Folio))
            {
                var actionData = job.Data as Folio;
                if (actionData != null)
                {
                    ConsoleLogger.WriteToLog("WebserviceQueue - Processing a SetPmsFolio request");
                    result = WebserviceQueueHandler.HandleSetPmsFolio(actionData);
                }
            }
            else
            {
                ConsoleLogger.WriteToLog("WebserviceQueue - Unknown job type: {0}", job.JobType.ToString());
            }

            if (!result && job.NumberOfMaxRetry != 0)
            {
                // If webservice handler failed, place job back in queue
                if (showError)
                ConsoleLogger.WriteToLog("WebserviceQueue - Job of type '{0}' FAILED - Data: {1}", job.JobType.ToString(), job.Data.ToString());

                // Check if we still have internet or a connection to the webservice(s)
                Global.Status.Refresh(false);                

                if (job.NumberOfMaxRetry == -1 || (job.NumberOfRetry <= job.NumberOfMaxRetry))
                {
                    // Increment num. of tries and put job back in the queue
                    job.NumberOfRetry++;
                    this.EnqueueDelayed(job, job.RetryDelay);
                }
                else
                {
                    // Write to failed folder
                    string filename = Path.Combine(serializeDirectoryFailed, job.Guid + ".job");
                    string xml = JsonHelper.Serialize(job);
                    File.WriteAllText(filename, xml);

                    // Send to terminal log
                    var log = new WriteToLog
                    {
                        Message = "Error writing job (" + job.Guid + ") to webservice",
                        Log = JsonHelper.Serialize(job),
                        TerminalLogType = (int)TerminalLogType.Message
                    };
                    Enqueue(new WebserviceJob(typeof(WriteToLog), log) { NumberOfMaxRetry = 0 });
                }
            }
        }

        #endregion
    }
}
