﻿using System;
using System.Collections.Generic;
using CraveOnsiteServer.Data.Managers;
using Dionysos;
using Newtonsoft.Json;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Logging;

namespace CraveOnsiteServer.Data.Queue
{
    public class PmsQueueHandler
    {
        #region Methods

        public static bool ProcessGetFolio(PmsJob job)
        {
            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessGetFolio - Start");

            bool success = false;
            try
            {
                List<Folio> folios = Global.PmsConnector.GetFolio(job.DeliverypointNumber);
                if (folios.Count == 0)
                {
                    PmsManager.Instance.ClearFolio(job.DeliverypointNumber);
                    return true;
                }

                foreach (Folio folio in folios)
                {
                    PmsManager.Instance.SavePmsModel(folio);
                }
                success = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessGetFolio - Exception {0}", ex.Message);
                PmsManager.Instance.ClearFolio(job.DeliverypointNumber);
            }

            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessGetFolio - End");

            return success;
        }

        public static bool ProcessGetGuestInformation(PmsJob job)
        {
            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessGetGuestInformation - Start");

            bool success = false;
            try
            {
                List<GuestInformation> guestInformations = Global.PmsConnector.GetGuestInformation(job.DeliverypointNumber);
                foreach (GuestInformation guestInfo in guestInformations)
                {
                    PmsManager.Instance.SavePmsModel(guestInfo);
                }
                success = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessGetGuestInformation - Exception {0}", ex.Message);
            }

            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessGetGuestInformation - End");

            return success;
        }

        public static bool ProcessGetExpressCheckout(PmsJob job)
        {
            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessGetExpressCheckout - Start");

            bool success = false;
            try
            {
                Checkout checkoutInfo = new Checkout();
                checkoutInfo.ChargePaid = false;
                checkoutInfo.ChargePriceIn = job.Balance;
                checkoutInfo.DeliverypointNumber = job.DeliverypointNumber;

                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessGetExpressCheckout - Deliverypoint: '{0}' - Call - Begin", job.DeliverypointNumber);

                bool checkoutRequestSent = Global.PmsConnector.Checkout(checkoutInfo);

                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessGetExpressCheckout - Deliverypoint: '{0}' - CheckoutRequestSent: '{1}'", job.DeliverypointNumber, checkoutRequestSent);
                success = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessGetExpressCheckout - Exception {0}", ex.Message);
            }

            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessGetExpressCheckout - End");

            return success;
        }

        public static bool ProcessSetPmsWakeUpStatus(PmsJob job)
        {
            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessSetPmsWakeUpStatus - Start");

            bool success = false;
            try
            {
                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessSetPmsWakeUpStatus - Deliverypoint: '{0}' - Call - Begin", job.DeliverypointNumber);
                //bool pmsWakeUpStatusSet = Global.PmsConnector.SetWakeUpStatus(job.WakeUpStatus);              
                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessSetPmsWakeUpStatus - Deliverypoint: '{0}' - Call - End", job.DeliverypointNumber);

                success = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessSetPmsWakeUpStatus - Exception {0}", ex.Message);
            }

            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessSetPmsWakeUpStatus - End");

            return success;
        }

        public static bool ProcessSetRoomData(PmsJob job)
        {
            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessSetRoomData - Start");

            bool success = true;
            try
            {
                RoomData roomData = job.RoomData;
                if (roomData == null)
                {
                    ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessSetRoomData - Ignoring room data as data is null");
                }
                else if (!roomData.ClassOfServiceSet && !roomData.DoNotDisturbSet && !roomData.VoicemailMessageSet)
                {
                    ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessSetRoomData - Ignoring room data as data isn't modified {0} {1} {2}", roomData.ClassOfServiceSet, roomData.DoNotDisturbSet, roomData.VoicemailMessageSet);
                }
                else
                {
                    string roomDataFromFile;
                    if (PmsManager.Instance.LoadPmsModel(roomData.DeliverypointNumber, typeof(GuestInformation).Name, out roomDataFromFile))
                    {
                        GuestInformation guestInfo = JsonConvert.DeserializeObject<GuestInformation>(roomDataFromFile);
                        if (guestInfo != null)
                        {
                            bool guestInfoChanged = ((guestInfo.ClassOfService != roomData.ClassOfService && roomData.ClassOfServiceSet) || (guestInfo.DoNotDisturb != roomData.DoNotDisturb && roomData.DoNotDisturbSet) || (guestInfo.VoicemailMessage != roomData.VoicemailMessage && roomData.VoicemailMessageSet));
                            if (guestInfoChanged)
                            {
                                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessSetRoomData - GuestInformation changed");

                                if (roomData.ClassOfServiceSet)
                                {
                                    guestInfo.ClassOfService = roomData.ClassOfService;
                                }
                                if (roomData.DoNotDisturbSet)
                                {
                                    guestInfo.DoNotDisturb = roomData.DoNotDisturb;
                                }
                                if (roomData.VoicemailMessageSet)
                                {
                                    guestInfo.VoicemailMessage = roomData.VoicemailMessage;
                                }

                                PmsManager.Instance.SavePmsModel(guestInfo);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessSetRoomData - Exception {0}", ex.Message);
                success = false;
            }

            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessSetRoomData - End");

            return success;
        }

        public static bool ProcessEditGuestInformation(PmsJob job)
        {
            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessEditGuestInformation - Deliverypoint: '{0}' - Start", job.DeliverypointNumber);

            bool success = false;
            try
            {
                GuestInformation newGuestInfo = job.GuestInformation;
                if (newGuestInfo != null)
                {
                    string guestInfoModel;
                    if (PmsManager.Instance.LoadPmsModel(newGuestInfo.DeliverypointNumber, typeof(GuestInformation).Name, out guestInfoModel))
                    {
                        GuestInformation oldGuestInfo = JsonConvert.DeserializeObject<GuestInformation>(guestInfoModel);
                        if (oldGuestInfo != null)
                        {
                            if (!newGuestInfo.Title.IsNullOrWhiteSpace())
                            {
                                oldGuestInfo.Title = newGuestInfo.Title;
                            }
                            if (!newGuestInfo.CustomerLastname.IsNullOrWhiteSpace())
                            {
                                oldGuestInfo.CustomerLastname = newGuestInfo.CustomerLastname;
                            }
                            if (!newGuestInfo.CustomerFirstname.IsNullOrWhiteSpace())
                            {
                                oldGuestInfo.CustomerFirstname = newGuestInfo.CustomerFirstname;
                            }
                            if (!newGuestInfo.LanguageCode.IsNullOrWhiteSpace())
                            {
                                oldGuestInfo.LanguageCode = newGuestInfo.LanguageCode;
                            }
                            if (!newGuestInfo.GroupName.IsNullOrWhiteSpace())
                            {
                                oldGuestInfo.GroupName = newGuestInfo.GroupName;
                            }
                            if (oldGuestInfo.Vip != newGuestInfo.Vip)
                            {
                                oldGuestInfo.Vip = newGuestInfo.Vip;
                            }
                            if (!oldGuestInfo.Arrival.Equals(newGuestInfo.Arrival))
                            {
                                oldGuestInfo.Arrival = newGuestInfo.Arrival;
                            }
                            if (!oldGuestInfo.Departure.Equals(newGuestInfo.Departure))
                            {
                                oldGuestInfo.Departure = newGuestInfo.Departure;
                            }
                            if (oldGuestInfo.TvSetting != newGuestInfo.TvSetting && newGuestInfo.TvSetting != (int)PMSTvSetting.Unknown)
                            {
                                oldGuestInfo.TvSetting = newGuestInfo.TvSetting;
                            }
                            if (oldGuestInfo.MinibarSetting != newGuestInfo.MinibarSetting && newGuestInfo.TvSetting != (int)PMSMinibarSetting.Unknown)
                            {
                                oldGuestInfo.MinibarSetting = newGuestInfo.MinibarSetting;
                            }
                            if (oldGuestInfo.AllowViewFolio != newGuestInfo.AllowViewFolio && newGuestInfo.AllowViewFolioSet)
                            {
                                oldGuestInfo.AllowViewFolio = newGuestInfo.AllowViewFolio;
                            }
                            if (oldGuestInfo.AllowExpressCheckout != newGuestInfo.AllowExpressCheckout && newGuestInfo.AllowExpressCheckoutSet)
                            {
                                oldGuestInfo.AllowExpressCheckout = newGuestInfo.AllowExpressCheckout;
                            }

                            PmsManager.Instance.SavePmsModel(oldGuestInfo);
                            success = true;
                        }
                    }
                }

                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessEditGuestInformation - Deliverypoint: '{0}' - End", job.DeliverypointNumber);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessEditGuestInformation - Exception {0}", ex.Message);
            }
            return success;
        }

        public static bool ProcessMoveRoom(PmsJob job)
        {
            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessMoveRoom - Deliverypoint: '{0}' to '{1}' - Start", job.DeliverypointNumberOld, job.DeliverypointNumber);

            bool succes = false;

            // Load old guest info
            string guestInfoModel;
            if (!PmsManager.Instance.LoadPmsModel(job.DeliverypointNumberOld, typeof(GuestInformation).Name, out guestInfoModel))
            {
                ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessMoveRoom - PMS model couldn't be loaded for deliverypoint: '{0}'", job.DeliverypointNumberOld);
            }
            else
            {
                GuestInformation oldGuestInfo = JsonConvert.DeserializeObject<GuestInformation>(guestInfoModel);
                if (oldGuestInfo != null)
                {
                    ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessMoveRoom - Old guest info: '{0}'", guestInfoModel);

                    // Check out old room
                    oldGuestInfo.Occupied = false;
                    PmsManager.Instance.SavePmsModel(oldGuestInfo, true);
                    PmsManager.Instance.ClearFolio(job.DeliverypointNumberOld);

                    // Check-in with existing guest info, but changed room number
                    GuestInformation newGuestInfo = JsonConvert.DeserializeObject<GuestInformation>(guestInfoModel);
                    newGuestInfo.DeliverypointNumber = job.DeliverypointNumber;
                    newGuestInfo.Occupied = true;
                    PmsManager.Instance.SavePmsModel(newGuestInfo, true);

                    succes = true;
                }
            }

            ConsoleLogger.WriteToLog("PmsQueueHandler - ProcessMoveRoom - Deliverypoint: '{0}' to '{1}' - End", job.DeliverypointNumberOld, job.DeliverypointNumber);

            return succes;
        }

        #endregion
    }
}