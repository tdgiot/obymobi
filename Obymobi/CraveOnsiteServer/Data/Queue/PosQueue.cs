﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using CraveOnsiteServer.Server;
using Obymobi;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logging;
using Obymobi.Integrations.POSLogic.POS;

namespace CraveOnsiteServer.Data.Queue
{
    public class PosQueue
    {
        #region Fields

        private readonly Queue<PosJob> queue = new Queue<PosJob>();
        private readonly List<PosJob> delayedPosJobs = new List<PosJob>();
        private readonly object posQueueLock = new object();
        private readonly AutoResetEvent queueWaitHandle = new AutoResetEvent(false);

        private Thread consumerThread;

        private bool isRunning = true;
        //private readonly string serializeDirectory;

        #endregion

        #region Constructor

        public PosQueue()
        {
            consumerThread = null;
            //serializeDirectory = DataManager.CreateDirectory("Data\\PosQueue");
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Start POS queue
        /// </summary>
        public void StartConsumer()
        {
            //LoadQueueFromFile();

            isRunning = true;

            if (consumerThread == null)
            {
                consumerThread = new Thread(this.ConsumeItems)
                {
                    IsBackground = true,
                };
                consumerThread.Start();
            }
        }

        /// <summary>
        /// Stop POS queue
        /// </summary>
        public void StopConsumer()
        {
            isRunning = false;

            if (consumerThread != null)
            {
                consumerThread.Abort();
                consumerThread = null;

                // Add any delayed tasks to queue
                foreach (var posJob in delayedPosJobs)
                {
                    if (!queue.Contains(posJob))
                        queue.Enqueue(posJob);
                }

                //SaveQueueToFile();
            }
        }

        public int Size
        {
            get
            {
                lock (posQueueLock)
                {
                    return this.queue.Count;
                }
            }
        }

        /// <summary>
        /// Queue new job in the POS queue
        /// </summary>
        /// <param name="item">PosJob object</param>
        /// <returns>True of the PosJob is successfully added to the POS queue</returns>
        public bool Enqueue(PosJob item)
        {
            return Enqueue(item, false);
        }

        /// <summary>
        /// Queue new job in the POS queue
        /// </summary>
        /// <param name="item">PosJob object</param>
        /// <param name="isDelayed">True if the PosJob was originally delayed due to an error</param>
        /// <returns>True of the PosJob is successfully added to the POS queue</returns>
        private bool Enqueue(PosJob item, bool isDelayed)
        {
            ConsoleLogger.WriteToLog("PosQueue - Enqueue - BEGIN");

            if (!isRunning)
            {
                ConsoleLogger.WriteToLog("PosQueue - Enqueue - ERROR: PosQueue is not running");
                return false;
            }

            if (item.PosOrder == null)
            {
                ConsoleLogger.WriteToLog("PosQueue - Enqueue - PosJob with empty Posorder. Ignoring item...");
                return false;
            }

            bool itemQueued = false;
        	bool isExpired;
            lock (posQueueLock)
            {
                if (isDelayed)
                    delayedPosJobs.Remove(item);

				// Check if order might have been expired
            	isExpired = isJobExpired(item);
				if (!isExpired)
				{
					if (!queue.Contains(item))
					{
						queue.Enqueue(item);
						itemQueued = true;

						ConsoleLogger.WriteToLog("Pos Queue - Enqueing new item (queue size: {0})", queue.Count);
					}
				}
            }

			if (isExpired)
			{
				ConsoleLogger.WriteToLog("Pos Queue - Order Timed-out. Canceling order.");
				item.Table.HandleOrderTimedOut(item);
			}

            // After enqueuing the item, signal the consumer thread.
            if (itemQueued)
                queueWaitHandle.Set();

            ConsoleLogger.WriteToLog("PosQueue - Enqueue - END");

            return true;
        }

        /// <summary>
        /// Enqueue a PosJob with a set delay
        /// </summary>
        /// <param name="item">PosJob object</param>
        /// <param name="delay">Delay in milliseconds</param>
        private void Enqueue(PosJob item, int delay)
        {
        	ConsoleLogger.WriteToLog("PosQueue - Enqueue with delay {0} - BEGIN", delay);

			bool canQueue = !isJobExpired(item);
			if (canQueue)
			{
				this.delayedPosJobs.Add(item);
				Task.Factory.StartNew(() =>
										{
											Thread.Sleep(delay);
											Enqueue(item, true);
										}, TaskCreationOptions.LongRunning);
			}
			else
			{
				ConsoleLogger.WriteToLog("Order Timed-out. Canceling order.");
				item.Table.HandleOrderTimedOut(item);
			}

        	ConsoleLogger.WriteToLog("PosQueue - Enqueue with delay - END");
        }

		private bool isJobExpired(PosJob item)
		{
			bool isExpired = false;
			if (item != null && item.OriginalOrder != null && item.OriginalOrder.OrderRoutestephandler != null && 
				item.OriginalOrder.OrderRoutestephandler.Timeout > 0 && item.OriginalOrder.OrderRoutestephandler.TimeoutExpires.HasValue)
			{
				DateTime createdDateTime = item.OriginalOrder.CreatedOnServer;
				if (DateTime.Now.Subtract(createdDateTime).TotalMinutes >= item.OriginalOrder.OrderRoutestephandler.Timeout) // Timeout is in minutes.
				{
					isExpired = true;
				}
			}

			return isExpired;
		}

        #endregion

        #region Queue Handler

        /// <summary>
        /// Main method for consuming the POS queue
        /// </summary>
        private void ConsumeItems()
        {
            ConsoleLogger.WriteToLog("Pos Queue - Consume Items - STARTED");

            while (isRunning)
            {
                ConsoleLogger.WriteToLog("Pos Queue - Consume Items - BEGIN");

                PosJob nextItem = null;
                lock (posQueueLock)
                {
                    // Check if we have items in the POS queue
                    bool doesItemExist = (queue.Count > 0);
                    if (doesItemExist)
                    {
                        // Dequeue item from POS queue
                        nextItem = queue.Dequeue();

                        // Write to log
                        ConsoleLogger.WriteToLog("Pos Queue - Dequeing item (items left: {0})", queue.Count);

                        if (nextItem == null)
                        {
                            ConsoleLogger.WriteToLog("Pos Queue - Stopping Queue");
                            return;
                        }
                    }
                }

                if (nextItem != null)
                {
                    ConsoleLogger.WriteToLog("Pos Queue - Have item, call SendPosOrderToPos");

                    // There was an item in the queue, process it...
                    SendPosOrderToPos(nextItem);

                    ConsoleLogger.WriteToLog("Pos Queue - Consume Items - END");
                }
                else
                {
                    ConsoleLogger.WriteToLog("Pos Queue - No more items in queue, waiting for a new one...");
                    ConsoleLogger.WriteToLog("Pos Queue - Consume Items - END");

                    queueWaitHandle.WaitOne();
                }
            }

            ConsoleLogger.WriteToLog("Pos Queue - Consume Items - STOPPED");
        }

        /// <summary>
        /// Send Posorder to POS
        /// </summary>
        private void SendPosOrderToPos(PosJob job)
        {
            ConsoleLogger.WriteToLog("Pos Queue - Send Pos Order to Pos - BEGIN");

            int deliverypoint = 0;
            Table table = Server.Server.Instance.GetTableById(job.DeliverypointId);
            if (table != null)
            {
                deliverypoint = table.Deliverypoint.Number;
            }

            // Vars
            Posorder posorder = job.PosOrder;
            int orderId = job.OrderId;

            // Result
            var result = new OrderRoutestephandlerSaveStatus();
            if (job.OriginalOrder != null)
            {
                result = OrderRoutestephandlerHelper.CreateRoutestephandlerSaveStatusModelFromOrderModel(job.OriginalOrder);
            }

            // Send the order to the POS
            bool retry = true;
            bool sent = false;

            try
            {
                if (job.OriginalOrder != null)
                    ConsoleLogger.WriteToLog("Pos Queue - Processing order with GUID '{0}'", job.OriginalOrder.Guid);
                else
                    ConsoleLogger.WriteToLog("Pos Queue - Processing POS message with PosdeliverypointExternalId '{0}'", job.PosOrder.PosdeliverypointExternalId);

                job.AttemptsMade++;

				if (Global.PosConnector != null)
				{
					// Pass POS order to PosConnector for some black magic
					var processingError = Global.PosConnector.SaveOrder(posorder);
					if (processingError != OrderProcessingError.None)
                    {
                        result.ErrorMessage = posorder.ErrorMessage;

						// Oops.. our pentagram failed.. Now Satan is mad :<
						throw new POSException(processingError, "Error while processing order to POS: " + processingError);
					}
				}

            	sent = true;

                // Commented by - FO
                // GK-ROUTING: What was commented by FO?                
                result.Error = (int)OrderProcessingError.None;
                result.Status = (int)OrderRoutestephandlerStatus.Completed;

                if (job.OriginalOrder != null)
                    ConsoleLogger.WriteToLog("Pos Queue - Done processing GUID '{0}'", job.OriginalOrder.Guid);
                else
                    ConsoleLogger.WriteToLog("Pos Queue - Done processing POS message '{0}'", job.PosOrder.PosdeliverypointExternalId);
            }
            catch (NonFatalSaveOrderException dex)
            {
                ConsoleLogger.WriteToLog("Pos Queue - Failed with {0}: {1}", dex.GetType().ToString(), dex.Message);
                ConsoleLogger.WriteToLog("Pos Queue - Order: \r\n{0}", XmlHelper.Serialize(job.PosOrder)); 
                // Deliverypoint Error, Retry.
                //TerminalLogType logType;
                string logText;
                result.Status = (int)OrderRoutestephandlerStatus.BeingHandled;
                result.Error = (int)dex.ErrorType;

                if (dex is DeliverypointLockedException)
                    logText = string.Format("Attempt {0} failed: DeliverypointLockedException: Deliverypoint: '{1}', PosdeliverypointExternalId: '{2}'  Order: '{3}'", job.AttemptsMade, deliverypoint, posorder.PosdeliverypointExternalId, orderId);
                else if (dex is ConnectivityException)
                    logText = string.Format("Attempt {0} failed: ConnectivityException: '{1}'", job.AttemptsMade, dex.Message);
                else if (dex is InvalidProductsOrAlterationsException)
                    logText = string.Format("InvalidProductsOrAlterationsException: '{0}'", dex.Message);
                else
                    logText = string.Format("Attempt {0} failed: UnspecifiedNonFatalPosProblem: '{1}'", job.AttemptsMade, dex.Message);

                // Check if this is already the 3rd attempt that failed
                if (job.AttemptsMade >= 3)
                {
                    // Three attempts failed, mark the order as unprocessable
                    logText = string.Format("Attempt 3 failed: '{0}'. Mark the order as unprocessable", dex.Message);
                    result.Status = (int)OrderStatus.Unprocessable;
                }

                ConsoleLogger.WriteToLog(logText);

                // Write log to webservice
                var writeToLog = new WriteToLog
                {
                    TerminalLogType = (int)TerminalLogType.DeliverypointLockedPosException,
                    Message = dex.Message,
                    Log = logText + "\r\n" + result.ToString() + "\r\n Order: " + XmlHelper.Serialize(job.PosOrder),
                    Status = result.Error.ToEnum<OrderProcessingError>().ToString(),
                    orderId = orderId
                };

                if (job.OriginalOrder != null)
                {
                    writeToLog.OrderGuid = job.OriginalOrder.Guid;
                }

                DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(WriteToLog), writeToLog));

                // Send a message to the POS. Only if we have an order id (system messages get send without one)
                if (orderId != -1)
                {
                    if (dex is DeliverypointLockedException)
                    {
                        Posorder posMessage = OnSiteServerPosHelper.CreateMessageForPos(PosServiceMessageType.UnlockDeliverypoint, deliverypoint);
                        if (posMessage != null)
                        {
                            Enqueue(new PosJob
                            {
                                Identifier = Dionysos.TimeStamp.CreateTimeStamp(true),
                                OriginalOrder = null,
                                DeliverypointId = job.DeliverypointId,
                                OrderId = -1,
                                PosOrder = posMessage,
                                Table = null
                            });
                        }
                    }
                }
            }
            catch (ObymobiException oex)
            {
                ConsoleLogger.WriteToLog("Pos Queue - Obymobi Exception: {0}\r{1}\r{2}\r", oex.ErrorEnumType, oex.ErrorEnumValueText, oex.BaseMessage);
                ConsoleLogger.WriteToLog("Pos Queue - Order: \r\n{0}", XmlHelper.Serialize(job.PosOrder)); 
                retry = false;

                result.Status = (int)OrderRoutestephandlerStatus.Failed;
                string errorStatusText;
                if (oex is POSException)
                {
                    result.Error = (int)((POSException)oex).ErrorType;
                    errorStatusText = ((POSException)oex).ErrorType.ToString();
                }
                else
                {
                    result.Error = (int)OrderProcessingError.UnspecifiedError;
                    errorStatusText = OrderProcessingError.UnspecifiedError.ToString();
                }

                // Write log to webservice
                var writeToLog = new WriteToLog
                {
                    TerminalLogType = (int)TerminalLogType.NonQualifiedPosException,
                    Message = oex.BaseMessage,
                    Log = oex.AdditionalMessage + "\r\n Order: " + XmlHelper.Serialize(job.PosOrder),
                    Status = errorStatusText,
                    orderId = orderId
                };
                if (job.OriginalOrder != null)
                {
                    writeToLog.OrderGuid = job.OriginalOrder.Guid;
                    //if (orderId == null || orderId == -1)
                    //    writeToLog.orderId = job.OriginalOrder.OrderId;
                }
                DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(WriteToLog), writeToLog));
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("Pos Queue - Exception: {0}", ex.Message);
                ConsoleLogger.WriteToLog("Pos Queue - Order: \r\n{0}", XmlHelper.Serialize(job.PosOrder)); 
                retry = false;
                result.Status = (int)OrderRoutestephandlerStatus.Failed;
                result.Error = (int)OrderProcessingError.UnspecifiedError;

                // Write log to webservice 
                var writeToLog = new WriteToLog
                {
                    Message = ex.Message,
                    Log = String.Format("PosQueue - Exception: {0}", ex.Message) + "\r\n Order: " + XmlHelper.Serialize(job.PosOrder),
                    Status = ((int)OrderProcessingError.UnspecifiedError).ToString(CultureInfo.InvariantCulture)
                };

                if (job.OriginalOrder != null)
                {
                    writeToLog.OrderGuid = job.OriginalOrder.Guid;
                    //writeToLog.orderId = job.OriginalOrder.OrderId;
                }
                DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(WriteToLog), writeToLog));
            }
            finally // FINALLY!! - You can say that again!
            {
                // Pause 
                if (!sent && (retry && job.AttemptsMade <= 2))
                {
                    // Put job back in queue with a small delay (60 sec)
                    Enqueue(job, 60000);
                }
                else
                {
                    if (job.Table != null)
                    {
                        ConsoleLogger.WriteToLog("Pos Queue - Call HandleOrderResult for table {0}", job.Table.Deliverypoint.Number);
                        // Send result back to the table
                        job.Table.HandleOrderResult(job, result);
                    }
                    else if (orderId != -1) // If order id equals -1 then it's a POS message 
                    {
                        ConsoleLogger.WriteToLog("Pos Queue - Order is not linked to a table. Possibly received from webservice.");
                    }
                }
            }

            ConsoleLogger.WriteToLog("Pos Queue - Send Pos Order to Pos - END");
        }


        #endregion

    }
}
