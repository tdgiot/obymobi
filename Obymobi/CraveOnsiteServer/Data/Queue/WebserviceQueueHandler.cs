﻿using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace CraveOnsiteServer.Data.Queue
{
    public class WebserviceQueueHandler
    {
        public static bool HandleSaveOrder(Order order)
        {
            return WebserviceManager.SaveOrder(order);
        }

        public static bool HandleSetRoutestephandlerSaveStatuses(OrderRoutestephandlerSaveStatus routestephandlerSaveStatus)
        {
            var routestephandlerSaveStatusArray = new [] { routestephandlerSaveStatus };
            return WebserviceManager.SetRoutestephandlerSaveStatuses(routestephandlerSaveStatusArray);
        }

        public static bool HandleWriteToLog(WriteToLog logObj)
        {
            return WebserviceManager.WriteToLog(logObj);
        }

        public static bool HandleSetTerminalOperationMode(TerminalOperationMode mode)
        {
            return WebserviceManager.SetTerminalOperationMode(mode);
        }

        public static bool HandleSetTerminalOperationState(TerminalOperationState state)
        {
            return WebserviceManager.SetTerminalOperationState(state);
        }

        public static bool HandleSetPmsFolio(Folio folio)
        {
            return WebserviceManager.SetPmsFolio(folio);
        }

        public static bool HandleSetPmsGuestInformation(GuestInformation guestInformation)
        {
            return WebserviceManager.SetPmsGuestInformation(guestInformation);
        }

        public static bool HandleSetPmsCheckedIn(GuestInformation guestInformation)
        {
            return WebserviceManager.SetPmsCheckedIn(guestInformation);
        }

        public static bool HandleSetPmsCheckedOut(string deliverypointNumber)
        {
            return WebserviceManager.SetPmsCheckedOut(deliverypointNumber);
        }

        public static bool HandleSetPmsExpressCheckedOut(Checkout checkout)
        {
            return WebserviceManager.SetPmsExpressCheckedOut(checkout);
        }

        public static bool HandleSetPmsWakeUp(WakeUpStatus wakeUpStatus)
        {
            return WebserviceManager.SetPmsWakeUp(wakeUpStatus);
        }

        public static bool HandleClearPmsWakeUp(WakeUpStatus wakeUpStatus)
        {
            return WebserviceManager.ClearPmsWakeUp(wakeUpStatus);
        }

        public static bool HandleSaveGuestInformations(GuestInformation[] guestInformations)
        {
            return WebserviceManager.SaveGuestInformations(guestInformations);
        }

        public static bool HandleSaveFolios(Folio[] folios)
        {
            return WebserviceManager.SaveFolios(folios);
        }
    }
}
