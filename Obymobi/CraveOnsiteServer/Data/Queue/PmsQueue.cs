﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Logging;
using Obymobi.Integrations.PMS;

namespace CraveOnsiteServer.Data.Queue
{
    public class PmsQueue
    {
        #region Fields

        private static PmsQueue pmsQueue;

        private const int PROCESS_THREADS = 8;        
        
        private static readonly object queueLock = new object();
        private readonly BlockingCollection<PmsJob> queue = new BlockingCollection<PmsJob>();
        private readonly CancellationTokenSource queueCancelToken;

        #endregion

        #region Constructor

        private PmsQueue(CancellationTokenSource cancellationTokenSource)
        {
            this.queueCancelToken = cancellationTokenSource;

            for (int i = 0; i < PmsQueue.PROCESS_THREADS; i++)
            {
                new Thread((this.ProcessWork)).Start(this.queueCancelToken.Token);
            }            
        }

        private static PmsQueue Instance
        {
            get
            {
                lock (PmsQueue.queueLock)
                {
                    if (PmsQueue.pmsQueue == null)
                    {
                        CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
                        PmsQueue.pmsQueue = new PmsQueue(cancellationTokenSource);
                    }                   
                }

                return PmsQueue.pmsQueue;
            }
        }

        #endregion

        #region Public Methods

        public static void Enqueue(PmsJob pmsJob)
        {
            PmsQueue.Instance.EnqueueInternal(pmsJob);
        }

        public static void Stop()
        {
            if (PmsQueue.Instance.queueCancelToken != null && !PmsQueue.Instance.queueCancelToken.IsCancellationRequested)
            {
                PmsQueue.Instance.queueCancelToken.Cancel(false);
                PmsQueue.Instance.queue.Add(null);
            }            
        }

        #endregion

        #region Private Methods

        private void EnqueueInternal(PmsJob job)
        {
            this.queue.TryAdd(job);            
        }
        
        private void ProcessWork(object cancellationToken)
        {
            CancellationToken token = (CancellationToken)cancellationToken;
            try
            {
                foreach (PmsJob job in this.queue.GetConsumingEnumerable(token))
                {
                    try
                    {
                        ConsoleLogger.WriteToLog("Pms Queue - Dequeing item (items left: {0})", this.queue.Count);
                        this.PerformPmsInteraction(job);
                    }
                    catch (Exception ex)
                    {
                        ConsoleLogger.WriteToLog("PmsQueue - ProcessWork - Exception while processing queue: {0}", ex.Message);
                    }
                }
            }
            catch (OperationCanceledException ex)
            {
                ConsoleLogger.WriteToLog("PmsQueue - ProcessWork - Queue cancelled");
            }
        }

        private void PerformPmsInteraction(PmsJob job)
        {
            if (Global.PmsConnector == null)
            {
                ConsoleLogger.WriteToLog("Pms Queue - PMS Interaction not possible, not PMS connector configured / loaded");
                
                WriteToLog writeToLog = new WriteToLog
                {
                    Message = "No PMS Configured on Terminal: " + Global.TerminalId,
                    Log = "No PMS Configured on Terminal: " + Global.TerminalId,
                    Status = PmsError.NoPmsConfigured.ToString()
                };

                DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(WriteToLog), writeToLog));
                return;
            }

            bool retry = false;
            bool success = false;
            OrderRoutestephandlerSaveStatus result = new OrderRoutestephandlerSaveStatus();
            try
            {
                switch (job.JobType)
                {
                    case PmsJob.PmsJobType.GetFolio:
                        success = PmsQueueHandler.ProcessGetFolio(job);
                        break;
                    case PmsJob.PmsJobType.GetGuestInformation:
                        success = PmsQueueHandler.ProcessGetGuestInformation(job);
                        break;
                    case PmsJob.PmsJobType.GetExpressCheckout:
                        success = PmsQueueHandler.ProcessGetExpressCheckout(job);
                        break;
                    case PmsJob.PmsJobType.SetPmsWakeUpStatus:
                        success = PmsQueueHandler.ProcessSetPmsWakeUpStatus(job);
                        break;
                    case PmsJob.PmsJobType.SetRoomData:
                        success = PmsQueueHandler.ProcessSetRoomData(job);
                        break;
                    case PmsJob.PmsJobType.EditGuestInformation:
                        success = PmsQueueHandler.ProcessEditGuestInformation(job);
                        break;
                    case PmsJob.PmsJobType.RoomMove:
                        success = PmsQueueHandler.ProcessMoveRoom(job);
                        break;
                    default:
                        ConsoleLogger.WriteToLog("PmsJobType not implemented: {0}", job.JobType.ToString());
                        break;
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("Pms Queue - Exception: {0}", ex.Message);
                result.Status = (int)GenericResult.Failure;
                result.Error = (int)PmsError.UnspecifiedError;

                // Write log to webservice 
                WriteToLog writeToLog = new WriteToLog
                {
                    Message = ex.Message,
                    Log = String.Format("PmsQueue - Exception: {0} - Error: {1}", ex.Message, PmsError.UnspecifiedError.ToString()),
                    Status = result.Error.ToString()
                };

                ConsoleLogger.WriteToLog("PMSQueue - Exception: '{0}'", writeToLog.Message);
                ConsoleLogger.WriteToLog("PMSQueue - Log: '{0}'", writeToLog.Log);

                DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(WriteToLog), writeToLog));
            }
            finally 
            {
                if (!success)
                {
                    ConsoleLogger.WriteToLog("PmsQueue - Job Failed: '{0}', DeliverypointNumber: '{1}'", job.JobType.ToString(), job.DeliverypointNumber);
                }
            }
        }

        #endregion        
    }
}
