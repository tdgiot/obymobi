﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CraveOnsiteServer.Data.Managers;
using CraveOnsiteServer.Logic.Enum;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Logging;

namespace CraveOnsiteServer.Data
{
    public class DataManager
    {
        #region Fields

        private static DataManager sInstance;

        private bool isLoaded;
        public bool IsInitialized { get; private set; }

        public CraveService.CraveService Webservice { get; private set; }
        public Queue.WebserviceQueue WebserviceQueue { get; private set; }

        public CompanyManager CompanyManager { get; private set; }
        public DeliverypointManager DeliverypointManager { get; private set; }
        public TerminalManager TerminalManager { get; private set; }
        public PosIntegrationInformationManager PosIntegrationInformationManager { get; private set; }
        public OrderManager OrderManager { get; private set; }

        #endregion

        #region Constructor

        private DataManager()
        {
            this.IsInitialized = false;

            try
            {
                this.Webservice = new CraveService.CraveService();
                this.WebserviceQueue = new Queue.WebserviceQueue();

                this.CompanyManager = new CompanyManager();
                this.TerminalManager = new TerminalManager();
                this.DeliverypointManager = new DeliverypointManager();
                this.PosIntegrationInformationManager = new PosIntegrationInformationManager();
                this.OrderManager = new OrderManager();

                this.isLoaded = false;
                this.IsInitialized = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: Exception while creating new DataManager instance: {0}\n\n{1}", ex.Message, ex.StackTrace);
            }
        }

        public static DataManager Instance
        {
            get { return DataManager.sInstance ?? (DataManager.sInstance = new DataManager()); }
        }

        #endregion

        #region Public Methods

        public void Reset()
        {
            this.IsInitialized = false;
            this.isLoaded = false;

            this.Webservice = null;
            this.WebserviceQueue = null;

            DataManager.sInstance = null;
        }

        public void InitializeWebservice()
        {
            this.Webservice = new CraveService.CraveService
                             {
                                 Url = Global.Status.CraveServiceUrl,
                                 EnableDecompression = true
                             };
        }

        public void Load()
        {
            if (!this.PreloadTerminalInfo())
            {
                WriteToLog log = new WriteToLog(TerminalLogType.Start, "DataManager - Initialize - Unable to load data.");
                DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(WriteToLog), log), true);
                throw new Exception("DataManager - Unable to load data");
            }
            else
            {
                this.isLoaded = true;
            }
        }

        public bool PreloadTerminalInfo()
        {
            bool toReturn = false;
            if (!this.TerminalManager.Load())
            {
                ConsoleLogger.WriteToLog("ERROR: Failed to load Terminal data");
            }
            else if (this.TerminalManager.GetTerminal() != null && this.TerminalManager.GetTerminal().HandlingMethod.ToEnum<TerminalType>() == TerminalType.Console)
            {
                ConsoleLogger.WriteToLog("ERROR: On-site server can not be used with this terminal order handling method!");
            }
            else if (!this.CompanyManager.Load())
            {
                ConsoleLogger.WriteToLog("ERROR: Failed to load Company data");
            }
            else if (!this.PosIntegrationInformationManager.Load())
            {
                ConsoleLogger.WriteToLog("ERROR: Failed to load PosIntegrationInformation data.");
            }
            else if (!this.DeliverypointManager.Load())
            {
                ConsoleLogger.WriteToLog("ERROR: Failed to load Deliverypoint data.");
            }
            else
            {
                toReturn = true;
            }
            return toReturn;
        }

        /// <summary>
        /// Reload data while server stays running
        /// </summary>
        /// <returns>Object (LoadResult) with the status of the reload</returns>
        public LoadResult Reload()
        {
            LoadResult result = LoadResult.ERROR;
            if (!this.isLoaded)
            {
                result = LoadResult.LOADED;
            }
            else
            {
                ConsoleLogger.WriteToLog("RELOAD - Start");
                Global.ServerState = ServerState.RELOADING;
                List<IManager> managerList = new List<IManager>();
                bool hasNewData = false;

                ConsoleLogger.WriteToLog(" * Reloading Company");
                managerList.Add(this.CompanyManager);
                result = this.CompanyManager.Reload();
                if (result == LoadResult.ERROR)
                    ConsoleLogger.WriteToLog("ERROR: Failed to reload Company data");
                else if (result == LoadResult.NEW_DATA)
                    hasNewData = true;

                if (result != LoadResult.ERROR)
                {
                    ConsoleLogger.WriteToLog(" * Reloading Terminal");
                    managerList.Add(this.TerminalManager);
                    result = this.TerminalManager.Reload();
                    if (result == LoadResult.ERROR)
                        ConsoleLogger.WriteToLog("ERROR: Failed to reload Terminal data");
                    else if (result == LoadResult.NEW_DATA)
                    {
                        hasNewData = true;

                        if (this.TerminalManager.GetTerminal().HandlingMethod.ToEnum<TerminalType>() == TerminalType.Console)
                        {
                            ConsoleLogger.WriteToLog("ERROR: On-site server can not be used with this terminal order handling method!");
                            result = LoadResult.ERROR;
                        }
                    }

                }

                if (result != LoadResult.ERROR)
                {
                    ConsoleLogger.WriteToLog(" * Reloading Deliverypoints");
                    managerList.Add(this.DeliverypointManager);
                    result = this.DeliverypointManager.Reload();
                    if (result == LoadResult.ERROR)
                        ConsoleLogger.WriteToLog("ERROR: Failed to reload Deliverypoint data.");
                    else if (result == LoadResult.NEW_DATA)
                        hasNewData = true;
                }

                if (result != LoadResult.ERROR)
                {
                    ConsoleLogger.WriteToLog(" * Reloading PosIntegrationInformation");
                    managerList.Add(this.PosIntegrationInformationManager);
                    result = this.PosIntegrationInformationManager.Reload();
                    if (result == LoadResult.ERROR)
                        ConsoleLogger.WriteToLog("ERROR: Failed to reload PosIntegrationInformation data.");
                    else if (result == LoadResult.NEW_DATA)
                        hasNewData = true;
                }

                if (result == LoadResult.ERROR)
                {
                    ConsoleLogger.WriteToLog("Failed to reload managers. Reverting data.");
                    foreach (IManager manager in managerList)
                    {
                        manager.ReloadRevert();
                    }
                }

                managerList.Clear();

                if (result != LoadResult.NEW_DATA && hasNewData)
                    result = LoadResult.NEW_DATA;

                ConsoleLogger.WriteToLog("RELOAD DATA - Completed");
            }

            return result;
        }

        /// <summary>
        /// Write string to file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool WriteToFile(string filename, string data)
        {
            string directory = DataManager.CreateDirectory("Data");
            string fullPath = Path.Combine(directory, filename);

            try
            {
                File.WriteAllText(fullPath, data);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("Could not write data file ({0}) to directory {1}\n\rException: {2}", new object[] { filename, directory, ex.Message });
                return false;
            }

            return true;
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Creates a directory relative to the application directory
        /// </summary>
        /// <param name="relativeDir">The directory relative to the application directory</param>
        /// <returns>The full path to the relative directory</returns>
        public static string CreateDirectory(string relativeDir)
        {
            string dir = Path.Combine(Global.RootDirectory, relativeDir);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            return dir;
        }

        /// <summary>
        /// Check if two objects are exactly the same
        /// </summary>
        /// <typeparam name="T">Object Type</typeparam>
        /// <param name="self">Object one</param>
        /// <param name="to">Object two</param>
        /// <param name="ignore">Array of property names to ignore</param>
        /// <returns>True if all properties have the sama value</returns>
        public static bool PublicPropertiesEqual<T>(T self, T to, params string[] ignore) where T : class
        {
            if (self != null && to != null)
            {
                Type type = typeof(T);
                List<string> ignoreList = new List<string>(ignore);
                foreach (System.Reflection.PropertyInfo pi in type.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
                {
                    if (!ignoreList.Contains(pi.Name))
                    {
                        object selfValue = type.GetProperty(pi.Name).GetValue(self, null);
                        object toValue = type.GetProperty(pi.Name).GetValue(to, null);

                        if (selfValue != toValue && (selfValue == null || !selfValue.Equals(toValue)))
                        {
                            if (selfValue != null)
                            {
                                Type selfType = selfValue.GetType();
                                if (selfType.IsArray)
                                {
                                    object[] selfArray = (object[])selfValue;
                                    object[] toArray = (object[])toValue;
                                    if (selfArray != toArray || selfArray.Length != toArray.Length || selfArray.Where((t, i) => !DataManager.PublicPropertiesEqual(t, toArray[i])).Any())
                                    {
                                        return false;
                                    }
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            return self == to;
        }

        #endregion
    }
}
