﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CraveOnsiteServer.Data
{
    /// <summary>
    /// Used for Webservice Jobs that don't require a model
    /// </summary>
    public class NonModelWebserviceJob
    {
        public enum NonModelWebserviceJobType : int
        {
            SetPmsCheckedOut = 1
        }

        public NonModelWebserviceJobType JobType { get; set; }

        public string DeliverypointNumber { get; set; }
    }
}
