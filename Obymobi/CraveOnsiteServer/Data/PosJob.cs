﻿using System.Xml.Serialization;
using CraveOnsiteServer.Server;
using Obymobi.Logic.Model;

namespace CraveOnsiteServer.Data
{
    public class PosJob
    {
        /// <summary>
        /// Unique Identifiers
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Table from which the order is send
        /// </summary>
        [XmlIgnore]
        public Table Table { get; set; }

        /// <summary>
        /// The actual PosOrder
        /// </summary>
        public Posorder PosOrder { get; set; }

        /// <summary>
        /// Original order, for reference
        /// </summary>
        public Order OriginalOrder { get; set; }

        /// <summary>
        /// Deliverypoint from where the order is send
        /// </summary>
        public int DeliverypointId { get; set; }

        /// <summary>
        /// OrderId
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// How many times this job is processed when failed
        /// </summary>
        public int AttemptsMade { get; set; }
    }
}
