﻿using System.IO;
using System.Xml.Serialization;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace CraveOnsiteServer.Data
{
    public class PmsJob
    {
        public enum PmsJobType : int
        {
            Unknown = 0,
            GetFolio = 1,
            GetGuestInformation = 2,
            GetExpressCheckout = 3,
            SetPmsWakeUpStatus = 4,
            SetRoomData = 5,
            EditGuestInformation = 6,
            RoomMove = 7
        }

        public PmsJob()
        {
            this.Identifier = string.Empty;
            this.DeliverypointId = 0;
            this.DeliverypointNumber = string.Empty;
            this.JobType = PmsJobType.Unknown;
            this.AttemptsMade = 0;
            this.RetryDelay = 0;         
        }

        /// <summary>
        /// Unique Identifiers
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// DeliverypointNumber for which this is meant
        /// </summary>
        public string DeliverypointNumber { get; set; }

        /// <summary>
        /// Balance to be used with Express Checkout
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// DeliverypointId for which the action is meant
        /// </summary>
        public int DeliverypointId { get; set; }       

        public PmsJobType JobType
        {
            get;
            set;
        }

        /// <summary>
        /// How many times this job is processed when failed
        /// </summary>
        public int AttemptsMade { get; set; }

        /// <summary>
        /// Number of miliseconds before job gets posted back in queue (default is 30 seconds)
        /// -1 if job can be removed if failed
        /// </summary>
        public int RetryDelay { get; set; }

        /// <summary>
        /// Xml to cached
        /// </summary>
        public string Xml { get; set; }

        /// <summary>
        /// Roomdata 
        /// </summary>
        public RoomData RoomData { get; set; }

        /// <summary>
        /// GuestInformation
        /// </summary>
        public GuestInformation GuestInformation { get; set; }

        /// <summary>
        /// Old room number (used for moving room)
        /// </summary>
        public string DeliverypointNumberOld { get; set; }
    }
}
