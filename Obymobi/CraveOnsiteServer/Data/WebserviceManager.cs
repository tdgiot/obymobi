﻿using System;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Logic.HelperClasses;
using Dionysos;
using Obymobi.Security;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Obymobi;
using Obymobi.Logging;

namespace CraveOnsiteServer.Data
{
    public static class WebserviceManager
    {
        public static T ConvertModel<T>(object obj)
        {
            T toReturn = default(T);
            try
            {
                XmlSerializer s = new XmlSerializer(obj.GetType());

                MemoryStream m = new MemoryStream();
                s.Serialize(m, obj);

                m.Position = 0; // Reset

                XmlSerializer xmlDeserializer = new XmlSerializer(typeof(T));
                toReturn = (T)xmlDeserializer.Deserialize(m);
            }
            catch (Exception ex)
            {
                throw new ObymobiException(XmlHelperResult.SerializeError, ex, "Model: {0}", obj.GetType());
            }

            return toReturn;
        }

        public static long GetTimeStampAndHash(out string hash, params object[] parameters)
        {
            long timestamp = DateTime.UtcNow.ToUnixTime();
            hash = Hasher.GetHashFromParameters(Global.Salt, timestamp, parameters);

            return timestamp;
        }

        #region Webservice Get Methods

        public static Company GetCompany()
        {
            Company toReturn = null;
            string methodCall = string.Empty;
            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, Global.CompanyId);

                methodCall = "WebserviceManager.GetCompany({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, Global.CompanyId, hash);

                CraveService.ObyTypedResultOfCompany wsResult = DataManager.Instance.Webservice.GetCompany(timestamp, Global.MacAddress, Global.CompanyId, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    toReturn = WebserviceManager.ConvertModel<Company>(wsResult.ModelCollection[0]);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return toReturn;
        }

        public static Company[] GetCompanies()
        {
            // DO NOT USE THIS METHOD AS A SOURCE FOR COPY/FEEST CODE - It's the only one that's different from the rest!
            Company[] toReturn = null;
            string methodCall = string.Empty;
            try
            {
                long timestamp = DateTime.UtcNow.ToUnixTime();
                string hash = Hasher.GetHashFromParameters(Obymobi.Constants.ObymobiConstants.GenericSalt, timestamp, Global.CompanyOwner, Global.CompanyPassword);

                methodCall = "WebserviceManager.GetCompanies({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.CompanyOwner, Global.CompanyPassword, hash);

                CraveService.ObyTypedResultOfCompany wsResult = DataManager.Instance.Webservice.GetCompanies(timestamp, Global.CompanyOwner, Global.CompanyPassword, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    toReturn = WebserviceManager.ConvertModel<Company[]>(wsResult.ModelCollection);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return toReturn;
        }

        public static Deliverypoint[] GetDeliverypoints(int deliverypointgroupId)
        {
            Deliverypoint[] toReturn = null;
            string methodCall = string.Empty;
            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, deliverypointgroupId);

                methodCall = "WebserviceManager.GetDeliverypoints({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, deliverypointgroupId, hash);

                CraveService.ObyTypedResultOfDeliverypoint wsResult = DataManager.Instance.Webservice.GetDeliverypoints(timestamp, Global.MacAddress, deliverypointgroupId, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    toReturn = WebserviceManager.ConvertModel<Deliverypoint[]>(wsResult.ModelCollection);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return toReturn;
        }

        public static Terminal GetTerminal()
        {
            Terminal toReturn = null;
            string methodCall = string.Empty;
            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, Global.TerminalId, Dionysos.Global.ApplicationInfo.ApplicationVersion, string.Empty, (int)DeviceType.OnSiteServer);

                methodCall = "WebserviceManager.GetTerminal({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, Global.TerminalId, Dionysos.Global.ApplicationInfo.ApplicationVersion, string.Empty, (int)DeviceType.OnSiteServer, hash);
                
                CraveService.ObyTypedResultOfTerminal wsResult = DataManager.Instance.Webservice.GetTerminal(timestamp, Global.MacAddress, Global.TerminalId, Dionysos.Global.ApplicationInfo.ApplicationVersion, string.Empty, (int)DeviceType.OnSiteServer, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {                    
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    toReturn = WebserviceManager.ConvertModel<Terminal>(wsResult.ModelCollection[0]);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return toReturn;
        }

        public static Product[] GetProducts()
        {
            Product[] toReturn = null;
            string methodCall = string.Empty;
            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress);

                methodCall = "WebserviceManager.GetProducts({0}, {1}, {2})".FormatSafe(timestamp, Global.MacAddress, hash);

                CraveService.ObyTypedResultOfProduct result = DataManager.Instance.Webservice.GetProducts(timestamp, Global.MacAddress, hash);
                if (result != null && result.ResultCode == (int)GenericWebserviceCallResult.Success && result.ModelCollection.Length > 0)
                {
                    toReturn = WebserviceManager.ConvertModel<Product[]>(result.ModelCollection);
                }
                else
                {
                    if (result == null)
                    {
                        ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                    }
                    else
                    {
                        ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, result.ResultCode, result.ResultMessage);
                    }

                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }
            return toReturn;
        }

        public static PosIntegrationInformation GetPosIntegrationInformation()
        {
            PosIntegrationInformation toReturn = null;
            string methodCall = string.Empty;
            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress);

                methodCall = "WebserviceManager.GetPosIntegrationInformation({0}, {1}, {2})".FormatSafe(timestamp, Global.MacAddress, hash);

                CraveService.ObyTypedResultOfPosIntegrationInformation result = DataManager.Instance.Webservice.GetPosIntegrationInformation(timestamp, Global.MacAddress, hash);
                if (result != null && result.ResultCode == (int)GenericWebserviceCallResult.Success && result.ModelCollection.Length > 0)
                {
                    toReturn = WebserviceManager.ConvertModel<PosIntegrationInformation>(result.ModelCollection[0]);
                }
                else
                {
                    if (result == null)
                    {
                        ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);                        
                    }
                    else
                    {
                        ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, result.ResultCode, result.ResultMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }
            return toReturn;
        }

        public static Order[] GetOrders()
        {
            Order[] toReturn = null;
            string methodCall = string.Empty;
            try
            {
                // (long timestamp, string macAddress, int statusCode, string statusMessage, string version, bool isConsole, bool isMasterConsole, string hash)                
                const int statusCode = (int)GenericWebserviceCallResult.Success;
                string statusMessage = string.Empty;
                string version = Dionysos.Global.ApplicationInfo.ApplicationVersion.Substring(2);
                const bool isConsole = false;
                const bool isMasterConsole = false;

                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, statusCode, statusMessage, version, isConsole, isMasterConsole);

                methodCall = "WebserviceManager.GetOrders({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})".FormatSafe(timestamp, Global.MacAddress, statusCode, statusMessage, version, isConsole, isMasterConsole, hash);

                CraveService.ObyTypedResultOfOrder wsResult = 
                    DataManager.Instance.Webservice.GetOrders(timestamp, Global.MacAddress, statusCode, statusMessage, version, isConsole, isMasterConsole, hash);

                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    toReturn = WebserviceManager.ConvertModel<Order[]>(wsResult.ModelCollection);
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return toReturn;
        }

		public static Netmessage[] GetNetmessages()
		{
			Netmessage[] toReturn = null;
			string methodCall = string.Empty;
			try
			{
				string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, 0, 0, 0, 0, Global.TerminalId);

				methodCall = "WebserviceManager.GetNetmessages({0}, {1}, 0, 0, 0, 0, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, Global.TerminalId, hash);

				CraveService.ObyTypedResultOfNetmessage wsResult = DataManager.Instance.Webservice.GetNetmessages(timestamp, Global.MacAddress, 0, 0, 0, 0, Global.TerminalId, hash);
				if (wsResult == null)
				{
					ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
				}
				else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
				{
					ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
				}
				else
                    toReturn = WebserviceManager.ConvertModel<Netmessage[]>(wsResult.ModelCollection);
			}
			catch (Exception ex)
			{
				ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
			}

			return toReturn;
		}

        #endregion

        #region Webservice Set Methods

        public static void UploadLogs()
        {
            DateTime since = DateTime.Today.AddDays(-7);

            try
            {
                List<string> files = DesktopLogger.GetLogFiles(since);

                foreach (string file in files)
                {
                    WriteToLog log = new WriteToLog();
                    log.TerminalLogType = (int)TerminalLogType.ShippedLog;
                    log.Message = file;
                    log.Log = DesktopLogger.ReadLog(file);

                    WebserviceManager.WriteToLog(log);
                }
            }
            catch(Exception ex)
            {
                ConsoleLogger.WriteToLog("WebserviceManager.UploadLogs ERROR - " + ex.Message);
            }
            
        }
        
        public static bool WriteToLog(WriteToLog logObj)
        {
            bool result = false;
            string methodCall = string.Empty;
            // (long timestamp, string macAddress, int terminalId, int orderId, string orderGuid, int type, string message, string status, string log, string hash)
            try
            {
                string hash;
                int orderId = logObj.orderId ?? -1;
                logObj.Message = logObj.Message.Replace(Environment.NewLine, "\n");
                logObj.Log = logObj.Log.Replace(Environment.NewLine, "\n");
                logObj.Log = logObj.Log.Replace("\n\r", "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, Global.CompanyId, Global.TerminalId, orderId, logObj.OrderGuid, logObj.TerminalLogType, logObj.Message, logObj.Status, logObj.Log);

                methodCall = "WebserviceManager.WriteToLog({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9})"
                    .FormatSafe(timestamp, Global.MacAddress, Global.CompanyId, Global.TerminalId, orderId, logObj.OrderGuid, logObj.TerminalLogType, logObj.Message, logObj.Status, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult =
                    DataManager.Instance.Webservice.WriteToLog(timestamp, Global.MacAddress, Global.CompanyId, Global.TerminalId, orderId, logObj.OrderGuid, logObj.TerminalLogType, logObj.Message, logObj.Status, logObj.Log, hash);
                if (wsResult != null && wsResult.ResultCode == (int)GenericWebserviceCallResult.Success)
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool SaveOrder(Order order)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = XmlHelper.Serialize(order).Replace(Environment.NewLine, "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, xml);

                methodCall = "WebserviceManager.SaveOrder({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);
                
                CraveService.ObyTypedResultOfOrder wsResult = DataManager.Instance.Webservice.SaveOrder(timestamp, Global.MacAddress, xml, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool SetRoutestephandlerSaveStatuses(OrderRoutestephandlerSaveStatus[] routestephandlerSaveStatuses)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = XmlHelper.Serialize(routestephandlerSaveStatuses).Replace(Environment.NewLine, "\n");

                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, xml);

                methodCall = "WebserviceManager.SetRoutestephandlerSaveStatuses({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                CraveService.ObyTypedResultOfOrderRoutestephandlerSaveStatus wsResult = DataManager.Instance.Webservice.SetRoutestephandlerSaveStatuses(timestamp, Global.MacAddress, xml, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool SetTerminalOperationMode(TerminalOperationMode operationMode)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, (int)operationMode);

                methodCall = "WebserviceManager.SetTerminalOperationMode({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, (int)operationMode, hash);

                CraveService.ObyTypedResultOfTerminalOperationMode wsResult = DataManager.Instance.Webservice.SetTerminalOperationMode(timestamp, Global.MacAddress, (int)operationMode, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool SetTerminalOperationState(TerminalOperationState operationState)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, (int)operationState);

                methodCall = "WebserviceManager.SetTerminalOperationState({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, (int)operationState, hash);

                CraveService.ObyTypedResultOfTerminalOperationState wsResult = DataManager.Instance.Webservice.SetTerminalOperationState(timestamp, Global.MacAddress, (int)operationState, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool SetPmsFolio(Folio folio)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = folio.toXml().Replace(Environment.NewLine, "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, xml);

                methodCall = "WebserviceManager.SetPmsFolio({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = DataManager.Instance.Webservice.SetPmsFolio(timestamp, Global.MacAddress, xml, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool SetPmsGuestInformation(GuestInformation guestInformation)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = guestInformation.toXml().Replace(Environment.NewLine, "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, xml);

                methodCall = "WebserviceManager.SetPmsGuestInformation({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = DataManager.Instance.Webservice.SetPmsGuestInformation(timestamp, Global.MacAddress, xml, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool SetPmsCheckedIn(GuestInformation guestInformation)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = guestInformation.toXml().Replace(Environment.NewLine, "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, xml);

                methodCall = "WebserviceManager.SetPmsCheckedIn({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = DataManager.Instance.Webservice.SetPmsCheckedIn(timestamp, Global.MacAddress, xml, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool SetPmsCheckedOut(string deliverypointNumber)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, 0, deliverypointNumber);

                methodCall = "WebserviceManager.SetPmsCheckedOut({0}, {1}, {2}, {3}, {4})".FormatSafe(timestamp, Global.MacAddress, 0, deliverypointNumber, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = DataManager.Instance.Webservice.SetPmsCheckedOut(timestamp, Global.MacAddress, 0, deliverypointNumber, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool SetPmsExpressCheckedOut(Checkout checkoutInfo)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = checkoutInfo.toXml().Replace(Environment.NewLine, "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, xml);

                methodCall = "WebserviceManager.SetPmsExpressCheckedOut({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = DataManager.Instance.Webservice.SetPmsExpressCheckedOut(timestamp, Global.MacAddress, xml, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool SetPmsWakeUp(WakeUpStatus wakeUpStatus)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = wakeUpStatus.toXml().Replace(Environment.NewLine, "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, xml);

                methodCall = "WebserviceManager.SetPmsWakeUp({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = DataManager.Instance.Webservice.SetPmsWakeUp(timestamp, Global.MacAddress, xml, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool ClearPmsWakeUp(WakeUpStatus wakeUpStatus)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = wakeUpStatus.toXml().Replace(Environment.NewLine, "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, xml);

                methodCall = "WebserviceManager.SetPmsWakeUp({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = DataManager.Instance.Webservice.ClearPmsWakeUp(timestamp, Global.MacAddress, xml, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }        

		public static void ProcessNetmessages()
		{
            string methodCall = string.Empty;
			try
			{
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, 0, 0, 0, 0, Global.TerminalId);

                methodCall = "WebserviceManager.ProcessNetmessages({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})".FormatSafe(timestamp, Global.MacAddress, 0, 0, 0, 0, Global.TerminalId, hash);
        
				DataManager.Instance.Webservice.ProcessNetmessages(timestamp, Global.MacAddress, 0, 0, 0, 0, Global.TerminalId, hash);
			}
			catch (Exception ex)
			{
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
			}
		}

		public static bool VerifyNetmessage(string netmessageGuid)
		{
            string methodCall = string.Empty;
			bool result = false;

			try
			{
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, netmessageGuid);

				methodCall = "WebserviceManager.VerifyNetmessageByGuid({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, netmessageGuid, string.Empty, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = DataManager.Instance.Webservice.VerifyNetmessageByGuid(timestamp, Global.MacAddress, netmessageGuid, string.Empty, hash);
                
				if (wsResult == null)
				{
					ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
				}
				else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
				{
					ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
			}

			return result;
		}

        public static bool CompleteNetmessage(string netmessageGuid)
        {
            string methodCall = string.Empty;
            bool result = false;

            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, netmessageGuid);

                string errorMessage = string.Empty;
                methodCall = "WebserviceManager.CompleteNetmessageByGuid({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, netmessageGuid, errorMessage, string.Empty, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = DataManager.Instance.Webservice.CompleteNetmessageByGuid(timestamp, Global.MacAddress, netmessageGuid, errorMessage, string.Empty, hash);
                
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool SaveGuestInformations(GuestInformation[] guestInformations)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = XmlHelper.Serialize(guestInformations).Replace(Environment.NewLine, "\n");

                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, xml);

                methodCall = "WebserviceManager.SaveGuestInformations({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = DataManager.Instance.Webservice.SaveGuestInformations(timestamp, Global.MacAddress, xml, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        public static bool SaveFolios(Folio[] folios)
        {
            bool result = false;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = XmlHelper.Serialize(folios).Replace(Environment.NewLine, "\n");

                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, xml);

                methodCall = "WebserviceManager.SaveFolios({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = DataManager.Instance.Webservice.SaveFolios(timestamp, Global.MacAddress, xml, hash);
                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
                else
                    result = true;
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return result;
        }

        #endregion

        #region Pos synchronisation methods

        // CleanUpPosData

        public static CraveService.ObyTypedResultOfPoscategory SetPosCategories(long batchId, Poscategory[] poscategories)
        {
            CraveService.ObyTypedResultOfPoscategory wsResult = null;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = XmlHelper.Serialize(poscategories).Replace(Environment.NewLine, "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, batchId, xml);

                methodCall = "WebserviceManager.SetPosCategories({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                wsResult = DataManager.Instance.Webservice.SetPosCategories(timestamp, Global.MacAddress, batchId, xml, hash); 

                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return wsResult;
        }
                
        public static CraveService.ObyTypedResultOfPosdeliverypointgroup SetPosDeliverypointgroups(long batchId, Posdeliverypointgroup[] posdeliveyrpointgroups)
        {
            CraveService.ObyTypedResultOfPosdeliverypointgroup wsResult = null;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = XmlHelper.Serialize(posdeliveyrpointgroups).Replace(Environment.NewLine, "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, batchId, xml);

                methodCall = "WebserviceManager.SetPosDeliverypointgroups({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                wsResult = DataManager.Instance.Webservice.SetPosDeliverypointgroups(timestamp, Global.MacAddress, batchId, xml, hash);

                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return wsResult;
        }
        
        public static CraveService.ObyTypedResultOfPosdeliverypoint SetPosDeliverypoints(long batchId, Posdeliverypoint[] posdeliverypoints)
        {
            CraveService.ObyTypedResultOfPosdeliverypoint wsResult = null;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = XmlHelper.Serialize(posdeliverypoints).Replace(Environment.NewLine, "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, batchId, xml);

                methodCall = "WebserviceManager.SetPosDeliverypoint({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                wsResult = DataManager.Instance.Webservice.SetPosDeliverypoints(timestamp, Global.MacAddress, batchId, xml, hash);

                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return wsResult;
        }

        public static CraveService.ObyTypedResultOfPosproduct SetPosProducts(long batchId, List<Posproduct> posproducts)
        {
            CraveService.ObyTypedResultOfPosproduct wsResult = null;
            string methodCall = string.Empty;
            try
            {
                string hash;
                string xml = XmlHelper.Serialize(posproducts).Replace(Environment.NewLine, "\n");
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress, batchId, xml);

                methodCall = "WebserviceManager.SetPosproduct({0}, {1}, {2}, {3})".FormatSafe(timestamp, Global.MacAddress, xml, hash);

                wsResult = DataManager.Instance.Webservice.SetPosProducts(timestamp, Global.MacAddress, batchId, xml, hash);

                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return wsResult;
        }

        public static CraveService.ObyTypedResultOfSimpleWebserviceResult CleanUpPosData()
        {
            CraveService.ObyTypedResultOfSimpleWebserviceResult wsResult = null;
            string methodCall = string.Empty;
            try
            {
                string hash;
                long timestamp = WebserviceManager.GetTimeStampAndHash(out hash, Global.MacAddress);

                methodCall = "WebserviceManager.CleanUpPosData({0}, {1}, {2})".FormatSafe(timestamp, Global.MacAddress, hash);

                wsResult = DataManager.Instance.Webservice.CleanUpPosData(timestamp, Global.MacAddress, hash);

                if (wsResult == null)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result is NULL", methodCall);
                }
                else if (wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                {
                    ConsoleLogger.WriteToLog("ERROR: {0} - Result: {1} - {2}", methodCall, wsResult.ResultCode, wsResult.ResultMessage);
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("ERROR: {0} Exception: {1} - {2}", methodCall, ex.Message, (ex.InnerException != null) ? ex.InnerException.Message : "");
            }

            return wsResult;
        } 

        #endregion
    }
}
