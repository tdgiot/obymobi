﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using CraveOnsiteServer.Data;
using CraveOnsiteServer.Logic.Enum;
using Dionysos;
using Dionysos.IO;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Configuration;
using Obymobi.Printing;
using Global = CraveOnsiteServer.Data.Global;
using System.Net.NetworkInformation;
using CraveOnsiteServer.Data.Managers;
using Dionysos.Configuration;
using Obymobi.Constants;
using Obymobi.Logging;
using Obymobi.Integrations.POS;
using Obymobi.Integrations.PMS.Configuration;

namespace CraveOnsiteServer
{
	public class ConsoleApplication
	{
		public static ConsoleApplication Instance { get; private set; }
		public static Dictionary<string, object> Arguments;		

		public ConsoleApplication(string applicationPath, Dictionary<string, object> args)
		{
            TaskScheduler.UnobservedTaskException += ConsoleApplication.TaskScheduler_UnobservedTaskException;

            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                   | SecurityProtocolType.Tls11
                                                   | SecurityProtocolType.Tls12
                                                   | SecurityProtocolType.Ssl3;

            ConsoleApplication.Instance = this;
			ConsoleApplication.Arguments = args;

			Global.IsConsoleMode = ConsoleLogger.IsConsoleMode = (bool)ConsoleApplication.Arguments["console"];

		    if (Environment.CommandLine.Contains("-debug"))
		    {
		        DesktopLogger.SetLoggingLevel(LoggingLevel.Debug);
		    }

		    ConsoleLogger.WriteToLog(@"======================================================");
		    ConsoleLogger.WriteToLog(@"Starting Crave On-Site Server {0}{1}", Dionysos.Global.ApplicationInfo.ApplicationVersion, (TestUtil.IsPcDeveloper) ? " (Developer Edition)" : "");
		    ConsoleLogger.WriteToLog(@"======================================================");
			ConsoleLogger.WriteToLog(@"Base dir: {0}", applicationPath);
			ConsoleLogger.WriteToLog("");

			Global.RootDirectory = applicationPath;
			if (Global.RootDirectory == null)
			{
				ConsoleLogger.WriteToLog("ERROR: Could not determine application path!");
			}
		}

        static void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            if (e.Exception == null)
            {
                e.SetObserved();
                return;
            }

            e.Exception.Flatten().Handle(ex =>
                               {
                                   ConsoleLogger.WriteToLog("InnerException: " + ex.Message);
                                   ConsoleLogger.WriteToLog("InnerException StackTrace: " + ex.ProcessStackTrace());
                                   ConsoleLogger.WriteToLog("InnerException ToString: " + ex.ToString());
                                   return true;
                               });
            e.SetObserved();
        }

		public bool Start()
		{
			Stopwatch startupTimer = new Stopwatch();
			startupTimer.Start();
                       
            // Initialize Configuration and Globals
            ConsoleLogger.WriteToLog("Initializing configuration and globals...");
            this.InitializeGlobals();

			// Start Webservice Queue
			ConsoleLogger.WriteToLog("Starting webservice queue...");
			DataManager.Instance.WebserviceQueue.StartConsumer();

            // Load data
            ConsoleLogger.WriteToLog("Preloading terminal info...");
		    DataManager.Instance.Load();

            // Initialize CDN manager
            ConsoleLogger.WriteToLog("Initializing CDN manager...");
            CdnManager.Instance.Initialize(DataManager.Instance.CompanyManager.Company.CloudStorageAccounts, Global.CdnRootContainer);

		    // Initialize PmsConnector
			ConsoleLogger.WriteToLog("Initializing PMS connector...");
            PmsManager.Instance.Initialize();

			// Initialize PosConnector
			ConsoleLogger.WriteToLog("Initializing POS connector...");
            PosManager.Instance.Initialize();

			// Setting up printer (for when we need it)
			Company companyObj = DataManager.Instance.CompanyManager.Company;
			Terminal terminalObj = DataManager.Instance.TerminalManager.GetTerminal();
			PrintManager printManager = PrintManager.GetSingleton();
			printManager.Init(terminalObj.ReceiptTypes, companyObj.Name, companyObj.Addressline1, companyObj.Addressline2, companyObj.Addressline3, companyObj.Telephone, companyObj.Fax, companyObj.Website, companyObj.Email, "");

			startupTimer.Stop();
			ConsoleLogger.WriteToLog("Server initialized in {0} seconds.", (startupTimer.ElapsedMilliseconds / 1000));

			// Run server
			ConsoleLogger.WriteToLog("Starting network acceptor...");
			Server.Server.Instance.StartServer();

			return true;
		}

		private void InitializeGlobals()
		{
			// Set the configuration provider
			XmlConfigurationProvider provider = new XmlConfigurationProvider(LogCallback) { ManualConfigFilePath = Path.Combine(Global.RootDirectory, "CraveOnSiteServer.config") };

			Dionysos.Global.ConfigurationProvider = provider;
			Dionysos.Global.ConfigurationInfo.Add(new CraveCloudConfigInfo());
			Dionysos.Global.ConfigurationInfo.Add(new CraveOnSiteServerConfigInfo());
			Dionysos.Global.ConfigurationInfo.Add(new POSConfigurationInfo());
			Dionysos.Global.ConfigurationInfo.Add(new PmsConfigurationInfo());

			// Check if Company password is saved encrypted
			string passwordString = ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.CompanyPassword);
			string passwordDecrypt1 = "";
			string passwordDecrypt2 = "";
			try
			{
				passwordDecrypt1 = Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(passwordString);
				passwordDecrypt2 = ConfigurationManager.GetPassword(CraveOnSiteServerConfigConstants.CompanyPassword);
			}
			catch (Exception)
			{
				ConsoleLogger.WriteToLog("Failed to decrypt company password! Might be because password wasn't encrypted (yet).");
			}

			if (passwordDecrypt1.Length == 0 || !passwordDecrypt1.Equals(passwordDecrypt2))
			{
				ConfigurationManager.SetPassword(CraveOnSiteServerConfigConstants.CompanyPassword, passwordString);
				ConsoleLogger.WriteToLog("Company password was not saved encrypted. Now it is :)");
			}

            // Get the MAC address used in communication with the Webservice
            Global.MacAddress = ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.MacAddress);
            if (Global.MacAddress.IsNullOrWhiteSpace())
            {
                NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
                NetworkInterface firstOperational = interfaces.FirstOrDefault(i => i.OperationalStatus == OperationalStatus.Up);
                if (firstOperational != null)
                {
                    // Format mac lower case with : seperators.
                    string macRaw = firstOperational.GetPhysicalAddress().ToString().ToLower();
                    string macFormatted = macRaw;
                    int insertedCount = 0;
                    if(!macRaw.Contains(":"))
                    {
                        for (int i = 2; i < macRaw.Length; i = i + 2)
                            macFormatted = macFormatted.Insert(i + insertedCount++, ":");
                    }

                    ConfigurationManager.SetValue(CraveOnSiteServerConfigConstants.MacAddress, macFormatted);
                    Global.MacAddress = ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.MacAddress);
                }
                else
                {
                    ConsoleLogger.WriteToLog("A Mac-address is required for Webservice communication and can't be determined.");
                    ConsoleLogger.WriteToLog("Verify you have at least one working network connection.");
                    throw new Exception("Can't start: Mac Address can't be retrieved from network intefaces.");
                }
            }                        

			// Load configuration in Global
			Global.CompanyOwner = ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.CompanyOwner);
			Global.CompanyPassword = ConfigurationManager.GetPassword(CraveOnSiteServerConfigConstants.CompanyPassword);
			Global.TerminalId = ConfigurationManager.GetInt(CraveOnSiteServerConfigConstants.TerminalId);
			Global.CompanyId = ConfigurationManager.GetInt(CraveOnSiteServerConfigConstants.CompanyId);
			Global.CustomerId = ConfigurationManager.GetInt(CraveOnSiteServerConfigConstants.CustomerId);
			Global.CompanyIdLastModifiedTicks = ConfigurationManager.GetInt(CraveOnSiteServerConfigConstants.CompanyIdLastModifiedTicks);
			Global.DeliverypointgroupId = ConfigurationManager.GetInt(CraveOnSiteServerConfigConstants.DeliverypointgroupId);
			Global.RequestInterval = ConfigurationManager.GetInt(CraveOnSiteServerConfigConstants.RequestInterval);

			Global.ServerState = ServerState.OFFLINE;

			Global.Status = new Obymobi.Logic.Status.OnsiteServerStatus();
            Global.Status.CheckInternetAvailability = ConfigurationManager.GetBool(CraveOnSiteServerConfigConstants.CheckInternetAvailability);
            Global.Status.UrlProtocol = (UrlProtocol)ConfigurationManager.GetInt(CraveOnSiteServerConfigConstants.UrlProtocol);
			Global.Status.Logged += (this.Status_Logged);

			if (ConfigurationManager.GetString(CraveCloudConfigConstants.CloudEnvironment) == "NOTSET")
			{
				ConsoleLogger.WriteToLog("Upgrading to CloudEnvironment Configuration");

				string webserviceUrl = ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.WebserviceUrl1);

				if (webserviceUrl.Contains("app.", StringComparison.InvariantCultureIgnoreCase))
					ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.ProductionPrimary.ToString());
				else if (webserviceUrl.Contains("dev.", StringComparison.InvariantCultureIgnoreCase))
					ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.Development.ToString());
				else if (webserviceUrl.Contains("test.", StringComparison.InvariantCultureIgnoreCase))
					ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.Test.ToString());
				else
				{
					ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, CloudEnvironment.Manual.ToString());
					ConfigurationManager.SetValue(CraveCloudConfigConstants.ManualWebserviceBaseUrl, webserviceUrl);
				}

				ConsoleLogger.WriteToLog("CloudEnvironment Configuration upgraded to: {0}", ConfigurationManager.GetString(CraveCloudConfigConstants.CloudEnvironment));
			}

            ConsoleLogger.WriteToLog("Check Cloud Status...");
            Global.Status.Refresh();

            if (Global.Status.IsWebserviceAvailable)
            {
                ConsoleLogger.WriteToLog("Initializing webservice using url '{0}'...", Global.Status.CraveServiceUrl);
                DataManager.Instance.InitializeWebservice();
            }

            // Always perform a GetCompanies to download the salt (it might have changed)
            Company[] companies = WebserviceManager.GetCompanies();
            if (companies == null)
            {
                ConsoleLogger.WriteToLog("Salt is missing: GetCompanies gave null/exception.");
            }
            else if (companies.Length == 0)
            {
                ConsoleLogger.WriteToLog("Salt is missing: GetCompanies gave 0 results.");
            }
            else
            {
                Company company = companies.FirstOrDefault(c => c.CompanyId == Global.CompanyId);
                if (company == null)
                {
                    ConsoleLogger.WriteToLog("Salt is missing: GetCompanies didn't include a company for this terminal with CompanyId '{0}'.", Global.CompanyId);
                }
                else if (company.Salt.IsNullOrWhiteSpace())
                {
                    ConsoleLogger.WriteToLog("Salt is missing: GetCompanies returned a Company without salt for CompanyId '{0}'.", Global.CompanyId);
                }                
                else
                {
                    Global.Salt = company.Salt;
                    string encrypted = Dionysos.Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(company.Salt);
                    ConfigurationManager.SetValue(CraveOnSiteServerConfigConstants.Salt, encrypted);

                    if (company.SaltPms.IsNullOrWhiteSpace())
                    {
                        ConsoleLogger.WriteToLog("Salt is missing: GetCompanies returned a Company without saltPms for CompanyId '{0}'.", Global.CompanyId);
                    }
                    else
                    {
                        string decryptedSaltPms = Dionysos.Security.Cryptography.Cryptographer.DecryptUsingCBC(company.SaltPms);
                        Global.SaltPms = decryptedSaltPms;    
                    }

                    Global.Status.NocStatusTextFile = string.Format(ObymobiConstants.NocStatusTextFile_Company, company.CompanyId);
                }
            }
            
            if (Global.Salt.IsNullOrWhiteSpace())
            {
                Global.Salt = Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.Salt));
                if (Global.Salt.IsNullOrWhiteSpace())
                {
                    ConsoleLogger.WriteToLog("Salt is missing.");
                    throw new Exception("Salt is missing");
                }
            }

			// Check if the companyId changed manually. If so, reset the timestamps
			if (Global.CompanyId != Global.CompanyIdLastModifiedTicks)
			{
				ConfigurationManager.SetValue(CraveOnSiteServerConfigConstants.CompanyIdLastModifiedTicks, Global.CompanyId);
				ConfigurationManager.SetValue(CraveOnSiteServerConfigConstants.PosIntegrationInformationLastModifiedTicks, -1);

                string directory = DataManager.CreateDirectory("Data");
                DirectoryInfo info = new DirectoryInfo(directory);
                FileUtil.DeleteFilesRecursive(info, false);
			}
			
            Global.CompanyIdLastModifiedTicks = ConfigurationManager.GetInt(CraveOnSiteServerConfigConstants.CompanyIdLastModifiedTicks);
			Global.PosIntegrationInformationLastModifiedTicks = ConfigurationManager.GetLong(CraveOnSiteServerConfigConstants.PosIntegrationInformationLastModifiedTicks);
            Global.CdnRootContainer = this.GetCdnRootContainerForEnvironment(Global.Status.CloudEnvironment);

			ConsoleLogger.WriteToLog("");
			ConsoleLogger.WriteToLog("************************************************");
			ConsoleLogger.WriteToLog("** Version:               {0}", Dionysos.Global.ApplicationInfo.ApplicationVersion);
			ConsoleLogger.WriteToLog("** Root Directory:        {0}", Global.RootDirectory);
			ConsoleLogger.WriteToLog("**");
			ConsoleLogger.WriteToLog("** Company Id:            {0}", Global.CompanyId);
			ConsoleLogger.WriteToLog("** Company Owner:         {0}", Global.CompanyOwner);
			ConsoleLogger.WriteToLog("** Terminal Id:           {0}", Global.TerminalId);
			ConsoleLogger.WriteToLog("** Customer Id:           {0}", Global.CustomerId);
			ConsoleLogger.WriteToLog("**");
			ConsoleLogger.WriteToLog("** Cloud Environment:     {0}", Global.Status.CloudEnvironment);
			if (Global.Status.CloudEnvironment == CloudEnvironment.Manual)
				ConsoleLogger.WriteToLog("** Webservice:     {0}", Global.Status.ManualWebserviceBaseUrl);
			ConsoleLogger.WriteToLog("** Request Interval:      {0}", Global.RequestInterval);
            ConsoleLogger.WriteToLog("** Cdn Root Container:    {0}", Global.CdnRootContainer);
			ConsoleLogger.WriteToLog("**");

			if (Global.PosIntegrationInformationLastModifiedTicks < 0)
				ConsoleLogger.WriteToLog("** Local Pos info Last Modified Date: No local pos info");
			else
				ConsoleLogger.WriteToLog("** Local Pos info Last Modified Date: {0}", new DateTime(Global.PosIntegrationInformationLastModifiedTicks));

			ConsoleLogger.WriteToLog("** Local Last CompanyId used: {0}", Global.CompanyIdLastModifiedTicks);
			ConsoleLogger.WriteToLog("************************************************");
			ConsoleLogger.WriteToLog("");
		}

        private void LogCallback(string obj)
        {
            ConsoleLogger.WriteToLog(obj);
        }

        public void Close()
		{
			Server.Server.Instance.StopServer();
		}

		public bool Restart()
		{
			DataManager.Instance.Reset();

			ConsoleLogger.WriteToLog("*******************************************");
			ConsoleLogger.WriteToLog("*************** RESTARTING ****************");
			ConsoleLogger.WriteToLog("*******************************************");

			Server.Server.Reset();
		    return this.Start();
		}

		private void Status_Logged(object sender, LoggingLevel level, string log)
		{
			ConsoleLogger.WriteToLog(log);
		}

        public string GetCdnRootContainerForEnvironment(CloudEnvironment environment)
        {
            string bucketName;
            switch (environment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    bucketName = ObymobiConstants.PrimaryAmazonRootContainer;
                    break;
                case CloudEnvironment.Test:
                    bucketName = ObymobiConstants.TestAmazonRootContainer;
                    break;
                case CloudEnvironment.Development:
                    bucketName = ObymobiConstants.DevelopmentAmazonRootContainer;
                    break;
                case CloudEnvironment.Manual:
                    bucketName = string.Format("{0}", ConfigurationManager.GetString(CraveOnSiteServerConfigConstants.ManualCdnRootContainer));
                    break;
                default:
                    throw new NotImplementedException(environment.ToString());
            }

            return bucketName;
        }
	}
}
