﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Obymobi.Logging;
using System.Threading;

namespace CraveOnsiteServer
{
    public partial class CraveOnsiteServer : ServiceBase
    {
        private static ConsoleApplication application;
        private static Dictionary<string, object> arguments;

        public CraveOnsiteServer(Dictionary<string, object> args)
        {
            InitializeComponent();

            arguments = args;
        }

        protected override void OnStart(string[] args)
        {
            ConsoleLogger.IsConsoleMode = false;
            ConsoleLogger.WriteToLog("Service - OnStart");

            string applicationPath = Assembly.GetExecutingAssembly().Location;
            applicationPath = Path.GetDirectoryName(applicationPath);
            application = new ConsoleApplication(applicationPath, arguments);

            Task.Factory.StartNew(() =>
            {
                try
                {
                    if (!application.Start())
                    {
                        application.Close();
                    }
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteToLog("GLOBAL EXCEPTION: {0}\n\n{1}", ex.Message, ex.StackTrace);
                }

                try
                {
                    // Kill the process if it is already running
                    //ServiceInstaller.StopService(Program.applicationName);
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteToLog(ex.Message);
                }
            });
        }

        protected override void OnStop()
        {
            ConsoleLogger.IsConsoleMode = false;
            ConsoleLogger.WriteToLog("Service - OnStop");

            try
            {
                ConsoleApplication.Instance.Close();
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("Error while closing application: {0}", ex.Message);
            }

            //Task.Factory.StartNew(ApplicationStop);
        }
    }
}
