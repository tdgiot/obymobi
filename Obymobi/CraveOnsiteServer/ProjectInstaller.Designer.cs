﻿namespace CraveOnsiteServer
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CraveOnsiteServerProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.CraveOnsiteServerServiceInstaller1 = new System.ServiceProcess.ServiceInstaller();
            // 
            // CraveOnsiteServerProcessInstaller1
            // 
            this.CraveOnsiteServerProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.CraveOnsiteServerProcessInstaller1.Password = null;
            this.CraveOnsiteServerProcessInstaller1.Username = null;
            // 
            // CraveOnsiteServerServiceInstaller1
            // 
            this.CraveOnsiteServerServiceInstaller1.Description = "Serves as proxy between Crave products and Point-of-Sale";
            this.CraveOnsiteServerServiceInstaller1.DisplayName = "Crave On-site Server";
            this.CraveOnsiteServerServiceInstaller1.ServiceName = "CraveOnsiteServer";
            this.CraveOnsiteServerServiceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.CraveOnsiteServerProcessInstaller1,
            this.CraveOnsiteServerServiceInstaller1});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller CraveOnsiteServerProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller CraveOnsiteServerServiceInstaller1;
    }
}