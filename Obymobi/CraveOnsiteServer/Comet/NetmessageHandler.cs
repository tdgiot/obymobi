﻿using System;
using CraveOnsiteServer.Data;
using CraveOnsiteServer.Data.Queue;
using CraveOnsiteServer.Server;
using Obymobi.Enums;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logging;

namespace CraveOnsiteServer.Comet
{
    public class NetmessageHandler
    {
        private readonly CometHandler client;

        public NetmessageHandler(CometHandler cometHandler = null)
        {
            client = cometHandler;
        }

        public void OnNetmessageReceived(Netmessage netmessage)
        {                        
            try
            {
                switch (netmessage.GetMessageType())
                {
                    case NetmessageType.RoutestephandlerStatusUpdated:
                        RoutestephandlerStatusUpdated(netmessage);
                        break;
                    case NetmessageType.GetPmsFolio:
                        GetPmsFolio(netmessage);
                        break;
                    case NetmessageType.GetPmsGuestInformation:
                        GetPmsGuestInformation(netmessage);
                        break;
                    case NetmessageType.GetPmsExpressCheckout:
                        GetPmsExpressCheckout(netmessage);
                        break;
                    case NetmessageType.AuthenticateResult:
                        AuthenticateResult(netmessage);
                        break;
                    case NetmessageType.TerminalCommand:
                        TerminalCommand(netmessage);
                        break;
                    case NetmessageType.Ping:
                        Pong();
                        break;
                    case NetmessageType.Disconnect:
                        Disconnect();
                        break;
                    default:
                        ConsoleLogger.WriteToLog("NetmessageHandler - MessageType {0} is not handled.", netmessage.GetMessageType());
                        break;
                }
            }
            catch(Exception ex)
            {
                int commandInt;
                TerminalCommand command;
                if (netmessage.MessageType == NetmessageType.TerminalCommand && int.TryParse(netmessage.FieldValue1, out commandInt) && EnumUtil.TryParse(commandInt, out command))
                {
                    ConsoleLogger.WriteToLog("NetmessageHandler - Message Received and ended in exception, Terminal Command: '{0}', FV1: '{1}', FV2: '{2}', FV3: '{3}'",
                        command.ToString(), netmessage.FieldValue1, netmessage.FieldValue2, netmessage.FieldValue3);                                        
                }
                else
                {
                    ConsoleLogger.WriteToLog("NetmessageHandler - Message Received and ended in exception, Type: '{0}', FV1: '{1}', FV2: '{2}', FV3: '{3}'",
                        netmessage.MessageType, netmessage.FieldValue1, netmessage.FieldValue2, netmessage.FieldValue3);
                }

                ConsoleLogger.WriteToLog("NetmessageHandler - Exception: {0}", ex.Message);

                throw;
            }

        }

        private void Disconnect()
        {
            if (this.client != null)
            {
                this.client.Disconnect(true);
            }
        }

        private void Pong()
        {
            if (this.client != null && this.client.IsConnectionReady())
            {
                this.client.Pong();
            }
        }

        private void AuthenticateResult(Netmessage message)
        {
            var netmessage = message.ConvertTo<NetmessageAuthenticateResult>();

            if (this.client != null)
            {
                if (netmessage.IsAuthenticated)
                {
                    ConsoleLogger.WriteToLog("Authenticated to CometServer");
                    this.client.SetAuthenticated(true);

                    var clientType = new NetmessageSetClientType();
                    clientType.ClientType = NetmessageClientType.OnsiteServer;
                    clientType.TerminalId = Global.TerminalId;

                    this.client.SendMessage(clientType);
                }
                else
                {
                    this.client.SetAuthenticated(false);
                    ConsoleLogger.WriteToLog("Failed to autenticate. Message: {0}", message.FieldValue2);
                }
            }
        }

        private void GetPmsFolio(Netmessage message)
        {
            message = NetmessageHelper.CreateTypedNetmessageModelFromModel(message);
            ConsoleLogger.WriteToLog("NetmessageHandler - GetPmsFolio - {0} - {1}", message.NetmessageId, message.ToString());
            if (WebserviceManager.VerifyNetmessage(message.Guid))
            {
                var job = new PmsJob();
                job.JobType = PmsJob.PmsJobType.GetFolio;
                job.DeliverypointNumber = message.FieldValue1;
                PmsQueue.Enqueue(job);

                WebserviceManager.CompleteNetmessage(message.Guid);
            }
        }

        private void GetPmsGuestInformation(Netmessage message)
        {
            message = NetmessageHelper.CreateTypedNetmessageModelFromModel(message);
            ConsoleLogger.WriteToLog("NetmessageHandler - GetPmsGuestInformation - {0} - {1}", message.NetmessageId, message.ToString());
            if (WebserviceManager.VerifyNetmessage(message.Guid))
            {
                var job = new PmsJob();
                job.JobType = PmsJob.PmsJobType.GetGuestInformation;
                job.DeliverypointNumber = message.FieldValue1;
                PmsQueue.Enqueue(job);

                WebserviceManager.CompleteNetmessage(message.Guid);
            }
        }

        private void GetPmsExpressCheckout(Netmessage message)
        {
            message = NetmessageHelper.CreateTypedNetmessageModelFromModel(message);
            ConsoleLogger.WriteToLog("NetmessageHandler - GetPmsExpressCheckout - {0} - {1}", message.NetmessageId, message.ToString());
            if (WebserviceManager.VerifyNetmessage(message.Guid))
            {
                var job = new PmsJob();
                job.JobType = PmsJob.PmsJobType.GetExpressCheckout;
                job.DeliverypointNumber = message.FieldValue1;
                try
                {
                    job.Balance = NumericsUtil.ParseInvariantCulture(message.FieldValue2);
                }
                catch (Exception)
                {
                    ConsoleLogger.WriteToLog("NetmessageHandler.GetPmsExpressCheckout - FAILED TO CONVERT BALANCE TO DECIMAL - Value: {1}", message.FieldValue2);
                    job.Balance = 0;
                }
                PmsQueue.Enqueue(job);

                WebserviceManager.CompleteNetmessage(message.Guid);
            }
        }

        private void TerminalCommand(Netmessage message)
        {
            message = NetmessageHelper.CreateTypedNetmessageModelFromModel(message);
            ConsoleLogger.WriteToLog("CometConnection - TerminalCommand (MessageId: {0}) - {1}", message.NetmessageId, message.ToString());
            bool reportedbackCompletedNetMessage = false;
            if (WebserviceManager.VerifyNetmessage(message.Guid))
            {
                var commandMessage = message.ConvertTo<NetmessageTerminalCommand>();
                TerminalCommand command;
                if (!EnumUtil.TryParse(commandMessage.TerminalCommand, out command))
                {
                    ConsoleLogger.WriteToLog("NetmessageHandler - TerminalCommand - Error: Can't convert TerminalCommand '{0}' to TerminalCommand Enum.", commandMessage.TerminalCommand);
                }
                else
                {
                    switch (command)
                    {
                        case Obymobi.Enums.TerminalCommand.DoNothing:
                            ConsoleLogger.WriteToLog("NetmessageHandler - TerminalCommand - Command: Do Nothing. So nothing will be done.");
                            break;
                        case Obymobi.Enums.TerminalCommand.RestartApplication:
                            WebserviceManager.SetTerminalOperationState(TerminalOperationState.Restarting);
                            ConsoleLogger.WriteToLog("NetmessageHandler - TerminalCommand - Command: RestartApplication.");
                            reportedbackCompletedNetMessage = true;
                            WebserviceManager.CompleteNetmessage(message.Guid); // Must report back here, otherwise we get a loop.
                            Server.Server.Instance.IsRestart = true;
                            Server.Server.Instance.StopServer();
                            break;
                        case Obymobi.Enums.TerminalCommand.RestartDevice:
                            ConsoleLogger.WriteToLog("NetmessageHandler - TerminalCommand - Command: RestartDevice - NOT SUPPORTED BY OSS.");
                            break;
                        case Obymobi.Enums.TerminalCommand.RestartInRecovery:
                            ConsoleLogger.WriteToLog("NetmessageHandler - TerminalCommand - Command: RestartInRecovery - NOT SUPPORTED BY OSS..");
                            break;
                        case Obymobi.Enums.TerminalCommand.SendLogToWebservice:
                            ConsoleLogger.WriteToLog("NetmessageHandler - TerminalCommand - Command: SendLogToWebservice.");
                            WebserviceManager.UploadLogs();
                            break;
                        case Obymobi.Enums.TerminalCommand.NonPosDataSynchronisation:
                            ConsoleLogger.WriteToLog("NetmessageHandler - TerminalCommand - Command: NonPosDataSynchronisation.");
                            WebserviceManager.SetTerminalOperationState(TerminalOperationState.SynchronizingPosData);
                            Server.Server.Instance.ReloadData();
                            break;
                        case Obymobi.Enums.TerminalCommand.PosDataSynchronisation:
                            ConsoleLogger.WriteToLog("NetmessageHandler - TerminalCommand - Command: PosDataSynchronisation.");
                            WebserviceManager.SetTerminalOperationState(TerminalOperationState.SynchronizingData);
                            OnSiteServerPosHelper.SynchronizeWithPos();
                            break;
                        case Obymobi.Enums.TerminalCommand.MenuConsistencyCheck:
                            ConsoleLogger.WriteToLog("NetmessageHandler - TerminalCommand - Command: MenuConsistencyCheck.");
                            WebserviceManager.SetTerminalOperationState(TerminalOperationState.CheckingMenuConsistency);
                            OnSiteServerPosHelper.CheckMenuConsistency();
                            break;
                    }

                    // Return to normal state
                    DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(TerminalOperationState), (int)TerminalOperationState.Normal));
                }

                if(!reportedbackCompletedNetMessage)
                    WebserviceManager.CompleteNetmessage(message.Guid);
            }
        }

        private void RoutestephandlerStatusUpdated(Netmessage message)
        {
            message = NetmessageHelper.CreateTypedNetmessageModelFromModel(message);
            ConsoleLogger.WriteToLog("NetmessageHandler - RoutestephandlerStatusUpdated - {0} - {1}", message.NetmessageId, message.ToString());

            bool retrieveOrders = true;
            if (WebserviceManager.VerifyNetmessage(message.Guid))
            {
                bool completeNetmessage = true;
                try
                {
                    var statusUpdate = message.ConvertTo<NetmessageRoutestephandlerStatusUpdated>();
                    OrderRoutestephandlerStatus status = statusUpdate.OrderRoutestephandlerStatus;
                    if (status == OrderRoutestephandlerStatus.BeingHandled ||
                        status == OrderRoutestephandlerStatus.Completed ||
                        status == OrderRoutestephandlerStatus.Failed ||
                        status == OrderRoutestephandlerStatus.RetrievedByHandler)
                    {
                        // These are all stated that don't indicate new orders, so don't retrieve
                        retrieveOrders = false;
                        ConsoleLogger.WriteToLog("NetmessageHandler - RoutestephandlerStatusUpdated: No orders will be retrieved, status doens't require that: " + status.ToString());
                    }
                }
                catch(Exception ex)
                { 
                    // In case of exception, just retrieve orders
                    ConsoleLogger.WriteToLog("NetmessageHandler - RoutestephandlerStatusUpdated: ERROR - " + ex.Message);
                }

                // Retrieve Orders
                if (retrieveOrders)
                {
                    completeNetmessage = Server.Server.Instance.GetOrders();
                }

                if (completeNetmessage)
                {
                    WebserviceManager.CompleteNetmessage(message.Guid);
                }
            }
        }

    }
}
