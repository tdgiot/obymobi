==============================================================================
===                    Crave Interactive On-Site Server                    ===
==============================================================================

== Description ==

Version numbers are formatted in the following way: 1.YYYYMMDDhh

== Legend ==

* Bugfix
+ New feature
~ Chore

== Changelog ==
1.201209xx
 * Fixed "crashing" of clients when they were connected and an update was available
	- Connected clients are now flagged when they can be updated. When a flagged clients creates a 2nd 
		connection,	for downloading the update, the OSS will handle this as a 2nd connection instead of 
		overwriting the 1st socket connection.
	- Update connections are not added to the login queue anymore, since this isn't supported by the 
		client update code, but the OSS will directly send the update file to the client.

1.201207xx
 + Encryption company owner password
 + Routing - Added printer route handler
 * Finally fixed the webservicpoller dead-lock
 * Fixed bug where wrong orders would be returned when getting Unprocessable orders from OrderManager
 ~ SystemStatus log will now be overwritten each time it's generated

1.20120326
 + When client logs in with wrong credentials, those crendentials are logged
 * POS fix for Firefly specific issues
 ~ Moarrr logging

1.20120228
 * Fixed bug where new order statuses where saved before the actual order was saved on the webservice (ID: 25447449)

1.20120224
 ~ Enabled login queue machinism again
 * Various small fixes to make the login queue more stable

1.20120222
 * Changed the table and POS queue locking mechanism because of possible deadlock

1.20120220
 * Temporarly disabled login queue becuase of Firefly issues

1.20120215
 + New login queue mechanism, amount of simultanious logins can be set in config file

1.20110827 - 1.20120214
-- LOTS OF CHANGES WHICH ARE LOST IN TIME AND SPACE --

1.2011082609
* Products with options can not be ordered (Story ID:17129055)
* Client crash after receiving RSA encrypted AES key (random) (Story ID:17121945)
* Client "refreshes" (Story ID:17182119)
+ Extra packet validation to check in client when using OnSrv (Story ID:17397915)