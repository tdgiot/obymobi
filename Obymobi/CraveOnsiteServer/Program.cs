    using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.IO;
using System.Reflection;
using Dionysos.Logging;
    using Microsoft.AspNet.SignalR.Client;
    using Microsoft.Win32;

namespace CraveOnsiteServer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            // MDB: THIS CAN NOT BE USED AS IT CRASHES WITH .NET 4        
            // http://stackoverflow.com/questions/3790344/debugger-launch-is-now-crashing-my-windows-service-after-upgrading-to-net-4-0
            // https://connect.microsoft.com/VisualStudio/feedback/details/611486/debugger-launch-is-now-crashing-my-net-application-after-upgrading-to-net-4-0

            //#if (DEBUG)
            //            try
            //            {
            //                if (TestUtil.IsPcDeveloper)
            //                {
            //                    // Launch debugger
            //                    System.Diagnostics.Debugger.Launch();
            //                }
            //            }
            //            catch { }
            //#endif

            // Begin with this
            InitializeGlobals();

            // Parse any arguments passed
            var args = ParseConsoleArguments();

            string applicationPath = Assembly.GetExecutingAssembly().Location;
            applicationPath = Path.GetDirectoryName(applicationPath);

            if (Environment.CommandLine.Contains("-console"))
            {
                var application = new ConsoleApplication(applicationPath, args);
                if (!application.Start())
                {
                    application.Close();
                }

                Console.WriteLine(@"-----------------------------------------");
                Console.WriteLine(@"Press enter to continue...");
                Console.ReadLine();
            }
            else
            {
                var servicesToRun = new ServiceBase[] 
                                                  { 
                                                      new CraveOnsiteServer(args) 
                                                  };
                ServiceBase.Run(servicesToRun);
            }
        }

        private static Dictionary<string, object> ParseConsoleArguments()
        {
            var arguments = new Dictionary<string, object>();
            arguments.Add("console", false);
            arguments.Add("webservice", "");

            string[] args = Environment.CommandLine.Split(' ');
            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i];

                if (arg == "-console")
                    arguments["console"] = true;
                else if (arg == "-webservice")
                {
                    if (++i < args.Length)
                        arguments["webservice"] = args[i];
                }
            }

            return arguments;
        }

        private static void InitializeGlobals()
        {
            // Set the application information
            Dionysos.Global.ApplicationInfo = new Dionysos.ServiceProcess.ServiceInfo();
            Dionysos.Global.ApplicationInfo.ApplicationName = "CraveOnsiteServer";
            Dionysos.Global.ApplicationInfo.ApplicationVersion = "1.2022072001";            
            Dionysos.Global.ApplicationInfo.BasePath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            Dionysos.GlobalLight.LoggingProvider = new AsyncLoggingProvider(Path.Combine(Dionysos.Global.ApplicationInfo.BasePath, "Logs"), "CloudStatus", 14, "CraveOnsiteServer");

            // Try to write the application details to the registry
            try
            {
                RegistryKey craveKey = Registry.LocalMachine.CreateSubKey(@"Software\Crave");
                using (RegistryKey reqsrvKey = craveKey.CreateSubKey(Dionysos.Global.ApplicationInfo.ApplicationName))
                {
                    reqsrvKey.SetValue("Name", Dionysos.Global.ApplicationInfo.ApplicationName);
                    reqsrvKey.SetValue("Version", Dionysos.Global.ApplicationInfo.ApplicationVersion);
                    reqsrvKey.SetValue("BasePath", Dionysos.Global.ApplicationInfo.BasePath);
                }
            }
            catch
            {
            }
        }
    }
}
