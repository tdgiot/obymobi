﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CraveOnsiteServer.Data;
using CraveOnsiteServer.Logic.Enum;
using Obymobi;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Logic.Routing;
using Obymobi.Printing;
using Obymobi.Logging;
using Obymobi.Integrations.POS;

namespace CraveOnsiteServer.Server
{
	public class Table
	{
		#region Fields

		public Deliverypoint Deliverypoint { get; private set; }

        private readonly object tableQueueLock = new object();
		private readonly Queue<PosJob> tableQueue = new Queue<PosJob>();

		private readonly string serializeDirectory;

		private bool hasItemInPosQueue;
		private readonly object itemInPosQueueBoolLock = new object();

		private readonly bool isPhantom;

		public bool HasItemInPosQueue
		{
			set
			{
				lock (itemInPosQueueBoolLock)
				{
					hasItemInPosQueue = value;
				}
			}
			get
			{
				lock (itemInPosQueueBoolLock)
				{
					return hasItemInPosQueue;
				}
			}
		}

		public TableState State { get; private set; }
		public DateTime TableLockTime { get; private set; }

		private readonly List<PosJob> unprocessedOrders = new List<PosJob>();

		#endregion

		#region Constructor

		/// <summary>
		/// Ctor
		/// </summary>
		/// <param name="deliverypoint">Reference to the deliverypoint this table is linked to</param>
		/// <param name="isPhantom">When true this table is not saved in memory and is removed after one use.</param>
		public Table(Deliverypoint deliverypoint, bool isPhantom = false)
		{
			this.Deliverypoint = deliverypoint;
			this.State = TableState.OPEN;
			this.isPhantom = isPhantom;
			serializeDirectory = DataManager.CreateDirectory("Data\\PosQueue\\" + deliverypoint.DeliverypointId);

			if (!this.isPhantom)
				LoadQueueFromFile();
		}

		#endregion

		#region Serializing

		private void SaveQueueItemToFile(PosJob item)
		{
			if (!this.isPhantom && item != null)
			{
				string filename = Path.Combine(serializeDirectory, item.Identifier + ".job");
				string str = XmlHelper.Serialize(item);
				File.WriteAllText(filename, str);
			}
		}

		private void RemoveSavedQueueItem(PosJob item)
		{
			if (!this.isPhantom && item != null)
			{
				try
				{
					string fileName = Path.Combine(serializeDirectory, item.Identifier + ".job");
					if (File.Exists(fileName))
						File.Delete(fileName);
				}
				catch (Exception ex)
				{
					System.Diagnostics.Debug.WriteLine(ex.Message + "\nCould not remove job item: " + item.Identifier);
				}
			}
		}

		private void LoadQueueFromFile()
		{
			string[] files = Directory.GetFiles(serializeDirectory, "*.job");
			//ConsoleLogger.WriteToLog(" * Loading table {0} queue: {1} item(s)", Deliverypoint.Number, files.Length);
			foreach (var fileStr in files)
			{
				var fileSerialized = File.ReadAllText(fileStr);

				try
				{
					var job = XmlHelper.Deserialize<PosJob>(fileSerialized);
					job.Table = this;

					lock (tableQueueLock)
					{
						this.tableQueue.Enqueue(job);
					}
				}
				catch (Exception)
				{
					try
					{
						var job = Logic.JsonHelper.Deserialize<PosJob>(fileSerialized);
						job.Table = this;

						lock (tableQueueLock)
						{
							this.tableQueue.Enqueue(job);
						}
					}
					catch (Exception)
					{
						File.Delete(fileStr);
					}
				}
			}
		}

		private void OrderRoutestephandlerPOS(Order order, OrderRoutestephandlerSaveStatus result)
		{
			// Retrieve posinformation
			
            var posInfo = DataManager.Instance.PosIntegrationInformationManager.GetPosIntegrationInformation();
			Posorder posorder = null;
			try
			{
				ConsoleLogger.WriteToLog("Converting Order to POS Order");

				// Convert the Order
				var converter = new OnsiteOrderConverter(posInfo);
				posorder = converter.ConvertToPosOrder(order);

                ConsoleLogger.WriteToLog("Hax: external dp id = {0}", posorder.PosdeliverypointExternalId);
			}
			catch (ObymobiException oex)
			{
				ConsoleLogger.WriteToLog("Obymobi Exception: {0} - {1} - {2}", oex.ErrorEnumType, oex.ErrorEnumValueText, oex.Message);
				if (oex.InnerException != null)
					ConsoleLogger.WriteToLog("POS Converted - Inner Exception: {0}", oex.InnerException);
				if (oex.StackTrace.Length > 0)
					ConsoleLogger.WriteToLog(oex.StackTrace);

				// Write log to webservice
				var writeToLog = new WriteToLog
				{
					Message = "POS Converter ObymobiException",
					Log = String.Format("Obymobi Exception: {0} - {1} - {2}", oex.ErrorEnumType, oex.ErrorEnumValueText, oex.Message),
					Status = OrderProcessingError.OnsitePosConversionFailed.ToString(),
					OrderGuid = order.Guid,
					orderId = order.OrderId
				};
				DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(WriteToLog), writeToLog));
			}
			catch (Exception ex)
			{
				ConsoleLogger.WriteToLog("POS Converter - Exception: {0}", ex.Message);
				if (ex.InnerException != null)
					ConsoleLogger.WriteToLog("POS Converted - Inner Exception: {0}", ex.InnerException);
				if (ex.StackTrace.Length > 0)
					ConsoleLogger.WriteToLog(ex.StackTrace);

				// Write log to webservice
				var writeToLog = new WriteToLog
				{
					Message = "POS Converter Exception",
					Log = String.Format("Exception: {0}", ex.Message),
					Status = OrderProcessingError.OnsitePosConversionFailed.ToString(),
					OrderGuid = order.Guid,
					orderId = order.OrderId
				};
				DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(WriteToLog), writeToLog));
			}

		    if (posorder == null) // Could not create POS Order, send error to client
		    {
		        result.Status = (int)OrderRoutestephandlerStatus.Failed;
		        result.Error = (int)OrderProcessingError.OnsitePosConversionFailed;

                ConsoleLogger.WriteToLog("Order could not be converted. Sending result to webservice");
		        DataManager.Instance.OrderManager.UpdateOrder(order);

		        var saveStatus = new WebserviceJob(typeof(OrderRoutestephandlerSaveStatus), result);
		        DataManager.Instance.WebserviceQueue.Enqueue(saveStatus);
		    }
            else
		    {
                ConsoleLogger.WriteToLog("Converted PosOrder Data\n\r{0}", posorder.ToString());
                ConsoleLogger.WriteToLog("Order sucessfully converted. Putting it in the table queue.");
                
		        // Push order to POS queue
		        this.Enqueue(new PosJob
		                     {
		                         Identifier = Dionysos.TimeStamp.CreateTimeStamp(true),
		                         OriginalOrder = order,
		                         DeliverypointId = order.DeliverypointId,
		                         OrderId = order.OrderId,
		                         PosOrder = posorder,
		                         Table = this
		                     });
		    }
		}

		private void OrderRoutestephandlerPrinter(Order order, OrderRoutestephandlerSaveStatus result)
		{
			try
			{
				if (PrintManager.GetSingleton().IsPrinterOnline(DataManager.Instance.TerminalManager.GetTerminal().PrinterName))
				{
					PrintManager.GetSingleton().Print(order, new[] { DataManager.Instance.TerminalManager.GetTerminal().PrinterName });
					result.Status = (int)OrderRoutestephandlerStatus.Completed;
				}
				else
				{
					ConsoleLogger.WriteToLog("[ERROR] Failed to send order to printer. Printer is not online.");
					result.Status = (int)OrderRoutestephandlerStatus.Failed;
					result.Error = (int)OrderProcessingError.PrinterFailure;
				}
			}
			catch (Exception ex)
			{
				ConsoleLogger.WriteToLog("[ERROR] Failed to send order to printer: {0}", ex.Message);
				result.Status = (int)OrderRoutestephandlerStatus.Failed;
				result.Error = (int)OrderProcessingError.PrinterFailure;
			}
			finally
			{
				order.OrderRoutestephandler.Status = result.Status;
				DataManager.Instance.OrderManager.UpdateOrder(order);

				var saveStatus = new WebserviceJob(typeof(OrderRoutestephandlerSaveStatus), result);
				DataManager.Instance.WebserviceQueue.Enqueue(saveStatus);
			}
		}

		#endregion

        /// <summary>
        /// Get total pending orders for this table
        /// </summary>
        /// <returns></returns>
        private int GetOrderCount()
        {
            return this.tableQueue.Count;
        }

		#region Public Methods

		public void PushOrder()
		{
			if (GetOrderCount() > 0)
				Dequeue();
		}

		/// <summary>
		/// Push a new order to the table queue.
		/// </summary>
		/// <param name="order">Order object</param>
		public void PushOrder(Order order)
		{
			ConsoleLogger.WriteToLog("Table {0} - Push Order - BEGIN", this.Deliverypoint.Number);

			// Prepare a OrderSaveStatus model            
			var result = new OrderRoutestephandlerSaveStatus
			             	{
			             		OrderRoutestephandlerGuid = order.OrderRoutestephandler.Guid,
								OrderGuid = order.Guid,
								OrderId = order.OrderId,
			             		Status = (int)OrderRoutestephandlerStatus.BeingHandled
			             	};

			ConsoleLogger.WriteToLog("PushOrder - Check if order with orderroutestephandler GUID ({0}) exists.", order.OrderRoutestephandler.Guid);

			if (!DataManager.Instance.OrderManager.HasOrderOrderRoutestepHandlerGuid(order.OrderRoutestephandler.Guid))
			{
				// Save order on local machine
				DataManager.Instance.OrderManager.AddOrder(order);

				// Update the status if it's an order pulled from the request service
                DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(OrderRoutestephandlerSaveStatus), result.Clone()));

				if (order.OrderRoutestephandler.HandlerType == (int)RoutestephandlerType.POS)
				{
					OrderRoutestephandlerPOS(order, result);
				}
				else if (order.OrderRoutestephandler.HandlerType == (int)RoutestephandlerType.Printer)
				{
                    OrderRoutestephandlerPrinter(order, result);
				}
				else
				{
					ConsoleLogger.WriteToLog("Table.PushOrder - Order (id: {0}) has an Routestephandler which isn't for the OSS ({1})", order.OrderId, order.OrderRoutestephandler.HandlerType);
				}
			}
			else
			{
				ConsoleLogger.WriteToLog("Order with GUID ({0}) is already processed. Disregard order, acquire currency.", order.Guid);
				var log = new WriteToLog
				          	{
				          		Message = "Received already processed order.",
				          		Log = String.Format("Received an order (GUID: {0}) which was previously processed by the terminal. Disregarding order.", order.Guid),
				          		TerminalLogType = (int)TerminalLogType.Message
				          	};
				DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof (WriteToLog), log));
			}

			ConsoleLogger.WriteToLog("Table {0} - Push Order - END", this.Deliverypoint.Number);
		}

		/// <summary>
		/// Gets the save result from the POS queue and does stuff with the table if it's bad
		/// </summary>
		/// <param name="job">Original PosJob object passed to the PosQ</param>
		/// <param name="result">Result object</param>
		public void HandleOrderResult(PosJob job, OrderRoutestephandlerSaveStatus result)
		{
			ConsoleLogger.WriteToLog("Table {0} - Handle Order Result - BEGIN", this.Deliverypoint.Number);

			result.OrderGuid = job.OriginalOrder.Guid;
			result.OrderId = job.OriginalOrder.OrderId;
			result.OrderRoutestephandlerGuid = job.OriginalOrder.OrderRoutestephandler.Guid;

			bool retryFailed = false;
			if (!job.OriginalOrder.IsWebserviceOrder)
			{
				// ES: Code for retry button in Emenu
				if (job.OriginalOrder != null && job.OriginalOrder.OrderRoutestephandler.IsFailed())
				{
					ConsoleLogger.WriteToLog("Table {0} - Order is manually retried...", this.Deliverypoint.Number);

					// Manually retried
					if (result.Status == (int)OrderRoutestephandlerStatus.Failed)
					{
						ConsoleLogger.WriteToLog("Table {0} - ...but failed again!", this.Deliverypoint.Number);
						// Failed again
						retryFailed = true;
					}
					else
					{
						ConsoleLogger.WriteToLog("Table {0} - ...and is now processed!", this.Deliverypoint.Number);

						// Great success!
						unprocessedOrders.Remove(job);

						if (!HasUnprocessedOrders())
						{
							ConsoleLogger.WriteToLog("Table {0} - No more unprocessed orders. Unlocking the table.", this.Deliverypoint.Number);
							UnlockTable();
						}
						else
						{
							ConsoleLogger.WriteToLog("Table {0} - Table has still some unprocessed orders. Keeping it locked!", this.Deliverypoint.Number);
						}
					}
				}
			}

			// Send result to client. Even if the job failed
			if (!retryFailed)
			{
				ConsoleLogger.WriteToLog("Table {0} - Order is not linked to a client. Just updated it on the webservice.", this.Deliverypoint.Number);

				// Update posjob order
				job.OriginalOrder.OrderRoutestephandler.Status = result.Status;
				DataManager.Instance.OrderManager.UpdateOrder(job.OriginalOrder);

				// Send order result to webservice
				var saveStatus = new WebserviceJob(typeof (OrderRoutestephandlerSaveStatus), result.Clone());
				DataManager.Instance.WebserviceQueue.Enqueue(saveStatus);
				

				// Lock the table if job failed
				var resultEnum = result.Status.ToEnum<OrderRoutestephandlerStatus>();

				// GK-ROUTING: Here we still have to differentiate between a Direct-OSS and ROUTED order. 
				// The former would fail directly, the latter just continues it's route.
				if (RoutingHelper.IsFailed(resultEnum))
				{
					// GK-ROUTING: Disabled direct locking:
					// Only directly lock on OSS orders, because Webservice orders might have fall back route for processing
					if (!job.OriginalOrder.IsWebserviceOrder)
					{
						ConsoleLogger.WriteToLog("Table {0} - Order status ({1}) is not processed. Locking table", this.Deliverypoint.Number, resultEnum);
						LockTable();
					}

					// GK-Routing why are all order set to the same status? Couldn't just one order fail? 
					// GK-Routing: Probably have to review code below, do we want to update all orders from queue for webservice?
					lock (tableQueueLock)
					{
						ConsoleLogger.WriteToLog("Table {0} - Give all orders in queue the same status as last processed order.", this.Deliverypoint.Number);
						// Update all queued orders and clients
					    foreach (var posJob in tableQueue)
					    {
					        // Update result with current order GUID and ID
					        OrderRoutestephandlerSaveStatus saveJobData = new OrderRoutestephandlerSaveStatus();
					        saveJobData.OrderRoutestephandlerGuid = posJob.OriginalOrder.OrderRoutestephandler.Guid;
					        saveJobData.Error = posJob.OriginalOrder.OrderRoutestephandler.Error;
					        saveJobData.Status = posJob.OriginalOrder.OrderRoutestephandler.Status;

					        // .. just update the order locally
					        posJob.OriginalOrder.OrderRoutestephandler.Status = saveJobData.Status;
					        DataManager.Instance.OrderManager.UpdateOrder(posJob.OriginalOrder);

					        // ... and send update to webservice
					        saveStatus = new WebserviceJob(typeof(OrderRoutestephandlerSaveStatus), saveJobData);
					        DataManager.Instance.WebserviceQueue.Enqueue(saveStatus);

					    }
					}

					ConsoleLogger.WriteToLog("Table {0} - Add all orders to the unprocesesd order list.", this.Deliverypoint.Number);

					// Add all jobs to the unprocessed list
					unprocessedOrders.Add(job);
					unprocessedOrders.AddRange(tableQueue);

					ConsoleLogger.WriteToLog("Table {0} - Clear table queue.", this.Deliverypoint.Number);

					// Clear the table queue
					lock (tableQueueLock)
					{
						tableQueue.Clear();
					}
				}
			}

			// Remove saved job file
			RemoveSavedQueueItem(job);

			// Dequeue another job and Enqueue it in the POS queue
			// Will automatically stop if the table is locked
			HasItemInPosQueue = false;
			Dequeue();

			ConsoleLogger.WriteToLog("Table {0} - Handle Order Result - END", this.Deliverypoint.Number);
		}

		public void AddUnprocessableOrder(Order order)
		{
			// Convert the Order
			try
			{
				var posInfo = DataManager.Instance.PosIntegrationInformationManager.GetPosIntegrationInformation();
				var converter = new OnsiteOrderConverter(posInfo);
				Posorder posorder = converter.ConvertToPosOrder(order);

				var posJob = new PosJob
				             	{
				             		Identifier = Dionysos.TimeStamp.CreateTimeStamp(true),
				             		OriginalOrder = order,
				             		DeliverypointId = order.DeliverypointId,
				             		OrderId = order.OrderId,
				             		PosOrder = posorder,
				             		Table = this
				             	};
				this.unprocessedOrders.Add(posJob);
			}
			catch (Exception ex)
			{
				ConsoleLogger.WriteToLog("Table {0} - AddUnprocessableOrder - Failed to add unprocessable order: {1}", this.Deliverypoint.Number, ex.Message);

				// Failed, set to processed, nothing we can do
				order.OrderRoutestephandler.Status = (int)OrderRoutestephandlerStatus.Completed;
				DataManager.Instance.OrderManager.UpdateOrder(order);
			}
		}

		public void HandleOrderTimedOut(PosJob item)
		{
			ConsoleLogger.WriteToLog("Table {0} - Set Order Timed-Out {1} - BEGIN", this.Deliverypoint.Number, item.OriginalOrder.Guid);

			// Get order from storage
			bool lockTable = false;
			Order originalOrder = DataManager.Instance.OrderManager.GetOrderByOrderRoutestephandlerGuid(item.OriginalOrder.OrderRoutestephandler.Guid);
			if (originalOrder != null)
			{
				if (originalOrder.OrderRoutestephandler.Status != (int)OrderRoutestephandlerStatus.Completed && 
					originalOrder.OrderRoutestephandler.Status != (int)OrderRoutestephandlerStatus.Failed)
				{
					originalOrder.Status = 9996;
					originalOrder.StatusText = "Timeout";
					originalOrder.OrderRoutestephandler.Status = 9996;
					DataManager.Instance.OrderManager.UpdateOrder(item.OriginalOrder);

					lockTable = true;
				}
			}
			else
			{
				item.OriginalOrder.Status = 9996;
				item.OriginalOrder.StatusText = "Timeout";
				item.OriginalOrder.OrderRoutestephandler.Status = 9996;
				DataManager.Instance.OrderManager.UpdateOrder(item.OriginalOrder);

				lockTable = true;
			}

			ConsoleLogger.WriteToLog("Table {0} - Set Order Timed-Out - END", this.Deliverypoint.Number, item.OriginalOrder.Guid);

			if (lockTable)
				LockTable();

			// Remove saved job file
			RemoveSavedQueueItem(item);

			// Dequeue another job and Enqueue it in the POS queue
			// Will automatically stop if the table is locked
			HasItemInPosQueue = false;
			Dequeue();
		}

		#region Table Locking

		private void LockTable()
		{
			ConsoleLogger.WriteToLog("Table {0} - Lock Table - BEGIN", this.Deliverypoint.Number);

			if (this.State == TableState.OPEN)
			{
				ConsoleLogger.WriteToLog("Table {0} - LOCKED!", this.Deliverypoint.Number);
				this.State = TableState.LOCKED;
				TableLockTime = DateTime.Now;
			}

			ConsoleLogger.WriteToLog("Table {0} - Lock Table - END", this.Deliverypoint.Number);
		}

		private void UnlockTable()
		{
			ConsoleLogger.WriteToLog("Table {0} - Unlock Table - BEGIN", this.Deliverypoint.Number);

			if (this.State == TableState.LOCKED)
			{
				ConsoleLogger.WriteToLog("Table {0} - UNLOCKED!", this.Deliverypoint.Number);
				this.State = TableState.OPEN;

				Dequeue();
			}

			ConsoleLogger.WriteToLog("Table {0} - Unlock Table - END", this.Deliverypoint.Number);
		}

		#endregion

		#region Unprocessed Orders

		/// <summary>
		/// Returns true if this table has unprocessed orders
		/// </summary>
		/// <returns>True/False</returns>
		private bool HasUnprocessedOrders()
		{
			return unprocessedOrders.Count > 0;
		}

		#endregion

		#endregion

		#region Private Methods

		/// <summary>
		/// Actually put the order in the table queue for processing
		/// </summary>
		/// <param name="job">PosJob object</param>
		private void Enqueue(PosJob job)
		{
			ConsoleLogger.WriteToLog("Table Queue - {0} - Enqueue - BEGIN", this.Deliverypoint.Number);

			lock (tableQueueLock)
			{
				// Put job in table queue
				tableQueue.Enqueue(job);

				// Write serialized version to file (for when the OSS haz a boo-boo)
				SaveQueueItemToFile(job);

				// Write to log
				ConsoleLogger.WriteToLog("Table Queue - {0} - Enqueing item - Queue size: {1}", this.Deliverypoint.Number, tableQueue.Count);
			}

			ConsoleLogger.WriteToLog("Table Queue - {0} - State: {1} - Has Item In Pos Queue: {2} - Order Orginal OrderRouteStepHandler Status: {3}",
			                         this.Deliverypoint.Number, this.State, HasItemInPosQueue, job.OriginalOrder.OrderRoutestephandler.Status.ToEnum<OrderRoutestephandlerStatus>());

			if (this.State != TableState.OPEN)
			{
				ConsoleLogger.WriteToLog("!!!!!! TABLE {0} STATE IS {1} !!!!!!", this.Deliverypoint.Number, this.State);
			}

			ConsoleLogger.WriteToLog("Table Queue - {0} - Enqueue - END", this.Deliverypoint.Number);

			// If the table is not locked and this if the first item in the queue,
			// Dequeue the item which will push the item to the real Pos Queue
			if (this.State == TableState.OPEN && !HasItemInPosQueue)
			{
				Dequeue();
			}
			else if (this.State != TableState.OPEN && job.OriginalOrder.OrderRoutestephandler.Error == (int)OrderProcessingError.TableQueueuIsLocked)
			{
				// Manual retry
				Dequeue();
			}
		}

		/// <summary>
		/// Dequeues a PosJob from the table queue and pushes it in the POS queue
		/// </summary>
		private void Dequeue()
		{
			ConsoleLogger.WriteToLog("Table Queue - {0} - Dequeue - BEGIN", this.Deliverypoint.Number);

			PosJob job = null;
			lock (tableQueueLock)
			{
				ConsoleLogger.WriteToLog("Table Queue - Queue size: {0} | Item in Pos Queue: {1}", tableQueue.Count, HasItemInPosQueue);
				if (tableQueue.Count > 0 && !HasItemInPosQueue)
				{
					job = tableQueue.Peek();
					// The order is being retried manually or the table is open
					if (job.OriginalOrder.OrderRoutestephandler.Error == (int)OrderProcessingError.TableQueueuIsLocked ||
					    this.State == TableState.OPEN)
					{
						job = tableQueue.Dequeue();
						HasItemInPosQueue = true;

						// Write to log
						ConsoleLogger.WriteToLog("Table Queue - {0} - Dequeuing item - Queue size: {1})", this.Deliverypoint.Number, tableQueue.Count);
					}
					else
					{
                        // Write to log
                        ConsoleLogger.WriteToLog("Table Queue - {0} - Could not dequeue item. OrderRoutestephandler status: {2} - Table Status: {3} - Queue size: {1})",
                                                 this.Deliverypoint.Number, tableQueue.Count, job.OriginalOrder.OrderRoutestephandler.Status.ToEnum<OrderRoutestephandlerStatus>(), State);

						job = null;
					}
				}
			}

			if (job != null)
			{
				// Queue job in the POS queue
				Server.Instance.PosQueue.Enqueue(job);
			}

			ConsoleLogger.WriteToLog("Table Queue - {0} - Dequeue - END", this.Deliverypoint.Number);
		}

		#endregion
	}
}