﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using CraveOnsiteServer.Data;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using CraveOnsiteServer.Data.Managers;
using Global = CraveOnsiteServer.Data.Global;
using Posalteration = Obymobi.Logic.Model.Posalteration;
using Poscategory = Obymobi.Logic.Model.Poscategory;
using Posdeliverypoint = Obymobi.Logic.Model.Posdeliverypoint;
using Posdeliverypointgroup = Obymobi.Logic.Model.Posdeliverypointgroup;
using Posorder = Obymobi.Logic.Model.Posorder;
using Posorderitem = Obymobi.Logic.Model.Posorderitem;
using Posproduct = Obymobi.Logic.Model.Posproduct;
using CraveOnsiteServer.CraveService;
using Obymobi.Logging;

namespace CraveOnsiteServer.Server
{
    public class OnSiteServerPosHelper
    {
        private static int isConsistencyCheckRunning = 0;

        public static void CheckMenuConsistency()
        {
            if (Interlocked.CompareExchange(ref isConsistencyCheckRunning, 1, 0) == 1)
            {
                ConsoleLogger.WriteToLog("CheckMenuConsistency - Consistency check already running. Skipping request.");
                return;
            }

            long batchId = Convert.ToInt64(DateTime.Now.DateTimeToSimpleDateTimeStamp());
            string log = "Menu consistency checker: " + batchId + "\n\r";
            ConsoleLogger.WriteToLog("CheckMenuConsistency - Start - BatchId: {0}", batchId);
            var posInfo = DataManager.Instance.PosIntegrationInformationManager.GetPosIntegrationInformation();
            var posConnectorType = posInfo.Terminal.POSConnectorType.ToEnum<POSConnectorType>();
            if (Global.PosConnector != null)
            {
                try
                {
                    ConsoleLogger.WriteToLog("CheckMenuConsistency - Pos Connector found");

                    // Posproducts
                    ConsoleLogger.WriteToLog("CheckMenuConsistency - Posproducts");
                    Posproduct[] posproducts = Global.PosConnector.GetPosproducts();
                    ConsoleLogger.WriteToLog("CheckMenuConsistency - Posproducts.Count: {0}", posproducts.Length);
                    if (posproducts.Length > 0)
                    {
                        ConsoleLogger.WriteToLog("CheckMenuConsistency - Process in batches.");
                        DataManager.Instance.Webservice.Timeout = 600000;

                        // MDB TODO Products retrieved from the webservice are not all products but only visible products in a category
                        var webserviceProducts = WebserviceManager.GetProducts();
                        if (webserviceProducts != null)
                        {
                            log += CheckProducts(posproducts, webserviceProducts);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log += String.Format("An exception occurred while synchronizing with the POS. {0}.\n\r", ex.Message);
                }
            }
            else
            {
                log += "ERROR: Pos Connector not found (null)\n\r";
            }

            log += "------------------------END--------------------------";
            ConsoleLogger.WriteToLog(log);
            var job = new WriteToLog
            {
                Message = "POS Consistency Checker",
                Log = log,
                TerminalLogType = (int)TerminalLogType.Message
            };
            DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(WriteToLog), job));
            ConsoleLogger.WriteToLog("CheckMenuConsistency - End - BatchId {0}", batchId);

            // Set consistency running flag to "not running"
            Interlocked.Exchange(ref isConsistencyCheckRunning, 0);
        }

        private static string CheckProducts(Posproduct[] posproducts, Obymobi.Logic.Model.Product[] webserviceProducts)
        {
            string log = "";

            // Loop all POS products
            foreach (var posproduct in posproducts)
            {
                bool productFound = false;

                if (posproduct == null)
                    continue;

                // Check if products exists in webserviceProducts posproduct
                foreach (var webserviceProduct in webserviceProducts)
                {
                    if (webserviceProduct == null || webserviceProduct.Posproduct == null)
                        continue;

                    var wsPosProduct = webserviceProduct.Posproduct;
                    if (posproduct.ExternalId != wsPosProduct.ExternalId)
                        continue;

                    productFound = true;

                    // Check if price is in sync
                    if (posproduct.PriceIn != wsPosProduct.PriceIn)
                        log += String.Format("[W] Product {0} ({1}) - Price out of sync.\n\r", posproduct.Name, posproduct.ExternalId);

                    // Check alterations
                    foreach (var posalteration in posproduct.Posalterations)
                    {
                        if (posalteration == null)
                            continue;

                        bool alterationFound = false;
                        foreach (var wsPosAlteration in wsPosProduct.Posalterations)
                        {
                            if (wsPosAlteration == null || posalteration.ExternalId != wsPosProduct.ExternalId)
                                continue;

                            alterationFound = true;
                            log += CheckConsitencyAlteration(posproduct, posalteration, wsPosAlteration);

                            break;
                        }

                        if (!alterationFound)
                            log += String.Format("[E] Product {0} ({1}) - Missing alteration {2} ({3}).\n\r", posproduct.Name, posproduct.ExternalId, posalteration.Name, posalteration.ExternalId);
                    }

                    break;
                }

                if (!productFound)
                    log += String.Format("[E] Product {0} ({1}) - Product does not exists in CMS.\n\r", posproduct.Name, posproduct.ExternalId);
            }

            return log;
        }

        private static string CheckConsitencyAlteration(Posproduct parent, Posalteration a, Posalteration b)
        {
            var log = "";
            if (a.MinOptions != b.MinOptions || a.MaxOptions != b.MaxOptions)
            {
                log += String.Format("[W] Product {0} ({1}) - Alteration {2} ({3}) - Min/Max Options out of sync.\n\r", parent.Name, parent.ExternalId, a.Name, a.ExternalId);
            }

            // Check all alteration items
            foreach (var aPosAlterationItem in a.Posalterationitems)
            {
                if (aPosAlterationItem == null)
                    continue;

                bool alterationItemFound = b.Posalterationitems.Any(bPosAlterationItem => aPosAlterationItem.ExternalId == bPosAlterationItem.ExternalId);
                if (!alterationItemFound)
                {
                    log += String.Format("[E] Product {0} ({1}) - Alteration {2} ({3}) - Missing alterationitems.\n\r", parent.Name, parent.ExternalId, a.Name, a.ExternalId);
                    break;
                }
            }

            // Check alteration options
            foreach (var aPosAlterationOption in a.Posalterationoptions)
            {
                if (aPosAlterationOption == null)
                    continue;

                bool alterationItemFound = false;

                foreach (var bPosAlterationOption in b.Posalterationoptions)
                {
                    if (bPosAlterationOption == null || aPosAlterationOption.ExternalId != bPosAlterationOption.ExternalId)
                        continue;

                    alterationItemFound = true;
                    if (aPosAlterationOption.PriceIn != bPosAlterationOption.PriceIn)
                    {
                        log += String.Format("[W] Product {0} ({1}) - Alteration {2} ({3}) - Option {4} ({5}) - Price out of sync.\n\r",
                                                parent.Name, parent.ExternalId, a.Name, a.ExternalId, aPosAlterationOption.Name, aPosAlterationOption.ExternalId);
                    }

                    break;
                }

                if (!alterationItemFound)
                {
                    log += String.Format("[E] Product {0} ({1}) - Alteration {2} ({3}) - Missing alterationoptions.\n\r", parent.Name, parent.ExternalId, a.Name, a.ExternalId);
                    break;
                }
            }

            return log;
        }

        private static int isPosSynchronizing = 0;

        /// <summary>
        /// Synchronize products defined in the POS to the CMS
        /// </summary>
        public static void SynchronizeWithPos()
        {
            if (Interlocked.CompareExchange(ref isPosSynchronizing, 1, 0) == 1)
            {
                ConsoleLogger.WriteToLog("SynchronizeWithPOS - Synchronization already running. Skipping sync request.");
                return;
            }

            long batchId = Convert.ToInt64(DateTime.Now.DateTimeToSimpleDateTimeStamp());

            ConsoleLogger.WriteToLog("SynchronizeWithPOS - Start - BatchId: {0}", batchId);
            if (Global.PosConnector != null)
            {
                try
                {
                    ConsoleLogger.WriteToLog("SynchronizeWithPOS - Pos Connector found");

                    ConsoleLogger.WriteToLog("SynchronizeWithPOS - PrePosSynchronisation");
                    Global.PosConnector.PrePosSynchronisation();

                    // Poscategories
                    ConsoleLogger.WriteToLog("SynchronizeWithPOS - Poscategories");
                    Poscategory[] poscategories = Global.PosConnector.GetPoscategories();
                    ConsoleLogger.WriteToLog("SynchronizeWithPOS - Poscategories.Count: {0}", poscategories.Length);
                    if (poscategories.Length > 0)
                    {
                        var wsResult = WebserviceManager.SetPosCategories(batchId, poscategories);
                        if (wsResult != null && wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                            throw new Exception(string.Format("SetPosCategories failed: Code: {0}, Message: {1}", wsResult.ResultCode, wsResult.ResultMessage));
                    }

                    // Posproducts
                    ConsoleLogger.WriteToLog("SynchronizeWithPOS - Posproducts");

                    Posproduct[] posproducts = Global.PosConnector.GetPosproducts();
                    ConsoleLogger.WriteToLog("SynchronizeWithPOS - Posproducts.Count: {0}", posproducts.Length);
                    if (posproducts.Length > 0)
                    {
                        ConsoleLogger.WriteToLog("SynchronizeWithPOS - Process in batches.");
                        DataManager.Instance.Webservice.Timeout = 600000;

                        // Try in batches
                        bool complete = false;
                        int takeFrom = 0;
                        int batch = 1;
                        int batchSize = 25;
                        int total = (posproducts.Length / batchSize) + 1;

                        while (!complete)
                        {
                            int takeTill = takeFrom + batchSize;
                            if (takeTill > posproducts.Length - 1)
                            {
                                complete = true;
                                takeTill = posproducts.Length;
                            }

                            var selection = new List<Posproduct>();
                            for (int i = takeFrom; i < takeTill; i++)
                            {
                                selection.Add(posproducts[i]);
                            }

                            takeFrom = takeTill;

                            ConsoleLogger.WriteToLog("Processing batch {0} of {1}", batch, total);

                            //string xml = XmlHelper.Serialize(selection);
                            //xml = xml.Replace("&amp;#39;", string.Empty);

                            var wsResult = WebserviceManager.SetPosProducts(batchId, selection);
                            if (wsResult != null && wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                                throw new Exception(string.Format("SetPosProducts failed: Code: {0}, Message: {1}", wsResult.ResultCode, wsResult.ResultMessage));

                            batch++;
                        }
                    }

                    // Posdeliverypointgroups
                    ConsoleLogger.WriteToLog("SynchronizeWithPOS - Posdeliverypointgroups");
                    Posdeliverypointgroup[] posdeliverypointgroups = Global.PosConnector.GetPosdeliverypointgroups();
                    ConsoleLogger.WriteToLog("SynchronizeWithPOS - posdeliverypointgroups.Count: {0}", posdeliverypointgroups.Length);
                    if (posdeliverypointgroups.Length > 0)
                    {
                        var wsResult = WebserviceManager.SetPosDeliverypointgroups(batchId, posdeliverypointgroups);
                        if (wsResult != null && wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                            throw new Exception(string.Format("SetPosDeliverypointgroups failed: Code: {0}, Message: {1}", wsResult.ResultCode, wsResult.ResultMessage));
                    }

                    // Posdeliverypoints
                    ConsoleLogger.WriteToLog("SynchronizeWithPOS - Posdeliverypoints");
                    Posdeliverypoint[] posdeliverypoints = Global.PosConnector.GetPosdeliverypoints();
                    ConsoleLogger.WriteToLog("SynchronizeWithPOS - posdeliverypoints.Count: {0}", posdeliverypoints.Length);
                    if (posdeliverypoints.Length > 0)
                    {
                        ConsoleLogger.WriteToLog("SynchronizeWithPOS - Process in batches.");
                        DataManager.Instance.Webservice.Timeout = 600000;

                        // Try in batches
                        bool complete = false;
                        int takeFrom = 0;
                        int batch = 1;
                        int batchSize = 25;
                        int total = (posdeliverypoints.Length / batchSize) + 1;

                        while (!complete)
                        {
                            int takeTill = takeFrom + batchSize;
                            if (takeTill > posdeliverypoints.Length - 1)
                            {
                                complete = true;
                                takeTill = posdeliverypoints.Length;
                            }

                            var selection = new List<Posdeliverypoint>();
                            for (int i = takeFrom; i < takeTill; i++)
                            {
                                selection.Add(posdeliverypoints[i]);
                            }

                            takeFrom = takeTill;

                            ConsoleLogger.WriteToLog("Processing batch {0} of {1}", batch, total);

                            var wsResult = WebserviceManager.SetPosDeliverypoints(batchId, selection.ToArray());
                            if (wsResult != null && wsResult.ResultCode != (int)GenericWebserviceCallResult.Success)
                                throw new Exception(string.Format("SetPosDeliverypoints failed: Code: {0}, Message: {1}", wsResult.ResultCode, wsResult.ResultMessage));
                            
                            batch++;
                        }
                    }

                    // Clean up!
                    ConsoleLogger.WriteToLog("SynchronizeWithPOS - CleanUpPosData");
                    ObyTypedResultOfSimpleWebserviceResult res = WebserviceManager.CleanUpPosData();
                    if (res.ResultCode != 100)
                    {
                        throw new Exception(string.Format("CleanUpPosData failed: Code: {0}, Message: {1}", res.ResultCode, res.ResultMessage));
                    }
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteToLog("An exception occurred while synchronizing with the POS. {0}.\r\n\r\n{1}", ex.Message, ex.StackTrace);
                }
                finally
                {
                    ConsoleLogger.WriteToLog("SynchronizeWithPOS - PostPosSynchronisation");
                    Global.PosConnector.PostPosSynchronisation();

                    // Set flag to not syncing
                    Interlocked.Exchange(ref isPosSynchronizing, 0);
                }
            }
            else
            {
                ConsoleLogger.WriteToLog("SynchronizeWithPOS - Pos Connector not found (null)");
                
                // Set flag to not syncing
                Interlocked.Exchange(ref isPosSynchronizing, 0);
            }

            ConsoleLogger.WriteToLog("SynchronizeWithPOS - End - BatchId {0}", batchId);
        }

        /// <summary>
        /// Send service message to the POS
        /// </summary>
        /// <param name="posMessageType"></param>
        /// <param name="deliverypointNumber"></param>
        public static Posorder CreateMessageForPos(PosServiceMessageType posMessageType, int deliverypointNumber)
        {
            PosIntegrationInformationManager posInfo = DataManager.Instance.PosIntegrationInformationManager;

            string deliverypointExternalId = posInfo.GetPosIntegrationInformation().SystemMessagesDeliverypointId;
            if (deliverypointExternalId.IsNullOrWhiteSpace())
                deliverypointExternalId = posInfo.GetPosIntegrationInformation().AltSystemMessagesDeliverypointId;

            Posorder posorder = null;

            if (!deliverypointExternalId.IsNullOrWhiteSpace())
            {
                var posorderitem = new Posorderitem { Quantity = deliverypointNumber };
                string posproductExternalId = string.Empty;
                switch (posMessageType)
                {
                    case PosServiceMessageType.BatteryLow:
                        posproductExternalId = posInfo.GetPosIntegrationInformation().BatteryLowProductId;
                        if (!posproductExternalId.IsNullOrWhiteSpace())
                        {
                            ConsoleLogger.WriteToLog("OrderHelper.SendMessageToPos - Battery low at deliverypoint {0}.", deliverypointNumber);
                            posorderitem.PosproductExternalId = posproductExternalId;
                        }
                        break;
                    case PosServiceMessageType.UnlockDeliverypoint:
                        posproductExternalId = posInfo.GetPosIntegrationInformation().UnlockDeliverypointProductId;
                        if (!posproductExternalId.IsNullOrWhiteSpace())
                        {
                            ConsoleLogger.WriteToLog("OrderHelper.SendMessageToPos - Deliverypoint {0} locked.", deliverypointNumber);
                            posorderitem.PosproductExternalId = posproductExternalId;
                        }
                        break;
                    case PosServiceMessageType.ClientDisconnected:
                        posproductExternalId = posInfo.GetPosIntegrationInformation().ClientDisconnectedProductId;
                        if (!posproductExternalId.IsNullOrWhiteSpace())
                        {
                            ConsoleLogger.WriteToLog("OrderHelper.SendMessageToPos - Client disconnected for deliverypoint {0}.", deliverypointNumber);
                            posorderitem.PosproductExternalId = posproductExternalId;
                        }
                        break;
                }

                if (!posproductExternalId.IsNullOrWhiteSpace() && posproductExternalId != "0")
                {
                    posorder = new Posorder
                    {
                        PosdeliverypointExternalId = deliverypointExternalId,
                        Posorderitems = new[] { posorderitem }
                    };
                }
            }
            else
                ConsoleLogger.WriteToLog("OrderHelper.SendMessageToPos - No system messages deliverypoint external id could be retrieved from the POS integration information!");

            return posorder;
        }
    }
}
