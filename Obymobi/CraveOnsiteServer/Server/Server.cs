﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CraveOnsiteServer.Logic.Enum;
using CraveOnsiteServer.Net;
using CraveOnsiteServer.Data;
using CraveOnsiteServer.Data.Managers;
using CraveOnsiteServer.Data.Queue;
using Dionysos;
using Dionysos.Net;
using Newtonsoft.Json;
using Obymobi.Logic.Model;
using Obymobi.Enums;
using Global = CraveOnsiteServer.Data.Global;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.CloudStorage.Queue;
using Obymobi.Integrations.PMS;
using Obymobi.Logging;

namespace CraveOnsiteServer.Server
{
    public class Server
    {
        #region Fields

        private static Server instance;

        private static readonly CountdownEvent Countdown = new CountdownEvent(1);
        private readonly Dictionary<int, Table> tableList = new Dictionary<int, Table>();

        private ConnectionSwitcher connectionSwitcher;

        public bool IsRestart { get; set; }
        public PosQueue PosQueue { get; private set; }

        #endregion

        #region Constructor

        private Server()
        {
            this.IsRestart = false;

            // Start queues
            this.PosQueue = new PosQueue();

            // Add Event listeners for PMS
            if (Global.PmsConnector != null)
            {
                Global.PmsConnector.CheckedIn += (this.PmsConnector_CheckedIn);
                Global.PmsConnector.CheckedOut += (this.PmsConnector_CheckedOut);
                Global.PmsConnector.FolioUpdated += (this.Folio_Updated);
                Global.PmsConnector.GuestInformationUpdated += (this.GuestInformation_Updated);
                Global.PmsConnector.WakeUpSet += (this.WakeUp_Set);
                Global.PmsConnector.WakeUpCleared += (this.WakeUp_Cleared);
                Global.PmsConnector.GuestInformationEdited += (this.GuestInformation_Edited);
                Global.PmsConnector.RoomDataUpdated += (this.RoomData_Updated);
                Global.PmsConnector.Moved += Room_Moved;
            }
        }

        public static Server Instance
        {
            get { return Server.instance ?? (Server.instance = new Server()); }
        }

        public static void Reset()
        {
            Server.instance = null;
        }

        #endregion

        #region Events

        public delegate void UpdateHandler();
        public event UpdateHandler UpdateEvent;
        public void OnUpdateEvent()
        {
            if (this.UpdateEvent != null)
            {
                this.UpdateEvent();
            }            
        }

        public delegate void ApplicationUpdateHandler(string applicationCode, int version);
        public event ApplicationUpdateHandler ApplicationUpdateEvent;
        public void OnApplicationUpdateEvent(string applicationCode, int version)
        {
            if (this.ApplicationUpdateEvent != null)
            {
                this.ApplicationUpdateEvent(applicationCode, version);
            }            
        }

        void Status_WebserviceUrlChanged(object sender, EventArgs e)
        {
            DataManager.Instance.Webservice.Url = Global.Status.CraveServiceUrl;
        }

        #endregion

        #region Server/Client Control

        public bool StartServer()
        {
            Server.Countdown.Reset();

            // Set server operation mode
            DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(TerminalOperationMode), (int)TerminalOperationMode.Normal));
            DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(TerminalOperationState), (int)TerminalOperationState.Normal));

            // Create a table foreach deliverypoint
            this.LoadTables();

            // Start POS Queue
            this.PosQueue.StartConsumer();

            // Start Comet Connection
            this.connectionSwitcher = new ConnectionSwitcher();
            this.connectionSwitcher.OnLog += message => ConsoleLogger.WriteToLog(message, LoggingLevel.Verbose);
			this.connectionSwitcher.Initialize();

            Global.Status.WebserviceUrlChanged += this.Status_WebserviceUrlChanged;

            // Push unprocessable orders to their tables
            this.PushUnprocessedOrdersToTable();

            // Enqueue any previous orders
            this.EnqueueOrders();

            // Synchronise the clients with the pms guest service statuses
            if (!DataManager.Instance.TerminalManager.SynchronizeWithPms())
            {
                ConsoleLogger.WriteToLog("Not synchronizing with pms, turned off for terminal");
            }
            else
            {
                ConsoleLogger.WriteToLog("Synchronizing with pms");
                PmsManager.Instance.SynchronizeWithPms();
            }

            // Create a log message that the terminal has been started
            WriteToLog log = new WriteToLog(TerminalLogType.Start, string.Format("Terminal {0} ({1}) started\r\n", Global.TerminalId, NetUtil.GetMachineName()));
            log.Log += string.Format("Machine Name:\t{0}\r\n", NetUtil.GetMachineName());
            log.Log += string.Format("Local IP(s):\t{0}\r\n", NetUtil.GetLocalIpsJoined());
            log.Log += string.Format("Remote IP:\t{0}\r\n", NetUtil.GetRemoteIp());
            DataManager.Instance.WebserviceQueue.Enqueue(new WebserviceJob(typeof(WriteToLog), log));
            
            // Get Orders (always get the Orders once on a restart)
            ConsoleLogger.WriteToLog("Get Orders (once on start up)...");
            this.GetOrders();

            // Block until singnal has been called
            Server.Countdown.Wait();

            ConsoleLogger.WriteToLog("***** SERVER CLOSED ******");
            Global.ServerState = ServerState.OFFLINE;

            if (this.IsRestart)
            {
                ConsoleApplication.Instance.Restart();
            }

            return true;
        }

        public void StopServer()
        {
            ConsoleLogger.WriteToLog("** STOPPING SERVER **");

            // Stop queue thread(s)
            ConsoleLogger.WriteToLog("Stopping Queues and Polling threads");
            DataManager.Instance.WebserviceQueue.StopConsumer();

            PmsQueue.Stop();
            CloudStorageQueue.Stop();
            this.PosQueue.StopConsumer();

            if (Global.PmsConnector != null)
            {
                Global.PmsConnector.StopReceiving();
            }

            // Write to log
            string message = string.Format("Terminal {0} ({1}) {2}\r\n", Global.TerminalId, NetUtil.GetMachineName(), this.IsRestart ? "restarting" : "stopped");
            WebserviceManager.WriteToLog(new WriteToLog(TerminalLogType.Stop, message));

            // Stopping ConnectionSwitcher
            ConsoleLogger.WriteToLog("Stopping Connection Switcher");
            if (this.connectionSwitcher != null)
            {
                this.connectionSwitcher.Stop();
            }

            // Signal blocking to continue
            Server.Countdown.Signal();
        }

        public void RestartServer()
        {
            this.IsRestart = true;
            this.StopServer();
        }

        public void ReloadData()
        {
            ConsoleLogger.WriteToLog(">>>>>>>>>> RELOADING MODE - BEGIN <<<<<<<<<<");
            ConsoleLogger.WriteToLog("Note: Server is not accepting any new connections while reloading!!");
            Global.ServerState = ServerState.RELOADING;

            ConsoleLogger.WriteToLog("- Checking internet & webservice connection.");
            Global.Status.Refresh();
            if (Global.Status.IsWebserviceAvailable)
            {
                LoadResult result = DataManager.Instance.Reload();
                if (result == LoadResult.ERROR)
                {
                    ConsoleLogger.WriteToLog("Reloading failed. Data has been reverted back to original.");
                }
                else
                {
                    ConsoleLogger.WriteToLog("Reloading completed.");
                }
            }
            else
            {
                ConsoleLogger.WriteToLog("Reloading failed. Could not connect to the webservice.");
            }

            ConsoleLogger.WriteToLog(">>>>>>>>>> RELOADING MODE - END <<<<<<<<<<");

            Global.ServerState = ServerState.ONLINE;
        }

        #endregion

        #region Public Methods
        
        private void LoadTables()
        {
            ConsoleLogger.WriteToLog(" * Initializing deliverypoints");

            this.tableList.Clear();

            foreach (Deliverypoint deliverypoint in DataManager.Instance.DeliverypointManager.GetDeliverypoints())
            {
                if (!this.tableList.ContainsKey(deliverypoint.DeliverypointId))
                {
                    this.tableList.Add(deliverypoint.DeliverypointId, new Table(deliverypoint));
                }
            }
        }

        private void EnqueueOrders()
        {
            foreach (Table table in this.tableList.Values)
            {
                table.PushOrder();
            }
        }

        public Table GetTableById(int deliverypointId)
        {
            Table returnValue;
            this.tableList.TryGetValue(deliverypointId, out returnValue);
            return returnValue;
        }

        /// <summary>
        /// Get Table object by table number
        /// </summary>
        /// <param name="number">Number of the table</param>
        /// <returns>Table object if found, otherwise null</returns>
        public Table GetTableByNumber(int number)
        {
            return this.tableList.Values.FirstOrDefault(t => t.Deliverypoint.Number == number);
        }

        public bool GetOrders()
        {
            bool toReturn = false;
            int attempts = 0;

            while (!toReturn && attempts < 3)
            {
                if (attempts > 0)
                {
                    ConsoleLogger.WriteToLog("GetOrders - {0}nd attempt. Waiting {1} seconds before execution.", attempts, attempts * 3);
                    Thread.Sleep(attempts * 3 * 1000);
                }

                attempts++;
                Order[] orders = null;
                if (DataManager.Instance.Webservice != null)                
                    orders = WebserviceManager.GetOrders();                

                int newOrders = 0;
                if (orders != null)
                {
                    toReturn = true;

                    // Walk through the retrieved orders
                    // and push them to their deliverypoint for handling
                    foreach (Order order in orders)
                    {
                        //Use a default table if there is no Deliverypoint number - Simphony supports pick up
                        if (order.ServiceMethodType == ServiceMethodType.PickUp)
                        {
                            Table table = new Table(new Deliverypoint());

                            table.PushOrder();
                        }
                        else
                        {
                            // Check if we don't already have this order in our system
                            if (!DataManager.Instance.OrderManager.HasOrderWithOrderRoutestepHandlerId(order.OrderRoutestephandler.OrderRoutestephandlerId))
                            {
                                newOrders++;

                                order.Guid = Guid.NewGuid().ToString();
                                order.IsWebserviceOrder = true;
                                order.IsSavedOnWebservice = true;

                                if (order.DeliverypointNumber == 0)
                                {

                                }
                                Table table = this.GetTableByNumber(order.DeliverypointNumber);
                                if (table != null)
                                {
                                    table.PushOrder(order);
                                }
                                else
                                {
                                    // Check if this deliverypoint actually exists
                                    Deliverypoint dp = DataManager.Instance.PosIntegrationInformationManager.GetPosDeliverypointById(order.DeliverypointId);
                                    if (dp != null)
                                    {
                                        // It's a webservice order for a table which is not in my current deliverypointgroup,
                                        // but the deliverypoint exists in PosIntegrationInformation, so it must be ok.
                                        // Create a phantom table which is only used once.
                                        table = new Table(dp, true);
                                        table.PushOrder(order);
                                    }
                                    else
                                    {
                                        // This should never happen
                                        ConsoleLogger.WriteToLog("Server.GetOrders - Order (id: {0}) has an unknown deliverypoint number ({1})", order.OrderId, order.DeliverypointNumber);

                                        OrderRoutestephandlerSaveStatus routestephelperStatus = OrderRoutestephandlerHelper.CreateRoutestephandlerSaveStatusModelFromOrderModel(order);
                                        routestephelperStatus.Status = (int)OrderRoutestephandlerStatus.Failed;
                                        routestephelperStatus.Error = (int)OrderProcessingError.NoTableForDeliverypointAtOss;

                                        WebserviceJob job = new WebserviceJob(typeof(OrderRoutestephandlerSaveStatus), routestephelperStatus);
                                        DataManager.Instance.WebserviceQueue.Enqueue(job);
                                    }
                                }
                            }
                            else
                            {
                                ConsoleLogger.WriteToLog("Server.GetOrders - Order (id: {0}) was already processed and is ignored.", order.OrderId);
                            }
                        }
                    }
                }

                if (newOrders > 0 && orders != null)
                {
                    if (newOrders != orders.Length)
                        ConsoleLogger.WriteToLog("Server.GetOrders - {0} order(s) retrieved, {1} order(s) already processed, {2} order(s) to be processed.", orders.Length, orders.Length - newOrders, newOrders);
                    else
                        ConsoleLogger.WriteToLog("Server.GetOrders - {0} new order{1} found.", newOrders, (newOrders > 1 ? "s" : string.Empty));
                }
                else
                    ConsoleLogger.WriteToLog("Server.GetOrders - No new orders found.");
            }
            return toReturn;
        }

        #endregion

        #region Private Methods

        private void PushUnprocessedOrdersToTable()
        {
            List<Order> orders = DataManager.Instance.OrderManager.GetUnprocessableOrders();
            foreach (Order order in orders)
            {
                Table t = this.GetTableByNumber(order.DeliverypointNumber);
                if (t != null)
                    t.AddUnprocessableOrder(order);
            }
        }

        #endregion

        #region Pms Event Handlers

        private void PmsConnector_CheckedOut(PmsConnector sender, GuestInformation guestInformation)
        {
            ConsoleLogger.WriteToLog("Server - CheckedOut - DeliverypointNumber: '{0}'.", guestInformation.DeliverypointNumber);
            ConsoleLogger.WriteToLog("Server - CheckedOut - Data: '{0}'.", guestInformation.ToString());

            PmsManager.Instance.SavePmsModel(guestInformation);
            PmsManager.Instance.ClearFolio(guestInformation.DeliverypointNumber);
        }

        private void PmsConnector_CheckedIn(PmsConnector sender, GuestInformation guestInformation)
        {
            ConsoleLogger.WriteToLog("Server - CheckedIn - DeliverypointNumber: '{0}'.", guestInformation.DeliverypointNumber);
            ConsoleLogger.WriteToLog("Server - CheckedIn - Data: '{0}'.", guestInformation.ToString());

            PmsManager.Instance.SavePmsModel(guestInformation);
        }

        private void GuestInformation_Updated(PmsConnector sender, GuestInformation guestInformation)
        {
            ConsoleLogger.WriteToLog("Server - GuestInformationUpdated - DeliverypointNumber: '{0}'.", guestInformation.DeliverypointNumber);
            ConsoleLogger.WriteToLog("Server - GuestInformationUpdated - Data: '{0}'.", guestInformation.ToString());

            PmsManager.Instance.SavePmsModel(guestInformation);
            PmsManager.Instance.CheckForFolioUpdate(guestInformation);
        }

        private void Folio_Updated(PmsConnector sender, Folio folio)
        {
            ConsoleLogger.WriteToLog("Server - FolioUpdated - DeliverypointNumber: '{0}'.", folio.DeliverypointNumber);

            PmsManager.Instance.SavePmsModel(folio);
        }

        private void WakeUp_Set(PmsConnector sender, WakeUpStatus wakeUpStatus)
        {
            ConsoleLogger.WriteToLog("Server - WakeUpSet - DeliverypointNumber: '{0}'.", wakeUpStatus.DeliverypointNumber);

            // Not implemented yet
            // PmsManager.Instance.SavePmsModel(wakeUpStatus);
        }

        private void WakeUp_Cleared(PmsConnector sender, WakeUpStatus wakeUpStatus)
        {
            ConsoleLogger.WriteToLog("Server - WakeUpCleared - DeliverypointNumber: '{0}'.", wakeUpStatus.DeliverypointNumber);
            
            // Not implemented yet
            // PmsManager.Instance.SavePmsModel(wakeUpStatus);
        }

        private void RoomData_Updated(PmsConnector sender, RoomData roomData)
        {
            ConsoleLogger.WriteToLog("Server - RoomDataUpdated - DeliverypointNumber: '{0}'.", roomData.DeliverypointNumber);

            if (roomData.DeliverypointNumber.IsNullOrWhiteSpace())
            {
                ConsoleLogger.WriteToLog("Server - RoomDataUpdated - No DeliverypointNumber");
            }
            else
            {
                ConsoleLogger.WriteToLog("Server - RoomDataUpdated - RoomData received for room: '{0}'.", roomData.DeliverypointNumber);
                ConsoleLogger.WriteToLog("Server - RoomDataUpdated - Data: '{0}'.", roomData.ToString());

                PmsJob pmsJob = new PmsJob();
                pmsJob.JobType = PmsJob.PmsJobType.SetRoomData;
                pmsJob.DeliverypointNumber = roomData.DeliverypointNumber;
                pmsJob.RoomData = roomData;

                PmsQueue.Enqueue(pmsJob);
            }
        }

        private void GuestInformation_Edited(PmsConnector sender, GuestInformation guestInformation)
        {
            ConsoleLogger.WriteToLog("Server - GuestInformationEdited - DeliverypointNumber: '{0}'.", guestInformation.DeliverypointNumber);
            ConsoleLogger.WriteToLog("Server - GuestInformationEdited - Data: '{0}'.", guestInformation.ToString());

            PmsJob pmsJob = new PmsJob();
            pmsJob.JobType = PmsJob.PmsJobType.EditGuestInformation;
            pmsJob.DeliverypointNumber = guestInformation.DeliverypointNumber;
            pmsJob.GuestInformation = guestInformation;

            PmsQueue.Enqueue(pmsJob);
        }

        private void Room_Moved(PmsConnector sender, string fromDeliverypointNumber, string fromStationNumber, string toDeliverypointNumber, string toStationNumber)
        {
            ConsoleLogger.WriteToLog("Server - RoomMoved - DeliverypointNumber: '{0}' to '{1}'.", fromDeliverypointNumber, toDeliverypointNumber);

            PmsJob pmsJob = new PmsJob();
            pmsJob.JobType = PmsJob.PmsJobType.RoomMove;
            pmsJob.DeliverypointNumber = toDeliverypointNumber;
            pmsJob.DeliverypointNumberOld = fromDeliverypointNumber;

            PmsQueue.Enqueue(pmsJob);
        }

        #endregion
    }
}
