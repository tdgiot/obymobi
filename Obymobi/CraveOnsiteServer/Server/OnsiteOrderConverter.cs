﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi;
using Obymobi.Enums;
using Obymobi.Integrations.POS;
using Obymobi.Integrations.POS.Agilysys;
using Obymobi.Logging;
using Obymobi.Logic.Model;
using static Obymobi.Integrations.POS.PosHelperWrapper;

namespace CraveOnsiteServer.Server
{
    /// <summary>
    /// OnsiteOrderConverter class
    /// </summary>
    public class OnsiteOrderConverter
    {
        /// <summary>
        /// OnsiteOrderConverterResult enums
        /// </summary>
        public enum OnsiteOrderConverterResult
        {
            /// <summary>
            /// Success!
            /// </summary>
            Success = 100,
            /// <summary>
            /// Deliverypoint can't be found
            /// </summary>
            DeliverypointNotFound = 200,
            /// <summary>
            /// POS deliverypoint can't be found
            /// </summary>
            PosdeliverypointNotFound = 201,
            /// <summary>
            /// Product can't be found
            /// </summary>
            ProductNotFound = 202,
            /// <summary>
            /// POS product can't be found
            /// </summary>
            PosproductNotFound = 203,
            /// <summary>
            /// Alteration can't be found
            /// </summary>
            AlterationNotFound = 204,
            /// <summary>
            /// POS alteration can't be found
            /// </summary>
            PosalterationNotFound = 205,
            /// <summary>
            /// Alterationoption can't be found
            /// </summary>
            AlterationoptionNotFound = 206,
            /// <summary>
            /// POS alteration can't be found
            /// </summary>
            PosalterationoptionNotFound = 207,
        }

        PosHelper posHelper = null;

        PosIntegrationInformation posInfo = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="OnsiteOrderConverter"/> class.
        /// </summary>
        /// <param name="posIntegrationInformation">The pos integration information.</param>
        public OnsiteOrderConverter(PosIntegrationInformation posIntegrationInformation)
        {
            this.posInfo = posIntegrationInformation;

            // Find the related Pos Helper
            this.posHelper = this.GetPosHelper(this.posInfo.POSConnectorType.ToEnum<POSConnectorType>());
        }

        /// <summary>
        /// Gets the pos helper.
        /// </summary>
        /// <param name="posConnectorType">Type of the pos connector.</param>
        /// <returns></returns>
        public PosHelper GetPosHelper(POSConnectorType posConnectorType)
        {
            PosHelper toReturn = null;

            switch (posConnectorType)
            {
                case POSConnectorType.Unknown:
                    toReturn = new DummyPosHelper();
                    break;
                //case POSConnectorType.Aloha:
                //    toReturn = new AlohaHelper();
                //    break;
                case POSConnectorType.Agilysys:
                    toReturn = new AgilysysHelper();
                    break;
                case POSConnectorType.SimphonyGen1:
                    toReturn = new DummyPosHelper();
                    break;
                default:
                    throw new ObymobiException(PosSpecificLogicHelperResult.PosHelperNotImplementedForPosConnectorType, "ConnectorType: {0}", posConnectorType);
            }

            return toReturn;
        }

        /// <summary>
        /// Processes a order received from client to be useable in this class.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        public Posorder ConvertToPosOrder(Order order)
        {
            // The replicates the following steps:

            // 1. The client saving the order to the webservice: This logic is 'copied' from OrderHelper.SaveOrderForCustomer() which is called when 
            // a Client saves a order using ObymobiTypedWebservice.SaveOrder().
            this.ReplicateObymobitypedwebserviceSaveOrder(order);

            // 2. The Terminal retrieving the orders from the webservice: This logic is 'copied' from OrderHelper.CreateOrderModelFromEntity
            this.ReplicatePosWebserviceGetOrders(order);

            // 3. The Terminal 'Saves' the order to retrieve a PosOrder
            var posorder = this.ReplicatePosorderhelperCreatePosorderFromOrder(order);

            return posorder;
        }

        /// <summary>
        /// Replicates the obymobitypedwebservice save order.
        /// </summary>
        /// <param name="order">The order.</param>
        private void ReplicateObymobitypedwebserviceSaveOrder(Order order)
        {
            // ObymobiTypedWebservice.SaveOrder() -- BEGIN

            // TODO: Check business hours - Not yet implemented, not relevant for Emenu's

            // The following logic comes from CreateOrderEntityFromModel -- BEGIN   
            // We don't have to replicate all logic, as we don't translate the Model to an Entity here

            // Order Type
            if (order.Type <= 0)
                order.Type = (int)OrderType.Standard;

            // Directly routed 
            order.Status = (int)OrderStatus.Routed;
            order.Created = Dionysos.DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.Now);

            // Verify Deliverypoint (if not found, method will throw exception)
            if (this.posInfo.POSConnectorType != (int)POSConnectorType.Agilysys) // Agilysys doesn't store deliverypoints
            {
                this.GetDeliverypointByDeliverypointId(order.DeliverypointId);
            }
            
            // Obsolete for porting: Deliverypointgroup Disabled (because we're on the terminal)
            // Obsolete for porting: Verify if the Deliverypoint has a Terminal (because we're on the terminal)
            // Obsolete for porting: Check if Terminal is online (because we're on the terminal)

            foreach (var orderitem in order.Orderitems)
            {
                if (orderitem.Type == OrderitemType.Product)
                {
                    Product p = this.GetProduct(orderitem.ProductId);

                    // Retrieve price from local store, don't 'trust' Client
                    orderitem.ProductName = p.Name;
                    orderitem.ProductPriceIn = p.PriceIn;
                    orderitem.VatPercentage = p.VatPercentage;
                }
            }

            // The following logic comes from CreateOrderEntityFromModel -- END

            this.LogOrder(order);
            
            // ObymobiTypedWebservice.SaveOrder() -- END

            // Apply POS specific logic
            this.posHelper.PrepareOrderForPos(order, this.posInfo);

            // DK: Temporary (2020-02-03) solution until we have implemented a proper guest count feature
            this.ExtractNumberOfGuestsAlteration(order);
        }

        private void LogOrder(Order order)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormatLine("**** NEW ORDER - Guid: {0} ****", order.Guid);
            foreach (Orderitem orderitem in order.Orderitems)
            {
                sb.AppendFormatLine("- {0} (ProductId: {1})", orderitem.ProductName, orderitem.ProductId);

                foreach (Alterationitem alterationitem in orderitem.Alterationitems)
                {
                    sb.AppendFormatLine("  * {0} (AlterationId: {1}) - {2} (AlterationoptionId: {3})", alterationitem.AlterationName, alterationitem.AlterationId, alterationitem.AlterationoptionName, alterationitem.AlterationoptionId);
                }
            }

            sb.AppendFormatLine("****************************************");

            ConsoleLogger.WriteToLog(sb.ToString());
        }

        private void ExtractNumberOfGuestsAlteration(Order order)
        {
            foreach (Orderitem orderitem in order.Orderitems)
            {
                foreach (Alterationitem alterationitem in orderitem.Alterationitems)
                {
                    if (alterationitem.AlterationName.Trim().Equals("Number of Guests", StringComparison.InvariantCultureIgnoreCase))
                    {
                        int guestCount;
                        if (int.TryParse(alterationitem.AlterationoptionName, out guestCount))
                        {
                            order.GuestCount = guestCount;
                            ConsoleLogger.WriteToLog("Order (id: {0}) guest count: {1}", order.OrderId, guestCount);
                        }
                        else
                        {
                            ConsoleLogger.WriteToLog("Failed to parse Number of Guests alteration. [alterationId={0}, alterationoptionId={1}, value={2}]", alterationitem.AlterationId, alterationitem.AlterationoptionId, alterationitem.AlterationoptionName);
                        }

                        List<Alterationitem> newList = orderitem.Alterationitems.ToList();
                        newList.Remove(alterationitem);
                        orderitem.Alterationitems = newList.ToArray();

                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Replicates the pos webservice get orders.
        /// </summary>
        /// <param name="order">The order.</param>
        private void ReplicatePosWebserviceGetOrders(Order order)
        {
            // OrderHelper.CreateOrderModelFromEntity -- BEGIN

            // Obsolete for porting: Retrieving Customer Name > Not relevant here.

            // TODO: GK Deliverpoint number going ok? Debug!

            // TODO: Processing time expired?
            List<Orderitem> convertedOrderitems = new List<Orderitem>();
            foreach (var orderitem in order.Orderitems)
            {
                convertedOrderitems.AddRange(this.ReplicateOrderitemHelperCreateOrderitemModelFromEntity(orderitem));
            }

            order.Orderitems = convertedOrderitems.ToArray();

            // OrderHelper.CreateOrderModelFromEntity -- END
        }

        /// <summary>
        /// Replicates the orderitem helper create orderitem model from entity.
        /// </summary>
        /// <param name="orderitemSource">The orderitem source.</param>
        /// <returns></returns>
        private List<Orderitem> ReplicateOrderitemHelperCreateOrderitemModelFromEntity(Orderitem orderitemSource)
        {
            // OrderitemHelper.CreateOrderitemModelFromEntity(orderitem) -- BEGIN

            // If an Orderitem has Alterationitems that are linked to products therse
            // Alterationitems will be converted to Orderitems, there for this method can return more than 
            // one Orderitem when it's done converting.
            List<Orderitem> orderitems = new List<Orderitem>();

            Orderitem orderitemTarget = new Orderitem();
            orderitemTarget.OrderitemId = orderitemSource.OrderitemId;
            orderitemTarget.ProductId = orderitemSource.ProductId;
            orderitemTarget.ProductName = orderitemSource.ProductName;
            orderitemTarget.ProductPriceIn = orderitemSource.ProductPriceIn;
            orderitemTarget.Quantity = orderitemSource.Quantity;
            orderitemTarget.VatPercentage = orderitemSource.VatPercentage;
            orderitemTarget.Notes = orderitemSource.Notes;
            orderitemTarget.Type = orderitemSource.Type;
            orderitemTarget.BenchmarkInformation = orderitemSource.BenchmarkInformation;

            orderitemTarget.FieldValue1 = orderitemSource.FieldValue1;
            orderitemTarget.FieldValue2 = orderitemSource.FieldValue2;
            orderitemTarget.FieldValue3 = orderitemSource.FieldValue3;
            orderitemTarget.FieldValue4 = orderitemSource.FieldValue4;
            orderitemTarget.FieldValue5 = orderitemSource.FieldValue5;
            orderitemTarget.FieldValue6 = orderitemSource.FieldValue6;
            orderitemTarget.FieldValue7 = orderitemSource.FieldValue7;
            orderitemTarget.FieldValue8 = orderitemSource.FieldValue8;
            orderitemTarget.FieldValue9 = orderitemSource.FieldValue9;
            orderitemTarget.FieldValue10 = orderitemSource.FieldValue10;

            // Iterate over Alterationitems to verify if they should be used as Products or real Alterations
            List<Orderitem> alterationOrderitems = new List<Orderitem>();
            if (orderitemSource.Alterationitems != null)
            {
                List<Alterationitem> alterationItems = new List<Alterationitem>();
                foreach (var alterationitem in orderitemSource.Alterationitems)
                {
                    var alteration = this.GetAlteration(alterationitem.AlterationId);
                    var alterationoption = this.GetAlterationoption(alterationitem.AlterationId, alterationitem.AlterationoptionId);

                    // Check whether the alteration option is connected to a POS product
                    // and whether the POS product is connected to a regular product
                    if (alterationoption.PosproductId > 0)
                    {
                        var product = this.GetProductByPosproductId(alterationoption.PosproductId);

                        // It's related to a product, so we have to add an orderitem for 
                        Orderitem alterationOrderitem = new Orderitem();
                        alterationOrderitem.OrderitemId = orderitemTarget.OrderitemId + alterationoption.AlterationoptionId;
                        alterationOrderitem.ProductId = product.ProductId;
                        alterationOrderitem.ProductName = product.Name;
                        alterationOrderitem.Quantity = orderitemTarget.Quantity;

                        if (alterationoption.PriceIn > 0)
                        {
                            alterationOrderitem.ProductPriceIn = alterationoption.PriceIn;
                        }

                        alterationOrderitem.Notes = string.Empty;

                        // Apply POS specific logic
                        this.posHelper.PrepareOrderitemFromAlterationitemForPos(alterationOrderitem, alteration, this.posInfo);

                        if (alterationoption.IsProductRelated)
                            alterationOrderitems.Insert(0, alterationOrderitem);
                        else
                            alterationOrderitems.Add(alterationOrderitem);
                    }
                    else
                    {
                        // Nothing to do, we already have the alterationitem
                        alterationItems.Add(alterationitem);
                    }
                }

                orderitemTarget.Alterationitems = alterationItems.ToArray();
            }

            orderitems.Add(orderitemTarget);
            if (alterationOrderitems.Count > 0)
                orderitems.AddRange(alterationOrderitems);

            // OrderitemHelper.CreateOrderitemModelFromEntity(orderitem) -- END            

            // Apply POS specific logic
            this.posHelper.PrepareOrderitemForPos(orderitemSource, orderitems, this.posInfo);

            return orderitems;
        }

        /// <summary>
        /// Converts an order that was received from the order to a posorder.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        private Posorder ReplicatePosorderhelperCreatePosorderFromOrder(Order order)
        {
            Posorder posorder = new Posorder();
            posorder.PosorderId = order.OrderId;

            // Arrange deliverypoint            
            if (this.posInfo.POSConnectorType != (int) POSConnectorType.Agilysys)
            {
                // Agilysys accepts all deliverypoints
                this.SetPosdeliverypointExternalId(order, posorder);    
            }
            else
            {
                posorder.PosdeliverypointExternalId = order.DeliverypointNumber.ToString();
            }

            posorder.Type = order.Type;
            posorder.GuestCount = order.CoversCount;
            posorder.FieldValue1 = order.FieldValue1;
            posorder.FieldValue2 = order.FieldValue2;
            posorder.FieldValue3 = order.FieldValue3;
            posorder.FieldValue4 = order.FieldValue4;
            posorder.FieldValue5 = order.FieldValue5;
            posorder.FieldValue6 = order.FieldValue6;
            posorder.FieldValue7 = order.FieldValue7;
            posorder.FieldValue8 = order.FieldValue8;
            posorder.FieldValue9 = order.FieldValue8;
            posorder.FieldValue10 = order.FieldValue10;
            posorder.Notes = order.Notes;

            if (!string.IsNullOrEmpty(order.CheckoutMethodName))
            {
                posorder.Notes += $"\n Checkout Name: {order.CheckoutMethodName}";
            }

            if (!string.IsNullOrEmpty(order.ServiceMethodName))
            {
                posorder.Notes += $"\n Service Method Name: {order.ServiceMethodName}";
            }

            if (!string.IsNullOrEmpty(order.CheckoutMethodName))
            {
                posorder.Notes += $"\n Payment Method Name: {order.PaymentmethodName}";
            }

            if (!string.IsNullOrEmpty(order.CardSummary))
            {
                posorder.Notes += $"\n Last 4 card digits: {order.CardSummary}";
            }

            List<Posorderitem> orderitems = new List<Posorderitem>();

            //int categoryId = -1;
            foreach (Orderitem orderitem in order.Orderitems)
            {
                if (orderitem.Type == OrderitemType.Product)
                {
                    // Get the product
                    Product product = this.posInfo.Products.SingleOrDefault(p => p.ProductId == orderitem.ProductId);

                    if (product == null)
                        throw new ObymobiException(OnsiteOrderConverterResult.ProductNotFound, "Order: {0}, Product not found - ProductId: {1}", order.OrderId, orderitem.ProductId);

                    #region Add a category header if needed

                    var categories = from c in this.posInfo.Categories where c.ContainedProductIds.Contains(product.ProductId) select c;

                    if (categories != null && categories.Count() > 0)
                    {
                        // GK This just assumes 1 category
                        var category = categories.First();

                        List<Posorderitem> categoryOrderitems = new List<Posorderitem>();
                        orderitems.AddRange(this.GetCategoryProducts(category, categoryOrderitems, orderitem.OrderitemId));

                        // Check whether the category is connect to a pos product
                        if (category.ProductId > 0)
                        {
                            // Find related Product
                            var categoryProduct = this.GetProduct(category.ProductId);// this.posInfo.Products.SingleOrDefault(p => p.ProductId == category.ProductId);

                            if (categoryProduct != null && categoryProduct.Posproduct != null && categoryProduct.Posproduct.PosproductId > 0)
                            {
                                Posorderitem categoryItem = new Posorderitem();
                                categoryItem.PosorderitemId = orderitem.OrderitemId + category.ProductId;
                                categoryItem.PosproductExternalId = categoryProduct.Posproduct.ExternalId;
                                categoryItem.Quantity = 1;
                                categoryItem.ProductPriceIn = 0;
                                categoryItem.Description = categoryProduct.Name;

                                orderitems.Add(categoryItem);
                            }
                        }
                    }

                    #endregion

                    // Add the orderitem
                    Posorderitem item = this.ConvertOrderitem(orderitem);
                    orderitems.Add(item);
                }
                else
                {
                    orderitems.Add(new Posorderitem
                    {
                        Quantity = orderitem.Quantity,
                        ProductPriceIn = orderitem.ProductPriceIn,
                        Type = orderitem.Type
                    });

                }
            }

            posorder.Posorderitems = orderitems.ToArray();

            return posorder;
        }


        /// <summary>
        /// Converts the orderitem.
        /// </summary>
        /// <param name="orderitem">The orderitem.</param>
        /// <returns></returns>
        private Posorderitem ConvertOrderitem(Orderitem orderitem)
        {
            Posproduct posproduct = this.GetProduct(orderitem.ProductId).Posproduct;

            Posorderitem item = new Posorderitem();
            item.PosorderitemId = orderitem.OrderitemId;
            item.PosproductExternalId = posproduct.ExternalId;
            item.Quantity = orderitem.Quantity;
            item.ProductPriceIn = orderitem.ProductPriceIn;
            item.Description = orderitem.ProductName;
            item.Notes = orderitem.Notes;
            item.Type = orderitem.Type;

            item.FieldValue1 = posproduct.FieldValue1;
            item.FieldValue2 = posproduct.FieldValue2;
            item.FieldValue3 = posproduct.FieldValue3;
            item.FieldValue4 = posproduct.FieldValue4;
            item.FieldValue5 = posproduct.FieldValue5;
            item.FieldValue6 = posproduct.FieldValue6;
            item.FieldValue7 = posproduct.FieldValue7;
            item.FieldValue8 = posproduct.FieldValue8;
            item.FieldValue9 = posproduct.FieldValue8;
            item.FieldValue10 = posproduct.FieldValue10;

            if (orderitem.Alterationitems != null && orderitem.Alterationitems.Length > 0)
            {
                List<Posalterationitem> posalterationitems = new List<Posalterationitem>();

                foreach (Alterationitem alterationitem in orderitem.Alterationitems)
                {
                    Posalterationitem posalterationitem = new Posalterationitem();
                    posalterationitem.PosalterationitemId = orderitem.OrderitemId + alterationitem.AlterationoptionId;

                    Alteration alteration = this.GetAlteration(alterationitem.AlterationId);

                    posalterationitem.ExternalPosalterationId = alteration.Posalteration?.ExternalId;

                    var alterationoption = this.GetAlterationoption(alterationitem.AlterationId, alterationitem.AlterationoptionId);
                    posalterationitem.ExternalPosalterationoptionId = alterationoption.Posalterationoption?.ExternalId;

                    // MDB unTill fix, GK check!
                    posalterationitem.FieldValue1 = alteration.Posalteration?.FieldValue1;

                    posalterationitems.Add(posalterationitem);
                }

                item.Posalterationitems = posalterationitems.ToArray();
            }

            return item;
        }

        /// <summary>
        /// Sets the posdeliverypoint external id.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <param name="posorder">The posorder.</param>
        private void SetPosdeliverypointExternalId(Order order, Posorder posorder)
        {
            Deliverypoint deliverypoint = this.posInfo.Deliverypoints.SingleOrDefault(dp => dp.DeliverypointId == order.DeliverypointId);

            if (deliverypoint == null)
                throw new ObymobiException(OnsiteOrderConverterResult.DeliverypointNotFound, "Order: {0}, Failed for Deliverypointnumber: {1}",
                    order.OrderId, order.DeliverypointNumber);
            else if (deliverypoint.Posdeliverypoint == null || deliverypoint.Posdeliverypoint.PosdeliverypointId <= 0)
                throw new ObymobiException(OnsiteOrderConverterResult.DeliverypointNotFound, "Order: {0}, Failed for Deliverypointnumber: {1}",
                    order.OrderId, order.DeliverypointNumber);

            posorder.PosdeliverypointExternalId = deliverypoint.Posdeliverypoint.ExternalId;
        }

        /// <summary>
        /// Gets the category products.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <param name="categoryOrderitems">The category orderitems.</param>
        /// <param name="orderItemId">The order item id.</param>
        /// <returns></returns>
        private List<Posorderitem> GetCategoryProducts(Category category, List<Posorderitem> categoryOrderitems, int orderItemId)
        {
            List<Posorderitem> orderitems = categoryOrderitems;

            if (category != null && category.ParentCategoryId > 0)
            {
                var parentCategory = this.posInfo.Categories.SingleOrDefault(c => c.CategoryId == category.ParentCategoryId);

                if (parentCategory != null && parentCategory.ProductId > 0)
                {
                    // Find related Product
                    var product = this.posInfo.Products.SingleOrDefault(p => p.ProductId == parentCategory.ProductId);

                    if (product != null)
                    {
                        Posorderitem categoryItem = new Posorderitem();
                        categoryItem.PosorderitemId = orderItemId + parentCategory.ProductId;
                        categoryItem.PosproductExternalId = product.Posproduct.ExternalId;
                        categoryItem.Quantity = 1;
                        categoryItem.ProductPriceIn = 0;
                        categoryItem.Description = product.Name;

                        orderitems.Add(categoryItem);
                    }
                }

                this.GetCategoryProducts(parentCategory, orderitems, orderItemId);

            }

            return orderitems;
        }

        #region Lookup Methods

        Dictionary<int, Deliverypoint> deliverypointsByDeliverypointNumber = new Dictionary<int, Deliverypoint>();
        Dictionary<int, Deliverypoint> deliverypointsByDeliverypointId = new Dictionary<int, Deliverypoint>();
        Dictionary<int, Product> productsByProductId = new Dictionary<int, Product>();
        Dictionary<int, Product> productsByPosproductId = new Dictionary<int, Product>();
        Dictionary<int, Posproduct> posproductsByPosproductId = new Dictionary<int, Posproduct>();
        Dictionary<int, Alteration> alterationsByAlterationId = new Dictionary<int, Alteration>();
        Dictionary<int, Alterationoption> alterationoptionsByAlterationoptionId = new Dictionary<int, Alterationoption>();

        private Deliverypoint GetDeliverypointByDeliverypointNumber(int deliverypointNumber)
        {
            if (this.deliverypointsByDeliverypointNumber.ContainsKey(deliverypointNumber))
                return this.deliverypointsByDeliverypointNumber[deliverypointNumber];

            Deliverypoint deliverypoint = this.posInfo.Deliverypoints.FirstOrDefault(dp => dp.Number == deliverypointNumber);

            if (deliverypoint == null)
                throw new ObymobiException(OnsiteOrderConverterResult.DeliverypointNotFound, "DeliverypointNumber: {0}", deliverypointNumber);
            else if (deliverypoint.Posdeliverypoint == null || deliverypoint.Posdeliverypoint.PosdeliverypointId <= 0)
                throw new ObymobiException(OnsiteOrderConverterResult.PosdeliverypointNotFound, "No Pos Deliverypointnumber found DeliverypointNumber: {0}", deliverypointNumber);

            this.deliverypointsByDeliverypointId.Add(deliverypointNumber, deliverypoint);

            return deliverypoint;
        }

        private Deliverypoint GetDeliverypointByDeliverypointId(int deliverypointId)
        {
            if (this.deliverypointsByDeliverypointId.ContainsKey(deliverypointId))
                return this.deliverypointsByDeliverypointId[deliverypointId];

            Deliverypoint deliverypoint = this.posInfo.Deliverypoints.FirstOrDefault(dp => dp.DeliverypointId == deliverypointId);

            if (deliverypoint == null)
                throw new ObymobiException(OnsiteOrderConverterResult.DeliverypointNotFound, "DeliverypointId: {0}", deliverypointId);
            else if (deliverypoint.Posdeliverypoint == null || deliverypoint.Posdeliverypoint.PosdeliverypointId <= 0)
                throw new ObymobiException(OnsiteOrderConverterResult.PosdeliverypointNotFound, "DeliverypointId: {0}", deliverypointId);

            this.deliverypointsByDeliverypointId.Add(deliverypointId, deliverypoint);

            return deliverypoint;
        }

        private Posproduct GetPosproduct(int posproductId)
        {
            if (this.posproductsByPosproductId.ContainsKey(posproductId))
                return this.posproductsByPosproductId[posproductId];

            // Find the Posproduct            
            var posproductProduct = this.GetProductByPosproductId(posproductId);

            this.posproductsByPosproductId.Add(posproductId, posproductProduct.Posproduct);

            return posproductProduct.Posproduct;
        }

        private Product GetProductByPosproductId(int posproductId)
        {
            if (this.productsByPosproductId.ContainsKey(posproductId))
                return this.productsByPosproductId[posproductId];

            // Find the Posproduct
            var product = this.posInfo.Products.FirstOrDefault(p => p.Posproduct != null && p.Posproduct.PosproductId == posproductId);

            if (product == null)
                throw new ObymobiException(OnsiteOrderConverterResult.ProductNotFound, "PosproductId: {0}", posproductId);

            this.productsByPosproductId.Add(posproductId, product);

            return product;
        }

        private Product GetProduct(int productId)
        {
            if (this.productsByProductId.ContainsKey(productId))
                return this.productsByProductId[productId];

            // Find the Product
            var product = this.posInfo.Products.FirstOrDefault(p => p.ProductId == productId);

            // Verify we found it and a Posproduct is related
            if (product == null)
                throw new ObymobiException(OnsiteOrderConverterResult.ProductNotFound, "ProductId: {0}", productId);
            if (product.Posproduct == null || product.Posproduct.PosproductId <= 0)
                throw new ObymobiException(OnsiteOrderConverterResult.PosproductNotFound, "ProductId: {0}, PosproductId: {1}", productId, product.Posproduct?.PosproductId ?? 0);

            this.productsByProductId.Add(productId, product);

            return product;
        }

        private Alterationoption GetAlterationoption(int alterationId, int alterationoptionId)
        {
            if (this.alterationoptionsByAlterationoptionId.ContainsKey(alterationoptionId))
                return this.alterationoptionsByAlterationoptionId[alterationoptionId];

            // First retrieve the Alteration (which is muck quicker than iterating of each Alteration's it Alterationoptions)
            Alteration alteration = this.GetAlteration(alterationId);

            // No check on alteration == null, because that would be thrown by this.GetAlteration()

            // Check if the Alterationoption is there
            Alterationoption alterationoption = alteration.Alterationoptions.FirstOrDefault(ao => ao.AlterationoptionId == alterationoptionId);

            if (alterationoption == null)
                throw new ObymobiException(OnsiteOrderConverterResult.AlterationoptionNotFound, "AlterationoptionId: {0}, AlterationId: {1}", alterationoptionId, alterationId);

            this.alterationoptionsByAlterationoptionId.Add(alterationoptionId, alterationoption);

            return alterationoption;
        }

        private Alteration GetAlteration(int alterationId)
        {
            if (this.alterationsByAlterationId.ContainsKey(alterationId))
                return this.alterationsByAlterationId[alterationId];

            // Find a product containing this Alteration
            var productWithAlteration = this.posInfo.Products.FirstOrDefault(p => p.Alterations.Any(a => a.AlterationId == alterationId));

            // Check if we found a product
            if (productWithAlteration == null)
                throw new ObymobiException(OnsiteOrderConverterResult.AlterationNotFound, "AlterationId: {0}", alterationId);

            // Fetch the Alteration from the Product
            var alteration = productWithAlteration.Alterations.First(a => a.AlterationId == alterationId);

            this.alterationsByAlterationId.Add(alterationId, alteration);

            return alteration;
        }

        #endregion
    }
}
