﻿using System;
using System.Collections.Generic;
using CraveOnsiteServer.Comet;
using CraveOnsiteServer.Logic;
using Dionysos;
using Obymobi.Enums;
using Global = CraveOnsiteServer.Data.Global;

namespace CraveOnsiteServer.Net
{
    public class ConnectionSwitcher : IConnectionEventListener
    {
        private Dictionary<ClientCommunicationMethod, IConnectionHandler> handlers;
        private readonly object switcherLock = new object();

        private bool firstInitialization;
        private ClientCommunicationMethod currentHandler;

        #region Events 

        public delegate void LogHandler(string message);

        /// <summary>
        /// Event to catch all logging inside ConnectionSwitcher
        /// </summary>
        public event LogHandler OnLog;

        #endregion

        public ConnectionSwitcher()
        {
            handlers = new Dictionary<ClientCommunicationMethod, IConnectionHandler>();
            firstInitialization = true;

            Global.Status.ClientCommunicationChanged += Status_ClientCommunicationChanged;
        }

        public void Initialize()
        {
            ClientCommunicationMethod availableMethods = Global.Status.ClientCommunication;
            if (availableMethods.HasFlag(ClientCommunicationMethod.Webservice))
            {
                handlers.Add(ClientCommunicationMethod.Webservice, new HeartbeatHandler());
            }

            if (availableMethods.HasFlag(ClientCommunicationMethod.SignalR))
            {
                var handler = new CometHandler(this);
                handler.Activate();
                handlers.Add(ClientCommunicationMethod.SignalR, handler);
            }

            if (this.firstInitialization)
            {
                // Activate the current handler
                var method = FindFirstConnectedHandler();
                WriteToLog("Initialize - Activating first connection handler '{0}'", method.GetName());

                IConnectionHandler handler;
                if (handlers.TryGetValue(method, out handler))
                {
                    currentHandler = method;
                    handler.Activate();
                }

                this.firstInitialization = false;
            }
        }

        public void Stop()
        {
            foreach (var connectionHandler in handlers)
            {
                connectionHandler.Value.Destroy();
            }
        }

        public bool TryGetHandler<T>(ClientCommunicationMethod handlerType, out T handler) where T : IConnectionHandler
        {
            IConnectionHandler tmpHandler;
            var retValue = handlers.TryGetValue(handlerType, out tmpHandler);

            if (retValue)
                handler = (T) tmpHandler;
            else
                handler = default(T);

            return retValue;
        }

        #region Private Methods

        void WriteToLog(string message, params object[] args)
        {
            if (OnLog != null)
            {
                OnLog(message.FormatSafe(args));
            }
        }

        /// <summary>
        /// A handler went down, if it's the current handler find next available handler with lower priority
        /// </summary>
        /// <param name="handler">Communication method that went down</param>
        void HandlerDisconnected(ClientCommunicationMethod handler)
        {
            lock (switcherLock)
            {
                WriteToLog("HandlerDisconnected - Handler '{0}' disconnected. Current handler is '{1}'", handler.GetName(), currentHandler.GetName());

                if (handler == currentHandler)
                {
                    // Get next handler
                    var nextHandler = FindNextLowerPriorityHandler(currentHandler);
                    if (nextHandler != currentHandler)
                    {
                        WriteToLog("HandlerDisconnected - Switching ConnectionHandlers. From: '{0}' to '{1}'.", currentHandler.GetName(), nextHandler.GetName());

                        // Stop current handler
                        IConnectionHandler handlerCurrent;
                        if (handlers.TryGetValue(currentHandler, out handlerCurrent))
                        {
                            handlerCurrent.Deactivate();
                        }

                        // Set next handler as current
                        currentHandler = nextHandler;

                        // Activate current handler
                        IConnectionHandler connectionHandler;
                        if (handlers.TryGetValue(currentHandler, out connectionHandler))
                        {
                            connectionHandler.Activate();
                        }
                    }
                    else
                    {
                        WriteToLog("HandlerDisconnected - Next handler '{0}' is the same as current '{1}'", nextHandler.GetName(), currentHandler.GetName());
                        WriteToLog("HandlerDisconnected - Available communication methods: {0}", Global.Status.ClientCommunication.ToString());
                    }
                }
                else
                {
                    WriteToLog("HandlerDisconnected - Current handler '{0}' does not equals disconnected handler '{1}'. Skipping...", currentHandler.GetName(), handler.GetName());
                }
            }
        }

        /// <summary>
        /// A handler came online. If it has a higher priority than the current, switch to the first available handler.
        /// </summary>
        /// <param name="handler"></param>
        void HandlerOnline(ClientCommunicationMethod handler)
        {
            lock (switcherLock)
            {
                WriteToLog("HandlerOnline - Handler '{0}' is back online. Current handler: '{1}'.", handler.GetName(), currentHandler.GetName());

                if (handler != currentHandler && handler.IsHigherPriority(currentHandler))
                {
                    var nextHandler = FindFirstConnectedHandler();
                    if (nextHandler != currentHandler)
                    {
                        WriteToLog("HandlerOnline - Switching ConnectionHandlers. From: '{0}' to '{1}'.", currentHandler.GetName(), nextHandler.GetName());

                        // Stop current handler
                        IConnectionHandler handlerCurrent;
                        if (handlers.TryGetValue(currentHandler, out handlerCurrent))
                        {
                            handlerCurrent.Deactivate();
                        }

                        // Set next handler as current
                        currentHandler = nextHandler;

                        // Activate current handler
                        IConnectionHandler connectionHandler;
                        if (handlers.TryGetValue(currentHandler, out connectionHandler))
                        {
                            connectionHandler.Activate();
                        }
                    }
                }
            }
        }

        ClientCommunicationMethod FindNextLowerPriorityHandler(ClientCommunicationMethod handler)
        {
            IConnectionHandler connHandler;
            if (Global.Status.ClientCommunication.HasFlag(handler) && handlers.TryGetValue(handler, out connHandler) && connHandler.IsConnectionReady())
            {
                return handler;
            }

            if (handler == ClientCommunicationMethod.Webservice)
            {
                return handler;
            }
            
            return FindNextLowerPriorityHandler(handler.GetLowerPriority());
        }

        ClientCommunicationMethod FindFirstConnectedHandler()
        {
            var availableHandler = ClientCommunicationMethod.Webservice;

            foreach (var handlerEnum in EnumUtil.GetValues<ClientCommunicationMethod>())
            {
                IConnectionHandler handler;
                if (Global.Status.ClientCommunication.HasFlag(handlerEnum) && handlers.TryGetValue(handlerEnum, out handler) && handler.IsConnectionReady())
                {
                    if (handlerEnum.IsHigherPriority(availableHandler))
                        availableHandler = handlerEnum;
                }
            }

            return availableHandler;
        }

        void Status_ClientCommunicationChanged(object sender, EventArgs e)
        {
            lock (switcherLock)
            {
                var newMethods = Global.Status.ClientCommunication;
                var newHandlers = new Dictionary<ClientCommunicationMethod, IConnectionHandler>();

                // Check if the current handlers are still relevant
                foreach (var connectionPair in handlers)
                {
                    if (newMethods.HasFlag(connectionPair.Key))
                    {
                        newHandlers.Add(connectionPair.Key, connectionPair.Value);
                    }
                    else
                    {
                        connectionPair.Value.Destroy();
                    }

                    // Remove flag from new methods
                    newMethods &= ~connectionPair.Key;
                }

                // Check for any new methods
                foreach (Enum entry in newMethods.GetIndividualFlags())
                {
                    if (entry.Equals(ClientCommunicationMethod.Webservice))
                    {
                        newHandlers.Add(ClientCommunicationMethod.Webservice, new HeartbeatHandler());
                    }
                    else if (entry.Equals(ClientCommunicationMethod.SignalR))
                    {
                        CometHandler cometHandler = new CometHandler(this);
                        cometHandler.Activate();

                        newHandlers.Add(ClientCommunicationMethod.SignalR, cometHandler);
                    }
                }

                this.handlers = newHandlers;
            }
        }

        #endregion

        public void OnWebserviceConnectionChanged(bool isConnected)
        {
            
        }

        public void OnCometConnectionChanged(bool isConnected)
        {
            WriteToLog("OnCometConnectionChanged - Comet connection changed: {0}", isConnected);

            if (isConnected)
            {
                HandlerOnline(ClientCommunicationMethod.SignalR);
            }
            else
            {
                HandlerDisconnected(ClientCommunicationMethod.SignalR);
            }
        }
    }
}
