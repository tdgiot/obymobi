﻿using Obymobi.Interfaces;

namespace CraveOnsiteServer.Logic.Interfaces
{
    public interface ICacheManager
    {
        void Save(string filename, IPmsModel model);
    }
}
