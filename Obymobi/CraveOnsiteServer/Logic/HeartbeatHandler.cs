﻿using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceProcess;
using System.Threading;
using CraveOnsiteServer.Comet;
using CraveOnsiteServer.Data;
using CraveOnsiteServer.Net;
using Dionysos;
using Dionysos.ServiceProcess.HelperClasses;
using Global = CraveOnsiteServer.Data.Global;
using Obymobi.Logging;

namespace CraveOnsiteServer.Logic
{
    public class HeartbeatHandler : IConnectionHandler
    {
        #region Constants

        private const int LOG_CLEANUP_THRESHOLD = 86400; // 24 Hours

        #endregion

        private Thread webserviceThread;
        private readonly AutoResetEvent isStopped = new AutoResetEvent(false);

        private bool netmessageHandlingEnabled;

        private NetmessageHandler netmessageHandler;

        private List<Application> applicationList;
        private readonly Dictionary<string, DateTime> serviceRestartTimes = new Dictionary<string, DateTime>();
        private readonly Dictionary<string, DateTime> serviceRunTimes = new Dictionary<string, DateTime>();

        private DateTime lastLogCleanup = DateTime.MinValue;

        public HeartbeatHandler()
        {
            netmessageHandler = new NetmessageHandler();

            SetInstalledApplications();

            webserviceThread = new Thread(WebservicePoller);
            webserviceThread.Start();
        }

        private void SetInstalledApplications()
        {
            applicationList = new List<Application>();

            string path = @"C:\Program Files\Grenos\Obymobi Request Service\ObymobiRequestService.exe";
            if (File.Exists(path))
                applicationList.Add(new Application("ObymobiRequestService", path, "ObymobiRequestService"));

            path = @"C:\Program Files\Crave\Crave Onsite Server\CraveOnsiteServer.exe";
            if (File.Exists(path))
                applicationList.Add(new Application("CraveOnsiteServer", path, "CraveOnsiteServer"));

            path = @"C:\Program Files\Crave\Crave Onsite Agent\CraveOnsiteAgent.exe";
            if (File.Exists(path))
                applicationList.Add(new Application("CraveOnsiteAgent", path, "CraveOnsiteAgent"));
        }

        public void Activate()
        {
            ConsoleLogger.WriteToLog("HeartbeatHandler - Activated");
            netmessageHandlingEnabled = true;
        }

        public void Deactivate()
        {
            ConsoleLogger.WriteToLog("HeartbeatHandler - Deactivated");
            netmessageHandlingEnabled = false;
        }

        public bool IsConnectionReady()
        {
            return true;
        }

        public void Destroy()
        {
            ConsoleLogger.WriteToLog("HeartbeatHandler - Destroy");
            netmessageHandler = null;

            StopThread();
        }

        private void StopThread()
        {
            if (webserviceThread != null && webserviceThread.IsAlive)
            {
                isStopped.Set();

                webserviceThread.Join(new TimeSpan(0, 0, 10));
                webserviceThread = null;
            }
        }

        #region Webservice Poller Methods

        private void WebservicePoller()
        {
            ConsoleLogger.WriteToLog("WebservicePoller - START");

            bool lastWebserviceStatus = Global.Status.IsWebserviceAvailable;

            while (!isStopped.WaitOne(new TimeSpan(0, 0, Global.RequestInterval)))
            {
                ConsoleLogger.WriteToLog("------------------- START --------------------", LoggingLevel.Debug, "WebservicePoller-", false);

                Global.LastPollingUpdate = DateTime.Now;

                // Check connection
                try
                {
                    ConsoleLogger.WriteToLog("- Check connectivity", LoggingLevel.Debug, "WebservicePoller-", false);
                    Global.Status.Refresh(false);
                }
                catch (Exception ex)
                {
                    ConsoleLogger.WriteToLog("Exception while refreshing CloudStatus: {0}", ex.Message);
                    ConsoleLogger.WriteToLog(ex.ProcessStackTrace(true));
                }

                bool webserviceAlive = Global.Status.IsWebserviceAvailable;
                if (webserviceAlive)
                {
                    // We had no internet or webservice connection,
                    // initialize our webservice
                    if (!lastWebserviceStatus)
                    {
                        ConsoleLogger.WriteToLog("- Re-initiazing webservice", LoggingLevel.Debug, "WebservicePoller-", false);

                        DataManager.Instance.InitializeWebservice();
                        DataManager.Instance.WebserviceQueue.ManualProcessAfterNoInternet();
                    }

                    // ----------------------------------------------------------------
                    // Comet/Netmessage
                    if (netmessageHandlingEnabled)
                    {
                        ConsoleLogger.WriteToLog("- Receiving Netmessages from webservice", LoggingLevel.Debug, "WebservicePoller-", false);
                        ReceiveNetmessages();
                    }                    
                }
                else
                {
                    ConsoleLogger.WriteToLog("- Failed to find online webservice", LoggingLevel.Debug, "WebservicePoller-", false);
                }

                // Check if all applications are running
                foreach (Application application in applicationList)
                {
                    CheckApplicationAlive(application);
                }

                // Cleaning up old logs
                if (lastLogCleanup.TotalSecondsToNow() >= LOG_CLEANUP_THRESHOLD)
                {
                    lastLogCleanup = DateTime.Now;

                    ConsoleLogger.WriteToLog("- Cleaning Logs", LoggingLevel.Verbose, "WebservicePoller-", false);
                    ConsoleLogger.Cleanup(30);
                }

                ConsoleLogger.WriteToLog("------------------- END --------------------", LoggingLevel.Debug, "WebservicePoller-", false);

                lastWebserviceStatus = webserviceAlive;
            }

            ConsoleLogger.WriteToLog("WebservicePoller - STOPPED");
        }

        private void ReceiveNetmessages()
        {
            var netmessages = WebserviceManager.GetNetmessages();
            if (netmessages != null && netmessages.Length > 0)
            {
                foreach (var netmessage in netmessages)
                {
                    this.netmessageHandler.OnNetmessageReceived(netmessage);
                }
            }
        }

        private void CheckApplicationAlive(Application application)
        {
            // Don't check self
            if (application.Name.Contains("onsiteserver", StringComparison.OrdinalIgnoreCase))
                return;

            var sc = new ServiceController(application.ServiceName);
            string serviceState;
            switch (sc.Status)
            {
                case ServiceControllerStatus.Running:
                    serviceState = "Running";
                    break;
                case ServiceControllerStatus.Stopped:
                    serviceState = "Stopped";
                    break;
                case ServiceControllerStatus.Paused:
                    serviceState = "Paused";
                    break;
                case ServiceControllerStatus.StopPending:
                    serviceState = "Stopping";
                    break;
                case ServiceControllerStatus.StartPending:
                    serviceState = "Starting";
                    break;
                default:
                    serviceState = "Status Changing";
                    break;
            }

            if (!this.serviceRunTimes.ContainsKey(application.Name))
                this.serviceRunTimes.Add(application.Name, DateTime.Now.AddMinutes(-5));
            if (!this.serviceRestartTimes.ContainsKey(application.Name))
                this.serviceRestartTimes.Add(application.Name, DateTime.Now.AddMinutes(-15));

            try
            {
                if (sc.Status != ServiceControllerStatus.Running && sc.Status != ServiceControllerStatus.StartPending)
                {
                    // Only restart service when it's down for more then 5 minutes and last restart was atleast 15 minutes ago
                    if ((DateTime.Now - this.serviceRunTimes[application.Name]).TotalMinutes >= 5 &&
                        (DateTime.Now - this.serviceRestartTimes[application.Name]).TotalMinutes >= 15)
                    {
                        ConsoleLogger.WriteToLog("Process '{0}' was not running (it was: '{1}'), starting it.", application.Name, serviceState);

                        this.serviceRestartTimes[application.Name] = DateTime.Now;

                        sc.Start();
                        if (!ServiceHelper.StartService(application.ServiceName, 20000))
                            ConsoleLogger.WriteToLog("Process '{0}' was not running (it was: '{1}'), start failed.", application.Name, serviceState);
                        else
                        {
                            ConsoleLogger.WriteToLog("Process '{0}' was not running (it was: '{1}'), start succeeded.", application.Name, serviceState);
                            this.serviceRunTimes[application.Name] = DateTime.Now;
                        }
                    }
                }
                else if (sc.Status == ServiceControllerStatus.Running)
                {
                    this.serviceRunTimes[application.Name] = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("Process '{0}' could not be started: {1}.", application.Name, ex.Message);
            }
        }

        #endregion
    }
}
