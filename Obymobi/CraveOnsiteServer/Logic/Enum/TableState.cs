﻿using Dionysos;

namespace CraveOnsiteServer.Logic.Enum
{
    public enum TableState : int
    {
        [StringValue("Unknown")]
        UNKNOWN = 0,

        [StringValue("Open")]
        OPEN = 1,
        
        [StringValue("Closed")]
        CLOSED = 2,

        [StringValue("Locked")]
        LOCKED = 3,

        [StringValue("Unprocessed Order")]
        UNPROCESSED_ORDER = 4
    }
}
