﻿namespace CraveOnsiteServer.Logic.Enum
{
    public enum LoadResult
    {
        LOADED,
        ERROR,
        NEW_DATA
    }
}
