﻿using Dionysos;

namespace CraveOnsiteServer.Logic.Enum
{
    public enum ServerState : int
    {
        /// <summary>
        /// Online 
        /// </summary>
        [StringValue("Online")]
        ONLINE = 100,

        /// <summary>
        /// Offline
        /// </summary>
        [StringValue("Offline")]
        OFFLINE = 200,

        /// <summary>
        /// Loading data
        /// </summary>
        [StringValue("Loading data")]
        LOADING = 301,

        /// <summary>
        /// Reloading data
        /// </summary>
        [StringValue("Reloading data")]
        RELOADING = 302,

        /// <summary>
        /// Loading data failed
        /// </summary>
        [StringValue("Loading of data failed")]
        LOADING_FAILED = 303,

        /// <summary>
        /// Loading data completed
        /// </summary>
        [StringValue("Loading of data completed")]
        LOADING_SUCCESS = 304,
    }
}
