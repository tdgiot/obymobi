﻿using System;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CraveOnsiteServer.Logic
{
	public class JsonHelper
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj">Object to serialize</param>
		/// <returns>Json data</returns>
		public static string Serialize(Object obj)
		{
			JsonSerializerSettings settings = new JsonSerializerSettings();
		    settings.NullValueHandling = NullValueHandling.Ignore;
			settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
			return JsonConvert.SerializeObject(obj, new Formatting(), settings);
		}

		/// <summary>
		/// Deserialize any type
		/// </summary>
		/// <typeparam name="T">Type to deserialize to</typeparam>
		/// <param name="json">Json data</param>
		/// <returns>T</returns>
		public static T Deserialize<T>(string json)
		{
			return (T)JsonConvert.DeserializeObject(json, typeof(T));
		}
	}
}