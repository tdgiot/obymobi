﻿namespace Obymobi.Integrations.PMS.Innsist.Messages
{
    public class RoomMoved : MessageBase
    {
        public string RoomIdOld { get; set; }
    }
}