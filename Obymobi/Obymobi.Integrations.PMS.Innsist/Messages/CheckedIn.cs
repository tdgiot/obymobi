﻿using System;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.PMS.Innsist.Messages
{
    public class CheckedIn : MessageBase
    {
        public string Title
        { get; set; }

        public string Last
        { get; set; }

        public string First
        { get; set; }

        public string Language
        { get; set; }

        public string Group
        { get; set; }

        public bool Vip
        { get; set; }

        public DateTime Arrival
        { get; set; }

        public DateTime Departure
        { get; set; }

        public bool ExpressCheckout
        { get; set; }

        public string GuestId { get; set; }

        public string AccountNumber { get; set; }

        public GuestInformation ToGuestInformationModel()
        {
            GuestInformation gi = new GuestInformation();
            gi.DeliverypointNumber = this.RoomId;
            gi.AccountNumber = this.AccountNumber;
            gi.GuestId = this.GuestId;
            gi.Title = this.Title;
            gi.CustomerLastname = this.Last;
            gi.CustomerFirstname = this.First;
            gi.LanguageCode = this.Language;
            gi.GroupName = this.Group;
            gi.Arrival = DateTime.SpecifyKind(this.Arrival, DateTimeKind.Unspecified);
            gi.Departure = DateTime.SpecifyKind(this.Departure, DateTimeKind.Unspecified);
            gi.Vip = this.Vip;

            gi.TvSetting = (int)PMSTvSetting.Unknown;
            gi.MinibarSetting = (int)PMSMinibarSetting.Unknown;
            gi.AllowViewFolio = true;
            gi.AllowExpressCheckout = this.ExpressCheckout;
            gi.Occupied = true;

            return gi;
        }
    }
}