﻿using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Integrations.Interfaces;
using Obymobi.Integrations.PMS.Configuration;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.PMS.Innsist
{
    public class TCAPosConfgurationAdapter : IConfigurationAdapter
    {
        #region Properties

        /// <summary>
        /// Gets or sets the webservice to call to URL.
        /// </summary>
        /// <value>
        /// The webservice to call to URL.
        /// </value>
        public string WebserviceUrl { get; set; }

        public string WebserviceToBeCalledOnUrl { get; set; }

        public string FromSystemId { get; set; }
        public string ToSystemId { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }

        public string WakeUpCode { get; set; }
        public string PosId { get; set; }

        #endregion

        public TCAPosConfgurationAdapter()
        {
            WakeUpCode = "C1";
        }

        public void ReadFromTerminalModel(Terminal terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;
            this.WebserviceToBeCalledOnUrl = terminal.PosValue2;
            this.Username = terminal.PosValue3;
            this.Password = terminal.PosValue4;
            this.FromSystemId = terminal.PosValue5;
            this.ToSystemId = terminal.PosValue6;
            this.WakeUpCode = terminal.PosValue7;
            this.PosId = terminal.PosValue8;
        }

        public void ReadFromTerminalEntity(TerminalEntity terminal)
        {
            this.WebserviceUrl = terminal.PosValue1;
            this.WebserviceToBeCalledOnUrl = terminal.PosValue2;
            this.Username = terminal.PosValue3;
            this.Password = terminal.PosValue4;
            this.FromSystemId = terminal.PosValue5;
            this.ToSystemId = terminal.PosValue6;
            this.WakeUpCode = terminal.PosValue7;
            this.PosId = terminal.PosValue8;
        }

        public void ReadFromConfigurationManager()
        {
            this.WebserviceUrl = ConfigurationManager.GetString(PmsConfigurationConstants.InnsistWebserviceUrl);
            this.WebserviceToBeCalledOnUrl = ConfigurationManager.GetString(PmsConfigurationConstants.InnsistWebserviceToBeCalledOnUrl);
            this.Username = ConfigurationManager.GetString(PmsConfigurationConstants.InnsistUsername);
            this.Password = ConfigurationManager.GetString(PmsConfigurationConstants.InnsistPassword);
            this.FromSystemId = ConfigurationManager.GetString(PmsConfigurationConstants.InnsistFromSystemId);
            this.ToSystemId = ConfigurationManager.GetString(PmsConfigurationConstants.InnsistToSystemId);
            this.WakeUpCode = ConfigurationManager.GetString(PmsConfigurationConstants.InnsistWakeUpCode);
            this.PosId = ConfigurationManager.GetString(PmsConfigurationConstants.InnsistPosId);
        }

        public void WriteToConfigurationManager()
        {
            ConfigurationManager.SetValue(PmsConfigurationConstants.InnsistWebserviceUrl, this.WebserviceUrl);
            ConfigurationManager.SetValue(PmsConfigurationConstants.InnsistWebserviceToBeCalledOnUrl, this.WebserviceToBeCalledOnUrl);
            ConfigurationManager.SetValue(PmsConfigurationConstants.InnsistUsername, this.Username);
            ConfigurationManager.SetValue(PmsConfigurationConstants.InnsistPassword, this.Password);
            ConfigurationManager.SetValue(PmsConfigurationConstants.InnsistFromSystemId, this.FromSystemId);
            ConfigurationManager.SetValue(PmsConfigurationConstants.InnsistToSystemId, this.ToSystemId);
            ConfigurationManager.SetValue(PmsConfigurationConstants.InnsistWakeUpCode, this.WakeUpCode);
            ConfigurationManager.SetValue(PmsConfigurationConstants.InnsistPosId, this.PosId);
        }

        public void WriteToTerminalEntity(TerminalEntity terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
            terminal.PosValue2 = this.WebserviceToBeCalledOnUrl;
            terminal.PosValue3 = this.Username;
            terminal.PosValue4 = this.Password;
            terminal.PosValue5 = this.FromSystemId;
            terminal.PosValue6 = this.ToSystemId;
            terminal.PosValue7 = this.WakeUpCode;
            terminal.PosValue8 = this.PosId;
        }

        public void WriteToTerminalModel(Terminal terminal)
        {
            terminal.PosValue1 = this.WebserviceUrl;
            terminal.PosValue2 = this.WebserviceToBeCalledOnUrl;
            terminal.PosValue3 = this.Username;
            terminal.PosValue4 = this.Password;
            terminal.PosValue5 = this.FromSystemId;
            terminal.PosValue6 = this.ToSystemId;
            terminal.PosValue7 = this.WakeUpCode;
            terminal.PosValue8 = this.PosId;
        }
    }
}
