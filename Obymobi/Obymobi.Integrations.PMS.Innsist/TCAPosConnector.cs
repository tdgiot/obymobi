﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.ServiceModel.Description;
using Dionysos;
using Obymobi.Integrations.PMS.FlatWsdl;
using Obymobi.Integrations.PMS.Innsist.InnsistClientSvc;
using Obymobi.Integrations.PMS.Innsist.InnsistWebservice;
using Obymobi.Integrations.PMS.Innsist.Messages;
using Obymobi.Integrations.PMS.Interfaces;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.PMS.Innsist
{
    public class TCAPosConnector : PmsConnector
    {
        #region  Fields

        private readonly InnsistPOSServices service;
        private readonly TCAPosConfgurationAdapter config;

        private ExternalWebservice webserviceToBeCalledOn;
        private FlatWsdlServiceHost host;

        private readonly bool sandboxMode;
        private readonly bool fakeMode;

        #endregion

        #region Constructors

        public TCAPosConnector(TCAPosConfgurationAdapter config = null, LogHandler logHandler = null)
            : base(logHandler)
        {
            if (config == null)
            {
                config = new TCAPosConfgurationAdapter();
                config.ReadFromConfigurationManager();
            }

            this.config = config;

            if (config.WebserviceUrl.Contains("216.87.172.219:8587"))
            {
                this.sandboxMode = true;
            }
            else if (config.WebserviceUrl.Contains("=FAKE"))
            {
                this.fakeMode = true;
            }
            // Trust all certificates
            ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

            // Initialize webservice
            service = new InnsistPOSServices();
            service.Url = config.WebserviceUrl;

            // Setup credentials
            From headerFrom = new From();
            headerFrom.SystemId = config.FromSystemId;

            CredentialType headerFromCredential = new CredentialType();
            headerFromCredential.UserName = config.Username;
            headerFromCredential.Password = config.Password;
            headerFrom.Credential = headerFromCredential;

            To headerTo = new To();
            headerTo.SystemId = config.ToSystemId;

            HTNGHeader header = new HTNGHeader();
            header.From = headerFrom;
            header.To = headerTo;
            service.HTNGHeaderValue = header;

            this.PmsMessageReceived += TCAPosConnector_PmsMessageReceived;
            Initialize();
        }

        #endregion

        public string Ping()
        {
            if (this.fakeMode)
            {
                return "";
            }

            OTA_PingRQ request = new OTA_PingRQ();
            request.EchoData = "Hello Crave!";
            
            OTA_PingRS response = this.service.ping(request);
            
            if (response != null && response.Items.Length > 0)
            {
                object responseObj = response.Items[0];
                SuccessType successType;
                string errorMessage;
                if (ValidateResponse(responseObj, out successType, out errorMessage))
                {
                    return response.Items[1].ToString();
                }

                return errorMessage;
            }

            return "Reponse null or item length is zero";
        }

        public override string LogPrefix()
        {
            return "TCAPosConnector";
        }

        public override bool Checkout(Checkout checkoutInfo)
        {
            bool isCheckedOut = false;

            ReservationType reservation;
            if (SearchReservation(checkoutInfo.DeliverypointNumber, out reservation))
            {
                CheckOutRQ request = new CheckOutRQ();
                request.CheckOutExpress = true;
                request.PMSCode = reservation.PMSCode;

                if (this.sandboxMode)
                {
                    request.SessionID = "f07abc6f";
                }

                CheckOutRS response = this.service.checkOut(request);
                if (response != null)
                {
                    SuccessType successType;
                    string errorMessage;
                    if (ValidateResponse(response.Item, out successType, out errorMessage))
                    {
                        isCheckedOut = true;
                    }
                    else
                    {
                        LogError("Checkout - DP: {0} - Error: {1}", checkoutInfo.DeliverypointNumber, errorMessage);
                    }
                }
                else
                {
                    LogError("Checkout - Empty reponse from server");
                }
            }
            else
            {
                LogWarning("Checkout - Unable to find active reservation for room: {0}", checkoutInfo.DeliverypointNumber);
            }

            return isCheckedOut;
        }

        public override List<GuestInformation> GetGuestInformation(string deliverypointNumber)
        {
            this.LogInfo("GetGuestInformation - Deliverypoint: {0}", deliverypointNumber);

            List<GuestInformation> toReturn = new List<GuestInformation>();

            ReservationType reservation;
            if (!SearchReservation(deliverypointNumber, out reservation))
            {
                LogVerbose("GetGuestInformation - Unable to find guest information for room {0}", deliverypointNumber);
                toReturn.Add(new GuestInformation { Error = "Unable to find guest information for room " + deliverypointNumber });
                return toReturn;
            }

            // Create request
            GetFoliosRQ request = new GetFoliosRQ();
            request.PMSCode = reservation.PMSCode;

            // Get response from webservice
            GetFoliosRS response = service.getFolios(request);
            if (response != null && response.Items.Length > 0)
            {
                this.LogVerbose("GetGuestInformation - Item length: {0}", response.Items.Length);

                foreach (object reponseObj in response.Items)
                {
                    GetFoliosRSFolios folios;
                    string errorMessage;
                    if (ValidateResponse(reponseObj, out folios, out errorMessage))
                    {
                        BookingType bookingType;
                        if (!GetBooking(reservation.PMSCode, out bookingType))
                        {
                            LogWarning("Failed to get active booking information for PMSCode {0}", reservation.PMSCode);
                        }

                        foreach (FolioType folioType in folios.Folio)
                        {                            
                            GuestInformation guestInfo = new GuestInformation();
                            guestInfo.DeliverypointNumber = folioType.Guest.RoomID;
                            guestInfo.GuestId = folioType.Guest.GuestID;

                            guestInfo.CreditLimit = folioType.CreditLimit.Amount;
                            guestInfo.Occupied = (bookingType != null);

                            if (bookingType != null)
                            {
                                guestInfo.Arrival = bookingType.StayDateRange.ArrivalDate;
                                guestInfo.Departure = bookingType.StayDateRange.DepartureDate;

                                BookingTypeGuest guest = bookingType.Guest;
                                guestInfo.Vip = guest.GuestType.Equals("VIP");
                                guestInfo.Zipcode = guest.Address.ZIPCode;

                                BookingTypeGuestPersonName guestName = guest.PersonName;
                                guestInfo.CustomerFirstname = guestName.GivenName;
                                guestInfo.CustomerLastname = guestName.Surname;
                                guestInfo.CustomerLastnamePrefix = "";
                            }

                            toReturn.Add(guestInfo);
                        }
                    }
                    else
                    {
                        this.LogError("GetGuestInformation - DP: {0} - Error: {1}", deliverypointNumber, errorMessage);
                        toReturn.Add(new GuestInformation { Error = errorMessage });
                    }
                }
            }
            else
            {
                LogVerbose("GetGuestInformation - Empty response");
                toReturn.Add(new GuestInformation { Error = "Empty response from webservice" });
            }
            
            return toReturn;
        }

        public override List<Folio> GetFolio(string deliverypointNumber)
        {
            LogInfo("GetFolio - Deliverypoint: {0}", deliverypointNumber);

            List<Folio> toReturn = new List<Folio>();

            ReservationType reservation;
            if (!SearchReservation(deliverypointNumber, out reservation))
            {
                toReturn.Add(new Folio { Error = "Unable to find guest information for room " + deliverypointNumber });
                return toReturn;
            }

            // Create request
            GuestBalanceRQ request = new GuestBalanceRQ();
            request.PMSCode = reservation.PMSCode;

            // Get response from webservice
            GuestBalanceRS response = service.getGuestBalance(request);
            if (response != null && response.Item != null)
            {
                GuestBalanceRSBalances balances;
                string errorMessage;
                if (ValidateResponse(response.Item, out balances, out errorMessage))
                {
                    foreach (BalanceType balanceType in balances.Balance)
                    {
                        Folio folio = new Folio();
                        folio.Balance = balanceType.Balance.AmountAfterTax;
                        folio.DeliverypointNumber = deliverypointNumber;
                        folio.AccountNumber = balanceType.Guest.GuestID;

                        List<FolioItem> folioItems = new List<FolioItem>();
                        foreach (TransactionType transactionType in balanceType.Transactions)
                        {
                            FolioItem folioItem = new FolioItem();
                            folioItem.Code = transactionType.Code;
                            folioItem.ReferenceId = transactionType.ReferenceID;
                            
                            try
                            {
                                string createdDateTime = transactionType.Date + " " + transactionType.Time;
                                folioItem.Created = DateTime.ParseExact(createdDateTime, "yyyyMMdd HHmm", CultureInfo.InvariantCulture);
                            }
                            catch (Exception)
                            {
                                folioItem.Created = DateTime.MinValue;
                            }

                            folioItem.Description = transactionType.Description;
                            folioItem.PriceIn = transactionType.Amount.AmountAfterTax;
                            folioItem.CurrencyCode = transactionType.Amount.CurrencyCode;

                            folioItems.Add(folioItem);
                        }
                        folio.FolioItems = folioItems.ToArray();

                        toReturn.Add(folio);
                    }
                }
                else
                {
                    this.LogError("GetFolio - DP: {0} - Error: {1}", deliverypointNumber, errorMessage);
                    toReturn.Add(new Folio {Error = errorMessage});
                }
            }
            else
            {
                LogVerbose("GetFolio - Empty response");
                toReturn.Add(new Folio {Error = "Empty response from webservice"});
            }

            return toReturn;
        }

        public override decimal GetBalance(string deliverypointNumber)
        {
            LogInfo("GetBalance - Deliverypoint: {0}", deliverypointNumber);

            decimal totalBalance = 0;

            ReservationType reservation;
            if (SearchReservation(deliverypointNumber, out reservation))
            {
                // Create request
                GuestBalanceRQ request = new GuestBalanceRQ();
                request.PMSCode = reservation.PMSCode;

                // Get response from webservice
                GuestBalanceRS response = this.service.getGuestBalance(request);
                if (response != null)
                {
                    GuestBalanceRSBalances balances;
                    string errorMessage;
                    if (ValidateResponse(response.Item, out balances, out errorMessage))
                    {
                        decimal sum = 0;
                        foreach (BalanceType balanceType in balances.Balance)
                        {
                            sum += balanceType.Balance.AmountAfterTax;
                        }
                        totalBalance += sum;
                    }
                    else
                    {
                        this.LogError("GetBalance - DP: {0} - Error: {1}", deliverypointNumber, errorMessage);
                    }
                }
                else
                {
                    LogVerbose("GetBalance - Empty response");
                }
            }

            return totalBalance;
        }

        public override bool AddToFolio(string deliverypointNumber, List<FolioItem> folioItems)
        {
            LogVerbose("AddToFolio - DeliverypointNumber: {0}, folioItems: {1}", deliverypointNumber, folioItems.Count);

            ReservationType reservation;
            if (!SearchReservation(deliverypointNumber, out reservation))
            {
                LogWarning("AddToFolio - No reservation found for room {0}", deliverypointNumber);
                return false;
            }

            FolioType folioType;
            if (!GetOpenFolio(deliverypointNumber, out folioType))
            {
                LogWarning("AddToFolio - No billable folio found for room {0}", deliverypointNumber);
                return false;
            }

            int referenceId = GetNextReferenceId(reservation.PMSCode);

            int failedCalls = 0;
            foreach (FolioItem folioItem in folioItems)
            {
                // Increment ref id
                referenceId++;

                TransactionRequestType request = new TransactionRequestType();
                request.Action = ServiceRequestTypeAction.Commit;
                request.PMSCode = reservation.PMSCode;
                request.Surname = reservation.GuestsList[0].PersonName.Surname;
                request.RoomID = deliverypointNumber;
                request.PosID = this.config.PosId;

                request.Price = new TotalType
                                {
                                    AmountAfterTax = folioItem.PriceIn,
                                    CurrencyCode = folioItem.CurrencyCode
                                };
                request.Service = new ServiceType
                                  {
                                      Code = folioItem.Code,
                                      ReferenceID = "C" + referenceId,
                                      Description = folioItem.Description,
                                      Quantity = "1",
                                      RequestedIndicator = true,
                                      StartDate = folioItem.Created,
                                      StartTime = folioItem.Created,
                                      CreationDate = folioItem.Created,
                                      CreationTime = folioItem.Created
                                  };

                LogDebug("AddToFolio - RefId: {0}, Code: {1}, Description: {2}", request.Service.ReferenceID, folioItem.Code, folioItem.Description);

                PostTransactionRS response = this.service.postTransaction(request);
                if (response != null && response.Items.Length > 0)
                {
                    ChargeStatusType statusType;
                    string errorMessage;
                    if (ValidateResponse(response.Items[0], out statusType, out errorMessage))
                    {
                        if (statusType.TransactionStatusCode != ChargeStatusTypeTransactionStatusCode.Committed)
                        {
                            LogWarning("AddToFolio - RefId: {0}, StatusCode: {1}", request.Service.ReferenceID, statusType.TransactionStatusCode);
                        }
                    }
                    else
                    {
                        LogWarning("AddToFolio - DP: {0} - Error: {1}", deliverypointNumber, errorMessage);
                        failedCalls++;
                    }
                }
                else
                {
                    LogWarning("AddToFolio - Empty reponse from server. Code: {0}, Description: {1}", folioItem.Code, folioItem.Description);
                    failedCalls++;
                }
            }

            return folioItems.Count == 1 ? (failedCalls == 0) : (failedCalls != folioItems.Count);
        }

        public override bool SetWakeUpStatus(WakeUpStatus wakeUpStatus)
        {
            this.LogInfo("SetWakeUpStatus - {0}", wakeUpStatus.DeliverypointNumber);

            ReservationType reservation;
            if (!this.SearchReservation(wakeUpStatus.DeliverypointNumber, out reservation))
            {
                return false;
            }

            bool toReturn = false;
            
            ServiceType serviceType = new ServiceType();
            serviceType.Code = this.config.WakeUpCode;
            serviceType.ReferenceID = DateTime.UtcNow.ToString("YYYYMMddHHmmss");
            serviceType.Description = "Crave WakeUp Status";
            serviceType.Quantity = "1";
            serviceType.RequestedIndicator = true;
            serviceType.StartDate = wakeUpStatus.WakeUpDate;
            serviceType.StartTime = wakeUpStatus.WakeUpTime;

            ServiceRequestType request = new ServiceRequestType();
            request.PMSCode = reservation.PMSCode;
            request.Date = DateTime.UtcNow;
            request.Service = serviceType;

            SpecialRequestRS response = this.service.postSpecialRequest(request);
            if (response != null)
            {
                object responseObj = (response.Items.Length > 1) ? response.Items[1] : response.Items[0];
                string errorMessage;
                ServiceType serviceDetails;
                if (this.ValidateResponse(responseObj, out serviceDetails, out errorMessage))
                {
                    toReturn = true;
                }
                else
                {
                    this.LogError("SetWakeUpStatus - DP: {0} - Error: {1}", wakeUpStatus.DeliverypointNumber, errorMessage);
                }
            }
            else
            {
                LogError("SetWakeUpStatus - Empty reponse from server");
            }

            return toReturn;
        }

        public override RoomState GetRoomStatus(string deliverypointNumber)
        {
            // Not supported
            return RoomState.Unknown;
        }

        public override bool UpdateRoomState(string deliverypointNumber, RoomState state)
        {
            // Not supported
            return true;
        }

        public override void StopReceiving()
        {
            if (this.host != null)
            {
                this.host.Close();
                this.host = null;
            }
        }

        protected override void InitializePmsReceiver()
        {
            Uri baseAddress = new Uri(this.config.WebserviceToBeCalledOnUrl);

            // Create the ServiceHost.
            if (this.host == null)
            {
                // Init the service
                this.webserviceToBeCalledOn = new ExternalWebservice(this.receivedMessages);

                // Host it
                this.host = new FlatWsdlServiceHost(this.webserviceToBeCalledOn, baseAddress);

                // No check for Endpoints > 0, should be there, otherwise a crash is correct.
                this.host.Description.Endpoints[0].Binding.Namespace = "http://htng.org/2011B/HTNG_GuestAndRoomStatusService";

                // Enable metadata publishing.
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
                this.host.Description.Behaviors.Add(smb);

                // Open the ServiceHost to start listening for messages. Since
                // no endpoints are explicitly configured, the runtime will create
                // one endpoint per base address for each service contract implemented
                // by the service.
                this.host.Open();

                LogInfo("Receiver running on {0}", this.config.WebserviceToBeCalledOnUrl);
            }
        }

        protected override string SendMessageToPms(IPmsMessage message)
        {
            // Not used
            return string.Empty;
        }

        private bool GetBooking(string pmsCode, out BookingType bookingType)
        {
            this.LogVerbose("GetBooking - PMSCode: {0}", pmsCode);

            bookingType = null;

            GetBookingsRQ req = new GetBookingsRQ();
            req.PMSCode = pmsCode;

            GetBookingsRS resp = service.getBookings(req);
            if (resp != null)
            {
                GetBookingsRSBookings bookings;
                string errorMessage;
                if (ValidateResponse(resp.Item, out bookings, out errorMessage))
                {
                    DateTime today = DateTime.Today;

                    foreach (BookingType booking in bookings.Booking)
                    {
                        if (booking.StayDateRange.ArrivalDate <= today || booking.StayDateRange.DepartureDate >= today)
                        {
                            bookingType = booking;
                            break;
                        }
                    }
                }
                else
                {
                    LogError("GetBookings - PMSCode: {0} - Error: {1}", pmsCode, errorMessage);
                }
            }

            return (bookingType != null);
        }

        private bool SearchReservation(string deliverypointNumber, out ReservationType reservation)
        {
            this.LogInfo("SearchReservation - Deliverypoint: {0}", deliverypointNumber);

            reservation = null;

            if (this.fakeMode)
            {
                return false;
            }

            SearchReservationsRQ request = new SearchReservationsRQ();
            request.RoomID = deliverypointNumber;
            request.ReservationStatus = new [] { RsvStatusType.Inhouse };

            service.HTNGHeaderValue.TimeStamp = DateTime.Today;
            service.HTNGHeaderValue.EchoToken = GuidUtil.CreateGuid();
            service.HTNGHeaderValue.Action = TransactionActionType.Ignore;

            SearchReservationsRS response = service.searchReservations(request);
            if (response != null)
            {
                SearchReservationsRSReservations reservations;
                string errorMessage;
                if (ValidateResponse(response.Item, out reservations, out errorMessage))
                {
                    foreach(ReservationType reservationType in reservations.Reservation)
                    {
                        reservation = reservationType;
                        break;
                    }
                }
                else
                {
                    this.LogError("SearchReservation - DP: {0} - Error: {1}", deliverypointNumber, errorMessage);
                }
            }
            else
            {
                this.LogError("SearchReservation {0} - Empty result from server");
            }

            return reservation != null;
        }

        private bool GetOpenFolio(string deliverypointNumber, out FolioType folio)
        {
            folio = null;

            ReservationType reservation;
            if (!SearchReservation(deliverypointNumber, out reservation))
            {
                LogVerbose("GetOpenFolio - Unable to find guest information for room {0}", deliverypointNumber);
                return false;
            }

            // Create request
            GetFoliosRQ request = new GetFoliosRQ();
            request.PMSCode = reservation.PMSCode;

            // Get response from webservice
            GetFoliosRS response = service.getFolios(request);
            if (response != null && response.Items.Length > 0)
            {
                this.LogVerbose("GetOpenFolio - Item length: {0}", response.Items.Length);

                foreach (object reponseObj in response.Items)
                {
                    GetFoliosRSFolios folios;
                    string errorMessage;
                    if (ValidateResponse(reponseObj, out folios, out errorMessage))
                    {
                        foreach (FolioType folioType in folios.Folio)
                        {
                            if (folioType.Status == FolioTypeStatus.Opened && folioType.ExtraChargesAllowed)
                            {
                                folio = folioType;
                                break;
                            }
                        }
                    }

                    if (folio != null)
                        break;
                }
            }

            return (folio != null);
        }

        private int GetNextReferenceId(string pmsCode)
        {
            int referenceId = 1;

            // Create request
            GuestBalanceRQ request = new GuestBalanceRQ();
            request.PMSCode = pmsCode;

            // Get response from webservice
            GuestBalanceRS response = service.getGuestBalance(request);
            if (response != null && response.Item != null)
            {
                GuestBalanceRSBalances balances;
                string errorMessage;
                if (ValidateResponse(response.Item, out balances, out errorMessage))
                {
                    foreach (TransactionType transactionType in balances.Balance.SelectMany(balanceType => balanceType.Transactions))
                    {
                        int refId;
                        if (int.TryParse(transactionType.ReferenceID, out refId) && refId > referenceId)
                        {
                            referenceId = refId;
                        }
                    }
                }
            }

            return referenceId;
        }

        private bool ValidateResponse<T>(object obj, out T objCast, out string errorMessage)
        {
            objCast = default(T);
            errorMessage = string.Empty;

            if (obj == null)
            {
                errorMessage = "Validate object is null";
            }
            else if (obj is ErrorType)
            {
                ErrorType error = (ErrorType)obj;
                errorMessage = string.Format("{2} ({0} - {1})", error.Code, error.Type, error.Value);
            }
            else if (obj is WarningType)
            {
                WarningType warning = (WarningType)obj;
                errorMessage = string.Format("{2} ({0} - {1})", warning.Code, warning.Type, warning.Value);
            }
            else if (obj is T == false)
            {
                errorMessage = string.Format("Validate object ({0}) is not of type {1}", obj.GetType(), typeof(T));
            }
            else
            {
                objCast = (T)obj;
            }

            return errorMessage.Length == 0;
        }

        void TCAPosConnector_PmsMessageReceived(PmsConnector sender, IPmsMessage pmsMessage)
        {
            MessageBase message = pmsMessage as MessageBase;
            if (message == null)
                return;

            if (message.GetType() == typeof(CheckedIn))
            {
                this.LogInfo("Check In message received: RoomId: '{0}'", message.RoomId);
                
                CheckedIn checkIn = (CheckedIn)message;
                GuestInformation guestInformation = checkIn.ToGuestInformationModel();

                this.OnCheckedIn(guestInformation);
            }
            else if (message.GetType() == typeof(CheckedOut))
            {
                this.LogInfo("Check Out message received: RoomId: '{0}'", message.RoomId);

                CheckedOut checkedOut = (CheckedOut)message;
                GuestInformation guestInformation = checkedOut.ToGuestInformationModel();

                this.OnCheckedOut(guestInformation);
            }
            else if (message.GetType() == typeof(RoomMoved))
            {
                RoomMoved moveInfo = (RoomMoved)message;
                this.LogInfo("Roommove message received: From Room: '{0}', To Room: '{1}'", moveInfo.RoomIdOld, moveInfo.RoomId);

                this.OnMoved(moveInfo.RoomIdOld, string.Empty, moveInfo.RoomId, string.Empty);
            }
            else if (message.GetType() == typeof(StayUpdated))
            {
                StayUpdated stayUpdated = (StayUpdated)message;
                this.LogInfo("Refresh results message received: Room: '{0}'", stayUpdated.RoomId);

                GuestInformation guestInformation = stayUpdated.ToGuestInformationModel();

                this.OnGuestInformationUpdated(guestInformation);
            }
            else if (message.GetType() == typeof(WakeupScheduled))
            {
                WakeupScheduled wakeupScheduled = (WakeupScheduled)message;
                this.LogInfo("WakeupScheduled received. Room: {0}, Status: {1}", wakeupScheduled.RoomId, wakeupScheduled.Status);

                if (wakeupScheduled.Status != ActionType.Add)
                {
                    this.OnWakeUpCleared(wakeupScheduled.ToWakeUpStatusModel());
                }
                if (wakeupScheduled.Status == ActionType.Add || wakeupScheduled.Status == ActionType.AddUpdate || wakeupScheduled.Status == ActionType.Replace)
                {
                    this.OnWakeUpSet(wakeupScheduled.ToWakeUpStatusModel());
                }
            }
            else
            {
                this.LogWarning("Unsollicited message, message is ignored: " + message.GetMessageSignature());
            }
        }
    }
}