<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:tns="http://htng.org/2009B" elementFormDefault="qualified" targetNamespace="http://htng.org/2009B" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:simpleType name="AlphaLength3">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for an Alpha String, length exactly 3.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-zA-Z]{3}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="HotelResStatusType">
    <xs:annotation>
      <xs:documentation xml:lang="en">Hotel reservation statuses from the PMS or reservation system.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="tns:StringLength1to16">
      <xs:enumeration value="Reserved">
        <xs:annotation>
          <xs:documentation xml:lang="en">The reservation has been reserved.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
      <xs:enumeration value="Requested">
        <xs:annotation>
          <xs:documentation xml:lang="en">The reservation has been requested but has not yet been reserved.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
      <xs:enumeration value="Request denied">
        <xs:annotation>
          <xs:documentation xml:lang="en">The request for the reservation has been denied.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
      <xs:enumeration value="No-show">
        <xs:annotation>
          <xs:documentation xml:lang="en">This reservation is in "no show" status. Typically this means the person for whom this reservation belonged did not check in and the reservation was moved to "no show" status.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
      <xs:enumeration value="Cancelled">
        <xs:annotation>
          <xs:documentation xml:lang="en">This reservation has been cancelled.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
      <xs:enumeration value="In-house">
        <xs:annotation>
          <xs:documentation xml:lang="en">This reservation has been check in, and is in "in-house" status.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
      <xs:enumeration value="Checked out">
        <xs:annotation>
          <xs:documentation xml:lang="en">The guest has checked out and the reservation has been changed to "Checked out" status</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
      <xs:enumeration value="Waitlisted">
        <xs:annotation>
          <xs:documentation xml:lang="en">This reservation is in waitlist status and the reservation has not been confirmed.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="ISO3166">
    <xs:annotation>
      <xs:documentation xml:lang="en">Specifies a 2 character country code as defined in ISO3166.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-zA-Z]{2}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="ListOfStringLength1to8">
    <xs:annotation>
      <xs:documentation xml:lang="en">List of StringLength1to8.</xs:documentation>
    </xs:annotation>
    <xs:list itemType="tns:StringLength1to8" />
  </xs:simpleType>
  <xs:simpleType name="Money">
    <xs:annotation>
      <xs:documentation xml:lang="en">Specifies an amount, max 3 decimals.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:decimal">
      <xs:fractionDigits value="3" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="Numeric0to999">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Numeric values, from 0 to 999 inclusive.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:integer">
      <xs:minInclusive value="0" />
      <xs:maxInclusive value="999" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="Numeric1to3">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Numeric values, from 1 to 3 inclusive.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:integer">
      <xs:minInclusive value="1" />
      <xs:maxInclusive value="3" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="Numeric1to999">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Numeric values, from 1 to 999 inclusive.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:integer">
      <xs:minInclusive value="1" />
      <xs:maxInclusive value="999" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="NumericStringLength1to3">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Numeric Strings length 1 to 3.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[0-9]{1,3}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="NumericStringLength1to5">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Numeric Strings, length 1 to 5.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[0-9]{1,5}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="NumericStringLength1to8">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Numeric Strings, length 1 to 8.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[0-9]{1,8}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="OTA_CodeType">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for codes in the OTA code tables. Possible values of this pattern are 1, 101, 101.EQP, or 101.EQP.X.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[0-9A-Z]{1,3}(\.[A-Z]{3}(\.X){0,1}){0,1}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StateProvCodeType">
    <xs:annotation>
      <xs:documentation xml:lang="en">The standard code or abbreviation for the state, province, or region.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="tns:StringLength1to8">
      <xs:minLength value="2" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StringLength0to128">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Character Strings, length 0 to 128.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="0" />
      <xs:maxLength value="128" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StringLength0to255">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Character Strings, length 0 to 255.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="0" />
      <xs:maxLength value="255" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StringLength0to32">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Character Strings, length 0 to 32.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="0" />
      <xs:maxLength value="32" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StringLength0to64">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Character Strings, length 0 to 64.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="0" />
      <xs:maxLength value="64" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StringLength0to8">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Character Strings, length 0 to 8.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="0" />
      <xs:maxLength value="8" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StringLength1to128">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Character Strings, length 1 to 128.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="128" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StringLength1to16">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Character Strings, length 1 to 16.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="16" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StringLength1to255">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Character Strings, length 1 to 255.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="255" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StringLength1to32">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Character Strings, length 1 to 32.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="32" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StringLength3">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Strings, length exactly 3.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="3" />
      <xs:maxLength value="3" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StringLength1to64">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Character Strings, length 1 to 64.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="64" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="StringLength1to8">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for Character Strings, length 1 to 8.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:minLength value="1" />
      <xs:maxLength value="8" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="TransactionActionType">
    <xs:annotation>
      <xs:documentation xml:lang="en">To specify the type of action requested when more than one function could be handled by the message.	</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:enumeration value="Book" />
      <xs:enumeration value="Quote" />
      <xs:enumeration value="Hold" />
      <xs:enumeration value="Initiate" />
      <xs:enumeration value="Ignore" />
      <xs:enumeration value="Modify" />
      <xs:enumeration value="Commit" />
      <xs:enumeration value="Cancel" />
      <xs:enumeration value="CommitOverrideEdits">
        <xs:annotation>
          <xs:documentation xml:lang="en">Commit the transaction and override the end transaction edits.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
      <xs:enumeration value="VerifyPrice">
        <xs:annotation>
          <xs:documentation xml:lang="en">Perform a price verification.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="UpperCaseAlphaLength1to2">
    <xs:annotation>
      <xs:documentation xml:lang="en">Used for an Alpha String, length 1 to 2 (for letter codes).</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[A-Z]{1,2}" />
    </xs:restriction>
  </xs:simpleType>
  <xs:simpleType name="ActionType">
    <xs:annotation>
      <xs:documentation>Identifes an action to take place.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:enumeration value="Add-Update">
        <xs:annotation>
          <xs:documentation xml:lang="en">Typically used to add an item where it does not exist or to update an item where it does exist.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
      <xs:enumeration value="Cancel">
        <xs:annotation>
          <xs:documentation xml:lang="en">Typically used to cancel an existing item.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
      <xs:enumeration value="Delete">
        <xs:annotation>
          <xs:documentation xml:lang="en">Typically used to remove specified data.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
      <xs:enumeration value="Add">
        <xs:annotation>
          <xs:documentation xml:lang="en">Typically used to add data whether data already exists or not.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
      <xs:enumeration value="Replace">
        <xs:annotation>
          <xs:documentation xml:lang="en">Typically used to overlay existing data.</xs:documentation>
        </xs:annotation>
      </xs:enumeration>
    </xs:restriction>
  </xs:simpleType>
</xs:schema>