﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Analytics;
using Obymobi.Web.Analytics.Extensions.SpreadsheetLight;
using SpreadsheetLight;

namespace Obymobi.Services.Logic.Analytics.ReportBuilders
{
    public class PageViewsReportBuilder : IReportBuilder
    {
        private readonly CloudEnvironment cloudEnvironment;

        public PageViewsReportBuilder(CloudEnvironment cloudEnvironment)
        {
            this.cloudEnvironment = cloudEnvironment;
        }

        public bool TryBuild(ReportProcessingTaskEntity task, out ReportFile reportFile)
        {
            reportFile = null;

            Filter filter = Obymobi.Logic.HelperClasses.XmlHelper.Deserialize<Filter>(task.Filter);
            IReport report = new PageviewsReport(filter, this.cloudEnvironment);

            if (!report.RunReport(out SLDocument spreadsheet))
            {
                return false;
            }

            reportFile = new ReportFile(spreadsheet.ToFile());
            return true;
        }
    }
}
