﻿using Obymobi.Analytics.Reports;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Extensions.SpreadsheetLight;
using SpreadsheetLight;

namespace Obymobi.Services.Logic.Analytics.ReportBuilders
{
    public class KpiReportBuilder : IReportBuilder
    {
        private readonly string googleApisJwt;

        public KpiReportBuilder(string googleApisJwt)
        {
            this.googleApisJwt = googleApisJwt;
        }

        public bool TryBuild(ReportProcessingTaskEntity task, out ReportFile reportFile)
        {
            KpiReport kpiReport = new KpiReport(task, this.googleApisJwt);
            SLDocument document = kpiReport.RunReport(false);

            reportFile = new ReportFile(document.ToFile());
            return true;
        }
    }
}
