﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Services.Logic.Analytics.ReportBuilders
{
    public interface IReportBuilder
    {
        bool TryBuild(ReportProcessingTaskEntity task, out ReportFile report);
    }
}
