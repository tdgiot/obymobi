﻿using System.IO;
using Newtonsoft.Json;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Reports;
using Obymobi.Web.Analytics.Requests;

namespace Obymobi.Services.Logic.Analytics.ReportBuilders
{
    public class BatchReportBuilder : IReportBuilder
    {
        public bool TryBuild(ReportProcessingTaskEntity task, out ReportFile reportFile)
        {
            BatchReportRequest request = JsonConvert.DeserializeObject<BatchReportRequest>(task.Filter);
            BatchReport report = new BatchReport(task.FromUTC.GetValueOrDefault(), task.TillUTC.GetValueOrDefault(), request);

            while (report.RunAReport())
            {
                // Running... 
            }

            reportFile = new ReportFile(ConvertToZippedFile(report));
            return true;
        }

        private static byte[] ConvertToZippedFile(BatchReport report)
        {
            byte[] file;

            using (MemoryStream ms = new MemoryStream())
            {
                report.WriteZipFile(ms);
                file = ms.ToArray();
            }

            return file;
        }
    }
}
