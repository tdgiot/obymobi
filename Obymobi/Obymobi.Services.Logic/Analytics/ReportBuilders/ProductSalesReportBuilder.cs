﻿using Newtonsoft.Json;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Converters;
using Obymobi.Web.Analytics.Extensions.SpreadsheetLight;
using Obymobi.Web.Analytics.Reports.ProductSummaryReport;
using Obymobi.Web.Analytics.Repositories;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using Obymobi.Web.Analytics.Requests;
using SpreadsheetLight;

namespace Obymobi.Services.Logic.Analytics.ReportBuilders
{
    public class ProductSalesReportBuilder : IReportBuilder
    {
        private readonly IOrderRepository orderRepository;
        private readonly ICompanyRepository companyRepository;
        private readonly ITagRepository tagRepository;

        public ProductSalesReportBuilder(IOrderRepository orderRepository, 
            ICompanyRepository companyRepository, 
            ITagRepository tagRepository)
        {
            this.orderRepository = orderRepository;
            this.companyRepository = companyRepository;
            this.tagRepository = tagRepository;
        }

        public bool TryBuild(ReportProcessingTaskEntity task, out ReportFile reportFile)
        {
            reportFile = null;

            ProductReportRequest request = JsonConvert.DeserializeObject<ProductReportRequest>(task.Filter, new ProductReportSpreadSheetFilterConverter());

            ProductReport productReport = new ProductReport(request, orderRepository, companyRepository, tagRepository);
            if (!productReport.CreateReport(out SLDocument spreadsheet))
            {
                return false;
            }

            reportFile = new ReportFile(spreadsheet.ToFile());
            return true;
        }
    }
}
