﻿using Newtonsoft.Json;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Converters;
using Obymobi.Web.Analytics.Extensions.SpreadsheetLight;
using Obymobi.Web.Analytics.Reports;
using Obymobi.Web.Analytics.Repositories;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using Obymobi.Web.Analytics.Requests;
using SpreadsheetLight;

namespace Obymobi.Services.Logic.Analytics.ReportBuilders
{
    public class InventoryReportBuilder : IReportBuilder
    {
        private readonly IOrderRepository orderRepository;
        private readonly ICompanyRepository companyRepository;
        private readonly ITagRepository tagRepository;

        public InventoryReportBuilder(IOrderRepository orderRepository, 
            ICompanyRepository companyRepository,
            ITagRepository tagRepository)
        {
            this.orderRepository = orderRepository;
            this.companyRepository = companyRepository;
            this.tagRepository = tagRepository;
        }

        public bool TryBuild(ReportProcessingTaskEntity task, out ReportFile reportFile)
        {
            reportFile = null;

            InventoryReportRequest request = JsonConvert.DeserializeObject<InventoryReportRequest>(task.Filter, new InventoryReportSpreadSheetFilterConverter());
            

            InventoryReport inventoryReport = new InventoryReport(request, orderRepository, companyRepository, tagRepository);
            if (!inventoryReport.CreateReport(out SLDocument spreadsheet))
            {
                return false;
            }

            reportFile = new ReportFile(spreadsheet.ToFile());
            return true;
        }
    }
}
