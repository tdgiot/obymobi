﻿using Dionysos.Logging;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Analytics;
using Obymobi.Web.Analytics.Extensions.SpreadsheetLight;
using Obymobi.Web.Reporting;
using SpreadsheetLight;

namespace Obymobi.Services.Logic.Analytics.ReportBuilders
{
    public class BrokenLinksReportBuilder : IReportBuilder
    {
        private readonly ILoggingProvider loggingProvider;

        public BrokenLinksReportBuilder(ILoggingProvider loggingProvider)
        {
            this.loggingProvider = loggingProvider;
        }

        public bool TryBuild(ReportProcessingTaskEntity task, out ReportFile reportFile)
        {
            reportFile = null;

            IReport report = new ExternalContentReport(this.loggingProvider);

            if (!report.RunReport(out SLDocument spreadsheet))
            {
                return false;
            }

            reportFile = new ReportFile(spreadsheet.ToFile());
            return true;
        }
    }
}
