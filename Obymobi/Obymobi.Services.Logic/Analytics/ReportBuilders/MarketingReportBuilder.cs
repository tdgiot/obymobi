﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Analytics;
using Obymobi.Web.Analytics.Extensions.SpreadsheetLight;
using Obymobi.Web.Reporting;
using SpreadsheetLight;

namespace Obymobi.Services.Logic.Analytics.ReportBuilders
{
    public class MarketingReportBuilder : IReportBuilder
    {
        private readonly CloudEnvironment cloudEnvironment;

        public MarketingReportBuilder(CloudEnvironment cloudEnvironment)
        {
            this.cloudEnvironment = cloudEnvironment;
        }

        public bool TryBuild(ReportProcessingTaskEntity task, out ReportFile reportFile)
        {
            reportFile = null;

            Filter filter = Obymobi.Logic.HelperClasses.XmlHelper.Deserialize<Filter>(task.Filter);
            IReport report = new MarketingReport(filter, this.cloudEnvironment);

            if (!report.RunReport(out SLDocument spreadsheet))
            {
                return false;
            }

            reportFile = new ReportFile(spreadsheet.ToFile());
            return true;
        }
    }
}
