﻿using Newtonsoft.Json;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Converters;
using Obymobi.Web.Analytics.Extensions.SpreadsheetLight;
using Obymobi.Web.Analytics.Reports.TransactionsAppLess;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using Obymobi.Web.Analytics.Requests;
using SpreadsheetLight;

namespace Obymobi.Services.Logic.Analytics.ReportBuilders
{
    public class TransactionsAppLessReportBuilder : IReportBuilder
    {
        private readonly IOrderRepository orderRepository;
        private readonly ICompanyRepository companyRepository;

        public TransactionsAppLessReportBuilder(IOrderRepository orderRepository,
                                                ICompanyRepository companyRepository)
        {
            this.orderRepository = orderRepository;
            this.companyRepository = companyRepository;
        }

        public bool TryBuild(ReportProcessingTaskEntity task, out ReportFile reportFile)
        {
            reportFile = null;

            TransactionsAppLessRequest request = JsonConvert.DeserializeObject<TransactionsAppLessRequest>(task.Filter, new TransactionsAppLessSpreadSheetFilterConverter());

            OrderReport orderReport = new OrderReport(request, this.orderRepository, this.companyRepository);
            if (!orderReport.CreateReport(out SLDocument spreadsheet))
            {
                return false;
            }

            reportFile = new ReportFile(spreadsheet.ToFile());
            return true;
        }
    }
}
