﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Extensions.SpreadsheetLight;
using Obymobi.Web.Analytics.Reports;
using Obymobi.Web.Analytics.Requests;
using SpreadsheetLight;

namespace Obymobi.Services.Logic.Analytics.ReportBuilders
{
    public class TransactionsReportBuilder : IReportBuilder
    {
        public bool TryBuild(ReportProcessingTaskEntity task, out ReportFile reportFile)
        {
            reportFile = null;

            TransactionsRequest transactionsRequest = Obymobi.Logic.HelperClasses.XmlHelper.Deserialize<TransactionsRequest>(task.Filter);
            TransactionsReport report = new TransactionsReport(transactionsRequest);

            if (!report.CreateReport(out SLDocument spreadsheet))
            {
                return false;
            }

            reportFile = new ReportFile(spreadsheet.ToFile());
            return true;
        }
    }
}
