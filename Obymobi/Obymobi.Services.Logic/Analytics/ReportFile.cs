﻿namespace Obymobi.Services.Logic.Analytics
{
    public class ReportFile
    {
        public ReportFile(byte[] file)
        {
            this.File = file;
        }
        
        public byte[] File { get; }
    }
}
