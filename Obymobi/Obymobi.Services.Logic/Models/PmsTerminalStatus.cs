﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Services.Logic.Models
{
    public class PmsTerminalStatus
    {
        public int TerminalId { get; set; }
        public int CompanyId { get; set; }
        public bool IsOnline { get; set; }
        public string LastSyncAsString { get; set; }

        public bool Equals(PmsTerminalStatus obj)
        {
            return this.TerminalId == obj.TerminalId &&
                this.CompanyId == obj.CompanyId &&
                this.IsOnline == obj.IsOnline &&
                this.LastSyncAsString.Equals(obj.LastSyncAsString, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
