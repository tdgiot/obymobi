﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Services.Logic.HelperClasses
{
    public class CompanyHelper
    {
        public static CompanyEntity GetCompanyWithSupportpool(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(CompanyFields.CompanyId == companyId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathSupportpoolEntity);

            RelationCollection relations = new RelationCollection(CompanyEntityBase.Relations.SupportpoolEntityUsingSupportpoolId);

            CompanyCollection companies = new CompanyCollection();
            companies.GetMulti(filter, 0, null, relations, prefetch);

            if (companies.Count > 0)
                return companies[0];
            else
                return null;
        }
    }
}
