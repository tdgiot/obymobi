﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Services.Logic.HelperClasses
{
    public class RoutestephandlerHelper
    {
        #region Methods

        /// <summary>
        ///     Get a Obymobi.Data.CollectionClasses.RoutestephandlerEntity instance by email
        /// </summary>
        /// <returns></returns>
        public static RoutestephandlerEntity GetRoutestephandlerByEmail(string email)
        {
            PredicateExpression filter = new PredicateExpression(RoutestephandlerFields.FieldValue1 == email);

            PrefetchPath prefetch = new PrefetchPath(EntityType.RoutestephandlerEntity);
            prefetch.Add(RoutestephandlerEntityBase.PrefetchPathRoutestepEntity).SubPath.Add(RoutestepEntityBase.PrefetchPathRouteEntity);

            RoutestephandlerCollection routestephandlerCollection = new RoutestephandlerCollection();
            routestephandlerCollection.GetMulti(filter);

            if (routestephandlerCollection.Count > 0)
                return routestephandlerCollection[0];
            else
                return null;
        }

        #endregion
    }
}