﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Services.Logic.HelperClasses
{
    public class TerminalHelper
    {
        public static TerminalCollection GetOfflineTerminals(int maxMinutesSinceLastCall, bool onlyWhenInBusinessHours, bool prefetchSupportPools = false)
        {
            TerminalCollection offlineTerminals = new TerminalCollection();

            // Get all offline terminals
            TerminalCollection terminals = new TerminalCollection();
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeviceFields.LastRequestUTC < DateTime.UtcNow.AddMinutes(-1 * maxMinutesSinceLastCall));
            filter.Add(CompanyFields.UseMonitoring == true);
            filter.Add(TerminalFields.UseMonitoring == true);
            filter.Add(TerminalFields.OfflineNotificationEnabled == true);

            RelationCollection relations = new RelationCollection();
            relations.Add(TerminalEntity.Relations.CompanyEntityUsingCompanyId);
            relations.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);

            PrefetchPath pathTerminal = new PrefetchPath(EntityType.TerminalEntity);
            pathTerminal.Add(TerminalEntityBase.PrefetchPathDeviceEntity);
            if (prefetchSupportPools)
            {
                pathTerminal.Add(TerminalEntity.PrefetchPathCompanyEntity).SubPath.Add(CompanyEntity.PrefetchPathSupportpoolEntity);
            }
            else
            {
                pathTerminal.Add(TerminalEntity.PrefetchPathCompanyEntity);
            }
            
            terminals.GetMulti(filter, 0, null, relations, pathTerminal);

            if (onlyWhenInBusinessHours)
            {
                for (int i = 0; i < terminals.Count; i++)
                {
                    TerminalEntity terminal = terminals[i];
                    if (Obymobi.Logic.HelperClasses.CompanyHelper.IsCompanyInBusinessHours(terminal.CompanyId))
                    {
                        offlineTerminals.Add(terminal);
                    }
                }
            }
            else
            {
                offlineTerminals = terminals;
            }

            return offlineTerminals;
        }        
    }
}
