﻿using DevExpress.XtraScheduler;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic;
using Obymobi.Services.Logic.Converters;

namespace Obymobi.Services.Logic.Scheduling
{
    public class AppointmentConverter
    {
        private readonly CompanyTimeZoneConverter timeZoneConverter;

        public AppointmentConverter(CompanyTimeZoneConverter timeZoneConverter)
        {
            this.timeZoneConverter = timeZoneConverter;
        }

        public Appointment CreateAppointment(UIScheduleItemOccurrenceEntity occurrence)
        {
            Appointment appointment = null;

            switch (occurrence.Type)
            {
                case (int)AppointmentType.Normal:
                case (int)AppointmentType.ChangedOccurrence:
                    appointment = CreateNormalAppointment(occurrence);
                    break;
                case (int)AppointmentType.Pattern:
                    appointment = CreateRecurringAppointment(occurrence);
                    break;
            }

            return appointment;
        }

        private Appointment CreateNormalAppointment(UIScheduleItemOccurrenceEntity occurrence)
        {
            return new DevExpressAppointment(AppointmentType.Normal)
            {
                LabelKey = occurrence.UIScheduleItemOccurrenceId,
                Start = timeZoneConverter.UtcToLocalTime(occurrence.StartTimeUTC.GetValueOrDefault(), occurrence.ParentCompanyId),
                End = timeZoneConverter.UtcToLocalTime(occurrence.EndTimeUTC.GetValueOrDefault(), occurrence.ParentCompanyId)
            };
        }

        private Appointment CreateRecurringAppointment(UIScheduleItemOccurrenceEntity occurrence)
        {
            Appointment appointment = new DevExpressAppointment(AppointmentType.Pattern)
            {
                LabelKey = occurrence.UIScheduleItemOccurrenceId,
                Start = timeZoneConverter.UtcToLocalTime(occurrence.StartTimeUTC.GetValueOrDefault(), occurrence.ParentCompanyId),
                End = timeZoneConverter.UtcToLocalTime(occurrence.EndTimeUTC.GetValueOrDefault(), occurrence.ParentCompanyId),
                RecurrenceInfo =
                {
                    Type = (RecurrenceType)occurrence.RecurrenceType,
                    Range = (RecurrenceRange)occurrence.RecurrenceRange,
                    Start = timeZoneConverter.UtcToLocalTime(occurrence.RecurrenceStartUTC.GetValueOrDefault(), occurrence.ParentCompanyId),
                    End = timeZoneConverter.UtcToLocalTime(occurrence.RecurrenceEndUTC.GetValueOrDefault(), occurrence.ParentCompanyId),
                    OccurrenceCount = occurrence.RecurrenceOccurrenceCount,
                    Periodicity = occurrence.RecurrencePeriodicity,
                    DayNumber = occurrence.RecurrenceDayNumber,
                    WeekDays = (WeekDays)occurrence.RecurrenceWeekDays,
                    WeekOfMonth = (WeekOfMonth)occurrence.RecurrenceWeekOfMonth,
                    Month = occurrence.RecurrenceMonth
                }
            };

            foreach (UIScheduleItemOccurrenceEntity occurrenceException in occurrence.UIScheduleItemOccurrenceCollection)
            {
                appointment.CreateException(AppointmentType.DeletedOccurrence, occurrenceException.RecurrenceIndex);
            }

            return appointment;
        }
    }
}