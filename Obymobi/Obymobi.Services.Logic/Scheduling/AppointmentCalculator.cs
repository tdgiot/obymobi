﻿using System;
using DevExpress.XtraScheduler;
using Obymobi.Services.Logic.Scheduling.Calculators;

namespace Obymobi.Services.Logic.Scheduling
{
    public class AppointmentCalculator
    {
        private readonly SingleAppointmentCalculator singleAppointmentCalculator;
        private readonly RecurringAppointmentCalculator recurringAppointmentCalculator;

        public AppointmentCalculator()
        {
            singleAppointmentCalculator = new SingleAppointmentCalculator();
            recurringAppointmentCalculator = new RecurringAppointmentCalculator();
        }

        public Appointment GetActiveAppointment(Appointment appointment, DateTime now)
        {
            IAppointmentCalculator calculator = GetAppointmentCalculator(appointment);
            return calculator.GetActiveAppointment(appointment, now);
        }

        private IAppointmentCalculator GetAppointmentCalculator(Appointment appointment)
        {
            switch (appointment.Type)
            {
                case AppointmentType.Normal:
                    return singleAppointmentCalculator;

                case AppointmentType.Pattern:
                    return recurringAppointmentCalculator;

                case AppointmentType.ChangedOccurrence:
                    return singleAppointmentCalculator;

                default:
                    throw new ArgumentOutOfRangeException($"Unsupported appointment type. (AppointmentType={appointment.Type})");
            }
        }
    }
}