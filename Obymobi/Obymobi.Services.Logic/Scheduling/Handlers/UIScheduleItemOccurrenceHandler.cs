﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Services.Logic.Scheduling.Handlers
{
    public class UIScheduleItemOccurrenceHandler
    {
        private readonly IUIScheduleItemOccurrenceHandler scheduledMessageHandler;
        private readonly IUIScheduleItemOccurrenceHandler reportProcessingTaskTemplateHandler;

        public UIScheduleItemOccurrenceHandler()
        {
            this.scheduledMessageHandler = new ScheduledMessageHandler();
            this.reportProcessingTaskTemplateHandler = new ReportProcessingTaskTemplateHandler();
        }

        public void Handle(UIScheduleItemOccurrenceEntity occurrence)
        {
            IUIScheduleItemOccurrenceHandler handler = this.GetHandler(occurrence);
            if (handler == null) 
            {
                return;
            }

            handler.Handle(occurrence);
        }

        private IUIScheduleItemOccurrenceHandler GetHandler(UIScheduleItemOccurrenceEntity occurrence)
        {
            if (occurrence.ReportProcessingTaskTemplateId.HasValue)
            {
                return this.reportProcessingTaskTemplateHandler;
            }
            else if (occurrence.UIScheduleItemId.HasValue && occurrence.UIScheduleItemEntity.ScheduledMessageId.HasValue)
            {
                return this.scheduledMessageHandler;
            }

            return null;
        }
    }
}