﻿using System;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Services.Logic.Scheduling.Handlers
{
    public class ScheduledMessageHandler : IUIScheduleItemOccurrenceHandler
    {
        public void Handle(UIScheduleItemOccurrenceEntity occurrence)
        {
            if (!occurrence.MessagegroupId.HasValue || !occurrence.UIScheduleItemEntity.ScheduledMessageId.HasValue)
            {
                return;
            }

            if (DetermineScheduledMessageHistoryExist(occurrence))
            {
                return;
            }

            MessageEntity message = SaveScheduledMessage(occurrence);
            if (message != null)
            {
                SaveScheduledMessageHistory(occurrence, message);
            }
        }

        private static bool DetermineScheduledMessageHistoryExist(UIScheduleItemOccurrenceEntity occurrence)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ScheduledMessageHistoryFields.ScheduledMessageId == occurrence.UIScheduleItemEntity.ScheduledMessageId);
            filter.Add(ScheduledMessageHistoryFields.MessagegroupId == occurrence.MessagegroupId);
            filter.Add(ScheduledMessageHistoryFields.Sent == occurrence.LastTriggeredUTC);

            ScheduledMessageHistoryCollection scheduledMessageHistory = new ScheduledMessageHistoryCollection();
            return scheduledMessageHistory.GetDbCount(filter) > 0;
        }

        private static void SaveScheduledMessageHistory(UIScheduleItemOccurrenceEntity occurrence, MessageEntity messageEntity)
        {
            ScheduledMessageHistoryEntity historyEntity = new ScheduledMessageHistoryEntity();
            historyEntity.ParentCompanyId = messageEntity.CompanyId;
            historyEntity.ScheduledMessageId = occurrence.UIScheduleItemEntity.ScheduledMessageId.GetValueOrDefault();
            historyEntity.MessagegroupId = occurrence.MessagegroupId.GetValueOrDefault();
            historyEntity.UIScheduleItemOccurrenceId = occurrence.UIScheduleItemOccurrenceId;
            historyEntity.UIScheduleName = occurrence.UIScheduleItemEntity.UIScheduleEntity.Name;
            historyEntity.MessageId = messageEntity.MessageId;
            historyEntity.Title = messageEntity.Title;
            historyEntity.Message = messageEntity.Message;
            historyEntity.RecipientsAsString = messageEntity.RecipientsAsString;
            historyEntity.Sent = occurrence.StartTimeUTC.GetValueOrDefault();
            historyEntity.Save();
        }

        private static MessageEntity SaveScheduledMessage(UIScheduleItemOccurrenceEntity occurrence)
        {
            int duration = (int)occurrence.EndTimeUTC.GetValueOrDefault().Subtract(occurrence.StartTimeUTC.GetValueOrDefault()).TotalMinutes;
            ScheduledMessageEntity scheduledMessageEntity = occurrence.UIScheduleItemEntity.ScheduledMessageEntity;

            MessageEntity message = new MessageEntity();
            message.CompanyId = scheduledMessageEntity.CompanyId;
            message.Title = scheduledMessageEntity.Title;
            message.Message = scheduledMessageEntity.Message;
            message.Duration = duration;
            message.MediaId = scheduledMessageEntity.MediaId ?? 0;
            message.EntertainmentId = scheduledMessageEntity.EntertainmentId ?? 0;
            message.ProductCategoryId = scheduledMessageEntity.ProductCategoryId ?? 0;
            message.CategoryId = scheduledMessageEntity.CategoryId ?? 0;
            message.SiteId = scheduledMessageEntity.SiteId ?? 0;
            message.PageId = scheduledMessageEntity.PageId ?? 0;
            message.Url = scheduledMessageEntity.Url;
            message.Urgent = scheduledMessageEntity.Urgent;
            message.MessageLayoutType = scheduledMessageEntity.MessageLayoutType;
            message.RecipientsOkResultAsString = " ";
            message.RecipientsYesResultAsString = " ";
            message.RecipientsNoResultAsString = " ";
            message.RecipientsTimeOutResultAsString = " ";
            message.RecipientsClearManuallyResultAsString = " ";
            message.UpdatedBy = 0;
            message.CreatedBy = 0;
            message.Queued = true;
            message.Validator = null;

            if (message.EntertainmentId != 0 || message.ProductCategoryId != 0 || message.CategoryId != 0 || message.SiteId != 0 || message.PageId != 0 || !string.IsNullOrWhiteSpace(message.Url))
            {
                message.MessageButtonType = (int)MessageButtonType.YesNo;
            }
            else
            {
                message.MessageButtonType = (int)MessageButtonType.Ok;
            }

            DeliverypointCollection onlineDeliverypoints = DeliverypointHelper.GetDeliverypointsLinkedToOnlineClientsForCompany(scheduledMessageEntity.CompanyId);

            foreach (DeliverypointEntity deliverypoint in occurrence.MessagegroupEntity.DeliverypointCollectionViaMessagegroupDeliverypoint)
            {
                if (onlineDeliverypoints.All(x => x.DeliverypointId != deliverypoint.DeliverypointId))
                {
                    continue;
                }

                if (scheduledMessageEntity.ProductCategoryId.HasValue && scheduledMessageEntity.ProductCategoryEntity.ProductEntity.SubTypeAsEnum == ProductSubType.ServiceItems)
                {
                    bool ignoreClient = deliverypoint.ClientCollection.Any(clientEntity => clientEntity.DeviceId.HasValue && clientEntity.DeviceEntity.ApplicationVersion.Equals("19022521", StringComparison.InvariantCultureIgnoreCase));
                    if (ignoreClient)
                    {
                        continue;
                    }
                }

                message.MessageRecipientCollection.Add(MessageHelper.CreateMessageRecipientEntity(0, 0, deliverypoint.DeliverypointId, null));
            }

            message.RecipientsCount = message.MessageRecipientCollection.Count;
            message.RecipientsAsString = string.Join(",", message.MessageRecipientCollection.Select(x => x.DeliverypointId.ToString()));
            message.Save(true);

            return message;
        }
    }
}