﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Services.Logic.Scheduling.Handlers
{
    public interface IUIScheduleItemOccurrenceHandler
    {
        void Handle(UIScheduleItemOccurrenceEntity occurrence);
    }
}