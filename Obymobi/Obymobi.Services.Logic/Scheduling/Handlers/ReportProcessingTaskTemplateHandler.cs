﻿using Newtonsoft.Json;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.Analytics.Bridges.RefinedAbstractions;
using Obymobi.Web.Analytics.Factories;
using Obymobi.Web.Analytics.Requests.RequestBase;
using System;

namespace Obymobi.Services.Logic.Scheduling.Handlers
{
	public class ReportProcessingTaskTemplateHandler : IUIScheduleItemOccurrenceHandler
	{
		private readonly ReportRequestBridgeFactory reportRequestBridgeFactory;

		public ReportProcessingTaskTemplateHandler() => this.reportRequestBridgeFactory = new ReportRequestBridgeFactory();

		public void Handle(UIScheduleItemOccurrenceEntity occurrence)
		{
			if (!occurrence.ReportProcessingTaskTemplateId.HasValue)
			{
				return;
			}

			CreateReportProcessingTask(occurrence.ReportProcessingTaskTemplateEntity);
		}

		private void CreateReportProcessingTask(ReportProcessingTaskTemplateEntity template)
		{
			TimeZoneInfo companyTimeZoneInfo = template.CompanyEntity.TimeZoneInfo;
			Tuple<DateTime, DateTime> reportingPeriod = ReportingPeriodHelper.GetDateTimeForPeriod(template.ReportingPeriod, companyTimeZoneInfo);

			ReportRefinedAbstraction reportRequestFactory = reportRequestBridgeFactory.GetReportRequestRefinedAbstraction(template.ReportType);
			ReportRequest request = reportRequestFactory.CreateReportRequest(template.Filter);

			request.UntilDateTimeUtc = TimeZoneInfo.ConvertTimeToUtc(reportingPeriod.Item2, companyTimeZoneInfo);

			ReportProcessingTaskEntity task = new ReportProcessingTaskEntity
			{
				CompanyId = template.CompanyId,
				AnalyticsReportType = template.ReportType,
				Filter = JsonConvert.SerializeObject(request, Formatting.Indented),
				FromUTC = request.FromDateTimeUtc,
				TillUTC = request.UntilDateTimeUtc,
				TimeZoneOlsonId = template.CompanyEntity.TimeZoneOlsonId,
				Email = template.Email
			};
			task.ReportName = $"{template.FriendlyName} ({task.PeriodText})";
			task.Save();
		}
	}
}