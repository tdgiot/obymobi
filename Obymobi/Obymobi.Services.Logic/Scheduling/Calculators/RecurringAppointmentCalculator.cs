﻿using System;
using System.Linq;
using DevExpress.XtraScheduler;

namespace Obymobi.Services.Logic.Scheduling.Calculators
{
    public class RecurringAppointmentCalculator : IAppointmentCalculator
    {
        public Appointment GetActiveAppointment(Appointment appointment, DateTime now)
        {
            TimeInterval interval = new TimeInterval(now, TimeSpan.FromMilliseconds(1));

            OccurrenceCalculator occurrenceCalculator = OccurrenceCalculator.CreateInstance(appointment.RecurrenceInfo);
            Appointment firstOccurrence = occurrenceCalculator.CalcOccurrences(interval, appointment).FirstOrDefault();

            if (firstOccurrence == null)
            {
                return null;
            }

            if (appointment.GetExceptions().Any(x => x.RecurrenceIndex == firstOccurrence.RecurrenceIndex))
            {
                return null;
            }

            return firstOccurrence;
        }
    }
}