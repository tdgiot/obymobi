﻿using System;
using DevExpress.XtraScheduler;

namespace Obymobi.Services.Logic.Scheduling.Calculators
{
    public class SingleAppointmentCalculator : IAppointmentCalculator
    {
        public Appointment GetActiveAppointment(Appointment appointment, DateTime now)
        {
            bool isActive = appointment.Start <= now && appointment.End > now;
            return isActive ? appointment : null;
        }
    }
}