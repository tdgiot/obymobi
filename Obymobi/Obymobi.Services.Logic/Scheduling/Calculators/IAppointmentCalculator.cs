﻿using DevExpress.XtraScheduler;
using System;

namespace Obymobi.Services.Logic.Scheduling.Calculators
{
    public interface IAppointmentCalculator
    {
        Appointment GetActiveAppointment(Appointment appointment, DateTime now);
    }
}