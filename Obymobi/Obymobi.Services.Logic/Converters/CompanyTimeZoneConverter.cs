﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Services.Logic.Converters
{
    public class CompanyTimeZoneConverter
    {
        private readonly IDictionary<int, TimeZoneInfo> companyTimeZones;

        public CompanyTimeZoneConverter(CompanyCollection companies)
            => companyTimeZones = companies.ToDictionary(
                company => company.CompanyId,
                company => company.TimeZoneInfo);

        public DateTime UtcToLocalTime(DateTime utc, int? companyId)
            => utc.UtcToLocalTime(GetTimeZoneInfo(companyId.GetValueOrDefault()));

        private TimeZoneInfo GetTimeZoneInfo(int companyId)
            => companyTimeZones.ContainsKey(companyId)
            ? companyTimeZones[companyId]
            : throw new KeyNotFoundException($"Company id {companyId} does not exist in dictionary");
    }
}
