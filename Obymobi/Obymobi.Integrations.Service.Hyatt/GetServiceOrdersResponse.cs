﻿using System.Collections.Generic;

namespace Obymobi.Integrations.Service.Hyatt
{
    public class GetServiceOrdersResponse
    {
        public IEnumerable<ServiceOrder> service_orders { get; set; }
    }
}
