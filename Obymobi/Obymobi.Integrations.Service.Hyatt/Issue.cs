﻿namespace Obymobi.Integrations.Service.Hyatt
{
    public class Issue
    {
        public string id { get; set; }
        public string name { get; set; }
        public bool active { get; set; }
    }
}
