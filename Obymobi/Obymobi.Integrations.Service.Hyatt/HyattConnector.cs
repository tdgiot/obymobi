﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Dionysos;
using Newtonsoft.Json;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Integrations.Interfaces;
using System.Threading.Tasks;
using System.IO;

namespace Obymobi.Integrations.Service.Hyatt
{
    public class HyattConnector : ExternalSystemConnector, IExternalProductSynchronizer
    {
        #region Fields

        private readonly string applicationId = string.Empty;
        private readonly string apiUrl = string.Empty;
        private readonly string publicKey = string.Empty;
        private readonly string privateKey = string.Empty;
        private readonly string hotelId = string.Empty;

        private readonly ExternalProductSynchronizer externalProductSynchronizer;
        private readonly ExternalDeliverypointSynchronizer externalDeliverypointSynchronizer;

        private string accessToken = string.Empty;

        #endregion

        #region Constructors

        public HyattConnector(int companyId) : base(companyId, ExternalSystemType.Hyatt_HotSOS)
        {
            if (TEST_MODE)
            {
                this.applicationId = "d815607b46e49e7082907ee889ebf0c3";
                this.apiUrl = "https://api-uat.hyatt.com";
                this.publicKey = "d815607b46e49e7082907ee889ebf0c3";
                this.privateKey = "17d776d51e42d4b0a5b02c9121d2b80e";
                this.hotelId = "ORLAN";
            }
            else
            {
                this.applicationId = this.externalSystemEntity.StringValue1;
                this.apiUrl = this.externalSystemEntity.StringValue2;
                this.publicKey = this.externalSystemEntity.StringValue3;
                this.privateKey = this.externalSystemEntity.StringValue4;
                this.hotelId = this.externalSystemEntity.StringValue5;
            }

            this.externalProductSynchronizer = new ExternalProductSynchronizer(this.externalSystemEntity);
            this.externalDeliverypointSynchronizer = new ExternalDeliverypointSynchronizer(this.externalSystemEntity);
        }

        #endregion

        #region Properties

        public ExternalProductSynchronizer ExternalProductSynchronizer
        {
            get 
            { 
                return this.externalProductSynchronizer; 
            }
        }

        #endregion

        #region Methods

        public override bool HasValidConfiguration()
        {
            return (
                        !this.externalSystemEntity.StringValue1.IsNullOrWhiteSpace() &&
                        !this.externalSystemEntity.StringValue2.IsNullOrWhiteSpace() && 
                        !this.externalSystemEntity.StringValue3.IsNullOrWhiteSpace() &&
                        !this.externalSystemEntity.StringValue4.IsNullOrWhiteSpace() &&
                        !this.externalSystemEntity.StringValue5.IsNullOrWhiteSpace()
                   );
        }

        public async Task Authorize()
        {
            string url = this.GetUrl("/oauth/token?grant_type=client_credentials");
            string json = string.Empty;

            try
            {
                using (HttpClient client = this.GetHttpClientForAuthorization())
                {
                    var response = await client.PostAsync(url, null);
                    json = await response.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (!json.IsNullOrWhiteSpace())
            {
                Authorization authorization = JsonConvert.DeserializeObject<Authorization>(json);
                if (authorization == null)
                    throw new AuthorizationException(string.Format("Could not authorize at Hyatt platform services! Authorization object is null, json is '{0}'!", json));
                if (authorization.access_token.IsNullOrWhiteSpace())
                    throw new AuthorizationException("Could not authorize at Hyatt platform services! Authorization object is null, access token is empty!");

                this.accessToken = authorization.access_token;
            }
        }

        public Version GetHotSOSVersion()
        {
            Version version = null;

            string url = this.GetUrl("/v1/hotsos/version");

            string json = this.DoHttpGet(url);
            if (!json.IsNullOrWhiteSpace())
                version = JsonConvert.DeserializeObject<Version>(json);

            return version;
        }

        public IEnumerable<ExternalProduct> GetExternalProducts()
        {
            List<ExternalProduct> externalProducts = new List<ExternalProduct>();

            string url = this.GetUrl(string.Format("/v1/hotsos/property/{0}/issues", this.hotelId));

            string json = this.DoHttpGet(url);
            if (!json.IsNullOrWhiteSpace())
            {
                GetIssuesResponse issueCollection = JsonConvert.DeserializeObject<GetIssuesResponse>(json);
                foreach (Issue issue in issueCollection.issues)
                {
                    ExternalProduct externalProduct = new ExternalProduct
                    {
                        Id = issue.id,
                        Name = issue.name,
                        Visible = issue.active,
                        Price = 0
                    };
                    externalProducts.Add(externalProduct);
                }
            }

            return externalProducts;
        }

        public ServiceOrder CreateServiceOrder(OrderEntity orderEntity)
        {
            ExternalDeliverypointEntity externalDeliverypointEntity = this.GetExternalDeliverypointEntityForOrder(orderEntity);
            ExternalProductEntity externalProductEntity = this.GetExternalProductEntityForOrder(orderEntity);

            string url = this.GetUrl(string.Format("/v1/hotsos/property/{0}/serviceOrders", this.hotelId));

            CreateServiceOrder createServiceOrder = new CreateServiceOrder();
            createServiceOrder.applicationId = this.applicationId;
            createServiceOrder.issueId = externalProductEntity.Id;
            createServiceOrder.roomId = externalDeliverypointEntity.Id;
            createServiceOrder.newRemark = orderEntity.Notes;

            string requestJson = JsonConvert.SerializeObject(createServiceOrder);

            string response = this.DoHttpPost(url, requestJson);

            if (response.IsNullOrWhiteSpace())
                throw new ExternalSystemException(OrderProcessingError.HyattInvalidApiResult, "The Hyatt Platform Service API returned an empty result");

            if (response.Contains("\"AdditionalInformation\":") && response.Contains("\"Message\":") && response.Contains("\"Code\":"))
            {
                if (response.Contains("DUPLICATE_LIMIT_EXCEEDED"))
                    throw new ExternalSystemException(OrderProcessingError.HyattRequestAlreadyExists, string.Format("There is already an open request for issue {0} and room {1}", createServiceOrder.issueId, createServiceOrder.roomNumber));
                else
                    throw new ExternalSystemException(OrderProcessingError.HyattInvalidApiResult, "The Hyatt Platform Service API returned an invalid result: {0}", response);
            }

            return JsonConvert.DeserializeObject<ServiceOrder>(response);
        }

        public ServiceOrder GetServiceOrder(string serviceOrderId)
        {
            ServiceOrder serviceOrder = null;

            string url = this.GetUrl(string.Format("/v1/hotsos/property/{0}/serviceOrders/{1}", this.hotelId, serviceOrderId));

            string json = this.DoHttpGet(url);
            if (!json.IsNullOrWhiteSpace())
            {
                ServiceOrderWrapper wrapper = JsonConvert.DeserializeObject<ServiceOrderWrapper>(json);
                if (wrapper != null)
                    serviceOrder = wrapper.service_order;
            }

            return serviceOrder;
        }

        public IEnumerable<ServiceOrder> GetServiceOrders(string reservationId)
        {
            IEnumerable<ServiceOrder> serviceOrders = null;

            string url = this.GetUrl(string.Format("/v1/hotsos/property/{0}/serviceOrders/resvNo/{1}", this.hotelId, reservationId));

            string json = this.DoHttpGet(url);
            if (!json.IsNullOrWhiteSpace())
            {
                serviceOrders = JsonConvert.DeserializeObject<IEnumerable<ServiceOrder>>(json);
            }

            return serviceOrders;
        }

        #region Helpers methods

        private string GetUrl(string relativeUrl)
        {
            return string.Format("{0}{1}", this.apiUrl, relativeUrl);
        }

        private string DoHttpGet(string url)
        {
            string result = string.Empty;

            using (WebClient webClient = new WebClient())
            {
                webClient.Proxy = null;
                webClient.UseDefaultCredentials = false;
                webClient.Headers.Add("Content-Type", "application/json");
                webClient.Headers.Add("Authorization", string.Format("bearer {0}", this.accessToken));
                result = webClient.DownloadString(url);
            }

            return result;
        }

        private string DoHttpPost(string url, string json)
        {
            string result = string.Empty;

            using (WebClient webClient = new WebClient())
            {
                try
                {
                    webClient.Proxy = null;
                    webClient.UseDefaultCredentials = false;
                    webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                    webClient.Headers.Add(HttpRequestHeader.Authorization, string.Format("bearer {0}", this.accessToken));
                    result = webClient.UploadString(url, json);
                }
                catch (WebException wex)
                {
                    // Ready the response body when the call fails
                    using (StreamReader r = new StreamReader(wex.Response.GetResponseStream()))
                    {
                        result = r.ReadToEnd();
                    }
                }
            }

            return result;
        }

        public HttpClient GetHttpClientForAuthorization()
        {
            var authValue = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{0}:{1}", this.publicKey, this.privateKey))));

            var client = new HttpClient(){
                DefaultRequestHeaders = { Authorization = authValue}
                //Set some other client defaults like timeout / BaseAddress
            };

            return client;
        }

        #endregion

        #endregion
    }
}
