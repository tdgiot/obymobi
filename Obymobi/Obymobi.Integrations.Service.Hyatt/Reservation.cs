﻿using System;

namespace Obymobi.Integrations.Service.Hyatt
{
    public class Reservation
    {
        public Accompanyingguest[] AccompanyingGuests { get; set; }
        public Alert[] Alerts { get; set; }
        public string AlternateFirstName { get; set; }
        public string AlternateLastName { get; set; }
        public string AlternateNameTitle { get; set; }
        public string AlternateSalutation { get; set; }
        public string ArrivalDate { get; set; }
        public string ArrivalTime { get; set; }
        public string BlockName { get; set; }
        public string BonusCodes { get; set; }
        public string BusinessDateCreated { get; set; }
        public string CancelPolicy { get; set; }
        public string ChainCode { get; set; }
        public string CommissionPolicy { get; set; }
        public string ComputedReservationStatus { get; set; }
        public bool ConfidentialRate { get; set; }
        public string ConfirmationNumber { get; set; }
        public int CurrentBalanceAmount { get; set; }
        public string CurrentBalanceCurrencyCode { get; set; }
        public Dailycharge[] DailyCharges { get; set; }
        public string DepartureDate { get; set; }
        public string DepartureTime { get; set; }
        public int DepositAmount { get; set; }
        public string DepositAmountCurrencyCode { get; set; }
        public int DepositDueAmount { get; set; }
        public string DepositDueCurrencyCode { get; set; }
        public string DepositDueDate { get; set; }
        public string DepositPolicy { get; set; }
        public int DiscountAmount { get; set; }
        public string DiscountReason { get; set; }
        public string DiscountType { get; set; }
        public bool DoNotMove { get; set; }
        public string ExternalReference { get; set; }
        public string ExternalReferenceType { get; set; }
        public Fixedcharge[] FixedCharges { get; set; }
        public Guarantee Guarantee { get; set; }
        public string GuaranteePolicy { get; set; }
        public string GuestFirstName { get; set; }
        public string GuestGender { get; set; }
        public string GuestLanguagePreference { get; set; }
        public string GuestLastName { get; set; }
        public string GuestMiddleName { get; set; }
        public int GuestNameID { get; set; }
        public string GuestNameSuffix { get; set; }
        public string GuestNameTitle { get; set; }
        public string InvBlockCode { get; set; }
        public string LegNumber { get; set; }
        public string MarketingInformation { get; set; }
        public string Miscellaneous { get; set; }
        public Namemembership[] NameMemberships { get; set; }
        public bool NoPost { get; set; }
        public int NumberOfAdults { get; set; }
        public int NumberOfChildren { get; set; }
        public int NumberOfRooms { get; set; }
        public string PackageOptions { get; set; }
        public Package[] Packages { get; set; }
        public Paymentmethod[] PaymentMethods { get; set; }
        public Paymentrouting[] PaymentRoutings { get; set; }
        public string PenaltyPolicy { get; set; }
        public string PointsPolicy { get; set; }
        public Profilesummary[] ProfileSummaries { get; set; }
        public Profile[] Profiles { get; set; }
        public string Promotion { get; set; }
        public string PromotionCode { get; set; }
        public string PromotionCodes { get; set; }
        public string PropertyName { get; set; }
        public string ProprietaryGDSCode { get; set; }
        public bool Queued { get; set; }
        public string RatePlanCategory { get; set; }
        public string RatePlanCode { get; set; }
        public string RatePlanDescription { get; set; }
        public string RateRules { get; set; }
        public string ReservationComment { get; set; }
        public Reservationcomment[] ReservationComments { get; set; }
        public string ReservationStatus { get; set; }
        public Reservationtrace[] ReservationTraces { get; set; }
        public int ResvNameID { get; set; }
        public string[] RoomFeatures { get; set; }
        public string RoomNumber { get; set; }
        public Roomrate[] RoomRates { get; set; }
        public string RoomStatus { get; set; }
        public string RoomType { get; set; }
        public string RoomTypeDescription { get; set; }
        public string RoomTypeShortDescription { get; set; }
        public string RoomTypeToCharge { get; set; }
        public string RoomTypeToChargeDescription { get; set; }
        public string RoomTypeToChargeShortDescription { get; set; }
        public Sharingguest[] SharingGuests { get; set; }
        public string[] SpecialRequestCodes { get; set; }
        public string Spirit { get; set; }
        public string TaSpecialRequest { get; set; }
        public bool TaxInclusive { get; set; }
        public string TaxInformation { get; set; }
        public int TotalAmount { get; set; }
        public string TotalCurrencyCode { get; set; }
        public int TotalFixedCharges { get; set; }
        public int TotalRoomRateAndPackages { get; set; }
        public int TotalTaxesAndFees { get; set; }
        public bool TurnDown { get; set; }
        public Userdefinedfield[] UserDefinedFields { get; set; }
        public string VipCode { get; set; }
    }

    public class Guarantee
    {
        public bool CancellationRequired { get; set; }
        public bool CreditCardRequired { get; set; }
        public string Deadline { get; set; }
        public string Description { get; set; }
        public string GuaranteeType { get; set; }
        public Guaranteesaccepted[] GuaranteesAccepted { get; set; }
        public bool MandatoryDeposit { get; set; }
        public string PaymentType { get; set; }
    }

    public class Guaranteesaccepted
    {
        public string AgentPhone { get; set; }
        public string CardType { get; set; }
        public string CreditCardNumber { get; set; }
        public string Expiration { get; set; }
        public int GuaranteeCompanyID { get; set; }
        public int GuaranteeTravelAgentID { get; set; }
    }

    public class Accompanyingguest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int NameId { get; set; }
    }

    public class Alert
    {
        public string Area { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool Global { get; set; }
        public int Id { get; set; }
        public bool PrinterNotification { get; set; }
        public bool ScreenNotification { get; set; }
        public string Type { get; set; }
    }

    public class Dailycharge
    {
        public Fixedchargelistandtotals FixedChargeListAndTotals { get; set; }
        public string PostingDate { get; set; }
        public Roomandpackagecharges RoomAndPackageCharges { get; set; }
        public Taxesandfees TaxesAndFees { get; set; }
        public int TotalCharges { get; set; }
    }

    public class Fixedchargelistandtotals
    {
        public Fixedcharge[] FixedCharges { get; set; }
        public int TotalCharges { get; set; }
    }

    public class Fixedcharge
    {
        public int Amount { get; set; }
        public string AmountCurrencyCode { get; set; }
        public string BeginDate { get; set; }
        public DateTime DateToExecute { get; set; }
        public int DayToExecute { get; set; }
        public string Description { get; set; }
        public string EndDate { get; set; }
        public int FixedChargeId { get; set; }
        public string Frequency { get; set; }
        public int Quantity { get; set; }
        public string Supplement { get; set; }
        public int TaxesAndFeesAmount { get; set; }
        public string TaxesAndFeesCurrencyCode { get; set; }
        public string TransactionCode { get; set; }
    }

    public class Roomandpackagecharges
    {
        public Charge[] Charges { get; set; }
        public int TotalCharges { get; set; }
    }

    public class Charge
    {
        public int Amount { get; set; }
        public string Code { get; set; }
        public string CodeType { get; set; }
        public string CurrencyCode { get; set; }
        public string Description { get; set; }
        public bool FixedRate { get; set; }
    }

    public class Taxesandfees
    {
        public Charge[] Charges { get; set; }
        public int TotalCharges { get; set; }
    }

    public class Namemembership
    {
        public bool Active { get; set; }
        public bool Central { get; set; }
        public int CurrentPoints { get; set; }
        public int DisplaySequence { get; set; }
        public string EffectiveDate { get; set; }
        public string EnrolledAt { get; set; }
        public string EnrollmentCode { get; set; }
        public string EnrollmentSource { get; set; }
        public string ExpirationDate { get; set; }
        public string ExternalId { get; set; }
        public string MemberName { get; set; }
        public string MembershipClass { get; set; }
        public int MembershipId { get; set; }
        public string MembershipLevel { get; set; }
        public string MembershipNextLevel { get; set; }
        public string MembershipNumber { get; set; }
        public string MembershipType { get; set; }
        public bool NameMembershipActive { get; set; }
        public int OperaId { get; set; }
        public string PointsLabel { get; set; }
        public bool Preferred { get; set; }
        public bool Primary { get; set; }
        public int ResvNameId { get; set; }
        public string Status { get; set; }
        public int TransferPoints { get; set; }
        public bool UsedInReservation { get; set; }
    }

    public class Package
    {
        public int Allowance { get; set; }
        public int Amount { get; set; }
        public string CalculationRule { get; set; }
        public string Description { get; set; }
        public string EndDate { get; set; }
        public string EndTime { get; set; }
        public bool IsAddRateCombinedLine { get; set; }
        public bool IsAddRateSeparateLine { get; set; }
        public bool IsIncludedInRate { get; set; }
        public bool IsSellSeparate { get; set; }
        public bool IsTaxIncluded { get; set; }
        public int PackageAmount { get; set; }
        public string PackageCode { get; set; }
        public string PackageGroup { get; set; }
        public string PostingRhythm { get; set; }
        public int Quantity { get; set; }
        public string ShortDescription { get; set; }
        public string Source { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public float TaxAmount { get; set; }
        public int TotalDepositAmount { get; set; }
    }

    public class Paymentmethod
    {
        public string AccountCode { get; set; }
        public int AmountOrPercentage { get; set; }
        public int ApprovalAmount { get; set; }
        public int AuthorizationRule { get; set; }
        public string AuthorizationRuleDescription { get; set; }
        public int CardID { get; set; }
        public string CardolderName { get; set; }
        public string CreditCardLastFour { get; set; }
        public string CreditCardNumber { get; set; }
        public string CreditCardToken { get; set; }
        public string CreditCardType { get; set; }
        public bool EmailFolio { get; set; }
        public int EstimatedCashPayment { get; set; }
        public string ExpirationDate { get; set; }
        public string Owner { get; set; }
        public string PaymentMethod { get; set; }
        public bool Swiped { get; set; }
        public int Window { get; set; }
    }

    public class Paymentrouting
    {
        public string ApprovalAmount { get; set; }
        public string BeginDate { get; set; }
        public string CalculationMethod { get; set; }
        public int Covers { get; set; }
        public string CreditCardNumber { get; set; }
        public string EndDate { get; set; }
        public bool Friday { get; set; }
        public int Limit { get; set; }
        public bool Monday { get; set; }
        public string Owner { get; set; }
        public string PaymentMethod { get; set; }
        public int Percent { get; set; }
        public string Room { get; set; }
        public string RoutingInstruction { get; set; }
        public string RoutingInstructionsId { get; set; }
        public string RoutingType { get; set; }
        public bool Saturday { get; set; }
        public bool Sunday { get; set; }
        public bool Thursday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public int Window { get; set; }
    }

    public class Profilesummary
    {
        public string Name { get; set; }
        public int NameID { get; set; }
        public string Type { get; set; }
    }

    public class Profile
    {
        public bool Active { get; set; }
        public Address[] Addresses { get; set; }
        public Email[] Emails { get; set; }
        public Governmentid[] GovernmentID { get; set; }
        public string Name { get; set; }
        public string NameCode { get; set; }
        public int NameId { get; set; }
        public Namemembership[] NameMembership { get; set; }
        public string Nationality { get; set; }
        public Personname PersonName { get; set; }
        public Phone[] Phones { get; set; }
        public Preference[] Preferences { get; set; }
        public Privacyoption[] PrivacyOptions { get; set; }
        public Profilenote[] ProfileNote { get; set; }
        public bool TaxExempt { get; set; }
        public string Type { get; set; }
        public string VipCode { get; set; }
    }

    public class Personname
    {
        public string AlternateFirstName { get; set; }
        public string AlternateLastName { get; set; }
        public string AlternateNameTitle { get; set; }
        public string AlternateSalutation { get; set; }
        public string BusinessTitle { get; set; }
        public string FirstName { get; set; }
        public string GuestGender { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string NameTitle { get; set; }
    }

    public class Address
    {
        public bool Active { get; set; }
        public int AddressId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressType { get; set; }
        public string BarCode { get; set; }
        public string CityExtension { get; set; }
        public string CityName { get; set; }
        public string CountryCode { get; set; }
        public int DisplaySequence { get; set; }
        public string ExternalId { get; set; }
        public string OtherAddressType { get; set; }
        public string PostalCode { get; set; }
        public bool Primary { get; set; }
        public string StateProv { get; set; }
    }

    public class Email
    {
        public bool Active { get; set; }
        public string EmailAddress { get; set; }
        public int EmailId { get; set; }
        public bool Primary { get; set; }
        public string Type { get; set; }
    }

    public class Governmentid
    {
        public string CountryOfIssue { get; set; }
        public int DisplaySequence { get; set; }
        public int DocumentID { get; set; }
        public string DocumentNumber { get; set; }
        public string DocumentType { get; set; }
        public string EffectiveDate { get; set; }
        public string ExpirationDate { get; set; }
        public string PlaceOfIssue { get; set; }
        public bool Primary { get; set; }
        public string Resort { get; set; }
    }

    public class Phone
    {
        public bool Active { get; set; }
        public string AreaCode { get; set; }
        public string CountryAccessCode { get; set; }
        public int DisplaySequence { get; set; }
        public string Extension { get; set; }
        public string ExternalId { get; set; }
        public int PhoneId { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneRole { get; set; }
        public string PhoneType { get; set; }
        public bool Primary { get; set; }
        public bool PrimaryPhone { get; set; }
    }

    public class Preference
    {
        public bool Active { get; set; }
        public string Description { get; set; }
        public string OtherPreferenceType { get; set; }
        public string PreferenceType { get; set; }
        public string PreferenceValue { get; set; }
        public string ResortCode { get; set; }
    }

    public class Privacyoption
    {
        public string OptionType { get; set; }
        public string OptionValue { get; set; }
    }

    public class Profilenote
    {
        public string Note { get; set; }
        public string NoteCode { get; set; }
        public int NoteId { get; set; }
        public string NoteTitle { get; set; }
    }

    public class Reservationcomment
    {
        public string Description { get; set; }
        public string Id { get; set; }
        public bool Internal { get; set; }
        public string Type { get; set; }
    }

    public class Reservationtrace
    {
        public string Department { get; set; }
        public string Id { get; set; }
        public string ResolvedBy { get; set; }
        public string TraceDate { get; set; }
        public string TraceResolvedDate { get; set; }
        public string TraceText { get; set; }
        public string TraceTime { get; set; }
    }

    public class Roomrate
    {
        public int BaseAmount { get; set; }
        public string BaseCurrencyCode { get; set; }
        public string EffectiveDate { get; set; }
        public string EndDate { get; set; }
        public bool RateChangeIndicator { get; set; }
        public string RateCode { get; set; }
        public string RoomType { get; set; }
        public string StartDate { get; set; }
        public bool SuppressRate { get; set; }
    }

    public class Sharingguest
    {
        public int GuestNameID { get; set; }
        public string Name { get; set; }
        public int ResvNameID { get; set; }
        public int ShareAmount { get; set; }
    }

    public class Userdefinedfield
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
