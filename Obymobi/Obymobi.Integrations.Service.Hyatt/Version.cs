﻿namespace Obymobi.Integrations.Service.Hyatt
{
    public class Version
    {
        public string implementation_version { get;set; }

        public string implementation_title { get;set; }

        public string specification_version { get;set; }

        public string specification_title { get;set; }

        public string build_number { get;set; }

        public string environment_configuration { get; set; }
    }
}
