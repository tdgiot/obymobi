﻿using System.Collections.Generic;

namespace Obymobi.Integrations.Service.Hyatt
{
    public class GetIssuesResponse
    {
        public IEnumerable<Issue> issues { get; set; }
    }
}
