﻿using System;

namespace Obymobi.Integrations.Service.Hyatt
{
    public class CreateServiceOrder
    {
        public string applicationId { get; set; }
        public string issueId { get; set; }
        public string roomId { get; set; }

        /// <remarks>Optional</remarks>
        public String roomNumber { get; set; }

        /// <remarks>Optional</remarks>
        public String requestedById { get; set; }

        /// <remarks>Optional</remarks>
        public DateTime? actionTime { get; set; }

        /// <remarks>Optional</remarks>
        public String newRemark { get; set; }

        /// <remarks>Optional</remarks>
        public String reservationId { get; set; }

        /// <remarks>Optional</remarks>
        public String assignedId { get; set; }

        /// <remarks>Optional</remarks>
        public int? priority { get; set; }

        /// <remarks>Optional</remarks>
        public String trade { get; set; }
    }
}
