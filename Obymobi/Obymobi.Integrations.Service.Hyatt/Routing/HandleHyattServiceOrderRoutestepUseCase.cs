﻿using System;
using System.Data;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logging;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.Routing.Requests;
using Obymobi.Logic.Routing.UseCases;

namespace Obymobi.Integrations.Service.Hyatt.Routing
{
    public class HandleHyattServiceOrderRoutestepUseCase : HandleRoutestepUseCaseBase<HandleRoutestepRequest>
    {
        public override void HandleRoutestep(HandleRoutestepRequest request)
        {
            int orderRoutestephandlerEntityId = request.OrderRoutestephandlerEntityId;
            var orderRoutestephandlerEntity = new OrderRoutestephandlerEntity(orderRoutestephandlerEntityId);
            if (!orderRoutestephandlerEntity.IsNew)
            {
                try
                {
                    PrefetchPath prefetch = new PrefetchPath(EntityType.OrderEntity);
                    prefetch.Add(OrderEntity.PrefetchPathDeliverypointEntity).SubPath.Add(DeliverypointEntity.PrefetchPathDeliverypointExternalDeliverypointCollection);
                    prefetch.Add(OrderEntity.PrefetchPathOrderitemCollection).SubPath.Add(OrderitemEntity.PrefetchPathProductEntity).SubPath.Add(ProductEntity.PrefetchPathExternalProductEntity);

                    var orderEntity = new OrderEntity(orderRoutestephandlerEntity.OrderId, prefetch);
                    if (!orderEntity.IsNew)
                    {
                        // Create a Hyatt service request based on the current order
                        HyattConnector connector = new HyattConnector(orderEntity.CompanyId);
                        if (connector.CreateServiceOrder(orderEntity) != null)
                            orderRoutestephandlerEntity.StatusAsEnum = OrderRoutestephandlerStatus.Completed;
                    }
                }
                catch (ExternalSystemException esEx)
                {
                    orderRoutestephandlerEntity.StatusAsEnum = OrderRoutestephandlerStatus.Failed;
                    orderRoutestephandlerEntity.ErrorCodeAsEnum = esEx.ErrorType;
                    orderRoutestephandlerEntity.OrderEntity.ErrorText = string.Format("An external system exception was thrown whilst trying to create the service order. Exception {0}", esEx.Message);
                    Exceptionlogger.CreateExceptionlog(esEx);
                }
                catch (Exception ex)
                {
                    orderRoutestephandlerEntity.StatusAsEnum = OrderRoutestephandlerStatus.Failed;
                    orderRoutestephandlerEntity.OrderEntity.ErrorText = string.Format("An exception was thrown whilst trying to create  the service order. Exception {0}", ex.Message);
                    Exceptionlogger.CreateExceptionlog(ex);
                }
                finally
                {
                    // Create a new transaction
                    var transaction = new Transaction(IsolationLevel.ReadCommitted, nameof(HandleHyattServiceOrderRoutestepUseCase));

                    // Start working with the transaction
                    try
                    {
                        orderRoutestephandlerEntity.AddToTransaction(transaction);
                        orderRoutestephandlerEntity.Save();

                        // Commit the transaction
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        // Something went wrong, rollback the full transaction
                        transaction.Rollback();

                        Exceptionlogger.CreateExceptionlog(ex);
                        throw ex;
                    }
                    finally
                    {
                        // Dispose the Transaction to free the DB connection
                        transaction.Dispose();
                    }
                }
            }
        }
    }
}
