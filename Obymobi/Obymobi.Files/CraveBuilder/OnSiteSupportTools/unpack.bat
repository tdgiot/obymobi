@ECHO OFF
echo -------------------------------------
echo Updating Crave On-site SupportTools...
echo -------------------------------------
taskkill /im CraveOnsiteSupportTools.exe /f
if %errorlevel%==9009 (
	tskill CraveOnsiteSupportTools
)
echo -------------------------------------
xcopy "C:\Program Files\Crave\Crave Onsite SupportTools\Updates\CraveOnsiteSupportTools\unpacked" "C:\Program Files\Crave\Crave Onsite SupportTools" /E /C /R /I /K /Y
echo -------------------------------------
NET START "CraveOnsiteSupportTools"
echo -------------------------------------
echo Done
echo -------------------------------------


