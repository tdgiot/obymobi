@ECHO OFF
echo -------------------------------------
echo Updating Crave On-site Agent...
echo -------------------------------------
taskkill /im CraveOnsiteAgent.exe /f
if %errorlevel%==9009 (
	tskill CraveOnsiteAgent
)
echo -------------------------------------
xcopy "C:\Program Files\Crave\Crave Onsite Agent\Updates\CraveOnsiteAgent\unpacked" "C:\Program Files\Crave\Crave Onsite Agent" /E /C /R /I /K /Y
echo -------------------------------------
NET START "CraveOnsiteAgent"
echo -------------------------------------
echo Done
echo -------------------------------------


