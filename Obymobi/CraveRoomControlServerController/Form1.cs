﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using Crave.RoomControl.Enums;
using Crave.RoomControl.Models;
using Newtonsoft.Json;
using Obymobi.Security;

namespace CraveRoomControlServerController
{
    public partial class Form1 : Form
    {
        private const string TOKEN = "smdepy5zehj9z3hiy942iff4tdaalq9pmw3kz5dq";

        public Form1()
        {
            InitializeComponent();

            this.cbCommand.DataSource = Enum.GetValues(typeof(CommandType));
        }

        private bool CheckConfig()
        {
            Uri uriResult;
            bool result = Uri.TryCreate(this.tbHost.Text, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            if (!result)
            {
                MessageBox.Show("Host is not a valid URL", "Error", MessageBoxButtons.OK);
                return false;
            }

            if (this.tbRoom.TextLength == 0)
            {
                MessageBox.Show("Room number has not been filled", "Error", MessageBoxButtons.OK);
                return false;
            }

            return true;
        }

        private string CallServer(string path)
        {
            try
            {
                HttpClient httpClient = new HttpClient();
                httpClient.Timeout = TimeSpan.FromSeconds(8);
                return httpClient.GetStringAsync(string.Format("{0}/{1}", this.tbHost.Text, path)).Result;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        #region Events

        private void rbHost_Leave(object sender, EventArgs e)
        {
            if (this.tbHost.Text.EndsWith("/"))
            {
                this.tbHost.Text = this.tbHost.Text.Substring(0, this.tbHost.TextLength - 1);
            }
        }

        private async void btnSendUpdate_Click(object sender, EventArgs e)
        {
            if (!CheckConfig()) return;

            this.btnSendUpdate.Enabled = false;

            CommandType commandType;
            if (!Enum.TryParse(this.cbCommand.SelectedValue.ToString(), out commandType))
            {
                return;
            }

            await Task.Factory.StartNew(() =>
                                        {
                                            string hash = Hasher.GetHashFromParameters(TOKEN, this.tbRoom.Text, (int)commandType, this.tbParam1.Text, this.tbParam2.Text, this.tbDeviceAddress.Text);
                                            string url = string.Format("command?roomId={0}&command={1}&parameter1={2}&parameter2={3}&deviceAddress={4}&hash={5}",
                                                                       this.tbRoom.Text,
                                                                       (int)commandType,
                                                                       this.tbParam1.Text,
                                                                       this.tbParam2.Text,
                                                                       this.tbDeviceAddress.Text,
                                                                       hash);
                                            CallServer(url);

                                            if (this.cbNotify.Checked)
                                            {
                                                url = string.Format("debug?notify=1&roomId={0}&command={1}&parameter1={2}&parameter2={3}",
                                                                    this.tbRoom.Text,
                                                                    (int)commandType,
                                                                    this.tbParam1.Text,
                                                                    this.tbParam2.Text);
                                                CallServer(url);
                                            }
                                        }, TaskCreationOptions.LongRunning);

            this.btnSendUpdate.Enabled = true;
        }

        private async void btnRefreshConfig_Click(object sender, EventArgs e)
        {
            if (!CheckConfig()) return;

            this.btnRefreshConfig.Enabled = false;

            IProgress<string> progress = new Progress<string>(s => tbConfig.Text = s);
            await Task.Factory.StartNew(() =>
                                        {
                                            string response = CallServer("items?roomId=" + this.tbRoom.Text);
                                            string result;
                                            try
                                            {
                                                Room[] model = JsonConvert.DeserializeObject<Room[]>(response);
                                                result = JsonConvert.SerializeObject(model, Formatting.Indented);
                                            }
                                            catch (Exception ex)
                                            {
                                                result = "Unable to deserialize:" + response + "\n\r" + ex.Message;
                                            }

                                            progress.Report(result);
                                        }, TaskCreationOptions.LongRunning);

            this.btnRefreshConfig.Enabled = true;
        }

        #endregion

        private void cbCommand_SelectedIndexChanged(object sender, EventArgs e)
        {
            CommandType commandType;
            if (!Enum.TryParse(this.cbCommand.SelectedValue.ToString(), out commandType))
            {
                return;
            }

            string param1 = "", param2 = "";

            switch (commandType)
            {
                case CommandType.HvacSetTargetTemperature:
                    param1 = "Temperature";
                    param2 = "HVAC ID";
                    break;
                case CommandType.HvacSetMode:
                    param1 = "Mode";
                    param2 = "HVAC ID";
                    break;
                case CommandType.HvacSetFanMode:
                    param1 = "Fan Mode";
                    param2 = "HVAC ID";
                    break;
                case CommandType.HvacSetScale:
                    param1 = "Scale type";
                    param2 = "HVAC ID";
                    break;
                case CommandType.BlindOpen:
                case CommandType.BlindClose:
                case CommandType.BlindToggle:
                case CommandType.BlindStop:
                    param1 = "Blind ID";
                    param2 = "Is Curtain (1 | 0)";
                    break;
                case CommandType.LightSetLevel:
                    param1 = "Light ID";
                    param2 = "Light level";
                    break;
                case CommandType.SceneActivate:
                case CommandType.SceneDeactivate:
                    param1 = "Scene name";
                    break;
                case CommandType.DoNotDisturbSetState:
                case CommandType.MakeUpRoomSetState:
                    param1 = "State (0 | 1)";
                    break;
                case CommandType.ToggleLight:
                    param1 = "Light ID";
                    break;
            }

            this.lblParam1.Text = param1;
            this.tbParam1.Enabled = param1.Length > 0;
            if (!this.tbParam1.Enabled) this.tbParam1.Text = "";

            this.lblParam2.Text = param2;
            this.tbParam2.Enabled = param2.Length > 0;
            if (!this.tbParam2.Enabled) this.tbParam2.Text = "";
        }
    }
}
