using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Integrations.Interfaces;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Collections.Generic;

namespace Obymobi.Integrations.Service.Alice
{
    public class AliceConnector : ExternalSystemConnector, IExternalProductSynchronizer, IExternalDeliverypointSynchronizer
    {
        private readonly StaffApi staffApi;

        public AliceConnector(int companyId) : base(companyId, ExternalSystemType.Alice)
        {

            string apiUrl = this.externalSystemEntity.StringValue1;
            string apiKey = this.externalSystemEntity.StringValue2;
            string username = this.externalSystemEntity.StringValue3;
            string password = this.externalSystemEntity.StringValue4;
            string hotelId = this.externalSystemEntity.StringValue5;

            this.staffApi = new StaffApi(apiUrl, apiKey, username, password, hotelId);

            this.ExternalProductSynchronizer = new ExternalProductSynchronizer(this.externalSystemEntity);
            this.ExternalDeliverypointSynchronizer = new AliceDeliverypointSynchronizer(this.externalSystemEntity);
        }

        public ExternalProductSynchronizer ExternalProductSynchronizer { get; }

        public ExternalDeliverypointSynchronizer ExternalDeliverypointSynchronizer { get; }

        public override bool HasValidConfiguration()
        {
            return !this.externalSystemEntity.StringValue1.IsNullOrWhiteSpace() &&
                   !this.externalSystemEntity.StringValue2.IsNullOrWhiteSpace() &&
                   !this.externalSystemEntity.StringValue3.IsNullOrWhiteSpace() &&
                   !this.externalSystemEntity.StringValue4.IsNullOrWhiteSpace() &&
                   !this.externalSystemEntity.StringValue5.IsNullOrWhiteSpace();
        }

        public IEnumerable<ExternalProduct> GetExternalProducts()
        {
            var services = this.staffApi.GetServices();

            List<ExternalProduct> externalProducts = new List<ExternalProduct>();
            foreach (Models.Service service in services)
            {
                externalProducts.Add(new ExternalProduct
                {
                    Id = service.Id.ToString(),
                    Name = service.Name,
                    Price = (decimal)service.Price.GetValueOrDefault(0)
                });
            }

            return externalProducts;
        }

        public IEnumerable<ExternalDeliverypoint> GetExternalDeliverypoints()
        {
            PredicateExpression filter = new PredicateExpression(DeliverypointFields.CompanyId == this.externalSystemEntity.CompanyId);
            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.GetMulti(filter);

            List<ExternalDeliverypoint> externalDeliverypoints = new List<ExternalDeliverypoint>();
            foreach (DeliverypointEntity deliverypointEntity in deliverypointCollection)
            {
                externalDeliverypoints.Add(new ExternalDeliverypoint
                {
                    Id = deliverypointEntity.Number,
                    Name = deliverypointEntity.Number
                });
            }

            return externalDeliverypoints;
        }
    }
}
