﻿namespace Obymobi.Integrations.Service.Alice.Enums
{
    public enum FacilityType
    {
        FrontDesk = 0,
        Housekeeping = 1,
        BellService = 2,
        Valet = 3,
        Spa = 4,
        Restaurant = 5,
        Reservations = 6,
        RoomService = 7,
        Concierge = 8,
        Health = 9,
        Maintenance = 10
    }
}
