﻿namespace Obymobi.Integrations.Service.Alice.Enums
{
    public enum ServiceOptionDataType
    {
        Text = 0,
        Number = 1,
        Check = 2,
        SelectOne = 3,
        SelectMulti = 4,
        Date = 5,
        Time = 6,
        DateTime = 7,
        SelectOneSub = 8,
        SelectMultiSub = 9,
        Contact = 10,
        Info = 11,
        SelectOneOption = 12,
        SelectMultiOption = 13,
        CreditCard = 14,
        Checklist = 15
    }
}
