﻿namespace Obymobi.Integrations.Service.Alice.Enums
{
    public enum Statuses
    {
        New = 0,
        Approved = 1,
        Rejected = 2,
        Closed = 3
    }
}
