﻿namespace Obymobi.Integrations.Service.Alice.Enums
{
    public enum StaffServiceRequestRequester
    {
        Guest = 0,
        Internal = 1
    }
}
