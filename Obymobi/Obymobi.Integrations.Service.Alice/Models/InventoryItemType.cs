﻿using System.Collections.Generic;

namespace Obymobi.Integrations.Service.Alice.Models
{
    public class InventoryItemType
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public ICollection<InventoryItemField> Fields { get; set; }
    }
}
