﻿namespace Obymobi.Integrations.Service.Alice.Models
{
    public class ServiceRequestOptionInfo
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public string DisplayValue { get; set; }
    }
}
