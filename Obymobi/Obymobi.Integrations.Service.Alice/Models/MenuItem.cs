﻿using System.Collections.Generic;

namespace Obymobi.Integrations.Service.Alice.Models
{
    public class MenuItem
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public string Information { get; set; }

        public ICollection<MenuItemOption> Options { get; set; }

        public double? Price { get; set; }
        
        public string Currency { get; set; }
    }
}
