﻿namespace Obymobi.Integrations.Service.Alice.Models
{
    public class InventoryItemField
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public string DataType { get; set; }

        //public System.Collections.Generic.ICollection<NamedObject> Options { get; set; }

        public bool? Required { get; set; }
    }
}
