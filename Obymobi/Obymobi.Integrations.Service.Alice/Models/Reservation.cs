﻿using System;
using System.Collections.Generic;

namespace Obymobi.Integrations.Service.Alice.Models
{
    public class Reservation
    {
        public string Uuid { get; set; }

        public string ReservationNumber { get; set; }

        public string RoomNumber { get; set; }

        public string Email { get; set; }

        public string Firstname { get; set; }

        public string Language { get; set; }

        public string Lastname { get; set; }

        public string LocationCustom { get; set; }

        public string Phone { get; set; }

        public string Prefix { get; set; }

        public ICollection<ReservationCustomFieldValue> CustomFieldValues { get; set; }

        public DateTime? Start { get; set; }

        public DateTime? End { get; set; }
    }
}
