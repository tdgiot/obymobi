﻿using System;
using System.Collections.Generic;

namespace Obymobi.Integrations.Service.Alice.Models
{
    public class Menu
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public string Information { get; set; }

        public ICollection<MenuItem> Items { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }
    }
}
