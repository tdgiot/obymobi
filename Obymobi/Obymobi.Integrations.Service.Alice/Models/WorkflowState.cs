﻿namespace Obymobi.Integrations.Service.Alice.Models
{
    public class WorkflowState
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Abbreviation { get; set; }
    }
}
