﻿using System;
using System.Collections.Generic;

namespace Obymobi.Integrations.Service.Alice.Models
{
    public class ServiceRequest
    {
        public long? Id { get; set; }

        public long? ServiceId { get; set; }

        public string ServiceName { get; set; }

        public string Info { get; set; }

        public string Status { get; set; }

        public long? FacilityId { get; set; }

        public string FacilityName { get; set; }

        public ICollection<ServiceRequestOptionInfo> Options { get; set; }

        public WorkflowState WorkflowState { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? DueDate { get; set; }
    }
}
