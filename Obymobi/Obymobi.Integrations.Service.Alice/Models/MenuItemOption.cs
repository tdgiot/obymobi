﻿using System.Collections.Generic;

namespace Obymobi.Integrations.Service.Alice.Models
{
    public class MenuItemOption
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public double? Price { get; set; }

        public bool? Required { get; set; }

        public string DataType { get; set; }

        public ICollection<string> Values { get; set; }

        public ICollection<MenuItemOption> Options { get; set; }
    }
}
