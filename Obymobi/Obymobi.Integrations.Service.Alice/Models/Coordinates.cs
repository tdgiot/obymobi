﻿namespace Obymobi.Integrations.Service.Alice.Models
{
    public class Coordinates
    {
        /// <summary>Latitude</summary>
        public double? Lat { get; set; }

        /// <summary>Longitude</summary>
        public double? Lng { get; set; }
    }
}
