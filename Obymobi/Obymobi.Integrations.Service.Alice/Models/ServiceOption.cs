using System.Collections.Generic;

namespace Obymobi.Integrations.Service.Alice.Models
{
    public class ServiceOption
    {
        public long? Id { get; set; }
        
        public string DataType { get; set; }

        public string Group { get; set; }
        
        public string Name { get; set; }

        public bool? Required { get; set; }

        public ICollection<string> Values { get; set; }
    }
}
