﻿using System.Collections.Generic;

namespace Obymobi.Integrations.Service.Alice.Models
{
    public class Service
    {
        public long? Id { get; set; }
        
        public string Name { get; set; }

        public string Information { get; set; }

        public double? Price { get; set; }

        public ICollection<ServiceOption> Options { get; set; }
    }
}
