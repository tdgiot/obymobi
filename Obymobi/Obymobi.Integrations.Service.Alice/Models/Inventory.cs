﻿using System.Collections.Generic;

namespace Obymobi.Integrations.Service.Alice.Models
{
    public class Inventory
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public ICollection<InventoryItemType> InventoryItemTypes { get; set; }
    }
}
