﻿namespace Obymobi.Integrations.Service.Alice.Models
{
    public class Hotel
    {
        public long? Id { get; set; }

        public string Address { get; set; }

        public string CheckoutTime { get; set; }

        /// <summary>Full city name</summary>
        public string City { get; set; }

        public string Contact { get; set; }

        public Coordinates Coordinates { get; set; }

        public string Currency { get; set; }

        public Facility[] Facilities { get; set; }

        public string ImageUrl { get; set; }

        public string Information { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

        /// <summary>Number of rooms in the hotel</summary>
        public string Rooms { get; set; }

        /// <summary>Timezone offset in seconds</summary>
        public int? TimeZone { get; set; }
    }
}
