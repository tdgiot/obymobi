﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Integrations.Service.Alice.Models
{
    public class HskpLocationPmsStatus
    {
        [Newtonsoft.Json.JsonProperty("frontOfficeStatus", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public LocationPmsStatus FrontOfficeStatus { get; set; }

        [Newtonsoft.Json.JsonProperty("frontOfficeStatusLastUpdatedDate", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public System.DateTimeOffset? FrontOfficeStatusLastUpdatedDate { get; set; }

        [Newtonsoft.Json.JsonProperty("hskpStatus", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public LocationPmsStatus HskpStatus { get; set; }

        [Newtonsoft.Json.JsonProperty("hskpStatusLastUpdatedDate", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public System.DateTimeOffset? HskpStatusLastUpdatedDate { get; set; }

        [Newtonsoft.Json.JsonProperty("id", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public long? Id { get; set; }

        [Newtonsoft.Json.JsonProperty("roomStatus", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public LocationPmsStatus RoomStatus { get; set; }

        [Newtonsoft.Json.JsonProperty("roomStatusLastUpdatedDate", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public System.DateTimeOffset? RoomStatusLastUpdatedDate { get; set; }

        [Newtonsoft.Json.JsonProperty("roomStatusLatestReset", Required = Newtonsoft.Json.Required.Default, NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public System.DateTimeOffset? RoomStatusLatestReset { get; set; }
    }
}
