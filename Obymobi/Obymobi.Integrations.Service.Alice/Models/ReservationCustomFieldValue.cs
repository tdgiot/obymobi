﻿namespace Obymobi.Integrations.Service.Alice.Models
{
    public class ReservationCustomFieldValue
    {
        public long? Id { get; set; }

        public long? FieldId { get; set; }

        public string Value { get; set; }

        public string DisplayValue { get; set; }
    }
}
