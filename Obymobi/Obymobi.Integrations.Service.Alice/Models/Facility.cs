﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Obymobi.Integrations.Service.Alice.Enums;

namespace Obymobi.Integrations.Service.Alice.Models
{
    public class Facility
    {
        public long? Id { get; set; }

        public string Name { get; set; }

        public string Group { get; set; }

        public string Information { get; set; }

        public string Phone { get; set; }

        public Inventory[] Inventories { get; set; }

        public Menu[] Menus { get; set; }

        public Service[] Services { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public FacilityType Type { get; set; }
    }
}
