﻿namespace Obymobi.Integrations.Service.Alice.Models
{
    public class Option
    {
        /// <summary>Option id</summary>
        public long? Id { get; set; }

        public string Name { get; set; }

        /// <summary>Option value</summary>
        public string Value { get; set; }
    }
}
