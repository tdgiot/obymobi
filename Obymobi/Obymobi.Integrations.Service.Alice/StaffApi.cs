using System.Collections.Generic;
using Obymobi.Integrations.Exceptions;
using Obymobi.Integrations.Service.Alice.Models;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serializers.NewtonsoftJson;

namespace Obymobi.Integrations.Service.Alice
{
    public class StaffApi
    {
        private readonly string baseUrl;
        private readonly string apiKey;
        private readonly string hotelId;
        private readonly HttpBasicAuthenticator basicAuthenticator;

        public StaffApi(string baseUrl, string apiKey, string username, string password, string hotelId)
        {
            this.baseUrl = baseUrl.TrimEnd('/');
            this.apiKey = apiKey;
            this.hotelId = hotelId;
            this.basicAuthenticator = new HttpBasicAuthenticator(username, password);
        }

        public ICollection<Facility> GetFacilities()
        {
            IRestClient client = CreateClient();

            IRestRequest request = new RestRequest($"/staff/v1/hotels/{this.hotelId}/facilities", DataFormat.Json);
            IRestResponse<Facility[]> response = client.Get<Facility[]>(request);

            if (!string.IsNullOrWhiteSpace(response.ErrorMessage))
            {
                throw new IntegrationsException(response.ErrorMessage, response.ErrorException);
            }

            if (!response.IsSuccessful)
            {
                return new Facility[0];
            }

            return response.Data;
        }

        public ICollection<Models.Service> GetServices()
        {
            Dictionary<long, Models.Service> services = new Dictionary<long, Models.Service>();
            foreach (Facility facility in this.GetFacilities())
            {
                if (!facility.Id.HasValue)
                {
                    continue;
                }

                ICollection<Models.Service> facilityServices = this.GetServices(facility.Id.Value);
                foreach (Models.Service facilityService in facilityServices)
                {
                    if (!facilityService.Id.HasValue)
                    {
                        continue;
                    }

                    if (!services.ContainsKey(facilityService.Id.Value))
                    {
                        services.Add(facilityService.Id.Value, facilityService);
                    }
                }
            }

            return services.Values;
        }

        private ICollection<Models.Service> GetServices(long facilityId)
        {
            IRestClient client = CreateClient();
            
            IRestRequest request = new RestRequest($"/staff/v1/hotels/{this.hotelId}/facilities/{facilityId}/services", DataFormat.Json);
            IRestResponse<Models.Service[]> response = client.Get<Models.Service[]>(request);

            if (!string.IsNullOrWhiteSpace(response.ErrorMessage))
            {
                throw new IntegrationsException(response.ErrorMessage, response.ErrorException);
            }

            if (!response.IsSuccessful)
            {
                return new Models.Service[0];
            }

            return response.Data;
        }

        public ICollection<Models.Location> GetLocations()
        {
            IRestClient client = CreateClient();

            IRestRequest request = new RestRequest($"/staff/v1/hotels/{this.hotelId}/locations", DataFormat.Json);
            IRestResponse<Location[]> response = client.Get<Location[]>(request);

            if (!string.IsNullOrWhiteSpace(response.ErrorMessage))
            {
                throw new IntegrationsException(response.ErrorMessage, response.ErrorException);
            }

            if (!response.IsSuccessful)
            {
                return new Location[0];
            }

            return response.Data;
        }

        private IRestClient CreateClient()
        {
            IRestClient client = new RestClient(this.baseUrl)
                .UseSerializer<JsonNetSerializer>();

            client.Authenticator = basicAuthenticator;
            client.AddDefaultQueryParameter("apikey", this.apiKey);

            return client;
        }
    }
}
