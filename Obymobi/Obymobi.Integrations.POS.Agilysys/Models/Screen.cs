﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "screen-data-request")]
    public class ScreenDataRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
    }

    [XmlRoot(ElementName = "screen-data-request-Body")]
    public class ScreenDataRequestBody
    {
        [XmlElement(ElementName = "screen-data-request")]
        public ScreenDataRequest ScreenDataRequest { get; set; }
    }

    [XmlRoot(ElementName = "button")]
    public class Button
    {
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "button-text")]
        public string Buttontext { get; set; }
        [XmlAttribute(AttributeName = "left")]
        public string Left { get; set; }
        [XmlAttribute(AttributeName = "top")]
        public string Top { get; set; }
        [XmlAttribute(AttributeName = "width")]
        public string Width { get; set; }
        [XmlAttribute(AttributeName = "height")]
        public string Height { get; set; }
        [XmlAttribute(AttributeName = "color")]
        public string Color { get; set; }
    }

    [XmlRoot(ElementName = "screen-data")]
    public class Screen
    {
        [XmlElement(ElementName = "button")]
        public List<Button> Buttons { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "button-text")]
        public string ButtonText { get; set; }
    }

    [XmlRoot(ElementName = "screen-data-response")]
    public class ScreenDataResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "primary-screen-id")]
        public string PrimaryScreenId { get; set; }
        [XmlElement(ElementName = "screen-data")]
        public List<Screen> Screens { get; set; }
    }

    [XmlRoot(ElementName = "screen-data-response-Body")]
    public class ScreenDataResponseBody
    {
        [XmlElement(ElementName = "screen-data-response")]
        public ScreenDataResponse ScreenDataResponse { get; set; }
    }
}
