﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "room-auth-request-data")]
    public class RoomAuthRequestData
    {
        [XmlElement(ElementName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlElement(ElementName = "profit-center-id")]
        public string ProfitcenterId { get; set; }
        [XmlElement(ElementName = "table-name")]
        public string TableName { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "tender-id")]
        public string TenderId { get; set; }
        [XmlElement(ElementName = "auth-amount")]
        public AuthAmount AuthAmount { get; set; }
        [XmlElement(ElementName = "room-number")]
        public string RoomNumber { get; set; }
        [XmlElement(ElementName = "guest-name")]
        public string GuestName { get; set; }
        [XmlElement(ElementName = "account-number")]
        public string AccountNumber { get; set; }
    }

    [XmlRoot(ElementName = "room-auth-request")]
    public class RoomAuthRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader Transservicesheader { get; set; }
        [XmlElement(ElementName = "room-auth-request-data")]
        public RoomAuthRequestData RoomAuthRequestData { get; set; }
    }

    [XmlRoot(ElementName = "room-auth-request-Body")]
    public class RoomauthrequestBody
    {
        [XmlElement(ElementName = "room-auth-request")]
        public RoomAuthRequest RoomAuthRequest { get; set; }
    }

    [XmlRoot(ElementName = "room-auth-response-data")]
    public class RoomAuthResponseData
    {
        [XmlElement(ElementName = "reference-key")]
        public string ReferenceKey { get; set; }
        [XmlElement(ElementName = "draft-text")]
        public string DraftText { get; set; }
        [XmlElement(ElementName = "display-text")]
        public string DisplayText { get; set; }
    }

    [XmlRoot(ElementName = "room-auth-response")]
    public class RoomAuthResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "room-auth-response-data")]
        public RoomAuthResponseData RoomAuthResponseData { get; set; }
    }

    [XmlRoot(ElementName = "room-auth-response-Body")]
    public class RoomAuthResponseBody
    {
        [XmlElement(ElementName = "room-auth-response")]
        public RoomAuthResponse RoomAuthResponse { get; set; }
    }
}
