﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "add-giftcard-payment-request-data")]
    public class AddgiftcardPaymentRequestData
    {
        [XmlElement(ElementName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlElement(ElementName = "table-name")]
        public string TableName { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "authorization-account-number")]
        public string AuthorizationAccountNumber { get; set; }
        [XmlElement(ElementName = "auth-amount")]
        public AuthAmount AuthAmount { get; set; }
    }

    [XmlRoot(ElementName = "add-giftcard-payment-request")]
    public class AddgiftcardPaymentRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "add-giftcard-payment-request-data")]
        public AddgiftcardPaymentRequestData AddgiftcardPaymentRequestData { get; set; }
    }

    [XmlRoot(ElementName = "add-giftcard-payment-request-Body")]
    public class AddgiftcardPaymentRequestBody
    {
        [XmlElement(ElementName = "add-giftcard-payment-request")]
        public AddgiftcardPaymentRequest AddgiftcardPaymentRequest { get; set; }
    }

    [XmlRoot(ElementName = "add-giftcard-payment-response-data")]
    public class AddGiftcardPaymentResponseData
    {
        [XmlElement(ElementName = "draft-text")]
        public string DraftText { get; set; }
    }

    [XmlRoot(ElementName = "add-giftcard-payment-response")]
    public class AddGiftcardPaymentResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "add-giftcard-payment-response-data")]
        public AddGiftcardPaymentResponseData AddGiftcardPaymentResponseData { get; set; }
    }

    [XmlRoot(ElementName = "add-giftcard-payment-response-Body")]
    public class AddGiftcardPaymentResponseBody
    {
        [XmlElement(ElementName = "add-giftcard-payment-response")]
        public AddGiftcardPaymentResponse AddGiftcardPaymentResponse { get; set; }
    }
}
