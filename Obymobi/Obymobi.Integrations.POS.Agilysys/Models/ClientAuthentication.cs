﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "client-authentication-request")]
    public class ClientAuthenticationRequest
    {
        [XmlElement(ElementName = "client-id")]
        public string ClientId { get; set; }
        [XmlElement(ElementName = "authentication-code")]
        public string AuthenticationCode { get; set; }
    }

    [XmlRoot(ElementName = "client-authentication-request-Body")]
    public class ClientAuthenticationRequestBody
    {
        [XmlElement(ElementName = "client-authentication-request")]
        public ClientAuthenticationRequest ClientAuthenticationRequest { get; set; }
    }

    public class ClientAuthenticationResponse
    {
        [XmlElement(ElementName = "client-id")]
        public string ClientId { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "authentication-status")]
        public string AuthenticationStatus { get; set; }
        [XmlElement(ElementName = "session-id")]
        public string SessionId { get; set; }
    }

    [XmlRoot(ElementName = "client-authentication-response-Body")]
    public class ClientAuthenticationResponseBody
    {
        [XmlElement(ElementName = "client-authentication-response")]
        public ClientAuthenticationResponse ClientAuthenticationResponse { get; set; }
    }
}
