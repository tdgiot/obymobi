﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{

    [XmlRoot(ElementName = "payment-amount")]
    public class PaymentAmount
    {
        [XmlAttribute(AttributeName = "tender-amount")]
        public string TenderAmount { get; set; }
        [XmlAttribute(AttributeName = "tip-amount")]
        public string TipAmount { get; set; }
    }

    [XmlRoot(ElementName = "add-cash-payment-request-data")]
    public class AddCashPaymentRequestData
    {
        [XmlElement(ElementName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlElement(ElementName = "table-name")]
        public string TableName { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "payment-amount")]
        public PaymentAmount PaymentAmount { get; set; }
    }

    [XmlRoot(ElementName = "add-cash-payment-request")]
    public class AddCashPaymentRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "add-cash-payment-request-data")]
        public AddCashPaymentRequestData AddCashPaymentRequestData { get; set; }
    }

    [XmlRoot(ElementName = "add-cash-payment-request-Body")]
    public class AddCashPaymentRequestBody
    {
        [XmlElement(ElementName = "add-cash-payment-request")]
        public AddCashPaymentRequest AddCashPaymentRequest { get; set; }
    }

    [XmlRoot(ElementName = "add-cash-payment-response-data")]
    public class AddCashPaymentResponseData
    {
        [XmlElement(ElementName = "draft-text")]
        public string DraftText { get; set; }
    }

    [XmlRoot(ElementName = "add-cash-payment-response")]
    public class AddCashPaymentResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "add-cash-payment-response-data")]
        public AddCashPaymentResponseData AddCashPaymentResponseData { get; set; }
    }

    [XmlRoot(ElementName = "add-cash-payment-response-Body")]
    public class AddCashPaymentResponseBody
    {
        [XmlElement(ElementName = "add-cash-payment-response")]
        public AddCashPaymentResponse AddCashPaymentResponse { get; set; }
    }
}
