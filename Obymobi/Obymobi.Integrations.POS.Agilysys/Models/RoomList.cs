﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "track-information")]
    public class TrackInformation
    {
        [XmlAttribute(AttributeName = "location")]
        public string Location { get; set; }
        [XmlAttribute(AttributeName = "length")]
        public string Length { get; set; }
        [XmlAttribute(AttributeName = "data")]
        public string Data { get; set; }
    }

    [XmlRoot(ElementName = "room-list-request-data")]
    public class RoomListRequestData
    {
        [XmlElement(ElementName = "profit-center-id")]
        public string ProfitcenterId { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "tender-id")]
        public string TenderId { get; set; }
        [XmlElement(ElementName = "room-number")]
        public string RoomNumber { get; set; }
        [XmlElement(ElementName = "guest-name")]
        public string GuestName { get; set; }
        [XmlElement(ElementName = "track-information")]
        public List<TrackInformation> TrackInformation { get; set; }
    }

    [XmlRoot(ElementName = "room-list-request")]
    public class RoomListRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "room-list-request-data")]
        public RoomListRequestData RoomListRequestData { get; set; }
    }

    [XmlRoot(ElementName = "room-list-request-Body")]
    public class RoomListRequestBody
    {
        [XmlElement(ElementName = "room-list-request")]
        public RoomListRequest RoomListRequest { get; set; }
    }

    [XmlRoot(ElementName = "room-account-guest-record")]
    public class RoomAccountGuestRecord
    {
        [XmlElement(ElementName = "room-number")]
        public string RoomNumber { get; set; }
        [XmlElement(ElementName = "account-number")]
        public string AccountNumber { get; set; }
        [XmlElement(ElementName = "guest-name")]
        public string GuestName { get; set; }
        [XmlElement(ElementName = "charge-allowed")]
        public string ChargeAllowed { get; set; }
    }

    [XmlRoot(ElementName = "room-list-response-data")]
    public class RoomListResponseData
    {
        [XmlElement(ElementName = "room-account-guest-record")]
        public List<RoomAccountGuestRecord> RoomAccountGuestRecords { get; set; }
    }

    [XmlRoot(ElementName = "room-list-response")]
    public class RoomListResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "room-list-response-data")]
        public RoomListResponseData RoomListResponseData { get; set; }
    }

    [XmlRoot(ElementName = "room-list-response-Body")]
    public class RoomListResponseBody
    {
        [XmlElement(ElementName = "room-list-response")]
        public RoomListResponse RoomListResponse { get; set; }
    }
}
