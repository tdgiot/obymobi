﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "account-request-data")]
    public class AccountRequestData
    {
        [XmlElement(ElementName = "profit-center-id")]
        public string ProfitcenterId { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "inquiry-class")]
        public string InquiryClass { get; set; }
        [XmlElement(ElementName = "tender-id")]
        public string TenderId { get; set; }
        [XmlElement(ElementName = "account-number")]
        public string AccountNumber { get; set; }
        [XmlElement(ElementName = "account-name")]
        public string AccountName { get; set; }
        [XmlElement(ElementName = "track-information")]
        public List<TrackInformation> TrackInformations { get; set; }
    }

    [XmlRoot(ElementName = "account-request")]
    public class AccountRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "account-request-data")]
        public AccountRequestData AccountRequestData { get; set; }
    }

    [XmlRoot(ElementName = "account-request-Body")]
    public class AccountRequestBody
    {
        [XmlElement(ElementName = "account-request")]
        public AccountRequest AccountRequest { get; set; }
    }

    [XmlRoot(ElementName = "account-record")]
    public class AccountRecord
    {
        [XmlElement(ElementName = "account-name")]
        public string AccountName { get; set; }
        [XmlElement(ElementName = "authorization-account")]
        public string AuthorizationAccount { get; set; }
        [XmlElement(ElementName = "posting-account")]
        public string PostingAccount { get; set; }
        [XmlElement(ElementName = "account-type")]
        public string AccountType { get; set; }
        [XmlElement(ElementName = "charge-allowed")]
        public string ChargeAllowed { get; set; }
        [XmlElement(ElementName = "charges-to-date")]
        public string ChargesToDate { get; set; }
        [XmlElement(ElementName = "limit-on-account")]
        public string LimitOnAccount { get; set; }
        [XmlElement(ElementName = "remaining-amount")]
        public string RemainingAmount { get; set; }
        [XmlElement(ElementName = "additional-data")]
        public string AdditionalData { get; set; }
    }

    [XmlRoot(ElementName = "account-response-data")]
    public class AccountResponseData
    {
        [XmlElement(ElementName = "account-record")]
        public List<AccountRecord> AccountRecords { get; set; }
    }

    [XmlRoot(ElementName = "account-response")]
    public class AccountResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string Servicecompletionstatus { get; set; }
        [XmlElement(ElementName = "account-response-data")]
        public AccountResponseData AccountResponseData { get; set; }
    }

    [XmlRoot(ElementName = "account-response-Body")]
    public class AccountResponseBody
    {
        [XmlElement(ElementName = "account-response")]
        public AccountResponse AccountResponse { get; set; }
    }
}
