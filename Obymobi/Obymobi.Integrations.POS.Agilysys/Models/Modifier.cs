﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "modifier-data-request")]
    public class ModifierDataRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
    }

    [XmlRoot(ElementName = "modifier-data-request-Body")]
    public class ModifierDataRequestBody
    {
        [XmlElement(ElementName = "modifier-data-request")]
        public ModifierDataRequest ModifierDataRequest { get; set; }
    }

    [XmlRoot(ElementName = "modifier-data")]
    public class Modifier
    {
        [XmlElement(ElementName = "modifier-id")]
        public string ModifierId { get; set; }
        [XmlElement(ElementName = "modifier-description")]
        public string ModifierDescription { get; set; }
        [XmlElement(ElementName = "modifier-button-text")]
        public string ModifierButtonText { get; set; }
        [XmlElement(ElementName = "choicegroup-id")]
        public string ChoicegroupId { get; set; }
    }

    [XmlRoot(ElementName = "modifier-data-response")]
    public class ModifierDataResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "modifier-data")]
        public List<Modifier> Modifiers { get; set; }
    }

    [XmlRoot(ElementName = "modifier-data-response-Body")]
    public class ModifierDataResponseBody
    {
        [XmlElement(ElementName = "modifier-data-response")]
        public ModifierDataResponse ModifierDataResponse { get; set; }
    }
}
