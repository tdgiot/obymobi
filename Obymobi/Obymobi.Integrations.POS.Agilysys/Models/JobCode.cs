﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "jobcode-data-request")]
    public class JobCodeDataRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
    }

    [XmlRoot(ElementName = "jobcode-data-request-Body")]
    public class JobCodeDataRequestBody
    {
        [XmlElement(ElementName = "jobcode-data-request")]
        public JobCodeDataRequest JobCodeDataRequest { get; set; }
    }

    [XmlRoot(ElementName = "jobcode")]
    public class JobCode
    {
        [XmlElement(ElementName = "jobcode-id")]
        public string JobCodeId { get; set; }
        [XmlElement(ElementName = "jobcode-description")]
        public string JobCodeDescription { get; set; }
        [XmlElement(ElementName = "jobcode-button-text")]
        public string JobCodeButtonText { get; set; }
    }

    [XmlRoot(ElementName = "jobcode-data-response")]
    public class JobCodeDataResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "jobcode")]
        public List<JobCode> JobCodes { get; set; }
    }

    [XmlRoot(ElementName = "jobcode-data-response-Body")]
    public class JobCodeDataResponseBody
    {
        [XmlElement(ElementName = "jobcode-data-response")]
        public JobCodeDataResponse JobCodeDataResponse { get; set; }
    }
}
