﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "get-giftcard-balance-request-data")]
    public class GetGiftcardBalanceRequestData
    {
        [XmlElement(ElementName = "table-name")]
        public string TableName { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "tender-id")]
        public string TenderId { get; set; }
        [XmlElement(ElementName = "track-information")]
        public List<TrackInformation> Trackinformations { get; set; }
    }

    [XmlRoot(ElementName = "get-giftcard-balance-request")]
    public class GetGiftcardBalanceRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "get-giftcard-balance-request-data")]
        public GetGiftcardBalanceRequestData GetGiftcardBalanceRequestData { get; set; }
    }

    [XmlRoot(ElementName = "get-giftcard-balance-request-Body")]
    public class GetGiftcardBalanceRequestBody
    {
        [XmlElement(ElementName = "get-giftcard-balance-request")]
        public GetGiftcardBalanceRequest GetGiftcardBalanceRequest { get; set; }
    }

    [XmlRoot(ElementName = "get-giftcard-balance-response-data")]
    public class GetGiftcardBalanceResponseData
    {
        [XmlElement(ElementName = "card-balance")]
        public string CardBalance { get; set; }
    }

    [XmlRoot(ElementName = "get-giftcard-balance-response")]
    public class GetGiftcardBalanceResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "get-giftcard-balance-response-data")]
        public GetGiftcardBalanceResponseData GetGiftcardBalanceResponseData { get; set; }
    }

    [XmlRoot(ElementName = "get-giftcard-balance-response-Body")]
    public class GetGiftcardBalanceResponseBody
    {
        [XmlElement(ElementName = "get-giftcard-balance-response")]
        public GetGiftcardBalanceResponse GetGiftcardBalanceResponse { get; set; }
    }

}
