﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "generic-auth-request-data")]
    public class GenericAuthRequestData
    {
        [XmlElement(ElementName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlElement(ElementName = "profit-center-id")]
        public string ProfitcenterId { get; set; }
        [XmlElement(ElementName = "table-name")]
        public string TableName { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "tender-id")]
        public string TenderId { get; set; }
        [XmlElement(ElementName = "auth-amount")]
        public AuthAmount AuthAmount { get; set; }
        [XmlElement(ElementName = "account-number")]
        public string AccountNumber { get; set; }
        [XmlElement(ElementName = "track-information")]
        public List<TrackInformation> TrackInformations { get; set; }
    }

    [XmlRoot(ElementName = "generic-auth-request")]
    public class GenericAuthRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "generic-auth-request-data")]
        public GenericAuthRequestData GenericAuthRequestData { get; set; }
    }

    [XmlRoot(ElementName = "generic-auth-request-Body")]
    public class GenericAuthRequestBody
    {
        [XmlElement(ElementName = "generic-auth-request")]
        public GenericAuthRequest GenericAuthRequest { get; set; }
    }
}
