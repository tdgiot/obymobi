﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "tender-data-request")]
    public class TenderDataRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
    }

    [XmlRoot(ElementName = "tender-data-request-Body")]
    public class TenderDataRequestBody
    {
        [XmlElement(ElementName = "tender-data-request")]
        public TenderDataRequest TenderDataRequest { get; set; }
    }

    [XmlRoot(ElementName = "tender-data")]
    public class Tender
    {
        [XmlElement(ElementName = "tender-id")]
        public string TenderId { get; set; }
        [XmlElement(ElementName = "tender-description")]
        public string TenderDescription { get; set; }
        [XmlElement(ElementName = "tender-button-text")]
        public string TenderButtonText { get; set; }
        [XmlElement(ElementName = "tender-class")]
        public string TenderClass { get; set; }
        [XmlElement(ElementName = "over-payment")]
        public string OverPayment { get; set; }
        [XmlElement(ElementName = "early-payment")]
        public string EarlyPayment { get; set; }
    }

    [XmlRoot(ElementName = "tender-data-response")]
    public class TenderDataResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "tender-data")]
        public List<Tender> Tenders { get; set; }
    }

    [XmlRoot(ElementName = "tender-data-response-Body")]
    public class TenderDataResponseBody
    {
        [XmlElement(ElementName = "tender-data-response")]
        public TenderDataResponse TenderDataResponse { get; set; }
    }
}
