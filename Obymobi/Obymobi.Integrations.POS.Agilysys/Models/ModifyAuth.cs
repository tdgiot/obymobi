﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "modify-auth-request-data")]
    public class ModifyAuthRequestData
    {
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlElement(ElementName = "reference-key")]
        public string ReferenceKey { get; set; }
        [XmlElement(ElementName = "authorization-amount")]
        public string AuthorizationAmount { get; set; }
        [XmlElement(ElementName = "void-authorization")]
        public string VoidAuthorization { get; set; }
    }

    [XmlRoot(ElementName = "modify-auth-request")]
    public class ModifyAuthRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "modify-auth-request-data")]
        public ModifyAuthRequestData ModifyAuthRequestData { get; set; }
    }

    [XmlRoot(ElementName = "modify-auth-request-Body")]
    public class ModifyAuthRequestBody
    {
        [XmlElement(ElementName = "modify-auth-request")]
        public ModifyAuthRequest ModifyAuthRequest { get; set; }
    }

    [XmlRoot(ElementName = "modify-auth-response-data")]
    public class ModifyAuthResponseData
    {
        [XmlElement(ElementName = "reference-key")]
        public string ReferenceKey { get; set; }
        [XmlElement(ElementName = "draft-text")]
        public string DraftText { get; set; }
        [XmlElement(ElementName = "display-text")]
        public string DisplayText { get; set; }
    }

    [XmlRoot(ElementName = "modify-auth-response")]
    public class ModifyAuthResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "modify-auth-response-data")]
        public ModifyAuthResponseData ModifyAuthResponseData { get; set; }
    }

    [XmlRoot(ElementName = "modify-auth-response-Body")]
    public class ModifyAuthResponseBody
    {
        [XmlElement(ElementName = "modify-auth-response")]
        public ModifyAuthResponse ModifyAuthResponse { get; set; }
    }
}
