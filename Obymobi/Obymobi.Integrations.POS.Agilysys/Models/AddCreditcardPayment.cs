﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "add-creditcard-payment-request-data")]
    public class AddCreditcardPaymentRequestData
    {
        [XmlElement(ElementName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlElement(ElementName = "table-name")]
        public string TableName { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "authorization-account-number")]
        public string AuthorizationAccountNumber { get; set; }
        [XmlElement(ElementName = "authorization-account-name")]
        public string AuthorizationAccountName { get; set; }
        [XmlElement(ElementName = "expiration-date")]
        public ExpirationDate ExpirationDate { get; set; }
        [XmlElement(ElementName = "auth-amount")]
        public AuthAmount AuthAmount { get; set; }
    }

    [XmlRoot(ElementName = "add-creditcard-payment-request")]
    public class AddCreditcardPaymentRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "add-creditcard-payment-request-data")]
        public AddCreditcardPaymentRequestData AddCreditcardPaymentRequestData { get; set; }
    }

    [XmlRoot(ElementName = "add-creditcard-payment-request-Body")]
    public class AddCreditcardPaymentRequestBody
    {
        [XmlElement(ElementName = "add-creditcard-payment-request")]
        public AddCreditcardPaymentRequest AddCreditcardPaymentRequest { get; set; }
    }

    [XmlRoot(ElementName = "add-creditcard-payment-response-data")]
    public class AddCreditcardPaymentResponseData
    {
        [XmlElement(ElementName = "reference-key")]
        public string ReferenceKey { get; set; }
        [XmlElement(ElementName = "draft-text")]
        public string DraftText { get; set; }
    }

    [XmlRoot(ElementName = "add-creditcard-payment-response")]
    public class AddCreditcardPaymentResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "add-creditcard-payment-response-data")]
        public AddCreditcardPaymentResponseData AddCreditcardPaymentResponseData { get; set; }
    }

    [XmlRoot(ElementName = "add-creditcard-payment-response-Body")]
    public class AddCreditcardPaymentResponseBody
    {
        [XmlElement(ElementName = "add-creditcard-payment-response")]
        public AddCreditcardPaymentResponse AddCreditcardPaymentResponse { get; set; }
    }
}
