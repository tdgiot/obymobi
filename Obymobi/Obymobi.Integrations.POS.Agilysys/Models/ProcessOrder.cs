﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "order-header")]
    public class OrderHeader
    {
        [XmlElement(ElementName = "table-name")]
        public string TableName { get; set; }
        [XmlElement(ElementName = "team-id")]
        public string TeamId { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "guest-count")]
        public string GuestCount { get; set; }
        [XmlElement(ElementName = "profitcenter-id")]
        public string ProfitcenterId { get; set; }
        [XmlElement(ElementName = "check-type-id")]
        public string CheckTypeId { get; set; }
        [XmlElement(ElementName = "receipt-required")]
        public string ReceiptRequired { get; set; }
    }

    [XmlRoot(ElementName = "payment-data")]
    public class PaymentData
    {
        [XmlElement(ElementName = "tender-id")]
        public string Tenderid { get; set; }
        [XmlElement(ElementName = "tender-amount-total")]
        public string TenderAmountTotal { get; set; }
        [XmlElement(ElementName = "tip-amount")]
        public string TipAmount { get; set; }
        [XmlElement(ElementName = "room-number")]
        public string RoomNumber { get; set; }
        [XmlElement(ElementName = "authorization-account-number")]
        public string AuthorizationAccountNumber { get; set; }
        [XmlElement(ElementName = "authorization-name")]
        public string AuthorizationName { get; set; }
        [XmlElement(ElementName = "additional-information")]
        public string AdditionalInformation { get; set; }
    }

    [XmlRoot(ElementName = "order-payment")]
    public class OrderPayment
    {
        [XmlElement(ElementName = "payment-data")]
        public PaymentData PaymentData { get; set; }
    }

    [XmlRoot(ElementName = "process-order-request")]
    public class ProcessOrderRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "order-type")]
        public string OrderType { get; set; }
        [XmlElement(ElementName = "order-header")]
        public OrderHeader OrderHeader { get; set; }
        [XmlElement(ElementName = "order-body")]
        public Order Order { get; set; }
        [XmlElement(ElementName = "order-payment")]
        public OrderPayment OrderPayment { get; set; }
    }

    [XmlRoot(ElementName = "process-order-request-Body")]
    public class ProcessOrderRequestBody
    {
        [XmlElement(ElementName = "process-order-request")]
        public ProcessOrderRequest ProcessOrderRequest { get; set; }
    }

    [XmlRoot(ElementName = "process-order-response")]
    public class ProcessOrderResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "service-error")]
        public ServiceErrorResponse ServiceError { get; set; }
        [XmlElement(ElementName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlElement(ElementName = "change-due")]
        public string ChangeDue { get; set; }
        [XmlElement(ElementName = "receipt-text")]
        public string ReceiptText { get; set; }
    }

    [XmlRoot(ElementName = "service-error")]
    public class ServiceErrorResponse
    {
        [XmlElement(ElementName = "service-error-id")]
        public string ServiceErrorId { get; set; }

        [XmlElement(ElementName = "service-error-display-message")]
        public string ServiceErrorDisplayMessage { get; set; }
    }


    [XmlRoot(ElementName = "process-order-response-Body")]
    public class ProcessOrderResponseBody
    {
        [XmlElement(ElementName = "process-order-response")]
        public ProcessOrderResponse ProcessOrderResponse { get; set; }
    }
}
