﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "trans-services-header")]
    public class TransServicesHeader
    {
        [XmlElement(ElementName = "client-id")]
        public string ClientId { get; set; }
        [XmlElement(ElementName = "session-id")]
        public string SessionId { get; set; }
        [XmlElement(ElementName = "authentication-code")]
        public string AuthenticationCode { get; set; }
    }

    public interface IOrderItem
    {
        
    }

    [XmlRoot(ElementName = "modifier")]
    public class OrderModifier : IOrderItem
    {
        [XmlElement(ElementName = "modifier-id")]
        public string ModifierId { get; set; }
        [XmlElement(ElementName = "modifier-quantity")]
        public string ModifierQuantity { get; set; }
        [XmlElement(ElementName = "modifier-kitchen-print-indicator")]
        public string Modifierkitchenprintindicator { get; set; }
    }

    [XmlRoot(ElementName = "item")]
    public class OrderItem : IOrderItem
    {
        [XmlElement(ElementName = "item-id")]
        public string ItemId { get; set; }
        [XmlElement(ElementName = "item-quantity")]
        public string ItemQuantity { get; set; }
        [XmlElement(ElementName = "item-price")]
        public string ItemPrice { get; set; }
        [XmlElement(ElementName = "seat-number")]
        public string SeatNumber { get; set; }
        [XmlElement(ElementName = "course-number")]
        public string CourseNumber { get; set; }
        [XmlElement(ElementName = "item-kitchen-print-indicator")]
        public string ItemKitchenPrintIndicator { get; set; }
        [XmlElement(ElementName = "modifier")]
        public List<OrderModifier> Modifiers { get; set; }
    }

    [XmlRoot(ElementName = "order-body")]
    public class Order : IXmlSerializable
    {
        [XmlIgnore]
        public List<IOrderItem> OrderItems { get; set; }
        [XmlElement(ElementName = "item")]
        public List<OrderItem> Items { get; set; }
        [XmlElement(ElementName = "modifier")]
        public List<OrderModifier> Modifiers { get; set; }
        [XmlElement(ElementName = "package")]
        public List<OrderPackage> Packages { get; set; }
        [XmlAttribute(AttributeName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlAttribute(AttributeName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlAttribute(AttributeName = "profitcenterid")]
        public string ProfitcenterId { get; set; }
        [XmlAttribute(AttributeName = "check-type-id")]
        public string CheckTypeId { get; set; }
        [XmlElement(ElementName = "typed-special-instruction")]
        public string TypedSpecialInstruction { get; set; }
        [XmlElement(ElementName = "service-charge")]
        public string ServiceCharge { get; set; }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            XmlSerializer itemSerializer = new XmlSerializer(typeof (OrderItem));
            XmlSerializer modifierSerializer = new XmlSerializer(typeof (OrderModifier));

            foreach (IOrderItem item in this.OrderItems)
            {
                if (item.GetType() == typeof(OrderItem))
                {
                    itemSerializer.Serialize(writer, item);
                }
                else if (item.GetType() == typeof(OrderModifier))
                {
                    modifierSerializer.Serialize(writer, item);
                }
            }
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
            {
                XmlSerializer itemSerializer = new XmlSerializer(typeof(OrderItem));
                XmlSerializer modifierSerializer = new XmlSerializer(typeof(OrderModifier));
                if (reader.Name == "item")
                {
                    OrderItem orderItem = (OrderItem)itemSerializer.Deserialize(reader);
                    this.OrderItems.Add(orderItem);
                }
                else if (reader.Name == "modifier")
                {
                    OrderModifier modifier = (OrderModifier)modifierSerializer.Deserialize(reader);
                    this.OrderItems.Add(modifier);
                }
            }
        }
    }

    [XmlRoot(ElementName = "package")]
    public class OrderPackageItem
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "price")]
        public string Price { get; set; }
    }

    [XmlRoot(ElementName = "package")]
    public class OrderPackage
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "price")]
        public string Price { get; set; }
        [XmlElement(ElementName = "item")]
        public List<OrderPackageItem> Items { get; set; }
    }

    [XmlRoot(ElementName = "expiration-date")]
    public class ExpirationDate
    {
        [XmlAttribute(AttributeName = "format")]
        public string Format { get; set; }
        [XmlAttribute(AttributeName = "date")]
        public string Date { get; set; }
    }

    [XmlRoot(ElementName = "auth-amount")]
    public class AuthAmount
    {
        [XmlAttribute(AttributeName = "tender-amount")]
        public string TenderAmount { get; set; }
        [XmlAttribute(AttributeName = "tip-amount")]
        public string TipAmount { get; set; }
    }
}
