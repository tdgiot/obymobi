﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "folio-auth-request-data")]
    public class FolioAuthRequestData
    {
        [XmlElement(ElementName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlElement(ElementName = "profit-center-id")]
        public string ProfitcenterId { get; set; }
        [XmlElement(ElementName = "table-name")]
        public string TableName { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "tender-id")]
        public string TenderId { get; set; }
        [XmlElement(ElementName = "auth-amount")]
        public AuthAmount AuthAmount { get; set; }
        [XmlElement(ElementName = "folio-number")]
        public string FolioNumber { get; set; }
        [XmlElement(ElementName = "track-information")]
        public List<TrackInformation> TrackInformation { get; set; }
    }

    [XmlRoot(ElementName = "folio-auth-request")]
    public class FolioAuthRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "folio-auth-request-data")]
        public FolioAuthRequestData FolioAuthRequestData { get; set; }
    }

    [XmlRoot(ElementName = "folio-auth-request-Body")]
    public class FolioAuthRequestBody
    {
        [XmlElement(ElementName = "folio-auth-request")]
        public FolioAuthRequest FolioAuthRequest { get; set; }
    }

    [XmlRoot(ElementName = "folio-auth-response-data")]
    public class FolioAuthResponseData
    {
        [XmlElement(ElementName = "reference-key")]
        public string ReferenceKey { get; set; }
        [XmlElement(ElementName = "draft-text")]
        public string DraftText { get; set; }
        [XmlElement(ElementName = "display-text")]
        public string DisplayText { get; set; }
    }

    [XmlRoot(ElementName = "folio-auth-response")]
    public class FolioAuthResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "folio-auth-response-data")]
        public FolioAuthResponseData FolioAuthResponseData { get; set; }
    }

    [XmlRoot(ElementName = "folio-auth-response-Body")]
    public class FolioAuthResponseBody
    {
        [XmlElement(ElementName = "folio-auth-response")]
        public FolioAuthResponse FolioAuthResponse { get; set; }
    }

    [XmlRoot(ElementName = "generic-auth-response-data")]
    public class GenericAuthResponseData
    {
        [XmlElement(ElementName = "reference-key")]
        public string ReferenceKey { get; set; }
        [XmlElement(ElementName = "draft-text")]
        public string DraftText { get; set; }
        [XmlElement(ElementName = "display-text")]
        public string DisplayText { get; set; }
    }

    [XmlRoot(ElementName = "generic-auth-response")]
    public class GenericAuthResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "generic-auth-response-data")]
        public GenericAuthResponseData GenericAuthResponseData { get; set; }
    }

    [XmlRoot(ElementName = "generic-auth-response-Body")]
    public class GenericAuthResponseBody
    {
        [XmlElement(ElementName = "generic-auth-response")]
        public GenericAuthResponse GenericAuthResponse { get; set; }
    }
}
