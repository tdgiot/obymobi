﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "order-search-criteria")]
    public class OrderSearchCriteria
    {
        [XmlElement(ElementName = "profitcenter-id")]
        public string ProfitcenterId { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "table-name")]
        public string TableName { get; set; }
    }

    [XmlRoot(ElementName = "order-list-request")]
    public class OrderListRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "order-search-criteria")]
        public OrderSearchCriteria OrderSearchCriteria { get; set; }
    }

    [XmlRoot(ElementName = "order-list-request-Body")]
    public class OrderListRequestBody
    {
        [XmlElement(ElementName = "order-list-request")]
        public OrderListRequest OrderListRequest { get; set; }
    }

    [XmlRoot(ElementName = "order")]
    public class OrderListOrder
    {
        [XmlAttribute(AttributeName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlAttribute(AttributeName = "table")]
        public string Table { get; set; }
        [XmlAttribute(AttributeName = "employee-id")]
        public string EmployeeId { get; set; }
    }

    [XmlRoot(ElementName = "order-list")]
    public class OrderList
    {
        [XmlElement(ElementName = "order")]
        public List<OrderListOrder> Orders { get; set; }
    }

    [XmlRoot(ElementName = "order-list-response")]
    public class OrderListResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "order-list")]
        public OrderList OrderList { get; set; }
    }

    [XmlRoot(ElementName = "order-list-response-Body")]
    public class OrderListResponseBody
    {
        [XmlElement(ElementName = "order-list-response")]
        public OrderListResponse OrderListResponse { get; set; }
    }
}
