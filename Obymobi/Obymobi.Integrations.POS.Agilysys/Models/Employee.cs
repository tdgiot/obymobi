﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "employee-data-request")]
    public class EmployeeDataRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
    }

    [XmlRoot(ElementName = "employee-data-request-Body")]
    public class EmployeeDataRequestBody
    {
        [XmlElement(ElementName = "employee-data-request")]
        public EmployeeDataRequest EmployeeDataRequest { get; set; }
    }

    [XmlRoot(ElementName = "employee")]
    public class Employee
    {
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "employee-name")]
        public string EmployeeName { get; set; }
    }

    [XmlRoot(ElementName = "employee-data-response")]
    public class EmployeeDataResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "employee")]
        public List<Employee> Employees { get; set; }
    }

    [XmlRoot(ElementName = "employee-data-response-Body")]
    public class EmployeeDataResponseBody
    {
        [XmlElement(ElementName = "employee-data-response")]
        public EmployeeDataResponse EmployeeDataResponse { get; set; }
    }
}
