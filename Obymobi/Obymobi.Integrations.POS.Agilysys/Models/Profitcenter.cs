﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "profitcenter-data-request")]
    public class ProfitcenterDataRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
    }

    [XmlRoot(ElementName = "profitcenter-data-request-Body")]
    public class ProfitcenterDataRequestBody
    {
        [XmlElement(ElementName = "profitcenter-data-request")]
        public ProfitcenterDataRequest ProfitcenterDataRequest { get; set; }
    }

    [XmlRoot(ElementName = "profitcenter-data")]
    public class Profitcenter
    {
        [XmlElement(ElementName = "profitcenter-id")]
        public string ProfitcenterId { get; set; }
        [XmlElement(ElementName = "profitcenter-primary-indicator")]
        public string ProfitcenterPrimaryIndicator { get; set; }
        [XmlElement(ElementName = "profitcenter-description")]
        public string ProfitcenterDescription { get; set; }
        [XmlElement(ElementName = "profitcenter-button-text")]
        public string ProfitcenterButtonText { get; set; }
    }

    [XmlRoot(ElementName = "profitcenter-data-response")]
    public class ProfitcenterDataResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "profitcenter-data")]
        public List<Profitcenter> Profitcenters { get; set; }
    }

    [XmlRoot(ElementName = "profitcenter-data-response-Body")]
    public class ProfitcenterdataresponseBody
    {
        [XmlElement(ElementName = "profitcenter-data-response")]
        public ProfitcenterDataResponse ProfitcenterDataResponse { get; set; }
    }
}
