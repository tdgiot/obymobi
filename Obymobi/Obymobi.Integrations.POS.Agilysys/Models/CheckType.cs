﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "check-type-data-request")]
    public class ChecktypeDataRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
    }

    [XmlRoot(ElementName = "check-type-data-request-Body")]
    public class ChecktypeDataRequestBody
    {
        [XmlElement(ElementName = "check-type-data-request")]
        public ChecktypeDataRequest ChecktypeDataRequest { get; set; }
    }

    [XmlRoot(ElementName = "check-type-data")]
    public class CheckType
    {
        [XmlElement(ElementName = "check-type-id")]
        public string CheckTypeId { get; set; }
        [XmlElement(ElementName = "check-type-name")]
        public string CheckTypeName { get; set; }
    }

    [XmlRoot(ElementName = "check-type-data-response")]
    public class CheckTypeDataResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "check-type-data")]
        public List<CheckType> CheckTypes { get; set; }
    }

    [XmlRoot(ElementName = "check-type-data-response-Body")]
    public class CheckTypeDataResponseBody
    {
        [XmlElement(ElementName = "check-type-data-response")]
        public CheckTypeDataResponse CheckTypeDataResponse { get; set; }
    }
}
