﻿using System.Xml.Serialization;

namespace Obymobi.Integrations.POS.Agilysys.Models
{
    [XmlRoot(ElementName = "creditcard-authorization-request-data")]
    public class CreditcardAuthorizationRequestData
    {
        [XmlElement(ElementName = "order-number")]
        public string OrderNumber { get; set; }
        [XmlElement(ElementName = "profit-center-id")]
        public string ProfitCenterId { get; set; }
        [XmlElement(ElementName = "table-name")]
        public string TableName { get; set; }
        [XmlElement(ElementName = "employee-id")]
        public string EmployeeId { get; set; }
        [XmlElement(ElementName = "input-type")]
        public string InputType { get; set; }
        [XmlElement(ElementName = "authorization-account-number")]
        public string AuthorizationAccountNumber { get; set; }
        [XmlElement(ElementName = "authorization-account-name")]
        public string AuthorizationAccountName { get; set; }
        [XmlElement(ElementName = "expiration-date")]
        public ExpirationDate ExpirationDate { get; set; }
        [XmlElement(ElementName = "auth-amount")]
        public AuthAmount AuthAmount { get; set; }
    }

    [XmlRoot(ElementName = "creditcard-authorization-request")]
    public class CreditcardAuthorizationRequest
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "creditcard-authorization-request-data")]
        public CreditcardAuthorizationRequestData CreditcardAuthorizationRequestData { get; set; }
    }

    [XmlRoot(ElementName = "creditcard-authorization-request-Body")]
    public class CreditcardAuthorizationRequestBody
    {
        [XmlElement(ElementName = "creditcard-authorization-request")]
        public CreditcardAuthorizationRequest CreditcardAuthorizationRequest { get; set; }
    }

    [XmlRoot(ElementName = "creditcard-authorization-response-data")]
    public class CreditcardAuthorizationResponseData
    {
        [XmlElement(ElementName = "reference-key")]
        public string ReferenceKey { get; set; }
        [XmlElement(ElementName = "draft-text")]
        public string DraftText { get; set; }
    }

    [XmlRoot(ElementName = "creditcard-authorization-response")]
    public class CreditcardAuthorizationResponse
    {
        [XmlElement(ElementName = "trans-services-header")]
        public TransServicesHeader TransServicesHeader { get; set; }
        [XmlElement(ElementName = "service-completion-status")]
        public string ServiceCompletionStatus { get; set; }
        [XmlElement(ElementName = "creditcard-authorization-response-data")]
        public CreditcardAuthorizationResponseData CreditcardAuthorizationResponseData { get; set; }
    }

    [XmlRoot(ElementName = "creditcard-authorization-response-Body")]
    public class CreditcardAuthorizationResponseBody
    {
        [XmlElement(ElementName = "creditcard-authorization-response")]
        public CreditcardAuthorizationResponse CreditcardAuthorizationResponse { get; set; }
    }
}
