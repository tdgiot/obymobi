﻿using Obymobi.Data.EntityClasses;
using Obymobi.Integrations.Interfaces;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.POS.Agilysys
{
    /// <summary>
    /// AgilysysConfigurationAdapter class
    /// </summary>
    public class AgilysysConfigurationAdapter : IConfigurationAdapter
    {

        #region Methods
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Terminal terminal)
        {
            WebserviceUrl = terminal.PosValue1;
            ClientId = terminal.PosValue2;
            AuthenticationCode = terminal.PosValue3;
            EmployeeId = terminal.PosValue4;
            CheckTypeId = terminal.PosValue5;
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(TerminalEntity terminal)
        {
            terminal.PosValue1 = WebserviceUrl;
            terminal.PosValue2 = ClientId;
            terminal.PosValue3 = AuthenticationCode;
            terminal.PosValue4 = EmployeeId;
            terminal.PosValue5 = CheckTypeId;
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            WebserviceUrl = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AgilysysWebserviceUrl);
            ClientId = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AgilysysClientId);
            AuthenticationCode = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AgilysysAuthenticationCode);
            EmployeeId = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AgilysysEmployeeId);
            CheckTypeId = Dionysos.ConfigurationManager.GetString(POSConfigurationConstants.AgilysysCheckTypeId);
        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AgilysysWebserviceUrl, WebserviceUrl);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AgilysysClientId, ClientId);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AgilysysAuthenticationCode, AuthenticationCode);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AgilysysEmployeeId, EmployeeId);
            Dionysos.ConfigurationManager.SetValue(POSConfigurationConstants.AgilysysCheckTypeId, CheckTypeId);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = WebserviceUrl;
            terminal.PosValue2 = ClientId;
            terminal.PosValue3 = AuthenticationCode;
            terminal.PosValue4 = EmployeeId;
            terminal.PosValue5 = CheckTypeId;
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Terminal terminal)
        {
            WebserviceUrl = terminal.PosValue1;
            ClientId = terminal.PosValue2;
            AuthenticationCode = terminal.PosValue3;
            EmployeeId = terminal.PosValue4;
            CheckTypeId = terminal.PosValue5;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets webservice url
        /// </summary>
        /// <value>
        /// The webservice url
        /// </value>
        public string WebserviceUrl { get; set; }
        /// <summary>
        /// Gets or sets the client id
        /// </summary>
        /// <value>
        /// The client id
        /// </value>
        public string ClientId { get; set; }
        /// <summary>
        /// Gets or sets the authentication code
        /// </summary>
        /// <value>
        /// The authentication code
        /// </value>
        public string AuthenticationCode { get; set; }
        /// <summary>
        /// Gets or sets the employee id
        /// </summary>
        /// <value>
        /// The employee id
        /// </value>
        public string EmployeeId { get; set; }
        /// <summary>
        /// Gets or sets the check type id
        /// </summary>
        /// <value>
        /// The check type id
        /// </value>
        public string CheckTypeId { get; set; }

        #endregion
    }
}
