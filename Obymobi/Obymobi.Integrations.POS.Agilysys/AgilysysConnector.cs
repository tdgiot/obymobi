﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Integrations.POS.Agilysys.Models;
using Obymobi.Integrations.POS.Interfaces;
using Obymobi.Logging;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.POS.Agilysys
{
    public class AgilysysConnector : IPOSConnector
    {
        private readonly string webserviceUrl;
        private readonly string clientId;
        private readonly string authenticationCode;
        private readonly string employeeId;
        private readonly string checkTypeId;

        private string sessionId;

        public AgilysysConnector()
        {
            this.WriteToLogExtended("Constructor", "Start");

            this.webserviceUrl = ConfigurationManager.GetString(POSConfigurationConstants.AgilysysWebserviceUrl);
            this.clientId = ConfigurationManager.GetString(POSConfigurationConstants.AgilysysClientId);
            this.authenticationCode = ConfigurationManager.GetString(POSConfigurationConstants.AgilysysAuthenticationCode);
            this.employeeId = ConfigurationManager.GetString(POSConfigurationConstants.AgilysysEmployeeId);
            this.checkTypeId = ConfigurationManager.GetString(POSConfigurationConstants.AgilysysCheckTypeId);

            if (this.webserviceUrl.IsNullOrWhiteSpace())
            {
                this.WriteToLogExtended("Constructor", "Webservice url is empty");
            }

            this.WriteToLogExtended("Constructor", "WebserviceUrl: {0}", this.webserviceUrl);
            this.WriteToLogExtended("Constructor", "Client ID: {0}", this.clientId);
            this.WriteToLogExtended("Constructor", "Authentication Code: {0}", this.authenticationCode);
            this.WriteToLogExtended("Constructor", "Employee ID: {0}", this.employeeId);
            this.WriteToLogExtended("Constructor", "Check Type ID: {0}", this.checkTypeId);

            this.WriteToLogExtended("Constructor", "End");
        }

        private TransServicesHeader GetTransServicesHeader()
        {
            return new TransServicesHeader
            {
                ClientId = this.clientId,
                AuthenticationCode = this.authenticationCode,
                SessionId = this.sessionId
            };
        }

        public void PrePosSynchronisation()
        {
            // Authenticate
            ClientAuthenticationRequestBody requestBody = new ClientAuthenticationRequestBody
            {
                ClientAuthenticationRequest = new ClientAuthenticationRequest
                {
                    ClientId = this.clientId,
                    AuthenticationCode = this.authenticationCode
                }
            };

            ClientAuthenticationResponseBody responseBody = this.PerformRequest<ClientAuthenticationResponseBody, ClientAuthenticationRequestBody>(requestBody);
            if (responseBody.ClientAuthenticationResponse.AuthenticationStatus.Equals("yes", StringComparison.InvariantCultureIgnoreCase))
            {
                this.sessionId = responseBody.ClientAuthenticationResponse.SessionId;
                this.WriteToLogExtended("PrePosSynchronisation", "Successfully authorized with client/code: {0}/{1}. Session ID: {2}", this.clientId, this.authenticationCode, this.sessionId);
            }
            else
            {
                throw new Exception("Client Authentication failed. Authentication code: " + this.authenticationCode);
            }
        }

        private List<Choicegroup> GetChoicegroups()
        {
            List<Choicegroup> choicegroups = new List<Choicegroup>();

            ChoicegroupDataRequestBody requestBody = new ChoicegroupDataRequestBody
            {
                ChoicegroupDataRequest = new ChoicegroupDataRequest
                {
                    TransServicesHeader = this.GetTransServicesHeader()
                }
            };

            ChoicegroupDataResponseBody responseBody = this.PerformRequest<ChoicegroupDataResponseBody, ChoicegroupDataRequestBody>(requestBody);
            if (responseBody.ChoicegroupDataResponse.Choicegroups != null)
            {
                choicegroups.AddRange(responseBody.ChoicegroupDataResponse.Choicegroups);
            }

            return choicegroups;
        }

        private List<Modifier> GetModifiers()
        {
            List<Modifier> modifiers = new List<Modifier>();

            ModifierDataRequestBody requestBody = new ModifierDataRequestBody
            {
                ModifierDataRequest = new ModifierDataRequest
                {
                    TransServicesHeader = this.GetTransServicesHeader()
                }
            };

            ModifierDataResponseBody responseBody = this.PerformRequest<ModifierDataResponseBody, ModifierDataRequestBody>(requestBody);
            if (responseBody.ModifierDataResponse.Modifiers != null)
            {
                modifiers.AddRange(responseBody.ModifierDataResponse.Modifiers);
            }

            return modifiers;
        }

        private List<Item> GetItems()
        {
            List<Item> items = new List<Item>();

            ItemDataRequestBody requestBody = new ItemDataRequestBody
            {
                ItemDataRequest = new ItemDataRequest
                {
                    TransServicesHeader = this.GetTransServicesHeader()
                }
            };

            ItemDataResponseBody responseBody = this.PerformRequest<ItemDataResponseBody, ItemDataRequestBody>(requestBody);
            if (responseBody.ItemDataResponse.Items != null)
            {
                items.AddRange(responseBody.ItemDataResponse.Items);
            }

            return items;
        }

        private Dictionary<string, Posalterationoption> GetPosalterationoptions()
        {
            Dictionary<string, Posalterationoption> posalterationoptions = new Dictionary<string, Posalterationoption>();

            // Get modifiers
            List<Modifier> modifiers = this.GetModifiers();

            // Create posalterationoptions
            foreach (Modifier modifier in modifiers)
            {
                Posalterationoption posalterationoption = new Posalterationoption();
                posalterationoption.ExternalId = modifier.ModifierId;
                posalterationoption.Name = modifier.ModifierDescription;
                posalterationoptions.Add(posalterationoption.ExternalId, posalterationoption);
            }

            return posalterationoptions;
        }

        private Dictionary<string, Posalteration> GetPosalterations()
        {
            Dictionary<string, Posalteration> posalterations = new Dictionary<string, Posalteration>();

            // Get posalterationoptions
            Dictionary<string, Posalterationoption> posalterationoptions = this.GetPosalterationoptions();

            // Get choicegroups
            List<Choicegroup> choicegroups = this.GetChoicegroups();

            // Create posalterations
            foreach (Choicegroup choicegroup in choicegroups)
            {
                Posalteration posalteration = new Posalteration();
                posalteration.ExternalId = choicegroup.Id;
                posalteration.Name = choicegroup.Name;
                posalteration.MinOptions = int.Parse(choicegroup.MinimumChoices);
                posalteration.MaxOptions = int.Parse(choicegroup.MaximumChoices);
                
                // Set the options
                if (choicegroup.Modifiers != null)
                {
                    List<Posalterationoption> options = new List<Posalterationoption>();
                    foreach (ChoicegroupModifier modifier in choicegroup.Modifiers)
                    {
                        if (!posalterationoptions.ContainsKey(modifier.ModifierId))
                        {
                            this.WriteToLogExtended("GetPosalterations", "Unable to find modifier (ID: {0}) for choicegroup {1}", modifier.ModifierId, choicegroup.Id);
                            continue;
                        }

                        Posalterationoption posalterationoption = posalterationoptions[modifier.ModifierId];
                        if (posalterationoption != null)
                        {
                            options.Add(posalterationoption);
                        }
                    }
                    posalteration.Posalterationoptions = options.ToArray();
                }

                posalterations.Add(posalteration.ExternalId, posalteration);
            }
            return posalterations;
        }

        public Posproduct[] GetPosproducts()
        {
            List<Posproduct> posproducts = new List<Posproduct>();

            // Get posalterations
            Dictionary<string, Posalteration> posalterations = this.GetPosalterations();

            // Get items
            List<Item> items = this.GetItems();

            // Create the pos models
            foreach (Item item in items)
            {
                // Create the pos products
                Posproduct posproduct = new Posproduct();
                posproduct.ExternalId = item.ItemId;
                posproduct.ExternalPoscategoryId = item.ItemProductClass;
                posproduct.Name = item.ItemDescription;
                posproduct.PriceIn = Decimal.Parse(item.ItemBaseprice) / 100;
                posproduct.VatTariff = 1;
                
                // Set the alterations
                if (item.ChoicegroupSelectionData != null && item.ChoicegroupSelectionData.ChoicegroupIds != null)
                {
                    List<Posalteration> alterations = new List<Posalteration>();
                    foreach (string choicegroupId in item.ChoicegroupSelectionData.ChoicegroupIds)
                    {
                        if (!posalterations.ContainsKey(choicegroupId))
                        {
                            this.WriteToLogExtended("GetPosproducts", "Unable to find choicegroup (ID: {0}) for item {1}", choicegroupId, item.ItemId);
                            continue;
                        }

                        Posalteration posalteration = posalterations[choicegroupId];
                        if (posalteration != null)
                        {
                            alterations.Add(posalteration);
                        }
                    }
                    posproduct.Posalterations = alterations.ToArray();
                }

                posproducts.Add(posproduct);
            }

            return posproducts.ToArray();
        }

        public Poscategory[] GetPoscategories()
        {
            List<Poscategory> poscategories = new List<Poscategory>();
            return poscategories.ToArray();
        }

        public Posdeliverypointgroup[] GetPosdeliverypointgroups()
        {
            List<Posdeliverypointgroup> posdeliverypointgroups = new List<Posdeliverypointgroup>();
            return posdeliverypointgroups.ToArray();
        }

        public Posdeliverypoint[] GetPosdeliverypoints()
        {
            List<Posdeliverypoint> posdeliverypoints = new List<Posdeliverypoint>();
            return posdeliverypoints.ToArray();
        }

        public void PostPosSynchronisation()
        {
            
        }

        public Posorder GetPosorder(string deliverypointNumber)
        {
            throw new NotImplementedException();
        }

        public Enums.OrderProcessingError SaveOrder(Posorder posorder)
        {
            ProcessOrderRequestBody requestBody = new ProcessOrderRequestBody
            {
                ProcessOrderRequest = new ProcessOrderRequest
                {
                    TransServicesHeader = this.GetTransServicesHeader(),
                    OrderHeader = new OrderHeader
                    {
                        EmployeeId = this.employeeId,
                        TableName = posorder.PosdeliverypointExternalId,
                        GuestCount = posorder.GuestCount > 0 ? posorder.GuestCount.ToString() : ""
                    },
                    Order = new Models.Order
                    {
                        //Items = new List<OrderItem>(),
                        OrderItems = new List<IOrderItem>(),
                        Modifiers = new List<OrderModifier>()
                    }
                }
            };

            foreach (Posorderitem posorderitem in posorder.Posorderitems)
            {
                OrderItem orderItem = new OrderItem
                {
                    ItemId = posorderitem.PosproductExternalId,
                    ItemQuantity = posorderitem.Quantity.ToString(),
                    Modifiers = new List<OrderModifier>()
                };

                requestBody.ProcessOrderRequest.Order.OrderItems.Add(orderItem);

                if(posorderitem.Posalterationitems != null)
                {
                    foreach (Posalterationitem posalterationitem in posorderitem.Posalterationitems)
                    {
                        OrderModifier modifier = new OrderModifier()
                        {
                            ModifierId = posalterationitem.ExternalPosalterationoptionId
                        };
                        requestBody.ProcessOrderRequest.Order.OrderItems.Add(modifier);
                    }
                }
            }

            if (!this.checkTypeId.IsNullOrWhiteSpace())
            {
                requestBody.ProcessOrderRequest.OrderHeader.CheckTypeId = this.checkTypeId;
            }

            string requestXml = XmlHelper.Serialize(requestBody);
            this.WriteToLogExtended("SaveOrder", "Submit order request:\n\r{0}", requestXml);

            ProcessOrderResponseBody responseBody = this.PerformRequest<ProcessOrderResponseBody, ProcessOrderRequestBody>(requestBody);

            string responseXml = XmlHelper.Serialize(responseBody);
            this.WriteToLogExtended("SaveOrder", "Submit order response:\n\r{0}", responseXml);

            if (!responseBody.ProcessOrderResponse.ServiceCompletionStatus.Equals("ok", StringComparison.InvariantCultureIgnoreCase))
            {
                if (responseBody.ProcessOrderResponse.ServiceError != null)
                {
                    posorder.ErrorMessage = $"{responseBody.ProcessOrderResponse.ServiceError.ServiceErrorDisplayMessage} (ServiceErrorId: {responseBody.ProcessOrderResponse.ServiceError.ServiceErrorId})\n\n";
                    posorder.ErrorMessage += $"Request: {requestXml}\n\n";
                    posorder.ErrorMessage += $"Response: {responseXml}\n\n";
                }
                else
                {
                    posorder.ErrorMessage = "No error message from POS";
                }

                return OrderProcessingError.PosErrorSaveOrderFailed;
            }
            this.WriteToLogExtended("SaveOrder", "Order saved successfully. External order ID: {0}", responseBody.ProcessOrderResponse.OrderNumber);
            return OrderProcessingError.None;
        }

        public bool PrintConfirmationReceipt(string confirmationCode, string name, string phonenumber, string deliverypointNumber)
        {
            throw new NotImplementedException();
        }

        public bool PrintServiceRequestReceipt(string serviceDescription)
        {
            throw new NotImplementedException();
        }

        public bool PrintCheckoutRequestReceipt(string checkoutDescription)
        {
            throw new NotImplementedException();
        }

        public bool NotifyLockedDeliverypoint()
        {
            throw new NotImplementedException();
        }

        public string ErrorMessage
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        private void WriteToLogExtended(string methodName, string message, params object[] args)
        {
            ConsoleLogger.WriteToLog("AgilysysConnector - {0} - {1}", methodName, string.Format(message, args));
        }

        private TResponse PerformRequest<TResponse, TRequest>(TRequest requestBody)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TRequest));

            StringWriter requestWriter = new StringWriter();
            serializer.Serialize(requestWriter, requestBody);

            StringBuilder soapRequest = new StringBuilder();
            soapRequest.AppendFormatLine("<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope\">");
            soapRequest.AppendFormatLine("   <soapenv:Header/>");
            soapRequest.AppendFormatLine("   <soapenv:Body>");
            soapRequest.Append(requestWriter);
            soapRequest.AppendFormatLine("   </soapenv:Body>");
            soapRequest.AppendFormatLine("</soap:Envelope>");

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(this.webserviceUrl);
            webRequest.Headers.Add("SOAPAction", @"http://schemas.xmlsoap.org/soap/envelope/");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";

            using (Stream stream = webRequest.GetRequestStream())
            {
                using (StreamWriter streamWriter = new StreamWriter(stream))
                {
                    streamWriter.Write(soapRequest.ToString());
                }
            }

            XmlSerializer deserializer = new XmlSerializer(typeof(TResponse));
            TResponse responseBody;
            using (StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
            {
                XDocument soapDocument = XDocument.Load(responseReader);
                XNamespace soapNamespace = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope");

                string soapBody = soapDocument.Descendants(soapNamespace + "Body").Elements().First().ToString();
                responseBody = (TResponse)deserializer.Deserialize(new StringReader(soapBody));
            }
            return responseBody;
        }
    }
}
