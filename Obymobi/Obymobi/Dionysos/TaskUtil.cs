using System;
using System.Threading.Tasks;
using System.Threading;

namespace Dionysos
{
    public static class TaskUtil
    {
        /// <summary>
        /// Starts the task delayed.
        /// </summary>
        /// <param name="task">The task.</param>
        /// <param name="milliseconds">The milliseconds.</param>
        /// <param name="action">The action.</param>
        /// <returns></returns>
        public static Task StartDelayed(this TaskFactory task, int milliseconds, Action action)
        {
            // Create a trigger used to start the task 
            var tcs = new TaskCompletionSource<object>();

            // Start a timer that will trigger it             
            var timer = new Timer(x => tcs.SetResult(null), null, milliseconds, Timeout.Infinite);

            // Create and return a task that will be scheduled when the trigger fires. 
            return tcs.Task.ContinueWith(x =>
            {
                timer.Dispose();
                action();
            });
        }

        /// <summary>  
        /// Emulates a using block with asynchronous support.  
        /// </summary>  
        /// <typeparam name="T">A type that implements <see cref="IDisposable"/></typeparam>  
        /// <param name="disposableAcquisition">A <see cref="Func{TResult}"/> that gets  
        /// an object that implements <see cref="IDisposable"/>.</param>  
        /// <param name="action">The action to perform asynchronously.</param>  
        public static Task StartWithUsing<T>(
            Func<T> disposableAcquisition, Action<T> action)
            where T : IDisposable
        {
            T disposable = disposableAcquisition();

            return Task.Factory.StartNew(() => action(disposable))
                .ContinueWith(task =>
                {
                    if (!ReferenceEquals(disposable, null))
                    {
                        disposable.Dispose();
                    }
                    return task;
                });
        }

        /// <summary>  
        /// Emulates a using block with asynchronous support.  
        /// </summary>  
        /// <typeparam name="T">A type that implements <see cref="IDisposable"/></typeparam>  
        /// <param name="disposableAcquisition">A <see cref="Func{TResult}"/> that gets  
        /// an object that implements <see cref="IDisposable"/>.</param>  
        /// <param name="taskFunc">A <see cref="Func{T,Task}"/> that  
        /// uses the <see cref="IDisposable"/> object.</param>  
        /// <returns></returns>  
        public static Task StartWithUsing<T>(
            Func<T> disposableAcquisition, Func<T, Task> taskFunc)
            where T : IDisposable
        {
            T instance = disposableAcquisition();

            return taskFunc(instance)
                .ContinueWith(task =>
                {
                    if (!ReferenceEquals(instance, null))
                    {
                        instance.Dispose();
                    }
                    return task;
                });
        }

        /// <summary>  
        /// Emulates a using block with asynchronous support for a task that returns a result.  
        /// </summary>  
        /// <typeparam name="T">A type that implements <see cref="IDisposable"/></typeparam>  
        /// <typeparam name="TResult">The type or the result produced by the task.</typeparam>  
        /// <param name="disposableAcquisition">A <see cref="Func{TResult}"/> that gets  
        /// an object that implements <see cref="IDisposable"/>.</param>  
        /// <param name="taskFunc">A <see cref="Func{T,Task}"/> that uses  
        /// the <see cref="IDisposable"/> object.</param>  
        public static Task<TResult> StartWithUsing<T, TResult>(
            Func<T> disposableAcquisition, Func<T, Task<TResult>> taskFunc)
            where T : IDisposable
        {
            T instance = disposableAcquisition();

            return taskFunc(instance)
                .ContinueWith(task =>
                {
                    if (!ReferenceEquals(instance, null))
                    {
                        instance.Dispose();
                    }
                    return task.Result;
                });
        }
    }
}
