using System;
using System.Diagnostics;
using System.Threading;
using ThreadState = System.Threading.ThreadState;

namespace Dionysos
{
    public class InfiniteLooper : IDisposable
    {
        private readonly AutoResetEvent isStopping = new AutoResetEvent(false);

        private Thread mainThread;
        private Timer mainThreadWatcher;
        private readonly object mainThreadLock = new object();

        /// <summary>
        /// Initialization action, is run once
        /// </summary>
        private readonly Action initAction;
        /// <summary>
        /// Action which is executed every run
        /// </summary>
        private readonly Action infiniteAction;
        /// <summary>
        /// Time in milliseconds between each run execution
        /// </summary>
        private readonly int delayTime;
        /// <summary>
        /// Timeout in milliseconds after which the run is terminated with a TimeOutException
        /// </summary>
        private readonly int executionTimeout = -1;
        
        private long lastRunTimestamp;
        private DateTime looperLastCompleted = DateTime.UtcNow;

        public delegate void OnExceptionHandler(Exception ex);
        public delegate void OnExecutionRunCompleted(TimeSpan executionTime);

        private readonly OnExceptionHandler exceptionHandler;
        private readonly OnExecutionRunCompleted executionRunCompletedHandler;

        private bool isCompleted;
        private readonly object isCompletedSync = new object();
        
        public bool IsCompleted
        {
            get
            {
                lock (isCompletedSync)
                {
                    return isCompleted;
                }
            }
            private set
            {
                lock (isCompletedSync)
                {
                    isCompleted = value;
                }
            }
        }

        /// <summary>
        /// Gets the running state of the looper. Method is thread-safe.
        /// </summary>
        public bool IsRunning
        {
            get
            {
                lock (this.mainThreadLock)
                {
                    // We use ThreadState != Stopped, so it only counts as not running when the main thread is actually stopped
                    return (this.mainThread != null && this.mainThread.IsAlive && (this.mainThread.ThreadState != ThreadState.Stopped || this.mainThread.ThreadState != ThreadState.Aborted));
                }
            }
        }

        public bool IsStopping { get; private set; }

        public string ThreadName
        {
            get
            {
                lock (this.mainThreadLock)
                {
                    if (this.mainThread != null)
                        return this.mainThread.Name;
                }
                return null;
            }
            set
            {
                lock (this.mainThreadLock)
                {
                    if (this.mainThread != null)
                    {
                        this.mainThread.Name = value;
                    }
                }
            }
        }

        /// <summary>
        /// Create and starts a new instance of the infinite looper
        /// </summary>
        /// <param name="action">Action to run forever. It's not needed to add an infinite while or for loop.</param>
        /// <param name="delayBetweenExecution">Delay in milliseconds between each loop.</param>
        /// <param name="onExceptionHandler">Handler for when the called action throws an uncaught exception</param>
        /// <returns>Instance of the infinite looper class</returns>
        public static InfiniteLooper StartNew(Action action, int delayBetweenExecution, OnExceptionHandler onExceptionHandler)
        {
            return StartNew(action, delayBetweenExecution, -1, null, onExceptionHandler);
        }

        /// <summary>
        /// Create and starts a new instance of the infinite looper
        /// </summary>
        /// <param name="action">Action to run forever. It's not needed to add an infinite while or for loop.</param>
        /// <param name="delayBetweenExecution">Delay in milliseconds between each loop.</param>
        /// <param name="executionTimeout">Time in milliseconds after which a timeout exception is thrown</param>
        /// <param name="onExceptionHandler">Handler for when the called action throws an uncaught exception</param>
        /// <returns>Instance of the infinite looper class</returns>
        public static InfiniteLooper StartNew(Action action, int delayBetweenExecution, int executionTimeout, OnExceptionHandler onExceptionHandler)
        {
            return StartNew(action, delayBetweenExecution, executionTimeout, null, onExceptionHandler);
        }

        /// <summary>
        /// Create and starts a new instance of the infinite looper
        /// </summary>
        /// <param name="action">Action to run forever. It's not needed to add an infinite while or for loop.</param>
        /// <param name="delayBetweenExecution">Delay in milliseconds between each loop.</param>
        /// <param name="executionTimeout">Time in milliseconds after which a timeout exception is thrown</param>
        /// <param name="onExecutionRunCompleted">Handler which gets called after each run. Passes a <see cref="System.TimeSpan"/> with the total execution time for the last run.</param>
        /// <param name="onExceptionHandler">Handler for when the called action throws an uncaught exception</param>
        /// <returns>Instance of the infinite looper class</returns>
        public static InfiniteLooper StartNew(Action action, int delayBetweenExecution, int executionTimeout, OnExecutionRunCompleted onExecutionRunCompleted, OnExceptionHandler onExceptionHandler)
        {
            return StartNew(null, action, delayBetweenExecution, executionTimeout, onExecutionRunCompleted, onExceptionHandler);
        }

        /// <summary>
        /// Create and starts a new instance of the infinite looper
        /// </summary>
        /// <param name="initialization">Initialization action which is run before the main loop</param>
        /// <param name="action">Action to run forever. It's not needed to add an infinite while or for loop.</param>
        /// <param name="delayBetweenExecution">Delay in milliseconds between each loop.</param>
        /// <param name="executionTimeout">Time in milliseconds after which a timeout exception is thrown</param>
        /// <param name="onExecutionRunCompleted">Handler which gets called after each run. Passes a <see cref="System.TimeSpan"/> with the total execution time for the last run.</param>
        /// <param name="onExceptionHandler">Handler for when the called action throws an uncaught exception</param>
        /// <returns>Instance of the infinite looper class</returns>
        public static InfiniteLooper StartNew(Action initialization, Action action, int delayBetweenExecution, int executionTimeout, OnExecutionRunCompleted onExecutionRunCompleted, OnExceptionHandler onExceptionHandler)
        {
            var looper = new InfiniteLooper(initialization, action, delayBetweenExecution, executionTimeout, onExecutionRunCompleted, onExceptionHandler);
            looper.Start();

            return looper;
        }

        /// <summary>
        /// Create a new instance of the InifiteLooper.
        /// </summary>
        /// <param name="action">Action to run forever. It's not needed to add an inifite while or for loop.</param>
        /// <param name="delayBetweenExecution">Delay in milliseconds between each loop</param>
        /// <param name="executionTimeout">Time in milliseconds after which a timeout exception is thrown</param>
        /// <param name="onExceptionHandler">Handler for when the called action throws an uncaught exception</param>
        public InfiniteLooper(Action action, int delayBetweenExecution, int executionTimeout, OnExceptionHandler onExceptionHandler)
            : this(action, delayBetweenExecution, executionTimeout, null, onExceptionHandler)
        {
        }

        /// <summary>
        /// Create a new instance of the InifiteLooper.
        /// </summary>
        /// <param name="action">Action to run forever. It's not needed to add an inifite while or for loop.</param>
        /// <param name="delayBetweenExecution">Delay in milliseconds between each loop</param>
        /// <param name="executionTimeout">Time in milliseconds after which a timeout exception is thrown</param>
        /// <param name="onExecutionRunCompleted">Handler which gets called after each run. Passes a <see cref="System.TimeSpan"/> with the total execution time for the last run.</param>
        /// <param name="onExceptionHandler">Handler for when the called action throws an uncaught exception</param>
        public InfiniteLooper(Action action, int delayBetweenExecution, int executionTimeout, OnExecutionRunCompleted onExecutionRunCompleted, OnExceptionHandler onExceptionHandler)
            : this(null, action, delayBetweenExecution, executionTimeout, onExecutionRunCompleted, onExceptionHandler)
        {
        }

        /// <summary>
        /// Create a new instance of the InifiteLooper.
        /// </summary>
        /// <param name="initialization">Initialization action which is run before the main loop</param>
        /// <param name="action">Action to run forever. It's not needed to add an inifite while or for loop.</param>
        /// <param name="delayBetweenExecution">Delay in milliseconds between each loop</param>
        /// <param name="executionTimeout">Time in milliseconds after which a timeout exception is thrown</param>
        /// <param name="onExecutionRunCompleted">Handler which gets called after each run. Passes a <see cref="System.TimeSpan"/> with the total execution time for the last run.</param>
        /// <param name="onExceptionHandler">Handler for when the called action throws an uncaught exception</param>
        public InfiniteLooper(Action initialization, Action action, int delayBetweenExecution, int executionTimeout, OnExecutionRunCompleted onExecutionRunCompleted, OnExceptionHandler onExceptionHandler)
        {
            this.initAction = initialization;
            this.infiniteAction = action;
            this.delayTime = delayBetweenExecution;
            this.lastRunTimestamp = 0;

            if (onExceptionHandler == null)
                throw new Exception("InfiniteLooper OnExceptionHandler can not be null.");

            this.exceptionHandler = onExceptionHandler;
            this.executionRunCompletedHandler = onExecutionRunCompleted;
            this.executionTimeout = executionTimeout;

            InitializeThread();
        }

        /// <summary>
        /// Initialize a new thread to be run
        /// </summary>
        private void InitializeThread()
        {
            this.IsStopping = false;
            this.mainThread = new Thread(() =>
                {
                    if (initAction != null)
                    {
                        initAction();
                    }

                    var waitInterval = TimeSpan.FromMilliseconds(this.delayTime);
                    for (; !isStopping.WaitOne(waitInterval);)
                    {
                        lock (this.mainThreadLock)
                        {
                            var stopwatch = new Stopwatch();
                            stopwatch.Start();

                            try
                            {
                                if (executionTimeout <= 0)
                                {
                                    // No execution timeout, just run it untill it's finished
                                    infiniteAction();
                                }
                                else
                                {
                                    // Execute action. When running a watcher will also be started to make sure the action doesn't run
                                    // longer than the set waitInterval. If this does happen a TimeoutException will be thrown.
                                    Run(TimeSpan.FromMilliseconds(executionTimeout));
                                }
                            }
                            catch (ThreadAbortException)
                            {
                                break;
                            }
                            catch (Exception ex)
                            {
                                // Set flag so thread will stop
                                this.isStopping.Set();

                                if (!this.IsStopping)
                                {
                                    // Exception handler can never be null, exception is thrown in constructor
                                    ThreadPool.QueueUserWorkItem(state => this.exceptionHandler(ex));
                                }
                            }
                            finally
                            {
                                stopwatch.Stop();
                            }

                            this.looperLastCompleted = DateTime.UtcNow;
                            if (this.executionRunCompletedHandler != null)
                            {
                                // Send execution time to 
                                this.executionRunCompletedHandler.BeginInvoke(stopwatch.Elapsed, null, null);
                            }

                            // Update last runtimestamp
                            this.lastRunTimestamp = this.looperLastCompleted.ToUnixTime();
                        }
                    }

                    Debug.WriteLine("MainThread stopped");
                });
            this.mainThread.Name = "InfiniteLooper";
        }

        /// <summary>
        /// Starts the infinite looper and watch timer. Method is thread-safe.
        /// </summary>
        public void Start()
        {
            lock (this.mainThreadLock)
            {
                if (!IsRunning)
                {
                    if (this.mainThread == null || this.mainThread.ThreadState == ThreadState.Stopped || this.mainThread.ThreadState == ThreadState.Aborted)
                    {
                        InitializeThread();
                    }

                    if (this.mainThread != null)
                    {
                        // Update last runtimestamp                        
                        this.lastRunTimestamp = DateTime.UtcNow.ToUnixTime();

                        // Start main thread
                        this.mainThread.Start();

                        // Start thread watcher
                        if (this.mainThreadWatcher != null)
                        {
                            this.mainThreadWatcher.Dispose();
                            this.mainThreadWatcher = null;
                        }

                        // Start main thread watcher (with delay)
                        this.mainThreadWatcher = new Timer(TimeCallBack, null, this.delayTime, this.delayTime);
                    }
                }
            }
        }

        /// <summary>
        /// Stops the main looping thread. Blocks the calling thread for max delayBetweenExecution milliseconds until the thread terminates. Method is thread-safe.
        /// </summary>
        /// <returns>True if the thread was running and is successfully terminated. Otherwise false</returns>
        public bool Stop()
        {
            lock (this.mainThreadLock)
            {
                if (this.mainThreadWatcher != null)
                {
                    // Dipose thread watcher
                    this.mainThreadWatcher.Dispose();
                    this.mainThreadWatcher = null;
                }

                if (IsRunning)
                {
                    // Mark loop at stopped
                    isStopping.Set();
                    this.IsStopping = true;

                    var result = true;

                    // Wait (delay + 1) seconds before throwing timeout
                    if (this.mainThread != null)
                    {
                        result = this.mainThread.Join(TimeSpan.FromMilliseconds(this.delayTime + 1000));

                        if (!result)
                            this.mainThread.Abort();
                    }

                    this.mainThread = null;

                    return result;
                }

            }

            return false;
        }

        /// <summary>
        /// Executes the spcified function within the current thread, aborting it
        /// if it does not complete within the specified timeout interval. 
        /// </summary>
        /// <returns>result of the function</returns>
        /// <remarks>
        /// The performance trick is that we do not interrupt the current
        /// running thread. Instead, we just create a watcher that will sleep
        /// until the originating thread terminates or until the timeout is
        /// elapsed.
        /// </remarks>
        /// <exception cref="ArgumentNullException">if function is null</exception>
        /// <exception cref="TimeoutException">if the function does not finish in time </exception>
        private void Run(TimeSpan timeout)
        {
            IsCompleted = false;

            WaitCallback watcher = obj =>
            {
                var watchedThread = obj as Thread;

                lock (isCompletedSync)
                {
                    if (!IsCompleted)
                    {
                        Monitor.Wait(isCompletedSync, timeout);
                    }
                }

                // CAUTION: the call to Abort() can be blocking in rare situations
                // http://msdn.microsoft.com/en-us/library/ty8d3wta.aspx
                // Hence, it should not be called with the 'lock' as it could deadlock
                // with the 'finally' block below.
                if (!IsCompleted)
                {
                    if (watchedThread != null)
                        watchedThread.Abort();
                }
            };

            try
            {
                ThreadPool.QueueUserWorkItem(watcher, Thread.CurrentThread);

                // Execute real work here
                infiniteAction();
            }
            catch (ThreadAbortException)
            {
                // This is our own exception.
                Thread.ResetAbort();

                throw new TimeoutException(string.Format("The operation has timed out after {0}.", timeout));
            }
            finally
            {
                lock (isCompletedSync)
                {
                    IsCompleted = true;
                    Monitor.Pulse(isCompletedSync);
                }
            }
        }

        /// <summary>
        /// Callback method used by the mainThreadWatcher Timer. If the main thread has missed two runs it will be restarted.
        /// </summary>
        /// <param name="o"></param>
        private void TimeCallBack(object o)
        {
            lock (this.mainThreadLock)
            {
                if (!IsRunning)
                {
                    Start();
                }
                else
                {
                    // Calculate last time run
                    var timeDiff = DateTime.UtcNow.ToUnixTime() - this.lastRunTimestamp;

                    // Missed 2 runs, restart the thread
                    if (timeDiff >= (this.delayTime * 2))
                    {
                        Stop();

                        Start();
                    }
                }
            }
        }

        public void Dispose()
        {
            isStopping.Set();
            isStopping.Close();
        }
    }
}
