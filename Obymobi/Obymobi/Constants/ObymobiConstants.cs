using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Constants
{
    public static class ObymobiConstants
    {
        public const string CloudManualContainerSettingName = "CloudManualContainer";
        public const string AmazonS3AwsAccessKeySettingName = "AWSAccessKey";
        public const string AmazonS3AwsSecretAccessKeySettingName = "AWSSecretKey";

        public const string ProductionAmazonS3AwsAdminAccessKeyEncrypted = "0+BEOVs9rVAlZeXJDcluEF8WB6HXJ8R/FjvqGHAsm9A=";
        public const string ProductionAmazonS3AwsAdminSecretAccessKeyEncrypted = "wz/wSVKwb/gHN0xZ5VveGbzk8bzlUPAONlHKZh8JsEue4J/IjOHSaFB/0cawc8Xa";

        public const string DevelopmentAmazonS3AwsAdminAccessKeyEncrypted = "A7aYaDHn5RNRxUSsT5QEZgiPv7spaC/oTHCJpff54Ms=";
        public const string DevelopmentAmazonS3AwsAdminSecretAccessKeyEncrypted = "gta/7/bDrRTDOKUQb2fsXPY1fSUn1GBA/dcxwBa3t5rkTMgrr6n475d+AXwHplay";

        public const string GeneralAuthorizerGodModeOverrideThreadVariable = "GeneralAuthorizer.GodModeOverride";
        public const string GeneralValidatorDisableUpdateVersions = "GeneralValidat.DisableUpdateVersions";

        public const int MobileCheckinDeliverypointNumber = 99999;
        public const int MinimumPasswordLength = 8; // BB 2015.10.03 Added MinimumPasswordLength constant

        public const string RestApiBaseUrl = "RestApiBaseUrl";

        // Salts are a bit 'hidden' against decompilation
        private static readonly string saltA = "tItNQP0Mlb1Cr3jZCjrtVetQYdbd9gQTBau3kpsF9tL5KOT0Wikgz";
        private static readonly string saltB = "YkuDEEy7oYAczNaY+w49SzjqGrRuJ5eYMlw";
        private static readonly string saltC = "uu6jN1T6oJiTBylxt93E6SlWU2h0iOJk7ki2a57HVeDhDqxtDUxxg0=";
        private static readonly string saltD = "a+9sl92jP6M6NUr5GElqU4Yex/qFZ";
        private static readonly string saltE = "IU8qvm0J7RMQupnm986TVi4AZOGab";
        private static readonly string saltF = "80jfmZUg4hglR8D63RHwe+KNjw4y3t9ac0f";
        private static readonly string saltG = "PtxiNdZJ8MIX199qH2sHSCaAaYyu96WdCnXljYaJELptVlJLWauuaFR/uTm2rr1";
        private static readonly string saltH = "Y5S3MS2W84AZji0IAjcbsVsHyiRIXEPvkc4A5JY0u3g/3yKAv5Imb8/ZgtJpSabVJ";

        public static readonly string PrimaryWebserviceUrl = "https://obymobi-api.prod.trueguest.tech";
        public static readonly string SecondaryWebserviceUrl = "https://obymobi-api.prod.trueguest.tech";
        public static readonly string DevelopmentWebserviceUrl = "https://obymobi-api.dev.trueguest.tech";
        public static readonly string TestWebserviceUrl = "https://obymobi-api.test.trueguest.tech";
        public static readonly string DemoWebserviceUrl = "https://obymobi-api.demo.trueguest.tech";
        public static readonly string StagingWebserviceUrl = "https://obymobi-api.stag.trueguest.tech";

        #region Amazon CDN

        /// <summary>
        /// {0} = container
        /// {1} = filename
        /// </summary>
        public const string AmazonBlobStorageUrlFormat = "https://s3-ap-southeast-1.amazonaws.com/{0}/{1}";
        public const string AmazonStorageUrlFormat = "https://s3-ap-southeast-1.amazonaws.com/{0}/";

        public const string AmazonS3AwsCompanyUsername = "{0}-{1}"; // {0} = Environment prefix, {1} = CompanyId        
        public const string AmazonS3AwsCompanyContainer = "companies/" + ObymobiConstants.AmazonS3AwsCompanyUsername;
        public const string AmazonS3AwsGroupname = "companies";
        public const string AmazonS3AwsPolicyName = "companies-policy";

        // Production CDN
        public static readonly string PrimaryAmazonRootContainer = "cravecloud-tdg-production";
        public static readonly string PrimaryAmazonCdnBaseUrl = AmazonStorageUrlFormat.FormatSafe(PrimaryAmazonRootContainer);
        public static readonly string PrimaryAmazonCdnBaseUrlFiles = Dionysos.Web.WebPathCombiner.CombinePaths(ObymobiConstants.PrimaryAmazonCdnBaseUrl, ObymobiConstants.CloudContainerMediaFiles);
        public static readonly string PrimaryAmazonCdnBaseUrlPublished = Dionysos.Web.WebPathCombiner.CombinePaths(ObymobiConstants.PrimaryAmazonCdnBaseUrl, ObymobiConstants.CloudContainerPublished);

        // Development CDN
        public static readonly string DevelopmentAmazonRootContainer = "cravecloud-tdg-development";
        public static readonly string DevelopmentAmazonCdnBaseUrl = AmazonStorageUrlFormat.FormatSafe(DevelopmentAmazonRootContainer);
        public static readonly string DevelopmentAmazonCdnBaseUrlFiles = Dionysos.Web.WebPathCombiner.CombinePaths(ObymobiConstants.DevelopmentAmazonCdnBaseUrl, ObymobiConstants.CloudContainerMediaFiles);
        public static readonly string DevelopmentAmazonCdnBaseUrlPublished = Dionysos.Web.WebPathCombiner.CombinePaths(ObymobiConstants.DevelopmentAmazonCdnBaseUrl, ObymobiConstants.CloudContainerPublished);

        // Test CDN
        public static readonly string TestAmazonRootContainer = "cravecloud-tdg-test";
        public static readonly string TestAmazonCdnBaseUrl = AmazonStorageUrlFormat.FormatSafe(TestAmazonRootContainer);
        public static readonly string TestAmazonCdnBaseUrlFiles = Dionysos.Web.WebPathCombiner.CombinePaths(ObymobiConstants.TestAmazonCdnBaseUrl, ObymobiConstants.CloudContainerMediaFiles);
        public static readonly string TestAmazonCdnBaseUrlPublished = Dionysos.Web.WebPathCombiner.CombinePaths(ObymobiConstants.TestAmazonCdnBaseUrl, ObymobiConstants.CloudContainerPublished);

        // Staging CDN
        public static readonly string StagingAmazonRootContainer = "cravecloud-tdg-staging";
        public static readonly string StagingAmazonCdnBaseUrl = AmazonStorageUrlFormat.FormatSafe(StagingAmazonRootContainer);
        public static readonly string StagingAmazonCdnBaseUrlFiles = Dionysos.Web.WebPathCombiner.CombinePaths(StagingAmazonCdnBaseUrl, CloudContainerMediaFiles);
        public static readonly string StagingAmazonCdnBaseUrlPublished = Dionysos.Web.WebPathCombiner.CombinePaths(StagingAmazonCdnBaseUrl, CloudContainerPublished);

        // GK I'm not most proud of this... Maybe come back later with new ideas.
        // It's being set by the AmazonCloudStorageVerifier
        public static string ManualAmazonCdnBaseUrl = string.Empty;
        public static string ManualAmazonCdnBaseUrlFiles = string.Empty;
        public static string ManualAmazonCdnBaseUrlPublished = string.Empty;

        public static string DatabaseTimeZone = "W. Europe Standard Time";

        #endregion

        #region Google Analytics

        public static string GetGoogleAnalyticsTrackingId(CloudEnvironment cloudEnvironment)
        {
            switch (cloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    return "UA-241858455-1";
                case CloudEnvironment.Test:
                case CloudEnvironment.Demo:
                case CloudEnvironment.Development:
                case CloudEnvironment.Manual:
                case CloudEnvironment.Staging:
                    return "UA-241858455-2";
                default:
                    throw new NotImplementedException("CloudEnvironment: {0}".FormatSafe(cloudEnvironment.ToString()));
            }
        }


        #endregion

        #region OpenWeatherMap (openweathermap.org)

        public static string GetOpenWeatherMapAppId(CloudEnvironment cloudEnvironment)
        {
            switch (cloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    return "eb4117cca3f9859d069e68374dacc015";
                case CloudEnvironment.Demo:
                case CloudEnvironment.Test:
                case CloudEnvironment.Development:
                case CloudEnvironment.Manual:
                case CloudEnvironment.Staging:
                    return "242049df4c614371fd4c3d70aa787fd9";
                default:
                    throw new NotImplementedException("CloudEnvironment: {0}".FormatSafe(cloudEnvironment.ToString()));
            }
        }

        #endregion

        #region AccuWeather (developer.accuweather.com)

        public static string GetAccuWeatherAppId(CloudEnvironment cloudEnvironment)
        {
            switch (cloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    return "xZAKv5GVgVhRtHDm86t3nAT6kMzpIi1I";
                case CloudEnvironment.Demo:
                case CloudEnvironment.Test:
                case CloudEnvironment.Development:
                case CloudEnvironment.Manual:
                case CloudEnvironment.Staging:
                    return "xZAKv5GVgVhRtHDm86t3nAT6kMzpIi1I";
                default:
                    throw new NotImplementedException("CloudEnvironment: {0}".FormatSafe(cloudEnvironment.ToString()));
            }
        }

        #endregion

        #region DarkSky Weather API (darksky.net)

        public static readonly string DarkSkySecretKey = "4493451ecc39a539b8ff5331e9418775";

        #endregion

        #region Email (sendgrid.com)

        public const string SENDGRID_BOUNCED_EMAILS_URL = "https://api.sendgrid.com/v3/suppression/bounces?start_time={0}";

        #endregion

        public static readonly string NocStatusTextFile = "status/last-state.txt";
        public static readonly string NocStatusTextFile_Company = "status/last-state-{0}.txt";
        public static readonly string NocStatusConnectionInformationFile = "status/connection-string.txt";

        public static readonly string NocStatusServiceUrl1 = GetNocStatusServiceUrl1();
        public static readonly string NocStatusStorageUrl1 = GetNocStatusStorageUrl1();
        public static readonly string NocStatusTextFileUrl1 = GetNocStatusTextFileUrl1();
        public static readonly string NocStatusConnectionInformationFileUrl1 = GetNocStatusConnectionInformationFileUrl1();

        public static readonly string NocStatusServiceUrl2 = GetNocStatusServiceUrl2();
        public static readonly string NocStatusStorageUrl2 = GetNocStatusStorageUrl2();
        public static readonly string NocStatusTextFileUrl2 = GetNocStatusTextFileUrl2();
        public static readonly string NocStatusConnectionInformationFileUrl2 = GetNocStatusConnectionInformationFileUrl2();

        public const string API_VERSION = "32.0";

        public static readonly string GenericSalt = GetGenericSalt();
        public static readonly string MobileSalt = GetMobileSalt();
        public static readonly string NocSalt = GetNocSalt();
        public static readonly string MediaGatewaySalt = GetMediaGatewaySalt();

        public static readonly List<string> SupportpoolFallbackPhonenumbers = new List<string> { };
        public static readonly List<string> SupportpoolFallbackEmailaddresses = new List<string> { "crave.tdg@gmail.com" };

        public static string SupportpoolFallbackPhonenumbersCsv()
        {
            return String.Join(",", ObymobiConstants.SupportpoolFallbackPhonenumbers.ToArray());
        }

        public static string SupportpoolFallbackEmailaddressesCsv()
        {
            return String.Join(",", ObymobiConstants.SupportpoolFallbackEmailaddresses.ToArray());
        }

        // WebApp Directories
        /// <summary>
        /// Returns the path for the Console Web Application: 'console'
        /// </summary>
        public const string WebAppPathConsole = "console";

        /// <summary>
        /// Returns the path for the Mobile Web Application: 'mobile'
        /// </summary>
        public const string WebAppPathMobile = "mobile";

        /// <summary>
        /// Returns the path for the Management Web Application: 'management'
        /// </summary>
        public const string WebAppPathManagement = "management";

        /// <summary>
        /// Returns the path for the Noc AWeb pplication: 'noc'
        /// </summary>
        public const string WebAppPathNoc = "noc";

        /// <summary>
        /// Returns the path for the Messaging Web Application: 'messaging'
        /// </summary>
        public const string WebAppPathMessaging = "messaging";

        /// <summary>
        /// Returns the path for the Api Web Application: 'api'
        /// </summary>
        public const string WebAppPathApi = "api";

        /// <summary>
        /// Returns the subpath used in api for the storage of Media Files: 'files'
        /// </summary>
        public const string CloudContainerMediaFiles = "files";

        /// <summary>
        /// Returns the subpath used in api for the storage of Media Files: 'published'
        /// </summary>
        public const string CloudContainerPublished = "published";

        /// <summary>
        /// Returns the path for the NocService Web Application: 'nocservice'
        /// </summary>
        public const string WebAppPathNocService = "nocservice";

        /// <summary>
        /// Returns the path for the Backend Services Web Applicaiton: 'services'
        /// </summary>
        public const string WebAppPathServices = "services";

        /// <summary>
        /// JPG quality in .NET
        /// </summary>
        public const int JpgQualityMedia = 50;

        #region Development Overwrites

        private static string GetGenericSalt() => Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(saltA + saltC);

        private static string GetMobileSalt() => Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(saltB + saltD);

        private static string GetNocSalt() => Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(saltE + saltF);

        private static string GetMediaGatewaySalt() => Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(saltG + saltH);

        private static string GetNocStatusServiceUrl1() => "https://nocservice.crave-emenu.com/";

        private static string GetNocStatusStorageUrl1() => ObymobiConstants.PrimaryAmazonCdnBaseUrl;

        private static string GetNocStatusTextFileUrl1() => Dionysos.Web.WebPathCombiner.CombinePaths(GetNocStatusStorageUrl1(), ObymobiConstants.NocStatusTextFile);

        private static string GetNocStatusConnectionInformationFileUrl1() => Dionysos.Web.WebPathCombiner.CombinePaths(GetNocStatusStorageUrl1(), ObymobiConstants.NocStatusConnectionInformationFile);

        private static string GetNocStatusServiceUrl2() => "https://nocservice.craveinteractive.net/";

        private static string GetNocStatusStorageUrl2() => ObymobiConstants.PrimaryAmazonCdnBaseUrl;

        private static string GetNocStatusTextFileUrl2() => Dionysos.Web.WebPathCombiner.CombinePaths(GetNocStatusStorageUrl2(), ObymobiConstants.NocStatusTextFile);

        private static string GetNocStatusConnectionInformationFileUrl2() => Dionysos.Web.WebPathCombiner.CombinePaths(GetNocStatusStorageUrl2(), ObymobiConstants.NocStatusConnectionInformationFile);

        #endregion
    }
}
