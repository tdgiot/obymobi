﻿namespace Obymobi.Constants
{
    public class CraveConstants
    {
        public const int ApiVersion = 24;

        public const string CraveObymobiCmsUrl = "Crave.Obymobi.Cms.Url";
        public const string CraveObymobiNocUrl = "Crave.Obymobi.Noc.Url";
        public const string CraveObymobiMessagingUrl = "Crave.Obymobi.Messaging.Url";
        public const string CraveObymobiApiUrl = "Crave.Obymobi.Api.Url";
        public const string CraveObymobiServicesUrl = "Crave.Obymobi.Services.Url";
    }
}
