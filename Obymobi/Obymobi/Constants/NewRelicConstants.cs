﻿namespace Obymobi.Constants
{
    public class NewRelicConstants
    {
        public const string NEWRELIC_MESSAGING_NETMESSAGE_TXCOUNTER = "Custom/Messaging/Netmessage/Count_Transmitted";
        public const string NEWRELIC_MESSAGING_NETMESSAGE_RXCOUNTER = "Custom/Messaging/Netmessage/Count_Received";
        public const string NEWRELIC_MESSAGING_NETMESSAGE_PINGCOUNTER = "Custom/Messaging/Netmessage/Count_Pings";
        public const string NEWRELIC_MESSAGING_NETMESSAGE_PONGCOUNTER = "Custom/Messaging/Netmessage/Count_Pongs";
        public const string NEWRELIC_MESSAGING_NETMESSAGE_PONGQUEUE = "Custom/Messaging/Netmessage/Count_Pong_Queue";
        public const string NEWRELIC_MESSAGING_NETMESSAGE_QUEUED = "Custom/Messaging/Netmessage/State_Queued";
        public const string NEWRELIC_MESSAGING_NETMESSAGE_TIMEOUT = "Custom/Messaging/Netmessage/State_Timeout";
        public const string NEWRELIC_MESSAGING_NETMESSAGE_VERIFY_TIME = "Custom/Messaging/Netmessage/Avg_Response_Verify";
        public const string NEWRELIC_MESSAGING_NETMESSAGE_COMPLETE_TIME = "Custom/Messaging/Netmessage/Avg_Response_Complete";

        public const string NEWRELIC_MESSAGING_PING_METRIC = "Custom/Messaging/Looper/Ping_Execution_Time";
        public const string NEWRELIC_MESSAGING_PONG_METRIC = "Custom/Messaging/Looper/Pong_Execution_Time";
        public const string NEWRELIC_MESSAGING_LOGCLEANUP_METRIC = "Custom/Messaging/Looper/LogCleanup_Execution_Time";
        public const string NEWRELIC_MESSAGING_NETMESSAGE_METRIC = "Custom/Messaging/Looper/Netmessage_Execution_Time";
        public const string NEWRELIC_MESSAGING_DATA_GATHERER_METRIC = "Custom/Messaging/Looper/Data_Gatherer_Execution_Time";
        
        public const string NEWRELIC_MESSAGING_TOTAL_REQUESTS = "Custom/Messaging/Connections/Total_Requests";
        public const string NEWRELIC_MESSAGING_TOTAL_REQUESTS_POKEIN = "Custom/Messaging/Connections/Total_Requests/PokeIn";
        public const string NEWRELIC_MESSAGING_TOTAL_REQUESTS_SIGNALR = "Custom/Messaging/Connections/Total_Requests/SignalR";

        public const string NEWRELIC_MESSAGING_NEWCONNECTIONS_POKEIN = "Custom/Messaging/Connections/New_Connections_PokeIn";
        public const string NEWRELIC_MESSAGING_NEWCONNECTIONS_SIGNALR = "Custom/Messaging/Connections/New_Connections_SignalR";
        public const string NEWRELIC_MESSAGING_NEWCONNECTIONS_HITRUN = "Custom/Messaging/Connections/New_Connections_HitRun";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_TOTAL_ACTIVE = "Custom/Messaging/Connections/Total_Active_Connections";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_TOTAL_DEVICES = "Custom/Messaging/Connections/Total_Active_Devices";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_TOTAL_TERMINALS = "Custom/Messaging/Connections/Total_Device_Terminals";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_TOTAL_CLIENTS = "Custom/Messaging/Connections/Total_Device_Clients";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_TOTAL_SUPPORTTOOLS = "Custom/Messaging/Connections/Total_Device_SupportTools";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_TOTAL_SANDBOX = "Custom/Messaging/Connections/Total_Device_Sandbox";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_UNIQUE_CLIENTS = "Custom/Messaging/Connections/Unique_Device_Clients";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_UNIQUE_SUPPORTTOOLS = "Custom/Messaging/Connections/Unique_Device_SupportTools";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_UNIQUE_TERMINALS = "Custom/Messaging/Connections/Unique_Device_Terminals";

        public const string NEWRELIC_MESSAGING_CONNECTIONS_COMET_TOTAL_POKEIN = "Custom/Messaging/Connections/Comet_Total_PokeIn";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_COMET_TOTAL_SIGNALR = "Custom/Messaging/Connections/Comet_Total_SignalR";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_COMET_APP_POKEIN = "Custom/Messaging/Connections/Comet_{0}_PokeIn";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_COMET_APP_SIGNALR = "Custom/Messaging/Connections/Comet_{0}_SignalR";

        public const string NEWRELIC_MESSAGING_CONNECTIONS_METHOD_WEBSERVICE = "Custom/Messaging/Connections/Method_Webservice";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_METHOD_COMET = "Custom/Messaging/Connections/Method_Comet";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_COMPANY_WEBSERVICE = "Custom/Messaging/Connections/Webservice/{0}";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_COMPANY_COMET = "Custom/Messaging/Connections/Comet/{0}";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_COMPANY_TOTAL_TERMINALS = "Custom/Messaging/Connections/Total/Terminal/{0}";
        public const string NEWRELIC_MESSAGING_CONNECTIONS_COMPANY_TOTAL_CLIENTS = "Custom/Messaging/Connections/Total/Client/{0}";
        
        public const string NEWRELIC_MESSAGING_CONNECTIONS_DISCONNECTS = "Custom/Messaging/Connections/Disconnects/{0}";

        public const string NEWRELIC_API_LOOPER_DATABASE_METRIC = "Custom/Api/Looper/DatabasePoller_Execution_Time";
        public const string NEWRELIC_API_LOOPER_SUPPORTNOTIFICATION_METRIC = "Custom/Api/Looper/SupportNotificationPoller_Execution_Time";
        public const string NEWRELIC_API_LOOPER_MEDIATASK_METRIC = "Custom/Api/Looper/MediaTaskPoller_Execution_Time";
        public const string NEWRELIC_API_LOOPER_CLOUDTASK_METRIC = "Custom/Api/Looper/CloudTaskPoller_Execution_Time";
        public const string NEWRELIC_API_LOOPER_METRIC_FORMAT = "Custom/Api/Looper/{0}_Execution_Time";

        public const string NEWRELIC_API_ORDERS_PLACED = "Custom/Api/Orders/Count_Placed_Orders";
        public const string NEWRELIC_API_REQUESTS_PLACED = "Custom/Api/Orders/Count_Placed_Requests";

        public const string NEWRELIC_API_SEND_ANNOUNCEMENTS = "Custom/Api/Announcements/Count_Send";

        public const string NEWRELIC_API_ORDERS_STATUS_NOTROUTED = "Custom/Api/Orders/Status_NotRouted";
        public const string NEWRELIC_API_ORDERS_STATUS_ROUTED = "Custom/Api/Orders/Status_Routed";
        public const string NEWRELIC_API_ORDERS_STATUS_INROUTE = "Custom/Api/Orders/Status_InRoute";
        public const string NEWRELIC_API_ORDERS_STATUS_WAITINGFORPAYMENT = "Custom/Api/Orders/Status_WaitingForPayment";
        public const string NEWRELIC_API_ORDERS_STATUS_UNPROCESSABLE = "Custom/Api/Orders/Status_Unprocessable";

        public const string NEWRELIC_API_GET_NETMESSAGES = "Custom/Api/GetNetmessages/{0}";

        // Netstat Data
        public const string NEWRELIC_NETSTAT_ACTIVE_REQUESTS_APPPOOL = "Custom/Netstat/ActiveRequests/AppPool_{0}";
        public const string NEWRELIC_NETSTAT_CLIENTCONNECTIONS = "Custom/Netstat/ClientConnections/{0}";
        public const string NEWRELIC_NETSTAT_USED_LOCAL_PORTS = "Custom/Netstat/Used_Local_Ports";
        public const string NEWRELIC_NETSTAT_USED_REMOTE_PORTS = "Custom/Netstat/Used_Remote_Ports";

        // SqlServer Performance Counters
        public const string NEWRELIC_SQLSERVER_LOCK_ESCALATION_SEC = "Custom/SqlServer/Locks/TableLockEscalation";
        public const string NEWRELIC_SQLSERVER_LOCK_REQUESTS_SEC = "Custom/SqlServer/Locks/Requests";
        public const string NEWRELIC_SQLSERVER_LOCK_TIMEOUTS_SEC = "Custom/SqlServer/Locks/Timeouts";
        public const string NEWRELIC_SQLSERVER_BATCH_REQUESTS_SEC = "Custom/SqlServer/Statistics/BatchRequests";
        public const string NEWRELIC_SQLSERVER_DATABASES_PERCENTLOGUSED = "Custom/SqlServer/Databases/PercentLogUsed";
    }
}
