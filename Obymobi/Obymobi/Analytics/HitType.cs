﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics
{
    public enum HitType
    {
        [AnalyticsEnumStringValue("pageview")]
        PageView,
        [AnalyticsEnumStringValue("screenview")]
        ScreenView,
        [AnalyticsEnumStringValue("event")]
        Event,
        [AnalyticsEnumStringValue("transaction")]
        Transaction,
        [AnalyticsEnumStringValue("item")]
        Item,
        [AnalyticsEnumStringValue("social")]
        Social,
        [AnalyticsEnumStringValue("timing")]
        Timing,
        [AnalyticsEnumStringValue("exception")]
        Exception
    }
}
