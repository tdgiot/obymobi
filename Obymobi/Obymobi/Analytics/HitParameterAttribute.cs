﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics
{
    [AttributeUsage(AttributeTargets.All)]
    public class HitParameterAttribute : System.Attribute
    {
        public readonly string GoogleParameterName;
        public readonly int Length;

        public HitParameterAttribute(string googleParameterName)
        {
            this.GoogleParameterName = googleParameterName;
        }

        public HitParameterAttribute(string googleParameterName, int length)
        {
            this.GoogleParameterName = googleParameterName;
            this.Length = length;
        }
    }
}
