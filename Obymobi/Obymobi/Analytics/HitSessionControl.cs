﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics
{
    public enum HitSessionControl
    {
        [AnalyticsEnumStringValue("")]
        NoAction,
        [AnalyticsEnumStringValue("start")]
        Start,
        [AnalyticsEnumStringValue("end")]
        End
    }
}
