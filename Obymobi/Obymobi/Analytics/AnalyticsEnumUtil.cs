﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics
{
    public static class AnalyticsEnumUtil
    {
        public static string GetAnalyticsEnumStringValue(this Enum value)
        {
            string toReturn = value.ToString();

            try
            {
                string stringValue = String.Empty;

                // Get the Type of the Enum
                Type enumType = value.GetType();

                // Get FieldInfo for the Type
                FieldInfo fieldInfo = enumType.GetField(value.ToString());

                if (fieldInfo != null)
                {
                    // Get the StringValueAttributes
                    AnalyticsEnumStringValueAttribute[] stringValueAttributes = fieldInfo.GetCustomAttributes(typeof(AnalyticsEnumStringValueAttribute), false) as AnalyticsEnumStringValueAttribute[];


                    if (stringValueAttributes.Length == 1)
                        toReturn = stringValueAttributes[0].StringValue;
                    // GAKR Better something than nothing           
                    //else if (stringValueAttributes.Length > 1)            
                    //    throw new InvalidOperationException(string.Format("Field '{0}' of Enum '{1}' has multiple AnalyticsEnumStringValueAttribute's", fieldInfo.Name, enumType.FullName));
                }
            }
            catch (Exception)
            {
                if (TestUtil.IsPcDeveloper)
                    throw;
            }

            return toReturn;
        }
    }
}
