﻿using Newtonsoft.Json;
using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics
{
    public class HitBase
    {        
        // GK Maybe it's possible to have HitParamter inherit from JsonProperty... 
        [HitParameter("v")]
        [JsonProperty(PropertyName = "v")]
        public string ProtocolVersion = "1";

        [HitParameter("cid")]
        [JsonProperty(PropertyName = "cid")]
        public string AnalyticsClientId;

        [HitParameter("uid", 128)]
        [JsonProperty(PropertyName = "uid")]
        public string UserId;

        [HitParameter("dr")]
        [JsonProperty(PropertyName = "dr")]
        public string DocumentReferrer;

        [HitParameter("sr", 16)]
        [JsonProperty(PropertyName = "sr")]
        public string ScreenResolution;

        [HitParameter("ul", 16)]
        [JsonProperty(PropertyName = "ul")]
        public string UserLanguage;

        [HitParameter("t", 32)]
        [JsonProperty(PropertyName = "t")]
        public HitType Type;

        [HitParameter("ni")]
        [JsonProperty(PropertyName = "ni")]
        public bool NonInteractive = false;

        // Application
        [HitParameter("cd", 2048)]
        [JsonProperty(PropertyName = "cd")]
        public string ScreenName;

        [HitParameter("an", 128)]
        [JsonProperty(PropertyName = "an")]
        public string ApplicationName;

        [HitParameter("aid", 128)]
        [JsonProperty(PropertyName = "aid")]
        public string ApplicationId;

        [HitParameter("av", 32)]
        [JsonProperty(PropertyName = "av")]
        public string ApplicationVersion;

        [HitParameter("aiid", 128)]
        [JsonProperty(PropertyName = "aiid")]
        public string ApplicationInstallerId;

        // Events
        [HitParameter("ec", 32)]
        [JsonProperty(PropertyName = "ec")]
        public EventCategory EventCategory;

        [HitParameter("ea", 64)]
        [JsonProperty(PropertyName = "ea")]
        public string EventAction;

        [HitParameter("el")]
        [JsonProperty(PropertyName = "el")]
        public string EventLabel;

        [HitParameter("ev")]
        [JsonProperty(PropertyName = "ev")]
        public int EventValue = 0;

        [HitParameter("exd")]
        [JsonProperty(PropertyName = "exd")]
        public string ExceptionDescription;

        [HitParameter("exf")]
        [JsonProperty(PropertyName = "exf")]
        public bool ExceptionFatal;

        [HitParameter("utc")]
        [JsonProperty(PropertyName = "utc")]
        public TimingCategory TimingCategory;

        [HitParameter("utv")]
        [JsonProperty(PropertyName = "utv")]
        public string TimingVariable;

        [HitParameter("utl")]
        [JsonProperty(PropertyName = "utl")]
        public string TimingLabel;

        [HitParameter("utt")]
        [JsonProperty(PropertyName = "utt")]
        public int TimingMilliseconds;

        [HitParameter("sc")]
        [JsonProperty(PropertyName = "sc")]
        public HitSessionControl SessionControl;

        // Custom Dimensions (first 20 are )
        // Include to be able to record a value to each custom dimenions which is required for testing purposes.
        [HitParameter("cd1")]
        [JsonProperty(PropertyName = "cd1")]
        public string LegacyCustomDimension1;

        [HitParameter("cd2")]
        [JsonProperty(PropertyName = "cd2")]
        public string LegacyCustomDimension2;

        //  public string Session; // 1 Obsolete, not used in previous version.
        //  public string DeliverypointNumber; // 2 Obsolete, not used in previous version.
        [HitParameter("cd3", 20)]
        [JsonProperty(PropertyName = "cd3")]
        public string MacAddress;

        [HitParameter("cd4", 256)]
        [JsonProperty(PropertyName = "cd4")]
        public string DeviceType;

        [HitParameter("cd5", 20)]
        [JsonProperty(PropertyName = "cd5")]
        public string TimeStamp;

        [HitParameter("cd6")]
        [JsonProperty(PropertyName = "cd6")]
        public string ProductName;

        [HitParameter("cd7")]
        [JsonProperty(PropertyName = "cd7")]
        public string AlterationName;

        [HitParameter("cd8")]
        [JsonProperty(PropertyName = "cd8")]
        public string ServiceRequestName;

        [HitParameter("cd9")]
        [JsonProperty(PropertyName = "cd9")]
        public string CategoryName;

        [HitParameter("cd10")]
        [JsonProperty(PropertyName = "cd10")]
        public string EntertainmentName;

        [HitParameter("cd11")]
        [JsonProperty(PropertyName = "cd11")]
        public string ApplicationType;

        [HitParameter("cd12")]
        [JsonProperty(PropertyName = "cd12")]
        public int DeliverypointNumber;

        [HitParameter("cd13")]
        [JsonProperty(PropertyName = "cd13")]
        public string DeliverypointGroupName;

        [HitParameter("cd14")]
        [JsonProperty(PropertyName = "cd14")]
        public string PrimaryKeys;

        [HitParameter("cd15")]
        [JsonProperty(PropertyName = "cd15")]
        public string IsTablet = "1";

        [HitParameter("cd16", 32)]
        [JsonProperty(PropertyName = "cd16")]
        public string OperatingSystem = "Android";

        [HitParameter("cd17")]
        [JsonProperty(PropertyName = "cd17")]
        public string CompanyName;

        [HitParameter("cd18", 32)]
        [JsonProperty(PropertyName = "cd18")]
        public string LegacyApplicationVersion; // 19

        [HitParameter("cd19")]
        [JsonProperty(PropertyName = "cd19")]
        public string NavigationSource; // 19
        
        [HitParameter("cd20")]
        [JsonProperty(PropertyName = "cd20")]
        public string LegacyApplicationName; // 20

        [HitParameter("cd21")]
        [JsonProperty(PropertyName = "cd21")]
        public string HitVersion = "1.0";

        [HitParameter("cd22")]
        [JsonProperty(PropertyName = "cd22")]
        public int CompanyId;

        [HitParameter("cd23")]
        [JsonProperty(PropertyName = "cd23")]
        public int ProductId;

        [HitParameter("cd24")]
        [JsonProperty(PropertyName = "cd24")]
        public int CategoryId;

        [HitParameter("cd25")]
        [JsonProperty(PropertyName = "cd25")]
        public int EntertainmentId;

        [HitParameter("cd26")]
        [JsonProperty(PropertyName = "cd26")]
        public int DeliverypointId;

        [HitParameter("cd27")]
        [JsonProperty(PropertyName = "cd27")]
        public string DeliverypointName;

        [HitParameter("cd28")]
        [JsonProperty(PropertyName = "cd28")]
        public int AdvertisementId;

        [HitParameter("cd29")]
        [JsonProperty(PropertyName = "cd29")]
        public string AdvertisementName;

        [HitParameter("cd30")]
        [JsonProperty(PropertyName = "cd30")]
        public string AdvertisementLocation;

        [HitParameter("cd31")]
        [JsonProperty(PropertyName = "cd31")]
        public int SiteId;

        [HitParameter("cd32")]
        [JsonProperty(PropertyName = "cd32")]
        public string SiteName;

        [HitParameter("cd33")]
        [JsonProperty(PropertyName = "cd33")]
        public int PageId;

        [HitParameter("cd34")]
        [JsonProperty(PropertyName = "cd34")]
        public string PageName;

        [HitParameter("cd35")]
        [JsonProperty(PropertyName = "cd35")]
        public int TabId;

        [HitParameter("cd36")]
        [JsonProperty(PropertyName = "cd36")]
        public string TabName;

        [HitParameter("cd37")]
        [JsonProperty(PropertyName = "cd37")]
        public int MessageId;

        [HitParameter("cd38")]
        [JsonProperty(PropertyName = "cd38")]
        public string MessageName;

        [HitParameter("cd39")]
        [JsonProperty(PropertyName = "cd39")]
        public int RoomControlAreaId;

        [HitParameter("cd40")]
        [JsonProperty(PropertyName = "cd40")]
        public string RoomControlAreaName;

        [HitParameter("cd41")]
        [JsonProperty(PropertyName = "cd41")]
        public int WidgetId;

        [HitParameter("cd42")]
        [JsonProperty(PropertyName = "cd42")]
        public string WidgetName;

        [HitParameter("cd43")]
        [JsonProperty(PropertyName = "cd43")]
        public string WidgetCaption;

        [HitParameter("cd44")]
        [JsonProperty(PropertyName = "cd44")]
        public int DeliverypointGroupId;

        [HitParameter("cd45")]
        [JsonProperty(PropertyName = "cd45")]
        public int ClientId;

        [HitParameter("cd46")]
        [JsonProperty(PropertyName = "cd46")]
        public string OrderSource;

        [HitParameter("cd47")]
        [JsonProperty(PropertyName = "cd47")]
        public string Action;

        [HitParameter("cd48")]
        [JsonProperty(PropertyName = "cd48")]
        public string AttachmentName;

        [HitParameter("cd49")]
        [JsonProperty(PropertyName = "cd49")]
        public int AttachmentId;

        [HitParameter("cd50")]
        [JsonProperty(PropertyName = "cd50")]
        public int RoomControlAreaSectionId;

        [HitParameter("cd51")]
        [JsonProperty(PropertyName = "cd51")]
        public string RoomControlAreaSectionName;

        [HitParameter("cd52")]
        [JsonProperty(PropertyName = "cd52")]
        public int RoomControlAreaSectionItemId;

        [HitParameter("cd53")]
        [JsonProperty(PropertyName = "cd53")]
        public string RoomControlAreaSectionItemName;

        [HitParameter("cd54")]
        [JsonProperty(PropertyName = "cd54")]
        public string RoomControlSceneName;

        [HitParameter("cd55")]
        [JsonProperty(PropertyName = "cd55")]
        public string RoomControlComponentName;

        [HitParameter("cd56")]
        [JsonProperty(PropertyName = "cd56")]
        public int RoomControlComponentId;

        [HitParameter("cd57")]
        [JsonProperty(PropertyName = "cd57")]
        public int RoomControlStationListingId;

        [HitParameter("cd58")]
        [JsonProperty(PropertyName = "cd58")]
        public string RoomControlStationListingName;

        [HitParameter("cd59")]
        [JsonProperty(PropertyName = "cd59")]
        public string RoomControlThermostatMode;

        [HitParameter("cd60")]
        [JsonProperty(PropertyName = "cd60")]
        public string RoomControlThermostatScale;

        [HitParameter("cd61")]
        [JsonProperty(PropertyName = "cd61")]
        public string RoomControlThermostatTemperature;

        [HitParameter("cd62")]
        [JsonProperty(PropertyName = "cd62")]
        public string RoomControlThermostatFanMode;

        [HitParameter("cd63")]
        [JsonProperty(PropertyName = "cd63")]
        public int RoomControlStationId;

        [HitParameter("cd64")]
        [JsonProperty(PropertyName = "cd64")]
        public string RoomControlStationName;

        [HitParameter("cd65")]
        [JsonProperty(PropertyName = "cd65")]
        public string RoomControlTvRemoteAction;

        [HitParameter("cd66")]
        [JsonProperty(PropertyName = "cd66")]
        public int MediaId;

        [HitParameter("cd67")]
        [JsonProperty(PropertyName = "cd67")]
        public int OrderId;

        [HitParameter("cd68")]
        [JsonProperty(PropertyName = "cd68")]
        public int OrderitemId;

        [HitParameter("cd69")]
        [JsonProperty(PropertyName = "cd69")]
        public string TestPrefix;

        [HitParameter("cd70")]
        [JsonProperty(PropertyName = "cd70")]
        public string MessageResponse;

        [HitParameter("cd71")]
        [JsonProperty(PropertyName = "cd71")]
        public string ClientOperationMode;

        [HitParameter("cd72")]
        [JsonProperty(PropertyName = "cd72")]
        public string OrderType; // Unfortunately it's hard to use an Enum that's not analytics specific as we can't generate those (for now), therefore it's Order Type as String for now

        [HitParameter("cd73", 36)] // f0fa3fdd-9e2d-4d67-a709-9462dc9bc32b
        [JsonProperty(PropertyName = "cd73")]
        public string HitGuid;

        [HitParameter("cd74")]
        [JsonProperty(PropertyName = "cd74")]
        public int MapId;

        [HitParameter("cd75")]
        [JsonProperty(PropertyName = "cd75")]
        public string MapName;

        [HitParameter("cd76")]
        [JsonProperty(PropertyName = "cd76")]
        public int PointOfInterestId;

        [HitParameter("cd77")]
        [JsonProperty(PropertyName = "cd77")]
        public string PointOfInterestName;

        [HitParameter("cd78")]
        [JsonProperty(PropertyName = "cd78")]
        public int ClientConfigurationId;

        [HitParameter("cd79")]
        [JsonProperty(PropertyName = "cd79")]
        public int TerminalConfigurationId;

        [HitParameter("cd80")]
        [JsonProperty(PropertyName = "cd80")]
        public int AdvertisementConfigurationId;

        [HitParameter("cd81")]
        [JsonProperty(PropertyName = "cd81")]
        public int EntertainmentConfigurationId;

        // Metrics
        [HitParameter("cm1")]
        [JsonProperty(PropertyName = "cm1")]
        public double EventRevenue = 0;

        private static Dictionary<string, FieldInfo> fieldMappings = null;

        public static Dictionary<string, FieldInfo> GetFieldMappings()
        {
            if (fieldMappings == null)
            {
                fieldMappings = new Dictionary<string, FieldInfo>();
                var fields = typeof(HitBase).GetFields();
                foreach (var field in fields)
                {                    
                    var attributes = field.GetCustomAttributes(typeof(HitParameterAttribute), false);
                    if (attributes == null && attributes.Length != 1)
                        continue;

                    HitParameterAttribute attribute = (HitParameterAttribute)attributes[0];
                    fieldMappings.Add(field.Name, field);
                }
            }

            return fieldMappings;
        }
    }
}
