﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics
{
    public enum EventCategory
    {
        [AnalyticsEnumStringValue("Not set")]
        NotSet,
        [AnalyticsEnumStringValue("Aquisition")]
        Aquisition,
		[AnalyticsEnumStringValue("Activation")]
		Activation,
        [AnalyticsEnumStringValue("Transactions")]
        Transactions,
        [AnalyticsEnumStringValue("ScreenView")]
        ScreenView,
        [AnalyticsEnumStringValue("Advertising")]
        Advertising,
        [AnalyticsEnumStringValue("Exploratory")]
        Exploratory,
        [AnalyticsEnumStringValue("InternetTab")]
        InternetTab,
        [AnalyticsEnumStringValue("Messaging")]
        Messaging,
        [AnalyticsEnumStringValue("Connectivity")]
        Connectivity,
        [AnalyticsEnumStringValue("Room Controls")]
        RoomControls,
        [AnalyticsEnumStringValue("Features")]
        Features,
        [AnalyticsEnumStringValue("Widgets")]
        Widgets,
        [AnalyticsEnumStringValue("Technical")]
        Technical,
        [AnalyticsEnumStringValue("Exception")]
        Exception,
        [AnalyticsEnumStringValue("Timing")]
        Timing,
        [AnalyticsEnumStringValue("Testing")]
        Testing,
        [AnalyticsEnumStringValue("Map")]
        Map,
    }
}
