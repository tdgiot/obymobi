﻿using Obymobi.Constants;
using Obymobi.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Security
{
    public class CustomerPasswordHelper
    {
        /// <summary>
        /// Get the hash to be used when creating a user and/or updating it's password
        /// </summary>
        /// <param name="plainPassword"></param>
        /// <returns></returns>
        public static string GetPasswordTransferHash(string plainPassword)
        {
            if (plainPassword.IsNullOrWhiteSpace())
                throw new Exception("A password must be at least one character!");

            return Hasher.GetHash(ObymobiConstants.MobileSalt, plainPassword);
        }

        /// <summary>
        /// Get the hash to be used when storing a Password hash to the database
        /// </summary>
        /// <returns></returns>
        public static string GetPasswordStorageHash(string passwordTransferHash, string salt)
        {
            return Hasher.GetHash(salt, passwordTransferHash);
        }
    }
}
