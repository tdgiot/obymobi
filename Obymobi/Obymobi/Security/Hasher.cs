﻿using System.Text;
using Dionysos;
using System;
using System.Globalization;

namespace Obymobi.Security
{
    public static class Hasher
    {
        public enum HasherResult
        {
            SaltIsEmpty = 200,
            TypeNotImplementedForHasing = 201
        }

        /// <summary>
        /// Get hash value for supplied text and salt (salt will be added at the end of the text)
        /// </summary>
        /// <param name="salt">Salt to be appended at the end of the text</param>
        /// <param name="text">Text to be hashed (after salt has been added to the end)</param>
        /// <returns>Hashed value of the supplied text + salt</returns>
        public static string GetHash(string salt, string text)
        {

            if (salt.IsNullOrWhiteSpace())
            {
                throw new ObymobiException(HasherResult.SaltIsEmpty);
            }

            return Dionysos.Security.Cryptography.Hasher.GetHash(text + salt, Dionysos.Security.Cryptography.HashType.SHA256);
        }

        /// <summary>
        /// Get hashed value for supplied parameters and salt. All parameters will be concatted with salt at the end)
        /// </summary>
        /// <param name="salt">Salt to be appended at the end of the text</param>
        /// <param name="parameters">The parameters to be hashed</param>
        /// <returns>Hashed value of the supplied text + salt</returns>
        public static string GetHashFromParameters(string salt, params object[] parameters)
        {
            return Hasher.GetHash(salt, Hasher.CombineParametersToString(parameters));
        }

        /// <summary>
        /// Returns if the supplied hash is valid for the supplied text & salt (salt will be added after the text)
        /// </summary>
        /// <param name="hash">Hash value supplied to be verified</param>
        /// <param name="salt">Salt to be appended at the end of the text</param>
        /// <param name="text">Text to be hashed (when the salt has been added to it)</param>
        /// <returns>True or false</returns>
        public static bool IsHashValid(string hash, string salt, string text)
        {
            string calculatedHash = Hasher.GetHash(salt, text);

            return (hash.Equals(calculatedHash, System.StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Returns if the supplied hash is valid for the concatted string of all supplied parameters & salt (salt will be added after the parameters)
        /// </summary>
        /// <param name="hash">Hash value supplied to be verified</param>
        /// <param name="salt">Salt to be appended at the end of the text</param>
        /// <param name="parameters">The parameters to be hashed (when the salt has been added to it)</param>
        /// <returns>True or false</returns>
        public static bool IsHashValidForParameters(string hash, string salt, params object[] parameters)
        {
            return IsHashValid(hash, salt, Hasher.CombineParametersToString(parameters));
        }

        private static string CombineParametersToString(params object[] parameters)
        {
            var sb = new StringBuilder();
            foreach (var parameter in parameters)
            {
                if (parameter.GetType().IsArray)
                {
                    foreach (var item in (object[])parameter)
                        sb.Append(Hasher.getParameterStringValue(item));
                }
                else
                    sb.Append(Hasher.getParameterStringValue(parameter));                    
            }
            return sb.ToString();
        }

        private static string getParameterStringValue(object o)
        {
            string toReturn = string.Empty;

            if (o == null)            
                toReturn = string.Empty; // GK indeed, that does nozthing.            
            else if (o is double)            
                toReturn = ((double)o).ToString("N", CultureInfo.InvariantCulture);
            else if (o is decimal)
                toReturn = ((decimal)o).ToString("N", CultureInfo.InvariantCulture);                     
            else if (o is float)            
                toReturn = ((float)o).ToString("N", CultureInfo.InvariantCulture);            
            else if (o.GetType().IsPrimitive || o is string)            
                toReturn = o.ToString();            
            else
                throw new ObymobiException(HasherResult.TypeNotImplementedForHasing, "Type not supported for hash: '{0}'", o.GetType());

            return toReturn;
        }

        /// <summary>
        /// Determines if a type is numeric.  Nullable numeric types are considered numeric.
        /// </summary>
        /// <remarks>
        /// Boolean is not considered numeric.
        /// </remarks>
        public static bool IsNumericType(Type type)
        {
            if (type == null)
            {
                return false;
            }

            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                case TypeCode.Single:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    return true;
                case TypeCode.Object:
                    if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        return IsNumericType(Nullable.GetUnderlyingType(type));
                    }
                    return false;
            }
            return false;
        }
    }
}
