﻿using System;
using Dionysos;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Enums;
using System.Diagnostics;

namespace Obymobi
{
    [Serializable]
    public class ObymobiMobileException : Exception
    {
        protected Enum errorEnumValue = null;
        protected string additionalInformation = string.Empty;

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>		
        /// <param name="errorEnumValue">Enum value of the error</param>		
        public ObymobiMobileException(Enum errorEnumValue)
            : base(errorEnumValue.ToString())
        {
            this.errorEnumValue = errorEnumValue;
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>		
        /// <param name="errorEnumValue">Enum value of the error</param>		
        /// <param name="ex">Exception that was thrown and is represented by this Obymobi exception</param>		
        public ObymobiMobileException(Enum errorEnumValue, Exception ex)
            : base(errorEnumValue.ToString())
        {
            this.errorEnumValue = errorEnumValue;
            this.additionalInformation += string.Format("InnerException: {0}\r\n\r\n Inner Stack Trace: {1}", ex.Message, ex.StackTrace);
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>
        /// <param name="errorEnumValue">Enum value of the error</param>		
        /// <param name="message">string.Format style message</param>		
        /// <param name="args">Arguments to be used in the message parameter</param>		
        public ObymobiMobileException(Enum errorEnumValue, string message, params object[] args)
            : base(message.FormatSafe(args))
        {
            this.errorEnumValue = errorEnumValue;
            this.additionalInformation = StringUtilLight.FormatSafe(message, args);
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>
        /// <param name="errorEnumValue">Enum value of the error</param>		
        /// <param name="ex">Exception that was thrown and is represented by this Obymobi exception</param>		
        /// <param name="message">string.Format style message</param>			
        /// <param name="args">Arguments to be used in the message parameter</param>		
        public ObymobiMobileException(Enum errorEnumValue, Exception ex, string message, params object[] args)
            : base(message.FormatSafe(args))
        {
            this.errorEnumValue = errorEnumValue;
            this.additionalInformation = message.FormatSafe(args);

            if (ex != null)
                this.additionalInformation += StringUtilLight.FormatSafe("\r\n\r\nInnerException: {0}\r\n\r\n Inner Stack Trace: {1}", ex.Message, ex.StackTrace);
        }

        /// <summary>
        /// Constructs an instance of the Lynx_media.TechnicalException class
        /// </summary>
        /// <param name="errorEnumValue">Enum value of the error</param>		
        /// <param name="message">string.Format style message</param>		
        /// <param name="args">Arguments to be used in the message parameter</param>		
        public ObymobiMobileException(Enum errorEnumValue, string xml)
            : base(errorEnumValue.ToString())
        {
            this.errorEnumValue = errorEnumValue;
            this.additionalInformation = xml;
        }

        /// <summary>
        /// Gets or sets the ErrorEnumValue
        /// </summary>
        public Enum ErrorEnumValue
        {
            get
            {
                if (this.errorEnumValue == null)
                    return UnspecifiedError.Unknown;
                else
                    return this.errorEnumValue;
            }
        }

        public string ErrorEnumType
        {
            get
            {
                if (this.errorEnumValue == null)
                    return "null";
                else
                    return this.errorEnumValue.GetType().ToString();
            }
        }

        public string ErrorEnumValueText
        {
            get
            {
                if (this.errorEnumValue == null)
                    return "null";
                else
                    return this.errorEnumValue.ToString();
            }
        }

        public int ErrorEnumValueInt
        {
            get
            {
                if (this.errorEnumValue == null)
                    return -1;
                else
                {
                    try
                    {
						return (int)(object)this.ErrorEnumValue;
                    }
                    catch
                    {
                        return -1;
                    }
                }
            }
        }

        public string BaseMessage
        {
            get { return base.Message; }
        }

        protected bool writtenToDebug = false;
        /// <summary>
        /// Implementation of Message containing all info specific for obymobi
        /// </summary>        
        public override string Message
        {
            get
            {
                string text = StringUtilLight.FormatSafe("Message: {0}\r\n\r\nError Type: {1}\r\n\r\nErrorValue: {2} ({3})\r\n\r\nAdditional Information: {4}\r\n\r\n",
                                base.Message, this.ErrorEnumType, this.ErrorEnumValueText, this.ErrorEnumValueInt, this.additionalInformation);

                if (!this.writtenToDebug && TestUtil.IsPcDeveloper)
                {
                    Debug.WriteLine(text);
                    this.writtenToDebug = true;
                }

                return text;
            }
        }

    }
}
