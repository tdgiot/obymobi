using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Attributes
{
    /// <summary>
    /// Attribute class which is used at the code generation process for Android application 
    /// to indicate that the data from the webservice has to be stored locally
    /// </summary>
    public class ClientLocalStorageAndroid : Attribute
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Attributes.ClientLocalStorageAndroid type
        /// </summary>
        public ClientLocalStorageAndroid()
        {
        }

        #endregion
    }
}
