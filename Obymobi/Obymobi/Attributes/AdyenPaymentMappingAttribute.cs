﻿using System;
using System.Collections.Generic;
using Obymobi.Enums;

namespace Obymobi.Attributes
{
    [AttributeUsage(AttributeTargets.Field)]
    public class AdyenPaymentMappingAttribute : Attribute
    {
        public AdyenPaymentMappingAttribute(string mappingName, params AdyenPaymentMethodBrand[] adyenPaymentMethodBrands)
        {
            this.MappingName = mappingName;
            this.AdyenPaymentMethodBrands = adyenPaymentMethodBrands;
        }
        public string MappingName { get; }

        public IEnumerable<AdyenPaymentMethodBrand> AdyenPaymentMethodBrands { get; }
    }
}
