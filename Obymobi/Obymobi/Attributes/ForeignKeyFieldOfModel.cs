using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Attributes
{
    /// <summary>
    /// Attribute class which is used to indicate that a property on a model 
    /// </summary>
	public class ForeignKeyFieldOfModel : Attribute
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Attributes.ExportToMobileClientAttribute type
        /// </summary>
		public ForeignKeyFieldOfModel()
        {
        }

        #endregion
    }
}
