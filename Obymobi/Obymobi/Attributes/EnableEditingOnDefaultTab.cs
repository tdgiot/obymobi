using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Attributes
{
    /// <summary>
    /// Attribute class which is used to indicate that a CustomTextType textbox is enabled on the default culture tab.
    /// </summary>
    public class EnableEditingOnDefaultTab : Attribute
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Attributes.EnableEditingOnDefault type
        /// </summary>
        public EnableEditingOnDefaultTab()
        {
        }

        #endregion
    }
}
