using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Attributes
{
    /// <summary>
    /// Attribute class which is used to indicate that a CustomTextType textbox is rendered.
    /// </summary>
    public class DisableRendering : Attribute
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Attributes.DisableRendering type
        /// </summary>
        public DisableRendering()
        {
        }

        #endregion
    }
}
