using System;

namespace Obymobi.Attributes
{
    /// <summary>
    /// Attribute class which is used to indicate that the text in a textbox is saved with two spaces at the end of each line and rendered without those spaces.
    /// </summary>
    public class Markdown : Attribute
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Attributes.Markdown type
        /// </summary>
        public Markdown()
        { }

        #endregion
    }
}
