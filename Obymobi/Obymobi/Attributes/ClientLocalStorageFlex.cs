using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Attributes
{
    /// <summary>
    /// Attribute class which is used at the code generation process for Flex application 
    /// to indicate that the data from the webservice has to be stored locally
    /// </summary>
    public class ClientLocalStorageFlex : Attribute
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Attributes.ClientLocalStorageFlex type
        /// </summary>
        public ClientLocalStorageFlex()
        {
        }

        #endregion
    }
}
