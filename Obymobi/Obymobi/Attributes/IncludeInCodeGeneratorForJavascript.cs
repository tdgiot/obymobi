using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Attributes
{
    /// <summary>
    /// Attribute class which is used to indicate that a class or property should be exported while generating code for a mobile application
    /// </summary>
    public class IncludeInCodeGeneratorForJavascript : Attribute
    {
        public IncludeInCodeGeneratorForJavascript()
        {

        }
    }
}
