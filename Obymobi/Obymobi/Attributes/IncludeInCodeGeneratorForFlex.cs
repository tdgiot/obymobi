using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Attributes
{
    /// <summary>
    /// Attribute class which is used to indicate that a class or property should NOT be exported to the mobile client code
    /// </summary>
    public class IncludeInCodeGeneratorForFlex : Attribute
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Attributes.IncludeInCodeGeneratorForFlex type
        /// </summary>
        public IncludeInCodeGeneratorForFlex()
        {
        }

        #endregion
    }
}