using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Attributes
{
    /// <summary>
    /// Attribute class which is used to indicate that a class or property should be exported while generating code for a Xamarin application
    /// </summary>
    public class IncludeInCodeGeneratorForXamarin : Attribute
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Attributes.IncludeInCodeGeneratorForXamarin type
        /// </summary>
        public IncludeInCodeGeneratorForXamarin()
        {
        }

        #endregion
    }
}
