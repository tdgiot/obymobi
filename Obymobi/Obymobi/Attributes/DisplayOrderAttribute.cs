﻿using System;

namespace Obymobi.Attributes
{
    /// <summary>
    /// Attribute that can be used to set a custom sort order.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class DisplayOrderAttribute : Attribute
    {
        public DisplayOrderAttribute(int displayOrder)
        {
            this.DisplayOrder = displayOrder;
        }

        public int DisplayOrder { get; }
    }
}
