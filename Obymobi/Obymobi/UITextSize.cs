﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi
{
    public class UITextSize
    {
        public UITextSizeType Type { get; private set; }
        public int TextSize { get; private set; }
        public int TextSizeTmini { get; private set; }
        public UITextSizeGroup Group { get; private set; }

        public UITextSize(UITextSizeType type, int textSize, int textSizeTmini, UITextSizeGroup group)
        {
            this.Type = type;
            this.TextSize = textSize;
            this.TextSizeTmini = textSizeTmini;
            this.Group = group;
        }
    }
}
