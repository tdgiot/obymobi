﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi
{
    public static class UIColors
    {
        public static List<UIColor> Colors 
        {
            get
            {
                if(UIColors.colors == null)
                {
                    UIColors.colors = UIColors.CreateColors();
                }
                return UIColors.colors;
            }
        }

        private static List<UIColor> colors = null;

        private static List<UIColor> CreateColors()
        {
            List<UIColor> colors = new List<UIColor>();
            colors.Add(new UIColor(UIColorType.ListCategoryBackgroundColor, 102, 227, 223, 220, UIColorGroup.Lists, "A1", "A-categories.jpg"));
            colors.Add(new UIColor(UIColorType.ListCategoryL2BackgroundColor, 102, 227, 223, 220, UIColorGroup.Lists, "A1", "A-categories.jpg"));
            colors.Add(new UIColor(UIColorType.ListCategoryL3BackgroundColor, 102, 227, 223, 220, UIColorGroup.Lists, "A1", "A-categories.jpg"));
            colors.Add(new UIColor(UIColorType.ListCategoryL4BackgroundColor, 102, 227, 223, 220, UIColorGroup.Lists, "A1", "A-categories.jpg"));
            colors.Add(new UIColor(UIColorType.ListCategoryL5BackgroundColor, 102, 227, 223, 220, UIColorGroup.Lists, "A1", "A-categories.jpg"));
            colors.Add(new UIColor(UIColorType.ListCategoryDividerColor, 102, 195, 192, 189, UIColorGroup.Lists, "A2", "A-categories.jpg"));
            colors.Add(new UIColor(UIColorType.ListCategoryTextColor, 255, 255, 255, 255, UIColorGroup.Lists, "A3", "A-categories.jpg"));
            colors.Add(new UIColor(UIColorType.ListCategorySelectedBackgroundColor, 102, 48, 149, 180, UIColorGroup.Lists, "A4", "A-categories.jpg"));
            colors.Add(new UIColor(UIColorType.ListCategorySelectedDividerColor, 102, 48, 149, 180, UIColorGroup.Lists, "A5", "A-categories.jpg"));
            colors.Add(new UIColor(UIColorType.ListCategorySelectedTextColor, 255, 255, 255, 255, UIColorGroup.Lists, "A6", "A-categories.jpg"));
            colors.Add(new UIColor(UIColorType.ListItemBackgroundColor, 102, 240, 240, 240, UIColorGroup.Lists, "A7", "A-items.jpg"));
            colors.Add(new UIColor(UIColorType.ListItemDividerColor, 102, 211, 209, 206, UIColorGroup.Lists, "A8", "A-items.jpg"));
            colors.Add(new UIColor(UIColorType.ListItemTextColor, 255, 255, 255, 255, UIColorGroup.Lists, "A9", "A-items.jpg"));
            colors.Add(new UIColor(UIColorType.ListItemSelectedBackgroundColor, 102, 48, 149, 180, UIColorGroup.Lists, "A10", "A-items.jpg"));
            colors.Add(new UIColor(UIColorType.ListItemSelectedDividerColor, 102, 48, 149, 180, UIColorGroup.Lists, "A11", "A-items.jpg"));
            colors.Add(new UIColor(UIColorType.ListItemSelectedTextColor, 255, 255, 255, 255, UIColorGroup.Lists, "A12", "A-items.jpg"));

            colors.Add(new UIColor(UIColorType.WidgetBackgroundColor, 102, 0, 0, 0, UIColorGroup.Widgets, "B1", "B.jpg"));
            colors.Add(new UIColor(UIColorType.WidgetBorderColor, 102, 153, 153, 153, UIColorGroup.Widgets, "B2", "B.jpg"));
            colors.Add(new UIColor(UIColorType.WidgetTextColor, 255, 255, 255, 255, UIColorGroup.Widgets, "B3", "B.jpg"));
            colors.Add(new UIColor(UIColorType.WidgetSecondaryTextColor, 255, 173, 173, 173, UIColorGroup.Widgets));

            colors.Add(new UIColor(UIColorType.HeaderColor, 255, 70, 70, 70, UIColorGroup.Header, "C1", "C.jpg"));
            colors.Add(new UIColor(UIColorType.TabBackgroundBottomColor, 255, 25, 25, 25, UIColorGroup.Header, "C3", "C.jpg"));
            colors.Add(new UIColor(UIColorType.TabBackgroundTopColor, 255, 99, 99, 99, UIColorGroup.Header, "C2", "C.jpg"));
            colors.Add(new UIColor(UIColorType.TabBorderColor, 255, 18, 18, 18, UIColorGroup.Header, "C4", "C.jpg"));
            colors.Add(new UIColor(UIColorType.TabDividerColor, 225, 116, 116, 116, UIColorGroup.Header, "C5", "C.jpg"));
            colors.Add(new UIColor(UIColorType.TabActiveTextColor, 255, 255, 255, 0, UIColorGroup.Header, "C6", "C.jpg"));
            colors.Add(new UIColor(UIColorType.TabInactiveTextColor, 255, 255, 255, 255, UIColorGroup.Header, "C7", "C.jpg"));
            colors.Add(new UIColor(UIColorType.BasketTabTextGlow, 255, 255, 255, 0, UIColorGroup.Header));

            colors.Add(new UIColor(UIColorType.PageBackgroundColor, 255, 255, 255, 255, UIColorGroup.Pages, "D1", "D.jpg"));
            colors.Add(new UIColor(UIColorType.PageBorderColor, 255, 171, 171, 171, UIColorGroup.Pages, "D2", "D.jpg"));
            colors.Add(new UIColor(UIColorType.PageTitleTextColor, 255, 86, 81, 79, UIColorGroup.Pages, "D3", "D.jpg"));
            colors.Add(new UIColor(UIColorType.PageDescriptionTextColor, 255, 106, 106, 104, UIColorGroup.Pages, "D4", "D.jpg"));
            colors.Add(new UIColor(UIColorType.PageErrorTextColor, 255, 206, 16, 26, UIColorGroup.Pages, "D5", "D.jpg"));
            colors.Add(new UIColor(UIColorType.PagePriceTextColor, 255, 110, 103, 99, UIColorGroup.Pages, "D6", "D.jpg"));
            colors.Add(new UIColor(UIColorType.PageDividerTopColor, 255, 229, 229, 229, UIColorGroup.Pages, "D7", "D.jpg"));
            colors.Add(new UIColor(UIColorType.PageDividerBottomColor, 255, 239, 239, 239, UIColorGroup.Pages, "D8", "D.jpg"));
            colors.Add(new UIColor(UIColorType.PageFooterColor, 255, 227, 223, 220, UIColorGroup.Pages, "D9", "D.jpg"));
            colors.Add(new UIColor(UIColorType.PageInstructionsTextColor, 255, 191, 191, 191, UIColorGroup.Pages, "D10", "D.jpg"));

            colors.Add(new UIColor(UIColorType.FooterBackgroundBottomColor, 255, 51, 51, 51, UIColorGroup.Footer, "E2", "E.jpg"));
            colors.Add(new UIColor(UIColorType.FooterBackgroundTopColor, 255, 27, 27, 27, UIColorGroup.Footer, "E1", "E.jpg"));
            colors.Add(new UIColor(UIColorType.FooterDividerColor, 255, 51, 51, 51, UIColorGroup.Footer, "E3", "E.jpg"));
            colors.Add(new UIColor(UIColorType.FooterTextColor, 255, 102, 102, 102, UIColorGroup.Footer, "E4", "E.jpg", false));
            colors.Add(new UIColor(UIColorType.FooterButtonBackgroundTopColor, 255, 86, 86, 86, UIColorGroup.Footer, "E5", "E.jpg"));
            colors.Add(new UIColor(UIColorType.FooterButtonBackgroundBottomColor, 255, 30, 30, 30, UIColorGroup.Footer, "E6", "E.jpg"));
            colors.Add(new UIColor(UIColorType.FooterButtonBorderColor, 255, 115, 115, 115, UIColorGroup.Footer, "E7", "E.jpg"));
            colors.Add(new UIColor(UIColorType.FooterButtonTextColor, 255, 255, 255, 255, UIColorGroup.Footer, "E8", "E.jpg", false));
            colors.Add(new UIColor(UIColorType.FooterButtonDisabledBackgroundTopColor, 255, 64, 64, 64, UIColorGroup.Footer, "E5", "E.jpg"));
            colors.Add(new UIColor(UIColorType.FooterButtonDisabledBackgroundBottomColor, 255, 64, 64, 64, UIColorGroup.Footer, "E6", "E.jpg"));
            colors.Add(new UIColor(UIColorType.FooterButtonDisabledBorderColor, 255, 89, 89, 89, UIColorGroup.Footer, "E7", "E.jpg"));
            colors.Add(new UIColor(UIColorType.FooterButtonDisabledTextColor, 255, 136, 136, 136, UIColorGroup.Footer, "E8", "E.jpg", false));
            colors.Add(new UIColor(UIColorType.FooterRadioHighlightColor, 255, 255, 255, 255, UIColorGroup.Footer, false));

            colors.Add(new UIColor(UIColorType.ButtonBackgroundTopColor, 255, 86, 86, 86, UIColorGroup.Buttons, "F1", "F.jpg"));
            colors.Add(new UIColor(UIColorType.ButtonBackgroundBottomColor, 255, 30, 30, 30, UIColorGroup.Buttons, "F2", "F.jpg"));
            colors.Add(new UIColor(UIColorType.ButtonBorderColor, 255, 115, 115, 115, UIColorGroup.Buttons, "F3", "F.jpg"));
            colors.Add(new UIColor(UIColorType.ButtonTextColor, 255, 255, 255, 255, UIColorGroup.Buttons, "F4", "F.jpg"));
            colors.Add(new UIColor(UIColorType.ButtonPositiveBackgroundTopColor, 255, 147, 208, 100, UIColorGroup.Buttons, "F5", "F.jpg"));
            colors.Add(new UIColor(UIColorType.ButtonPositiveBackgroundBottomColor, 255, 89, 151, 42, UIColorGroup.Buttons, "F6", "F.jpg"));
            colors.Add(new UIColor(UIColorType.ButtonPositiveBorderColor, 255, 255, 255, 255, UIColorGroup.Buttons, "F7", "F.jpg"));
            colors.Add(new UIColor(UIColorType.ButtonPositiveTextColor, 255, 255, 255, 255, UIColorGroup.Buttons, "F8", "F.jpg"));
            colors.Add(new UIColor(UIColorType.ButtonDisabledBackgroundTopColor, 255, 64, 64, 64, UIColorGroup.Buttons, "F9", "F.jpg"));
            colors.Add(new UIColor(UIColorType.ButtonDisabledBackgroundBottomColor, 255, 64, 64, 64, UIColorGroup.Buttons, "F10", "F.jpg"));
            colors.Add(new UIColor(UIColorType.ButtonDisabledBorderColor, 255, 89, 89, 89, UIColorGroup.Buttons, "F11", "F.jpg"));
            colors.Add(new UIColor(UIColorType.ButtonDisabledTextColor, 255, 136, 136, 136, UIColorGroup.Buttons, "F12", "F.jpg"));

            colors.Add(new UIColor(UIColorType.DialogBackgroundColor, 255, 26, 26, 26, UIColorGroup.Dialogs, "H1", "H.jpg"));
            colors.Add(new UIColor(UIColorType.DialogBorderColor, 255, 115, 115, 115, UIColorGroup.Dialogs, "H2", "H.jpg"));
            colors.Add(new UIColor(UIColorType.DialogPanelBackgroundColor, 255, 51, 51, 51, UIColorGroup.Dialogs, "H3", "H.jpg"));
            colors.Add(new UIColor(UIColorType.DialogPanelBorderColor, 255, 115, 115, 115, UIColorGroup.Dialogs, "H4", "H.jpg"));
            colors.Add(new UIColor(UIColorType.DialogPrimaryTextColor, 255, 255, 255, 255, UIColorGroup.Dialogs, "H5", "H.jpg"));
            colors.Add(new UIColor(UIColorType.DialogSecondaryTextColor, 255, 190, 190, 190, UIColorGroup.Dialogs, "H6", "H.jpg"));
            colors.Add(new UIColor(UIColorType.DialogTitleTextColor, 255, 255, 255, 255, UIColorGroup.Dialogs, "H7", "H.jpg"));

            colors.Add(new UIColor(UIColorType.TextboxBackgroundColor, 255, 0, 0, 0, UIColorGroup.Textboxes, "G1", "G.jpg"));
            colors.Add(new UIColor(UIColorType.TextboxBorderColor, 255, 74, 74, 74, UIColorGroup.Textboxes, "G2", "G.jpg"));
            colors.Add(new UIColor(UIColorType.TextboxTextColor, 255, 255, 255, 255, UIColorGroup.Textboxes, "G3", "G.jpg"));
            colors.Add(new UIColor(UIColorType.TextboxCursorColor, 255, 128, 128, 128, UIColorGroup.Textboxes));

            colors.Add(new UIColor(UIColorType.SpinnerBackgroundColor, 255, 51, 51, 51, UIColorGroup.Spinners, "I1", "I.jpg"));
            colors.Add(new UIColor(UIColorType.SpinnerBorderColor, 255, 115, 115, 115, UIColorGroup.Spinners, "I2", "I.jpg"));
            colors.Add(new UIColor(UIColorType.SpinnerTextColor, 255, 255, 255, 255, UIColorGroup.Spinners, "I3", "I.jpg"));

            colors.Add(new UIColor(UIColorType.RoomControlButtonOuterBorderColor, 255, 204, 204, 204, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlButtonInnerBorderTopColor, 255, 242, 240, 239, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlButtonInnerBorderBottomColor, 255, 228, 229, 227, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlButtonBackgroundTopColor, 255, 242, 240, 239, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlButtonBackgroundBottomColor, 255, 225, 224, 224, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlDividerTopColor, 255, 229, 229, 229, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlDividerBottomColor, 255, 239, 239, 239, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlButtonTextColor, 255, 86, 81, 79, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlTitleColor, 255, 86, 81, 79, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlTextColor, 255, 106, 106, 104, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlPageBackgroundColor, 255, 255, 255, 255, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlSpinnerBorderColor, 255, 216, 215, 214, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlSpinnerBackgroundColor, 255, 241, 240, 239, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlSpinnerTextColor, 255, 86, 81, 79, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlButtonIndicatorColor, 255, 114, 240, 134, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlButtonIndicatorBackgroundColor, 255, 214, 213, 213, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlHeaderButtonBackgroundColor, 102, 240, 240, 240, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlHeaderButtonSelectorColor, 255, 64, 109, 126, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlHeaderButtonTextColor, 255, 86, 81, 79, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlStationNumberColor, 255, 180, 180, 180, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlSliderMinColor, 255, 239, 171, 113, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlSliderMaxColor, 255, 240, 217, 120, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlToggleOnTextColor, 255, 255, 255, 255, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlToggleOffTextColor, 255, 86, 81, 79, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlToggleBackgroundColor, 255, 241, 240, 239, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlToggleBorderColor, 255, 216, 215, 214, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.RoomControlThermostatComponentColor, 255, 191, 191, 191, UIColorGroup.RoomControl));

            colors.Add(new UIColor(UIColorType.ListItemPriceTextColor, 255, 240, 240, 240, UIColorGroup.Lists));
            colors.Add(new UIColor(UIColorType.ListItemSelectedPriceTextColor, 255, 240, 240, 240, UIColorGroup.Lists));

            colors.Add(new UIColor(UIColorType.AlterationDialogBackgroundColor, 255, 26, 26, 26, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogBorderColor, 255, 115, 115, 115, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogPanelBackgroundColor, 255, 51, 51, 51, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogPanelBorderColor, 255, 115, 115, 115, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogListItemBackgroundColor, 0, 0, 0, 0, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogListItemSelectedBackgroundColor, 255, 76, 76, 76, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogListItemSelectedBorderColor, 0, 0, 0, 0, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogInputBackgroundColor, 0, 0, 0, 0, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogInputBorderColor, 255, 93, 93, 93, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogInputTextColor, 255, 255, 255, 255, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogInputHintTextColor, 255, 128, 128, 128, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogPrimaryTextColor, 255, 255, 255, 255, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogSecondaryTextColor, 255, 106, 106, 104, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogRadioButtonColor, 255, 255, 255, 255, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogCheckBoxColor, 255, 255, 255, 255, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogCloseButtonColor, 255, 255, 255, 255, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogListViewArrowColor, 255, 255, 255, 255, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogListViewDividerColor, 255, 255, 255, 255, UIColorGroup.AlterationDialog));
	        colors.Add(new UIColor(UIColorType.AlterationDialogListItemSelectedPrimaryTextColor, 255, 255, 255, 255, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogListItemSelectedSecondaryTextColor, 255, 255, 255, 255, UIColorGroup.AlterationDialog));
	        colors.Add(new UIColor(UIColorType.AlterationDialogListItemSelectedRadioButtonColor, 255, 255, 255, 255, UIColorGroup.AlterationDialog));
            colors.Add(new UIColor(UIColorType.AlterationDialogListItemSelectedCheckBoxColor, 255, 255, 255, 255, UIColorGroup.AlterationDialog));

            colors.Add(new UIColor(UIColorType.AreaTabActiveText, 255, 255, 255, 255, UIColorGroup.RoomControl));
            colors.Add(new UIColor(UIColorType.AreaTabInactiveText, 255, 120, 120, 120, UIColorGroup.RoomControl));

            colors.Add(new UIColor(UIColorType.BrowserAgeVerificationTitle, 255, 86, 81, 79, UIColorGroup.BrowserAgeVerification));
            colors.Add(new UIColor(UIColorType.BrowserAgeVerificationText, 255, 106, 106, 104, UIColorGroup.BrowserAgeVerification));
            colors.Add(new UIColor(UIColorType.BrowserAgeVerificationBackground, 255, 255, 255, 255, UIColorGroup.BrowserAgeVerification));
            colors.Add(new UIColor(UIColorType.BrowserAgeVerificationButtonBackgroundTop, 255, 86, 86, 86, UIColorGroup.BrowserAgeVerification));
            colors.Add(new UIColor(UIColorType.BrowserAgeVerificationButtonBackgroundBottom, 255, 30, 30, 30, UIColorGroup.BrowserAgeVerification));
            colors.Add(new UIColor(UIColorType.BrowserAgeVerificationButtonBorderColor, 255, 115, 115, 115, UIColorGroup.BrowserAgeVerification));
            colors.Add(new UIColor(UIColorType.BrowserAgeVerificationButtonText, 255, 255, 255, 255, UIColorGroup.BrowserAgeVerification));

            colors.Add(new UIColor(UIColorType.BrowserNavigationBarBackground, 255, 0, 0, 0, UIColorGroup.Browser));
            colors.Add(new UIColor(UIColorType.BrowserNavigationBarText, 255, 255, 255, 255, UIColorGroup.Browser));

            colors.Add(new UIColor(UIColorType.ProductDialogBackgroundColor, 255, 255, 255, 255, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogHeaderColor, 255, 40, 75, 97, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogHeaderTextColor, 255, 255, 255, 255, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogHeaderCloseColor, 255, 255, 255, 255, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnBorderColor, 255, 191, 191, 191, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnBackgroundColor, 255, 231, 231, 231, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnHeaderPrimaryTextColor, 255, 3, 42, 71, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnHeaderSecondaryTextColor, 180, 3, 42, 71, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionColor, 125, 255, 255, 255, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionDividerTopColor, 255, 191, 191, 191, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionDividerBottomColor, 255, 255, 255, 255, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionPrimaryTextInactiveColor, 255, 3, 42, 71, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionPrimaryTextActiveColor, 255, 102, 212, 97, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionSecondaryTextInactiveColor, 180, 3, 42, 71, UIColorGroup.ProductDialog)); 
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionSecondaryTextActiveColor, 255, 102, 212, 97, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionCheckmarkColor, 255, 102, 212, 97, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionMinusButtonTextColor, 255, 255, 255, 255, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionMinusButtonBorderColor, 255, 151, 151, 151, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionMinusButtonBackgroundColor, 255, 40, 75, 97, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionPlusButtonTextColor, 255, 255, 255, 255, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionPlusButtonInactiveBorderColor, 255, 151, 151, 151, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionPlusButtonInactiveBackgroundColor, 255, 183, 183, 183, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionPlusButtonActiveBorderColor, 255, 63, 182, 56, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionPlusButtonActiveBackgroundColor, 255, 102, 212, 92, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionQuantityTextColor, 255, 255, 255, 255, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionQuantityBorderColor, 255, 63, 182, 56, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionQuantityBackgroundColor, 255, 102, 212, 97, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionSuggestionTextColor, 255, 102, 212, 97, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionSuggestionBorderColor, 255, 191, 191, 191, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnOptionSuggestionBackgroundColor, 255, 228, 228, 228, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnSelectedOptionBackgroundColor, 255, 255, 255, 255, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnSelectedOptionPrimaryTextInactiveColor, 255, 3, 42, 71, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnSelectedOptionPrimaryTextActiveColor, 255, 3, 42, 71, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnSelectedOptionSecondaryTextInactiveColor, 255, 102, 212, 97, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnSelectedOptionSecondaryTextActiveColor, 255, 102, 212, 97, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnInputFieldCaptionTextColor, 255, 3, 42, 71, UIColorGroup.ProductDialog));            
            colors.Add(new UIColor(UIColorType.ProductDialogColumnInputFieldTextColor, 255, 153, 160, 168, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnInputFieldBorderColor, 255, 153, 160, 168, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnInputFieldBackgroundColor, 0, 0, 0, 0, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnDateSpinnerBorderColor, 255, 153, 160, 168, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnDateSpinnerBackgroundColor, 0, 0, 0, 0, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnDatePanelBorderColor, 255, 191, 191, 191, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnDatePanelBackgroundColor, 0, 0, 0, 0, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogColumnInstructionsTextColor, 255, 3, 42, 71, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterColor, 255, 255, 255, 255, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterDividerColor, 255, 191, 191, 191, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterBackButtonTextColor, 255, 255, 255, 255, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterBackButtonBorderInactiveColor, 255, 151, 151, 151, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterBackButtonBorderActiveColor, 255, 151, 151, 151, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterBackButtonBackgroundInactiveColor, 255, 183, 183, 183, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterBackButtonBackgroundActiveColor, 255, 40, 75, 97, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterNextButtonTextColor, 255, 255, 255, 255, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterNextButtonBorderInactiveColor, 255, 151, 151, 151, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterNextButtonBorderActiveColor, 255, 63, 182, 56, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterNextButtonBackgroundInactiveColor, 255, 183, 183, 183, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterNextButtonBackgroundActiveColor, 255, 102, 212, 97, UIColorGroup.ProductDialog));
            colors.Add(new UIColor(UIColorType.ProductDialogFooterPriceTextColor, 255, 3, 42, 71, UIColorGroup.ProductDialog));

            colors.Add(new UIColor(UIColorType.MapMarkerBackgroundColor, 255, 255, 255, 255, UIColorGroup.Map));
            colors.Add(new UIColor(UIColorType.MapMarkerImageBackgroundColor, 255, 81, 83, 86, UIColorGroup.Map));
            colors.Add(new UIColor(UIColorType.MapMarkerTitleTextColor, 255, 255, 255, 255, UIColorGroup.Map));
            colors.Add(new UIColor(UIColorType.MapMarkerTitleBackgroundColor, 150, 80, 80, 80, UIColorGroup.Map));
            colors.Add(new UIColor(UIColorType.MapMarkerLocationTextColor, 255, 66, 66, 66, UIColorGroup.Map));
            colors.Add(new UIColor(UIColorType.MapMarkerDescriptionTextColor, 255, 142, 143, 145, UIColorGroup.Map));
            colors.Add(new UIColor(UIColorType.MapMarkerDetailsTextColor, 255, 66, 66, 66, UIColorGroup.Map));

            colors.Add(new UIColor(UIColorType.ToggleButtonSliderMinColor, 255, 241, 240, 239, UIColorGroup.ToggleButton));
            colors.Add(new UIColor(UIColorType.ToggleButtonSliderMaxColor, 255, 51, 51, 51, UIColorGroup.ToggleButton));
            colors.Add(new UIColor(UIColorType.ToggleButtonOnTextColor, 255, 51, 51, 51, UIColorGroup.ToggleButton));
            colors.Add(new UIColor(UIColorType.ToggleButtonOffTextColor, 255, 242, 240, 239, UIColorGroup.ToggleButton));
            colors.Add(new UIColor(UIColorType.ToggleButtonBackgroundColor, 255, 51, 51, 51, UIColorGroup.ToggleButton));
            colors.Add(new UIColor(UIColorType.ToggleButtonBorderColor, 255, 115, 115, 115, UIColorGroup.ToggleButton));
            colors.Add(new UIColor(UIColorType.ToggleButtonBackgroundTopColor, 255, 242, 240, 239, UIColorGroup.ToggleButton));
            colors.Add(new UIColor(UIColorType.ToggleButtonBackgroundBottomColor, 255, 225, 224, 224, UIColorGroup.ToggleButton));

	        colors.Add(new UIColor(UIColorType.ScreenSaverBackgroundColor, 255, 0, 0, 0, UIColorGroup.ScreenSaver));
	        colors.Add(new UIColor(UIColorType.ScreenSaverDateTextColor, 255, 173, 173, 173, UIColorGroup.ScreenSaver));
	        colors.Add(new UIColor(UIColorType.ScreenSaverTimeTextColor, 255, 255, 255, 255, UIColorGroup.ScreenSaver));
	        colors.Add(new UIColor(UIColorType.ScreenSaverColonTextColor, 255, 173, 173, 173, UIColorGroup.ScreenSaver));
	        colors.Add(new UIColor(UIColorType.ScreenSaverAmPmTextColor, 255, 173, 173, 173, UIColorGroup.ScreenSaver));
	        colors.Add(new UIColor(UIColorType.ScreenSaverRadioIconColor, 255, 255, 255, 255, UIColorGroup.ScreenSaver));
	        colors.Add(new UIColor(UIColorType.ScreenSaverRadioTextColor, 255, 255, 255, 255, UIColorGroup.ScreenSaver));
	        colors.Add(new UIColor(UIColorType.ScreenSaverAlarmIconColor, 255, 255, 255, 255, UIColorGroup.ScreenSaver));
	        colors.Add(new UIColor(UIColorType.ScreenSaverAlarmTextColor, 255, 255, 255, 255, UIColorGroup.ScreenSaver));
            colors.Add(new UIColor(UIColorType.ScreenSaverScreenOffIconColor, 255, 255, 255, 255, UIColorGroup.ScreenSaver));
	        colors.Add(new UIColor(UIColorType.ScreenSaverHomeIconColor, 255, 255, 255, 255, UIColorGroup.ScreenSaver));

            colors.Add(new UIColor(UIColorType.QrCodeBackgroundColor, 255, 255, 255, 255, UIColorGroup.QrCode));
            colors.Add(new UIColor(UIColorType.QrCodeForegroundColor, 255, 0, 0, 0, UIColorGroup.QrCode));

            return colors;
        }
        
        public static UIColor GetByType(UIColorType type)
        {
            return UIColors.Colors.FirstOrDefault(x => x.Type == type);
        }
    }
}
