﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public interface IAlterationComponent
    {
        string ItemId { get; }
        string ParentItemId { get; }
        string ItemType { get; }
        string Name { get; set; }
        int SortOrder { get; set; }
        bool Visible { get; }
        string TypeName { get; }
        bool IsNew { get; set; }
        bool Save();
        bool Delete();
    }
}
