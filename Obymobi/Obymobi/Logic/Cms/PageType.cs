﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public enum PageType
    {
        TextWithImageLeft = 1,
        TextWithImageRight = 2,
        TextWithImageLeftTransparent = 3,
        TextWithImageRightTransparent = 4,
        TextWithImagesBottom = 5,
        SingleImage = 6,
        YouTube = 7,
        Map = 8,
        WebView = 9,
        Venue = 10,
        WebLink = 11,
        PageLink = 12
    }
}
