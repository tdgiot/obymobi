﻿using System.Collections.Generic;

namespace Obymobi.Logic.Cms
{    
    public interface IPage
    {
        int PrimaryId { get; set; }
        string Name { get; set; }
        int SortOrder { get; set; }
        int PageType { get; set; }
        PageType PageTypeAsEnum { get; set; }
        int? ParentPageId { get; set; }
        bool IsNew { get; set; }
        IPage PageTemplate { get; }
        List<IPage> IPageCollection { get; }
        List<IPageTypeElementData> IPageElementCollection { get; }
        bool Save();
        bool Delete();
    }
}
