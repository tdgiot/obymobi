﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public enum PageElementType : int
    {
        SingleLineText = 100,
        MultiLineText = 200,
        Image = 300,
        WebView = 400,
        YouTube = 500,
        Map = 700,
        Venue = 800,
        PageLink = 900
    }
}
