﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Logic.Cms
{
    public class MapPageType : PageTypeBase
    {
        #region Fields

        private SingleLineTextElement title;
        private MapElement map;

        #endregion

        #region Constructor & Initialization

        internal MapPageType()
            : base(PageType.Map, "Map", "Map")
        {
        }

        protected override List<SiteType> InitCompatibleSiteTypes()
        {
            List<SiteType> siteTypes = new List<SiteType>();
            siteTypes.Add(SiteType.Microsite);
            return siteTypes;
        }

        protected override List<PageTypeElement> InitPageTypeElements()
        {
            this.title = new SingleLineTextElement("title", "Title", this);
            this.map = new MapElement("Map", "Map", this);

            return new List<PageTypeElement>() { this.title, this.map };
        }

        #endregion

        #region Methods

        protected override void BindDataSource()
        {
            this.title.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.title.SystemName); 
            this.map.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.map.SystemName);

            if (this.title.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.title.SystemName))
                this.title.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.title.SystemName);
            if (this.map.IsEmpty() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.map.SystemName))
                this.map.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.map.SystemName);
        }

        #endregion

        #region Page Elements

        public MapElement Map
        {
            get
            {
                return this.map;
            }
        }

        #endregion
    }
}
