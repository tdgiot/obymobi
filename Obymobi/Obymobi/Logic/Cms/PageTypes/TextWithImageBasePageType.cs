﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Logic.Cms
{
    public abstract class TextWithImageBasePageType : PageTypeBase
    {
        // Doesn't have to be abstract, but also should never be intialized, so it's marked that way.

        #region Fields

        private SingleLineTextElement title;
        private MultiLineTextElement text;
        private ImageElement image;

        #endregion

        #region Constructor & Initialization

        internal TextWithImageBasePageType(PageType pageType, string systemName, string name)
            : base(pageType, systemName, name)
        {
        }

        protected override List<SiteType> InitCompatibleSiteTypes()
        {
            List<SiteType> siteTypes = new List<SiteType>();
            siteTypes.Add(SiteType.Microsite);
            siteTypes.Add(SiteType.Directory);
            return siteTypes;
        }

        protected override List<PageTypeElement> InitPageTypeElements()
        {
            this.title = new SingleLineTextElement("title", "Title", this);
            this.text = new MultiLineTextElement("text", "Text", this);
            this.image = new ImageElement("image", "Image", this, MediaTypeGroup.SiteMobileHeaderImage, MediaTypeGroup.SiteTwoThirdsPageImage, MediaType.SitePageTwoThirds1280x800);

            return new List<PageTypeElement>() { this.title, this.text, this.image };
        }       

        #endregion

        #region Methods

        protected override void BindDataSource()
        {
            this.title.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.title.SystemName);
            this.text.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.text.SystemName);
            this.image.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.image.SystemName);

            if (this.title.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.title.SystemName))
                this.title.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.title.SystemName);
            if (this.text.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.text.SystemName))
                this.text.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.text.SystemName);
            if ((this.image.DataSource == null || !this.image.DataSource.HasMedia) && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.image.SystemName))
                this.image.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.image.SystemName);
        }

        #endregion

        #region Page Elements

        public SingleLineTextElement Title
        {
            get
            {
                return this.title;
            }
        }

        public MultiLineTextElement Text
        {
            get
            {
                return this.text;
            }
        }

        public ImageElement Image
        {
            get
            {
                return this.image;
            }
        }

        #endregion
    }
}
