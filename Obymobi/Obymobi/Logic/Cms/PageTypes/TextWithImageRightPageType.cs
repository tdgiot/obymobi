﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public class TextWithImageRightPageType : TextWithImageBasePageType
    {
        public TextWithImageRightPageType()
            : base(PageType.TextWithImageRight, "TextWithImageRight", "Text with image right")
        { }
    }
}
