﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Logic.Cms
{
    public class SingleImagePageType : PageTypeBase
    {
        #region Fields

        private SingleLineTextElement title;
        private ImageElement image;

        #endregion

        #region Constructor & Initialization

        internal SingleImagePageType()
            : base(PageType.SingleImage, "SingleImage", "Single Image")
        {
        }

        protected override List<SiteType> InitCompatibleSiteTypes()
        {
            List<SiteType> siteTypes = new List<SiteType>();            
            siteTypes.Add(SiteType.Microsite);            
            return siteTypes;
        }

        protected override List<PageTypeElement> InitPageTypeElements()
        {
            this.title = new SingleLineTextElement("title", "Title", this);
            this.image = new ImageElement("image", "Image", this, MediaTypeGroup.SiteMobileHeaderImage, MediaTypeGroup.SiteFullPageImage, MediaType.SitePageFull1280x800);

            return new List<PageTypeElement>() { this.title, this.image };
        }

        #endregion

        #region Methods

        protected override void BindDataSource()
        {
            this.title.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.title.SystemName); 
            this.image.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.image.SystemName);

            if (this.title.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.title.SystemName))
                this.title.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.title.SystemName);
            if ((this.image.DataSource == null || !this.image.DataSource.HasMedia) && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.image.SystemName))
                this.image.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.image.SystemName);
        }

        #endregion

        #region Page Elements

        public SingleLineTextElement Title
        {
            get
            {
                return this.title;
            }
        }

        public ImageElement Image
        {
            get
            {
                return this.image;
            }
        }

        #endregion

    }
}
