﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public class TextWithImageLeftTransparentPageType : TextWithImageTransparentBasePageType
    {
        public TextWithImageLeftTransparentPageType()
            : base(PageType.TextWithImageLeftTransparent, "TextWithImageLeftTransparent", "Text with image left transparent")
        { }
    }
}
