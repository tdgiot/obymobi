﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public class TextWithImageRightTransparentPageType : TextWithImageTransparentBasePageType
    {
        public TextWithImageRightTransparentPageType()
            : base(PageType.TextWithImageRightTransparent, "TextWithImageRightTransparent", "Text with image right transparent")
        { }
    }
}
