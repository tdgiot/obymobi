﻿using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Logic.Cms
{
    public class PageLinkPageType : PageTypeBase
    {
        #region Fields

        private PageLinkElement pageLink;
        private MultiLineTextElement text;
        private SingleLineTextElement buttonText;
        private ImageElement image;
        private SingleLineTextElement title;

        #endregion

        #region Constructor & Initialization

        internal PageLinkPageType()
            : base(PageType.PageLink, "pageLink", "Page Link")
        {
        }

        protected override List<SiteType> InitCompatibleSiteTypes()
        {
            List<SiteType> siteTypes = new List<SiteType>();            
            siteTypes.Add(SiteType.Directory);
            siteTypes.Add(SiteType.Microsite);
            return siteTypes;
        }

        protected override List<PageTypeElement> InitPageTypeElements()
        {
            this.title = new SingleLineTextElement("title", "Title", this);
            this.text = new MultiLineTextElement("text", "Text", this);
            this.image = new ImageElement("image", "Image", this, MediaTypeGroup.SiteMobileHeaderImage, MediaTypeGroup.SiteTwoThirdsPageImage, MediaType.SitePageTwoThirds1280x800);
            this.buttonText = new SingleLineTextElement("buttonText", "Button Text", this);
            this.pageLink = new PageLinkElement("pageLinkElement", "Page Link", this);

            return new List<PageTypeElement>() { this.title, this.text, this.image, this.buttonText, this.pageLink };
        }

        #endregion

        #region Methods

        protected override void BindDataSource()
        {
            this.title.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.title.SystemName);
            this.text.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.text.SystemName);
            this.image.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.image.SystemName);
            this.buttonText.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.buttonText.SystemName);
            this.pageLink.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.pageLink.SystemName);

            if (this.title.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.title.SystemName))
                this.title.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.title.SystemName);
            if (this.text.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.text.SystemName))
                this.text.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.text.SystemName);
            if ((this.image.DataSource == null || !this.image.DataSource.HasMedia) && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.image.SystemName))
                this.image.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.image.SystemName);
            if (this.buttonText.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.buttonText.SystemName))
                this.buttonText.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.buttonText.SystemName);
            if (this.pageLink.IsEmpty() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.pageLink.SystemName))
                this.pageLink.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.pageLink.SystemName);
        }

        #endregion

        #region Page Elements

        public SingleLineTextElement Title
        {
            get
            {
                return this.title;
            }
        }

        public MultiLineTextElement Text
        {
            get
            {
                return this.text;
            }
        }

        public ImageElement Image
        {
            get
            {
                return this.image;
            }
        }

        public SingleLineTextElement ButtonText
        {
            get
            {
                return this.buttonText;
            }
        }

        public PageLinkElement PageLink
        {
            get
            {
                return this.pageLink;
            }
        }      

        #endregion
    }
}
