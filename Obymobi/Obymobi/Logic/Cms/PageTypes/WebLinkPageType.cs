﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Logic.Cms
{
    public class WebLinkPageType : PageTypeBase
    {
        #region Fields
        
        private WebViewElement webViewElement;
        private MultiLineTextElement text;
        private SingleLineTextElement buttonText;
        private ImageElement image;
        private SingleLineTextElement title;

        #endregion

        #region Constructor & Initialization

        internal WebLinkPageType()
            : base(PageType.WebLink, "WebLink", "Web Link")
        {
        }

        protected override List<SiteType> InitCompatibleSiteTypes()
        {
            List<SiteType> siteTypes = new List<SiteType>();
            siteTypes.Add(SiteType.Microsite);
            siteTypes.Add(SiteType.Directory);
            return siteTypes;
        }

        protected override List<PageTypeElement> InitPageTypeElements()
        {
            this.title = new SingleLineTextElement("title", "Title", this);            
            this.text = new MultiLineTextElement("text", "Text", this);
            this.image = new ImageElement("image", "Image", this, MediaTypeGroup.SiteMobileHeaderImage, MediaTypeGroup.SiteTwoThirdsPageImage, MediaType.SitePageTwoThirds1280x800);
            this.buttonText = new SingleLineTextElement("buttonText", "Button Text", this);
            this.webViewElement = new WebViewElement("webViewelement", "Url", this);

            return new List<PageTypeElement>() { this.title, this.text, this.image, this.buttonText, this.webViewElement };
        }

        #endregion

        #region Methods

        protected override void BindDataSource()
        {            
            this.title.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.title.SystemName);
            this.text.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.text.SystemName);
            this.image.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.image.SystemName);
            this.buttonText.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.buttonText.SystemName);
            this.webViewElement.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.webViewElement.SystemName);

            if (this.title.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.title.SystemName))
                this.title.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.title.SystemName);
            if (this.text.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.text.SystemName))
                this.text.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.text.SystemName);
            if ((this.image.DataSource == null || !this.image.DataSource.HasMedia) && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.image.SystemName))
                this.image.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.image.SystemName);
            if (this.buttonText.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.buttonText.SystemName))
                this.buttonText.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.buttonText.SystemName);
            if (this.webViewElement.Url.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.webViewElement.SystemName))
                this.webViewElement.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.webViewElement.SystemName);
        }

        #endregion

        #region Page Elements

        public SingleLineTextElement Title
        {
            get
            {
                return this.title;
            }
        }

        public MultiLineTextElement Text
        {
            get
            {
                return this.text;
            }
        }

        public ImageElement Image
        {
            get
            {
                return this.image;
            }
        }

        public SingleLineTextElement ButtonText
        {
            get
            {
                return this.buttonText;
            }
        }

        public WebViewElement Url
        {
            get
            {
                return this.webViewElement;
            }
        }

        #endregion
    }
}
