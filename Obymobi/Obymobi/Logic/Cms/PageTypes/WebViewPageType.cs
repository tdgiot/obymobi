﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Logic.Cms
{
    public class WebViewPageType : PageTypeBase
    {
        #region Fields
        
        private WebViewElement webViewElement;

        #endregion

        #region Constructor & Initialization

        internal WebViewPageType()
            : base(PageType.WebView, "WebView", "Web View")
        {
        }

        protected override List<SiteType> InitCompatibleSiteTypes()
        {
            List<SiteType> siteTypes = new List<SiteType>();
            siteTypes.Add(SiteType.Microsite);
            return siteTypes;
        }

        protected override List<PageTypeElement> InitPageTypeElements()
        {            
            this.webViewElement = new WebViewElement("webViewelement", "Web View", this);

            return new List<PageTypeElement>() { this.webViewElement };
        }

        #endregion

        #region Methods

        protected override void BindDataSource()
        {
            this.webViewElement.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.webViewElement.SystemName);
            
            if (this.webViewElement.Url.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.webViewElement.SystemName))
                this.webViewElement.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.webViewElement.SystemName);
        }

        #endregion

        #region Page Elements

        public WebViewElement WebView
        {
            get
            {
                return this.webViewElement;
            }
        }

        #endregion
    }
}
