﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Logic.Cms
{
    public class YouTubePageType : PageTypeBase
    {
        #region Fields

        private SingleLineTextElement title;
        private YouTubeElement youTubeVideo;        

        #endregion

        #region Constructor & Initialization

        internal YouTubePageType()
            : base(PageType.YouTube, "YouTube", "YouTube")
        {
        }

        protected override List<SiteType> InitCompatibleSiteTypes()
        {
            List<SiteType> siteTypes = new List<SiteType>();
            siteTypes.Add(SiteType.Microsite);
            return siteTypes;
        }

        protected override List<PageTypeElement> InitPageTypeElements()
        {
            this.title = new SingleLineTextElement("title", "Title", this);
            this.youTubeVideo = new YouTubeElement("youTubeVideo", "YouTube Video", this);

            return new List<PageTypeElement>() { this.title, this.youTubeVideo };
        }

        #endregion

        #region Methods

        protected override void BindDataSource()
        {
            this.title.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.title.SystemName); 
            this.youTubeVideo.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.youTubeVideo.SystemName);

            if (this.title.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.title.SystemName))
                this.title.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.title.SystemName);
            if (this.youTubeVideo.VideoId.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.youTubeVideo.SystemName))
                this.youTubeVideo.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.youTubeVideo.SystemName);
        }

        #endregion

        #region Page Elements

        public SingleLineTextElement Title
        {
            get
            {
                return this.title;
            }
        }

        public YouTubeElement Video
        {
            get
            {
                return this.youTubeVideo;
            }
        }

        #endregion
    }
}
