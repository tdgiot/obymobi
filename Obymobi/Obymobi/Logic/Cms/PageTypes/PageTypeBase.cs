﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public abstract class PageTypeBase
    {
        #region Fields

        private IPage dataSourceAsPage = null;
        private IEnumerable<IPageTypeElementData> dataSourceAsPageElements = null; 

        #endregion

        #region Constructor & Initialization

        public PageTypeBase(PageType pageType, string systemName, string name)
        {            
            this.SystemName = systemName;
            this.Name = name;
            this.PageType = pageType;

            this.CompatibleSiteTypes = new ReadOnlyCollection<SiteType>(this.InitCompatibleSiteTypes());
            this.PageTypeElements = new ReadOnlyCollection<PageTypeElement>(this.InitPageTypeElements());                        
        }

        /// <summary>
        /// Get all the SiteTypes with which the PageType is compatible
        /// </summary>
        /// <returns></returns>
        protected abstract List<SiteType> InitCompatibleSiteTypes();

        /// <summary>
        /// In this method the local Page Elements should be initialized and set to 
        /// the private fields which are exposed by properties, all elements should be returned.
        /// </summary>
        /// <returns></returns>
        protected abstract List<PageTypeElement> InitPageTypeElements();

        #endregion
        
        
        #region Methods

        /// <summary>
        /// Is used to bind the just set DataSource to the PageElements for the implementation of the PageType
        /// </summary>
        protected abstract void BindDataSource();      

        #endregion


        #region Properties

        /// <summary>
        /// User friendly Name of the Page Type
        /// </summary>
        public readonly string Name;
        
        /// <summary>
        /// System Name of the PageType used internally
        /// </summary>
        public readonly string SystemName;

        /// <summary>
        /// The PageType of the Page
        /// </summary>
        public readonly PageType PageType;

        /// <summary>
        /// The Site Type with which the Page Type is compatible
        /// </summary>
        public readonly ReadOnlyCollection<SiteType> CompatibleSiteTypes;
        
        /// <summary>
        /// The Page Type Elements for the Page Type
        /// </summary>
        public readonly ReadOnlyCollection<PageTypeElement> PageTypeElements;

        /// <summary>
        /// Can be used to set the datasource of this pageTypeBase
        /// </summary>
        public IPage DataSource
        {
            set
            {
                this.dataSourceAsPage = value;
                this.BindDataSource();
            }
        }

        /// <summary>
        /// The PageElements of the Page DataSource (PageEntity)
        /// </summary>
        public IEnumerable<IPageTypeElementData> PageElementsCollection
        {
            get
            {
                if (this.dataSourceAsPageElements != null)
                    return this.dataSourceAsPageElements;
                else if (this.dataSourceAsPage != null)
                    return this.dataSourceAsPage.IPageElementCollection;                
                else
                    return new List<IPageTypeElementData>();               
            }
            set
            {
                this.dataSourceAsPageElements = value;
                this.BindDataSource();
            }
        }

        /// <summary>
        /// The PageTemplateElements of the PageTemplate DataSource (PageTemplateEntity)
        /// </summary>
        public IEnumerable<IPageTypeElementData> PageTemplateElementsCollection
        {
            get
            {
                if (this.dataSourceAsPage != null && this.dataSourceAsPage.PageTemplate != null)
                    return this.dataSourceAsPage.PageTemplate.IPageElementCollection;
                else
                    return new List<IPageTypeElementData>();
            }
        }

        #endregion
    }
}
