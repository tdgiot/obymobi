﻿using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Logic.Cms
{
    public class VenuePageType : PageTypeBase
    {
        #region Fields

        private VenueElement venue;
        private MultiLineTextElement description;
        private SingleLineTextElement buttonText;

        #endregion

        #region Constructor & Initialization

        internal VenuePageType()
            : base(PageType.Venue, "venue", "Venue Summary")
        {
        }

        protected override List<SiteType> InitCompatibleSiteTypes()
        {
            List<SiteType> siteTypes = new List<SiteType>();            
            siteTypes.Add(SiteType.Directory);
            siteTypes.Add(SiteType.Microsite);
            return siteTypes;
        }

        protected override List<PageTypeElement> InitPageTypeElements()
        {
            this.venue = new VenueElement("venue", "Venue", this);

            this.description = new MultiLineTextElement("text", "Text", this);
            this.description.CmsEditable = false;

            this.buttonText = new SingleLineTextElement("buttonText", "Button Text", this);

            return new List<PageTypeElement>() { this.venue, this.description, this.buttonText };
        }

        #endregion

        #region Methods

        protected override void BindDataSource()
        {            
            this.venue.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.venue.SystemName);
            this.description.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.description.SystemName);
            this.buttonText.DataSource = this.PageElementsCollection.FirstOrDefault(x => x.SystemName == this.buttonText.SystemName);

            if (this.venue.IsEmpty() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.venue.SystemName))
                this.venue.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.venue.SystemName);
            if (this.description.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.description.SystemName))
                this.description.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.description.SystemName);
            if (this.buttonText.Text.IsNullOrWhiteSpace() && this.PageTemplateElementsCollection.Any(x => x.SystemName == this.buttonText.SystemName))
                this.buttonText.DataSource = this.PageTemplateElementsCollection.FirstOrDefault(x => x.SystemName == this.buttonText.SystemName);
        }

        #endregion

        #region Page Elements

        public VenueElement Venue
        {
            get
            {
                return this.venue;
            }
        }

        public string Description
        {
            get
            {
                return this.description.Text;
            }
        }

        public SingleLineTextElement ButtonText
        {
            get
            {
                return this.buttonText;
            }
        }

        #endregion
    }
}
