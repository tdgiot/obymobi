﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public class TextWithImageLeftPageType : TextWithImageBasePageType
    {
        public TextWithImageLeftPageType()
            : base(PageType.TextWithImageLeft, "TextWithImageLeft", "Text with image left")
        { }
    }
}
