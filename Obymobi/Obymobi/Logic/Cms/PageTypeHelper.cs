﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Obymobi.Logic.Cms
{
    public static class PageTypeHelper
    {
        #region Init of PageTypes

        private static readonly ReadOnlyCollection<PageTypeBase> PageTypes = new ReadOnlyCollection<PageTypeBase>(
            new List<PageTypeBase> {
                    new SingleImagePageType(),
                    new TextWithImageLeftPageType(),
                    new TextWithImageRightPageType(),
                    new TextWithImageLeftTransparentPageType(),
                    new TextWithImageRightTransparentPageType(),
                    new WebViewPageType(),
                    new WebLinkPageType(),
                    new MapPageType(),
                    new VenuePageType(),
                    new YouTubePageType(),
                    new PageLinkPageType()
                }
            );

        #endregion

        public static List<PageTypeBase> GetPageTypes(SiteType siteType) => PageTypes.Where(x => x.CompatibleSiteTypes.Contains(siteType)).OrderBy(x => x.Name).ToList();

        public static string GetPageTypeName(PageType pageType) => GetPageTypeInstance(pageType).Name;

        public static PageTypeBase GetPageTypeInstance(PageType pageType)
        {
            PageTypeBase toReturn;
            switch (pageType)
            {
                case PageType.TextWithImageLeft:
                    toReturn = new TextWithImageLeftPageType();
                    break;
                case PageType.TextWithImageRight:
                    toReturn = new TextWithImageRightPageType();
                    break;
                case PageType.TextWithImageLeftTransparent:
                    toReturn = new TextWithImageLeftTransparentPageType();
                    break;
                case PageType.TextWithImageRightTransparent:
                    toReturn = new TextWithImageRightTransparentPageType();
                    break;
                case PageType.SingleImage:
                    toReturn = new SingleImagePageType();
                    break;
                case PageType.Map:
                    toReturn = new MapPageType();
                    break;
                case PageType.WebView:
                    toReturn = new WebViewPageType();
                    break;
                case PageType.YouTube:
                    toReturn = new YouTubePageType();
                    break;
                case PageType.Venue:
                    toReturn = new VenuePageType();
                    break;
                case PageType.WebLink:
                    toReturn = new WebLinkPageType();
                    break;
                case PageType.PageLink:
                    toReturn = new PageLinkPageType();
                    break;
                default:
                    throw new NotImplementedException("PageTypeHelper.GetPageTypeInstance for PageType: " + pageType);                    
            }

            return toReturn;
        }

        public static string GetPageElementName(PageType pageType, string pageElementSystemName)
        {
            var pageTypeInstance = GetPageTypeInstance(pageType);
            var pageElementInstance = pageTypeInstance.PageTypeElements.FirstOrDefault(x => x.SystemName == pageElementSystemName);
            return pageElementInstance.Name;
        }

        public static T GetPageElement<T>(PageType pageType, string pageElementSystemName) where T : PageTypeElement
        {
            var pageTypeInstance = GetPageTypeInstance(pageType);
            var pageElementInstance = pageTypeInstance.PageTypeElements.FirstOrDefault(x => x.SystemName == pageElementSystemName);

            return pageElementInstance is T pageTypeElement 
                ? pageTypeElement 
                : throw new InvalidCastException("Invalid cast, the requested PageTypeElement is of type: " + pageElementInstance.GetType().FullName);
        }
    }
}
