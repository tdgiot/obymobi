﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Enums;

namespace Obymobi.Logic.Cms
{
    public interface IMenuItem
    {
        string ItemId { get; }
        string ParentItemId { get; }
        string ItemType { get; }
        string Name { get; set; }
        int SortOrder { get; set; }
        string TypeName { get; }
        VisibilityType VisibilityType { get; }
        bool IsLinkedToBrand { get; }
        bool IsNew { get; set; }
        bool Save();
        bool Delete();
    }
}
