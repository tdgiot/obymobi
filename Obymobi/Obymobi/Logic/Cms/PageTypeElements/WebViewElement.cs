﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public class WebViewElement : PageTypeElement
    {
        public WebViewElement(string systemName, string name, PageTypeBase page)
            : base(PageElementType.WebView, systemName, name, page, true)
        { }

        public string Url
        {
            get
            {
                if (this.DataSource == null)
                    return string.Empty;
                else
                    return this.DataSource.StringValue1;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.StringValue1 = value;
            }
        }
    }
}
