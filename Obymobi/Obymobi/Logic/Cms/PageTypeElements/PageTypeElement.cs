﻿namespace Obymobi.Logic.Cms
{
    public abstract class PageTypeElement
    {
        protected PageTypeElement(PageElementType pageTypeElementType, string systemName, string name, PageTypeBase page, bool languageAgnosticOnDefault = false)
        {
            this.PageTypeElementType = pageTypeElementType;
            this.SystemName = systemName;
            this.Name = name;
            this.LanguageAgnosticOnDefault = languageAgnosticOnDefault;
            this.Page = page;

            this.CmsEditable = true;
        }

        public bool CmsEditable { get; set; }

        public string Name { get; protected set; }
        public string SystemName { get; protected set; }
        public bool LanguageAgnosticOnDefault { get; protected set; }
        public PageElementType PageTypeElementType { get; protected set; }
        public PageTypeBase Page { get; protected set; }

        public bool SaveDataSource()
        {
            return this.DataSource.Save();
        }

        /// <summary>
        /// This should only be used in very rare cases.
        /// </summary>        
        public IPageTypeElementData DataSource;        
    }
}
