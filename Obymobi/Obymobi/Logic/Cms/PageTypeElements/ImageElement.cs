﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public class ImageElement : PageTypeElement
    {
        public ImageElement(string systemName, string name, PageTypeBase page, MediaTypeGroup mediaTypeGroupPhone, MediaTypeGroup mediaTypeGroupTablet, params MediaType[] mediaTypes)
            : base(PageElementType.Image, systemName, name, page, true)
        {
            this.MediaTypeGroupPhone = mediaTypeGroupPhone;
            this.MediaTypeGroupTablet = mediaTypeGroupTablet;
            this.MediaTypes = mediaTypes;
        }

        public readonly MediaTypeGroup MediaTypeGroupPhone;
        public readonly MediaTypeGroup MediaTypeGroupTablet;
        public readonly MediaType[] MediaTypes;

        public string MediaUrl { get; set; }

        public List<MediaTypeGroup> GetMediaTypeGroups()
        {
            List<MediaTypeGroup> toReturn = new List<MediaTypeGroup>();
            toReturn.Add(this.MediaTypeGroupPhone);
            if(this.MediaTypeGroupPhone != this.MediaTypeGroupTablet)
                toReturn.Add(this.MediaTypeGroupTablet);
            return toReturn;
        }

        public List<MediaType> GetMediaTypes()
        {
            List<MediaType> mediaTypes = new List<MediaType>();
            if (this.MediaTypes != null)
            {
                mediaTypes.AddRange(this.MediaTypes);
            }
            return mediaTypes;
        }
    }
}
