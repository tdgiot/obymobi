﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public class PageLinkElement : PageTypeElement
    {
        public PageLinkElement(string systemName, string name, PageTypeBase page)
            : base(PageElementType.PageLink, systemName, name, page, true)
        {            
        }

        public int PageId
        {
            get
            {
                if (this.DataSource == null)
                    return 0;
                else
                    return this.DataSource.IntValue1 ?? 0;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.IntValue1 = value;
            }
        }

        public int PageTemplateId
        {
            get
            {
                if (this.DataSource == null)
                    return 0;
                else
                    return this.DataSource.IntValue1 ?? 0;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.IntValue1 = value;
            }
        }

        public bool IsEmpty()
        {
            return (this.PageId <= 0);
        }     
    }
}
