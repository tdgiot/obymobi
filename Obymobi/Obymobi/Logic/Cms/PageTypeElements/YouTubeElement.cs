﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public class YouTubeElement : PageTypeElement
    {
        public YouTubeElement(string systemName, string name, PageTypeBase page)
            : base(PageElementType.YouTube, systemName, name, page, true)
        {            
        }

        public string VideoId
        {
            get
            {
                if (this.DataSource == null)
                    return string.Empty;
                else
                    return this.DataSource.StringValue1;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.StringValue1 = value;
            }
        }

        public bool AutoPlay
        {
            get
            {
                if (this.DataSource == null)
                    return true;
                else
                    return this.DataSource.BoolValue1 ?? true;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.BoolValue1 = value;
            }
        }

        public bool AutoHide
        {
            get
            {
                if (this.DataSource == null)
                    return false;
                else
                    return this.DataSource.BoolValue2 ?? true;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.BoolValue2 = value;
            }
        }

        public bool Loop
        {
            get
            {
                if (this.DataSource == null)
                    return false;
                else
                    return this.DataSource.BoolValue3 ?? false;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.BoolValue3 = value;
            }
        }

        public bool ClosedCaptions
        {
            get
            {
                if (this.DataSource == null)
                    return false;
                else
                    return this.DataSource.BoolValue4 ?? false;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.BoolValue4 = value;
            }
        }
    }
}
