﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Logic.Cms
{
    public class MapElement : PageTypeElement
    {
        public MapElement(string systemName, string name, PageTypeBase page)
            : base(PageElementType.Map, systemName, name, page, true)
        {            
        }

        public string MarkerPosition
        {
            get
            {
                if (this.DataSource == null)
                    return string.Empty;
                else
                    return this.DataSource.StringValue1;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.StringValue1 = value;
            }
        }

        public string MarkerLabel
        {
            get
            {
                if (this.DataSource == null)
                    return string.Empty;
                else
                    return this.DataSource.StringValue2;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.StringValue2 = value;
            }
        }

        public string MarkerText
        {
            get
            {
                if (this.DataSource == null)
                    return string.Empty;
                else
                    return this.DataSource.StringValue3 ?? string.Empty;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.StringValue3 = value;
            }
        }

        public int ZoomLevel
        {
            get
            {
                if (this.DataSource == null)
                    return 8;
                else
                    return this.DataSource.IntValue1 ?? 8;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.IntValue1 = value;
            }
        }

        public bool IsEmpty()
        {
            return this.MarkerPosition.IsNullOrWhiteSpace();
        }
    }
}
