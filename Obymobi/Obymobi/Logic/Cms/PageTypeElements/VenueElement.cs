﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public class VenueElement : PageTypeElement
    {
        public VenueElement(string systemName, string name, PageTypeBase page)
            : base(PageElementType.Venue, systemName, name, page, true)
        {            
        }

        public int CompanyId
        {
            get
            {
                if (this.DataSource == null)
                    return 0;
                else
                    return this.DataSource.IntValue1 ?? 0;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.IntValue1 = value;
            }
        }

        public int PointOfInterestId
        {
            get
            {
                if (this.DataSource == null)
                    return 0;
                else
                    return this.DataSource.IntValue2 ?? 0;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.IntValue2 = value;
            }
        }

        /// <summary>
        /// Used for the HTML renderer and IRT, as it can't open Companies/POIs
        /// </summary>
        public int FallBackSiteId
        {
            get
            {
                if (this.DataSource == null)
                    return 0;
                else
                    return this.DataSource.IntValue3 ?? 0;
            }
            set
            {
                if (this.DataSource == null)
                    throw new NullReferenceException("DataSource is null");

                this.DataSource.IntValue3 = value;
            }
        }

        public bool IsEmpty()
        {
            return (this.CompanyId <= 0 && this.PointOfInterestId <= 0);
        }     
    }
}
