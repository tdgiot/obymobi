﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public enum SiteType : int
    {
        Unknown = 0,
        Microsite = 1,
        Directory = 2,
        //Hotel = 3,
        //SinglePage = 4,
    }
}
