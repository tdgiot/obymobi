﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{
    public class PageTypeElementData : IPageTypeElementData
    {
        public string SystemName { get; set; }

        public string StringValue1 { get; set; }
        public string StringValue2 { get; set; }
        public string StringValue3 { get; set; }
        public string StringValue4 { get; set; }
        public string StringValue5 { get; set; }

        public bool? BoolValue1 { get; set; }
        public bool? BoolValue2 { get; set; }
        public bool? BoolValue3 { get; set; }
        public bool? BoolValue4 { get; set; }
        public bool? BoolValue5 { get; set; }

        public int? IntValue1 { get; set; }
        public int? IntValue2 { get; set; }
        public int? IntValue3 { get; set; }
        public int? IntValue4 { get; set; }
        public int? IntValue5 { get; set; }
        
        public string CultureCode { get; set; }
        public int PrimaryId { get; set; }
        public bool HasMedia { get; set; }

        public bool Save()
        {
            // No implementation needed, is only for CMS.
            return true;
        }
    }
}
