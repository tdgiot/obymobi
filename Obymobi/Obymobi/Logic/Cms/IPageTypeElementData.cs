﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Cms
{    
    public interface IPageTypeElementData
    {
        string SystemName { get; set; }

        string StringValue1 { get; set; }
        string StringValue2 { get; set; }
        string StringValue3 { get; set; }
        string StringValue4 { get; set; }
        string StringValue5 { get; set; }

        bool? BoolValue1 { get; set; }
        bool? BoolValue2 { get; set; }
        bool? BoolValue3 { get; set; }
        bool? BoolValue4 { get; set; }
        bool? BoolValue5 { get; set; }

        int? IntValue1 { get; set; }
        int? IntValue2 { get; set; }
        int? IntValue3 { get; set; }
        int? IntValue4 { get; set; }
        int? IntValue5 { get; set; }
        
        string CultureCode { get; set; }
        int PrimaryId { get; set; }
        bool HasMedia { get; set; }

        /// <summary>
        /// Used for the CMS, can be non implemented at other places.
        /// </summary>
        /// <returns></returns>
        bool Save();        
    }
}
