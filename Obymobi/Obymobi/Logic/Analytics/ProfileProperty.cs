﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public class ProfileProperty<T> : EventProperty<T>
    {
        public ProfileProperty(ProfilePropertyType<T> propertyType, T value, string identifier = null)
            : base(propertyType.Name, value, propertyType.ReportToProfileToConnectors, propertyType.ReportWithEventToConnectors, identifier)
        { 
        }        
    }
}
