﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    // GK Design choice: Not an enum, because we need to report string values to every 
    // connector and might want to include spaces + we need additional properties.
    public static class EventTypes
    {
        /// IMPORTANT: If you add anything to this class you also need to update the related documentation: 
        /// "O:\Software Development\Documentation\Functional specification\20140223 - Analytics Reporting\Analytics Events.docx"

        #region Actionable Events

        private static readonly ReadOnlyCollection<string> all = new ReadOnlyCollection<string>(new List<string> { GoogleAnalyticsConnector.ClassNameStatic, MixPanelConnector.ClassNameStatic });
        private static readonly ReadOnlyCollection<string> googleOnly = new ReadOnlyCollection<string>(new List<string> { GoogleAnalyticsConnector.ClassNameStatic });        

        // Acquisation Events
        public static readonly EventType RequestedDownloadLinkSMS = new EventType(EventCategory.Aquisition, "Requested download link SMS", all);
        public static readonly EventType VisitedMobileLandingPage = new EventType(EventCategory.Aquisition, "Visited mobile landing page", all);
        public static readonly EventType WentToAppStore = new EventType(EventCategory.Aquisition, "Went to App Store", all);

        // Activation
        public static readonly EventType FirstLaunchOfApplication = new EventType(EventCategory.Activation, "First launch of application", all);
        public static readonly EventType ApplicationGotFocus = new EventType(EventCategory.Activation, "Application got focus", all);
        public static readonly EventType ApplicationLostFocus = new EventType(EventCategory.Activation, "Application lost focus", all);
        
        // Ordering of Products
        public static readonly EventType AddedFirstProductToBasket = new EventType(EventCategory.Transactions, "Added first product to basket", all);
        public static readonly EventType AddedProductToBasket = new EventType(EventCategory.Transactions, "Added product to basket", all);
        public static readonly EventType StartedCheckout = new EventType(EventCategory.Transactions, "Started checkout", all);
        public static readonly EventType CompletedCheckout = new EventType(EventCategory.Transactions, "Completed checkout", all);

        // Service Request
        public static readonly EventType StartedServiceRequest = new EventType(EventCategory.Transactions, "Started service request", all);
        public static readonly EventType CompletedServiceRequest = new EventType(EventCategory.Transactions, "Completed Service request", all);

        // Advertising & promoition
        public static readonly EventType AdvertisementClicked = new EventType(EventCategory.Advertising, "Advertisement clicked", all);
        public static readonly EventType AdvertisementViewed = new EventType(EventCategory.Advertising, "Advertisement viewed", googleOnly);
        public static readonly EventType MessageClicked = new EventType(EventCategory.Advertising, "Message clicked", all);
        public static readonly EventType MessageViewed = new EventType(EventCategory.Advertising, "Message viewed", googleOnly);

        #endregion

        #region Vanity & Explorative Events

        // Vanity Metrics
        public static readonly EventType ProductViewed = new EventType(EventCategory.Transactions, "Product Viewed", googleOnly);
        public static readonly EventType CategoryViewed = new EventType(EventCategory.Transactions, "Category Viewed", googleOnly);
        public static readonly EventType ProductOrdered = new EventType(EventCategory.Transactions, "Product Ordered", googleOnly);        
        public static readonly EventType SaveOrderFailed = new EventType(EventCategory.Transactions, "Save order failed", all);
        public static readonly EventType SaveServiceRequestFailed = new EventType(EventCategory.Transactions, "Save service request failed", all);

        public static readonly EventType NativeScreenView = new EventType(EventCategory.ScreenView, "Native view", googleOnly);        
        public static readonly EventType CompanyScreenView = new EventType(EventCategory.ScreenView, "Company view", googleOnly);
        public static readonly EventType CompanyScreenLeft = new EventType(EventCategory.ScreenView, "Company view left", all);
        public static readonly EventType PointOfInterestScreenView = new EventType(EventCategory.ScreenView, "POI view", googleOnly);
		public static readonly EventType PointOfInterestScreenLeft = new EventType(EventCategory.ScreenView, "POI view left", all);
        public static readonly EventType NativePageView = new EventType(EventCategory.ScreenView, "Native Page view", googleOnly);
        public static readonly EventType EntertainmentView = new EventType(EventCategory.ScreenView, "Entertainment view", googleOnly);
        public static readonly EventType BrowserScreenView = new EventType(EventCategory.ScreenView, "Browser view", googleOnly);

        public static readonly EventType CompanySelected = new EventType(EventCategory.Exploratory, "Company selected", googleOnly);
        public static readonly EventType PointOfInterestSelected = new EventType(EventCategory.Exploratory, "POI selected", googleOnly);

        public static readonly EventType LanguageChanged = new EventType(EventCategory.Transactions, "Language Changed", googleOnly);
	    public static readonly EventType AttachmentViewed = new EventType(EventCategory.Transactions, "Attachment Viewed", googleOnly);
        public static readonly EventType DocumentPrinted = new EventType(EventCategory.Transactions, "Document Printed", googleOnly);

	    public static readonly EventType BluetoothConnected = new EventType(EventCategory.Connectivity, "Bluetooth Connected", googleOnly);
	    public static readonly EventType BluetoothDisconnected = new EventType(EventCategory.Connectivity, "Bluetooth Disconnected", googleOnly);

        #endregion

        /// IMPORTANT: If you add anything to this class you also need to update the related documentation: 
        /// "O:\Software Development\Documentation\Functional specification\20140223 - Analytics Reporting\Analytics Events.docx"
    }
}
