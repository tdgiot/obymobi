﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dionysos;
using System.Collections.ObjectModel;
using Obymobi.Enums;

namespace Obymobi.Logic.Analytics
{
    public class AnalyticsService : IDisposable
    {
        public enum AnalyticsServiceResult
        {
            NoIdentifierSpecified = 200
        }

        // private static MixPanelConnector MetricsConnector = new MixPanelConnector();        
        private readonly ReadOnlyCollection<AnalyticsConnector> analyticsConnectors;
        private bool useBatches;
        private EventPropertyCollection defaultEventProperties;
        private object defaultEventPropertiesLock;


        #region Constructor
                
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectors">Connectors to which to submit the Events/Properties</param>
        /// <param name="useBatches">If set to true the Events/Properties will be submitted in batches (useful for mobile devices)</param>
        public AnalyticsService(List<AnalyticsConnector> connectors, ApplicationType applicationType, string applicationName, string applicationVersion, OperatingSystem operatingSystem,
            string operatingSystemVersion, bool tablet, string deviceModel, Tuple<int, int> resolutionWidthHeight, bool useBatches)
        {
            this.analyticsConnectors = new ReadOnlyCollection<AnalyticsConnector>(connectors);
            this.useBatches = useBatches;
            this.defaultEventPropertiesLock = new object();

            // Setup the event properties that in principle won't change during the application / instance life time.            
            lock (this.defaultEventPropertiesLock)
            {
                this.defaultEventProperties = new EventPropertyCollection();
                this.defaultEventProperties.Add(EventPropertyTypes.ApplicationType, applicationType);
                this.defaultEventProperties.Add(EventPropertyTypes.ApplicationName, applicationName);
                this.defaultEventProperties.Add(EventPropertyTypes.ApplicationVersion, applicationVersion);
                this.defaultEventProperties.Add(EventPropertyTypes.OperatingSystem, operatingSystem);
                this.defaultEventProperties.Add(EventPropertyTypes.OperatingSystemVersion, operatingSystemVersion);
                this.defaultEventProperties.Add(EventPropertyTypes.Tablet, tablet);
                this.defaultEventProperties.Add(EventPropertyTypes.Model, deviceModel);
                this.defaultEventProperties.Add(EventPropertyTypes.ResolutionWidthHeight, resolutionWidthHeight);
            }
            
            // Start processor
            Task.Factory.StartNew(() => this.ProcessQueue());            
        }

        #endregion

        #region Tracking Methods

        // GK PILS This code will need a refactoring, all those string (although at the moment only used in 1 place, so safe) are not good.
        // Also I need some mechanism to determine which property is used as the Event Label for Google Analytics
        // deciced, due to time restrictions and to first see how this code will work for us in real life
        // to do later.

        // GK Back again, it could be so much 'nicer' but the thing works and I still haven't figured out what would make it much better
        // and am a bit afraid I might over-engineer this with all the generic implementations due to two statistics connectors and being
        // prepared to move to another one as well in the future. ... 
        // Ideas:
        //- Zowel MixPanel connector als de Event Queue logic voegen properties toe aan events, definieren per Connector
        //- Nagaan dat the PlatformProvider te benodige properties kan ophalen
        //- Nagaan wat er gebeurd als er proprties worden gezet zonder identifier
        //- Event Properties maken ipv non-typed sheit
        //- Settings per ENV in ObymobiConstants, niet in code (trackers ids, e.d.)
        //- Ensure Identifier is UUID, not email
        //- Hoe te matchen Hotel Naam / SMS bericht
        //- Typed properties ipv key/value pairs
        //- Which events are  handled by API
        //- SetDeviceConfiguration, when launched, maybe request using PlatformProvider to ensure when and how for analytics

  // GAKR: I commented this all out as it didn't seem to be used anywhere and Obymobi.sln still compiled after this was removed.
        
  //      // GK Design choice: Not create all different event classes because this is the only place
  //      // where this properties and their names are defined. So need need for that.
  //      public void RequestedDownloadLinkSms(int companyId, string companyName, string campaignSource, string campaignName, string campaignMedium, string campaignContent, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.RequestedDownloadLinkSMS, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);            
  //          e.Properties.Add(ProfilePropertyTypes.CampaignName, campaignName);
  //          e.Properties.Add(ProfilePropertyTypes.CampaignSource, campaignSource);
  //          e.Properties.Add(ProfilePropertyTypes.CampaignMedium, campaignMedium);
  //          e.Properties.Add(ProfilePropertyTypes.CampaignContent, campaignContent);
  //          e.Label = "{0} | {1} | {2} | {3}".FormatSafe(campaignName, campaignSource, campaignMedium, campaignContent);
  //          this.QueueEvent(e);
  //      }

  //      public void VisitedMobileLandingPage(int companyId, string companyName, string campaignSource, string campaignName, string campaignMedium, string campaignContent, OperatingSystem operatingSystem, string identifier = null, DateTime? dateTime = null)
  //      {                            
  //          Event e = new Event(EventTypes.VisitedMobileLandingPage, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(ProfilePropertyTypes.CampaignName, campaignName);
  //          e.Properties.Add(ProfilePropertyTypes.CampaignSource, campaignSource);
  //          e.Properties.Add(ProfilePropertyTypes.CampaignMedium, campaignMedium);
  //          e.Properties.Add(ProfilePropertyTypes.CampaignContent, campaignContent);
  //          e.Properties.Add(EventPropertyTypes.OperatingSystem, operatingSystem);
  //          e.Label = "{0} | {1} | {2}".FormatSafe(campaignName, campaignSource, campaignMedium);
  //          this.QueueEvent(e);                
  //      }

  //      public void WentToAppStore(int companyId, string companyName, string campaignSource, string campaignName, string campaignMedium, string campaignContent, OperatingSystem operatingSystem, AppStore appStore, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.WentToAppStore, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);            
  //          e.Properties.Add(EventPropertyTypes.AppStore, appStore);
  //          e.Properties.Add(ProfilePropertyTypes.CampaignName, campaignName);
  //          e.Properties.Add(ProfilePropertyTypes.CampaignSource, campaignSource);
  //          e.Properties.Add(ProfilePropertyTypes.CampaignMedium, campaignMedium);
  //          e.Properties.Add(ProfilePropertyTypes.CampaignContent, campaignContent);
  //          e.Properties.Add(EventPropertyTypes.OperatingSystem, operatingSystem);
  //          e.Label = "{0} | {1} | {2} | {3}".FormatSafe(campaignName, campaignSource, campaignMedium, appStore.ToString());
  //          this.QueueEvent(e);
  //      }

  //      public void FirstLaunchOfApplication(string identifier = null, DateTime? dateTime = null)
  //      {
		//	// Queue will be flushed after this event to make sure we get the event before the user closes the App
  //          // GK It's mandatory to flush on exit of the app, so no need. - To ensure a manual flush should be called after the event was added.
		//	Event e = new Event(EventTypes.FirstLaunchOfApplication, identifier, dateTime);
  //          e.Label = this.GetApplicationTypeNameVersionLabel();            
  //          this.QueueEvent(e);
  //      }

  //      public void ApplicationGotFocus(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, string version, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.ApplicationGotFocus, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Label = this.GetApplicationTypeNameVersionLabel();            
  //          this.QueueEvent(e);
  //      }

  //      public void ApplicationLostFocus(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int secondsOfFocus, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.ApplicationLostFocus, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.Seconds, secondsOfFocus);
  //          e.Label = this.GetApplicationTypeNameVersionLabel();            
  //          e.Value = secondsOfFocus;
  //          this.QueueEvent(e);
  //      }

  //      public void AddedFirstProductToBasket(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int categoryId, string categoryName, int productId, string productName, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.AddedFirstProductToBasket, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.ProductId, productId);
  //          e.Properties.Add(EventPropertyTypes.ProductName, productName);
		//	e.Properties.Add(EventPropertyTypes.CategoryId, categoryId);
		//	e.Properties.Add(EventPropertyTypes.CategoryName, categoryName);
		//	e.Label = "{0}/{1}".FormatSafe(categoryName, productName);
  //          this.QueueEvent(e);
  //      }

  //      public void AddedProductToBasket(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int categoryId, string categoryName, int productId, string productName, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.AddedProductToBasket, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.ProductId, productId);
  //          e.Properties.Add(EventPropertyTypes.ProductName, productName);
		//	e.Properties.Add(EventPropertyTypes.CategoryId, categoryId);
		//	e.Properties.Add(EventPropertyTypes.CategoryName, categoryName);
		//	e.Label = "{0}/{1}".FormatSafe(categoryName, productName);
  //          this.QueueEvent(e);
  //      }

  //      public void StartedCheckout(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.StartedCheckout, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Label = "{0}/{1}".FormatSafe(companyName, deliverypointgroupName);
  //          this.QueueEvent(e);
  //      }

  //      public void SaveOrderFailed(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, string errorCode, string errorText, decimal orderValue, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.SaveOrderFailed, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Label = errorCode;
  //          e.Value = Convert.ToInt32(Math.Round(orderValue));
  //          e.Properties.Add(EventPropertyTypes.OrderValue, orderValue);
  //          e.Properties.Add(EventPropertyTypes.ErrorCode, errorCode);
  //          e.Properties.Add(EventPropertyTypes.ErrorText, errorText);            
  //          this.QueueEvent(e);
  //      }

  //      public void CompletedCheckout(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, decimal orderValue, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.CompletedCheckout, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.OrderValue, orderValue);
  //          e.Label = "{0}/{1}".FormatSafe(companyName, deliverypointgroupName);
		//	e.Value = Convert.ToInt32(Math.Round(orderValue));
  //          this.QueueEvent(e);
  //      }

  //      public void StartedServiceRequest(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int categoryId, string categoryName, int productId, string productName, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.StartedServiceRequest, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.ProductId, productId);
  //          e.Properties.Add(EventPropertyTypes.ProductName, productName);
		//	e.Properties.Add(EventPropertyTypes.CategoryId, categoryId);
		//	e.Properties.Add(EventPropertyTypes.CategoryName, categoryName);
  //          e.Label = "{0}/{1}".FormatSafe(categoryName, productName);
  //          this.QueueEvent(e);
  //      }

  //      public void SaveServiceRequestFailed(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int productId, string productName, string errorCode, string errorText, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.SaveServiceRequestFailed, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.ErrorCode, errorCode);
  //          e.Properties.Add(EventPropertyTypes.ErrorText, errorText);
  //          e.Label = errorCode;
  //          this.QueueEvent(e);
  //      }

  //      public void CompletedServiceRequest(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int productId, string productName, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.CompletedServiceRequest, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.ProductId, productId);
  //          e.Properties.Add(EventPropertyTypes.ProductName, productName);
		//	e.Label = productName;
  //          this.QueueEvent(e);
  //      }

  //      public void AdvertisementClicked(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, string advertisementLocation, string advertisementId, string advertisementReference, string advertisementAction, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.AdvertisementClicked, identifier, dateTime);
  //          e.Type.SetName("{0}/{1}".FormatSafe(advertisementLocation, advertisementReference));

  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.AdvertisementId, advertisementId);
  //          e.Properties.Add(EventPropertyTypes.AdvertisementLocation, advertisementLocation);
  //          e.Properties.Add(EventPropertyTypes.AdvertisementReference, advertisementReference);
  //          e.Properties.Add(EventPropertyTypes.AdvertisementTarget, advertisementAction);
  //          e.Label = "Clicked";
  //          this.QueueEvent(e);
  //      }

  //      public void AdvertisementViewed(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, string advertisementLocation, string advertisementId, string advertisementReference, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.AdvertisementViewed, identifier, dateTime);
  //          e.Type.SetName("{0}/{1}".FormatSafe(advertisementLocation, advertisementReference));

  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.AdvertisementId, advertisementId);
  //          e.Properties.Add(EventPropertyTypes.AdvertisementLocation, advertisementLocation);
  //          e.Properties.Add(EventPropertyTypes.AdvertisementReference, advertisementReference);
  //          e.Label = "Viewed";
  //          this.QueueEvent(e);
  //      }

  //      public void MessageClicked(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int messageId, string messageTitle, string messageResponse, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.AdvertisementClicked, identifier, dateTime);
  //          e.Type.SetName(messageTitle);

  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.MessageId, messageId);
  //          e.Properties.Add(EventPropertyTypes.MessageTitle, messageTitle);
  //          e.Properties.Add(EventPropertyTypes.MessageResponse, messageResponse);
  //          e.Label = messageResponse;
  //          this.QueueEvent(e);
  //      }

  //      public void MessageViewed(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int messageId, string messageTitle, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.AdvertisementClicked, identifier, dateTime);
  //          e.Type.SetName(messageTitle);

  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.MessageId, messageId);
  //          e.Properties.Add(EventPropertyTypes.MessageTitle, messageTitle);
  //          e.Label = "Viewed";
  //          this.QueueEvent(e);
  //      }

  //      public void ProductOrdered(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int categoryId, string categoryName, int productId, string productName, int quantity, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.ProductOrdered, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.CategoryId, categoryId);
  //          e.Properties.Add(EventPropertyTypes.CategoryName, categoryName);
  //          e.Properties.Add(EventPropertyTypes.ProductId, productId);
  //          e.Properties.Add(EventPropertyTypes.ProductName, productName);
  //          e.Properties.Add(EventPropertyTypes.Quantity, quantity);
  //          e.Label = productName;
  //          e.Value = quantity;
  //          this.QueueEvent(e);
  //      }

  //      public enum NativeScreen
  //      { 
  //          CompanyCarousel,
  //          Map
  //      }

  //      // Screen Views
  //      public void NativeScreenViewed(NativeScreen screen, NavigationSource source, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.NativeScreenView, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.NavigationSource, source.ToString());
  //          e.ScreenViewPath = "/" + screen.ToString();
  //          this.QueueEvent(e);
  //      }

  //      public void CategoryViewed(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int categoryId, string categoryName, NavigationSource source, string identifier = null, DateTime? dateTime = null)
  //      {
		//	Event e = new Event(EventTypes.CategoryViewed, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);            
  //          e.Properties.Add(EventPropertyTypes.CategoryId, categoryId);
  //          e.Properties.Add(EventPropertyTypes.CategoryName, categoryName);
  //          e.Properties.Add(EventPropertyTypes.NavigationSource, source.ToString());
		//	e.Label = categoryName;
		//	e.ScreenViewPath = "/" + companyName + "/" + e.Label;
  //          this.QueueEvent(e);
  //      }

  //      public void ProductViewed(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int categoryId, string categoryName, int productId, string productName, NavigationSource source, string identifier = null, DateTime? dateTime = null)
  //      {
		//	Event e = new Event(EventTypes.ProductViewed, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.CategoryId, categoryId);
  //          e.Properties.Add(EventPropertyTypes.CategoryName, categoryName);
  //          e.Properties.Add(EventPropertyTypes.ProductId, productId);
  //          e.Properties.Add(EventPropertyTypes.ProductName, productName);
  //          e.Properties.Add(EventPropertyTypes.NavigationSource, source.ToString());
  //          e.Label = string.Format("{0}/{1}", categoryName, productName);
		//	e.ScreenViewPath = "/" + companyName + "/" + e.Label;
  //          this.QueueEvent(e);
  //      }

  //      public void EntertainmentViewed(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int entertainmentId, string entertainmentName, NavigationSource source, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.CategoryViewed, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.EntertainmentId, entertainmentId);
  //          e.Properties.Add(EventPropertyTypes.EntertainmentName, entertainmentName);
  //          e.Properties.Add(EventPropertyTypes.NavigationSource, source.ToString());
  //          e.Label = entertainmentName;
		//	e.ScreenViewPath = "/" + companyName + "/" + e.Label;
  //          this.QueueEvent(e);
  //      }

  //      public void NativePageViewed(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int siteId, string siteName, int pageId, string pageName, NavigationSource source, string identifier = null, DateTime? dateTime = null)
  //      {
		//	Event e = new Event(EventTypes.NativePageView, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.SiteId, siteId);
  //          e.Properties.Add(EventPropertyTypes.SiteName, siteName);
  //          e.Properties.Add(EventPropertyTypes.PageId, pageId);
  //          e.Properties.Add(EventPropertyTypes.PageName, pageName);
  //          e.Properties.Add(EventPropertyTypes.NavigationSource, source.ToString());            
  //          e.Label = string.Format("/{0}/{1}", siteName, pageName);
		//	e.ScreenViewPath = "/" + companyName + "/" + e.Label;
  //          this.QueueEvent(e);
  //      }

  //      public void MicrositeViewed(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, string url, NavigationSource source, string identifier = null, DateTime? dateTime = null)
		//{ 							   
		//	if(url.StartsWith("http://", StringComparison.InvariantCultureIgnoreCase))
		//		url = url.Substring(7);
		//	if(url.StartsWith("https://", StringComparison.InvariantCultureIgnoreCase))
		//		url = url.Substring(8);

  //          Event e = new Event(EventTypes.BrowserScreenView, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Properties.Add(EventPropertyTypes.NavigationSource, source.ToString());
  //          e.Label = url;
		//	e.ScreenViewPath = "/" + companyName + string.Format("/web/{0}/", url);
  //          this.QueueEvent(e);
  //      }

  //      public void PointOfInterestPageViewed(int pointOfInterestId, string pointOfInterestName, int tabId, string tabName, NavigationSource source, string identifier = null, DateTime? dateTime = null)
  //      {
		//	Event e = new Event(EventTypes.PointOfInterestScreenView, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.PointOfInterestId, pointOfInterestId);
  //          e.Properties.Add(EventPropertyTypes.PointOfInterestName, pointOfInterestName);
  //          e.Properties.Add(EventPropertyTypes.TabId, tabId);
  //          e.Properties.Add(EventPropertyTypes.TabName, tabName);
  //          e.Properties.Add(EventPropertyTypes.NavigationSource, source.ToString());
  //          e.Label = tabName;
		//	e.ScreenViewPath = "/" + pointOfInterestName + "/" + e.Label;
  //          this.QueueEvent(e);
  //      }

		//public void PointOfInterestPageLeft(int pointOfInterestId, string pointOfInterestName, int tabId, string tabName, int seconds, NavigationSource source, string identifier = null, DateTime? dateTime = null)
  //      {
		//	Event e = new Event(EventTypes.PointOfInterestScreenLeft, identifier, dateTime);
		//	e.Properties.Add(EventPropertyTypes.PointOfInterestId, pointOfInterestId);
		//	e.Properties.Add(EventPropertyTypes.PointOfInterestName, pointOfInterestName);
  //          e.Properties.Add(EventPropertyTypes.TabId, tabId);
  //          e.Properties.Add(EventPropertyTypes.TabName, tabName);
  //          e.Properties.Add(EventPropertyTypes.Seconds, seconds);
  //          e.Properties.Add(EventPropertyTypes.NavigationSource, source.ToString());
  //          e.Label = tabName;
		//	e.ScreenViewPath = "/" + pointOfInterestName + "/" + e.Label;
  //          e.Value = seconds;
  //          this.QueueEvent(e);
  //      }

  //      public void CompanyPageViewed(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int tabId, string tabName, NavigationSource source, string identifier = null, DateTime? dateTime = null)
		//{
		//	Event e = new Event(EventTypes.CompanyScreenView, identifier, dateTime);
		//	e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
		//	e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
		//	e.Properties.Add(EventPropertyTypes.TabId, tabId);
		//	e.Properties.Add(EventPropertyTypes.TabName, tabName);
  //          e.Properties.Add(EventPropertyTypes.NavigationSource, source.ToString());
  //          e.Label = tabName;
		//	e.ScreenViewPath = "/" + companyName + "/" + e.Label;
		//	this.QueueEvent(e);
		//}

  //      public void CompanyPageLeft(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, int tabId, string tabName, int seconds, NavigationSource source, string identifier = null, DateTime? dateTime = null)
		//{
		//	Event e = new Event(EventTypes.CompanyScreenLeft, identifier, dateTime);
		//	e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
		//	e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
		//	e.Properties.Add(EventPropertyTypes.TabId, tabId);
		//	e.Properties.Add(EventPropertyTypes.TabName, tabName);
		//	e.Properties.Add(EventPropertyTypes.Seconds, seconds);
  //          e.Properties.Add(EventPropertyTypes.NavigationSource, source.ToString());
  //          e.Label = tabName;
		//	e.ScreenViewPath = "/" + companyName + "/" + e.Label;
		//	e.Value = seconds;
		//	this.QueueEvent(e);
		//}

  //      public void CompanySelected(int companyId, string companyName, string selectionSource, NavigationSource navigationSource, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.CompanySelected, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.NavigationSource, navigationSource.ToString());
  //          e.Label = selectionSource.ToString();
  //          this.QueueEvent(e);
  //      }

  //      public void PointOfInterestSelected(int companyId, string companyName, string selectionSource, NavigationSource navigationSource, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.PointOfInterestSelected, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.NavigationSource, navigationSource.ToString());
  //          e.Label = selectionSource.ToString();
  //          this.QueueEvent(e);
  //      }

  //      public void LanguageChanged(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, string languageCode, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.LanguageChanged, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Label = languageCode;
  //          this.QueueEvent(e);
  //      }

  //      public void BluetoothConnected(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, string deviceName, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.BluetoothConnected, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Label = deviceName;
  //          this.QueueEvent(e);
  //      }

  //      public void BluetoothDisconnected(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, string deviceName, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.BluetoothDisconnected, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Label = deviceName;
  //          this.QueueEvent(e);
  //      }

  //      public void DocumentPrinted(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, string documentName, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.DocumentPrinted, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Label = documentName;
  //          e.ScreenViewPath = "/" + companyName + "/" + e.Label;
  //          this.QueueEvent(e);
  //      }

  //      public void AttachmentViewed(int companyId, string companyName, int deliverypointgroupId, string deliverypointgroupName, int deliverypointNumber, string attachmentName, string identifier = null, DateTime? dateTime = null)
  //      {
  //          Event e = new Event(EventTypes.AttachmentViewed, identifier, dateTime);
  //          e.Properties.Add(EventPropertyTypes.CompanyId, companyId);
  //          e.Properties.Add(EventPropertyTypes.CompanyName, companyName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupId, deliverypointgroupId);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointgroupName, deliverypointgroupName);
  //          e.Properties.Add(EventPropertyTypes.DeliverypointNumber, deliverypointNumber);
  //          e.Label = attachmentName;
  //          e.ScreenViewPath = "/" + companyName + "/" + e.Label;
  //          this.QueueEvent(e);
  //      }

        #endregion

        #region Session 

        private readonly ProfilePropertyCollection profileProperties = new ProfilePropertyCollection();

        /// <summary>
        /// Sets a Profile Property and overwrites an existing one
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        /// <param name="publishDirectly"></param>
        public void SetProfileProperty<T>(ProfilePropertyType<T> type, T value, string identifier = null, bool publishDirectly = true)
        {
            this.SetProfileProperty<T>(new ProfileProperty<T>(type, value, identifier));
        }

        /// <summary>
        /// Sets a Profile Property and overwrites an existing one
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        /// <param name="publishDirectly"></param>
        public void SetProfileProperty<T>(ProfileProperty<T> property, bool publishDirectly = true)
        {
            lock (this.profileProperties)
            {
                IProperty existingProperty = this.profileProperties.FirstOrDefault(x => x.Name.Equals(property.Name));

                if(existingProperty != null)
                    this.profileProperties.Remove(existingProperty);

                this.profileProperties.Add(property);
            }

            this.QueueProperty(property);
        }

        /// <summary>
        /// This unsets the property from the Profile of the AnalyticsService, but doesn't remove it in the connectors
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyType"></param>
        public void UnsetProfileProperty<T>(PropertyType<T> propertyType)
        {
            lock (this.profileProperties)
            { 
                IProperty existingProperty = this.profileProperties.FirstOrDefault(x => x.Name.Equals(propertyType.Name));
                if (existingProperty != null)
                    this.profileProperties.Remove(existingProperty);
            }
        }

        /// <summary>
        /// This unsets the property from the Profile of the AnalyticsService, but doesn't remove it in the connectors
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        public void UnsetProfileProperty<T>(Property<T> property)
        {
            lock (this.profileProperties)
            {
                IProperty existingProperty = this.profileProperties.FirstOrDefault(x => x.Name.Equals(property.Name));
                if (existingProperty != null)
                    this.profileProperties.Remove(existingProperty);
            }
        }

        #endregion

        #region Public operation methods

        public void Flush()
        {
            this.waitHandle.Set();
        }

        public void SetCampaignInfo(string campaignName, string campaignSource, string campaignMedium, string campaignContent)
        {
            campaignName = campaignName ?? string.Empty;
            campaignSource = campaignSource ?? string.Empty;
            campaignMedium = campaignMedium ?? string.Empty;
            campaignContent = campaignContent ?? string.Empty;

            lock (this.defaultEventPropertiesLock)
            {
                IProperty campaignNameProperty = this.defaultEventProperties.FirstOrDefault(x => x.Name.Equals(ProfilePropertyTypes.CampaignName.Name));
                if (campaignNameProperty != null) this.defaultEventProperties.Remove(campaignNameProperty);
                this.defaultEventProperties.Add(ProfilePropertyTypes.CampaignName, campaignName);

                IProperty campaignSourceProperty = this.defaultEventProperties.FirstOrDefault(x => x.Name.Equals(ProfilePropertyTypes.CampaignSource.Name));
                if (campaignSourceProperty != null) this.defaultEventProperties.Remove(campaignSourceProperty);
                this.defaultEventProperties.Add(ProfilePropertyTypes.CampaignSource, campaignSource);

                IProperty campaignMediumProperty = this.defaultEventProperties.FirstOrDefault(x => x.Name.Equals(ProfilePropertyTypes.CampaignMedium.Name));
                if (campaignMediumProperty != null) this.defaultEventProperties.Remove(campaignMediumProperty);
                this.defaultEventProperties.Add(ProfilePropertyTypes.CampaignMedium, campaignMedium);

                IProperty campaignContentProperty = this.defaultEventProperties.FirstOrDefault(x => x.Name.Equals(ProfilePropertyTypes.CampaignContent.Name));
                if (campaignContentProperty != null) this.defaultEventProperties.Remove(campaignContentProperty);
                this.defaultEventProperties.Add(ProfilePropertyTypes.CampaignContent, campaignContent);
            }
        }

        #endregion

        #region Producer Consumer

        private int autoFlushInterval = 60 * 1000;
        private int autoFlushQueueCount = 30;
        private AutoResetEvent waitHandle = new AutoResetEvent(false);

        // To prevent the device from constantly sending data, it's done in batches
        // a batch is started:
        // 1. Every 60 seconds
        // 2. Every 30 events
        // 3. When Flush is called
        // Which ever hits first causes the items to be submitted

        private void QueueEvent(Event e)
        {
            if (AnalyticsService.VerboseLogging)
				Log.Verbose("AnalyticsService", "QueueuEvent", "Event: {0}", e.ToString());		

            lock(this.profileProperties)
            {
				if(e.Identifier.IsNullOrWhiteSpace())
				{
                    var identifierProperty = this.profileProperties.FirstOrDefault(x => x.Name.Equals(ProfilePropertyTypes.Identifier.Name));
                    if (identifierProperty != null && !identifierProperty.ValueAsString.IsNullOrWhiteSpace())
                        e.Identifier = identifierProperty.ValueAsString;
                    else
                    { 
                        string message;
                        if(identifierProperty == null)
                            message = "Event was queued without an identifier and no identifier property was set.";
                        else 
                            message = "Event was queued without an identifier and the identifier property in sesson is null.";
                            
                        Log.Error("AnalyticsService", "QueueuEvent", message);

                        throw new ObymobiException(AnalyticsServiceResult.NoIdentifierSpecified, message);
                    }
				}

                // Add the Profile Properties to the Event            
                foreach (var property in this.profileProperties)
                {
					// Add only if it doesn't exist
                    if(!e.Properties.Any(x => x.Name.Equals(property.Name))) e.Properties.Add(property);
                }
            }    
     
            // Add the default properties
            lock (this.defaultEventPropertiesLock)
            {
                foreach (var property in this.defaultEventProperties)
                {
					// Add only if it doesn't exist
                    if (!e.Properties.Any(x => x.Name.Equals(property.Name))) e.Properties.Add(property);
                }
            }

            // GK - Sometimes I can't help my self, need to do a one liner now and then
            this.queue.Enqueue(new AnalyticsServiceQueueItem { e = e });

			if (this.queue.Count > this.autoFlushQueueCount || !this.useBatches)
                this.waitHandle.Set();
        }

        private void QueueProperty(IProperty property)
        {
            if (AnalyticsService.VerboseLogging)
                Log.Verbose("AnalyticsService", "QueueuProperty", "Property: {0}", property.ToString());

            if (property.Identifier.IsNullOrWhiteSpace())
            {
                lock (this.profileProperties)
                {
                    var identifierProperty = this.profileProperties.FirstOrDefault(x => x.Name.Equals(ProfilePropertyTypes.Identifier.Name));
                    if (identifierProperty != null && !identifierProperty.ValueAsString.IsNullOrWhiteSpace())
                        property.Identifier = identifierProperty.ValueAsString;
                    else
                    {
                        string message;
                        if (identifierProperty == null)
                            message = "Property was queued without an identifier and no identifier property was set.";
                        else
                            message = "Property was queued without an identifier and the identifier property in sesson is null.";

                        Log.Error("AnalyticsService", "QueueProperty", message);

                        throw new ObymobiException(AnalyticsServiceResult.NoIdentifierSpecified, message);
                    }
                }
            }
                       
            // GK - Sometimes I can't help my self, need to do a one liner now and then
            this.queue.Enqueue(new AnalyticsServiceQueueItem { property = property });

            if (this.queue.Count > this.autoFlushQueueCount || !this.useBatches)
                this.waitHandle.Set();
        }

        private class AnalyticsServiceQueueItem
        { 
            public Event e = null;
            public IProperty property = null;
        }

        private ConcurrentQueue<AnalyticsServiceQueueItem> queue = new ConcurrentQueue<AnalyticsServiceQueueItem>();

        private void ProcessQueue()
        {
			while (true)
			{
				try
				{
					// Wait for processing
					waitHandle.WaitOne(this.autoFlushInterval); // We either get signaled or just do it after X seconds.

					Log.Debug("AnalyticsService", "ProcessQueue", "Processing of Queue of '{0}' items started.", this.queue.Count);
					int startCount = queue.Count;
					int processedCount = 0;

					List<AnalyticsServiceQueueItem> requeue = new List<AnalyticsServiceQueueItem>();

					// Process everything in the queue, yes, this means if the user
					// interacts fast enough this loop will run forever. 
					AnalyticsServiceQueueItem entry;										                           
					while (this.queue.TryDequeue(out entry))
					{
						processedCount++;

						if (entry.e != null)
						{
                            ProcessEvent(requeue, entry);
						} 
						else if (entry.property != null)
						{
                            ProcessProperty(requeue, entry);
						}
					}

					foreach (var item in requeue)
					{
						queue.Enqueue(item);
					}

					Log.Debug("AnalyticsService", "ProcessQueue", "Processing of Queue completed - Start count: {0}, Processed: {1}, Requeued {2}.", startCount, processedCount, requeue.Count);
				} catch (Exception ex)
				{
					Log.Error("AnalyticsService", "ProcessQueue", "Process Que Failed: {0}\r\n{1}", ex.Message, ex.StackTrace);
				}
			}
        }

        private void ProcessEvent(List<AnalyticsServiceQueueItem> requeue, AnalyticsServiceQueueItem entry)
        {
            foreach (AnalyticsConnector connector in analyticsConnectors)
            {
                try
                {
                    // Log only if the event is enabled for that connector.
                    if (entry.e.ConnectorsToReportTo.Keys.Contains(connector.ClassName))
                    {
                        if (connector.RecordEvent(entry.e))
                            entry.e.ConnectorsToReportTo.Remove(connector.ClassName);
                        else
                        {
                            // max 3 attempts
                            if (entry.e.ConnectorsToReportTo[connector.ClassName] < 3)
                            {
                                Log.Debug("AnalyticsService", "ProcessQueue", "Requeue of event '{0}' for {1} attempt.",
                                    entry.e.ToString(), entry.e.ConnectorsToReportTo[connector.ClassName]);

                                if (!requeue.Contains(entry))
                                    requeue.Add(entry);
                            }
                            else
                            {
                                Log.Debug("AnalyticsService", "ProcessQueue", "Event '{0}' failed after 3 attempts.",
                                    entry.e.ToString());
                            }

                            entry.e.ConnectorsToReportTo[connector.ClassName]++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("AnalyticsService", "ProcessQueue", "Failed for Event: {0}\r\nMessage: {1}\r\nStack: {2}", entry.e.ToString(), ex.Message, ex.StackTrace);
                    if (TestUtil.IsPcDeveloper)
                        throw;
                }
            }
        }

        private void ProcessProperty(List<AnalyticsServiceQueueItem> requeue, AnalyticsServiceQueueItem entry)
        {
            foreach (AnalyticsConnector connector in analyticsConnectors)
            {
                try
                {
                    if (entry.property.ReportToProfileToConnectors.ContainsKey(connector.ClassName))
                    {
                        if (connector.SetProperty(entry.property))
                            entry.property.ReportToProfileToConnectors.Remove(connector.ClassName);
                        else
                        {
                            // max 3 attempts                            
                            if (entry.property.ReportToProfileToConnectors[connector.ClassName] < 3)
                            {
                                Log.Debug("AnalyticsService", "ProcessProperty", "Requeue of property '{0}' for {1} attempt.",
                                    entry.property.ToString(), entry.property.ReportToProfileToConnectors[connector.ClassName]);

                                if (!requeue.Contains(entry))
                                    requeue.Add(entry);
                            }
                            else
                            {
                                Log.Debug("AnalyticsService", "ProcessProperty", "Property '{0}' failed after 3 attempts.",
                                    entry.e.ToString());
                            }

                            entry.property.ReportToProfileToConnectors[connector.ClassName]++;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("AnalyticsService", "ProcessQueue", "Failed for Property: {0}\r\nMessage: {1}\r\nStack: {2}", entry.property.ToString(), ex.Message, ex.StackTrace);
                    if (TestUtil.IsPcDeveloper)
                        throw;
                }
            }
        }

        #endregion

        #region Private helpers

        private string GetApplicationTypeNameVersionLabel()
        {
            string toReturn = string.Empty;
            lock (this.defaultEventPropertiesLock)
            {
                var applicationTypeProperty = this.defaultEventProperties.FirstOrDefault(x => x.Name.Equals(EventPropertyTypes.ApplicationType.Name));
                var applicationNameProperty = this.defaultEventProperties.FirstOrDefault(x => x.Name.Equals(EventPropertyTypes.ApplicationName.Name));
                var applicationVersionProperty = this.defaultEventProperties.FirstOrDefault(x => x.Name.Equals(EventPropertyTypes.ApplicationVersion.Name));

                if (applicationTypeProperty != null && applicationNameProperty != null && applicationVersionProperty != null)
                {
					toReturn = "{0} | {1} | {2}".FormatSafe(applicationTypeProperty.ValueAsString, applicationNameProperty.ValueAsString, applicationVersionProperty.ValueAsString);
                }
            }
            return toReturn;
        }

        #endregion

        #region Properties

        public static bool VerboseLogging = true;

        #endregion

        public void Dispose()
        {
            waitHandle.Close();
        }
    }
}
