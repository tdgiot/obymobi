﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public abstract class AnalyticsConnector
    {
        // These methods should not be used directly, only by the Analytics Service
        internal abstract bool RecordEvent(Event e, bool isAnalyticsService = true);
        internal abstract bool SetProperty(IProperty p, bool isAnalyticsService = true);

        protected bool executeWebcall(string url)
        {
            bool toReturn = false;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.KeepAlive = false; // important to have calls after each other
                request.Proxy = null; // important to prevent a Proxy look up which is slow.

				if(AnalyticsService.VerboseLogging)
					Log.Verbose(this.ClassName, "executeWebCall", url);

				int responseCodeInt = -1;
				String responseString = string.Empty;
				using(HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				{
                    responseCodeInt = (int)response.StatusCode;
					if(responseCodeInt >= 200 && responseCodeInt < 300)
						toReturn = true;

					// Get response in case of a non succesful call or verbose logging
					using (Stream stream = response.GetResponseStream())
					{
						StreamReader reader = new StreamReader(stream, Encoding.UTF8);
						responseString = reader.ReadToEnd();
					}

					if (toReturn)
					{
						// Success                    
						if(this.ClassName == MixPanelConnector.ClassNameStatic && !responseString.Equals("1"))
						{
							request = (HttpWebRequest)WebRequest.Create(url + "&verbose=1");
							request.KeepAlive = false; // important to have calls after each other
							request.Proxy = null; // important to prevent a Proxy look up which is slow.

							using(HttpWebResponse response2 = (HttpWebResponse)request.GetResponse())
							{							
								using (Stream stream = response2.GetResponseStream())
								{
									StreamReader reader = new StreamReader(stream, Encoding.UTF8);
									responseString = reader.ReadToEnd();
								}
							}

							Log.Warning(this.ClassName, "executeWebCall", "Failed with response code: {0} - {1} - Response: {2}", responseCodeInt, response.StatusCode.ToString(), responseString);
						}
						else
						{
							if(AnalyticsService.VerboseLogging)
							{
								Log.Verbose(this.ClassName, "executeWebCall", "Complete with response code: {0} - {1} - Response: {2}", responseCodeInt, response.StatusCode.ToString(), responseString);
							}
						}
					}
					else
					{			
						// Failure		                   
						Log.Warning(this.ClassName, "executeWebCall", "Non 2xx result: {0} - {1} - {2}", response.StatusCode, response.StatusDescription, responseString);
					}
				}								                  
            }
            catch (Exception ex)
            {
				Log.Error(this.ClassName, "executeWebCall", "Exception: {0}\r\nStack: {1}", ex.Message, ex.StackTrace);
                //Log.Warning(ClassName, "executeWebcall", "Got an exception while requesting to '{0}': {1}", url, ex.Message);
            }

            return toReturn;
        }

        public abstract string ClassName { get; }
    }
}
