﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Dionysos;
using System.Globalization;
using System.Diagnostics;
using System.Threading.Tasks;
using Obymobi.Enums;

namespace Obymobi.Logic.Analytics
{
    public class MixPanelConnector : AnalyticsConnector
    {
        // For testing:
        // API Key: 25a1eec9402c8144a8295815f2cca4e4
        // API Secret: 758d5e86f314ddf8c097d8a9d6df82b1
        // Token: a3f8a9dbb699275b1c0a0b57803c089d        
        private string eventReportingUrl = "http://api.mixpanel.com/track/?data=";
        private string propertyReportingUrl = "http://api.mixpanel.com/engage/?data=";
        private string apiKey;
        private string projectToken = string.Empty;
        public static bool importHistory = false;                    
        private static string historyImportUrl = "http://api.mixpanel.com/import/?data=";

        public MixPanelConnector(CloudEnvironment cloudEnvironment)
        {
            switch (cloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    this.apiKey = "6648a3eece0686c9687139b4811db4e1";
                    this.projectToken = "b7f42166850608f7d8882a5ad3386ab5";
                    break;
                case CloudEnvironment.Demo:
                    this.apiKey = "59b51972a56646f58454e0108beb9fcb";
                    this.projectToken = "1b1990fe5841ed89e601b9b52100749a";
                    break;
                case CloudEnvironment.Test:
                    this.apiKey = "0dc97e7e3964feda572379610118570a";
                    this.projectToken = "0ecb38d70060b3be728b326852fa4ef4";
                    break;
                case CloudEnvironment.Development:
                case CloudEnvironment.Manual:
                    this.apiKey = "c3ec272a8834cce0681132752d308834";
                    this.projectToken = "48ace91a3a9a04b6715b6c87c2880141";
                    break;
            }
        }

        public MixPanelConnector(string apiKey, string projectToken)
        {
            this.apiKey = apiKey;
            this.projectToken = projectToken;                        
        }

        internal override bool RecordEvent(Event e, bool isAnalyticsService = true)
        {
            // Create Json
            StringBuilder json = new StringBuilder();
            json.AppendFormat("{{");
            json.AppendFormat("   \"event\": \"{0}\",", e.Name);
            json.AppendFormat("   \"properties\": {{");
            json.AppendFormat("       \"distinct_id\": \"{0}\",", e.Identifier);
            json.AppendFormat("       \"token\": \"{0}\",", this.projectToken);
            json.AppendFormat("       \"time\": {0}", e.DateTimeUtcEpoch);

            // Other properties
            for (int i = 0; i < e.Properties.Count; i++)
            {
                var property = e.Properties[i];
                if (property.ReportWithEventToConnectors.ContainsKey(this.ClassName))
                {
                    if (property.ValueAsDecimal.HasValue || property.ValueAsInt.HasValue)
						json.AppendFormat(",       \"{0}\": {1}", property.Name, property.ValueAsString);
                    else
						json.AppendFormat(",       \"{0}\": \"{1}\"", property.Name, property.ValueAsString);
                }
            }          

            if (e.ScreenViewPath.IsNullOrWhiteSpace())
                json.AppendFormat(",\"screenViewPath\": \"{0}\"", e.ScreenViewPath);                            

            json.AppendFormat("   }}");
            json.AppendFormat("}}");

            byte[] bytes = Encoding.UTF8.GetBytes(json.ToString());
            var base64 = Convert.ToBase64String(bytes);            

            string url;
            if (MixPanelConnector.importHistory == true)
            {
                url = historyImportUrl + base64;
                url += "&api_key=";
                url += apiKey;
            }                
            else
                url = this.eventReportingUrl + base64;

            return this.executeWebcall(url);            
        }

        internal override bool SetProperty(IProperty p, bool isAnalyticsService = true)
        {
            // Create Json
            StringBuilder json = new StringBuilder();
            json.AppendFormat("{{");
            json.AppendFormat("   \"$token\": \"{0}\",", this.projectToken);
            json.AppendFormat("   \"$distinct_id\": \"{0}\",", p.Identifier);

			if(p.Name.Equals(ProfilePropertyTypes.FirstLaunch) ||
				p.Name.Equals(ProfilePropertyTypes.CampaignMedium) ||
				p.Name.Equals(ProfilePropertyTypes.CampaignSource) ||
                p.Name.Equals(ProfilePropertyTypes.CampaignContent) ||
				p.Name.Equals(ProfilePropertyTypes.CampaignName))
				json.AppendFormat("   \"$set_once\": {{ ");         
			else
	            json.AppendFormat("   \"$set\": {{ ");         

            if(p.ValueAsDecimal.HasValue || p.ValueAsInt.HasValue)
                json.AppendFormat("       \"{0}\": {1}", p.Name, p.ValueAsString);
            else
                json.AppendFormat("       \"{0}\": \"{1}\"", p.Name, p.ValueAsString);
            json.AppendFormat("   }}");
            json.AppendFormat("}}");

            byte[] bytes = Encoding.UTF8.GetBytes(json.ToString());
            var base64 = Convert.ToBase64String(bytes);

            string url = this.propertyReportingUrl + base64;

            return this.executeWebcall(url);              
        }

        public static string ClassNameStatic
        {
            get
            {
                return "MixPanelConnector";
            }
        }

        public override string ClassName
        {
            get
            {
                return MixPanelConnector.ClassNameStatic;
            }
        }

    }
}
