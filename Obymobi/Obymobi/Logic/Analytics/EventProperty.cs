﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public class EventProperty<T> : Property<T>
    {
        public EventProperty(EventPropertyType<T> propertyType, T value, string identifier = null)
            : base(propertyType, value, identifier)
        { }

        internal EventProperty(string propertyTypeName, T value, ReadOnlyCollection<string> reportToProfileToConnectors, ReadOnlyCollection<string> reportWithEventToConnectors, string identifier = null) :
            base(propertyTypeName, value, reportToProfileToConnectors, reportWithEventToConnectors, identifier)
        {
        }
    }
}
