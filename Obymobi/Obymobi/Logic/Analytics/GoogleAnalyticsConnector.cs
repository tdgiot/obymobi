﻿using System;
using System.Linq;
using Dionysos;
using Obymobi.Constants;
using Obymobi.Enums;

namespace Obymobi.Logic.Analytics
{
    public class GoogleAnalyticsConnector : AnalyticsConnector
    {
        // https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide        
        private string trackingId = string.Empty;
        private string baseUrl = "http://www.google-analytics.com/collect?";
        private int hitsInSession = 0;

        public GoogleAnalyticsConnector(CloudEnvironment cloudEnvironment)
        {
            this.trackingId = ObymobiConstants.GetGoogleAnalyticsTrackingId(cloudEnvironment);
        }

        public GoogleAnalyticsConnector(string trackingId)
        {
            this.trackingId = trackingId;
        }

        #region Public interface methods

        internal override bool RecordEvent(Event e, bool isAnalyticsService = true)
        {
            if (e.Category == EventCategory.ScreenView || !e.ScreenViewPath.IsNullOrWhiteSpace())
            {
                this.TrackScreenView(e);
            }

            bool toReturn = this.TrackAppEvent(e);

            // This is the limit per session on Google Analytics - beter to restart the session than losing 
            // the events after that.
            // http://developers.google.com/analytics/devguides/collection/protocol/v1/limits-quotas
            if (this.hitsInSession > 475)
            {
                this.ReportSessionEnd(e);
                this.ReportSessionStart(e);
                this.hitsInSession = 0;
            }

            return toReturn;
        }

        internal override bool SetProperty(IProperty property, bool isAnalyticsService = true)
        {
            //throw new NotImplementedException();
            return true;
        }

        #endregion

        #region Tracking Methods

        private bool TrackScreenView(Event e)
        {
            string screenViewPath = string.Empty;

            if (!e.ScreenViewPath.IsNullOrWhiteSpace())
            {
                screenViewPath = e.ScreenViewPath.StartsWith("//") ? e.ScreenViewPath.Substring(1) : e.ScreenViewPath;

                // To cover up sloppy programming
                screenViewPath = screenViewPath.Replace("//", "/");
            }

            if (!screenViewPath.IsNullOrWhiteSpace())
            {
                string queryString = string.Format("v=1&tid={0}&cid={1}&t=appview&cd={2}",
                              this.trackingId,
                              Uri.EscapeDataString(e.Identifier),
                                Uri.EscapeDataString(screenViewPath));

                this.AppendCustomDimensionQueryStringParameters(e, ref queryString);

                string url = this.baseUrl + queryString;

                this.hitsInSession++;
                return this.executeWebcall(url);
            }
            else
            {
                return true;
            }
        }

        private bool TrackAppEvent(Event e, bool dedicatedSessionEvent = false)
        {
            if (e.Label.IsNullOrWhiteSpace())
                e.Label = e.ScreenViewPath;

			string queryString = string.Format("v=1&tid={0}&cid={1}&t=event&ec={2}&ea={3}",
                this.trackingId,
                Uri.EscapeDataString(e.Identifier),
				// "  %26 end es! sls/ bsls\\ gt>%20 SPACEls< char' char2\" "
				Uri.EscapeDataString(e.Category),
                Uri.EscapeDataString(e.Name));

            // Session events should only report the bare minimal
            if (dedicatedSessionEvent)
            {
                // Add the session related variable
                if (e.Type.Name.Equals(EventTypes.ApplicationGotFocus.Name)) queryString += "&sc=start";
                else if (e.Type.Name.Equals(EventTypes.ApplicationLostFocus.Name)) queryString += "&sc=end";
                else Log.Warning("GoogleAnalyticsConnector", "TrackAppEvent", "Unsupported Session Action: " + e.Type.Name);
            }
            else
            {
                // Fire dedicated start/end session events
                if (e.Type.Name.Equals(EventTypes.ApplicationGotFocus.Name)) this.ReportSessionStart(e);
                else if (e.Type.Name.Equals(EventTypes.ApplicationLostFocus.Name)) this.ReportSessionEnd(e);

                // Event Label
                if (!e.Label.IsNullOrWhiteSpace()) queryString += "&el=" + Uri.EscapeDataString(e.Label);
                else if (!e.ScreenViewPath.IsNullOrWhiteSpace()) queryString += "&el=" + Uri.EscapeDataString(e.ScreenViewPath);
                else queryString += "&el=default";// +Uri.EscapeDataString(e.Label);

                // Event Value
                if (e.Value.HasValue && e.Value >= 0)
                    queryString += "&ev=" + Uri.EscapeUriString(e.Value.ToString());

                // Always append custom dimensions
                this.AppendCustomDimensionQueryStringParameters(e, ref queryString);
            }

            string url = this.baseUrl + queryString;

            this.hitsInSession++;
            return this.executeWebcall(url);
        }

        #endregion

        private void ReportSessionStart(Event e)
        {
            Event startSession = new Event(EventTypes.ApplicationGotFocus, e.Identifier, null);
            this.TrackAppEvent(startSession, true);
        }

        private void ReportSessionEnd(Event e)
        {
            Event endSession = new Event(EventTypes.ApplicationLostFocus, e.Identifier, null);
            this.TrackAppEvent(endSession, true);
        }

        private void AppendCustomDimensionQueryStringParameters(Event e, ref string queryString)
        {
            string primaryKeys = "";

            // Company is a special case
            var companyNameProperty = e.Properties.FirstOrDefault(x => x.Name.Equals(EventPropertyTypes.CompanyName.Name));
            var companyIdProperty = e.Properties.FirstOrDefault(x => x.Name.Equals(EventPropertyTypes.CompanyId.Name));
            if (companyNameProperty != null && companyIdProperty != null)
            {
				queryString += "&cd{0}={1}".FormatSafe((int)GoogleAnalyticsCustomDimensions.Company, Uri.EscapeDataString("{0} [{1}]".FormatSafe(companyNameProperty.ValueAsString, companyIdProperty.ValueAsString)));
            }

            string deliverypointgroupName = null;
            int? deliverypointgroupId = null;
            int? deliverypointNumber = null;
            bool applicationNameSet = false;
            // Iterate over all properties and apply them where useful
            foreach (var property in e.Properties)
            {
                // Add Custom Dimensions that have a dedicated field
                if (property.Name.Equals(EventPropertyTypes.OperatingSystem.Name))
                {
                    queryString += this.GetCustomDimensionFragment(GoogleAnalyticsCustomDimensions.OperatingSystem, property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(EventPropertyTypes.Tablet.Name) && property.ValueAsBool.HasValue)
                {
                    queryString += this.GetCustomDimensionFragment(GoogleAnalyticsCustomDimensions.IsTablet, property.ValueAsBool.Value ? "1" : "0");                    
                }
                else if (property.Name.Equals(EventPropertyTypes.Model.Name))
                {
                    queryString += this.GetCustomDimensionFragment(GoogleAnalyticsCustomDimensions.DeviceType, property.ValueAsStringUrlEncoded);                                        
                }
                else if (property.Name.Equals(EventPropertyTypes.ApplicationType.Name)) 
                {
                    queryString += this.GetCustomDimensionFragment(GoogleAnalyticsCustomDimensions.ApplicationType, property.ValueAsStringUrlEncoded); 
                }
                else if (property.Name.Equals(EventPropertyTypes.NavigationSource.Name)) 
                {
                    queryString += this.GetCustomDimensionFragment(GoogleAnalyticsCustomDimensions.NavigationSource, property.ValueAsStringUrlEncoded);                     
                }
                // Here are the primary keys which we concat in one field.
                else if (property.Name.Equals(EventPropertyTypes.CompanyId.Name))
                {
                    primaryKeys += this.GetPrimaryKeyFragment(PrimaryKeyPrefix.Company, property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(EventPropertyTypes.ProductId.Name))
                {
                    primaryKeys += this.GetPrimaryKeyFragment(PrimaryKeyPrefix.Product, property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(EventPropertyTypes.CategoryId.Name))
                {
                    primaryKeys += this.GetPrimaryKeyFragment(PrimaryKeyPrefix.Category, property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(EventPropertyTypes.SiteId.Name))
                {
                    primaryKeys += this.GetPrimaryKeyFragment(PrimaryKeyPrefix.Site, property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(EventPropertyTypes.PageId.Name))
                {
                    primaryKeys += this.GetPrimaryKeyFragment(PrimaryKeyPrefix.Page, property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(EventPropertyTypes.TabId.Name))
                {
                    primaryKeys += this.GetPrimaryKeyFragment(PrimaryKeyPrefix.Tab, property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(EventPropertyTypes.MessageId.Name))
                {
                    primaryKeys += this.GetPrimaryKeyFragment(PrimaryKeyPrefix.Message, property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(EventPropertyTypes.EntertainmentId.Name))
                {
                    primaryKeys += this.GetPrimaryKeyFragment(PrimaryKeyPrefix.Entertainment, property.ValueAsStringUrlEncoded);
                }
                // Handle properties that have a dedicated field in Google Analytics 
                else if (property.Name.Equals(EventPropertyTypes.ResolutionWidthHeight.Name))
                {
                    queryString += "&sr={0}".FormatSafe(property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(EventPropertyTypes.ApplicationVersion.Name))
                {
                    queryString += "&av={0}".FormatSafe(property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(EventPropertyTypes.ApplicationName.Name)) 
                { 
                    queryString += "&an={0}".FormatSafe(property.ValueAsStringUrlEncoded); 
					applicationNameSet = true; 
                }
                // The Campaign related values
                else if (property.Name.Equals(ProfilePropertyTypes.CampaignName.Name))
                {
                    queryString += "&cn={0}".FormatSafe(property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(ProfilePropertyTypes.CampaignSource.Name))
                {
                    queryString += "&cs={0}".FormatSafe(property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(ProfilePropertyTypes.CampaignMedium.Name))
                {
                    queryString += "&cm={0}".FormatSafe(property.ValueAsStringUrlEncoded);
                }
                else if (property.Name.Equals(ProfilePropertyTypes.CampaignContent.Name))
                {
                    queryString += "&cc={0}".FormatSafe(property.ValueAsStringUrlEncoded);
                }
                // Here are the Deliverypoint(group) related items
                else if (property.Name.Equals(EventPropertyTypes.DeliverypointgroupId.Name))
                {
                    primaryKeys += this.GetPrimaryKeyFragment(PrimaryKeyPrefix.DeliverypointGroup, property.ValueAsStringUrlEncoded);
                    deliverypointgroupId = property.ValueAsInt;
                }
                else if (property.Name.Equals(EventPropertyTypes.DeliverypointgroupName.Name))
                {
                    deliverypointgroupName = property.ValueAsStringUrlEncoded;
                }
                else if (property.Name.Equals(EventPropertyTypes.DeliverypointNumber.Name))
                {
                    deliverypointNumber = property.ValueAsInt;
                }
            }

            // Set an Application Name if not set, otherwise Google won't record it
            if (!applicationNameSet) queryString += "&an=Unknown";

            // Set the compiled Primary Keys dimenions
            if (!primaryKeys.IsNullOrWhiteSpace()) queryString += "&cd{0}={1}".FormatSafe((int)GoogleAnalyticsCustomDimensions.PrimaryKeys, Uri.EscapeDataString(primaryKeys));

            // Set the Deliverypoint information if complete
            if (!deliverypointgroupName.IsNullOrWhiteSpace() && deliverypointgroupId.HasValue)
            {
                // Prepare the require bit (DPG information)
                string deliverypointInformation = "{0} [{1}]".FormatSafe(deliverypointgroupName, deliverypointgroupId);
                queryString += "&cd{0}={1}".FormatSafe((int)GoogleAnalyticsCustomDimensions.Deliverypointgroup, Uri.EscapeDataString(deliverypointInformation));

                if (deliverypointNumber.HasValue) queryString += "&cd{0}={1}".FormatSafe((int)GoogleAnalyticsCustomDimensions.Deliverypointnumber, Uri.EscapeDataString(deliverypointNumber.Value.ToString()));
            }            
        }

        private string GetCustomDimensionFragment(GoogleAnalyticsCustomDimensions dimension, string value)
        {
			return "&cd{0}={1}".FormatSafe((int)dimension, value);
        }

        private string GetPrimaryKeyFragment(string primaryKeyPrefix, string value)
        {
            return "[{0}-{1}]".FormatSafe(primaryKeyPrefix, value);
        }

        public static string ClassNameStatic
        {
            get
            {
                return "GoogleAnalyticsConnector";
            }
        }

        public override string ClassName
        {
            get
            {
                return GoogleAnalyticsConnector.ClassNameStatic;
            }
        }

		/// <summary>
		/// Replaces & with Url safe character.
		/// </summary>
		/// <returns>The URL safe.</returns>
		/// <param name="input">Input.</param>
		private static string MakeUrlSafe(string input)
		{
			return input.Replace("&", "%28");
		}
    }
}
