﻿using System;

namespace Obymobi
{
	public static class PrimaryKeyPrefix
	{
        public const string Company = "C";
        public const string PointOfInterest = "POI";
		public const string Product = "P";
		public const string Category = "CT";
		public const string Site = "S";
		public const string Page = "PG";
		public const string Advertisement = "A";
		public const string Tab = "T";        
        public const string DeliverypointGroup = "DPG";
        public const string Message = "MSG";
        public const string Entertainment = "ETM";
	}
}

