﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Logic.Analytics
{
    public class Event
    {
		internal Event(EventType type, string identifier, DateTime? dateTime = null)
        {
            this.Type = type;
            this.Properties = new EventPropertyCollection();
            this.Identifier = identifier;
            DateTimeUtc = dateTime ?? DateTime.UtcNow;
            this.Label = "";
            foreach (var connector in type.ReportToConnectors)
            {
                this.ConnectorsToReportTo.Add(connector, 0);
            }
            this.Value = null;
            this.Label = null;
        }

        public EventType Type { get; set; }
        public string Identifier { get; set; }
        public EventPropertyCollection Properties { get; set; }
        public DateTime DateTimeUtc { get; set; }
        public string ScreenViewPath { get; set; }
        public Dictionary<string, int> ConnectorsToReportTo = new Dictionary<string, int>();
        public string Label { get; set; }
        public int? Value { get; set; }		

        public long DateTimeUtcEpoch 
        {
            get
            {
                return Dionysos.DateTimeUtil.ToUnixTime(this.DateTimeUtc);
            }
        }

        public string Name
        {
            get
            {
                return this.Type.Name;
            }
        }

        public string Category
        {
            get
            {
                return this.Type.Category;
            }
        }

        public override string ToString()
        {
            if(this.Type.Category.Equals(EventCategory.ScreenView))
            {
                return "Event Type: {0} - {1}".FormatSafe(this.Type.Name, this.ScreenViewPath);
            }
            else if(this.Properties.Count > 0)
            {
                string propertiesText = string.Empty;
                foreach (var prop in this.Properties)
                    propertiesText += "'{0}': {1}, ".FormatSafe(prop.Name, prop.ValueAsString);
                return "Event Type: {0} - {1}".FormatSafe(this.Type.Name, propertiesText);
            }
            else
            {
				return "Event Type: " + this.Type.Name;
            }            
        }
    }
}
