﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public enum GoogleAnalyticsCustomDimensions
    {
        Session = 1,
        //DeliverypointNumber = 2,
        MACAddress = 3,
        DeviceType = 4,
        Timestamp = 5,
        Product = 6,
        Alteration = 7,
        ServiceRequest = 8,
        Category = 9,
        Entertainment = 10,
		// Below are used, above are just copied from legacy Emenu - not need really.
        ApplicationType = 11, // Was: DirectoryItem = 11,
        Deliverypointnumber = 12, // Was: Advertisement = 12,
        Deliverypointgroup = 13, // Was: Suggestion = 13, 
		PrimaryKeys = 14, // Was: Survey = 14
        IsTablet = 15,
        OperatingSystem = 16,
        Company = 17,
		// ApplicationVersion = 18, -- Can be used for another purpose (it's captured in a dedicated Dimension: App Name)
        NavigationSource = 19,
		// ApplicationName = 20, -- Can be used for another purpuse (it's  captured in a dedicated Dimension: App Version)
    }
}
