﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public class EventPropertyTypes
    {
		private static readonly ReadOnlyCollection<string> all = new ReadOnlyCollection<string>(new List<string> { GoogleAnalyticsConnector.ClassNameStatic, MixPanelConnector.ClassNameStatic });
		//private static readonly ReadOnlyCollection<string> googleOnly = new ReadOnlyCollection<string>(new List<string> { GoogleAnalyticsConnector.ClassNameStatic });        
        private static readonly ReadOnlyCollection<string> mixpanelOnly = new ReadOnlyCollection<string>(new List<string> { MixPanelConnector.ClassNameStatic });
        private static readonly ReadOnlyCollection<string> none = new ReadOnlyCollection<string>(new List<string> { });

        /// IMPORTANT: If you add anything to this class you also need to update the related documentation: 
        /// "O:\Software Development\Documentation\Functional specification\20140223 - Analytics Reporting\Analytics Events.docx"

        /// It's of no use to add Google Analytics to any of the EventPropertyTypes as it requires a custom implemenation per EventProperty
        /// anyway for it to be used as Custom Dimension. This in contrast with Mix Panel which needs no setup to add properties to an Event.

        internal static readonly EventPropertyType<ApplicationType> ApplicationType = new EventPropertyType<ApplicationType>("Application Type", none, all);

        public static readonly EventPropertyType<string> ApplicationVersion = new EventPropertyType<string>("Application Version", none, all);
        public static readonly EventPropertyType<string> ApplicationName = new EventPropertyType<string>("Application Name", none, all);
        public static readonly EventPropertyType<OperatingSystem> OperatingSystem = new EventPropertyType<OperatingSystem>("Operating System", none, all);
        public static readonly EventPropertyType<string> OperatingSystemVersion = new EventPropertyType<string>("Operating System Version", mixpanelOnly, all);
        public static readonly EventPropertyType<bool> Tablet = new EventPropertyType<bool>("Tablet", none, all);
        public static readonly EventPropertyType<string> Model = new EventPropertyType<string>("Model", none, all);
        public static readonly EventPropertyType<Tuple<int, int>> ResolutionWidthHeight = new EventPropertyType<Tuple<int, int>>("Resolution", none, all);
        

        //internal static readonly EventPropertyType<string> CampaignSource = new EventPropertyType<string>("Campaign Source", none, mixpanelOnly);        
        //public static readonly EventPropertyType<string> CampaignName = new EventPropertyType<string>("Campaign Name", none, mixpanelOnly);
        //public static readonly EventPropertyType<string> CampaignMedium = new EventPropertyType<string>("Campaign Medium", none, mixpanelOnly);
        //public static readonly EventPropertyType<string> CampaignContent = new EventPropertyType<string>("Campaign Content", none, mixpanelOnly);
        public static readonly EventPropertyType<AppStore> AppStore = new EventPropertyType<AppStore>("App Store", none, mixpanelOnly);
        //public static readonly EventPropertyType<int> SecondsOfFocus = new EventPropertyType<int>("Seconds of Focus", none, mixpanelOnly);
        public static readonly EventPropertyType<int> ProductId = new EventPropertyType<int>("Product Id", none, mixpanelOnly);
        public static readonly EventPropertyType<string> ProductName = new EventPropertyType<string>("Product Name", none, mixpanelOnly);
		public static readonly EventPropertyType<int> CompanyId = new EventPropertyType<int>("Company Id", none, all);
		public static readonly EventPropertyType<string> CompanyName = new EventPropertyType<string>("Company Name", none, all);
        public static readonly EventPropertyType<int> CategoryId = new EventPropertyType<int>("Category Id", none, mixpanelOnly);
        public static readonly EventPropertyType<string> CategoryName = new EventPropertyType<string>("Category Name", none, mixpanelOnly);
        public static readonly EventPropertyType<int> EntertainmentId = new EventPropertyType<int>("Entertainment Id", none, mixpanelOnly);
        public static readonly EventPropertyType<string> EntertainmentName = new EventPropertyType<string>("Entertainment Name", none, mixpanelOnly);
        public static readonly EventPropertyType<int> SiteId = new EventPropertyType<int>("Site Id", none, mixpanelOnly);
        public static readonly EventPropertyType<string> SiteName = new EventPropertyType<string>("Site Name", none, mixpanelOnly);
        public static readonly EventPropertyType<int> PageId = new EventPropertyType<int>("Page Id", none, mixpanelOnly);
        public static readonly EventPropertyType<string> PageName = new EventPropertyType<string>("Page Name", none, mixpanelOnly);
        public static readonly EventPropertyType<decimal> OrderValue = new EventPropertyType<decimal>("Order Value", none, mixpanelOnly);
        public static readonly EventPropertyType<int> Quantity = new EventPropertyType<int>("Quantity", none, mixpanelOnly);
        public static readonly EventPropertyType<string> ErrorCode = new EventPropertyType<string>("Error Code", none, mixpanelOnly);
        public static readonly EventPropertyType<string> ErrorText = new EventPropertyType<string>("Error Text", none, mixpanelOnly);
        public static readonly EventPropertyType<string> AdvertisementId = new EventPropertyType<string>("Advertisement Id", none, mixpanelOnly);
        public static readonly EventPropertyType<string> AdvertisementLocation = new EventPropertyType<string>("Advertisement Location", none, mixpanelOnly);
        public static readonly EventPropertyType<string> AdvertisementReference = new EventPropertyType<string>("Advertisement Reference", none, mixpanelOnly);
        public static readonly EventPropertyType<string> AdvertisementTarget = new EventPropertyType<string>("Advertisement Target", none, mixpanelOnly);
        public static readonly EventPropertyType<int> MessageId = new EventPropertyType<int>("Message Id", none, mixpanelOnly);
        public static readonly EventPropertyType<string> MessageTitle = new EventPropertyType<string>("Message Title", none, mixpanelOnly);
        public static readonly EventPropertyType<string> MessageResponse = new EventPropertyType<string>("Message Response", none, mixpanelOnly);        
        public static readonly EventPropertyType<int> TabId = new EventPropertyType<int>("Tab Id", none, mixpanelOnly);
        public static readonly EventPropertyType<string> TabName = new EventPropertyType<string>("Tab Name", none, mixpanelOnly);
        public static readonly EventPropertyType<int> Seconds = new EventPropertyType<int>("Seconds", none, mixpanelOnly);
        public static readonly EventPropertyType<int> PointOfInterestId = new EventPropertyType<int>("Point of Interest Id", none, mixpanelOnly);
        public static readonly EventPropertyType<string> PointOfInterestName = new EventPropertyType<string>("Point of Interest Name", none, mixpanelOnly);
        public static readonly EventPropertyType<string> NavigationSource = new EventPropertyType<string>("Navigation Source", none, all);
        public static readonly EventPropertyType<int> DeliverypointgroupId = new EventPropertyType<int>("Deliverypointgroup Id", none, all);
        public static readonly EventPropertyType<string> DeliverypointgroupName = new EventPropertyType<string>("Deliverypointgroup Name", none, all);
        public static readonly EventPropertyType<int> DeliverypointNumber = new EventPropertyType<int>("Deliverypoint Number", none, all);        

        /// IMPORTANT: If you add anything to this class you also need to update the related documentation: 
        /// "O:\Software Development\Documentation\Functional specification\20140223 - Analytics Reporting\Analytics Events.docx"

    }
}
