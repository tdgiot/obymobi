﻿namespace Obymobi.Logic.Analytics
{
    // GK Design choice: Not an enum, because we need to report string values to every 
    // connector and might want to include spaces.
    public class EventCategory
    {
        public const string Aquisition = "Aquisition";
        public const string Activation = "Activation";
        public const string Transactions = "Transactions";
        public const string ScreenView = "ScreenView";
        public const string Advertising = "Advertising";
        public const string Exploratory = "Exploratory";
        public const string InternetTab = "InternetTab";
        public const string Messaging = "Messaging";
        public const string Connectivity = "Connectivity";
    }
}
