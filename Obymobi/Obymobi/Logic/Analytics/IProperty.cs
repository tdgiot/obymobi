﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public interface IProperty
    {
        string Identifier { get; set; }
        string Name { get; }
        DateTime DateTimeUtc { get; set; }
        string ValueAsString { get; }
        string ValueAsStringUrlEncoded { get; }
        decimal? ValueAsDecimal { get; }
        bool? ValueAsBool { get; }
        int? ValueAsInt { get; }
        int Attempts { get; set; }
        Dictionary<string, int> ReportWithEventToConnectors { get; }
        Dictionary<string, int> ReportToProfileToConnectors { get; }        
    }
}
