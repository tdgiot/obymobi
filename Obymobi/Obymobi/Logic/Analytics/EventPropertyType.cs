﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public class EventPropertyType<T> : PropertyType<T>
    {
        internal EventPropertyType(string name, ReadOnlyCollection<string> reportToProfileToConnectors, ReadOnlyCollection<string> reportWithEventToConnectors)
            : base(name, reportToProfileToConnectors, reportWithEventToConnectors)
        {
        }
    }
}
