﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Dionysos;
using System.Globalization;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Obymobi.Logic.Analytics
{
  /*  internal class KissMetricsConnector : AnalyticsConnector
    {        
        public const string eventReportingUrl = "https://trk.kissmetrics.com/e?_k=" + apiKey;
        public const string propertyReportingUrl = "https://trk.kissmetrics.com/s?_k=" + apiKey;
        public const string apiKey = "5d09ec0980add2795b1bbff2122d5ed51f6d29f3";

        public KissMetricsConnector()
        { 

        }

        internal override bool RecordEvent(Event e)
        {
            // Convert event to url
            StringBuilder url = new StringBuilder();
            url.AppendFormat("{0}&_p={1}&_n={2}&_t={3}&_d=1",
                KissMetricsConnector.eventReportingUrl,
                Uri.EscapeDataString(e.Identifier),
                Uri.EscapeDataString(e.Name),
                DateTimeUtil.ToUnixTime(e.DateTimeUtc));

            foreach (var property in e.Properties)
            { 
                if(property.Value == null)
                    url.AppendFormat("&{0}={1}", property.Key, string.Empty);
                else if(property.Value is decimal)
                    url.AppendFormat("&{0}={1}", property.Key, ((decimal)property.Value).ToString("N", CultureInfo.InvariantCulture));
                else if(property.Value is double)
                    url.AppendFormat("&{0}={1}", property.Key, ((double)property.Value).ToString("N", CultureInfo.InvariantCulture));
                else if(property.Value is float)                
                    url.AppendFormat("&{0}={1}", property.Key, ((float)property.Value).ToString("N", CultureInfo.InvariantCulture));
                else
                    url.AppendFormat("&{0}={1}", property.Key, property.Value);
            }

            return this.executeWebcall(url.ToString());            
        }

        internal override bool SetProperty(Property p)
        {
            // Convert event to url
            string valueSafe;
            if (p.Value == null)
                valueSafe = string.Empty;
            else if (p.Value is decimal)
                valueSafe = ((decimal)p.Value).ToString("N", CultureInfo.InvariantCulture);
            else if (p.Value is double)
                valueSafe = ((double)p.Value).ToString("N", CultureInfo.InvariantCulture);
            else if (p.Value is float)
                valueSafe = ((float)p.Value).ToString("N", CultureInfo.InvariantCulture);
            else
                valueSafe = p.Value.ToString();

            string url = "{0}&_p={1}&{2}={3}&_t={4}&_d=1".FormatSafe(
                KissMetricsConnector.propertyReportingUrl,
                Uri.EscapeDataString(p.Identifier),
                Uri.EscapeDataString(p.Key),
                Uri.EscapeDataString(valueSafe),
                DateTimeUtil.ToUnixTime(p.DateTimeUtc));

            return this.executeWebcall(url.ToString());            
        }

        public override string ClassName
        {
            get
            {
                return "KissMetricsConnector";
            }
        }
    }*/
}
