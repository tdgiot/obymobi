﻿namespace Obymobi.Logic.Analytics
{
    public enum NavigationSource
    {
		// We might not know or forgot to set it.
        Unspecified,
		// We don't know, but we know that we don't know
        Undeterminable,
		// The screen was shown because it was the last active screen at the moment the application lost focus
        ApplicationGotFocus,
		// The screen was shown a result of an action of a Advertisement
        Advertisement,
		// Navigation as a result of opening something, i.e. first product in category, first tab of a company, first page of a native site
		FirstItemToAppear, 
		// User really choose to go there, i.e. clicking a tab, clicking a product or link to a page in the sites
        NormalNavigation,
		// User is shown the screen as result of pressing back (not a real intentional navigation action)
        BackNavigation, 
        // User swiped to navigate
        SwipeNavigation,
        // User uses arrows to navigate
        ArrowNavigation,
        // User used the pager to navigate
        PagerNavigation,
    }
}
