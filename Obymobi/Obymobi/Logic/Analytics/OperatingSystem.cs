﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public enum OperatingSystem
    {
        NotSet,
		iOS,
        Android,
        WindowsPhone,
        WindowsDesktop,
        MacOS,
    }
}
