﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public enum AppStore
    {
        AppleAppStore,
        GooglePlay,
        WindowsPhoneStore
    }
}
