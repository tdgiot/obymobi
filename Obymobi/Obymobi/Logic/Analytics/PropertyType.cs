﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public abstract class PropertyType<T>
    {
        internal PropertyType(string name, ReadOnlyCollection<string> reportToProfileToConnectors, ReadOnlyCollection<string> reportWithEventToConnectors)
        {
            this.Name = name;
            this.ReportToProfileToConnectors = reportToProfileToConnectors;
            this.ReportWithEventToConnectors = reportWithEventToConnectors;
        }

        public readonly string Name;
        public readonly ReadOnlyCollection<string> ReportWithEventToConnectors;
        public readonly ReadOnlyCollection<string> ReportToProfileToConnectors;
    }
}
