﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public class ProfilePropertyCollection : List<IProperty>
    {        
        public ProfileProperty<T> Add<T>(ProfilePropertyType<T> type, T value, string identifier = null)
        {
            var toReturn = new ProfileProperty<T>(type, value, identifier);
            this.Add(toReturn);
            return toReturn;
        }
        
        public ProfileProperty<T> Add<T>(ProfileProperty<T> profileProperty)
        {
            base.Add(profileProperty);
            return profileProperty;
        }

		private new void Add(IProperty item)
        {
			throw new InvalidOperationException();
        }
    }
}
