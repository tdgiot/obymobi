﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public class EventPropertyCollection : List<IProperty>
    {
        public EventProperty<T> Add<T>(EventPropertyType<T> type, T value, string identifier = null)
        {
            var toReturn = new EventProperty<T>(type, value, identifier);
            this.Add(toReturn);
            return toReturn;
        }

        public EventProperty<T> Add<T>(EventProperty<T> EventProperty)
        {
            base.Add(EventProperty);
            return EventProperty;
        }

		private new void Add(IProperty item)
        {
			throw new InvalidOperationException();
        }
    }
}
