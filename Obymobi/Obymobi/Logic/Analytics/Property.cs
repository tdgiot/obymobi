﻿    using System;
using System.Collections.Generic;
using Dionysos;
using System.Collections.ObjectModel;
using System.Globalization;

namespace Obymobi.Logic.Analytics
{
	public abstract class Property<T> : IProperty
    {
        public Property(PropertyType<T> propertyType, T value, string identifier = null)
            : this(propertyType.Name, value, propertyType.ReportToProfileToConnectors, propertyType.ReportWithEventToConnectors, identifier)
        {
        }

        internal Property(string propertyTypeName, T value, ReadOnlyCollection<string> reportToProfileToConnectors, ReadOnlyCollection<string> reportWithEventToConnectors, string identifier = null)
        {
            this.Identifier = identifier;
            this.Value = value;
            this.DateTimeUtc = DateTime.UtcNow;
            this.Attempts = 0;
			this.Name = propertyTypeName;

            this.ReportWithEventToConnectors = new Dictionary<string, int>();
            this.ReportToProfileToConnectors = new Dictionary<string, int>();

            foreach (var connector in reportToProfileToConnectors)
                this.ReportToProfileToConnectors.Add(connector, 0);

            foreach (var connector in reportWithEventToConnectors)
                this.ReportWithEventToConnectors.Add(connector, 0);
        }

        public Dictionary<string, int> ReportWithEventToConnectors {get;set;}  //= new Dictionary<string,int>();
        public Dictionary<string, int> ReportToProfileToConnectors {get;set;}  //= new Dictionary<string,int>();

        public string Identifier { get; set; }
        public DateTime DateTimeUtc { get; set; }        
        public T Value { get; internal set; }
        public int Attempts { get; set; }        
		//public PropertyType<T> Type { get; set; }

        public string Name
		{
			get;
			private set;
        }

        public string ValueAsString
        {
            get 
            {
				if (this.Value == null)
                    return string.Empty;
                else if (this.Value is Tuple<int, int>)
                {
                    Tuple<int, int> tuple = (Tuple<int, int>)Convert.ChangeType(this.Value, typeof(Tuple<int, int>));
                    return string.Format("{0}x{1}", tuple.Item1, tuple.Item2);
                }
                else if (this.Value is decimal)
                    return ((decimal)Convert.ChangeType(this.Value, typeof(decimal))).ToString("0.00", CultureInfo.InvariantCulture);
                else if (this.Value is double)
                    return ((double)Convert.ChangeType(this.Value, typeof(double))).ToString("0.00", CultureInfo.InvariantCulture);
                else if (this.Value is float)
                    return ((float)Convert.ChangeType(this.Value, typeof(float))).ToString("0.00", CultureInfo.InvariantCulture);
                else
                    return this.Value.ToString();
            }
        }

        public string ValueAsStringUrlEncoded
        {
            get
            {
                string value = this.ValueAsString;
                if (!value.IsNullOrWhiteSpace())
                    return Uri.EscapeUriString(value);
                else
                    return string.Empty;
            }
        }

        public decimal? ValueAsDecimal
        {
            get 
            {
                if (this.Value is decimal)
                    return (decimal)Convert.ChangeType(this.Value, typeof(decimal));
                else
                    return null;
            }
        }

        public int? ValueAsInt
        {
            get 
            {
                if (this.Value is int)
                    return (int)Convert.ChangeType(this.Value, typeof(int));
                else
                    return null;
            }
        }

        public bool? ValueAsBool
        {
            get
            {
                if (this.Value is bool)
                    return (bool)Convert.ChangeType(this.Value, typeof(bool));
                else
                    return null;
            }
        }        

        public override string ToString()
        {
            if (this.ValueAsDecimal.HasValue)
                return "Name: {0}, Value: {1}".FormatSafe(this.Name, this.ValueAsDecimal);
            else if (this.ValueAsInt.HasValue)
                return "Name: {0}, Value: {1}".FormatSafe(this.Name, this.ValueAsInt);
            else
                return "Name: {0}, Value: {1}".FormatSafe(this.Name, this.ValueAsString);
        }
    }
}
