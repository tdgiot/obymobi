﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public static class ProfilePropertyTypes
    {
		private static readonly ReadOnlyCollection<string> all = new ReadOnlyCollection<string>(new List<string> { GoogleAnalyticsConnector.ClassNameStatic, MixPanelConnector.ClassNameStatic });
		//private static readonly ReadOnlyCollection<string> googleOnly = new ReadOnlyCollection<string>(new List<string> { GoogleAnalyticsConnector.ClassNameStatic });        
        private static readonly ReadOnlyCollection<string> mixpanelOnly = new ReadOnlyCollection<string>(new List<string> { MixPanelConnector.ClassNameStatic });
		private static readonly ReadOnlyCollection<string> none = new ReadOnlyCollection<string>(new List<string> {  });

        /// IMPORTANT: If you add anything to this class you also need to update the related documentation: 
        /// "O:\Software Development\Documentation\Functional specification\20140223 - Analytics Reporting\Analytics Events.docx"

        /// <summary>
        /// This is a very special property and is required to be intialized and is used to sent as the identifier to the analytics package
        /// </summary>
		public static readonly ProfilePropertyType<string> Identifier = new ProfilePropertyType<string>("Identifier", mixpanelOnly, none);

		public static readonly ProfilePropertyType<string> FirstLaunch = new ProfilePropertyType<string>("First Launch", mixpanelOnly, none);
        
        public static readonly ProfilePropertyType<string> CampaignSource = new ProfilePropertyType<string>("Campaign Source", mixpanelOnly, all);

        public static readonly ProfilePropertyType<string> CampaignName = new ProfilePropertyType<string>("Campaign Name", mixpanelOnly, all);

        public static readonly ProfilePropertyType<string> CampaignMedium = new ProfilePropertyType<string>("Campaign Medium", mixpanelOnly, all);

        public static readonly ProfilePropertyType<string> CampaignContent = new ProfilePropertyType<string>("Campaign Content", mixpanelOnly, all);                
        
		public static readonly ProfilePropertyType<string> Country = new ProfilePropertyType<string>("Country", mixpanelOnly, all);
        
		public static readonly ProfilePropertyType<string> Language = new ProfilePropertyType<string>("Language", mixpanelOnly, all);        		

        /// IMPORTANT: If you add anything to this class you also need to update the related documentation: 
        /// "O:\Software Development\Documentation\Functional specification\20140223 - Analytics Reporting\Analytics Events.docx"
    }
}
