﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Analytics
{
    public class EventType
    {
        internal EventType(string category, string name, ReadOnlyCollection<string> reportToConnectors, params string[] synonyms)
        {
            this.Category = category;
            this.Name = name;
            this.ReportToConnectors = new ReadOnlyCollection<string>(reportToConnectors);

            // GAKR This has been introduced to support backwards compatability if we rename Events that we report to the Googz
            List<string> nameAndSynonyms = new List<string>();
            nameAndSynonyms.Add(name);

            if (synonyms != null)
                foreach (string synonym in synonyms)
                    nameAndSynonyms.Add(synonym);
            this.NameAndSynonyms = new ReadOnlyCollection<string>(nameAndSynonyms);
        }

        public void SetName(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Shortcut for case insensitive 'Contains'
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool NameAndSynonymsContains(string value)
        {
            return this.NameAndSynonyms.Contains(value, StringComparer.OrdinalIgnoreCase);
        }

        public readonly string Category;
        public string Name;
        public readonly ReadOnlyCollection<string> ReportToConnectors;
        public readonly ReadOnlyCollection<string> NameAndSynonyms;
    }
}
