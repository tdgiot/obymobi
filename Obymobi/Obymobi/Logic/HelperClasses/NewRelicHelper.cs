﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Net;
using System.Text;
using Dionysos;

namespace Obymobi.Logic.HelperClasses
{
    public static class NewRelicHelper
    {
        public static string SendDeploymentInformation(string apiKey, string applicationName, string user, string description)
        {
            if (apiKey.IsNullOrWhiteSpace())
                throw new Exception("NewRelic.ApiKey not set!");

            using (var wb = new WebClient())
            {
                wb.Proxy = null;
                wb.Headers.Add("x-api-key", apiKey);

                var data = new NameValueCollection();
                data["deployment[app_name]"] = applicationName;
                data["deployment[description]"] = description;
                data["deployment[user]"] = user;

                var response = wb.UploadValues("https://api.newrelic.com/deployments.xml", "POST", data);
                
                return Encoding.Default.GetString(response);
            }
        }

        public static void TraceThis()
        {
            Debug.WriteLine("NewRelicHelper.TraceThis");
        }
    }
}
