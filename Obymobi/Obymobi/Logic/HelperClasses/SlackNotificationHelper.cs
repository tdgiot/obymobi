﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Web.Configuration;
using Newtonsoft.Json;
using Obymobi.Cms.Logic.Models;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Logic.HelperClasses
{
    public class SlackNotificationHelper
    {
        public static void PostNotificationToSlackWebHook(string notificationText, UserEntity user)
        {
            string slackWebhookNotificationUrl = WebConfigurationManager.AppSettings["SlackWebhookNotificationUrl"];

            if (string.IsNullOrWhiteSpace(slackWebhookNotificationUrl))
            {
                throw new ArgumentNullException(nameof(slackWebhookNotificationUrl));
            }
            else if (string.IsNullOrWhiteSpace(notificationText))
            {
                throw new ArgumentNullException(nameof(notificationText));
            }

            ForePublishSlackNotification slackNotificationModel = new ForePublishSlackNotification(notificationText, user.Username);

            ServicePointManager.Expect100Continue = true;

            WebRequest webRequest = WebRequest.Create(slackWebhookNotificationUrl);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.AuthenticationLevel = AuthenticationLevel.None;

            using (StreamWriter streamWriter = new StreamWriter(webRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(slackNotificationModel);

                streamWriter.Write(json);
            }

            _ = webRequest.GetResponse();
        }
    }
}
