﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Obymobi.Enums;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// XmlHelper class
    /// </summary>
    public class XmlHelper
    {
        /// <summary>
        /// Serialize a object to a string using the XmlSerializer
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <param name="removeWhiteSpace">if set to <c>true</c> [remove white space].</param>
        /// <returns>
        /// String
        /// </returns>
		public static string Serialize(Object obj, bool removeWhiteSpace = false)
		{
			string toReturn = string.Empty;
			try
			{
				XmlSerializer s = new XmlSerializer(obj.GetType());

				MemoryStream m = new MemoryStream();
				s.Serialize(m, obj);

				StreamReader r = new StreamReader(m);
				m.Position = 0;
				toReturn = r.ReadToEnd();

                if (removeWhiteSpace)
                {
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.PreserveWhitespace = false;                    
                    doc.LoadXml(toReturn);
                    toReturn = doc.OuterXml;
                }
			}
			catch (Exception ex)
			{
				throw new ObymobiException(XmlHelperResult.SerializeError, ex, "Model: {0}", obj.GetType());
			}

			return toReturn;
		}

        /// <summary>
        /// Deserialize any type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <param name="flexWorkaround">Flex workaround, see: https://bugs.adobe.com/jira/browse/SDK-18326 for more information. Default is true/param>
        /// <returns></returns>
        public static T Deserialize<T>(string xml, bool flexWorkaround = true)
		{
            if (flexWorkaround)
            {
                // Flex bug workaround, see: https://bugs.adobe.com/jira/browse/SDK-18326
                xml = xml.Replace("&lt;", string.Empty);
                xml = xml.Replace("&gt;", string.Empty);
            }

            xml = RemoveInvalidXmlChars(xml);

		    XmlSerializer xmlSerializer;
			T toReturn;
			try
			{
				xmlSerializer = new XmlSerializer(typeof(T));
				toReturn = (T)xmlSerializer.Deserialize(new StringReader(xml));
			}
			catch (Exception ex)
			{
			    if (xml.Length < 1000)
			    {
			        throw new ObymobiException(XmlHelperResult.SerializeError, ex, "Type: {0}\r\nXml: {1}", typeof(T).FullName, xml);
			    }

			    throw new ObymobiException(XmlHelperResult.SerializeError, ex, "Type: {0}", typeof(T).FullName);
			}

            //if (toReturn is ModelBase)
            //{
            //    var model = toReturn as ModelBase;
            //    model.DeserializedFromXml = xml;
            //}                

			return toReturn;
		}

        private static string RemoveInvalidXmlChars(string text)
        {
            var validXmlChars = text.Where(ch => XmlConvert.IsXmlChar(ch)).ToArray();
            return new string(validXmlChars);
        }
    }
}
