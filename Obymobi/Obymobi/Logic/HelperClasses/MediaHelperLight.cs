﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.HelperClasses
{
    public static class MediaHelperLight
    {
        /// <summary>
        /// Gets the correct media type if the supplied mediaType was generic (i.e. MediaType.GenericproductButton becomes MediaType.ProductButton), otherwise does nothing.
        /// </summary>
        /// <param name="mediaType">Type of the media.</param>
        /// <returns></returns>
        public static MediaType GetCorrectMediaTypeIfGeneric(MediaType mediaType)
        {
            MediaType toReturn = mediaType;

            switch (mediaType)
            {
                case MediaType.GenericproductButton:
                    toReturn = MediaType.ProductButton;
                    break;
                case MediaType.GenericproductBranding:
                    toReturn = MediaType.ProductBranding;
                    break;
                case MediaType.GenericcategoryButton:
                    toReturn = MediaType.CategoryButton;
                    break;
                case MediaType.AlterationImage800x480:
                    toReturn = MediaType.AlterationImage800x480;
                    break; 
                case MediaType.GenericProductButton800x480:
                    toReturn = MediaType.ProductButton800x480;
                    break;
                case MediaType.GenericProductBranding800x480:
                    toReturn = MediaType.ProductBranding800x480;
                    break;
                case MediaType.CraveGenericProductBrandingMobile:
                    toReturn = MediaType.CraveProductBrandingMobile;
                    break;
                case MediaType.GenericProductBranding1280x800:
                    toReturn = MediaType.ProductBranding1280x800;
                    break;
                case MediaType.GenericProductButton1280x800:
                    toReturn = MediaType.ProductButton1280x800;
                    break;
                case MediaType.GenericProductPagePhotoHorizontal1280x800:
                    toReturn = MediaType.ProductPagePhotoHorizontal1280x800;
                    break;
                case MediaType.GenericProductPagePhotoVertical1280x800:
                    toReturn = MediaType.ProductPagePhotoVertical1280x800;
                    break;
                case MediaType.GenericProductIcon1280x800:
                    toReturn = MediaType.ProductIcon1280x800;
                    break;
                case MediaType.GenericProductBranding1280x720:
                    toReturn = MediaType.ProductBranding1280x720;
                    break;
                case MediaType.GenericProductButton1280x720:
                    toReturn = MediaType.ProductButton1280x720;
                    break;
                // BYOD Category Branding
                case MediaType.GenericCategoryBrandingiPad:
                    toReturn = MediaType.CategoryBrandingiPad;
                    break;
                case MediaType.GenericCategoryBrandingiPadRetina:
                    toReturn = MediaType.CategoryBrandingiPadRetina;
                    break;
                case MediaType.GenericCategoryBrandingiPhone:
                    toReturn = MediaType.CategoryBrandingiPhone;
                    break;
                case MediaType.GenericCategoryBrandingiPhoneRetina:
                    toReturn = MediaType.CategoryBrandingiPhoneRetina;
                    break;
                case MediaType.GenericCategoryBrandingPhoneSmall:
                    toReturn = MediaType.CategoryBrandingPhoneSmall;
                    break;
                case MediaType.GenericCategoryBrandingPhoneNormal:
                    toReturn = MediaType.CategoryBrandingPhoneNormal;
                    break;
                case MediaType.GenericCategoryBrandingPhoneLarge:
                    toReturn = MediaType.CategoryBrandingPhoneLarge;
                    break;
                case MediaType.GenericCategoryBrandingPhoneXLarge:
                    toReturn = MediaType.CategoryBrandingPhoneXLarge;
                    break;
                case MediaType.GenericCategoryBrandingTabletSmall:
                    toReturn = MediaType.CategoryBrandingTabletSmall;
                    break;
                case MediaType.GenericCategoryBrandingTabletNormal:
                    toReturn = MediaType.CategoryBrandingTabletNormal;
                    break;
                case MediaType.GenericCategoryBrandingTabletLarge:
                    toReturn = MediaType.CategoryBrandingTabletLarge;
                    break;
                case MediaType.GenericCategoryBrandingTabletXLarge:
                    toReturn = MediaType.CategoryBrandingTabletXLarge;
                    break;
                // BYOD Category Button
                case MediaType.GenericCategoryButtoniPad:
                    toReturn = MediaType.CategoryButtoniPad;
                    break;
                case MediaType.GenericCategoryButtoniPadRetina:
                    toReturn = MediaType.CategoryButtoniPadRetina;
                    break;
                case MediaType.GenericCategoryButtoniPhone:
                    toReturn = MediaType.CategoryButtoniPhone;
                    break;
                case MediaType.GenericCategoryButtoniPhoneRetina:
                    toReturn = MediaType.CategoryButtoniPhoneRetina;
                    break;
                case MediaType.GenericCategoryButtonPhoneSmall:
                    toReturn = MediaType.CategoryButtonPhoneSmall;
                    break;
                case MediaType.GenericCategoryButtonPhoneNormal:
                    toReturn = MediaType.CategoryButtonPhoneNormal;
                    break;
                case MediaType.GenericCategoryButtonPhoneLarge:
                    toReturn = MediaType.CategoryButtonPhoneLarge;
                    break;
                case MediaType.GenericCategoryButtonPhoneXLarge:
                    toReturn = MediaType.CategoryButtonPhoneXLarge;
                    break;
                case MediaType.GenericCategoryButtonTabletSmall:
                    toReturn = MediaType.CategoryButtonTabletSmall;
                    break;
                case MediaType.GenericCategoryButtonTabletNormal:
                    toReturn = MediaType.CategoryButtonTabletNormal;
                    break;
                case MediaType.GenericCategoryButtonTabletLarge:
                    toReturn = MediaType.CategoryButtonTabletLarge;
                    break;
                case MediaType.GenericCategoryButtonTabletXLarge:
                    toReturn = MediaType.CategoryButtonTabletXLarge;
                    break;
                // BYOD Product Branding
                case MediaType.GenericProductBrandingiPad:
                    toReturn = MediaType.ProductBrandingiPad;
                    break;
                case MediaType.GenericProductBrandingiPadRetina:
                    toReturn = MediaType.ProductBrandingiPadRetina;
                    break;
                case MediaType.GenericProductBrandingiPhone:
                    toReturn = MediaType.ProductBrandingiPhone;
                    break;
                case MediaType.GenericProductBrandingiPhoneRetina:
                    toReturn = MediaType.ProductBrandingiPhoneRetina;
                    break;
				case MediaType.GenericProductBrandingiPhoneRetinaHD: // BBFIX 2014.11.09 Added missing HD and HDPlus ones
					toReturn = MediaType.ProductBrandingiPhoneRetinaHD;
					break;
				case MediaType.GenericProductBrandingiPhoneRetinaHDPlus:
					toReturn = MediaType.ProductBrandingiPhoneRetinaHDPlus;
					break;
                case MediaType.GenericProductBrandingPhoneSmall:
                    toReturn = MediaType.ProductBrandingPhoneSmall;
                    break;
                case MediaType.GenericProductBrandingPhoneNormal:
                    toReturn = MediaType.ProductBrandingPhoneNormal;
                    break;
                case MediaType.GenericProductBrandingPhoneLarge:
                    toReturn = MediaType.ProductBrandingPhoneLarge;
                    break;
                case MediaType.GenericProductBrandingPhoneXLarge:
                    toReturn = MediaType.ProductBrandingPhoneXLarge;
                    break;
                case MediaType.GenericProductBrandingTabletSmall:
                    toReturn = MediaType.ProductBrandingTabletSmall;
                    break;
                case MediaType.GenericProductBrandingTabletNormal:
                    toReturn = MediaType.ProductBrandingTabletNormal;
                    break;
                case MediaType.GenericProductBrandingTabletLarge:
                    toReturn = MediaType.ProductBrandingTabletLarge;
                    break;
                case MediaType.GenericProductBrandingTabletXLarge:
                    toReturn = MediaType.ProductBrandingTabletXLarge;
                    break;
                // BYOD Product Button
                case MediaType.GenericProductButtoniPad:
                    toReturn = MediaType.ProductButtoniPad;
                    break;
                case MediaType.GenericProductButtoniPadRetina:
                    toReturn = MediaType.ProductButtoniPadRetina;
                    break;
                case MediaType.GenericProductButtoniPhone:
                    toReturn = MediaType.ProductButtoniPhone;
                    break;
                case MediaType.GenericProductButtoniPhoneRetina:
                    toReturn = MediaType.ProductButtoniPhoneRetina;
                    break;
				case MediaType.GenericProductButtoniPhoneRetinaHD: // BBFIX 2014.11.09 Added missing HD and HDPlus ones
					toReturn = MediaType.ProductButtoniPhoneRetinaHD;
					break;
				case MediaType.GenericProductButtoniPhoneRetinaHDPlus:
					toReturn = MediaType.ProductButtoniPhoneRetinaHDPlus;
					break;
                case MediaType.GenericProductButtonPhoneSmall:
                    toReturn = MediaType.ProductButtonPhoneSmall;
                    break;
                case MediaType.GenericProductButtonPhoneNormal:
                    toReturn = MediaType.ProductButtonPhoneNormal;
                    break;
                case MediaType.GenericProductButtonPhoneLarge:
                    toReturn = MediaType.ProductButtonPhoneLarge;
                    break;
                case MediaType.GenericProductButtonPhoneXLarge:
                    toReturn = MediaType.ProductButtonPhoneXLarge;
                    break;
                case MediaType.GenericProductButtonTabletSmall:
                    toReturn = MediaType.ProductButtonTabletSmall;
                    break;
                case MediaType.GenericProductButtonTabletNormal:
                    toReturn = MediaType.ProductButtonTabletNormal;
                    break;
                case MediaType.GenericProductButtonTabletLarge:
                    toReturn = MediaType.ProductButtonTabletLarge;
                    break;
                case MediaType.GenericProductButtonTabletXLarge:
                    toReturn = MediaType.ProductButtonTabletXLarge;
                    break;
                default:
                    break;
                // IRT Sites (Samsung, Sony Z1, Sony Z2, Crave Tablet)
                case MediaType.TemplateSitePageIcon1280x800:
                    toReturn = MediaType.SitePageIcon1280x800;
                    break;
                case MediaType.TemplateSiteFullPageImage1280x800:
                    toReturn = MediaType.SiteFullPageImage1280x800;
                    break;
                case MediaType.TemplateSiteFullPageImageDirectory1280x800:
                    toReturn = MediaType.SiteFullPageImageDirectory1280x800;
                    break;
                case MediaType.TemplateSiteTwoThirdsPageImage1280x800:
                    toReturn = MediaType.SiteTwoThirdsPageImage1280x800;
                    break;
                case MediaType.TemplateSiteTwoThirdsPageImageDirectory1280x800:
                    toReturn = MediaType.SiteTwoThirdsPageImageDirectory1280x800;
                    break;
                // IRT Sites (Android TV Box)
                case MediaType.TemplateSitePageIcon1280x720:
                    toReturn = MediaType.SitePageIcon1280x720;
                    break;
                case MediaType.TemplateSiteFullPageImage1280x720:
                    toReturn = MediaType.SiteFullPageImage1280x720;
                    break;
                case MediaType.TemplateSiteFullPageImageDirectory1280x720:
                    toReturn = MediaType.SiteFullPageImageDirectory1280x720;
                    break;
                case MediaType.TemplateSiteTwoThirdsPageImage1280x720:
                    toReturn = MediaType.SiteTwoThirdsPageImage1280x720;
                    break;
                case MediaType.TemplateSiteTwoThirdsPageImageDirectory1280x720:
                    toReturn = MediaType.SiteTwoThirdsPageImageDirectory1280x720;
                    break;
                // PhoneSmall Sites
                case MediaType.TemplateSitePageIconPhoneSmall:
                    toReturn = MediaType.SitePageIconPhoneSmall;
                    break;
                case MediaType.TemplateSiteMobileHeaderImagePhoneSmall:
                    toReturn = MediaType.SiteMobileHeaderImagePhoneSmall;
                    break;
                // PhoneNormal Sites
                case MediaType.TemplateSitePageIconPhoneNormal:
                    toReturn = MediaType.SitePageIconPhoneNormal;
                    break;
                case MediaType.TemplateSiteMobileHeaderImagePhoneNormal:
                    toReturn = MediaType.SiteMobileHeaderImagePhoneNormal;
                    break;
                // PhoneLarge Sites
                case MediaType.TemplateSitePageIconPhoneLarge:
                    toReturn = MediaType.SitePageIconPhoneLarge;
                    break;
                case MediaType.TemplateSiteMobileHeaderImagePhoneLarge:
                    toReturn = MediaType.SiteMobileHeaderImagePhoneLarge;
                    break;
                // PhoneXLarge Sites
                case MediaType.TemplateSitePageIconPhoneXLarge:
                    toReturn = MediaType.SitePageIconPhoneXLarge;
                    break;
                case MediaType.TemplateSiteMobileHeaderImagePhoneXLarge:
                    toReturn = MediaType.SiteMobileHeaderImagePhoneXLarge;
                    break;
                // TabletSmall Sites
                case MediaType.TemplateSitePageIconTabletSmall:
                    toReturn = MediaType.SitePageIconTabletSmall;
                    break;
                // TabletNormal Sites
                case MediaType.TemplateSitePageIconTabletNormal:
                    toReturn = MediaType.SitePageIconTabletNormal;
                    break;
                // TabletLarge Sites
                case MediaType.TemplateSitePageIconTabletLarge:
                    toReturn = MediaType.SitePageIconTabletLarge;
                    break;
                // TabletXLarge Sites
                case MediaType.TemplateSitePageIconTabletXLarge:
                    toReturn = MediaType.SitePageIconTabletXLarge;
                    break;
                // Iphone Sites
                case MediaType.TemplateSitePageIconiPhone:
                    toReturn = MediaType.SitePageIconiPhone;
                    break;
                case MediaType.TemplateSiteMobileHeaderImageiPhone:
                    toReturn = MediaType.SiteMobileHeaderImageiPhone;
                    break;
                // IphoneRetina Sites
                case MediaType.TemplateSitePageIconiPhoneRetina:
                    toReturn = MediaType.SitePageIconiPhoneRetina;
                    break;
                case MediaType.TemplateSiteMobileHeaderImageiPhoneRetina:
                    toReturn = MediaType.SiteMobileHeaderImageiPhoneRetina;
                    break;
                // Ipad Sites
                case MediaType.TemplateSitePageIconiPad:
                    toReturn = MediaType.SitePageIconiPad;
                    break;
                case MediaType.TemplateSiteFullPageImageiPad:
                    toReturn = MediaType.SiteFullPageImageiPad;
                    break;
                case MediaType.TemplateSiteTwoThirdsPageImageiPad:
                    toReturn = MediaType.SiteTwoThirdsPageImageiPad;
                    break;
                case MediaType.TemplateSiteCinemaWideScreeniPad:
                    toReturn = MediaType.SiteCinemaWideScreeniPad;
                    break;
                // IpadRetina Sites
                case MediaType.TemplateSitePageIconiPadRetina:
                    toReturn = MediaType.SitePageIconiPadRetina;
                    break;
                case MediaType.TemplateSiteFullPageImageiPadRetina:
                    toReturn = MediaType.SiteFullPageImageiPadRetina;
                    break;
                case MediaType.TemplateSiteTwoThirdsPageImageiPadRetina:
                    toReturn = MediaType.SiteTwoThirdsPageImageiPadRetina;
                    break;
                case MediaType.TemplateSiteCinemaWideScreeniPadRetina:
                    toReturn = MediaType.SiteCinemaWideScreeniPadRetina;
                    break;
                // IphoneRetinaHD Sites
                case MediaType.TemplateSitePageIconiPhoneRetinaHD:
                    toReturn = MediaType.SitePageIconiPhoneRetinaHD;
                    break;
                case MediaType.TemplateSiteMobileHeaderImageiPhoneRetinaHD:
                    toReturn = MediaType.SiteMobileHeaderImageiPhoneRetinaHD;
                    break;
                // IphoneRetinaHDPlus Sites
                case MediaType.TemplateSitePageIconiPhoneRetinaHDPlus:
                    toReturn = MediaType.SitePageIconiPhoneRetinaHDPlus;
                    break;
                case MediaType.TemplateSiteMobileHeaderImageiPhoneRetinaHDPlus:
                    toReturn = MediaType.SiteMobileHeaderImageiPhoneRetinaHDPlus;
                    break;
            }

            return toReturn;
        }
    }
}
