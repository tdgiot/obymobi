﻿using System;
using System.Collections.Generic;
using Obymobi.Enums;
using System.IO;
using Dionysos;
using Obymobi.Constants;
using Obymobi.Configuration;
using System.Net;
using Obymobi.Logic.Model;
using System.Runtime.CompilerServices;
using Dionysos.Web;
using System.Diagnostics;
using System.Threading;

namespace Obymobi.Logic.Status
{
    public class CloudStatus : LoggingClassBase
    {
        #region Result Enum

        public enum CloudStatusResult : int
        {
            PropertiesRequestedBeforeInitialRefresh = 200,
            NonImplementedOrIncorrectCloudEnvironment = 201
        }

        #endregion

        #region Fields

        private bool isInternetAvailable = true;
        private bool isWebserviceAvailable = true;
        private bool hasBeenRefreshedAtLeastOnce = false;
        private DateTime? lastRefresh = null;
        private bool isRefreshing = false;
        private bool statusFileAvailable = false;
        private string statusFileContents = string.Empty;
        private NocStatusReportBase lastNocStatusReport = null;
        private ClientCommunicationMethod communicationMethod = ClientCommunicationMethod.All;
        private bool httpsRequired = false;

        private string nocStatusTextFile = string.Empty;

        private const int INDEX_ENVIRONMENT = 0;
        private const int INDEX_ALTERNATIVE_URL = 1;
        private const int INDEX_CLIENTCOMMUNICATION_GLOBAL = 4;
        private const int INDEX_CLIENTCOMMUNICATION_COMPANY = 5;
        private const int INDEX_HTTPS_REQUIRED = 6;

        private bool httpsRequiredFlagSet = false;
        private bool firstRefresh;

        private bool checkInternetAvailability = true;
        private UrlProtocol urlProtocol = UrlProtocol.Default;

        #endregion

        #region Constructors

        public CloudStatus()
        {
            this.firstRefresh = true;
        }

        public CloudStatus(bool optimisticInit)
        {
            if (optimisticInit)
            {
                this.isInternetAvailable = true;
                this.isWebserviceAvailable = true;
                this.hasBeenRefreshedAtLeastOnce = true;
            }
        }

        #endregion

        #region Methods

        [MethodImpl(MethodImplOptions.Synchronized)]
        public virtual void Refresh()
        {
            this.isRefreshing = true;

            try
            {
                // Register pre-Refresh values
                CloudEnvironment enviromentBeforeRefresh = this.CloudEnvironment;
                string manualUrlBeforeRefresh = this.ManualWebserviceBaseUrl;
                string productionOverwriteUrlBeforeRefresh = this.ProductionOverwriteWebserviceBaseUrl;
                this.hasBeenRefreshedAtLeastOnce = true;

                // If in production, get the current production environment
                bool nocStatusTextFileAvailable = false;

                // Try 3 times, because it's almost impossible it's not online
                // AND it's very important
                int loopSleepLength = 1500;
                int maxLoops = 3;
                int loopsPerformed = 0;
                while (loopsPerformed < maxLoops)
                {
                    try
                    {
                        if (loopsPerformed > 0)
                            Thread.Sleep(loopSleepLength);

                        Log.Verbose("CloudStatus", "refresh", "updateCurrentProductionEnvironment attempt {0} of {1}.", loopsPerformed + 1, maxLoops);
                        nocStatusTextFileAvailable = this.UpdateCurrentProductionEnvironment();

                        if (nocStatusTextFileAvailable)
                            break;
                    }
                    catch (Exception ex)
                    {
                        this.LogError("An exception occurred while trying to refresh the cloud status. Message: {0}", ex.Message);
                    }
                    finally
                    {
                        loopsPerformed++;
                    }
                }

                // Refresh the webservice availability
                // regardless of the internetAvailable flag
                this.RefreshWebserviceAvailability();

                if (this.IsWebserviceAvailable)
                {
                    // Webservice is available
                    // so we imply that internet is available too
                    this.IsInternetAvailable = true;
                }
                else if (nocStatusTextFileAvailable)
                {
                    // We received a response from one of the noc status text files
                    // so we imply that internet is available too
                    this.IsInternetAvailable = true;
                }
                else
                {
                    // Webservice could not be reached
                    // Noc status text files could not be reached as well

                    // Therefore, refresh the internet availability
                    this.RefreshInternetAvailability();
                }


                // Notify if the Webserivue Url changed, due to a switch of environment or change of custom url
                if (this.CloudEnvironment != enviromentBeforeRefresh ||
                    this.CloudEnvironment == Enums.CloudEnvironment.Manual && !this.ManualWebserviceBaseUrl.Equals(manualUrlBeforeRefresh) ||
                    this.CloudEnvironment == Enums.CloudEnvironment.ProductionOverwrite && !this.ProductionOverwriteWebserviceBaseUrl.Equals(productionOverwriteUrlBeforeRefresh))
                {
                    this.OnWebserviceUrlChanged();
                }

                if (this.IsWebserviceAvailable && this.IsInternetAvailable)
                    this.LogDebug("OK - (Environment: {0}, BaseUrl: {1}, Webservice: {2}, Internet: {3}, ClientCommunication: {4})", this.CloudEnvironment.ToString(), this.WebserviceBaseUrl, this.IsWebserviceAvailable, this.IsInternetAvailable, this.ClientCommunication);
                else
                    this.LogDebug("NOT OK - (Environment: {0}, BaseUrl: {1}, Webservice: {2}, Internet: {3}, ClientCommunication: {4})", this.CloudEnvironment.ToString(), this.WebserviceBaseUrl, this.IsWebserviceAvailable, this.IsInternetAvailable, this.ClientCommunication);

                this.lastRefresh = DateTime.UtcNow;
            }
            finally
            {
                this.isRefreshing = false;
            }
        }

        private bool UpdateCurrentProductionEnvironment()
        {
            bool internetAvailable = false;
            List<string> textFileUrls = new List<string>();

            List<string> cdnUrls = this.CdnBaseUrls;

            if (!this.nocStatusTextFile.IsNullOrWhiteSpace())
            {
                foreach (string url in cdnUrls)
                {
                    textFileUrls.Add(url + this.nocStatusTextFile);

                    if (url.Contains("https"))
                        textFileUrls.Add(url.Replace("https", "http") + this.nocStatusTextFile); // Fallback to HTTP in case HTTPS does not work
                }
            }

            // Backwards compatibility to global NOC status file
            foreach (string url in cdnUrls)
            {
                textFileUrls.Add(url + ObymobiConstants.NocStatusTextFile);

                if (url.Contains("https"))
                    textFileUrls.Add(url.Replace("https", "http") + ObymobiConstants.NocStatusTextFile); // Fallback to HTTP in case HTTPS does not work
            }

            bool success = false;
            this.statusFileAvailable = false;
            this.statusFileContents = string.Empty;
            this.lastNocStatusReport = null;

            // Iterate over NocStatusServices
            foreach (var url in textFileUrls)
            {
                // Try to get the file with the status from the Noc Status Service and check it's contents
                try
                {
                    // Add a cache-breaker
                    string cacheSaveUrl = string.Format("{0}?update={1}", url, DateTime.UtcNow.Ticks);

                    WebRequest request = WebRequest.Create(cacheSaveUrl);
                    request.Proxy = null;
                    var response = (HttpWebResponse)request.GetResponse();

                    if (response != null && response.StatusCode == HttpStatusCode.OK)
                    {
                        internetAvailable = true;

                        // Validate it's the correct type of content
                        StreamReader sr = new StreamReader(response.GetResponseStream());
                        string content = sr.ReadToEnd();

                        this.statusFileContents = content;
                        success = NocStatusReportBase.ValidateStatusString(content);
                        this.statusFileAvailable = success;

                        // With a valid StatusReport continue
                        if (success)
                        {
                            // Get the NocStatusReport for the CloudEnvironment and Alteranative Webservice Url if required                            
                            var report = new NocStatusReportBase(content);
                            CloudEnvironment environment;
                            this.lastNocStatusReport = report;

                            if (EnumUtil.TryParse(report.Environment, out environment) && Enum.IsDefined(typeof(CloudEnvironment), environment))
                            {
                                if (environment == CloudEnvironment.ProductionOverwrite)
                                {
                                    // Overwrite in Production
                                    this.CloudEnvironment = environment;
                                    this.ProductionOverwriteWebserviceBaseUrl = report.AlternativeUrl;
                                }
                                else if (environment == CloudEnvironment.Manual)
                                {
                                    // Do nothing, because it's a development environment
                                }
                                else
                                {
                                    // Non-overwrite cloud environment
                                    this.CloudEnvironment = environment;
                                    this.ProductionOverwriteWebserviceBaseUrl = string.Empty;
                                }

                                // Set communication methods
                                ClientCommunicationMethod clientCommunicationMethod = (ClientCommunicationMethod)report.ClientCommunication;
                                if (clientCommunicationMethod == ClientCommunicationMethod.None)
                                {
                                    this.ClientCommunication = ClientCommunicationMethod.All;
                                }
                                else
                                {
                                    this.ClientCommunication = clientCommunicationMethod;
                                }

                                // Set the HTTPS required flag
                                this.HttpsRequired = (report.HttpsRequired > 0);
                                this.httpsRequiredFlagSet = true;
                            }
                            else
                            {
                                // Invalid Environment
                                success = false;
                                this.LogWarning("Received a non-existent Environment from Noc Status Service: '{0}' - IGNORED", report.Environment);
                            }
                        }
                        else
                        {
                            this.LogWarning("Received a non-compatible Noc Status Report Environment from Noc Status Service: '{0}' - IGNORED", content);
                        }
                    }

                    response.Close();
                }
                catch (Exception ex)
                {
                    this.LogError("Received an Exception while trying to get status from NOC Status Service '{0}': '{1}'", url, ex.GetAllMessages());
                }

                if (!success)
                {
                    // We couldn't get Noc Status, just continue on old version
                    this.LogError("ERROR: Couldn't get status from Noc StatusService, continue on last known settings.", LoggingLevel.Warning);
                }
                else
                {
                    break;
                }
            }

            return internetAvailable;
        }

        private void RefreshWebserviceAvailability()
        {
            bool success = false;
            string urlAppendix = "IsAlive?update={0}".FormatSafe(DateTime.UtcNow.Ticks);
            string serviceUrl = this.CraveServiceUrl;
            string url = WebPathCombiner.CombinePaths(serviceUrl, urlAppendix);

            // Try to get the file with the status from the Noc Status Service and check it's contents
            HttpWebResponse response;
            try
            {
                WebRequest request = WebRequest.Create(url);

                request.Timeout = 5000;
                if (TestUtil.IsPcDeveloper && this.firstRefresh)
                {
                    request.Timeout = 60 * 1000;
                    this.LogDebug("Time out is set to 60 seconds which allows for enough time to compile the webservice if required.");
                    this.firstRefresh = false;
                }
                request.Proxy = null;
                response = (HttpWebResponse)request.GetResponse();

                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    // Validate it's the correct type of content
                    StreamReader sr = new StreamReader(response.GetResponseStream());
                    string content = sr.ReadToEnd();

                    if (content.Contains("true", StringComparison.CurrentCultureIgnoreCase))
                        success = true;
                }

                if (response != null)
                    response.Close();
            }
            catch (Exception ex)
            {
                this.LogWarning("Webservice not available on: '{0}'", this.CraveServiceUrl);
                this.LogWarning("Exception: '{0}'", ex.Message);

                if (TestUtil.IsPcDeveloper && !TestUtil.IsPcGabriel && !TestUtil.IsPcBattleStationDanny && !TestUtil.IsPcMathieu && !TestUtil.IsPcFloris)
                    throw;
            }

            this.IsWebserviceAvailable = success;

            if (!this.IsWebserviceAvailable)
                this.LogError("Webservice is not available on: {0}".FormatSafe(url));

        }

        private void RefreshInternetAvailability()
        {
            if (this.checkInternetAvailability)
            {
                List<string> urlsToCheck = new List<string> { "http://checkip.dyndns.org", "http://myip.dnsomatic.com/", "http://icanhazip.com/", "http://www.google.com", "http://www.bing.com" };

                foreach (var url in urlsToCheck)
                {
                    var sw = new Stopwatch();
                    sw.Start();

                    try
                    {
                        WebRequest request = WebRequest.Create(url);
                        request.Proxy = null;
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                        if (response != null && response.StatusCode == HttpStatusCode.OK)
                        {
                            // Internet is online ;)
                            this.IsInternetAvailable = true;
                        }
                    }
                    catch
                    {
                        this.IsInternetAvailable = false;
                    }

                    sw.Stop();
                    Log.Debug("CloudStatus", "RefreshInternetAvailability", "{0}ms @ {1}", sw.ElapsedMilliseconds, url);

                    if (this.IsInternetAvailable)
                        break;
                    else
                        this.LogWarning("Checking internet connection - Failed to connect to: {0}", url);
                }

                if (!this.IsInternetAvailable)
                    this.LogError("Internet is not available");
            }
            else
            {
                Log.Info("CloudStatus", "RefreshInternetAvailability", "Internet availability check was disabled.");
                this.IsInternetAvailable = true;
            }
        }

        private string GetUrl(string url)
        {
            if (this.urlProtocol == UrlProtocol.Default && !this.HttpsRequired)
            {
                url = url.Replace("https://", "http://");
            }
            else if (this.urlProtocol == UrlProtocol.Http)
            {
                url = url.Replace("https://", "http://");
            }
            else if (this.urlProtocol == UrlProtocol.Https)
            {
                url = url.Replace("http://", "https://");
            }

            return url;
        }

        public override string LogPrefix()
        {
            return "CloudStatus";
        }

        #endregion

        #region Properties

        public bool IsProduction
        {
            get
            {
                return (this.CloudEnvironment == Enums.CloudEnvironment.ProductionPrimary ||
                    this.CloudEnvironment == Enums.CloudEnvironment.ProductionSecondary ||
                    this.CloudEnvironment == Enums.CloudEnvironment.ProductionOverwrite);
            }
        }

        /// <summary>
        /// Gets or sets the statusFileContents
        /// </summary>
        public string StatusFileContents
        {
            get
            {
                return this.statusFileContents;
            }
        }


        /// <summary>
        /// Gets or sets the statusFileAvailable
        /// </summary>
        public bool StatusFileAvailable
        {
            get
            {
                return this.statusFileAvailable;
            }
        }


        public bool IsRefreshing
        {
            get
            {
                return this.isRefreshing;
            }
        }

        /// <summary>
        /// Gets or sets the lastRefresh
        /// </summary>
        public DateTime? LastRefresh
        {
            get
            {
                return this.lastRefresh;
            }
        }

        public NocStatusReportBase LastNocStatusReport
        {
            get
            {
                return this.lastNocStatusReport;
            }
        }

        public string NocStatusTextFile
        {
            get
            {
                return this.nocStatusTextFile;
            }
            set
            {
                this.nocStatusTextFile = value;
            }
        }


        /// <summary>
        /// Gets or sets the PropertyName
        /// </summary>
        public CloudEnvironment CloudEnvironment
        {
            get
            {
                string value = Dionysos.ConfigurationManager.GetString(CraveCloudConfigConstants.CloudEnvironment);
                CloudEnvironment environment;
                if (!EnumUtil.TryParse(value, out environment))
                {
                    throw new ObymobiException(CloudStatusResult.NonImplementedOrIncorrectCloudEnvironment, null, "Not supported: '{0}'", value);
                }

                return environment;
            }
            set
            {
                Dionysos.ConfigurationManager.SetValue(CraveCloudConfigConstants.CloudEnvironment, value.ToString());
            }
        }

        /// <summary>
        /// Gets or sets the ClientCommunicationMethod
        /// </summary>
        public ClientCommunicationMethod ClientCommunication
        {
            get { return this.communicationMethod; }
            set
            {
                if (this.communicationMethod != value)
                {
                    OnClientCommunicationMethodChanged();
                    this.communicationMethod = value;
                }
            }
        }

        public bool HttpsRequired
        {
            get { return this.httpsRequired; }
            set { this.httpsRequired = value; }
        }

        /// <summary>
        /// Get / sets the Manual Webservice Url
        /// </summary>      
        public string ManualWebserviceBaseUrl
        {
            get
            {
                return Dionysos.ConfigurationManager.GetString(CraveCloudConfigConstants.ManualWebserviceBaseUrl);
            }
            set
            {
                Dionysos.ConfigurationManager.SetValue(CraveCloudConfigConstants.ManualWebserviceBaseUrl, value);
            }
        }

        /// <summary>
        /// Get / sets the Production Overwrite Webservice Url
        /// </summary>      
        private string ProductionOverwriteWebserviceBaseUrl
        {
            get
            {
                return Dionysos.ConfigurationManager.GetString(CraveCloudConfigConstants.ProductionOverwriteWebserviceBaseUrl);
            }
            set
            {
                Dionysos.ConfigurationManager.SetValue(CraveCloudConfigConstants.ProductionOverwriteWebserviceBaseUrl, value);
            }
        }

        /// <summary>
        /// Gets or sets the WebserviceBaseUrl for the CloudEnviroment (which is the path to the Webservice, so including API)
        /// Also when there's an alternative url in use it will be reflected in this property
        /// </summary>
        public string WebserviceBaseUrl
        {
            get
            {
                if (!this.hasBeenRefreshedAtLeastOnce)
                    throw new ObymobiException(CloudStatusResult.PropertiesRequestedBeforeInitialRefresh);

                string url;
                switch (this.CloudEnvironment)
                {
                    case CloudEnvironment.ProductionPrimary:
                        url = ObymobiConstants.PrimaryWebserviceUrl;
                        break;
                    case CloudEnvironment.ProductionSecondary:
                        url = ObymobiConstants.SecondaryWebserviceUrl;
                        break;
                    case CloudEnvironment.ProductionOverwrite:
                        url = this.ProductionOverwriteWebserviceBaseUrl;
                        break;
                    case CloudEnvironment.Test:
                        url = ObymobiConstants.TestWebserviceUrl;
                        break;
                    case CloudEnvironment.Development:
                        url = ObymobiConstants.DevelopmentWebserviceUrl;
                        break;
                    case CloudEnvironment.Demo:
                        url = ObymobiConstants.DemoWebserviceUrl;
                        break;
                    case CloudEnvironment.Staging:
                        url = ObymobiConstants.StagingWebserviceUrl;
                        break;
                    case CloudEnvironment.Manual:
                        url = this.ManualWebserviceBaseUrl;
                        break;
                    default:
                        //this.LogError("No WebserviceBaseUrl available for Environment: '{0}'", this.CloudEnvironment.ToString());
                        //throw new NotImplementedException();

                        // Think positive
                        url = ObymobiConstants.PrimaryWebserviceUrl;
                        this.LogError("No WebserviceBaseUrl available for Environment: '{0}', using primary webservice url '{1}'.", this.CloudEnvironment.ToString(), url);
                        break;
                }

                if (this.httpsRequiredFlagSet)
                    url = this.GetUrl(url);

                return url;
            }
        }

        public string CraveServiceUrl
        {
            get
            {
                var toReturn = string.Empty;

                if (!this.WebserviceBaseUrl.IsNullOrWhiteSpace())
                {
                    toReturn = WebPathCombiner.CombinePaths(this.WebserviceBaseUrl, ObymobiConstants.API_VERSION + "/CraveService.asmx");
                }
                return toReturn;
            }
        }

        public string MobileServiceUrl
        {
            get
            {
                var toReturn = string.Empty;

                if (!this.WebserviceBaseUrl.IsNullOrWhiteSpace())
                {
                    toReturn = WebPathCombiner.CombinePaths(this.WebserviceBaseUrl, ObymobiConstants.API_VERSION + "/MobileService.asmx");
                }
                return toReturn;
            }
        }

        public string MessagingUrl
        {
            get
            {
                // GK Simple approach, also to have it work with subpaths:                
                var toReturn = string.Empty;
                if (!this.WebserviceBaseUrl.IsNullOrWhiteSpace())
                {
                    toReturn = this.WebserviceBaseUrl.Replace(ObymobiConstants.WebAppPathApi, ObymobiConstants.WebAppPathMessaging);
                }
                return toReturn;
            }
        }

        public List<string> CdnBaseUrls
        {
            get
            {
                List<string> toReturn = new List<string>();
                switch (this.CloudEnvironment)
                {
                    case CloudEnvironment.ProductionPrimary:
                    case CloudEnvironment.ProductionSecondary:
                    case CloudEnvironment.ProductionOverwrite:
                        toReturn.Add(ObymobiConstants.PrimaryAmazonCdnBaseUrl);
                        break;
                    case CloudEnvironment.Test:
                        toReturn.Add(ObymobiConstants.TestAmazonCdnBaseUrl);
                        break;
                    case CloudEnvironment.Development:
                        toReturn.Add(ObymobiConstants.DevelopmentAmazonCdnBaseUrl);
                        break;
                    case CloudEnvironment.Staging:
                        toReturn.Add(ObymobiConstants.StagingAmazonCdnBaseUrl);
                        break;
                    case CloudEnvironment.Manual:
                        // Amazon
                        string amazonBaseUrl = Dionysos.ConfigurationManager.GetString(CraveCloudConfigConstants.ManualAmazonCdnBaseUrl);
                        if (!amazonBaseUrl.IsNullOrWhiteSpace())
                            toReturn.Add(amazonBaseUrl);
                        break;
                    default:
                        if (TestUtil.IsPcDeveloper)
                        {
                            this.LogError("No WebserviceBaseUrl available for Environment: '{0}'", this.CloudEnvironment.ToString());
                            throw new NotImplementedException();
                        }

                        // Think positive
                        toReturn.Add(ObymobiConstants.PrimaryAmazonCdnBaseUrlFiles);
                        this.LogError("No WebserviceBaseUrl available for Environment: '{0}', using primary CDN urls.", this.CloudEnvironment.ToString());
                        break;
                }

                if (toReturn.Count == 0)
                    throw new Exception("Zero CDN Urls available, have you configured your Amazon container - If not, you should set the ConfigurationValues in CraveWorld.cs?");

                return toReturn;
            }
        }

        /// <summary>
        /// Gets or sets the isWebserviceAvailable
        /// </summary>
        public bool IsWebserviceAvailable
        {
            get
            {
                if (!this.hasBeenRefreshedAtLeastOnce)
                    throw new ObymobiException(CloudStatusResult.PropertiesRequestedBeforeInitialRefresh);

                return this.isWebserviceAvailable;
            }
            set
            {
                var oldValue = this.isWebserviceAvailable;
                this.isWebserviceAvailable = value;

                if (this.isWebserviceAvailable != oldValue)
                    OnIsWebserviceAvailableChanged();
            }
        }


        /// <summary>
        /// Gets or sets the isInternetAvailable
        /// </summary>
        public bool IsInternetAvailable
        {
            get
            {
                if (!this.hasBeenRefreshedAtLeastOnce)
                    throw new ObymobiException(CloudStatusResult.PropertiesRequestedBeforeInitialRefresh);

                return this.isInternetAvailable;
            }
            set
            {
                var oldValue = this.isInternetAvailable;
                this.isInternetAvailable = value;

                if (this.isInternetAvailable != oldValue)
                    OnIsInternetAvailableChanged();
            }
        }

        public bool CheckInternetAvailability
        {
            get
            {
                return this.checkInternetAvailability;
            }
            set
            {
                this.checkInternetAvailability = value;
            }
        }

        public UrlProtocol UrlProtocol
        {
            get
            {
                return this.urlProtocol;
            }
            set
            {
                this.urlProtocol = value;
            }
        }

        #endregion

        #region Events, Delegates & Related methods

        public event EventHandler IsInternetAvailableChanged;
        public event EventHandler IsWebserviceAvailableChanged;
        public event EventHandler WebserviceUrlChanged;
        public event EventHandler ClientCommunicationChanged;

        /// <summary>
        /// Called when [internet changed].
        /// </summary>
        public void OnIsInternetAvailableChanged()
        {
            if (this.IsInternetAvailable)
                this.LogDebug("IsInternetAvailable changed to: '{0}'", this.IsInternetAvailable);
            else
                this.LogError("IsInternetAvailable changed to: '{0}'", this.IsInternetAvailable);

            if (this.IsInternetAvailableChanged != null)
                this.IsInternetAvailableChanged(null, null);
        }

        /// <summary>
        /// Called when [internet changed].
        /// </summary>
        public void OnIsWebserviceAvailableChanged()
        {
            if (this.IsWebserviceAvailable)
                this.LogDebug("IsWebserviceAvailable changed to: '{0}'", this.IsWebserviceAvailable);
            else
                this.LogError("IsWebserviceAvailable changed to: '{0}'", this.IsWebserviceAvailable);

            if (this.IsWebserviceAvailableChanged != null)
                this.IsWebserviceAvailableChanged(null, null);
        }

        /// <summary>
        /// Called when [internet changed].
        /// </summary>
        public void OnWebserviceUrlChanged()
        {
            this.LogDebug("Webservice Url changed to: '{0}'", this.WebserviceBaseUrl);

            if (this.WebserviceUrlChanged != null)
                this.WebserviceUrlChanged(null, null);
        }

        /// <summary>
        /// Called when [client communication methods changed].
        /// </summary>
        public void OnClientCommunicationMethodChanged()
        {
            this.LogDebug("ClientCommunication changed to: '{0}'", this.ClientCommunication.ToString());

            if (this.ClientCommunicationChanged != null)
                this.ClientCommunicationChanged(null, null);
        }

        #endregion

    }
}
