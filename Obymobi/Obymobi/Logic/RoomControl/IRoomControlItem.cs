﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.RoomControl
{
    public interface IRoomControlItem
    {
        string ItemId { get; }
        string ParentItemId { get; }
        string ItemType { get; }
        string Name { get; set; }
        string NameSystem { get; }
        int SortOrder { get; set; }
        string TypeName { get; }
        bool IsNew { get; set; }
        bool Save();
        bool Delete();
    }
}
