﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile
{
    /// <summary>
    /// Model class which represents a orderitem
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Orderitem"), IncludeInCodeGeneratorForXamarin]
    public class Orderitem : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Orderitem type
        /// </summary>
        public Orderitem()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the orderitem
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int OrderitemId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the order
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int OrderId
        { get; set; }

        /// <summary>
        /// Gets or sets a unique identifier
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Guid
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the category
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int CategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string ProductName
        { get; set; }

        /// <summary>
        /// Gets or sets the quantity of the orderitem
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Quantity
        { get; set; }

        /// <summary>
        /// Gets or sets the price including vat of the orderitem
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public decimal ProductPriceIn
        { get; set; }

        /// <summary>
        /// Gets or sets the quantity of the orderitem
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Notes
        { get; set; }

        /// <summary>
        /// Gets or sets items of the order
        /// </summary>
        [XmlArray("Alterationitems")]
        [XmlArrayItem("Alterationitem")]
        [IncludeInCodeGeneratorForXamarin]
        public Alterationitem[] Alterationitems
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Orderitem Clone()
        {
            return this.Clone<Orderitem>();
        }

        #endregion
    }
}
