﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile
{
    /// <summary>
    /// Model class which represents a media culture
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "MediaCulture"), IncludeInCodeGeneratorForXamarin]
    public class MediaCulture : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.MediaCulture type
        /// </summary>
        public MediaCulture()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the media culture
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]        
        [IncludeInCodeGeneratorForXamarin]
        public int MediaCultureId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the media
        /// </summary>
        [XmlElement]        
        [IncludeInCodeGeneratorForXamarin]
        public int MediaId
        { get; set; }        

        /// <summary>
        /// Gets or sets the culture code
        /// </summary>
        [XmlElement]        
        [IncludeInCodeGeneratorForXamarin]
        public string CultureCode
        { get; set; }

        /// <summary>
        /// Parent of this media culture
        /// </summary>
        [XmlAttribute("Parent")]
        public string Parent
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public MediaCulture Clone()
        {
            return this.Clone<MediaCulture>();
        }

        #endregion
    }
}
