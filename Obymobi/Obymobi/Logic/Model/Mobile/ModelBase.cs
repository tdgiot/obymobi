﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Obymobi.Logic.HelperClasses;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Obymobi.Logic.Model.Mobile
{    
    /// <summary>
    /// Base class for models
    /// </summary>
    [XmlTypeAttribute(TypeName = "MobileModelBase", Namespace = "urn:bar")]    
    [Serializable]
    public class ModelBase
    {
        private static BinaryFormatter binaryFormatter = new BinaryFormatter();

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected T Clone<T>()
        {
            // Performance issue?
            // http://code.google.com/p/protobuf-net/wiki/Performance
            return XmlHelper.Deserialize<T>(XmlHelper.Serialize(this));
        }

        public T CloneUsingBinaryFormatter<T>()
        {
            //byte[] serializedObject;
            T clonedObject;
            using(MemoryStream stream = new MemoryStream())
            {
                lock(binaryFormatter)
                {
                    ModelBase.binaryFormatter.Serialize(stream, this);
                    //serializedObject = stream.ToArray();
                    stream.Position = 0;
                    clonedObject = (T)binaryFormatter.Deserialize(stream);
                }
            }

            return clonedObject;
        }
    }
}
