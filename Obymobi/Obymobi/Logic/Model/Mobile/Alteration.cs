﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile
{
    /// <summary>
    /// Model class which represents an alteration
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Alteration"), IncludeInCodeGeneratorForXamarin]
    public class Alteration : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Alteration type
        /// </summary>
        public Alteration()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the alteration
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the type of this alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the default alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int DefaultAlterationoptionId
        { get; set; }

        /// <summary>
        /// Gets or sets the minimal amount of options needed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int MinOptions
        { get; set; }

        /// <summary>
        /// Gets or sets the maximal amount of options possible
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int MaxOptions
        { get; set; }

        /// <summary>
        /// Gets or sets the start time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string StartTime
        { get; set; }

        /// <summary>
        /// Gets or sets the end time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string EndTime
        { get; set; }

        /// <summary>
        /// Gets or sets the minimum amount of leading minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int MinLeadMinutes
        { get; set; }

        /// <summary>
        /// Gets or sets the maximum amount of leading minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int MaxLeadHours
        { get; set; }

        /// <summary>
        /// Gets or sets the time intervals in minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int IntervalMinutes
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not to show a date picker
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool ShowDatePicker
        { get; set; }

        /// <summary>
        /// Gets or sets the free text value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Value
        { get; set; }

        /// <summary>
        /// Gets or sets wether or not the alteration should be chosen from the order view
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool OrderLevelEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the parentId of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ParentAlterationId 
        { get; set; }

        /// <summary>
        /// Gets or sets the visiblity of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool Visible
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets the options of the alteration
        /// </summary>
        [XmlArray("Alterationoptions")]
        [XmlArrayItem("Alterationoption")]
        [IncludeInCodeGeneratorForXamarin]
        public Alterationoption[] Alterationoptions
        { get; set; }        

        /// <summary>
        /// Gets or sets the alterations of the product
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForXamarin]
        public Media[] Media
        { get; set; }

        /// <summary>
        /// Gets or sets the alteration products of the alteration
        /// </summary>
        [XmlArray("Products")]
        [XmlArrayItem("Product")]
        [IncludeInCodeGeneratorForXamarin]
        public Product[] Products
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Alteration Clone()
        {
            return this.Clone<Alteration>();
        }

        #endregion
    }
}
