﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile
{
    /// <summary>
    /// Model class which represents an language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Businesshours"), IncludeInCodeGeneratorForXamarin, IncludeInCodeGeneratorForAndroid]
    public class Businesshours : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Language type
        /// </summary>
        public Businesshours()
        {
        }

        #endregion

        #region Xml Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        [IncludeInCodeGeneratorForAndroid]
        public int BusinesshoursId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        [IncludeInCodeGeneratorForAndroid]
        public int OpeningDay
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        [IncludeInCodeGeneratorForAndroid]
        public int OpeningTime
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        [IncludeInCodeGeneratorForAndroid]
        public int ClosingDay
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        [IncludeInCodeGeneratorForAndroid]
        public int ClosingTime
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Businesshours Clone()
        {
            return this.Clone<Businesshours>();
        }

        public override string ToString()
        {
            return string.Format("Opening day/time: {0} - {1} Closing day/time: {2} - {3}", this.OpeningDay, this.OpeningTime, this.ClosingDay, this.ClosingTime);
        }

        #endregion
    }
}
