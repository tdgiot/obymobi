﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile
{
    /// <summary>
    /// Model class which represents a category suggestion
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CategorySuggestion"), IncludeInCodeGeneratorForXamarin]
    public class CategorySuggestion : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CategorySuggestion type
        /// </summary>
        public CategorySuggestion()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int CategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the suggested product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of this suggestion
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets if the suggest should be shown at checkout
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool Checkout
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CategorySuggestion Clone()
        {
            return this.Clone<CategorySuggestion>();
        }

        #endregion
    }
}
