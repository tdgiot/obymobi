﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile
{
    /// <summary>
    /// Model class which represents an access code
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "AccessCode"), IncludeInCodeGeneratorForXamarin]
    public class AccessCode : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.AccessCode type
        /// </summary>
        public AccessCode()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the access code
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int AccessCodeId
        { get; set; }

        /// <summary>
        /// Gets or sets the code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Code
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the access code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Type
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool AnalyticsEnabled { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public AccessCode Clone()
        {
            return this.Clone<AccessCode>();
        }

        #endregion
    }
}
