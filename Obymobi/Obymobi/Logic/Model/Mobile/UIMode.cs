﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile
{
    /// <summary>
    /// Model class which represents a UIMode item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "UIMode"), IncludeInCodeGeneratorForXamarin]
    public class UIMode : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Mobile.UIMode type
        /// </summary>
        public UIMode()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the UIMode
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int UIModeId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the tabs
        /// </summary>
        [XmlArray("UITabs")]
        [XmlArrayItem("UITab")]
        [IncludeInCodeGeneratorForXamarin]
        public UITab[] UITabs
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UIMode Clone()
        {
            return this.Clone<UIMode>();
        }

        #endregion
    }
}
