﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile
{
    /// <summary>
    /// Model class which represents a product
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Product"), IncludeInCodeGeneratorForXamarin]
    public class Product : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Product type
        /// </summary>
        public Product()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int CategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SubType
        { get; set; }

        /// <summary>
        /// Gets or sets the price of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public decimal PriceIn
        { get; set; }

        /// <summary>
        /// Gets or sets the price of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int VatPercentage
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets the geofencing type of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool Geofencing
        { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether free text is allowed for this product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool AllowFreeText
        { get; set; }

        /// <summary>
        /// Gets of sets the schedule id of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ScheduleId
        { get; set; }

        /// <summary>
        /// Gets or sets the flag indicating whether the price should be visible for this product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool HidePrice
        { get; set; }

        /// <summary>
        /// Gets or sets the alterations of the product
        /// </summary>
        [XmlArray("Alterations")]
        [XmlArrayItem("Alteration")]
        [IncludeInCodeGeneratorForXamarin]
        public Alteration[] Alterations
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string ButtonText
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string WebTypeTabletUrl
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string WebTypeSmartphoneUrl
        { get; set; }   

        /// <summary>
        /// Gets or sets the alterations of the product
        /// </summary>
        [IncludeInCodeGeneratorForXamarin]
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        public Media[] Media
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [XmlArray("Attachments")]
        [XmlArrayItem("Attachment")]
        [IncludeInCodeGeneratorForXamarin]
        public Attachment[] Attachments
        { get; set; }

        /// <summary>
        /// Gets or sets the suggested products of the product
        /// </summary>
        [XmlArray("ProductSuggestions")]
        [XmlArrayItem("ProductSuggestion")]
        [IncludeInCodeGeneratorForXamarin]
        public ProductSuggestion[] ProductSuggestions
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Product Clone()
        {
            return this.Clone<Product>();
        }

        #endregion
    }
}
