using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum ExternalSystemType : int
    {
        #region POS (Range: 101 to 199)

        Deliverect = 101,

        #endregion

        #region Other (Range 201 to 299)

        HotSOS = 201,
        Quore = 202,
        Google = 203,
        Hyatt_HotSOS = 204,
        Alice = 205

        #endregion
    }
}
