﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    /// <summary>
    /// OrderPaymentState
    /// Try NOT to extend this because the more states the more scenario's ahve to be implemented
    /// 0 - 99      Not Paid - Not allowed to be processed yet.
    /// 100 - 199   Paid - Allowed to be processed
    /// 200 - 299   Post payment - Allowed to be processed (at the bar, on tab, etc)
    /// </summary>
    public enum OrderPaymentState : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = -1,
        /// <summary>
        /// Not paid yet
        /// </summary>
        NotPaid = 0,
        /// <summary>
        /// Paid
        /// </summary>
        PaidReal = 100,
        /// <summary>
        /// Paid or processed as a Demo
        /// </summary>
        PaidDemo = 110,
        /// <summary>
        /// Pay at Pick up
        /// </summary>
        PayAtPickup = 200,
        /// <summary>
        /// Is on Tab (can be processed, but requires payment later)
        /// </summary>
        OnTab = 210
    }
}
