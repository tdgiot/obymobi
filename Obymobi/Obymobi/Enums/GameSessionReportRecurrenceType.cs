using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum GameSessionReportRecurrenceType : int
	{
        Hourly = 1,
        Daily = 2
	}
}
