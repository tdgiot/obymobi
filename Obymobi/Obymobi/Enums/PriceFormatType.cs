﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Defines the format type of prices
    /// </summary>
    public enum PriceFormatType : int
    {
        /// <summary>
        /// No decimal
        /// </summary>        
        [StringValue("No decimal (e.g. $5)")]
        NoDecimal = 0,

        /// <summary>
        /// One decimal
        /// </summary>
        [StringValue("One decimal (e.g. $5.0)")]
        OneDecimal = 1,

        /// <summary>
        /// Two decimals
        /// </summary>
        [StringValue("Two decimals (e.g. $5.00)")]
        TwoDecimals = 2
    }
}
