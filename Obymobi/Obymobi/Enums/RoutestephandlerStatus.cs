﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum OrderRoutestephandlerStatus : int
    {
        /// <summary>
        /// Step is waiting for the previous step(s) to be completed or the whole route to be started if it's the first step
        /// </summary>
        WaitingForPreviousStep = 0,
        /// <summary>
        /// Order (step) is waiting to be retrieved by handler
        /// </summary>
        WaitingToBeRetrieved = 10,
        /// <summary>
        /// This step is now the current step and waiting to be handled 
        /// (might be waiting to be retrieved by a device)
        /// </summary>
        RetrievedByHandler = 15,
        /// <summary>
        /// This step has been picked up by it's handler and is now being processed
        /// </summary>
        BeingHandled = 20,
        /// <summary>
        /// This step has been picked up by a parallel console and is now being processed
        /// </summary>
        BeingHandledByOtherStep = 21,
        /// <summary>
        /// This step has been completed by the handler 
        /// Once an order is completed the route is deleted as well, so you only retrieve 
        /// steps that are completed but are part of a route that's not compelted yet
        /// </summary>
        Completed = 30,
        /// <summary>
        /// This step has been completed because another order in the same stepnumber was completed
        /// </summary>
        CompletedByOtherStep = 31,
        /// <summary>
        /// Step was cancelled because another step in the route failed, which fails the complete route.
        /// </summary>
        CancelledDueToOtherStepFailure = 9998,
        /// <summary>
        /// Step failed.
        /// </summary>
        Failed = 9999
    }
}
