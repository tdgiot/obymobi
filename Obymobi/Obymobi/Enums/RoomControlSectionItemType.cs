﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum RoomControlSectionItemType : int
    {
        /// <summary>
        /// Scene
        /// </summary>
        [StringValue("Scene")]
        Scene = 1,

        /// <summary>
        /// Station Listing
        /// </summary>
        [StringValue("Station listing")]
        StationListing = 2,

        /// <summary>
        /// Wake up timer
        /// </summary>
        [StringValue("Wake up timer")]
        WakeUpTimer = 3,

        /// <summary>
        /// Sleep timer
        /// </summary>
        [StringValue("Sleep timer")]
        SleepTimer = 4,

        /// <summary>
        /// Remote control
        /// </summary>
        [StringValue("Remote control")]
        RemoteControl = 5,
    }
}
