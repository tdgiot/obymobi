﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum ApiAuthenticationType : int
	{
        /// <summary>
        /// Unknown 
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Google
        /// </summary>
        [StringValue("Google")]
        Google = 1,
	}
}
