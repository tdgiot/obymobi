﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum ActionType : int
    {
        [StringValue("Internal Link")]
        InternalLink = 0,

        [StringValue("Embedded Link")]
        EmbeddedLink = 4,

        [StringValue("External Link")]
        ExternalLink = 1,

        [StringValue("Phone")]
        PhoneCall = 2,

        [StringValue("Download")]
        Download = 3,
    }
}
