﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    /// <summary>
    /// 0 - 100     Is not yet started
    /// 200 - 999   Is Pending / In Progress
    /// 1000        Is completed succesfully
    /// 9999        Is completed in error
    /// </summary>
    public enum PaymentStatus : int
    {
        Unknown = 0,
        NotStarted = 10,
        Succesfull = 1000,
        Failed = 9999
    }
}
