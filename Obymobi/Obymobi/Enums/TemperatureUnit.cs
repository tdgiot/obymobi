﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum TemperatureUnit
    {
        [StringValue("Celsius")]
        Celsius,
        [StringValue("Fahrenheit")]
        Fahrenheit
    }
}
