using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    [Obsolete("CompanyCategory is obsolete. Use MarkerIcon instead.")]
    public enum CompanyCategory
    {
        CityGuide = 10000,
        Hotel = 20010,
        Casino = 20020,
        HistoricalBuilding = 20030,
        Park = 20040,
        ThemePark = 20050,
        WildlifePark = 20060,
        Garden = 20070,
        ShoppingCentre = 20080,
        Attraction = 20090,
        Museum = 20100,
        Gallery = 20105,
        Zoo = 20110,
        Store = 20120,
        Theatre = 20130,
        Cinema = 20140,
        Restaurant = 20150,
        Cafe = 20160,
        TeaShop = 20170,
        Bar = 20180
    }
}


        //Unknown = 0,
        //CraveCustomer = 1000,

        //// Places to stay
        //PlaceToStay = 100000,
        //Hotel = 110000,
        //Appartment = 120000,
        ////Campsite = 13000,
        ////HolidayVillage = 14000,

        //// Food & Drinking
        //EatingAndDrinking = 200000,
        //Restaurant = 210000,
        //Bar = 220000,

        //// Attractions
        //Attraction = 300000,
        //Museum = 310000,
        //Funride = 320000,
        //AmusementPark = 330000

// As seen at https://support.google.com/earth/answer/180709?hl=en
    //Bank
    //Bar
    //Car dealership
    //Car rental
    //Car repair
    //Coffee shop or bakery
    //Daycare
    //Dining
    //Electronics, computers, etc.
    //Fast food
    //Gas station
    //Gift shop
    //Hair care
    //Internet service provider or rental establishment
    //Jewelry store
    //Legal
    //Lodging 
    //Motorcycle 
    //Music
    //Pet-related business
    //Photo studio or equipment
    //Shopping
    //Supermarket
    //Swimming
    //Tea
    //Telecommunications 
    //Travel agent
    //Parking
    //Taxi stand
    //Truck or SUV dealer
    //Baseball
    //Fishing
    //Campground
    //Golf course
    //Mountain
    //Park
    //Rest area
    //Soccer or sports complex
    //Tennis court or shop
    //Winter sports
    //Cemetery 
    //Fire station
    //Mosque
    //Synagogue
    //Temple or place of worship
    //Dentist
    //Doctor
    //Hospital
    //Pharmacy
    //Veterinarian  
    //Amusement park
    //Aquarium
    //Bowling 
    //Casino
    //Fitness
    //Movies or rentals
    //Music hall or events venue
    //Zoo
    //Academy or educational institute 
    //Airport
    //Civic building
    //Courthouse
    //Embassy or government
    //Historical place
    //Industry
    //Library
    //Monument
    //Museum
    //Police 
    //Post office
    //Residential complex 
    //School
    //Stadium
    //Tourist destination
    //University