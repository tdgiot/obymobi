﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// KEEP THIS IN SYNC WITH THE VALUES IN ANDROID!
    /// </summary>
    public enum WifiSecurityMethod : int
    {
        None = 0,
        WEP = 1,
        [StringValue("WPA / WPA2")]
        WpaWpa2Psk = 2,
        EAP = 3
    }
}
