using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
	/// Enumeration which represents the CompanyOwnerSaveResult
    /// </summary>
    public enum CompanyOwnerSaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Failed
        ///// </summary>
        //Failed = 200,

        /// <summary>
        /// UsernameEmpty
        /// </summary>
        [StringValue("De gebruikersnaam is niet opgegeven.")]
        UsernameEmpty = 201,

        /// <summary>
        /// PasswordEmpty
        /// </summary>
        [StringValue("Het wachtwoord is niet opgegeven.")]
        PasswordEmpty = 202,

		/// <summary>
		/// InvalidUsername
		/// </summary>
		[StringValue("De opgegeven gebruikersnaam is niet geldig")]
		InvalidUsername = 203,

        /// <summary>
        /// UsernameAlreadyExists
        /// </summary>
        [StringValue("De opgegeven gebruikersnaam wordt al gebruikt")]
        UsernameAlreadyExists = 204,
    }
}
