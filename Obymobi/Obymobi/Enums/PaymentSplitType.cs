﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum PaymentSplitType
    {
        [StringValue("Balance Account")]
        BalanceAccount = 1,
        [StringValue("Commission")]
        Commission = 2,
        [StringValue("Default")]
        Default = 3,
        [StringValue("Market Place")]
        MarketPlace = 4,
        [StringValue("Payment Fee")]
        PaymentFee = 5,
        [StringValue("VAT")]
        VAT = 6,
        [StringValue("Verification")]
        Verification = 7
    }
}