﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Enums
{
	public enum ApplessFeatureFlags
	{
		ReadOnly = 0,
		Maps = 1,
		PwaDisabled = 2,
		CustomerAnalytics = 3,
		EatOutToHelpOutDiscount = 999
	}
}
