﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the DeliverypointSaveResult
    /// </summary>
    public enum OrderitemSaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Failed
        ///// </summary>
        //Failed = 200,

        /// <summary>
        /// QuantityLessThanOrZero
        /// </summary>
        [StringValue("Er is geen aantal opgegeven.")]
        QuantityLessThanOrZero = 201,

        /// <summary>
        /// PriceExLessThanZero
        /// </summary>
        [StringValue("Er is geen prijs opgegeven.")]
        PriceExLessThanZero = 202,

        /// <summary>
        /// VatPercentageLessThanZero
        /// </summary>
        [StringValue("Er is geen BTW-percentage opgegeven.")]
        VatPercentageLessThanZero = 203,

        /// <summary>
        /// PriceExSubtotalInCents larger or smaller than int
        /// </summary>
        [StringValue("Het aantal x de prijs voor het orderitem in centen is groter dan int.Max of kleiner dan int.Min.")]
        PriceExSubtotalInCentsLargerOrSmallerThanInt = 204,

        /// <summary>
        /// Change not allowed to an existing orderitem
        /// </summary>
        [StringValue("Wijziging niet toegestaan voor bestaande orderitem.")]
        ChangeNotAllowedToExistingOrderitem = 205,

        /// <summary>
        /// Orderitems of paid orders can't be deleted
        /// </summary>
        [StringValue("Het orderitem is niet gevalideerd door de OrderProcessingHelper.")]
        OrderitemIsNotValidatedByOrderProcessingHelper = 207,


    }
}
