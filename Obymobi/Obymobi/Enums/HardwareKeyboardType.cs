﻿using Dionysos;
namespace Obymobi.Enums
{
    public enum HardwareKeyboardType
    {
        [StringValue("None")]
        None = 0,
        [StringValue("Sony BKB10")]
        Sony_BKB10 = 1,
    }
}
