﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum TerminalStatusCode : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// OK
        /// </summary>
        [StringValue("OK")]
        OK = 100,

        /// <summary>
        /// Printer error
        /// </summary>
        [StringValue("Printerfout")]
        PrinterError = 200,

        /// <summary>
        /// Configuration error
        /// </summary>
        [StringValue("Configuratiefout")]
        ConfigurationError = 201,

        /// <summary>
        /// Internet error
        /// </summary>
        [StringValue("Internetfout")]
        InternetError = 202,

        /// <summary>
        /// Webservice error
        /// </summary>
        [StringValue("Webservicefout")]
        WebserviceError = 203,

        /// <summary>
        /// Database error
        /// </summary>
        [StringValue("Databasefout")]
        DatabaseError = 204,

        /// <summary>
        /// POS error
        /// </summary>
        [StringValue("Kassafout")]
        POSError = 205,

        /// <summary>
        /// Application stopped
        /// </summary>
        [StringValue("Applicatie gestopt")]
        ApplicationStopped = 206,
    }
}
