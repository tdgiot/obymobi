﻿using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration of service types.
    /// </summary>
    public enum ServiceMethodType
    {
        /// <summary>
        /// An order delivered to the guest room.
        /// </summary>
        [StringValue("Room Service")]
        RoomService = 0,

        /// <summary>
        /// An order delivered to a table.
        /// </summary>
        [StringValue("Table Service")]
        TableService = 1,

        /// <summary>
        /// An order which has to be picked up by the guest.
        /// </summary>
        [StringValue("Pick Up")]
        PickUp = 2,

        /// <summary>
        /// An order which has to be delivered to the guest.
        /// </summary>
        [StringValue("Delivery")]
        Delivery = 3,
    }
}
