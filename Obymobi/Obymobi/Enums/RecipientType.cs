namespace Obymobi.Enums
{
	/// <summary>
    /// Enumeration which represents the RecipientType
	/// </summary>
	public enum RecipientType : int
	{
		Unknown = 0, 
               
		Customer = 1,

        Client = 2,

        Deliverypoint = 3
	}
}
