﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum ScheduledCommandTaskStatus : int
	{
        /// <summary>
        /// Inactive
        /// </summary>
        Inactive = 0,

        /// <summary>
        /// Pending c.q. waiting to be started
        /// </summary>
        Pending = 100,

        /// <summary>
        /// Started c.q. in progress
        /// </summary>
        Started = 200,

        /// <summary>
        /// Completed
        /// </summary>
        Completed = 300,

        /// <summary>
        /// Expired
        /// </summary>
        Expired = 400,

        /// <summary>
        /// Completed with errors i.e. failed or expired commands
        /// </summary>
        CompletedWithErrors = 999
	}
}
