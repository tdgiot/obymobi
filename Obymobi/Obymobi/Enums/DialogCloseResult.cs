using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	/// <summary>
    /// Enumeration which represents the dialog close result
	/// </summary>
	public enum DialogCloseResult : int
	{
        [StringValue("Unknown")]
        Unknown = 0,

        [StringValue("Ok button")]
		OkButton = 1,

        [StringValue("Yes button")]
		YesButton = 2,

        [StringValue("No button")]
        NoButton = 3,

        [StringValue("Time out")]
        TimeOut = 4,

        [StringValue("Clear manually")]
        ClearManually = 5,
    }
}
