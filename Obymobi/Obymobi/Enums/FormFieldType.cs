using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents the result of saving a feature
	/// </summary>
	public enum FormFieldType : int
	{
        [StringValue("Text")]
		Text = 100,

        [StringValue("Number")]
        Number = 200,

        [StringValue("Email")]
		Email = 300,

        [StringValue("Website")]
		Website = 400,

		[StringValue("Date")]
		Date = 500,

        [StringValue("Checkbox")]
        Checkbox = 600

        //[StringValue("Options")]
		//Options = 4,

        //[StringValue("List")]
		//List = 6,
	}
}
