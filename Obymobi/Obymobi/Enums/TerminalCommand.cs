﻿namespace Obymobi.Enums
{
    public enum TerminalCommand
    {
        DoNothing = 0,
        RestartApplication = 1,
        RestartDevice = 2,
        RestartInRecovery = 3,
        SendLogToWebservice = 4,
        NonPosDataSynchronisation = 5,
        PosDataSynchronisation = 6,
        MenuConsistencyCheck = 7,
        DownloadConsoleUpdate = 8,
        DownloadAgentUpdate = 9,
        DownloadSupportToolsUpdate = 10,
        InstallConsoleUpdate = 11,
        InstallAgentUpdate = 12,
        InstallSupportToolsUpdate = 13,
        ClearBrowserCache = 14,
        DownloadOSUpdate = 15,
        InstallOSUpdate = 16,
        DownloadInstallMessagingServiceUpdate = 17,
    }
}
