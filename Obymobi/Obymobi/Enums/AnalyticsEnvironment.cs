﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum AnalyticsEnvironment
    {
        [StringValue("Don't track")]
        None = 0,

        [StringValue("Demo")]
        NonProduction = 1,

        [StringValue("Production")]
        Production = 2
    }
}
