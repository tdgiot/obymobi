using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum DeviceTokenState : int
	{
        /// <summary>
        /// Queued
        /// </summary>
        Queued = 0,

        /// <summary>
        /// Waiting to be retrieved by device
        /// </summary>
        WaitingToBeRetrieved = 1,

        /// <summary>
        /// Acknowledged
        /// </summary>
        Acknowledged = 2,
    }
}
