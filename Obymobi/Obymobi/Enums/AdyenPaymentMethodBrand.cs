﻿using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.Enums
{
    public enum AdyenPaymentMethodBrand
    {
        [AdyenPaymentMapping("mc")]
        [StringValue("Mastercard")]
        MasterCard = 1,

        [AdyenPaymentMapping("visa")]
        [StringValue("Visa")]
        Visa = 2,

        [AdyenPaymentMapping("amex")]
        [StringValue("American Express")]
        AmericanExpress = 3,

        [AdyenPaymentMapping("discover")]
        [StringValue("Discover")]
        Discover = 4,

        [AdyenPaymentMapping("bancontact")]
        [StringValue("Bancontact")]
        Bancontact = 5,

        [AdyenPaymentMapping("cartesbancaires")]
        [StringValue("Cartes Bancaires")]
        CartesBancaires = 6,

        [AdyenPaymentMapping("chinaunionpay")]
        [StringValue("China Union Pay")]
        ChinaUnionPay = 7,

        [AdyenPaymentMapping("dankort")]
        [StringValue("Dankort")]
        Dankort = 8,

        [AdyenPaymentMapping("diners")]
        [StringValue("Diners")]
        Diners = 9,

        [AdyenPaymentMapping("elo")]
        [StringValue("Elo")]
        Elo = 10,

        [AdyenPaymentMapping("hipercard")]
        [StringValue("Hipercard")]
        Hipercard = 11,

        [AdyenPaymentMapping("jcb")]
        [StringValue("JCB")]
        JCB = 12,

        [AdyenPaymentMapping("maestro")]
        [StringValue("Maestro")]
        Maestro = 13,

        [AdyenPaymentMapping("uatp")]
        [StringValue("UATP")]
        UATP = 14,

        [AdyenPaymentMapping("visaelectron")]
        [StringValue("Visa Electron")]
        VisaElectron = 15,

        [AdyenPaymentMapping("vpay")]
        [StringValue("V-Pay")]
        VPay = 16
    }
}
