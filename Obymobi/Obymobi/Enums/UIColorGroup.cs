using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.Enums
{
	public enum UIColorGroup : int
    {
        [StringValue("Lists")]
        Lists = 1,

        [StringValue("Widgets")]
        Widgets = 2,

        [StringValue("Header")]
        Header = 3,

        [StringValue("Pages")]
        Pages = 4,

        [StringValue("Footer")]
        Footer = 5,

        [StringValue("Buttons")]
        Buttons = 6,

        [StringValue("Textboxes")]
        Textboxes = 7,

        [StringValue("Dialogs")]
        Dialogs = 8,

        [StringValue("Spinners")]
        Spinners = 9,

        [StringValue("Room Control")]
        RoomControl = 10,

        [StringValue("Alteration Dialog")]
        AlterationDialog = 11,

        [StringValue("Browser Age Verification")]
        BrowserAgeVerification = 12,

        [StringValue("Product Dialog")]
        ProductDialog = 13,

        [StringValue("Browser")]
        Browser = 14,

        [StringValue("Map")]
        Map = 15,

        [StringValue("Toggle Button")]
        ToggleButton = 16,

        [StringValue("Screensaver")]
        ScreenSaver = 17,

        [StringValue("Qr Code")]
        QrCode = 18
    }
}
