﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum VisibilityType : int
    {
        /// <summary>
        /// Always
        /// </summary>
        [StringValue("Always")]
        Always = 0,

        /// <summary>
        /// Never
        /// </summary>
        [StringValue("Never")]
        Never = 1,

        /// <summary>
        /// When one or more linked items (e.g. widget or slide) are visible
        /// </summary>
        [StringValue("When linked item visible")]
        WhenLinkedItemVisible = 2,

        /// <summary>
        /// When orderable according to schedule
        /// </summary>
        [StringValue("When available on schedule")]
        WhenAvailableOnSchedule = 3,

        /// <summary>
        /// When linked as suggestion
        /// </summary>
        [StringValue("When linked as suggestion")]
        WhenLinkedAsSuggestion = 4,
    }
}
