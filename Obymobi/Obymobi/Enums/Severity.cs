﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumarator which indicates the severity of the current Obymobi status
    /// </summary>
    public enum Severity : int
    {
        /// <summary>
        /// Severity is unknown
        /// </summary>
        Unknown = -1,

        /// <summary>
        /// No severity, all systems are up-and-running
        /// </summary>
        None = 0,

		/// <summary>
		/// Low severity, some systems are down
		/// </summary>
		VeryLow = 10,

        /// <summary>
        /// Low severity, some systems are down
        /// </summary>
        Low = 20,

        /// <summary>
        /// Medium severity, Obymobi is still running but some systems are down
        /// </summary>
        Medium = 30,

        /// <summary>
        /// Critical severity, Obymobi is down
        /// </summary>
        High = 40,
    }
}
