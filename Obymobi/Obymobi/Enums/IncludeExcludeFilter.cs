using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum IncludeExcludeFilter : int
	{
        /// <summary>
        /// No filter
        /// </summary>
        None = 0,

        /// <summary>
        /// Include filter
        /// </summary>
        Include = 1,

        /// <summary>
        /// Exclude filter
        /// </summary>
        Exclude = 2,
    }
}
