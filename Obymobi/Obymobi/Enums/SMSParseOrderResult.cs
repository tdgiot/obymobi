﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum SMSParseOrderResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// No Oby-code was specified in the SMS
        /// </summary>
        [StringValue("Er is geen Oby-code opgegeven in de SMS.")]
        NoObycodeSpecified = 200,

        /// <summary>
        /// Company does not allow SMS ordering
        /// </summary>
        [StringValue("Het is niet mogelijk om via SMS te bestellen bij dit bedrijf.")]
        CompanyDoesNotAllowSMSOrdering = 201,

        /// <summary>
        /// No deliverypoint number was specified in the SMS
        /// </summary>
        [StringValue("Er is geen tafelnummer opgegeven in de SMS.")]
        NoDeliverypointNumberSpecified = 202,

        /// <summary>
        /// No orderitems were specified in the SMS
        /// </summary>
        [StringValue("Er zijn geen producten en aantallen opgegeven in de SMS.")]
        NoOrderitemsSpecified = 203,

        /// <summary>
        /// Deliverypoint number could not be parsed
        /// </summary>
        [StringValue("Het opgegeven tafelnummer is in een onjuist formaat.")]
        DeliverypointCouldNotBeParsed = 204,

        /// <summary>
        /// Quantity could not be parsed
        /// </summary>
        [StringValue("Het opgegeven aantal is in een onjuist formaat.")]
        QuantityCouldNotBeParsed = 205,

        /// <summary>
        /// Company has no paymentmethod with system name 'cash'
        /// </summary>
        [StringValue("Het bedrijf heeft geen betalingsmethod cash.")]
        CompanyHasNoCashPaymentmethod = 206
    }
}
