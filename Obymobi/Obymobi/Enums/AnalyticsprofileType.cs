using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
    /// Enumeration which represents the AnalyticsprofileType
	/// </summary>
	public enum AnalyticsprofileType : int
	{
        [StringValue("Emenu")]
		Emenu = 1,

        [StringValue("Microsite")]
		Microsite = 2
	}
}
