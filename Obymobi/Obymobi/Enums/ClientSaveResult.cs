using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents the result of saving a Category
	/// </summary>
	public enum ClientSaveResult : int
	{
		/// <summary>
		/// Authentication result is unknown
		/// </summary>
        [StringValue("Onbekend.")]
		Unknown = 0,

		/// <summary>
		/// User was successfully authenticated
		/// </summary>
        [StringValue("Succes.")]
		Success = 100,

		/// <summary>
		/// Name Empty
		/// </summary>
		[StringValue("Serienummer is niet uniek.")]
		NonUniqueSerialNumber = 201,

        /// <summary>
        /// Name Empty
        /// </summary>
        [StringValue("Een tafel kan slechts aan één tablet worden verbonden, kies een andere tafel.")]
        MultipleClientsForDeliverypoint = 202,

		/// <summary>
		/// Name Empty
		/// </summary>
		[StringValue("Serienummer is niet gevuld.")]
		SerialNumberEmpty = 203,

		/// <summary>
		/// Name Empty
		/// </summary>
		[StringValue("Bedrijf is niet gevuld.")]
		CompanyIdEmpty = 204,

		/// <summary>
		/// Name Empty
		/// </summary>
		[StringValue("Tafel hoort niet bij bedrijf.")]
		CompanyDeliverypointMismatch = 205,

		/// <summary>
		/// Name Empty
		/// </summary>
		[StringValue("Meerdere klanten verbonden aan tablet.")]
		MultipleCustomersForTablet = 206,

        /// <summary>
        /// Name Empty
        /// </summary>
        [StringValue("Tafel en tafelgroep gespecificeerd.")]
        DeliverypointAndDeliverypointgroupSpecified = 207,

        /// <summary>
        /// Name Empty
        /// </summary>
        [StringValue("Een apparaat kan slechts aan één client worden verbonden, kies een andere apparaat.")]
        MultipleClientsForDevice = 208,

        [StringValue("De ingestelde tafel zit in een andere tafelgroep dan de ingestelde tafelgroep voor het apparaat.")]
        DeliverypointAndDeliverypointgroupDontMatch = 209,

        [StringValue("De ingestelde tafel hoort bij een ander bedrijf.")]
        DeliverypointDoesNotBelongToCompanyOfClient = 210,

        [StringValue("De ingestelde tafelgroep hoort bij een ander bedrijf.")]
        DeliverypointgroupDoesNotBelongToCompanyOfClient = 211,

        [StringValue("Een apparaat kan slechts aan één client of terminal worden verbonden, kies een andere apparaat.")]
        DeviceAlreadyConnectToTerminal = 212,

	}
}
