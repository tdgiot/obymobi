﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum OrderProcessingError : int
    {
        /// <summary>
        /// No error
        /// </summary>
        [StringValue("Geen error bekend.")]
        None = 0,

        /// <summary>
        /// Unknown error
        /// </summary>
        [StringValue("Manually Processed.")]
        ManuallyProcessed = 1,

        /// <summary>
        /// Unknown error
        /// </summary>
        [StringValue("Error niet duidelijk.")]
        UnspecifiedError = 2,

        #region At on-site- server 200 - 299

        /// <summary>
        /// Order could not be converted to a POS order
        /// </summary>
        [StringValue("Onsite Pos Conversion Failed.")]
        OnsitePosConversionFailed = 200,

        /// <summary>
        /// Table queue is locked
        /// </summary>
        [StringValue("Table queue locked.")]
        TableQueueuIsLocked = 201,

        /// <summary>
        /// No Table For Deliverypoint At Oss
        /// </summary>
        [StringValue("Table queue locked.")]
        NoTableForDeliverypointAtOss = 202,

        /// <summary>
        /// Failed to print
        /// </summary>
        [StringValue("Printer Failure")]
        PrinterFailure = 203,

        #endregion

        // GK > Refacorting could be use for splitting the NON-orderprocessing POS errors out of this file
        #region Pos (Connector) Related 500 - 1499

        #region Generic Pos Errors

        #region Non-Connector specific non-fatal order processing errors (500 - 549)

        /// <summary>
        /// An error happened processing to the POS that's not fatal
        /// </summary>
        [StringValue("Onbekend niet-fataal Pos probleem.")]
        PosErrorUnspecifiedNonFatalPosProblem = 500,

        /// <summary>
        /// The deliverypoint is locked on the POS
        /// </summary>
        [StringValue("Tafel geblokkeerd in kassa.")]
        PosErrorDeliverypointLocked = 501,

        /// <summary>
        /// A connection failure happened that could be reattempted
        /// </summary>
        [StringValue("Verbindingsprobleem met kassa.")]
        PosErrorConnectivityProblem = 502,

        /// <summary>
        /// There's something not accepatbale by the POS about the product(s) and/or alteration(s)
        /// </summary>
        [StringValue("There's something not accepatbale by the POS about the product(s) and/or alteration(s).")]
        PosErrorProductAndOrAlterationConfigurationInvalid = 503,

        #endregion

        #region Non-Connector specific fatal Errors (550 - 599)

        /// <summary>
        /// An error happened processing to the POS that's fatal
        /// </summary>
        [StringValue("Onbekend fataal Pos probleem.")]
        PosErrorUnspecifiedFatalPosProblem = 550,

        /// <summary>
        /// Order could not be converted to POS order
        /// </summary>
        [StringValue("Kan order item niet omzetten naar POS")]
        PosErrorCouldNotInitializeConnector = 551,

        /// <summary>
        /// Order could not be converted to POS order
        /// </summary>
        [StringValue("Kan order item niet omzetten naar POS")]
        PosErrorUnknownPosItem = 552,

        /// <summary>
        /// Api interface gave unknown error
        /// </summary>
        [StringValue("Api interface gave unknown error")]
        PosErrorApiInterfaceException = 553,

        /// <summary>
        /// Could not print via POS
        /// </summary>
        [StringValue("Could not print via POS")]
        PosErrorPrintingError = 554,

        /// <summary>
        /// Save order failed via POS
        /// </summary>
        [StringValue("Save order failed via POS")]
        PosErrorSaveOrderFailed = 555,

        /// <summary>
        /// Any conversion from one data type to another that goes wrong
        /// </summary>
        [StringValue("Any conversion from one data type to another that goes wrong")]
        PosErrorNonParseableOrConvertableDataType = 556,

        /// <summary>
        /// Configuration Incomplete
        /// </summary>
        [StringValue("Configuration is incomplete or incorrect")]
        PosErrorConfigurationIncomplete = 557,

        /// <summary>
        /// Configuration Incorrect
        /// </summary>
        [StringValue("Configuration is incomplete or incorrect")]
        PosErrorConfigurationIncorrect = 558,

        [StringValue("Paymentmethod not implemented")]
        PosErrorPaymentmethodNotImplemented = 559,

        [StringValue("Ongeldig PosProduct ExternalId formaat.")]
        IncompatiblePosProductExternalId = 560,

        [StringValue("Ongeldig PosProductAlteration ExternalId formaat.")]
        IncompatiblePosAlterationExternalId = 561,

        [StringValue("Ongeldig PosProductAlterationOption ExternalId formaat.")]
        IncompatiblePosAlterationOptionExternalId = 562,

        #endregion

        #region Pos Synchronisation Errors (600 - 629)

        /// <summary>
        /// Could not retrieve Pos Products
        /// </summary>
        [StringValue("Could not retrieve Pos Products")]
        PosErrorCouldNotGetPosProducts = 600,

        /// <summary>
        /// Could not retrieve Pos Categories
        /// </summary>
        [StringValue("Could not retrieve Pos Categories")]
        PosErrorCouldNotGetPosCategories = 601,

        /// <summary>
        /// Could not retrieve Pos DeliverypointGroups
        /// </summary>
        [StringValue("Could not retrieve Pos DeliverypointGroups")]
        PosErrorCouldNotGetPosDeliverypointGroups = 602,

        /// <summary>
        /// Could not retrieve Pos Deliverypoints
        /// </summary>
        [StringValue("Could not retrieve Pos Deliverypoints")]
        PosErrorCouldNotGetPosDeliverypoints = 603,

        /// <summary>
        /// Could not retrieve Pos Deliverypoints
        /// </summary>
        [StringValue("Could not retrieve Pos Deliverypoints")]
        PosErrorCouldNotGetPosOrders = 604,

        #endregion

        #endregion

        #region Vectron 750-799
        /// <summary>
        /// An exception was thrown while trying to display the output of the Flexipos ordering.
        /// </summary>
        [StringValue("An exception was thrown while trying to display the output of the Flexipos ordering.")]
        VectronCouldNotDisplayOut = 750,

        /// <summary>
        /// The clerk could not be opened!
        /// </summary>
        [StringValue("The clerk could not be opened!")]
        VectronClerkCouldNotBeOpened = 751,

        /// <summary>
        /// The Guest check could not be opened
        /// </summary>
        [StringValue("The guest check could not be opened")]
        VectronGuestCheckCouldNotBeOpened = 753,

        /// <summary>
        /// Product could not be added to check
        /// </summary>
        [StringValue("Product could not be added to check")]
        VectronProductCouldNotBeAddedToCheck = 755,

        /// <summary>
        /// Could not clode guest check
        /// </summary>
        [StringValue("Could not close guest check")]
        VectronCouldNotCloseGuestCheck = 756,

        /// <summary>
        /// Could not clode guest check
        /// </summary>
        [StringValue("Could not close clerk")]
        VectronCouldNotCloseClerk = 757,

        /// <summary>
        /// Could not hide output
        /// </summary>
        [StringValue("Could not hide output")]
        VectronCouldNotHideOutput = 758,

        /// <summary>
        /// FlexiPos could not be killed
        /// </summary>
        [StringValue("FlexiPos could not be killed")]
        VectronFlexiPosCouldNotBeKilled = 760,

        /// <summary>
        /// Vectron Pos Products File Not Found
        /// </summary>
        [StringValue("Vectron Pos Products File Not Found")]
        VectronPosProductsFileNotFound = 760,

        /// <summary>
        /// Vectron Pos Categories File Not Found
        /// </summary>
        [StringValue("Vectron Pos Categories File Not Found")]
        VectronPosCategoriesFileNotFound = 761,

        #endregion

        #region Aloha 800 - 849

        /// <summary>
        /// Aloha Could Not Get Profiles
        /// </summary>
        [StringValue("Aloha Could Not Get Profiles")]
        AlohaCouldNotGetProfiles = 800,

        /// <summary>
        /// Aloha Could Not Set Profile.
        /// </summary>
        [StringValue("Aloha Could Not Set Profile.")]
        AlohaCouldNotSetProfile = 801,

        /// <summary>
        /// Aloha Could Not Login Employee.
        /// </summary>        
        [StringValue("Aloha Could Not Login Employee.")]
        AlohaCouldNotLoginEmployee = 802,

        /// <summary>
        /// Aloha Could Not Clock In Employee.
        /// </summary>
        [StringValue("Aloha Could Not Clock In Employee.")]
        AlohaCouldNotClockInEmployee = 803,

        /// <summary>
        /// Aloha Could Not Get Open Tables.
        /// </summary>
        [StringValue("Aloha Could Not Get Open Tables.")]
        AlohaCouldNotGetOpenTables = 804,

        /// <summary>
        /// Aloha Could Not Get Table Details.
        /// </summary>
        [StringValue("Aloha Could Not Get Table Details.")]
        AlohaCouldNotGetTableDetails = 805,

        /// <summary>
        /// Aloha Could Not Open Table.
        /// </summary>
        [StringValue("Aloha Could Not Open Table.")]
        AlohaCouldNotOpenTable = 806,

        /// <summary>
        /// Aloha Could Not Add Items To Check.
        /// </summary>
        [StringValue("Aloha Could Not Add Items To Check.")]
        AlohaCouldNotAddItemsToCheck = 807,

        /// <summary>
        /// Aloha Could Not End Check.
        /// </summary>
        [StringValue("Aloha Could Not End Check.")]
        AlohaCouldNotEndCheck = 808,

        /// <summary>
        /// Aloha Could Not End Table.
        /// </summary>
        [StringValue("Aloha Could Not End Table.")]
        AlohaCouldNotEndTable = 809,

        /// <summary>
        /// Aloha Could Not Log Out Employee.
        /// </summary>
        [StringValue("Aloha Could Not Log Out Employee.")]
        AlohaCouldNotLogOutEmployee = 810,

        /// <summary>
        /// Aloha could not open check
        /// </summary>
        [StringValue("Aloha could not open check.")]
        AlohaCouldNotOpenCheck = 811,

        /// <summary>
        /// Aloha Could Not get check details
        /// </summary>
        [StringValue("Aloha Could Not get check details.")]
        AlohaCouldNotGetCheckDetails = 812,

        #endregion

        #region Unitouch 850 - 899

        /// <summary>
        /// Could not open table
        /// </summary>
        [StringValue("Could not open table")]
        UnitouchCouldNotOpenTable = 850,

        /// <summary>
        /// Table was not yet opened
        /// </summary>        
        [StringValue("Table was not yet opened")]
        UnitouchTableWasNotOpenedYet = 851,

        /// <summary>
        /// Could not set user
        /// </summary>
        [StringValue("Could not set user")]
        UnitouchCouldNotSetUser = 852,

        /// <summary>
        /// Could not get bill
        /// </summary>        
        [StringValue("Could not get bill")]
        UnitouchCouldNotGetBill = 853,

        /// <summary>
        /// Could not put order
        /// </summary>        
        [StringValue("Could not put order")]
        UnitouchCouldNotPutOrder = 854,

        /// <summary>
        /// Could not put orderitems
        /// </summary>        
        [StringValue("Could not put orderitems")]
        UnitouchCouldNotPutOrderitems = 855,

        #endregion

        #region MicrosMcp 900 - 949

        MicrosMcpCouldntGetSession = 900, 

        MicrosMcpCouldntGetStore = 901,

        MicrosMcpCouldntClearOrder = 902,

        MicrosMcpSessionWasntInitialized = 903,

        MicrosMcpStoreWasntInitialized = 904,

        MicrosMcpMenuWasntInitialized = 905, 

        #endregion

        #endregion

        #region External system 1400 - 1449

        NoExternalProductConnectedToProduct = 1400,

        MultipleExternalProductsConnectedToProduct = 1401,

        IdIsEmptyForExternalProduct = 1402,

        NoExternalDeliverypointConnectedToDeliverypoint = 1403,

        MultipleExternalDeliverypointsConnectedToDeliverypoint = 1404,

        IdIsEmptyForExternalDeliverypoint = 1405,

        #endregion

        #region HotSOS 1500 - 1549

        HotSOSNoIssueConnectedToProduct = 1500,

        HotSOSNoRoomConnectedToDeliverypoint = 1501,

        #endregion

        #region Quore 1550 - 1599

        QuoreInvalidApiResult = 1550,

        QuoreRequestAlreadyExists = 1551,

        #endregion

        #region Hyatt Platform Services 1600 - 1649

        HyattInvalidApiResult = 1600,
    
        HyattRequestAlreadyExists = 1601,

        #endregion

        #region Alice 1650 - 1699

        AliceNoReservationFound = 1650,

        #endregion

        #region Failures

        /// <summary>
        /// The order was cancelled by the customer
        /// </summary>
        [StringValue("Probleem met Smtp Server")]
        UnspecifiedSendingMailError = 9994,

        /// <summary>
        /// The order was cancelled by the customer
        /// </summary>
        [StringValue("Probleem met Smtp Server")]
        SmtpServerError = 9995,

        /// <summary>
        /// The order has timed out.
        /// </summary>
        [StringValue("Niet op tijd verwerkt.")]
        TimedOut = 9996,

        /// <summary>
        /// The order was cancelled by the customer
        /// </summary>
        [StringValue("Er is geen route beschikbaar voor de order.")]
        Unroutable = 9997,

        /// <summary>
        /// The order was cancelled by the customer
        /// </summary>
        [StringValue("De order is door de klant geannuleerd.")]
        CancelledByCustomer = 9998,

        #endregion
    }
}
