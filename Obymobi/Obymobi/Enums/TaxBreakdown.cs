﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum TaxBreakdown
    {
        [StringValue("None")]
        None = 0,

        [StringValue("Receipt")]
        Receipt = 1,

        [StringValue("Receipt and checkout")]
        ReceiptAndCheckout = 2
    }
}
