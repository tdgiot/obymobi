﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum RoutestephandlerType : int
    {
        /// <summary>
        /// Console
        /// </summary>
        [StringValue("Console")]
        Console = 100,

        /// <summary>
        /// Printer
        /// </summary>
        [StringValue("Printer")]
        Printer = 200,

        /// <summary>
        /// POS
        /// </summary>
        [StringValue("POS")]
        POS = 300,

		/// <summary>
		/// Email order
		/// </summary>
		[StringValue("E-mail order")]
		EmailOrder = 400,

		/// <summary>
		/// SMS
		/// </summary>
		[StringValue("SMS")]
		SMS = 500,

        /// <summary>
        /// HotSOS
        /// </summary>
        [StringValue("HotSOS")]
        HotSOS = 600,

        /// <summary>
        /// Quore
        /// </summary>
        [StringValue("Quore")]
        Quore = 601,

        /// <summary>
        /// Hyatt
        /// </summary>
        [StringValue("Hyatt (HotSOS)")]
        Hyatt_HotSOS = 602,

        /// <summary>
        /// Alice
        /// </summary>
        [StringValue("Alice")]
        Alice = 603,

        /// <summary>
        /// External System
        /// </summary>
        [StringValue("External System")]
        ExternalSystem = 604,

        /// <summary>
        /// Email document
        /// </summary>
        [StringValue("E-mail document")]
        EmailDocument = 700
    }
}
