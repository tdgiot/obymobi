using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	/// <summary>
	/// List of available comet server types
	/// </summary>
	public enum CometServerType
	{
		/// <summary>
		/// PokeIn.com
		/// </summary>
		PokeIn,

        /// <summary>
        /// ASP.Net SignalR
        /// </summary>
        SignalR,

        /// <summary>
        /// Fake client for internal Comet calls from validators.
        /// See CometServerProvider class
        /// </summary>
        Dummy
	}
}
