﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum PaymentTransactionStatus
    {
        [StringValue("Pending")]
        Pending = 0, // Default state
        [StringValue("Initiated")]
        Initiated = 1,
        [StringValue("Paid")]
        Paid = 2,
        [StringValue("Failed")]
        Failed = 3,
        [StringValue("Cancelled")]
        Cancelled = 4,
        [StringValue("Error")]
        Error = 100
    }
}