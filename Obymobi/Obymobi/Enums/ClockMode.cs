﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum ClockMode
    {
        [StringValue("24 Hour")]
        Hour24 = 0,
        [StringValue("AM / PM")]
        AmPm = 1,
    }
}
