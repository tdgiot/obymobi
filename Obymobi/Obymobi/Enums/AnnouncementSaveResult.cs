using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents the result of saving an announcement
	/// </summary>
	public enum AnnouncementSaveResult : int
	{
		/// <summary>
		/// Authentication result is unknown
		/// </summary>
        [StringValue("Onbekend.")]
		Unknown = 0,

		/// <summary>
		/// User was successfully authenticated
		/// </summary>
        [StringValue("Succes.")]
		Success = 100,

		/// <summary>
		/// Reorder dialog announcement already exists
		/// </summary>
		[StringValue("ReorderDialogAnnouncementAlreadyExists.")]
        ReorderDialogAnnouncementAlreadyExists = 201,
	}
}
