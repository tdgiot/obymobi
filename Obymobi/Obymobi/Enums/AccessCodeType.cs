using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum AccessCodeType : int
	{
        /// <summary>
        /// Show i.e. add the selected companies to the existing list of companies
        /// </summary>
        Show = 0,

        /// <summary>
        /// Show only i.e. only show the selected companies
        /// </summary>
        ShowOnly = 100,
	}
}
