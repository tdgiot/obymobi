using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum ColumnSeparationType : int
	{
        /// <summary>
        /// Tabs
        /// </summary>
        Tabs = 0,
        /// <summary>
        /// Spaces
        /// </summary>
        Spaces = 1
	}
}
