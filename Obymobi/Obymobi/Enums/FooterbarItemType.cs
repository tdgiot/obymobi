﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum FooterbarItemType
    {
        [StringValue("Icon")]
        Icon = 1,

        [StringValue("Button")]
        Button = 2,

        //[StringValue("Text")]
        //TextView = 3
    }

    public enum FooterbarItemPosition
    {
        [StringValue("Left")]
        Left = -1,
        [StringValue("Center")]
        Center = 0,
        [StringValue("Right")]
        Right = 1
    }

    public enum FooterbarItemAction
    {
        [StringValue("Turn screen off")]
        TurnScreenOff,

        [StringValue("Estimated Delivery Time")]
        EstimatedDeliveryTime
    }
}