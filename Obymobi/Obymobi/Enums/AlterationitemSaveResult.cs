using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum AlterationitemSaveResult
    {
        /// <summary>
        /// It's not possible to add Alterationitemswithout a link to a Posalterationitem.
        /// </summary>
        [StringValue("It's not possible to add Alterationitems without a link to a Posalterationitem.")]
        CantAddAlterationitemsWithoutPosalterationitem = 201,

        [StringValue("It's not possible to add Alterationitems for which the AlterationOption is not related to a Posproduct.")]
        PosConnectorRequiresPosproductForAlterationOption = 202,

        //[StringValue("It's not possible to add Alterationitems for which the AlterationOption is not related to a Posproduct.")]
       MultipleAlterationitemsForPosalterationitem = 203
    }
}
