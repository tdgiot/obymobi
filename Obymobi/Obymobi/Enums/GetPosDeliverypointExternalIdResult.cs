using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum GetPosDeliverypointExternalIdResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// No pos deliverypoint founds for the specified number
        /// </summary>
        [StringValue("Er is geen pos tafel gevonden voor het opgegeven tafelnummer.")]
        NoPosdeliverypointFound = 200,

        /// <summary>
        /// Multiple pos deliverypoints found
        /// </summary>
        [StringValue("Er zijn meerdere pos tafels gevonden voor het opgegeven tafelnummer.")]
        MultiplePosdeliverypointsFound = 201
    }
}
