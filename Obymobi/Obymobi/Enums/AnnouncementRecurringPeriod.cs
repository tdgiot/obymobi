using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum AnnouncementRecurringPeriod : int
	{
        EveryXMinutes = 1,

        Hourly = 2,

        Daily = 3,

        Weekly = 4,

        Monthly = 5,

        Yearly = 6
	}
}
