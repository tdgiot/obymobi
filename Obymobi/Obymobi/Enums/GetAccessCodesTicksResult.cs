using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum GetAccessCodesTicksResult
    {
        Unspecified = 0,

        Success = 100,

        AuthenticationError = 998,

        Failure = 999
    }
}
