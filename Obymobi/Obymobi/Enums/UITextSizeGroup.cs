using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.Enums
{
	public enum UITextSizeGroup : int
    {
        [StringValue("Controls")]
        Controls = 1,

        [StringValue("Widgets")]
        Widgets = 2,

        [StringValue("Dialogs")]
        Dialogs = 3,

        [StringValue("Lists")]
        Lists = 4,

        [StringValue("Pages")]
        Pages = 5,

        [StringValue("Entertainment")]
        Entertainment = 6,

        [StringValue("Messages")]
        Messages = 7,

        [StringValue("Browser")]
        Browser = 8,

        [StringValue("PDF")]
        PDF = 9,

        [StringValue("Room Controls")]
        RoomControls = 10,

        [StringValue("Alterations")]
        Alterations = 11,

        [StringValue("Product dialog")]
        ProductDialog = 12,

        [StringValue("Other")]
        Other = 13
    }
}
