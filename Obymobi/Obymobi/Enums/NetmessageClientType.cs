namespace Obymobi.Enums
{
	/// <summary>
	/// Netmessage sender/receiver id type
	/// </summary>
    public enum NetmessageClientType
    {
        Unknown = 0,
        Emenu = 1,
        OnsiteServer = 2,
        Console = 3,
        SupportTools = 4,
        Cms = 5,
        Webservice = 6,
        MobileNoc = 7,

        External = 50
    }
}
