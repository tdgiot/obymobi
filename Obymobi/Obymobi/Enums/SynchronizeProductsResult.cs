﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum SynchronizeProductsResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Multiple products found for the specified external id
        /// </summary>
        [StringValue("Er zijn meerdere producten gevonden voor de opgegeven externe id.")]
        MultipleProductsFound = 200,

        /// <summary>
        /// Multiple pos categories found for the specified external id
        /// </summary>
        [StringValue("Er zijn meerdere pos categorieen gevonden voor de opgegeven externe id.")]
        MultiplePoscategoriesFound = 201,

        /// <summary>
        /// Multiple pos products found for the specified external id
        /// </summary>
        [StringValue("Er zijn meerdere pos producten gevonden voor de opgegeven externe id.")]
        MultiplePosproductsFound = 202,

        /// <summary>
        /// Multiple categories found for the pos category id
        /// </summary>
        [StringValue("Er zijn meerdere categorieen gevonden voor de opgegeven pos categorie id.")]
        MultipleCategoriesFound = 203,

        /// <summary>
        /// Multiple product categories found for the category id and product id
        /// </summary>
        [StringValue("Er zijn meerdere product categorieen gevonden voor de opgegeven categorie id and product id.")]
        MultipleProductCategoriesFound = 204
    }
}
