﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum RoomControlWidgetType : int
    {
        /// <summary>
        /// Light 2x1
        /// </summary>
        [StringValue("Light 2x1")]
        Light2x1 = 1,

        /// <summary>
        /// Scene 1x1
        /// </summary>
        [StringValue("Scene 1x1")]
        Scene1x1 = 2,

        /// <summary>
        /// Blind 2x1
        /// </summary>
        [StringValue("Blind 1x2")]
        Blind1x2 = 3,

        /// <summary>
        /// Thermostat 4x3
        /// </summary>
        [StringValue("Thermostat 4x3")]
        Thermostat4x2 = 4,

        /// <summary>
        /// Volume 1x2
        /// </summary>
        [StringValue("Volume 1x2")]
        Volume1x2 = 5,

        /// <summary>
        /// Light 1x1
        /// </summary>
        [StringValue("Light 1x1")]
        Light1x1 = 6,

        /// <summary>
        /// Channel 1x2
        /// </summary>
        [StringValue("Channel 1x2")]
        Channel1x2 = 7,

        /// <summary>
        /// Navigation 1x2
        /// </summary>
        [StringValue("Navigation 1x2")]
        Navigation1x2 = 8,

        /// <summary>
        /// Volume 1x4
        /// </summary>
        [StringValue("Volume 1x4")]
        Volume1x4 = 9,

        /// <summary>
        /// Channel 1x4
        /// </summary>
        [StringValue("Channel 1x4")]
        Channel1x4 = 10,

        /// <summary>
        /// Navigation 2x3
        /// </summary>
        [StringValue("Navigation 2x3")]
        Navigation2x3 = 11,

        /// <summary>
        /// Numpad 2x3
        /// </summary>
        [StringValue("Numpad 2x3")]
        Numpad2x3 = 12,

        /// <summary>
        /// Power button 1x1
        /// </summary>
        [StringValue("Power Button 1x1")]
        PowerButton1x1 = 13,

        /// <summary>
        /// Blind 1x1
        /// </summary>
        [StringValue("Blind 1x1")]
        Blind1x1 = 14,

        /// <summary>
        /// Channel 1x1
        /// </summary>
        [StringValue("Channel 1x1")]
        Channel1x1 = 15,

        /// <summary>
        /// Volume 1x1
        /// </summary>
        [StringValue("Volume 1x1")]
        Volume1x1 = 16,

        /// <summary>
        /// Area Blinds 1x1
        /// </summary>
        [StringValue("Area Blinds 1x2")]
        AreaBlinds1x2 = 17,

        /// <summary>
        /// Placeholder 1x1
        /// </summary>
        [StringValue("Placeholder 1x1")]
        Placeholder1x1 = 101,

        /// <summary>
        /// Placeholder 1x2
        /// </summary>
        [StringValue("Placeholder 1x2")]
        Placeholder1x2 = 102,
    }
}
