﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum SurveyResultSaveResult : int
    {
        /// <summary>
        /// Survey result was successfully saved
        /// </summary>
        [StringValue("Antwoorden zijn succesvol verzonden")]
        Success = 100,

		/// <summary>
		/// Xml Serialize Error
		/// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de antwoorden.")]
		XmlSerializeError = 201,

        /// <summary>
        /// Survey result failed to save
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de antwoorden. (205)")]
        EntitySaveRecursiveFalse = 202,

        /// <summary>
        /// Survey result failed to save
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de antwoorden. (206)")]
        EntitySaveRecursiveException = 203,
    }
}
