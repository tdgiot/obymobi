using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum FavoritecompanySaveResult : int
    {
        /// <summary>
        /// Favorite company save result is unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Favorite company was successfully saved
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Favorite company failed to save
        ///// </summary>
        //Failed = 200,

        ///// <summary>
        ///// Entity Save False
        ///// </summary>
        //EntitySaveFalse = 201,

        ///// <summary>
        ///// Entity Save Exception
        ///// </summary>
        //EntitySaveException = 202,

        ///// <summary>
        ///// Favorite company was already saved
        ///// </summary>
        //AlreadyExists = 300
    }
}
