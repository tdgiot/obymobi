﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum FileNameType
    {
        // Local, - Legacy.
        Cdn,
        Format,
        DeletionWildcard
    }
}
