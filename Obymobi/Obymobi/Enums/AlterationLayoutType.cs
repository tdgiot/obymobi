using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
    /// Enumeration which represents the AlterationLayoutType
	/// </summary>
	public enum AlterationLayoutType : int
	{
        [StringValue("Text on top")]
        TextOnTop = 0,

        [StringValue("Image & Text on top")]
		ImageAndTextOnTop = 1,

        [StringValue("Image & Text on left")]
		ImageAndTextOnLeft = 2,
	}
}
