﻿namespace Obymobi.Enums
{
    public enum WeatherType
    {
        _Unknown = 0,
        Blustery = 1, // Gusts of wind
        Clear = 2,
        Cloudy = 3,
        CloudyPartly = 4,
        Drizzle = 5, // Light rain
        Dust = 6,
        Fog = 7,
        Hail = 8,
        Haze = 9,
        Hurricane = 10,
        Mist = 11,
        Rain = 12,
        RainAndSleet = 13,
        Shower = 14,
        Smoke = 15,
        Snow = 16,
        Storm = 17,
        Thunderstorm = 18,
        Tornado = 19,
        TropicalStorm = 20,
        Wind = 21,
    }
}
