using Dionysos;

namespace Obymobi.Enums
{
    public enum ConnectionState
    {
        [StringValue("Connecting")]
        Connecting,

        [StringValue("Connected")]
        Connected,

        [StringValue("Disconnecting")]
        Disconnecting,

        [StringValue("Disconnected")]
        Disconnected,

        [StringValue("Reconnecting")]
        Reconnecting
    }
}
