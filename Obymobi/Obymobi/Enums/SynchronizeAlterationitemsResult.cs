﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum SynchronizeAlterationitemsResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Multiple alteration items found for the specified external id
        /// </summary>
        [StringValue("Er zijn meerdere alteration items gevonden voor de opgegeven pos alteration id.")]
        MultipleAlterationitemsFound = 200
    }
}
