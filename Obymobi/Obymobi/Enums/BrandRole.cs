﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum BrandRole
    {
        [StringValue("Owner")]
        Owner = 1000,

        [StringValue("Editor")]
        Editor = 500,

        [StringValue("Viewer")]
        Viewer = 100
    }
}