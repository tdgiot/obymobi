﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum NocStatusSystemState : int
    {
        /// <summary>
        /// Normal operational mode
        /// </summary>
        Normal = 0,
        Failing = 1
    }
}
