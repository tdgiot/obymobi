using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum LogSortOrder : int
	{
        /// <summary>
        /// Sort by recommendation.
        /// </summary>
        [StringValue("Aanbevolen.")]
        Recommended = 0,

        /// <summary>
        /// Sort by name.
        /// </summary>
        [StringValue("Naam.")]
        Name = 1
	}
}
