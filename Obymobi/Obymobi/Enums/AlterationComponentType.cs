﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public sealed class AlterationComponentType
    {
        public static readonly string Product = "Product";
        public static readonly string Alteration = "Alteration";
    }
}
