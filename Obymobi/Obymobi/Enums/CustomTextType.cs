using System;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the type of the custom text
    /// </summary>
    public enum CustomTextType : int
    {
        #region Company (Range: 1 to 1000)

        [MultiLineTextBox]
        [StringValue("Description")]
        CompanyDescription = 1,

        [StringValue("Description short")]
        CompanyDescriptionShort = 2,

        [MultiLineTextBox]
        [EnableEditingOnDefaultTab]
        [StringValue("Venue page description")]
        CompanyVenuePageDescription = 3,

        [EnableEditingOnDefaultTab]
        [StringValue("PMS terminal offline notification title")]
        CompanyPmsTerminalOfflineNotificationTitle = 4,

        [EnableEditingOnDefaultTab]
        [StringValue("PMS terminal offline notification text")]
        CompanyPmsTerminalOfflineNotificationText = 5,

        #endregion

        #region Deliverypointgroup (Range: 1000 to 2000)

        [StringValue("On the case title")]
		DeliverypointgroupOrderProcessingNotificationTitle = 1000,

        [StringValue("On the case text")]
        DeliverypointgroupOrderProcessingNotificationText = 1001,

        [StringValue("Service on the case title")]
        DeliverypointgroupServiceProcessingNotificationTitle = 1002,

        [StringValue("Service on the case text")]
        DeliverypointgroupServiceProcessingNotificationText = 1003,

        [StringValue("Complete title")]
        DeliverypointgroupOrderProcessedNotificationTitle = 1004,

        [StringValue("Complete text")]
        DeliverypointgroupOrderProcessedNotificationText = 1005,

        [StringValue("Service complete title")]
        DeliverypointgroupServiceProcessedNotificationTitle = 1006,

        [StringValue("Service complete text")]
        DeliverypointgroupServiceProcessedNotificationText = 1007,

        [StringValue("Freeform message title")]
        DeliverypointgroupFreeformMessageTitle = 1008,

        [StringValue("Out of charge title")]
        DeliverypointgroupOutOfChargeTitle = 1009,

        [StringValue("Out of charge text")]
        DeliverypointgroupOutOfChargeText = 1010,

        [StringValue("Order processed title")]
        DeliverypointgroupOrderProcessedTitle = 1011,

        [StringValue("Order processed text")]
        DeliverypointgroupOrderProcessedText = 1012,

        [StringValue("Order failed title")]
        DeliverypointgroupOrderFailedTitle = 1013,

        [StringValue("Order failed text")]
        DeliverypointgroupOrderFailedText = 1014,

        [StringValue("Order saving title")]
        DeliverypointgroupOrderSavingTitle = 1015,

        [StringValue("Order saving text")]
        DeliverypointgroupOrderSavingText = 1016,

        [StringValue("Order completed title")]
        DeliverypointgroupOrderCompletedTitle = 1017,

        [StringValue("Order completed text")]
        DeliverypointgroupOrderCompletedText = 1018,

        [StringValue("Service saving title")]
        DeliverypointgroupServiceSavingTitle = 1019,

        [StringValue("Service saving text")]
        DeliverypointgroupServiceSavingText = 1020,

        [StringValue("Service processed title")]
        DeliverypointgroupServiceProcessedTitle = 1021,

        [StringValue("Service processed text")]
        DeliverypointgroupServiceProcessedText = 1022,

        [StringValue("Service failed title")]
        DeliverypointgroupServiceFailedTitle = 1023,

        [StringValue("Service failed text")]
        DeliverypointgroupServiceFailedText = 1024,

        [StringValue("Rating saving title")]
        DeliverypointgroupRatingSavingTitle = 1025,

        [StringValue("Rating saving text")]
        DeliverypointgroupRatingSavingText = 1026,

        [StringValue("Rating processed title")]
        DeliverypointgroupRatingProcessedTitle = 1027,

        [StringValue("Rating processed text")]
        DeliverypointgroupRatingProcessedText = 1028,

        [StringValue("Ordering not available text")]
        DeliverypointgroupOrderingNotAvailableText = 1029,

        [StringValue("Pms device locked title")]
        DeliverypointgroupPmsDeviceLockedTitle = 1030,

        [StringValue("Pms device locked text")]
        DeliverypointgroupPmsDeviceLockedText = 1031,

        [StringValue("Pms checkin failed title")]
        DeliverypointgroupPmsCheckinFailedTitle = 1032,

        [StringValue("Pms checkin failed text")]
        DeliverypointgroupPmsCheckinFailedText = 1033,

        [StringValue("Pms restart title")]
        DeliverypointgroupPmsRestartTitle = 1034,

        [StringValue("Pms restart text")]
        DeliverypointgroupPmsRestartText = 1035,

        [StringValue("Pms welcome title")]
        DeliverypointgroupPmsWelcomeTitle = 1036,

        [StringValue("Pms welcome text")]
        DeliverypointgroupPmsWelcomeText = 1037,

        [StringValue("Pms checkout complete title")]
        DeliverypointgroupPmsCheckoutCompleteTitle = 1038,

        [StringValue("Pms checkout complete text")]
        DeliverypointgroupPmsCheckoutCompleteText = 1039,

        [StringValue("Pms checkout approve text")]
        DeliverypointgroupPmsCheckoutApproveText = 1040,

        [StringValue("Charger removed title")]
        DeliverypointgroupChargerRemovedTitle = 1041,

        [StringValue("Charger removed text")]
        DeliverypointgroupChargerRemovedText = 1042,

        [StringValue("Charger removed reminder title")]
        DeliverypointgroupChargerRemovedReminderTitle = 1043,

        [StringValue("Charger removed reminder text")]
        DeliverypointgroupChargerRemovedReminderText = 1044,

        [StringValue("Turn off privacy title")]
        DeliverypointgroupTurnOffPrivacyTitle = 1045,

        [StringValue("Turn off privacy text")]
        DeliverypointgroupTurnOffPrivacyText = 1046,

        [StringValue("Item currently unavailable text")]
        DeliverypointgroupItemCurrentlyUnavailableText = 1047,

        [StringValue("Alarm set while not charging title")]
        DeliverypointgroupAlarmSetWhileNotChargingTitle = 1048,

        [StringValue("Alarm set while not charging text")]
        DeliverypointgroupAlarmSetWhileNotChargingText = 1049,

        [StringValue("Hotel url 1")]
        DeliverypointgroupHotelUrl1 = 1050,

        [StringValue("Hotel url 1 caption")]
        DeliverypointgroupHotelUrl1Caption = 1051,

        [StringValue("Hotel url 2")]
        DeliverypointgroupHotelUrl2 = 1052,

        [StringValue("Hotel url 2 caption")]
        DeliverypointgroupHotelUrl2Caption = 1053,

        [StringValue("Hotel url 3")]
        DeliverypointgroupHotelUrl3 = 1054,

        [StringValue("Hotel url 3 caption")]
        DeliverypointgroupHotelUrl3Caption = 1055,

        [StringValue("Deliverypoint caption")]
        DeliverypointgroupDeliverypointCaption = 1056,

        [EnableEditingOnDefaultTab]
        [StringValue("Printing confirmation title")]
        DeliverypointgroupPrintingConfirmationTitle = 1057,

        [EnableEditingOnDefaultTab]
        [StringValue("Printing confirmation text")]
        DeliverypointgroupPrintingConfirmationText = 1058,

        [EnableEditingOnDefaultTab]
        [StringValue("Printing succeeded title")]
        DeliverypointgroupPrintingSucceededTitle = 1059,

        [EnableEditingOnDefaultTab]
        [StringValue("Printing succeeded text")]
        DeliverypointgroupPrintingSucceededText = 1060,

        [EnableEditingOnDefaultTab]
        [StringValue("Suggestions caption")]
        DeliverypointgroupSuggestionsCaption = 1061,

        [EnableEditingOnDefaultTab]
        [StringValue("Pages caption")]
        DeliverypointgroupPagesCaption = 1062,

        [EnableEditingOnDefaultTab]
        [StringValue("Roomservice charge text")]
        DeliverypointgroupRoomserviceChargeText = 1063,

        [EnableEditingOnDefaultTab]
        [StringValue("Title")]
        DeliverypointgroupTitle = 1064,

        [MultiLineTextBox]
        [EnableEditingOnDefaultTab]
        [StringValue("Description")]
        DeliverypointgroupDescription = 1065,

        [EnableEditingOnDefaultTab]
        [StringValue("Delivery location mismatch title")]
        DeliverypointgroupDeliveryLocationMismatchTitle = 1066,

        [EnableEditingOnDefaultTab]
        [StringValue("Delivery location mismatch text")]
        DeliverypointgroupDeliveryLocationMismatchText = 1067,

        [EnableEditingOnDefaultTab]
        [StringValue("Browser age verification title")]
        DeliverypointgroupBrowserAgeVerificationTitle = 1068,

        [MultiLineTextBox]
        [EnableEditingOnDefaultTab]
        [StringValue("Browser age verification text")]
        DeliverypointgroupBrowserAgeVerificationText = 1069,

        [EnableEditingOnDefaultTab]
        [StringValue("Browser age button")]
        DeliverypointgroupBrowserAgeVerificationButton = 1070,

        [EnableEditingOnDefaultTab]
        [StringValue("Restart application dialog title")]
        DeliverypointgroupRestartApplicationDialogTitle = 1071,

        [EnableEditingOnDefaultTab]
        [StringValue("Restart application dialog text")]
        DeliverypointgroupRestartApplicationDialogText = 1072,

        [EnableEditingOnDefaultTab]
        [StringValue("Reboot device dialog title")]
        DeliverypointgroupRebootDeviceDialogTitle = 1073,

        [EnableEditingOnDefaultTab]
        [StringValue("Reboot device dialog text")]
        DeliverypointgroupRebootDeviceDialogText = 1074,

        [EnableEditingOnDefaultTab]
        [StringValue("Estimated delivery time dialog title")]
        DeliverypointgroupEstimatedDeliveryTimeDialogTitle = 1075,

        [EnableEditingOnDefaultTab]
        [StringValue("Estimated delivery time dialog text")]
        DeliverypointgroupEstimatedDeliveryTimeDialogText = 1076,

        [EnableEditingOnDefaultTab]
        [StringValue("Clearing basket title")]
        DeliverypointgroupClearBasketTitle = 1078,

        [EnableEditingOnDefaultTab]
        [StringValue("Clearing basket text")]
        DeliverypointgroupClearBasketText = 1079,

        [EnableEditingOnDefaultTab]
        [StringValue("Product comment title")]
        DeliverypointgroupProductExtraCommentTitle = 1080,

        [EnableEditingOnDefaultTab]
        [StringValue("Product comment message")]
        DeliverypointgroupProductExtraCommentText = 1081,

        #endregion

        #region Category (Range: 2000 to 3000)

        [StringValue("Name")]
        CategoryName = 2000,

        [Markdown]
        [MultiLineTextBox]
        [StringValue("Description")]
        CategoryDescription = 2001,

        [EnableEditingOnDefaultTab]
        [StringValue("Order processed title")]
        CategoryOrderProcessedTitle = 2002,

        [EnableEditingOnDefaultTab]
        [StringValue("Order processed text")]
        CategoryOrderProcessedText = 2003,

        [EnableEditingOnDefaultTab]
        [StringValue("Order confirmation title")]
        CategoryOrderConfirmationTitle = 2004,

        [EnableEditingOnDefaultTab]
        [StringValue("Order confirmation text")]
        CategoryOrderConfirmationText = 2005,

        [EnableEditingOnDefaultTab]
        [StringValue("Button text")]
        CategoryButtonText = 2006,

        [EnableEditingOnDefaultTab]
        [StringValue("Alteration button text")]
        CategoryCustomizeButtonText = 2007,

        #endregion

        #region Product (Range: 3000 to 4000)

        [StringValue("Name")]
        ProductName = 3000,

        [Markdown]
        [MultiLineTextBox]
        [StringValue("Description")]
        ProductDescription = 3001,

        [StringValue("Button text")]
        ProductButtonText = 3002,

        [EnableEditingOnDefaultTab]
        [StringValue("Order processed title")]
        ProductOrderProcessedTitle = 3003,

        [EnableEditingOnDefaultTab]
        [StringValue("Order processed text")]
        ProductOrderProcessedText = 3004,

        [EnableEditingOnDefaultTab]
        [StringValue("Order confirmation title")]
        ProductOrderConfirmationTitle = 3005,

        [EnableEditingOnDefaultTab]
        [StringValue("Order confirmation text")]
        ProductOrderConfirmationText = 3006,

        [StringValue("Alteration button text")]
        ProductCustomizeButtonText = 3007,

        [EnableEditingOnDefaultTab]
        [StringValue("Instructions title")]
        ProductInstructionsTitle = 3008,

        [EnableEditingOnDefaultTab]
        [StringValue("Instructions subtitle")]
        ProductInstructionsSubtitle = 3009,

        [Markdown]
        [MultiLineTextBox]
        [EnableEditingOnDefaultTab]
        [StringValue("Instructions text")]
        ProductInstructionsText = 3010,

        [EnableEditingOnDefaultTab]
        [StringValue("Suggestions title")]
        ProductSuggestionsTitle = 3011,

        [EnableEditingOnDefaultTab]
        [StringValue("Suggestions subtitle")]
        ProductSuggestionsSubtitle = 3012,
        
        [MultiLineTextBox]
        [StringValue("Short description")]
        ProductShortDescription = 3013,

        #endregion        

        #region ActionButton (Range: 4000 to 5000)

        [StringValue("Name")]
        ActionButtonName = 4000,

        #endregion

        #region Advertisement (Range: 5000 to 6000)

        [StringValue("Name")]
        AdvertisementName = 5000,

        [MultiLineTextBox]
        [StringValue("Description")]
        AdvertisementDescription = 5001,

        #endregion

        #region Alteration (Range: 6000 to 7000)

        [StringValue("Name")]
        AlterationName = 6000,

        [MultiLineTextBox]
        [StringValue("Description")]
        AlterationDescription = 6001,
       
        [StringValue("Column title")]
        AlterationColumnTitle = 6002,

        [StringValue("Column subtitle")]
        AlterationColumnSubtitle = 6003,

        [StringValue("Overwrite 'No Thanks' option")]
        AlterationNoThanksOption = 6004,

        #endregion

        #region Alterationoption (Range: 7000 to 8000)

        [StringValue("Name")]
        AlterationoptionName = 7000,

        [MultiLineTextBox]
        [StringValue("Description")]
        AlterationoptionDescription = 7001,

        #endregion

        #region Amenity (Range: 8000 to 9000)

        [StringValue("Name")]
        AmenityName = 8000,

        #endregion

        #region Announcement (Range: 9000 to 10000)

        [StringValue("Title")]
        AnnouncementTitle = 9000,

        [MultiLineTextBox]
        [StringValue("Text")]
        AnnouncementText = 9001,

        #endregion

        #region Attachment (Range: 10000 to 11000)

        [StringValue("Name")]
        AttachmentName = 10000,

        #endregion

        #region Entertainmentcategory (Range: 11000 to 12000)

        [StringValue("Name")]
        EntertainmentcategoryName = 11000,

        #endregion

        #region Genericcategory (Range: 12000 to 13000)

        [StringValue("Name")]
        GenericcategoryName = 12000,

        #endregion

        #region Genericproduct (Range: 13000 to 14000)

        [StringValue("Name")]
        GenericproductName = 13000,

        [MultiLineTextBox]
        [StringValue("Description")]
        GenericproductDescription = 13001,

        #endregion

        #region Page (Range: 14000 to 15000)

        [StringValue("Name")]
        PageName = 14000,

        #endregion

        #region PageTemplate (Range: 15000 to 16000)

        [StringValue("Template name")]
        PageTemplateName = 15000,

        #endregion

        #region PointOfInterest (Range: 16000 to 17000)

        [MultiLineTextBox]
        [StringValue("Description")]
        PointOfInterestDescription = 16000,

        [StringValue("Description single line")]
        PointOfInterestDescriptionSingleLine = 16001,

        [MultiLineTextBox]
        [StringValue("Venue page description")]
        PointOfInterestVenuePageDescription = 16002,

        #endregion

        #region RoomControlArea (Range: 17000 to 18000)

        [StringValue("Name")]
        RoomControlAreaName = 17000,

        #endregion

        #region RoomControlComponent (Range: 18000 to 19000)

        [StringValue("Name")]
        RoomControlComponentName = 18000,

        #endregion

        #region RoomControlSection (Range: 19000 to 20000)

        [StringValue("Name")]
        RoomControlSectionName = 19000,

        #endregion

        #region RoomControlSectionItem (Range: 20000 to 21000)

        [StringValue("Name")]
        RoomControlSectionItemName = 20000,

        #endregion

        #region RoomControlWidget (Range: 21000 to 22000)

        [StringValue("Caption")]
        RoomControlWidgetCaption = 21000,

        #endregion

        #region ScheduledMessage (Range: 22000 to 23000)

        [StringValue("Title")]
        ScheduledMessageTitle = 22000,

        [StringValue("Message")]
        ScheduledMessageMessage = 22001,

        #endregion

        #region Site (Range: 23000 to 24000)

        [MultiLineTextBox]
        [StringValue("Description")]
        SiteDescription = 23000,

        #endregion

        #region SiteTemplate (Range: 24000 to 25000)

        [StringValue("Name")]
        SiteTemplateName = 24000,

        #endregion

        #region Station (Range: 25000 to 26000)

        [StringValue("Name")]
        StationName = 25000,

        [MultiLineTextBox]
        [StringValue("Description")]
        StationDescription = 25001,

        [StringValue("Success message")]
        StationSuccessMessage = 25002,

        #endregion

        #region UIFooterItem (Range: 26000 to 27000)

        [StringValue("Name")]
        UIFooterItemName = 26000,

        #endregion

        #region UITab (Range: 27000 to 28000)

        [StringValue("Caption")]
        UITabCaption = 27000,

        [StringValue("Url")]
        UITabUrl = 27001,

        #endregion

        #region UIWidget (Range: 28000 to 29000)

        [StringValue("Caption")]
        UIWidgetCaption = 28000,

        [DisableRendering]
        [StringValue("FieldValue 1")]
        UIWidgetFieldValue1 = 28001,

        [DisableRendering]
        [StringValue("FieldValue 2")]
        UIWidgetFieldValue2 = 28002,

        [DisableRendering]
        [StringValue("FieldValue 3")]
        UIWidgetFieldValue3 = 28003,

        [DisableRendering]
        [StringValue("FieldValue 4")]
        UIWidgetFieldValue4 = 28004,

        [DisableRendering]
        [StringValue("FieldValue 5")]
        UIWidgetFieldValue5 = 28005,

        #endregion

        #region VenueCategory (Range: 29000 to 30000)

        [StringValue("Name")]
        VenueCategoryName = 29000,

        [StringValue("Name plural")]
        VenueCategoryNamePlural = 29001,

        #endregion

        #region Availability (Range: 30000 to 31000)

        [StringValue("Name")]
        AvailabilityName = 30000,

        [StringValue("Status")]
        AvailabilityStatus = 30001,

        #endregion

        #region ClientConfiguration (Range: 31000 to 32000)

        [EnableEditingOnDefaultTab]
        [StringValue("On the case title")]
        ClientConfigurationOrderProcessingNotificationTitle = 31000,

        [EnableEditingOnDefaultTab]
        [StringValue("On the case text")]
        ClientConfigurationOrderProcessingNotificationText = 31001,

        [EnableEditingOnDefaultTab]
        [StringValue("Service on the case title")]
        ClientConfigurationServiceProcessingNotificationTitle = 31002,

        [EnableEditingOnDefaultTab]
        [StringValue("Service on the case text")]
        ClientConfigurationServiceProcessingNotificationText = 31003,

        [EnableEditingOnDefaultTab]
        [StringValue("Complete title")]
        ClientConfigurationOrderProcessedNotificationTitle = 31004,

        [EnableEditingOnDefaultTab]
        [StringValue("Complete text")]
        ClientConfigurationOrderProcessedNotificationText = 31005,

        [EnableEditingOnDefaultTab]
        [StringValue("Service complete title")]
        ClientConfigurationServiceProcessedNotificationTitle = 31006,

        [EnableEditingOnDefaultTab]
        [StringValue("Service complete text")]
        ClientConfigurationServiceProcessedNotificationText = 31007,

        [EnableEditingOnDefaultTab]
        [StringValue("Freeform message title")]
        ClientConfigurationFreeformMessageTitle = 31008,

        [EnableEditingOnDefaultTab]
        [StringValue("Out of charge title")]
        ClientConfigurationOutOfChargeTitle = 31009,

        [EnableEditingOnDefaultTab]
        [StringValue("Out of charge text")]
        ClientConfigurationOutOfChargeText = 31010,

        [EnableEditingOnDefaultTab]
        [StringValue("Order processed title")]
        ClientConfigurationOrderProcessedTitle = 31011,

        [EnableEditingOnDefaultTab]
        [StringValue("Order processed text")]
        ClientConfigurationOrderProcessedText = 31012,

        [EnableEditingOnDefaultTab]
        [StringValue("Order failed title")]
        ClientConfigurationOrderFailedTitle = 31013,

        [EnableEditingOnDefaultTab]
        [StringValue("Order failed text")]
        ClientConfigurationOrderFailedText = 31014,

        [EnableEditingOnDefaultTab]
        [StringValue("Order saving title")]
        ClientConfigurationOrderSavingTitle = 31015,

        [EnableEditingOnDefaultTab]
        [StringValue("Order saving text")]
        ClientConfigurationOrderSavingText = 31016,

        [EnableEditingOnDefaultTab]
        [StringValue("Order completed title")]
        ClientConfigurationOrderCompletedTitle = 31017,

        [EnableEditingOnDefaultTab]
        [StringValue("Order completed text")]
        ClientConfigurationOrderCompletedText = 31018,

        [EnableEditingOnDefaultTab]
        [StringValue("Service saving title")]
        ClientConfigurationServiceSavingTitle = 31019,

        [EnableEditingOnDefaultTab]
        [StringValue("Service saving text")]
        ClientConfigurationServiceSavingText = 31020,

        [EnableEditingOnDefaultTab]
        [StringValue("Service processed title")]
        ClientConfigurationServiceProcessedTitle = 31021,

        [EnableEditingOnDefaultTab]
        [StringValue("Service processed text")]
        ClientConfigurationServiceProcessedText = 31022,

        [EnableEditingOnDefaultTab]
        [StringValue("Service failed title")]
        ClientConfigurationServiceFailedTitle = 31023,

        [EnableEditingOnDefaultTab]
        [StringValue("Service failed text")]
        ClientConfigurationServiceFailedText = 31024,

        [EnableEditingOnDefaultTab]
        [StringValue("Ordering not available text")]
        ClientConfigurationOrderingNotAvailableText = 31025,

        [EnableEditingOnDefaultTab]
        [StringValue("Pms device locked title")]
        ClientConfigurationPmsDeviceLockedTitle = 31026,

        [EnableEditingOnDefaultTab]
        [StringValue("Pms device locked text")]
        ClientConfigurationPmsDeviceLockedText = 31027,

        [EnableEditingOnDefaultTab]
        [StringValue("Pms checkin failed title")]
        ClientConfigurationPmsCheckinFailedTitle = 31028,

        [EnableEditingOnDefaultTab]
        [StringValue("Pms checkin failed text")]
        ClientConfigurationPmsCheckinFailedText = 31029,

        [EnableEditingOnDefaultTab]
        [StringValue("Pms restart title")]
        ClientConfigurationPmsRestartTitle = 31030,

        [EnableEditingOnDefaultTab]
        [StringValue("Pms restart text")]
        ClientConfigurationPmsRestartText = 31031,

        [EnableEditingOnDefaultTab]
        [StringValue("Pms welcome title")]
        ClientConfigurationPmsWelcomeTitle = 31032,

        [EnableEditingOnDefaultTab]
        [StringValue("Pms welcome text")]
        ClientConfigurationPmsWelcomeText = 31033,

        [EnableEditingOnDefaultTab]
        [StringValue("Pms checkout complete title")]
        ClientConfigurationPmsCheckoutCompleteTitle = 31034,

        [EnableEditingOnDefaultTab]
        [StringValue("Pms checkout complete text")]
        ClientConfigurationPmsCheckoutCompleteText = 31035,

        [EnableEditingOnDefaultTab]
        [StringValue("Pms checkout approve text")]
        ClientConfigurationPmsCheckoutApproveText = 31036,

        [EnableEditingOnDefaultTab]
        [StringValue("Charger removed title")]
        ClientConfigurationChargerRemovedTitle = 31037,

        [EnableEditingOnDefaultTab]
        [StringValue("Charger removed text")]
        ClientConfigurationChargerRemovedText = 31038,

        [EnableEditingOnDefaultTab]
        [StringValue("Charger removed reminder title")]
        ClientConfigurationChargerRemovedReminderTitle = 31039,

        [EnableEditingOnDefaultTab]
        [StringValue("Charger removed reminder text")]
        ClientConfigurationChargerRemovedReminderText = 31040,

        [EnableEditingOnDefaultTab]
        [StringValue("Turn off privacy title")]
        ClientConfigurationTurnOffPrivacyTitle = 31041,

        [EnableEditingOnDefaultTab]
        [StringValue("Turn off privacy text")]
        ClientConfigurationTurnOffPrivacyText = 31042,

        [EnableEditingOnDefaultTab]
        [StringValue("Item currently unavailable text")]
        ClientConfigurationItemCurrentlyUnavailableText = 31043,

        [EnableEditingOnDefaultTab]
        [StringValue("Alarm set while not charging title")]
        ClientConfigurationAlarmSetWhileNotChargingTitle = 31044,

        [EnableEditingOnDefaultTab]
        [StringValue("Alarm set while not charging text")]
        ClientConfigurationAlarmSetWhileNotChargingText = 31045,

        [EnableEditingOnDefaultTab]
        [StringValue("Deliverypoint caption")]
        ClientConfigurationDeliverypointCaption = 31046,

        [EnableEditingOnDefaultTab]
        [StringValue("Printing confirmation title")]
        ClientConfigurationPrintingConfirmationTitle = 31047,

        [EnableEditingOnDefaultTab]
        [StringValue("Printing confirmation text")]
        ClientConfigurationPrintingConfirmationText = 31048,

        [EnableEditingOnDefaultTab]
        [StringValue("Printing succeeded title")]
        ClientConfigurationPrintingSucceededTitle = 31049,

        [EnableEditingOnDefaultTab]
        [StringValue("Printing succeeded text")]
        ClientConfigurationPrintingSucceededText = 31050,

        [EnableEditingOnDefaultTab]
        [StringValue("Suggestions caption")]
        ClientConfigurationSuggestionsCaption = 31051,

        [EnableEditingOnDefaultTab]
        [StringValue("Roomservice charge text")]
        ClientConfigurationRoomserviceChargeText = 31052,

        [EnableEditingOnDefaultTab]
        [StringValue("Delivery location mismatch title")]
        ClientConfigurationDeliveryLocationMismatchTitle = 31053,

        [EnableEditingOnDefaultTab]
        [StringValue("Delivery location mismatch text")]
        ClientConfigurationDeliveryLocationMismatchText = 31054,

        [EnableEditingOnDefaultTab]
        [StringValue("Browser age verification title")]
        ClientConfigurationBrowserAgeVerificationTitle = 31055,

        [MultiLineTextBox]
        [EnableEditingOnDefaultTab]
        [StringValue("Browser age verification text")]
        ClientConfigurationBrowserAgeVerificationText = 31056,

        [EnableEditingOnDefaultTab]
        [StringValue("Browser age button")]
        ClientConfigurationBrowserAgeVerificationButton = 31057,

        [EnableEditingOnDefaultTab]
        [StringValue("Restart application dialog title")]
        ClientConfigurationRestartApplicationDialogTitle = 31058,

        [EnableEditingOnDefaultTab]
        [StringValue("Restart application dialog text")]
        ClientConfigurationRestartApplicationDialogText = 31059,

        [EnableEditingOnDefaultTab]
        [StringValue("Reboot device dialog title")]
        ClientConfigurationRebootDeviceDialogTitle = 31060,

        [EnableEditingOnDefaultTab]
        [StringValue("Reboot device dialog text")]
        ClientConfigurationRebootDeviceDialogText = 31061,

        [EnableEditingOnDefaultTab]
        [StringValue("Estimated delivery time dialog title")]
        ClientConfigurationEstimatedDeliveryTimeDialogTitle = 31062,

        [EnableEditingOnDefaultTab]
        [StringValue("Estimated delivery time dialog text")]
        ClientConfigurationEstimatedDeliveryTimeDialogText = 31063,

        [EnableEditingOnDefaultTab]
        [StringValue("Clearing basket title")]
        ClientConfigurationClearBasketTitle = 31064,

        [EnableEditingOnDefaultTab]
        [StringValue("Clearing basket text")]
        ClientConfigurationClearBasketText = 31065,

        #endregion

        #region Productgroup (Range: 32000 to 33000)

        [StringValue("Name")]
        ProductgroupName = 32000,
        
        [StringValue("Column title")]
        ProductgroupColumnTitle = 32001,

        [StringValue("Column subtitle")]
        ProductgroupColumnSubtitle = 32002,

        #endregion

        #region Routestephandler (Range: 33000 to 34000)

        [EnableEditingOnDefaultTab]
        [StringValue("On the case caption")]
        RoutestephandlerOnTheCaseCaption = 33000,

        [EnableEditingOnDefaultTab]
        [StringValue("Complete caption")]
        RoutestephandlerCompleteCaption = 33001,

        [EnableEditingOnDefaultTab]
        [StringValue("Manually process order caption")]
        RoutestephandlerManuallyProcessOrderCaption = 33002,

        #endregion

        #region InfraredCommand (Range: 34000 to 35000)

        [StringValue("Name")]
        InfraredCommandName = 34000,

        #endregion

        #region AppLess - carousel item (35000 to 36000)

        [StringValue("Message")]
        AppLessCarouselItemMessage = 35000,

        #endregion

        #region AppLess - navigation menu item (36000 to 37000)

        [StringValue("Text")]
        AppLessNavigationMenuItemText = 36000,

        #endregion

        #region AppLess - Widget Action Banner (37000 to 38000)

        [StringValue("Text")]
        AppLessWidgetActionBannerText = 37000,

        #endregion

        #region AppLess - Widget Action Button (38000 to 39000)

        [StringValue("Text")]
        AppLessWidgetActionButtonText = 38000,

        #endregion

        #region AppLess - Widget Language Switcher (39000 to 40000)

        [StringValue("Text")]
        AppLessWidgetLanguageSwitcherText = 39000,

        #endregion

        #region AppLess - Widget Page Title (40000 to 41000)

        [StringValue("Title")]
        AppLessWidgetPageTitle = 40000,

        #endregion

        #region Outlet (41000 to 42000)

        [EnableEditingOnDefaultTab]
        [StringValue("Checkout Information Section Title")]
        CheckoutInformationSectionTitle = 41000,

        [EnableEditingOnDefaultTab]
        [StringValue("Checkout Information Section Description")]
        CheckoutInformationSectionDescription = 41001,

        [EnableEditingOnDefaultTab]
        [StringValue("Customer Details Section Title")]
        CustomerDetailsSectionTitle = 41002,

        [EnableEditingOnDefaultTab]
        [StringValue("Customer Details Description")]
        CustomerDetailsDescription = 41003,

        [EnableEditingOnDefaultTab]
        [StringValue("Customer Details Phone Label")]
        CustomerDetailsPhoneLabel = 41004,

        [EnableEditingOnDefaultTab]
        [StringValue("Customer Details Email Label")]
        CustomerDetailsEmailLabel = 41005,
        
        [EnableEditingOnDefaultTab]
        [StringValue("Payment Section Title")]
        PaymentSectionTitle = 41006,

        [EnableEditingOnDefaultTab]
        [StringValue("Order Summary Section Title")]
        OrderSummarySectionTitle = 41007,

        [EnableEditingOnDefaultTab]
        [StringValue("Order Summary Items Label")]
        OrderSummaryItemsLabel = 41008,

        [EnableEditingOnDefaultTab]
        [StringValue("Order Summary Total Label")]
        OrderSummaryTotalLabel = 41009,

        [EnableEditingOnDefaultTab]
        [StringValue("Place Order Button Text")]
        PlaceOrderButtonText = 41010,

        [EnableEditingOnDefaultTab]
        [StringValue("Place Service Request Button Text")]
        PlaceServiceRequestButtonText = 41011,

        [EnableEditingOnDefaultTab]
        [StringValue("Order Completed Confirmation Title")]
        OrderCompletedConfirmationTitle = 41012,

        [EnableEditingOnDefaultTab]
        [StringValue("Order Completed Confirmation Description")]
        OrderCompletedConfirmationDescription = 41013,

        [EnableEditingOnDefaultTab]
        [StringValue("Order Failed Confirmation Title")]
        OrderFailedConfirmationTitle = 41014,

        [EnableEditingOnDefaultTab]
        [StringValue("Order Failed Confirmation Description")]
        OrderFailedConfirmationDescription = 41015,

        [EnableEditingOnDefaultTab]
        [StringValue("Service Type Section Title")]
        ServiceTypeSectionTitle = 41016,

        [EnableEditingOnDefaultTab]
        [StringValue("Tipping Label")]
        TippingLabel = 41017,

        [EnableEditingOnDefaultTab]
        [StringValue("Tipping Section Title")]
        TippingSectionTitle = 41018,

        [EnableEditingOnDefaultTab]
        [StringValue("Tipping Description")]
        TippingDescription = 41019,

        [EnableEditingOnDefaultTab]
        [StringValue("Customer Details Phone Placeholder")]
        CustomerDetailsPhonePlaceholder = 41020,

        [EnableEditingOnDefaultTab]
        [StringValue("Customer Details Email Placeholder")]
        CustomerDetailsEmailPlaceholder = 41021,

        [EnableEditingOnDefaultTab]
        [StringValue("Outlet Closed Text")]
        OutletClosedText = 41022,
        
        [EnableEditingOnDefaultTab]
        [StringValue("Ordering Disabled Text")]
        OutletOrderingDisabledText = 41023,

        [EnableEditingOnDefaultTab]
        [StringValue("Customer Details Name Label")]
        CustomerDetailsNameLabel = 41024,

        [EnableEditingOnDefaultTab]
        [StringValue("Customer Details Name Placeholder")]
        CustomerDetailsNamePlaceholder = 41025,

        [EnableEditingOnDefaultTab]
        [StringValue("View Basket Button Text")]
        ViewBasketButtonText = 41026,

        [EnableEditingOnDefaultTab]
        [StringValue("Back To Basket Button Text")]
        BackToBasketButtonText = 41027,

        [EnableEditingOnDefaultTab]
        [StringValue("Back To Homepage Button Text")]
        BackToHomepageButtonText = 41028,

        [EnableEditingOnDefaultTab]
        [StringValue("Checkout View Title")]
        CheckoutTitle = 41029,

        [EnableEditingOnDefaultTab]
        [StringValue("Payment Is Processing Message")]
        PaymentIsProcessingMessage = 41030,

        [EnableEditingOnDefaultTab]
        [StringValue("Payment Failed Popup Title")]
        PaymentViewPopupPaymentNotValidTitle = 41031,

        [EnableEditingOnDefaultTab]
        [StringValue("Payment Failed Popup Message")]
        PaymentViewPopupPaymentNotValidMessage = 41032,
              
        [EnableEditingOnDefaultTab]
        [StringValue("Checkout Failed Popup Title")]
        CheckoutPopupFailedTitle = 41033,

        [EnableEditingOnDefaultTab]
        [StringValue("Checkout Failed Popup Message")]
        CheckoutPopupFailedMessage = 41034,

        [EnableEditingOnDefaultTab]
        [StringValue("Checkout Popup Failed Close")]
        CheckoutPopupFailedClose = 41035,

        // AcceptOurTermsLabel = 41036,

        // TermsAndConditionsText = 41037,

        [EnableEditingOnDefaultTab]
        [StringValue("Basket Total Price")]
        BasketTotalPrice = 41038,

        [EnableEditingOnDefaultTab]
        [StringValue("Basket View Title")]
        BasketTitle = 41039,

        [EnableEditingOnDefaultTab]
        [StringValue("Checkout Button Text")]
        CheckoutButtonText = 41040,

        [EnableEditingOnDefaultTab]
        [StringValue("Payment Failed Popup Close")]
        PaymentViewPopupPaymentNotValidClose = 41041,

        [EnableEditingOnDefaultTab]
        [StringValue("Payment Pending Title")]
        PaymentPendingTitle = 41042,

        [EnableEditingOnDefaultTab]
        [StringValue("Payment Pending Message")]
        PaymentPendingMessage = 41043,

        [EnableEditingOnDefaultTab]
        [StringValue("Payment Cancelled Title")]
        PaymentCancelledTitle = 41045,

        [EnableEditingOnDefaultTab]
        [StringValue("Payment Cancelled Message")]
        PaymentCancelledMessage = 41046,

        [EnableEditingOnDefaultTab]
        [StringValue("Basket Minimal Order Value")]
        BasketCheckoutMinimalOrderValue = 41047,

        [EnableEditingOnDefaultTab]
        [StringValue("Payment Details Incorrect Message")]
        PaymentServiceInvalidInformation = 41048,

        [EnableEditingOnDefaultTab]
        [StringValue("Product Available Again")]
        ProductsAvailableAgain = 41049,

        [Markdown]
        [EnableEditingOnDefaultTab]
        [MultiLineTextBox]
        [StringValue("Hard Opt-In Checkbox Label")]
        OptInCheckboxLabel = 41050,

        [EnableEditingOnDefaultTab]
        [StringValue("Order Summary Subtotal Label")]
        OrderSummarySubtotalLabel = 41051,

        [Markdown]
        [EnableEditingOnDefaultTab]
        [MultiLineTextBox]
        [StringValue("Hard Opt-In Radio Button Description")]
        OptInRadioButtonDescription = 41052,

        [EnableEditingOnDefaultTab]
        [StringValue("Hard Opt-In Radio Button Yes label")]
        OptInRadioButtonYesLabel = 41053,

        [EnableEditingOnDefaultTab]
        [StringValue("Hard Opt-In Radio Button No label")]
        OptInRadioButtonNoLabel = 41054,

        [EnableEditingOnDefaultTab]
        [StringValue("Payment Config Incorrect Message")]
        PaymentConfigurationInvalid = 41055,


        [EnableEditingOnDefaultTab]
        [StringValue("Covers Default Option Text")]
        CoversDefaultOptionText = 41056,


        [EnableEditingOnDefaultTab]
        [StringValue("Covers Other Option Text")]
        CoversOtherOptionText = 41057,


        [EnableEditingOnDefaultTab]
        [StringValue("Covers Numeric Placeholder Text")]
        CoversNumericPlaceholderText = 41058,

        #endregion

        #region Service method (42000 to 43000)

        [StringValue("Service Method Label")]
        ServiceMethodLabel = 42000,

        [StringValue("Service Method Description")]
        ServiceMethodDescription = 42001,

        [EnableEditingOnDefaultTab]
        [StringValue("Deliverypoint Label")]
        DeliverypointLabel = 42002,

        [EnableEditingOnDefaultTab]
        [StringValue("Room Entry Text")]
        RoomEntryText = 42003,

        [EnableEditingOnDefaultTab]
        [StringValue("Last Name Label")]
        LastNameLabel = 42004,

        [EnableEditingOnDefaultTab]
        [StringValue("Service Charge Label")]
        ServiceChargeLabel = 42005,

        //TODO decide to keep
        //[EnableEditingOnDefaultTab]
        //[StringValue("Confirmation Text")]
        //ConfirmationText = 42006,

        //TODO Nobody seems to be missing this text: https://craveinteractive.atlassian.net/browse/APPLESS-1398
        //[EnableEditingOnDefaultTab]
        //[StringValue("Order Number Label")]
        //OrderNumberLabel = 42007,

        //TODO decide to keep
        //[EnableEditingOnDefaultTab]
        //[StringValue("Order Completed Text")]
        //OrderCompletedText = 42008,

        [EnableEditingOnDefaultTab]
        [StringValue("Deliverypointgroup Label")]
        DeliveryPointGroupLabel = 42009,

        // [EnableEditingOnDefaultTab]
        // [StringValue("Building name label")]
        // BuildingNameLabel = 42010,
        //
        // [EnableEditingOnDefaultTab]
        // [StringValue("Building name placeholder")]
        // BuildingNamePlaceholder = 42011,

        // [EnableEditingOnDefaultTab]
        // [StringValue("Address label")]
        // AddressLabel = 42012,
        //
        // [EnableEditingOnDefaultTab]
        // [StringValue("Address placeholder")]
        // AddressPlaceholder = 42013,

        // [EnableEditingOnDefaultTab]
        // [StringValue("Zip code label")]
        // ZipCodeLabel = 42014,
        //
        // [EnableEditingOnDefaultTab]
        // [StringValue("Zip code placeholder")]
        // ZipCodePlaceholder = 42015,

        // [EnableEditingOnDefaultTab]
        // [StringValue("City label")]
        // CityLabel = 42016,
        //
        // [EnableEditingOnDefaultTab]
        // [StringValue("City placeholder")]
        // CityPlaceholder = 42017,

        [EnableEditingOnDefaultTab]
        [StringValue("Instructions label")]
        InstructionsLabel = 42018,

        [EnableEditingOnDefaultTab]
        [StringValue("Instructions placeholder")]
        InstructionsPlaceholder = 42019,

        [EnableEditingOnDefaultTab]
        [StringValue("Address missing text")]
        AddressMissingText = 42020,

        [EnableEditingOnDefaultTab]
        [StringValue("City required text")]
        CityRequiredText = 42021,
        
        [EnableEditingOnDefaultTab]
        [StringValue("Postal code required text")]
        PostalCodeRequiredText = 42022,
        
        [EnableEditingOnDefaultTab]
        [StringValue("Address outside range")]
        AddressOutsideRangeText = 42023,
        
        [EnableEditingOnDefaultTab]
        [StringValue("House number missing text")]
        HouseNumberMissingText = 42024,

        [EnableEditingOnDefaultTab]
        [StringValue("Free of charge label")]
        ServiceFreeLabel = 42025,

        [EnableEditingOnDefaultTab]
        [StringValue("Delivery charge label")]
        DeliveryChargeLabel = 42026,

        [EnableEditingOnDefaultTab]
        [StringValue("Address lookup label")]
        AddressLookupLabel = 42027,

        [EnableEditingOnDefaultTab]
        [StringValue("Minimum order value")]
        MinimalOrderPriceText = 42028,

        [EnableEditingOnDefaultTab]
        [StringValue("Ask for Covers title")]
        CoversTitleText = 42029,

        [EnableEditingOnDefaultTab]
        [StringValue("Ask for Covers description")]
        CoversDescText = 42030,

        #endregion

        #region Checkout Method (43000 to 44000)

        [StringValue("Label")]
        CheckoutMethodLabel = 43000,

        [StringValue("Description")]
        CheckoutMethodDescription = 43001,

        [StringValue("Confirmation Description")]
        CheckoutMethodConfirmationDescription = 43002,
        
        [EnableEditingOnDefaultTab]
        [StringValue("Deliverypoint Label")]
        CheckoutMethodDeliverypointLabel = 43003,

        [EnableEditingOnDefaultTab]
        [StringValue("Last Name Label")]
        CheckoutMethodLastNameLabel = 43004,

        [EnableEditingOnDefaultTab]
        [StringValue("Deliverypointgroup Label")]
        CheckoutMethodDeliveryPointGroupLabel = 43005,

        [Markdown]
        [MultiLineTextBox]
        [StringValue("Terms and Conditions label")]
        CheckoutMethodTermsAndConditionsLabel = 43006,

        [EnableEditingOnDefaultTab]
        [StringValue("Terms and Conditions required error")]
        CheckoutMethodTermsAndConditionsRequiredError = 43007,

        #endregion

        #region Appless - Widget Wait Time (44000 to 45000)

        [StringValue("Text")]
        AppLessWidgetWaitTimeText = 44000,

        #endregion
        
        #region Appless - Widget Opening Time (45000 to 46000)

        [StringValue("Title")]
        AppLessWidgetOpeningTimeTitle = 45000,

        [EnableEditingOnDefaultTab]
        [StringValue("Currently Open")]
        AppLessWidgetOpeningTimeCurrentlyOpen = 45001,

        [EnableEditingOnDefaultTab]
        [StringValue("Closed")]
        AppLessWidgetOpeningTimeClosed = 45002,

        #endregion

        #region AppLess - Widget Markdown (46000 to 47000)

        [StringValue("Title")]
        AppLessWidgetMarkdownTitle = 46000,

        [Markdown]
        [MultiLineTextBox]
        [StringValue("Text")]
        AppLessWidgetMarkdownText = 46001,

        #endregion

        #region AppLess - Application Configuration (47000 to 48000)

        [EnableEditingOnDefaultTab]
        [StringValue("This Is Required")]
        [DisplayOrder(1)]
        ThisIsRequired = 47001,

        [EnableEditingOnDefaultTab]
        [StringValue("Cookie Accept Button Text")]
        [DisplayOrder(1)]
        CookieAcceptButtonText = 47002,

        [EnableEditingOnDefaultTab]
        [StringValue("Cookie Terms And Conditions Label")]
        [DisplayOrder(1)]
        CookieTermsAndConditionsLabel = 47003,

        [EnableEditingOnDefaultTab]
        [StringValue("Cookie Title")]
        [DisplayOrder(1)]
        CookieTitleText = 47004,

        [EnableEditingOnDefaultTab]
        [StringValue("Phone Number Invalid")]
        [DisplayOrder(1)]
        InvalidPhoneNumberText = 47005,

        [EnableEditingOnDefaultTab]
        [StringValue("Email Address Invalid")]
        [DisplayOrder(1)]
        InvalidEmailAddressText = 47006,

        [EnableEditingOnDefaultTab]
        [StringValue("Disclaimer Label")]
        [DisplayOrder(1)]
        DisclaimerLabel = 47007,

        [EnableEditingOnDefaultTab]
        [StringValue("Save Changes Button Text")]
        [DisplayOrder(1)]
        SaveChangesButtonText = 47008,

        [EnableEditingOnDefaultTab]
        [StringValue("Show More")]
        [DisplayOrder(1)]
        ShowMoreText = 47009,

        [EnableEditingOnDefaultTab]
        [StringValue("Show Less")]
        [DisplayOrder(1)]
        ShowLessText = 47010,

        [EnableEditingOnDefaultTab]
        [StringValue("Popup Cancel")]
        [DisplayOrder(1)]
        PopupCancel = 47011,

        [EnableEditingOnDefaultTab]
        [StringValue("Payment Order Total Label")]
        [DisplayOrder(1)]
        PaymentOrderTotal = 47012,

        [EnableEditingOnDefaultTab]
        [StringValue("Payment Choose Payment Method")]
        [DisplayOrder(1)]
        PaymentChoosePaymentMethod = 47013,

        [EnableEditingOnDefaultTab]
        [StringValue("Product Remove Item")]
        [DisplayOrder(1)]
        ProductRemoveItem = 47014,

        [EnableEditingOnDefaultTab]
        [StringValue("Alterations Minimum Required Text")]
        [DisplayOrder(1)]
        AlterationsMinRequired = 47015,

        [EnableEditingOnDefaultTab]
        [StringValue("Alterations Maximum Reached Text")]
        [DisplayOrder(1)]
        AlterationsMaxRequired = 47016,

        [EnableEditingOnDefaultTab]
        [StringValue("Alterations Optional Text")]
        [DisplayOrder(1)]
        AlterationsOptional = 47017,

        [EnableEditingOnDefaultTab]
        [StringValue("Alterations Min Options")]
        [DisplayOrder(1)]
        AlterationsMinOptions = 47018,

        [EnableEditingOnDefaultTab]
        [StringValue("Alterations Max Options")]
        [DisplayOrder(1)]
        AlterationsMaxOptions = 47019,

        [EnableEditingOnDefaultTab]
        [StringValue("Alterations Add Another")]
        [DisplayOrder(1)]
        AlterationsAddAnother = 47020,

        [EnableEditingOnDefaultTab]
        [StringValue("Alterations Chosen Options Label")]
        [DisplayOrder(1)]
        AlterationsChosenOptionsLabel = 47021,

        [EnableEditingOnDefaultTab]
        [StringValue("Popup Remove Product Title")]
        [DisplayOrder(1)]
        ProductViewPopupRemoveProductTitle = 47023,

        [EnableEditingOnDefaultTab]
        [StringValue("Popup Remove Product Message")]
        [DisplayOrder(1)]
        ProductViewPopupRemoveProductMessage = 47024,

        [EnableEditingOnDefaultTab]
        [StringValue("Popup Remove Product Cancel")]
        [DisplayOrder(1)]
        ProductViewPopupRemoveProductCancel = 47025,

        [EnableEditingOnDefaultTab]
        [StringValue("Popup Remove Product Cta")]
        [DisplayOrder(1)]
        ProductViewPopupRemoveProductCta = 47026,

        [EnableEditingOnDefaultTab]
        [StringValue("Alterations Add Options")]
        [DisplayOrder(1)]
        AlterationOptionsAdd = 47027,
        
        [EnableEditingOnDefaultTab]
        [StringValue("Options")]
        [DisplayOrder(1)]
        RadioButtonsOptions = 47028,

        [EnableEditingOnDefaultTab]
        [StringValue("PWA Add To Home Screen Button Text")]
        [DisplayOrder(1)]
        PwaAddToHomeScreenCta = 47029,

        [EnableEditingOnDefaultTab]
        [StringValue("PWA Add  To Home Screen Message")]
        [DisplayOrder(1)]
        PwaAddToHomeScreenText = 47030,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error request invalid")]
        ValidationErrorRequestInvalid = 47031,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error outlet invalid")]
        ValidationErrorOutletInvalid = 47032,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error outlet operational state invalid")]
        ValidationErrorOutletOperationalStateInvalid = 47033,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error outlet closed due businesshours")]
        [DisplayOrder(101)]
        ValidationErrorOutletClosedDueBusinessHours = 47034,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error service method invalid")]
        ValidationErrorServiceMethodInvalid = 47035,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error service method not active")]
        [DisplayOrder(200)]
        ValidationErrorServiceMethodNotActive = 47036,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error charge required")]
        ValidationErrorServiceChargeRequired = 47037,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error service charge not required")]
        ValidationErrorServiceChargeNotRequired = 47038,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error service charge invalid")]
        ValidationErrorServiceChargeInvalid = 47039,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error checkout method invalid")]
        ValidationErrorCheckoutMethodInvalid = 47040,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error checkout method not active")]
        [DisplayOrder(301)]
        ValidationErrorCheckoutMethodNotActive = 47041,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error payment data invalid")]
        [DisplayOrder(400)]
        ValidationErrorPaymentDataInvalid = 47042,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error payment data unsupported paymentMethod")]
        [DisplayOrder(401)]
        ValidationErrorPaymentDataUnsupportedPaymentMethod = 47043,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error customer last name invalid")]
        ValidationErrorCustomerLastNameInvalid = 47044,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error customer email invalid")]
        ValidationErrorCustomerEmailInvalid = 47045,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error email required")]
        ValidationErrorCustomerEmailRequired = 47046,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error customer phone invalid")]
        ValidationErrorCustomerPhoneInvalid = 47047,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error customer phone required")]
        ValidationErrorCustomerPhoneRequired = 47048,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error error tip required")]
        ValidationErrorTipRequired = 47049,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error tip not required")]
        ValidationErrorTipNotRequired = 47050,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error tip invalid minimum")]
        ValidationErrorTipInvalidMinimum = 47051,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error service method delivery point required")]
        ValidationErrorServiceMethodDeliveryPointRequired = 47052,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error service method delivery point not required")]
        ValidationErrorServiceMethodDeliveryPointNotRequired = 47053,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error service method delivery point invalid")]
        [DisplayOrder(201)]
        ValidationErrorServiceMethodDeliveryPointInvalid = 47054,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error checkout method delivery point required")]
        ValidationErrorCheckoutMethodDeliveryPointRequired = 47055,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error checkout method delivery point not required")]
        ValidationErrorCheckoutMethodDeliveryPointNotRequired = 47056,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error checkout method delivery point invalid")]
        [DisplayOrder(300)]
        ValidationErrorCheckoutMethodDeliveryPointInvalid = 47057,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error product invalid")]
        ValidationErrorProductInvalid = 47058,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error product price invalid")]
        [DisplayOrder(103)]
        ValidationErrorProductPriceInvalid = 47059,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error product unavailable")]
        [DisplayOrder(102)]
        ValidationErrorProductUnavailable = 47060,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error alteration invalid")]
        ValidationErrorAlterationInvalid = 47061,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error price invalid")]
        ValidationErrorAlterationPriceInvalid = 47062,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error alteration time invalid")]
        ValidationErrorAlterationTimeInvalid = 47063,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error order intake disabled")]
        [DisplayOrder(100)]
        ValidationErrorOrderIntakeDisabled = 47064,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error delivery location invalid")]
        ValidationErrorDeliveryLocationInvalid = 47065,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error delivery street address missing")]
        ValidationErrorDeliveryStreetAddressMissing = 47066,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error delivery zip code missing")]
        ValidationErrorDeliveryZipCodeMissing = 47067,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error delivery city missing")]
        ValidationErrorDeliveryCityMissing = 47068,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error delivery address invalid")]
        ValidationErrorDeliveryAddressInvalid = 47069,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error delivery geo location invalid")]
        ValidationErrorDeliveryGeoLocationInvalid = 47070,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error delivery information missing")]
        ValidationErrorDeliveryInformationMissing = 47071,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error delivery information not required")]
        ValidationErrorDeliveryInformationNotRequired = 47072,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error delivery rate invalid")]
        ValidationErrorDeliveryRateInvalid = 47073,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error location outside delivery area")]
        [DisplayOrder(500)]
        ValidationErrorDeliveryLocationOutsideDeliveryArea = 47074,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error no delivery distance specified for service method")]
        ValidationErrorNoDeliveryDistanceSpecifiedForServiceMethod = 47075,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error unable to retrieve distance between outlet and delivery location")]
        [DisplayOrder(501)]
        ValidationErrorUnableToRetrieveDistanceBetweenOutletAndDeliveryLocation = 47076,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error company invalid")]
        ValidationErrorCompanyInvalid = 47077,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error category invalid")]
        ValidationErrorCategoryInvalid = 47078,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error checkout method receipt template missing")]
        ValidationErrorCheckoutMethodReceiptTemplateMissing = 47079,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error invalid order state")]
        ValidationErrorInvalidOrderState = 47080,

        [EnableEditingOnDefaultTab]
        [StringValue("Validation error payment state invalid")]
        ValidationErrorPaymentStateInvalid = 47081,

        [EnableEditingOnDefaultTab]
        [StringValue("Tax breakdown rate")]
        TaxBreakdownRate = 47082,

        [EnableEditingOnDefaultTab]
        [StringValue("Tax breakdown netto")]
        TaxBreakdownNetto = 47083,

        [EnableEditingOnDefaultTab]
        [StringValue("Tax breakdown VAT")]
        TaxBreakdownVAT = 47084,

        #endregion

        #region AppLess - (landing)page (48000 to 49000)

        [StringValue("Page name")]
        ApplessPageName = 48000,

        #endregion
    }
}
