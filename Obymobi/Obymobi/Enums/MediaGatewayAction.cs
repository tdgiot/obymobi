using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum MediaGatewayAction
    {
        Ping,
        FileExists,
        FileRequiresUpload,
        ReadFile,
        WriteFile,
        DeleteFile,
    }
}
