﻿namespace Obymobi.Enums
{
    public enum MessagingServiceConnectionType
    {
        Unknown = -1,
        IoT = 1,
        Polling = 2
    }
}
