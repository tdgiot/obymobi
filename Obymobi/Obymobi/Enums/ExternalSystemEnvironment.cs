﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum ExternalSystemEnvironment
    {
        [StringValue("Live")]
        Live = 1,

        [StringValue("Test")]
        Test = 2
    }
}
