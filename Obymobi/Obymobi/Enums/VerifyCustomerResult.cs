﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum VerifyCustomerResult : int
    {
        Unknown = 0,

        Success = 100,

        CustomerIdUnknown = 200,

        NoConfirmationForAnonymousAccounts = 401,

        Blacklisted = 402,

        EmailAddressIncorrectFormat = 403,

        NewEmailAddressAlreadyExists = 404,

        EmailAddressAlreadyVerified = 405,

        Failure = 998,

        InvalidHash = 999
    }
}
