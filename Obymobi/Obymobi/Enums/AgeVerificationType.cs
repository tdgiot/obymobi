using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum AgeVerificationType : int
	{
        /// <summary>
        /// Not verified
        /// </summary>
        NotVerified = 0,

        /// <summary>
        /// No product on the Order required Age Verification
        /// </summary>
        NotRequired = 100,

        /// <summary>
        /// The birthdate from the Customer entity was used
        /// </summary>
        BirthdateOfCustomer = 200,

        /// <summary>
        /// A tickbox was shown to indicate the right age
        /// </summary>
        Tickbox = 300
	}
}
