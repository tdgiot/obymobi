namespace Obymobi.Enums
{
	public enum PublishingStatus : int
	{
        InProgress = 100,
        Success = 200,
        SuccessAfterRetry = 300,
        NothingToPublish = 400,
        Failure = 500,
	}
}
