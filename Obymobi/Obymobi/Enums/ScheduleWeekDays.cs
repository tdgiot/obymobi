using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    [Flags]
    public enum ScheduleWeekDays : int
    {
        Sunday = 1,
        Monday = 2,
        Tuesday = 4,
        Wednesday = 8,
        Thursday = 16,
        Friday = 32,
        WorkDays = 62,
        Saturday = 64,
        WeekendDays = 65,
        EveryDay = 127
    }
}
