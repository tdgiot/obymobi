﻿using System;
using Dionysos;

namespace Obymobi.Enums
{
    [Flags]
    public enum OrderNotificationMethod
    {
        [StringValue("Disabled")]
        None = 0,

        [StringValue("Email")]
        Email = 1,

        [StringValue("SMS")]
        SMS = 2,

        [StringValue("Email & SMS")]
        EmailSMS = Email | SMS
    }
}