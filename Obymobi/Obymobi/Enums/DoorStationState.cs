﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum DoorStationState : int
    {
        /// <summary>
        /// Housekeeping
        /// </summary>
        [StringValue("Housekeeping")]
        Housekeeping = 0,

        /// <summary>
        /// Bellman
        /// </summary>
        [StringValue("Bellman")]
        Bellman = 1,

        /// <summary>
        /// Linens
        /// </summary>
        [StringValue("Linens")]
        Linens = 2,

        /// <summary>
        /// Valet
        /// </summary>
        [StringValue("Valet")]
        Valet = 3,

        /// <summary>
        /// Turn down
        /// </summary>
        [StringValue("Turn down")]
        Turndown = 4,

        /// <summary>
        /// Roll away
        /// </summary>
        [StringValue("Roll away")]
        Rollaway = 5,

        /// <summary>
        /// Do not disturb
        /// </summary>
        [StringValue("Do Not Disturb")]
        DoNotDisturb = 6,

        /// <summary>
        /// Service room
        /// </summary>
        [StringValue("Service Room")]
        ServiceRoom = 7
    }
}
