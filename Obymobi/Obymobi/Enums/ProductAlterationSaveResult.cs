﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum ProductAlterationSaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("The alteration isn't configured correctly to be used by the POS and therefore can not be added.")]
        AlterationNotReadyForPos = 200,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("It's not possible to add Alterations to Products without a link to a PosproductPosalteration.")]
        CantAddAlterationsToProductsWithoutPosproductPosalteration = 201,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("It's not possible to remove an Alteration from a Product when the alteration is mandatory in the POS.")]
        CantRemoveAlterationFromProductWhenRequiredInPos = 203,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("It's not possible to change an Alteration from a Product when the alteration is mandatory in the POS.")]
        CantChangeAlterationFromProductWhenRequiredInPos = 204,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("It's not possible to use an Alteration more the once.")]
        CantUseAlterationTwice = 205,

        [StringValue("It's not possible to use a package type alteration when a company is configured for alterationg dialog mode V1.")]
        PackageTypeAlterationNotAllowedWithV1AlterationDialogMode = 206,

        [StringValue("Alteration has this product configured thus it can not be used with this product")]
        ProductAlterationCircularDependencyDetected = 207
    }
}
