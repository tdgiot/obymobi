﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the ProductCategorySaveResult
    /// </summary>
    public enum ProductCategorySaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Failed
        /// </summary>
        [StringValue("Producten kunnen niet worden toegevoegd aan categorieën waarin zich sub-categorieën bevinden.")]
        ProductIdCategoryIdCombinationNotUnique = 201,


        /// <summary>
        /// Failed
        /// </summary>
        [StringValue("De combinatie van velden 'ProductId' en 'CategoryId' moet uniek zijn..")]
        CategoryContainsSubCategories = 202,

        /// <summary>
        /// Failed
        /// </summary>
        [StringValue("Product - Category link invalid, different companies")]
        ProductCategoryLinkOfDifferentCompanies = 203,

        /// <summary>
        /// Failed
        /// </summary>
        [StringValue("The Product of the Product - Category link would become part of multiple routes by saving this entiy")]
        MultipleRoutesForProduct = 204
    }
}