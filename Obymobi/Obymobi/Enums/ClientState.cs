using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum ClientState : int
    {
        /// <summary>
        /// Online 
        /// </summary>
        [StringValue("Online.")]
        Online = 100,

        /// <summary>
        /// Offline
        /// </summary>
        [StringValue("Offline.")]
        Offline = 200,

        /// <summary>
        /// Unknown Error
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 301,

        /// <summary>
        /// Unknown client id
        /// </summary>
        [StringValue("De opgegeven client id is onbekend.")]
        ClientIdIncorrect = 302,
    }
}
