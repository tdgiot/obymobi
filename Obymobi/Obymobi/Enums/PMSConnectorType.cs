﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum PMSConnectorType : int
    {
        [StringValue("None / Unknown")]
        Unknown = 0,
        [StringValue("Comtrol")]
        Comtrol = 1,
        [StringValue("Tigertms")]
        Tigertms = 2,
        [StringValue("Innsist")]
        Innsist = 3,
    }
}
