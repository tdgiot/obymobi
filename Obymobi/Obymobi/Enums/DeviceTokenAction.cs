using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum DeviceTokenAction : int
	{
        /// <summary>
        /// Renew
        /// </summary>
        Renew = 0,

        /// <summary>
        /// Revoke
        /// </summary>
        Revoke = 1,
	}
}
