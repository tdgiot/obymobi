using Dionysos;
namespace Obymobi.Enums
{
    public enum MessageLayoutType
    {
        /// <summary>
        /// AutoSize
        /// </summary>
        [StringValue("Auto size")]
        AutoSize = 0,

        /// <summary>
        /// Small
        /// </summary>
        [StringValue("Small")]
        Small = 1,

        /// <summary>
        /// Medium
        /// </summary>
        [StringValue("Medium")]
        Medium = 2,

        /// <summary>
        /// Large
        /// </summary>
        [StringValue("Large")]
        Large = 3,
    }
}
