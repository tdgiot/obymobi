using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	/// <summary>
    /// Enumeration which represents the AlterationType
	/// </summary>
	public enum AlterationType : int
	{
        [StringValue("Options")]
		Options = 1,

        [StringValue("Date/time")]
		DateTime = 2,

        [StringValue("Date")]
        Date = 3,

        [StringValue("Email")]
        Email = 4,

        [StringValue("Text")]
        Text = 5,

        [StringValue("Numeric")]
        Numeric = 6,

        [StringValue("Package")]
        Package = 7,

        [StringValue("Category")]
        Category = 8,

        [StringValue("Summary")]
        Summary = 9,

        [StringValue("Page")]
        Page = 10,

        [StringValue("Notes")]
        Notes = 11,

        [StringValue("Image")]
        Image = 12,

        [StringValue("Instruction")]
        Instruction = 13,

        [StringValue("Form")]
        Form = 14
	}
}
