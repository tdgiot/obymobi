﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum PaymentProviderEnvironment
    {
        [StringValue("Live")]
        Live = 1,
        [StringValue("Test")]
        Test = 2,
    }
}