using Dionysos;
namespace Obymobi.Enums
{
    public enum BrowserAgeVerificationLayoutType
    {
        [StringValue("Big image top, description, button")]
        BigImageTopDescriptionButton = 0,

        [StringValue("Small image top, description, button")]
        SmallImageTopDescriptionButton = 1,

        [StringValue("Big Image top, button, description")]
        BigImageTopButtonDescription = 2,

        [StringValue("Small Image top, button, description")]
        SmallImageTopButtonDescription = 3,

        [StringValue("Big image left, description, button")]
        BigImageLeftDescriptionButton = 4,

        [StringValue("Big image right, description, button")]
        BigImageRightDescriptionButton = 5
    }
}
