using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum MapType : int
	{
        Normal = 1,
        Satellite = 2,
        Terrain = 3,
        Hybrid = 4
    }
}
