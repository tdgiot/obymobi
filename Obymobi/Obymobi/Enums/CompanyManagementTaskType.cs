﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum CompanyManagementTaskType
    {
        CopyCompany,
        ExportCompany,
        ImportCompany,
        CopyPointOfInterest,
        ExportPointOfInterest,
        ImportPointOfInterest
    }
}
