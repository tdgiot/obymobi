using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum GenericResult : int
	{
		/// <summary>
		/// Unknown
		/// </summary>
		[StringValue("Success.")]
		Success = 100,

		/// <summary>
		/// Generic Entity Save Exception
		/// </summary>
        [StringValue("Er is een EntitySaveException opgetreden.")]
		EntitySaveException = 200,

		/// <summary>
		/// Generic Entity False
		/// </summary>
        [StringValue("Entity.Save() returnde false")]
		EntitySaveFalse = 201,

		/// <summary>
		/// Generic Entity Delete Exception
		/// </summary>
        [StringValue("Er is een EntityDeleteException opgetreden.")]
		EntityDeleteException = 202,

		/// <summary>
		/// Unknown
		/// </summary>
		[StringValue("Failure.")]
		Failure = 203,

        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Invalid Hash.")]
        InvalidHash = 204,
	}
}
