using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the result of the authentication process
    /// </summary>
    public enum BusinesshoursSaveResult : int
    {
        /// <summary>
        /// State unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Unknown error
        ///// </summary>
        //UnkownError = 200,

        /// <summary>
        /// Incorrect format
        /// </summary>
        [StringValue("Datum en tijd zijn ongeldig.")]
        DayOfWeekAndTimeInvalid = 201,

        /// <summary>
        /// Close after close
        /// </summary>
        [StringValue("Zelfde type twee maal in zelfde rij.")]
        SameTypeTwiceInARow = 202,	

        /// <summary>
        /// CompanyId 0
        /// </summary>
        [StringValue("Company id is 0.")]
        CompanyIdZero = 206,

        /// <summary>
        /// First must be opening
        /// </summary>
        [StringValue("De eerste moet opening zijn.")]
        FirstMustBeOpening = 207,

    }
}
