﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum SurveyQuestionType : int
    {
        /// <summary>
        /// Descriptive Text
        /// </summary>
        DescriptiveText = 100,

        /// <summary>
        /// Single Textbox
        /// </summary>
        SingleTextbox = 200,

        /// <summary>
        /// Multiple Choice
        /// </summary>
        MultipleChoice = 300,

        /// <summary>
        /// Form
        /// </summary>
        Form = 400,

        /// <summary>
        /// Matrix
        /// </summary>
        Matrix = 500,

        /// <summary>
        /// Ranking
        /// </summary>
        Ranking = 600,

        /// <summary>
        /// Rating
        /// </summary>
        Rating = 700,

        /// <summary>
        /// Yes / No
        /// </summary>
        YesNo = 800
    }
}
