﻿namespace Obymobi.Enums
{
    /// <summary>
    /// Service charge type
    /// </summary>
    public enum ChargeType
    {
        /// <summary>
        /// Defines that the service charge type isn't used.
        /// </summary>
        None = 0,

        /// <summary>
        /// Applies a fixed price for the service charge.
        /// </summary>
        Fixed = 1,

        /// <summary>
        /// Applies a percentage based price for the service charge.
        /// </summary>
        Percentage = 2
    }
}
