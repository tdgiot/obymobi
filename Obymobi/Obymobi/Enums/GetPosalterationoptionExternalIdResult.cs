using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum GetPosalterationoptionExternalIdResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// No pos alteration option found for the specified id
        /// </summary>
        [StringValue("Er zijn geen pos alteration options gevonden voor het opgegeven alteration option id.")]
        NoPosalterationoptionFound = 200,

        /// <summary>
        /// Multiple pos alteration options found
        /// </summary>
        [StringValue("Er zijn meerdere pos alteration options gevonden voor het opgegeven alteration option id.")]
        MultiplePosalterationoptionsFound = 201
    }
}
