﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum TerminalOperationState : int
    {
        /// <summary>
        /// Unknown state
        /// </summary>
        [StringValue("Unknown")]
        Unknown = 0,

        /// <summary>
        /// Terminal used in normal operation mode
        /// </summary>
        [StringValue("Normal")]
        Normal = 1,

        /// <summary>
        /// Synchronizing data on terminal
        /// </summary>
        [StringValue("Synchronizing data")]
        SynchronizingData = 2,

        /// <summary>
        /// Synchronizing pos data on terminal
        /// </summary>
        [StringValue("Synchronizing pos-data")]
        SynchronizingPosData = 3,

        /// <summary>
        /// Restarting the terminal
        /// </summary>
        [StringValue("Restarting")]
        Restarting = 4,

        /// <summary>
        /// Checking menu consistency on terminal
        /// </summary>
        [StringValue("Checking menu consistency")]
        CheckingMenuConsistency = 5
    }
}
