﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum EntertainmentCloseInterval
    {
        [StringValue("Default - Never")]
        Never = 0,
        
        [StringValue("After 10 Minutes")]
        After10Minutes = 10,
        [StringValue("After 30 Minutes")]
        After30Minutes = 30,
        [StringValue("After 60 Minutes")]
        After60Minutes = 60,
        [StringValue("After 2 Hours")]
        After2Hours = 120,
        [StringValue("After 3 Hours")]
        After3Hours = 180,
    }
}
