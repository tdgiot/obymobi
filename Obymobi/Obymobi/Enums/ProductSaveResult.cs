﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the ProductSaveResult
    /// </summary>
    public enum ProductSaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Failed
        ///// </summary>
        //Failed = 200,

        /// <summary>
        /// NameEmpty
        /// </summary>
        [StringValue("Er is geen naam opgegeven.")]
        NameEmpty = 201,

        /// <summary>
        /// NameEmpty
        /// </summary>
        [StringValue("The product was linked to a Posproduct of another company.")]
        LinkedToPosproductOfOtherCompany = 202,

        /// <summary>
        /// NameEmpty
        /// </summary>
        [StringValue("The Save() result was FALSE.")]
        GenericSaveFalseResult = 203,

        /// <summary>
        /// NameEmpty
        /// </summary>
        [StringValue("Can't delete products that are used by Alteration Options.")]
        CantDeleteProductsThatAreUsedByAlterationOptions = 204,

        /// <summary>
        /// NameEmpty
        /// </summary>
        [StringValue("A product already exists with the specified posproduct id.")]
        ProductAlreadyExistsForThisPosproduct = 205,

        /// <summary>
        /// NameInvalidCharacters
        /// </summary>
        [StringValue("The product name contains invalid characters.")]
        NameInvalidCharacters = 206,

        /// <summary>
        /// NameInvalidCharacters
        /// </summary>
        [StringValue("The product name contains invalid characters.")]
        TriedToSaveMultipleProductsForOnePosProductId = 207,

        [StringValue("Product SubType Web Link Requires Smartphone And Table Url To BeFilled.")]
        ProductSubTypeWebLinkRequiresSmartphoneAndTableUrlToBeFilled = 208,

        [StringValue("Web Link Url Is Not Valid.")]
        WebLinkUrlIsNotValid = 209,

        [StringValue("Something went wrong trying to update a product with a brand product.")]
        UpdateProductWithBrandProductFailed = 210,

        [StringValue("Something went wrong trying to remove a brand product from a product.")]
        RemoveBrandProductFromProductFailed = 211,
    }
}