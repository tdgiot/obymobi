﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum VerifyTerminalResult : int
    {
        /// <summary>
        /// Terminal was successfully verified
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Unknown terminal id
        /// </summary>
        [StringValue("Onbekende terminal id.")]
        TerminalIdUnknown = 200,

        /// <summary>
        /// Multiple terminals found
        /// </summary>
        [StringValue("Meerdere terminals gevonden.")]
        MultipleTerminalsFound = 201,

        /// <summary>
        /// Invalid company id
        /// </summary>
        [StringValue("Ongeldige company id.")]
        InvalidCompanyId = 202,

        /// <summary>
        /// Invalid username
        /// </summary>
        [StringValue("Ongeldige username.")]
        InvalidUsername = 203,

        /// <summary>
        /// Invalid password
        /// </summary>
        [StringValue("Ongeldig password.")]
        InvalidPassword = 204
    }
}
