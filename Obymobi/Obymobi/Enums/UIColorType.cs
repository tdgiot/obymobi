using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.Enums
{
	/// <summary>
    /// Enumeration which represents the type of the UI color
	/// </summary>
	public enum UIColorType : int
    {
        [StringValue("List Category Background (Level 1)")]
        ListCategoryBackgroundColor = 1,

        [StringValue("List Category Background (Level 2)")]
        ListCategoryL2BackgroundColor = 2,

        [StringValue("List Category Background (Level 3)")]
        ListCategoryL3BackgroundColor = 3,

        [StringValue("List Category Background (Level 4)")]
        ListCategoryL4BackgroundColor = 4,

        [StringValue("List Category Background (Level 5)")]
        ListCategoryL5BackgroundColor = 5,

        [StringValue("List Category Divider")]
        ListCategoryDividerColor = 6,

        [StringValue("List Category Text")]
        ListCategoryTextColor = 7,

        [StringValue("List Category Selected Background")]
        ListCategorySelectedBackgroundColor = 8,

        [StringValue("List Category Selected Divider")]
        ListCategorySelectedDividerColor = 9,

        [StringValue("List Category Selected Text")]
        ListCategorySelectedTextColor = 10,

        [StringValue("List Item Background")]
        ListItemBackgroundColor = 11,

        [StringValue("List Item Divider")]
        ListItemDividerColor = 12,

        [StringValue("List Item Text")]
        ListItemTextColor = 13,

        [StringValue("List Item Selected")]
        ListItemSelectedBackgroundColor = 14,

        [StringValue("List Item Selected Divider")]
        ListItemSelectedDividerColor = 15,

        [StringValue("List Item Selected Text")]
        ListItemSelectedTextColor = 16,

        [StringValue("List Item Price Text")]
        ListItemPriceTextColor = 17,

        [StringValue("List Item Selected Price Text")]
        ListItemSelectedPriceTextColor = 18,

        [StringValue("Widget Background")]
        WidgetBackgroundColor = 19,

        [StringValue("Widget Border")]
        WidgetBorderColor = 20,

        [StringValue("Widget Text")]
        WidgetTextColor = 21,

        [StringValue("Header")]
        HeaderColor = 22,

        [StringValue("Tab Background Bottom")]
        TabBackgroundBottomColor = 23,

        [StringValue("Tab Background Top")]
        TabBackgroundTopColor = 24,

        [StringValue("Tab Divider")]
        TabDividerColor = 25,

        [StringValue("Tab Border")]
        TabBorderColor = 26,

        [StringValue("Tab Active Text")]
        TabActiveTextColor = 27,

        [StringValue("Tab Inactive Text")]
        TabInactiveTextColor = 28,

        [StringValue("Page Background")]
        PageBackgroundColor = 29,

        [StringValue("Page Border")]
        PageBorderColor = 30,

        [StringValue("Page Title Text")]
        PageTitleTextColor = 31,

        [StringValue("Page Description Text")]
        PageDescriptionTextColor = 32,

        [StringValue("Page Error Text")]
        PageErrorTextColor = 33,

        [StringValue("Page Price Text")]
        PagePriceTextColor = 34,

        [StringValue("Page Instructions Text")]
        PageInstructionsTextColor = 35,

        [StringValue("Page Divider Top")]
        PageDividerTopColor = 36,

        [StringValue("Page Divider Bottom")]
        PageDividerBottomColor = 37,

        [StringValue("Page Footer")]
        PageFooterColor = 38,

        [StringValue("Footer Background Bottom")]
        FooterBackgroundBottomColor = 39,

        [StringValue("Footer Background Top")]
        FooterBackgroundTopColor = 40,

        [StringValue("Footer Divider")]
        FooterDividerColor = 41,

        [StringValue("Footer Text")]
        FooterTextColor = 42,

        [StringValue("Footer Button Background Top")]
        FooterButtonBackgroundTopColor = 43,

        [StringValue("Footer Button Background Bottom")]
        FooterButtonBackgroundBottomColor = 44,

        [StringValue("Footer Button Border Color")]
        FooterButtonBorderColor = 45,

        [StringValue("Footer Button Text Color")]
        FooterButtonTextColor = 46,

        [StringValue("Footer Button Disabled Background Top")]
        FooterButtonDisabledBackgroundTopColor = 47,

        [StringValue("Footer Button Disabled Background Bottom")]
        FooterButtonDisabledBackgroundBottomColor = 48,

        [StringValue("Footer Button Disabled Border")]
        FooterButtonDisabledBorderColor = 49,

        [StringValue("Footer Button Disabled Text")]
        FooterButtonDisabledTextColor = 50,

        [StringValue("Button Background Top")]
        ButtonBackgroundTopColor = 51,

        [StringValue("Button Background Bottom")]
        ButtonBackgroundBottomColor = 52,

        [StringValue("Button Border Color")]
        ButtonBorderColor = 53,

        [StringValue("Button Text")]
        ButtonTextColor = 54,

        [StringValue("Button Positive Background Top")]
        ButtonPositiveBackgroundTopColor = 55,

        [StringValue("Button Positive Background Bottom")]
        ButtonPositiveBackgroundBottomColor = 56,

        [StringValue("Button Positive Border")]
        ButtonPositiveBorderColor = 57,

        [StringValue("Button Positive Text")]
        ButtonPositiveTextColor = 58,

        [StringValue("Button Disabled Background Top")]
        ButtonDisabledBackgroundTopColor = 59,

        [StringValue("Button Disabled Background Bottom")]
        ButtonDisabledBackgroundBottomColor = 60,

        [StringValue("Button Disabled Border")]
        ButtonDisabledBorderColor = 61,

        [StringValue("Button Disabled Text")]
        ButtonDisabledTextColor = 62,

        [StringValue("Dialog Background")]
        DialogBackgroundColor = 63,

        [StringValue("Dialog Border")]
        DialogBorderColor = 64,

        [StringValue("Dialog Panel Background")]
        DialogPanelBackgroundColor = 65,

        [StringValue("Dialog Panel Border")]
        DialogPanelBorderColor = 66,

        [StringValue("Dialog Title Text")]
        DialogTitleTextColor = 67,

        [StringValue("Dialog Primary Text")]
        DialogPrimaryTextColor = 68,

        [StringValue("Dialog Secondary Text")]
        DialogSecondaryTextColor = 69,

        [StringValue("Textbox Background")]
        TextboxBackgroundColor = 70,

        [StringValue("Textbox Border")]
        TextboxBorderColor = 71,

        [StringValue("Textbox Text")]
        TextboxTextColor = 72,

        [StringValue("Textbox Cursor")]
        TextboxCursorColor = 73,

        [StringValue("Spinner Background")]
        SpinnerBackgroundColor = 74,

        [StringValue("Spinner Border")]
        SpinnerBorderColor = 75,

        [StringValue("Spinner Text")]
        SpinnerTextColor = 76,

        [StringValue("Room Control Button Outer Border")]
        RoomControlButtonOuterBorderColor = 77,

        [StringValue("Room Control Button Inner Border Top")]
        RoomControlButtonInnerBorderTopColor = 78,

        [StringValue("Room Control Button Inner Border Bottom")]
        RoomControlButtonInnerBorderBottomColor = 79,

        [StringValue("Room Control Button Background Top")]
        RoomControlButtonBackgroundTopColor = 80,

        [StringValue("Room Control Button Background Bottom")]
        RoomControlButtonBackgroundBottomColor = 81,

        [StringValue("Room Control Divider Top")]
        RoomControlDividerTopColor = 82,

        [StringValue("Room Control Divider Bottom")]
        RoomControlDividerBottomColor = 83,

        [StringValue("Room Control Button Text")]
        RoomControlButtonTextColor = 84,

        [StringValue("Room Control Title")]
        RoomControlTitleColor = 85,

        [StringValue("Room Control Text")]
        RoomControlTextColor = 86,

        [StringValue("Room Control Page Background")]
        RoomControlPageBackgroundColor = 87,

        [StringValue("Room Control Spinner Border")]
        RoomControlSpinnerBorderColor = 88,

        [StringValue("Room Control Spinner Background")]
        RoomControlSpinnerBackgroundColor = 89,

        [StringValue("Room Control Spinner Text")]
        RoomControlSpinnerTextColor = 90,

        [StringValue("Room Control Button Indicator")]
        RoomControlButtonIndicatorColor = 91,

        [StringValue("Room Control Button Indicator Background")]
        RoomControlButtonIndicatorBackgroundColor = 92,

        [StringValue("Room Control Header Button Background")]
        RoomControlHeaderButtonBackgroundColor = 93,

        [StringValue("Room Control Header Button Selector")]
        RoomControlHeaderButtonSelectorColor = 94,

        [StringValue("Room Control Header Button Text")]
        RoomControlHeaderButtonTextColor = 95,

        [StringValue("Room Control Station Number")]
        RoomControlStationNumberColor = 96,

        [StringValue("Room Control Slider Min")]
        RoomControlSliderMinColor = 97,

        [StringValue("Room Control Slider Max")]
        RoomControlSliderMaxColor = 98,

        [StringValue("Room Control Toggle On Text")]
        RoomControlToggleOnTextColor = 99,

        [StringValue("Room Control Toggle Off Text")]
        RoomControlToggleOffTextColor = 100,

        [StringValue("Room Control Toggle Background")]
        RoomControlToggleBackgroundColor = 101,

        [StringValue("Room Control Toggle Border")]
        RoomControlToggleBorderColor = 102,

        [StringValue("Room Control Thermostat Component")]
        RoomControlThermostatComponentColor = 103,

        [StringValue("Alteration Dialog Background")]
        AlterationDialogBackgroundColor = 104,

        [StringValue("Alteration Dialog Border")]
        AlterationDialogBorderColor = 105,

        [StringValue("Alteration Dialog Panel Background")]
        AlterationDialogPanelBackgroundColor = 106,

        [StringValue("Alteration Dialog Panel Border")]
        AlterationDialogPanelBorderColor = 107,

        [StringValue("Alteration Dialog List Item Background")]
        AlterationDialogListItemBackgroundColor = 108,

        [StringValue("Alteration Dialog List Item Selected Background")]
        AlterationDialogListItemSelectedBackgroundColor = 109,

        [StringValue("Alteration Dialog List Item Selected Border")]
        AlterationDialogListItemSelectedBorderColor = 110,

        [StringValue("Alteration Dialog Input Background")]
        AlterationDialogInputBackgroundColor = 111,

        [StringValue("Alteration Dialog Input Border")]
        AlterationDialogInputBorderColor = 112,

        [StringValue("Alteration Dialog Input Text")]
        AlterationDialogInputTextColor = 113,

        [StringValue("Alteration Dialog Input Hint Text")]
        AlterationDialogInputHintTextColor = 114,

        [StringValue("Alteration Dialog Primary Text")]
        AlterationDialogPrimaryTextColor = 115,

        [StringValue("Alteration Dialog Secondary Text")]
        AlterationDialogSecondaryTextColor = 116,

        [StringValue("Alteration Dialog Radiobutton")]
        AlterationDialogRadioButtonColor = 117,

        [StringValue("Alteration Dialog Checkbox")]
        AlterationDialogCheckBoxColor = 118,

        [StringValue("Alteration Dialog Close Button")]
        AlterationDialogCloseButtonColor = 119,

        [StringValue("Alteration Dialog Listview Arrow")]
        AlterationDialogListViewArrowColor = 120,

        [StringValue("Alteration Dialig Listview Divider")]
        AlterationDialogListViewDividerColor = 121,

        [StringValue("Area Tab Active")]
        AreaTabActiveText = 122,

        [StringValue("Area Tab Inactive")]
        AreaTabInactiveText = 123,

        [StringValue("Title")]
        BrowserAgeVerificationTitle = 124,

        [StringValue("Text")]
        BrowserAgeVerificationText = 125,

        [StringValue("Background")]
        BrowserAgeVerificationBackground = 126,

        [StringValue("Button Background Top")]
        BrowserAgeVerificationButtonBackgroundTop = 127,

        [StringValue("Button Background Bottom")]
        BrowserAgeVerificationButtonBackgroundBottom = 128,

        [StringValue("Button Border Color")]
        BrowserAgeVerificationButtonBorderColor = 129,

        [StringValue("Button Text")]
        BrowserAgeVerificationButtonText = 130,

        [StringValue("Background")]
        ProductDialogBackgroundColor = 131,

        [StringValue("Header")]
        ProductDialogHeaderColor = 132,

        [StringValue("Header Text")]
        ProductDialogHeaderTextColor = 133,

        [StringValue("Header Close")]
        ProductDialogHeaderCloseColor = 134,

        [StringValue("Column Border")]
        ProductDialogColumnBorderColor = 135,

        [StringValue("Column Background")]
        ProductDialogColumnBackgroundColor = 136,

        [StringValue("Column Header Primary Text")]
        ProductDialogColumnHeaderPrimaryTextColor = 137,

        [StringValue("Column Header Secondary Text")]
        ProductDialogColumnHeaderSecondaryTextColor = 138,

        [StringValue("Option")]
        ProductDialogColumnOptionColor = 139,        

        [StringValue("Option Divider Top")]
        ProductDialogColumnOptionDividerTopColor = 140,

        [StringValue("Option Divider Bottom")]
        ProductDialogColumnOptionDividerBottomColor = 141,

        [StringValue("Option Primary Text Inactive")]
        ProductDialogColumnOptionPrimaryTextInactiveColor = 142,

        [StringValue("Option Primary Text Active")]
        ProductDialogColumnOptionPrimaryTextActiveColor = 143,

        [StringValue("Option Secondary Text Inactive")]
        ProductDialogColumnOptionSecondaryTextInactiveColor = 144,

        [StringValue("Option Secondary Text Active")]
        ProductDialogColumnOptionSecondaryTextActiveColor = 145,

        [StringValue("Option Checkmark")]
        ProductDialogColumnOptionCheckmarkColor = 146,

        [StringValue("Option Minus Button Text")]
        ProductDialogColumnOptionMinusButtonTextColor = 147,

        [StringValue("Option Minus Button Border")]
        ProductDialogColumnOptionMinusButtonBorderColor = 148,

        [StringValue("Option Minus Button Background")]
        ProductDialogColumnOptionMinusButtonBackgroundColor = 149,

        [StringValue("Option Plus Button Text")]
        ProductDialogColumnOptionPlusButtonTextColor = 150,

        [StringValue("Option Plus Button Inactive Border")]
        ProductDialogColumnOptionPlusButtonInactiveBorderColor = 151,

        [StringValue("Option Plus Button Inactive Background")]
        ProductDialogColumnOptionPlusButtonInactiveBackgroundColor = 152,

        [StringValue("Option Plus Button Active Border")]
        ProductDialogColumnOptionPlusButtonActiveBorderColor = 153,

        [StringValue("Option Plus Button Active Background")]
        ProductDialogColumnOptionPlusButtonActiveBackgroundColor = 154,

        [StringValue("Option Quantity Text")]
        ProductDialogColumnOptionQuantityTextColor = 155,

        [StringValue("Option Quantity Border")]
        ProductDialogColumnOptionQuantityBorderColor = 156,

        [StringValue("Option Quantity Background")]
        ProductDialogColumnOptionQuantityBackgroundColor = 157,

        [StringValue("Option Suggestion Text")]
        ProductDialogColumnOptionSuggestionTextColor = 158,

        [StringValue("Option Suggestion Border")]
        ProductDialogColumnOptionSuggestionBorderColor = 159,

        [StringValue("Option Suggestion Background")]
        ProductDialogColumnOptionSuggestionBackgroundColor = 160,

        [StringValue("Selected Option Background")]
        ProductDialogColumnSelectedOptionBackgroundColor = 161,

        [StringValue("Selected Option Primary Text Inactive")]
        ProductDialogColumnSelectedOptionPrimaryTextInactiveColor = 162,

        [StringValue("Selected Option Primary Text Active")]
        ProductDialogColumnSelectedOptionPrimaryTextActiveColor = 163,

        [StringValue("Selected Option Secondary Text Inactive")]
        ProductDialogColumnSelectedOptionSecondaryTextInactiveColor = 164,

        [StringValue("Selected Option Secondary Text Active")]
        ProductDialogColumnSelectedOptionSecondaryTextActiveColor = 165,

        [StringValue("Input Field Caption Text")]
        ProductDialogColumnInputFieldCaptionTextColor = 166,

        [StringValue("Input Field Text")]
        ProductDialogColumnInputFieldTextColor = 167,

        [StringValue("Input Field Border")]
        ProductDialogColumnInputFieldBorderColor = 168,

        [StringValue("Input Field Background")]
        ProductDialogColumnInputFieldBackgroundColor = 169,

        [StringValue("Date Spinner Border")]
        ProductDialogColumnDateSpinnerBorderColor = 170,

        [StringValue("Date Spinner Background")]
        ProductDialogColumnDateSpinnerBackgroundColor = 171,

        [StringValue("Date Panel Border")]
        ProductDialogColumnDatePanelBorderColor = 172,

        [StringValue("Date Panel Background")]
        ProductDialogColumnDatePanelBackgroundColor = 173,

        [StringValue("Instructions Text")]
        ProductDialogColumnInstructionsTextColor = 174,

        [StringValue("Footer")]
        ProductDialogFooterColor = 175,

        [StringValue("Footer Divider")]
        ProductDialogFooterDividerColor = 176,

        [StringValue("Footer Back Button Text")]
        ProductDialogFooterBackButtonTextColor = 177,

        [StringValue("Footer Back Button Border Inactive")]
        ProductDialogFooterBackButtonBorderInactiveColor = 178,

        [StringValue("Footer Back Button Border Active")]
        ProductDialogFooterBackButtonBorderActiveColor = 179,

        [StringValue("Footer Back Button Background Inactive")]
        ProductDialogFooterBackButtonBackgroundInactiveColor = 180,

        [StringValue("Footer Back Button Background Active")]
        ProductDialogFooterBackButtonBackgroundActiveColor = 181,

        [StringValue("Footer Next Button Text")]
        ProductDialogFooterNextButtonTextColor = 182,

        [StringValue("Footer Next Button Border Inactive")]
        ProductDialogFooterNextButtonBorderInactiveColor = 183,

        [StringValue("Footer Next Button Border Active")]
        ProductDialogFooterNextButtonBorderActiveColor = 184,

        [StringValue("Footer Next Button Background Inactive")]
        ProductDialogFooterNextButtonBackgroundInactiveColor = 185,

        [StringValue("Footer Next Button Background Active")]
        ProductDialogFooterNextButtonBackgroundActiveColor = 186,

        [StringValue("Footer Price Text")]
        ProductDialogFooterPriceTextColor = 187,

        [StringValue("Navigation Bar Background")]
        BrowserNavigationBarBackground = 188,

        [StringValue("Navigation Bar Text")]
        BrowserNavigationBarText = 189,

        [StringValue("Basket Tab Text Glow")]
        BasketTabTextGlow = 190,

        [StringValue("Footer Radio Highlight")]
        FooterRadioHighlightColor = 191,

        [StringValue("Widget Secondary Text")]
        WidgetSecondaryTextColor = 192,

        [StringValue("Alteration Dialog List Item Selected Primary Text")]
	    AlterationDialogListItemSelectedPrimaryTextColor = 193,

        [StringValue("Alteration Dialog List Item Selected Secondary Text")]
	    AlterationDialogListItemSelectedSecondaryTextColor = 194,

        [StringValue("Alteration Dialog List Item Selected Radio Button")]
	    AlterationDialogListItemSelectedRadioButtonColor = 195,

        [StringValue("Alteration Dialog List Item Selected Check Box")]
	    AlterationDialogListItemSelectedCheckBoxColor = 196,

        [StringValue("Map Marker Background")]
        MapMarkerBackgroundColor = 197,

        [StringValue("Map Marker Image Background")]
        MapMarkerImageBackgroundColor= 198,

        [StringValue("Map Marker Title Text")]
        MapMarkerTitleTextColor = 199,

        [StringValue("Map Marker Title Background")]
        MapMarkerTitleBackgroundColor = 200,

        [StringValue("Map Marker Location Text Color")]
        MapMarkerLocationTextColor = 201,

        [StringValue("Map Marker Description Text Color")]
        MapMarkerDescriptionTextColor = 202,

        [StringValue("Map Marker Details Text Color")]
        MapMarkerDetailsTextColor = 203,

        [StringValue("Toggle Button Slider Color Left")]
        ToggleButtonSliderMinColor = 204,

        [StringValue("Toggle Button Slider Color Right")]
        ToggleButtonSliderMaxColor = 205,

        [StringValue("Toggle Button On Text Color")]
        ToggleButtonOnTextColor = 206,

        [StringValue("Toggle Button Off Text Color")]
        ToggleButtonOffTextColor = 207,

        [StringValue("Toggle Button Background Color")]
        ToggleButtonBackgroundColor = 208,

        [StringValue("Toggle Button Border Color")]
        ToggleButtonBorderColor = 209,

        [StringValue("Toggle Button Background Top Color")]
        ToggleButtonBackgroundTopColor = 210,

        [StringValue("Toggle Button Background Bottom Color")]
        ToggleButtonBackgroundBottomColor = 211,

        [StringValue("Screensaver Background Color")]
	    ScreenSaverBackgroundColor = 300,

        [StringValue("Screensaver Date Text Color")]
	    ScreenSaverDateTextColor = 301,

        [StringValue("Screensaver Time Text Color")]
	    ScreenSaverTimeTextColor = 302,

        [StringValue("Screensaver Colon Text Color")]
	    ScreenSaverColonTextColor = 303,

        [StringValue("Screensaver AM PM Text Color")]
	    ScreenSaverAmPmTextColor = 304,

        [StringValue("Screensaver Radio Icon Color")]
	    ScreenSaverRadioIconColor = 305,

        [StringValue("Screensaver Radio Text Color")]
	    ScreenSaverRadioTextColor = 306,

        [StringValue("Screensaver Alarm Icon Color")]
	    ScreenSaverAlarmIconColor = 307,

        [StringValue("Screensaver Alarm Text Color")]
	    ScreenSaverAlarmTextColor = 308,

        [StringValue("Screensaver Screen Off Icon Color")]
	    ScreenSaverScreenOffIconColor = 309,

        [StringValue("Screensaver Home Icon Color")]
	    ScreenSaverHomeIconColor = 310,

        [StringValue("QR Code Background Color")]
        QrCodeBackgroundColor = 311,

        [StringValue("QR Code Foreground Color")]
        QrCodeForegroundColor = 312,
    }
}
