using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum GetAlterationoptionIdResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// No pos alteration option found for the specified id
        /// </summary>
        [StringValue("Er zijn geen alteration options gevonden voor de opgegeven external pos alteration option id.")]
        NoAlterationoptionFound = 200,

        /// <summary>
        /// Multiple pos alterations found
        /// </summary>
        [StringValue("Er zijn meerdere alteration options gevonden voor de opgegeven pos alteration option id.")]
        MultipleAlterationoptionsFound = 201
    }
}
