using Dionysos;

namespace Obymobi.Enums
{
    public enum WebLinkType
    {
        [StringValue("Button")]
        Button,

        [StringValue("QR-Code")]
        QrCode,

        [StringValue("Combination")]
        Combination
    }
}
