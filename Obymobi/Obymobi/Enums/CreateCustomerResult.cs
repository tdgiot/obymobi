using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum CreateCustomerResult
    {
        Unspecified = 0,

        Success = 100,

		PasswordIncorrect = 204,

        InvalidHash = 210,

		AwaitingAttempts = 221,

        AuthenticationError = 300,

        InvalidCountryCode = 301,

        MultipleCustomersFoundForGuid = 400,

		IdentifierAlreadyUsedForDevice = 984,

        AnonymousPasswordInvalid = 985,

        AnonymousUserNotFound = 986,

        PasswordMissing = 987,

        EmailAddressIsRequired = 989,
        
        EmailIncorrectFormat = 990,
        
        PhonenumberIncorrectFormat = 991,
        
        EmailAlreadyInUse = 992,
        
        PhonenumberAlreadyInUse = 993,

        InvalidDeviceType = 995,

        InvalidDevicePlatform = 996,
        
        InvalidSocialMediaType = 997,

        Failure = 999
    }
}
