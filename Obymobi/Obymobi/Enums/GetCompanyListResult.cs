using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum GetCompanyListResult
    {
        Unspecified = 0,

        Success = 100,

        InvalidDeviceType = 210,

        InvalidAccessCodeCombination = 220,

        AuthenticationError = 998,

        Failure = 999
    }
}
