using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum GetPospaymentmethodExternalIdResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// No pos paymentmethods founds for the specified number
        /// </summary>
        [StringValue("Er is geen pos betalingswijze gevonden voor de opgegeven id.")]
        NoPospaymentmethodFound = 200,

        /// <summary>
        /// Multiple pos paymentmethods found
        /// </summary>
        [StringValue("Er zijn meerdere pos betalingswijzen gevonden voor de opgegeven id.")]
        MultiplePospaymentmethodsFound = 201
    }
}

