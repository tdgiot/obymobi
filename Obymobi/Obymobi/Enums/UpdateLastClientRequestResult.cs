﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum UpdateLastClientRequestResult : int
    {
        /// <summary>
        /// Update last client result is unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Last request was successfully updated
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Client was not found in the database 
        /// </summary>
        [StringValue("De opgegeven client is niet bekend.")]
        ClientUnknown = 200,

        /// <summary>
        /// Multiple clients found for the specified client id
        /// </summary>
        [StringValue("Er zijn meerdere clients gevonden voor de opgegeven client id.")]
        MultipleClientsFound = 300,

        /// <summary>
        /// Customer was not found in the database
        /// </summary>
        [StringValue("De opgegeven klant is niet bekend.")]
        CustomerUnknown = 400
    }
}
