﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum UIModeType : int
    {
        /// <summary>
        /// In-room Tablet
        /// </summary>
        [StringValue("Venue owned in-room devices")]
        VenueOwnedUserDevices = 1,

        /// <summary>
        /// Console
        /// </summary>
        [StringValue("Venue owned staff devices")]
        VenueOwnedStaffDevices = 2,

        /// <summary>
        /// Smartphones
        /// </summary>
        [StringValue("Guest owned mobile devices")]
        GuestOwnedMobileDevices = 3,

        /// <summary>
        /// Tablets
        /// </summary>
        [StringValue("Guest owned tablet devices")]
        GuestOwnedTabletDevices = 4
    }
}
