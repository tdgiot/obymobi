﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum ClientEntertainmentSaveResult : int
    {
        /// <summary>
        /// Message was successfully saved
        /// </summary>
        [StringValue("ClientEntertainment is succesvol opgeslagen")]
        Success = 100,

		/// <summary>
		/// Xml Serialize Error
		/// </summary>
        [StringValue("Er is een fout opgetreden bij het opslaan van de ClientEntertainment.")]
		XmlSerializeError = 201,

        /// <summary>
        /// ClientEntertainment failed to save
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het opslaan van de ClientEntertainment. (202)")]
        EntitySaveRecursiveFalse = 202,

        /// <summary>
        /// ClientEntertainment failed to save
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het opslaan van de ClientEntertainment. (203)")]
        EntitySaveRecursiveException = 203,
    }
}
