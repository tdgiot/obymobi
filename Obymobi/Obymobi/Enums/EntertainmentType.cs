using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents the types of entertainment items
	/// </summary>
	public enum EntertainmentType
	{
        [StringValue("Web")]
		Web = 1,

        [StringValue("Android")]
		Android = 2,

        // Deprecated, keeping for future reference
        // Socialmedia = 4,

        [StringValue("Survey")]
        Survey = 6,

        [StringValue("Product rating")]
        ProductRating = 7,

        [StringValue("Browser")]
        Browser = 8,

        [StringValue("Restricted Browser")]
        RestrictedBrowser = 9,

        [StringValue("Cms")]
        Cms = 10,

        [StringValue("Bill")]
        Bill = 11,

        [StringValue("Ipad browser")]
        IpadBrowser = 12,

        [StringValue("Custom browser")]
        CustomBrowser = 13,

        [StringValue("Site")]
        Site = 14,
	}
}
