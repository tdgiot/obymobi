﻿namespace Obymobi.Enums
{
    public enum OrderitemType
    {
        Product = 1,
        Tip = 2,
        ServiceCharge = 3,
        DeliveryCharge = 4,
        EatOutToHelpOutDiscount = 999
    }
}
