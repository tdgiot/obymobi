﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum UITabType : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Unknown")]
        Unknown = 0,

        #region Emenu
        // Emenu = 1 t'm 100

        /// <summary>
        /// Home 
        /// </summary>
        [StringValue("Home")]
        Home = 1,

        /// <summary>
        /// Basket
        /// </summary>
        [StringValue("Basket")]
        Basket = 2,

        /// <summary>
        /// More
        /// </summary>
        [StringValue("More")]
        More = 3,

        /// <summary>
        /// Category
        /// </summary>
        [StringValue("Category")]
        Category = 4,

        /// <summary>
        /// Entertainment
        /// </summary>
        [StringValue("Entertainment")]
        Entertainment = 5,

        /// <summary>
        /// Web
        /// </summary>
        [StringValue("Web")]
        Web = 6,

        /// <summary>
        /// Placeholder
        /// </summary>
        [StringValue("Placeholder")]
        Placeholder = 7,

        /// <summary>
        /// Placeholder
        /// </summary>
        [StringValue("Site")]
        Site = 8,

        /// <summary>
        /// Room control
        /// </summary>
        [StringValue("Room control")]
        RoomControl = 9,

        /// <summary>
        /// Used to show the 'Crave Explorer' in our Apps within a PoI/Company
        /// centered around the location of the PoI/Company
        /// </summary>
        [StringValue("Explore map")]
        ExploreMap = 10,

        #endregion

        #region Console
        // Console = 101 t'm 200

        /// <summary>
        /// Orders
        /// </summary>
        [StringValue("Orders")]
        Orders = 101,

        /// <summary>
        /// Message
        /// </summary>
        [StringValue("Message")]
        Message = 102,

        /// <summary>
        /// Management
        /// </summary>
        [StringValue("Management")]
        Management = 103,

        /// <summary>
        /// Socialmedia
        /// </summary>
        [StringValue("Socialmedia")]
        Socialmedia = 104,

        /// <summary>
        /// MasterOrders
        /// </summary>
        [StringValue("Master orders")]
        MasterOrders = 105,

        /// <summary>
        /// TerminalStatus
        /// </summary>
        [StringValue("Terminal status")]
        TerminalStatus = 106,

        /// <summary>
        /// OrderCount
        /// </summary>
        [StringValue("Order count")]
        OrderCount = 107,

        /// <summary>
        /// Cms
        /// </summary>
        [StringValue("Cms")]
        Cms = 108,

        /// <summary>
        /// DeliveryTimes
        /// </summary>
        [StringValue("Delivery times")]
        DeliveryTimes = 109,

        /// <summary>
        /// UI Modes tab
        /// </summary>
        [StringValue("UI modes")]
        UIModes = 110,

        #endregion
    }
}
