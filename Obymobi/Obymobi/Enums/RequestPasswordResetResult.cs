﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum RequestPasswordResetResult
    {
        Unknown = 0,

        Success = 100,

        EmailAddressUnknown = 200,

        NoEmailOrPhonenumberSet = 201,
        
        NoPasswordResetForAnonymousAccounts = 202,

        Blacklisted = 204,

        EmailAddressIncorrectFormat = 205,

        TooManyPasswordResetRequests = 206,

        MultipleCustomersWithEmailAddress = 207,

        Failure = 998,

        InvalidHash = 999
    }
}
