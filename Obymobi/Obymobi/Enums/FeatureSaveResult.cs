using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents the result of saving a feature
	/// </summary>
	public enum FeatureSaveResult : int
	{
		/// <summary>
		/// Authentication result is unknown
		/// </summary>
        [StringValue("Onbekend.")]
		Unknown = 0,

		/// <summary>
		/// Success
		/// </summary>
        [StringValue("Succes.")]
		Success = 100,

		/// <summary>
		/// Name Empty
		/// </summary>
		[StringValue("Code is niet uniek.")]
		NonUniqueCode = 201,
	}
}
