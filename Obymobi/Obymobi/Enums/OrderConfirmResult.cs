﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum OrderConfirmResult : int
	{
		/// <summary>
		/// Order save result is unknown
		/// </summary>
        [StringValue("Onbekend.")]
		Unknown = 0,

		/// <summary>
		/// Order was successfully saved
		/// </summary>
        [StringValue("Succes.")]
		Success = 100,

        /// <summary>
        /// Order failed to save
        /// </summary>
        [StringValue("Er is geen order gevonden om te bevestigen.")]
        NoOrderFoundForConfirmation = 200,

        /// <summary>
        /// Order failed to save
        /// </summary>
        [StringValue("De opgegeven bevestigingscode is onjuist.")]
        IncorrectConfirmationCode = 201,

        /// <summary>
        /// Thrown for orders of a type for which no confirmation logic is implemented
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verifieren van de bevestigingscode.")]
        NoImplementationFoundForConfirmationOfOrdertype = 202,

        /// <summary>
        /// Confirmation still waiting to be printed
        /// </summary>
        [StringValue("De bevestigingscode wordt over enkele ogenblikken afgedrukt.")]
        ConfirmationOrderWaitingToBePrinted = 203
	}
}
