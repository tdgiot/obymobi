﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Enums
{
    public enum AlterationVersion
    {
        Packages = 1,
        OptionsWithinOptions = 2
    }
}
