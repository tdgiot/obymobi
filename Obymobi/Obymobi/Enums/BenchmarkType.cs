using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum BenchmarkType
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown,

        /// <summary>
        /// Category
        /// </summary>
        Category,

        /// <summary>
        /// Product
        /// </summary>
        Product,

        /// <summary>
        /// Advertisement
        /// </summary>
        Advertisement,

        /// <summary>
        /// Entertainment
        /// </summary>
        Entertainment,

        /// <summary>
        /// Order
        /// </summary>
        Order,

        /// <summary>
        /// Orderitem
        /// </summary>
        Orderitem
    }
}
