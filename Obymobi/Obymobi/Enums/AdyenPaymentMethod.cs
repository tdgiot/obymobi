﻿using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.Enums
{
    public enum AdyenPaymentMethod
    {
        [AdyenPaymentMapping("scheme", AdyenPaymentMethodBrand.MasterCard, AdyenPaymentMethodBrand.Visa, AdyenPaymentMethodBrand.AmericanExpress)]
        [StringValue("Credit card")]
        CreditCard = 1,

        [AdyenPaymentMapping("paywithgoogle", AdyenPaymentMethodBrand.MasterCard, AdyenPaymentMethodBrand.Visa, AdyenPaymentMethodBrand.AmericanExpress)]
        [StringValue("Google pay")]
        GooglePay = 2,

        [AdyenPaymentMapping("applepay", AdyenPaymentMethodBrand.MasterCard, AdyenPaymentMethodBrand.Visa, AdyenPaymentMethodBrand.AmericanExpress)]
        [StringValue("Apple Pay")]
        ApplePay = 3,

        [AdyenPaymentMapping("ideal")]
        [StringValue("IDeal")]
        IDeal = 4,

        [AdyenPaymentMapping("paymaya_wallet")]
        [StringValue("PayMaya")]
        PayMaya = 5,

        [AdyenPaymentMapping("doku_ovo")]
        [StringValue("OVO via DOKU")]
        DokuOVO = 6
    }
}
