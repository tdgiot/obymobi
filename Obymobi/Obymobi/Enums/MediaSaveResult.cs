using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents the MediaSaveResult
	/// </summary>
    public enum MediaSaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Failed
        ///// </summary>
        //Failed = 200,

        /// <summary>
        /// NoForeignKeySet
        /// </summary>
        [StringValue("Er is geen foreign key opgegeven.")]
        NoForeignKeySet = 201,

        /// <summary>
        /// CopyFileToMediaFolderCallNotAllowedWithoutRelatedEntity
        /// </summary>
        [StringValue("Het kopieren van een bestand naar media map is niet toegestaan zonder gerelateerde entity.")]
        CopyFileToMediaFolderCallNotAllowedWithoutRelatedEntity = 202,

        /// <summary>
        /// FilenameInUse
        /// </summary>
        [StringValue("Het bestand is momenteel in gebruik.")]
        FilenameInUse = 202,

        /// <summary>
        /// FilenameInUse
        /// </summary>
        [StringValue("Het bestand werd niet gevonden op de schijf.")]
        FileNotFound = 203,
        
        MissingOrUnsupportedRelatedEntity = 204,

        OnlyOneMediaPdfPerAttachment = 205,

        MimeTypeNotSet = 206,

        ExtensionNotSet = 207,

        ExtensionInvalidFormat = 208,

    }
}