using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the CompanySaveResult
    /// </summary>
    public enum CompanySaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Failed
        ///// </summary>
        //Failed = 200,

        /// <summary>
        /// NameEmpty
        /// </summary>
        [StringValue("Er is geen naam opgegeven.")]
        NameEmpty = 201,

        /// <summary>
        /// CurrencyChangeNotAllowed
        /// </summary>
        [StringValue("Het is niet toegestaan om de valuta van een bedrijf te veranderen.")]
        CurrencyChangeNotAllowed = 202,

        /// <summary>
        /// CurrencyChangeNotAllowed
        /// </summary>
        [StringValue("U heeft niet voldoende rechten om een bedrijf te verwijderen.")]
        NotEnoughRightsToDeleteCompany = 203,

        /// <summary>
        /// CurrencyChangeNotAllowed
        /// </summary>
        [StringValue("De 'Verwijder'-schakelaar is niet ingeschakeld, daardoor kan het bedrijf niet worden verwijderd.")]
        DeleteCompanyFlagIsNotSet = 204,

        /// <summary>
        /// NameInvalid
        /// </summary>
        [StringValue("De naam bevat ongeldige tekens.")]
        NameInvalid = 205,

        /// <summary>
        /// EmailIsUsedBySurvey
        /// </summary>
        [StringValue("De email kan niet leeg zijn. Een survey is ingesteld om zijn resultaten hier naartoe te sturen.")]
        EmailIsUsedBySurvey = 206,

        /// <summary>
        /// UserIsUsedForAutoLoginThroughEmail
        /// </summary>
        [StringValue("Deze gebruiker wordt gebruikt om in te loggen via de email bij het versturen van enquete resultaten.")]
        UserIsUsedForAutoLoginThroughEmail = 207,

        /// <summary>
        /// UserIsUsedForAutoLoginThroughCmsUITab
        /// </summary>
        [StringValue("Deze gebruiker wordt gebruikt om in te loggen via de console bij het bekijken van enquete resultaten.")]
        UserIsUsedForAutoLoginThroughCmsUITab = 208,

        /// <summary>
        /// ClientsRunningBelowMinimumApplicationVersionForRestApi
        /// </summary>        
        ClientsRunningBelowMinimumApplicationVersionForRestApi = 209,
    }
}
