﻿namespace Obymobi.Enums
{
    public enum MessagingServiceConnectionStatus
    {
        Unknown = -1,
        Connecting,
        Connected,
        ConnectionLost,
        Reconnecting
    }
}
