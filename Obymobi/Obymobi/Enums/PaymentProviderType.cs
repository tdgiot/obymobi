﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum PaymentProviderType
    {
        [StringValue("Adyen")]
        Adyen = 1,
        //[StringValue("Mollie")]
        //Mollie = 2,
        [StringValue("Omise")]
        Omise = 3,
        [StringValue("Adyen HPP")]
        AdyenHpp = 4,
		[StringValue("Adyen For Platforms")]
		AdyenForPlatforms = 5
    }
}