﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum TagType
    {
        [StringValue("Company")]
        Company,

        [StringValue("System")]
        System
    }
}
