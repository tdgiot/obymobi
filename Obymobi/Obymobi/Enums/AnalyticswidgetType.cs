using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
    /// Enumeration which represents the AnalyticswidgetType
	/// </summary>
	public enum AnalyticswidgetType : int
	{
        [StringValue("Top pageviews pie chart")]
		TopPageviewsPieChart = 1,

        [StringValue("Top entertainment pie chart")]
		TopEntertainmentPieChart = 2,

        [StringValue("Orders per deliverypoint table")]
        OrdersPerDeliverypoint = 3,

        [StringValue("Bestsellers table")]
        Bestsellers = 4,

        [StringValue("Most popular categories table")]
        MostPopularCategories = 5,

        [StringValue("Emenu usage table")]
        EmenuUsage = 6,

		[StringValue("Terminal Up-/Downtime")]
		TerminalUptime = 7,

        [StringValue("Top advertisements")]
        TopAdvertisements = 8,

        [StringValue("Top announcements")]
        TopAnnouncements = 9,

        [StringValue("Custom")]
        Custom = 10
	}
}
