﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum CompanyManagementTaskStatus
    {
        Pending = 0,

        Started = 10,

        Completed = 20,

        Failed = 99
    }
}
