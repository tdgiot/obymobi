﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum RatingSaveResult : int
    {
        /// <summary>
        /// Rating was successfully saved
        /// </summary>
        [StringValue("Waardering is succesvol verzonden")]
        Success = 100,

		/// <summary>
		/// Xml Serialize Error
		/// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de waarderingen.")]
		XmlSerializeError = 201,

        /// <summary>
        /// Rating failed to save
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de waarderingen. (205)")]
        EntitySaveRecursiveFalse = 202,

        /// <summary>
        /// Rating failed to save
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de waarderingen. (206)")]
        EntitySaveRecursiveException = 203,
    }
}
