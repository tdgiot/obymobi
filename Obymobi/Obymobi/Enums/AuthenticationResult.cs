using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the result of the authentication process
    /// </summary>
    public enum AuthenticationResult : int
    {
        /// <summary>
        /// User was successfully authenticated
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Telephone number was not found in the database .
        /// </summary>
        [StringValue("Het opgegeven telefoonnummer is onbekend.")]
        PhonenumberUnknown = 200,

        /// <summary>
        /// Customer was not found in the database 
        /// </summary>
        [StringValue("Er bestaat geen customer in de database voor de opgegeven id.")]
        CustomerIdUnknown = 201,

        /// <summary>
        /// Serializing the result to XML failed
        /// </summary>
        [StringValue("Er is een fout opgetreden.")]
        SerializeToXmlFailed = 302,

        /// <summary>
        /// User is Blacklisted
        /// </summary>
        [StringValue("Je account is geblokkeerd vanwege misbruik.")]
        Blacklisted = 203,

        /// <summary>
        /// Incorrect password specified by the user
        /// </summary>
        [StringValue("De opgegeven pincode is onjuist.")]
        PasswordIncorrect = 204,

        /// <summary>
        /// User has been locked out due to multiple failed login attempts
        /// </summary>
        [StringValue("Je account is geblokkeerd omdat je drie maal een foute pincode hebt opgegeven.")]
        LockedOut = 205,

        /// <summary>
        /// There was no pincode entered for the signon
        /// </summary>
        [StringValue("Er is geen pincode opgegeven bij het inloggen.")]
        NoPincodeWasSupplied = 206,

        /// <summary>
        /// There was no pincode entered for the signon
        /// </summary>
        [StringValue("Er is geen telefoonnummer of e-mailadres opgegeven bij het inloggen.")]
        NoEmailAddressSupplied = 207,

        /// <summary>
        /// There was no pincode entered for the signon
        /// </summary>
        [StringValue("Er is een telefoonnummer én e-mailadres opgegeven bij het inloggen.")]
        MultipleCustomerIdentifiersSupplied = 208,

        /// <summary>
        /// There was no pincode entered for the signon
        /// </summary>
        [StringValue("Er zijn meerdere gebruikers gevonden met hetzelfde e-mailadres.")]
        MultipleCustomersFound = 209,

        /// <summary>
        /// E-mail was not found in the database .
        /// </summary>
        [StringValue("Het opgegeven e-mailadres is onbekend.")]
        EmailUnkown = 210,

        /// <summary>
        /// No companies found for User
        /// </summary>
        [StringValue("Voor de gebruiker zijn bedrijven ingesteld.")]
        NoCompaniesFoundForUser = 211,

        /// <summary>
        /// Unknown Error
        /// </summary>
        [StringValue("Onbekende fout.")]
        UnknownError = 212,

        /// <summary>
        /// Account not activated
        /// </summary>
        [StringValue("Account is niet geactiveerd.")]
        NotActivated = 214,
   
        /// <summary>
        /// Credentials Unknown or Invalid
        /// </summary>
        [StringValue("Logingegevens onbekend of incorrect.")]
        CredentialsUnknownOrInvalid = 216,

        /// <summary>
        /// Password not supplied
        /// </summary>
        [StringValue("Wachtwoord niet opgegeven.")]
        PasswordNotSupplied = 217,

        /// <summary>
        /// Facebook user ID not supplied
        /// </summary>
        [StringValue("Facebook user ID niet opgegeven.")]
        FacebookUserIdNotSupplied = 218,

        /// <summary>
        /// Facebook access key not supplied
        /// </summary>
        [StringValue("Facebook access key niet opgegeven.")]
        FacebookAccessKeyNotSupplied = 219,

        /// <summary>
        /// There are multiple customers for this Facebook account
        /// </summary>
        [StringValue("Er zijn meerdere gebruikers gevonden met dezelfde Facebook account.")]
        MultipleCustomersFoundForFacebook = 220,

        /// <summary>
        /// Too many failed attempts. Waiting for next 3 attempts.
        /// </summary>
        [StringValue("Te vaak verkeerde logingegevens ingevoerd.")]
        AwaitingAttempts = 221,

        /// <summary>
        /// Company is not configured to be using the Console
        /// </summary>
        [StringValue("Het bedrijf is niet geconfigureerd voor Console gebruik.")]
        CompanyNotConfiguredForConsoleUsage = 222,

        /// <summary>
        /// The Order Handling Method could not be determined
        /// </summary>
        [StringValue("De orderverwerkingsmethode kan niet worden vastgesteld.")]
        OrderHandlingMethodCannotBeDetermined = 223,

        /// <summary>
        /// Account isn't verified
        /// </summary>
        [StringValue("Account is nog niet geverifieerd.")]
        AccountNotVerified = 224,
    }
}
