﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the result of saving a Posdeliverypoint
    /// </summary>
    public enum PosdeliverypointSaveResult : int
    {
        /// <summary>
        /// Authentication result is unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// User was successfully authenticated
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Company id was not specified
        /// </summary>
        [StringValue("Er is geen company id opgegeven.")]
        CompanyIdEmpty = 200,

        /// <summary>
        /// External id was not specified
        /// </summary>
        [StringValue("Er is geen externe id opgegeven.")]
        ExternalIdEmpty = 201,

        /// <summary>
        /// External id already exists in database
        /// </summary>
        [StringValue("De opgegeven externe id is niet uniek.")]
        ExternalIdAlreadyExists = 202,

        /// <summary>
        /// Name was not specified
        /// </summary>
        [StringValue("Er is geen naam opgegeven.")]
        NameEmpty = 203
    }
}
