using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum CloudEnvironment : int
    {
        /// <summary>
        /// Work with Primary environment (automatically determined by CloudStatus)
        /// </summary>
        ProductionPrimary = 0,
        /// <summary>
        /// Work with Secondary environment (automatically determined by CloudStatus)
        /// </summary>
        ProductionSecondary = 1,
        /// <summary>
        /// Production is overwritten manualy (automatically determined by CloudStatus), this disable updating automatically by the NOC. 
        /// The AlternativeUrl should be filled.
        /// </summary>
        ProductionOverwrite = 2,
        /// <summary>
        /// Run on the Test environment (no fail-overing)        
        /// </summary>
        Test = 3,
        /// <summary>
        /// Run on the Developement environment (no fail-overing)           
        /// </summary>
        Development = 4,
        /// <summary>
        /// Run on the Demo environment (no fail-overing)           
        /// </summary>
        Demo = 5,
        /// <summary>
        /// Used for Manual configuration of a WebserviceUrl (i.e. local testing or special installations)
        /// Don't forget to also set the AlternativeWebserviceUrl
        /// </summary>
        Manual = 6,
        /// <summary>
        /// Staging environment
        /// </summary>
        Staging = 7,
    }
}
