﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum PokeinLogType : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Customer
        /// </summary>
        Customer = 100,

        /// <summary>
        /// Client
        /// </summary>
        Client = 101,

        /// <summary>
        /// Console
        /// </summary>
        Console = 200,

        /// <summary>
        /// CMS
        /// </summary>
        CMS = 300,

        /// <summary>
        /// Emenu
        /// </summary>
        Emenu = 400,

        /// <summary>
        /// OnsiteServer
        /// </summary>
        OnSiteServer = 500
    }
}
