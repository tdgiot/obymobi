﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum ScheduledCommandStatus : int
	{
        /// <summary>
        /// Inactive
        /// </summary>
        Inactive = 0,

        /// <summary>
        /// Pending c.q. waiting to be started
        /// </summary>
        Pending = 100,

        /// <summary>
        /// Started c.q. in progress
        /// </summary>
        Started = 200,

        /// <summary>
        /// Succeeded
        /// </summary>
        Succeeded = 300,

        /// <summary>
        /// Expired
        /// </summary>
        Expired = 400,

        /// <summary>
        /// Skipped
        /// </summary>
        Skipped = 500,

        /// <summary>
        /// Failed
        /// </summary>
        Failed = 999
	}
}
