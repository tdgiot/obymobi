using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum FormSaveResult : int
    {
        /// <summary>
        /// Rating was successfully saved
        /// </summary>
        [StringValue("Formulier is succesvol verzonden")]
        Success = 100,

		/// <summary>
		/// Xml Serialize Error
		/// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van het formulier.")]
		XmlSerializeError = 201,

        /// <summary>
        /// Rating failed to save
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van het formulier. (202)")]
        EntitySaveRecursiveFalse = 202,

        /// <summary>
        /// Rating failed to save
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van het formulier. (203)")]
        EntitySaveRecursiveException = 203,
    }
}
