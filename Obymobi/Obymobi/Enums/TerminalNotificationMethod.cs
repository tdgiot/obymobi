﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    /// <summary>
    /// Notification Methods for Events on the Console
    /// </summary>
    public enum TerminalNotificationMethod : int
    {
        /// <summary>
        /// Unknown = not configured
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Uses the default of the Crave Platform (= Visual & Audio Infinite Repeat)
        /// </summary>
        Default = 1,
        /// <summary>
        /// No notifications
        /// </summary>
        NoNotification = 2,

        /*
        /// <summary>
        /// Visual notification, once
        /// </summary>
        VisualOnce = 100,
        /// <summary>
        /// Audio notification, once
        /// </summary>
        AudioOnce = 101,
        /// <summary>
        /// Visual & Audio notification, once
        /// </summary>
        VisualAndAudioOnce = 102,

        /// <summary>
        /// Visual notification, twice (with 30s interval)
        /// </summary>
        VisualTwice = 200,
        /// <summary>
        /// Audio notification, twice (with 30s interval)
        /// </summary>
        AudioTwice = 201,
        /// <summary>
        /// Visual & Audio notification, twice (with 30s interval)
        /// </summary>
        VisualAndAudioTwice = 202,

        // Thrice? 
        // http://www.bbc.co.uk/worldservice/learningenglish/grammar/learnit/learnitv304.shtml

        /// <summary>
        /// Visual notification, thrice (with 30s interval)        
        /// </summary>
        VisualThrice = 300,
        /// <summary>
        /// Audio notification, thrice (with 30s interval)
        /// </summary>
        AudioThrice = 301,
        /// <summary>
        /// Visual & Audio notification, thrice (with 30s interval)
        /// </summary>
        VisualAndAudioThrice = 302,

        /// <summary>
        /// Visual notification, infinite repeat (with 30s interval)
        /// </summary>
        VisualInfiniteRepeat = 400,
        /// <summary>
        /// Audio notification, infinite repeat (with 30s interval)
        /// </summary>
        AudioInfiniteRepeat = 401,
        /// <summary>
        /// Audio and Visual notification, infinite repeat (with 30s interval)
        /// </summary>
        VisualAndAudioInfiniteRepeat = 402,

        /// <summary>
        /// Visual notification, infinite repeat (with no interval between notifications)
        /// </summary>
        VisualInfiniteConstant = 500,
        /// <summary>
        /// Visual notification, infinite repeat (with no interval between notifications)
        /// </summary>
        AudioInfiniteConstant = 501,
        /// <summary>
        /// Visual notification, infinite repeat (with no interval between notifications)
        /// </summary>
        VisualAndAudioInfiniteConstant = 502
        */
    }
}
