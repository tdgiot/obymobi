﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum ScreensaverMode : int
    {
        [StringValue("Disabled")]
        Disabled = 0,

        [StringValue("Always")]
        Always = 1,

        [StringValue("Night Mode Only")]
        NightModeOnly = 2,
    }
}
