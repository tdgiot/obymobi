using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the result of the authentication process
    /// </summary>
	public enum ConfirmationCodeDeliveryType : int
    {
        /// <summary>
        /// The Confirmation Code receipt will be delivered by the staff to the guest.
        /// </summary>
        [StringValue("Bevestigingscode wordt gebracht bij de consument.")]
        DeliveredToCustomer = 100,

        /// <summary>
        /// The Confirmation Code receipt will be retrieved from the bar/reception by the guest.
        /// </summary>
        [StringValue("Bevestigingscode dient afgehaald te worden door de consument.")]
        RetrievedByCustomer = 200,	
    }
}
