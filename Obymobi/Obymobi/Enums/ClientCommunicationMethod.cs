using System;
using System.Reflection;

namespace Obymobi.Enums
{
	/// <summary>
	/// Types of communication
	/// </summary>
	/// <remarks>
	/// VALUES NEED TO BE POWERS OF TWO (0,1,2,4,8,16,etc..)
	/// </remarks>
	[Flags]
	public enum ClientCommunicationMethod
	{
		/// <summary>
		/// Dont use. Internally used to fetch 0 values
		/// </summary>
		[CommunicationMethodAttr(10000)]
		None = 0,

		/// <summary>
		/// Force webservice communication
		/// </summary>
		/// <remarks>
        /// DEFAULT IN DATABASE!! IF YOU CHANGE THIS ALSO CHANGE THE LastCommunicationMethod FIELD ON Terminal AND Client TABLE!!
        /// </remarks>
        [CommunicationMethodAttr(9999)]
		Webservice = 1,

        /// <summary>
        /// PokeIn comet connection
        /// </summary>
        [CommunicationMethodAttr(1)]
        XPokeIn = 2,

        /// <summary>
        /// SignalR comet connection
        /// </summary>
        [CommunicationMethodAttr(2)]
        SignalR = 4,

        [CommunicationMethodAttr(10000)]
		All = (Webservice | XPokeIn | SignalR)
	}

    class CommunicationMethodAttr : Attribute
    {
        internal CommunicationMethodAttr(int priority)
        {
            this.Priority = priority;
        }

        public int Priority { get; private set; }
    }

    public static class ClientCommunicationMethodExt
    {
        public static int GetPriority(this ClientCommunicationMethod p)
        {
            var attr = GetAttr(p);
            return attr.Priority;
        }

        /// <summary>
        /// Checks if this ClientCommunicationMethod has a higher priority than supplied method.
        /// </summary>
        /// <param name="method"></param>
        /// <param name="otherMethod"></param>
        /// <returns></returns>
        public static bool IsHigherPriority(this ClientCommunicationMethod method, ClientCommunicationMethod otherMethod)
        {
            return (method.GetPriority() < otherMethod.GetPriority());
        }

        /// <summary>
        /// Gets a lower priority method
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public static ClientCommunicationMethod GetLowerPriority(this ClientCommunicationMethod method)
        {
            var lowerPriority = ClientCommunicationMethod.Webservice;
            var values = EnumUtil.GetValues<ClientCommunicationMethod>();
            foreach (var clientCommunicationMethod in values)
            {
                if (clientCommunicationMethod == ClientCommunicationMethod.None || clientCommunicationMethod == ClientCommunicationMethod.All)
                {
                    continue;
                }

                if (clientCommunicationMethod.GetPriority() > method.GetPriority() && clientCommunicationMethod.GetPriority() < lowerPriority.GetPriority())
                {
                    lowerPriority = clientCommunicationMethod;
                }
            }

            return lowerPriority;
        }

        public static string GetName(this ClientCommunicationMethod method)
        {
            if (method == ClientCommunicationMethod.Webservice)
                return "Webservice";
            if (method == ClientCommunicationMethod.XPokeIn)
                return "PokeIn (Obsolete)";
            if (method == ClientCommunicationMethod.SignalR)
                return "SignalR";

            return "Unknown";
        }

        private static CommunicationMethodAttr GetAttr(ClientCommunicationMethod p)
        {
            return (CommunicationMethodAttr)Attribute.GetCustomAttribute(ForValue(p), typeof(CommunicationMethodAttr));
        }

        private static MemberInfo ForValue(ClientCommunicationMethod p)
        {
            return typeof(ClientCommunicationMethod).GetField(Enum.GetName(typeof(ClientCommunicationMethod), p));
        }
    }
}
