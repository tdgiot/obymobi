﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    /// <summary>
    /// KEEP THIS IN SYNC WITH THE VALUES IN ANDROID!
    /// </summary>
    public enum WifiEapMode : int
    {
        Peap = 0,
        Tls = 1,
        Ttls = 2,
        Pwd = 3
    }
}
