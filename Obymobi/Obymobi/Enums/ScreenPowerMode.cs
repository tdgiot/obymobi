﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum ScreenOffMode
    {
        [StringValue("Responsive (Brightness 0%)")]
        Responsive = 0,

        [StringValue("Non-Responsive (Screen Off)")]
        NonResponsive = 1,
    }

    public enum PowerButtonMode
    {
        [StringValue("Dialog")]
        Dialog = 0,

        [StringValue("Screen Off")]
        ScreenOff = 1,

        [StringValue("Do Nothing")]
        DoNothing = 2
    }
}
