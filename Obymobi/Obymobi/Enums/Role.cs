﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum Role
    {
        /// <summary>
        /// God mode
        /// </summary>
        [StringValue("GodMode")]
        GodMode = 9999,

        /// <summary>
        /// Crave
        /// </summary>
        [StringValue("Crave")]
        Crave = 5000,

        /// <summary>
        /// Administrator
        /// </summary>
        [StringValue("Administrator")]
        Administrator = 1000,

        /// <summary>
        /// Reseller
        /// </summary>
        [StringValue("Reseller")]
        Reseller = 500,

        /// <summary>
        /// Supervisor
        /// </summary>
        [StringValue("Supervisor")]
        Supervisor = 250,

        /// <summary>
        /// Console
        /// </summary>
        [StringValue("Console")]
        Console = 150,

        /// <summary>
        /// Customer
        /// </summary>
        [StringValue("Customer")]
        Customer = 100
    }
}
