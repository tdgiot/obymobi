﻿using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which defines the POS system to connect with
    /// </summary>
    public enum POSConnectorType : int
    {
        [StringValue("None / Unknown")]
        Unknown = 0,
        Agilysys = 14,
        SimphonyGen1 = 15
    }
}
