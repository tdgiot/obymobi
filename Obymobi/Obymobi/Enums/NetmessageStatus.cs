namespace Obymobi.Enums
{
	public enum NetmessageStatus
	{
		/// <summary>
		/// Message has been saved, but not yet delivered to the client
		/// </summary>
		WaitingForVerification = 0,

		/// <summary>
		/// Message has successfully been delivered to the client
		/// </summary>
		Delivered = 1,
		
		/// <summary>
		/// Message is cancelled because the client configuration changed (ClientValidator)
		/// </summary>
		CancelledNewConfiguration = 2,

        TimedOut = 3,

        Completed = 1000,
	}
}
