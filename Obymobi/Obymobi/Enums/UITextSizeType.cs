using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.Enums
{
	/// <summary>
    /// Enumeration which represents the type of the UI text size.
	/// </summary>
	public enum UITextSizeType : int
    {
	    // Controls
        [StringValue("Button")]
	    Button = 101,
        [StringValue("Textbox")]
	    Textbox = 102,
        [StringValue("Spinner")]
	    Spinner = 103,
        [StringValue("Tab")]
	    Tab = 104,
        [StringValue("Bullet pager")]
	    BulletPager = 105,
        [StringValue("CheckBox")]
	    CheckBox = 106,
        [StringValue("RadioButton")]
	    RadioButton = 107,
        [StringValue("TextView")]
	    TextView = 108,
        [StringValue("Toast")]
        Toast = 109,

	    // Widgets
        [StringValue("Widget")]
	    Widget = 201,
        [StringValue("Widget caption")]
	    WidgetCaption = 202,
        [StringValue("Widget description")]
	    WidgetDescription = 203,

	    // Dialogs
        [StringValue("Dialog title")]
	    DialogTitle = 301,
        [StringValue("Dialog subtitle")]
	    DialogSubtitle = 302,
        [StringValue("Dialog text")]
	    DialogText = 303,

	    // Lists
        [StringValue("List category")]
	    ListCategory = 401,
        [StringValue("List item")]
	    ListItem = 402,
        [StringValue("List item price")]
	    ListItemPrice = 403,

	    // Pages
        [StringValue("Page title")]
	    PageTitle = 501,
        [StringValue("Page description")]
	    PageDescription = 502,
        [StringValue("Page error")]
	    PageError = 503,
        [StringValue("Page price")]
	    PagePrice = 504,
        [StringValue("Page button")]
	    PageButton = 505,

	    // Entertainment
        [StringValue("Entertainment name")]
	    EntertainmentName = 601,
        [StringValue("Entertainment description")]
	    EntertainmentDescription = 602,

	    // Messages
        [StringValue("Message title")]
	    MessageTitle = 701,
        [StringValue("Message text")]
        MessageText = 702,

	    // Browser
        [StringValue("Browser close button")]
	    BrowserCloseButton = 801,
        [StringValue("Browser address header")]
	    BrowserAddressHeader = 802,

	    // PDF
        [StringValue("PDF panel")]
	    PdfPanel = 901,

	    // Room controls
        [StringValue("Room control")]
	    RoomControl = 1001,
        [StringValue("Room control spinner")]
	    RoomControlSpinner = 1002,
        [StringValue("Room control station name")]
	    RoomControlStationName = 1003,
        [StringValue("Room control station description")]
	    RoomControlStationDescription = 1004,
        [StringValue("Room control station number")]
	    RoomControlStationNumber = 1005,
        [StringValue("Room control widget caption")]
	    RoomControlWidgetCaption = 1006,
        [StringValue("Room control widget value")]
	    RoomControlWidgetValue = 1007,
        [StringValue("Room control thermostat fan caption")]
	    RoomControlThermostatFanCaption = 1008,
        [StringValue("Room control sleep view")]
	    RoomControlSleepViewTextView = 1009,
        [StringValue("Room control area")]
	    RoomControlArea = 1010,
        [StringValue("Room control two state button center text")]
	    RoomControlTwoStateButtonCenterText = 1011,
        [StringValue("Room control two state button top bottom text")]
	    RoomControlTwoStateButtonTopBottomText = 1012,
        [StringValue("Room control temperature seek arc")]
	    RoomControlTemperatureSeekArc = 1013,
        [StringValue("Room control temperature seek arc widget")]
	    RoomControlTemperatureSeekArcWidget = 1014,
        [StringValue("Room control temperature toggle button")]
	    RoomControlTemperatureToggleButton = 1015,
        [StringValue("Room control curtain name")]
	    RoomControlCurtainName = 1016,
        [StringValue("Room control curtain button")]
	    RoomControlCurtainButton = 1017,
        [StringValue("Room control light name")]
	    RoomControlLightName = 1018,
        [StringValue("Room control light percentage")]
	    RoomControlLightPercentage = 1019,
        [StringValue("Room control scene name")]
	    RoomControlSceneName = 1020,
        [StringValue("Room control scene button")]
	    RoomControlSceneButton = 1021,
        [StringValue("Room control blind name")]
	    RoomControlBlindName = 1022,
        [StringValue("Room control blind button")]
	    RoomControlBlindButton = 1023,
        [StringValue("Room control widget name")]
	    RoomControlWidgetName = 1024,
        [StringValue("Room control widget button")]
	    RoomControlWidgetButton = 1025,
        [StringValue("Room control remote control button")]
	    RoomControlRemoteControlButton = 1026,

	    // Alterations
        [StringValue("Alteration dialog title")]
	    AlterationDialogTitle = 1101,
        [StringValue("Alteration dialog input")]
	    AlterationDialogInput = 1102,
        [StringValue("Alteration dialog description")]
	    AlterationDialogDescription = 1103,
        [StringValue("Alteration dialog price")]
	    AlterationDialogPrice = 1104,
        [StringValue("Alteration dialog date")]
	    AlterationDialogDate = 1105,
        [StringValue("Alteration dialog summary product")]
	    AlterationDialogSummaryProduct = 1106,
        [StringValue("Alteration dialog summary price")]
	    AlterationDialogSummaryPrice = 1107,
        [StringValue("Alteration dialog summary no items")]
	    AlterationDialogSummaryNoItems = 1108,
        [StringValue("Alteration dialog summary total")]
	    AlterationDialogSummaryTotal = 1109,

	    // Product dialog
        [StringValue("Product dialog title")]
	    ProductDialogTitle = 1201,
        [StringValue("Product dialog subtitle")]
	    ProductDialogSubtitle = 1202,
        [StringValue("Product dialog button")]
	    ProductDialogButton = 1203,
        [StringValue("Product dialog price")]
	    ProductDialogPrice = 1204,
        [StringValue("Product dialog description")]
	    ProductDialogDescription = 1205,
        [StringValue("Product dialog column instructions")]
	    ProductDialogColumnInstructions = 1206,
        [StringValue("Product dialog column option secondary text")]
	    ProductDialogColumnOptionSecondaryText = 1207,
        [StringValue("Product dialog column option suggestion text")]
	    ProductDialogColumnOptionSuggestionText = 1208,

	    // Other
        [StringValue("Alarm clock alert dialog")]
	    AlarmClockAlertDialog = 1301,
        [StringValue("Bluetooth control dialog subtext")]
	    BluetoothControlDialogSubtext = 1302,
        [StringValue("Bluetooth control dialog name")]
	    BluetoothControlDialogName = 1303,
        [StringValue("Bluetooth control dialog status")]
	    BluetoothControlDialogStatus = 1304,
        [StringValue("Bluetooth devices dialog")]
	    BluetoothDevicesDialog = 1305,
        [StringValue("Currency dialog radio button")]
	    CurrencyDialogRadioButton = 1306,
        [StringValue("Language dialog radio button")]
	    LanguageDialogRadioButton = 1307,
        [StringValue("Log dialog text")]
	    LogDialogText = 1308,
        [StringValue("Management dialog text")]
	    ManagementDialogText = 1309,
        [StringValue("Management dialog table number caption")]
	    ManagementDialogTableNumberCaption = 1310,
        [StringValue("Management dialog table number value")]
	    ManagementDialogTableNumberValue = 1311,
        [StringValue("Management dialog status caption")]
	    ManagementDialogStatusCaption = 1312,
        [StringValue("Management dialog status text")]
	    ManagementDialogStatusText = 1313,
        [StringValue("Orderitem added dialog button add")]
	    OrderitemAddedDialogButtonAdd = 1314,
        [StringValue("Orderitem added dialog quantity")]
	    OrderitemAddedDialogQuantity = 1315,
        [StringValue("Screen saver dialog")]
	    ScreenSaverDialog = 1316,
        [StringValue("Screen saver dialog time")]
	    ScreenSaverDialogTime = 1317,
        [StringValue("Screen saver dialog colon")]
	    ScreenSaverDialogColon = 1318,
        [StringValue("Service item added dialog button add")]
	    ServiceItemAddedDialogButtonAdd = 1319,
        [StringValue("Service item added dialog quantity")]
        ServiceItemAddedDialogQuantity = 1320,
        [StringValue("Venue dialog")]
	    VenueDialog = 1321,
        [StringValue("Web attachment dialog")]
	    WebAttachmentDialog = 1322,
        [StringValue("Order view header")]
	    OrderViewHeader = 1323,
        [StringValue("Order deliverypoint small")]
        OrderDeliverypointSmall = 1324,
        [StringValue("Order deliverypoint medium")]
	    OrderDeliverypointMedium = 1325,
        [StringValue("Order deliverypoint large")]
	    OrderDeliverypointLarge = 1326,
        [StringValue("Connect Bluetooth device widget 1x1")]
	    ConnectBtDeviceWidget1x1 = 1327,
        [StringValue("Connect Bluebooth device widget")]
	    ConnectBtDeviceWidget = 1328,
        [StringValue("Room control navigation widget 1x2")]
	    RoomControlNavigationWidget1x2 = 1329,
        [StringValue("Venue header view tab")]
	    VenueHeaderViewTab = 1330,
        [StringValue("Order history button")]
	    OrderHistoryButton = 1331,
        [StringValue("Order history quantity")]
	    OrderHistoryQuantity = 1332,
        [StringValue("Venue summary category")]
	    VenueSummaryCategory = 1333,
        [StringValue("Console dialog tab")]
	    ConsoleDialogTab = 1334,
        [StringValue("Console UI mode name")]
	    ConsoleUIModeName = 1335,
        [StringValue("Screen saver dialog radio")]
        ScreenSaverDialogRadio = 1336
    }
}
