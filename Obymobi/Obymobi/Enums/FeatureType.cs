using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents the type of feature
	/// </summary>
	public enum FeatureType : int
	{
		/// <summary>
		/// Rate our products
		/// </summary>
        [StringValue("Rate our products")]
		RateOurProducts = 100
	}
}
