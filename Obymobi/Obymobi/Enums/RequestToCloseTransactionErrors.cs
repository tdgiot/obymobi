﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum RequestToCloseTransactionErrors : int
	{
		/// <summary>
		/// No Open Transaction
		/// </summary>
        [StringValue("Er is geen lopende transactie.")]
		NoOpenTransaction = 200,
	}
}
