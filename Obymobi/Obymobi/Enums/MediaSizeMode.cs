﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum MediaSizeMode : int
    {
        /// <summary>
        /// Fits both sides, resulting in white space if it's not exactly the same ratio as it's container
        /// </summary>
        [StringValue("Fit All")]
        FitAll = 0,
        /// <summary>
        /// Fits the image based on its width, result in either horizontal white space (image is flatter than it's container) 
        /// or vertical scrolling (image is higher than it's container).
        /// </summary>
        [StringValue("Fit Width")]
        FitWidth = 1,
        /// <summary>
        /// Will zoom to a specific percentage which has to be entered on the Media
        /// </summary>
        [StringValue("Specific Level")]
        ManualZoom = 2,
    }
}
