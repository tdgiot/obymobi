﻿namespace Obymobi.Enums
{
    public enum CategoryViewType
    {
        Inherit = 0,

        Standard = 1,

        Cascading = 2
    }
}
