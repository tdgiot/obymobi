﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public sealed class MenuItemType
    {
        public static readonly string Category = "Category";
        public static readonly string Product = "Product";
    }
}
