﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum SynchronizeAlterationoptionsResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Multiple alteration options found for the specified external id
        /// </summary>
        [StringValue("Er zijn meerdere alteration options gevonden voor de opgegeven pos alteration id.")]
        MultipleAlterationoptionsFound = 200,

        /// <summary>
        /// Multiple alteration options found for the specified external id
        /// </summary>
        [StringValue("Het Posproduct kon niet worden gevonden via de PosproductExternalId.")]
        RelatedPosproductNotFound = 201,

        /// <summary>
        /// Multiple alteration options found for the specified external id
        /// </summary>
        [StringValue("The related Posproduct for an AlterationOption doesn't have a Product")]
        PosproductForAlterationOptionHasNoProduct = 202,

        /// <summary>
        /// Multiple alteration options found for the specified external id
        /// </summary>
        [StringValue("The related Posproduct for an AlterationOption has multiple products")]
        PosproductForAlterationOptionHasMultipleProduct = 203
    }
}
