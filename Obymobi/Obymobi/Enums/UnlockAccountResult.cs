﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum UnlockAccountResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Password was successfully reset
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Customer unknown
        /// </summary>
        [StringValue("Het opgegeven telefoonnummer is onbekend.")]
        UnknownPhonenumber = 201,

        /// <summary>
        /// Customer unknown
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de SMS.")]
        SmsSentFailed = 302,

        /// <summary>
        /// Account was not locked
        /// </summary>
        [StringValue("Het opgegeven account was niet geblokkeerd.")]
        AccountNotLocked = 303
    }
}
