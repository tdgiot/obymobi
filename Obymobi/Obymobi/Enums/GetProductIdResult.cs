using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum GetProductIdResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// No pos product found for the specified id
        /// </summary>
        [StringValue("Er zijn geen producten gevonden voor de opgegeven external pos product id.")]
        NoProductFound = 200,

        /// <summary>
        /// Multiple pos products found
        /// </summary>
        [StringValue("Er zijn meerdere producten gevonden voor de opgegeven pos product id.")]
        MultipleProductsFound = 201
    }
}
