﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum RoomControlAreaType : int
    {
        /// <summary>
        /// Normal
        /// </summary>
        [StringValue("Normal")]
        Normal = 1,

        /// <summary>
        /// Show All
        /// </summary>
        [StringValue("Show All")]
        ShowAll = 2,
    }
}
