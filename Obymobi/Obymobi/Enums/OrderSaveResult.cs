﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum OrderSaveResult : int
    {
        /// <summary>
        /// Unspecified
        /// </summary>
        [StringValue("Unspecified")]
        Unspecified = 0,

        /// <summary>
        /// Order was successfully saved
        /// </summary>
        [StringValue("Bestelling is succesvol verzonden")]
        Success = 100,

        /// <summary>
        /// Xml Serialize Error
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de bestelling")]
        XmlSerializeError = 201,

        /// <summary>
        /// Deliverypoint is unknown
        /// </summary>
        [StringValue("Er is een onjuist {0}nummer opgegeven.")]
        UnknownDeliverypoint = 202,

        /// <summary>
        /// Deliverypoint is unknown
        /// </summary>
        [StringValue("Er is een onbekend product besteld.")]
        UnknownProduct = 203,

        /// Deliverypoint is unknown
        /// </summary>
        [StringValue("Er is een ongeldig product besteld.")]
        InvalidProductForCompany = 204,

        /// <summary>
        /// Order failed to save
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de bestelling. (205)")]
        EntitySaveRecursiveFalse = 205,

        /// <summary>
        /// Order failed to save
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de bestelling. (206)")]
        EntitySaveRecursiveException = 206,

        /// <summary>
        /// User has an open tap in another company
        /// </summary>
        [StringValue("Er is een openstaande transactie bij een ander bedrijf.")]
        OpenTransactionInOtherCompany = 207,

        /// <summary>
        /// User is blacklisted
        /// </summary>
        [StringValue("Je account is geblokkeerd.")]
        CustomerBlacklisted = 208,

        /// <summary>
        /// The previous order is still pending
        /// </summary>
        [StringValue("Je vorige bestelling is nog niet verwerkt.")]
        PreviousOrderIsPending = 209,

        /// <summary>
        /// Transaction Already Opened
        /// </summary>
        [StringValue("Transactie is al geopend.")]
        TransactionAlreadyOpened = 210,

        /// <summary>
        /// Transaction Already Closed
        /// </summary>
        [StringValue("Transactie is al gesloten.")]
        TransactionAlreadyClosed = 211,

        /// <summary>
        /// Transaction is closing
        /// </summary>
        [StringValue("Transactie wordt momenteel gesloten.")]
        TransactionIsClosing = 212,

        /// <summary>
        /// An error occurred while executing the payment request
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzoek tot betaling.")]
        PaymentRequestExecutionError = 213,

        /// <summary>
        /// The company is outside it's business hours
        /// </summary>
        [StringValue("Het bedrijf is momenteel gesloten.")]
        CompanyIsClosed = 214,

        /// <summary>
        /// User is locked out
        /// </summary>
        [StringValue("Je account is geblokkeerd.")]
        CustomerLockedOut = 215,

        /// <summary>
        /// Paymentmethod not supported by company
        /// </summary>
        [StringValue("De betaalmethode wordt niet ondersteunt door dit bedrijf.")]
        PaymentmethodNotSupported = 216,

        /// <summary>
        /// Change not allowed to an existing order
        /// </summary>
        [StringValue("Wijziging niet toegestaan voor bestaande order.")]
        ChangeNotAllowedToExistingOrder = 217,

        /// <summary>
        /// Product can not be ordered due to visibily or order schedule
        /// </summary>
        [StringValue("Dit product kan momenteel niet (meer) besteld worden.")]
        ProductOrderHoursNotAvailable = 219,

        /// Deliverypoint is unknown
        /// </summary>
        [StringValue("Een of meerder producten kunnen niet worden besteld.")]
        InvalidProducts = 220,

        [StringValue("Bestellen vanaf een mobiel apparaat is uitgeschakeld.")]
        MobileOrderingDisabled = 221,

		[StringValue("Niet binnen bereik van het hotel")]
		GeoFencingOutOfRange = 222,

		[StringValue("Onmogelijk om locatie te bepalen")]
		GeoFencingNoLocation = 223,

		[StringValue("Customer moet gezet zijn")]
		CustomerMustBeSet = 224,

        /// <summary>
        /// The company is outside it's business hours
        /// </summary>
        [StringValue("Het bedrijf is momenteel niet in staat om bestellingen te ontvangen.")]
        NoTerminalConnectedToDeliverypoint = 315,

        /// <summary>
        /// Terminal offline while company is in business
        /// </summary>
        [StringValue("Het bedrijf is momenteel niet in staat om bestellingen te ontvangen.")]
        TerminalOffline = 316,

        /// <summary>
        /// Transaction without created date
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de bestelling. (317)")]
        TransactionWithoutCreatedDate = 317,

        /// <summary>
        /// No order items for order
        /// </summary>
        [StringValue("Er bevinden zich geen producten in de bestelling.")]
        NoOrderitemsForOrder = 318,

        /// <summary>
        /// Deliverypoint is unknown
        /// </summary>
        [StringValue("Het opgegeven {0}nummer is (tijdelijk) niet ingebruik.")]
        DeliverypointgroupDisabled = 321,

        /// <summary>
        /// No order items for order
        /// </summary>
        [StringValue("De order is niet gevalideerd door de OrderProcessingHelper.")]
        OrderIsNotValidatedByOrderProcessingHelper = 323,

        /// <summary>
        /// Overlapping exceptions found
        /// </summary>
        [StringValue("Er is een overlap aangetroffen in de openingstijden.")]
        OverlappingBusinesshourexceptions = 324,
        
        /// <summary>
        /// No business hours found for company
        /// </summary>
        [StringValue("Er zijn geen openingstijden beschikbaar.")]
        NoBusinesshoursAvailable = 325,

        /// <summary>
        /// Comet provider could not be set or found
        /// </summary>
        [StringValue("Comet provider could not be set or found")]
        CometProviderNotSet = 500,

        /// <summary>
        /// Failed to save Netmessage object in the database
        /// </summary>
        [StringValue("Failed to save Netmessage object in the database.")]
        FailedSavingNetmessage = 501,

        /// <summary>
        /// Guid not unique
        /// </summary>
        [StringValue("Guid not unique.")]
        GuidNotUnique = 502,

        /// <summary>
        /// Guid not unique
        /// </summary>
        [StringValue("Analytics Recording Failed.")]
        AnalyticsRecordingFailed = 503,

        AuthenticationError = 998,

        Failure = 999
    }
}
