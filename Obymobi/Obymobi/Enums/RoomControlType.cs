﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum RoomControlType : int
    {
        [StringValue("None")]
        None = -1,
        [StringValue("Demo Mode")]
        Demo = 0,
        [StringValue("Control4")]
        Control4 = 1,
        [StringValue("Control4 (Crave API)")]
        C4CraveApi = 2,
        [StringValue("Elecon (Modbus)")]
        Modbus = 3,
        [StringValue("Inncom")]
        Inncom = 4,
        [StringValue("Interel")]
        Interel = 5,
        [StringValue("RoomControlServer")]
        RoomControlServer = 6
    }
}