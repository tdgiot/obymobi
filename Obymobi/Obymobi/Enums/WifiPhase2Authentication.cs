﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    /// <summary>
    /// KEEP THIS IN SYNC WITH THE VALUES IN ANDROID!
    /// </summary>
    public enum WifiPhase2Authentication : int
    {
        None = 0,
        Pap = 1,
        Mschap = 2,
        MschapV2 = 3,
        Gtc = 4
    }
}
