﻿using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration of checkout method types.
    /// </summary>
    public enum CheckoutType
    {
        /// <summary>
        /// Charge to guest room.
        /// </summary>
        [StringValue("Charge To Room")]
        ChargeToRoom = 0,

        /// <summary>
        /// Pay via a payment provider.
        /// </summary>
        [StringValue("Payment Provider")]
        PaymentProvider = 1,

        /// <summary>
        /// Pay via a payment provider.
        /// </summary>
        [StringValue("Pay later")]
        PayLater = 2,

        /// <summary>
        /// Free of charge checkout.
        /// </summary>
        [StringValue("Free Of Charge")]
        FreeOfCharge = 3,
    }
}
