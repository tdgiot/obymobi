﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum ReportingPeriod
    {
        [StringValue("Today")]
        Today,
        [StringValue("Trading Day")]
        TradingDay,
        [StringValue("Yesterday")]
        Yesterday,
        [StringValue("Last 2 Days")]
        Last2days,
        [StringValue("Last 5 Days")]
        Last5days,
        [StringValue("Last 7 Days")]
        Last7days,
        [StringValue("Last 14 Days")]
        Last14days,
        [StringValue("Last 28 Days")]
        Last28days,
        [StringValue("Last 30 Days")]
        Last30days,
        [StringValue("Last 60 Days")]
        Last60days,
        [StringValue("Last 90 Days")]
        Last90days,
        [StringValue("Last 180 Days")]
        Last180days,
        [StringValue("Last 365 Days")]
        Last365days,
        [StringValue("Current Month")]
        CurrentMonth,
        [StringValue("Current Quarter")]
        CurrentQuarter,
        [StringValue("Current Year")]
        CurrentYear,
        [StringValue("Last Month")]
        LastMonth,
        [StringValue("Last Quarter")]
        LastQuarter,
        [StringValue("Last Year")]
        LastYear,
        [StringValue("Manual")]
        Manual
    }
}