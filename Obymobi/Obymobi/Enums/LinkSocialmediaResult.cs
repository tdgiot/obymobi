﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum LinkSocialmediaResult : int
    {
        Unknown = 0,

        Success = 100,

        SocialmediaAccountAlreadyLinked = 430,

        EmailAlreadyUsedByCustomer = 431,

        Failure = 998,

        InvalidHash = 999
    }
}
