﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum UnspecifiedError : int
	{
		/// <summary>
		/// Unknown
		/// </summary>
		[StringValue("Er is een technische fout opgetreden.")]
		Unknown = 0,
	}
}
