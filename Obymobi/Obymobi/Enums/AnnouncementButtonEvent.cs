using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum AnnouncementButtonEvent : int
	{
        [StringValue("No action")]
        None = 0,

        [StringValue("Go to Home")]
        GoToHome = 1,

        [StringValue("Go to Drinks (Deprecated - Use Category)")]
        GoToDrinks = 2,

        [StringValue("Go to Food (Deprecated - Use Category)")]
        GoToFood = 3,

        [StringValue("Go to More")]
        GoToMore = 4,

        [StringValue("Go to Basket")]
        GoToBasket = 5,

        [StringValue("Go idle")]
        GoIdle = 6,

        [StringValue("Go to category")]
        GoToCategory = 7,

        [StringValue("Go to entertainment category")]
        GoToEntertainmentCategory = 8
	}
}
