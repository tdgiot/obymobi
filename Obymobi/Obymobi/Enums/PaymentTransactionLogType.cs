﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum PaymentTransactionLogType
    {
        [StringValue("Generic")]
        Generic = 0,
        [StringValue("Payment")]
        Payment = 1,
        [StringValue("Refund")]
        Refund = 2,
        [StringValue("Chargeback")]
        Chargeback = 3,
        [StringValue("Cancelled")]
        Cancelled = 4,
        [StringValue("Failed")]
        Failed = 10
    }
}