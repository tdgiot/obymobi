﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum UpdateStatus : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Idle")]
        Idle = 0,

        /// <summary>
        /// Download started
        /// </summary>
        [StringValue("Download started")]
        DownloadStarted = 100,

        /// <summary>
        /// Download completed
        /// </summary>
        [StringValue("Download completed")]
        DownloadCompleted = 200,

        /// <summary>
        /// Download failed
        /// </summary>
        [StringValue("Download failed")]
        DownloadFailed = 300,

        /// <summary>
        /// Installation started
        /// </summary>
        [StringValue("Installation started")]
        InstallationStarted = 400,

        /// <summary>
        /// Installation completed
        /// </summary>
        [StringValue("Installation completed")]
        InstallationCompleted = 500,

        /// <summary>
        /// Installation failed
        /// </summary>
        [StringValue("Installation failed")]
        InstallationFailed = 600
    }
}