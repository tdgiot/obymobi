﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum BluetoothDeviceType : int
	{
        [StringValue("Sony BKB10 Printer")]
        Sony_BKB10 = 0,

        [StringValue("Sony BSC10 Speaker")]
        Sony_BSC10 = 1,

        [StringValue("Crave Dock")]
        Crave_Dock = 2
	}
}
