﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum SendEmailConfirmationResult : int
    {
        Unknown = 0,

        Success = 100,

        EmailAddressUnknown = 200,

        NoConfirmationForAnonymousAccounts = 201,

        Blacklisted = 204,

        EmailAddressIncorrectFormat = 205,

        EmailAddressAlreadyVerified = 206,

        NoInitialLinkIdentifierOrNewEmailGuidSet = 207,

        Failure = 998,

        InvalidHash = 999
    }
}
