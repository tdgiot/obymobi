﻿namespace Obymobi.Enums
{
    public enum OrderNotificationType
    {
        OrderConfirmation = 1,
        PaymentPending = 2,
        PaymentFailed = 3,
        Receipt = 4,
        OrderComplete = 5
    }
}