﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum OptInType
    {
        [StringValue("Disabled")]
        NoOptIn = 0,

        [StringValue("Hard Opt-In (checkbox)")]
        HardOptInCheckbox = 1,

        [StringValue("Hard Opt-In (radio-buttons)")]
        HardOptInRadioButtons = 2
    }
}
