using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    /// <summary>
    /// Used to group Media with equal ratio's and equal purposes to be grouped in 1 tab in the CMS
    /// </summary>
    public enum MediaTypeGroup
    {
        None = 0,
        ByodProductButton = 100000,
        ByodProductBranding = 110000,
        ByodCategoryButton = 120000,
        ByodCategoryBranding = 130000,
        ByodCompanyList = 140000,
        ByodDeliverypointgroupList = 140001,
        ByodHomepage = 150000,
        ByodPointOfInterestList = 160000,
        ByodPointOfInterestHomepage = 170000,
        ByodMapIcon = 180000,
        ByodPointOfInterestMapIcon = 190000,
		ByodHomepage2 = 200000,
		ByodHomepageAdvert = 210000,
        ByodHomepage3 = 220000,

        ByodProductButtonPlaceholder = 230000,
        ByodProductBrandingPlaceholder = 240000,
        ByodCategoryButtonPlaceholder = 250000,
        ByodCategoryBrandingPlaceholder = 260000,

        ByodAlterationDialogPage = 270000,

        ByodAttachmentThumbnail = 280000,

        SitePageIcon = 10000000,
        SiteFullPageImage = 10000100,
        SiteTwoThirdsPageImage = 10000200,
        SiteHdWideScreen = 10000300,
        SiteCinemaWideScreen = 10000400,
        SiteMobileHeaderImage = 10000500,
        
        SiteIrtPointOfInterestSummaryImage = 10000600,
        SiteIrtPointOfInterestSummaryMicrositeImage = 10000610,
        SiteTabletPointOfInterestSummaryImage = 10000700,
        SiteMobilePointOfInterestSummaryImage = 10000800,
        
        SiteIrtCompanySummaryImage = 10000900,
        SiteIrtCompanySummaryMicrositeImage = 10000910,
        SiteTabletCompanySummaryImage = 10001000,
        SiteMobileCompanySummaryImage = 10001100,

        SiteBranding = 10001200,

        AppLessHeaderLogo = 10002001,
        AppLessHero = 10002002,
        AppLessCarousel = 10002003,
        AppLessActionBanner = 10002004,
        AppLessApplicationIcon = 10002005,
        AppLessProductBanner = 10002006,
        AppLessCategoryBanner = 10002007
    }
}
