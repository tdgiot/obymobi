﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum OrderStatusUpdateResult : int
    {
        /// <summary>
        /// Order status is unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// The order is successfully updated
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// At least updating one of the statuss failed
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het updaten van de order status.")]
        Failed = 200,
    }
}
