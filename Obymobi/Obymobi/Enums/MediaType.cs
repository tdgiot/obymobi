using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enum to be used to specify the type of media entity
    /// </summary>
    public enum MediaType : int
    {
        NoMediaTypeSpecified = 0,
        MediaTypeNotSpecifiedForMediaTypeGroupDeviceCombination = 1,

        #region Otoucho - 1280x800

        /// <summary>
        /// Big photo which is displayed when a user clicks the branding photo
        /// </summary>
        [StringValue("Productfoto")]
        ProductPhoto = 100,

        /// <summary>
        /// Small photo which is displayed when product is displayed
        /// </summary>
        [StringValue("Product branding")]
        ProductBranding = 200,

        /// <summary>
        /// Button image of a product
        /// </summary>
        [StringValue("Product knop")]
        ProductButton = 300,

        /// <summary>
        /// Button image of a category
        /// </summary>
        [StringValue("Categorie knop")]
        CategoryButton = 400,

        /// <summary>
        /// Image of an advertisement
        /// </summary>
        [StringValue("Advertentie")]
        Advertisement = 500,

        /// <summary>
        /// Big image of an advertisement
        /// </summary>
        [StringValue("Advertentiefoto")]
        AdvertisementPhoto = 600,

        /// <summary>
        /// Button image of an entertainment item
        /// </summary>
        [StringValue("Entertainment knop")]
        EntertainmentButton = 700,

        /// <summary>
		/// Basisproduct knop
        /// </summary>
        [StringValue("Basisproduct knop")]
        GenericproductButton = 601,

        /// <summary>
		/// Basisproduct branding
        /// </summary>
        [StringValue("Basisproduct branding")]
        GenericproductBranding = 701,

        /// <summary>
        /// Eye-catcher of the agecheck view
        /// </summary>
        [StringValue("Leeftijdcontrole")]
        AgeCheck = 1000,

        /// <summary>
        /// Eye-catcher of the homepage view
        /// </summary>
        [StringValue("Homepage")]
        Homepage = 1100,

        /// <summary>
        /// Basiscategory knop
        /// </summary>
        [StringValue("Basiscategorie knop")]
        GenericcategoryButton = 1200,

        #endregion

        #region Archos - 800x480

        /// <summary>
        /// Crave notification dialog
        /// </summary>
        [StringValue("Notification venster")]
        NotificationDialog800x480 = 1300,

        /// <summary>
        /// Button image of a product
        /// </summary>
        [StringValue("Product knop")]
        ProductButton800x480 = 1400,

        /// <summary>
        /// Crave product button placeholder
        /// </summary>
        [StringValue("Crave product button placeholder")]
        ProductButtonPlaceholder800x480 = 1401,

        /// <summary>
        /// Brading image of a product
        /// </summary>
        [StringValue("Product branding")]
        ProductBranding800x480 = 1500,

        /// <summary>
        /// Crave product button placeholder
        /// </summary>
        [StringValue("Crave product button placeholder")]
        ProductBrandingPlaceholder800x480 = 1501,

        /// <summary>
        /// Basisproduct knop
        /// </summary>
        [StringValue("Basisproduct knop")]
        GenericProductButton800x480 = 1700,

        /// <summary>
        /// Basisproduct branding
        /// </summary>
        [StringValue("Basisproduct branding")]
        GenericProductBranding800x480 = 1800,

        /// <summary>
        /// Eye-catcher of the homepage view
        /// </summary>
        [StringValue("Homepage")]
        Homepage800x480 = 1900,

        /// <summary>
        /// Button image on an entertainment item
        /// </summary>
        [StringValue("Entertainment knop")]
        EntertainmentButton800x480 = 2000,

        /// <summary>
        /// Button image on an entertainment item with out an image
        /// </summary>
        [StringValue("Entertainment knop placeholder")]
        EntertainmentButtonPlaceholder800x480 = 2001,

        /// <summary>
        /// Branding image of a social medium
        /// </summary>
        [StringValue("Socialmedia branding")]
        SocialmediaBranding800x480 = 2100,

        /// <summary>
        /// Branding image of the customer data capture
        /// </summary>
        [StringValue("Customer data capture branding")]
        CustomerDataCaptureBranding800x480 = 2200,

        /// <summary>
        /// Photo of a category
        /// </summary>
        [StringValue("Photo of a category")]
        CategoryPhoto800x480 = 2300,

        /// <summary>
        /// Placeholder for category photo
        /// </summary>
        [StringValue("Placeholder for category photo")]
        CategoryPhotoPlaceholder800x480 = 2301,

        /// <summary>
        /// Branding image of a category
        /// </summary>
        [StringValue("Photo of a category")]
        CategoryBranding800x480 = 2400,

        /// <summary>
        /// Homepage fullscreen
        /// </summary>
        [StringValue("Homepage fullscreen")]
        HomepageFullscreen800x480 = 2500,

        /// <summary>
        /// Image next to an alteration
        /// </summary>
        [StringValue("Alteration Image")]
        AlterationImage800x480 = 2600,

        /// <summary>
        /// Branding image of a product on mobile devices
        /// </summary>
        [StringValue("Photo of a product on mobile devices")]
        CraveProductBrandingMobile = 2700,

        /// <summary>
        /// Image to show with the battery out of charge message
        /// </summary>
        [StringValue("Battery out of charge image")]
        OutOfCharge800x480 = 2800,

        /// <summary>
        /// Branding image of a generic product on mobile devices
        /// </summary>
        [StringValue("Photo of a product on mobile devices")]
        CraveGenericProductBrandingMobile = 3000,

        /// <summary>
        /// Image to show with the order processed message
        /// </summary>
        [StringValue("Order processed image")]
        OrderProcessed800x480 = 3100,

        /// <summary>
        /// Image to show with the service request processed message
        /// </summary>
        [StringValue("Service request processed image")]
        ServiceProcessed800x480 = 3200,

        /// <summary>
        /// Image to show with the rating processed message
        /// </summary>
        [StringValue("Rating processed image")]
        RatingProcessed800x480 = 3300,

        /// <summary>
        /// Image to show with the social media processed message
        /// </summary>
        [StringValue("Socialmedia processed image")]
        SocialmediaProcessed800x480 = 3400,

        /// <summary>
        /// Image to show with the survey processed message
        /// </summary>
        [StringValue("Survey processed image")]
        SurveyProcessed800x480 = 3500,

        /// <summary>
        /// Image to show with the form processed message
        /// </summary>
        [StringValue("Form processed image")]
        FormProcessed800x480 = 3600,

        /// <summary>
        /// Crave basket image
        /// </summary>
        [StringValue("Crave Basket")]
        Basket800x480 = 3700,

        /// <summary>
        /// Crave advertisement image
        /// </summary>
        [StringValue("Crave Advertisement")]
        Advertisement800x480 = 3800,

        /// <summary>
        /// Crave console logo
        /// </summary>
        [StringValue("Crave Console Logo")]
        CraveConsoleLogo = 3900,

        /// <summary>
        /// Crave Product Added
        /// </summary>
        [StringValue("Crave Product Added")]
        ProductAdded800x480 = 4000,

        /// <summary>
        /// Advertisement half next to category (Crave)
        /// </summary>
        [StringValue("Crave Half Advertisement")]
        AdvertisementHalf800x480 = 6800,

        #endregion

        #region Samsung - 1280x800

        /// <summary>
        /// Branding image of a product
        /// </summary>
        [StringValue("Product branding")]
        ProductBranding1280x800 = 4100,

        /// <summary>
        /// Product branding placeholder
        /// </summary>
        [StringValue("Product branding placeholder")]
        ProductBrandingPlaceholder1280x800 = 4101,

        /// <summary>
        /// Product photo horizontal
        /// </summary>
        [StringValue("Product photo horizontal")]
        ProductPagePhotoHorizontal1280x800 = 4110,

        /// <summary>
        /// Product photo horizontal placeholder
        /// </summary>
        [StringValue("Product photo horizontal placeholder")]
        ProductPagePhotoHorizontalPlaceholder1280x800 = 4111,

        /// <summary>
        /// Product photo vertical
        /// </summary>
        [StringValue("Product photo vertical")]
        ProductPagePhotoVertical1280x800 = 4120,

        /// <summary>
        /// Product photo vertical placeholder
        /// </summary>
        [StringValue("Product photo vertical placeholder")]
        ProductPagePhotoVerticalPlaceholder1280x800 = 4121,

        /// <summary>
        /// Button image of a product
        /// </summary>
        [StringValue("Product button")]
        ProductButton1280x800 = 4200,

        /// <summary>
        /// Product button placeholder
        /// </summary>
        [StringValue("Product button placeholder")]
        ProductButtonPlaceholder1280x800 = 4201,

        /// <summary>
        /// Product icon
        /// </summary>
        [StringValue("Product icon")]
        ProductIcon1280x800 = 4210,

        /// <summary>
        /// Product icon placeholder
        /// </summary>
        [StringValue("Product icon placeholder")]
        ProductIconPlaceholder1280x800 = 4211,

        /// <summary>
        /// Basisproduct knop
        /// </summary>
        [StringValue("Basisproduct knop")]
        GenericProductButton1280x800 = 4300,

        /// <summary>
        /// Generic product icon
        /// </summary>
        [StringValue("Generic product icon")]
        GenericProductIcon1280x800 = 4310,

        /// <summary>
        /// Basisproduct branding
        /// </summary>
        [StringValue("Basisproduct branding")]
        GenericProductBranding1280x800 = 4400,

        /// <summary>
        /// Generic product photo horizontal
        /// </summary>
        [StringValue("Generic product horizontal photo")]
        GenericProductPagePhotoHorizontal1280x800 = 4410,

        /// <summary>
        /// Generic product photo vertical
        /// </summary>
        [StringValue("Generic product vertical photo")]
        GenericProductPagePhotoVertical1280x800 = 4420,

        /// <summary>
        /// Eye-catcher of the homepage view
        /// </summary>
        [StringValue("Homepage")]
        Homepage1280x800 = 4500,

        /// <summary>
        /// Button image on an entertainment item
        /// </summary>
        [StringValue("Entertainment knop")]
        EntertainmentButton1280x800 = 4600,

        /// <summary>
        /// Entertainment button placeholder
        /// </summary>
        [StringValue("Entertainment button placeholder")]
        EntertainmentButtonPlaceholder1280x800 = 4601,

        /// <summary>
        /// Branding image of a social medium
        /// </summary>
        [StringValue("Socialmedia branding")]
        SocialmediaBranding1280x800 = 4700,

        /// <summary>
        /// Branding image of the customer data capture
        /// </summary>
        [StringValue("Customer data capture branding")]
        CustomerDataCaptureBranding1280x800 = 4800,

        /// <summary>
        /// Photo of a category
        /// </summary>
        [StringValue("Photo of a category")]
        CategoryPhoto1280x800 = 4900,

        /// <summary>
        /// Photo of a horizontal category page
        /// </summary>
        [StringValue("Photo of a horizontal category page")]
        CategoryPagePhotoHorizontal1280x800 = 4910,

        /// <summary>
        /// Photo of a vertical category page
        /// </summary>
        [StringValue("Photo of a vertical category page")]
        CategoryPagePhotoVertical1280x800 = 4920,

        /// <summary>
        /// Category photo placeholder
        /// </summary>
        [StringValue("Category button placeholder")]
        CategoryPhotoPlaceholder1280x800 = 4901,

        /// <summary>
        /// Category page horizontal photo placeholder
        /// </summary>
        [StringValue("Category page horizontal photo placeholder")]
        CategoryPagePhotoHorizontalPlaceholder1280x800 = 4911,

        /// <summary>
        /// Category page photo placeholder
        /// </summary>
        [StringValue("Category page vertical photo placeholder")]
        CategoryPagePhotoVerticalPlaceholder1280x800 = 4921,

        /// <summary>
        /// Category page photo full page
        /// </summary>
        [StringValue("Category page full page photo")]
        CategoryPagePhotoFullPage1280x800 = 4930,

        /// <summary>
        /// Branding image of a category
        /// </summary>
        [StringValue("Photo of a category")]
        CategoryBranding1280x800 = 5000,

        /// <summary>
        /// Homepage fullscreen
        /// </summary>
        [StringValue("Homepage fullscreen")]
        HomepageFullscreen1280x800 = 5100,

        /// <summary>
        /// Image to show with the battery out of charge message
        /// </summary>
        [StringValue("Battery out of charge image")]
        OutOfCharge1280x800 = 5200,

        /// <summary>
        /// Image to show with the order processed message
        /// </summary>
        [StringValue("Order processed image")]
        OrderProcessed1280x800 = 5300,

        /// <summary>
        /// Image to show with the service request processed message
        /// </summary>
        [StringValue("Service request processed image")]
        ServiceProcessed1280x800 = 5400,

        /// <summary>
        /// Image to show with the rating processed message
        /// </summary>
        [StringValue("Rating processed image")]
        RatingProcessed1280x800 = 5500,

        /// <summary>
        /// Image to show with the social media processed message
        /// </summary>
        [StringValue("Socialmedia processed image")]
        SocialmediaProcessed1280x800 = 5600,

        /// <summary>
        /// Image to show with the survey processed message
        /// </summary>
        [StringValue("Survey processed image")]
        SurveyProcessed1280x800 = 5700,

        /// <summary>
        /// Image to show with the form processed message
        /// </summary>
        [StringValue("Form processed image")]
        FormProcessed1280x800 = 5800,

        /// <summary>
        /// Basket image
        /// </summary>
        [StringValue("Crave Basket")]
        Basket1280x800 = 5900,

        /// <summary>
        /// Advertisement image
        /// </summary>
        [StringValue("Crave Advertisement")]
        Advertisement1280x800 = 6000,

        /// <summary>
        /// Product Added Small
        /// </summary>
        [StringValue("Crave Product Added Small")]
        ProductAddedSmall1280x800 = 6099,

        /// <summary>
        /// Product Added
        /// </summary>
        [StringValue("Crave Product Added")]
        ProductAdded1280x800 = 6100,

        /// <summary>
        /// Notification dialog
        /// </summary>
        [StringValue("Crave Product Added")]
        NotificationDialog1280x800 = 6200,

        /// <summary>
        /// Image next to an alteration
        /// </summary>
        [StringValue("Alteration Image")]
        AlterationImage1280x800 = 6300,

        /// <summary>
        /// Image on top of printed receipts
        /// </summary>
        [StringValue("Receipt Logo Image")]
        CraveReceiptLogo = 6400,

        /// <summary>
        /// Image in the company image gallery
        /// </summary>
        [StringValue("Gallery Image")]
        Gallery = 6500,

        /// <summary>
        /// Image advertisements frontpage emenu
        /// </summary>
        [StringValue("Crave Home Advertisement")]
        AdvertisementSmall1280x800 = 6600,

        /// <summary>
        /// Advertisement half next to category (1280x800)
        /// </summary>
        [StringValue("Advertisement half")]
        AdvertisementHalf1280x800 = 6700,

        /// <summary>
        /// Survey page bottom (1280x800)
        /// </summary>
        [StringValue("Survey Page Bottom")]
        SurveyPageBottom1280x800 = 6900,

        /// <summary>
        /// Survey page right (1280x800)
        /// </summary>
        [StringValue("Survey Page Right")]
        SurveyPageRight1280x800 = 7000,

        /// <summary>
        /// Survey page logo (1280x800)
        /// </summary>
        [StringValue("Survey Page Logo")]
        SurveyPageLogo1280x800 = 7100,

        /// <summary>
        /// Survey page right half (1280x800)
        /// </summary>
        [StringValue("Survey Page Right Half")]
        SurveyPageRightHalf1280x800 = 7200,

        /// <summary>
        /// Charger removed dialog (1280x800)
        /// </summary>
        [StringValue("Charger Removed Dialog")]
        ChargerRemovedDialog1280x800 = 7300,

        /// <summary>
        /// Charger removed reminder dialog (1280x800)
        /// </summary>
        [StringValue("Charger Removed Reminder Dialog")]
        ChargerRemovedReminderDialog1280x800 = 7400,

        [StringValue("Clear Basket Dialog")]
        ClearBasketDialog1280x800 = 7401,

        /// <summary>
        /// Survey answer required (1280x800)
        /// </summary>
        [StringValue("Survey Answer Required")]
        SurveyAnswerRequired1280x800 = 7500,

        /// <summary>
        /// PMS Message Dialog (1280x800)
        /// </summary>
        [StringValue("PMS Message Dialog")]
        PmsMessage1280x800 = 7600,

        /// <summary>
        /// PMS Bill logo (1280x800)
        /// </summary>
        [StringValue("PMS Bill logo")]
        PmsBill1280x800 = 7700,

        /// <summary>
        /// PMS Checkout approval (1280x800)
        /// </summary>
        [StringValue("PMS Checkout approval")]
        PmsCheckoutApproval1280x800 = 7800,

        /// <summary>
        /// Alarm Set While Not Charging (1280x800)
        /// </summary>
        [StringValue("Alarm Set While Not Charging")]
        AlarmSetWhileNotCharging1280x800 = 7801,

        SitePageIcon1280x800 = 7900,
        SiteFullPageImage1280x800 = 7910,
        SiteFullPageImageDirectory1280x800 = 7911,
        SiteTwoThirdsPageImage1280x800 = 7920,
        SiteTwoThirdsPageImageDirectory1280x800 = 7921,
        SiteHdWideScreen1280x800 = 7930,
        SiteCinemaWideScreen1280x800 = 7940,
        SitePointOfInterestSummaryPage1280x800 = 7950,
        SiteCompanySummaryPage1280x800 = 7951,
        SitePointOfInterestSummaryPageMicrosite1280x800 = 7952,
        SiteCompanySummaryPageMicrosite1280x800 = 7953,
        SiteBranding1280x800 = 7954,

        TemplateSitePageIcon1280x800 = 8000,
        TemplateSiteFullPageImage1280x800 = 8010,
        TemplateSiteFullPageImageDirectory1280x800 = 8011,
        TemplateSiteTwoThirdsPageImage1280x800 = 8020,
        TemplateSiteTwoThirdsPageImageDirectory1280x800 = 8021,

        // Widget media types
        AdvertisementWidgetSmall1280x800 = 8101,
        AdvertisementWidgetMedium1280x800 = 8102,
        WidgetIconSmall1280x800 = 8103,
        WidgetIconMedium1280x800 = 8104,
        Widget1x1Background1280x800 = 8105,
        Widget1x2Background1280x800 = 8106,
        Widget2x1Background1280x800 = 8107,
        Widget2x2Background1280x800 = 8108,
        AdvertisementWidgetSmallFull1280x800 = 8109,
        AdvertisementWidgetMediumFull1280x800 = 8110,
        AdvertisementWidget1x1Full1280x800 = 8111,
        WidgetPdf1280x800 = 8112,
        WidgetAvailability1280x800 = 8113,
        Widget2x4Background1280x800 = 8114,

        // App customizations
        ScreenBackground1280x800 = 8200,
        FooterLogo1280x800 = 8201,
        FooterBrightnessIcon1280x800 = 8202,
        FooterWarningIcon1280x800 = 8203,
        FooterPendingOrdersIcon1280x800 = 8204,
        FooterGuestInformationIcon1280x800 = 8205,
        FooterCustomIcon1280x800 = 8206,
        ScreensaverBackground1280x800 = 8207,

        RoomControlSectionIcon1280x800 = 8300,
        RoomControlSectionItemIcon1280x800 = 8301,
        RoomControlStation1280x800 = 8302,

        AlterationDialogPage1280x800 = 8303,
        AlterationDialogColumn1280x800 = 8304,

        BrowserAgeVerificationImageSmall1280x800 = 8305,
        BrowserAgeVerificationBackground1280x800 = 8306,
        BrowserAgeVerificationImageBig1280x800 = 8307,

        RestartApplicationDialog1280x800 = 8308,
        RebootDeviceDialog1280x800 = 8309,

        CategoryConfirmation1280x800 = 8310,
        ProductConfirmation1280x800 = 8311,
        CategoryProcessed1280x800 = 8312,
        ProductProcessed1280x800 = 8313,

        GallerySmall = 8399,
        GalleryLarge = 8400,

        ProductDialogAlterationColumn1280x800 = 8500,
        ProductDialogProductgroupColumn1280x800 = 8501,

        SiteMenuHeader1280x800 = 8600,
        SiteMenuIcon1280x800 = 8601,
        SitePageFull1280x800 = 8602,
        SitePageTwoThirds1280x800 = 8603,
        SitePageTwoThirdsPointOfInterest1280x800 = 8604,
        SitePageTwoThirdsCompany1280x800 = 8605,

        MapMenuItem1280x800 = 8606,
        MapMarkerPointOfInterest1280x800 = 8607,
        MapMarkerCompany1280x800 = 8608,

        QrCodeLogo1280x800 = 8609,

        #endregion

        #region Android Smart Tv Box - 1280x720

        /// <summary>
        /// Branding image of a product
        /// </summary>
        [StringValue("Product branding")]
        ProductBranding1280x720 = 10100,

        /// <summary>
        /// Product branding placeholder
        /// </summary>
        [StringValue("Product branding placeholder")]
        ProductBrandingPlaceholder1280x720 = 10101,

        /// <summary>
        /// Button image of a product
        /// </summary>
        [StringValue("Product button")]
        ProductButton1280x720 = 10200,

        /// <summary>
        /// Product button placeholder
        /// </summary>
        [StringValue("Product button placeholder")]
        ProductButtonPlaceholder1280x720 = 10201,

        /// <summary>
        /// Basisproduct knop
        /// </summary>
        [StringValue("Basisproduct knop")]
        GenericProductButton1280x720 = 10300,

        /// <summary>
        /// Basisproduct branding
        /// </summary>
        [StringValue("Basisproduct branding")]
        GenericProductBranding1280x720 = 10400,

        /// <summary>
        /// Eye-catcher of the homepage view
        /// </summary>
        [StringValue("Homepage")]
        Homepage1280x720 = 10500,

        /// <summary>
        /// Button image on an entertainment item
        /// </summary>
        [StringValue("Entertainment knop")]
        EntertainmentButton1280x720 = 10600,

        /// <summary>
        /// Entertainment button placeholder
        /// </summary>
        [StringValue("Entertainment button placeholder")]
        EntertainmentButtonPlaceholder1280x720 = 10601,

        /// <summary>
        /// Branding image of a social medium
        /// </summary>
        [StringValue("Socialmedia branding")]
        SocialmediaBranding1280x720 = 10700,

        /// <summary>
        /// Photo of a category
        /// </summary>
        [StringValue("Photo of a category")]
        CategoryPhoto1280x720 = 10900,

        /// <summary>
        /// Category photo placeholder
        /// </summary>
        [StringValue("Category button placeholder")]
        CategoryPhotoPlaceholder1280x720 = 10901,

        /// <summary>
        /// Branding image of a category
        /// </summary>
        [StringValue("Photo of a category")]
        CategoryBranding1280x720 = 11000,

        /// <summary>
        /// Image to show with the battery out of charge message
        /// </summary>
        [StringValue("Battery out of charge image")]
        OutOfCharge1280x720 = 11200,

        /// <summary>
        /// Image to show with the order processed message
        /// </summary>
        [StringValue("Order processed image")]
        OrderProcessed1280x720 = 11300,

        /// <summary>
        /// Image to show with the service request processed message
        /// </summary>
        [StringValue("Service request processed image")]
        ServiceProcessed1280x720 = 11400,

        /// <summary>
        /// Image to show with the rating processed message
        /// </summary>
        [StringValue("Rating processed image")]
        RatingProcessed1280x720 = 11500,

        /// <summary>
        /// Image to show with the social media processed message
        /// </summary>
        [StringValue("Socialmedia processed image")]
        SocialmediaProcessed1280x720 = 11600,

        /// <summary>
        /// Image to show with the survey processed message
        /// </summary>
        [StringValue("Survey processed image")]
        SurveyProcessed1280x720 = 11700,

        /// <summary>
        /// Image to show with the form processed message
        /// </summary>
        [StringValue("Form processed image")]
        FormProcessed1280x720 = 11800,

        /// <summary>
        /// Basket image
        /// </summary>
        [StringValue("Crave Basket")]
        Basket1280x720 = 11900,

        /// <summary>
        /// Advertisement image
        /// </summary>
        [StringValue("Crave Advertisement")]
        Advertisement1280x720 = 12000,

        /// <summary>
        /// Product Added
        /// </summary>
        [StringValue("Crave Product Added")]
        ProductAdded1280x720 = 12100,

        /// <summary>
        /// Notification dialog
        /// </summary>
        [StringValue("Crave Product Added")]
        NotificationDialog1280x720 = 12200,

        /// <summary>
        /// Image next to an alteration
        /// </summary>
        [StringValue("Alteration Image")]
        AlterationImage1280x720 = 12300,

        /// <summary>
        /// Image advertisements frontpage emenu
        /// </summary>
        [StringValue("Crave Home Advertisement")]
        AdvertisementSmall1280x720 = 12600,

        /// <summary>
        /// Advertisement half next to category (1280x720)
        /// </summary>
        [StringValue("Advertisement half")]
        AdvertisementHalf1280x720 = 12700,

        /// <summary>
        /// Survey page bottom (1280x720)
        /// </summary>
        [StringValue("Survey Page Bottom")]
        SurveyPageBottom1280x720 = 12900,

        /// <summary>
        /// Survey page right (1280x720)
        /// </summary>
        [StringValue("Survey Page Right")]
        SurveyPageRight1280x720 = 13000,

        /// <summary>
        /// Survey page logo (1280x720)
        /// </summary>
        [StringValue("Survey Page Logo")]
        SurveyPageLogo1280x720 = 13100,

        /// <summary>
        /// Survey page right half (1280x720)
        /// </summary>
        [StringValue("Survey Page Right Half")]
        SurveyPageRightHalf1280x720 = 13200,

        /// <summary>
        /// Survey answer required (1280x720)
        /// </summary>
        [StringValue("Survey answer required")]
        SurveyAnswerRequired1280x720 = 13300,

        /// <summary>
        /// PMS Message Dialog (1280x720)
        /// </summary>
        [StringValue("PMS Message Dialog")]
        PmsMessage1280x720 = 13400,

        /// <summary>
        /// PMS Bill logo (1280x720)
        /// </summary>
        [StringValue("PMS Bill logo")]
        PmsBill1280x720 = 13500,

        /// <summary>
        /// PMS Checkout approval (1280x720)
        /// </summary>
        [StringValue("PMS Checkout approval")]
        PmsCheckoutApproval1280x720 = 13600,

        /// <summary>
        /// Homepage Fullscreen (1280x720)
        /// </summary>
        [StringValue("Homepage Fullscreen")]
        HomepageFullscreen1280x720 = 13700,

        SitePageIcon1280x720 = 13800,
        SiteFullPageImage1280x720 = 13810,
        SiteFullPageImageDirectory1280x720 = 13811,
        SiteTwoThirdsPageImage1280x720 = 13820,
        SiteTwoThirdsPageImageDirectory1280x720 = 13821,
        SiteHdWideScreen1280x720 = 13830,
        SiteCinemaWideScreen1280x720 = 13840,
        SitePointOfInterestSummaryPage1280x720 = 13850,
        SiteCompanySummaryPage1280x720 = 13851,
        SiteCompanySummaryPageMicrosite1280x720 = 13852,
        SitePointOfInterestSummaryPageMicrosite1280x720 = 13853,
        //SiteBranding1280x720 = 13854,

        TemplateSitePageIcon1280x720 = 13900,
        TemplateSiteFullPageImage1280x720 = 13910,
        TemplateSiteFullPageImageDirectory1280x720 = 13911,
        TemplateSiteTwoThirdsPageImage1280x720 = 13920,
        TemplateSiteTwoThirdsPageImageDirectory1280x720 = 13921,

        #endregion

        #region Small (phone)

        /// <summary>
        /// Product button small (phone)
        /// </summary>
        [StringValue("Product button small (phone)")]
        ProductButtonPhoneSmall = 1011100,

        /// <summary>
        /// Generic product button small (phone)
        /// </summary>
        [StringValue("Generic product button small (phone)")]
        GenericProductButtonPhoneSmall = 1011101,

        /// <summary>
        /// Product button placeholder small (phone)
        /// </summary>
        [StringValue("Product button placeholder small (phone)")]
        ProductButtonPlaceholderPhoneSmall = 1011102,

        /// <summary>
        /// Product branding small (phone)
        /// </summary>
        [StringValue("Product branding small (phone)")]
        ProductBrandingPhoneSmall = 1011200,

        /// <summary>
        /// Generic product branding small (phone)
        /// </summary>
        [StringValue("Generic product branding small (phone)")]
        GenericProductBrandingPhoneSmall = 1011201,

        /// <summary>
        /// Product branding placeholder small (phone)
        /// </summary>
        [StringValue("Product branding placeholder small (phone)")]
        ProductBrandingPlaceholderPhoneSmall = 1011202,

        /// <summary>
        /// Category button small (phone)
        /// </summary>
        [StringValue("Category button small (phone)")]
        CategoryButtonPhoneSmall = 1011300,

        /// <summary>
        /// Generic category button small (phone)
        /// </summary>
        [StringValue("Generic category button small (phone)")]
        GenericCategoryButtonPhoneSmall = 1011301,

        /// <summary>
        /// Category button placeholder small (phone)
        /// </summary>
        [StringValue("Category button placeholder small (phone)")]
        CategoryButtonPlaceholderPhoneSmall = 1011302,

        /// <summary>
        /// Category branding small (phone)
        /// </summary>
        [StringValue("Category branding small (phone)")]
        CategoryBrandingPhoneSmall = 1011400,

        /// <summary>
        /// Generic category branding small (phone)
        /// </summary>
        [StringValue("Generic category branding small (phone)")]
        GenericCategoryBrandingPhoneSmall = 1011401,

        /// <summary>
        /// Category branding placeholder small (phone)
        /// </summary>
        [StringValue("Category branding placeholder small (phone)")]
        CategoryBrandingPlaceholderPhoneSmall = 1011402,

        /// <summary>
        /// Company list small (phone)
        /// </summary>
        [StringValue("Company list small (phone)")]
        CompanyListPhoneSmall = 1011500,

        /// <summary>
        /// Homepage small (phone)
        /// </summary>
        [StringValue("Homepage small (phone)")]
        HomepagePhoneSmall = 1011600,

        /// <summary>
        /// PointOfInterest list small (phone)
        /// </summary>
        [StringValue("PointOfInterest list small (phone)")]
        PointOfInterestListPhoneSmall = 1011700,

        /// <summary>
        /// PointOfInterest homepage small (phone)
        /// </summary>
        [StringValue("Homepage small (phone)")]
        PointOfInterestHomepagePhoneSmall = 1011800,

        /// <summary>
        /// Map icon small (phone)
        /// </summary>
        [StringValue("Map icon small (phone)")]
        MapIconPhoneSmall = 1011900,

        /// <summary>
        /// PointOfInterest map icon small (phone)
        /// </summary>
        [StringValue("Map icon small (phone)")]
        PointOfInterestMapIconPhoneSmall = 1012000,

        SitePageIconPhoneSmall = 1011905,
        //SiteFullPageImagePhoneSmall = 1011910,
        //SiteTwoThirdsPageImagePhoneSmall = 1011920,
        //SiteHdWideScreenPhoneSmall = 1011930,
        //SiteCinemaWideScreenPhoneSmall = 1011940,
        SiteMobileHeaderImagePhoneSmall = 1011950,
        SitePointOfInterestSummaryPagePhoneSmall = 1011955,
        SiteCompanySummaryPagePhoneSmall = 1011956,
        SiteBrandingPhoneSmall = 1011957,

        TemplateSitePageIconPhoneSmall = 1012005,
        TemplateSiteMobileHeaderImagePhoneSmall = 1012050,

        AlterationDialogPagePhoneSmall = 1012001,
        DeliverypointgroupListPhoneSmall = 1012002,

        #endregion

        #region Normal (phone)

        /// <summary>
        /// Product button normal (phone)
        /// </summary>
        [StringValue("Product button normal (phone)")]
        ProductButtonPhoneNormal = 1012100,

        /// <summary>
        /// Generic product button normal (phone)
        /// </summary>
        [StringValue("Generic product button normal (phone)")]
        GenericProductButtonPhoneNormal = 1012101,

        /// <summary>
        /// Product button placeholder normal (phone)
        /// </summary>
        [StringValue("Product button placeholder normal (phone)")]
        ProductButtonPlaceholderPhoneNormal = 1012102,

        /// <summary>
        /// Product branding normal (phone)
        /// </summary>
        [StringValue("Product branding normal (phone)")]
        ProductBrandingPhoneNormal = 1012200,

        /// <summary>
        /// Generic product branding normal (phone)
        /// </summary>
        [StringValue("Generic product branding normal (phone)")]
        GenericProductBrandingPhoneNormal = 1012201,

        /// <summary>
        /// Product branding placeholder normal (phone)
        /// </summary>
        [StringValue("Product branding placeholder normal (phone)")]
        ProductBrandingPlaceholderPhoneNormal = 1012202,

        /// <summary>
        /// Category button normal (phone)
        /// </summary>
        [StringValue("Category button normal (phone)")]
        CategoryButtonPhoneNormal = 1012300,

        /// <summary>
        /// Generic category button normal (phone)
        /// </summary>
        [StringValue("Generic category button normal (phone)")]
        GenericCategoryButtonPhoneNormal = 1012301,

        /// <summary>
        /// Category button placeholder normal (phone)
        /// </summary>
        [StringValue("Category button placeholder normal (phone)")]
        CategoryButtonPlaceholderPhoneNormal = 1012302,

        /// <summary>
        /// Category branding normal (phone)
        /// </summary>
        [StringValue("Category branding normal (phone)")]
        CategoryBrandingPhoneNormal = 1012400,

        /// <summary>
        /// Generic category branding normal (phone)
        /// </summary>
        [StringValue("Generic category branding normal (phone)")]
        GenericCategoryBrandingPhoneNormal = 1012401,

        /// <summary>
        /// Category branding placeholder normal (phone)
        /// </summary>
        [StringValue("Category branding placeholder normal (phone)")]
        CategoryBrandingPlaceholderPhoneNormal = 1012402,

        /// <summary>
        /// Company list normal (phone)
        /// </summary>
        [StringValue("Company list normal (phone)")]
        CompanyListPhoneNormal = 1012500,

        /// <summary>
        /// Homepage normal (phone)
        /// </summary>
        [StringValue("Homepage normal (phone)")]
        HomepagePhoneNormal = 1012600,

        /// <summary>
        /// PointOfInterest list normal (phone)
        /// </summary>
        [StringValue("PointOfInterest list normal (phone)")]
        PointOfInterestListPhoneNormal = 1012700,

        /// <summary>
        /// PointOfInterest homepage normal (phone)
        /// </summary>
        [StringValue("PointOfInterest homepage normal (phone)")]
        PointOfInterestHomepagePhoneNormal = 1012800,

        /// <summary>
        /// Map icon normal (phone)
        /// </summary>
        [StringValue("Map icon normal (phone)")]
        MapIconPhoneNormal = 1012900,

        SitePageIconPhoneNormal = 1012905,
        //SiteFullPageImagePhoneNormal = 1012910,
        //SiteTwoThirdsPageImagePhoneNormal = 1012920,
        //SiteHdWideScreenPhoneNormal = 1012930,
        //SiteCinemaWideScreenPhoneNormal = 1012940,
        SiteMobileHeaderImagePhoneNormal = 1012950,
        SitePointOfInterestSummaryPagePhoneNormal = 1012955,
        SiteCompanySummaryPagePhoneNormal = 1012956,
        SiteBrandingPhoneNormal = 1012957,

        TemplateSitePageIconPhoneNormal = 1013005,
        TemplateSiteMobileHeaderImagePhoneNormal = 1013050,

        [StringValue("Notification Icon (Android)")]
        NotificationIconNormal = 1012990,

        /// <summary>
        /// PointOfInterest map icon normal (phone)
        /// </summary>
        [StringValue("PointOfInterest map icon normal (phone)")]
        PointOfInterestMapIconPhoneNormal = 1013000,

        AlterationDialogPagePhoneNormal = 1013001,
        DeliverypointgroupListPhoneNormal = 1013002,

        #endregion

        #region Large (phone)

        /// <summary>
        /// Product button large (phone)
        /// </summary>
        [StringValue("Product button large (phone)")]
        ProductButtonPhoneLarge = 1013100,

        /// <summary>
        /// Generic product button large (phone)
        /// </summary>
        [StringValue("Generic product button large (phone)")]
        GenericProductButtonPhoneLarge = 1013101,

        /// <summary>
        /// Product button placeholder large (phone)
        /// </summary>
        [StringValue("Product button placeholder large (phone)")]
        ProductButtonPlaceholderPhoneLarge = 1013102,

        /// <summary>
        /// Product branding large (phone)
        /// </summary>
        [StringValue("Product branding large (phone)")]
        ProductBrandingPhoneLarge = 1013200,

        /// <summary>
        /// Generic product branding large (phone)
        /// </summary>
        [StringValue("Generic product branding large (phone)")]
        GenericProductBrandingPhoneLarge = 1013201,

        /// <summary>
        /// Product branding placeholder large (phone)
        /// </summary>
        [StringValue("Product branding placeholder large (phone)")]
        ProductBrandingPlaceholderPhoneLarge = 1013202,

        /// <summary>
        /// Category button large (phone)
        /// </summary>
        [StringValue("Category button large (phone)")]
        CategoryButtonPhoneLarge = 1013300,

        /// <summary>
        /// Generic category button large (phone)
        /// </summary>
        [StringValue("Generic category button large (phone)")]
        GenericCategoryButtonPhoneLarge = 1013301,

        /// <summary>
        /// Category button placeholder large (phone)
        /// </summary>
        [StringValue("Category button placeholder large (phone)")]
        CategoryButtonPlaceholderPhoneLarge = 1013302,

        /// <summary>
        /// Category branding large (phone)
        /// </summary>
        [StringValue("Category branding large (phone)")]
        CategoryBrandingPhoneLarge = 1013400,

        /// <summary>
        /// Generic category branding large (phone)
        /// </summary>
        [StringValue("Generic category branding large (phone)")]
        GenericCategoryBrandingPhoneLarge = 1013401,

        /// <summary>
        /// Category branding placeholder large (phone)
        /// </summary>
        [StringValue("Category branding placeholder large (phone)")]
        CategoryBrandingPlaceholderPhoneLarge = 1013402,

        /// <summary>
        /// Company list large (phone)
        /// </summary>
        [StringValue("Company list large (phone)")]
        CompanyListPhoneLarge = 1013500,

        /// <summary>
        /// Homepage large (phone)
        /// </summary>
        [StringValue("Homepage large (phone)")]
        HomepagePhoneLarge = 1013600,

        /// <summary>
        /// PointOfInterest list large (phone)
        /// </summary>
        [StringValue("PointOfInterest list large (phone)")]
        PointOfInterestListPhoneLarge = 1013700,

        /// <summary>
        /// PointOfInterest homepage large (phone)
        /// </summary>
        [StringValue("PointOfInterest homepage large (phone)")]
        PointOfInterestHomepagePhoneLarge = 1013800,

        /// <summary>
        /// Map icon large (phone)
        /// </summary>
        [StringValue("Map icon large (phone)")]
        MapIconPhoneLarge = 1013900,

        /// <summary>
        /// PointOfInterest map icon large (phone)
        /// </summary>
        [StringValue("PointOfInterest map icon large (phone)")]
        PointOfInterestMapIconPhoneLarge = 1014000,

        SitePageIconPhoneLarge = 1013905,
        //SiteFullPageImagePhoneLarge = 1013910,
        //SiteTwoThirdsPageImagePhoneLarge = 1013920,
        //SiteHdWideScreenPhoneLarge = 1013930,
        //SiteCinemaWideScreenPhoneLarge = 1013940,
        SiteMobileHeaderImagePhoneLarge = 1013950,
        SitePointOfInterestSummaryPagePhoneLarge = 1013955,
        SiteCompanySummaryPagePhoneLarge = 1013956,
        SiteBrandingPhoneLarge = 1013957,

        TemplateSitePageIconPhoneLarge = 1014005,
        TemplateSiteMobileHeaderImagePhoneLarge = 1014050,

        AlterationDialogPagePhoneLarge = 1014001,
        DeliverypointgroupListPhoneLarge = 1014002,

        #endregion

        #region XLarge (phone)

        /// <summary>
        /// Product button xlarge (phone)
        /// </summary>
        [StringValue("Product button xlarge (phone)")]
        ProductButtonPhoneXLarge = 1014100,

        /// <summary>
        /// Generic product button xlarge (phone)
        /// </summary>
        [StringValue("Generic product button xlarge (phone)")]
        GenericProductButtonPhoneXLarge = 1014101,

        /// <summary>
        /// Product button placeholder xlarge (phone)
        /// </summary>
        [StringValue("Product button placeholder xlarge (phone)")]
        ProductButtonPlaceholderPhoneXLarge = 1014102,

        /// <summary>
        /// Product branding xlarge (phone)
        /// </summary>
        [StringValue("Product branding xlarge (phone)")]
        ProductBrandingPhoneXLarge = 1014200,

        /// <summary>
        /// Generic product branding xlarge (phone)
        /// </summary>
        [StringValue("Generic product branding xlarge (phone)")]
        GenericProductBrandingPhoneXLarge = 1014201,

        /// <summary>
        /// Product branding placeholder xlarge (phone)
        /// </summary>
        [StringValue("Product branding placeholder xlarge (phone)")]
        ProductBrandingPlaceholderPhoneXLarge = 1014202,

        /// <summary>
        /// Category button xlarge (phone)
        /// </summary>
        [StringValue("Category button xlarge (phone)")]
        CategoryButtonPhoneXLarge = 1014300,

        /// <summary>
        /// Generic category button xlarge (phone)
        /// </summary>
        [StringValue("Generic category button xlarge (phone)")]
        GenericCategoryButtonPhoneXLarge = 1014301,

        /// <summary>
        /// Category button placeholder xlarge (phone)
        /// </summary>
        [StringValue("Category button placeholder xlarge (phone)")]
        CategoryButtonPlaceholderPhoneXLarge = 1014302,

        /// <summary>
        /// Category branding xlarge (phone)
        /// </summary>
        [StringValue("Category branding xlarge (phone)")]
        CategoryBrandingPhoneXLarge = 1014400,

        /// <summary>
        /// Generic category branding xlarge (phone)
        /// </summary>
        [StringValue("Generic category branding xlarge (phone)")]
        GenericCategoryBrandingPhoneXLarge = 1014401,

        /// <summary>
        /// Category branding placeholder xlarge (phone)
        /// </summary>
        [StringValue("Category branding placeholder xlarge (phone)")]
        CategoryBrandingPlaceholderPhoneXLarge = 1014402,

        /// <summary>
        /// Company list xlarge (phone)
        /// </summary>
        [StringValue("Company list xlarge (phone)")]
        CompanyListPhoneXLarge = 1014500,

        /// <summary>
        /// Homepage xlarge (phone)
        /// </summary>
        [StringValue("Homepage xlarge (phone)")]
        HomepagePhoneXLarge = 1014600,

        /// <summary>
        /// PointOfInterest list xlarge (phone)
        /// </summary>
        [StringValue("PointOfInterest list xlarge (phone)")]
        PointOfInterestListPhoneXLarge = 1014700,

        /// <summary>
        /// PointOfInterest homepage xlarge (phone)
        /// </summary>
        [StringValue("PointOfInterest homepage xlarge (phone)")]
        PointOfInterestHomepagePhoneXLarge = 1014800,

        /// <summary>
        /// Map icon xlarge (phone)
        /// </summary>
        [StringValue("Map icon xlarge (phone)")]
        MapIconPhoneXLarge = 1014900,

        /// <summary>
        /// PointOfInterest map icon xlarge (phone)
        /// </summary>
        [StringValue("PointOfInterest map icon xlarge (phone)")]
        PointOfInterestMapIconPhoneXLarge = 1015000,

        SitePageIconPhoneXLarge = 1014905,
        //SiteFullPageImagePhoneXLarge = 1014910,
        //SiteTwoThirdsPageImagePhoneXLarge = 1014920,
        //SiteHdWideScreenPhoneXLarge = 1014930,
        //SiteCinemaWideScreenPhoneXLarge = 1014940,
        SiteMobileHeaderImagePhoneXLarge = 1014950,
        SitePointOfInterestSummaryPagePhoneXLarge = 1014955,
        SiteCompanySummaryPagePhoneXLarge = 1014956,
        SiteBrandingPhoneXLarge = 1014957,

        TemplateSitePageIconPhoneXLarge = 1015005,
        TemplateSiteMobileHeaderImagePhoneXLarge = 1015050,

        AlterationDialogPagePhoneXLarge = 1015001,
        DeliverypointgroupListPhoneXLarge = 1015002,

        #endregion

        #region Small (tablet)

        /// <summary>
        /// Product button small (tablet)
        /// </summary>
        [StringValue("Product button small (tablet)")]
        ProductButtonTabletSmall = 2011100,

        /// <summary>
        /// Generic product button small (tablet)
        /// </summary>
        [StringValue("Generic product button small (tablet)")]
        GenericProductButtonTabletSmall = 2011101,

        /// <summary>
        /// Product button placeholder small (tablet)
        /// </summary>
        [StringValue("Product button placeholder small (tablet)")]
        ProductButtonPlaceholderTabletSmall = 2011102,

        /// <summary>
        /// Product branding small (tablet)
        /// </summary>
        [StringValue("Product branding small (tablet)")]
        ProductBrandingTabletSmall = 2011200,

        /// <summary>
        /// Generic product branding small (tablet)
        /// </summary>
        [StringValue("Generic product branding small (tablet)")]
        GenericProductBrandingTabletSmall = 2011201,

        /// <summary>
        /// Product branding placeholder small (tablet)
        /// </summary>
        [StringValue("Product branding placeholder small (tablet)")]
        ProductBrandingPlaceholderTabletSmall = 2011202,

        /// <summary>
        /// Category button small (tablet)
        /// </summary>
        [StringValue("Category button small (tablet)")]
        CategoryButtonTabletSmall = 2011300,

        /// <summary>
        /// Generic category button small (tablet)
        /// </summary>
        [StringValue("Generic category button small (tablet)")]
        GenericCategoryButtonTabletSmall = 2011301,

        /// <summary>
        /// Category button placeholder small (tablet)
        /// </summary>
        [StringValue("Category button placeholder small (tablet)")]
        CategoryButtonPlaceholderTabletSmall = 2011302,

        /// <summary>
        /// Category branding small (tablet)
        /// </summary>
        [StringValue("Category branding small (tablet)")]
        CategoryBrandingTabletSmall = 2011400,

        /// <summary>
        /// Generic category branding small (tablet)
        /// </summary>
        [StringValue("Generic category branding small (tablet)")]
        GenericCategoryBrandingTabletSmall = 2011401,

        /// <summary>
        /// Category branding placeholder small (tablet)
        /// </summary>
        [StringValue("Category branding placeholder small (tablet)")]
        CategoryBrandingPlaceholderTabletSmall = 2011402,

        /// <summary>
        /// Company list small (tablet)
        /// </summary>
        [StringValue("Company list small (tablet)")]
        CompanyListTabletSmall = 2011500,

        /// <summary>
        /// Homepage small (tablet)
        /// </summary>
        [StringValue("Homepage small (tablet)")]
        HomepageTabletSmall = 2011600,

        /// <summary>
        /// PointOfInterest list small (tablet)
        /// </summary>
        [StringValue("PointOfInterest list small (tablet)")]
        PointOfInterestListTabletSmall = 2011700,

        /// <summary>
        /// PointOfInterest homepage small (tablet)
        /// </summary>
        [StringValue("PointOfInterest homepage small (tablet)")]
        PointOfInterestHomepageTabletSmall = 2011800,

        /// <summary>
        /// Map icon small (tablet)
        /// </summary>
        [StringValue("Map icon (tablet)")]
        MapIconTabletSmall = 2011900,

        /// <summary>
        /// PointOfInterest map icon small (tablet)
        /// </summary>
        [StringValue("PointOfInterest map icon small (tablet)")]
        PointOfInterestMapIconTabletSmall = 2012000, // GK wrong sequence

        SitePageIconTabletSmall = 2011910,
        SiteFullPageImageTabletSmall = 2011915,
        SiteTwoThirdsPageImageTabletSmall = 2011920,
        SiteHdWideScreenTabletSmall = 2011925,
        SiteCinemaWideScreenTabletSmall = 2011930,
        SiteMobileHeaderImageTabletSmall = 2011935,
        SitePointOfInterestSummaryPageTabletSmall = 2011955,
        SiteCompanySummaryPageTabletSmall = 2011956,
        SiteBrandingTabletSmall = 2011957,

        TemplateSitePageIconTabletSmall = 2012010,

        #endregion

        #region Normal (tablet)

        /// <summary>
        /// Product button normal (tablet)
        /// </summary>
        [StringValue("Product button normal (tablet)")]
        ProductButtonTabletNormal = 2012100,

        /// <summary>
        /// Generic product button normal (tablet)
        /// </summary>
        [StringValue("Generic product button normal (tablet)")]
        GenericProductButtonTabletNormal = 2012101,

        /// <summary>
        /// Product button placeholder normal (tablet)
        /// </summary>
        [StringValue("Product button placeholder normal (tablet)")]
        ProductButtonPlaceholderTabletNormal = 2012102,

        /// <summary>
        /// Product branding normal (tablet)
        /// </summary>
        [StringValue("Product branding normal (tablet)")]
        ProductBrandingTabletNormal = 2012200,

        /// <summary>
        /// Generic product branding normal (tablet)
        /// </summary>
        [StringValue("Product branding normal (tablet)")]
        GenericProductBrandingTabletNormal = 2012201,

        /// <summary>
        /// Product branding placeholder normal (tablet)
        /// </summary>
        [StringValue("Product branding placeholder normal (tablet)")]
        ProductBrandingPlaceholderTabletNormal = 2012202,

        /// <summary>
        /// Category button normal (tablet)
        /// </summary>
        [StringValue("Category button normal (tablet)")]
        CategoryButtonTabletNormal = 2012300,

        /// <summary>
        /// Generic category button normal (tablet)
        /// </summary>
        [StringValue("Generic category button normal (tablet)")]
        GenericCategoryButtonTabletNormal = 2012301,

        /// <summary>
        /// Category button placeholder normal (tablet)
        /// </summary>
        [StringValue("Category button placeholder normal (tablet)")]
        CategoryButtonPlaceholderTabletNormal = 2012302,

        /// <summary>
        /// Category branding normal (tablet)
        /// </summary>
        [StringValue("Category branding normal (tablet)")]
        CategoryBrandingTabletNormal = 2012400,

        /// <summary>
        /// Generic category branding normal (tablet)
        /// </summary>
        [StringValue("Generic category branding normal (tablet)")]
        GenericCategoryBrandingTabletNormal = 2012401,

        /// <summary>
        /// Category branding placeholder normal (tablet)
        /// </summary>
        [StringValue("Category branding placeholder normal (tablet)")]
        CategoryBrandingPlaceholderTabletNormal = 2012402,

        /// <summary>
        /// Company list normal (tablet)
        /// </summary>
        [StringValue("Company list normal (tablet)")]
        CompanyListTabletNormal = 2012500,

        /// <summary>
        /// Homepage normal (tablet)
        /// </summary>
        [StringValue("Homepage normal (tablet)")]
        HomepageTabletNormal = 2012600,

        /// <summary>
        /// PointOfInterest list normal (tablet)
        /// </summary>
        [StringValue("PointOfInterest list normal (tablet)")]
        PointOfInterestListTabletNormal = 2012700,

        /// <summary>
        /// PointOfInterest homepage normal (tablet)
        /// </summary>
        [StringValue("PointOfInterest homepage normal (tablet)")]
        PointOfInterestHomepageTabletNormal = 2012800,

        /// <summary>
        /// Map icon normal (tablet)
        /// </summary>
        [StringValue("Map icon normal (tablet)")]
        MapIconTabletNormal = 2012900,

        /// <summary>
        /// PointOfInterest map icon normal (tablet)
        /// </summary>
        [StringValue("PointOfInterest map icon normal (tablet)")]
        PointOfInterestMapIconTabletNormal = 2013000, // gk wrong sequence

        SitePageIconTabletNormal = 2012910,
        SiteFullPageImageTabletNormal = 2012915,
        SiteTwoThirdsPageImageTabletNormal = 2012920,
        SiteHdWideScreenTabletNormal = 2012925,
        SiteCinemaWideScreenTabletNormal = 2012930,
        SiteMobileHeaderImageTabletNormal = 2012935,
        SitePointOfInterestSummaryPageTabletNormal = 2012955,
        SiteCompanySummaryPageTabletNormal = 2012956,
        SiteBrandingTabletNormal = 2012957,

        TemplateSitePageIconTabletNormal = 2013010,

        #endregion

        #region Large (tablet)

        /// <summary>
        /// Product button large (tablet)
        /// </summary>
        [StringValue("Product button large (tablet)")]
        ProductButtonTabletLarge = 2013100,

        /// <summary>
        /// Generic product button large (tablet)
        /// </summary>
        [StringValue("Generic product button large (tablet)")]
        GenericProductButtonTabletLarge = 2013101,

        /// <summary>
        /// Product button placeholder large (tablet)
        /// </summary>
        [StringValue("Product button placeholder large (tablet)")]
        ProductButtonPlaceholderTabletLarge = 2013102,

        /// <summary>
        /// Product branding large (tablet)
        /// </summary>
        [StringValue("Product branding large (tablet)")]
        ProductBrandingTabletLarge = 2013200,

        /// <summary>
        /// Generic product branding large (tablet)
        /// </summary>
        [StringValue("Generic product branding large (tablet)")]
        GenericProductBrandingTabletLarge = 2013201,

        /// <summary>
        /// Product branding placeholder large (tablet)
        /// </summary>
        [StringValue("Product branding placeholder large (tablet)")]
        ProductBrandingPlaceholderTabletLarge = 2013202,

        /// <summary>
        /// Category button large (tablet)
        /// </summary>
        [StringValue("Category button large (tablet)")]
        CategoryButtonTabletLarge = 2013300,

        /// <summary>
        /// Generic category button large (tablet)
        /// </summary>
        [StringValue("Generic category button large (tablet)")]
        GenericCategoryButtonTabletLarge = 2013301,

        /// <summary>
        /// Category button placeholder large (tablet)
        /// </summary>
        [StringValue("Category button placeholder large (tablet)")]
        CategoryButtonPlaceholderTabletLarge = 2013302,

        /// <summary>
        /// Category branding large (tablet)
        /// </summary>
        [StringValue("Category branding large (tablet)")]
        CategoryBrandingTabletLarge = 2013400,

        /// <summary>
        /// Generic category branding large (tablet)
        /// </summary>
        [StringValue("Generic category branding large (tablet)")]
        GenericCategoryBrandingTabletLarge = 2013401,

        /// <summary>
        /// Category branding placeholder large (tablet)
        /// </summary>
        [StringValue("Category branding placeholder large (tablet)")]
        CategoryBrandingPlaceholderTabletLarge = 2013402,

        /// <summary>
        /// Company list large (tablet)
        /// </summary>
        [StringValue("Company list large (tablet)")]
        CompanyListTabletLarge = 2013500,

        /// <summary>
        /// Homepage large (tablet)
        /// </summary>
        [StringValue("Homepage large (tablet)")]
        HomepageTabletLarge = 2013600,

        /// <summary>
        /// PointOfInterest list large (tablet)
        /// </summary>
        [StringValue("PointOfInterest list large (tablet)")]
        PointOfInterestListTabletLarge = 2013700,

        /// <summary>
        /// PointOfInterest homepage large (tablet)
        /// </summary>
        [StringValue("PointOfInterest homepage large (tablet)")]
        PointOfInterestHomepageTabletLarge = 2013800,

        /// <summary>
        /// Map icon large (tablet)
        /// </summary>
        [StringValue("Map icon large (tablet)")]
        MapIconTabletLarge = 2013900,

        /// <summary>
        /// PointOfInterest map icon large (tablet)
        /// </summary>
        [StringValue("PointOfInterest map icon large (tablet)")]
        PointOfInterestMapIconTabletLarge = 2014000, // gk wrong sequence

        SitePageIconTabletLarge = 2013910,
        SiteFullPageImageTabletLarge = 2013915,
        SiteTwoThirdsPageImageTabletLarge = 2013920,
        SiteHdWideScreenTabletLarge = 2013925,
        SiteCinemaWideScreenTabletLarge = 2013930,
        SiteMobileHeaderImageTabletLarge = 2013935,
        SitePointOfInterestSummaryPageTabletLarge = 2013955,
        SiteCompanySummaryPageTabletLarge = 2013956,
        SiteBrandingTabletLarge = 2013957,

        TemplateSitePageIconTabletLarge = 2014010,

        #endregion

        #region XLarge (tablet)

        /// <summary>
        /// Product button xlarge (tablet)
        /// </summary>
        [StringValue("Product button xlarge (tablet)")]
        ProductButtonTabletXLarge = 2014100,

        /// <summary>
        /// Generic product button xlarge (tablet)
        /// </summary>
        [StringValue("Generic product button xlarge (tablet)")]
        GenericProductButtonTabletXLarge = 2014101,

        /// <summary>
        /// Product button placeholder xlarge (tablet)
        /// </summary>
        [StringValue("Product button placeholder xlarge (tablet)")]
        ProductButtonPlaceholderTabletXLarge = 2014102,

        /// <summary>
        /// Product branding xlarge (tablet)
        /// </summary>
        [StringValue("Product branding xlarge (tablet)")]
        ProductBrandingTabletXLarge = 2014200,

        /// <summary>
        /// Generic product branding xlarge (tablet)
        /// </summary>
        [StringValue("Generic product branding xlarge (tablet)")]
        GenericProductBrandingTabletXLarge = 2014201,

        /// <summary>
        /// Product branding placeholder xlarge (tablet)
        /// </summary>
        [StringValue("Product branding placeholder xlarge (tablet)")]
        ProductBrandingPlaceholderTabletXLarge = 2014202,

        /// <summary>
        /// Category button xlarge (tablet)
        /// </summary>
        [StringValue("Category button xlarge (tablet)")]
        CategoryButtonTabletXLarge = 2014300,

        /// <summary>
        /// Generic category button xlarge (tablet)
        /// </summary>
        [StringValue("Generic category button xlarge (tablet)")]
        GenericCategoryButtonTabletXLarge = 2014301,

        /// <summary>
        /// Category button placeholder xlarge (tablet)
        /// </summary>
        [StringValue("Category button placeholder xlarge (tablet)")]
        CategoryButtonPlaceholderTabletXLarge = 2014302,

        /// <summary>
        /// Category branding xlarge (tablet)
        /// </summary>
        [StringValue("Category branding xlarge (tablet)")]
        CategoryBrandingTabletXLarge = 2014400,

        /// <summary>
        /// Generic category branding xlarge (tablet)
        /// </summary>
        [StringValue("Generic category branding xlarge (tablet)")]
        GenericCategoryBrandingTabletXLarge = 2014401,

        /// <summary>
        /// Category branding placeholder xlarge (tablet)
        /// </summary>
        [StringValue("Category branding placeholder xlarge (tablet)")]
        CategoryBrandingPlaceholderTabletXLarge = 2014402,

        /// <summary>
        /// Company list xlarge (tablet)
        /// </summary>
        [StringValue("Company list xlarge (tablet)")]
        CompanyListTabletXLarge = 2014500,

        /// <summary>
        /// Homepage xlarge (tablet)
        /// </summary>
        [StringValue("Homepage xlarge (tablet)")]
        HomepageTabletXLarge = 2014600,

        /// <summary>
        /// PointOfInterest list xlarge (tablet)
        /// </summary>
        [StringValue("PointOfInterest list xlarge (tablet)")]
        PointOfInterestListTabletXLarge = 2014700,

        /// <summary>
        /// PointOfInterest homepage xlarge (tablet)
        /// </summary>
        [StringValue("PointOfInterest homepage xlarge (tablet)")]
        PointOfInterestHomepageTabletXLarge = 2014800,

        /// <summary>
        /// Map icon xlarge (tablet)
        /// </summary>
        [StringValue("Map icon xlarge (tablet)")]
        MapIconTabletXLarge = 2014900,

        /// <summary>
        /// PointOfInterest map icon xlarge (tablet)
        /// </summary>
        [StringValue("PointOfInterest map icon xlarge (tablet)")]
        PointOfInterestMapIconTabletXLarge = 2015000,

        SitePageIconTabletXLarge = 2014910,
        SiteFullPageImageTabletXLarge = 2014915,
        SiteTwoThirdsPageImageTabletXLarge = 2014920,
        SiteHdWideScreenTabletXLarge = 2014925,
        SiteCinemaWideScreenTabletXLarge = 2014930,
        SiteMobileHeaderImageTabletXLarge = 2014935,
        SitePointOfInterestSummaryPageTabletXLarge = 2014955,
        SiteCompanySummaryPageTabletXLarge = 2014956,
        SiteBrandingTabletXLarge = 2014957,

        TemplateSitePageIconTabletXLarge = 2015010,

        #endregion

        #region iPhone

        /// <summary>
        /// Product button iPhone
        /// </summary>
        [StringValue("Product button iPhone")]
        ProductButtoniPhone = 3011100,

        /// <summary>
        /// Generic product button iPhone
        /// </summary>
        [StringValue("Generic product button iPhone")]
        GenericProductButtoniPhone = 3011101,

        /// <summary>
        /// Product button placeholder iPhone
        /// </summary>
        [StringValue("Product button placeholder iPhone")]
        ProductButtonPlaceholderiPhone = 3011102,

        /// <summary>
        /// Product branding iPhone
        /// </summary>
        [StringValue("Product branding iPhone")]
        ProductBrandingiPhone = 3011200,

        /// <summary>
        /// Generic product branding iPhone
        /// </summary>
        [StringValue("Generic product branding iPhone")]
        GenericProductBrandingiPhone = 3011201,

        /// <summary>
        /// Product branding placeholder iPhone
        /// </summary>
        [StringValue("Product branding placeholder iPhone")]
        ProductBrandingPlaceholderiPhone = 3011202,

        /// <summary>
        /// Category button iPhone
        /// </summary>
        [StringValue("Category button iPhone")]
        CategoryButtoniPhone = 3011300,

        /// <summary>
        /// Generic category button iPhone
        /// </summary>
        [StringValue("Generic category button iPhone")]
        GenericCategoryButtoniPhone = 3011301,

        /// <summary>
        /// Category button placeholder iPhone
        /// </summary>
        [StringValue("Category button placeholder iPhone")]
        CategoryButtonPlaceholderiPhone = 3011302,

        /// <summary>
        /// Category branding iPhone
        /// </summary>
        [StringValue("Category branding iPhone")]
        CategoryBrandingiPhone = 3011400,

        /// <summary>
        /// Generic category branding iPhone
        /// </summary>
        [StringValue("Generic category branding iPhone")]
        GenericCategoryBrandingiPhone = 3011401,

        /// <summary>
        /// Category branding placeholder iPhone
        /// </summary>
        [StringValue("Category branding placeholder iPhone")]
        CategoryBrandingPlaceholderiPhone = 3011402,

        /// <summary>
        /// Alteration Dialog Page iPhone
        /// </summary>
        [StringValue("Alteration dialog page iPhone")]
        AlterationDialogPageiPhone = 3011403,

        /// <summary>
        /// Company list iPhone
        /// </summary>
        [StringValue("Company list iPhone")]
        CompanyListiPhone = 3011500,

        /// <summary>
        /// Deliverypointgroup list iPhone
        /// </summary>
        [StringValue("Deliverypointgroup list iPhone")]
        DeliverypointgroupListiPhone = 3011501,

        /// <summary>
        /// Homepage iPhone
        /// </summary>
        [StringValue("Homepage iPhone")]
        HomepageiPhone = 3011600,

        /// <summary>
        /// PointOfInterest list iPhone
        /// </summary>
        [StringValue("PointOfInterest list iPhone")]
        PointOfInterestListiPhone = 3011700,

        /// <summary>
        /// PointOfInterest homepage iPhone
        /// </summary>
        [StringValue("Homepage iPhone")]
        PointOfInterestHomepageiPhone = 3011800,

        /// <summary>
        /// Map icon iPhone
        /// </summary>
        [StringValue("Map icon iPhone")]
        MapIconiPhone = 3011900,

        /// <summary>
        /// PointOfInterest map icon iPhone
        /// </summary>
        [StringValue("PointOfInterest map icon iPhone")]
        PointOfInterestMapIconiPhone = 3012000,

        SitePageIconiPhone = 3011910,
        //SiteFullPageImageiPhone = 3011915,
        //SiteTwoThirdsPageImageiPhone = 3011920,
        //SiteHdWideScreeniPhone = 301125,
        //SiteCinemaWideScreeniPhone = 3011930,
        SiteMobileHeaderImageiPhone = 3011935,
        SitePointOfInterestSummaryPageiPhone = 3011955,
        SiteCompanySummaryPageiPhone = 3011956,
        SiteBrandingiPhone = 3011957,

        TemplateSitePageIconiPhone = 3012010,
        TemplateSiteMobileHeaderImageiPhone = 3012035,

        AttachmentThumbnailiPhone = 3012020,

        #endregion

        #region iPhone Retina

        /// <summary>
        /// Product button iPhone Retina
        /// </summary>
        [StringValue("Product button iPhone Retina")]
        ProductButtoniPhoneRetina = 3012100,

        /// <summary>
        /// Generic product button iPhone Retina
        /// </summary>
        [StringValue("Generic product button iPhone Retina")]
        GenericProductButtoniPhoneRetina = 3012101,

        /// <summary>
        /// Product button placeholder iPhone Retina
        /// </summary>
        [StringValue("Product button placeholder iPhone Retina")]
        ProductButtonPlaceholderiPhoneRetina = 3012102,

        /// <summary>
        /// Product branding iPhone Retina
        /// </summary>
        [StringValue("Product branding iPhone Retina")]
        ProductBrandingiPhoneRetina = 3012200,

        /// <summary>
        /// Generic product branding iPhone Retina
        /// </summary>
        [StringValue("Generic product branding iPhone Retina")]
        GenericProductBrandingiPhoneRetina = 3012201,

        /// <summary>
        /// Product branding placeholder iPhone Retina
        /// </summary>
        [StringValue("Product branding placeholder iPhone Retina")]
        ProductBrandingPlaceholderiPhoneRetina = 3012202,

        /// <summary>
        /// Category button iPhone Retina
        /// </summary>
        [StringValue("Category button iPhone Retina")]
        CategoryButtoniPhoneRetina = 3012300,

        /// <summary>
        /// Generic category button iPhone Retina
        /// </summary>
        [StringValue("Generic category button iPhone Retina")]
        GenericCategoryButtoniPhoneRetina = 3012301,

        /// <summary>
        /// Category button placeholder iPhone Retina
        /// </summary>
        [StringValue("Category button placeholder iPhone Retina")]
        CategoryButtonPlaceholderiPhoneRetina = 3012302,

        /// <summary>
        /// Category branding iPhone Retina
        /// </summary>
        [StringValue("Category branding iPhone Retina")]
        CategoryBrandingiPhoneRetina = 3012400,

        /// <summary>
        /// Generic category branding iPhone Retina
        /// </summary>
        [StringValue("Generic category branding iPhone Retina")]
        GenericCategoryBrandingiPhoneRetina = 3012401,

        /// <summary>
        /// Category branding placeholder iPhone Retina
        /// </summary>
        [StringValue("Category branding placeholder iPhone Retina")]
        CategoryBrandingPlaceholderiPhoneRetina = 3012402,

        /// <summary>
        /// Alteration dialog page iPhone Retina
        /// </summary>
        [StringValue("Alteration dialog page iPhone Retina")]
        AlterationDialogPageiPhoneRetina = 3012403,

        /// <summary>
        /// Company list iPhone Retina
        /// </summary>
        [StringValue("Company list iPhone Retina")]
        CompanyListiPhoneRetina = 3012500,

        /// <summary>
        /// Deliverypointgroup list iPhone Retina
        /// </summary>
        [StringValue("Deliverypointgroup list iPhone Retina")]
        DeliverypointgroupListiPhoneRetina = 3012501,

        /// <summary>
        /// Homepage iPhone Retina
        /// </summary>
        [StringValue("Homepage iPhone Retina")]
        HomepageiPhoneRetina = 3012600,

        /// <summary>
        /// PointOfInterest list iPhone Retina
        /// </summary>
        [StringValue("PointOfInterest list iPhone Retina")]
        PointOfInterestListiPhoneRetina = 3012700,

        /// <summary>
        /// PointOfInterest homepage iPhone Retina
        /// </summary>
        [StringValue("PointOfInterest homepage iPhone Retina")]
        PointOfInterestHomepageiPhoneRetina = 3012800,

        /// <summary>
        /// Map icon iPhone Retina
        /// </summary>
        [StringValue("Map icon iPhone Retina")]
        MapIconiPhoneRetina = 3012900,

        /// <summary>
        /// PointOfInterest map icon iPhone Retina
        /// </summary>
        [StringValue("PointOfInterest map icon iPhone Retina")]
        PointOfInterestMapIconiPhoneRetina = 3013000,

        SitePageIconiPhoneRetina = 3012910,
        //SiteFullPageImageiPhoneRetina = 3012915,
        //SiteTwoThirdsPageImageiPhoneRetina = 3012920,
        //SiteHdWideScreeniPhoneRetina = 301225,
        //SiteCinemaWideScreeniPhoneRetina = 3012930,
        SiteMobileHeaderImageiPhoneRetina = 3012935,
        SitePointOfInterestSummaryPageiPhoneRetina = 3012955,
        SiteCompanySummaryPageiPhoneRetina = 3012956,
        SiteBrandingiPhoneRetina = 3012957,

        TemplateSitePageIconiPhoneRetina = 3013010,
        TemplateSiteMobileHeaderImageiPhoneRetina = 3013035,

        AttachmentThumbnailiPhoneRetina = 3013020,

        #endregion

        #region iPad

        /// <summary>
        /// Product button iPad
        /// </summary>
        [StringValue("Product button iPad")]
        ProductButtoniPad = 3013100,

        /// <summary>
        /// Generic product button iPad
        /// </summary>
        [StringValue("Generic product button iPad")]
        GenericProductButtoniPad = 3013101,

        /// <summary>
        /// Product button placeholder iPad
        /// </summary>
        [StringValue("Product button placeholder iPad")]
        ProductButtonPlaceholderiPad = 3013102,

        /// <summary>
        /// Product branding iPad
        /// </summary>
        [StringValue("Product branding iPad")]
        ProductBrandingiPad = 3013200,

        /// <summary>
        /// Generic product branding iPad
        /// </summary>
        [StringValue("Generic product branding iPad")]
        GenericProductBrandingiPad = 3013201,

        /// <summary>
        /// Product branding placeholder iPad
        /// </summary>
        [StringValue("Product branding placeholder iPad")]
        ProductBrandingPlaceholderiPad = 3013202,

        /// <summary>
        /// Category button iPad
        /// </summary>
        [StringValue("Category button iPad")]
        CategoryButtoniPad = 3013300,

        /// <summary>
        /// Generic category button iPad
        /// </summary>
        [StringValue("Generic category button iPad")]
        GenericCategoryButtoniPad = 3013301,

        /// <summary>
        /// Category button placeholder iPad
        /// </summary>
        [StringValue("Category button placeholder iPad")]
        CategoryButtonPlaceholderiPad = 3013302,

        /// <summary>
        /// Category branding iPad
        /// </summary>
        [StringValue("Category branding iPad")]
        CategoryBrandingiPad = 3013400,

        /// <summary>
        /// Generic category branding iPad
        /// </summary>
        [StringValue("Generic category branding iPad")]
        GenericCategoryBrandingiPad = 3013401,

        /// <summary>
        /// Category branding placeholder iPad
        /// </summary>
        [StringValue("Category branding placeholder iPad")]
        CategoryBrandingPlaceholderiPad = 3013402,

        /// <summary>
        /// Alteration dialog page iPad
        /// </summary>
        [StringValue("Alteration dialog page iPad")]
        AlterationDialogPageiPad = 3013403,

        /// <summary>
        /// Company list iPad
        /// </summary>
        [StringValue("Company list iPad")]
        CompanyListiPad = 3013500,

        /// <summary>
        /// Deliverypointgroup list iPad
        /// </summary>
        [StringValue("Deliverypointgroup list iPad")]
        DeliverypointgroupListiPad = 3013501,

        /// <summary>
        /// Homepage iPad
        /// </summary>
        [StringValue("Homepage iPad")]
        HomepageiPad = 3013600,

        /// <summary>
        /// PointOfInterest list iPad
        /// </summary>
        [StringValue("PointOfInterest list iPad")]
        PointOfInterestListiPad = 3013700,

        /// <summary>
        /// PointOfInterest homepage iPad
        /// </summary>
        [StringValue("PointOfInterest homepage iPad")]
        PointOfInterestHomepageiPad = 3013800,

        /// <summary>
        /// Map icon iPad
        /// </summary>
        [StringValue("Map icon iPad")]
        MapIconiPad = 3013900,

        /// <summary>
        /// PointOfInterest map icon iPad
        /// </summary>
        [StringValue("PointOfInterest map icon iPad")]
        PointOfInterestMapIconiPad = 3014000,

        /// <summary>
        /// Homepage2 iPad 
        /// </summary>
        [StringValue("Homepage2 iPad")]
        Homepage2iPad = 3014001,

        /// <summary>
        /// Homepage3 iPad 
        /// </summary>
        [StringValue("Homepage3 iPad")]
        Homepage3iPad = 3014002,

        /// <summary>
        /// Homepage advert iPad 
        /// </summary>
        [StringValue("HomepageAdvert iPad")]
        HomepageAdvertiPad = 3014202,

        SitePageIconiPad = 3013910,
        SiteFullPageImageiPad = 3013915,
        SiteTwoThirdsPageImageiPad = 3013920,
        SiteHdWideScreeniPad = 3013925,
        SiteCinemaWideScreeniPad = 3013930,
        //SiteMobileHeaderImageiPad = 3013935,
        SitePointOfInterestSummaryPageiPad = 3013955,
        SiteCompanySummaryPageiPad = 3013956,
        SiteBrandingiPad = 3013957,

        TemplateSitePageIconiPad = 3014010,
        TemplateSiteFullPageImageiPad = 3014015,
        TemplateSiteTwoThirdsPageImageiPad = 3014020,
        TemplateSiteCinemaWideScreeniPad = 3014030,

        AttachmentThumbnailiPad = 3014040,

        #endregion

        #region iPad Retina

        /// <summary>
        /// Product button iPad Retina
        /// </summary>
        [StringValue("Product button iPad Retina")]
        ProductButtoniPadRetina = 3014100,

        /// <summary>
        /// Generic product button iPad Retina
        /// </summary>
        [StringValue("Generic product button iPad Retina")]
        GenericProductButtoniPadRetina = 3014101,

        /// <summary>
        /// Product button placeholder iPad Retina
        /// </summary>
        [StringValue("Product button placeholder iPad Retina")]
        ProductButtonPlaceholderiPadRetina = 3014102,

        /// <summary>
        /// Product branding iPad Retina
        /// </summary>
        [StringValue("Product branding iPad Retina")]
        ProductBrandingiPadRetina = 3014200,

        /// <summary>
        /// Generic product branding iPad Retina
        /// </summary>
        [StringValue("Generic product branding iPad Retina")]
        GenericProductBrandingiPadRetina = 3014201,

        /// <summary>
        /// Product branding placeholder iPad Retina
        /// </summary>
        [StringValue("Product branding placeholder iPad Retina")]
        ProductBrandingPlaceholderiPadRetina = 3014203,

        /// <summary>
        /// Category button iPad Retina
        /// </summary>
        [StringValue("Category button iPad Retina")]
        CategoryButtoniPadRetina = 3014300,

        /// <summary>
        /// Generic category button iPad Retina
        /// </summary>
        [StringValue("Generic category button iPad Retina")]
        GenericCategoryButtoniPadRetina = 3014301,

        /// <summary>
        /// Category button placeholder iPad Retina
        /// </summary>
        [StringValue("Category button placeholder iPad Retina")]
        CategoryButtonPlaceholderiPadRetina = 3014302,

        /// <summary>
        /// Category branding iPad Retina
        /// </summary>
        [StringValue("Category branding iPad Retina")]
        CategoryBrandingiPadRetina = 3014400,

        /// <summary>
        /// Generic category branding iPad Retina
        /// </summary>
        [StringValue("Generic category branding iPad Retina")]
        GenericCategoryBrandingiPadRetina = 3014401,

        /// <summary>
        /// Category branding placeholder iPad Retina
        /// </summary>
        [StringValue("Category branding placeholder iPad Retina")]
        CategoryBrandingPlaceholderiPadRetina = 3014402,

        /// <summary>
        /// Alteration dialog page iPad Retina
        /// </summary>
        [StringValue("Alteration dialog page iPad Retina")]
        AlterationDialogPageiPadRetina = 3014403,

        /// <summary>
        /// Company list iPad Retina
        /// </summary>
        [StringValue("Company list iPad Retina")]
        CompanyListiPadRetina = 3014500,

        /// <summary>
        /// Deliverypointgroup list iPad Retina
        /// </summary>
        [StringValue("Deliverypointgroup list iPad Retina")]
        DeliverypointgroupListiPadRetina = 3014501,

        /// <summary>
        /// Homepage iPad Retina
        /// </summary>
        [StringValue("Homepage iPad Retina")]
        HomepageiPadRetina = 3014600,

        /// <summary>
        /// PointOfInterest list iPad Retina
        /// </summary>
        [StringValue("PointOfInterest list iPad Retina")]
        PointOfInterestListiPadRetina = 3014700,

        /// <summary>
        /// PointOfInterest homepage iPad Retina
        /// </summary>
        [StringValue("PointOfInterest homepage iPad Retina")]
        PointOfInterestHomepageiPadRetina = 3014800,

        /// <summary>
        /// Map icon iPad Retina
        /// </summary>
        [StringValue("Map icon iPad Retina")]
        MapIconiPadRetina = 3014900,

        /// <summary>
        /// PointOfInterest map icon iPad Retina
        /// </summary>
        [StringValue("PointOfInterest map icon iPad Retina")]
        PointOfInterestMapIconiPadRetina = 3015000,

        /// <summary>
        /// Homepage2 iPad Retina
        /// </summary>
        [StringValue("Homepage2 iPad Retina")]
        Homepage2iPadRetina = 3015001,

        /// <summary>
        /// Homepage Advert iPad Retina
        /// </summary>
        [StringValue("Homepage Advert iPad Retina")]
        HomepageAdvertiPadRetina = 3015002,

        /// <summary>
        /// Homepage3 iPad Retina
        /// </summary>
        [StringValue("Homepage3 iPad Retina")]
        Homepage3iPadRetina = 3015003,

        SitePageIconiPadRetina = 3014910,
        SiteFullPageImageiPadRetina = 3014915,
        SiteTwoThirdsPageImageiPadRetina = 3014920,
        SiteHdWideScreeniPadRetina = 3014925,
        SiteCinemaWideScreeniPadRetina = 3014930,
        //SiteMobileHeaderImageiPadRetina = 3014935,
        SitePointOfInterestSummaryPageiPadRetina = 3014955,
        SiteCompanySummaryPageiPadRetina = 3014956,
        SiteBrandingiPadRetina = 3014957,

        TemplateSitePageIconiPadRetina = 3015010,
        TemplateSiteFullPageImageiPadRetina = 3015015,
        TemplateSiteTwoThirdsPageImageiPadRetina = 3015020,
        TemplateSiteCinemaWideScreeniPadRetina = 3015030,

        AttachmentThumbnailiPadRetina = 3015040,

        #endregion

        #region Non Device Related

        [StringValue("RouteStep Handler Email Resource")]
        RouteStepHandlerEmailResource = 4015000,

        [StringValue("Product Attachment Image")]
        AttachmentImage = 4015010,

        [StringValue("Product Attachment Pdf")]
        AttachmentPdf = 4015015,

        [StringValue("Product Attachment Thumbnail")]
        AttachmentThumbnail = 4015020,

        [StringValue("Download Page Branding")]
        DownloadPageBranding = 4015025,

        #endregion        

        #region iPhone Retina HD

        /// <summary>
        /// Product button iPhone Retina HD
        /// </summary>
        [StringValue("Product button iPhone Retina HD")]
        ProductButtoniPhoneRetinaHD = 5011100,

        /// <summary>
        /// Generic product button iPhone Retina HD
        /// </summary>
        [StringValue("Generic product button iPhone Retina HD")]
        GenericProductButtoniPhoneRetinaHD = 5011101,

        /// <summary>
        /// Product button placeholder iPhone Retina HD
        /// </summary>
        [StringValue("Product button placeholder iPhone Retina HD")]
        ProductButtonPlaceholderiPhoneRetinaHD = 5011102,

        /// <summary>
        /// Product branding iPhone Retina HD
        /// </summary>
        [StringValue("Product branding iPhone Retina HD")]
        ProductBrandingiPhoneRetinaHD = 5011200,

        /// <summary>
        /// Generic product branding iPhone Retina HD
        /// </summary>
        [StringValue("Generic product branding iPhone Retina HD")]
        GenericProductBrandingiPhoneRetinaHD = 5011201,

        /// <summary>
        /// Product branding placeholder iPhone Retina HD
        /// </summary>
        [StringValue("Product branding placeholder iPhone Retina HD")]
        ProductBrandingPlaceholderiPhoneRetinaHD = 5011202,

        /// <summary>
        /// Category button iPhone Retina HD
        /// </summary>
        [StringValue("Category button iPhone Retina HD")]
        CategoryButtoniPhoneRetinaHD = 5011300,

        /// <summary>
        /// Generic category button iPhone Retina HD
        /// </summary>
        [StringValue("Generic category button iPhone Retina HD")]
        GenericCategoryButtoniPhoneRetinaHD = 5011301,

        /// <summary>
        /// Category button placeholder iPhone Retina HD
        /// </summary>
        [StringValue("Category button placeholder iPhone Retina HD")]
        CategoryButtonPlaceholderiPhoneRetinaHD = 5011302,

        /// <summary>
        /// Category branding iPhone Retina HD
        /// </summary>
        [StringValue("Category branding iPhone Retina HD")]
        CategoryBrandingiPhoneRetinaHD = 5011400,

        /// <summary>
        /// Generic category branding iPhone Retina HD
        /// </summary>
        [StringValue("Generic category branding iPhone Retina HD")]
        GenericCategoryBrandingiPhoneRetinaHD = 5011401,

        /// <summary>
        /// Category branding placeholder iPhone Retina HD
        /// </summary>
        [StringValue("Category branding placeholder iPhone Retina HD")]
        CategoryBrandingPlaceholderiPhoneRetinaHD = 5011402,

        /// <summary>
        /// Alteration dialog page iPhone Retina HD
        /// </summary>
        [StringValue("Alteration dialog page iPhone Retina HD")]
        AlterationDialogPageiPhoneRetinaHD = 5011403,

        /// <summary>
        /// Company list iPhone Retina HD
        /// </summary>
        [StringValue("Company list iPhone Retina HD")]
        CompanyListiPhoneRetinaHD = 5011500,

        /// <summary>
        /// Deliverypointgroup list iPhone Retina HD
        /// </summary>
        [StringValue("Deliverypointgroup list iPhone Retina HD")]
        DeliverypointgroupListiPhoneRetinaHD = 5011501,

        /// <summary>
        /// Homepage iPhone Retina HD
        /// </summary>
        [StringValue("Homepage iPhone Retina HD")]
        HomepageiPhoneRetinaHD = 5011600,

        /// <summary>
        /// PointOfInterest list iPhone Retina HD
        /// </summary>
        [StringValue("PointOfInterest list iPhone Retina HD")]
        PointOfInterestListiPhoneRetinaHD = 5011700,

        /// <summary>
        /// PointOfInterest homepage iPhone Retina HD
        /// </summary>
        [StringValue("Homepage iPhone Retina HD")]
        PointOfInterestHomepageiPhoneRetinaHD = 5011800,

        /// <summary>
        /// Map icon iPhone Retina HD
        /// </summary>
        [StringValue("Map icon iPhone Retina HD")]
        MapIconiPhoneRetinaHD = 5011900,

        /// <summary>
        /// PointOfInterest map icon iPhone Retina HD
        /// </summary>
        [StringValue("PointOfInterest map icon iPhone Retina HD")]
        PointOfInterestMapIconiPhoneRetinaHD = 5012000,

        SitePageIconiPhoneRetinaHD = 5011910,
        //SiteFullPageImageiPhoneRetinaHD = 5011915,
        //SiteTwoThirdsPageImageiPhoneRetinaHD = 5011920,
        //SiteHdWideScreeniPhoneRetinaHD = 501125,
        //SiteCinemaWideScreeniPhoneRetinaHD = 5011930,
        SiteMobileHeaderImageiPhoneRetinaHD = 5011935,
        SitePointOfInterestSummaryPageiPhoneRetinaHD = 5011955,
        SiteCompanySummaryPageiPhoneRetinaHD = 5011956,
        SiteBrandingiPhoneRetinaHD = 5011957,

        TemplateSitePageIconiPhoneRetinaHD = 5012010,
        TemplateSiteMobileHeaderImageiPhoneRetinaHD = 5011936,

        AttachmentThumbnailiPhoneRetinaHD = 5012020,

        #endregion

        #region iPhone Retina HD Plus

        /// <summary>
        /// Product button iPhone Retina HD Plus
        /// </summary>
        [StringValue("Product button iPhone Retina HD Plus")]
        ProductButtoniPhoneRetinaHDPlus = 6012100,

        /// <summary>
        /// Generic product button iPhone Retina HD Plus
        /// </summary>
        [StringValue("Generic product button iPhone Retina HD Plus")]
        GenericProductButtoniPhoneRetinaHDPlus = 6012101,

        /// <summary>
        /// Product button placeholder iPhone Retina HD Plus
        /// </summary>
        [StringValue("Product button placeholder iPhone Retina HD Plus")]
        ProductButtonPlaceholderiPhoneRetinaHDPlus = 6012102,

        /// <summary>
        /// Product branding iPhone Retina HD Plus
        /// </summary>
        [StringValue("Product branding iPhone Retina HD Plus")]
        ProductBrandingiPhoneRetinaHDPlus = 6012200,

        /// <summary>
        /// Generic product branding iPhone Retina HD Plus
        /// </summary>
        [StringValue("Generic product branding iPhone Retina HD Plus")]
        GenericProductBrandingiPhoneRetinaHDPlus = 6012201,

        /// <summary>
        /// Product branding placeholder iPhone Retina HD Plus
        /// </summary>
        [StringValue("Product branding placeholder iPhone Retina HD Plus")]
        ProductBrandingPlaceholderiPhoneRetinaHDPlus = 6012202,

        /// <summary>
        /// Category button iPhone Retina HD Plus
        /// </summary>
        [StringValue("Category button iPhone Retina HD Plus")]
        CategoryButtoniPhoneRetinaHDPlus = 6012300,

        /// <summary>
        /// Generic category button iPhone Retina HD Plus
        /// </summary>
        [StringValue("Generic category button iPhone Retina HD Plus")]
        GenericCategoryButtoniPhoneRetinaHDPlus = 6012301,

        /// <summary>
        /// Category button placeholder iPhone Retina HD Plus
        /// </summary>
        [StringValue("Category button placeholder iPhone Retina HD Plus")]
        CategoryButtonPlaceholderiPhoneRetinaHDPlus = 6012302,

        /// <summary>
        /// Category branding iPhone Retina HD Plus
        /// </summary>
        [StringValue("Category branding iPhone Retina HD Plus")]
        CategoryBrandingiPhoneRetinaHDPlus = 6012400,

        /// <summary>
        /// Generic category branding iPhone Retina HD Plus
        /// </summary>
        [StringValue("Generic category branding iPhone Retina HD Plus")]
        GenericCategoryBrandingiPhoneRetinaHDPlus = 6012401,

        /// <summary>
        /// Category branding placeholder iPhone Retina HD Plus
        /// </summary>
        [StringValue("Category branding placeholder iPhone Retina HD Plus")]
        CategoryBrandingPlaceholderiPhoneRetinaHDPlus = 6012402,

        /// <summary>
        /// Alteration dialog page iPhone Retina HD Plus
        /// </summary>
        [StringValue("Alteration dialog page iPhone Retina HD Plus")]
        AlterationDialogPageiPhoneRetinaHDPlus = 6012403,

        /// <summary>
        /// Company list iPhone Retina HD Plus
        /// </summary>
        [StringValue("Company list iPhone Retina HD Plus")]
        CompanyListiPhoneRetinaHDPlus = 6012500,

        /// <summary>
        /// Deliverypointgroup list iPhone Retina HD Plus
        /// </summary>
        [StringValue("Deliverypointgroup list iPhone Retina HD Plus")]
        DeliverypointgroupListiPhoneRetinaHDPlus = 6012501,

        /// <summary>
        /// Homepage iPhone Retina HD Plus
        /// </summary>
        [StringValue("Homepage iPhone Retina HD Plus")]
        HomepageiPhoneRetinaHDPlus = 6012600,

        /// <summary>
        /// PointOfInterest list iPhone Retina HD Plus
        /// </summary>
        [StringValue("PointOfInterest list iPhone Retina HD Plus")]
        PointOfInterestListiPhoneRetinaHDPlus = 6012700,

        /// <summary>
        /// PointOfInterest homepage iPhone Retina HD Plus
        /// </summary>
        [StringValue("PointOfInterest homepage iPhone Retina HD Plus")]
        PointOfInterestHomepageiPhoneRetinaHDPlus = 6012800,

        /// <summary>
        /// Map icon iPhone Retina HD Plus
        /// </summary>
        [StringValue("Map icon iPhone Retina HD Plus")]
        MapIconiPhoneRetinaHDPlus = 6012900,

        /// <summary>
        /// PointOfInterest map icon iPhone Retina HD Plus
        /// </summary>
        [StringValue("PointOfInterest map icon iPhone Retina HD Plus")]
        PointOfInterestMapIconiPhoneRetinaHDPlus = 6013000,

        SitePageIconiPhoneRetinaHDPlus = 6012910,
        SiteMobileHeaderImageiPhoneRetinaHDPlus = 6012935,
        SitePointOfInterestSummaryPageiPhoneRetinaHDPlus = 6012955,
        SiteCompanySummaryPageiPhoneRetinaHDPlus = 6012956,
        SiteBrandingiPhoneRetinaHDPlus = 6012957,

        TemplateSitePageIconiPhoneRetinaHDPlus = 6013010,
        TemplateSiteMobileHeaderImageiPhoneRetinaHDPlus = 6012936,

        AttachmentThumbnailiPhoneRetinaHDPlus = 6013020,

        #endregion

        #region Appless

        #region Appless small

        /// <summary>
        /// AppLess header logo (small)
        /// </summary>
        [StringValue("AppLess header logo (small)")]
		AppLessHeaderLogoSmall = 700001,

        /// <summary>
		/// AppLess hero (small)
		/// </summary>
		[StringValue("AppLess hero (small)")]
        AppLessHeroSmall = 700002,

        /// <summary>
		/// AppLess carousel (small)
		/// </summary>
		[StringValue("AppLess carousel (small)")]
        AppLessCarouselSmall = 700003,

        /// <summary>
		/// AppLess action banner (small)
		/// </summary>
		[StringValue("AppLess action banner (small)")]
        AppLessActionBannerSmall = 700004,

        /// <summary>
        /// AppLess application icon (small)
        /// </summary>
        [StringValue("AppLess application icon (small)")]
        AppLessApplicationIconSmall = 700005,

        /// <summary>
        /// AppLess product banner (small)
        /// </summary>
        [StringValue("AppLess product banner (small)")]
        AppLessProductBannerSmall = 700006,

        /// <summary>
        /// AppLess category banner (small)
        /// </summary>
        [StringValue("AppLess category banner (small)")]
        AppLessCategoryBannerSmall = 700007,

        #endregion

        #region Appless medium

        /// <summary>
        /// AppLess header logo (medium)
        /// </summary>
        [StringValue("AppLess header logo (medium)")]
        AppLessHeaderLogoMedium = 800001,

        /// <summary>
		/// AppLess hero (medium)
		/// </summary>
		[StringValue("AppLess hero (medium)")]
        AppLessHeroMedium = 800002,

        /// <summary>
		/// AppLess carousel (medium)
		/// </summary>
		[StringValue("AppLess carousel (medium)")]
        AppLessCarouselMedium = 800003,

        /// <summary>
		/// AppLess action banner (medium)
		/// </summary>
		[StringValue("AppLess action banner (medium)")]
        AppLessActionBannerMedium = 800004,

        /// <summary>
        /// AppLess application icon (medium)
        /// </summary>
        [StringValue("AppLess application icon (medium)")]
        AppLessApplicationIconMedium = 800005,

        /// <summary>
        /// AppLess product banner (medium)
        /// </summary>
        [StringValue("AppLess product banner (medium)")]
        AppLessProductBannerMedium = 800006,

        /// <summary>
        /// AppLess catgeory banner (medium)
        /// </summary>
        [StringValue("AppLess category banner (medium)")]
        AppLessCategoryBannerMedium = 800007,

        #endregion

        #region Appless large

        /// <summary>
        /// AppLess header logo (large)
        /// </summary>
        [StringValue("AppLess header logo (large)")]
        AppLessHeaderLogoLarge = 900001,

        /// <summary>
		/// AppLess hero (large)
		/// </summary>
		[StringValue("AppLess hero (large)")]
        AppLessHeroLarge = 900002,

        /// <summary>
		/// AppLess carousel (large)
		/// </summary>
		[StringValue("AppLess carousel (large)")]
        AppLessCarouselLarge = 900003,

        /// <summary>
		/// AppLess action banner (large)
		/// </summary>
		[StringValue("AppLess action banner (large)")]
        AppLessActionBannerLarge = 900004,

        /// <summary>
        /// AppLess application icon (large)
        /// </summary>
        [StringValue("AppLess application icon (large)")]
        AppLessApplicationIconLarge = 900005,

        /// <summary>
        /// AppLess product banner (large)
        /// </summary>
        [StringValue("AppLess product banner (large)")]
        AppLessProductBannerLarge = 900006,

        /// <summary>
        /// AppLess category banner (large)
        /// </summary>
        [StringValue("AppLess category banner (large)")]
        AppLessCategoryBannerLarge = 900007

        #endregion

        #endregion
    }
}