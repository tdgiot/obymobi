using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the DeliverypointSaveResult
    /// </summary>
    public enum DeliverypointSaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Failed
        ///// </summary>
        //Failed = 200,

        /// <summary>
        /// NameEmpty
        /// </summary>
        [StringValue("Er is geen naam opgegeven.")]
        NameEmpty = 201,

        /// <summary>
        /// DeliverypointgroupIdEmpty
        /// </summary>
        [StringValue("Er is geen deliverypointgroup id opgegeven.")]
        DeliverypointgroupIdEmpty = 202,

        /// <summary>
        /// DeliverypointgroupIdEmpty
        /// </summary>
        [StringValue("Naam moet eindigen met het nummer van de tafel.")]
        NameAndNumberDontMatch = 203,

        /// <summary>
        /// DeliverypointgroupIdEmpty
        /// </summary>
        [StringValue("Het gekozen PosDeliverypoint is van een ander bedrijf.")]
        PosdeliverypointDoesntBelongToCompany = 204,

        [StringValue("Meerdere Deliverypoints gevonden voor een Posdeliverypoint.")]
        MultipleDeliverypointsForPosdeliverypoint = 205,

        /// <summary>
        /// NameEmpty
        /// </summary>
        [StringValue("Nummer moet een numerieke waarde zijn.")]
        NonIntegerNumbersAreNotSupported = 206,

        /// <summary>
        /// MultipleDeliverypointsWithSameNumber
        /// </summary>
        [StringValue("Nummber moet uniek in deliverypointgroup zijn.")]
        MultipleDeliverypointsWithSameNumber = 207,

        /// <summary>
        /// MultipleDeliverypointsWithSameNumber
        /// </summary>
        [StringValue("De Deliverypointgroep kan niet worden aangepast voor een Deliverypoint dat gebruikt wordt door een Client")]
        DeliverypointGroupCantBeChangedWhenDeliverypointIsLinkedToClients = 208,
        

    }
}
