﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum SystemType : int
    {
        /// <summary>
        /// Crave
        /// </summary>
        [StringValue("Crave")]
        Crave = 1,

        /// <summary>
        /// Otoucho
        /// </summary>
        [StringValue("Otoucho")]
        Otoucho = 2
    }
}
