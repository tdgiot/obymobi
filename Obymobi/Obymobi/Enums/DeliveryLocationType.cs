﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum DeliveryLocationType
    {
        /// <summary>
        /// Inherit from category
        /// </summary>
        [StringValue("Inherit from category")]
        Inherit = 0,

        /// <summary>
        /// Room
        /// </summary>
        [StringValue("Room")]
        Room = 1,

        /// <summary>
        /// No delivery
        /// </summary>
        [StringValue("No delivery")]
        NoDelivery = 999
    }
}
