﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the PaymentSaveResult
    /// </summary>
    public enum PaymentSaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Failed
        ///// </summary>
        //Failed = 200,

        /// <summary>
        /// AmountEmptyOrZero
        /// </summary>
        [StringValue("Er is geen bedrag opgegeven.")]
        AmountEmptyOrZero = 201,

        /// <summary>
        /// Double TransactionId for PaymentmethodType
        /// </summary>
        [StringValue("Dubbele transactie Id voor betaalmethode.")]
        DoubleTransactionIdForPaymentmethodType = 202,

        /// <summary>
        /// The order alread has a pending payment, first cancel that.
        /// </summary>
        [StringValue("Meerdere betaling niet toegestaan per order.")]
        MultiplePaymentsPerOrderNotallowed = 203,

        /// <summary>
        /// The amount paid is not the same as the vlaue of the order
        /// </summary>
        [StringValue("Het bedrag is niet gelijk aan de waarde van de order.")]
        AmountIsNotSameAsOrderValue = 204,

        /// <summary>
        /// The order is already paid
        /// </summary>
        [StringValue("De order is al betaald.")]
        OrderIsAlreadyPaid = 205,

        /// <summary>
        /// No currency is supplied
        /// </summary>
        [StringValue("Er is geen valuta opgegeven.")]
        CurrencyIdMissing = 206,

        [StringValue("Betaalde betalingen kunnen niet worden gewijzigd.")]
        ChangeNotAllowedToPaidPayment = 207,

        [StringValue("Betaalde betalingen kunnen niet worden verwijderd.")]
        DeleteNotAllowedToPaidPayment = 208
    }
}
