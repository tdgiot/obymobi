﻿namespace Obymobi.Enums
{
    public enum ActionButtonIconType
    {
        None = 0,
        CaretLeft = 1,
        CaretRight = 2,
        Search = 3,
        Exit = 4,
        Phone = 5,
        Food = 6,
        Info = 7,
        Weather = 8,
        Translation = 9,
        // TODO map disabled till icon is available in the front-end.
        // Map = 10
    }
}
