using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum UrlProtocol : int
	{
        /// <summary>
        /// Default i.e. use protocol that is set on NOC service
        /// </summary>
        Default = 0,

        /// <summary>
        /// Force HTTP protocol
        /// </summary>
        Http = 1,

        /// <summary>
        /// Force HTTPS protocol
        /// </summary>
        Https = 2
	}
}
