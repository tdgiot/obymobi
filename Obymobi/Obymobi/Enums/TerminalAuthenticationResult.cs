﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the result of the terminal authentication process
    /// </summary>
    public enum TerminalAuthenticationResult : int
    {
        /// <summary>
        /// Authentication result is unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// User was successfully authenticated
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Terminal was not found in the database 
        /// </summary>
        [StringValue("De opgegeven terminal is niet gevonden.")]
        TerminalUnknown = 200,

        /// <summary>
        /// Terminal was not related to the company
        /// </summary>
        [StringValue("De opgegeven terminal is van een ander bedrijf.")]
        TerminalFromOtherCompany = 201
    }
}
