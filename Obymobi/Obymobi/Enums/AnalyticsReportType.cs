﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum AnalyticsReportType
    {
        [StringValue("Transactions")]
        Transactions = 1,
        [StringValue("Page views")]
        PageViews = 2,
        [StringValue("Marketing")]
        Marketing = 3,
        [StringValue("Broken links")]
        BrokenLinks = 4,
        [StringValue("Wake up calls")]
        WakeUpCalls = 5,
        [StringValue("Batch")]
        Batch = 6,
        [StringValue("Product overview")]
        ProductOverview = 7,
        [StringValue("Kpi")]
        Kpi = 8,
        [StringValue("Transactions AppLess")]
        TransactionsAppLess = 9,
        [StringValue("Product Sales Report")]
        ProductSales = 10,
        [StringValue("Inventory Report")]
        Inventory = 11

    }
}
