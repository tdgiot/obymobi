﻿using Dionysos;
namespace Obymobi.Enums
{
    public enum DeviceModel
    {
        [StringValue("None")]
        Unknown = 0,

        [StringValue("Archos")]
        Archos70 = 2,
        [StringValue("Samsung Galaxy 10.1 Tab")]
        SamsungP4Wifi = 3,
        [StringValue("Sony Xperia Z")]
        SonySGP311 = 4,
        [StringValue("Samsung Galaxy 10.1 Tab2")]
        SamsungP5110 = 5,
        [StringValue("Sony Xperia Z2")]
        SonySGP511 = 6,
        [StringValue("Android TV Box")]
        AndroidTvBox = 30,
        [StringValue("Crave Tab 10.1")]
        MtkEsky27 = 50,
        [StringValue("Crave Tab 10.2")]
        IntelAnzhen4 = 51,
        [StringValue("Crave T-Mini")]
        RockchipRK312x = 70,
		[StringValue("Crave T-Mini V2")]
        RockchipRK312x_v2 = 71,
        [StringValue("Crave T3")]
        RockchipRK3566_10inch = 80,
        [StringValue("Crave T3-Mini")]
        RockchipRK3566_8inch = 90
    }
}
