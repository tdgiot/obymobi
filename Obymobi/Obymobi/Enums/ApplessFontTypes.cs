﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum ApplessFontTypes : int
    {
        [StringValue("Serif")]
        Serif = 0,

        [StringValue("Sans-Serif")]
        SansSerif = 1
    }
}
