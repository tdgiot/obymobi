using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents the GenericycategorySaveResult
	/// </summary>
	public enum GenericcategorySaveResult : int
	{
		/// <summary>
		/// Unknown
		/// </summary>
        [StringValue("Onbekend.")]
		Unknown = 0,

		/// <summary>
		/// Success
		/// </summary>
        [StringValue("Succes.")]
		Success = 100,

        ///// <summary>
        ///// Failed
        ///// </summary>
        //Failed = 200,

        /// <summary>
        /// NameEmpty
        /// </summary>
        [StringValue("Er is geen naam opgegeven.")]
        NameEmpty = 201,

        /// <summary>
        /// CircularReference
        /// </summary>
        [StringValue("Er generieke categorie bevat een cirkelverwijzing naar zichzelf.")]
        CircularReference = 202,

        /// <summary>
        /// SelfReference
        /// </summary>
        [StringValue("De categorie verwijst naar zichzelf.")]
        SelfReference = 203
	}
}
