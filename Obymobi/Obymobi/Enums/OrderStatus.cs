﻿using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// This should only indicate the status of the order, not the cause of a certain (failed) status.
    /// (In the past this enum combined status & caused, this is now moved to Orderroutestephandlers 
    /// and .ErrorCode on the Order which is populated based on OrderProcessingError enum.
    /// </summary>
    public enum OrderStatus
    {
        /// <summary>
        /// Order status is unknown
        /// </summary>
        [StringValue("Onbekend.")]
        NotRouted = 0,

        /// <summary>
        /// Waiting for the order to be paid by the customer
        /// </summary>
        [StringValue("Waiting for payment.")]
        WaitingForPayment = 50,

        /// <summary>
        /// The order has been routed, waiting for the RoutingHelper to Start it.
        /// </summary>
        [StringValue("Route is weggeschreven, afwachting eerste van 'Start Route' command via RoutingHelper.")]
        Routed = 150,

        /// <summary>
        /// The order has been routed, and the steps have started being processed
        /// </summary>
        [StringValue("Route is gestart, eerste afhandelaar is begonnen.")]
        InRoute = 175,

        /// <summary>
        /// Wait for the order to be paid for by the customer
        /// </summary>
        [StringValue("Wacht op betaling.")]
        WaitingForPaymentCompleted = 301,

        /// <summary>
        /// The order is processed = done
        /// </summary>
        [StringValue("Verwerkt.")]
        Processed = 1000,

        ///// <summary>
        ///// http://www.youtube.com/watch?v=SiMHTK15Pik
        ///// </summary>
        //[StringValue("IT'S OVER NINE THOUSAND!")]
        //Over9000 = 9001,

        /// <summary>
        /// The order cannot be processed
        /// </summary>
        [StringValue("The order cannot be processed")]
        Unprocessable = 9999
    }
}
