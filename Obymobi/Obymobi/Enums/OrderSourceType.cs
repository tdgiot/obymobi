﻿using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Keep this file synchronized with Android enum CraveBase/logic/enums/OrderSource.java
    /// </summary>
    public enum OrderSourceType
    {
        [StringValue("Unknown")]
        Unknown = 0,
        [StringValue("Normal")]
        Normal = 1,
        [StringValue("Suggestion")]
	    Suggestion = 2,
        [StringValue("Advertisement")]
	    Advertisement = 3,
        [StringValue("Advertisement Homepage")]
	    AdvertisementHomepage = 4,
        [StringValue("Advertisement Homepage Slideshow")]
        AdvertisementHomepageSlideshow = 5,
        [StringValue("Advertisement Message")]
        AdvertisementMessage = 6
    }
}
