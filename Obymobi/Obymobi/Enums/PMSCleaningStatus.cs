﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum PMSCleaningStatus : int
    {
        [StringValue("Unknown")]
        Unknown = -1,
        [StringValue("Dirty/Vacant")]
        DirtyVacant = 1,
        [StringValue("Dirty/Occupied")]
        DirtyOccupied = 2,
        [StringValue("Clean/Vacant")]
        CleanVacant = 3,
        [StringValue("Clean/Occupied")]
        CleanOccupied = 4,
        [StringValue("InspectedVacant")]
        InspectedVacant = 5,
        [StringValue("InspectedOccupied")]
        InspectedOccupied = 6,
    }
}
