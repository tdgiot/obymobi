using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents the type of the category
	/// </summary>
	public enum CategoryType : int
	{
		/// <summary>
		/// Type is unknown
		/// </summary>
        [StringValue("Inherit from parent.")]
		Inherit = 0,

		/// <summary>
        /// Type is normal
		/// </summary>
        [StringValue("Normaal.")]
		Normal = 1,

        /// <summary>
        /// Type is service items
        /// </summary>
        [StringValue("Service items.")]
        ServiceItems = 2,

        /// <summary>
        /// Type is directory
        /// </summary>
        [StringValue("Directory.")]
        Directory = 3
	}
}
