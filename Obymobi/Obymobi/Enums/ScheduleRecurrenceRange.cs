using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum ScheduleRecurrenceRange : int
	{
        NoEndDate = 0,
        OccurrenceCount = 1,
        EndByDate = 2
	}
}
