﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum TerminalState : int 
	{
		/// <summary>
		/// Online 
		/// </summary>
        [StringValue("Online.")]
		Online = 100,		

		/// <summary>
		/// Stopped, offline but predictable since it was closed
		/// </summary>
        [StringValue("Offline na sluiting.")]
		OfflineByStopped = 200,

		/// <summary>
		/// Offline, meaning it should be online but 
		/// </summary>
        [StringValue("Offline na storing.")]
		OfflineByFailure = 300,

		/// <summary>
		/// Unknown Error
		/// </summary>
        [StringValue("Onbekend.")]
		Unknown = 301,

		/// <summary>
		/// Unknown Error
		/// </summary>
        [StringValue("De opgegeven terminal id is onbekend.")]
		TerminalIdIncorrect = 302,

		/// <summary>
		/// Offline, meaning it should be online but 
		/// </summary>
        [StringValue("Offline en nooit opgestart.")]
		OfflineNeverStarted = 303,
	}
}
