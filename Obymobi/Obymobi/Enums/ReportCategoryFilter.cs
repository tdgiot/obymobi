using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum ReportCategoryFilter : int
	{
        AllCategories = 0,
        IncludeCategories = 1,
        ExcludeCategories = 2,
	}
}
