using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum BenchmarkSource
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown,

        /// <summary>
        /// Advertisement
        /// </summary>
        Advertisement,
        
        /// <summary>
        /// Homepage
        /// </summary>
        Homepage,

        /// <summary>
        /// Menu
        /// </summary>
        Menu,

        /// <summary>
        /// Browse
        /// </summary>
        Browse,

        /// <summary>
        /// Suggestion
        /// </summary>
        Suggestion
    }
}
