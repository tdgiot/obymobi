﻿namespace Obymobi.Enums
{
    public enum OrderNotificationLogStatus
    {
        Pending = 10,

        Success = 1000,
        SuccessWithErrors = 10001,

        Failed = 99999,
    }
}