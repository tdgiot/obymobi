﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum SynchronizeProductAlterationsResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Multiple product alterationss found for the specified external id
        /// </summary>
        [StringValue("Er zijn meerdere product alterations gevonden voor de opgegeven pos product alteration id.")]
        MultipleProductAlterationsFound = 200
    }
}
