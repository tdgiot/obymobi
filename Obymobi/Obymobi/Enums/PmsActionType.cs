using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum PmsActionType : int
	{
        /// <summary>
        /// Add to messagegroup
        /// </summary>
        AddToMessagegroup = 0,
        /// <summary>
        /// SendMessage
        /// </summary>
        SendMessage = 1,
        /// <summary>
        /// Change client configuration
        /// </summary>
        ChangeClientConfiguration = 2
    }
}
