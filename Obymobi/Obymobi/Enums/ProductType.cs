﻿namespace Obymobi.Enums
{
    public enum ProductType : int
    {
        /// <summary>
        /// Product
        /// </summary>
        Product = 200,

        /// <summary>
        /// Service
        /// </summary>
        Service = 300,

        /// <summary>
        /// Paymentmethod
        /// </summary>
        Paymentmethod = 400,

        /// <summary>
        /// Deliverypoint
        /// </summary>
        Deliverypoint = 500,

        /// <summary>
        /// Tablet
        /// </summary>
        Tablet = 600,

        /// <summary>
        /// Category
        /// </summary>
        Category = 700,

        /// <summary>
        /// Category
        /// </summary>
        PosServiceMessage = 800,

        /// <summary>
        /// Email document
        /// </summary>
        Document = 900,

        /// <summary>
        /// Pms checkout
        /// </summary>
        PmsCheckout = 1000,

        /// <summary>
        /// Brand Product
        /// </summary>
        BrandProduct = 1100,
    }
}
