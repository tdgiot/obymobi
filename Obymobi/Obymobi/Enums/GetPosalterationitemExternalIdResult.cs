using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum GetPosalterationitemExternalIdResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// No pos alteration item found for the specified id
        /// </summary>
        [StringValue("Er is geen pos alteration items gevonden voor het opgegeven alteration item id.")]
        NoPosalterationitemFound = 200,

        /// <summary>
        /// Multiple pos alteration items found
        /// </summary>
        [StringValue("Er zijn meerdere pos alteration items gevonden voor het opgegeven alteration item id.")]
        MultiplePosalterationitemsFound = 201
    }
}
