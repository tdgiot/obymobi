using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum ScheduleRecurrenceType : int
	{
        Daily = 0,
        Weekly = 1,
        Monthly = 2,
        Yearly = 3,
        Minutely = 4,
        Hourly = 5
	}
}
