﻿namespace Obymobi.Enums
{
    public enum OnSiteServerWebserviceJobType
    {
        PmsCheckedIn = 1,
        PmsGetGuestInformation = 2,
        PmsWakeUpSet = 3,
        PmsWakeUpCleared = 4
    }
}