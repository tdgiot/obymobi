﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the PaymentmethodSaveResult
    /// </summary>
    public enum PaymentmethodSaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Failed
        ///// </summary>
        //[StringValue("")]
        //Failed = 200,

        /// <summary>
        /// NameEmpty
        /// </summary>
        [StringValue("Er is geen naam opgegeven.")]
        NameEmpty = 201,

        /// <summary>
        /// OnlyOnePaymentmethodPerPaymentmethodTypeAllowed
        /// </summary>
        [StringValue("Er mag maar één paymentmethod per paymentmethodtype in de database zitten.")]
        OnlyOnePaymentmethodPerPaymentmethodTypeAllowed = 202,

        /// <summary>
        /// OnlyOnePaymentmethodPerPaymentmethodTypeAllowed
        /// </summary>
        [StringValue("Indien er voor de Paymentmethod al betalingen (Payments) zijn gedaan mag hij niet worden gewijzigd.")]
        PaymentmethodCantBeChangedWhenPaymentsAreRelated = 203,

        /// <summary>
        /// OnlyOnePaymentmethodPerPaymentmethodTypeAllowed
        /// </summary>
        [StringValue("Indien er voor de Paymentmethod al betalingen (Payments) zijn gedaan mag hij niet worden verwijderd.")]
        PaymentmethodCantBeDeletedWhenPaymentsAreRelated = 204,

    }
}