using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum RouteType : int
	{
        /// <summary>
        /// Normal route.
        /// </summary>
        Normal = 0,

        /// <summary>
        /// System message route.
        /// </summary>
        SystemMessage = 1,

        /// <summary>
        /// Order notes route.
        /// </summary>
        OrderNotes = 2,

        /// <summary>
        /// Email document route.
        /// </summary>
        EmailDocument = 3
    }
}
