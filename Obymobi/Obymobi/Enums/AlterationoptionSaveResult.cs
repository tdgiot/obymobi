using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum AlterationoptionSaveResult
    {
        /// <summary>
        /// User was successfully authenticated
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// User was successfully authenticated
        /// </summary>
        [StringValue("Alterationoption is linked to a Posproduct but the Posproduct is not linked to a product.")]
        LinkedToPosproductNotLinkedToExactlyOneProduct = 200,

    }
}
