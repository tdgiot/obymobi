﻿using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// When changing anything in this enum you also have to change it in Product.aspx.cs (CMS)
    /// </remarks>
    public enum ProductSubType
    {
        /// <summary>
        /// Type is unknown
        /// </summary>
        [StringValue("Inherit from category")]
        Inherit = 0,

        /// <summary>
        /// Type is normal
        /// </summary>
        [StringValue("Normaal.")]
        Normal = 1,

        /// <summary>
        /// Type is service items
        /// </summary>
        [StringValue("Service items.")]
        ServiceItems = 2,

        /// <summary>
        /// Type is directory
        /// </summary>
        [StringValue("Directory.")]
        Directory = 3,

        /// <summary>
        /// Type is Web Link
        /// </summary>
        [StringValue("WebLink.")]
        WebLink = 4,

        /// <summary>
        /// Type is a system related product.
        /// </summary>
        [StringValue("System product")]
        SystemProduct = 5
    }
}
