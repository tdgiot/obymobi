using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents the type of the cloud storage account
	/// </summary>
	public enum CloudStorageAccountType : int
	{
		/// <summary>
        /// Type is amazon
		/// </summary>
        [StringValue("Amazon")]
		Amazon = 0,

        // First we'll work amazon
        ///// <summary>
        ///// Type is azure
        ///// </summary>
        //[StringValue("Azure")]
        //Azure = 1
	}
}
