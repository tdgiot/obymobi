using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum CustomerSaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Failed
        /// </summary>
        [StringValue("Er is een fout opgetreden bij opslaan van je account. (200)")]
        Failed = 200,

        /// <summary>
        /// PhonenumberAlreadyInUse
        /// </summary>
        [StringValue("Er bestaat al een account voor het opgegeven telefoonnummer.")]
        PhonenumberAlreadyInUse = 201,

        /// <summary>
        /// PhonenumberEmpty
        /// </summary>
        [StringValue("Er is geen telefoonnummer opgegeven.")]
        PhonenumberEmpty = 202,

        /// <summary>
        /// PasswordEmpty
        /// </summary>
        [StringValue("Er is geen pincode ingegeven.")]
        PasswordEmpty = 203,

        /// <summary>
        /// PhonenumberEmpty
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de SMS.")]
        SmsNotSent = 304,

        /// <summary>
        /// PasswordEmpty
        /// </summary>
        [StringValue("Het opgegeven telefoonnummer is in een ongeldig formaat.")]
        PhonenumberIncorrectFormat = 205,

        /// <summary>
        /// EmailAlreadyInUse
        /// </summary>
        [StringValue("Er bestaat al een account voor het opgegeven e-mailadres.")]
        EmailAlreadyInUse = 206,

        /// <summary>
        /// PasswordEmpty
        /// </summary>
        [StringValue("Het opgegeven e-mailadres is in een ongeldig formaat.")]
        EmailIncorrectFormat = 207,

        /// <summary>
        /// Entity Save Error
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het opslaan van je account. (306)")]
        EntitySaveError = 306,

        /// <summary>
        /// Entity Save Error
        /// </summary>
        [StringValue("Er is geen Emailadres opgegeven..")]
        EmailaddressMissing = 308,

        InvalidCountryCode = 309
    }
}
