using Dionysos;

namespace Obymobi.Enums
{
	public enum MessagegroupType : int
	{
        [StringValue("Custom")]
		Custom = 0,

        [StringValue("Check-in (PMS)")]
		Checkin = 1,

        [StringValue("Check-out (PMS)")]
        Checkout = 2,

        [StringValue("Group (PMS)")]
        Group = 3
	}
}
