using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum BenchmarkAdvertisementType
    {
        /// <summary>
        /// View
        /// </summary>
        View,

        /// <summary>
        /// Click
        /// </summary>
        Click
    }
}
