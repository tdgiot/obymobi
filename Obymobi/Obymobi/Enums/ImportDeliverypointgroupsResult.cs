using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum ImportPosdeliverypointgroupsResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// No xml specified
        /// </summary>
        [StringValue("De opgegeven Xml is leeg.")]
        XmlIsEmpty = 200,

        /// <summary>
        /// No valid company id specified
        /// </summary>
        [StringValue("De opgegeven company id moet groter zijn dan 0.")]
        CompanyIdIsZeroOrLess = 201,

        /// <summary>
        /// Multiple pos deliverypointgroups found for the specified external id
        /// </summary>
        [StringValue("Er zijn meerdere pos deliverypoint groups gevonden voor de opgegeven externe id.")]
        MultiplePosdeliverypointgroupsFound = 202
    }
}
