﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum DeviceSaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Failed
        /// </summary>
        [StringValue("Er is een fout opgetreden bij opslaan van het device. (200)")]
        Failed = 200,

        /// <summary>
        /// PhonenumberAlreadyInUse
        /// </summary>
        [StringValue("Er bestaat al een device met dezelfde identifier.")]
        IdentifierAlreadyUsedForDevice = 201
    }
}
