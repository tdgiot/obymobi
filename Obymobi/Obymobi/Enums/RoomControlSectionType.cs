﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum RoomControlSectionType : int
    {
        /// <summary>
        /// Lighting 
        /// </summary>
        [StringValue("Lighting")]
        Lighting = 1,

        /// <summary>
        /// Comfort
        /// </summary>
        [StringValue("Comfort")]
        Comfort = 2,

        /// <summary>
        /// Drapes
        /// </summary>
        [StringValue("Blinds")]
        Blinds = 3,

        /// <summary>
        /// Service
        /// </summary>
        [StringValue("Service")]
        Service = 4,

        /// <summary>
        /// Screen Sleep
        /// </summary>
        [StringValue("Screen Sleep")]
        ScreenSleep = 5,

        /// <summary>
        /// Dashboard
        /// </summary>
        [StringValue("Dashboard")]
        Dashboard = 6,

        /// <summary>
        /// Timers
        /// </summary>
        [StringValue("Timers")]
        Timers = 7,

        /// <summary>
        /// Custom
        /// </summary>
        [StringValue("Custom")]
        Custom = 99
    }
}
