﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum AttachmentType
    {
        [StringValue("Not set")]
        NotSet = 0,

        [StringValue("Document")]
        Document = 1,

        [StringValue("Website")]
        Website = 2,

        [StringValue("Affiliate Book Online")]
        AffiliateBookOnline = 3,

        [StringValue("Affiliate Video Agent")]
        AffiliateVideoAgent = 4,

        [StringValue("Affiliate Price & Availability")]
        AffiliatePrices = 5,

        [StringValue("Youtube video")]
        Youtube = 6,

        [StringValue("Telephone number")]
        Telephone = 7
    }
}
