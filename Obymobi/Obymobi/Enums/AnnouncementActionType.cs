using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum AnnouncementActionType : int
	{
        Category = 1,

        Entertainment = 2,

        Product = 3
	}
}
