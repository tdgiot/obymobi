using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enum representation of Mobile Website Runtime errors
    /// </summary>
    public enum MobileWebsiteError : int
    {
        #region Generic Errors

        /// <summary>
        /// On purpose error for demonstration/testing purposes
        /// </summary>
        [StringValue("Dit is een demonstratie error.")]
        DemonstrationError = 1999,

        /// <summary>
        /// MissingQueryStringParameters
        /// </summary>
        [StringValue("De pagina is onjuist opgevraagd. Probeer het later opnieuw.")]
        MissingQueryStringParameters = 2000,

        /// <summary>
        /// MissingQueryStringParameters
        /// </summary>
        [StringValue("Het is niet mogelijk je laatste order te halen.")]
        NoLastOrderToRepeat = 2001,

        /// <summary>
        /// Order loaded from other user
        /// </summary>
        [StringValue("De opgevraagde bestelling kan niet worden getoond.")]
        OrderFromOtherUser = 3000,

        #endregion
    }
}
