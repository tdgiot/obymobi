using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum GetPointOfInterestTicksResult
    {
        Unspecified = 0,

        Success = 100,

        UnknownPointOfInterestId = 200,

        AuthenticationError = 998,

        Failure = 999
    }
}
