﻿namespace Obymobi.Enums
{
    /// <summary>
    /// 
    /// </summary>
    public enum PosServiceMessageType : int
    {
        /// <summary>
        /// 
        /// </summary>
        UnlockDeliverypoint = 100,

        /// <summary>
        /// 
        /// </summary>
        BatteryLow = 200,

        /// <summary>
        /// 
        /// </summary>
        ClientDisconnected = 300,
    }
}
