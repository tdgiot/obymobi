namespace Obymobi.Enums
{
    public enum ClientCommand
    {
        DoNothing = 0,
        RestartApplication = 1,
        RestartDevice = 2,
        RestartInRecovery = 3,
        SendLogToWebservice = 4,
        DailyOrderReset = 5,
        DownloadEmenuUpdate = 6,
        DownloadAgentUpdate = 7,
        DownloadSupportToolsUpdate = 8,
        InstallEmenuUpdate = 9,
        InstallAgentUpdate = 10,
        InstallSupportToolsUpdate = 11,
        ClearBrowserCache = 12,
        DownloadOSUpdate = 13,
        InstallOSUpdate = 14,
        ClearSendMessages = 15,
        GetRoomControlStatus = 16,
        DownloadInstallMessagingServiceUpdate = 17
    }
}
