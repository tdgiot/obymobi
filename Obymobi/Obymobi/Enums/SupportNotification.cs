﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum SupportNotification
    {
        OfflineTerminals,
        OfflineClients,
        UnprocessableOrders,
        NotRetrievedInTime,
        NotHandledInTime,
        TooMuchOfflineClientsJump,
        BouncedEmails
    }
}
