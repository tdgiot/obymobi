using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the result of saving a Category
    /// </summary>
    public enum CategorySaveResult : int
    {
        /// <summary>
        /// Authentication result is unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// User was successfully authenticated
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Telephone number was not found in the database 
        ///// </summary>
        //PhonenumberUnknown = 200,

        /// <summary>
        /// Name Empty
        /// </summary>
        [StringValue("Er is geen naam opgegeven voor de categorie.")]
        NameEmpty = 201,

        /// <summary>
        /// SelfReferencing
        /// </summary>
        [StringValue("De categorie verwijst naar zichzelf.")]
        SelfReferencing = 202,

        /// <summary>
        /// CircularReference
        /// </summary>
        [StringValue("De categorie bevat een cirkelverwijzing naar zichzelf.")]
        CircularReference = 203,

        /// <summary>
        /// CompanyIdEmpty
        /// </summary>
        [StringValue("Er is geen company id opgegeven voor de categorie.")]
        CompanyIdEmpty = 204,

        /// <summary>
        /// CompanyIdEmpty
        /// </summary>
        [StringValue("De bovenliggende categorie bevat producten.")]
        ParentCategoryContainsProducts = 205,

        [StringValue("Routes zijn niet toegestaan voor het type van de category.")]
        RouteNotAllowedForCategoryType = 206,

        [StringValue("Kies een MenuId")]
        MenuIdIsRequired = 207
        ///// <summary>
        ///// Incorrect password specified by the user
        ///// </summary>
        //PasswordIncorrect = 300
    }
}
