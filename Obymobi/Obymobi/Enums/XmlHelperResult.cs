﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the result of the authentication process
    /// </summary>
    public enum XmlHelperResult : int
    {
        /// <summary>
        /// Authentication result is unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// User was successfully authenticated
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Telephone number was not found in the database 
        /// </summary>
        [StringValue("Fout.")]
        Error = 200,

		/// <summary>
		/// Telephone number was not found in the database 
		/// </summary>
        [StringValue("Serializatiefout.")]
		SerializeError = 201,

		/// <summary>
		/// Telephone number was not found in the database 
		/// </summary>
        [StringValue("Deserializatiefout.")]
		DeserializeError = 202,
    }
}
