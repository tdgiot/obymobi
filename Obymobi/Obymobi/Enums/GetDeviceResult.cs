using Dionysos;

namespace Obymobi.Enums
{
	public enum GetDeviceResult : int
	{
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Multiple devices found for the specified identifier
        /// </summary>
        [StringValue("Meerdere apparaten gevonden voor de opgegeven identifier")]
        MultipleDevicesFoundForIdentifier = 200,

        /// <summary>
        /// No client id specified
        /// </summary>
        [StringValue("Er is geen identifier opgegeven")]
        NoIdentifierSpecified = 201,

		/// <summary>
		/// No device found for specified identifier
		/// </summary>
		[StringValue("Geen apparaat gevonden voor de opgegeven identifier")]
		NoDeviceFoundForIdentifier = 202,
	}
}
