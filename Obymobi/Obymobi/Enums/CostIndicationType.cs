using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum CostIndicationType : int
	{
        NotSet = 0,
        Free = 1,
        Cheap = 2,
        Average = 3,
        Expensive = 4
	}
}
