﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents the VatTariffSaveResult
	/// </summary>
	public enum VatTariffSaveResult : int
	{
		/// <summary>
		/// Unknown
		/// </summary>
        [StringValue("Onbekend.")]
		Unknown = 0,

		/// <summary>
		/// Success
		/// </summary>
        [StringValue("Succes.")]
		Success = 100,

        ///// <summary>
        ///// Failed
        ///// </summary>
        //Failed = 200,

		/// <summary>
		/// NameEmpty
		/// </summary>
        [StringValue("Er is geen naam opgegeven.")]
		NameEmpty = 201,

		/// <summary>
		/// PercentageEmptyOrLessThanZero
		/// </summary>
        [StringValue("Er is geen BTW-percentage ingegeven.")]
		PercentageEmptyOrLessThanZero = 202,		
	}
}