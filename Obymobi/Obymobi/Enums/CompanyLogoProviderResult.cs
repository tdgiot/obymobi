using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum CompanyLogoProviderResult : int
	{
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

		/// <summary>
		/// Success
		/// </summary>
        [StringValue("Succes.")]
		Success = 100,	

        ///// <summary>
        ///// Failure
        ///// </summary>
        //Failure = 200,

        /// <summary>
        /// CompanyIdMissing
        /// </summary>
        [StringValue("Er is geen company id meegegeven in de query string.")]
        CompanyIdMissing = 201,

        /// <summary>
        /// PreferredWidthMissing
        /// </summary>
        [StringValue("Er is geen breedte meegegeven in de query string.")]
        PreferredWidthMissing = 202,

        /// <summary>
        /// PreferredHeightMissing
        /// </summary>
        [StringValue("Er is geen hoogte meegegeven in de query string.")]
        PreferredHeightMissing = 203,

        /// <summary>
        /// NoLogoMediaEntity
        /// </summary>
        [StringValue("Er is geen media entity gevonden voor de opgegeven company id.")]
        NoLogoMediaEntity = 204,

        /// <summary>
        /// FileMissingAfterResize
        /// </summary>
        [StringValue("Het bestand ontbreekt nadat de grootte ervan is aangepast.")]
        FileMissingAfterResize = 205,

        /// <summary>
        /// LogoIsNotAWebImage
        /// </summary>
        [StringValue("De gevonden media entity is geen webafbeelding.")]
        LogoIsNotAWebImage = 206
	}
}
