using Dionysos;

namespace Obymobi.Enums
{
    public enum ClientOperationMode : int
    {
        /// <summary>
        /// Disabled, client can't be used
        /// </summary>
        [StringValue("Onbekend")]
        Unknown = 0,

        /// <summary>
        /// Ordering, client used in normal operation mode
        /// </summary>
        [StringValue("Bestellen (normaal)")]
        Ordering = 1,

        /// <summary>
        /// Kiosk mode, manually configured
        /// </summary>
        [StringValue("Kiosk (handmatig)")]
        KioskManual = 200,

        /// <summary>
        /// Kiosk mode, automatically configured
        /// </summary>
        [StringValue("Kiosk (automatisch door onverwerkte bestellingen)")]
        KioskAutomaticUnprocessedOrders = 201,

        /// <summary>
        /// Kiosk mode, automatically configured
        /// </summary>
        [StringValue("Kiosk (automatisch door onverwerkbare bestellingen)")]
        KioskAutomaticUnprocessableOrders = 202,

        /// <summary>
        /// Kiosk mode, automatically configured
        /// </summary>
        [StringValue("Kiosk (automatisch door terminal niet online)")]
        KioskAutomaticNoTerminalOnline = 203,

        /// <summary>
        /// Kiosk mode, automatically configured
        /// </summary>
        [StringValue("Kiosk (automatisch door geen tafel geconfigureerd)")]
        KioskNoDeliverypointConfigured = 204,

        /// <summary>
        /// Kiosk mode, automatically configured
        /// </summary>
        [StringValue("Kiosk (niet verbonden met wifi)")]
        KioskDisconnectedFromWifi = 205,

        /// <summary>
        /// Kiosk mode, automatically configured
        /// </summary>
        [StringValue("Kiosk (niet verbonden met webservice)")]
        KioskDisconnectedFromWebservice = 206,

        /// <summary>
        /// Kiosk mode, automatically configured
        /// </summary>
        [StringValue("Kiosk (niet verbonden met PokeIn)")]
        KioskDisconnectedFromPokeIn = 207,

        /// <summary>
        /// Disabled, client can't be used
        /// </summary>
        [StringValue("Uitgeschakeld / afgesloten")]
        Disabled = 300,
    }
}
