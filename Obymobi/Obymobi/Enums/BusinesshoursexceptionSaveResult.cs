using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents the result of the authentication process
	/// </summary>
	public enum BusinesshoursexceptionSaveResult : int
	{
		/// <summary>
		/// State unknown
		/// </summary>
        [StringValue("Onbekend.")]
		Unknown = 0,

		/// <summary>
		/// Success
		/// </summary>
        [StringValue("Succes.")]
		Success = 100,

        ///// <summary>
        ///// Unknown error
        ///// </summary>
        //UnknownError = 200,

        /// <summary>
        /// No Company Id
        /// </summary>
        [StringValue("Er is geen company id opgegeven.")]
        NoCompanyId = 201,

        /// <summary>
        /// Overlaps with other exception
        /// </summary>
        [StringValue("Er zit overlap in de openingstijden.")]
        Overlapping = 201,

        /// <summary>
        /// From is after Till
        /// </summary>
        [StringValue("De vanaf tijd is na de tot tijd.")]
        FromIsAfterTill = 201,
	}
}
