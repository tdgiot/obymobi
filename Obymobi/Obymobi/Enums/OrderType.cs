﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Defines the type of order it is, which 
    /// </summary>
    public enum OrderType : int
    {
        /// <summary>
        /// Standard order, print receipt and serve
        /// </summary>        
        [StringValue("Standard")]
        Standard = 200,

        /// <summary>
        /// Request for service
        /// </summary>
        [StringValue("Request for service")]
        RequestForService = 500,

        /// <summary>
        /// Request for checkout
        /// </summary>
        [StringValue("Request for checkout")]
        RequestForCheckout = 600,

        /// <summary>
        /// Request for checkout
        /// </summary>
        [StringValue("Unlock deliverypoint request")]
        UnlockDeliverypointRequest = 700,

        /// <summary>
        /// Request for battery charge
        /// </summary>
        [StringValue("Request for battery charge")]
        RequestForBatteryCharge = 800,

        /// <summary>
        /// Request for printing
        /// </summary>
        [StringValue("Request for printing")]
        RequestForPrint = 900,

        /// <summary>
        /// New document
        /// </summary>
        [StringValue("New document")]
        Document = 1000,

        /// <summary>
        /// Wake up
        /// </summary>
        [StringValue("Request for wake-up")]
        RequestForWakeUp = 1100,

        /// <summary>
        /// Product interest shown
        /// </summary>
        [StringValue("Product interest shown")]
        ProductInterestShown = 1200,

        // GK We should forget the Obymobi Transaction System        
        ///// <summary>
        ///// Confirmation Request for first order to open transaction, when confirmed converts to Standard
        ///// </summary>
        //[StringValue("Open transactie")]
        //OpenTransaction = 100,

        ///// <summary>
        ///// Close the transaction, empty order to request closing the transaction
        ///// </summary>
        //[StringValue("Sluit transactie")]
        //CloseTransaction = 300,

        ///// <summary>
        ///// Standard order, print receipt and serve
        ///// </summary>
        //[StringValue("Gegarandeerde betaling")]
        //PaymentGuaranteed = 400,

    }
}
