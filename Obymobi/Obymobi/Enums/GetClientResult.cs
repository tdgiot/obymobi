using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum GetClientResult : int
	{
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// No client found for the specified customer id
        /// </summary>
        [StringValue("Geen clients gevonden voor de opgegeven customer id.")]
        NoClientFoundForCustomerId = 200,

        /// <summary>
        /// Multiple clients found for the specified customer id
        /// </summary>
        [StringValue("Meerdere clients gevonden voor de opegegven customer id")]
        MultipleClientsFoundForCustomerId = 201,

        /// <summary>
        /// No client id specified
        /// </summary>
        [StringValue("Er is geen client id opgegeven")]
        NoClientIdSpecified = 202,

        /// <summary>
        /// No client found for the specified client id
        /// </summary>
        [StringValue("Geen clients gevonden voor de opgegeven client id")]
        NoClientFoundForClientId = 203,

        /// <summary>
        /// No client id or identifier specified
        /// </summary>
        [StringValue("Er is geen client id of identifier opgegeven")]
        NoClientIdOrIdentifierSpecified = 204,

        /// <summary>
        /// No client found for the specified client MAC address
        /// </summary>
        [StringValue("Geen clients gevonden voor de opgegeven identifier")]
        NoClientFoundForIdentifier = 205,

        /// <summary>
        /// Multiple clients found for the specified MAC address
        /// </summary>
        [StringValue("Meerdere clients gevonden voor de opgegeven identifier")]
        MultipleClientsFoundForIdentifier = 206,

        /// <summary>
        /// The device is configured for a client of another company
        /// </summary>
        [StringValue("Het apparaat is geconfigureerd voor een ander bedrijf")]
        DeviceConfiguredForAClientOfAnotherCompany = 207,
	}
}
