﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum UnlinkSocialmediaResult : int
    {
        Unknown = 0,

        Success = 100,

        CustomerSocialmediaNotFound = 500,

        Failure = 998,

        InvalidHash = 999
    }
}
