﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	/// <summary>
	/// Enumeration which represents a Website error
	/// </summary>
	public enum WebsiteError : int
	{
		#region Login / Session related

		/// <summary>
		/// TwoTypesOfUsersSignOnAtTheSameTime
		/// </summary>
        [StringValue("Twee gebruikers van verschillende types zijn tegelijkertijd aangemeld.")]
		TwoTypesOfUsersSignOnAtTheSameTime = 2000,

		/// <summary>
		/// NoLogicImplementedForSignOnOfUserType
		/// </summary>
        [StringValue("Er is geen logica geimplementeerd voor dit gebruikerstype.")]
		NoLogicImplementedForSignOnOfUserType = 2001,

		/// <summary>
		/// Unknown Authentication Error
		/// </summary>
        [StringValue("Er is een onbekende authenticatiefout opgetreden.")]
		UnknownAuthenticationError = 2002,

		/// <summary>
		/// UserControl not implemented for usertype
		/// </summary>
        [StringValue("Er is geen usercontrol geimplementeerd voor dit gebruikerstype.")]
		UserControlNotImplementedForUserType = 2003,

		#endregion


	}
}