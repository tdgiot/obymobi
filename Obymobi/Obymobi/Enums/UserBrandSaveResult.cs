﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum UserBrandSaveResult
    {
        [StringValue("Can not delete the last owner of a brand")]
        CanNotDeleteLastOwner
    }
}