﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum PMSMinibarSetting : int
    {
        [StringValue("Unknown")]
        Unknown = -1,
        [StringValue("Standard")]
        Standard = 0,
        [StringValue("Locked")]
        Locked = 1,
        [StringValue("Unused")]
        Unused = 2,
    }
}
