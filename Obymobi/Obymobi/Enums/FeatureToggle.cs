﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum FeatureToggle
    {
        [StringValue("Tagging")]
        Tagging_Cms_EnableTagEditing = 1,
        [StringValue("Linking Products to Alteration options")]
        Alterations_Cms_EnableProductInherit = 2,
        [StringValue("Checkout Method Test Overrides")]
        CheckoutMethod_Cms_TestOverrides = 3,
        [StringValue("Product Sales Report Template")]
        Reporting_Cms_ProductSalesReportTemplate = 4,
        [StringValue("Inventory Report Template")]
        Reporting_Cms_InventoryReportTemplate = 5,
        [StringValue("Creating 'Adyen For Platforms' payment integration configurations")]
        CreatingAdyenForPlatformsPaymentIntegrationConfigurations = 6,
        [StringValue("All things London")]
        AllThingsLondon = 7,
        [StringValue("Covers")]
        Covers = 8,
        [StringValue("Multiple Campaign Codes")]
        MultipleCampaignCodes = 9,
        [StringValue("Hot Swappable Menu Content")]
        HotSwappableMenuContent = 10
    }
}
