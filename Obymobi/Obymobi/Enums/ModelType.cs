using Dionysos;

namespace Obymobi.Enums
{
    public enum ModelType : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend")]
        Unknown = 0,

        /// <summary>
        /// UI Widget
        /// </summary>
        [StringValue("UI widget")]
        UIWidget = 1,

        /// <summary>
        /// Availability
        /// </summary>
        [StringValue("Availability")]
        Availability = 2,

        /// <summary>
        /// Media
        /// </summary>
        [StringValue("Media")]
        Media = 3,
    }
}
