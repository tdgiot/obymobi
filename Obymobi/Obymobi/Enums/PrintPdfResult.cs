﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum PrintPdfResult : int
    {
        /// <summary>
        /// Unspecified
        /// </summary>
        [StringValue("Unspecified")]
        Unspecified = 0,

        /// <summary>
        /// Order was successfully saved
        /// </summary>
        [StringValue("Bestelling is succesvol verzonden")]
        Success = 100,

        /// <summary>
        /// File size was too large to email
        /// </summary>
        [StringValue("De pdf file is te groot om te emailen")]
        FileSizeTooLarge = 201,

        /// <summary>
        /// The device is not allowed to print this pdf
        /// </summary>
        [StringValue("Het is niet toegestaan om deze pdf te printen met dit apparaat")]
        DeviceNotAllowedToPrint = 202
    }
}
