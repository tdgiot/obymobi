using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum GetCompanyResult : int
	{
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Get company failed
        ///// </summary>
        //[StringValue("")]
        //Failed = 200,

        /// <summary>
        /// Search query to short
        /// </summary>
        [StringValue("De opgegeven zoekterm is te kort.")]
        SearchQueryToShort = 201,

        /// <summary>
        /// Specified company id is unknown
        /// </summary>
        [StringValue("De opgegeven company id is onbekend.")]
        CompanyIdUnknown = 202,

        /// <summary>
        /// Specified Oby-code is unknown
        /// </summary>
        [StringValue("De opgegeven Oby-code is onbekend.")]
        ObycodeUnknown = 203,

        /// <summary>
        /// Multiple companies found for the specified Oby-code
        /// </summary>
        [StringValue("Meerdere bedrijven gevonden voor de opgegeven Oby-code")]
        MultipleCompaniesFoundForObycode = 204,

        /// <summary>
        /// Multiple companies found for the specified company id
        /// </summary>
        [StringValue("Meerdere bedrijven gevonden voor de opgegeven company id")]
        MultipleCompaniesFoundForCompanyId = 205,

        InvalidDeviceType = 210,

        AuthenticationError = 998,

        Failure = 999
	}
}
