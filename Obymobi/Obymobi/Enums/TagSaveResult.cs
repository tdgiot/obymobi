﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum TagSaveResult
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Unknown.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Success.")]
        Success = 100,

        ///// <summary>
        ///// Failed
        ///// </summary>
        [StringValue("Failed.")]
        Failed = 200,

        /// <summary>
        /// Tag not unique
        /// </summary>
        [StringValue("The company tag isn't unique.")]
        TagNotUnique = 201,
    }
}
