using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the result of the authentication process
    /// </summary>
    public enum BusinesshourCheckResult : int
    {
        /// <summary>
        /// State unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Unknown error
        ///// </summary>
        //UnkownError = 200,

        /// <summary>
        /// Overlapping exceptions found
        /// </summary>
        [StringValue("Er is een overlap aangetroffen in de openingstijden.")]
        OverlappingBusinesshourexceptions = 201,

        /// <summary>
        /// No business hours available
        /// </summary>
        [StringValue("Er zijn geen openingstijden beschikbaar.")]
        NoBusinesshoursAvailable = 202,
    }
}
