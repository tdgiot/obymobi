using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum DeviceRebootMethod
    {
        [StringValue("Default (30 minutes)")]
        Default = 1,

        [StringValue("Reboot after 15 minutes")]
        RebootAfter15Minutes = 2
    }
}
