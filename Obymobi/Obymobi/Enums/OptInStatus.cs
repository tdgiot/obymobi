﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum OptInStatus
    {
        [StringValue("No Opt-In Asked")]
        NoOptInAsked = 0,

        [StringValue("Hard Opt-In")]
        HardOptIn = 1,

        [StringValue("Hard Opt-Out")]
        HardOptOut = 2,

        [StringValue("Hard Opt-In (existing)")]
        HardOptInExists = 3
    }
}
