using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum DevicePlatform
    {
        Unknown = 0,
        Android = 1,
        iOS = 2,
        Windows = 3,
        Blackberry = 4
    }
}
