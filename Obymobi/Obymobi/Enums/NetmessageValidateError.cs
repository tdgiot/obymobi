﻿using Dionysos;

namespace Obymobi.Enums
{
	public enum NetmessageValidateError
	{
		[StringValue("ValidationFailed")]
		ValidationFailed = 0,

		[StringValue("AlreadyExists")]
		AlreadyExists = 1,

		[StringValue("NoSuchMessageType")]
		NoSuchMessageType = 2,
	}
}
