﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum PMSTvSetting : int
    {
        [StringValue("Unknown")]
        Unknown = -1,
        [StringValue("Standard")]
        Standard = 0,
        [StringValue("Adult Blocked")]
        AdultBlocked = 1,
        [StringValue("Unlimited")]
        Unlimited = 2,
        [StringValue("None")]
        None = 3,
        [StringValue("Unused")]
        Unused = 4,
    }
}
