﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum SynchronizeAlterationsResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Multiple alterations found for the specified external id
        /// </summary>
        [StringValue("Er zijn meerdere alterations gevonden voor de opgegeven pos alteration id.")]
        MultipleAlterationsFound = 200
    }
}
