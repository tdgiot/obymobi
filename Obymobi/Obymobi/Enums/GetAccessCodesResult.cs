using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum GetAccessCodesResult
    {
        Unspecified = 0,

        Success = 100,

        NoAccessCodeFound = 210,

        MultipleAccessCodesFound = 220,

        AccessCodesIncompatible = 230,

        NoAccessCodeSpecified = 240,

        AccessCodeAlreadyAdded = 250,

        AuthenticationError = 998,

        Failure = 999
    }
}
