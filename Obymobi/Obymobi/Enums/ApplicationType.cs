using System;

namespace Obymobi.Enums
{
    public enum ApplicationType
    {
        NotSpecified = 0,
        Api = 1,
        Cms = 2,
        ByodApp = 3,
        IrtApp = 4
    }

    public class ApplicationTypeExt
    {
        public static ApplicationType FromString(string type)
        {
            if (type.Equals("ByodApp", StringComparison.InvariantCultureIgnoreCase))
                return ApplicationType.ByodApp;
            if (type.Equals("ItrApp", StringComparison.InvariantCultureIgnoreCase) || type.Equals("IrtApp", StringComparison.InvariantCultureIgnoreCase))
                return ApplicationType.IrtApp;
            if (type.Equals("Api", StringComparison.InvariantCultureIgnoreCase))
                return ApplicationType.Api;
            if (type.Equals("Cms", StringComparison.InvariantCultureIgnoreCase))
                return ApplicationType.Cms;

            return ApplicationType.NotSpecified;
        }
    }
}
