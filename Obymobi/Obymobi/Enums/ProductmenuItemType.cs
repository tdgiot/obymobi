﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum ProductmenuItemType : int
    {
        /// <summary>
        /// Category
        /// </summary>
        Category = 100,

        /// <summary>
        /// Product
        /// </summary>
        Product = 200,

        /// <summary>
        /// Service request
        /// </summary>
        Service = 300,
    }
}
