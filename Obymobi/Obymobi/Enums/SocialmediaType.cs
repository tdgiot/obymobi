﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum SocialmediaType : int
	{
        /// <summary>
        /// Unknown 
        /// </summary>
        Unknown = 0,

		/// <summary>
		/// Twitter 
		/// </summary>
        [StringValue("Twitter")]
		Twitter = 1,

		/// <summary>
		/// Facebook
		/// </summary>
        [StringValue("Facebook")]
		Facebook = 2,

        /// <summary>
        /// Google
        /// </summary>
        [StringValue("Google")]
        Google = 3,

        /// <summary>
        /// Foursquare
        /// </summary>
        [StringValue("Foursquare")]
        Foursquare = 4
	}
}
