using Dionysos;

namespace Obymobi.Enums
{
    public enum CloudProcessingTaskType
    {
        [StringValue("Entertainment")]
        Entertainment = 1,

        [StringValue("Weather")]
        Weather = 2,

        [StringValue("Release")]
        Release = 3,

        [StringValue("Media")]
        Media = 4,

        [StringValue("ApiResult")]
        ApiResult = 5,

        [StringValue("DeliveryTime")]
        DeliveryTime = 6,

        [StringValue("Unknown")]
        Unknown = 0
    }
}
