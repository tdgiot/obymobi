using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
	public enum GetPointOfInterestResult : int
	{
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unspecified = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Get company failed
        ///// </summary>
        //[StringValue("")]
        //Failed = 200,

        /// <summary>
        /// Search query to short
        /// </summary>
        [StringValue("De opgegeven zoekterm is te kort.")]
        SearchQueryToShort = 201,

        /// <summary>
        /// Specified company id is unknown
        /// </summary>
        PointOfInterestIdUnknown = 202,
     
        /// <summary>
        /// Multiple POIs found
        /// </summary>
        MultiplePointsOfInterestFoundForPointOfInterestId = 203,

        InvalidDeviceType = 210,

        AuthenticationError = 998,

        Failure = 999
	}
}
