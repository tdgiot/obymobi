﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum PasswordResetResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Password was successfully reset
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Customer unknown
        /// </summary>
        [StringValue("Het opgegeven telefoonnummer is onbekend.")]
        UnknownPhonenumber = 200,

        /// <summary>
        /// Email not found
        /// </summary>
        [StringValue("Het opgegeven e-mailadres is onbekend.")]
        UnknownEmail = 201,

        /// <summary>
        /// Customer not Found
        /// </summary>
        [StringValue("Gebruiker niet gevonden.")]
        UserNotFound = 202,

        /// <summary>
        /// The customer doesn't have an (valid) email set.
        /// </summary>
        [StringValue("Gebruiker heeft geen e-mailadres ingesteld.")]
        UserHasNoEmail = 203,

        /// <summary>
        /// Customer unknown
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het verzenden van de SMS.")]
        SmsSentFailed = 302
    }
}
