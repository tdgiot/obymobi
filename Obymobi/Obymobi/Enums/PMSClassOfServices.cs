﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum PMSClassOfService : int
    {
        [StringValue("Unknown")]
        Unknown = -1,
        [StringValue("Barred")]
        Barred = 0,
        [StringValue("Local calls only")]
        LocalCallsOnly = 1,
        [StringValue("National")]
        National = 2,
        [StringValue("Unrestricted")]
        Unrestricted = 3,
    }
}
