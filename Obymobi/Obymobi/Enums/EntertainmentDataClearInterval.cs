﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum EntertainmentDataClearInterval
    {
        [StringValue("Default - On Emenu Start / Daily Order Reset")]
        EmenuStart = 0,
        [StringValue("Never")]
        Never = 1,
        [StringValue("On App Close")]
        AppClose = 2,
        
        [StringValue("1 Hour After Last Close")]
        AfterOneHour = 10,
        [StringValue("6 Hours After Last Close")]
        AfterSixHours = 11,
        [StringValue("12 Hours After Last Close")]
        AfterTwelveHours = 12,
        [StringValue("1 Day After Last Close")]
        AfterOneDay = 13,
    }
}
