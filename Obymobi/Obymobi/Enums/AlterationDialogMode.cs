﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum AlterationDialogMode
    {
        [StringValue("V1")]
        v1 = 0,
        [StringValue("V2")]
        v2 = 1,
        [StringValue("V3")]
        v3 = 2,
    }
}
