﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum SynchronizeCategoriesResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Multiple categories found for the specified external id
        /// </summary>
        [StringValue("Er zijn meerdere categorieen gevonden voor de opgegeven pos categorie id.")]
        MultipleCategoriesFound = 200
    }
}
