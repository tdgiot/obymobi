﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    /// <summary>
    /// 0 - 100 Non-Generic / Special cases
    /// 100 - 500 Generic Payment methods
    /// </summary>
    public enum PaymentmethodType : int
    {
        /// <summary>
        /// Not set / Not a Paymentmethod for generic handling
        /// </summary>
        Unknown = 0,

        // All methods between 1 - 19 don't require an actual payment & are for demo purposes
        /// <summary>
        /// Demo
        /// </summary>
        Demo = 10,

        // All methods between 20 - 99 don't require an actual payment but can be processed directly.
        /// <summary>
        /// Order supplied bij de Emenu, no payment required
        /// </summary>
        EmenuTabletOrderWithoutPayment = 20,

        // All methods from 100 - 999 require actual payment handling before the order is allowed to be processed
        /// <summary>
        /// Paypal with a PayPal Account
        /// </summary>
        PaypalWithAccount = 100,

        /// <summary>
        /// Paypal using a Credit Card (not using a PayPal account)
        /// </summary>
        PaypalWithCreditcard = 110,
    }
}
