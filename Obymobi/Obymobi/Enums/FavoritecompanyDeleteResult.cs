using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum FavoritecompanyDeleteResult : int
    {
        /// <summary>
        /// Favorite company save result is unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Favorite company was successfully deleted
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Favorite company failed to delete
        ///// </summary>
        //Failed = 200,

        ///// <summary>
        ///// Favorite company was not found
        ///// </summary>
        //DidNotExist = 300,

        ///// <summary>
        ///// Multiple favorite companies found, this should not occur
        ///// </summary>
        //MultipleFound = 400
    }
}
