﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum UIWidgetType : int
    {
        /// <summary>
        /// WeatherClock v1 
        /// </summary>
        [StringValue("WeatherClock V1")]
        WeatherClockV1 = 1,

        /// <summary>
        /// WeatherClockV2
        /// </summary>
        [StringValue("WeatherClock V2")]
        WeatherClockV2 = 2,

        /// <summary>
        /// LanguagePicker
        /// </summary>
        [StringValue("Language Picker")]
        LanguagePicker = 3,

        /// <summary>
        /// AdvertisementSmall
        /// </summary>
        [StringValue("Advertisement Small")]
        AdvertisementSmall = 4,

        /// <summary>
        /// AdvertisementMedium
        /// </summary>
        [StringValue("Advertisement Medium")]
        AdvertisementMedium = 5,

        /// <summary>
        /// ConnectBtSpeaker
        /// </summary>
        [StringValue("Connect Bluetooth Speaker 1x1")]
        ConnectBtSpeaker1x1 = 6,

        /// <summary>
        /// ConnectBtKeyboard
        /// </summary>
        [StringValue("Connect Bluetooth Keyboard 1x1")]
        ConnectBtKeyboard1x1 = 7,

        /// <summary>
        /// PmsVoicemailButton
        /// </summary>
        [StringValue("PMS Voicemail Button 2x1")]
        PmsVoicemail2x1 = 8,

        /// <summary>
        /// ConnectBtSpeaker
        /// </summary>
        [StringValue("Connect Bluetooth Speaker 2x1")]
        ConnectBtSpeaker2x1 = 9,

        /// <summary>
        /// ConnectBtKeyboard
        /// </summary>
        [StringValue("Connect Bluetooth Keyboard 2x1")]
        ConnectBtKeyboard2x1 = 10,

        /// <summary>
        /// Placeholder
        /// </summary>
        [StringValue("Placeholder")]
        Placeholder = 11,

        /// <summary>
        /// PmsBill
        /// </summary>
        [StringValue("PMS Bill Button 2x1")]
        PmsBill2x1 = 12,

        /// <summary>
        /// ServiceRequest
        /// </summary>
        [StringValue("Service Request 2x1")]
        ServiceRequest2x1 = 13,

        /// <summary>
        /// PmsVoicemailButton
        /// </summary>
        [StringValue("PMS Voicemail Button 1x1")]
        PmsVoicemail1x1 = 14,

        /// <summary>
        /// PmsBill
        /// </summary>
        [StringValue("PMS Bill Button 1x1")]
        PmsBill1x1 = 15,

        /// <summary>
        /// Advertisement1x1
        /// </summary>
        [StringValue("Advertisement 1x1")]
        Advertisement1x1 = 16,

        /// <summary>
        /// LinkWidget1x1
        /// </summary>
        [StringValue("Link Widget 1x1")]
        LinkWidget1x1 = 17,

        /// <summary>
        /// LinkWidget2x1
        /// </summary>
        [StringValue("Link Widget 2x1")]
        LinkWidget2x1 = 18,

        /// <summary>
        /// LinkWidget2x2
        /// </summary>
        [StringValue("Link Widget 2x2")]
        LinkWidget2x2 = 19,

        /// <summary>
        /// SceneWidget1x1
        /// </summary>
        [StringValue("Scene Widget 1x1")]
        SceneWidget1x1 = 20,

        /// <summary>
        /// SceneWidget2x1
        /// </summary>
        [StringValue("Scene Widget 2x1")]
        SceneWidget2x1 = 21,

        /// <summary>
        /// Clock widget 2x2
        /// </summary>
        [StringValue("Clock Widget 2x2")]
        Clock2x2 = 22,

        /// <summary>
        /// Weather widget 2x2
        /// </summary>
        [StringValue("Weather Widget 2x3")]
        Weather2x3 = 23,

        /// <summary>
        /// Deliverytime widget 2x1
        /// </summary>
        [StringValue("Delivertime Widget 2x1")]
        Deliverytime2x1 = 24,

        /// <summary>
        /// Alarm clock widget 1x1
        /// </summary>
        [StringValue("Alarm Clock Widget 1x1")]
        AlarmClock1x1 = 25,

        /// <summary>
        /// Alarm clock widget 2x1
        /// </summary>
        [StringValue("Alarm Clock Widget 2x1")]
        AlarmClock2x1 = 26,

        /// <summary>
        /// Menu widget fill
        /// </summary>
        [StringValue("Menu Fill")]
        MenuFill = 27,

        /// <summary>
        /// Order history widget
        /// </summary>
        [StringValue("Order History")]
        OrderHistory = 28,

        /// <summary>
        /// Count Down
        /// </summary>
        [StringValue("Count Down 2x1")]
        CountDown2x1 = 29,

        /// <summary>
        /// Count Down
        /// </summary>
        [StringValue("Count Down 2x2")]
        CountDown2x2 = 30,

        /// <summary>
        /// Availability
        /// </summary>
        [StringValue("Availability 2x1")]
        Availability2x1 = 31,

        /// <summary>
        /// Availability
        /// </summary>
        [StringValue("Availability 2x2")]
        Availability2x2 = 32,

        /// <summary>
        /// Text 2x2
        /// </summary>
        [StringValue("Text 2x2")]
        Text2x2 = 33,

        /// <summary>
        /// Text 2x4
        /// </summary>
        [StringValue("Text 2x4")]
        Text2x4 = 34,

        /// <summary>
        /// Currency 2x1
        /// </summary>
        [StringValue("Currency 2x1")]
        Currency2x1 = 35,

        /// <summary>
        /// Currency 2x2
        /// </summary>
        [StringValue("Currency 2x2")]
        Currency2x2 = 36,

        /// <summary>
        /// Currency 1x1
        /// </summary>
        [StringValue("Currency 1x1")]
        Currency1x1 = 37,

        /// <summary>
        /// Deliverytime 1x1
        /// </summary>
        [StringValue("Delivertime Widget 1x1")]
        Deliverytime1x1 = 38,

        [StringValue("Connect Bt Device Instructions Widget 1x1")]
        ConnectBtDeviceInstructions1x1 = 39,

        [StringValue("Ad Widget 2x1")]
        AdWidget2x1 = 40,

        [StringValue("Ad Widget 2x2")]
        AdWidget2x2 = 41,

        [StringValue("Ad Widget 2x5")]
        AdWidget2x5 = 42,

        [StringValue("Ad Widget 2x8")]
        AdWidget2x8 = 43,

        [StringValue("Ad Widget 1x10")]
        AdWidget1x10 = 44,

        [StringValue("Connect Bt Device Instructions Widget 2x1")]
        ConnectBtDeviceInstructions2x1 = 45,

        [StringValue("Connect Bt Device Instructions Widget 2x2")]
        ConnectBtDeviceInstructions2x2 = 46,

        [StringValue("Connect Bluetooth Speaker 2x2")]
        ConnectBtSpeaker2x2 = 47,

        [StringValue("Ad Widget 2x10")]
        AdWidget2x10 = 48,

        [StringValue("Map Menu Fill")]
        MapMenuFill = 49,

        [StringValue("Map Page")]
        MapPage = 50,

        [StringValue("Service Request 1x1")]
        ServiceRequest1x1 = 51,

        [StringValue("Keyboard Language Picker")]
        KeyboardLanguagePicker = 52,
    }
}
