using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum ClientLogType : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Unknown")]
        Unknown = 0,

        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("GenericMessage")]
        GenericMessage = 5,

        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("ApplicationStarted")]
        ApplicationStarted = 10,

        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("ApplicationStopped")]
        ApplicationStopped = 15,

        /// <summary>
        /// Client wake up set
        /// </summary>
        [StringValue("Client Wake-Up Set")]
        ClientWakeUpSet = 20,

        /// <summary>
        /// Client wake up set
        /// </summary>
        [StringValue("Service Version Switched")]
        ServiceVersionSwitched = 25,

        [StringValue("Device Reboot")]
        DeviceReboot = 50,

        [StringValue("DeviceShutdown")]
        DeviceShutdown = 55,

        /// <summary>
        /// Automatic Switch To Kiosk
        /// </summary>
        [StringValue("Fail to kiosk because of non processed orders")]
        FailToKioskForUnprocessedOrders = 100,

        /// <summary>
        /// Automatic Switch To Kiosk
        /// </summary>
        [StringValue("Fail to kiosk because the related terminal is not online")]
        FailToKioskNoTerminalOnline = 101,

        /// <summary>
        /// Automatic Switch To Kiosk
        /// </summary>
        [StringValue("Fail to kiosk because of unprocessable orders")]
        FailToKioskForUnprocessableOrders = 102,

        /// <summary>
        /// Automatic Switch To Normal
        /// </summary>
        [StringValue("Automatic Switch To Ordering")]
        AutomaticReturnToOrdering = 200,

        /// <summary>
        /// Automatic Switch To Normal
        /// </summary>
        [StringValue("Recieved a log")]
        ReceivedLog = 300,

        /// <summary>
        /// Automatic Switch To Normal
        /// </summary>
        [StringValue("ShippedLog")]
        ShippedLog = 301,

        /// <summary>
        /// Device changed
        /// </summary>
        [StringValue("Device changed")]
        DeviceChanged = 305,

        /// <summary>
        /// Device changed
        /// </summary>
        [StringValue("Deliverypoint changed")]
        DeliverypointChanged = 306,

        /// <summary>
        /// Bericht
        /// </summary>
        [StringValue("NonQualifiedException")]
        NonQualifiedException = 400,

        /// <summary>
        /// Download started
        /// </summary>
        [StringValue("Download started")]
        DownloadStarted = 600,

        /// <summary>
        /// Download completed
        /// </summary>
        [StringValue("Download completed")]
        DownloadCompleted = 601,

        /// <summary>
        /// Download failed
        /// </summary>
        [StringValue("Download failed")]
        DownloadFailed = 602,

        /// <summary>
        /// Installation started
        /// </summary>
        [StringValue("Installation started")]
        InstallationStarted = 603,

        /// <summary>
        /// Installation completed
        /// </summary>
        [StringValue("Installation completed")]
        InstallationCompleted = 604,

        /// <summary>
        /// Installation failed
        /// </summary>
        [StringValue("Installation failed")]
        InstallationFailed = 605,

        /// <summary>
        /// Installation failed
        /// </summary>
        [StringValue("ConfigurationBackupRestored")]
        ConfigurationBackupRestored = 606,

        /// <summary>
        /// Installation failed
        /// </summary>
        [StringValue("OsUpdated")]
        OsUpdated = 607,

        /// <summary>
        /// Device froze
        /// </summary>
        [StringValue("FreezeLog")]
        FreezeLog = 608,

        /// <summary>
        /// Comet message
        /// </summary>
        [StringValue("CometMessage")]
        CometMessage = 700,

        /// <summary>
        /// Client Communication Method changed
        /// </summary>
        [StringValue("CommunicationMethodChanged")]
        CommunicationMethodChanged = 800,

        /// <summary>
        /// Received room control status
        /// </summary>
        [StringValue("ReceivedRoomControlStatus")]
        ReceivedRoomControlStatus = 900,

        /// <summary>
        /// Room control connected
        /// </summary>
        [StringValue("RoomControlConnected")]
        RoomControlConnected = 910,

        /// <summary>
        /// Room control disconnected
        /// </summary>
        [StringValue("RoomControlDisconnected")]
        RoomControlDisconnected = 920,
    }
}
