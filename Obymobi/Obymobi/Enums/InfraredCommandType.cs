using Dionysos;

namespace Obymobi.Enums
{
	public enum InfraredCommandType : int
	{
        [StringValue("Power")]
        Power = 1,

        [StringValue("Mute")]
        Mute = 2,

        [StringValue("Channel up")]
        ChannelUp = 3,

        [StringValue("Channel down")]
        ChannelDown = 4,

        [StringValue("Volume up")]
        VolumeUp = 5,

        [StringValue("Volume down")]
        VolumeDown = 6,

        [StringValue("Cursor up")]
        CursorUp = 7,

        [StringValue("Cursor right")]
        CursorRight = 8,

        [StringValue("Cursor down")]
        CursorDown = 9,

        [StringValue("Cursor left")]
        CursorLeft = 10,

        [StringValue("Enter")]
        Enter = 11,

        [StringValue("0")]
        Number0 = 12,

        [StringValue("1")]
        Number1 = 13,

        [StringValue("2")]
        Number2 = 14,

        [StringValue("3")]
        Number3 = 15,

        [StringValue("4")]
        Number4 = 16,

        [StringValue("5")]
        Number5 = 17,

        [StringValue("6")]
        Number6 = 18,

        [StringValue("7")]
        Number7 = 19,

        [StringValue("8")]
        Number8 = 20,

        [StringValue("9")]
        Number9 = 21,

        [StringValue("Guide")]
        Guide = 22,

        [StringValue("Dash")]
        Dash = 23,

        [StringValue("Custom button 1")]
        CustomButton1 = 101,

        [StringValue("Custom button 2")]
        CustomButton2 = 102,

        [StringValue("Custom button 3")]
        CustomButton3 = 103,

        [StringValue("Custom")]
        Custom = 99999
	}
}
