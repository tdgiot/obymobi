﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum SiteVersion
    {
        [StringValue("V1")]
        v1 = 0,
        [StringValue("V2")]
        v2 = 1        
    }
}
