using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum GetSiteResult
    {
        Unspecified = 0,

        Success = 100,

        InvalidSiteId = 200,

        InvalidLanguageCode = 201,

        AuthenticationError = 202,

        Failure = 999
    }
}
