﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum ExternalSystemLogType
    {
        [StringValue("Channel Status Update")]
        ChannelStatusUpdate = 1,

        [StringValue("Menu Update")]
        MenuUpdate = 2,

        [StringValue("Order Status Update")]
        OrderStatusUpdate = 3,

        [StringValue("Snooze Products Update")]
        SnoozeProductsUpdate = 4,
        
        [StringValue("Dispatch Order")]
        DispatchOrder = 5,

        [StringValue("Busy Mode")]
        BusyMode = 6
    }
}
