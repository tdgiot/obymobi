using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum MapProvider : int
	{
        Google = 0,
        Mapbox = 1
	}
}
