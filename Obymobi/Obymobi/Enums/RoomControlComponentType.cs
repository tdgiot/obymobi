﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum RoomControlComponentType : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Unknown")]
        Unknown = 0,

        /// <summary>
        /// Light
        /// </summary>
        [StringValue("Light")]
        Light = 1,

        /// <summary>
        /// Lighting Scene
        /// </summary>
        [StringValue("Lighting Scene")]
        LightingScene = 2,

        /// <summary>
        /// Thermostat
        /// </summary>
        [StringValue("Thermostat")]
        Thermostat = 3,

        /// <summary>
        /// Blind
        /// </summary>
        [StringValue("Blind")]
        Blind = 4,

        /// <summary>
        /// Service
        /// </summary>
        [StringValue("Service")]
        Service = 5,
    }
}
