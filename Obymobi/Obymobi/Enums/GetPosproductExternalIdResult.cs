using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum GetPosProductExternalIdResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// No pos product founds for the specified number
        /// </summary>
        [StringValue("Er is geen pos product gevonden voor het opgegeven product id.")]
        NoPosproductFound = 200,

        /// <summary>
        /// Multiple pos products found
        /// </summary>
        [StringValue("Er zijn meerdere pos producten gevonden voor het opgegeven product id.")]
        MultiplePosproductsFound = 201
    }
}
