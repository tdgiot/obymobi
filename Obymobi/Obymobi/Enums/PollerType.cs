﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum PollerType
    {
        [StringValue("ReportingPoller")]
        ReportingPoller = 1,
        [StringValue("CloudTaskPoller")]
        CloudTaskPoller = 2,
        [StringValue("DatabasePoller")]
        DatabasePoller = 3,
        [StringValue("SupportNotificationPoller")]
        SupportNotificationPoller = 4,
        [StringValue("MediaTaskPoller")]
        MediaTaskPoller = 5,
        [StringValue("CompanyManagementPoller")]
        CompanyManagementPoller = 6,
        [StringValue("PerformancePoller")]
        PerformancePoller = 7,
        [StringValue("ExternalContentPoller")]
        ExternalContentPoller = 8,
        [StringValue("CleanOfflineClientsPoller")]
        CleanOfflineClientsPoller = 9,
        [StringValue("CleanCloudStoragePoller")]
        CleanCloudStoragePoller = 10,
        [StringValue("HotSOSPoller")]
        HotSOSPoller = 11,
        [StringValue("WeatherPoller")]
        WeatherPoller = 12,
        [StringValue("ExternalLinkCheckPoller")]
        ExternalLinkCheckPoller = 13,
        [StringValue("MessagePoller")]
        MessagePoller = 14,
        [StringValue("CleanScheduledCommandTaskPoller")]
        CleanScheduledCommandTaskPoller = 15,
        [StringValue("ClientStatusPoller")]
        ClientStatusPoller = 16,
        [StringValue("NetmessageCleanerPoller")]
        NetmessageCleanerPoller = 17,
        [StringValue("CleanScheduledMediaPoller")]
        CleanScheduledMediaPoller = 18,
        [StringValue("ReconnectClientsToMessagingPoller")]
        ReconnectClientsToMessagingPoller = 19,
        [StringValue("GameSessionReportPoller")]
        GameSessionReportPoller = 20,
        [StringValue("ScheduledCommandTaskPoller")]
        ScheduledCommandTaskPoller = 21,
        [StringValue("PmsTerminalStatusPoller")]
        PmsTerminalStatusPoller = 22,
        [StringValue("AnalyticsProcessingTaskPoller")]
        AnalyticsProcessingTaskPoller = 23,
        [StringValue("DeviceTokenTaskPoller")]
        DeviceTokenTaskPoller = 24,
        [StringValue("UISchedulePoller")]
        UISchedulePoller = 25,
        [StringValue("CleanLogPoller")]
        CleanLogPoller = 26,

        [StringValue("TestPoller")]
        TestPoller = 100,
    }
}
