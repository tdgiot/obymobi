﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum AzureNotificationHubSaveResult
    {
        [StringValue("A hub with this configuration already exists")]
        HubAlreadyExists
    }
}
