﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum ResetPasswordResult
    {
        Unspecified = 0,

        Success = 100,

        CustomerIdUnknown = 200,

        ResetPasswordIdentifierInvalid = 201,

        ResetPasswordIdentifierExpired = 202,

        TooManyFailedAttempts = 204,

        InvalidHash = 205,

        Failure = 999

    }
}
