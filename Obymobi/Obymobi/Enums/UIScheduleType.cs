namespace Obymobi.Enums
{
	public enum UIScheduleType : int
	{
        /// <summary>
        /// Schedule used for sending reports.
        /// </summary>
        Reporting = 1,

        /// <summary>
        /// Combined schedule for widgets, messages and slide show images
        /// </summary>
        Legacy = 999,
	}
}
