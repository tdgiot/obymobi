﻿using System;
using Dionysos;

namespace Obymobi.Enums
{
    public enum NetmessageType
    {
        /// <summary>
        /// Offline
        /// </summary>
        [StringValue("Unknown")]
        Unknown = 0,

        /// <summary>
        /// Offline
        /// </summary>
        [StringValue("AnnounceNewMessage")]
        AnnounceNewMessage = 2,

        /// <summary>
        /// Order status updated
        /// </summary>
        [StringValue("OrderStatusUpdated")]
        OrderStatusUpdated = 3,

        /// <summary>
        /// Mobile ordering status changed
        /// </summary>
        [StringValue("SetMobileOrdering")]
        SetMobileOrdering = 4,

        /// <summary>
        /// Broadcast message
        /// </summary>
        [StringValue("BroadcastMessage")]
        BroadcastMessage = 5,

        /// <summary>
        /// Social media message
        /// </summary>
        [StringValue("SocialMediaMessage")]
        NewSocialMediaMessage = 6,

        /// <summary>
        /// Routestephandler status updated
        /// </summary>
        [StringValue("RoutestephandlerStatusUpdated")]
        RoutestephandlerStatusUpdated = 7,

        /// <summary>
        /// Checkout notification to Emenu
        /// </summary>
        [StringValue("SetPmsCheckedOut")]
        SetPmsCheckedOut = 8,

        /// <summary>
        /// Get Pms Folio from Oss
        /// </summary>
        [StringValue("GetPmsFolio")]
        GetPmsFolio = 9,

        /// <summary>
        /// Set Pms Folio to Emenu
        /// </summary>
        [StringValue("SetPmsFolio")]
        SetPmsFolio = 10,

        /// <summary>
        /// Check in notification to Emenu
        /// </summary>
        [StringValue("SetPmsCheckedIn")]
        SetPmsCheckedIn = 11,

        /// <summary>
        /// Set Pms Guest Information to Emenu
        /// </summary>
        [StringValue("SetPmsGuestInformation")]
        SetPmsGuestInformation = 12,

        /// <summary>
        /// Get Pms Guest information from Pms via Oss
        /// </summary>
        [StringValue("GetPmsGuestInformation")]
        GetPmsGuestInformation = 13,

        /// <summary>
        /// Set forward terminal ID for a console
        /// </summary>
        [StringValue("SetForwardTerminal")]
        SetForwardTerminal = 14,


        /// <summary>
        /// Get Pms Guest information from Pms via Oss
        /// </summary>
        [StringValue("GetPmsExpressCheckout")]
        GetPmsExpressCheckout = 15,

        /// <summary>
        /// Get Pms Guest information from Pms via Oss
        /// </summary>
        [StringValue("SetPmsExpressCheckedOut")]
        SetPmsExpressCheckedOut = 16,

        /// <summary>
        /// Authentication result
        /// </summary>
        [StringValue("AuthenticateValue")]
        AuthenticateResult = 17,

        /// <summary>
        /// Set network information like internal and external IP
        /// </summary>
        [StringValue("SetNetworkInformation")]
        SetNetworkInformation = 18,

        /// <summary>
        /// Set the client type
        /// </summary>
        [StringValue("SetClientType")]
        SetClientType = 19,

        /// <summary>
        /// Notifies the connected PokeIn clients that the company timestamp has been updated
        /// </summary>
        [StringValue("CompanyTimestampsUpdated")]
        [Obsolete("Not used anymore")]
        CompanyTimestampsUpdated = 20,

        /// <summary>
        /// Connect an external listener to an agent
        /// </summary>
        [StringValue("ConnectToAgent")]
        ConnectToAgent = 21,

        /// <summary>
        /// Command output returned by Agent
        /// </summary>
        [StringValue("AgentCommandResponse")]
        AgentCommandResponse = 22,

        /// <summary>
        /// Send request to Agent
        /// </summary>
        [StringValue("AgentCommandRequest")]
        AgentCommandRequest = 23,

        /// <summary>
        /// New event for external listeners
        /// </summary>
        [StringValue("NewEventExternalListner")]
        NewEventExternalListener = 24,

        /// <summary>
        /// Push event to external listener
        /// </summary>
        [StringValue("PushEventToExternalListeners")]
        PushEventToExternalListeners = 25,

        /// <summary>
        /// Set the Operation Mode of a Client
        /// </summary>
        [StringValue("SetClientOperationMode")]
        SetClientOperationMode = 26,

        /// <summary>
        /// Get connected clients
        /// </summary>
        [StringValue("GetConnectedClients")]
        GetConnectedClients = 27,

        /// <summary>
        /// New survey result has been saved
        /// </summary>
        [StringValue("NewSurveyResult")]
        NewSurveyResult = 28,

        /// <summary>
        /// New survey result has been saved
        /// </summary>
        [StringValue("SetDeliverypointId")]
        SetDeliverypointId = 29,

        /// <summary>
        /// Command without parameters for a Client
        /// </summary>
        [StringValue("ClientCommand")]
        ClientCommand = 30,

        /// <summary>
        /// Command without parameters for a Terminal
        /// </summary>
        [StringValue("TerminalCommand")]
        TerminalCommand = 31,

        /// <summary>
        /// Execute command directly on device shell
        /// </summary>
        [StringValue("ClientCommandExecute")]
        DeviceCommandExecute = 32,

        /// <summary>
        /// Application update availalbe
        /// </summary>
        [StringValue("UpdateAvailalbe")]
        UpdateAvailable = 33,

        /// <summary>
        /// Set Cloud Environment
        /// </summary>
        [StringValue("SetCloudEnvironment")]
        SetCloudEnvironment = 34,

        /// <summary>
        /// 
        /// </summary>
        [StringValue("SendInformation")]
        SendInformation = 35,

        /// <summary>
        /// Device Unlinked
        /// </summary>
        [StringValue("DeviceUnlink")]
        DeviceUnlink = 36,

        [StringValue("SandboxConfig")]
        SandboxConfig = 37,

        [StringValue("SetMasterTab")]
        SetMasterTab = 38,

        [StringValue("DownloadUpdate")]
        DownloadUpdate = 39,

        [StringValue("InstallUpdate")]
        InstallUpdate = 40,

        [StringValue("SetPmsWakeUp")]
        SetPmsWakeUp = 41,

        [StringValue("ClearPmsWakeUp")]
        ClearPmsWakeUp = 42,

        [StringValue("MenuUpdated")]
        MenuUpdated = 43,

        [StringValue("SwitchTab")]
        SwitchTab = 44,

        [StringValue("SendJsonObject")]
        SendJsonObject = 45,

        [StringValue("RefreshContent")]
        RefreshContent = 46,

        [StringValue("PmsTerminalStatusUpdated")]
        PmsTerminalStatusUpdated = 47,

        [StringValue(nameof(UpdateDeviceToken))]
        UpdateDeviceToken = 48,

        [StringValue(nameof(ClearCompanyCredentials))]
        ClearCompanyCredentials = 49,

        [StringValue(nameof(MigrateToRestApi))]
        MigrateToRestApi = 50,

        /// <summary>
        /// Message send to client when it's about to be removed from the server
        /// </summary>
        [StringValue("Disconnect")]
        Disconnect = 1000,

        /// <summary>
        /// Set the active comet handler for the client
        /// </summary>
        [StringValue("SwtichCometHandler")]
        SwitchCometHandler = 2000,

        /// <summary>
        /// Request connection state from services (Webservice, CMS, etc..) and total connected clients
        /// </summary>
        [StringValue("SystemState")]
        SystemState = 5000,

        /// <summary>
        /// Ping/Pong
        /// </summary>
        [StringValue("Ping")]
        Ping = 9998,

        /// <summary>
        /// Pong
        /// </summary>
        [StringValue("Pong")]
        Pong = 9999,

        Test = 10000,
    }
}
