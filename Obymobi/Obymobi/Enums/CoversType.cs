﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum CoversType
    {
        [StringValue("Dependent on products")]
        DependentOnProducts = 0,

        [StringValue("Always Ask")]
        AlwaysAsk = 1,

        [StringValue("Never Ask")]
        NeverAsk = 2,

        [StringValue("Inherit from category")]
        Inherit = 3
    }
}
