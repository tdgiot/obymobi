using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum PmsReportType : int
	{
        /// <summary>
        /// Plain text
        /// </summary>
        PlainText = 0,
        /// <summary>
        /// Excel
        /// </summary>
        Excel = 1,
	}
}
