﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum WeatherClockWidgetMode
    {
        [StringValue("V1")]
        v1 = 1,
        [StringValue("V2")]
        v2 = 2,
    }
}
