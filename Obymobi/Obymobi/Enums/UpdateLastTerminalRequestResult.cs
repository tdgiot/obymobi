﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum UpdateLastTerminalRequestResult : int
    {
        /// <summary>
        /// Update last terminal result is unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Last request was successfully updated
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Terminal was not found in the database 
        /// </summary>
        [StringValue("De opgegeven terminal is niet bekend.")]
        TerminalUnknown = 200,

        /// <summary>
        /// Multiple terminals found for the specified terminal id
        /// </summary>
        [StringValue("Er zijn meerdere terminals gevonden voor de opgegeven terminal id.")]
        MultipleTerminalsFound = 300
    }
}
