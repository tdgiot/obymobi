using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum DataType : int
	{
        /// <summary>
        /// String
        /// </summary>
        String = 0,
        /// <summary>
        /// Integer
        /// </summary>
        Integer = 1,
        /// <summary>
        /// DateTime
        /// </summary>
        DateTime = 2
    }
}
