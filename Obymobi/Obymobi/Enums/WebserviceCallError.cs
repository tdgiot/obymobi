﻿using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents a generic Webservice Call Result
    /// </summary>
    public enum GenericWebserviceCallResult
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// General Exception
        /// </summary>
        [StringValue("Er is een fout opgetreden.")]
        Failure = 200,

        /// <summary>
        /// General Exception
        /// </summary>
        [StringValue("Er is een UnspecifiedEntitySaveFalseFailure opgetreden.")]
        UnspecifiedEntitySaveFalseFailure = 201,

        /// <summary>
        /// General Exception
        /// </summary>
        [StringValue("Er kon geen int geparsed worden van de opgegeven string.")]
        ParseIntError = 202,

        /// <summary>
        /// General Exception
        /// </summary>
        [StringValue("Er is een technische fout opgetreden.")]
        TechnicalFailure = 203,

        /// <summary>
        /// General Exception
        /// </summary>
        [StringValue("Opgevraagde gegevens konden niet worden gevonden.")]
        EntityNotFoundByPrimaryKey = 204,

        /// <summary>
        /// Model conversion error
        /// </summary>
        [StringValue("Models konden niet worden geconverteerd.")]
        ModelConversionError = 205
    }
}