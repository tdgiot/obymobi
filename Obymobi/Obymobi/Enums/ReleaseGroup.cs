﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum ReleaseGroup
    {
        [StringValue("Dev")]
        Dev = 1,
        [StringValue("Test")]
        Test = 2,
        [StringValue("Beta")]
        Beta = 3,
        [StringValue("All")]
        All = 10
    }
}
