using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
	public enum AnnouncementDialogType : int
	{
        OkDialog = 1,

        NotificationDialog = 2,

        ReorderDialog = 3,

        YesNoDialog = 4
	}
}
