using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum SocialmediaMessageSaveResult : int
    {
        /// <summary>
        /// Message was successfully saved
        /// </summary>
        [StringValue("Bericht is succesvol opgeslagen")]
        Success = 100,

		/// <summary>
		/// Xml Serialize Error
		/// </summary>
        [StringValue("Er is een fout opgetreden bij het opslaan van het bericht.")]
		XmlSerializeError = 201,

        /// <summary>
        /// Rating failed to save
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het opslaan van het bericht. (202)")]
        EntitySaveRecursiveFalse = 202,

        /// <summary>
        /// Rating failed to save
        /// </summary>
        [StringValue("Er is een fout opgetreden bij het opslaan van het bericht. (203)")]
        EntitySaveRecursiveException = 203,
    }
}
