﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum CloudApplication : int
    {
        /// <summary>
        /// API
        /// </summary>
        API = 0,
        /// <summary>
        /// Management
        /// </summary>
        Management = 1,
        /// <summary>
        /// Messaging
        /// </summary>
        Messaging = 2,
        /// <summary>
        /// Mobile NOC
        /// </summary>
        NOC = 3,
        /// <summary>
        /// Services
        /// </summary>
        Services = 4,
        /// <summary>
        /// NOC Service
        /// </summary>
        NOCService = 5
    }
}
