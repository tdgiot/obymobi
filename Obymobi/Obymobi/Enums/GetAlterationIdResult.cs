using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum GetAlterationIdResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// No pos alteration found for the specified id
        /// </summary>
        [StringValue("Er is geen alterations gevonden voor de opgegeven external pos alteration id.")]
        NoAlterationFound = 200,

        /// <summary>
        /// Multiple pos alterations found
        /// </summary>
        [StringValue("Er zijn meerdere alterations gevonden voor de opgegeven pos alteration id.")]
        MultipleAlterationsFound = 201
    }
}
