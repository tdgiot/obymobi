﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the result of the validation of credentials process
    /// </summary>
    public enum ValidateCredentialsResult : int
    {
        /// <summary>
        /// Authentication result is unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// User was successfully authenticated
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Invalid credentials
        /// </summary>
        [StringValue("De opgegeven aanmeldgegevens zijn niet juist.")]
        InvalidCredentials = 200,

        /// <summary>
        /// Invalid company for the company owner
        /// </summary>
        [StringValue("De opgegeven aanmeldgegevens zijn niet juist voor de gebruikte CompanyId.")]
        InvalidCompanyId = 201,

        /// <summary>
        /// Multiple users found with same username
        /// </summary>
        [StringValue("Meerdere gebruikers gevonden met dezelfde gebruikersnaam.")]
        MultipleUsersWithSameUsername = 202,

        [StringValue("Er is geen Device gevonden voor het mac adres.")]
        NoDeviceFoundForMacAddress = 203,
        
        [StringValue("Er zijn meerdere Devices gevonden voor het mac adres.")]
        MultipleDevicesFoundForMacAddress = 204,

        [StringValue("Er zijn meerdere Clients gevonden voor het Device.")]
        MultipleClientsFoundForDevice = 205,

        [StringValue("Er zijn meerdere Terminals gevonden voor het Device.")]
        MultipleTerminalsFoundForDevice = 206,

        [StringValue("Er is geen Client, Terminal of Customer gevonden voor het Device.")]
        NoTerminalClientOrCustomerFoundForDevice = 207,

        [StringValue("Er is geen Company gevonden voor de Client of Terminal gevonden voor het Device.")]
        NoCompanyFoundForClientOrTerminal = 208,

        [StringValue("De Hash van de Request is ongeldig.")]
        RequestHashIsInvalid = 209,

        [StringValue("De methode is alleen bedoelt voor Terminals.")]
        CallOnlyAllowedForTerminals = 210,

        [StringValue("De methode is alleen bedoelt voor Clients.")]
        CallOnlyAllowedForClients = 211,

        [StringValue("De Timestamp valt niet binnen de toegestane interval")]
        TimestampIsOutOfRange = 212,

        [StringValue("De methode is alleen bedoelt voor Customers.")]
        CallOnlyAllowedForCustomers = 213,

        [StringValue("Er zijn meerdere Customers gevonden voor het Device.")]
        MultipleCustomersFoundForDevice = 214,

        [StringValue("Mac adres is leeg.")]
        MacAddressOrEmailUsedAsMacAddressIsEmtpty = 215,

        CustomerIsBlacklisted = 215,
    }
}
