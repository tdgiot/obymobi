﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum TerminalLogType : int
    {
        [StringValue("Device Reboot")]
        DeviceReboot = 50,

        [StringValue("DeviceShutdown")]
        DeviceShutdown = 55,

        /// <summary>
        /// Start 
        /// </summary>
        [StringValue("Start")]
        Start = 100,

        /// <summary>
        /// Stop
        /// </summary>
        [StringValue("Stop")]
        Stop = 200,

        /// <summary>
        /// Bericht
        /// </summary>
        [StringValue("Bericht")]
        Message = 300,

        /// <summary>
        /// Automatic Switch To Normal
        /// </summary>
        [StringValue("ShippedLog")]
        ShippedLog = 301,

        /// <summary>
        /// Bericht
        /// </summary>
        [StringValue("Device veranderd")]
        DeviceChanged = 305,

        /// <summary>
        /// Bericht
        /// </summary>
        [StringValue("Kassa synchronisatie rapport")]
        PosSynchronisationReport = 310,

        /// <summary>
        /// Bericht
        /// </summary>
        [StringValue("NonQualifiedException")]
        NonQualifiedException = 400,

        /// <summary>
        /// Bericht
        /// </summary>
        [StringValue("OrderTypeNotSupported")]
        OrderTypeNotSupported = 401,

        /// <summary>
        /// Onbekende POS fout
        /// </summary>
        [StringValue("NonQualifiedPosException")]
        NonQualifiedPosException = 500,

        /// <summary>
        /// Tafel geopend fout
        /// </summary>
        [StringValue("DeliverypointLockedPosException")]
        DeliverypointLockedPosException = 501,

        /// <summary>
        /// Onjuiste product / opties configuratie
        /// </summary>
        [StringValue("InvalidProductsOrAlterationsPosException")]
        InvalidProductsOrAlterationsPosException = 502,

        /// <summary>
        /// Verzoek tot sluiten van tafels mislukt.
        /// </summary>
        [StringValue("UnlockTablesRequestFailedPosException")]
        UnlockTablesRequestFailedPosException = 503,

        /// <summary>
        /// Connectivity problem
        /// </summary>
        [StringValue("ConnectivityException")]
        ConnectivityException = 504,

        /// <summary>
        /// UnspecifiedNonFatalPosProblem
        /// </summary>
        [StringValue("UnspecifiedNonFatalPosProblem")]
        UnspecifiedNonFatalPosProblem = 505,

        /// <summary>
        /// Download started
        /// </summary>
        [StringValue("Download started")]
        DownloadStarted = 600,

        /// <summary>
        /// Download completed
        /// </summary>
        [StringValue("Download completed")]
        DownloadCompleted = 601,

        /// <summary>
        /// Download failed
        /// </summary>
        [StringValue("Download failed")]
        DownloadFailed = 602,

        /// <summary>
        /// Installation started
        /// </summary>
        [StringValue("Installation started")]
        InstallationStarted = 603,

        /// <summary>
        /// Installation completed
        /// </summary>
        [StringValue("Installation completed")]
        InstallationCompleted = 604,

        /// <summary>
        /// Installation failed
        /// </summary>
        [StringValue("Installation failed")]
        InstallationFailed = 605,

        /// <summary>
        /// Installation failed
        /// </summary>
        [StringValue("ConfigurationBackupRestored")]
        ConfigurationBackupRestored = 606,

        /// <summary>
        /// Installation failed
        /// </summary>
        [StringValue("OsUpdated")]
        OsUpdated = 607,

        /// <summary>
        /// Device froze
        /// </summary>
        [StringValue("FreezeLog")]
        FreezeLog = 608,

        /// <summary>
        /// Last terminal communication method changed
        /// </summary>
        [StringValue("CommunicationMethodChanged")]
        CommunicationMethodChanged = 700,
    }
}
