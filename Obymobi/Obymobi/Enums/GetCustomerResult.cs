using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum GetCustomerResult
    {
        Unspecified = 0,

        Success = 100,
       
        Blacklisted = 203,

		PasswordIncorrect = 204, 

        MultipleCustomersFound = 209,

        EmailUnknown = 210,

        AwaitingAttempts = 221,

        InvalidSocialMediaType = 222,

		AccountNotVerified = 224,

        TooManyAttemptsSignInTemporarilyDisabled = 996,

        InvalidHash = 997,

        AuthenticationError = 998,

        Failure = 999
    }
}
