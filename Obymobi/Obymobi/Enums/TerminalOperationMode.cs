﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum TerminalOperationMode : int
    {
        /// <summary>
        /// Unknown. Operation mode could not be determined >:( Y U DO THAT
        /// </summary>
        [StringValue("Operation mode could not be determined")]
        Unknown = 0,

        /// <summary>
        /// Terminal used in normal operation mode
        /// </summary>
        [StringValue("Normal")]
        Normal = 1,
    }
}
