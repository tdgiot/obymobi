using Dionysos;

namespace Obymobi.Enums
{
    public enum ApplessWidgetType : int
	{
        [StringValue("Page title")]
        PageTitle = 0,

        [StringValue("Carousel")]
        Carousel = 1,

        [StringValue("Action buttons")]
        ActionButtons = 2,

        [StringValue("Action button")]
        ActionButton = 3,

        [StringValue("Action banner")]
        ActionBanner = 4,

        [StringValue("Language switcher")]
        LanguageSwitcher = 5,

        [StringValue("Hero")]
        Hero = 6,

        [StringValue("Wait Time")]
        WaitTime = 7,

        [StringValue("Opening Time")]
        OpeningTime = 8,

        [StringValue("Markdown")]
        Markdown = 9
    }
}
