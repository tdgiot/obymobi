using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public enum GetPosalterationExternalIdResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// No pos alteration found for the specified id
        /// </summary>
        [StringValue("Er is geen pos alterations gevonden voor het opgegeven alteration id.")]
        NoPosalterationFound = 200,

        /// <summary>
        /// Multiple pos alterations found
        /// </summary>
        [StringValue("Er zijn meerdere pos alterations gevonden voor het opgegeven alteration id.")]
        MultiplePosalterationsFound = 201
    }
}
