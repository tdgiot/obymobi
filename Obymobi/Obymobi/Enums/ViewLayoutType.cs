﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    public enum ViewLayoutType
    {
        /// <summary>
        /// Inherit from category
        /// </summary>
        [StringValue("Inherit from category")]
        Inherit = 0,

        /// <summary>
        /// Horizontal
        /// </summary>
        [StringValue("Horizontal image")]
        HorizontalImage = 1,

        /// <summary>
        /// Vertical
        /// </summary>
        [StringValue("Vertical wide image")]
        VerticalWideImage = 2,

        /// <summary>
        /// Vertical
        /// </summary>
        [StringValue("Vertical narrow image")]
        VerticalNarrowImage = 3,

        /// <summary>
        /// Text
        /// </summary>
        [StringValue("Text")]
        Text = 5, // Yes, I skipped the 4th because its used internally in the Emenu code
    }
}
