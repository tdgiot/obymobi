using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the CurrencySaveResult
    /// </summary>
    public enum CurrencySaveResult : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// Success
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        ///// <summary>
        ///// Failed
        ///// </summary>
        //Failed = 200,

        /// <summary>
        /// NameEmpty
        /// </summary>
        [StringValue("Er is geen naam opgegeven.")]
        NameEmpty = 201,

        /// <summary>
        /// SymbolEmpty
        /// </summary>
        [StringValue("Er is geen valuta symbool opgegeven.")]
        SymbolEmpty = 202,

        /// <summary>
        /// CodeEmpty
        /// </summary>
        [StringValue("Er is geen code opgegeven.")]
        CodeEmpty = 203,

        /// <summary>
        /// CodeEmpty
        /// </summary>
        [StringValue("Code heeft niet exact 3 karakaters.")]
        CodeNotExactThreeCharacters = 203,
    }
}