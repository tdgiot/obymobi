using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    /// <summary>
    /// Enumeration which represents the result of the company owner authentication process
    /// </summary>
    public enum CompanyOwnerAuthenticationResult : int
    {
        /// <summary>
        /// Authentication result is unknown
        /// </summary>
        [StringValue("Onbekend.")]
        Unknown = 0,

        /// <summary>
        /// User was successfully authenticated
        /// </summary>
        [StringValue("Succes.")]
        Success = 100,

        /// <summary>
        /// Username was not found in the database 
        /// </summary>
        [StringValue("De opgegeven gebruikersnaam is onbekend.")]
        UsernameUnknown = 200,

        /// <summary>
        /// Incorrect password specified by the company owner
        /// </summary>
        [StringValue("Het opgegeven wachtwoord is onjuist.")]
        PasswordIncorrect = 300,

        /// <summary>
        /// Multiple company owners found for the specified username
        /// </summary>
        [StringValue("Er zijn meerdere company owners gevonden voor de opgegeven gebruikersnaam.")]
        MultipleCompanyOwnersFound = 400
    }
}
