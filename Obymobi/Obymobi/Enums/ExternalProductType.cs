﻿using Dionysos;

namespace Obymobi.Enums
{
    public enum ExternalProductType
    {
        [StringValue("Product")]
        Product = 1,

        [StringValue("Modifier Group")]
        ModifierGroup = 2,

        [StringValue("Modifier")]
        Modifier = 3,

        [StringValue("Bundle")]
        Bundle = 4
    }
}
