﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Enums
{
    /// <summary>
    /// Types of support notification
    /// </summary>
    /// <remarks>
    /// VALUES NEED TO BE POWERS OF TWO (0,1,2,4,8,16 etc..)
    /// </remarks>
    public enum SupportNotificationType
    {
        /// <summary>
        /// Inherit from category
        /// </summary>
        [StringValue("Inherit from category")]
        Inherit = 0,

        /// <summary>
        /// No notifications
        /// </summary>
        [StringValue("None")]
        None = 1,

        /// <summary>
        /// 'Retrieval' timeout
        /// </summary>
        [StringValue("'Retrieval' timeout")]
        RetrievalTimeout = 2,

        /// <summary>
        /// 'On the case' timeout
        /// </summary>
        [StringValue("'On the case' timeout")]
        OnTheCaseTimeout = 4,

        /// <summary>
        /// 'Retrieval' & 'On the case' timeouts
        /// </summary>
        [StringValue("All")]
        All = (RetrievalTimeout | OnTheCaseTimeout),
    }
}
