﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Enums
{
    public sealed class RoomControlItemType
    {
        public static readonly string Area = "Area";
        public static readonly string Section = "Section";
        public static readonly string Component = "Component";
        public static readonly string SectionItem = "Section Item";
        public static readonly string Widget = "Widget";
    }
}
