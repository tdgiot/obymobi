﻿using Obymobi.Enums;

namespace Obymobi
{
    public class MediaRatioType
    {
        #region Constructors        

        /// <summary>
        /// Initializes a new instance of the <see cref="MediaRatioType"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="mediaType">Type of the media.</param>
        /// <param name="systemName">Name of the system.</param>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public MediaRatioType(string name, MediaType mediaType, string entityType, int width, int height, SystemType systemType, DeviceType deviceType = Enums.DeviceType.Unknown, MediaTypeGroup mediaTypeGroup = MediaTypeGroup.None, 
            bool deviceIndependent = false, int jpgQuality = Obymobi.Constants.ObymobiConstants.JpgQualityMedia, bool isSelectedByDefault = false)
        {
            this.Name = name;
            this.MediaType = mediaType;
            this.EntityType = entityType;
            this.Width = width;
            this.Height = height;
            this.SystemType = systemType;
            this.DeviceType = deviceType;
            this.MediaTypeGroup = mediaTypeGroup;
            this.JpgQuality = jpgQuality;
            this.DeviceTypeIndependent = deviceIndependent;
            this.IsSelectedByDefault = isSelectedByDefault;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the media ratio type
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the media of the media ratio type
        /// </summary>
        public MediaType MediaType { get; set; }

        /// <summary>
        /// Gets or sets the type of the entity of the media ratio type
        /// </summary>
        public string EntityType { get; set; }

        /// <summary>
        /// Gets or sets the width of the media ratio type
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Gets or sets the height of the media ratio type
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets the type of the system (Crave or Otoucho)
        /// </summary>
        public SystemType SystemType { get; set; }

        /// <summary>
        /// Gets or sets the type of the device
        /// </summary>
        public DeviceType DeviceType { get; set; }

        /// <summary>
        /// Gets or sets the group to which the MediaType belong
        /// </summary>
        public MediaTypeGroup MediaTypeGroup { get; set; }

        /// <summary>
        /// Gets or sets the JPG Quality
        /// </summary>
        public int JpgQuality { get; set; }

        /// <summary>
        /// Hint used by the webservices to not strip this Media out, but add it independent of the device type.
        /// </summary>
        public bool DeviceTypeIndependent { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is portrait.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is portrait; otherwise, <c>false</c>.
        /// </value>
        public bool IsPortrait
        {
            get
            {
                return this.Height > this.Width;
            }
        }

        /// <summary>
        /// Supported image extensions. If null, then the image extensions are not limited.
        /// </summary>
        public string[] SupportedImageExtensions { get; set; }

        public bool IsSelectedByDefault { get; set; }
        
        #endregion
    }
}
