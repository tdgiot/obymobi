﻿using Obymobi.Enums;
using System;

namespace Obymobi
{
    public class UIColor
    {
        public UIColorType Type { get; private set; }
        public int Color { get; private set; }
        public UIColorGroup Group { get; private set; }
        public bool OpacitySupported { get; private set; }

        public String PreviewName { get; private set; }
        public String PreviewFile { get; private set; }

        public UIColor(UIColorType type, int alpha, int red, int green, int blue, UIColorGroup group, bool opacitySupported = true)
            : this(type, alpha, red, green, blue, group, string.Empty, string.Empty, opacitySupported)
        {
        }

        public UIColor(UIColorType type, int alpha, int red, int green, int blue, UIColorGroup group, String previewName, String previewFile, bool opacitySupported = true)
        {
            this.Type = type;
            this.Color = alpha << 24 | red << 16 | green << 8 | blue;
            this.Group = group;
            this.PreviewName = previewName;
            this.PreviewFile = previewFile;
            this.OpacitySupported = opacitySupported;
        }
    }
}
