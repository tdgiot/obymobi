﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Obymobi.Resources;

namespace Obymobi
{
    /// <summary>
    /// Class which represents a currency and holds mappings of all currencies. 
    /// </summary>
    /// <remarks>StyleCop and ReSharper naming rules have been disabled for better readability purposes.</remarks>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "One time allowance of underscores.")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "One time ignoring of spelling.")]
    // ReSharper disable InconsistentNaming
    public class Currency
    {
        #region Fields

        /// <summary>The dictionary containing the currency, mapped by currency code.</summary>
        public static readonly Dictionary<string, Currency> Mappings = new Dictionary<string, Currency>();

        #region Currency list

        /// <summary>Afghan afghani (AFN)</summary>
        public static readonly Currency Afghan_afghani = new Currency("AFN", "؋", Strings.Currency_Afghan_afghani);
        
        /// <summary>Albanian lek (ALL)</summary>
        public static readonly Currency Albanian_lek = new Currency("ALL", "L", Strings.Currency_Albanian_lek);
        
        /// <summary>Algerian dinar (DZD)</summary>
        public static readonly Currency Algerian_dinar = new Currency("DZD", "د.ج", Strings.Currency_Algerian_dinar);
        
        /// <summary>Angolan kwanza (AOA)</summary>
        public static readonly Currency Angolan_kwanza = new Currency("AOA", "Kz", Strings.Currency_Angolan_kwanza);
        
        /// <summary>Argentine peso (ARS)</summary>
        public static readonly Currency Argentine_peso = new Currency("ARS", "$", Strings.Currency_Argentine_peso);
        
        /// <summary>Armenian dram (AMD)</summary>
        public static readonly Currency Armenian_dram = new Currency("AMD", string.Empty, Strings.Currency_Armenian_dram);
        
        /// <summary>Aruban florin (AWG)</summary>
        public static readonly Currency Aruban_florin = new Currency("AWG", "ƒ", Strings.Currency_Aruban_florin);
        
        /// <summary>Australian dollar (AUD)</summary>
        public static readonly Currency Australian_dollar = new Currency("AUD", "$", Strings.Currency_Australian_dollar);
        
        /// <summary>Azerbaijani manat (AZN)</summary>
        public static readonly Currency Azerbaijani_manat = new Currency("AZN", string.Empty, Strings.Currency_Azerbaijani_manat);
        
        /// <summary>Bahamian dollar (BSD)</summary>
        public static readonly Currency Bahamian_dollar = new Currency("BSD", "$", Strings.Currency_Bahamian_dollar);
        
        /// <summary>Bahraini dinar (BHD)</summary>
        public static readonly Currency Bahraini_dinar = new Currency("BHD", ".د.ب", Strings.Currency_Bahraini_dinar);
        
        /// <summary>Bangladeshi taka (BDT)</summary>
        public static readonly Currency Bangladeshi_taka = new Currency("BDT", "৳", Strings.Currency_Bangladeshi_taka);
        
        /// <summary>Barbadian dollar (BBD)</summary>
        public static readonly Currency Barbadian_dollar = new Currency("BBD", "$", Strings.Currency_Barbadian_dollar);
        
        /// <summary>Belarusian ruble (BYR)</summary>
        public static readonly Currency Belarusian_ruble = new Currency("BYR", "Br", Strings.Currency_Belarusian_ruble);
        
        /// <summary>Belize dollar (BZD)</summary>
        public static readonly Currency Belize_dollar = new Currency("BZD", "$", Strings.Currency_Belize_dollar);
        
        /// <summary>Bermudian dollar (BMD)</summary>
        public static readonly Currency Bermudian_dollar = new Currency("BMD", "$", Strings.Currency_Bermudian_dollar);
        
        /// <summary>Bhutanese ngultrum (BTN)</summary>
        public static readonly Currency Bhutanese_ngultrum = new Currency("BTN", "Nu.", Strings.Currency_Bhutanese_ngultrum);
        
        /// <summary>Bolivian boliviano (BOB)</summary>
        public static readonly Currency Bolivian_boliviano = new Currency("BOB", "Bs.", Strings.Currency_Bolivian_boliviano);
        
        /// <summary>Bosnia and Herzegovina convertible mark (BAM)</summary>
        public static readonly Currency Bosnia_and_Herzegovina_convertible_mark = new Currency("BAM", "KM", Strings.Currency_Bosnia_and_Herzegovina_convertible_mark);
        
        /// <summary>Botswana pula (BWP)</summary>
        public static readonly Currency Botswana_pula = new Currency("BWP", "P", Strings.Currency_Botswana_pula);
        
        /// <summary>Brazilian real (BRL)</summary>
        public static readonly Currency Brazilian_real = new Currency("BRL", "R$", Strings.Currency_Brazilian_real);

        /// <summary>British pound (GBP)</summary>
        public static readonly Currency British_pound = new Currency("GBP", "£", Strings.Currency_British_pound);

        /// <summary>Brunei dollar (BND)</summary>
        public static readonly Currency Brunei_dollar = new Currency("BND", "$", Strings.Currency_Brunei_dollar);
        
        /// <summary>Bulgarian lev (BGN)</summary>
        public static readonly Currency Bulgarian_lev = new Currency("BGN", "лв", Strings.Currency_Bulgarian_lev);
        
        /// <summary>Burundian franc (BIF)</summary>
        public static readonly Currency Burundian_franc = new Currency("BIF", "Fr", Strings.Currency_Burundian_franc);
        
        /// <summary>Cambodian riel (KHR)</summary>
        public static readonly Currency Cambodian_riel = new Currency("KHR", "៛", Strings.Currency_Cambodian_riel);
        
        /// <summary>Canadian dollar (CAD)</summary>
        public static readonly Currency Canadian_dollar = new Currency("CAD", "$", Strings.Currency_Canadian_dollar);
        
        /// <summary>Cape Verdean escudo (CVE)</summary>
        public static readonly Currency Cape_Verdean_escudo = new Currency("CVE", "$", Strings.Currency_Cape_Verdean_escudo);
        
        /// <summary>Cayman Islands dollar (KYD)</summary>
        public static readonly Currency Cayman_Islands_dollar = new Currency("KYD", "$", Strings.Currency_Cayman_Islands_dollar);
        
        /// <summary>Central African CFA franc (XAF)</summary>
        public static readonly Currency Central_African_CFA_franc = new Currency("XAF", "Fr", Strings.Currency_Central_African_CFA_franc);
        
        /// <summary>Chilean peso (CLP)</summary>
        public static readonly Currency Chilean_peso = new Currency("CLP", "$", Strings.Currency_Chilean_peso);
        
        /// <summary>Chinese yuan (CNY)</summary>
        public static readonly Currency Chinese_yuan = new Currency("CNY", "¥", Strings.Currency_Chinese_yuan);
        
        /// <summary>Colombian peso (COP)</summary>
        public static readonly Currency Colombian_peso = new Currency("COP", "$", Strings.Currency_Colombian_peso);
        
        /// <summary>Comorian franc (KMF)</summary>
        public static readonly Currency Comorian_franc = new Currency("KMF", "Fr", Strings.Currency_Comorian_franc);
        
        /// <summary>Congolese franc (CDF)</summary>
        public static readonly Currency Congolese_franc = new Currency("CDF", "Fr", Strings.Currency_Congolese_franc);
        
        /// <summary>Costa Rican colón (CRC)</summary>
        public static readonly Currency Costa_Rican_colon = new Currency("CRC", "¢", Strings.Currency_Costa_Rican_colon);
        
        /// <summary>Croatian kuna (HRK)</summary>
        public static readonly Currency Croatian_kuna = new Currency("HRK", "kn", Strings.Currency_Croatian_kuna);
        
        /// <summary>Cuban convertible peso (CUC)</summary>
        public static readonly Currency Cuban_convertible_peso = new Currency("CUC", "$", Strings.Currency_Cuban_convertible_peso);
        
        /// <summary>Cuban peso (CUP)</summary>
        public static readonly Currency Cuban_peso = new Currency("CUP", "$", Strings.Currency_Cuban_peso);
        
        /// <summary>Czech koruna (CZK)</summary>
        public static readonly Currency Czech_koruna = new Currency("CZK", "Kc", Strings.Currency_Czech_koruna);
        
        /// <summary>Danish krone (DKK)</summary>
        public static readonly Currency Danish_krone = new Currency("DKK", "kr", Strings.Currency_Danish_krone);
        
        /// <summary>Djiboutian franc (DJF)</summary>
        public static readonly Currency Djiboutian_franc = new Currency("DJF", "Fr", Strings.Currency_Djiboutian_franc);
        
        /// <summary>Dominican peso (DOP)</summary>
        public static readonly Currency Dominican_peso = new Currency("DOP", "$", Strings.Currency_Dominican_peso);
        
        /// <summary>Egyptian pound (EGP)</summary>
        public static readonly Currency Egyptian_pound = new Currency("EGP", "£", Strings.Currency_Egyptian_pound);
        
        /// <summary>Eritrean nakfa (ERN)</summary>
        public static readonly Currency Eritrean_nakfa = new Currency("ERN", "Nfk", Strings.Currency_Eritrean_nakfa);
        
        /// <summary>Euro (EUR)</summary>
        public static readonly Currency Euro = new Currency("EUR", "€", Strings.Currency_Euro);
        
        /// <summary>Ethiopian birr (ETB)</summary>
        public static readonly Currency Ethiopian_birr = new Currency("ETB", "Br", Strings.Currency_Ethiopian_birr);
        
        /// <summary>Falkland Islands pound (FKP)</summary>
        public static readonly Currency Falkland_Islands_pound = new Currency("FKP", "£", Strings.Currency_Falkland_Islands_pound);
        
        /// <summary>Fijian dollar (FJD)</summary>
        public static readonly Currency Fijian_dollar = new Currency("FJD", "$", Strings.Currency_Fijian_dollar);
        
        /// <summary>Gambian dalasi (GMD)</summary>
        public static readonly Currency Gambian_dalasi = new Currency("GMD", "D", Strings.Currency_Gambian_dalasi);
        
        /// <summary>Georgian lari (GEL)</summary>
        public static readonly Currency Georgian_lari = new Currency("GEL", "ლ", Strings.Currency_Georgian_lari);
        
        /// <summary>Ghana cedi (GHS)</summary>
        public static readonly Currency Ghana_cedi = new Currency("GHS", "₵", Strings.Currency_Ghana_cedi);
        
        /// <summary>Gibraltar pound (GIP)</summary>
        public static readonly Currency Gibraltar_pound = new Currency("GIP", "£", Strings.Currency_Gibraltar_pound);
        
        /// <summary>Guatemalan quetzal (GTQ)</summary>
        public static readonly Currency Guatemalan_quetzal = new Currency("GTQ", "Q", Strings.Currency_Guatemalan_quetzal);
        
        /// <summary>Guinean franc (GNF)</summary>
        public static readonly Currency Guinean_franc = new Currency("GNF", "Fr", Strings.Currency_Guinean_franc);
        
        /// <summary>Guyanese dollar (GYD)</summary>
        public static readonly Currency Guyanese_dollar = new Currency("GYD", "$", Strings.Currency_Guyanese_dollar);
        
        /// <summary>Haitian gourde (HTG)</summary>
        public static readonly Currency Haitian_gourde = new Currency("HTG", "G", Strings.Currency_Haitian_gourde);
        
        /// <summary>Honduran lempira (HNL)</summary>
        public static readonly Currency Honduran_lempira = new Currency("HNL", "L", Strings.Currency_Honduran_lempira);
        
        /// <summary>Hong Kong dollar (HKD)</summary>
        public static readonly Currency Hong_Kong_dollar = new Currency("HKD", "$", Strings.Currency_Hong_Kong_dollar);
        
        /// <summary>Hungarian forint (HUF)</summary>
        public static readonly Currency Hungarian_forint = new Currency("HUF", "Ft", Strings.Currency_Hungarian_forint);
        
        /// <summary>Icelandic króna (ISK)</summary>
        public static readonly Currency Icelandic_króna = new Currency("ISK", "kr", Strings.Currency_Icelandic_krona);
        
        /// <summary>Indian rupee (INR)</summary>
        public static readonly Currency Indian_rupee = new Currency("INR", "₹", Strings.Currency_Indian_rupee);
        
        /// <summary>Indonesian rupiah (IDR)</summary>
        public static readonly Currency Indonesian_rupiah = new Currency("IDR", "Rp", Strings.Currency_Indonesian_rupiah);
        
        /// <summary>Iranian rial (IRR)</summary>
        public static readonly Currency Iranian_rial = new Currency("IRR", "﷼", Strings.Currency_Iranian_rial);
        
        /// <summary>Iraqi dinar (IQD)</summary>
        public static readonly Currency Iraqi_dinar = new Currency("IQD", "ع.د", Strings.Currency_Iraqi_dinar);
        
        /// <summary>Manx pound (IMP)</summary>
        public static readonly Currency Manx_pound = new Currency("IMP", "£", Strings.Currency_Manx_pound);
        
        /// <summary>Israeli new shekel (ILS)</summary>
        public static readonly Currency Israeli_new_shekel = new Currency("ILS", "₪", Strings.Currency_Israeli_new_shekel);
        
        /// <summary>Jamaican dollar (JMD)</summary>
        public static readonly Currency Jamaican_dollar = new Currency("JMD", "$", Strings.Currency_Jamaican_dollar);
        
        /// <summary>Japanese yen (JPY)</summary>
        public static readonly Currency Japanese_yen = new Currency("JPY", "¥", Strings.Currency_Japanese_yen);
        
        /// <summary>Jersey pound (JEP)</summary>
        public static readonly Currency Jersey_pound = new Currency("JEP", "£", Strings.Currency_Jersey_pound);
        
        /// <summary>Jordanian dinar (JOD)</summary>
        public static readonly Currency Jordanian_dinar = new Currency("JOD", "د.ا", Strings.Currency_Jordanian_dinar);
        
        /// <summary>Kazakhstani tenge (KZT)</summary>
        public static readonly Currency Kazakhstani_tenge = new Currency("KZT", string.Empty, Strings.Currency_Kazakhstani_tenge);
        
        /// <summary>Kenyan shilling (KES)</summary>
        public static readonly Currency Kenyan_shilling = new Currency("KES", "Sh", Strings.Currency_Kenyan_shilling);
        
        /// <summary>North Korean won (KPW)</summary>
        public static readonly Currency North_Korean_won = new Currency("KPW", "₩", Strings.Currency_North_Korean_won);
        
        /// <summary>South Korean won (KRW)</summary>
        public static readonly Currency South_Korean_won = new Currency("KRW", "₩", Strings.Currency_South_Korean_won);
        
        /// <summary>Kuwaiti dinar (KWD)</summary>
        public static readonly Currency Kuwaiti_dinar = new Currency("KWD", "د.ك", Strings.Currency_Kuwaiti_dinar);
        
        /// <summary>Kyrgyzstani som (KGS)</summary>
        public static readonly Currency Kyrgyzstani_som = new Currency("KGS", "лв", Strings.Currency_Kyrgyzstani_som);
        
        /// <summary>Lao kip (LAK)</summary>
        public static readonly Currency Lao_kip = new Currency("LAK", "₭", Strings.Currency_Lao_kip);
        
        /// <summary>Lebanese pound (LBP)</summary>
        public static readonly Currency Lebanese_pound = new Currency("LBP", "ل.ل", Strings.Currency_Lebanese_pound);
        
        /// <summary>Lesotho loti (LSL)</summary>
        public static readonly Currency Lesotho_loti = new Currency("LSL", "L", Strings.Currency_Lesotho_loti);
        
        /// <summary>Liberian dollar (LRD)</summary>
        public static readonly Currency Liberian_dollar = new Currency("LRD", "$", Strings.Currency_Liberian_dollar);
        
        /// <summary>Libyan dinar (LYD)</summary>
        public static readonly Currency Libyan_dinar = new Currency("LYD", "ل.د", Strings.Currency_Libyan_dinar);
        
        /// <summary>Macanese pataca (MOP)</summary>
        public static readonly Currency Macanese_pataca = new Currency("MOP", "P", Strings.Currency_Macanese_pataca);
        
        /// <summary>Macedonian denar (MKD)</summary>
        public static readonly Currency Macedonian_denar = new Currency("MKD", "ден", Strings.Currency_Macedonian_denar);
        
        /// <summary>Malagasy ariary (MGA)</summary>
        public static readonly Currency Malagasy_ariary = new Currency("MGA", "Ar", Strings.Currency_Malagasy_ariary);
        
        /// <summary>Malawian kwacha (MWK)</summary>
        public static readonly Currency Malawian_kwacha = new Currency("MWK", "MK", Strings.Currency_Malawian_kwacha);
        
        /// <summary>Malaysian ringgit (MYR)</summary>
        public static readonly Currency Malaysian_ringgit = new Currency("MYR", "RM", Strings.Currency_Malaysian_ringgit);
        
        /// <summary>Maldivian rufiyaa (MVR)</summary>
        public static readonly Currency Maldivian_rufiyaa = new Currency("MVR", ".ރ", Strings.Currency_Maldivian_rufiyaa);
        
        /// <summary>Mauritanian ouguiya (MRO)</summary>
        public static readonly Currency Mauritanian_ouguiya = new Currency("MRO", "UM", Strings.Currency_Mauritanian_ouguiya);
        
        /// <summary>Mauritian rupee (MUR)</summary>
        public static readonly Currency Mauritian_rupee = new Currency("MUR", "₨", Strings.Currency_Mauritian_rupee);
        
        /// <summary>Mexican peso (MXN)</summary>
        public static readonly Currency Mexican_peso = new Currency("MXN", "$", Strings.Currency_Mexican_peso);
        
        /// <summary>Moldovan leu (MDL)</summary>
        public static readonly Currency Moldovan_leu = new Currency("MDL", "L", Strings.Currency_Moldovan_leu);
        
        /// <summary>Mongolian tögrög (MNT)</summary>
        public static readonly Currency Mongolian_togrog = new Currency("MNT", "₮", Strings.Currency_Mongolian_togrog);
        
        /// <summary>Moroccan dirham (MAD)</summary>
        public static readonly Currency Moroccan_dirham = new Currency("MAD", "د.م.", Strings.Currency_Moroccan_dirham);
        
        /// <summary>Mozambican metical (MZN)</summary>
        public static readonly Currency Mozambican_metical = new Currency("MZN", "MT", Strings.Currency_Mozambican_metical);
        
        /// <summary>Burmese kyat (MMK)</summary>
        public static readonly Currency Burmese_kyat = new Currency("MMK", "Ks", Strings.Currency_Burmese_kyat);
        
        /// <summary>Namibian dollar (NAD)</summary>
        public static readonly Currency Namibian_dollar = new Currency("NAD", "$", Strings.Currency_Namibian_dollar);
        
        /// <summary>Nepalese rupee (NPR)</summary>
        public static readonly Currency Nepalese_rupee = new Currency("NPR", "₨", Strings.Currency_Nepalese_rupee);
        
        /// <summary>Nicaraguan córdoba (NIO)</summary>
        public static readonly Currency Nicaraguan_cordoba = new Currency("NIO", "C$", Strings.Currency_Nicaraguan_cordoba);
        
        /// <summary>Nigerian naira (NGN)</summary>
        public static readonly Currency Nigerian_naira = new Currency("NGN", "₦", Strings.Currency_Nigerian_naira);
        
        /// <summary>Norwegian krone (NOK)</summary>
        public static readonly Currency Norwegian_krone = new Currency("NOK", "kr", Strings.Currency_Norwegian_krone);
        
        /// <summary>Omani rial (OMR)</summary>
        public static readonly Currency Omani_rial = new Currency("OMR", "ر.ع.", Strings.Currency_Omani_rial);
        
        /// <summary>Pakistani rupee (PKR)</summary>
        public static readonly Currency Pakistani_rupee = new Currency("PKR", "₨", Strings.Currency_Pakistani_rupee);
        
        /// <summary>Panamanian balboa (PAB)</summary>
        public static readonly Currency Panamanian_balboa = new Currency("PAB", "B/.", Strings.Currency_Panamanian_balboa);
        
        /// <summary>Papua New Guinean kina (PGK)</summary>
        public static readonly Currency Papua_New_Guinean_kina = new Currency("PGK", "K", Strings.Currency_Papua_New_Guinean_kina);
        
        /// <summary>Paraguayan guaraní (PYG)</summary>
        public static readonly Currency Paraguayan_guaraní = new Currency("PYG", "₲", Strings.Currency_Paraguayan_guarani);
        
        /// <summary>Peruvian nuevo sol (PEN)</summary>
        public static readonly Currency Peruvian_nuevo_sol = new Currency("PEN", "S/.", Strings.Currency_Peruvian_nuevo_sol);
        
        /// <summary>Philippine peso (PHP)</summary>
        public static readonly Currency Philippine_peso = new Currency("PHP", "₱", Strings.Currency_Philippine_peso);
        
        /// <summary>New Zealand dollar (NZD)</summary>
        public static readonly Currency New_Zealand_dollar = new Currency("NZD", "$", Strings.Currency_New_Zealand_dollar);
        
        /// <summary>Polish zloty (PLN)</summary>
        public static readonly Currency Polish_zloty = new Currency("PLN", "zl", Strings.Currency_Polish_zloty);
        
        /// <summary>Qatari riyal (QAR)</summary>
        public static readonly Currency Qatari_riyal = new Currency("QAR", "ر.ق", Strings.Currency_Qatari_riyal);
        
        /// <summary>Romanian leu (RON)</summary>
        public static readonly Currency Romanian_leu = new Currency("RON", "lei", Strings.Currency_Romanian_leu);
        
        /// <summary>Russian ruble (RUB)</summary>
        public static readonly Currency Russian_ruble = new Currency("RUB", "RUB", Strings.Currency_Russian_ruble);
        
        /// <summary>Rwandan franc (RWF)</summary>
        public static readonly Currency Rwandan_franc = new Currency("RWF", "Fr", Strings.Currency_Rwandan_franc);
        
        /// <summary>Saint Helena pound (SHP)</summary>
        public static readonly Currency Saint_Helena_pound = new Currency("SHP", "£", Strings.Currency_Saint_Helena_pound);
        
        /// <summary>East Caribbean dollar (XCD)</summary>
        public static readonly Currency East_Caribbean_dollar = new Currency("XCD", "$", Strings.Currency_East_Caribbean_dollar);
        
        /// <summary>Samoan tala (WST)</summary>
        public static readonly Currency Samoan_tala = new Currency("WST", "T", Strings.Currency_Samoan_tala);
        
        /// <summary>São Tomé and Príncipe dobra (STD)</summary>
        public static readonly Currency Sao_Tome_and_Principe_dobra = new Currency("STD", "Db", Strings.Currency_Sao_Tome_and_Principe_dobra);
        
        /// <summary>Saudi riyal (SAR)</summary>
        public static readonly Currency Saudi_riyal = new Currency("SAR", "ر.س", Strings.Currency_Saudi_riyal);
        
        /// <summary>Serbian dinar (RSD)</summary>
        public static readonly Currency Serbian_dinar = new Currency("RSD", "дин.", Strings.Currency_Serbian_dinar);
        
        /// <summary>Seychellois rupee (SCR)</summary>
        public static readonly Currency Seychellois_rupee = new Currency("SCR", "₨", Strings.Currency_Seychellois_rupee);
        
        /// <summary>Sierra Leonean leone (SLL)</summary>
        public static readonly Currency Sierra_Leonean_leone = new Currency("SLL", "Le", Strings.Currency_Sierra_Leonean_leone);
        
        /// <summary>Singapore dollar (SGD)</summary>
        public static readonly Currency Singapore_dollar = new Currency("SGD", "$", Strings.Currency_Singapore_dollar);
        
        /// <summary>Netherlands Antillean guilder (ANG)</summary>
        public static readonly Currency Netherlands_Antillean_guilder = new Currency("ANG", "ƒ", Strings.Currency_Netherlands_Antillean_guilder);
        
        /// <summary>Solomon Islands dollar (SBD)</summary>
        public static readonly Currency Solomon_Islands_dollar = new Currency("SBD", "$", Strings.Currency_Solomon_Islands_dollar);
        
        /// <summary>Somali shilling (SOS)</summary>
        public static readonly Currency Somali_shilling = new Currency("SOS", "Sh", Strings.Currency_Somali_shilling);
       
        /// <summary>South Sudanese pound (SSP)</summary>
        public static readonly Currency South_Sudanese_pound = new Currency("SSP", "£", Strings.Currency_South_Sudanese_pound);
        
        /// <summary>Sri Lankan rupee (LKR)</summary>
        public static readonly Currency Sri_Lankan_rupee = new Currency("LKR", "Rs", Strings.Currency_Sri_Lankan_rupee);
        
        /// <summary>Sudanese pound (SDG)</summary>
        public static readonly Currency Sudanese_pound = new Currency("SDG", "ج.س.", Strings.Currency_Sudanese_pound);
        
        /// <summary>Surinamese dollar (SRD)</summary>
        public static readonly Currency Surinamese_dollar = new Currency("SRD", "$", Strings.Currency_Surinamese_dollar);
        
        /// <summary>Swazi lilangeni (SZL)</summary>
        public static readonly Currency Swazi_lilangeni = new Currency("SZL", "L", Strings.Currency_Swazi_lilangeni);
        
        /// <summary>Swedish krona (SEK)</summary>
        public static readonly Currency Swedish_krona = new Currency("SEK", "kr", Strings.Currency_Swedish_krona);
        
        /// <summary>Swiss franc (CHF)</summary>
        public static readonly Currency Swiss_franc = new Currency("CHF", "Fr", Strings.Currency_Swiss_franc);
        
        /// <summary>Syrian pound (SYP)</summary>
        public static readonly Currency Syrian_pound = new Currency("SYP", "£", Strings.Currency_Syrian_pound);
        
        /// <summary>New Taiwan dollar (TWD)</summary>
        public static readonly Currency New_Taiwan_dollar = new Currency("TWD", "$", Strings.Currency_New_Taiwan_dollar);
        
        /// <summary>Tajikistani somoni (TJS)</summary>
        public static readonly Currency Tajikistani_somoni = new Currency("TJS", "ЅМ", Strings.Currency_Tajikistani_somoni);
        
        /// <summary>Tanzanian shilling (TZS)</summary>
        public static readonly Currency Tanzanian_shilling = new Currency("TZS", "Sh", Strings.Currency_Tanzanian_shilling);
        
        /// <summary>Thai baht (THB)</summary>
        public static readonly Currency Thai_baht = new Currency("THB", "฿", Strings.Currency_Thai_baht);
        
        /// <summary>West African CFA franc (XOF)</summary>
        public static readonly Currency West_African_CFA_franc = new Currency("XOF", "Fr", Strings.Currency_West_African_CFA_franc);
        
        /// <summary>Tongan pa?anga[O] (TOP)</summary>
        public static readonly Currency Tongan_pa_anga = new Currency("TOP", "T$", Strings.Currency_Tongan_pa_anga);
        
        /// <summary>Transnistrian ruble (PRB)</summary>
        public static readonly Currency Transnistrian_ruble = new Currency("PRB", "р.", Strings.Currency_Transnistrian_ruble);
        
        /// <summary>Trinidad and Tobago dollar (TTD)</summary>
        public static readonly Currency Trinidad_and_Tobago_dollar = new Currency("TTD", "$", Strings.Currency_Trinidad_and_Tobago_dollar);
        
        /// <summary>Tunisian dinar (TND)</summary>
        public static readonly Currency Tunisian_dinar = new Currency("TND", "د.ت", Strings.Currency_Tunisian_dinar);
        
        /// <summary>Turkish lira (TRY)</summary>
        public static readonly Currency Turkish_lira = new Currency("TRY", string.Empty, Strings.Currency_Turkish_lira);
        
        /// <summary>Turkmenistan manat (TMT)</summary>
        public static readonly Currency Turkmenistan_manat = new Currency("TMT", "m", Strings.Currency_Turkmenistan_manat);
        
        /// <summary>Ugandan shilling (UGX)</summary>
        public static readonly Currency Ugandan_shilling = new Currency("UGX", "Sh", Strings.Currency_Ugandan_shilling);
        
        /// <summary>Ukrainian hryvnia (UAH)</summary>
        public static readonly Currency Ukrainian_hryvnia = new Currency("UAH", "₴", Strings.Currency_Ukrainian_hryvnia);
        
        /// <summary>United Arab Emirates dirham (AED)</summary>
        public static readonly Currency United_Arab_Emirates_dirham = new Currency("AED", "د.إ", Strings.Currency_United_Arab_Emirates_dirham);
        
        /// <summary>United States dollar (USD)</summary>
        public static readonly Currency United_States_dollar = new Currency("USD", "$", Strings.Currency_United_States_dollar);
        
        /// <summary>Uruguayan peso (UYU)</summary>
        public static readonly Currency Uruguayan_peso = new Currency("UYU", "$", Strings.Currency_Uruguayan_peso);
        
        /// <summary>Uzbekistani som (UZS)</summary>
        public static readonly Currency Uzbekistani_som = new Currency("UZS", string.Empty, Strings.Currency_Uzbekistani_som);
        
        /// <summary>Vanuatu vatu (VUV)</summary>
        public static readonly Currency Vanuatu_vatu = new Currency("VUV", "Vt", Strings.Currency_Vanuatu_vatu);
        
        /// <summary>Venezuelan bolívar (VEF)</summary>
        public static readonly Currency Venezuelan_bolivar = new Currency("VEF", "Bs F", Strings.Currency_Venezuelan_bolivar);
        
        /// <summary>Vietnamese dong (VND)</summary>
        public static readonly Currency Vietnamese_dong = new Currency("VND", "₫", Strings.Currency_Vietnamese_dong);

        /// <summary>CFP franc (XPF)</summary>
        public static readonly Currency CFP_franc = new Currency("XPF", "Fr", Strings.Currency_CFP_franc);
        
        /// <summary>Yemeni rial (ZMW)</summary>
        public static readonly Currency Yemeni_rial = new Currency("ZMW", "ZK", Strings.Currency_Yemeni_rial);
        
        /// <summary>Zambian kwacha (BWP)</summary>
        public static readonly Currency Zambian_kwacha = new Currency("BWP", "P", Strings.Currency_Zambian_kwacha);
        
        /// <summary>South African rand (USD)</summary>
        public static readonly Currency South_African_rand = new Currency("ZAR", "R", Strings.Currency_South_African_rand);

        #endregion

        #region Unused currency list because of missing ISO 4217 currency code

        /*
        /// <summary>Faroese króna ()</summary>
        public static readonly Currency Faroese_krona = new Currency(string.Empty, "kr", Strings.Currency_Faroese_króna);

        /// <summary>British Virgin Islands dollar ()</summary>
        public static readonly Currency British_Virgin_Islands_dollar = new Currency(string.Empty, "$", Strings.Currency_British_Virgin_Islands_dollar);

        /// <summary>Cook Islands dollar ()</summary>
        public static readonly Currency Cook_Islands_dollar = new Currency(string.Empty, "$", Strings.Currency_Cook_Islands_dollar);

        /// <summary>Guernsey pound ((none))</summary>
        public static readonly Currency Guernsey_pound = new Currency(string.Empty, "£", Strings.Currency_Guernsey_pound);

        /// <summary>Kiribati dollar ()</summary>
        public static readonly Currency Kiribati_dollar = new Currency(string.Empty, "$", Strings.Currency_Kiribati_dollar);

        /// <summary>Micronesian dollar ()</summary>
        public static readonly Currency Micronesian_dollar = new Currency(string.Empty, "$", Strings.Currency_Micronesian_dollar);

        /// <summary>Nauruan dollar ()</summary>
        public static readonly Currency Nauruan_dollar = new Currency(string.Empty, "$", Strings.Currency_Nauruan_dollar);

        /// <summary>Niue dollar ()</summary>
        public static readonly Currency Niue_dollar = new Currency(string.Empty, "$", Strings.Currency_Niue_dollar);

        /// <summary>Palauan dollar ()</summary>
        public static readonly Currency Palauan_dollar = new Currency(string.Empty, "$", Strings.Currency_Palauan_dollar);

        /// <summary>Pitcairn Islands dollar ()</summary>
        public static readonly Currency Pitcairn_Islands_dollar = new Currency(string.Empty, "$", Strings.Currency_Pitcairn_Islands_dollar);

        /// <summary>South Georgia and the South Sandwich Islands pound ()</summary>
        public static readonly Currency South_Georgia_and_the_South_Sandwich_Islands_pound = new Currency(string.Empty, "£", Strings.Currency_South_Georgia_and_the_South_Sandwich_Islands_pound);

        /// <summary>Tuvaluan dollar ()</summary>
        public static readonly Currency Tuvaluan_dollar = new Currency(string.Empty, "$", Strings.Currency_Tuvaluan_dollar);

        /// <summary>Zimbabwean Bond ()</summary>
        public static readonly Currency Zimbabwean_Bond = new Currency(string.Empty, string.Empty, Strings.Currency_Zimbabwean_Bond);

        */

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Currency"/> class.
        /// </summary>
        /// <param name="code">The code of the currency.</param>
        /// <param name="symbol">The symbol of the symbol.</param>
        /// <param name="name">The name of the currency.</param>
        private Currency(string code, string symbol, string name)
        {
            this.Code = code;
            this.Symbol = symbol;
            this.Name = name;

            Mappings[code] = this;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the code of the currency.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the name of the currency.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the symbol of the currency.
        /// </summary>
        public string Symbol { get; set; }

        #endregion

        #region Operators

        /// <summary>
        /// Converts the specified string to the corresponding <see cref="Currency"/> instance. 
        /// </summary>
        /// <param name="code">The code of the currency.</param>
        public static explicit operator Currency(string code)
        {
            Currency result;
            if (Mappings.TryGetValue(code.ToUpper(), out result))
            {
                return result;
            }

            throw new InvalidCastException();
        }

        #endregion
    }
}
