﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Obymobi.Resources;

namespace Obymobi
{
    /// <summary>
    /// Class which represents a country and holds mappings of all countries.
    /// </summary>
    /// <remarks>StyleCop and ReSharper naming rules have been disabled for better readability purposes.</remarks>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "One time allowance of underscores.")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "One time ignoring of spelling.")]
    // ReSharper disable InconsistentNaming
    public class Country
    {
        #region Fields

        /// <summary>The dictionary containing the countries, mapped by country code.</summary>
        public static readonly Dictionary<string, Country> Mappings = new Dictionary<string, Country>();

        #region Country list

        /// <summary>Afghanistan (AFG)</summary>
        public static readonly Country Afghanistan = new Country("AF", "AFG", new[] { Currency.Afghan_afghani }, Strings.Country_Afghanistan);
        
        /// <summary>Albania (ALB)</summary>
        public static readonly Country Albania = new Country("AL", "ALB", new[] { Currency.Albanian_lek }, Strings.Country_Albania);
        
        /// <summary>Algeria (DZA)</summary>
        public static readonly Country Algeria = new Country("DZ", "DZA", new[] { Currency.Algerian_dinar }, Strings.Country_Algeria);

        /// <summary>American Samoa (ASM)</summary>
        public static readonly Country American_Samoa = new Country("AS", "ASM", new[] { Currency.Samoan_tala }, Strings.Country_American_Samoa);

        /// <summary>Andorra (AND)</summary>
        public static readonly Country Andorra = new Country("AD", "AND", new[] { Currency.Euro }, Strings.Country_Andorra);
        
        /// <summary>Angola (AGO)</summary>
        public static readonly Country Angola = new Country("AO", "AGO", new[] { Currency.Angolan_kwanza }, Strings.Country_Angola);
        
        /// <summary>Anguilla (AIA)</summary>
        public static readonly Country Anguilla = new Country("AI", "AIA", new[] { Currency.East_Caribbean_dollar }, Strings.Country_Anguilla);
        
        /// <summary>Antigua and Barbuda (ATG)</summary>
        public static readonly Country Antigua_and_Barbuda = new Country("AG", "ATG", new[] { Currency.East_Caribbean_dollar }, Strings.Country_Antigua_and_Barbuda);
        
        /// <summary>Argentina (ARG)</summary>
        public static readonly Country Argentina = new Country("AR", "ARG", new[] { Currency.Argentine_peso }, Strings.Country_Argentina);
        
        /// <summary>Armenia (ARM)</summary>
        public static readonly Country Armenia = new Country("AM", "ARM", new[] { Currency.Armenian_dram }, Strings.Country_Armenia);
        
        /// <summary>Aruba (ABW)</summary>
        public static readonly Country Aruba = new Country("AW", "ABW", new[] { Currency.Aruban_florin }, Strings.Country_Aruba);
        
        /// <summary>Australia (AUS)</summary>
        public static readonly Country Australia = new Country("AU", "AUS", new[] { Currency.Australian_dollar }, Strings.Country_Australia);
        
        /// <summary>Austria (AUT)</summary>
        public static readonly Country Austria = new Country("AT", "AUT", new[] { Currency.Euro }, Strings.Country_Austria);
        
        /// <summary>Azerbaijan (AZE)</summary>
        public static readonly Country Azerbaijan = new Country("AZ", "AZE", new[] { Currency.Azerbaijani_manat }, Strings.Country_Azerbaijan);
        
        /// <summary>Bahamas (BHS)</summary>
        public static readonly Country Bahamas = new Country("BS", "BHS", new[] { Currency.Bahamian_dollar }, Strings.Country_Bahamas);
        
        /// <summary>Bahrain (BHR)</summary>
        public static readonly Country Bahrain = new Country("BH", "BHR", new[] { Currency.Bahraini_dinar }, Strings.Country_Bahrain);
        
        /// <summary>Bangladesh (BGD)</summary>
        public static readonly Country Bangladesh = new Country("BD", "BGD", new[] { Currency.Bangladeshi_taka }, Strings.Country_Bangladesh);
        
        /// <summary>Barbados (BRB)</summary>
        public static readonly Country Barbados = new Country("BB", "BRB", new[] { Currency.Barbadian_dollar }, Strings.Country_Barbados);
        
        /// <summary>Belarus (BLR)</summary>
        public static readonly Country Belarus = new Country("BY", "BLR", new[] { Currency.Belarusian_ruble }, Strings.Country_Belarus);
        
        /// <summary>Belgium (BEL)</summary>
        public static readonly Country Belgium = new Country("BE", "BEL", new[] { Currency.Euro }, Strings.Country_Belgium);
        
        /// <summary>Belize (BLZ)</summary>
        public static readonly Country Belize = new Country("BZ", "BLZ", new[] { Currency.Belize_dollar }, Strings.Country_Belize);
        
        /// <summary>Benin (BEN)</summary>
        public static readonly Country Benin = new Country("BJ", "BEN", new[] { Currency.West_African_CFA_franc }, Strings.Country_Benin);
        
        /// <summary>Bermuda (BMU)</summary>
        public static readonly Country Bermuda = new Country("BM", "BMU", new[] { Currency.Bermudian_dollar }, Strings.Country_Bermuda);
        
        /// <summary>Bhutan (BTN)</summary>
        public static readonly Country Bhutan = new Country("BT", "BTN", new[] { Currency.Bhutanese_ngultrum, Currency.Indian_rupee }, Strings.Country_Bhutan);
        
        /// <summary>Bolivia (BOL)</summary>
        public static readonly Country Bolivia = new Country("BO", "BOL", new[] { Currency.Bolivian_boliviano }, Strings.Country_Bolivia);
        
        /// <summary>Bonaire (BES)</summary>
        public static readonly Country Bonaire = new Country("BQ", "BES", new[] { Currency.United_States_dollar }, Strings.Country_Bonaire);
        
        /// <summary>Bosnia and Herzegovina (BIH)</summary>
        public static readonly Country Bosnia_and_Herzegovina = new Country("BA", "BIH", new[] { Currency.Bosnia_and_Herzegovina_convertible_mark }, Strings.Country_Bosnia_and_Herzegovina);
        
        /// <summary>Botswana (BWA)</summary>
        public static readonly Country Botswana = new Country("BW", "BWA", new[] { Currency.Botswana_pula }, Strings.Country_Botswana);

        /// <summary>Bouvet Island (BVT)</summary>
        public static readonly Country Bouvet_Island = new Country("BV", "BVT", new[] { Currency.Norwegian_krone }, Strings.Country_Bouvet_Island);

        /// <summary>Brazil (BRA)</summary>
        public static readonly Country Brazil = new Country("BR", "BRA", new[] { Currency.Brazilian_real }, Strings.Country_Brazil);
        
        /// <summary>British Indian Ocean Territory (IOT)</summary>
        public static readonly Country British_Indian_Ocean_Territory = new Country("IO", "IOT", new[] { Currency.United_States_dollar }, Strings.Country_British_Indian_Ocean_Territory);
        
        /// <summary>British Virgin Islands (VGB)</summary>
        public static readonly Country British_Virgin_Islands = new Country("VG", "VGB", new[] { Currency.United_States_dollar }, Strings.Country_British_Virgin_Islands); // Removed Currency.British_Virgin_Islands_dollar

        /// <summary>Brunei (BRN)</summary>
        public static readonly Country Brunei_Darussalam = new Country("BN", "BRN", new[] { Currency.Brunei_dollar, Currency.Singapore_dollar }, Strings.Country_Brunei_Darussalam);
        
        /// <summary>Bulgaria (BGR)</summary>
        public static readonly Country Bulgaria = new Country("BG", "BGR", new[] { Currency.Bulgarian_lev }, Strings.Country_Bulgaria);
        
        /// <summary>Burkina Faso (BFA)</summary>
        public static readonly Country Burkina_Faso = new Country("BF", "BFA", new[] { Currency.West_African_CFA_franc }, Strings.Country_Burkina_Faso);
        
        /// <summary>Burundi (BDI)</summary>
        public static readonly Country Burundi = new Country("BI", "BDI", new[] { Currency.Burundian_franc }, Strings.Country_Burundi);
        
        /// <summary>Cambodia (KHM)</summary>
        public static readonly Country Cambodia = new Country("KH", "KHM", new[] { Currency.Cambodian_riel, Currency.United_States_dollar }, Strings.Country_Cambodia);
        
        /// <summary>Cameroon (CMR)</summary>
        public static readonly Country Cameroon = new Country("CM", "CMR", new[] { Currency.Central_African_CFA_franc }, Strings.Country_Cameroon);
        
        /// <summary>Canada (CAN)</summary>
        public static readonly Country Canada = new Country("CA", "CAN", new[] { Currency.Canadian_dollar }, Strings.Country_Canada);
        
        /// <summary>Cape Verde (CPV)</summary>
        public static readonly Country Cape_Verde = new Country("CV", "CPV", new[] { Currency.Cape_Verdean_escudo }, Strings.Country_Cape_Verde);
        
        /// <summary>Cayman Islands (CYM)</summary>
        public static readonly Country Cayman_Islands = new Country("KY", "CYM", new[] { Currency.Cayman_Islands_dollar }, Strings.Country_Cayman_Islands);
        
        /// <summary>Central African Republic (CAF)</summary>
        public static readonly Country Central_African_Republic = new Country("CF", "CAF", new[] { Currency.Central_African_CFA_franc }, Strings.Country_Central_African_Republic);
        
        /// <summary>Chad (TCD)</summary>
        public static readonly Country Chad = new Country("TD", "TCD", new[] { Currency.Central_African_CFA_franc }, Strings.Country_Chad);
        
        /// <summary>Chile (CHL)</summary>
        public static readonly Country Chile = new Country("CL", "CHL", new[] { Currency.Chilean_peso }, Strings.Country_Chile);
        
        /// <summary>China (CHN)</summary>
        public static readonly Country China = new Country("CN", "CHN", new[] { Currency.Chinese_yuan }, Strings.Country_China);

        /// <summary>Christmas Island (CXR)</summary>
        public static readonly Country Christmas_Island = new Country("CX", "CXR", new[] { Currency.Australian_dollar }, Strings.Country_Christmas_Island);

        /// <summary>Cocos (Keeling) Islands (CCK)</summary>
        public static readonly Country Cocos_Keeling_Islands = new Country("CC", "CCK", new[] { Currency.Australian_dollar }, Strings.Country_Cocos_Keeling_Islands);
        
        /// <summary>Colombia (COL)</summary>
        public static readonly Country Colombia = new Country("CO", "COL", new[] { Currency.Colombian_peso }, Strings.Country_Colombia);
        
        /// <summary>Comoros (COM)</summary>
        public static readonly Country Comoros = new Country("KM", "COM", new[] { Currency.Comorian_franc }, Strings.Country_Comoros);
        
        /// <summary>Congo, Democratic Republic of the (COG)</summary>
        public static readonly Country Congo = new Country("CG", "COG", new[] { Currency.Congolese_franc, Currency.Central_African_CFA_franc }, Strings.Country_Congo_Democratic_Republic_of_the);
        
        /// <summary>Cook Islands (COK)</summary>
        public static readonly Country Cook_Islands = new Country("CK", "COK", new[] { Currency.New_Zealand_dollar }, Strings.Country_Cook_Islands); // Removed Currency.Cook_Islands_dollar

        /// <summary>Costa Rica (CRI)</summary>
        public static readonly Country Costa_Rica = new Country("CR", "CRI", new[] { Currency.Costa_Rican_colon }, Strings.Country_Costa_Rica);
        
        /// <summary>Côte d"Ivoire (CIV)</summary>
        public static readonly Country Cote_d_Ivoire = new Country("CI", "CIV", new[] { Currency.West_African_CFA_franc }, Strings.Country_Cote_d_Ivoire);
        
        /// <summary>Croatia (HRV)</summary>
        public static readonly Country Croatia = new Country("HR", "HRV", new[] { Currency.Croatian_kuna }, Strings.Country_Croatia);
        
        /// <summary>Cuba (CUB)</summary>
        public static readonly Country Cuba = new Country("CU", "CUB", new[] { Currency.Cuban_convertible_peso, Currency.Cuban_peso }, Strings.Country_Cuba);
        
        /// <summary>Curaçao (CUW)</summary>
        public static readonly Country Curacao = new Country("CW", "CUW", new[] { Currency.Netherlands_Antillean_guilder }, Strings.Country_Curacao);
        
        /// <summary>Cyprus (CYP)</summary>
        public static readonly Country Cyprus = new Country("CY", "CYP", new[] { Currency.Euro }, Strings.Country_Cyprus);
        
        /// <summary>Czech Republic (CZE)</summary>
        public static readonly Country Czech_Republic = new Country("CZ", "CZE", new[] { Currency.Czech_koruna }, Strings.Country_Czech_Republic);
        
        /// <summary>Denmark (DNK)</summary>
        public static readonly Country Denmark = new Country("DK", "DNK", new[] { Currency.Danish_krone }, Strings.Country_Denmark);
        
        /// <summary>Djibouti (DJI)</summary>
        public static readonly Country Djibouti = new Country("DJ", "DJI", new[] { Currency.Djiboutian_franc }, Strings.Country_Djibouti);
        
        /// <summary>Dominica (DMA)</summary>
        public static readonly Country Dominica = new Country("DM", "DMA", new[] { Currency.East_Caribbean_dollar }, Strings.Country_Dominica);
        
        /// <summary>Dominican Republic (DOM)</summary>
        public static readonly Country Dominican_Republic = new Country("DO", "DOM", new[] { Currency.Dominican_peso }, Strings.Country_Dominican_Republic);
        
        /// <summary>East Timor (TLS)</summary>
        public static readonly Country East_Timor = new Country("TL", "TLS", new[] { Currency.United_States_dollar }, Strings.Country_East_Timor);
        
        /// <summary>Ecuador (ECU)</summary>
        public static readonly Country Ecuador = new Country("EC", "ECU", new[] { Currency.United_States_dollar }, Strings.Country_Ecuador);
        
        /// <summary>Egypt (EGY)</summary>
        public static readonly Country Egypt = new Country("EG", "EGY", new[] { Currency.Egyptian_pound }, Strings.Country_Egypt);
        
        /// <summary>El Salvador (SLV)</summary>
        public static readonly Country El_Salvador = new Country("SV", "SLV", new[] { Currency.United_States_dollar }, Strings.Country_El_Salvador);
        
        /// <summary>Equatorial Guinea (GNQ)</summary>
        public static readonly Country Equatorial_Guinea = new Country("GQ", "GNQ", new[] { Currency.Central_African_CFA_franc }, Strings.Country_Equatorial_Guinea);
        
        /// <summary>Eritrea (ERI)</summary>
        public static readonly Country Eritrea = new Country("ER", "ERI", new[] { Currency.Eritrean_nakfa }, Strings.Country_Eritrea);
        
        /// <summary>Estonia (EST)</summary>
        public static readonly Country Estonia = new Country("EE", "EST", new[] { Currency.Euro }, Strings.Country_Estonia);
        
        /// <summary>Ethiopia (ETH)</summary>
        public static readonly Country Ethiopia = new Country("ET", "ETH", new[] { Currency.Ethiopian_birr }, Strings.Country_Ethiopia);
        
        /// <summary>Falkland Islands (FLK)</summary>
        public static readonly Country Falkland_Islands = new Country("FK", "FLK", new[] { Currency.Falkland_Islands_pound }, Strings.Country_Falkland_Islands);
        
        /// <summary>Faroe Islands (FRO)</summary>
        public static readonly Country Faroe_Islands = new Country("FO", "FRO", new[] { Currency.Danish_krone }, Strings.Country_Faroe_Islands); // Removed Currency.Faroese_krona

        /// <summary>Fiji (FJI)</summary>
        public static readonly Country Fiji = new Country("FJ", "FJI", new[] { Currency.Fijian_dollar }, Strings.Country_Fiji);
        
        /// <summary>Finland (FIN)</summary>
        public static readonly Country Finland = new Country("FI", "FIN", new[] { Currency.Euro }, Strings.Country_Finland);
        
        /// <summary>France (FRA)</summary>
        public static readonly Country France = new Country("FR", "FRA", new[] { Currency.Euro }, Strings.Country_France);
        
        /// <summary>French Guiana (GUF)</summary>
        public static readonly Country French_Guiana = new Country("GF", "GUF", new[] { Currency.Euro }, Strings.Country_French_Guiana);

        /// <summary>French Polynesia (PYF)</summary>
        public static readonly Country French_Polynesia = new Country("PF", "PYF", new[] { Currency.CFP_franc }, Strings.Country_French_Polynesia);

        /// <summary>French Southern Territories (ATF)</summary>
        public static readonly Country French_Southern_Territories = new Country("TF", "ATF", new[] { Currency.Euro }, Strings.Country_French_Southern_Territories);

        /// <summary>Gabon (GAB)</summary>
        public static readonly Country Gabon = new Country("GA", "GAB", new[] { Currency.Central_African_CFA_franc }, Strings.Country_Gabon);
        
        /// <summary>Gambia (GMB)</summary>
        public static readonly Country Gambia = new Country("GM", "GMB", new[] { Currency.Gambian_dalasi }, Strings.Country_Gambia);
        
        /// <summary>Georgia (GEO)</summary>
        public static readonly Country Georgia = new Country("GE", "GEO", new[] { Currency.Georgian_lari }, Strings.Country_Georgia);
        
        /// <summary>Germany (DEU)</summary>
        public static readonly Country Germany = new Country("DE", "DEU", new[] { Currency.Euro }, Strings.Country_Germany);
        
        /// <summary>Ghana (GHA)</summary>
        public static readonly Country Ghana = new Country("GH", "GHA", new[] { Currency.Ghana_cedi }, Strings.Country_Ghana);
        
        /// <summary>Gibraltar (GIB)</summary>
        public static readonly Country Gibraltar = new Country("GI", "GIB", new[] { Currency.Gibraltar_pound }, Strings.Country_Gibraltar);
        
        /// <summary>Greece (GRC)</summary>
        public static readonly Country Greece = new Country("GR", "GRC", new[] { Currency.Euro }, Strings.Country_Greece);

        /// <summary>Greenland (GRL)</summary>
        public static readonly Country Greenland = new Country("GL", "GRL", new[] { Currency.Danish_krone }, Strings.Country_Greenland);

        /// <summary>Grenada (GRD)</summary>
        public static readonly Country Grenada = new Country("GD", "GRD", new[] { Currency.East_Caribbean_dollar }, Strings.Country_Grenada);

        /// <summary>Guadeloupe (GLP)</summary>
        public static readonly Country Guadeloupe = new Country("GP", "GLP", new[] { Currency.Euro }, Strings.Country_Guadeloupe);

        /// <summary>Guam (GUM)</summary>
        public static readonly Country Guam = new Country("GU", "GUM", new[] { Currency.United_States_dollar }, Strings.Country_Guam);

        /// <summary>Guatemala (GTM)</summary>
        public static readonly Country Guatemala = new Country("GT", "GTM", new[] { Currency.Guatemalan_quetzal }, Strings.Country_Guatemala);
        
        /// <summary>Guernsey (GGY)</summary>
        public static readonly Country Guernsey = new Country("GG", "GGY", new[] { Currency.British_pound }, Strings.Country_Guernsey); // Removed Currency.Guernsey_pound

        /// <summary>Guinea (GIN)</summary>
        public static readonly Country Guinea = new Country("GN", "GIN", new[] { Currency.Guinean_franc }, Strings.Country_Guinea);
        
        /// <summary>Guinea-Bissau (GNB)</summary>
        public static readonly Country Guinea_Bissau = new Country("GW", "GNB", new[] { Currency.West_African_CFA_franc }, Strings.Country_Guinea_Bissau);
        
        /// <summary>Guyana (GUY)</summary>
        public static readonly Country Guyana = new Country("GY", "GUY", new[] { Currency.Guyanese_dollar }, Strings.Country_Guyana);

        /// <summary> Heard Island and McDonald Islands (HMD)</summary>
        public static readonly Country Heard_Island_and_McDonald_Islands = new Country("HM", "HMD", new[] { Currency.Australian_dollar }, Strings.Country_Heard_Island_and_McDonald_Islands);

        /// <summary>Haiti (HTI)</summary>
        public static readonly Country Haiti = new Country("HT", "HTI", new[] { Currency.Haitian_gourde }, Strings.Country_Haiti);
        
        /// <summary>Honduras (HND)</summary>
        public static readonly Country Honduras = new Country("HN", "HND", new[] { Currency.Honduran_lempira }, Strings.Country_Honduras);
        
        /// <summary>Hong Kong (HKG)</summary>
        public static readonly Country Hong_Kong = new Country("HK", "HKG", new[] { Currency.Hong_Kong_dollar }, Strings.Country_Hong_Kong);
        
        /// <summary>Hungary (HUN)</summary>
        public static readonly Country Hungary = new Country("HU", "HUN", new[] { Currency.Hungarian_forint }, Strings.Country_Hungary);
        
        /// <summary>Iceland (ISL)</summary>
        public static readonly Country Iceland = new Country("IS", "ISL", new[] { Currency.Icelandic_króna }, Strings.Country_Iceland);
        
        /// <summary>India (IND)</summary>
        public static readonly Country India = new Country("IN", "IND", new[] { Currency.Indian_rupee }, Strings.Country_India);
        
        /// <summary>Indonesia (IDN)</summary>
        public static readonly Country Indonesia = new Country("ID", "IDN", new[] { Currency.Indonesian_rupiah }, Strings.Country_Indonesia);
        
        /// <summary>Iran (IRN)</summary>
        public static readonly Country Iran = new Country("IR", "IRN", new[] { Currency.Iranian_rial }, Strings.Country_Iran);
        
        /// <summary>Iraq (IRQ)</summary>
        public static readonly Country Iraq = new Country("IQ", "IRQ", new[] { Currency.Iraqi_dinar }, Strings.Country_Iraq);
        
        /// <summary>Ireland (IRL)</summary>
        public static readonly Country Ireland = new Country("IE", "IRL", new[] { Currency.Euro }, Strings.Country_Ireland);
        
        /// <summary>Isle of Man (IMN)</summary>
        public static readonly Country Isle_of_Man = new Country("IM", "IMN", new[] { Currency.British_pound, Currency.Manx_pound }, Strings.Country_Isle_of_Man);
        
        /// <summary>Israel (ISR)</summary>
        public static readonly Country Israel = new Country("IL", "ISR", new[] { Currency.Israeli_new_shekel }, Strings.Country_Israel);
        
        /// <summary>Italy (ITA)</summary>
        public static readonly Country Italy = new Country("IT", "ITA", new[] { Currency.Euro }, Strings.Country_Italy);
        
        /// <summary>Jamaica (JAM)</summary>
        public static readonly Country Jamaica = new Country("JM", "JAM", new[] { Currency.Jamaican_dollar }, Strings.Country_Jamaica);
        
        /// <summary>Japan (JPN)</summary>
        public static readonly Country Japan = new Country("JP", "JPN", new[] { Currency.Japanese_yen }, Strings.Country_Japan);
        
        /// <summary>Jersey (JEY)</summary>
        public static readonly Country Jersey = new Country("JE", "JEY", new[] { Currency.British_pound, Currency.Jersey_pound }, Strings.Country_Jersey);
        
        /// <summary>Jordan (JOR)</summary>
        public static readonly Country Jordan = new Country("JO", "JOR", new[] { Currency.Jordanian_dinar }, Strings.Country_Jordan);
        
        /// <summary>Kazakhstan (KAZ)</summary>
        public static readonly Country Kazakhstan = new Country("KZ", "KAZ", new[] { Currency.Kazakhstani_tenge }, Strings.Country_Kazakhstan);
        
        /// <summary>Kenya (KEN)</summary>
        public static readonly Country Kenya = new Country("KE", "KEN", new[] { Currency.Kenyan_shilling }, Strings.Country_Kenya);
        
        /// <summary>Kiribati (KIR)</summary>
        public static readonly Country Kiribati = new Country("KI", "KIR", new[] { Currency.Australian_dollar }, Strings.Country_Kiribati); // Removed Currency.Kiribati_dollar

        /// <summary>Korea, North (PRK)</summary>
        public static readonly Country Korea_North = new Country("KP", "PRK", new[] { Currency.North_Korean_won }, Strings.Country_Korea_North);
        
        /// <summary>Korea, South (KOR)</summary>
        public static readonly Country Korea_South = new Country("KR", "KOR", new[] { Currency.South_Korean_won }, Strings.Country_Korea_South);
        
        /// <summary>Kosovo (XKX)</summary>
        public static readonly Country Kosovo = new Country("XK", "XKX", new[] { Currency.Euro }, Strings.Country_Kosovo);
        
        /// <summary>Kuwait (KWT)</summary>
        public static readonly Country Kuwait = new Country("KW", "KWT", new[] { Currency.Kuwaiti_dinar }, Strings.Country_Kuwait);
        
        /// <summary>Kyrgyzstan (KGZ)</summary>
        public static readonly Country Kyrgyzstan = new Country("KG", "KGZ", new[] { Currency.Kyrgyzstani_som }, Strings.Country_Kyrgyzstan);
        
        /// <summary>Laos (LAO)</summary>
        public static readonly Country Lao = new Country("LA", "LAO", new[] { Currency.Lao_kip }, Strings.Country_Lao);
        
        /// <summary>Latvia (LVA)</summary>
        public static readonly Country Latvia = new Country("LV", "LVA", new[] { Currency.Euro }, Strings.Country_Latvia);
        
        /// <summary>Lebanon (LBN)</summary>
        public static readonly Country Lebanon = new Country("LB", "LBN", new[] { Currency.Lebanese_pound }, Strings.Country_Lebanon);
        
        /// <summary>Lesotho (LSO)</summary>
        public static readonly Country Lesotho = new Country("LS", "LSO", new[] { Currency.Lesotho_loti, Currency.South_African_rand }, Strings.Country_Lesotho);
        
        /// <summary>Liberia (LBR)</summary>
        public static readonly Country Liberia = new Country("LR", "LBR", new[] { Currency.Liberian_dollar }, Strings.Country_Liberia);
        
        /// <summary>Libya (LBY)</summary>
        public static readonly Country Libya = new Country("LY", "LBY", new[] { Currency.Libyan_dinar }, Strings.Country_Libya);
        
        /// <summary>Liechtenstein (LIE)</summary>
        public static readonly Country Liechtenstein = new Country("LI", "LIE", new[] { Currency.Swiss_franc }, Strings.Country_Liechtenstein);
        
        /// <summary>Lithuania (LTU)</summary>
        public static readonly Country Lithuania = new Country("LT", "LTU", new[] { Currency.Euro }, Strings.Country_Lithuania);
        
        /// <summary>Luxembourg (LUX)</summary>
        public static readonly Country Luxembourg = new Country("LU", "LUX", new[] { Currency.Euro }, Strings.Country_Luxembourg);
        
        /// <summary>Macau (MAC)</summary>
        public static readonly Country Macau = new Country("MO", "MAC", new[] { Currency.Macanese_pataca }, Strings.Country_Macau);
        
        /// <summary>Macedonia (MKD)</summary>
        public static readonly Country Macedonia = new Country("MK", "MKD", new[] { Currency.Macedonian_denar }, Strings.Country_Macedonia);
        
        /// <summary>Madagascar (MDG)</summary>
        public static readonly Country Madagascar = new Country("MG", "MDG", new[] { Currency.Malagasy_ariary }, Strings.Country_Madagascar);
        
        /// <summary>Malawi (MWI)</summary>
        public static readonly Country Malawi = new Country("MW", "MWI", new[] { Currency.Malawian_kwacha }, Strings.Country_Malawi);
        
        /// <summary>Malaysia (MYS)</summary>
        public static readonly Country Malaysia = new Country("MY", "MYS", new[] { Currency.Malaysian_ringgit }, Strings.Country_Malaysia);
        
        /// <summary>Maldives (MDV)</summary>
        public static readonly Country Maldives = new Country("MV", "MDV", new[] { Currency.Maldivian_rufiyaa }, Strings.Country_Maldives);
        
        /// <summary>Mali (MLI)</summary>
        public static readonly Country Mali = new Country("ML", "MLI", new[] { Currency.West_African_CFA_franc }, Strings.Country_Mali);
        
        /// <summary>Malta (MLT)</summary>
        public static readonly Country Malta = new Country("MT", "MLT", new[] { Currency.Euro }, Strings.Country_Malta);
        
        /// <summary>Marshall Islands (MHL)</summary>
        public static readonly Country Marshall_Islands = new Country("MH", "MHL", new[] { Currency.United_States_dollar }, Strings.Country_Marshall_Islands);

        /// <summary>Martinique (MQT)</summary>
        public static readonly Country Martinique = new Country("MQ", "MQT", new[] { Currency.Euro }, Strings.Country_Martinique);

        /// <summary>Mauritania (MRT)</summary>
        public static readonly Country Mauritania = new Country("MR", "MRT", new[] { Currency.Mauritanian_ouguiya }, Strings.Country_Mauritania);
        
        /// <summary>Mauritius (MUS)</summary>
        public static readonly Country Mauritius = new Country("MU", "MUS", new[] { Currency.Mauritian_rupee }, Strings.Country_Mauritius);

        /// <summary>Mayotte (MYT)</summary>
        public static readonly Country Mayotte = new Country("YT", "MYT", new[] { Currency.Euro }, Strings.Country_Mayotte);

        /// <summary>Mexico (MEX)</summary>
        public static readonly Country Mexico = new Country("MX", "MEX", new[] { Currency.Mexican_peso }, Strings.Country_Mexico);
        
        /// <summary>Micronesia (FSM)</summary>
        public static readonly Country Micronesia = new Country("FM", "FSM", new[] { Currency.United_States_dollar }, Strings.Country_Micronesia); // Removed Currency.Micronesian_dollar

        /// <summary>Moldova (MDA)</summary>
        public static readonly Country Moldova = new Country("MD", "MDA", new[] { Currency.Moldovan_leu }, Strings.Country_Moldova);
        
        /// <summary>Monaco (MCO)</summary>
        public static readonly Country Monaco = new Country("MC", "MCO", new[] { Currency.Euro }, Strings.Country_Monaco);
        
        /// <summary>Mongolia (MNG)</summary>
        public static readonly Country Mongolia = new Country("MN", "MNG", new[] { Currency.Mongolian_togrog }, Strings.Country_Mongolia);
        
        /// <summary>Montenegro (MNE)</summary>
        public static readonly Country Montenegro = new Country("ME", "MNE", new[] { Currency.Euro }, Strings.Country_Montenegro);
        
        /// <summary>Montserrat (MSR)</summary>
        public static readonly Country Montserrat = new Country("MS", "MSR", new[] { Currency.East_Caribbean_dollar }, Strings.Country_Montserrat);
        
        /// <summary>Morocco (MAR)</summary>
        public static readonly Country Morocco = new Country("MA", "MAR", new[] { Currency.Moroccan_dirham }, Strings.Country_Morocco);
        
        /// <summary>Mozambique (MOZ)</summary>
        public static readonly Country Mozambique = new Country("MZ", "MOZ", new[] { Currency.Mozambican_metical }, Strings.Country_Mozambique);
        
        /// <summary>Myanmar (MMR)</summary>
        public static readonly Country Myanmar = new Country("MM", "MMR", new[] { Currency.Burmese_kyat }, Strings.Country_Myanmar);
        
        /// <summary>Namibia (NAM)</summary>
        public static readonly Country Namibia = new Country("NA", "NAM", new[] { Currency.Namibian_dollar, Currency.South_African_rand }, Strings.Country_Namibia);
        
        /// <summary>Nauru (NRU)</summary>
        public static readonly Country Nauru = new Country("NR", "NRU", new[] { Currency.Australian_dollar }, Strings.Country_Nauru); // Removed Currency.Nauruan_dollar

        /// <summary>Nepal (NPL)</summary>
        public static readonly Country Nepal = new Country("NP", "NPL", new[] { Currency.Nepalese_rupee }, Strings.Country_Nepal);
        
        /// <summary>Netherlands[L] (NLD)</summary>
        public static readonly Country Netherlands = new Country("NL", "NLD", new[] { Currency.Euro }, Strings.Country_Netherlands);
        
        /// <summary>New Caledonia (NCL)</summary>
        public static readonly Country New_Caledonia = new Country("NC", "NCL", new[] { Currency.CFP_franc }, Strings.Country_New_Caledonia);
        
        /// <summary>New Zealand (NZL)</summary>
        public static readonly Country New_Zealand = new Country("NZ", "NZL", new[] { Currency.New_Zealand_dollar }, Strings.Country_New_Zealand);
        
        /// <summary>Nicaragua (NIC)</summary>
        public static readonly Country Nicaragua = new Country("NI", "NIC", new[] { Currency.Nicaraguan_cordoba }, Strings.Country_Nicaragua);
        
        /// <summary>Niger (NER)</summary>
        public static readonly Country Niger = new Country("NE", "NER", new[] { Currency.West_African_CFA_franc }, Strings.Country_Niger);
        
        /// <summary>Nigeria (NGA)</summary>
        public static readonly Country Nigeria = new Country("NG", "NGA", new[] { Currency.Nigerian_naira }, Strings.Country_Nigeria);
        
        /// <summary>Niue (NIU)</summary>
        public static readonly Country Niue = new Country("NU", "NIU", new[] { Currency.New_Zealand_dollar }, Strings.Country_Niue); // Removed Currency.Niue_dollar

        /// <summary>Norfolk Island (NFK)</summary>
        public static readonly Country Norfolk_Island = new Country("NF", "NFK", new[] { Currency.Australian_dollar }, Strings.Country_Norfolk_Island);

        /// <summary>Norway (NOR)</summary>
        public static readonly Country Norway = new Country("NO", "NOR", new[] { Currency.Norwegian_krone }, Strings.Country_Norway);

        /// <summary>Northern Mariana Islands (MNP)</summary>
        public static readonly Country Northern_Mariana_Islands = new Country("MP", "MNP", new[] { Currency.United_States_dollar }, Strings.Country_Northern_Mariana_Islands);

        /// <summary>Oman (OMN)</summary>
        public static readonly Country Oman = new Country("OM", "OMN", new[] { Currency.Omani_rial }, Strings.Country_Oman);
        
        /// <summary>Pakistan (PAK)</summary>
        public static readonly Country Pakistan = new Country("PK", "PAK", new[] { Currency.Pakistani_rupee }, Strings.Country_Pakistan);
        
        /// <summary>Palau (PLW)</summary>
        public static readonly Country Palau = new Country("PW", "PLW", new[] { Currency.United_States_dollar }, Strings.Country_Palau); // Removed Currency.Palauan_dollar

        /// <summary>Palestine (PSE)</summary>
        public static readonly Country Palestine = new Country("PS", "PSE", new[] { Currency.Israeli_new_shekel, Currency.Jordanian_dinar }, Strings.Country_Palestine);
        
        /// <summary>Panama (PAN)</summary>
        public static readonly Country Panama = new Country("PA", "PAN", new[] { Currency.Panamanian_balboa, Currency.United_States_dollar }, Strings.Country_Panama);
        
        /// <summary>Papua New Guinea (PNG)</summary>
        public static readonly Country Papua_New_Guinea = new Country("PG", "PNG", new[] { Currency.Papua_New_Guinean_kina }, Strings.Country_Papua_New_Guinea);
        
        /// <summary>Paraguay (PRY)</summary>
        public static readonly Country Paraguay = new Country("PY", "PRY", new[] { Currency.Paraguayan_guaraní }, Strings.Country_Paraguay);
        
        /// <summary>Peru (PER)</summary>
        public static readonly Country Peru = new Country("PE", "PER", new[] { Currency.Peruvian_nuevo_sol }, Strings.Country_Peru);
        
        /// <summary>Philippines (PHL)</summary>
        public static readonly Country Philippines = new Country("PH", "PHL", new[] { Currency.Philippine_peso }, Strings.Country_Philippines);
        
        /// <summary>Pitcairn Islands (PCN)</summary>
        public static readonly Country Pitcairn = new Country("PN", "PCN", new[] { Currency.New_Zealand_dollar }, Strings.Country_Pitcairn); // Removed Currency.Pitcairn_Islands_dollar

        /// <summary>Poland (POL)</summary>
        public static readonly Country Poland = new Country("PL", "POL", new[] { Currency.Polish_zloty }, Strings.Country_Poland);
        
        /// <summary>Portugal (PRT)</summary>
        public static readonly Country Portugal = new Country("PT", "PRT", new[] { Currency.Euro }, Strings.Country_Portugal);

        /// <summary>Puerto Rico (PRI)</summary>
        public static readonly Country Puerto_Rico = new Country("PR", "PRI", new[] { Currency.United_States_dollar }, Strings.Country_Puerto_Rico);

        /// <summary>Qatar (QAT)</summary>
        public static readonly Country Qatar = new Country("QA", "QAT", new[] { Currency.Qatari_riyal }, Strings.Country_Qatar);
        
        /// <summary>Romania (ROU)</summary>
        public static readonly Country Romania = new Country("RO", "ROU", new[] { Currency.Romanian_leu }, Strings.Country_Romania);
        
        /// <summary>Russia (RUS)</summary>
        public static readonly Country Russia = new Country("RU", "RUS", new[] { Currency.Russian_ruble }, Strings.Country_Russian_Federation);
        
        /// <summary>Rwanda (RWA)</summary>
        public static readonly Country Rwanda = new Country("RW", "RWA", new[] { Currency.Rwandan_franc }, Strings.Country_Rwanda);
        
        /// <summary> Reunion (REU)</summary>
        public static readonly Country Reunion = new Country("RE", "REU", new[] { Currency.Euro }, Strings.Country_Reunion);
        
        /// <summary>Saba (BLM)</summary>
        public static readonly Country Saint_Barthalemy = new Country("BL", "BLM", new[] { Currency.United_States_dollar }, Strings.Country_Saint_Barthalemy);
        
        /// <summary>Saint Helena (SHN)</summary>
        public static readonly Country Saint_Helena = new Country("SH", "SHN", new[] { Currency.Saint_Helena_pound }, Strings.Country_Saint_Helena);
        
        /// <summary>Saint Kitts and Nevis (KNA)</summary>
        public static readonly Country Saint_Kitts_and_Nevis = new Country("KN", "KNA", new[] { Currency.East_Caribbean_dollar }, Strings.Country_Saint_Kitts_and_Nevis);
        
        /// <summary>Saint Lucia (LCA)</summary>
        public static readonly Country Saint_Lucia = new Country("LC", "LCA", new[] { Currency.East_Caribbean_dollar }, Strings.Country_Saint_Lucia);
        
        /// <summary>Saint Martin (MAF)</summary>
        public static readonly Country Saint_Martin = new Country("MF", "MAF", new[] { Currency.Euro }, Strings.Country_Saint_Martin);
        
        /// <summary>Saint Pierre and Miquelon (SPM)</summary>
        public static readonly Country Saint_Pierre_and_Miquelon = new Country("PM", "SPM", new[] { Currency.Euro }, Strings.Country_Saint_Pierre_and_Miquelon);
        
        /// <summary>Saint Vincent and the Grenadines (VCT)</summary>
        public static readonly Country Saint_Vincent_and_the_Grenadines = new Country("VC", "VCT", new[] { Currency.East_Caribbean_dollar }, Strings.Country_Saint_Vincent_and_the_Grenadines);
        
        /// <summary>Samoa (WSM)</summary>
        public static readonly Country Samoa = new Country("WS", "WSM", new[] { Currency.Samoan_tala }, Strings.Country_Samoa);
        
        /// <summary>San Marino (SMR)</summary>
        public static readonly Country San_Marino = new Country("SM", "SMR", new[] { Currency.Euro }, Strings.Country_San_Marino);
        
        /// <summary>São Tomé and Príncipe (STP)</summary>
        public static readonly Country Sao_Tome_and_Principe = new Country("ST", "STP", new[] { Currency.Sao_Tome_and_Principe_dobra }, Strings.Country_Sao_Tome_and_Principe);
        
        /// <summary>Saudi Arabia (SAU)</summary>
        public static readonly Country Saudi_Arabia = new Country("SA", "SAU", new[] { Currency.Saudi_riyal }, Strings.Country_Saudi_Arabia);
        
        /// <summary>Senegal (SEN)</summary>
        public static readonly Country Senegal = new Country("SN", "SEN", new[] { Currency.West_African_CFA_franc }, Strings.Country_Senegal);
        
        /// <summary>Serbia (SRB)</summary>
        public static readonly Country Serbia = new Country("RS", "SRB", new[] { Currency.Serbian_dinar }, Strings.Country_Serbia);
        
        /// <summary>Seychelles (SYC)</summary>
        public static readonly Country Seychelles = new Country("SC", "SYC", new[] { Currency.Seychellois_rupee }, Strings.Country_Seychelles);
        
        /// <summary>Sierra Leone (SLE)</summary>
        public static readonly Country Sierra_Leone = new Country("SL", "SLE", new[] { Currency.Sierra_Leonean_leone }, Strings.Country_Sierra_Leone);
        
        /// <summary>Singapore (SGP)</summary>
        public static readonly Country Singapore = new Country("SG", "SGP", new[] { Currency.Brunei_dollar, Currency.Singapore_dollar }, Strings.Country_Singapore);
        
        /// <summary>Sint Maarten (SXM)</summary>
        public static readonly Country Sint_Maarten = new Country("SX", "SXM", new[] { Currency.Netherlands_Antillean_guilder }, Strings.Country_Sint_Maarten);
        
        /// <summary>Slovakia (SVK)</summary>
        public static readonly Country Slovakia = new Country("SK", "SVK", new[] { Currency.Euro }, Strings.Country_Slovakia);
        
        /// <summary>Slovenia (SVN)</summary>
        public static readonly Country Slovenia = new Country("SI", "SVN", new[] { Currency.Euro }, Strings.Country_Slovenia);
        
        /// <summary>Solomon Islands (SLB)</summary>
        public static readonly Country Solomon_Islands = new Country("SB", "SLB", new[] { Currency.Solomon_Islands_dollar }, Strings.Country_Solomon_Islands);
        
        /// <summary>Somalia (SOM)</summary>
        public static readonly Country Somalia = new Country("SO", "SOM", new[] { Currency.Somali_shilling }, Strings.Country_Somalia);
        
        /// <summary>South Africa (ZAF)</summary>
        public static readonly Country South_Africa = new Country("ZA", "ZAF", new[] { Currency.South_African_rand }, Strings.Country_South_Africa);
        
        /// <summary>South Georgia and the South Sandwich Islands (SGS)</summary>
        public static readonly Country South_Georgia_and_the_South_Sandwich_Islands = new Country("GS", "SGS", new[] { Currency.British_pound }, Strings.Country_South_Georgia_and_the_South_Sandwich_Islands); // Removed Currency.South_Georgia_and_the_South_Sandwich_Islands_pound

        /// <summary>Spain (ESP)</summary>
        public static readonly Country Spain = new Country("ES", "ESP", new[] { Currency.Euro }, Strings.Country_Spain);
        
        /// <summary>South Sudan (SSD)</summary>
        public static readonly Country South_Sudan = new Country("SS", "SSD", new[] { Currency.South_Sudanese_pound }, Strings.Country_South_Sudan);
        
        /// <summary>Sri Lanka (LKA)</summary>
        public static readonly Country Sri_Lanka = new Country("LK", "LKA", new[] { Currency.Sri_Lankan_rupee }, Strings.Country_Sri_Lanka);
        
        /// <summary>Sudan (SDN)</summary>
        public static readonly Country Sudan = new Country("SD", "SDN", new[] { Currency.Sudanese_pound }, Strings.Country_Sudan);
        
        /// <summary>Suriname (SUR)</summary>
        public static readonly Country Suriname = new Country("SR", "SUR", new[] { Currency.Surinamese_dollar }, Strings.Country_Suriname);
        
        /// <summary>Svalbard and Jan Mayen (SJM)</summary>
        public static readonly Country Svalbard_and_Jan_Mayen = new Country("SJ", "SJM", new[] { Currency.Norwegian_krone }, Strings.Country_Svalbard_and_Jan_Mayen);
        
        /// <summary>Swaziland (SWZ)</summary>
        public static readonly Country Swaziland = new Country("SZ", "SWZ", new[] { Currency.Swazi_lilangeni }, Strings.Country_Swaziland);
        
        /// <summary>Sweden (SWE)</summary>
        public static readonly Country Sweden = new Country("SE", "SWE", new[] { Currency.Swedish_krona }, Strings.Country_Sweden);
        
        /// <summary> Switzerland (CHE)</summary>
        public static readonly Country Switzerland = new Country("CH", "CHE", new[] { Currency.Swiss_franc }, Strings.Country_Switzerland);
        
        /// <summary>Syria (SYR)</summary>
        public static readonly Country Syria = new Country("SY", "SYR", new[] { Currency.Syrian_pound }, Strings.Country_Syrian_Arab_Republic);
        
        /// <summary>Taiwan (TWN)</summary>
        public static readonly Country Taiwan = new Country("TW", "TWN", new[] { Currency.New_Taiwan_dollar }, Strings.Country_Taiwan);
        
        /// <summary>Tajikistan (TJK)</summary>
        public static readonly Country Tajikistan = new Country("TJ", "TJK", new[] { Currency.Tajikistani_somoni }, Strings.Country_Tajikistan);
        
        /// <summary>Tanzania (TZA)</summary>
        public static readonly Country Tanzania = new Country("TZ", "TZA", new[] { Currency.Tanzanian_shilling }, Strings.Country_Tanzania);
        
        /// <summary>Thailand (THA)</summary>
        public static readonly Country Thailand = new Country("TH", "THA", new[] { Currency.Thai_baht }, Strings.Country_Thailand);
        
        /// <summary>Togo (TGO)</summary>
        public static readonly Country Togo = new Country("TG", "TGO", new[] { Currency.West_African_CFA_franc }, Strings.Country_Togo);
        
        /// <summary>Tonga (TKL)</summary>
        public static readonly Country Tokelau = new Country("TK", "TKL", new[] { Currency.Tongan_pa_anga }, Strings.Country_Tokelau);
        
        /// <summary>Transnistria (TON)</summary>
        public static readonly Country Tonga = new Country("TO", "TON", new[] { Currency.Transnistrian_ruble }, Strings.Country_Tonga);
        
        /// <summary>Trinidad and Tobago (TTO)</summary>
        public static readonly Country Trinidad_and_Tobago = new Country("TT", "TTO", new[] { Currency.Trinidad_and_Tobago_dollar }, Strings.Country_Trinidad_and_Tobago);
        
        /// <summary>Tunisia (TUN)</summary>
        public static readonly Country Tunisia = new Country("TN", "TUN", new[] { Currency.Tunisian_dinar }, Strings.Country_Tunisia);
        
        /// <summary>Turkey (TUR)</summary>
        public static readonly Country Turkey = new Country("TR", "TUR", new[] { Currency.Turkish_lira }, Strings.Country_Turkey);
        
        /// <summary>Turkmenistan (TKM)</summary>
        public static readonly Country Turkmenistan = new Country("TM", "TKM", new[] { Currency.Turkmenistan_manat }, Strings.Country_Turkmenistan);
        
        /// <summary>Turks and Caicos Islands (TCA)</summary>
        public static readonly Country Turks_and_Caicos_Islands = new Country("TC", "TCA", new[] { Currency.United_States_dollar }, Strings.Country_Turks_and_Caicos_Islands);
        
        /// <summary>Tuvalu (TUV)</summary>
        public static readonly Country Tuvalu = new Country("TV", "TUV", new[] { Currency.Australian_dollar }, Strings.Country_Tuvalu); // Removed Currency.Tuvaluan_dollar

        /// <summary>Uganda (UGA)</summary>
        public static readonly Country Uganda = new Country("UG", "UGA", new[] { Currency.Ugandan_shilling }, Strings.Country_Uganda);
        
        /// <summary>Ukraine (UKR)</summary>
        public static readonly Country Ukraine = new Country("UA", "UKR", new[] { Currency.Ukrainian_hryvnia, Currency.Russian_ruble }, Strings.Country_Ukraine);
        
        /// <summary>United Arab Emirates (ARE)</summary>
        public static readonly Country United_Arab_Emirates = new Country("AE", "ARE", new[] { Currency.United_Arab_Emirates_dirham }, Strings.Country_United_Arab_Emirates);
        
        /// <summary>United Kingdom (GBR)</summary>
        public static readonly Country United_Kingdom = new Country("GB", "GBR", new[] { Currency.British_pound }, Strings.Country_United_Kingdom);
        
        /// <summary>United States (USA)</summary>
        public static readonly Country United_States = new Country("US", "USA", new[] { Currency.United_States_dollar }, Strings.Country_United_States);
        
        /// <summary>Uruguay (URY)</summary>
        public static readonly Country Uruguay = new Country("UY", "URY", new[] { Currency.Uruguayan_peso }, Strings.Country_Uruguay);
        
        /// <summary>Uzbekistan (UZB)</summary>
        public static readonly Country Uzbekistan = new Country("UZ", "UZB", new[] { Currency.Uzbekistani_som }, Strings.Country_Uzbekistan);
        
        /// <summary>Vanuatu (VUT)</summary>
        public static readonly Country Vanuatu = new Country("VU", "VUT", new[] { Currency.Vanuatu_vatu }, Strings.Country_Vanuatu);
        
        /// <summary>Venezuela (VEN)</summary>
        public static readonly Country Venezuela = new Country("VE", "VEN", new[] { Currency.Venezuelan_bolivar }, Strings.Country_Venezuela);
        
        /// <summary>Vietnam (VNM)</summary>
        public static readonly Country Vietnam = new Country("VN", "VNM", new[] { Currency.Vietnamese_dong }, Strings.Country_Viet_Nam);
        
        /// <summary>Wallis and Futuna (WLF)</summary>
        public static readonly Country Wallis_and_Futuna = new Country("WF", "WLF", new[] { Currency.CFP_franc }, Strings.Country_Wallis_and_Futuna);
        
        /// <summary>Yemen (YEM)</summary>
        public static readonly Country Yemen = new Country("YE", "YEM", new[] { Currency.Yemeni_rial }, Strings.Country_Yemen);
        
        /// <summary>Zambia (ZMB)</summary>
        public static readonly Country Zambia = new Country("ZM", "ZMB", new[] { Currency.Zambian_kwacha }, Strings.Country_Zambia);
        
        /// <summary>Zimbabwe (ZWE)</summary>
        public static readonly Country Zimbabwe = new Country("ZW", "ZWE", new[] { Currency.Botswana_pula, Currency.British_pound, Currency.Euro, Currency.Indian_rupee, Currency.South_African_rand, Currency.United_States_dollar }, Strings.Country_Zimbabwe); // Removed Currency.Zimbabwean_Bond

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Country"/> class.
        /// </summary>
        /// <param name="codeAlpha2">The Alpha-2 country code.</param>
        /// <param name="codeAlpha3">The Alpha-3 country code.</param>
        /// <param name="currencies">The dictionary containing the currencies for the country.</param>
        /// <param name="name">The name of the country.</param>
        private Country(string codeAlpha2, string codeAlpha3, Currency[] currencies, string name)
        {
            this.CodeAlpha2 = codeAlpha2;
            this.CodeAlpha3 = codeAlpha3;
            this.Name = name;

            if (currencies != null && currencies.Length > 0)
            {                
                this.Currencies = currencies.ToDictionary(x => x.Code, x => x);
            }
            
            Mappings[codeAlpha3] = this;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Alpha-2 code of the country.
        /// </summary>
        public string CodeAlpha2 { get; set; }

        /// <summary>
        /// Gets or sets the Alpha-3 code of the country.
        /// </summary>
        public string CodeAlpha3 { get; set; }

        /// <summary>
        /// Gets or sets the name of the country.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the currencies of the country.
        /// </summary>
        public Dictionary<string, Currency> Currencies { get; set; }

        #endregion

        #region Operators

        /// <summary>
        /// Converts the specified string to the corresponding <see cref="Country"/> instance. 
        /// </summary>
        /// <param name="codeAlpha3">The Alpha-3 code of the country.</param>
        public static explicit operator Country(string codeAlpha3)
        {
            Country result;
            if (Mappings.TryGetValue(codeAlpha3.ToUpper(), out result))
            {
                return result;
            }

            throw new InvalidCastException();
        }

        #endregion
    }
}
