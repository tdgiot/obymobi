﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using Dionysos;

namespace Obymobi
{
    /// <summary>
    /// Class which represents a culture and holds mappings of all cultures.
    /// </summary>
    /// <remarks>StyleCop and ReSharper naming rules have been disabled for better readability purposes.</remarks>
    [SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "One time allowance of underscores.")]
    [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "One time ignoring of spelling.")]
    // ReSharper disable InconsistentNaming
    public class Culture
    {
        #region Fields

        /// <summary>The dictionary containing the cultures, mapped by culture code.</summary>
        public static readonly Dictionary<string, Culture> Mappings = new Dictionary<string, Culture>();

        #region Culture list

        /// <summary>Pashto (Afghanistan)</summary>
        public static readonly Culture Pashto_Afghanistan = new Culture("ps-AF", Country.Afghanistan, Language.Pashto);

        /// <summary>Dari (Afghanistan)</summary>
        public static readonly Culture Dari_Afghanistan = new Culture("prs-AF", Country.Afghanistan, Language.Dari);

        /// <summary>Albanian (Albania)</summary>
        public static readonly Culture Albanian_Albania = new Culture("sq-AL", Country.Albania, Language.Albanian);

        /// <summary>Arabic (Algeria)</summary>
        public static readonly Culture Arabic_Algeria = new Culture("ar-DZ", Country.Algeria, Language.Arabic);

        /// <summary>Spanish (Argentina)</summary>
        public static readonly Culture Spanish_Argentina = new Culture("es-AR", Country.Argentina, Language.Spanish);

        /// <summary>Armenian (Armenia)</summary>
        public static readonly Culture Armenian_Armenia = new Culture("hy-AM", Country.Armenia, Language.Armenian);

        /// <summary>English (Australia)</summary>
        public static readonly Culture English_Australia = new Culture("en-AU", Country.Australia, Language.English);

        /// <summary>German (Austria)</summary>
        public static readonly Culture German_Austria = new Culture("de-AT", Country.Austria, Language.German);

        /// <summary>Arabic (Bahrain)</summary>
        public static readonly Culture Arabic_Bahrain = new Culture("ar-BH", Country.Bahrain, Language.Arabic);

        /// <summary>Bengali (Bangladesh)</summary>
        public static readonly Culture Bengali_Bangladesh = new Culture("bn-BD", Country.Bangladesh, Language.Bengali);

        /// <summary>Basque (Basque)</summary>
        public static readonly Culture Basque_Basque = new Culture("eu-ES", Country.Spain, Language.Basque);

        /// <summary>Belarusian (Belarus)</summary>
        public static readonly Culture Belarusian_Belarus = new Culture("be-BY", Country.Belarus, Language.Belarusian);

        /// <summary>French (Belgium)</summary>
        public static readonly Culture French_Belgium = new Culture("fr-BE", Country.Belgium, Language.French);

        /// <summary>Dutch (Belgium)</summary>
        public static readonly Culture Dutch_Belgium = new Culture("nl-BE", Country.Belgium, Language.Dutch);

        /// <summary>English (Belize)</summary>
        public static readonly Culture English_Belize = new Culture("en-BZ", Country.Belize, Language.English);

        /// <summary>Spanish (Venezuela)</summary>
        public static readonly Culture Spanish_Venezuela = new Culture("es-VE", Country.Venezuela, Language.Spanish);

        /// <summary>Quechua (Bolivia)</summary>
        public static readonly Culture Quechua_Bolivia = new Culture("quz-BO", Country.Bolivia, Language.Quechua);

        /// <summary>Spanish (Bolivia)</summary>
        public static readonly Culture Spanish_Bolivia = new Culture("es-BO", Country.Bolivia, Language.Spanish);

        /// <summary>Portuguese (Brazil)</summary>
        public static readonly Culture Portuguese_Brazil = new Culture("pt-BR", Country.Brazil, Language.Portuguese);

        /// <summary>Malay (Brunei Darussalam)</summary>
        public static readonly Culture Malay_Brunei_Darussalam = new Culture("ms-BN", Country.Brunei_Darussalam, Language.Malay);

        /// <summary>Bulgarian (Bulgaria)</summary>
        public static readonly Culture Bulgarian_Bulgaria = new Culture("bg-BG", Country.Bulgaria, Language.Bulgarian);

        /// <summary>Khmer (Cambodia)</summary>
        public static readonly Culture Khmer_Cambodia = new Culture("km-KH", Country.Cambodia, Language.Khmer);

        /// <summary>French (Canada)</summary>
        public static readonly Culture French_Canada = new Culture("fr-CA", Country.Canada, Language.French);

        /// <summary>English (Canada)</summary>
        public static readonly Culture English_Canada = new Culture("en-CA", Country.Canada, Language.English);

        /// <summary>English (Caribbean)</summary>
        /// <remarks>This culture does not have a country attached to it, as the Caribbean is a region according to UN M.49. Use the CultureInfo property instead.</remarks>
        public static readonly Culture English_Caribbean = new Culture("en-029", null, Language.English);

        /// <summary>Catalan (Catalan)</summary>
        public static readonly Culture Catalan_Catalan = new Culture("ca-ES", Country.Spain, Language.Catalan);

        /// <summary>Mapudungun (Chile)</summary>
        public static readonly Culture Mapudungun_Chile = new Culture("arn-CL", Country.Chile, Language.Mapudungun);

        /// <summary>Spanish (Chile)</summary>
        public static readonly Culture Spanish_Chile = new Culture("es-CL", Country.Chile, Language.Spanish);

        /// <summary>Spanish (Colombia)</summary>
        public static readonly Culture Spanish_Colombia = new Culture("es-CO", Country.Colombia, Language.Spanish);

        /// <summary>Spanish (Costa Rica)</summary>
        public static readonly Culture Spanish_Costa_Rica = new Culture("es-CR", Country.Costa_Rica, Language.Spanish);

        /// <summary>Croatian (Croatia)</summary>
        public static readonly Culture Croatian_Croatia = new Culture("hr-HR", Country.Croatia, Language.Croatian);

        /// <summary>Azeri (Cyrillic, Azerbaijan)</summary>
        public static readonly Culture Azeri_Cyrillic_Azerbaijan = new Culture("az-Cyrl-AZ", Country.Azerbaijan, Language.Azeri);

        /// <summary>Serbian (Cyrillic, Bosnia and Herzegovina)</summary>
        public static readonly Culture Serbian_Cyrillic_Bosnia_and_Herzegovina = new Culture("sr-Cyrl-BA", Country.Bosnia_and_Herzegovina, Language.Serbian);

        /// <summary>Bosnian (Cyrillic, Bosnia and Herzegovina)</summary>
        public static readonly Culture Bosnian_Cyrillic_Bosnia_and_Herzegovina = new Culture("bs-Cyrl-BA", Country.Bosnia_and_Herzegovina, Language.Bosnian);

        /// <summary>Mongolian (Cyrillic, Mongolia)</summary>
        public static readonly Culture Mongolian_Cyrillic_Mongolia = new Culture("mn-MN", Country.Mongolia, Language.Mongolian);

        /// <summary>Serbian (Cyrillic, Montenegro)</summary>
        public static readonly Culture Serbian_Cyrillic_Montenegro = new Culture("sr-Cyrl-ME", Country.Montenegro, Language.Serbian);

        /// <summary>Serbian (Cyrillic, Serbia)</summary>
        public static readonly Culture Serbian_Cyrillic_Serbia = new Culture("sr-Cyrl-RS", Country.Serbia, Language.Serbian);

        /// <summary>Tajik (Cyrillic, Tajikistan)</summary>
        public static readonly Culture Tajik_Cyrillic_Tajikistan = new Culture("tg-Cyrl-TJ", Country.Tajikistan, Language.Tajik);

        /// <summary>Uzbek (Cyrillic, Uzbekistan)</summary>
        public static readonly Culture Uzbek_Cyrillic_Uzbekistan = new Culture("uz-Cyrl-UZ", Country.Uzbekistan, Language.Uzbek);

        /// <summary>Czech (Czech Republic)</summary>
        public static readonly Culture Czech_Czech_Republic = new Culture("cs-CZ", Country.Czech_Republic, Language.Czech);

        /// <summary>Danish (Denmark)</summary>
        public static readonly Culture Danish_Denmark = new Culture("da-DK", Country.Denmark, Language.Danish);

        /// <summary>Spanish (Dominican Republic)</summary>
        public static readonly Culture Spanish_Dominican_Republic = new Culture("es-DO", Country.Dominican_Republic, Language.Spanish);

        /// <summary>Quechua (Ecuador)</summary>
        public static readonly Culture Quechua_Ecuador = new Culture("quz-EC", Country.Ecuador, Language.Quechua);

        /// <summary>Spanish (Ecuador)</summary>
        public static readonly Culture Spanish_Ecuador = new Culture("es-EC", Country.Ecuador, Language.Spanish);

        /// <summary>Arabic (Egypt)</summary>
        public static readonly Culture Arabic_Egypt = new Culture("ar-EG", Country.Egypt, Language.Arabic);

        /// <summary>Spanish (El Salvador)</summary>
        public static readonly Culture Spanish_El_Salvador = new Culture("es-SV", Country.El_Salvador, Language.Spanish);

        /// <summary>Estonian (Estonia)</summary>
        public static readonly Culture Estonian_Estonia = new Culture("et-EE", Country.Estonia, Language.Estonian);

        /// <summary>Amharic (Ethiopia)</summary>
        public static readonly Culture Amharic_Ethiopia = new Culture("am-ET", Country.Ethiopia, Language.Amharic);

        /// <summary>Faroese (Faroe Islands)</summary>
        public static readonly Culture Faroese_Faroe_Islands = new Culture("fo-FO", Country.Faroe_Islands, Language.Faroese);

        /// <summary>Finnish (Finland)</summary>
        public static readonly Culture Finnish_Finland = new Culture("fi-FI", Country.Finland, Language.Finnish);

        /// <summary>Swedish (Finland)</summary>
        public static readonly Culture Swedish_Finland = new Culture("sv-FI", Country.Finland, Language.Swedish);

        /// <summary>Sami, Northern (Finland)</summary>
        public static readonly Culture Sami_Northern_Finland = new Culture("se-FI", Country.Finland, Language.Sami_Northern);

        /// <summary>Sami, Skolt (Finland)</summary>
        public static readonly Culture Sami_Skolt_Finland = new Culture("sms-FI", Country.Finland, Language.Sami_Skolt);

        /// <summary>Sami, Inari (Finland)</summary>
        public static readonly Culture Sami_Inari_Finland = new Culture("smn-FI", Country.Finland, Language.Sami_Inari);

        /// <summary>Macedonian (Macedonia)</summary>
        public static readonly Culture Macedonian_Macedonia = new Culture("mk-MK", Country.Macedonia, Language.Macedonian);

        /// <summary>French (France)</summary>
        public static readonly Culture French_France = new Culture("fr-FR", Country.France, Language.French);

        /// <summary>Breton (France)</summary>
        public static readonly Culture Breton_France = new Culture("br-FR", Country.France, Language.Breton);

        /// <summary>Occitan (France)</summary>
        public static readonly Culture Occitan_France = new Culture("oc-FR", Country.France, Language.Occitan);

        /// <summary>Corsican (France)</summary>
        public static readonly Culture Corsican_France = new Culture("co-FR", Country.France, Language.Corsican);

        /// <summary>Alsatian (France)</summary>
        public static readonly Culture Alsatian_France = new Culture("gsw-FR", Country.France, Language.Alsatian);

        /// <summary>Galician (Galician)</summary>
        public static readonly Culture Galician_Galician = new Culture("gl-ES", Country.Spain, Language.Galician);

        /// <summary>Georgian (Georgia)</summary>
        public static readonly Culture Georgian_Georgia = new Culture("ka-GE", Country.Georgia, Language.Georgian);

        /// <summary>German (Germany)</summary>
        public static readonly Culture German_Germany = new Culture("de-DE", Country.Germany, Language.German);

        /// <summary>Upper Sorbian (Germany)</summary>
        public static readonly Culture UpperSorbian_Germany = new Culture("hsb-DE", Country.Germany, Language.Upper_Sorbian);

        /// <summary>Lower Sorbian (Germany)</summary>
        public static readonly Culture LowerSorbian_Germany = new Culture("dsb-DE", Country.Germany, Language.Lower_Sorbian);

        /// <summary>Greek (Greece)</summary>
        public static readonly Culture Greek_Greece = new Culture("el-GR", Country.Greece, Language.Greek);

        /// <summary>Greenlandic (Greenland)</summary>
        public static readonly Culture Greenlandic_Greenland = new Culture("kl-GL", Country.Greenland, Language.Greenlandic);

        /// <summary>K"iche (Guatemala)</summary>
        public static readonly Culture Kiche_Guatemala = new Culture("qut-GT", Country.Guatemala, Language.K_iche);

        /// <summary>Spanish (Guatemala)</summary>
        public static readonly Culture Spanish_Guatemala = new Culture("es-GT", Country.Guatemala, Language.Spanish);

        /// <summary>Spanish (Honduras)</summary>
        public static readonly Culture Spanish_Honduras = new Culture("es-HN", Country.Honduras, Language.Spanish);

        /// <summary>Hungarian (Hungary)</summary>
        public static readonly Culture Hungarian_Hungary = new Culture("hu-HU", Country.Hungary, Language.Hungarian);

        /// <summary>Icelandic (Iceland)</summary>
        public static readonly Culture Icelandic_Iceland = new Culture(" is- IS", Country.Iceland, Language.Icelandic);

        /// <summary>Hindi (India)</summary>
        public static readonly Culture Hindi_India = new Culture("hi-IN", Country.India, Language.Hindi);

        /// <summary>Bengali (India)</summary>
        public static readonly Culture Bengali_India = new Culture("bn-IN", Country.India, Language.Bengali);

        /// <summary>Punjabi (India)</summary>
        public static readonly Culture Punjabi_India = new Culture("pa-IN", Country.India, Language.Punjabi);

        /// <summary>Gujarati (India)</summary>
        public static readonly Culture Gujarati_India = new Culture("gu-IN", Country.India, Language.Gujarati);

        /// <summary>Oriya (India)</summary>
        public static readonly Culture Oriya_India = new Culture("or-IN", Country.India, Language.Oriya);

        /// <summary>Tamil (India)</summary>
        public static readonly Culture Tamil_India = new Culture("ta-IN", Country.India, Language.Tamil);

        /// <summary>Telugu (India)</summary>
        public static readonly Culture Telugu_India = new Culture("te-IN", Country.India, Language.Telugu);

        /// <summary>Kannada (India)</summary>
        public static readonly Culture Kannada_India = new Culture("kn-IN", Country.India, Language.Kannada);

        /// <summary>Malayalam (India)</summary>
        public static readonly Culture Malayalam_India = new Culture("ml-IN", Country.India, Language.Malayalam);

        /// <summary>Assamese (India)</summary>
        public static readonly Culture Assamese_India = new Culture(" as- IN", Country.India, Language.Assamese);

        /// <summary>Marathi (India)</summary>
        public static readonly Culture Marathi_India = new Culture("mr-IN", Country.India, Language.Marathi);

        /// <summary>Sanskrit (India)</summary>
        public static readonly Culture Sanskrit_India = new Culture("sa-IN", Country.India, Language.Sanskrit);

        /// <summary>Konkani (India)</summary>
        public static readonly Culture Konkani_India = new Culture("kok-IN", Country.India, Language.Konkani);

        /// <summary>English (India)</summary>
        public static readonly Culture English_India = new Culture("en-IN", Country.India, Language.English);

        /// <summary>Indonesian (Indonesia)</summary>
        public static readonly Culture Indonesian_Indonesia = new Culture("id-ID", Country.Indonesia, Language.Indonesian);

        /// <summary>Persian (Iran)</summary>
        public static readonly Culture Persian_Iran = new Culture("fa-IR", Country.Iran, Language.Persian);

        /// <summary>Arabic (Iraq)</summary>
        public static readonly Culture Arabic_Iraq = new Culture("ar-IQ", Country.Iraq, Language.Arabic);

        /// <summary>Irish (Ireland)</summary>
        public static readonly Culture Irish_Ireland = new Culture("ga-IE", Country.Ireland, Language.Irish);

        /// <summary>English (Ireland)</summary>
        public static readonly Culture English_Ireland = new Culture("en-IE", Country.Ireland, Language.English);

        /// <summary>Urdu (Pakistan)</summary>
        public static readonly Culture Urdu_Pakistan = new Culture("ur-PK", Country.Pakistan, Language.Urdu);

        /// <summary>Hebrew (Israel)</summary>
        public static readonly Culture Hebrew_Israel = new Culture("he-IL", Country.Israel, Language.Hebrew);

        /// <summary>Italian (Italy)</summary>
        public static readonly Culture Italian_Italy = new Culture("it-IT", Country.Italy, Language.Italian);

        /// <summary>English (Jamaica)</summary>
        public static readonly Culture English_Jamaica = new Culture("en-JM", Country.Jamaica, Language.English);

        /// <summary>Japanese (Japan)</summary>
        public static readonly Culture Japanese_Japan = new Culture("ja-JP", Country.Japan, Language.Japanese);

        /// <summary>Arabic (Jordan)</summary>
        public static readonly Culture Arabic_Jordan = new Culture("ar-JO", Country.Jordan, Language.Arabic);

        /// <summary>Kazakh (Kazakhstan)</summary>
        public static readonly Culture Kazakh_Kazakhstan = new Culture("kk-KZ", Country.Kazakhstan, Language.Kazakh);

        /// <summary>Kiswahili (Kenya)</summary>
        public static readonly Culture Kiswahili_Kenya = new Culture("sw-KE", Country.Kenya, Language.Kiswahili);

        /// <summary>Korean (Korea)</summary>
        public static readonly Culture Korean_Korea = new Culture("ko-KR", Country.Korea_South, Language.Korean);

        /// <summary>Arabic (Kuwait)</summary>
        public static readonly Culture Arabic_Kuwait = new Culture("ar-KW", Country.Kuwait, Language.Arabic);

        /// <summary>Kyrgyz (Kyrgyzstan)</summary>
        public static readonly Culture Kyrgyz_Kyrgyzstan = new Culture("ky-KG", Country.Kyrgyzstan, Language.Kyrgyz);

        /// <summary>Lao (Lao P.D.R.)</summary>
        public static readonly Culture Lao_Lao = new Culture("lo-LA", Country.Lao, Language.Lao);

        /// <summary>Tamazight (Latin, Algeria)</summary>
        public static readonly Culture Tamazight_Latin_Algeria = new Culture("tzm-Latn-DZ", Country.Algeria, Language.Tamazight);

        /// <summary>Azeri (Latin, Azerbaijan)</summary>
        public static readonly Culture Azeri_Latin_Azerbaijan = new Culture("az-Latn-AZ", Country.Azerbaijan, Language.Azeri);

        /// <summary>Croatian (Latin, Bosnia and Herzegovina)</summary>
        public static readonly Culture Croatian_Latin_Bosnia_and_Herzegovina = new Culture("hr-BA", Country.Bosnia_and_Herzegovina, Language.Croatian);

        /// <summary>Bosnian (Latin, Bosnia and Herzegovina)</summary>
        public static readonly Culture Bosnian_Latin_Bosnia_and_Herzegovina = new Culture("bs-Latn-BA", Country.Bosnia_and_Herzegovina, Language.Bosnian);

        /// <summary>Serbian (Latin, Bosnia and Herzegovina)</summary>
        public static readonly Culture Serbian_Latin_Bosnia_and_Herzegovina = new Culture("sr-Latn-BA", Country.Bosnia_and_Herzegovina, Language.Serbian);

        /// <summary>Inuktitut (Latin, Canada)</summary>
        public static readonly Culture Inuktitut_Latin_Canada = new Culture("iu-Latn-CA", Country.Canada, Language.Inuktitut);

        /// <summary>Serbian (Latin, Montenegro)</summary>
        public static readonly Culture Serbian_Latin_Montenegro = new Culture("sr-Latn-ME", Country.Montenegro, Language.Serbian);

        /// <summary>Hausa (Latin, Nigeria)</summary>
        public static readonly Culture Hausa_Latin_Nigeria = new Culture("ha-Latn-NG", Country.Nigeria, Language.Hausa);

        /// <summary>Serbian (Latin, Serbia)</summary>
        public static readonly Culture Serbian_Latin_Serbia = new Culture("sr-Latn-RS", Country.Serbia, Language.Serbian);

        /// <summary>Uzbek (Latin, Uzbekistan)</summary>
        public static readonly Culture Uzbek_Latin_Uzbekistan = new Culture("uz-Latn-UZ", Country.Uzbekistan, Language.Uzbek);

        /// <summary>Latvian (Latvia)</summary>
        public static readonly Culture Latvian_Latvia = new Culture("lv-LV", Country.Latvia, Language.Latvian);

        /// <summary>Arabic (Lebanon)</summary>
        public static readonly Culture Arabic_Lebanon = new Culture("ar-LB", Country.Lebanon, Language.Arabic);

        /// <summary>Arabic (Libya)</summary>
        public static readonly Culture Arabic_Libya = new Culture("ar-LY", Country.Libya, Language.Arabic);

        /// <summary>German (Liechtenstein)</summary>
        public static readonly Culture German_Liechtenstein = new Culture("de-LI", Country.Liechtenstein, Language.German);

        /// <summary>Lithuanian (Lithuania)</summary>
        public static readonly Culture Lithuanian_Lithuania = new Culture("lt-LT", Country.Lithuania, Language.Lithuanian);

        /// <summary>Luxembourgish (Luxembourg)</summary>
        public static readonly Culture Luxembourgish_Luxembourg = new Culture("lb-LU", Country.Luxembourg, Language.Luxembourgish);

        /// <summary>German (Luxembourg)</summary>
        public static readonly Culture German_Luxembourg = new Culture("de-LU", Country.Luxembourg, Language.German);

        /// <summary>French (Luxembourg)</summary>
        public static readonly Culture French_Luxembourg = new Culture("fr-LU", Country.Luxembourg, Language.French);

        /// <summary>Malay (Malaysia)</summary>
        public static readonly Culture Malay_Malaysia = new Culture("ms-MY", Country.Malaysia, Language.Malay);

        /// <summary>English (Malaysia)</summary>
        public static readonly Culture English_Malaysia = new Culture("en-MY", Country.Malaysia, Language.English);

        /// <summary>Divehi (Maldives)</summary>
        public static readonly Culture Divehi_Maldives = new Culture("dv-MV", Country.Maldives, Language.Divehi);

        /// <summary>Maltese (Malta)</summary>
        public static readonly Culture Maltese_Malta = new Culture("mt-MT", Country.Malta, Language.Maltese);

        /// <summary>Spanish (Mexico)</summary>
        public static readonly Culture Spanish_Mexico = new Culture("es-MX", Country.Mexico, Language.Spanish);

        /// <summary>French (Monaco)</summary>
        public static readonly Culture French_Monaco = new Culture("fr-MC", Country.Monaco, Language.French);

        /// <summary>Arabic (Morocco)</summary>
        public static readonly Culture Arabic_Morocco = new Culture("ar-MA", Country.Morocco, Language.Arabic);

        /// <summary>Nepali (Nepal)</summary>
        public static readonly Culture Nepali_Nepal = new Culture("ne-NP", Country.Nepal, Language.Nepali);

        /// <summary>Dutch (Netherlands)</summary>
        public static readonly Culture Dutch_Netherlands = new Culture("nl-NL", Country.Netherlands, Language.Dutch);

        /// <summary>Frisian (Netherlands)</summary>
        public static readonly Culture Frisian_Netherlands = new Culture("fy-NL", Country.Netherlands, Language.Frisian);

        /// <summary>Maori (New Zealand)</summary>
        public static readonly Culture Maori_New_Zealand = new Culture("mi-NZ", Country.New_Zealand, Language.Maori);

        /// <summary>English (New Zealand)</summary>
        public static readonly Culture English_New_Zealand = new Culture("en-NZ", Country.New_Zealand, Language.English);

        /// <summary>Spanish (Nicaragua)</summary>
        public static readonly Culture Spanish_Nicaragua = new Culture("es-NI", Country.Nicaragua, Language.Spanish);

        /// <summary>Yoruba (Nigeria)</summary>
        public static readonly Culture Yoruba_Nigeria = new Culture("yo-NG", Country.Nigeria, Language.Yoruba);

        /// <summary>Igbo (Nigeria)</summary>
        public static readonly Culture Igbo_Nigeria = new Culture("ig-NG", Country.Nigeria, Language.Igbo);

        /// <summary>Norwegian, Bokmål (Norway)</summary>
        public static readonly Culture Norwegian_Bokmal_Norway = new Culture("nb-NO", Country.Norway, Language.Norwegian_Bokmal);

        /// <summary>Sami, Northern (Norway)</summary>
        public static readonly Culture Sami_Northern_Norway = new Culture("se-NO", Country.Norway, Language.Sami_Northern);

        /// <summary>Norwegian, Nynorsk (Norway)</summary>
        public static readonly Culture Norwegian_Nynorsk_Norway = new Culture("nn-NO", Country.Norway, Language.Norwegian_Nynorsk);

        /// <summary>Sami, Lule (Norway)</summary>
        public static readonly Culture Sami_Lule_Norway = new Culture("smj-NO", Country.Norway, Language.Sami_Lule);

        /// <summary>Sami, Southern (Norway)</summary>
        public static readonly Culture Sami_Southern_Norway = new Culture("sma-NO", Country.Norway, Language.Sami_Southern);

        /// <summary>Arabic (Oman)</summary>
        public static readonly Culture Arabic_Oman = new Culture("ar-OM", Country.Oman, Language.Arabic);

        /// <summary>Spanish (Panama)</summary>
        public static readonly Culture Spanish_Panama = new Culture("es-PA", Country.Panama, Language.Spanish);

        /// <summary>Spanish (Paraguay)</summary>
        public static readonly Culture Spanish_Paraguay = new Culture("es-PY", Country.Paraguay, Language.Spanish);

        /// <summary>Quechua (Peru)</summary>
        public static readonly Culture Quechua_Peru = new Culture("quz-PE", Country.Peru, Language.Quechua);

        /// <summary>Spanish (Peru)</summary>
        public static readonly Culture Spanish_Peru = new Culture("es-PE", Country.Peru, Language.Spanish);

        /// <summary>Filipino (Philippines)</summary>
        public static readonly Culture Filipino_Philippines = new Culture("fil-PH", Country.Philippines, Language.Filipino);

        /// <summary>Polish (Poland)</summary>
        public static readonly Culture Polish_Poland = new Culture("pl-PL", Country.Poland, Language.Polish);

        /// <summary>Portuguese (Portugal)</summary>
        public static readonly Culture Portuguese_Portugal = new Culture("pt-PT", Country.Portugal, Language.Portuguese);

        /// <summary>Tibetan (China)</summary>
        public static readonly Culture Tibetan_China = new Culture("bo-CN", Country.China, Language.Tibetan);

        /// <summary>Yi (China)</summary>
        public static readonly Culture Yi_China = new Culture("ii-CN", Country.China, Language.Yi);

        /// <summary>Uyghur (China)</summary>
        public static readonly Culture Uyghur_China = new Culture("ug-CN", Country.China, Language.Uyghur);

        /// <summary>Spanish (Puerto Rico)</summary>
        public static readonly Culture Spanish_Puerto_Rico = new Culture("es-PR", Country.Puerto_Rico, Language.Spanish);

        /// <summary>Arabic (Qatar)</summary>
        public static readonly Culture Arabic_Qatar = new Culture("ar-QA", Country.Qatar, Language.Arabic);

        /// <summary>English (Philippines)</summary>
        public static readonly Culture English_Philippines = new Culture("en-PH", Country.Philippines, Language.English);

        /// <summary>Romanian (Romania)</summary>
        public static readonly Culture Romanian_Romania = new Culture("ro-RO", Country.Romania, Language.Romanian);

        /// <summary>Russian (Russia)</summary>
        public static readonly Culture Russian_Russia = new Culture("ru-RU", Country.Russia, Language.Russian);

        /// <summary>Tatar (Russia)</summary>
        public static readonly Culture Tatar_Russia = new Culture("tt-RU", Country.Russia, Language.Tatar);

        /// <summary>Bashkir (Russia)</summary>
        public static readonly Culture Bashkir_Russia = new Culture("ba-RU", Country.Russia, Language.Bashkir);

        /// <summary>Yakut (Russia)</summary>
        public static readonly Culture Yakut_Russia = new Culture("sah-RU", Country.Russia, Language.Yakut);

        /// <summary>Kinyarwanda (Rwanda)</summary>
        public static readonly Culture Kinyarwanda_Rwanda = new Culture("rw-RW", Country.Rwanda, Language.Kinyarwanda);

        /// <summary>Arabic (Saudi Arabia)</summary>
        public static readonly Culture Arabic_Saudi_Arabia = new Culture("ar-SA", Country.Saudi_Arabia, Language.Arabic);

        /// <summary>Wolof (Senegal)</summary>
        public static readonly Culture Wolof_Senegal = new Culture("wo-SN", Country.Senegal, Language.Wolof);

        /// <summary>Chinese (Simplified, China)</summary>
        public static readonly Culture Chinese_Simplified_China = new Culture("zh-CN", Country.China, Language.Chinese);

        /// <summary>Chinese (Simplified, Singapore)</summary>
        public static readonly Culture Chinese_Simplified_Singapore = new Culture("zh-SG", Country.Singapore, Language.Chinese);

        /// <summary>English (Singapore)</summary>
        public static readonly Culture English_Singapore = new Culture("en-SG", Country.Singapore, Language.English);

        /// <summary>Slovak (Slovakia)</summary>
        public static readonly Culture Slovak_Slovakia = new Culture("sk-SK", Country.Slovakia, Language.Slovak);

        /// <summary>Slovenian (Slovenia)</summary>
        public static readonly Culture Slovenian_Slovenia = new Culture("sl-SI", Country.Slovenia, Language.Slovenian);

        /// <summary>Setswana (South Africa)</summary>
        public static readonly Culture Setswana_South_Africa = new Culture("tn-ZA", Country.South_Africa, Language.Setswana);

        /// <summary>isiXhosa (South Africa)</summary>
        public static readonly Culture IsiXhosa_South_Africa = new Culture("xh-ZA", Country.South_Africa, Language.IsiXhosa);

        /// <summary>isiZulu (South Africa)</summary>
        public static readonly Culture IsiZulu_South_Africa = new Culture("zu-ZA", Country.South_Africa, Language.IsiZulu);

        /// <summary>Afrikaans (South Africa)</summary>
        public static readonly Culture Afrikaans_South_Africa = new Culture("af-ZA", Country.South_Africa, Language.Afrikaans);

        /// <summary>Sesotho sa Leboa (South Africa)</summary>
        public static readonly Culture Sesotho_sa_Leboa_South_Africa = new Culture("nso-ZA", Country.South_Africa, Language.Sesotho_sa_Leboa);

        /// <summary>English (South Africa)</summary>
        public static readonly Culture English_South_Africa = new Culture("en-ZA", Country.South_Africa, Language.English);

        /// <summary>Spanish (Spain)</summary>
        public static readonly Culture Spanish_Spain = new Culture("es-ES", Country.Spain, Language.Spanish);

        /// <summary>Sinhala (Sri Lanka)</summary>
        public static readonly Culture Sinhala_Sri_Lanka = new Culture("si-LK", Country.Sri_Lanka, Language.Sinhala);

        /// <summary>Swedish (Sweden)</summary>
        public static readonly Culture Swedish_Sweden = new Culture("sv-SE", Country.Sweden, Language.Swedish);

        /// <summary>Sami, Northern (Sweden)</summary>
        public static readonly Culture Sami_Northern_Sweden = new Culture("se-SE", Country.Sweden, Language.Sami_Northern);

        /// <summary>Sami, Lule (Sweden)</summary>
        public static readonly Culture Sami_Lule_Sweden = new Culture("smj-SE", Country.Sweden, Language.Sami_Lule);

        /// <summary>Sami, Southern (Sweden)</summary>
        public static readonly Culture Sami_Southern_Sweden = new Culture("sma-SE", Country.Sweden, Language.Sami_Southern);

        /// <summary>Romansh (Switzerland)</summary>
        public static readonly Culture Romansh_Switzerland = new Culture("rm-CH", Country.Switzerland, Language.Romansh);

        /// <summary>German (Switzerland)</summary>
        public static readonly Culture German_Switzerland = new Culture("de-CH", Country.Switzerland, Language.German);

        /// <summary>Italian (Switzerland)</summary>
        public static readonly Culture Italian_Switzerland = new Culture("it-CH", Country.Switzerland, Language.Italian);

        /// <summary>French (Switzerland)</summary>
        public static readonly Culture French_Switzerland = new Culture("fr-CH", Country.Switzerland, Language.French);

        /// <summary>Inuktitut (Syllabics, Canada)</summary>
        public static readonly Culture Inuktitut_Syllabics_Canada = new Culture("iu-Cans-CA", Country.Canada, Language.Inuktitut);

        /// <summary>Syriac (Syria)</summary>
        public static readonly Culture Syriac_Syria = new Culture("syr-SY", Country.Syria, Language.Syriac);

        /// <summary>Arabic (Syria)</summary>
        public static readonly Culture Arabic_Syria = new Culture("ar-SY", Country.Syria, Language.Arabic);

        /// <summary>Thai (Thailand)</summary>
        public static readonly Culture Thai_Thailand = new Culture("th-TH", Country.Thailand, Language.Thai);

        /// <summary>Mongolian (Traditional Mongolian, China)</summary>
        public static readonly Culture Mongolian_Traditional_Mongolian_China = new Culture("mn-Mong-CN", Country.China, Language.Mongolian);

        /// <summary>Chinese (Traditional, Hong Kong)</summary>
        public static readonly Culture Chinese_Traditional_Hong_Kong = new Culture("zh-HK", Country.Hong_Kong, Language.Chinese);

        /// <summary>Chinese (Traditional, Macao)</summary>
        public static readonly Culture Chinese_Traditional_Macao = new Culture("zh-MO", Country.Macau, Language.Chinese);

        /// <summary>Chinese (Traditional, Taiwan)</summary>
        public static readonly Culture Chinese_Traditional_Taiwan = new Culture("zh-TW", Country.Taiwan, Language.Chinese);

        /// <summary>English (Trinidad and Tobago)</summary>
        public static readonly Culture English_Trinidad_and_Tobago = new Culture("en-TT", Country.Trinidad_and_Tobago, Language.English);

        /// <summary>Arabic (Tunisia)</summary>
        public static readonly Culture Arabic_Tunisia = new Culture("ar-TN", Country.Tunisia, Language.Arabic);

        /// <summary>Turkish (Turkey)</summary>
        public static readonly Culture Turkish_Turkey = new Culture("tr-TR", Country.Turkey, Language.Turkish);

        /// <summary>Turkmen (Turkmenistan)</summary>
        public static readonly Culture Turkmen_Turkmenistan = new Culture("tk-TM", Country.Turkmenistan, Language.Turkmen);

        /// <summary>Arabic (United Arab Emirates)</summary>
        public static readonly Culture Arabic_United_Arab_Emirates = new Culture("ar-AE", Country.United_Arab_Emirates, Language.Arabic);

        /// <summary>Ukrainian (Ukraine)</summary>
        public static readonly Culture Ukrainian_Ukraine = new Culture("uk-UA", Country.Ukraine, Language.Ukrainian);

        /// <summary>Welsh (United Kingdom)</summary>
        public static readonly Culture Welsh_United_Kingdom = new Culture("cy-GB", Country.United_Kingdom, Language.Welsh);

        /// <summary>Scottish Gaelic (United Kingdom)</summary>
        public static readonly Culture Scottish_Gaelic_United_Kingdom = new Culture("gd-GB", Country.United_Kingdom, Language.Scottish_Gaelic);

        /// <summary>English (United Kingdom)</summary>
        public static readonly Culture English_United_Kingdom = new Culture("en-GB", Country.United_Kingdom, Language.English);

        /// <summary>English (United States)</summary>
        public static readonly Culture English_United_States = new Culture("en-US", Country.United_States, Language.English);

        /// <summary>Spanish (United States)</summary>
        public static readonly Culture Spanish_United_States = new Culture("es-US", Country.United_States, Language.Spanish);

        /// <summary>Spanish (Uruguay)</summary>
        public static readonly Culture Spanish_Uruguay = new Culture("es-UY", Country.Uruguay, Language.Spanish);

        /// <summary>Vietnamese (Vietnam)</summary>
        public static readonly Culture Vietnamese_Vietnam = new Culture("vi-VN", Country.Vietnam, Language.Vietnamese);

        /// <summary>Arabic (Yemen)</summary>
        public static readonly Culture Arabic_Yemen = new Culture("ar-YE", Country.Yemen, Language.Arabic);

        /// <summary>English (Zimbabwe)</summary>
        public static readonly Culture English_Zimbabwe = new Culture("en-ZW", Country.Zimbabwe, Language.English);

        #endregion

        /// <summary>The .NET culture info instance for this culture.</summary>
        private CultureInfo cultureInfo = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Culture" /> class.
        /// </summary>
        /// <param name="code">The code of the culture.</param>
        /// <param name="country">The country of the culture.</param>
        /// <param name="language">The language of the culture.</param>
        private Culture(string code, Country country, Language language)
        {
            this.Code = code;
            this.Country = country;
            this.Language = language;

            Mappings[code] = this;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the code of the culture.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the country of the culture.
        /// </summary>
        public Country Country { get; set; }

        /// <summary>
        /// Gets or sets the language of the culture.
        /// </summary>
        public Language Language { get; set; }

        /// <summary>
        /// Gets the .NET culture info instance for this culture.
        /// </summary>
        public CultureInfo CultureInfo
        {
            get
            {
                if (this.cultureInfo == null && !this.Code.IsNullOrWhiteSpace())
                {                    
                    this.cultureInfo = CultureInfo.GetCultureInfo(this.Code);
                }

                return this.cultureInfo;
            }
        }

        /// <summary>
        /// Gets the name of the culture.
        /// </summary>
        public string Name
        {
            get
            {
                return this.Country != null ? string.Format("{0} {1}", this.Language.Name, this.Country.Name) : this.Language.Name;
            }
        }

        /// <summary>
        /// Gets the name of the culture postfixed with the culturecode.
        /// </summary>
        public string NameAndCultureCode
        {
            get
            {
                return string.Format("{0} ({1})", this.Name, this.Code);
            }
        }

        /// <summary>
        /// Gets the combination of language and country.
        /// </summary>
        /// <returns>Combination of language and country names.</returns>
        public override string ToString()
        {
            return this.NameAndCultureCode;
        }

        #endregion

        #region Operators

        /// <summary>
        /// Converts the specified string to the corresponding <see cref="Culture" /> instance. 
        /// </summary>
        /// <param name="code">The code of the culture.</param>
        public static explicit operator Culture(string code)
        {
            Culture result = Mappings.Values.SingleOrDefault(x => x.Code.ToLower() == code.ToLower());

            if (result == null)
            { 
                throw new InvalidCastException();
            }

            return result;
        }

        #endregion
    }
}
