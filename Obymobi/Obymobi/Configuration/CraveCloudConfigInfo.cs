using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Configuration;

namespace Obymobi.Configuration
{
    public class CraveCloudConfigInfo : ConfigurationItemCollection
    {
        const string SECTION_NAME = "CraveCloud";

        public CraveCloudConfigInfo()
        {
            // General            
            Add(new ConfigurationItem(SECTION_NAME, CraveCloudConfigConstants.CloudEnvironment, "Cloud Environment: ProductionPrimary, ProductionSecondary, ProductionOverwrite, Test, Development, Demo or Manual", "NOTSET", typeof(string), true));
            Add(new ConfigurationItem(SECTION_NAME, CraveCloudConfigConstants.ProductionOverwriteWebserviceBaseUrl, "This url is used when environment is 'ProductionOverwrite'", "", typeof(string), true));
            Add(new ConfigurationItem(SECTION_NAME, CraveCloudConfigConstants.ManualWebserviceBaseUrl, "This url is used when environment is 'Manual' including /api/", "", typeof(string), true));
            Add(new ConfigurationItem(SECTION_NAME, CraveCloudConfigConstants.ManualAzureCdnBaseUrl, "This Azure url is used when environment is 'Manual' including /files/", "", typeof(string), true));
            Add(new ConfigurationItem(SECTION_NAME, CraveCloudConfigConstants.ManualAmazonCdnBaseUrl, "This Amazon url is used when environment is 'Manual' including /files/", "", typeof(string), true));
        }
    }
}
