﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos.Configuration;

namespace Obymobi.Configuration
{
    public class CraveCloudConfigConstants : ConfigurationItemCollection
    {
        public const string CloudEnvironment = "CloudEnvironment";
        public const string ProductionOverwriteWebserviceBaseUrl = "ProductionOverwriteWebserviceBaseUrl";
        public const string ManualWebserviceBaseUrl = "ManualWebserviceBaseUrl";
        /// <summary>
        /// Altough the name doesn't explicitly states it, it does include the /files/ part
        /// </summary>
        public const string ManualAzureCdnBaseUrl = "ManualAzureCdnBaseUrl";
        /// <summary>
        /// Altough the name doesn't explicitly states it, it does include the /files/ part
        /// </summary>        
		public const string ManualAmazonCdnBaseUrl = "ManualAmazonCdnBaseUrl";
    }
}
