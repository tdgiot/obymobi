﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi
{
    public static class UITextSizes
    {
        public static List<UITextSize> TextSizes 
        {
            get
            {
                if(UITextSizes.textSizes == null)
                {
                    UITextSizes.textSizes = UITextSizes.CreateTextSizes();
                }
                return UITextSizes.textSizes;
            }
        }

        private static List<UITextSize> textSizes = null;

        private static List<UITextSize> CreateTextSizes()
        {
            List<UITextSize> textSizes = new List<UITextSize>();

            // Controls
            textSizes.Add(new UITextSize(UITextSizeType.Button, 22, 22, UITextSizeGroup.Controls));
            textSizes.Add(new UITextSize(UITextSizeType.Textbox, 22, 22, UITextSizeGroup.Controls));
            textSizes.Add(new UITextSize(UITextSizeType.Spinner, 22, 22, UITextSizeGroup.Controls));
            textSizes.Add(new UITextSize(UITextSizeType.Tab, 18, 22, UITextSizeGroup.Controls));
            textSizes.Add(new UITextSize(UITextSizeType.BulletPager, 14, 14, UITextSizeGroup.Controls));
            textSizes.Add(new UITextSize(UITextSizeType.CheckBox, 18, 18, UITextSizeGroup.Controls));
            textSizes.Add(new UITextSize(UITextSizeType.RadioButton, 18, 18, UITextSizeGroup.Controls));
            textSizes.Add(new UITextSize(UITextSizeType.TextView, 18, 20, UITextSizeGroup.Controls));
            textSizes.Add(new UITextSize(UITextSizeType.Toast, 25, 25, UITextSizeGroup.Controls));

            // Widgets
            textSizes.Add(new UITextSize(UITextSizeType.Widget, 18, 20, UITextSizeGroup.Widgets));
            textSizes.Add(new UITextSize(UITextSizeType.WidgetCaption, 16, 18, UITextSizeGroup.Widgets));
            textSizes.Add(new UITextSize(UITextSizeType.WidgetDescription, 14, 20, UITextSizeGroup.Widgets));

            // Dialogs
            textSizes.Add(new UITextSize(UITextSizeType.DialogTitle, 22, 28, UITextSizeGroup.Dialogs));
            textSizes.Add(new UITextSize(UITextSizeType.DialogSubtitle, 22, 24, UITextSizeGroup.Dialogs));
            textSizes.Add(new UITextSize(UITextSizeType.DialogText, 22, 26, UITextSizeGroup.Dialogs));

            // Lists
            textSizes.Add(new UITextSize(UITextSizeType.ListCategory, 18, 22, UITextSizeGroup.Lists));
            textSizes.Add(new UITextSize(UITextSizeType.ListItem, 18, 20, UITextSizeGroup.Lists));
            textSizes.Add(new UITextSize(UITextSizeType.ListItemPrice, 18, 22, UITextSizeGroup.Lists));

            // Pages
            textSizes.Add(new UITextSize(UITextSizeType.PageTitle, 30, 32, UITextSizeGroup.Pages));
            textSizes.Add(new UITextSize(UITextSizeType.PageDescription, 18, 22, UITextSizeGroup.Pages));
            textSizes.Add(new UITextSize(UITextSizeType.PageError, 18, 28, UITextSizeGroup.Pages));
            textSizes.Add(new UITextSize(UITextSizeType.PagePrice, 30, 32, UITextSizeGroup.Pages));
            textSizes.Add(new UITextSize(UITextSizeType.PageButton, 18, 22, UITextSizeGroup.Pages));

            // Entertainment
            textSizes.Add(new UITextSize(UITextSizeType.EntertainmentName, 24, 28, UITextSizeGroup.Entertainment));
            textSizes.Add(new UITextSize(UITextSizeType.EntertainmentDescription, 21, 24, UITextSizeGroup.Entertainment));

            // Messages
            textSizes.Add(new UITextSize(UITextSizeType.MessageTitle, 32, 34, UITextSizeGroup.Messages));
            textSizes.Add(new UITextSize(UITextSizeType.MessageText, 22, 26, UITextSizeGroup.Messages));

            // Browser
            textSizes.Add(new UITextSize(UITextSizeType.BrowserCloseButton, 26, 28, UITextSizeGroup.Browser));
            textSizes.Add(new UITextSize(UITextSizeType.BrowserAddressHeader, 26, 26, UITextSizeGroup.Browser));

            // PDF
            textSizes.Add(new UITextSize(UITextSizeType.PdfPanel, 26, 30, UITextSizeGroup.PDF));

            // Room controls
            textSizes.Add(new UITextSize(UITextSizeType.RoomControl, 18, 22, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlSpinner, 21, 21, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlStationName, 24, 26, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlStationDescription, 16, 18, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlStationNumber, 36, 40, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlWidgetCaption, 24, 26, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlWidgetValue, 60, 64, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlThermostatFanCaption, 24, 26, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlSleepViewTextView, 21, 21, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlArea, 22, 28, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlTwoStateButtonCenterText, 20, 20, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlTwoStateButtonTopBottomText, 40, 40, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlTemperatureSeekArc, 100, 100, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlTemperatureSeekArcWidget, 60, 60, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlTemperatureToggleButton, 18, 18, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlCurtainName, 21, 28, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlCurtainButton, 18, 18, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlLightName, 21, 21, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlLightPercentage, 15, 15, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlSceneName, 21, 24, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlSceneButton, 20, 20, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlBlindName, 21, 24, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlBlindButton, 20, 20, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlWidgetName, 21, 24, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlWidgetButton, 20, 20, UITextSizeGroup.RoomControls));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlRemoteControlButton, 20, 24, UITextSizeGroup.RoomControls));

            // Alterations
            textSizes.Add(new UITextSize(UITextSizeType.AlterationDialogTitle, 22, 24, UITextSizeGroup.Alterations));
            textSizes.Add(new UITextSize(UITextSizeType.AlterationDialogInput, 22, 22, UITextSizeGroup.Alterations));
            textSizes.Add(new UITextSize(UITextSizeType.AlterationDialogDescription, 18, 22, UITextSizeGroup.Alterations));
            textSizes.Add(new UITextSize(UITextSizeType.AlterationDialogPrice, 20, 20, UITextSizeGroup.Alterations));
            textSizes.Add(new UITextSize(UITextSizeType.AlterationDialogDate, 24, 24, UITextSizeGroup.Alterations));
            textSizes.Add(new UITextSize(UITextSizeType.AlterationDialogSummaryProduct, 24, 22, UITextSizeGroup.Alterations));
            textSizes.Add(new UITextSize(UITextSizeType.AlterationDialogSummaryPrice, 26, 24, UITextSizeGroup.Alterations));
            textSizes.Add(new UITextSize(UITextSizeType.AlterationDialogSummaryNoItems, 25, 22, UITextSizeGroup.Alterations));
            textSizes.Add(new UITextSize(UITextSizeType.AlterationDialogSummaryTotal, 30, 30, UITextSizeGroup.Alterations));

            // Product dialog
            textSizes.Add(new UITextSize(UITextSizeType.ProductDialogTitle, 24, 24, UITextSizeGroup.ProductDialog));
            textSizes.Add(new UITextSize(UITextSizeType.ProductDialogSubtitle, 19, 19, UITextSizeGroup.ProductDialog));
            textSizes.Add(new UITextSize(UITextSizeType.ProductDialogButton, 22, 22, UITextSizeGroup.ProductDialog));
            textSizes.Add(new UITextSize(UITextSizeType.ProductDialogPrice, 22, 22, UITextSizeGroup.ProductDialog));
            textSizes.Add(new UITextSize(UITextSizeType.ProductDialogDescription, 25, 25, UITextSizeGroup.ProductDialog));
            textSizes.Add(new UITextSize(UITextSizeType.ProductDialogColumnInstructions, 19, 19, UITextSizeGroup.ProductDialog));
            textSizes.Add(new UITextSize(UITextSizeType.ProductDialogColumnOptionSecondaryText, 16, 16, UITextSizeGroup.ProductDialog));
            textSizes.Add(new UITextSize(UITextSizeType.ProductDialogColumnOptionSuggestionText, 18, 18, UITextSizeGroup.ProductDialog));

            // Other
            textSizes.Add(new UITextSize(UITextSizeType.AlarmClockAlertDialog, 40, 40, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.BluetoothControlDialogSubtext, 18, 18, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.BluetoothControlDialogName, 24, 24, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.BluetoothControlDialogStatus, 16, 16, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.BluetoothDevicesDialog, 14, 14, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.CurrencyDialogRadioButton, 20, 20, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.LanguageDialogRadioButton, 20, 24, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.LogDialogText, 13, 13, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ManagementDialogText, 18, 18, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ManagementDialogTableNumberCaption, 30, 30, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ManagementDialogTableNumberValue, 70, 70, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ManagementDialogStatusCaption, 30, 30, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ManagementDialogStatusText, 20, 22, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.OrderitemAddedDialogButtonAdd, 37, 37, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.OrderitemAddedDialogQuantity, 20, 20, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ScreenSaverDialog, 64, 64, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ScreenSaverDialogTime, 400, 400, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ScreenSaverDialogColon, 320, 320, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ServiceItemAddedDialogButtonAdd, 37, 37, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ServiceItemAddedDialogQuantity, 20, 20, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.VenueDialog, 26, 26, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.WebAttachmentDialog, 26, 26, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.OrderViewHeader, 35, 35, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.OrderDeliverypointSmall, 14, 14, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.OrderDeliverypointMedium, 16, 16, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.OrderDeliverypointLarge, 18, 18, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ConnectBtDeviceWidget1x1, 16, 16, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ConnectBtDeviceWidget, 22, 22, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.RoomControlNavigationWidget1x2, 15, 15, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.VenueHeaderViewTab, 18, 18, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.OrderHistoryButton, 18, 20, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.OrderHistoryQuantity, 20, 20, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.VenueSummaryCategory, 16, 20, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ConsoleDialogTab, 18, 18, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ConsoleUIModeName, 20, 20, UITextSizeGroup.Other));
            textSizes.Add(new UITextSize(UITextSizeType.ScreenSaverDialogRadio, 64, 64, UITextSizeGroup.Other));

            return textSizes;
        }
        
        public static UITextSize GetByType(UITextSizeType type)
        {
            return UITextSizes.TextSizes.FirstOrDefault(x => x.Type == type);
        }
    }
}
