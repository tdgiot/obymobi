﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Obymobi.Integrations.RoomControl.Control4
{
    public class Control4Configuration
    {
        const int CONTROL4_ITEM_TYPE_DEVICE = 7;
        const int CONTROL4_ITEM_TYPE_ROOM = 8;
        const int CONTROL4_ITEM_TYPE_AGENT = 9;

        public List<Room> Rooms = new List<Room>();
        public List<Device> AdvancedScenes = new List<Device>();
        public List<Device> Scenes = new List<Device>();
        public List<Device> Services = new List<Device>();

        public Control4Configuration(Stream projectFileStream)
        {
            using (ZipInputStream zipInputStream = new ZipInputStream(projectFileStream))
            {
                ZipEntry zipEntry;
                while ((zipEntry = zipInputStream.GetNextEntry()) != null)
                {
                    if (zipEntry.Name.Equals("project.xml"))
                    {
                        XDocument xml = XDocument.Load(zipInputStream);
                        XElement systemItems = xml.Descendants("systemitems").FirstOrDefault();
                        if (systemItems != null)
                        {
                            this.FindRoomElements(systemItems.Elements("item"));
                        }
                        break;
                    }
                }
            }
        }

        private void FindRoomElements(IEnumerable<XElement> elements)
        {
            if (elements != null)
            {
                foreach (XElement element in elements)
                {
                    XElement typeElement = element.Element("type");
                    int type = typeElement != null ? int.Parse((string)typeElement) : 0;
                    if (type == CONTROL4_ITEM_TYPE_ROOM)
                    {
                        this.ParseRoom(element);
                    }
                    else if (type == CONTROL4_ITEM_TYPE_AGENT)
                    {
                        this.ParseAgent(element);
                    }
                    else
                    {
                        XElement subitems = element.Element("subitems");
                        if (subitems != null)
                        {
                            this.FindRoomElements(subitems.Elements("item"));
                        }
                    }
                }
            }
        }

        private void ParseRoom(XElement roomElement)
        {
            int roomId = int.Parse((string)roomElement.Element("id"));
            string roomName = (string)roomElement.Element("name");

            bool roomVisible = true;

            XElement stateElement = roomElement.Element("state");
            if (stateElement != null)
            {
                XDocument stateXml = XDocument.Parse(stateElement.Value);

                XElement roomHiddenElement = stateXml.Descendants("RoomHidden").FirstOrDefault();
                if (roomHiddenElement != null)
                {
                    roomVisible = !bool.Parse((string)roomHiddenElement.Value);
                }
            }

            if (roomVisible)
            {
                Control4Configuration.Room room = new Control4Configuration.Room(roomId, roomName);
                this.Rooms.Add(room);

                XElement subitems = roomElement.Element("subitems");
                if (subitems != null)
                {
                    this.FindDeviceElements(room, subitems.Elements("item"));
                }
            }
        }

        private void FindDeviceElements(Control4Configuration.Room room, IEnumerable<XElement> elements)
        {
            if (elements != null)
            {
                foreach (XElement element in elements)
                {
                    XElement typeElement = element.Element("type");
                    int type = typeElement != null ? int.Parse((string)typeElement) : 0;
                    if (type == CONTROL4_ITEM_TYPE_DEVICE)
                    {
                        this.ParseDevice(room, element);
                    }
                    else
                    {
                        XElement subitems = element.Element("subitems");
                        if (subitems != null)
                        {
                            this.FindDeviceElements(room, subitems.Elements("item"));
                        }
                    }
                }
            }
        }

        private void ParseDevice(Control4Configuration.Room room, XElement deviceElement)
        {
            int deviceId = int.Parse((string)deviceElement.Element("id"));
            string deviceName = (string)deviceElement.Element("name");
            string deviceDriver = (string)deviceElement.Descendants("config_data_file").FirstOrDefault();

            Control4Configuration.Device device = new Control4Configuration.Device(deviceId, deviceName);

            if (deviceDriver == "light.c4i" || deviceDriver == "outlet_light.c4i" || deviceDriver == "light_v2.c4i")
            {
                room.Lights.Add(device);
            }

            if (deviceDriver == "blind.c4i" || deviceDriver == "blind_mechoshade_single.c4i")
            {
                room.Blinds.Add(device);
            }

            if (deviceDriver == "control4_thermostat_proxy.c4i")
            {
                room.Thermostats.Add(device);
            }

            if (deviceDriver == "tv.c4i")
            {
                room.Televisions.Add(device);
            }
        }

        private void ParseAgent(XElement agentElement)
        {
            int agentId = int.Parse((string)agentElement.Element("id"));
            string agentName = (string)agentElement.Element("name");
            string agentDriver = (string)agentElement.Descendants("config_data_file").FirstOrDefault();

            XElement stateElement = agentElement.Element("state");
            if (stateElement != null && stateElement.Value != "")
            {
                XDocument stateXml = XDocument.Parse(stateElement.Value);

                IEnumerable<XElement> guestServiceElements = stateXml.Descendants("guest_service");
                if (guestServiceElements != null)
                {
                    foreach (XElement serviceElement in guestServiceElements)
                    {
                        int serviceId = int.Parse((string)serviceElement.Attribute("id"));
                        string serviceName = (string)serviceElement.Attribute("name");

                        Control4Configuration.Device service = new Control4Configuration.Device(serviceId, serviceName);

                        this.Services.Add(service);
                    }
                }

                IEnumerable<XElement> sceneElements = stateXml.Descendants("scene");
                if (sceneElements != null)
                {
                    foreach (XElement sceneElement in sceneElements)
                    {
                        XElement sceneIdElement = sceneElement.Element("sceneid");
                        if (sceneIdElement != null)
                        {
                            int sceneId = int.Parse((string)sceneIdElement);
                            string sceneName = (string)sceneElement.Element("name");

                            Control4Configuration.Device scene = new Control4Configuration.Device(sceneId, sceneName);

                            this.Scenes.Add(scene);
                        }
                    }
                }

                IEnumerable<XElement> advSceneElements = stateXml.Descendants("AdvScene");
                if (advSceneElements != null)
                {
                    foreach (XElement advSceneElement in advSceneElements)
                    {
                        int sceneId = int.Parse((string)advSceneElement.Element("scene_id"));
                        string sceneName = (string)advSceneElement.Element("name");

                        Control4Configuration.Device advScene = new Control4Configuration.Device(sceneId, sceneName);

                        this.AdvancedScenes.Add(advScene);
                    }
                }
            }   
        }

        public Control4Configuration.Room GetRoom(string name)
        {
            return this.Rooms.FirstOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public bool ContainsRoom(string name)
        {
            return this.GetRoom(name) != null;
        }

        public bool ContainsService(string name)
        {
            return this.Services.FirstOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)) != null;
        }

        public bool ContainsScene(string name)
        {
            return this.Scenes.FirstOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)) != null ||
                   this.AdvancedScenes.FirstOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)) != null;
        }

        public class Room : Device
        {
            public List<Device> Lights = new List<Device>();
            public List<Device> Blinds = new List<Device>();
            public List<Device> Thermostats = new List<Device>();
            public List<Device> Televisions = new List<Device>();

            public Room(int id, string name)
                : base(id, name)
            {
            }

            public bool ContainsLight(string name)
            {
                return this.Lights.FirstOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)) != null;
            }

            public bool ContainsBlind(string name)
            {
                return this.Blinds.FirstOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)) != null;
            }

            public bool ContainsThermostat(string name)
            {
                return this.Thermostats.FirstOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)) != null;
            }

            public bool ContainsTelevision(string name)
            {
                return this.Televisions.FirstOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)) != null;
            }
        }

        public class Device
        {
            public int Id { get; private set; }
            public string Name { get; private set; }

            public Device(int id, string name)
            {
                this.Id = id;
                this.Name = name;
            }
        }
    }
}
