﻿using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public abstract class ApiHelperBase<T>
    {
        private string javaWebToken;
        public ApiHelperBase(string javaWebToken)
        {
            this.javaWebToken = javaWebToken;

            // Init the service
            GoogleCredential credential;
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(this.javaWebToken)))
            {
                // https://cloud.google.com/storage/docs/json_api/v1/json-api-dotnet-samples
                credential = GoogleCredential.FromStream(stream);
                if (credential.IsCreateScopedRequired)
                    credential = credential.CreateScoped(this.GetRequiredScopes());
            }

            var initializer = new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = "Obymobi.Analytics.GoogleAnalytics.ApiHelperBase"
            };

            this.service = this.InitializeService(initializer);
        }

        protected abstract T InitializeService(BaseClientService.Initializer initializer);

        protected abstract string[] GetRequiredScopes();

        protected T GetService()             
        {
            return this.service;            
        }

        private T service;        
    }
}
