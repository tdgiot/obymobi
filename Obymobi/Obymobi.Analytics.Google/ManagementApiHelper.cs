﻿using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public class ManagementApiHelper : ApiHelperBase<AnalyticsService>
    {
        public ManagementApiHelper(string javaWebToken) : base(javaWebToken)
        { }

        public async Task<IList<Account>> GetAccountsAsync()
        {
            var result = await this.GetService().Management.Accounts.List().ExecuteAsync();
            return result.Items;
        }

        public async Task<IList<Webproperty>> GetPropertiesAsync(Account account)
        {
            return await this.GetPropertiesAsync(account.Id);
        }

        public async Task<IList<Webproperty>> GetPropertiesAsync(string accountId)
        {            
            var result = await this.GetService().Management.Webproperties.List(accountId).ExecuteAsync();
            return result.Items;            
        }

        public async Task<IList<Profile>> GetViewsAsync(Webproperty property)
        {
            var result = await this.GetService().Management.Profiles.List(property.AccountId, property.Id).ExecuteAsync();
            return result.Items;
        }

        public IList<Account> GetAccounts()
        {
            var result = this.GetService().Management.Accounts.List().Execute();
            return result.Items;
        }

        public IList<Webproperty> GetProperties(Account account)
        {
            return this.GetProperties(account.Id);
        }

        public IList<Webproperty> GetProperties(string accountId)
        {
            var result = this.GetService().Management.Webproperties.List(accountId).Execute();
            return result.Items;
        }

        public IList<Profile> GetViews(Webproperty property)
        {
            var result = this.GetService().Management.Profiles.List(property.AccountId, property.Id).Execute();
            return result.Items;
        }

        public IList<FlattenedViewInfo> GetAllViewsFlattened()
        {
            var accounts = this.GetAccounts();
            List<FlattenedViewInfo> toReturn = new List<FlattenedViewInfo>();

            foreach (var account in accounts)
            {
                var properties = this.GetProperties(account);
                foreach (var property in properties)
                {
                    var views = this.GetViews(property);
                    foreach (var view in views)
                    {
                        toReturn.Add(new FlattenedViewInfo(account, property, view));
                    }
                }
            }

            return toReturn;
        }

        public async Task<IList<CustomDimension>> GetCustomDimensions(Account account, Webproperty webproperty)
        {
            return await this.GetCustomDimensions(account.Id, webproperty.Id);
        }

        public async Task<IList<CustomDimension>> GetCustomDimensions(string accountId, string webPropertyId)
        {
            var dimensionsResult = await this.GetService().Management.CustomDimensions.List(accountId, webPropertyId).ExecuteAsync();            
            return dimensionsResult.Items ?? new List<CustomDimension>();
        }

        public async Task<bool> ValidateCustomDimensions(Account account, Webproperty webproperty, bool fixIssues = false)
        {
            return await this.ValidateCustomDimensions(account.Id, webproperty.Id, fixIssues);
        }

        public async Task<bool> ValidateCustomDimensions(string accountId, string webPropertyId, bool fixIssues = false)
        {
            // The assumption that we make is that the Dimensions are always configured in a continous sequence of id's (i.e. 1, 2, 3, 4, etc.)            
            bool isValid = true;
            
            var dimensionDefinitions = new List<CustomDimensionDefinition>(Enum.GetValues(typeof(CustomDimensionDefinition)).Cast<CustomDimensionDefinition>()); 
            IList<CustomDimension> dimensions = await this.GetCustomDimensions(accountId, webPropertyId);

            for (int i = dimensions.Count - 1; i >= 0; i--)
            {
                CustomDimension dimension = dimensions[i];

                if (!dimension.Index.HasValue)
                    continue; // Should never happen, and we can't handle it - leave it.

                // Do we need this dimension?
                bool needsUpdate = false;
                bool dimensionActive = dimension.Active ?? false;

                CustomDimensionDefinition dimensionDefinition = dimensionDefinitions.FirstOrDefault(x => (int)x == dimension.Index);
                if (dimensionDefinition == CustomDimensionDefinition.Unknown)
                {
                    // This dimension is configured but not Template exists for it, so deactivate. (But it's not invalid, as this gives no issues - just an additional dimension)
                    if (dimensionActive)
                    {
                        dimension.Active = false;
                        needsUpdate = true;
                    }
                }
                else if (!(dimension.Name.Equals(dimensionDefinition.ToString()) && dimensionActive))
                {
                    // This dimension is configured but not according to our Template
                    dimension.Name = dimensionDefinition.ToString();
                    dimension.Active = true;
                    needsUpdate = true;

                    // If we need to fix it means we are not valid: 
                    isValid = false;
                }

                if (needsUpdate && fixIssues)
                {
                    await this.GetService().Management.CustomDimensions.Update(dimension, accountId, webPropertyId, dimension.Id).ExecuteAsync();                    
                }

                // Remove a 'used' template because it's taken care of, this means we keep a remainder that we
                // still need to set at Google 
                if (dimensionDefinition != CustomDimensionDefinition.Unknown)
                    dimensionDefinitions.Remove(dimensionDefinition);
            }

            dimensionDefinitions.Remove(CustomDimensionDefinition.Unknown);

            // All Template that are still in our collection were not found at Google, so must be created
            if (dimensionDefinitions.Count > 0)
                isValid = false; // We still need to add items, so invalid.

            if (fixIssues)
            {
                foreach (var dimension in dimensionDefinitions.OrderBy(x => (int)x))
                {
                    CustomDimension cd = new CustomDimension();
                    cd.Name = dimension.ToString();
                    cd.Index = (int)dimension;
                    cd.Active = true;
                    cd.Scope = "HIT";
                    await this.GetService().Management.CustomDimensions.Insert(cd, accountId, webPropertyId).ExecuteAsync();
                }
            }

            return isValid;
        }

        protected override AnalyticsService InitializeService(BaseClientService.Initializer initializer)
        {
            return new AnalyticsService(initializer);
        }

        protected override string[] GetRequiredScopes()
        {
            return new string[] { AnalyticsService.Scope.Analytics, AnalyticsService.Scope.AnalyticsEdit };
        }
    }
}