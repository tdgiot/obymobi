﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public class MetricNames
    {
        static MetricNames()
        {
            List<MetricName> names = new List<MetricName>();

            names.Add(TotalEvents = new MetricName("Total Events", "ga:totalEvents", "EVENT"));
            names.Add(ScreenViews = new MetricName("Screen Views", "ga:screenviews", "APPVIEW"));

            dimensions = new ReadOnlyCollection<MetricName>(names);
        }

        private readonly static IReadOnlyCollection<MetricName> dimensions;
        public readonly static MetricName TotalEvents;
        public readonly static MetricName ScreenViews;

        public static bool TryGetMetricName(string name, out MetricName metricName)
        {
            metricName = MetricNames.GetMetricName(name);
            return (metricName != null);
        }

        public static MetricName GetMetricName(string name)
        {
            return dimensions.SingleOrDefault(x => x.FriendlyName.Equals(name, StringComparison.InvariantCultureIgnoreCase) ||
                                                    x.ReportingApiName.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public class MetricName
        {
            public MetricName(string friendlyName, string reportingApiName, string bigqueryHitType)
            {
                this.FriendlyName = friendlyName;
                this.ReportingApiName = reportingApiName;
                this.BigqueryHitType = bigqueryHitType;
            }

            public readonly string FriendlyName;
            public readonly string ReportingApiName;
            public readonly string BigqueryHitType;

            /// <summary>
            /// Allow it to be used as if it were a string
            /// </summary>
            /// <param name="dimension"></param>
            public static implicit operator string (MetricName dimension)
            {
                return dimension.ReportingApiName;
            }
        }

        // Just add more when you use more.
    }
}
