﻿using Google.Apis.Analytics.v3.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public struct FlattenedViewInfo
    {
        public FlattenedViewInfo(Account account, Webproperty webproperty, Profile profile)
        {
            this.AccountName = account.Name;
            this.AccountId = account.Id;
            this.PropertyName = webproperty.Name;
            this.PropertyId = webproperty.Id;
            this.ViewName = profile.Name;
            this.ViewId = profile.Id;
            this.Name = string.Format("{0} - {1} - {2}", this.AccountName, this.PropertyName, this.ViewName);
        }

        public readonly string AccountName;
        public readonly string PropertyName;
        public readonly string ViewName;
        public readonly string AccountId;
        public readonly string PropertyId;
        public readonly string ViewId;
        public readonly string Name;
    }
}
