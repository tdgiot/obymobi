﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public static class DimensionNames 
    {
        static DimensionNames()
        {
            List<DimensionName> names = new List<DimensionName>();
            
            names.Add(EventCategory = new DimensionName("Event Category", "ga:eventCategory", "hits.eventInfo.eventCategory"));
            names.Add(EventLabel = new DimensionName("Event Label", "ga:eventLabel", "hits.eventInfo.eventLabel"));
            names.Add(EventAction = new DimensionName("Event Action", "ga:eventAction", "hits.eventInfo.eventAction"));
            names.Add(ScreenName = new DimensionName("Screen Name", "ga:screenName", "hits.appInfo.screenName"));
            names.Add(Date = new DimensionName("Date", "ga:date", "date"));            

            dimensions = new ReadOnlyCollection<DimensionName>(names);
        }

        private readonly static IReadOnlyCollection<DimensionName> dimensions;
        public readonly static DimensionName EventCategory;
        public readonly static DimensionName EventLabel;
        public readonly static DimensionName EventAction;
        public readonly static DimensionName ScreenName;
        public readonly static DimensionName Date;

        public static bool TryGetDimensionName(string name, out DimensionName dimensionName)
        {
            dimensionName = DimensionNames.GetDimensionName(name);
            return (dimensionName != null);
        }

        public static DimensionName GetDimensionName(string name)
        {
            return dimensions.SingleOrDefault(x =>  x.FriendlyName.Equals(name, StringComparison.InvariantCultureIgnoreCase) || 
                                                    x.ReportingApiName.Equals(name, StringComparison.InvariantCultureIgnoreCase) ||
                                                    x.ReportingApiName.Equals("ga:" + name, StringComparison.InvariantCultureIgnoreCase));
        }

        public class DimensionName
        {
            public DimensionName(string friendlyName, string reportingApiName, string bigqueryFieldName)
            {
                this.FriendlyName = friendlyName;
                this.ReportingApiName = reportingApiName;
                this.BigqueryFieldName = bigqueryFieldName;
            }

            public readonly string FriendlyName;
            public readonly string ReportingApiName;
            public readonly string BigqueryFieldName;

            /// <summary>
            /// Allow it to be used as if it were a string
            /// </summary>
            /// <param name="dimension"></param>
            public static implicit operator string (DimensionName dimension)
            {
                return dimension.ReportingApiName;
            }
        }
    }
}

