using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
* WARNING: GENERATED CODE
* Changes to this class are futile - the full file will be replaced when code is generated
* To extend this class do so in the HitBase-class and re-generate the code
* RTFM: https://goo.gl/qKrdQX 
*/

namespace Obymobi.Analytics.Google
{
    public enum CustomDimensionDefinition : int
    {
        Unknown = 0,
        LegacyCustomDimension1 = 1,
        LegacyCustomDimension2 = 2,
        MacAddress = 3,
        DeviceType = 4,
        TimeStamp = 5,
        ProductName = 6,
        AlterationName = 7,
        ServiceRequestName = 8,
        CategoryName = 9,
        EntertainmentName = 10,
        ApplicationType = 11,
        DeliverypointNumber = 12,
        DeliverypointGroupName = 13,
        PrimaryKeys = 14,
        IsTablet = 15,
        OperatingSystem = 16,
        CompanyName = 17,
        LegacyApplicationVersion = 18,
        NavigationSource = 19,
        LegacyApplicationName = 20,
        HitVersion = 21,
        CompanyId = 22,
        ProductId = 23,
        CategoryId = 24,
        EntertainmentId = 25,
        DeliverypointId = 26,
        DeliverypointName = 27,
        AdvertisementId = 28,
        AdvertisementName = 29,
        AdvertisementLocation = 30,
        SiteId = 31,
        SiteName = 32,
        PageId = 33,
        PageName = 34,
        TabId = 35,
        TabName = 36,
        MessageId = 37,
        MessageName = 38,
        RoomControlAreaId = 39,
        RoomControlAreaName = 40,
        WidgetId = 41,
        WidgetName = 42,
        WidgetCaption = 43,
        DeliverypointGroupId = 44,
        ClientId = 45,
        OrderSource = 46,
        Action = 47,
        AttachmentName = 48,
        AttachmentId = 49,
        RoomControlAreaSectionId = 50,
        RoomControlAreaSectionName = 51,
        RoomControlAreaSectionItemId = 52,
        RoomControlAreaSectionItemName = 53,
        RoomControlSceneName = 54,
        RoomControlComponentName = 55,
        RoomControlComponentId = 56,
        RoomControlStationListingId = 57,
        RoomControlStationListingName = 58,
        RoomControlThermostatMode = 59,
        RoomControlThermostatScale = 60,
        RoomControlThermostatTemperature = 61,
        RoomControlThermostatFanMode = 62,
        RoomControlStationId = 63,
        RoomControlStationName = 64,
        RoomControlTvRemoteAction = 65,
        MediaId = 66,
        OrderId = 67,
        OrderitemId = 68,
        TestPrefix = 69,
        MessageResponse = 70,
        ClientOperationMode = 71,
        OrderType = 72,
        HitGuid = 73,
        MapId = 74,
        MapName = 75,
        PointOfInterestId = 76,
        PointOfInterestName = 77,
        ClientConfigurationId = 78,
        TerminalConfigurationId = 79,
        AdvertisementConfigurationId = 80,
        EntertainmentConfigurationId = 81,
        /*
        * WARNING: GENERATED CODE
        * Changes to this class are futile - the full file will be replaced when code is generated
        * To extend this class do so in the HitBase-class and re-generate the code
        * RTFM: https://goo.gl/qKrdQX 
        */
    }
}
