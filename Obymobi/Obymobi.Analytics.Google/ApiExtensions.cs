﻿using Google.Apis.AnalyticsReporting.v4.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public static class ApiExtensions
    {
        public static void AddMetric(this IList<Metric> list, string metric)
        {
            if (list == null)
                list = new List<Metric>();

            list.Add(new Metric { Expression = metric });
        }

        public static void AddDimension(this IList<Dimension> list, DimensionNames.DimensionName dimension)
        {
            if (list == null)
                list = new List<Dimension>();

            list.Add(new Dimension { Name = dimension.ReportingApiName });
        }

        public static void AddDimension(this IList<Dimension> list, CustomDimensionDefinition dimension)
        {
            if (list == null)
                list = new List<Dimension>();

            list.Add(new Dimension { Name = "ga:dimension" + (int)dimension });
        }

        public static void AddDateRange(this IList<DateRange> list, DateRange range)
        {
            if (list == null)
                list = new List<DateRange>();

            list.Add(range);                    
        }
    }
}
