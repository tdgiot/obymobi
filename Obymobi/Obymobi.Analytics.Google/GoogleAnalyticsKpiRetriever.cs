﻿using Dionysos;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public class GoogleAnalyticsKpiRetriever : ApiHelperBase<AnalyticsReportingService>
    {
        //private AccountConfiguration accountConfiguration;
        private string viewId;
        // 10 should be save, but just to be sure: https://developers.google.com/analytics/devguides/reporting/core/v4/limits-quotas
        private const int requestsPerSecond = 2;
        public GoogleAnalyticsKpiRetriever(string javaWebToken, string viewId) : base(javaWebToken)
        {
            this.viewId = viewId;
        }

        public Task<Int32> GetScreenViewCountAsync(Filter filter)
        {
            return GetSingleCountAsync(filter, MetricNames.ScreenViews);
        }

        public Task<Int32> GetEventCountAsync(Filter filter)
        {
            return GetSingleCountAsync(filter, MetricNames.TotalEvents);
        }

        private async Task<Int32> GetSingleCountAsync(Filter filter, string metric)
        {
            ReportRequest request = this.PrepareRequest(filter);

            // Metrics            
            request.Metrics.AddMetric(metric);

            var result = await GetReportAsync(request);

            int toReturn;
            TryGetSingleValue(result, out toReturn);
            return toReturn;
        }

        public List<IntervalResult<int>> GetScreenViewCount(Filter filter, ReportingInterval interval)
        {
            List<Tuple<DateTime, DateTime>> periods = IntervalCreator.GetIntervals(filter, interval);
            var toReturn = new List<IntervalResult<int>>();

            using (var rateGate = new RateGate(2, TimeSpan.FromSeconds(requestsPerSecond)))
            {
                Parallel.ForEach(periods, (period) => {                    
                    rateGate.WaitToProceed();                    
                    var periodFilter = filter.Clone();
                    periodFilter.FromUtc = period.Item1;
                    periodFilter.TillUtc = period.Item2;
                    toReturn.Add(new IntervalResult<int>(period.Item1, period.Item2, GetScreenViewCount(periodFilter)));
                });
            }

            return toReturn;
        }

        public int GetScreenViewCount(Filter filter)
        {
            return GetSingleCount(filter, MetricNames.ScreenViews);
        }

        public List<IntervalResult<int>> GetEventCount(Filter filter, ReportingInterval interval)
        {
            List<Tuple<DateTime, DateTime>> periods = IntervalCreator.GetIntervals(filter, interval);
            var toReturn = new List<IntervalResult<int>>();

            using (var rateGate = new RateGate(2, TimeSpan.FromSeconds(requestsPerSecond)))
            {
                Parallel.ForEach(periods, (period) => {
                    Debug.WriteLine("{0} - {1} - Ready", period.Item1, DateTime.Now.Ticks);
                    rateGate.WaitToProceed();
                    Debug.WriteLine("{0} - {1} - Go", period.Item1, DateTime.Now.Ticks);
                    var periodFilter = filter.Clone();
                    periodFilter.FromUtc = period.Item1;
                    periodFilter.TillUtc = period.Item2;
                    toReturn.Add(new IntervalResult<int>(period.Item1, period.Item2, GetEventCount(periodFilter)));
                });
            }

            return toReturn;
        }

        public int GetEventCount(Filter filter)
        {
            return GetSingleCount(filter, MetricNames.TotalEvents);
        }

        private int GetSingleCount(Filter filter, string metric)
        {
            ReportRequest request = this.PrepareRequest(filter);

            // Metrics            
            request.Metrics.AddMetric(metric);

            var result = GetReport(request);

            int toReturn;
            TryGetSingleValue(result, out toReturn);
            return toReturn;
        }

        public List<TopXItem> GetTopXEventCount(Filter filter)
        {
            return this.GetTopXCount(filter, MetricNames.TotalEvents);
        }

        public List<TopXItem> GetTopXScreenCount(Filter filter)
        {
            return this.GetTopXCount(filter, MetricNames.ScreenViews);
        }

        public List<TopXItem> GetTopXCount(Filter filter, string metric)
        {
            ReportRequest request = this.PrepareRequest(filter);
            request.Metrics.AddMetric(metric);
            request.OrderBys.Add(new OrderBy { FieldName = metric, SortOrder = "descending" });            

            var results = GetReport(request);

            List<TopXItem> toReturn = new List<TopXItem>();

            // First line are the names of the dimensions
            TopXItem headings = new TopXItem();
            headings.Dimensions.AddRange(filter.Dimensions);
            toReturn.Add(headings);

            if (results != null && results.Data != null && results.Data.Rows != null) // Hippe shit: https://msdn.microsoft.com/en-us/library/dn986595.aspx (https://visualstudio.uservoice.com/forums/121579-visual-studio/suggestions/3990187-add-operator-to-c)
            {
                foreach (var row in results.Data.Rows)
                {
                    TopXItem topEvent = new TopXItem();
                    topEvent.Count = Convert.ToInt32(row.Metrics[0].Values[0] ?? "0");
                    topEvent.Dimensions.AddRange(row.Dimensions);                        
                    toReturn.Add(topEvent);
                }
            }            

            return toReturn;
        }

        public class TopXItem
        {
            public TopXItem()
            {
                Dimensions = new List<string>();
            }

            public List<string> Dimensions{ get; set; }
            public int Count { get; set; }
        }

        private enum MessageResponse
        {
            None,
            Yes,
            No,
            Ok               
        }

        public List<MessageStatistics> GetMessagingStatistics(Filter filter)
        {
            // We always need to apply a filter for Event Category
            filter.EventCategories.Add("Messaging");

            ReportRequest request = this.PrepareRequest(filter);
            request.Dimensions.AddDimension(DimensionNames.Date);
            request.Dimensions.AddDimension(DimensionNames.EventAction);
            request.Dimensions.AddDimension(DimensionNames.EventLabel);
            request.Dimensions.AddDimension(CustomDimensionDefinition.MessageResponse);
            request.Metrics.AddMetric(MetricNames.TotalEvents);
            request.PageSize = 10000; // We limit on the ToReturn, as each event can return more than 1 row (for the different Event Actions)            

            var results = GetReport(request);

            if (results.Data.RowCount.HasValue && results.Data.RowCount > 10000)
                throw new NotImplementedException("Can't process more than 10k rows as it requires paging");

            List<MessageStatistics> toReturn = new List<MessageStatistics>();

            if (results != null && results.Data != null && results.Data.Rows != null) // Hippe shit: https://msdn.microsoft.com/en-us/library/dn986595.aspx (https://visualstudio.uservoice.com/forums/121579-visual-studio/suggestions/3990187-add-operator-to-c)
            {
                foreach (var row in results.Data.Rows)
                {
                    DateTime date = DateTime.ParseExact(row.Dimensions[0], "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                    string action = row.Dimensions[1];
                    string title = row.Dimensions[2];
                    MessageResponse response = MessageResponse.None;
                    if (row.Dimensions[3].Equals("Yes"))
                        response = MessageResponse.Yes;
                    else if (row.Dimensions[3].Equals("No"))
                        response = MessageResponse.No;
                    else if (row.Dimensions[3].Equals("Ok"))
                        response = MessageResponse.Ok;

                    int eventCount = Convert.ToInt32(row.Metrics[0].Values[0]);

                    // Fetch or create the MessageStatistics for this Date + Title combination
                    MessageStatistics messageStats = toReturn.FirstOrDefault(x => x.Date.Equals(date) && x.Title.Equals(title));
                    if (messageStats == null)
                    {
                        messageStats = new MessageStatistics();
                        messageStats.Date = date;
                        messageStats.Title = title;
                        toReturn.Add(messageStats);
                    }

                    switch (action.ToLower())
                    {
                        case "message shown: idle":                                                        
                            messageStats.ShownIdle = eventCount;
                            break;
                        case "message shown: not idle":
                            messageStats.ShownNotIdle = eventCount;
                            break;
                        case "message viewed":
                            messageStats.Views = eventCount;
                            break;
                        case "message expired":
                            messageStats.Expirations = eventCount;
                            break;                        
                        case "message clicked":
                            if (response == MessageResponse.Yes)
                                messageStats.PostiveResponse = eventCount;
                            else if (response == MessageResponse.No)
                                messageStats.NegativeResponse = eventCount;
                            else if (response == MessageResponse.Ok)
                                messageStats.OkResponse = eventCount;
                            break;
                        default:
                            // can't handle
                            break;
                    }
                }
            }

            return toReturn;
        }

        public class MessageStatistics
        {
            public DateTime Date { get; set; }
            public String Title { get; set; }           
            public int ShownIdle { get; set; }
            public int ShownNotIdle { get; set; }
            public int Views { get; set; }
            public int Expirations { get; set; }
            public int Clicks { get; set; }            
            public int PostiveResponse { get; set; }
            public int NegativeResponse { get; set; }            
            public int OkResponse { get; set; }

            public int Responses { get { return this.PostiveResponse + this.NegativeResponse + this.OkResponse; } }
            public int Shown { get { return this.ShownIdle + this.ShownNotIdle; } }
        }


        private async Task<Report> GetReportAsync(ReportRequest request)
        {
            GetReportsRequest batchRequest = new GetReportsRequest();
            batchRequest.ReportRequests = new List<ReportRequest>();
            batchRequest.ReportRequests.Add(request);            

            // Fix FilterExpression
            if (request.FiltersExpression.EndsWith(";"))
                request.FiltersExpression = request.FiltersExpression.Substring(0, request.FiltersExpression.Length - 1);

            var service = GetService();
            var result = await service.Reports.BatchGet(batchRequest).ExecuteAsync().ConfigureAwait(false); // Prevent dead locks:             

            return result.Reports[0];
        }        

        private Report GetReport(ReportRequest request)
        {
            GetReportsRequest batchRequest = new GetReportsRequest();
            batchRequest.ReportRequests = new List<ReportRequest>();
            batchRequest.ReportRequests.Add(request);

            // Fix FilterExpression
            if (request.FiltersExpression.EndsWith(";"))
                request.FiltersExpression = request.FiltersExpression.Substring(0, request.FiltersExpression.Length - 1);

            var service = GetService();
            var result = service.Reports.BatchGet(batchRequest).Execute();

            return result.Reports[0];
        }

        // https://developers.google.com/analytics/devguides/reporting/core/v4/samples#dimensions_and_metrics

        private ReportRequest PrepareRequest(Filter filter)
        {
            ReportRequest request = new ReportRequest();
            request.Metrics = new List<Metric>();
            request.Dimensions = filter.GetDimensions();
            request.DateRanges = new List<DateRange>();
            request.OrderBys = new List<OrderBy>();

            request.FiltersExpression = filter.GetFilterExpression();
            request.DateRanges.Add(filter.GetDateRange());
            request.PageSize = filter.NoOfResults;
            request.ViewId = this.viewId;

            return request;
        }

        public bool TryGetSingleValue<T>(Report report, out T value)
        {
            bool toReturn = false;
            value = default(T);

            if (report != null &&
                    report.Data != null &&
                    report.Data.RowCount.HasValue &&
                    report.Data.RowCount.Value > 0 &&
                    report.Data.Rows[0].Metrics != null &&
                    report.Data.Rows[0].Metrics.Count > 0 &&
                    report.Data.Rows[0].Metrics[0].Values != null &&
                    report.Data.Rows[0].Metrics[0].Values.Count > 0 &&
                    report.Data.Rows[0].Metrics[0].Values[0] != null)
            {
                try
                {
                    value = (T)Convert.ChangeType(report.Data.Rows[0].Metrics[0].Values[0], typeof(T));
                    toReturn = true;
                }
                catch { } // Type could not be changed
            }

            if (!toReturn)
                value = default(T);

            return toReturn;
        }

        protected override AnalyticsReportingService InitializeService(BaseClientService.Initializer initializer)
        {
            return new AnalyticsReportingService(initializer);
        }

        protected override string[] GetRequiredScopes()
        {
            return new string[] { AnalyticsReportingService.Scope.AnalyticsReadonly };
        }

        public void ThisShouldNeverExist()
        {
            List<ReportRequest> requests = new List<ReportRequest>();

            DateTime start = DateTime.Now.Date.AddDays(-31);
            for (int i = 0; i < 30; i++)
            {
                var filter = new Filter { CompanyIds = new List<int> { 343 }, FromUtc = start.AddDays(i), TillUtc = start.AddDays(i) };
                var request = this.PrepareRequest(filter);
                if (request.FiltersExpression.EndsWith(";"))
                    request.FiltersExpression = request.FiltersExpression.Substring(0, request.FiltersExpression.Length - 1);
                request.Metrics.AddMetric(MetricNames.TotalEvents);
                requests.Add(request);
            }

            try
            {
                GetReportsRequest batchRequest = new GetReportsRequest();
                batchRequest.ReportRequests = new List<ReportRequest>();

                foreach (var request in requests)
                    batchRequest.ReportRequests.Add(request);

                // Fix FilterExpression
                var service = GetService();
                Stopwatch sw = new Stopwatch();
                sw.Start();
                var result = service.Reports.BatchGet(batchRequest).Execute();
                sw.Stop();
                Debug.WriteLine("Stopwatch: " + sw.ElapsedMilliseconds);

                // Validate
                foreach (var report in result.Reports)
                {
                    int toReturn;
                    TryGetSingleValue(report, out toReturn);
                    Debug.WriteLine("Result: " + toReturn);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Ex: " + ex.Message);
            }
        }
    }
}

