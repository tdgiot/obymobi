﻿using Google.Apis.Bigquery.v2;
using Google.Apis.Bigquery.v2.Data;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public class BigqueryKpiRetriever : ApiHelperBase<BigqueryService>
    {
        private string projectId;
        private string tableBaseName;
        private bool disableCaching;
        public BigqueryKpiRetriever(string javaWebToken, string projectId, string tableBaseName, bool disableCaching = false) : base(javaWebToken)
        {
            if (string.IsNullOrWhiteSpace(projectId))
                throw new ArgumentException("Empty", "ProjectId");

            if (string.IsNullOrWhiteSpace(tableBaseName))
                throw new ArgumentException("Empty", "TableBaseName");

            this.projectId = projectId;
            this.tableBaseName = tableBaseName;
            this.disableCaching = disableCaching;
        }

        public int GetScreenViewCount(Filter filter)
        {
            return GetSingleCount(filter, MetricNames.ScreenViews);
        }

        public int GetEventCount(Filter filter)
        {
            return GetSingleCount(filter, MetricNames.TotalEvents);
        }      

        private int GetSingleCount(Filter filter, MetricNames.MetricName metricName)
        {            
            int toReturn;
            this.TryGetSingleValue(filter, metricName, out toReturn);
            return toReturn;
        }

        public List<TopXItem> GetTopXEventCount(Filter filter)
        {
            throw new NotImplementedException();
        }

        public List<TopXItem> GetTopXScreenCount(Filter filter)
        {
            throw new NotImplementedException();
        }

        public List<TopXItem> GetTopXCount(Filter filter, string metric)
        {
            throw new NotImplementedException();
            //ReportRequest request = this.PrepareRequest(filter);
            //request.Metrics.AddMetric(metric);
            //request.OrderBys.Add(new OrderBy { FieldName = metric, SortOrder = "descending" });

            //var results = GetReport(request);

            //List<TopXItem> toReturn = new List<TopXItem>();

            //// First line are the names of the dimensions
            //TopXItem headings = new TopXItem();
            //headings.Dimensions.AddRange(filter.Dimensions);
            //toReturn.Add(headings);

            //if (results != null && results.Data != null && results.Data.Rows != null) // Hippe shit: https://msdn.microsoft.com/en-us/library/dn986595.aspx (https://visualstudio.uservoice.com/forums/121579-visual-studio/suggestions/3990187-add-operator-to-c)
            //{
            //    foreach (var row in results.Data.Rows)
            //    {
            //        TopXItem topEvent = new TopXItem();
            //        topEvent.Count = Convert.ToInt32(row.Metrics[0].Values[0] ?? "0");
            //        topEvent.Dimensions.AddRange(row.Dimensions);
            //        toReturn.Add(topEvent);
            //    }
            //}

            //return toReturn;
        }

        public class TopXItem
        {
            public TopXItem()
            {
                Dimensions = new List<string>();
            }

            public List<string> Dimensions { get; set; }
            public int Count { get; set; }
        }

        public bool TryGetSingleValue<T>(Filter filter, MetricNames.MetricName metricName, out T value)
        {            
            bool toReturn = false;
            value = default(T);

            var response = this.DoQuery(filter, metricName);

            if (response != null
                && response.Rows != null
                && response.Rows.Count == 1
                && response.Rows[0].F.Count == 1)
            {
                try
                {
                    value = (T)Convert.ChangeType(response.Rows[0].F[0].V, typeof(T));
                    toReturn = true;
                }
                catch { } // Type could not be changed
            }

            if (!toReturn)
                value = default(T);

            return toReturn;
        }

        private QueryResponse DoQuery(Filter filter, MetricNames.MetricName metricName)
        {
            // API Explorer: https://developers.google.com/apis-explorer/#p/bigquery/v2/
            QueryRequest requestBody = new QueryRequest();
            requestBody.Query = this.BuildQuery(filter, metricName);
            requestBody.TimeoutMs = 25000; // Takes 12000 on average.
            // requestBody.UseQueryCache = !this.disableCaching; // GAKR seems to give strange behaviour

            var request = new JobsResource.QueryRequest(GetService(), requestBody, this.projectId);            
            request.Fields = "rows,totalBytesProcessed,totalRows";

            QueryResponse response = request.Execute();

            Debug.WriteLine("Cached: " + response.CacheHit);
            if (response.Rows != null)
            {
                Debug.WriteLine("Results: " + response.Rows.Count);

                foreach (var row in response.Rows)
                {
                    Debug.WriteLine(string.Format("{0}", String.Join(",", row.F.Select(cell => cell.V.ToString()).ToList())));
                }
            }
            else
                Debug.WriteLine("No results");

            return response;
        }    

        private string BuildQuery(Filter filter, MetricNames.MetricName metricName)
        {
            // FROM (Table date ranges)
            // https://support.google.com/analytics/answer/4419694?hl=en#3days
            // TABLE_DATE_RANGE([73156703.ga_sessions_], TIMESTAMP('2016-01-01'), TIMESTAMP('2016-01-31')
            string fromString;
            if(filter.FromUtc.Date.Equals(filter.TillUtc.Date))
                fromString = string.Format("[{0}{1:yyyyMMdd}]", this.tableBaseName, filter.FromUtc);
            else
                fromString = string.Format("TABLE_DATE_RANGE([{0}], TIMESTAMP(\"{1:yyyy-MM-dd}\"), TIMESTAMP(\"{2:yyyy-MM-dd}\"))", this.tableBaseName, filter.FromUtc, filter.TillUtc);

            // WHERE
            // This could be made even more generic with reflection / attributes to link custom dimensions to the properties on Filter, but one step at a time.            
            List <string> whereElements = new List<string>();
            // http://stackoverflow.com/questions/36666083/bigquery-two-hitlevel-custom-dimensions            
            this.AddWhereElements(whereElements, CustomDimensionDefinition.CompanyId, filter.CompanyIds, filter.ExcludeCompanyIds);
            this.AddWhereElements(whereElements, CustomDimensionDefinition.SiteId, filter.SiteIds, filter.ExcludeSiteIds);
            this.AddWhereElements(whereElements, CustomDimensionDefinition.PageId, filter.PageIds, filter.ExcludePageIds);
            this.AddWhereElements(whereElements, CustomDimensionDefinition.ProductId, filter.ProductIds, filter.ExcludeProductIds);
            this.AddWhereElements(whereElements, CustomDimensionDefinition.EntertainmentId, filter.EntertainmentIds, filter.ExcludeEntertainmentIds);
            this.AddWhereElements(whereElements, CustomDimensionDefinition.CategoryId, filter.CategoryIds, filter.ExcludeCategoryIds);
            this.AddWhereElements(whereElements, CustomDimensionDefinition.TabId, filter.TabIds, filter.ExcludeTabIds);
            this.AddWhereElements(whereElements, CustomDimensionDefinition.DeliverypointId, filter.DeliverypointIds, filter.ExcludeDeliverypointIds);
            this.AddWhereElements(whereElements, CustomDimensionDefinition.DeliverypointGroupId, filter.DeliverypointgroupIds, filter.ExcludeDeliverypointgroupIds);
            this.AddWhereElements(whereElements, DimensionNames.ScreenName, filter.ScreenNames, filter.ExcludeScreenNames);
            this.AddWhereElements(whereElements, DimensionNames.EventCategory, filter.EventCategories, filter.ExcludeEventCategories);
            this.AddWhereElements(whereElements, DimensionNames.EventAction, filter.EventActions, filter.ExcludeEventActions);
            this.AddWhereElements(whereElements, DimensionNames.EventLabel, filter.EventLabels, filter.ExcludeEventLabels);

            if ((filter.PrimaryKeys.Count + filter.ExcludePrimaryKeys.Count) > 0)
                throw new NotImplementedException("PrimaryKeys is a legacy field and is and will not be supported for Bigquery");

            // Hit Type
            whereElements.Add(string.Format("hits.type = \"{0}\"", metricName.BigqueryHitType));

            string whereElementsString = string.Join("\nAND ", whereElements);

            string query = baseQuery;
            query = query.Replace("[[WHERE]]", whereElementsString);
            query = query.Replace("[[FROM]]", fromString);

            if (TestUtil.IsPcGabriel)
                Debug.WriteLine(query);

            return query;
        }

        private void AddWhereElements(List<string> whereElements, DimensionNames.DimensionName dimension, List<int> includeValues, List<string> excludeValues)
        {               
                             
            this.AddWhereElement(whereElements, dimension.BigqueryFieldName, includeValues.Select(x => x.ToString()), false, false);
            this.AddWhereElement(whereElements, dimension.BigqueryFieldName, excludeValues.Select(x => x.ToString()), false, true);
        }

        private void AddWhereElements(List<string> whereElements, DimensionNames.DimensionName dimension, List<string> includeValues, List<string> excludeValues)
        {
            // http://stackoverflow.com/a/12476612
            this.AddWhereElement(whereElements, dimension.BigqueryFieldName, includeValues, true, false);
            this.AddWhereElement(whereElements, dimension.BigqueryFieldName, excludeValues, true, true);
        }

        private void AddWhereElements(List<string> whereElements, CustomDimensionDefinition customDimension, List<int> includeValues, List<int> excludeValues)
        {
            this.AddWhereElement(whereElements, customDimension, includeValues.Select(x => x.ToString()), false, false);
            this.AddWhereElement(whereElements, customDimension, excludeValues.Select(x => x.ToString()), false, true);
        }

        private void AddWhereElements(List<string> whereElements, CustomDimensionDefinition customDimension, List<string> includeValues, List<string> excludeValues)
        {
            this.AddWhereElement(whereElements, customDimension, includeValues, true, false);
            this.AddWhereElement(whereElements, customDimension, excludeValues, true, true);            
        }

        private void AddWhereElement(List<string> whereElements, CustomDimensionDefinition customDimension, IEnumerable<string> values, bool useLikeOperator, bool exclude)
        {
            string dimensionDefinition = string.Format("hits.customDimensions.index = {0} AND hits.customDimensions.value", (int)customDimension);
            this.AddWhereElement(whereElements, dimensionDefinition, values, useLikeOperator, exclude);
        }

        private void AddWhereElement(List<string> whereElements, string dimensionDefinition, IEnumerable<string> values, bool useLikeOperator, bool exclude)
        {
            if (values.Count() <= 0)
                return;

            string whereElement = string.Empty;
            if (useLikeOperator || dimensionDefinition.Contains("customDimensions")) // Because custom dimensions are a combined field 'IN' doesn't seem to work.
            {
                if (exclude)
                    whereElement = "NOT (\n";
                else
                    whereElement = "(\n";

                bool first = true;
                foreach (var nonEscapedValue in values)
                {                    
                    string value = nonEscapedValue.Replace("\"", "\\\"");
                    if (!first)
                        whereElement += "OR \n";

                    if (useLikeOperator)
                        value = "%" + value + "%";

                    whereElement += string.Format("( {0} LIKE \"{1}\" )\n", dimensionDefinition, value);

                    first = false;
                }

                whereElement += ")\n";
            }
            else
            {
                // http://stackoverflow.com/a/12476612
                string valuesJoinedAndQuoted = "\"" + string.Join("\",\"", values.Select(x => x.Replace("\"", "\\\""))) + "\"";
                string excludeOperator = exclude ? "NOT " : string.Empty;
                whereElement = string.Format("{0} {1}IN ({2})", dimensionDefinition, excludeOperator, valuesJoinedAndQuoted);
            }

            whereElements.Add(whereElement);
        }

        private readonly string baseQuery =
@"
SELECT COUNT(*) FROM
(
  SELECT
    CONCAT(STRING(visitid), fullvisitorid, STRING(hits.hitNumber)) AS hitId,
  FROM
    [[FROM]]
  WHERE
    [[WHERE]]  
)
IGNORE CASE;
";

        //SELECT
        //  CONCAT(fullvisitorid, "-", STRING(visitid), "-", STRING(hits.hitNumber)) AS hitId,
        //  FIRST(IF(hits.customDimensions.index = 2, hits.customDimensions.value, NULL)) WITHIN RECORD cd2,
        //  FIRST(IF(hits.customDimensions.index = 3, hits.customDimensions.value, NULL)) WITHIN RECORD cd3,
        //  hits.customDimensions.index,
        //  hits.customDimensions.value
        //FROM
        //  [google.com:analytics - bigquery:LondonCycleHelmet.ga_sessions_20130910]
        //WHERE
        //  hits.customDimensions.index IN(2, 3)
        //  and fullVisitorId IN("5711522334224447562")

        #region Abstract class implementation

        protected override string[] GetRequiredScopes()
        {
            return new string[] {
                BigqueryService.Scope.Bigquery,
                BigqueryService.Scope.CloudPlatform,
                BigqueryService.Scope.CloudPlatformReadOnly
            };
        }

        protected override BigqueryService InitializeService(BaseClientService.Initializer initializer)
        {
            return new BigqueryService(initializer);
        }

        #endregion
    }
}
