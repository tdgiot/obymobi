﻿using Obymobi.Analytics.KpiFunctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public class GoogleAnalyticsKpiFunctionProcessor : KpiFunctionProcessorBase<GoogleAnalyticsKpiRetriever>
    {        
        public GoogleAnalyticsKpiFunctionProcessor(GoogleAnalyticsKpiRetriever googleAnalyticsKpiRetriever, IChildCategoryFetcher childCategoryFetcher, int? companyIdCheck = null) : base(googleAnalyticsKpiRetriever, childCategoryFetcher, companyIdCheck)
        {

        }

        protected override IAnalyticsFilter CreateBaseFilter()
        {
            return new Filter();
        }

        protected override bool ExecuteFunctionNonGeneric(string functionName, IAnalyticsFilter filter, out List<object[]> result)
        {
            result = null;
            var filterTyped = (Filter)filter;
            if (functionName.Equals("TopXEventCount"))
            {
                var topXProducts = this.kpiRetriever.GetTopXEventCount(filterTyped);
                result = this.topXItemsToGeneric(topXProducts);
            }
            else if (functionName.Equals("TopXScreenCount"))
            {
                var topXProducts = this.kpiRetriever.GetTopXScreenCount(filterTyped);
                result = this.topXItemsToGeneric(topXProducts);
            }
            else if (functionName.Equals("MessagingStatistics"))
            {
                var messageStats = this.kpiRetriever.GetMessagingStatistics(filterTyped);
                result = this.messagingStatisticsToGeneric(messageStats);
            }
            else
                return false;

            return true;
        }        

        private List<object[]> topXItemsToGeneric(List<GoogleAnalyticsKpiRetriever.TopXItem> events)
        {
            List<object[]> toReturn = new List<object[]>();            
            foreach (var e in events)
            {
                List<object> values = new List<object>();
                values.AddRange(e.Dimensions);
                values.Add(e.Count);
                toReturn.Add(values.ToArray());
            }

            return toReturn;
        }        
 
        private List<object[]> messagingStatisticsToGeneric(List<GoogleAnalyticsKpiRetriever.MessageStatistics> messageStats)
        {
            var ordered = messageStats.OrderBy(x => x.Date).ThenBy(x => x.Title);

            List<object[]> toReturn = new List<object[]>();

            foreach (var m in ordered)
            {
                List<object> values = new List<object>();
                values.Add(m.Date);//.ToString("yyyyMMdd"));
                values.Add(m.Title);
                values.Add(m.Shown);
                values.Add(m.ShownIdle);
                values.Add(m.ShownNotIdle);
                values.Add(m.Views);
                values.Add(m.Expirations);
                values.Add(m.Responses);
                values.Add(m.PostiveResponse);
                values.Add(m.NegativeResponse);
                values.Add(m.OkResponse);
                toReturn.Add(values.ToArray());
            }

            // Titles
            List<object> titles = new List<object>();
            titles.Add("Date");
            titles.Add("Title");
            titles.Add("Shown");
            titles.Add("Idle");
            titles.Add("Not Idle");
            titles.Add("Views");
            titles.Add("Expirations");
            titles.Add("Responses");
            titles.Add("Yes");
            titles.Add("No");
            titles.Add("Ok");
            toReturn.Insert(0, titles.ToArray());

            return toReturn;
        }
    }
}
