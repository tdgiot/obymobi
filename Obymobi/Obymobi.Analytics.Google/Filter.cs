﻿using Google.Apis.AnalyticsReporting.v4.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public class Filter : IAnalyticsFilter
    {
        public int NoOfResults { get; set; }
        public List<string> Dimensions { get; set; }
        
        public DateTime FromUtc { get; set; }
        public DateTime TillUtc { get; set; }

        public Filter()
        {
            CompanyIds = new List<int>();
            SiteIds = new List<int>();
            PageIds = new List<int>();
            ProductIds = new List<int>();
            EntertainmentIds = new List<int>();
            CategoryIds = new List<int>();
            TabIds = new List<int>();
            DeliverypointIds = new List<int>();
            DeliverypointgroupIds = new List<int>();
            ScreenNames = new List<string>();
            EventCategories = new List<string>();
            EventActions = new List<string>();
            EventLabels = new List<string>();
            PrimaryKeys = new List<string>();

            ExcludeCompanyIds = new List<int>();
            ExcludeSiteIds = new List<int>();
            ExcludePageIds = new List<int>();
            ExcludeProductIds = new List<int>();
            ExcludeEntertainmentIds = new List<int>();
            ExcludeCategoryIds = new List<int>();
            ExcludeTabIds = new List<int>();
            ExcludeDeliverypointIds = new List<int>();
            ExcludeDeliverypointgroupIds = new List<int>();
            ExcludeScreenNames = new List<string>();
            ExcludeEventCategories = new List<string>();
            ExcludeEventActions = new List<string>();
            ExcludeEventLabels = new List<string>();
            ExcludePrimaryKeys = new List<string>();

            Dimensions = new List<string>();

            NoOfResults = 10;
        }

        public List<int> CompanyIds { get; set; }
        public List<int> SiteIds { get; set; }
        public List<int> PageIds { get; set; }
        public List<int> ProductIds { get; set; }
        public List<int> EntertainmentIds { get; set; }
        public List<int> CategoryIds { get; set; }
        public List<int> TabIds { get; set; }
        public List<int> DeliverypointIds { get; set; }
        public List<int> DeliverypointgroupIds { get; set; }
        public List<string> ScreenNames { get; set; }
        public List<string> EventCategories { get; set; }
        public List<string> EventActions { get; set; }
        public List<string> EventLabels { get; set; }
        public List<string> PrimaryKeys { get; set; }

        public List<int> ExcludeCompanyIds { get; set; }
        public List<int> ExcludeSiteIds { get; set; }
        public List<int> ExcludePageIds { get; set; }
        public List<int> ExcludeProductIds { get; set; }
        public List<int> ExcludeEntertainmentIds { get; set; }
        public List<int> ExcludeCategoryIds { get; set; }
        public List<int> ExcludeTabIds { get; set; }
        public List<int> ExcludeDeliverypointIds { get; set; }
        public List<int> ExcludeDeliverypointgroupIds { get; set; }
        public List<string> ExcludeScreenNames { get; set; }
        public List<string> ExcludeEventCategories { get; set; }
        public List<string> ExcludeEventActions { get; set; }
        public List<string> ExcludeEventLabels { get; set; }
        public List<string> ExcludePrimaryKeys { get; set; }

        public DateRange GetDateRange()
        {
            DateRange range = new DateRange();
            range.StartDate = this.FromUtc.ToString("yyyy-MM-dd");
            range.EndDate = this.TillUtc.ToString("yyyy-MM-dd");
            return range;
        }

        public string GetFilterExpression()
        {
            StringBuilder sb = new StringBuilder();
            this.AppendPredicate(sb, this.CompanyIds, CustomDimensionDefinition.CompanyId, false);
            this.AppendPredicate(sb, this.SiteIds, CustomDimensionDefinition.SiteId, false);
            this.AppendPredicate(sb, this.PageIds, CustomDimensionDefinition.PageId, false);
            this.AppendPredicate(sb, this.ProductIds, CustomDimensionDefinition.ProductId, false);
            this.AppendPredicate(sb, this.EntertainmentIds, CustomDimensionDefinition.EntertainmentId, false);
            this.AppendPredicate(sb, this.CategoryIds, CustomDimensionDefinition.CategoryId, false);
            this.AppendPredicate(sb, this.TabIds, CustomDimensionDefinition.TabId, false);
            this.AppendPredicate(sb, this.DeliverypointIds, CustomDimensionDefinition.DeliverypointId, false);
            this.AppendPredicate(sb, this.DeliverypointgroupIds, CustomDimensionDefinition.DeliverypointGroupId, false);
            this.AppendPredicate(sb, this.ScreenNames, DimensionNames.ScreenName, false);
            this.AppendPredicate(sb, this.EventCategories, DimensionNames.EventCategory, false);
            this.AppendPredicate(sb, this.EventActions, DimensionNames.EventAction, false);
            this.AppendPredicate(sb, this.EventLabels, DimensionNames.EventLabel, false);
            this.AppendPredicate(sb, this.PrimaryKeys, CustomDimensionDefinition.PrimaryKeys, false);

            this.AppendPredicate(sb, this.ExcludeCompanyIds, CustomDimensionDefinition.CompanyId, true);
            this.AppendPredicate(sb, this.ExcludeSiteIds, CustomDimensionDefinition.SiteId, true);
            this.AppendPredicate(sb, this.ExcludePageIds, CustomDimensionDefinition.PageId, true);
            this.AppendPredicate(sb, this.ExcludeProductIds, CustomDimensionDefinition.ProductId, true);
            this.AppendPredicate(sb, this.ExcludeEntertainmentIds, CustomDimensionDefinition.EntertainmentId, true);
            this.AppendPredicate(sb, this.ExcludeCategoryIds, CustomDimensionDefinition.CategoryId, true);
            this.AppendPredicate(sb, this.ExcludeTabIds, CustomDimensionDefinition.TabId, true);
            this.AppendPredicate(sb, this.ExcludeDeliverypointIds, CustomDimensionDefinition.DeliverypointId, true);
            this.AppendPredicate(sb, this.ExcludeDeliverypointgroupIds, CustomDimensionDefinition.DeliverypointGroupId, true);
            this.AppendPredicate(sb, this.ExcludeScreenNames, DimensionNames.ScreenName, true);
            this.AppendPredicate(sb, this.ExcludeEventCategories, DimensionNames.EventCategory, true);
            this.AppendPredicate(sb, this.ExcludeEventActions, DimensionNames.EventAction, true);
            this.AppendPredicate(sb, this.ExcludeEventLabels, DimensionNames.EventLabel, true);
            this.AppendPredicate(sb, this.ExcludePrimaryKeys, CustomDimensionDefinition.PrimaryKeys, true);

            return sb.ToString();
        }

        public string PredicateExpression
        {
            get
            {
                return this.GetFilterExpression();
            }
        }

        public List<Dimension> GetDimensions()
        {
            List<Dimension> dimensions = new List<Dimension>();
            // This is where we add some black magic
            foreach (var name in this.Dimensions)
            {
                string parsedValue = name;
                CustomDimensionDefinition customDimension;
                DimensionNames.DimensionName dimensionName;

                if (EnumUtil.TryParse<CustomDimensionDefinition>(name, out customDimension)) // It's a defined Custom Dimension
                {
                    parsedValue = "ga:dimension" + (int)customDimension;
                }
                else if (DimensionNames.TryGetDimensionName(name, out dimensionName)) // It's a Dimension we support
                {
                    parsedValue = dimensionName;
                }
                else if (!parsedValue.StartsWith("ga:")) // It's a dimension that we don't support, just accept it.
                {
                    parsedValue = "ga:" + parsedValue;
                }

                dimensions.Add(new Dimension { Name = parsedValue });
            }
            return dimensions;
        }

        private void AppendPredicate<T>(StringBuilder sb, List<T> values, CustomDimensionDefinition dimension, bool negate)
        {
            this.AppendPredicate(sb, values, string.Format("ga:dimension{0}", (int)dimension), negate);
        }

        private void AppendPredicate<T>(StringBuilder sb, List<T> values, String dimension, bool negate)
        {
            // https://developers.google.com/analytics/devguides/reporting/core/v3/reference#filters
            for (int i = 0; i < values.Count; i++)
            {
                if (values.GetType() == typeof(List<string>))
                {
                    if (negate)
                        sb.AppendFormat("{0}!@{1}", dimension, values[i]);
                    else
                        sb.AppendFormat("{0}=@{1}", dimension, values[i]);
                }
                else
                {
                    if (negate)
                        sb.AppendFormat("{0}!={1}", dimension, values[i]);
                    else
                        sb.AppendFormat("{0}=={1}", dimension, values[i]);
                }


                if ((i + 1) < values.Count)
                {
                    sb.Append(",");
                }
            }

            if (values.Count > 0)
                sb.Append(";");
        }

        public Filter Clone()
        {
            return JsonConvert.DeserializeObject<Filter>(JsonConvert.SerializeObject(this));
        }
    }
}
