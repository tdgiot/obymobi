﻿using Obymobi.Analytics.KpiFunctions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public class BigqueryKpiFunctionProcessor : KpiFunctionProcessorBase<BigqueryKpiRetriever>
    {
        public BigqueryKpiFunctionProcessor(BigqueryKpiRetriever googleAnalyticsKpiRetriever, IChildCategoryFetcher childCategoryFetcher, int? companyIdCheck = null) : base(googleAnalyticsKpiRetriever, childCategoryFetcher, companyIdCheck)
        {

        }

        protected override IAnalyticsFilter CreateBaseFilter()
        {
            return new Filter();
        }

        protected override bool ExecuteFunctionNonGeneric(string functionName, IAnalyticsFilter filter, out List<object[]> result)
        {
            result = null;
            return false; // Everything is generic.
        }
    }
}
