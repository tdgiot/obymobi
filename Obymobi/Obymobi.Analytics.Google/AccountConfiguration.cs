﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public class AccountConfiguration
    {
        public readonly string AccountId;
        public readonly string PropertyId;
        public readonly string ViewId;

        public AccountConfiguration(string accountId, string propertyId, string viewId)
        {
            this.AccountId = accountId;
            this.PropertyId = propertyId;
            this.ViewId = viewId;
        }

        public static AccountConfiguration GetByCloudEnvironment(CloudEnvironment environment)
        {
            AccountConfiguration config;

            switch (environment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                case CloudEnvironment.Development:
                    config = AccountConfiguration.Live;
                    break;
                case CloudEnvironment.Test:
                    config = AccountConfiguration.Test;
                    break;
                case CloudEnvironment.Demo:
                    config = AccountConfiguration.Demo;
                    break;
                case CloudEnvironment.Manual:
                    if (TestUtil.IsPcGabriel)
                        config = AccountConfiguration.Test;
                    else
                        config = AccountConfiguration.Development;
                    break;
                default:
                    throw new NotImplementedException("Not implemented: " + environment);                    
            }

            return config;
        }

        public static readonly AccountConfiguration Development = new AccountConfiguration("45902422", "UA-45902422-1", "79299045");
        public static readonly AccountConfiguration Test = new AccountConfiguration("45902422", "UA-45902422-2", "79295496");
        public static readonly AccountConfiguration Demo = new AccountConfiguration("45902422", "UA-45902422-3", "79301501");
        public static readonly AccountConfiguration Live = new AccountConfiguration("45902422", "UA-45902422-4", "79296192");
    }
}
