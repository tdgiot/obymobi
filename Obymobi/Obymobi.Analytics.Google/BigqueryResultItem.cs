﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Google
{
    public class BigqueryResultItem<TResult>
    {
        public BigqueryResultItem(DateTime fromUtc, DateTime tillUtc, TResult result)
        {
            this.FromUtc = fromUtc;
            this.TillUtc = tillUtc;
            this.Result = result;
        }

        public DateTime FromUtc
        {
            get; private set;
        }

        public DateTime TillUtc
        {
            get; private set;
        }

        public TResult Result
        {
            get; private set;
        }
    }
}
