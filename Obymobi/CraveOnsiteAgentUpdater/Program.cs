﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CraveOnsiteAgentUpdater
{
    static class Program
    {
        static void Main(string[] args)
        {
            // Kill the agent
            if (KillProcess("CraveOnsiteAgent"))
            {
                try
                {
                    // Copy the files from the unpack dir
                    string unpackedDir = @"C:\Program Files\Crave\Crave Onsite Agent\Updates\CraveOnsiteAgent\unpacked";
                    if (Directory.Exists(unpackedDir))
                    {
                        foreach (string file in Directory.GetFiles(unpackedDir))
                        {
                            string fileName = Path.GetFileName(file);
                            if (!fileName.Equals("CraveOnsiteAgentUpdater.exe", StringComparison.InvariantCultureIgnoreCase))
                            {
                                string destination = string.Format("{0}{1}", @"C:\Program Files\Crave\Crave Onsite Agent\", fileName);
                                File.Copy(file, destination, true);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteToLog(ex.Message);
                }

                try
                {
                    // Check whether a post-update batch file exists
                    string postUpdateBatchFile = @"C:\Program Files\Crave\Crave Onsite Agent\postupdate.bat";
                    if (File.Exists(postUpdateBatchFile))
                    {
                        // Start the batch file
                        Process.Start(postUpdateBatchFile);

                        // Wait for 5 seconds
                        Thread.Sleep(5000);
                    }
                    else
                        WriteToLog("Post update file does not exist!");
                }
                catch (Exception ex)
                {
                    WriteToLog(ex.Message);
                }

                try
                {
                    // Start the agent
                    if (!StartService("CraveOnsiteAgent", 6000))
                        WriteToLog("Service could not be started!");
                }
                catch (Exception ex)
                {
                    WriteToLog(ex.Message);
                }
            }
            else
                WriteToLog("Process could not be killed!");
        }

        private static bool KillProcess(string processName)
        {
            bool success = true;

            try
            {
                // Kill the process
                foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcesses())
                {
                    if (process.ProcessName.Equals(processName, StringComparison.OrdinalIgnoreCase))
                    {
                        process.Kill();
                        break;
                    }
                }
            }
            catch
            {
                success = false;
            }

            Thread.Sleep(2000);

            // Check whether the process is killed
            foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcesses())
            {
                if (process.ProcessName.Equals(processName, StringComparison.OrdinalIgnoreCase))
                {
                    success = false;
                    break;
                }
            }

            return success;
        }

        private static bool StartService(string serviceName, int timeoutMilliseconds)
        {
            bool success = false;

            ServiceController serviceController = GetServiceController(serviceName);
            if (serviceController != null)
            {
                try
                {
                    TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

                    serviceController.Start();
                    serviceController.WaitForStatus(ServiceControllerStatus.Running, timeout);

                    success = true;
                }
                catch
                {
                    if (serviceController.Status == ServiceControllerStatus.Running || serviceController.Status == ServiceControllerStatus.StartPending)
                        success = true;
                }
            }

            return success;
        }

        private static ServiceController GetServiceController(string serviceName)
        {
            ServiceController serviceController = null;

            ServiceController[] services = ServiceController.GetServices();
            for (int i = 0; i < services.Length; i++)
            {
                ServiceController service = services[i];
                if (service.ServiceName.Equals(serviceName))
                {
                    serviceController = service;
                    break;
                }
            }

            if (serviceController == null)
                WriteToLog("Service could not be found!");

            return serviceController;
        }

        private static void WriteToLog(string contents)
        {
            DateTime now = DateTime.Now;
            File.AppendAllText(@"C:\Program Files\Crave\Crave Onsite Agent\CraveOnsiteAgentUpdater.log", string.Format("{0}{1}{2} {3}\r\n", now.Year.ToString("0000"), now.Month.ToString("00"), now.Day.ToString("00"), contents));
        }
    }
}
