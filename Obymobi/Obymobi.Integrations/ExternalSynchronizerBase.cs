﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Integrations
{
    public abstract class ExternalSynchronizerBase
    {
        protected readonly ExternalSystemEntity externalSystemEntity;

        public ExternalSynchronizerBase(ExternalSystemEntity externalSystemEntity)
        {
            this.externalSystemEntity = externalSystemEntity;
        }
    }
}
