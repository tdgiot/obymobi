﻿using System;

namespace Obymobi.Integrations.Exceptions
{
    public class IntegrationsException : Exception
    {
        public IntegrationsException()
        {
        }

        public IntegrationsException(string message)
            : base(message)
        {
        }

        public IntegrationsException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
