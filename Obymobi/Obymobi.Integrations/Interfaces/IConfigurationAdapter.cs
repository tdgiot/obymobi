﻿using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Integrations.Interfaces
{
    /// <summary>
    /// IConfigurationAdapter class
    /// </summary>
    public interface IConfigurationAdapter
    {
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        void ReadFromTerminalModel(Terminal terminal);

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        void ReadFromTerminalEntity(TerminalEntity terminal);

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        void ReadFromConfigurationManager();

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        void WriteToConfigurationManager();

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        void WriteToTerminalEntity(TerminalEntity terminal);

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        void WriteToTerminalModel(Terminal terminal);
    }
}
