﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Integrations.Interfaces
{
    public interface IExternalProductSynchronizer
    {
        IEnumerable<ExternalProduct> GetExternalProducts();

        ExternalProductSynchronizer ExternalProductSynchronizer { get; }
    }
}
