﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Integrations
{
    public class ExternalDeliverypointSynchronizer : ExternalSynchronizerBase
    {
        public ExternalDeliverypointSynchronizer(ExternalSystemEntity externalSystemEntity) : base(externalSystemEntity)
        {
        }

        public bool Synchronize(IEnumerable<ExternalDeliverypoint> externalDeliverypoints)
        {
            ExternalDeliverypointCollection externalDeliverypointCollection = externalSystemEntity.ExternalDeliverypointCollection;

            externalDeliverypoints = externalDeliverypoints.GroupBy(externalDeliverypoint => externalDeliverypoint.Id)
                                                           .Select(groupedExternalDeliverypoints => groupedExternalDeliverypoints.First())
                                                           .ToList();

            foreach (ExternalDeliverypoint externalDeliverypoint in externalDeliverypoints)
            {
                if (externalDeliverypoint.Id.IsNullOrWhiteSpace())
                    continue;

                ExternalDeliverypointEntity externalDeliverypointEntity = externalDeliverypointCollection.FirstOrDefault(x => x.Id.Equals(externalDeliverypoint.Id, StringComparison.OrdinalIgnoreCase));
                if (externalDeliverypointEntity == null)
                {
                    externalDeliverypointEntity = new ExternalDeliverypointEntity();
                    externalDeliverypointEntity.ExternalSystemId = this.externalSystemEntity.ExternalSystemId;
                    externalDeliverypointEntity.ParentCompanyId = this.externalSystemEntity.CompanyId;
                    externalDeliverypointEntity.Id = externalDeliverypoint.Id;
                }

                externalDeliverypointEntity.Name = externalDeliverypoint.Name;
                externalDeliverypointEntity.Visible = externalDeliverypoint.Visible;
                externalDeliverypointEntity.StringValue1 = externalDeliverypoint.StringValue1;
                externalDeliverypointEntity.StringValue2 = externalDeliverypoint.StringValue2;
                externalDeliverypointEntity.StringValue3 = externalDeliverypoint.StringValue3;
                externalDeliverypointEntity.StringValue4 = externalDeliverypoint.StringValue4;
                externalDeliverypointEntity.StringValue5 = externalDeliverypoint.StringValue5;
                externalDeliverypointEntity.IntValue1 = externalDeliverypoint.IntValue1;
                externalDeliverypointEntity.IntValue2 = externalDeliverypoint.IntValue2;
                externalDeliverypointEntity.IntValue3 = externalDeliverypoint.IntValue3;
                externalDeliverypointEntity.IntValue4 = externalDeliverypoint.IntValue4;
                externalDeliverypointEntity.IntValue5 = externalDeliverypoint.IntValue5;
                externalDeliverypointEntity.BoolValue1 = externalDeliverypoint.BoolValue1;
                externalDeliverypointEntity.BoolValue2 = externalDeliverypoint.BoolValue2;
                externalDeliverypointEntity.BoolValue3 = externalDeliverypoint.BoolValue3;
                externalDeliverypointEntity.BoolValue4 = externalDeliverypoint.BoolValue4;
                externalDeliverypointEntity.BoolValue5 = externalDeliverypoint.BoolValue5;

                externalDeliverypointEntity.Save();
            }

            foreach (ExternalDeliverypointEntity externalDeliverypointEntity in externalDeliverypointCollection)
            {
                ExternalDeliverypoint externalDeliverypoint = externalDeliverypoints.FirstOrDefault(x => x.Id.Equals(externalDeliverypointEntity.Id, StringComparison.OrdinalIgnoreCase));
                if (externalDeliverypoint == null)
                {
                    externalDeliverypointEntity.Delete();
                }                
            }

            return true;
        }
    }
}
