﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Integrations
{
    public class ExternalProductSynchronizer : ExternalSynchronizerBase
    {
        public ExternalProductSynchronizer(ExternalSystemEntity externalSystemEntity) : base(externalSystemEntity)
        {
        }

        public bool Synchronize(IEnumerable<ExternalProduct> externalProducts)
        {
            ExternalProductCollection externalProductCollection = this.externalSystemEntity.ExternalProductCollection;

            foreach (ExternalProduct externalProduct in externalProducts)
            {
                if (externalProduct.Id.IsNullOrWhiteSpace())
                    continue;

                ExternalProductEntity externalProductEntity = externalProductCollection.FirstOrDefault(x => x.Id.Equals(externalProduct.Id, StringComparison.OrdinalIgnoreCase));
                if (externalProductEntity == null)
                {
                    externalProductEntity = new ExternalProductEntity();
                    externalProductEntity.ExternalSystemId = this.externalSystemEntity.ExternalSystemId;
                    externalProductEntity.ParentCompanyId = this.externalSystemEntity.CompanyId;
                    externalProductEntity.Id = externalProduct.Id;
                }

                externalProductEntity.Name = externalProduct.Name;
                externalProductEntity.Price = externalProduct.Price;
                externalProductEntity.Visible = externalProduct.Visible;
                externalProductEntity.StringValue1 = externalProduct.StringValue1;
                externalProductEntity.StringValue2 = externalProduct.StringValue2;
                externalProductEntity.StringValue3 = externalProduct.StringValue3;
                externalProductEntity.StringValue4 = externalProduct.StringValue4;
                externalProductEntity.StringValue5 = externalProduct.StringValue5;
                externalProductEntity.IntValue1 = externalProduct.IntValue1;
                externalProductEntity.IntValue2 = externalProduct.IntValue2;
                externalProductEntity.IntValue3 = externalProduct.IntValue3;
                externalProductEntity.IntValue4 = externalProduct.IntValue4;
                externalProductEntity.IntValue5 = externalProduct.IntValue5;
                externalProductEntity.BoolValue1 = externalProduct.BoolValue1;
                externalProductEntity.BoolValue2 = externalProduct.BoolValue2;
                externalProductEntity.BoolValue3 = externalProduct.BoolValue3;
                externalProductEntity.BoolValue4 = externalProduct.BoolValue4;
                externalProductEntity.BoolValue5 = externalProduct.BoolValue5;

                externalProductEntity.Save();
            }

            foreach (ExternalProductEntity externalProductEntity in externalProductCollection)
            {
                ExternalProduct externalProduct = externalProducts.FirstOrDefault(x => x.Id.Equals(externalProductEntity.Id, StringComparison.OrdinalIgnoreCase));
                if (externalProduct == null)
                {
                    externalProductEntity.Delete();
                }                
            }

            return true;
        }
    }
}
