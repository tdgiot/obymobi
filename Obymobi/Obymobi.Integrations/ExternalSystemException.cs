﻿using System;
using Obymobi.Enums;

namespace Obymobi.Integrations
{
    public class ExternalSystemException : ObymobiException
    {
        #region Constructors

        public ExternalSystemException(OrderProcessingError errorType, string message)
            : base(errorType, message)
        {
            this.errorType = errorType;
        }

        public ExternalSystemException(OrderProcessingError errorType, string message, params object[] args)
            : base(errorType, message, args)
        {
            this.errorType = errorType;
        }

        public ExternalSystemException(OrderProcessingError errorType, Exception innerException, string message, params object[] args)
            : base(errorType, innerException, message, args)
        {
            this.errorType = errorType;
        }

        private OrderProcessingError errorType = OrderProcessingError.PosErrorUnspecifiedFatalPosProblem;

        public OrderProcessingError ErrorType
        {
            get
            {
                return this.errorType;
            }
            set
            {
                this.errorType = value;
            }
        }

        #endregion
    }
}
