﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Integrations.DataSources
{
    public class ExternalSystemDataSource
    {
        public ExternalSystemEntity GetExternalSystemEntityByType(ExternalSystemType type, int companyId)
        {
            ExternalSystemEntity externalSystemEntity = null;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ExternalSystemFields.Type == type);
            filter.Add(ExternalSystemFields.CompanyId == companyId);

            ExternalSystemCollection externalSystemCollection = new ExternalSystemCollection();
            externalSystemCollection.GetMulti(filter);

            if (externalSystemCollection.Count > 0)
                externalSystemEntity = externalSystemCollection[0];

            return externalSystemEntity;
        }
    }
}
