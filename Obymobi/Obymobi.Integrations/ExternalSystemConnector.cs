﻿using System;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Integrations.DataSources;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Integrations
{
    public abstract class ExternalSystemConnector
    {
        protected readonly ExternalSystemEntity externalSystemEntity;
        protected readonly bool TEST_MODE = false;

        private readonly ExternalSystemDataSource externalSystemDataSource = new ExternalSystemDataSource();

        public ExternalSystemConnector(int companyId, ExternalSystemType externalSystemType)
        {
#if !DEBUG
            TEST_MODE = false;
#endif

            if (!TEST_MODE)
            {
                this.externalSystemEntity = this.externalSystemDataSource.GetExternalSystemEntityByType(externalSystemType, companyId);
                if (this.externalSystemEntity == null)
                    throw new ApplicationException(string.Format("No external system has been set up for {0} yet!", externalSystemType));
            }
        }

        public abstract bool HasValidConfiguration();

        protected ExternalDeliverypointEntity GetExternalDeliverypointEntityForOrder(OrderEntity orderEntity)
        {
            if (!orderEntity.DeliverypointId.HasValue)
                throw new ApplicationException(string.Format("No delivery point linked to order '{0}'!", orderEntity.OrderId));

            return this.GetExternalDeliverypointEntity(orderEntity.DeliverypointId.Value);
        }

        protected ExternalProductEntity GetExternalProductEntityForOrder(OrderEntity orderEntity)
        {
            if (orderEntity.OrderitemCollection == null || orderEntity.OrderitemCollection.Count == 0)
                throw new ApplicationException(string.Format("No order items found for order '{0}'!", orderEntity.OrderId));

            if (!orderEntity.OrderitemCollection[0].ProductId.HasValue)
                throw new ApplicationException(string.Format("No product linked to order item '{0}'!", orderEntity.OrderitemCollection[0].OrderitemId));

            return this.GetExternalProductEntity(orderEntity.OrderitemCollection[0].ProductId.Value);
        }

        public ExternalDeliverypointEntity GetExternalDeliverypointEntity(int deliverypointId)
        {
            ExternalDeliverypointEntity externalDeliverypointEntity = null;

            if (deliverypointId <= 0)
            {
                throw new ApplicationException(string.Format("Invalid DeliverypointId '{0}'!", deliverypointId));
            }
            else
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ExternalDeliverypointFields.ExternalSystemId == this.externalSystemEntity.ExternalSystemId);
                filter.Add(DeliverypointExternalDeliverypointFields.DeliverypointId == deliverypointId);

                RelationCollection relations = new RelationCollection();
                relations.Add(DeliverypointExternalDeliverypointEntity.Relations.ExternalDeliverypointEntityUsingExternalDeliverypointId);

                ExternalDeliverypointCollection externalDeliverypointCollection = new ExternalDeliverypointCollection();
                externalDeliverypointCollection.GetMulti(filter, relations);

                if (externalDeliverypointCollection.Count == 0)
                {
                    throw new ExternalSystemException(OrderProcessingError.NoExternalDeliverypointConnectedToDeliverypoint, "No external deliverypoint of type '{0}' linked to deliverypoint '{0}'!", this.externalSystemEntity.Type, deliverypointId);
                }
                else if (externalDeliverypointCollection.Count > 1)
                {
                    throw new ExternalSystemException(OrderProcessingError.MultipleExternalProductsConnectedToProduct, "Multiple external deliverypoints of type '{0}' linked to deliverypoint '{0}'!", this.externalSystemEntity.Type, deliverypointId);
                }
                else
                {
                    externalDeliverypointEntity = externalDeliverypointCollection[0];

                    if (externalDeliverypointEntity.Id.IsNullOrWhiteSpace())
                        throw new ExternalSystemException(OrderProcessingError.IdIsEmptyForExternalDeliverypoint, "ID is empty for external deliverypoint '{0}'!", externalDeliverypointEntity.ExternalDeliverypointId);
                }
            }

            return externalDeliverypointEntity;
        }

        public ExternalProductEntity GetExternalProductEntity(int productId)
        {
            ExternalProductEntity externalProductEntity = null;

            if (productId <= 0)
            {
                throw new ApplicationException(string.Format("Invalid ProductId '{0}'!", productId));
            }
            else
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ExternalProductFields.ExternalSystemId == this.externalSystemEntity.ExternalSystemId);
                filter.Add(ProductFields.ProductId == productId);

                RelationCollection relations = new RelationCollection();
                relations.Add(ExternalProductEntity.Relations.ProductEntityUsingExternalProductId);

                ExternalProductCollection externalProductCollection = new ExternalProductCollection();
                externalProductCollection.GetMulti(filter, relations);

                if (externalProductCollection.Count == 0)
                {
                    throw new ExternalSystemException(OrderProcessingError.NoExternalProductConnectedToProduct, "No external product of type '{0}' linked to product '{0}'!", this.externalSystemEntity.Type, productId);
                }
                else if (externalProductCollection.Count > 1)
                {
                    throw new ExternalSystemException(OrderProcessingError.MultipleExternalProductsConnectedToProduct, "Multiple external products of type '{0}' linked to product '{0}'!", this.externalSystemEntity.Type, productId);
                }
                else
                {
                    externalProductEntity = externalProductCollection[0];

                    if (externalProductEntity.Id.IsNullOrWhiteSpace())
                        throw new ExternalSystemException(OrderProcessingError.IdIsEmptyForExternalProduct, "ID is empty for external product '{0}'!", externalProductEntity.ExternalProductId);
                }
            }

            return externalProductEntity;
        }
    }
}
