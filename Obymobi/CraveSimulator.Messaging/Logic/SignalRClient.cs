﻿using System;
using Dionysos;
using Microsoft.AspNet.SignalR.Client;
using Newtonsoft.Json;
using Obymobi.Enums;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;
using Obymobi.Security;
using ConnectionState = Microsoft.AspNet.SignalR.Client.ConnectionState;

namespace CraveSimulator.Messaging.Logic
{
    public class SignalRClient : BaseClient
    {
        #region  Fields

        private readonly object clientLock = new object();

        private HubConnection hubConnection;
        private IHubProxy hubProxy;

        #endregion

        #region Constructors

        public SignalRClient(string baseUrl, string macAddress, string salt, int clientId, int deliverypointgroupId, int companyId)
        {
            this.BaseUrl = baseUrl;
            this.MacAddress = macAddress;
            this.Salt = salt;
            this.ClientId = clientId;
            this.DeliverypointgroupId = deliverypointgroupId;
            this.CompanyId = companyId;
        }

        #endregion

        public override void Connect()
        {
            Disconnect();

            this.hubConnection = new HubConnection(this.BaseUrl);

            InitializeClient();

            OnEventLog("Connecting to: {0}", this.BaseUrl);
            this.hubConnection.Start();
        }

        public override void Disconnect(bool exit = false)
        {
            StopPingPongChecker();

            lock(this.clientLock)
            {
                this.IsAuthenticated = false;

                if (this.hubConnection != null)
                {
                    OnEventLog("Disconnecting current connection");

                    this.hubConnection.StateChanged -= hubConnection_StateChanged;
                    this.hubConnection.Error -= hubConnection_Error;

                    if (this.hubConnection.State != ConnectionState.Disconnected)
                        this.hubConnection.Stop();

                    this.hubConnection = null;

                    OnEventLog("Disconnected");
                }
            }

            if (exit)
            {
                // Dirty but meh
                Environment.Exit(-1);
            }
        }

        public override void TransmitMessage(Netmessage message)
        {
            if (this.State == ConnectionState.Connected)
            {
                // Create JSON
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                string netmessage = JsonConvert.SerializeObject(message, new Formatting(), settings);

                OnEventLog("Sending message to server: {0}", message.MessageType);

                lock (this.clientLock)
                {
                    InvokeMessage(netmessage);
                }
            }
        }

        private void InitializeClient()
        {
            lock (this.clientLock)
            {
                OnEventLog("Initializing client");

                this.hubConnection.Headers.Add("deviceIdentifier", this.MacAddress);
                this.hubConnection.StateChanged += hubConnection_StateChanged;
                this.hubConnection.Error += hubConnection_Error;
                //hubConnection.TraceLevel = TraceLevels.All;
                //hubConnection.TraceWriter = Console.Out;

                this.hubProxy = this.hubConnection.CreateHubProxy("SignalRInterface");
                this.hubProxy.On<string>("ReceiveMessage", ReceiveMessage);
                this.hubProxy.On<string>("MessageVerified", (s => { /* ignored */ }));
            }
        }

        private void Authenticate()
        {
            long timestamp = DateTime.UtcNow.ToUnixTime();
            string hash = Hasher.GetHashFromParameters(this.Salt, timestamp, this.MacAddress, this.CometIdentifier);

            OnEventLog("Authenticate - Comet Identifier: {0}", this.CometIdentifier);

            lock(this.clientLock)
            {
                InvokeAuthenticate(timestamp, this.MacAddress, hash);
            }
        }

        private void hubConnection_Error(Exception ex)
        {
            OnEventLog(ex.Message);
        }

        private void hubConnection_StateChanged(StateChange state)
        {
            OnEventLog("Stage changed: {0} -> {1}", state.OldState, state.NewState);

            this.State = state.NewState;

            if (state.NewState == ConnectionState.Connected)
            {
                if (this.CometIdentifier != this.hubConnection.ConnectionId)
                {
                    this.CometIdentifier = this.hubConnection.ConnectionId;

                    Authenticate();
                }
            }
            else if (state.NewState == ConnectionState.Disconnected)
            {
                Connect();
            }
        }

        private void ReceiveMessage(string data)
        {
            try
            {
                Netmessage netmessage = JsonConvert.DeserializeObject<Netmessage>(data);
                HandleNetmessage(netmessage);
            }
            catch (Exception ex)
            {
                OnEventLog("ReceiveMessage - Failed to deserialize message. Message: {0}", data);
                OnEventLog("ReceiveMessage - " + ex.Message);
            }
        }

        private void HandleNetmessage(Netmessage netmessage)
        {
            OnEventLog("Received message: {0}", netmessage.MessageType);

            if (netmessage.MessageType == NetmessageType.AuthenticateResult)
            {
                NetmessageAuthenticateResult message = netmessage.ConvertTo<NetmessageAuthenticateResult>();
                this.IsAuthenticated = message.IsAuthenticated;
                if (message.IsAuthenticated)
                {
                    SetClientType();
                    OnEventLog("Authenticated, ready to send messages now!");

                    StartPingPongChecker();
                }
                else
                {
                    OnEventLog("Failed to authenticate. Error: {0}", message.AuthenticateErrorMessage);
                    Disconnect(true);
                }
            }
            else if (netmessage.MessageType == NetmessageType.Ping)
            {
                Pong();
            }
            else if (netmessage.MessageType == NetmessageType.Disconnect)
            {
                Disconnect(!this.IsAuthenticated);
            }
        }

        private void SetClientType()
        {
            NetmessageSetClientType message = new NetmessageSetClientType
                                              {
                                                  ClientId = this.ClientId,
                                                  ClientType = NetmessageClientType.Emenu,
                                                  CompanyId = this.CompanyId,
                                                  DeliverypointgroupId = this.DeliverypointgroupId
                                              };
            TransmitMessage(message);
        }

        private async void InvokeAuthenticate(long timestamp, string identifier, string hash)
        {
            try
            {
                if (this.hubProxy != null)
                {
                    await this.hubProxy.Invoke("Authenticate", timestamp, identifier, hash);
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private async void InvokeMessage(string data)
        {
            try
            {
                if (this.hubProxy != null)
                {
                    await this.hubProxy.Invoke<string>("ReceiveMessage", data);
                }
            }
            catch (Exception ex)
            {
                OnEventLog("TransmitMessage - Exception: " + ex.Message);
            }
        }
    }
}