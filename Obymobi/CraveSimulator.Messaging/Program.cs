﻿using System;
using System.Net;
using System.Threading.Tasks;
using CraveSimulator.Messaging.Logic;

namespace CraveSimulator.Messaging
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 6)
            {
                if (TestUtil.IsPcBattleStationDanny)
                {
                    args = new[]
                           {
                               "http://localhost/trunk/messaging/signalr/",
                               "e0:63:e5:ea:dd:53",
                               "vrhafmprvfhagydktrihokexkktosvpyxyhpviitjctgkvhg",
                               "3665",
                               "231",
                               "199"
                           };
                }
                else
                {
                    Console.WriteLine(@"Missing parameters.. Check source for what's needed");
                    return;
                }
            }

            string baseUrl = args[0];
            string macAddress = args[1];
            string salt = args[2];
            int clientId = int.Parse(args[3]);
            int deliverypointgroupId = int.Parse(args[4]);
            int companyId = int.Parse(args[5]);

            ServicePointManager.DefaultConnectionLimit = 255;

            SignalRClient client = new SignalRClient(baseUrl, macAddress, salt, clientId, deliverypointgroupId, companyId);

            Task.Factory.StartNew(() => client.Connect());

            Console.Read();
            Environment.Exit(0);
        }
    }
}
