﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Authentication;
using Dionysos;
using Newtonsoft.Json;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Integrations.Interfaces;

namespace Obymobi.Integrations.Service.Quore
{
    public class QuoreConnector : ExternalSystemConnector, IExternalProductSynchronizer, IExternalDeliverypointSynchronizer
    {
        #region Fields

        private readonly string apiUrl = string.Empty;
        private readonly string apiToken = string.Empty;
        private readonly ExternalProductSynchronizer externalProductSynchronizer;
        private readonly ExternalDeliverypointSynchronizer externalDeliverypointSynchronizer;

        #endregion

        #region Constructors

        public QuoreConnector(int companyId) : base(companyId, ExternalSystemType.Quore)
        {
            this.apiUrl = this.externalSystemEntity.StringValue1;
            this.apiToken = this.externalSystemEntity.StringValue2;
            this.externalProductSynchronizer = new ExternalProductSynchronizer(this.externalSystemEntity);
            this.externalDeliverypointSynchronizer = new QuoreDeliverypointSynchronizer(this.externalSystemEntity);
        }

        #endregion

        #region Properties

        public ExternalProductSynchronizer ExternalProductSynchronizer
        {
            get 
            { 
                return this.externalProductSynchronizer; 
            }
        }

        public ExternalDeliverypointSynchronizer ExternalDeliverypointSynchronizer
        {
            get
            {
                return this.externalDeliverypointSynchronizer;
            }
        }

        public override bool HasValidConfiguration()
        {
            return (
                        !this.externalSystemEntity.StringValue1.IsNullOrWhiteSpace() &&
                        !this.externalSystemEntity.StringValue2.IsNullOrWhiteSpace()
                   );
        }

        #endregion

        #region Methods

        public IEnumerable<ExternalProduct> GetExternalProducts()
        {
            List<ExternalProduct> externalProducts = new List<ExternalProduct>();

            string url = this.GetUrl("action=getHKItems");

            string json = this.CallApi(url);
            if (!json.IsNullOrWhiteSpace())
            {
                IEnumerable<HKItem> items = JsonConvert.DeserializeObject<IEnumerable<HKItem>>(json);
                foreach (HKItem item in items)
                {
                    ExternalProduct externalProduct = new ExternalProduct
                    {
                        Id = item.id,
                        Name = item.name,
                        Price = 0
                    };
                    externalProducts.Add(externalProduct);
                }
            }

            return externalProducts;
        }

        public IEnumerable<ExternalDeliverypoint> GetExternalDeliverypoints()
        {
            List<ExternalDeliverypoint> externalDeliverypoints = new List<ExternalDeliverypoint>();

            string url = this.GetUrl("action=getAreas");

            string json = this.CallApi(url);
            if (!json.IsNullOrWhiteSpace())
            {
                IEnumerable<Area> items = JsonConvert.DeserializeObject<IEnumerable<Area>>(json);
                foreach (Area item in items)
                {
                    ExternalDeliverypoint externalDeliverypoint = new ExternalDeliverypoint
                    {
                        Id = item.id,
                        Name = item.name,
                        StringValue1 = item.alt_name,
                        StringValue2 = item.qac_id,
                        StringValue3 = item.qac_name
                    };
                    externalDeliverypoints.Add(externalDeliverypoint);
                }
            }

            return externalDeliverypoints;
        }

        public bool CreateServiceOrder(OrderEntity orderEntity)
        {
            bool success = false;

            ExternalDeliverypointEntity externalDeliverypointEntity = this.GetExternalDeliverypointEntityForOrder(orderEntity);
            ExternalProductEntity externalProductEntity = this.GetExternalProductEntityForOrder(orderEntity);

            string url = this.GetUrl("action=addServiceRequestByAreaName");
            url += string.Format("&area_name={0}", externalDeliverypointEntity.Name);
            url += string.Format("&item_id={0}", externalProductEntity.Id);
            url += string.Format("&where={0}", HKWhere.BringIntoTheRoom.ToIntString());
            url += string.Format("&when={0}", HKWhen.AsSoonAsPossible.ToIntString());

            string json = this.CallApi(url);

            if (json.IsNullOrWhiteSpace())
                throw new ExternalSystemException(OrderProcessingError.QuoreInvalidApiResult, "The result of the Quore API call was empty!");
            else if (json.Contains("request-already-exists"))
                throw new ExternalSystemException(OrderProcessingError.QuoreRequestAlreadyExists, "The request already existed in Quore! Its not possible to do the same request twice!");
            else if (!json.Contains("ok"))
                throw new ExternalSystemException(OrderProcessingError.QuoreInvalidApiResult, "The result of the Quore API call was not OK! Json: {0}", json);
            else
                success = true;                

            return success;
        }

        private string GetUrl(string append)
        {
            return string.Format("{0}?{1}", this.apiUrl, append);
        }

        private string CallApi(string url)
        {
            string result = string.Empty;

            using (WebClient webClient = new WebClient())
            {
                webClient.Proxy = null;
                webClient.UseDefaultCredentials = false;
                webClient.Headers.Add("Authorization", string.Format("OAuth oauth_token={0}", this.apiToken));
                webClient.Headers.Add("Version", "2");
                result = webClient.DownloadString(url);
            }

            if (result.Contains("authenticate-invalid-token"))
            {
                throw new AuthenticationException(result);
            }

            return result;
        }

        #endregion
    }
}
