﻿namespace Obymobi.Integrations.Service.Quore
{
    /// <summary>
    /// Enumerator which represents the location indication for a housekeeping request.
    /// </summary>
    public enum HKWhere
    {
        BringIntoTheRoom = 1,
        LeaveOutsideRoom = 2,
        PickupAtFrontDesk = 3
    }
}
