﻿namespace Obymobi.Integrations.Service.Quore
{
    /// <summary>
    /// Enumerator which represents the time indication for a house keeping request from the guest.
    /// </summary>
    public enum HKWhen
    {
        AsSoonAsPossible = 1,
        WithinTheHour = 2,
        ThisMorning = 3,
        ThisAfternoon = 4,
        ThisEvening = 5,
        Tonight = 6,
        Tomorrow = 7,

        //WithinTheHour = 15, // Double
        AfterAnHour = 16,
        LaterToday = 17,
        // Tomorrow = 18, // Double

        Morning = 26,
        Afternoon = 27,
        Evening = 28,
        SpecificTime = 29,
        OnePm = 30,
        TwoPm = 31,
        ThreePm = 32,
        FourPm = 33
    }
}
