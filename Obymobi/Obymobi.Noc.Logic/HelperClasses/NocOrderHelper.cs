﻿using System;
using System.Linq;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Routing;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Noc.Logic.HelperClasses
{
    /// <summary>
    /// NocOrderHelper class
    /// </summary>
	public class NocOrderHelper
	{
		/// <summary>
		/// Get all orders which status is NonRouted
		/// </summary>
		/// <param name="companyId"></param>
		/// <returns></returns>
		public static OrderCollection GetNonRoutedOrders(List<int> companyIds)
		{
			var orders = new OrderCollection();

			var filter = new PredicateExpression();
			filter.Add(CompanyFields.UseMonitoring == true);
			if (companyIds.Count > 0)
				filter.Add(CompanyFields.CompanyId == companyIds);

            filter.Add(OrderFields.Status == (int)OrderStatus.NotRouted);

            var relations = new RelationCollection();
            relations.Add(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);

			orders.GetMulti(filter, 0, null, relations, GetPrefetchPath());

			return orders;
		}

		/// <summary>
		/// Get all orders which have a route setup but are not started after 45 seconds
		/// </summary>
		/// <param name="companyId"></param>
		/// <returns></returns>
        public static OrderCollection GetNotStartedRoutedOrders(List<int> companyIds)
		{
			var filter = new PredicateExpression();
			filter.Add(CompanyFields.UseMonitoring == true);

			if (companyIds.Count > 0)
				filter.Add(CompanyFields.CompanyId == companyIds);

            filter.Add(OrderFields.Status == (int)OrderStatus.Routed);
			filter.Add(OrderFields.CreatedUTC <= DateTime.UtcNow.AddSeconds(-45));

            var orderRoutestephandlers = new OrderRoutestephandlerCollection();
            orderRoutestephandlers.GetMulti(filter, 0, null, GetOrderRoutestephandlerJoins(), GetOrderRoutestephandlerPrefetchPath());

            var orders = GetUniqueOrdersFromOrderRoutestephandlerCollection(orderRoutestephandlers);

			return orders;
		}

        /// <summary>
        /// Get all expired orders or all expired orders for a company
        /// </summary>
        /// <param name="companyIds">List of company Ids</param>
        /// <returns></returns>
        public static OrderCollection GetExpiredOrders(List<int> companyIds)
		{
            // REFACTOR: How to get expired orders

            //PredicateExpression orderFilter;
            //RelationCollection joins;

            // Get the basics for joining and filtering to get expired orders
            //RoutingHelper.GetExpiredOrdersRetrievalSet(out orderFilter, out joins);

            //if (companyIds.Count > 0)
                //orderFilter.Add(OrderFields.CompanyId == companyIds);

            OrderCollection orders = new OrderCollection();
            //orders.GetMulti(orderFilter, joins);

            return orders;
		}

        /// <summary>
        /// Get all orders with status Unprocessable
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static OrderCollection GetUnprocessableOrders(List<int> companyIds, int limit = 0)
        {
            var orders = new OrderCollection();
            
            var unprocessableOrderFilter = new PredicateExpression();
            unprocessableOrderFilter.Add(CompanyFields.UseMonitoring == true);
            unprocessableOrderFilter.Add(OrderFields.Status == (int)OrderStatus.Unprocessable);

            SortExpression sort = new SortExpression(new SortClause(OrderFields.CreatedUTC, SortOperator.Descending));
            
            if (companyIds.Count > 0)
                unprocessableOrderFilter.Add(CompanyFields.CompanyId == companyIds);
            
            var relations = new RelationCollection();
            relations.Add(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);

            orders.GetMulti(unprocessableOrderFilter, limit, sort, relations);

            return orders;
        }

        public static OrderCollection GetRetrievedOrders(List<int> companyIds)
        {
            var filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);

            if (companyIds.Count > 0)
                filter.Add(CompanyFields.CompanyId == companyIds);

            filter.Add(OrderRoutestephandlerFields.Status == (int)OrderRoutestephandlerStatus.RetrievedByHandler);

            var filterPotentialBeingHandledExpired = new PredicateExpression();
            filterPotentialBeingHandledExpired.Add(OrderRoutestephandlerFields.BeingHandledSupportNotificationSent == true);
            filterPotentialBeingHandledExpired.AddWithOr(OrderRoutestephandlerFields.BeingHandledUTC <= DateTime.UtcNow.AddMinutes(-15));
            filter.Add(filterPotentialBeingHandledExpired);

            var orderRoutestephandlers = new OrderRoutestephandlerCollection();
            orderRoutestephandlers.GetMulti(filter, 0, null, GetOrderRoutestephandlerJoins(), GetOrderRoutestephandlerPrefetchPath());

            var orders = GetUniqueOrdersFromOrderRoutestephandlerCollection(orderRoutestephandlers);

            return orders;
        }

        /// <summary>
        /// Gets the unique orderEntities from an orderRoutestephandlerCollection
        /// </summary>
        /// <param name="orderRoutestephandlers">The orderRoutestephandlerCollection</param>
        /// <returns>The order collection</returns>
        public static OrderCollection GetUniqueOrdersFromOrderRoutestephandlerCollection(OrderRoutestephandlerCollection orderRoutestephandlers)
        {
            var orders = new OrderCollection();

            foreach (var routestephandler in orderRoutestephandlers)
            {
                if (!orders.Contains(routestephandler.OrderEntity))
                    orders.Add(routestephandler.OrderEntity);
            }

            return orders;
        }

        #region Count

        /// <summary>
        /// Get all orders which status is NonRouted
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static int GetNonRoutedOrdersCount(int? companyId)
        {
            var orders = new OrderCollection();

            var nonRoutedOrdersfilter = new PredicateExpression();
            nonRoutedOrdersfilter.Add(CompanyFields.UseMonitoring == true);

            if (companyId.HasValue)
                nonRoutedOrdersfilter.Add(CompanyFields.CompanyId == companyId.Value);

            nonRoutedOrdersfilter.Add(OrderFields.Status == (int)OrderStatus.NotRouted);

            var relations = new RelationCollection();
            relations.Add(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);

            return orders.GetDbCount(nonRoutedOrdersfilter, relations);
        }

        /// <summary>
        /// Get all orders which have a route setup but are not started after 45 seconds
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static int GetNotStartedRoutedOrdersCount(int? companyId)
        {
            var orders = new OrderCollection();

            var nonStartedRouteOrdersFilter = new PredicateExpression();
            nonStartedRouteOrdersFilter.Add(CompanyFields.UseMonitoring == true);
            if (companyId.HasValue)
                nonStartedRouteOrdersFilter.Add(CompanyFields.CompanyId == companyId.Value);

            nonStartedRouteOrdersFilter.Add(OrderFields.Status == (int)OrderStatus.Routed);
            nonStartedRouteOrdersFilter.Add(OrderFields.CreatedUTC <= DateTime.UtcNow.AddSeconds(-45));

            var relations = new RelationCollection();
            relations.Add(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);

            return orders.GetDbCount(nonStartedRouteOrdersFilter, relations);
        }

        /// <summary>
        /// Gets the amount of orders which are still InRoute after 90 seconds
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static int GetExpiredOrdersCount(int? companyId)
        {
            int count = 0;

            var companyIds = new List<int>();
            if (companyId.HasValue)
                companyIds.Add(companyId.Value);

            OrderCollection expiredOrders = GetExpiredOrders(companyIds);
            if (expiredOrders != null)
                count = expiredOrders.Count;

            return count;
        }

        /// <summary>
        /// Gets the amount of orders with status Unprocessable
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static int GetUnprocessableOrderCount(int? companyId)
        {
            var orders = new OrderCollection();

            var unprocessableOrderFilter = new PredicateExpression();
            unprocessableOrderFilter.Add(CompanyFields.UseMonitoring == true);
            if (companyId.HasValue)
                unprocessableOrderFilter.Add(CompanyFields.CompanyId == companyId.Value);

            unprocessableOrderFilter.Add(OrderFields.Status == (int)OrderStatus.Unprocessable);

            var relations = new RelationCollection();
            relations.Add(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);

            return orders.GetDbCount(unprocessableOrderFilter, relations);
        }

        public static int GetFaultyOrderCountPastDay(List<int> companyIds)
        {
            OrderCollection orders = new OrderCollection();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);

            if (companyIds.Count > 0)
                filter.Add(CompanyFields.CompanyId == companyIds);
            
            filter.Add(OrderFields.Status != OrderStatus.Processed);
            filter.Add(OrderFields.CreatedUTC > DateTime.UtcNow.AddDays(-1));

            ////// Non routed orders
            //var nonRoutedOrdersFilter = new PredicateExpression();
            //nonRoutedOrdersFilter.Add(OrderFields.Status == (int)OrderStatus.NotRouted);
            //orderFilter.AddWithOr(nonRoutedOrdersFilter);

            ////// Expired orders
            //var expiredOrdersFilter = new PredicateExpression();
            //List<int> automaticHandlerTypes = RoutingHelper.GetAutomaticlyHandledRoutestephandlers().Select(type => (int)type).ToList(); // Only check steps that are automatic
            //expiredOrdersFilter.Add(OrderRoutestephandlerFields.HandlerType == automaticHandlerTypes);
            //List<int> currentStatuses = RoutingHelper.GetPendingStatuses().Select(type => (int)type).ToList(); // Only check steps that are current
            //expiredOrdersFilter.Add(OrderRoutestephandlerFields.Status == currentStatuses);
            //expiredOrdersFilter.Add(OrderRoutestephandlerFields.UpdatedUTC < DateTime.UtcNow.AddSeconds(-90)); // Only steps that are active for more than 90s
            //orderFilter.AddWithOr(expiredOrdersFilter);

            ////// Non started route orders
            //var nonStartedRouteOrdersFilter = new PredicateExpression();
            //nonStartedRouteOrdersFilter.Add(OrderFields.Status == (int)OrderStatus.Routed);
            //nonStartedRouteOrdersFilter.Add(OrderFields.CreatedUTC <= DateTime.UtcNow.AddSeconds(-45));
            //orderFilter.AddWithOr(nonStartedRouteOrdersFilter);

            //// Unprocessable orders
            //var unprocessableOrderFilter = new PredicateExpression();
            //unprocessableOrderFilter.Add(OrderFields.Status == (int)OrderStatus.Unprocessable);
            //orderFilter.AddWithOr(unprocessableOrderFilter);

            //filter.Add(orderFilter);

            //// Relations
            RelationCollection relations = new RelationCollection();
            relations.Add(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);
            //relations.Add(OrderEntityBase.Relations.OrderRoutestephandlerEntityUsingOrderId, JoinHint.Left);

            //return orders.GetDbCount(filter, relations);
            return orders.GetDbCount(filter, relations);
        }

        #endregion

        /// <summary>
		/// Get last X orders for a specific company
		/// </summary>
		/// <param name="companyId"></param>
		/// <param name="numberOfOrders"></param>
		/// <returns></returns>
		public static OrderCollection GetLastOrdersForCompany(int? companyId, int numberOfOrders)
		{
			var orders = new OrderCollection();
			var filter = new PredicateExpression(OrderFields.CompanyId == companyId);
			SortExpression sort = new SortExpression(OrderFields.CreatedUTC | SortOperator.Descending);
			orders.GetMulti(filter, numberOfOrders, sort);

			return orders;
		}

		#region Private Methods

		private static PrefetchPath GetPrefetchPath()
		{
			var pathOrders = new PrefetchPath(EntityType.OrderEntity);
			pathOrders.Add(OrderEntityBase.PrefetchPathCompanyEntity);
			pathOrders.Add(OrderEntityBase.PrefetchPathOrderRoutestephandlerCollection);
			
			return pathOrders;
		}

        private static PrefetchPath GetOrderRoutestephandlerPrefetchPath()
        {
            var prefetch = new PrefetchPath(EntityType.OrderRoutestephandlerEntity);
            prefetch.Add(OrderRoutestephandlerEntityBase.PrefetchPathOrderEntity);

            return prefetch;
        }

        private static RelationCollection GetOrderRoutestephandlerJoins()
        {
            var relations = new RelationCollection();
            relations.Add(OrderRoutestephandlerEntityBase.Relations.OrderEntityUsingOrderId);
            relations.Add(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);

            return relations;
        }

		#endregion
	}
}
