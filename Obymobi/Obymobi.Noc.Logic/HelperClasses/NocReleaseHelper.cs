﻿using System;
using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Noc.Logic.HelperClasses
{
	/// <summary>
	/// 
	/// </summary>
	public class NocReleaseHelper
	{
        /// <summary>
        /// Get list of outdated onsiteserver terminals
        /// </summary>
        /// <param name="companyId">The id of the company</param>
        /// <returns>A <see cref="TerminalCollection"/> instance with the outdated terminals</returns>
        public static TerminalCollection GetOutdatedTerminals(List<int> companyIds)
        {
            var filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);
            filter.Add(TerminalFields.UseMonitoring == true);

            if (companyIds.Count > 0)
                filter.Add(CompanyFields.CompanyId == companyIds);

            filter.Add(CompanyFields.SystemType == SystemType.Crave);
            filter.Add(DeviceFields.ApplicationVersion < CompanyFields.TerminalApplicationVersion);
            filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMinutes(-10));

            var relations = new RelationCollection();
            relations.Add(CompanyEntityBase.Relations.TerminalEntityUsingCompanyId);
            relations.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);

            var prefetch = new PrefetchPath(EntityType.TerminalEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathCompanyEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity);

            var terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter, 0, null, relations, prefetch);

            return terminalCollection;
        }
	}
}
