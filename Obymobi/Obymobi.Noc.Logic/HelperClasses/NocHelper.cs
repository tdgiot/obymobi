﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Dionysos.Web.Security;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Obymobi.Noc.Logic.HelperClasses
{
	public class NocHelper
	{
		public static UserEntity CurrentUser => UserManager.CurrentUser as UserEntity;

		public static TerminalCollection GetTerminals(List<int> companyIds, bool offline, int? minutes, bool disconnectedFromComet = false)
		{
			// Create the filter
			// Only get the terminals where monitoring is enabled
			PredicateExpression filter = new PredicateExpression();
			filter.Add(CompanyFields.UseMonitoring == true);
			filter.Add(TerminalFields.UseMonitoring == true);

			if (companyIds.Count > 0)
				filter.Add(TerminalFields.CompanyId == companyIds);

			if (offline)
				filter.Add(DeviceFields.LastRequestUTC <= DateTime.UtcNow.AddMinutes(-2));

			if (minutes.HasValue)
			{
				filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMinutes(minutes.Value * -1));
				filter.Add(DeviceFields.LastRequestUTC <= DateTime.UtcNow);
			}

			if (disconnectedFromComet)
				filter.Add(DeviceFields.CommunicationMethod == ClientCommunicationMethod.Webservice);

			// Create the relations
			RelationCollection relations = new RelationCollection();
			relations.Add(TerminalEntityBase.Relations.CompanyEntityUsingCompanyId);
			relations.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);

			// Create the sort
			SortExpression sort = new SortExpression();
			sort.Add(new SortClause(DeviceFields.LastRequestUTC, SortOperator.Descending));

			PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
			prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity);

			// TODO, only certain fields?

			// Get the terminals
			TerminalCollection terminals = new TerminalCollection();
			terminals.GetMulti(filter, 0, sort, relations, prefetch);

			return terminals;
		}

		public static CompanyCollection GetCompanies(List<int> companyIds)
		{
			// Create the filter
			// Only get the companies where monitoring is enabled
			PredicateExpression filter = new PredicateExpression();
			filter.Add(CompanyFields.UseMonitoring == true);

			if (companyIds.Count > 0)
				filter.Add(CompanyFields.CompanyId == companyIds);

			CompanyCollection companies = new CompanyCollection();
			companies.GetMulti(filter);

			return companies;
		}

		public static ClientCollection GetClients(List<int> companyIds, int? minutes, bool batteryLow, bool disconnectedFromComet = false)
		{
			// Create the filter
			// Only get the clients where monitoring is enabled
			PredicateExpression filter = new PredicateExpression();
			filter.Add(CompanyFields.UseMonitoring == true);

			// Only show clients that are linked to a device OR belong to an Otouch Company           
			filter.Add(ClientFields.DeviceId != DBNull.Value | CompanyFields.SystemType == (int)SystemType.Otoucho);

			if (companyIds.Count > 0)
			{
				filter.Add(ClientFields.CompanyId == companyIds);
			}

			if (minutes.HasValue)
			{
				filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMinutes(minutes.Value * -1));
			}

			if (batteryLow)
			{
				filter.Add(DeviceFields.BatteryLevel != DBNull.Value);
				filter.Add(DeviceFields.BatteryLevel < 20);
			}

			if (disconnectedFromComet)
				filter.Add(DeviceFields.CommunicationMethod == ClientCommunicationMethod.Webservice);

			// Create the relations
			RelationCollection relations = new RelationCollection();
			relations.Add(ClientEntityBase.Relations.CompanyEntityUsingCompanyId);
			relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

			// Create the sort
			SortExpression sort = new SortExpression();
			sort.Add(new SortClause(DeviceFields.LastRequestUTC, SortOperator.Descending));

			// TODO, only certain fields? > GK: First take the bigger performance gain - Prefetch!
			PrefetchPath path = new PrefetchPath(EntityType.ClientEntity);
			path.Add(ClientEntityBase.PrefetchPathDeliverypointEntity).SubPath.Add(DeliverypointEntityBase.PrefetchPathDeliverypointgroupEntity);
			path.Add(ClientEntityBase.PrefetchPathDeliverypointgroupEntity);
			path.Add(ClientEntityBase.PrefetchPathCompanyEntity);
			path.Add(ClientEntityBase.PrefetchPathDeviceEntity);

			// Get the clients
			ClientCollection clients = new ClientCollection();
			clients.GetMulti(filter, 0, sort, relations, path);

			return clients;
		}

		public static OrderCollection GetActiveOrdersForTerminal(int terminalId)
		{
			// Create the filters
			var filter = new PredicateExpression();
			filter.Add(OrderRoutestephandlerFields.TerminalId == terminalId);

			var subFilter = new PredicateExpression();
			subFilter.Add(OrderFields.Status != OrderStatus.Processed);
			subFilter.AddWithAnd(OrderFields.Status != OrderStatus.Unprocessable);

			filter.Add(subFilter);
			
			// Create relations
			var relations = new RelationCollection();
			relations.Add(OrderEntityBase.Relations.OrderRoutestephandlerEntityUsingOrderId);

			// Create prefetch path
			var prefetch = new PrefetchPath(EntityType.OrderEntity);
			prefetch.Add(OrderEntityBase.PrefetchPathOrderRoutestephandlerCollection);

			// Create the sort
			SortExpression sort = new SortExpression();
			sort.Add(new SortClause(OrderFields.CreatedUTC, SortOperator.Descending));

			// Fetch collection
			var collection = new OrderCollection();
			collection.GetMulti(filter, 0, sort, relations, prefetch);

			return collection;
		}

		public static OutletCollection GetOutlets(List<int> companyIds)
		{
			PredicateExpression filter = new PredicateExpression();

			if (!companyIds.Any())
			{
				return new OutletCollection();
			}

			filter.Add(new FieldCompareRangePredicate(OutletFields.CompanyId, companyIds));

			RelationCollection relations = new RelationCollection
			{
				OutletEntityBase.Relations.CompanyEntityUsingCompanyId
			};

			SortExpression sort = new SortExpression { new SortClause(OutletFields.Name, SortOperator.Ascending) };
			IncludeFieldsList includedFields = new IncludeFieldsList() { OutletFields.Name };

			OutletCollection outlets = new OutletCollection();
			outlets.GetMulti(filter, 0, sort, relations, null, includedFields, 0, 0);

			return outlets;
		}

		public static OrderCollection GetOrders(List<int> companyIds, int? clientId, int? minutes, int? orderId, int count = 50)
		{
			PredicateExpression filter = new PredicateExpression();

            if (companyIds.Count > 0)
				filter.Add(OrderFields.CompanyId == companyIds);
			
			if (clientId.HasValue)
				filter.Add(OrderFields.ClientId == clientId.Value);
			
			if (minutes.HasValue)
				filter.Add(OrderFields.CreatedUTC > DateTime.UtcNow.AddMinutes(minutes.Value * -1));

            if (orderId.HasValue)
                filter.AddWithAnd(OrderFields.OrderId == orderId.Value);


			return GetOrders(filter, null, true, count);
		}

		public static OrderCollection GetOrders(PredicateExpression customFilter, RelationCollection customRelations, bool monitoredCompaniesOnly, int maxResults)
		{
			// Create the filter
			// Only get the orders where monitoring is enabled
			PredicateExpression filter = new PredicateExpression();
			if(monitoredCompaniesOnly)
				filter.Add(CompanyFields.UseMonitoring == true);

			// Custom filter
			if(customFilter != null)
				filter.Add(customFilter);

			// Create the relations
			RelationCollection relations = new RelationCollection();
			relations.Add(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);
			if (customRelations != null)
				relations.AddRange(customRelations);            

			// Create the sort
			SortExpression sort = new SortExpression();
			sort.Add(new SortClause(OrderFields.OrderId, SortOperator.Descending));

			// TODO, only certain fields?

			// Get the orders
			OrderCollection orders = new OrderCollection();
			orders.GetMulti(filter, maxResults, sort, relations);

			return orders;
		}

		public static OrderCollection GetSubOrders(OrderEntity order)
		{
			var filter = new PredicateExpression();
			filter.Add(OrderFields.MasterOrderId == order.OrderId);

			// Get the orders
			var orders = new OrderCollection();
			orders.GetMulti(filter);

			return orders;
		}

		public static OrderitemCollection GetOrderitems(int orderId)
		{
			// Create the filter
			// Only get the order items for the specified order id
			PredicateExpression filter = new PredicateExpression();
			filter.Add(OrderitemFields.OrderId == orderId);
			filter.AddWithOr(OrderFields.MasterOrderId == orderId);

			RelationCollection relations = new RelationCollection();
			relations.Add(OrderitemEntityBase.Relations.OrderEntityUsingOrderId);

			// TODO, only certain fields?

			// Get the OrderRoutestephandlers
			OrderitemCollection orderitems = new OrderitemCollection();
			orderitems.GetMulti(filter, relations);

			return orderitems;
		}

		public static OrderRoutestephandlerCollection GetOrderRoutestephandlers(int orderId)
		{
			// Create the filter
			// Only get the order routestephandlers for the specified order id
			PredicateExpression filter = new PredicateExpression();
			filter.Add(OrderRoutestephandlerFields.OrderId == orderId);

			// Create the sort
			SortExpression sort = new SortExpression();
			sort.Add(new SortClause(OrderRoutestephandlerFields.Number, SortOperator.Ascending));

			// TODO, only certain fields?

			// Get the OrderRoutestephandlers
			OrderRoutestephandlerCollection orderRoutestephandlers = new OrderRoutestephandlerCollection();
			orderRoutestephandlers.GetMulti(filter, 0, sort);

			return orderRoutestephandlers;
		}

		public static OrderRoutestephandlerHistoryCollection GetOrderRoutestephandlersHistory(int orderId)
		{
			// Create the filter
			// Only get the order routestephandlers history for the specified order id
			PredicateExpression filter = new PredicateExpression();
			filter.Add(OrderRoutestephandlerHistoryFields.OrderId == orderId);

			// Create the sort
			SortExpression sort = new SortExpression();
			sort.Add(new SortClause(OrderRoutestephandlerHistoryFields.Number, SortOperator.Ascending));

			// TODO, only certain fields?

			// Get the OrderRoutestephandlersHistory
			OrderRoutestephandlerHistoryCollection orderRoutestephandlersHistory = new OrderRoutestephandlerHistoryCollection();
			orderRoutestephandlersHistory.GetMulti(filter, 0, sort);

			return orderRoutestephandlersHistory;
		}

		/// <summary>
		/// Get products linked to a category.
		/// </summary>
		public static ProductCollection GetLinkedProducts(IReadOnlyCollection<int> companyIdentities, int limit = 50, string searchRequest = "", VisibilityType? visibilityType = null)
		{
			if (!companyIdentities.Any())
            {
                throw new ArgumentNullException(nameof(companyIdentities), "The argument cannot be null or empty.");
            }

			PredicateExpression filter = new PredicateExpression();
			filter.Add(ProductFields.CompanyId.In(companyIdentities));
            filter.Add(ProductFields.Type == ProductType.Product);

			if (visibilityType != null)
            {
				filter.AddWithAnd(ProductFields.VisibilityType == visibilityType);
            }

			if (!searchRequest.IsNullOrWhiteSpace())
			{
				filter.AddWithAnd(ProductFields.Name.Contains(searchRequest));
			}
			
			return GetLinkedProducts(filter, limit);
		}

		private static ProductCollection GetLinkedProducts(PredicateExpression filter, int limit)
        {
            IncludeFieldsList includedProductFields = new IncludeFieldsList(ProductFields.Name, ProductFields.VisibilityType, ProductFields.SubType);
            IncludeFieldsList includedProductCategoryFields = new IncludeFieldsList(ProductCategoryFields.CategoryId);
            IncludeFieldsList includedCategoryFields = new IncludeFieldsList(CategoryFields.Name, CategoryFields.Type);

			RelationCollection relations = new RelationCollection();
            relations.Add(ProductEntityBase.Relations.ProductCategoryEntityUsingProductId);
            relations.Add(ProductCategoryEntityBase.Relations.CategoryEntityUsingCategoryId);

			PrefetchPath prefetch = new PrefetchPath(EntityType.ProductEntity);
            prefetch.Add(ProductEntityBase.PrefetchPathProductCategoryCollection, includedProductCategoryFields)
                .SubPath.Add(ProductCategoryEntityBase.PrefetchPathCategoryEntity, includedCategoryFields)
                .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, includedCategoryFields)
                .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, includedCategoryFields)
                .SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity, includedCategoryFields);

			SortExpression sort = new SortExpression(ProductFields.Name.Ascending());

			ProductCollection productCollection = new ProductCollection();
			productCollection.GetMulti(filter, limit, sort, relations, prefetch, includedProductFields, 0, 0);

			return productCollection;
		}

        public static TerminalLogCollection GetTerminalLogs(int? terminalId, int? orderId)
		{
			// Create the filter
			// Only get the terminals where monitoring is enabled
			PredicateExpression filter = new PredicateExpression();
			filter.Add(TerminalFields.UseMonitoring == true);

			if (terminalId.HasValue)
				filter.Add(TerminalLogFields.TerminalId == terminalId.Value);

			if (orderId.HasValue)
				filter.Add(TerminalLogFields.OrderId == orderId.Value);

			RelationCollection relations = new RelationCollection();
			relations.Add(TerminalLogEntityBase.Relations.TerminalEntityUsingTerminalId);

			// Create the sort
			SortExpression sort = new SortExpression();
			sort.Add(new SortClause(TerminalLogFields.CreatedUTC, SortOperator.Descending));

			// TODO, only certain fields?

			// Get the terminals
			TerminalLogCollection terminallogs = new TerminalLogCollection();
			terminallogs.GetMulti(filter, 25, sort, relations);

			return terminallogs;
		}

		public static ClientLogCollection GetClientLogs(int clientId)
		{
			// Create the filter
			// Only get the terminals where monitoring is enabled
			PredicateExpression filter = new PredicateExpression();

			filter.Add(ClientLogFields.ClientId == clientId);

			// Create the sort
			SortExpression sort = new SortExpression();
			sort.Add(new SortClause(ClientLogFields.CreatedUTC, SortOperator.Descending));

			// TODO, only certain fields?

			// Get the terminals
			ClientLogCollection clientlogs = new ClientLogCollection();
			clientlogs.GetMulti(filter, 25, sort);

			return clientlogs;
		}

		public static DeliverypointgroupCollection GetDeliverypointgroups(List<int> companyIds) 
		{
			// Create the filter
			// Only get the deliverypoint where monitoring is enabled
			PredicateExpression filter = new PredicateExpression();
			filter.Add(CompanyFields.UseMonitoring == true);

			if (companyIds.Count > 0)
				filter.Add(DeliverypointgroupFields.CompanyId == companyIds);

			// Create the relations
			RelationCollection relations = new RelationCollection();
			relations.Add(DeliverypointgroupEntityBase.Relations.CompanyEntityUsingCompanyId);

			// Create the sort
			SortExpression sort = new SortExpression();
			sort.Add(new SortClause(DeliverypointgroupFields.Name, SortOperator.Ascending));

			// Create the prefetch
			PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointgroupEntity);
			prefetch.Add(DeliverypointgroupEntityBase.PrefetchPathDeliverypointCollection);

			// TODO, only certain fields?

			// Get the terminals
			DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
			deliverypointgroups.GetMulti(filter, 0, sort, relations, prefetch);

			return deliverypointgroups;
		}

		public static DeliverypointCollection GetDeliverypoints(List<int> companyIds)
		{
			// Create the filter
			// Only get the deliverypoint where monitoring is enabled
			PredicateExpression filter = new PredicateExpression();
			filter.Add(CompanyFields.UseMonitoring == true);

			if (companyIds.Count > 0)
				filter.Add(DeliverypointFields.CompanyId == companyIds);

			// Create the relations
			RelationCollection relations = new RelationCollection();
			relations.Add(DeliverypointEntityBase.Relations.CompanyEntityUsingCompanyId);

			// Create the sort
			SortExpression sort = new SortExpression();
			sort.Add(new SortClause(DeliverypointFields.Number, SortOperator.Ascending));

			// Create the prefetch
			PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointEntity);
			prefetch.Add(DeliverypointEntityBase.PrefetchPathClientCollection);
			prefetch.Add(DeliverypointEntityBase.PrefetchPathDeliverypointgroupEntity);

			// TODO, only certain fields?

			// Get the terminals
			DeliverypointCollection deliverypoints = new DeliverypointCollection();
			deliverypoints.GetMulti(filter, 0, sort, relations, prefetch);

			return deliverypoints;
		}

		private static DateTime? GetScalarTyped(object scalarResult)
		{
			if (scalarResult == DBNull.Value)
				return null;
			else
				return (DateTime)scalarResult;
		}

		public static DateTime? GetLastTerminalActivity(int companyId)
		{
			// Create the filter
			PredicateExpression filter = new PredicateExpression();
			filter.Add(TerminalFields.CompanyId == companyId);
			filter.Add(TerminalFields.UseMonitoring == true);

			RelationCollection relations = new RelationCollection { DeviceEntity.Relations.TerminalEntityUsingDeviceId };
			
			return GetScalarTyped(new DeviceCollection().GetScalar(DeviceFieldIndex.LastRequestUTC, null, AggregateFunction.Max, filter, relations, null));
		}

		public static DateTime? GetLastClientActivity(int companyId)
		{
			PredicateExpression filter = new PredicateExpression { ClientFields.CompanyId == companyId };
			RelationCollection relations = new RelationCollection { ClientEntity.Relations.DeviceEntityUsingDeviceId };

			return GetScalarTyped(new DeviceCollection().GetScalar(DeviceFieldIndex.LastRequestUTC, null, AggregateFunction.Max, filter, relations, null));
		}

		public static DateTime? GetLastOrderPlaced(int companyId)
		{
			// Create the filter
			PredicateExpression filter = new PredicateExpression();
			filter.Add(OrderFields.CompanyId == companyId);

			OrderCollection orders = new OrderCollection();

			return GetScalarTyped(orders.GetScalar(OrderFieldIndex.CreatedUTC, null, AggregateFunction.Max, filter));
		}

		public static int GetIssueCount(List<int> companyIds)
		{
			// Non routed orders
			// Non started routes orders
			// Expired orders
			// Unprocessable orders
			// Non ordering clients
			// Offline terminals

			int faultyOrders = NocOrderHelper.GetFaultyOrderCountPastDay(companyIds);
			int offlineClients = NocCompanyHelper.GetNonOrderingClientsGroupedByOperationModeCount(companyIds);
			int offlineTerminals = NocCompanyHelper.GetOfflineTerminalsCount(companyIds);

			return faultyOrders + offlineClients + offlineTerminals;
		}
	}
}
