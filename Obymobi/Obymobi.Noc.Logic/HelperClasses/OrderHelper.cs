﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Noc.Logic.HelperClasses
{
    public class OrderHelper
    {
        public static List<DeviceActivityLog> GetTerminalLogsForOrder(int orderId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalLogFields.OrderId == orderId);

            TerminalLogCollection logs = new TerminalLogCollection();
            logs.GetMulti(filter);

            List<DeviceActivityLog> toReturn = new List<DeviceActivityLog>();

            foreach (TerminalLogEntity log in logs)
            {
                toReturn.Add(new DeviceActivityLog(log));
            }

            return toReturn;
        }
    }
}
