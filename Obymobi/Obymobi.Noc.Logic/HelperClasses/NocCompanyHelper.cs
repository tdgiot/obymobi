﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Constants;
using System.Data;
using Obymobi.Data.DaoClasses;
using Obymobi.Data;
using Dionysos.Data.LLBLGen;
using System.Collections.Generic;

namespace Obymobi.Noc.Logic.HelperClasses
{
    /// <summary>
    /// NocCompanyHelper class
    /// </summary>
	public class NocCompanyHelper
	{
		/// <summary>
        /// Get a list of companies with clients in a non ordering mode
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static ClientCollection GetNonOrderingClients2(int? companyId)
        {
            // Clients != Ordering OperationMode
            var filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);

            if (companyId.HasValue)
                filter.Add(CompanyFields.CompanyId == companyId.Value);

            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            filter.Add(ClientFields.OperationMode != (int)ClientOperationMode.Ordering);

            var clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, GetJoinCompany());
            
            return clientCollection;
        }

        /// <summary>
        /// Gets the client summaries per company or every company for companies with clients in non ordering mode
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <returns>
        /// DataTable with summaries
        /// Column 0: Company id
        /// Column 1: Company name
        /// Column 2: Operation mode
        /// Column 3: Client amount
        /// </returns>
        public static DataTable GetNonOrderingClientsGroupedByOperationMode(List<int> companyIds)
        {
            var fields = new ResultsetFields(4);
            fields.DefineField(ClientFields.CompanyId, 0);
            fields.DefineField(CompanyFields.Name, 1);
            fields.DefineField(ClientFields.OperationMode, 2);
            fields.DefineField(ClientFields.OperationMode, 3, "ClientAmount", AggregateFunction.CountRow);

            var groupBy = new GroupByCollection();
            groupBy.Add(ClientFields.CompanyId);
            groupBy.Add(ClientFields.OperationMode);
            groupBy.Add(CompanyFields.Name);

            // Clients != Ordering OperationMode
            var filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);

            if (companyIds.Count > 0)
                filter.Add(CompanyFields.CompanyId == companyIds);

            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            filter.Add(ClientFields.OperationMode != (int)ClientOperationMode.Ordering);

            var sort = new SortExpression();
            sort.Add(new SortClause(fields[1], SortOperator.Ascending));

            DataTable dt = new DataTable();
            TypedListDAO dao = new TypedListDAO();
            dao.GetMultiAsDataTable(fields, dt, 0, sort, filter, GetJoinCompany(), false, groupBy, null, 0, 0);
            
            // Column 0: Company id
            // Column 1: Company name
            // Column 2: Operation mode
            // Column 3: Client amount

            return dt;
        }

        /// <summary>
        /// Gets the amount of clients that are currently offline.
        /// </summary>
        /// <param name="companyId">The id of the company to get the offline client count for.</param>
        /// <returns>The amount of clients offline.</returns>
        public static int GetOfflineClientCount(int companyId)
        {
            PredicateExpression offlineFilter = new PredicateExpression();
            offlineFilter.Add(CompanyFields.UseMonitoring == true);
            offlineFilter.Add(CompanyFields.SystemType == SystemType.Crave);
            offlineFilter.Add(ClientFields.CompanyId == companyId);
            offlineFilter.Add(ClientFields.DeviceId != DBNull.Value);
            offlineFilter.Add(DeviceFields.LastRequestUTC <= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.CompanyEntityUsingCompanyId);
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            return EntityCollection.GetDbCount<ClientCollection>(offlineFilter, relations);
        }

        /// <summary>
        /// Gets the amount of clients for the specified company
        /// </summary>
        /// <param name="companyId">The id of the company to get the client count for.</param>
        /// <returns>The amount of the clients.</returns>
        public static int GetClientCount(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);
            filter.Add(CompanyFields.SystemType == SystemType.Crave);
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(ClientFields.DeviceId != DBNull.Value);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.CompanyEntityUsingCompanyId);

            return EntityCollection.GetDbCount<ClientCollection>(filter, relations);
        }

        public static List<Tuple<int, string, int>> GetCompaniesWithTooMuchOfflineClients(List<int> companyIds)
        {
            List<Tuple<int, string, int>> companyList = new List<Tuple<int, string, int>>();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);
            filter.Add(CompanyFields.SystemType == SystemType.Crave);
            
            if (companyIds.Count > 0)
                filter.Add(CompanyFields.CompanyId == companyIds);

            CompanyCollection companyCollection = EntityCollection.GetMulti<CompanyCollection>(filter);
            foreach (CompanyEntity companyEntity in companyCollection)
            {
                int offlineClients = GetOfflineClientCount(companyEntity.CompanyId);
                int totalClients = GetClientCount(companyEntity.CompanyId);
                int percentageOffline = Dionysos.Math.Percentage(offlineClients, totalClients, true);

                if (percentageOffline > companyEntity.PercentageDownForNotification)
                    companyList.Add(new Tuple<int, string, int>(companyEntity.CompanyId, companyEntity.Name, percentageOffline));
            }

            return companyList;
        }

        /// <summary>
        /// Gets the amount of clients in a non ordering mode
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static int GetNonOrderingClientsGroupedByOperationModeCount(List<int> companyIds)
        {
            // Clients != Ordering OperationMode
            var filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);

            if (companyIds.Count > 0)
                filter.Add(CompanyFields.CompanyId == companyIds);

            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            filter.Add(ClientFields.OperationMode != (int)ClientOperationMode.Ordering);

            var clientCollection = new ClientCollection();
            return clientCollection.GetDbCount(filter, GetJoinCompany());
        }

        /// <summary>
        /// Gets the offline terminals with online clients.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <returns></returns>
        public static TerminalCollection GetOfflineTerminals2(List<int> companyIds)
        {
            var filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);
            filter.Add(TerminalFields.UseMonitoring == true);

            if (companyIds.Count > 0)
                filter.Add(CompanyFields.CompanyId == companyIds);

            filter.Add(DeviceFields.LastRequestUTC <= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));

            var prefetch = new PrefetchPath(EntityType.TerminalEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathCompanyEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity);

            var terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter, 0, null, GetJoinTerminal(), prefetch);

            return terminalCollection;
        }

        /// <summary>
        /// Gets the terminals with online clients.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <returns></returns>
        public static TerminalCollection GetTerminals(int? companyId)
        {
            var filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);
            filter.Add(TerminalFields.UseMonitoring == true);

            if (companyId.HasValue)
                filter.Add(CompanyFields.CompanyId == companyId.Value);

            PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity);

            var terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter, 0, null, GetJoinTerminal(), prefetch);

            return terminalCollection;
        }

        /// <summary>
        /// Gets the amount of offline terminals with online clients.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <returns></returns>
        public static int GetOfflineTerminalsCount(List<int> companyIds)
        {
            var filter = new PredicateExpression();
            filter.Add(CompanyFields.UseMonitoring == true);
            filter.Add(TerminalFields.UseMonitoring == true);

            if (companyIds.Count > 0)
                filter.Add(CompanyFields.CompanyId == companyIds);

            filter.Add(DeviceFields.LastRequestUTC <= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));

            var terminalCollection = new TerminalCollection();
            return terminalCollection.GetDbCount(filter, GetJoinTerminal());
        }

        /// <summary>
        /// Gets the join terminal.
        /// </summary>
        /// <returns></returns>
		private static RelationCollection GetJoinTerminal()
		{
			// Company > Client
            // Company > Terminal
			RelationCollection joinTerminals = new RelationCollection();
			joinTerminals.Add(CompanyEntityBase.Relations.TerminalEntityUsingCompanyId);
		    joinTerminals.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);

			return joinTerminals;
		}

        /// <summary>
        /// Gets the join company.
        /// </summary>
        /// <returns></returns>
        private static RelationCollection GetJoinCompany()
        {
            // Client > Company
            RelationCollection joinTerminals = new RelationCollection();
            joinTerminals.Add(ClientEntityBase.Relations.CompanyEntityUsingCompanyId);
            joinTerminals.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            return joinTerminals;
        }
	}
}
