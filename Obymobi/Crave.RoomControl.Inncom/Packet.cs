﻿using System;
using System.Net.Sockets;
using System.Text;

namespace Crave.RoomControl.Inncom
{
    class Packet
    {
        // Constants
        public const int DEFAULT_READPOS = 12;
        public const int DEFAULT_HEADERSIZE = 12;

        // Members
        private byte[] buffer = null;
        private int readPos = DEFAULT_READPOS;

        private int tag;
        private int dataSize;
        private int sourceId;
        private int destinationId;
        private long requestId;

        public SocketError LastError { get; private set; }

        public Packet()
        {
            this.Reset();
        }

        public Packet(int tag)
        {
            this.Reset();
            this.Tag = tag;
        }

        private void Grow(int size)
        {
            if (this.buffer == null)
            {
                this.buffer = new byte[this.readPos + size];
            }
            else
            {
                byte[] newBuffer = new byte[this.buffer.Length + size];
                Buffer.BlockCopy(this.buffer, 0, newBuffer, 0, this.buffer.Length);
                this.buffer = newBuffer;
            }
        }

        public void Reset()
        {
            this.DataSize = 0;
            this.readPos = Packet.DEFAULT_READPOS;
            this.Tag = 0;
            this.DataSize = 0;
            this.SourceId = 0;
            this.DestinationId = 0;
            this.RequestId = 0;
            this.buffer = new byte[Packet.DEFAULT_HEADERSIZE];
        }

        public bool SendToServer(Socket socket)
        {
            try
            {
                this.Tag = this.Tag;
                this.DataSize = this.DataSize;
                this.SourceId = this.SourceId;
                this.DestinationId = this.DestinationId;
                this.RequestId = this.RequestId;

                socket.Send(this.buffer, this.DataSize + DEFAULT_HEADERSIZE, SocketFlags.None);
                return true;
            }
            catch (SocketException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public int Tag
        {
            get { return this.tag; }
            set
            {
                this.tag = value;

                if (this.buffer != null && this.buffer.Length > 1)
                {
                    this.buffer[0] = (byte)(value >> 8);
                    this.buffer[1] = (byte)(value);
                }
            }
        }

        public int DataSize
        {
            get { return this.dataSize; }
            set
            {
                this.dataSize = value;

                if (this.buffer != null && this.buffer.Length > 3)
                {
                    this.buffer[2] = (byte)(value >> 8);
                    this.buffer[3] = (byte)(value);
                } 
            }
        }

        public int SourceId
        {
            get { return this.sourceId; }
            set
            {
                this.sourceId = value;

                if (this.buffer != null && this.buffer.Length > 5)
                {
                    this.buffer[4] = (byte)(value >> 8);
                    this.buffer[5] = (byte)(value);
                }
            }
        }

        public int DestinationId 
        {
            get { return this.destinationId; }
            set
            {
                this.destinationId = value;

                if (this.buffer != null && this.buffer.Length > 7)
                {
                    this.buffer[6] = (byte)(value >> 8);
                    this.buffer[7] = (byte)(value);
                }
            }
        }

        public long RequestId
        {
            get { return this.requestId; }
            set
            {
                this.requestId = value;

                if (this.buffer != null && this.buffer.Length > 11)
                {
                    this.buffer[8] = (byte)(value >> 24);
                    this.buffer[9] = (byte)(value >> 16);
                    this.buffer[10] = (byte)(value >> 8);
                    this.buffer[11] = (byte)(value);
                }
            }
        }

        public int ReadPos
        {
            get { return this.readPos; }
        }

        public bool Receive(Socket socket)
        {
            this.LastError = SocketError.Success;

            this.buffer = new byte[Packet.DEFAULT_HEADERSIZE];

            try
            {
                socket.Receive(this.buffer, Packet.DEFAULT_HEADERSIZE, SocketFlags.None);
            }
            catch (SocketException sex)
            {
                this.LastError = (SocketError) sex.ErrorCode;

                return false;
            }
            catch (Exception)
            {
                LastError = SocketError.ConnectionReset;

                return false;
            }

            this.readPos = 0;

            this.Tag = this.ReadUShort();
            this.DataSize = this.ReadUShort();
            this.SourceId = this.ReadUShort();
            this.DestinationId = this.ReadUShort();
            this.RequestId = this.ReadULong();

            int readBytes = 0;
            if (this.DataSize > 0)
            {
                this.Grow(this.DataSize);

                int nextByte = 0;
                while (true)
                {
                    nextByte = socket.Receive(this.buffer, readBytes + DEFAULT_HEADERSIZE, this.DataSize - readBytes, SocketFlags.None);
                    if (nextByte > 0)
                    {
                        readBytes += nextByte;
                        if (readBytes == this.DataSize)
                        {
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return (this.DataSize == readBytes);
        }

        public byte ReadByte()
        {
            byte value = 0;
            if (this.buffer != null && this.buffer.Length >= this.readPos + 1)
            {
                value = this.buffer[this.readPos++];
            }
            return value;
        }

        public int ReadUShort()
        {
            int value = 0;
            if (this.buffer != null && this.buffer.Length >= this.readPos + 2)
            {
                value = ((this.buffer[this.readPos + 0] & 0xFF) << 8) |
                        ((this.buffer[this.readPos + 1] & 0xFF));

                this.readPos += 2;
            }
            return value;
        }

        public int ReadULong()
        {
            int value = 0;
            if (this.buffer != null && this.buffer.Length >= this.readPos + 4)
            {
                value = ((this.buffer[this.readPos + 0] & 0xFF) << 24) |
                        ((this.buffer[this.readPos + 1] & 0xFF) << 16) |
                        ((this.buffer[this.readPos + 2] & 0xFF) << 8) |
                        ((this.buffer[this.readPos + 3] & 0xFF));

                this.readPos += 4;
            }
            return value;
        }

        public byte[] ReadShortBinaryArray()
        {
            // Get the size
            int size = this.ReadByte();

            // Copy the bytes to a new buffer
            byte[] bytes = new byte[size];
            Buffer.BlockCopy(this.buffer, this.readPos, bytes, 0, size);
            this.readPos += size;

            return bytes;
        }

        public byte[] ReadLongBinaryArray()
        {
            // Get the size
            int size = this.ReadUShort();

            // Copy the bytes to a new buffer
            byte[] bytes = new byte[size];
            Buffer.BlockCopy(this.buffer, this.readPos, bytes, 0, size);
            this.readPos += size;

            return bytes;
        }

        public String ReadString()
        {
            // Get the size
            int size = this.ReadByte();

            // Copy the bytes to a new buffer
            byte[] bytes = new byte[size];
            Buffer.BlockCopy(this.buffer, this.readPos, bytes, 0, size);
            this.readPos += size;

            // Return the bytes as String
            return Encoding.UTF8.GetString(bytes);
        }

        public void AddByte(byte b)
        {
            this.Grow(1);

            if (this.buffer != null && this.buffer.Length >= this.readPos + 1)
            {
                this.buffer[this.readPos++] = b;
                this.DataSize++;
            }
        }

        public void AddUShort(int value)
        {
            this.Grow(2);

            if (this.buffer != null && this.buffer.Length >= this.readPos + 2)
            {
                this.buffer[this.readPos++] = (byte)(value >> 8);
                this.buffer[this.readPos++] = (byte)(value);

                this.DataSize += 2;
            }
        }

        public void AddULong(long value)
        {
            this.Grow(4);

            if (this.buffer != null && this.buffer.Length >= this.readPos + 4)
            {
                this.buffer[this.readPos++] = (byte)(value >> 24);
                this.buffer[this.readPos++] = (byte)(value >> 16);
                this.buffer[this.readPos++] = (byte)(value >> 8);
                this.buffer[this.readPos++] = (byte)(value);

                this.DataSize += 4;
            }
        }

        public void AddBlob(byte[] bytes)
        {
            this.Grow(bytes.Length);

            if (this.buffer != null && this.buffer.Length >= this.readPos + bytes.Length)
            {
                Buffer.BlockCopy(bytes, 0, this.buffer, this.readPos, bytes.Length);
                this.readPos += bytes.Length;
                this.DataSize += bytes.Length;
                return;
            }
        }

        public void AddShortBinaryArray(byte[] bytes)
        {
            this.AddByte((byte)bytes.Length);
            this.AddBlob(bytes);
        }

        public void AddLongBinaryArray(byte[] bytes)
        {
            this.AddUShort(bytes.Length);
            this.AddBlob(bytes);
        }

        public void AddString(String value)
        {
            // Convert the string to bytes
            byte[] fileBytes = Encoding.UTF8.GetBytes(value);

            // Add size
            this.AddByte((byte)fileBytes.Length);

            // Add data
            this.AddBlob(fileBytes);
        }
    }
}