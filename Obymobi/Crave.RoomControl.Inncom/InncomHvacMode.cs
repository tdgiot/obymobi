﻿using Dionysos;

namespace Crave.RoomControl.Inncom
{
    public enum InncomHvacMode
    {
        /// <summary>
        /// Off
        /// </summary>
        [StringValue("Off")]
        Off = 0,

        /// <summary>
        /// Fan Only (no heating, no cooling, fan adapting automatically)
        /// </summary>
        [StringValue("FanOnly")]
        FanOnly = 1,

        /// <summary>
        /// Fan Fixed (heating & cooling allowed but fan stays steady)
        /// </summary>
        [StringValue("FanFixed")]
        FanFixed = 2,

        /// <summary>
        /// Auto Auto (heating & cooling with fan adapting automatically)
        /// </summary>
        [StringValue("Auto")]
        Auto = 3
    }
}
