﻿using Dionysos.Configuration;

namespace Crave.RoomControl.Inncom.Config
{
    public class InncomConfigInfo : ConfigurationItemCollection
    {
        const string SECTION_NAME = "Inncom";

        public InncomConfigInfo()
        {
            Add(new ConfigurationItem(SECTION_NAME, InncomConfigConstants.InncomHost, "Host of InnCom room control server", "", typeof (string)));
            Add(new ConfigurationItem(SECTION_NAME, InncomConfigConstants.InncomPort, "Port of InnCom room control server", 0, typeof(int)));
        }
    }
}
