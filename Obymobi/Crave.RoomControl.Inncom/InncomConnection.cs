﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Crave.RoomControl.Enums;
using Crave.RoomControl.Models;
using Serilog;

namespace Crave.RoomControl.Inncom
{
    internal class InncomConnection
    {
        #region Fields

        public const long CRAVE_APPLICATION_ID = 0xFFFFFFBC;

	    public const long BEACON_TICKS = 60000;

	    public const int TAG_STARTUP = 0xFFFF;
	    public const int TAG_BEACON = 0xFFFE;
        public const int TAG_ATTRIBUTE_SYNCH_REQUEST = 0xFF80;
	    public const int TAG_ATTRIBUTE_UPDATE = 0xFF81;
	    public const int TAG_SYNCHRONIZATION_REQUEST = 0xFFA3;
	    public const int TAG_SYNCHRONIZATION_END = 0xFFAC;
        public const int TAG_LIGHT_OUTPUT_STATE_SUMMARY_REQUEST = 0xFF7B;
        public const int TAG_LIGHT_OUTPUT_STATE_SUMMARY = 0xFF7A;
        public const int TAG_ASYNC_ROOM_UPDATE = 0xFFA0;
        public const int TAG_GUESTROOM_CONTROLS = 0xFF83;

        public const int CMD_SET_HVAC_TEMPERATURE = 0x003F;
        public const int CMD_SET_HVAC_SCALE = 0x0040;
        public const int CMD_SET_HVAC_MODE = 0x0041;
        public const int CMD_SET_HVAC_FAN_SPEED = 0x0042;

        public const int CMD_SET_LIGHT_TOGGLE = 0x0052;
        public const int CMD_SET_LIGHT_LEVEL = 0x0054;
        public const int CMD_ACTIVATE_LIGHT_SCENE = 0x0058;
        public const int CMD_DEACTIVATE_LIGHT_SCENE = 0x0059;

        public const int CMD_DRAPE_CLOSE = 0x0090;
        public const int CMD_DRAPE_OPEN = 0x0091;
        public const int CMD_DRAPE_TOGGLE = 0x0092;
        public const int CMD_DRAPE_STOP = 0x0093;

        public const int CMD_SET_DO_NOT_DISTURB = 0x0020;
        public const int CMD_SET_MAKE_UP_ROOM = 0x0021;

        private readonly ILogger logger;
        private readonly string inncomHost;
        private readonly int inncomPort;

        private Socket socket;

        private Task receiveTask;
        private CancellationTokenSource receiveCancellationToken;

        private Task sendTask;
        private CancellationTokenSource sendCancellationToken;

        private readonly BlockingCollection<Packet> packetQueue = new BlockingCollection<Packet>();

        private readonly System.Timers.Timer beaconTimer = new System.Timers.Timer();

        private bool connected;

        #endregion

        public event Action<int, byte, int, int, int, int, int> OnRoomUpdate;
        public event Action<int, byte, int, int, int> OnRoomAttributeUpdate;
        public event Action<int, int, int> OnLightUpdate;

        #region Constructor

        public InncomConnection(ILogger logger, string inncomHost, int inncomPort)
        {
            this.logger = logger;
            this.inncomHost = inncomHost;
            this.inncomPort = inncomPort;

            this.beaconTimer.Interval = BEACON_TICKS;
            this.beaconTimer.AutoReset = true;
            this.beaconTimer.Elapsed += beaconTimer_Elapsed;
        }

        private void beaconTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (this.connected)
            {
                Packet packet = new Packet(TAG_BEACON);
                this.packetQueue.Add(packet);
            }
        }

        #endregion

        #region Methods

        public bool OpenConnection()
        {
            this.connected = false;

            this.logger.Information("Connecting to InnCom server on {0}:{1}...", this.inncomHost, this.inncomPort);

            try
            {
                this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                this.socket.Connect(this.inncomHost, this.inncomPort);

                if (this.socket.Connected)
                {
                    this.connected = true;

                    this.receiveCancellationToken = new CancellationTokenSource();
                    this.receiveTask = Task.Factory.StartNew(ReceiveLoop, this.receiveCancellationToken.Token);

                    this.sendCancellationToken = new CancellationTokenSource();
                    this.sendTask = Task.Factory.StartNew(SendLoop, this.sendCancellationToken.Token);

                    this.SendStartup();
                }
            }
            catch (Exception ex)
            {
                this.logger.Error(ex, "[Inncom] Exception while connecting to InnCom server on '{0}:{1}'.", this.inncomHost, this.inncomPort);
            }

            return this.connected;
        }

        private void ReceiveLoop()
        {
            while (this.connected && !this.receiveTask.IsCanceled)
            {
                Packet packet = new Packet();
                if (packet.Receive(this.socket))
                {
                    this.HandlePacket(packet);   
                }
                else
                {
                    this.logger.Error("[Inncom] Error while receiving packet. Reconnecting...");
                    this.Reconnect();
                }
            }
        }

        private void Reconnect()
        {
            this.CloseConnection();
            if (!this.OpenConnection())
            {
                this.logger.Warning("[Inncom] Could not reconnect. Trying to reconnect again...");
                this.Reconnect();
            }
        }

        private void SendLoop()
        {
            while (this.connected && !this.sendTask.IsCanceled)
            {
                Packet packet = this.packetQueue.Take(this.sendCancellationToken.Token);
                if (packet != null)
                {
                    packet.SendToServer(this.socket);                   

                    this.beaconTimer.Stop();
                    this.beaconTimer.Start();
                }
            }
        }

        public void CloseConnection()
        {
            this.logger.Information("[Inncom] Closing Inncom connection...");

            this.beaconTimer.Stop();
            
            this.sendCancellationToken.Cancel();
            this.receiveCancellationToken.Cancel();

            this.connected = false;
        }

        public void ForceRoomSync(string roomId)
        {
            this.logger.Debug("[Inncom] Force room sync: " + roomId);
            int id;
            if (int.TryParse(roomId, out id))
            {
                this.SendRequestAttributeSynch(id);
                this.SendLightOuputStateSummaryRequest(id);
            }
        }

        public void RoomAdded(string roomId)
        {
            this.logger.Debug("[Inncom] Room added: " + roomId);
            int id;
            if (int.TryParse(roomId, out id))
            {
                this.SendLightOuputStateSummaryRequest(id);    
                this.SendRequestAttributeSynch(id);
            }
        }
        
        private System.Collections.Generic.List<int> shownTags = new System.Collections.Generic.List<int>();
        private void HandlePacket(Packet packet)
        {
            //ConsoleLogger.WriteToLog("Received packet with tag: " + packet.Tag.ToString("X4"));

            switch (packet.Tag)
            {
                case TAG_STARTUP:
                    this.HandleStartupComplete();
                    break;

                case TAG_SYNCHRONIZATION_END:
                    this.HandleSynchronizationComplete(packet);
                    break;

                case TAG_ASYNC_ROOM_UPDATE:
                    this.HandleAsyncRoomUpdate(packet);
                    break;

                case TAG_ATTRIBUTE_UPDATE:
                    this.HandleAttributeUpdate(packet);
                    break;

                case TAG_LIGHT_OUTPUT_STATE_SUMMARY:
                    this.HandleLightOutputStateSummary(packet);
                    break;

                default:
                    /*if (!shownTags.Contains(packet.Tag))
                    {
                        ConsoleLogger.WriteToLog("Received unknown packet with tag: " + packet.Tag.ToString("X4"));
                        shownTags.Add(packet.Tag);
                    }*/
                    break;
            }
        }

        private void HandleStartupComplete()
        {
            // Start the beacon (keepalive ping)
            this.beaconTimer.Start();

            // Synchronize all rooms
            this.logger.Information("[Inncom] Starting synchronization...");    
            this.SendSynchronizationRequest();
        }

        private void HandleSynchronizationComplete(Packet packet)
        {
            int synchronizationState = packet.ReadUShort();
            if (synchronizationState > 0)
            {
                this.logger.Information("[Inncom] Synchronization complete. Continue with next block..."); 

                // Synchronization not yet complete, request the next block
                this.SendSynchronizationRequest(synchronizationState);
            }
            else
            {
                this.logger.Information("[Inncom] Synchronization complete.");    
            }
        }

        private void HandleAsyncRoomUpdate(Packet packet)
        {
            int roomId = packet.ReadUShort();
            byte flags = packet.ReadByte();
            int code = packet.ReadUShort();
            int param1 = packet.ReadUShort();
            int param2 = packet.ReadUShort();
            int param3 = packet.ReadUShort();
            int param4 = packet.ReadUShort();
            long activeTimestamp = packet.ReadULong();
            long acknowledgeTimestamp = packet.ReadULong();
            string handledBy = packet.ReadString();

            this.logger.Debug("HandleAsyncRoomUpdate roomId: " + roomId + " code: " + code + " param1: " + param1 + " param2: " + param2 + " param3: " + param3 + " param4: " + param4 + " active: " + activeTimestamp + " acknowledged: " + acknowledgeTimestamp + " handled by: " + handledBy);

            if (this.OnRoomUpdate != null)
            {
                this.OnRoomUpdate.Invoke(roomId, flags, code, param1, param2, param3, param4);
            }
        }

        
        private void HandleAttributeUpdate(Packet packet)
        {
            int roomId = packet.ReadUShort();
            byte type = packet.ReadByte();
            int attributeId = packet.ReadUShort();
            int attributeValue = packet.ReadUShort();
            int auxiliaryValue = packet.ReadUShort();

            this.logger.Debug("HandleAttributeUpdate roomId: " + roomId + " type: " + type + " attributeId: " + attributeId.ToString("X4") + " attributeValue: " + attributeValue + " auxiliaryValue:" + auxiliaryValue);
            if (this.OnRoomAttributeUpdate != null)
            {
                this.OnRoomAttributeUpdate.Invoke(roomId, type, attributeId, attributeValue, auxiliaryValue);
            }
        }
        
        private void HandleLightOutputStateSummary(Packet packet)
        {
            int roomId = packet.ReadUShort();
            packet.ReadUShort(); // Reserved for future

            this.logger.Debug("[Inncom] [{0}] HandleLightOutputStateSummary");

            while (packet.ReadPos < packet.DataSize)
            {
                int circuitId = packet.ReadByte();
                int level = packet.ReadByte();

                if (this.OnLightUpdate != null)
                {
                    this.OnLightUpdate.Invoke(roomId, circuitId, level);
                }
            }
        }

        public void SendStartup()
        {
            Packet startupPacket = new Packet(TAG_STARTUP);
            this.packetQueue.Add(startupPacket);
        }

        private void SendSynchronizationRequest(int synchronizationState = 0xFFFF)
        {
            Packet synchronizePacket = new Packet(TAG_SYNCHRONIZATION_REQUEST);
            synchronizePacket.AddUShort(synchronizationState);
            this.packetQueue.Add(synchronizePacket);
        }

        private void SendRequestAttributeSynch(int roomId)
        {
            Packet packet = new Packet(TAG_ATTRIBUTE_SYNCH_REQUEST);
            packet.AddUShort(roomId);
            this.packetQueue.Add(packet);   
        }

        public void SendLightOuputStateSummaryRequest(int roomId)
        {
            Packet packet = new Packet(TAG_LIGHT_OUTPUT_STATE_SUMMARY_REQUEST);
            packet.AddUShort(roomId);
            packet.AddUShort(0);
            this.packetQueue.Add(packet);   
        }

        public void SendGuestRoomControl(string roomId, int commandCode, int arg1, int arg2, String argStr, int deviceAddress = 0)
        {
            int roomIdInt;
            if (int.TryParse(roomId, out roomIdInt))
            {
                this.logger.Debug("[Inncom] [{0}] GuestRoomControl - Command: 0x{1:X4}, DEC: 0x{2:X4}, Arg1: {3:X4}, Arg2: {4}, ArgStr: {5}", roomIdInt, commandCode, (byte)deviceAddress, arg1, arg2, argStr);

                Packet packet = new Packet(TAG_GUESTROOM_CONTROLS);
                packet.AddUShort(roomIdInt); // Room ID
                packet.AddByte((byte)deviceAddress); // Device address
                packet.AddULong(0); // Flags
                packet.AddUShort(commandCode); // Command code
                packet.AddUShort(arg1); // Argument 1
                packet.AddUShort(arg2); // Argument 2
                packet.AddString(argStr); // String argument
                this.packetQueue.Add(packet);    
            }   
        }

        public void SendFrame(string frame)
        {
            byte[] bytes = Enumerable.Range(0, frame.Length)
                                     .Where(x => x % 2 == 0)
                                     .Select(x => Convert.ToByte(frame.Substring(x, 2), 16))
                                     .ToArray();

            try
            {
                this.socket.Send(bytes, bytes.Length, SocketFlags.None);
            }
            catch (SocketException ex)
            {
                if ((SocketError)ex.ErrorCode != SocketError.Interrupted && (SocketError)ex.ErrorCode != SocketError.ConnectionReset && (SocketError)ex.ErrorCode != SocketError.ConnectionAborted)
                    this.logger.Error(ex, "[Inncom] Error while writing packet buffer");
            }
            catch (Exception ex)
            {
                this.logger.Error(ex, "[Inncom] Packet Send Exception");
            }
        }

        #endregion

    }
}
