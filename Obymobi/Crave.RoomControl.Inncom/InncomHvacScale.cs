﻿using Dionysos;

namespace Crave.RoomControl.Inncom
{
    public enum InncomHvacScale
    {
        /// <summary>
        /// Fahrenheit
        /// </summary>
        [StringValue("Fahrenheit")]
        Fahrenheit = 0,

        /// <summary>
        /// Celcius
        /// </summary>
        [StringValue("Celcius")]
        Celcius = 1,

        /// <summary>
        /// Toggle
        /// </summary>
        [StringValue("Toggle")]
        Toggle = 2
    }
}
