﻿using Dionysos;

namespace Crave.RoomControl.Inncom
{
    // The fan speed that is used for manual (fan fixed) mode
    public enum InncomFanMode
    {
        /// <summary>
        /// Off
        /// </summary>
        [StringValue("Off")]
        Off = 0,

        /// <summary>
        /// Low
        /// </summary>
        [StringValue("Low")]
        Low = 1,

        /// <summary>
        /// Medium
        /// </summary>
        [StringValue("Medium")]
        Medium = 2,

        /// <summary>
        /// High
        /// </summary>
        [StringValue("High")]
        High = 3
    }
}
