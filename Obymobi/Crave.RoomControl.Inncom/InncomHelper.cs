﻿using System;
using Crave.RoomControl.Enums;

namespace Crave.RoomControl.Inncom
{
    public static class InncomHelper
    {
        public static HvacScale ExtractScale(int temperatureData)
        {
            return (temperatureData & 0x8000) > 0 ? HvacScale.Celsius : HvacScale.Fahrenheit;
        }

        public static int ExtractTemperature(int temperatureData)
        {
            int temperature = (temperatureData & 0x3FFF);
            int negBit = temperatureData & 0x4000;
            if (negBit > 0)
            {
                temperature = 0 - temperature;
            }
            return temperature;
        }

        public static HvacMode ExtractHvacMode(int modeData)
        {
            int modeInt = (modeData & 0x03);
            if (modeInt == (int)InncomHvacMode.Off)
            {
                return HvacMode.Off;
            }
            return HvacMode.Auto;
        }

        public static int HvacModeToInt(HvacMode mode)
        {
            int modeInt = (int)InncomHvacMode.Auto;
            if (mode == HvacMode.Off)
            {
                modeInt = (int)InncomHvacMode.Off;
            }
            return modeInt;
        }

        public static HvacFanMode ExtractHvacFanMode(int modeData)
        {
            int modeInt = (modeData & 0x03);
            if (modeInt == (int) InncomHvacMode.Auto)
            {
                return HvacFanMode.Auto;
            }

            int fanModeInt = (modeData & 0x0C) >> 2;
            if (fanModeInt == (int)InncomFanMode.Low)
            {
                return HvacFanMode.Low;
            }
            if (fanModeInt == (int)InncomFanMode.Medium)
            {
                return HvacFanMode.Medium;
            }
            if (fanModeInt == (int)InncomFanMode.High)
            {
                return HvacFanMode.High;
            }
            return HvacFanMode.Off;
        }

        public static int HvacFanModeToInt(HvacFanMode fanMode)
        {
            int fanModeInt = (int)InncomFanMode.Off;
            if (fanMode == HvacFanMode.Low)
            {
                fanModeInt = (int)InncomFanMode.Low;
            }
            else if (fanMode == HvacFanMode.Medium)
            {
                fanModeInt = (int)InncomFanMode.Medium;
            }
            else if (fanMode == HvacFanMode.High)
            {
                fanModeInt = (int)InncomFanMode.High;
            }
            return fanModeInt;
        }

        public static int ExtractCompressedTemperature(int temperatureData, HvacScale scale)
        {
            double temperature = ((temperatureData & 0x3F00) >> 8) + 40;
            if (scale == HvacScale.Celsius)
            {
                // F -> C = (T(F) - 32) / (9 / 5)
                temperature = (temperature - 32.0) / 1.8;
            }
            return (int)(temperature * 10.0);
        }

        public static int CreateTemperatureData(HvacScale scale, decimal temperature)
        {
            int temperatureRounded = (int)Math.Ceiling(temperature) & 0x3FFF;
            int scaleBit = scale == HvacScale.Celsius ? 0x8000 : 0x0;
            int negBit = temperature < 0 ? 0x4000 : 0x0;
            return temperatureRounded | scaleBit | negBit;
        }
    }
}
