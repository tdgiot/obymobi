﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Crave.RoomControl.Enums;
using Crave.RoomControl.Models;
using Crave.RoomControl.Web;
using Dionysos;
using Serilog;

namespace Crave.RoomControl.Inncom
{
    public class InncomServer : IntegrationBase
    {
        #region Constants

        private const int ATTRIBUTE_ROOM_TEMP = 0x003A;
        private const int ATTRIBUTE_HVAC_UI = 0x1010;
        private const int ATTRIBUTE_FAN_SPEED = 0x1070;

        private const int ATTRIBUTE_DO_NOT_DISTURB = 0x1008;
        private const int ATTRIBUTE_MAKE_UP_ROOM = 0x100A;

        private const int EVENT_HVAC_UI = 0xE010;

        private const int ALARM_DO_NOT_DISTURB = 0xE002;
        private const int ALARM_MAKE_UP_ROOM = 0xE003;

        #endregion

        private readonly ILogger logger;
        private readonly InncomConnection inncom;

        public InncomServer(ILogger logger, string host, int port)
        {
            if (host.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException("host", "Inncom server host has not been configured");
            }
            if (port <= 0)
            {
                throw new ArgumentNullException("port", "Inncom server port has not been configured");
            }

            this.logger = logger;
            this.inncom = new InncomConnection(this.logger, host, port);

            this.inncom.OnRoomAttributeUpdate += HandleRoomAttributeUpdate;
            this.inncom.OnRoomUpdate += HandleAsyncRoomUpdate;
            this.inncom.OnLightUpdate += HandleLightUpdated;
        }

        public override bool Start()
        {
            logger.Information("[Inncom] Opening Inncom connection...");
            if (!this.inncom.OpenConnection())
            {
                logger.Error("[Inncom] Could not open Inncom connection.");
                return false;
            }

            return true;
        }

        public override void Stop()
        {
            logger.Information("[Inncom] ** STOPPING INNCOM SERVER **");
            this.inncom.CloseConnection();
        }

        public override void Synchronize()
        {
            this.inncom.SendStartup();
        }

        public override Task<Room> RequestRoomAsync(string roomId)
        {
            return Task.Factory.StartNew(() =>
                                         {
                                             bool newRoom = !RoomExists(roomId);
                                             if (newRoom)
                                             {
                                                 // Load the room
                                                 GetRoom(roomId);

                                                 // Wait to receive all data
                                                 Thread.Sleep(6000);
                                             }

                                             Room room = GetRoom(roomId);
                                             if (room != null)
                                             {
                                                 if (room.Lights == null || room.Lights.Count == 0)
                                                 {
                                                     // Sync
                                                     inncom.SendLightOuputStateSummaryRequest(int.Parse(roomId));

                                                     // Wait to receive all data
                                                     Thread.Sleep(6000);
                                                 }
                                             }

                                             return room;
                                         });
        }

        public override void HandleDebugRequest(WebRequest request)
        {
            string frameStr;
            if (request.Arguments.TryGetValue("frame", out frameStr))
            {
                if (inncom != null)
                {
                    inncom.SendFrame(frameStr);
                }
            }
        }

        public override void HandleCommandRequest(Room room, CommandType command, string parameter1, string parameter2, int deviceAddress)
        {
            double parameter1AsDouble;
            double.TryParse(parameter1, out parameter1AsDouble);

            int parameter2AsInt;
            int.TryParse(parameter2, out parameter2AsInt);

            switch (command)
            {
                case CommandType.HvacSetTargetTemperature:
                    this.SetHvacTargetTemperature(room, parameter1AsDouble);
                    break;
                case CommandType.HvacSetMode:
                    this.SetHvacMode(room, (HvacMode)(int)parameter1AsDouble);
                    break;
                case CommandType.HvacSetFanMode:
                    this.SetHvacFanMode(room, (HvacFanMode)(int)parameter1AsDouble);
                    break;
                case CommandType.HvacSetScale:
                    this.SetHvacScale(room, (HvacScale)(int)parameter1AsDouble);
                    break;
                case CommandType.BlindOpen:
                case CommandType.BlindClose:
                case CommandType.BlindToggle:
                case CommandType.BlindStop:
                    this.DrapeAction(room, command, (int)parameter1AsDouble);
                    break;
                case CommandType.LightSetLevel:
                    this.SetLightLevel(room, (int)parameter1AsDouble, parameter2AsInt, deviceAddress);
                    break;
                case CommandType.SceneActivate:
                    this.ActivateScene(room, (int)parameter1AsDouble);
                    break;
                case CommandType.SceneDeactivate:
                    this.DeactivateScene(room, (int)parameter1AsDouble);
                    break;
                case CommandType.DoNotDisturbSetState:
                    this.SetDoNotDisturbState(room, (int)parameter1AsDouble);
                    break;
                case CommandType.MakeUpRoomSetState:
                    this.SetMakeUpRoomState(room, (int)parameter1AsDouble);
                    break;
                case CommandType.ToggleLight:
                    this.ToggleLight(room, (int)parameter1AsDouble, deviceAddress);
                    break;
            }
        }

        #region Client command implementations

        private void SetHvacTargetTemperature(Room room, double temperatureInKelvin)
        {
            RoomHvac roomHvac = room.GetHvac(room.RoomId);
            if (roomHvac == null)
            {
                roomHvac = new RoomHvac(room.RoomId);
                room.Hvacs.Add(roomHvac);
            }

            double convertedTemperature = ConvertTemperature(temperatureInKelvin, HvacScale.Kelvin, HvacScale.Fahrenheit);
            int temperatureData = (int)Math.Round(convertedTemperature) * 10; //InncomHelper.CreateTemperatureData(roomHvac.HvacScale, convertedTemperature * 10);

            this.logger.Information("[{0}] SetHvacTargetTemperature - Input temp: {1}. Converted temp: {2}. TemperatureData: {3}", room.RoomId, temperatureInKelvin, convertedTemperature, temperatureData);

            this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_SET_HVAC_TEMPERATURE, temperatureData, 0, string.Empty);
        }

        private void SetHvacMode(Room room, HvacMode mode)
        {
            RoomHvac roomHvac = room.GetHvac(room.RoomId);
            if (roomHvac == null)
            {
                roomHvac = new RoomHvac(room.RoomId);
                room.Hvacs.Add(roomHvac);
            }
            
            int modeInt = (int)InncomHvacMode.Off;
            if (mode != HvacMode.Off)
            {
                if (roomHvac.HvacFanMode == HvacFanMode.Auto)
                {
                    modeInt = (int)InncomHvacMode.Auto;
                }
                else
                {
                    modeInt = (int)InncomHvacMode.FanFixed;
                }
            }
            this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_SET_HVAC_MODE, modeInt, 0, string.Empty);
        }

        private void SetHvacFanMode(Room room, HvacFanMode fanMode)
        {
            if (fanMode == HvacFanMode.Auto)
            {
                this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_SET_HVAC_MODE, (int)InncomHvacMode.Auto, 0, string.Empty);
            }
            else
            {
                this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_SET_HVAC_MODE, (int)InncomHvacMode.FanFixed, 0, string.Empty);

                int fanModeInt = (int)InncomFanMode.Off;

                if (fanMode == HvacFanMode.Low)
                {
                    fanModeInt = (int)InncomFanMode.Low;
                }
                else if (fanMode == HvacFanMode.Medium)
                {
                    fanModeInt = (int)InncomFanMode.Medium;
                }
                else if (fanMode == HvacFanMode.High)
                {
                    fanModeInt = (int)InncomFanMode.High;
                }

                this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_SET_HVAC_FAN_SPEED, fanModeInt, 0, string.Empty);
            }
        }

        private void SetHvacScale(Room room, HvacScale scale)
        {
            if (scale == HvacScale.Celsius)
            {
                this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_SET_HVAC_SCALE, (int)InncomHvacScale.Celcius, 0, string.Empty);
            }
            else
            {
                this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_SET_HVAC_SCALE, (int)InncomHvacScale.Fahrenheit, 0, string.Empty);
            }
        }

        private void DrapeAction(Room room, CommandType command, int drapeId)
        {
            if (command == CommandType.BlindOpen)
            {
                this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_DRAPE_OPEN, drapeId, 0, "");
            }
            else if (command == CommandType.BlindClose)
            {
                this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_DRAPE_CLOSE, drapeId, 0, "");
            }
            else if (command == CommandType.BlindStop)
            {
                this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_DRAPE_STOP, drapeId, 0, "");
            }
            else if (command == CommandType.BlindToggle)
            {
                this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_DRAPE_TOGGLE, drapeId, 0, "");
            }
        }

        private void ToggleLight(Room room, int lightId, int deviceAddress = 0)
        {
            int circuit = 0;
            if (lightId > 0)
            {
                if (lightId >= 8000)
                {
                    circuit = (int)new System.ComponentModel.Int32Converter().ConvertFromString("0x" + lightId);
                }
                else if (deviceAddress == 0)
                    circuit = 0x8000 | (lightId & 0xFF);
                else
                    circuit = 0x0000 | (lightId & 0xFF);
            }

            this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_SET_LIGHT_TOGGLE, circuit, 0, "", deviceAddress);
        }

        private void SetLightLevel(Room room, int lightId, int level, int deviceAddress = 0)
        {
            int circuit = 0;
            if (lightId > 0)
            {
                if (deviceAddress == 0)
                    circuit = 0x8000 | (lightId & 0xFF);
                else
                    circuit = 0x0000 | (lightId & 0xFF);
            }

            if (deviceAddress == 0)
            {
                this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_SET_LIGHT_LEVEL, circuit, (level << 8) & 0xFF00, "", deviceAddress);
            }
            else
            {
                this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_SET_LIGHT_TOGGLE, circuit, (level << 8) & 0xFF00, "", deviceAddress);
            }
        }

        private void ActivateScene(Room room, int sceneId)
        {
            this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_ACTIVATE_LIGHT_SCENE, 0x0000, (sceneId << 8) & 0xFF00, "");
        }

        private void DeactivateScene(Room room, int sceneId)
        {
            this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_DEACTIVATE_LIGHT_SCENE, 0x0000, (sceneId << 8) & 0xFF00, "");
        }

        private void SetDoNotDisturbState(Room room, int state)
        {
            this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_SET_DO_NOT_DISTURB, state, 0, "");
        }

        private void SetMakeUpRoomState(Room room, int state)
        {
            this.inncom.SendGuestRoomControl(room.RoomId, InncomConnection.CMD_SET_MAKE_UP_ROOM, state, 0, "");
        }

        #endregion

        protected override void RoomCreated(string roomId)
        {
            this.inncom.RoomAdded(roomId);
        }

        #region Handle Inncom Changes

        public void HandleLightUpdated(int roomId, int circuitId, int level)
        {
            Room room = this.GetRoom(roomId.ToString());
            if (room != null)
            {
                RoomLight light = room.GetLight(circuitId);
                if (light == null)
                {
                    light = new RoomLight(circuitId, level);
                    room.Lights.Add(light);

                    this.BroadcastAttributeUpdate(room.RoomId, circuitId.ToString(), AttributeType.LightLevel, circuitId, level);
                    this.logger.Information("[{0}] Light Updated - Circuit ID: {1} Level: {2}", room.RoomId, circuitId, level);
                }
                else if (light.Level != level)
                {
                    light.Level = level;

                    this.BroadcastAttributeUpdate(room.RoomId, circuitId.ToString(), AttributeType.LightLevel, circuitId, level);
                    this.logger.Information("[{0}] Light Updated - Circuit ID: {1} Level: {2}", room.RoomId, circuitId, level);
                }
            }
        }

        public void HandleRoomAttributeUpdate(int roomId, byte type, int attributeId, int attributeValue, int auxiliaryValue)
        {
            Room room = this.GetRoom(roomId.ToString());
            if (room != null)
            {
                switch (attributeId)
                {
                    case ATTRIBUTE_ROOM_TEMP:
                        this.HandleRoomTempUpdate(room, attributeValue);
                        break;
                    case ATTRIBUTE_HVAC_UI:
                        this.HandleHvacUiAttributeUpdate(room, attributeValue, auxiliaryValue);
                        break;
                    case ATTRIBUTE_FAN_SPEED:
                        this.HandleFanSpeedUpdate(room, attributeValue);
                        break;
                    case ATTRIBUTE_DO_NOT_DISTURB:
                        this.HandleDoNotDisturbUpdate(room, attributeValue);
                        break;
                    case ATTRIBUTE_MAKE_UP_ROOM:
                        this.HandleMakeUpRoomUpdate(room, attributeValue);
                        break;
                }
            }
        }

        public void HandleAsyncRoomUpdate(int roomId, byte flags, int code, int param1, int param2, int param3, int param4)
        {
            Room room = this.GetRoom(roomId.ToString());
            if (room != null)
            {
                switch (code)
                {
                    case EVENT_HVAC_UI:
                        this.HandleHvacUiUpdate(room, param1);
                        break;

                    case ALARM_DO_NOT_DISTURB:
                        this.HandleDoNotDisturbUpdate(room, param1);
                        break;

                    case ALARM_MAKE_UP_ROOM:
                        this.HandleMakeUpRoomUpdate(room, param1);
                        break;
                }
            }
        }

        private void HandleRoomTempUpdate(Room room, int temperatureData)
        {
            if (temperatureData == 0)
            {
                return;
            }

            RoomHvac roomHvac = room.GetHvac(room.RoomId);
            if (roomHvac == null)
            {
                roomHvac = new RoomHvac(room.RoomId);
                room.Hvacs.Add(roomHvac);
            }
            
            HvacScale newScale = InncomHelper.ExtractScale(temperatureData);
            if (roomHvac.HvacScale != newScale)
            {
                roomHvac.HvacScale = newScale;
                this.BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.HvacScale, (int)newScale);

                this.logger.Information("[{0}] Scale Updated - Scale: {1}", room.RoomId, newScale);
            }

            int newTemperature = InncomHelper.ExtractTemperature(temperatureData);
            if (newTemperature > 150)
            {
                newTemperature = newTemperature / 10;
            }

            double newTempConverted = ConvertTemperature(newTemperature, roomHvac.HvacScale, HvacScale.Kelvin);

            if (roomHvac.RoomTemperature != newTempConverted)
            {
                roomHvac.RoomTemperature = newTempConverted;
                this.BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.HvacTemperature, newTempConverted);

                this.logger.Information("[{0}] Room Temperature Updated - Temperature: {1} ({2})", room.RoomId, newTempConverted, newTemperature);
            }
        }

        private void HandleHvacUiAttributeUpdate(Room room, int temperatureData, int modeData)
        {
            HvacScale newScale = InncomHelper.ExtractScale(temperatureData);
            this.HandleHvacScaleUpdate(room, newScale);

            int newTemperature = InncomHelper.ExtractTemperature(temperatureData);
            this.HandleHvacTargetTemperatureUpdate(room, newTemperature);

            HvacMode newMode = InncomHelper.ExtractHvacMode(modeData);
            this.HandleHvacModeUpdate(room, newMode);

            HvacFanMode newFanMode = InncomHelper.ExtractHvacFanMode(modeData);
            this.HandleHvacFanModeUpdate(room, newFanMode);
        }

        private void HandleHvacUiUpdate(Room room, int data)
        {
            HvacMode newHvacMode = InncomHelper.ExtractHvacMode(data);
            this.HandleHvacModeUpdate(room, newHvacMode);

            HvacFanMode newFanMode = InncomHelper.ExtractHvacFanMode(data);
            this.HandleHvacFanModeUpdate(room, newFanMode);

            HvacScale newScale = (data & 0x4000) > 0 ? HvacScale.Celsius : HvacScale.Fahrenheit;
            this.HandleHvacScaleUpdate(room, newScale);

            int newTemperature = InncomHelper.ExtractCompressedTemperature(data, newScale);
            this.HandleHvacTargetTemperatureUpdate(room, newTemperature);
        }

        private void HandleHvacScaleUpdate(Room room, HvacScale newScale)
        {
            RoomHvac roomHvac = room.GetHvac(room.RoomId);
            if (roomHvac == null)
            {
                roomHvac = new RoomHvac(room.RoomId);
                room.Hvacs.Add(roomHvac);
            }

            if (roomHvac.HvacScale != newScale)
            {
                roomHvac.HvacScale = newScale;
                this.BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.HvacScale, (int)newScale);

                this.logger.Information("[{0}] Scale Updated - Scale: {1}", room.RoomId, newScale);
            }
        }

        private void HandleHvacTargetTemperatureUpdate(Room room, int newTemperature)
        {
            RoomHvac roomHvac = room.GetHvac(room.RoomId);
            if (roomHvac == null)
            {
                roomHvac = new RoomHvac(room.RoomId);
                room.Hvacs.Add(roomHvac);
            }

            if (newTemperature > 150)
            {
                newTemperature = newTemperature / 10;
            }

            double newTempConverted = ConvertTemperature(newTemperature, roomHvac.HvacScale, HvacScale.Kelvin);

            if (roomHvac.HvacTemperature != newTempConverted)
            {
                roomHvac.HvacTemperature = newTempConverted;
                this.BroadcastAttributeUpdate(room.RoomId, roomHvac.Id, AttributeType.HvacTargetTemperature, newTempConverted);

                this.logger.Information("[{0}] Target Temperature Updated - Temperature: {1} ({2})", room.RoomId, newTempConverted, newTemperature);
            }
        }

        private void HandleHvacModeUpdate(Room room, HvacMode newMode)
        {
            RoomHvac roomHvac = room.GetHvac(room.RoomId);
            if (roomHvac == null)
            {
                roomHvac = new RoomHvac(room.RoomId);
                room.Hvacs.Add(roomHvac);
            }

            if (roomHvac.HvacMode != newMode)
            {
                roomHvac.HvacMode = newMode;
                this.BroadcastAttributeUpdate(room.RoomId, roomHvac.Id, AttributeType.HvacMode, (int)newMode);

                this.logger.Information("[{0}] HVAC Mode Updated - Mode: {1}", room.RoomId, newMode);
            }
        }

        private void HandleHvacFanModeUpdate(Room room, HvacFanMode newFanMode)
        {
            RoomHvac roomHvac = room.GetHvac(room.RoomId);
            if (roomHvac == null)
            {
                roomHvac = new RoomHvac(room.RoomId);
                room.Hvacs.Add(roomHvac);
            }

            if (roomHvac.HvacFanMode != newFanMode)
            {
                roomHvac.HvacFanMode = newFanMode;
                this.BroadcastAttributeUpdate(room.RoomId, roomHvac.Id, AttributeType.HvacFanMode, (int)newFanMode);

                this.logger.Information("[{0}] HVAC Fan Mode Updated - Mode: {1}", room.RoomId, newFanMode);
            }
        }

        private void HandleFanSpeedUpdate(Room room, int fanSpeed)
        {
            RoomHvac roomHvac = room.GetHvac(room.RoomId);
            if (roomHvac == null)
            {
                roomHvac = new RoomHvac(room.RoomId);
                room.Hvacs.Add(roomHvac);
            }

            // Create a mode data type so we can use the helper
            int modeData = (fanSpeed << 2);

            HvacFanMode newFanMode = InncomHelper.ExtractHvacFanMode(modeData);
            if (roomHvac.HvacFanMode != newFanMode)
            {
                roomHvac.HvacFanMode = newFanMode;
                this.BroadcastAttributeUpdate(room.RoomId, roomHvac.Id, AttributeType.HvacFanMode, (int)newFanMode);

                this.logger.Information("[{0}] HVAC Fan Mode Updated - Mode: {1}", room.RoomId, newFanMode);
            }
        }

        private void HandleDoNotDisturbUpdate(Room room, int state)
        {
            bool newState = state == 1;
            if (room.DoNotDisturb != newState)
            {
                room.DoNotDisturb = newState;
                this.BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.DoNotDisturbState, state);

                this.logger.Information("[{0}] Do Not Disturb Updated - State: {1}", room.RoomId, state);
            }
        }

        private void HandleMakeUpRoomUpdate(Room room, int state)
        {
            bool newState = state == 1;
            if (room.MakeUpRoom != newState)
            {
                room.MakeUpRoom = newState;
                this.BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.MakeUpRoomState, state);

                this.logger.Information("[{0}] Make Up Room Updated - State: {1}", room.RoomId, state);
            }
        }

        #endregion

        private static double ConvertTemperature(double value, HvacScale sourceScale, HvacScale targetScale)
        {
            double outputValue = value;
            if (sourceScale == HvacScale.Celsius)
            {
                if (targetScale == HvacScale.Kelvin)
                {
                    outputValue = value + 273.15;
                }
                else if (targetScale == HvacScale.Fahrenheit)
                {
                    outputValue = (value * ((double)9 / 5)) + 32;
                }
            }
            else if (sourceScale == HvacScale.Fahrenheit)
            {
                if (targetScale == HvacScale.Kelvin)
                {
                    outputValue = (value - 32) * ((double)5 / 9) + (double)273.15;
                }
                else if (targetScale == HvacScale.Celsius)
                {
                    outputValue = (value - 32) * ((double)5 / 9);
                }
            }
            else if (sourceScale == HvacScale.Kelvin)
            {
                if (targetScale == HvacScale.Fahrenheit)
                {
                    outputValue = (value - (double)273.15) * 9 / 5 + 32;
                }
                else if (targetScale == HvacScale.Celsius)
                {
                    outputValue = value - (double)273.15;
                }
            }

            return outputValue;
        }        
    }
}
