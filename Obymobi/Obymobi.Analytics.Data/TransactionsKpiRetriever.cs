﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Data.Linq;
using System.Diagnostics;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using System.Globalization;
using System.Linq.Expressions;
using System.Data.SqlTypes;
using Newtonsoft.Json;

namespace Obymobi.Analytics.Data
{    
    public class TransactionsKpiRetriever
    {
        bool runConsistencyChecks = false;

        public TransactionsKpiRetriever(bool runConsistencyChecks)
        {
            this.runConsistencyChecks = runConsistencyChecks;
        }

        public List<IntervalResult<decimal>> GetRevenue(Filter filter, ReportingInterval interval)
        {            
            // Get the periods
            List<Tuple<DateTime, DateTime>> periods = IntervalCreator.GetIntervals(filter, interval);
            var toReturn = new List<IntervalResult<decimal>>();

            foreach (var period in periods)
            {
                var periodFilter = filter.Clone();
                periodFilter.FromUtc = period.Item1;
                periodFilter.TillUtc = period.Item2;
                toReturn.Add(new IntervalResult<decimal>(period.Item1, period.Item2, GetRevenue(periodFilter)));
            }

            return toReturn;

            #region Experimenting
            //LinqMetaData metaData = new LinqMetaData();
            //var predicate = filter.GetOrderitemPredicate();
            //var result = metaData.Orderitem
            //    .Where(predicate)
            //    .GroupBy(oi => oi.OrderEntity.CreatedUTC.Value.Date)
            //    .Select(group => new { intervalStart = group.Key, revenue = group.Count() }).ToList();
            //Debug.WriteLine("Result");

            //var result2 = metaData.Orderitem
            //    .Where(predicate)
            //    .GroupBy(oi => oi.OrderEntity.CreatedUTC.Value.Date)
            //    .Select(group => new { intervalStart = group.Key, revenue = group.Sum(oi => oi.Quantity) }).ToList();
            //Debug.WriteLine("Result2");

            //var result3 = metaData.Orderitem
            //    .Where(predicate)
            //    .GroupBy(oi => oi.OrderEntity.CreatedUTC.Value.Date)
            //    .Select(group => new { intervalStart = group.Key, revenue = group.Sum(oi => oi.Quantity) }).ToList();
            //Debug.WriteLine("Result3");

            //var result4 = metaData.Orderitem
            //    .Where(oi => oi.OrderEntity.OrderId > 600215 && oi.OrderEntity.CompanyId == 343)
            //    .GroupBy(oi => oi.OrderEntity.CreatedUTC.Value.Date)
            //    .Select(group => new { intervalStart = group.Key, revenue = group.Sum(oi => (oi.ProductPriceIn)) }).ToList();
            //Debug.WriteLine("Result4");

            //try
            //{
            //    var result4b = metaData.Orderitem
            //        .Where(oi => oi.OrderEntity.OrderId > 600215 && oi.OrderEntity.CompanyId == 343)
            //        .GroupBy(oi => oi.OrderEntity.CreatedUTC.Value.Date)
            //        .Select(group => new { intervalStart = group.Key, revenue = group.Sum(oi => (oi.ProductPriceIn * oi.Quantity)) }).ToList();
            //    Debug.WriteLine("Result4b");
            //}
            //catch (Exception ex)
            //{
            //    Debug.WriteLine(ex.Message);
            //}

            //try
            //{
            //    var result4c = metaData.Order
            //        .Where(o => o.OrderId > 600215 && o.CompanyId == 343)
            //        .GroupBy(o => o.CreatedUTC.Value.Date)
            //        .Select(group => new { intervalStart = group.Key, revenue = group.Sum(o => o.OrderitemCollection.Sum(oi => (oi.ProductPriceIn * oi.Quantity))) }).ToList();
            //    Debug.WriteLine("Result4c");
            //}
            //catch (Exception ex)
            //{
            //    Debug.WriteLine(ex.Message);
            //}

            //var result5 = metaData.Orderitem
            //    .Where(predicate)
            //    .GroupBy(oi => oi.OrderEntity.CreatedUTC.Value.Date)
            //    .Select(group => new { intervalStart = group.Key, revenue = group.Sum(oi => (oi.ProductPriceIn * oi.Quantity)) }).ToList();
            //Debug.WriteLine("Result5");

            //var result6 = metaData.Orderitem
            //    .Where(predicate)
            //    .GroupBy(oi => oi.OrderEntity.CreatedUTC.Value.Date)
            //    .Select(group => new { intervalStart = group.Key, revenue = group.Sum(oi => oi.OrderitemAlterationitemCollection.Sum(oiai => oiai.OrderitemAlterationitemId)) }).ToList();
            //Debug.WriteLine("Result6");

            //var result7 = metaData.Orderitem
            //    .Where(predicate)
            //    .GroupBy(oi => oi.OrderEntity.CreatedUTC.Value.Date)
            //    .Select(group => new { intervalStart = group.Key, revenue = group.Sum(oi => oi.OrderitemAlterationitemCollection.Sum(oiai => oiai.AlterationoptionPriceIn)) }).ToList();
            //Debug.WriteLine("Result7");

            //var result8 = metaData.Orderitem
            //    .Where(predicate)
            //    .GroupBy(oi => oi.OrderEntity.CreatedUTC.Value.Date)
            //    .Select(group => new { intervalStart = group.Key, revenue = group.Sum(oi => oi.Quantity * (oi.ProductPriceIn + oi.OrderitemAlterationitemCollection.Sum(oiai => oiai.AlterationoptionPriceIn))) }).ToList();

            //Debug.WriteLine("Result8");

            //var result9 = metaData.Orderitem
            //    .Where(predicate)
            //    .GroupBy(oi => oi.OrderEntity.CreatedUTC.Value.Date)
            //    .Select(group => new { intervalStart = group.Key, revenue = group.Sum(oi => oi.Quantity * (oi.ProductPriceIn + (decimal?)oi.OrderitemAlterationitemCollection.Sum(oiai => oiai.AlterationoptionPriceIn) ?? 0)) })
            //    .ToList();
            //Debug.WriteLine("Result9");

            ////.Select(group => new { intervalStart = group.Key, revenue = group.Sum(oi => (oi.ProductPriceIn * oi.Quantity) + (((decimal?)(oi.OrderitemAlterationitemCollection.Sum(oiai => oiai.AlterationoptionPriceIn)) ?? 0) * oi.Quantity)) });

            //List<IntervalResult<decimal>> results = new List<IntervalResult<decimal>>();
            //foreach (var group in result9)
            //    results.Add(new IntervalResult<decimal>(group.intervalStart, group.intervalStart, group.revenue));

            //return null;
            #endregion 
        }

        public void Test(Filter filter)
        {
            LinqMetaData metaData = new LinqMetaData();
            var predicate = filter.GetOrderPredicate();

            var query2 = metaData.Order.Where(predicate).Select(o =>
                                   new
                                   {
                                       o.DeliverypointEntity.DeliverypointgroupId,
                                       o.DeliverypointgroupName,
                                       Revenue = (decimal)o.OrderitemCollection.Sum(oi => (oi.ProductPriceIn * oi.Quantity) + (oi.OrderitemAlterationitemCollection.Sum(oiai => oiai.AlterationoptionPriceIn) * oi.Quantity)),
                                       o.Type,
                                       o.MobileOrder
                                   });

            var result2 = query2.ToList();


            var query3 = metaData.Order.Where(predicate).Select(o =>
                                   new
                                   {
                                       o.DeliverypointEntity.DeliverypointgroupId,
                                       o.DeliverypointgroupName,
                                       Revenue = (decimal)o.OrderitemCollection.Sum(oi => (oi.ProductPriceIn * oi.Quantity) + (oi.OrderitemAlterationitemCollection.Sum(oiai => oiai.AlterationoptionPriceIn) * oi.Quantity)),
                                       o.Type,
                                       o.MobileOrder
                                   })
                               .GroupBy(q => new { q.DeliverypointgroupId, q.DeliverypointgroupName })
                               .Select(q =>
                                   new
                                   {
                                       DeliverypointgroupId = q.Key.DeliverypointgroupId,
                                       DeliverypointgroupName = q.Key.DeliverypointgroupName,
                                       OrderCount = q.Count()
                                   });


            var result3 = query3.ToList();

            var query3b = metaData.Order.Where(predicate).Select(o =>
                                   new
                                   {
                                       o.DeliverypointEntity.DeliverypointgroupId,
                                       o.DeliverypointgroupName,
                                       Revenue = (decimal)o.OrderitemCollection.Sum(oi => (oi.ProductPriceIn * oi.Quantity) + (oi.OrderitemAlterationitemCollection.Sum(oiai => oiai.AlterationoptionPriceIn) * oi.Quantity)),
                                       Standard = o.Type == (int)OrderType.Standard ? 1 : 0,
                                       RequestForService = o.Type == (int)OrderType.RequestForService ? 1 : 0,
                                       o.MobileOrder
                                   })
                               .GroupBy(q => new { q.DeliverypointgroupId, q.DeliverypointgroupName })
                               .Select(q =>
                                   new
                                   {
                                       DeliverypointgroupId = q.Key.DeliverypointgroupId,
                                       DeliverypointgroupName = q.Key.DeliverypointgroupName,
                                       StandardCount = q.Sum(c => c.Standard),
                                       ServiceCount = q.Sum(c => c.RequestForService)
                                   });


            var result3b = query3b.ToList();

            var query4 = metaData.Order.Where(predicate).Select(o =>
                                   new
                                   {
                                       o.DeliverypointEntity.DeliverypointgroupId,
                                       o.DeliverypointgroupName,
                                       Revenue = (decimal)o.OrderitemCollection.Sum(oi => (oi.ProductPriceIn * oi.Quantity) + (oi.OrderitemAlterationitemCollection.Sum(oiai => oiai.AlterationoptionPriceIn) * oi.Quantity)),
                                       o.Type,
                                       o.MobileOrder
                                   })
                               .GroupBy(q => new { q.DeliverypointgroupId, q.DeliverypointgroupName })
                               .Select(q =>
                                   new
                                   {
                                       DeliverypointgroupId = q.Key.DeliverypointgroupId,
                                       DeliverypointgroupName = q.Key.DeliverypointgroupName,
                                       InRoomTabletOrderCount = q.Count(c => c.Type == (int)OrderType.Standard && !c.MobileOrder),
                                       InRoomTabletServiceRequestCount = q.Count(c => c.Type == (int)OrderType.RequestForService && !c.MobileOrder)
                                   });

            var result4 = query4.ToList();


        }


        // Convenience method to be able to render the Start (or interval Dates for 'Daily') to the output
        public DateTime GetStartDate(Filter filter)
        {
            return filter.FromUtc;
        }

        public decimal GetRevenue(Filter filter)
        {
            LinqMetaData metaData = new LinqMetaData();
            var predicate = filter.GetOrderitemPredicate();
            var result = metaData.Orderitem.Where(predicate).Sum(oi => (oi.ProductPriceIn * oi.Quantity) + (((decimal?)(oi.OrderitemAlterationitemCollection.Sum(oiai => oiai.AlterationoptionPriceIn)) ?? 0) * oi.Quantity));

            if (this.runConsistencyChecks)
                this.GetRevenueConsistencyCheck(filter, result);

            return result;
        }

        public void GetRevenueConsistencyCheck(Filter filter, decimal linqResult)
        {
            string query = @"
            SELECT SUM((ProductPriceIn + ISNULL(AlterationsPriceIn, 0)) * Quantity) AS TotalRevenue
            FROM
            (
                SELECT
                    Quantity,
                    ProductPriceIn,
                    (SELECT sum(OrderitemAlterationitem.AlterationoptionPriceIn) FROM OrderitemAlterationitem WHERE OrderitemAlterationitem.OrderitemId = Orderitem.OrderitemId) AS AlterationsPriceIn
                FROM Orderitem
                LEFT JOIN [Order] ON Orderitem.OrderId = [Order].OrderId
                LEFT JOIN [Deliverypoint] on [Order].DeliverypointId = [Deliverypoint].DeliverypointId
                WHERE
                    [[WHERE]]
            ) oi
            ";

            query = query.Replace("[[WHERE]]", filter.GetWhereClause());
            this.ConsistencyCheck("Revenue", linqResult, query);
        }

        public List<IntervalResult<int>> GetOrderedQuantity(Filter filter, ReportingInterval interval)
        {
            // Get the periods
            List<Tuple<DateTime, DateTime>> periods = IntervalCreator.GetIntervals(filter, interval);
            var toReturn = new List<IntervalResult<int>>();

            foreach (var period in periods)
            {
                var periodFilter = filter.Clone();
                periodFilter.FromUtc = period.Item1;
                periodFilter.TillUtc = period.Item2;
                toReturn.Add(new IntervalResult<int>(period.Item1, period.Item2, GetOrderedQuantity(periodFilter)));
            }

            return toReturn;
        }

        public int GetOrderedQuantity(Filter filter)
        {
            LinqMetaData metaData = new LinqMetaData();
            var predicate = filter.GetOrderitemPredicate();
            var result = metaData.Orderitem.Where(predicate).Sum(oi => oi.Quantity);

            if (this.runConsistencyChecks)
                this.GetOrderedQuantityConsistencyCheck(filter, result);

            return result;
        }

        private void GetOrderedQuantityConsistencyCheck(Filter filter, int linqResult)
        {
            string query = @"
                            select SUM(Orderitem.Quantity)
                            from
                                Orderitem
                            left join
                                [Order] on Orderitem.OrderId = [Order].OrderId
                                LEFT JOIN [Deliverypoint] on [Order].DeliverypointId = [Deliverypoint].DeliverypointId
                            where [[WHERE]]";
            query = query.Replace("[[WHERE]]", filter.GetWhereClause());

            this.ConsistencyCheck("OrderedQuantity", linqResult, query);
        }

        public List<IntervalResult<int>> GetOrderCount(Filter filter, ReportingInterval interval)
        {
            List<Tuple<DateTime, DateTime>> periods = IntervalCreator.GetIntervals(filter, interval);
            var toReturn = new List<IntervalResult<int>>();

            foreach (var period in periods)
            {
                var periodFilter = filter.Clone();
                periodFilter.FromUtc = period.Item1;
                periodFilter.TillUtc = period.Item2;
                toReturn.Add(new IntervalResult<int>(period.Item1, period.Item2, GetOrderCount(periodFilter)));
            }

            return toReturn;
        }

        public int GetOrderCount(Filter filter)
        {
            LinqMetaData metaData = new LinqMetaData();
            var predicate = filter.GetOrderitemPredicate();
            var result = metaData.Orderitem.Where(predicate).Select(oi => oi.OrderId).Distinct().Count();            

            if (this.runConsistencyChecks)
                this.GetOrderCountConsistencyCheck(filter, result);

            return result;          
        }

        private void GetOrderCountConsistencyCheck(Filter filter, int linqResult)
        {
            string query = @"
                            select COUNT(DISTINCT Orderitem.OrderId)
                            from
                                Orderitem
                            left join
                                [Order] on Orderitem.OrderId = [Order].OrderId
                                LEFT JOIN [Deliverypoint] on [Order].DeliverypointId = [Deliverypoint].DeliverypointId
                            where [[WHERE]]";
            query = query.Replace("[[WHERE]]", filter.GetWhereClause());
            
            this.ConsistencyCheck("OrderCount", linqResult, query);
        }

        public int GetOrdersWithNotesCount(Filter filter)
        {
            // Not the most optimal in terms of database data transfer, but it's consistent for certain :)
            return GetOrdersWithNotes(filter).Count;
        }        

        public List<OrderWithNotes> GetOrdersWithNotes(Filter filter)
        {
            LinqMetaData metaData = new LinqMetaData();
            var predicate = filter.GetOrderPredicate();

            // Code below is mainly to transform result to the objects.
            var selectQuery = metaData.Order.Where(predicate).Select(o =>
                new
                {
                    OrderId = o.OrderId,
                    UtcCreated = o.CreatedUTC,
                    DeliverypointNumber = o.DeliverypointNumber,
                    DeliverypointName = o.DeliverypointName,
                    DeliverypointgroupName = o.DeliverypointgroupName,
                    Notes = o.Notes
                });

            selectQuery = selectQuery.OrderBy(x => x.UtcCreated);

            if(filter.NoOfResults > 0)
                selectQuery = selectQuery.Take(filter.NoOfResults);

            var result = selectQuery.Select(x => new OrderWithNotes {   OrderId = x.OrderId,
                                                                        DeliverypointgroupName = x.DeliverypointgroupName,
                                                                        DeliverypointName = x.DeliverypointName,
                                                                        DeliverypointNumber = x.DeliverypointNumber,
                                                                        Notes = x.Notes,
                                                                        UtcCreated = x.UtcCreated ?? (DateTime)SqlDateTime.MinValue });

            List<OrderWithNotes> toReturn = result.ToList();
            if (this.runConsistencyChecks)
            {
                this.GetOrdersWithNotesConsistencyCheck(filter, toReturn);
            }

            return toReturn;
        }

        public void GetOrdersWithNotesConsistencyCheck(Filter filter, List<OrderWithNotes> linqResultTable)
        {
            string query = @"
                            SELECT 
                                   [[TOP]]                                   
                                   [Order].OrderId, 
                                   [Order].CreatedUTC,                                    
                                   [Order].DeliverypointNumber,
	                               [Order].DeliverypointName,
	                               [Order].DeliverypointgroupName,
	                               [Order].Notes
                            FROM   [Order] 
                                LEFT JOIN [Deliverypoint] on [Order].DeliverypointId = [Deliverypoint].DeliverypointId
                            WHERE  [[WHERE]]
                            ORDER  BY [Order].[Created]";

            query = query.Replace("[[WHERE]]", filter.GetWhereClause());

            if(filter.NoOfResults > 0)
                query = query.Replace("[[TOP]]", "TOP({0})".FormatSafe(filter.NoOfResults.ToString()));
            else
                query = query.Replace("[[TOP]]", string.Empty);

            var tsqlResultTable = executeSqlQuery(query);
            var topListSql = new StringBuilder();
            var topListLinq = new StringBuilder();

            for (int i = 0; i < tsqlResultTable.Rows.Count; i++)
            {
                topListSql.AppendFormatLine("{0},{1},{2},{3},{4},{5}", tsqlResultTable.Rows[i][0], tsqlResultTable.Rows[i][1], tsqlResultTable.Rows[i][2], tsqlResultTable.Rows[i][3], tsqlResultTable.Rows[i][4], tsqlResultTable.Rows[i][5]);
                topListLinq.AppendFormatLine("{0},{1},{2},{3},{4},{5}", linqResultTable[i].OrderId, linqResultTable[i].UtcCreated, linqResultTable[i].DeliverypointNumber, linqResultTable[i].DeliverypointName, linqResultTable[i].DeliverypointgroupName, linqResultTable[i].Notes);
            }

            if (tsqlResultTable.Rows.Count != linqResultTable.Count ||
                !topListLinq.ToString().Equals(topListSql.ToString()))
                throw new Exception("Top lists by quantity are not equal\r\nSLQ: {0}\r\nLINQ: {1}".FormatSafe(topListSql.ToString(), topListLinq.ToString()));
        }
        

        public class OrderWithNotes
        {
            public int OrderId;
            public DateTime UtcCreated;            
            public string DeliverypointNumber;
            public string DeliverypointName;
            public string DeliverypointgroupName;
            public string Notes;
        }

        public List<OrderitemAlterationItem> GetOrderedAlterations(Filter filter)
        {
            LinqMetaData metaData = new LinqMetaData();
            var predicate = filter.GetOrderitemAlterationitemPredicate();

            // Code below is mainly to transform result to the objects.
            var selectQuery = metaData.OrderitemAlterationitem.Where(predicate).Select(oiai =>
                new
                {
                    AlterationName = oiai.AlterationName,
                    AlterationoptionName = oiai.AlterationoptionName,
                    DeliverypointNumber = oiai.OrderitemEntity.OrderEntity.DeliverypointNumber,
                    DeliverypointName = oiai.OrderitemEntity.OrderEntity.DeliverypointName,
                    DeliverypointgroupName = oiai.OrderitemEntity.OrderEntity.DeliverypointgroupName,
                    CreatedUTC = oiai.CreatedUTC,
                    OrderId = oiai.OrderitemEntity.OrderId,
                    OrderitemAlterationitemId = oiai.OrderitemAlterationitemId
                });

            selectQuery = selectQuery.OrderByDescending(x => x.OrderitemAlterationitemId);

            selectQuery = selectQuery.Take(filter.NoOfResults);

            var result = selectQuery.Select(x => new OrderitemAlterationItem
            {
                AlterationName = x.AlterationName,
                AlterationoptionName = x.AlterationoptionName,
                DeliverypointNumber = x.DeliverypointNumber,
                DeliverypointName = x.DeliverypointName,
                DeliverypointgroupName = x.DeliverypointgroupName,
                CreatedUTC = x.CreatedUTC,
                OrderId = x.OrderId,
                OrderitemAlterationitemId = x.OrderitemAlterationitemId
            });

            List<OrderitemAlterationItem> toReturn = result.ToList();
            if (this.runConsistencyChecks)
            {
                this.GetOrderedAlterationsConsistencyCheck(filter, toReturn);
            }

            return toReturn;
        }

        public void GetOrderedAlterationsConsistencyCheck(Filter filter, List<OrderitemAlterationItem> linqResultTable)
        {
            string query = @"
                            SELECT 
                                TOP([[TOP]])
	                            [OrderitemAlterationitem].AlterationName, 
                                [OrderitemAlterationitem].AlterationoptionName,                             
	                            [Order].DeliverypointNumber,    
                                [Order].DeliverypointName,	
	                            [Order].DeliverypointgroupName,
	                            [OrderitemAlterationitem].CreatedUTC,
	                            [Order].OrderId,
	                            [OrderitemAlterationitem].OrderitemAlterationitemId
                            FROM 
	                            OrderitemAlterationitem
	                            LEFT JOIN Alterationitem on OrderitemAlterationitem.AlterationitemId = Alterationitem.AlterationitemId		
	                            LEFT JOIN Orderitem on Orderitem.OrderitemId = OrderitemAlterationitem.OrderitemId
	                            LEFT JOIN [Order] on [Order].OrderId = Orderitem.OrderId
                                LEFT JOIN [Deliverypoint] on [Order].DeliverypointId = [Deliverypoint].DeliverypointId
                            WHERE  [[WHERE]]
                            ORDER  BY [OrderitemAlterationitem].[OrderitemAlterationitemId] DESC";

            query = query.Replace("[[WHERE]]", filter.GetWhereClause());
            query = query.Replace("[[TOP]]", filter.NoOfResults.ToString());

            var tsqlResultTable = executeSqlQuery(query);
            var topListSql = new StringBuilder();
            var topListLinq = new StringBuilder();

            for (int i = 0; i < tsqlResultTable.Rows.Count; i++)
            {
                topListSql.AppendFormatLine("{0},{1},{2},{3},{4},{5},{6}", tsqlResultTable.Rows[i][0], tsqlResultTable.Rows[i][1], tsqlResultTable.Rows[i][2], tsqlResultTable.Rows[i][3], tsqlResultTable.Rows[i][4], tsqlResultTable.Rows[i][5], tsqlResultTable.Rows[i][6]);
                topListLinq.AppendFormatLine("{0},{1},{2},{3},{4},{5},{6}", 
                    linqResultTable[i].AlterationName, linqResultTable[i].AlterationoptionName, linqResultTable[i].DeliverypointNumber, 
                    linqResultTable[i].DeliverypointName, linqResultTable[i].DeliverypointgroupName, linqResultTable[i].CreatedUTC, linqResultTable[i].OrderId, linqResultTable[i].OrderitemAlterationitemId);
            }

            if (!topListLinq.ToString().Equals(topListSql.ToString()))
                throw new Exception("Ordered Alterations are not equal\r\nSLQ: {0}\r\nLINQ: {1}".FormatSafe(topListSql.ToString(), topListLinq.ToString()));
        }

        public class OrderitemAlterationItem
        {
            public string AlterationName;
            public string AlterationoptionName;
            public string DeliverypointNumber;
            public string DeliverypointName;
            public string DeliverypointgroupName;
            public DateTime? CreatedUTC;
            public int OrderId;
            public int OrderitemAlterationitemId;            
        }        

        public List<TopXProduct> GetTopXProductsByRevenue(Filter filter)
        {
            return GetTopXProducts(filter, TopXProductOrder.Revenue);
        }

        public List<TopXProduct> GetTopXProductsByQuantity(Filter filter)
        {
            return GetTopXProducts(filter, TopXProductOrder.Quantity);
        }

        public List<TopXProduct> GetTopXProducts(Filter filter, TopXProductOrder ordering)
        {
            LinqMetaData metaData = new LinqMetaData();
            var predicate = filter.GetOrderitemPredicate();

            // Code below is mainly to transform result to the objects.
            var selectQuery = metaData.Orderitem.Where(predicate).Select(oi =>
                new
                {
                    ProductId = oi.ProductId,
                    ProductName = oi.ProductEntity.Name,
                    Quantity = oi.Quantity,
                    Revenue = (oi.ProductPriceIn + ((decimal?)(oi.OrderitemAlterationitemCollection.Sum(oiai => oiai.AlterationoptionPriceIn)) ?? 0)) * oi.Quantity
                })
            .GroupBy(x => new { x.ProductId, x.ProductName })
            .Select(x =>
                new
                {
                    ProductId = x.First().ProductId,
                    ProductName = x.First().ProductName,
                    Quantity = x.Sum(y => y.Quantity),
                    Revenue = x.Sum(y => y.Revenue)
                }
            );

            if (ordering == TopXProductOrder.Quantity)
                selectQuery = selectQuery.OrderByDescending(x => new { x.Quantity, x.ProductId });
            else if (ordering == TopXProductOrder.Revenue)
                selectQuery = selectQuery.OrderByDescending(x => new { x.Revenue, x.ProductId });

            selectQuery = selectQuery.Take(filter.NoOfResults);
            
            var result = selectQuery.Select(x => new TopXProduct { ProductId = x.ProductId, Name = x.ProductName, Quantity = x.Quantity, Revenue = x.Revenue }).ToList();            

            if (this.runConsistencyChecks)
                this.GetTopXProductsConsistencyCheck(filter, result, ordering);

            return result;
        }

        public void GetTopXProductsConsistencyCheck(Filter filter, List<TopXProduct> linqResultTable, TopXProductOrder ordering)
        {
            string query = @"
            SELECT	
                TOP([[TOP]])
	            oi.ProductId,
	            oi.Name, 
	            SUM(oi.Quantity) as Quantity, 
	            SUM((ProductPriceIn + ISNULL(AlterationsPriceIn, 0)) * Quantity) AS Revenue
            FROM
            (
                SELECT
                    OrderItem.ProductId,
		            Product.Name,
		            Quantity,
                    ProductPriceIn,
                    (SELECT sum(OrderitemAlterationitem.AlterationoptionPriceIn) FROM OrderitemAlterationitem WHERE OrderitemAlterationitem.OrderitemId = Orderitem.OrderitemId) AS AlterationsPriceIn
                FROM Orderitem
                LEFT JOIN [Order] ON Orderitem.OrderId = [Order].OrderId
                LEFT JOIN [Deliverypoint] on [Order].DeliverypointId = [Deliverypoint].DeliverypointId
	            LEFT JOIN [Product] ON Orderitem.ProductId = [Product].ProductId
                WHERE
                    [[WHERE]]
            ) oi
            GROUP BY oi.ProductId, oi.Name
            ORDER BY [[ORDERBY]] DESC, oi.ProductId DESC
            ";

            query = query.Replace("[[WHERE]]", filter.GetWhereClause());
            query = query.Replace("[[TOP]]", filter.NoOfResults.ToString());
            if (ordering == TopXProductOrder.Quantity)
                query = query.Replace("[[ORDERBY]]", "Quantity");
            else if (ordering == TopXProductOrder.Revenue)
                query = query.Replace("[[ORDERBY]]", "Revenue");
            else
                throw new NotImplementedException(ordering.ToString());

            var tsqlResultTable = executeSqlQuery(query);
            var topListSql = new StringBuilder();
            var topListLinq = new StringBuilder();

            for (int i = 0; i < tsqlResultTable.Rows.Count; i++)
            {
                topListSql.AppendFormatLine("{0},{1},{2},{3}", tsqlResultTable.Rows[i][0], tsqlResultTable.Rows[i][1], tsqlResultTable.Rows[i][2], tsqlResultTable.Rows[i][3]);
                topListLinq.AppendFormatLine("{0},{1},{2},{3}", linqResultTable[i].ProductId, linqResultTable[i].Name, linqResultTable[i].Quantity, linqResultTable[i].Revenue);
            }

            if (!topListLinq.ToString().Equals(topListSql.ToString()))
                throw new Exception("Top lists by quantity are not equal\r\nSLQ: {0}\r\nLINQ: {1}".FormatSafe(topListSql.ToString(), topListLinq.ToString()));            
        }

        public class TopXProduct
        {
            public int? ProductId { get; set; }
            public string Name { get; set; }
            public int Quantity { get; set; }
            public decimal Revenue { get; set; }
        }

        public enum TopXProductOrder
        {
            Revenue,
            Quantity
        }

        private void ConsistencyCheck<T>(string methodName, T linqResult, string query)
        {
            var sqlResult = default(T);

            var queryResult = executeSqlQuery(query);
            if (queryResult.Rows != null && queryResult.Rows.Count == 1 && queryResult.Columns.Count == 1)
            {
                sqlResult = (T)queryResult.Rows[0][0];
            }

            if (!sqlResult.Equals(linqResult))
                throw new Exception(string.Format("Results for {0} not equal SQL: {1} and LINQ: {2}", methodName, sqlResult, linqResult));
        }

        private static System.Data.DataTable executeSqlQuery(string query)
        {
            SqlCommand command = new SqlCommand(query);

            var queryResult = new System.Data.DataTable();
            try
            {

                using (SqlConnection connection = new SqlConnection(Obymobi.Data.DaoClasses.CommonDaoBase.ActualConnectionString))
                {
                    connection.Open();
                    SqlCommand useDbCommand = new SqlCommand("USE " + DbUtils.GetCurrentCatalogName(), connection);
                    useDbCommand.ExecuteNonQuery();
                    command.Connection = connection;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(queryResult);
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Query failed with message: " + ex.Message + "\r\nQuery:\r\n" + command.CommandText, ex);
            }

            return queryResult;
        }

        private static System.Data.DataTable executeSqlQuery(SqlCommand command)
        {
            var queryResult = new System.Data.DataTable();
            try
            {

                using (SqlConnection connection = new SqlConnection(Obymobi.Data.DaoClasses.CommonDaoBase.ActualConnectionString))
                {
                    connection.Open();
                    SqlCommand useDbCommand = new SqlCommand("USE " + DbUtils.GetCurrentCatalogName(), connection);
                    useDbCommand.ExecuteNonQuery();
                    command.Connection = connection;
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(queryResult);
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Query failed with message: " + ex.Message + "\r\nQuery:\r\n" + command.CommandText, ex);
            }

            return queryResult;
        }

        public class Filter : IAnalyticsFilter
       {
            public Filter()
            {
                CategoryIds = new List<int>();
                ProductIds = new List<int>();
                CompanyIds = new List<int>();
                DeliverypointIds = new List<int>();
                DeliverypointgroupIds = new List<int>();
                AlterationIds = new List<int>();
                OrderTypes = new List<OrderType>();
                ExcludeCategoryIds = new List<int>();
                ExcludeProductIds = new List<int>();
                ExcludeCompanyIds = new List<int>();
                ExcludeDeliverypointIds = new List<int>();
                ExcludeDeliverypointgroupIds = new List<int>();
                ExcludeAlterationIds = new List<int>();
                ExcludeOrderTypes = new List<OrderType>();
            }

            public DateTime FromUtc { get; set; } 
            public DateTime TillUtc { get; set; }
            public int NoOfResults { get; set; }

            public List<int> OrderTypesInt
            {
                get { return this.OrderTypes.Select(x => (int)x).ToList(); }
            }

            public List<int> ExcludeOrderTypesInt
            {
                get { return this.ExcludeOrderTypes.Select(x => (int)x).ToList(); }
            }

            public List<int> CategoryIds { get; set; } 
            public List<int> ProductIds { get; set; } 
            public List<int> CompanyIds { get; set; } 
            public List<int> DeliverypointIds { get; set; } 
            public List<int> DeliverypointgroupIds { get; set; }
            public List<int> AlterationIds{ get; set; }
            public List<OrderType> OrderTypes { get; set; }

            public List<int> ExcludeCategoryIds { get; set; }
            public List<int> ExcludeProductIds { get; set; }
            public List<int> ExcludeCompanyIds { get; set; } 
            public List<int> ExcludeDeliverypointIds { get; set; } 
            public List<int> ExcludeDeliverypointgroupIds { get; set; }
            public List<int> ExcludeAlterationIds { get; set; }
            public List<OrderType> ExcludeOrderTypes { get; set; } 


            public bool IncludeChildCategoryIds { get; set; }
            public bool IncludeFailedOrders { get; set; }

            public Filter Clone()
            {
                return JsonConvert.DeserializeObject<Filter>(JsonConvert.SerializeObject(this));
            } 

            public Expression<Func<OrderEntity, bool>> GetOrderPredicate()
            {
                var predicate = PredicateBuilder.Null<OrderEntity>();

                // Always the date filter
                predicate = predicate.And(o => o.CreatedUTC >= this.FromUtc && o.CreatedUTC <= this.TillUtc);

                // Optional filters
                if (this.OrderTypes.Count > 0)
                    predicate = predicate.And(o => this.OrderTypesInt.Contains(o.Type));

                if (this.ExcludeOrderTypes.Count > 0)
                    predicate = predicate.And(o => !this.ExcludeOrderTypesInt.Contains(o.Type));

                if (this.DeliverypointIds.Count > 0)
                    predicate = predicate.And(o => this.DeliverypointIds.Contains(o.DeliverypointId.Value));

                if (this.ExcludeDeliverypointIds.Count > 0)
                    predicate = predicate.And(o => !this.ExcludeDeliverypointIds.Contains(o.DeliverypointId.Value));

                if (this.DeliverypointgroupIds.Count > 0)
                    predicate = predicate.And(o => this.DeliverypointgroupIds.Contains(o.DeliverypointId.Value));

                if (this.ExcludeDeliverypointgroupIds.Count > 0)
                    predicate = predicate.And(o => !this.ExcludeDeliverypointgroupIds.Contains(o.DeliverypointId.Value));

                if (this.CompanyIds.Count > 0)
                    predicate = predicate.And(o => this.CompanyIds.Contains(o.CompanyId));

                if (this.ExcludeCompanyIds.Count > 0)
                    predicate = predicate.And(o => !this.ExcludeCompanyIds.Contains(o.CompanyId));

                if (this.CategoryIds.Count > 0) // Een Orderitem heeft één van deze category ids
                    predicate = predicate.And(o => o.OrderitemCollection.Any(oi => this.CategoryIds.Contains(oi.CategoryId ?? 0)));

                if (this.ExcludeCategoryIds.Count > 0)
                    predicate = predicate.And(o => !o.OrderitemCollection.Any(oi => this.CategoryIds.Contains(oi.CategoryId ?? 0)));

                if (this.ProductIds.Count > 0)
                    predicate = predicate.And(o => o.OrderitemCollection.Any(oi => this.ProductIds.Contains(oi.ProductId ?? 0)));

                if (this.ExcludeProductIds.Count > 0)
                    predicate = predicate.And(o => !o.OrderitemCollection.Any(oi => this.ProductIds.Contains(oi.ProductId ?? 0)));

                if (this.AlterationIds.Count > 0 || this.ExcludeAlterationIds.Count > 0)
                    throw new NotImplementedException("AlterationId filtering is not supported for Order-based retrieval");

                if (!this.IncludeFailedOrders)
                    predicate = predicate.And(o => o.ErrorCode == 0);

                return predicate;
            }

            public Expression<Func<OrderitemEntity, bool>> GetOrderitemPredicate()
            {
                var predicate = PredicateBuilder.Null<OrderitemEntity>();

                // Always the date filter
                predicate = predicate.And(oi => oi.OrderEntity.CreatedUTC >= this.FromUtc && oi.OrderEntity.CreatedUTC <= this.TillUtc);

                // Optional filters
                if (this.OrderTypes.Count > 0)
                    predicate = predicate.And(oi => this.OrderTypesInt.Contains(oi.OrderEntity.Type));

                if (this.ExcludeOrderTypes.Count > 0)
                    predicate = predicate.And(oi => !this.ExcludeOrderTypesInt.Contains(oi.OrderEntity.Type));

                if (this.DeliverypointIds.Count > 0)
                    predicate = predicate.And(oi => this.DeliverypointIds.Contains(oi.OrderEntity.DeliverypointId.Value));

                if (this.ExcludeDeliverypointIds.Count > 0)
                    predicate = predicate.And(oi => !this.ExcludeDeliverypointIds.Contains(oi.OrderEntity.DeliverypointId.Value));

                if (this.DeliverypointgroupIds.Count > 0)
                    predicate = predicate.And(oi => this.DeliverypointgroupIds.Contains(oi.OrderEntity.DeliverypointEntity.DeliverypointgroupId));

                if (this.ExcludeDeliverypointgroupIds.Count > 0)
                    predicate = predicate.And(oi => !this.ExcludeDeliverypointgroupIds.Contains(oi.OrderEntity.DeliverypointEntity.DeliverypointgroupId));

                if (this.CompanyIds.Count > 0)
                    predicate = predicate.And(oi => this.CompanyIds.Contains(oi.OrderEntity.CompanyId));

                if (this.ExcludeCompanyIds.Count > 0)
                    predicate = predicate.And(oi => !this.ExcludeCompanyIds.Contains(oi.OrderEntity.CompanyId));

                if (this.CategoryIds.Count > 0)
                    predicate = predicate.And(oi => this.CategoryIds.Contains(oi.CategoryId.Value));

                if (this.ExcludeCategoryIds.Count > 0)
                    predicate = predicate.And(oi => !this.ExcludeCategoryIds.Contains(oi.CategoryId.Value));

                if (this.ProductIds.Count > 0)
                    predicate = predicate.And(oi => this.ProductIds.Contains(oi.ProductId.Value));

                if (this.ExcludeProductIds.Count > 0)
                    predicate = predicate.And(oi => !this.ExcludeProductIds.Contains(oi.ProductId.Value));

                if (this.AlterationIds.Count > 0 || this.ExcludeAlterationIds.Count > 0)
                    throw new NotImplementedException("AlterationId filtering is not supported for Orderitem-based retrieval");

                if (!this.IncludeFailedOrders)
                    predicate = predicate.And(oi => oi.OrderEntity.ErrorCode == 0);

                return predicate;
            }

            public Expression<Func<OrderitemAlterationitemEntity, bool>> GetOrderitemAlterationitemPredicate()
            {
                var predicate = PredicateBuilder.Null<OrderitemAlterationitemEntity>();

                // Always the date filter
                predicate = predicate.And(oiai =>   oiai.OrderitemEntity.OrderEntity.CreatedUTC >= this.FromUtc &&
                                                    oiai.OrderitemEntity.OrderEntity.CreatedUTC <= this.TillUtc);

                // Optional filters
                if (this.OrderTypes.Count > 0)
                    predicate = predicate.And(oiai => this.OrderTypesInt.Contains(oiai.OrderitemEntity.OrderEntity.Type));

                if (this.ExcludeOrderTypes.Count > 0)
                    predicate = predicate.And(oiai => !this.ExcludeOrderTypesInt.Contains(oiai.OrderitemEntity.OrderEntity.Type));

                if (this.DeliverypointIds.Count > 0)
                    predicate = predicate.And(oiai => this.DeliverypointIds.Contains(oiai.OrderitemEntity.OrderEntity.DeliverypointId.Value));

                if (this.ExcludeDeliverypointIds.Count > 0)
                    predicate = predicate.And(oiai => !this.ExcludeDeliverypointIds.Contains(oiai.OrderitemEntity.OrderEntity.DeliverypointId.Value));

                if (this.DeliverypointgroupIds.Count > 0)
                    predicate = predicate.And(oiai => this.DeliverypointgroupIds.Contains(oiai.OrderitemEntity.OrderEntity.DeliverypointId.Value));

                if (this.ExcludeDeliverypointgroupIds.Count > 0)
                    predicate = predicate.And(oiai => !this.ExcludeDeliverypointgroupIds.Contains(oiai.OrderitemEntity.OrderEntity.DeliverypointId.Value));

                if (this.CompanyIds.Count > 0)
                    predicate = predicate.And(oiai => this.CompanyIds.Contains(oiai.OrderitemEntity.OrderEntity.CompanyId));

                if (this.ExcludeCompanyIds.Count > 0)
                    predicate = predicate.And(oiai => !this.ExcludeCompanyIds.Contains(oiai.OrderitemEntity.OrderEntity.CompanyId));

                if (this.CategoryIds.Count > 0)
                    predicate = predicate.And(oiai => this.CategoryIds.Contains(oiai.OrderitemEntity.CategoryId.Value));

                if (this.ExcludeCategoryIds.Count > 0)
                    predicate = predicate.And(oiai => !this.ExcludeCategoryIds.Contains(oiai.OrderitemEntity.CategoryId.Value));

                if (this.ProductIds.Count > 0)
                    predicate = predicate.And(oiai => this.ProductIds.Contains(oiai.OrderitemEntity.ProductId.Value));

                if (this.ExcludeProductIds.Count > 0)
                    predicate = predicate.And(oiai => !this.ExcludeProductIds.Contains(oiai.OrderitemEntity.ProductId.Value));

                if (this.AlterationIds.Count > 0)
                    predicate = predicate.And(oiai => this.AlterationIds.Contains(oiai.AlterationitemEntity.AlterationId));

                if (this.ExcludeAlterationIds.Count > 0)
                    predicate = predicate.And(oiai => !this.ExcludeAlterationIds.Contains(oiai.AlterationitemEntity.AlterationId));

                if (!this.IncludeFailedOrders)
                    predicate = predicate.And(oiai => oiai.OrderitemEntity.OrderEntity.ErrorCode == 0);

                return predicate;
            }

            public string GetWhereClause()
            {
                StringBuilder sb = new StringBuilder();

                sb.AppendFormatLine("[Order].CreatedUTC >= '{0:yyyy-MM-dd HH:mm:ss}' and [Order].CreatedUTC <= '{1:yyyy-MM-dd HH:mm:ss}'", this.FromUtc, this.TillUtc);

                // Optional filters
                if (this.OrderTypes.Count > 0)
                    sb.AppendFormatLine("\tand [Order].Type in ({0})", string.Join(",", this.OrderTypesInt));

                if (this.ExcludeOrderTypes.Count > 0)
                    sb.AppendFormatLine("\tand [Order].Type not in ({0})", string.Join(",", this.ExcludeOrderTypesInt));

                if (this.DeliverypointIds.Count > 0)
                    sb.AppendFormatLine("\tand [Order].DeliverypointId in ({0})", string.Join(",", this.DeliverypointIds));

                if (this.ExcludeDeliverypointIds.Count > 0)
                    sb.AppendFormatLine("\tand [Order].DeliverypointId not in ({0})", string.Join(",", this.ExcludeDeliverypointIds));

                if (this.DeliverypointgroupIds.Count > 0)
                    sb.AppendFormatLine("\tand [Deliverypoint].DeliverypointgroupId in ({0})", string.Join(",", this.DeliverypointgroupIds));

                if (this.ExcludeDeliverypointgroupIds.Count > 0)
                    sb.AppendFormatLine("\tand [Deliverypoint].DeliverypointgroupId not in ({0})", string.Join(",", this.ExcludeDeliverypointgroupIds));

                if (this.CompanyIds.Count > 0)
                    sb.AppendFormatLine("\tand [Order].CompanyId in ({0})", string.Join(",", this.CompanyIds));

                if (this.ExcludeCompanyIds.Count > 0)
                    sb.AppendFormatLine("\tand [Order].CompanyId not in ({0})", string.Join(",", this.ExcludeCompanyIds));

                if (this.CategoryIds.Count > 0)
                    sb.AppendFormatLine("\tand [Orderitem].CategoryId in ({0})", string.Join(",", this.CategoryIds));

                if (this.ExcludeCategoryIds.Count > 0)
                    sb.AppendFormatLine("\tand [Orderitem].CategoryId not in ({0})", string.Join(",", this.ExcludeCategoryIds));

                if (this.ProductIds.Count > 0)
                    sb.AppendFormatLine("\tand [Orderitem].ProductId in ({0})", string.Join(",", this.ProductIds));

                if (this.ExcludeProductIds.Count > 0)
                    sb.AppendFormatLine("\tand [Orderitem].ProductId not in ({0})", string.Join(",", this.ExcludeProductIds));

                if (this.AlterationIds.Count > 0)
                    sb.AppendFormatLine("\tand [Alterationitem].AlterationId in ({0})", string.Join(",", this.AlterationIds));

                if (this.ExcludeAlterationIds.Count > 0)
                    sb.AppendFormatLine("\tand [Alterationitem].AlterationId not in ({0})", string.Join(",", this.ExcludeAlterationIds));

                if(!this.IncludeFailedOrders)
                    sb.AppendFormatLine("\tand [Order].ErrorCode = 0", string.Join(",", this.ExcludeAlterationIds));                

                return sb.ToString();
            }
        }
    }
}
