﻿using Newtonsoft.Json;
using Obymobi.Analytics.KpiFunctions;
using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.Data
{
    public class KpiFunctionProcessor : KpiFunctionProcessorBase<TransactionsKpiRetriever>
    {
        public KpiFunctionProcessor(TransactionsKpiRetriever kpiRetriever, IChildCategoryFetcher childCategoryFetcher, int? companyIdCheck = null) : base(kpiRetriever, childCategoryFetcher, companyIdCheck)
        {

        }

        protected override IAnalyticsFilter CreateBaseFilter()
        {
            return new TransactionsKpiRetriever.Filter();
        }

        public override bool CanHandleFunction(string functionName)
        {
            if (base.CanHandleFunction(functionName))
                return true;
            else
            {
                // Allow for the 'Daily' functions
                if(functionName.StartsWith("Daily"))
                {
                    string actualFunctionName = functionName.Replace("Daily", string.Empty);
                    return base.CanHandleFunction(actualFunctionName);
                }
            }

            return false;
        }

        protected override bool ExecuteFunctionNonGeneric(string functionName, IAnalyticsFilter filter, out List<object[]> result)
        {
            result = null;
            var filterTyped = (TransactionsKpiRetriever.Filter)filter;
            if (filterTyped.OrderTypesInt.Contains((int)OrderType.RequestForWakeUp) && !functionName.StartsWith("OrdersWithNotes"))
            {
                throw new Exception("OrderType RequestForWakeUp can only be used with function 'OrdersWithNotes' because all others are Orderitem based and those orders don't have Orderitems");
            }
            else if (functionName.Equals("TopXProductsByQuantity"))
            {
                var topXProducts = this.kpiRetriever.GetTopXProductsByQuantity(filterTyped);
                result = this.topXProductsToGeneric(topXProducts);
            }
            else if (functionName.Equals("TopXProductsByRevenue"))
            {
                var topXProducts = this.kpiRetriever.GetTopXProductsByRevenue(filterTyped);
                result = this.topXProductsToGeneric(topXProducts);
            }
            else if (functionName.Equals("OrderedAlterations"))
            {
                var oiais = this.kpiRetriever.GetOrderedAlterations(filterTyped);
                result = this.orderedAlterationsToGeneric(oiais);
            }
            else if (functionName.Equals("OrdersWithNotes"))
            {
                var orders = this.kpiRetriever.GetOrdersWithNotes(filterTyped);

                // Some post processing
                if (filterTyped.OrderTypes.Count == 1 && filterTyped.OrderTypes[0] == Enums.OrderType.RequestForWakeUp)
                {
                    // Fix the Notes fields
                    foreach (var order in orders)
                        order.Notes = order.Notes.Substring(order.Notes.IndexOf("Wake up"));
                }

                result = this.ordersWithNotesToGeneric(orders);
            }
            else if (functionName.StartsWith("Daily"))
            {
                // Dirty little trick, just re-issue the method for every day instead of for the period.
                string actualFunctionName = functionName.Replace("Daily", string.Empty);

                // Create a filter per day
                List<object> dailyResults = new List<object>();
                for (DateTime current = filter.FromUtc; current <= filter.TillUtc; current = current.AddDays(1))
                {
                    // Clone filter
                    var dayFilter = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(filter), filter.GetType()) as IAnalyticsFilter;
                    dayFilter.FromUtc = current;
                    dayFilter.TillUtc = current.AddDays(1);
                    List<object[]> dailyResult = this.ExecuteFunction(actualFunctionName, dayFilter);
                    dailyResults.Add(dailyResult[0][0]);
                }

                // Execute the Total for whole period
                if (functionName.Equals("DailyStartDate"))
                {
                    dailyResults.Insert(0, "Total");
                }
                else
                {
                    List<object[]> totalResult = this.ExecuteFunction(actualFunctionName, filter);
                    dailyResults.Insert(0, totalResult[0][0]);
                }

                result = new List<object[]>();
                result.Add(dailyResults.ToArray());
            }
            else
                return false;

            return true;
        }

        private List<object[]> ordersWithNotesToGeneric(List<TransactionsKpiRetriever.OrderWithNotes> orders)
        {        
            List<object[]> toReturn = new List<object[]>();
            toReturn.Add(new object[] { "OrderId", "UtcCreated", "DeliverypointNumber", "DeliverypointName", "DeliverypointgroupName", "Notes" });

            foreach (var o in orders)
            {
                toReturn.Add(new object[] { o.OrderId, o.UtcCreated, o.DeliverypointNumber, o.DeliverypointName, o.DeliverypointgroupName, o.Notes });
            }

            return toReturn;
        }

        private List<object[]> topXProductsToGeneric(List<TransactionsKpiRetriever.TopXProduct> products)
        {
            List<object[]> toReturn = new List<object[]>();
            toReturn.Add(new object[] { "Name", "Quantity", "Revenue", "ProductId" });

            foreach (var product in products)
            {
                toReturn.Add(new object[] { product.Name, product.Quantity, product.Revenue, product.ProductId });
            }

            return toReturn;
        }

        private List<object[]> orderedAlterationsToGeneric(List<TransactionsKpiRetriever.OrderitemAlterationItem> oiais)
        {
            List<object[]> toReturn = new List<object[]>();
            toReturn.Add(new object[] { "AlterationName", "AlterationoptionName", "DeliverypointNumber", "DeliverypointName", "DeliverypointgroupName", "CreatedUTC", "OrderId", "OrderitemAlterationitemId" });

            foreach (var oiai in oiais)
            {
                toReturn.Add(new object[] { oiai.AlterationName, oiai.AlterationoptionName, oiai.DeliverypointNumber, oiai.DeliverypointName, oiai.DeliverypointgroupName, oiai.CreatedUTC, oiai.OrderId, oiai.OrderitemAlterationitemId });
            }

            return toReturn;
        }

    }
}
