using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v27
{
    /// <summary>
    /// Model class which represents an survey result
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "SurveyResultAnswer"), IncludeInCodeGeneratorForFlex]
    public class SurveyResultAnswer : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.SurveyResultAnswer type
        /// </summary>
        public SurveyResultAnswer()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the survey
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyResultAnswerId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the survey question
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyQuestionId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyAnswerId
        { get; set; }

        /// <summary>
        /// Gets or sets the comment of the result answer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Comment
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public SurveyResultAnswer Clone()
        {
            return this.Clone<SurveyResultAnswer>();
        }

        #endregion
    }
}
