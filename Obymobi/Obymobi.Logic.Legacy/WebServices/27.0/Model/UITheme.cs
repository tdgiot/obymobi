using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v27
{
    /// <summary>
    /// Model class which represents theme
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "UITheme"), IncludeInCodeGeneratorForAndroid]
    public class UITheme : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.UITheme type
        /// </summary>
        public UITheme()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the theme
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int UIThemeId
        { get; set; }

        [XmlArray("UIThemeColors")]
        [XmlArrayItem("UIThemeColor")]
        [IncludeInCodeGeneratorForAndroid]
        public UIThemeColor[] UIThemeColors
        { get; set; }

        [XmlArray("UIThemeTextSizes")]
        [XmlArrayItem("UIThemeTextSize")]
        [IncludeInCodeGeneratorForAndroid]
        public UIThemeTextSize[] UIThemeTextSizes
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the widget
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UITheme Clone()
        {
            return this.Clone<UITheme>();
        }

        #endregion
    }
}
