using System.Collections.Generic;

namespace Obymobi.Logic.Model.v27
{
    public class EmenuConfiguration
    {
        public class Deliverypoint
        {
            #region  Fields

            public int DeliverypointId;
            public string DeliverypointName;
            public string DeliverypointNumber;

            #endregion
        }

        public class WifiConfiguration
        {
            #region Fields

            public string Ssid;
            public bool HiddenSsid;
            public int Security;
            public string SecurityKey;
            public string Identity;
            public string AnonymousIdentity;
            public int EapMethod;
            public int Phase2Auth;

            #endregion
        }

        #region  Fields

        public int CompanyId;
        public string CompanyName;
        public string CompanyOwnerPassword;
        public string CompanyOwnerUsername;

        public int DeliverypointgroupId;
        public string DeliverypointgroupName;

        public List<Deliverypoint> Deliverypoints = new List<Deliverypoint>();
        public List<WifiConfiguration> WifiConfigurations = new List<WifiConfiguration>();

        public string Error;
        public int ErrorCode;

        #endregion
    }
}
