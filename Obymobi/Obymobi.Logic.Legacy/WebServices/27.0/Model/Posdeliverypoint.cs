using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v27
{
    /// <summary>
    /// Model class which represents a point-of-sale deliverypoint
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Posdeliverypoint")]
    public class Posdeliverypoint : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Posdeliverypoint type
        /// </summary>
        public Posdeliverypoint()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Posdeliverypoint type using the specified parameters
        /// </summary>
        public Posdeliverypoint(int PosdeliverypointId, int CompanyId, string ExternalId, string ExternalPosdeliverypointgroupId, string Name, string Number)
        {
            this.PosdeliverypointId = PosdeliverypointId;
            this.CompanyId = CompanyId;
            this.ExternalId = ExternalId;
            this.ExternalPosdeliverypointgroupId = ExternalPosdeliverypointgroupId;
            this.Name = Name;
            this.Number = Number;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the deliverypoint
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int PosdeliverypointId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the external id of the pos deliverypoint
        /// </summary>
        [XmlElement]
        public string ExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the external id of the pos deliverypointgroup
        /// </summary>
        [XmlElement]
        public string ExternalPosdeliverypointgroupId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the pos deliverypoint
        /// </summary>
        [XmlElement]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the number of the pos deliverypoint
        /// </summary>
        [XmlElement]
        public string Number
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }


        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Posdeliverypoint Clone()
        {
            return this.Clone<Posdeliverypoint>();
        }

        #endregion
    }
}
