namespace Obymobi.Logic.Model.v27.Converters
{
	public class RoomStatusConverter : ModelConverterBase<Obymobi.Logic.Model.v27.RoomStatus, Obymobi.Logic.Model.RoomStatus>
	{
        public RoomStatusConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v28.Converters.RoomStatusConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v27.RoomStatus ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomStatus source)
        {
            Obymobi.Logic.Model.v27.RoomStatus target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v27.RoomStatus();
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.Extension = source.Extension;
                target.CleaningStatus = source.CleaningStatus;
                target.AccountNumber = source.AccountNumber;
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomStatus ConvertLegacyModelToModel(Obymobi.Logic.Model.v27.RoomStatus source)
        {
            Obymobi.Logic.Model.RoomStatus target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomStatus();

                // Copy default values from new version
                new Obymobi.Logic.Model.v28.Converters.RoomStatusConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
