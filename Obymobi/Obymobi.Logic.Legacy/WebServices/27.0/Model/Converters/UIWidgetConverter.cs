namespace Obymobi.Logic.Model.v27.Converters
{
	public class UIWidgetConverter : ModelConverterBase<Obymobi.Logic.Model.v27.UIWidget, Obymobi.Logic.Model.UIWidget>
	{
        public UIWidgetConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v28.Converters.UIWidgetConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v27.UIWidget ConvertModelToLegacyModel(Obymobi.Logic.Model.UIWidget source)
        {
            Obymobi.Logic.Model.v27.UIWidget target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v27.UIWidget();
                target.UIWidgetId = source.UIWidgetId;
                target.UITabId = source.UITabId;
                target.Name = source.Name;
                target.Caption = source.Caption;
                target.Type = source.Type;
                target.SortOrder = source.SortOrder;
                target.AdvertisementId = source.AdvertisementId;
                target.ProductId = source.ProductId;
                target.CategoryId = source.CategoryId;
                target.EntertainmentId = source.EntertainmentId;
                target.EntertainmentcategoryId = source.EntertainmentcategoryId;
                target.SiteId = source.SiteId;
                target.PageId = source.PageId;
                target.UITabType = source.UITabType;
                target.Url = source.Url;
                target.RoomControlSectionType = source.RoomControlSectionType;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.MessageLayoutType = source.MessageLayoutType;

                if (source.UIWidgetLanguages != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v27.Converters.UIWidgetLanguageConverter uIWidgetLanguagesConverter = new Obymobi.Logic.Model.v27.Converters.UIWidgetLanguageConverter();
                    target.UIWidgetLanguages = (UIWidgetLanguage[])uIWidgetLanguagesConverter.ConvertArrayToLegacyArray(source.UIWidgetLanguages);
                }

                if (source.UIWidgetTimers != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v27.Converters.UIWidgetTimerConverter uIWidgetTimersConverter = new Obymobi.Logic.Model.v27.Converters.UIWidgetTimerConverter();
                    target.UIWidgetTimers = (UIWidgetTimer[])uIWidgetTimersConverter.ConvertArrayToLegacyArray(source.UIWidgetTimers);
                }

                if (source.UIWidgetAvailabilitys != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v27.Converters.UIWidgetAvailabilityConverter uIWidgetAvailabilitysConverter = new Obymobi.Logic.Model.v27.Converters.UIWidgetAvailabilityConverter();
                    target.UIWidgetAvailabilitys = (UIWidgetAvailability[])uIWidgetAvailabilitysConverter.ConvertArrayToLegacyArray(source.UIWidgetAvailabilitys);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v27.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v27.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v27.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v27.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.UIWidget ConvertLegacyModelToModel(Obymobi.Logic.Model.v27.UIWidget source)
        {
            Obymobi.Logic.Model.UIWidget target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UIWidget();

                // Copy default values from new version
                new Obymobi.Logic.Model.v28.Converters.UIWidgetConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
