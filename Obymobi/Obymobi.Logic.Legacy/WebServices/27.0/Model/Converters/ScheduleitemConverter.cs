namespace Obymobi.Logic.Model.v27.Converters
{
	public class ScheduleitemConverter : ModelConverterBase<Obymobi.Logic.Model.v27.Scheduleitem, Obymobi.Logic.Model.Scheduleitem>
	{
        public ScheduleitemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v28.Converters.ScheduleitemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v27.Scheduleitem ConvertModelToLegacyModel(Obymobi.Logic.Model.Scheduleitem source)
        {
            Obymobi.Logic.Model.v27.Scheduleitem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v27.Scheduleitem();
                target.ScheduleitemId = source.ScheduleitemId;
                target.DayOfWeek = source.DayOfWeek;
                target.TimeStart = source.TimeStart;
                target.TimeEnd = source.TimeEnd;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Scheduleitem ConvertLegacyModelToModel(Obymobi.Logic.Model.v27.Scheduleitem source)
        {
            Obymobi.Logic.Model.Scheduleitem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Scheduleitem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v28.Converters.ScheduleitemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
