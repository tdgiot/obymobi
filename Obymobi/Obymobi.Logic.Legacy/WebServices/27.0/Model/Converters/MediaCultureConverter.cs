namespace Obymobi.Logic.Model.v27.Converters
{
	public class MediaCultureConverter : ModelConverterBase<Obymobi.Logic.Model.v27.MediaCulture, Obymobi.Logic.Model.MediaCulture>
	{
        public MediaCultureConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v28.Converters.MediaCultureConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v27.MediaCulture ConvertModelToLegacyModel(Obymobi.Logic.Model.MediaCulture source)
        {
            Obymobi.Logic.Model.v27.MediaCulture target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v27.MediaCulture();
                target.MediaCultureId = source.MediaCultureId;
                target.MediaId = source.MediaId;
                target.Parent = source.Parent;
                target.CultureCode = source.CultureCode;
                target.Name = source.Name;
                target.LocalizedName = source.LocalizedName;
            }

            return target;
        }

        public override Obymobi.Logic.Model.MediaCulture ConvertLegacyModelToModel(Obymobi.Logic.Model.v27.MediaCulture source)
        {
            Obymobi.Logic.Model.MediaCulture target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.MediaCulture();

                // Copy default values from new version
                new Obymobi.Logic.Model.v28.Converters.MediaCultureConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
