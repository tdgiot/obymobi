using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Logic.Model.v27.Converters
{
	public class MediaConverter : ModelConverterBase<Obymobi.Logic.Model.v27.Media, Obymobi.Logic.Model.Media>
	{
        public MediaConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v28.Converters.MediaConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v27.Media ConvertModelToLegacyModel(Obymobi.Logic.Model.Media source)
        {
            Obymobi.Logic.Model.v27.Media target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v27.Media();
                target.MediaId = source.MediaId;
                target.Name = source.Name;
                target.FilePathRelativeToMediaPath = source.FilePathRelativeToMediaPath;
                target.CdnFilePathRelativeToMediaPath = source.CdnFilePathRelativeToMediaPath;
                target.GenericFile = source.GenericFile;
                target.MediaType = (int)MediaHelper.GetCorrectMediaTypeIfGeneric((MediaType)source.MediaType);

                if (source.MediaRatioTypeMedias != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v27.Converters.MediaRatioTypeMediaConverter mediaRatioTypeMediasConverter = new Obymobi.Logic.Model.v27.Converters.MediaRatioTypeMediaConverter();
                    target.MediaRatioTypeMedias = (MediaRatioTypeMedia[])mediaRatioTypeMediasConverter.ConvertArrayToLegacyArray(source.MediaRatioTypeMedias);
                }

                target.Parent = source.Parent;
                target.ActionProductId = source.ActionProductId;
                target.ActionCategoryId = source.ActionCategoryId;
                target.ActionEntertainmentId = source.ActionEntertainmentId;
                target.ActionEntertainmentcategoryId = source.ActionEntertainmentcategoryId;
                target.ActionUrl = source.ActionUrl;
                target.AgnosticMediaId = source.AgnosticMediaId;
                target.SizeMode = source.SizeMode;
                target.ZoomLevel = source.ZoomLevel;
                target.ActionSiteId = source.ActionSiteId;
                target.ActionPageId = source.ActionPageId;

                if (source.MediaCultures != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v27.Converters.MediaCultureConverter mediaCulturesConverter = new Obymobi.Logic.Model.v27.Converters.MediaCultureConverter();
                    target.MediaCultures = (MediaCulture[])mediaCulturesConverter.ConvertArrayToLegacyArray(source.MediaCultures);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Media ConvertLegacyModelToModel(Obymobi.Logic.Model.v27.Media source)
        {
            Obymobi.Logic.Model.Media target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Media();

                // Copy default values from new version
                new Obymobi.Logic.Model.v28.Converters.MediaConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
