namespace Obymobi.Logic.Model.v27.Converters
{
	public class PriceScheduleItemConverter : ModelConverterBase<Obymobi.Logic.Model.v27.PriceScheduleItem, Obymobi.Logic.Model.PriceScheduleItem>
	{
        public PriceScheduleItemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v28.Converters.PriceScheduleItemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v27.PriceScheduleItem ConvertModelToLegacyModel(Obymobi.Logic.Model.PriceScheduleItem source)
        {
            Obymobi.Logic.Model.v27.PriceScheduleItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v27.PriceScheduleItem();
                target.PriceScheduleItemId = source.PriceScheduleItemId;
                target.PriceLevelId = source.PriceLevelId;

                if (source.PriceScheduleItemOccurrences != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v27.Converters.PriceScheduleItemOccurrenceConverter priceScheduleItemOccurrencesConverter = new Obymobi.Logic.Model.v27.Converters.PriceScheduleItemOccurrenceConverter();
                    target.PriceScheduleItemOccurrences = (PriceScheduleItemOccurrence[])priceScheduleItemOccurrencesConverter.ConvertArrayToLegacyArray(source.PriceScheduleItemOccurrences);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.PriceScheduleItem ConvertLegacyModelToModel(Obymobi.Logic.Model.v27.PriceScheduleItem source)
        {
            Obymobi.Logic.Model.PriceScheduleItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.PriceScheduleItem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v28.Converters.PriceScheduleItemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
