namespace Obymobi.Logic.Model.v27.Converters
{
	public class StationConverter : ModelConverterBase<Obymobi.Logic.Model.v27.Station, Obymobi.Logic.Model.Station>
	{
        public StationConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("Url");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v28.Converters.StationConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v27.Station ConvertModelToLegacyModel(Obymobi.Logic.Model.Station source)
        {
            Obymobi.Logic.Model.v27.Station target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v27.Station();
                target.StationId = source.StationId;
                target.Caption = source.Caption;
                target.Scene = source.Scene;
                target.SuccessMessage = source.SuccessMessage;
                target.Description = source.Description;

                if (int.TryParse(source.Channel, out int number))
                {
                    target.Number = number;
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v27.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v27.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v27.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v27.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Station ConvertLegacyModelToModel(Obymobi.Logic.Model.v27.Station source)
        {
            Obymobi.Logic.Model.Station target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Station();

                // Copy default values from new version
                new Obymobi.Logic.Model.v28.Converters.StationConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
