using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v27
{
    /// <summary>
    /// Model class which represents a custom text
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CustomText"), IncludeInCodeGeneratorForXamarin]
    public class CustomText : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CustomText type
        /// </summary>
        public CustomText()
        {
        }        

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the custom text
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]        
        public int CustomTextId
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the custom text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the text of the custom text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Text
        { get; set; }

        /// <summary>
        /// Gets or sets the culture code of the custom text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string CultureCode
        { get; set; }

        /// <summary>
        /// Gets or sets the foreign key this custom text is attached to
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ForeignKey
        { get; set; }
    
        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CustomText Clone()
        {
            return this.Clone<CustomText>();
        }

        #endregion
    }
}
