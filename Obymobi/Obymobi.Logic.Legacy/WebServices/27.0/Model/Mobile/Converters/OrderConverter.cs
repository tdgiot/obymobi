using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v27.Converters
{
	public class OrderConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v27.Order, Obymobi.Logic.Model.Mobile.Order>
	{
        public OrderConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v27.Order ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Order source)
        {
            Obymobi.Logic.Model.Mobile.v27.Order target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v27.Order();
                target.OrderId = source.OrderId;
                target.Guid = source.Guid;
                target.CustomerId = source.CustomerId;
                target.CompanyId = source.CompanyId;
                target.DeliverypointId = source.DeliverypointId;
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.DeliverypointName = source.DeliverypointName;
                target.Status = source.Status;
                target.StatusText = source.StatusText;
                target.ErrorCode = source.ErrorCode;
                target.Notes = source.Notes;
                target.CustomerNameFull = source.CustomerNameFull;
                target.CustomerPhonenumber = source.CustomerPhonenumber;
                target.Type = source.Type;
                target.PendingPaymentmethodAndTransactionId = source.PendingPaymentmethodAndTransactionId;
                target.Created = source.Created;
                target.Email = source.Email;
                target.CreatedAsDateTime = source.CreatedAsDateTime;

                if (source.Orderitems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.OrderitemConverter orderitemsConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.OrderitemConverter();
                    target.Orderitems = (Orderitem[])orderitemsConverter.ConvertArrayToLegacyArray(source.Orderitems);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Order ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v27.Order source)
        {
            Obymobi.Logic.Model.Mobile.Order target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Order();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
