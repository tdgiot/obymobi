using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v27.Converters
{
	public class MediaCultureConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v27.MediaCulture, Obymobi.Logic.Model.Mobile.MediaCulture>
	{
        public MediaCultureConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v27.MediaCulture ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.MediaCulture source)
        {
            Obymobi.Logic.Model.Mobile.v27.MediaCulture target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v27.MediaCulture();
                target.MediaCultureId = source.MediaCultureId;
                target.MediaId = source.MediaId;
                target.CultureCode = source.CultureCode;
                target.Parent = source.Parent;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.MediaCulture ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v27.MediaCulture source)
        {
            Obymobi.Logic.Model.Mobile.MediaCulture target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.MediaCulture();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
