using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v27.Converters
{
	public class AttachmentConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v27.Attachment, Obymobi.Logic.Model.Mobile.Attachment>
	{
        public AttachmentConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v27.Attachment ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Attachment source)
        {
            Obymobi.Logic.Model.Mobile.v27.Attachment target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v27.Attachment();
                target.AttachmentId = source.AttachmentId;
                target.ProductId = source.ProductId;
                target.PageId = source.PageId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.Url = source.Url;
                target.AllowedDomains = source.AllowedDomains;
                target.AllowAllDomains = source.AllowAllDomains;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Attachment ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v27.Attachment source)
        {
            Obymobi.Logic.Model.Mobile.Attachment target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Attachment();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
