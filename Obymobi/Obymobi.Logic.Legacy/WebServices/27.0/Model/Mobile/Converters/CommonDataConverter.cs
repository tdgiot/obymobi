using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v27.Converters
{
	public class CommonDataConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v27.CommonData, Obymobi.Logic.Model.Mobile.CommonData>
	{
        public CommonDataConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v27.CommonData ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.CommonData source)
        {
            Obymobi.Logic.Model.Mobile.v27.CommonData target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v27.CommonData();
                target.LastModifiedTicks = source.LastModifiedTicks;

                if (source.VenueCategories != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.VenueCategoryConverter venueCategoriesConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.VenueCategoryConverter();
                    target.VenueCategories = (VenueCategory[])venueCategoriesConverter.ConvertArrayToLegacyArray(source.VenueCategories);
                }

                if (source.Amenities != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.AmenityConverter amenitiesConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.AmenityConverter();
                    target.Amenities = (Amenity[])amenitiesConverter.ConvertArrayToLegacyArray(source.Amenities);
                }

                if (source.ActionButtons != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.ActionButtonConverter actionButtonsConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.ActionButtonConverter();
                    target.ActionButtons = (ActionButton[])actionButtonsConverter.ConvertArrayToLegacyArray(source.ActionButtons);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.CommonData ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v27.CommonData source)
        {
            Obymobi.Logic.Model.Mobile.CommonData target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.CommonData();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
