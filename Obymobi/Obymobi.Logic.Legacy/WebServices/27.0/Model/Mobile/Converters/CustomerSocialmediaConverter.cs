using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v27.Converters
{
	public class CustomerSocialmediaConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v27.CustomerSocialmedia, Obymobi.Logic.Model.Mobile.CustomerSocialmedia>
	{
        public CustomerSocialmediaConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v27.CustomerSocialmedia ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.CustomerSocialmedia source)
        {
            Obymobi.Logic.Model.Mobile.v27.CustomerSocialmedia target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v27.CustomerSocialmedia();
                target.CustomerSocialmediaId = source.CustomerSocialmediaId;
                target.SocialmediaType = source.SocialmediaType;
                target.Email = source.Email;
                target.ExternalId = source.ExternalId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.CustomerSocialmedia ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v27.CustomerSocialmedia source)
        {
            Obymobi.Logic.Model.Mobile.CustomerSocialmedia target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.CustomerSocialmedia();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
