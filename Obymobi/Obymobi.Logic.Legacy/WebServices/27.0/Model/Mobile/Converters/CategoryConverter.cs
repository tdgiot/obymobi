using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v27.Converters
{
	public class CategoryConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v27.Category, Obymobi.Logic.Model.Mobile.Category>
	{
        public CategoryConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v27.Category ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Category source)
        {
            Obymobi.Logic.Model.Mobile.v27.Category target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v27.Category();
                target.CategoryId = source.CategoryId;
                target.ParentCategoryId = source.ParentCategoryId;
                target.Name = source.Name;
                target.Description = source.Description;
                target.SortOrder = source.SortOrder;
                target.Geofencing = source.Geofencing;
                target.ScheduleId = source.ScheduleId;

                if (source.Categories != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.CategoryConverter categoriesConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.CategoryConverter();
                    target.Categories = (Category[])categoriesConverter.ConvertArrayToLegacyArray(source.Categories);
                }

                if (source.Products != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.ProductConverter productsConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.ProductConverter();
                    target.Products = (Product[])productsConverter.ConvertArrayToLegacyArray(source.Products);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                target.Type = source.Type;
                target.HidePrices = source.HidePrices;

                if (source.CategorySuggestions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.CategorySuggestionConverter categorySuggestionsConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.CategorySuggestionConverter();
                    target.CategorySuggestions = (CategorySuggestion[])categorySuggestionsConverter.ConvertArrayToLegacyArray(source.CategorySuggestions);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Category ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v27.Category source)
        {
            Obymobi.Logic.Model.Mobile.Category target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Category();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
