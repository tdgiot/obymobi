using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v27.Converters
{
	public class MediaConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v27.Media, Obymobi.Logic.Model.Mobile.Media>
	{
        public MediaConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v27.Media ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Media source)
        {
            Obymobi.Logic.Model.Mobile.v27.Media target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v27.Media();
                target.MediaId = source.MediaId;
                target.Url = source.Url;
                target.Filename = source.Filename;
                target.MediaType = source.MediaType;
                target.CdnPath = source.CdnPath;
                target.ActionProductId = source.ActionProductId;
                target.ActionCategoryId = source.ActionCategoryId;
                target.ActionEntertainmentId = source.ActionEntertainmentId;
                target.ActionEntertainmentcategoryId = source.ActionEntertainmentcategoryId;
                target.ActionUrl = source.ActionUrl;
                target.SizeMode = source.SizeMode;
                target.ZoomLevel = source.ZoomLevel;
                target.Parent = source.Parent;
                target.AgnosticMediaId = source.AgnosticMediaId;
                target.ActionSiteId = source.ActionSiteId;
                target.ActionPageId = source.ActionPageId;
                target.RelatedCompanyId = source.RelatedCompanyId;
                target.RelatedBrandId = source.RelatedBrandId;

                if (source.MediaCultures != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.MediaCultureConverter mediaCulturesConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.MediaCultureConverter();
                    target.MediaCultures = (MediaCulture[])mediaCulturesConverter.ConvertArrayToLegacyArray(source.MediaCultures);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Media ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v27.Media source)
        {
            Obymobi.Logic.Model.Mobile.Media target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Media();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
