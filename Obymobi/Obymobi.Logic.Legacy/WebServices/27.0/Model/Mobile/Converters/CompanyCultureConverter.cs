using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v27.Converters
{
	public class CompanyCultureConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v27.CompanyCulture, Obymobi.Logic.Model.Mobile.CompanyCulture>
	{
        public CompanyCultureConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v27.CompanyCulture ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.CompanyCulture source)
        {
            Obymobi.Logic.Model.Mobile.v27.CompanyCulture target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v27.CompanyCulture();
                target.CompanyCultureId = source.CompanyCultureId;
                target.CultureCode = source.CultureCode;
                target.Name = source.Name;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.CompanyCulture ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v27.CompanyCulture source)
        {
            Obymobi.Logic.Model.Mobile.CompanyCulture target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.CompanyCulture();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
