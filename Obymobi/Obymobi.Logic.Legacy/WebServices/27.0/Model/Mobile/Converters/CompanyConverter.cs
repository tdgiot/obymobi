using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v27.Converters
{
	public class CompanyConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v27.Company, Obymobi.Logic.Model.Mobile.Company>
	{
        public CompanyConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v27.Company ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Company source)
        {
            Obymobi.Logic.Model.Mobile.v27.Company target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v27.Company();
                target.CompanyId = source.CompanyId;
                target.Currency = source.Currency;
                target.Name = source.Name;
                target.Addressline1 = source.Addressline1;
                target.Addressline2 = source.Addressline2;
                target.Addressline3 = source.Addressline3;
                target.Zipcode = source.Zipcode;
                target.City = source.City;
                target.Country = source.Country;
                target.Latitude = source.Latitude;
                target.Longitude = source.Longitude;
                target.GeoFencingEnabled = source.GeoFencingEnabled;
                target.GeoFencingRadius = source.GeoFencingRadius;
                target.AllowFreeText = source.AllowFreeText;
                target.CultureCode = source.CultureCode;
                target.Telephone = source.Telephone;
                target.GoogleAnalyticsId = source.GoogleAnalyticsId;
                target.IsHero = source.IsHero;

                if (source.Businesshours != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.BusinesshoursConverter businesshoursConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.BusinesshoursConverter();
                    target.Businesshours = (Businesshours[])businesshoursConverter.ConvertArrayToLegacyArray(source.Businesshours);
                }

                target.BusinesshoursIntermediate = source.BusinesshoursIntermediate;
                target.ActionButtonId = source.ActionButtonId;
                target.ActionButtonUrlTablet = source.ActionButtonUrlTablet;
                target.ActionButtonUrlMobile = source.ActionButtonUrlMobile;
                target.CostIndicationType = source.CostIndicationType;
                target.CostIndicationValue = source.CostIndicationValue;
                target.CompanyDataLastModifiedTicks = source.CompanyDataLastModifiedTicks;
                target.CompanyMediaLastModifiedTicks = source.CompanyMediaLastModifiedTicks;
                target.MenuDataLastModifiedTicks = source.MenuDataLastModifiedTicks;
                target.MenuMediaLastModifiedTicks = source.MenuMediaLastModifiedTicks;
                target.DeliverypointDataLastModifiedTicks = source.DeliverypointDataLastModifiedTicks;
                target.AzureNotificationTag = source.AzureNotificationTag;
                target.TimeZone = source.TimeZone;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.Deliverypointgroups != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.DeliverypointgroupConverter deliverypointgroupsConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.DeliverypointgroupConverter();
                    target.Deliverypointgroups = (Deliverypointgroup[])deliverypointgroupsConverter.ConvertArrayToLegacyArray(source.Deliverypointgroups);
                }

                if (source.Paymentmethods != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.PaymentmethodConverter paymentmethodsConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.PaymentmethodConverter();
                    target.Paymentmethods = (Paymentmethod[])paymentmethodsConverter.ConvertArrayToLegacyArray(source.Paymentmethods);
                }

                target.VenueCategoryIds = source.VenueCategoryIds;
                target.AmenityIds = source.AmenityIds;

                if (source.Schedules != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.ScheduleConverter schedulesConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.ScheduleConverter();
                    target.Schedules = (Schedule[])schedulesConverter.ConvertArrayToLegacyArray(source.Schedules);
                }

                if (source.CompanyCultures != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.CompanyCultureConverter companyCulturesConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.CompanyCultureConverter();
                    target.CompanyCultures = (CompanyCulture[])companyCulturesConverter.ConvertArrayToLegacyArray(source.CompanyCultures);
                }

                if (source.CompanyCurrencys != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.CompanyCurrencyConverter companyCurrencysConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.CompanyCurrencyConverter();
                    target.CompanyCurrencys = (CompanyCurrency[])companyCurrencysConverter.ConvertArrayToLegacyArray(source.CompanyCurrencys);
                }

                target.IsPointOfInterest = source.IsPointOfInterest;

                if (source.PointOfInterestUIMode != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.Mobile.v27.Converters.UIModeConverter pointOfInterestUIModeConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.UIModeConverter();
                    target.PointOfInterestUIMode = (UIMode)pointOfInterestUIModeConverter.ConvertModelToLegacyModel(source.PointOfInterestUIMode);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v27.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.Mobile.v27.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Company ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v27.Company source)
        {
            Obymobi.Logic.Model.Mobile.Company target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Company();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
