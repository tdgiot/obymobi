using Newtonsoft.Json;

namespace Obymobi.Logic.Model.v27
{
    /// <summary>
    /// 
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ManagerSurveyResult
    {
        /// <summary>
        /// Gets or sets the deliverypoint number.
        /// </summary>
        /// <value>
        /// The deliverypoint number.
        /// </value>
        [JsonProperty]
        public int DeliverypointNumber 
        { get; set; }

        /// <summary>
        /// Gets or sets the created.
        /// </summary>
        /// <value>
        /// The created.
        /// </value>
        [JsonProperty]
        public string Created 
        { get; set; }

        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        [JsonProperty]
        public SurveyResult Result 
        { get; set; }
    }
}
