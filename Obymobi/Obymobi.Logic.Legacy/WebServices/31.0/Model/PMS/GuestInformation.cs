using System;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;
using Obymobi.Interfaces;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Use this message to check in the (first) guest in a room 
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "GuestInformation"), IncludeInCodeGeneratorForAndroid]
    public class GuestInformation : ModelBase, IPmsModel
    {
        public GuestInformation()
        {
            // On default all is allowed, will be overwritten anyway
            // by a PMS that supports this per Guest
            this.AllowViewFolio = true;
            this.AllowFolioPosting = true;
            this.AllowExpressCheckout = true;
        }

        [XmlIgnore]
        public OnSiteServerWebserviceJobType OnSiteServerWebserviceJobType { get; set; }

        [XmlIgnore]
        public string Name { get { return this.GetType().Name; } }

        /// <summary>
        /// The DeliverypointNumber, i.e. RoomNumber
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string DeliverypointNumber { get; set; }

        /// <summary>
        /// Occupied
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool Occupied { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Title { get; set; }

        /// <summary>
        /// Reservation number
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Unique Id for a Group booking
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string GroupReference { get; set; }

        /// <summary>
        /// Human readable description for the group the guest belongs to
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string GroupName { get; set; }

        /// <summary>
        /// Company affiliation for guest
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Company { get; set; }

        /// <summary>
        /// Guest name to display, Firstname
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string CustomerFirstname { get; set; }

        /// <summary>
        /// Guest name to display, Lastname
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string CustomerLastname { get; set; }

        /// <summary>
        /// GuestId
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string GuestId { get; set; }

        /// <summary>
        /// Guest name to display, Lastname Prefix
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string CustomerLastnamePrefix { get; set; }

        /// <summary>
        /// Home ZIP code for the guest
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Zipcode { get; set; }

        /// <summary>
        /// Password to associate with guest when secure access to a resource is required
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Password { get; set; }

        /// <summary>
        /// Allow or Disallow simple posting for this guest
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool AllowFolioPosting { get; set; }

        /// <summary>
        /// Indicates if the allowViewFolio is set
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool AllowViewFolioSet { get; set; }

        /// <summary>
        /// Allow or Disallow simple posting for this guest
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool AllowViewFolio { get; set; }

        /// <summary>
        /// Indicates if the allowExpressCheckout is set
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool AllowExpressCheckoutSet { get; set; }

        /// <summary>
        /// Allow or Disallow simple posting for this guest
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool AllowExpressCheckout { get; set; }

        /// <summary>
        /// Amount that can be ordered in total using Crave
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public decimal CreditLimit { get; set; }

        /// <summary>
        /// Guest's primary spoken language
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string LanguageCode { get; set; }

        /// <summary>
        /// The VIP state of the guest
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool Vip { get; set; }

        /// <summary>
        /// An error in retrieving the Guest Information
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Error { get; set; }

        /// <summary>
        /// Indicates wether the current guest has a voicemail message awaiting
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool VoicemailMessage { get; set; }

        /// <summary>
        /// Arrival
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public DateTime Arrival { get; set; }

        /// <summary>
        /// Departure
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public DateTime Departure { get; set; }

        /// <summary>
        /// ClassOfService
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public int ClassOfService { get; set; }

        /// <summary>
        /// DoNotDisturb
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool DoNotDisturb { get; set; }

        /// <summary>
        /// TV Setting
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public int TvSetting { get; set; }

        /// <summary>
        /// Minibar Setting
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public int MinibarSetting { get; set; }

        public override string ToString()
        {
            return string.Format("DeliverypointNumber: {0}, Occupied: {1}, ViewBill: {2}, ExpressCheckout: {3}, Arrival: {4}, Departure: {5}, DoNotDisturb: {6}, ClassOfService: {7}, TvSetting: {8}, VoiceMail: {9}, Error: {10}",
                this.DeliverypointNumber,
                this.Occupied,
                this.AllowViewFolio,
                this.AllowExpressCheckout,
                this.Arrival,
                this.Departure,
                this.DoNotDisturb,
                this.ClassOfService,
                this.TvSetting,
                this.VoicemailMessage,
                this.Error);
        }
    }
}
