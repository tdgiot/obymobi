using System;
using System.Xml.Serialization;
using Dionysos;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// TerminalType enums
    /// </summary>
    public enum TerminalType : int
    {
        /// <summary>
        /// The terminalType is OSS
        /// </summary>
        [StringValue("On-site Server")]
        OnSiteServer = 1,

        /// <summary>
        /// The terminalType is Console
        /// </summary>
        [StringValue("Console")]
        Console = 2,

        /// <summary>
        /// The terminalType is Demo
        /// </summary>
        [StringValue("Demo")]
        Demo = 100
    }

    /// <summary>
    /// Model class which represents a terminal
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Terminal"), IncludeInCodeGeneratorForAndroid]
    public class Terminal : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Terminal type
        /// </summary>
        public Terminal()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Terminal type using the specified parameters
        /// </summary>
        public Terminal(int TerminalId, int CompanyId, string Name)
        {
            this.TerminalId = TerminalId;
            this.CompanyId = CompanyId;
            this.Name = Name;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TerminalId
        { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the DeliverypointgroupId
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DeliverypointgroupId
        { get; set; }

        /// <summary>
        /// Gets or sets the currency of the company
        /// </summary>
        [XmlElement]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the company
        /// </summary>
        [XmlElement]
        public string CompanyName
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the company
        /// </summary>
        [XmlElement]
        public string ReceiptTypes
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the company
        /// </summary>
        [XmlElement]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the company
        /// </summary>
        [XmlElement]
        public int RequestInterval
        { get; set; }

        /// <summary>
        /// Gets or sets the interval to diagnose the service
        /// </summary>
        [XmlElement]
        public int DiagnoseInterval
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the company
        /// </summary>
        [XmlElement]
        public string PrinterName
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the company
        /// </summary>
        [XmlElement]
        public int CultureString
        { get; set; }

        /// <summary>
        /// Gets or sets the date and time of the last request
        /// </summary>
        [XmlElement]
        public DateTime LastRequest
        { get; set; }

        /// <summary>
        /// Gets or sets the date and time of the last request notified by SMS
        /// </summary>
        [XmlElement]
        public DateTime LastRequestNotifiedBySMS
        { get; set; }

        /// <summary>
        /// Determines that a Sms is required about the status of this Terminal
        /// </summary>
        [XmlElement]
        public bool SmsNotificationRequired
        { get; set; }

        /// <summary>
        /// Gets or sets the last status code
        /// </summary>
        [XmlElement]
        public int LastStatus
        { get; set; }

        /// <summary>
        /// Gets or sets the last status text
        /// </summary>
        [XmlElement]
        public string LastStatusText
        { get; set; }

        /// <summary>
        /// Gets or sets the last status message
        /// </summary>
        [XmlElement]
        public string LastStatusMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the current state
        /// </summary>
        public string CurrentState
        { get; set; }

        /// <summary>
        /// Gets or sets the team viewer ID.
        /// </summary>
        /// <value>The team viewer ID.</value>
        [XmlElement]
        public string TeamviewerId
        { get; set; }

        /// <summary>
        /// Gets or sets the maximum amount of days to keep the log files
        /// </summary>
        [XmlElement]
        public int MaxDaysLogHistory
        { get; set; }

        /// <summary>
        /// Order handling method for this terminal
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        public int HandlingMethod
        { get; set; }

        /// <summary>
        /// Enable/Disable the ordering service for this terminal
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public bool SuspendService
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the POS connector
        /// </summary>
        [XmlElement]
        public int POSConnectorType
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the POS connector
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PMSConnectorType
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether to synchronize with the POS
        /// </summary>
        [XmlElement]
        public bool SynchronizeWithPOSOnStart
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether to synchronize with the PMS
        /// </summary>
        [XmlElement]
        public bool SynchronizeWithPMSOnStart
        { get; set; }

        /// <summary>
        /// Gets or sets the amount maximum mount of status reports to print
        /// </summary>
        [XmlElement]
        public int MaxStatusReports
        { get; set; }

        /// <summary>
        /// Gets or sets the order process time threshold in minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MaxProcessTime
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether this terminal is active
        /// </summary>
        [XmlElement]
        public bool Active
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether this terminal is active
        /// </summary>
        [XmlElement]
        public bool UseMonitoring
        { get; set; }

        /// <summary>
        /// Gets or sets the notification type for new orders
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public int NotificationForNewOrder
        { get; set; }

        /// <summary>
        /// Gets or sets the notification type for overdue orders
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public int NotificationForOverdueOrder
        { get; set; }

        /// <summary>
        /// Gets or sets default announcement duration (console setting)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AnnouncementDuration
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value 1
        /// </summary>
        [XmlElement]
        public string PosValue1
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value2
        /// </summary>
        [XmlElement]
        public string PosValue2
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value3
        /// </summary>
        [XmlElement]
        public string PosValue3
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value4
        /// </summary>
        [XmlElement]
        public string PosValue4
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value5
        /// </summary>
        [XmlElement]
        public string PosValue5
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value6
        /// </summary>
        [XmlElement]
        public string PosValue6
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value7
        /// </summary>
        [XmlElement]
        public string PosValue7
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value8
        /// </summary>
        [XmlElement]
        public string PosValue8
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value9
        /// </summary>
        [XmlElement]
        public string PosValue9
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value10
        /// </summary>
        [XmlElement]
        public string PosValue10
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value11
        /// </summary>
        [XmlElement]
        public string PosValue11
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value 12
        /// </summary>
        [XmlElement]
        public string PosValue12
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value 13
        /// </summary>
        [XmlElement]
        public string PosValue13
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value 14
        /// </summary>
        [XmlElement]
        public string PosValue14
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value 15
        /// </summary>
        [XmlElement]
        public string PosValue15
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value 16
        /// </summary>
        [XmlElement]
        public string PosValue16
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value 17
        /// </summary>
        [XmlElement]
        public string PosValue17
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value 18
        /// </summary>
        [XmlElement]
        public string PosValue18
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value 19
        /// </summary>
        [XmlElement]
        public string PosValue19
        { get; set; }

        /// <summary>
        /// Gets or sets the POS configuration value 20
        /// </summary>
        [XmlElement]
        public string PosValue20
        { get; set; }

        /// <summary>
        /// Get or sets the id of the support pool for this terminal
        /// </summary>
        [XmlElement]
        public int SupportpoolId
        { get; set; }

        /// <summary>
        /// Get or sets the reset timeout
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ResetTimeout
        { get; set; }

        /// <summary>
        /// Gets or sets the browser 1
        /// </summary>
        [XmlElement]
        public int Browser1EntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the browser 2
        /// </summary>
        [XmlElement]
        public int Browser2EntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets cms page
        /// </summary>
        [XmlElement]
        public int CmsPageEntertainmentId
        { get; set; }

        /// <summary>
        /// Get or sets the forward terminal id 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ForwardToTerminalId
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether to show notifications if a survey has been submitted
        /// </summary>
        [XmlElement]
        public bool SurveyResultNotifications
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether to print orders
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PrintingEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the last communication method
        /// </summary>
        [XmlElement]
        public int LastCommunicationMethod
        { get; set; }

        /// <summary>
        /// Gets or sets the latest support tools version
        /// </summary>
        [XmlElement]
        public string LastSupportToolsVersion
        { get; set; }

        /// <summary>
        /// Gets or sets the latest agent version
        /// </summary>
        [XmlElement]
        public string LastAgentVersion
        { get; set; }

        /// <summary>
        /// Gets or sets the last support tools activity
        /// </summary>
        [XmlElement]
        public DateTime LastSupportToolsRequest
        { get; set; }

        /// <summary>
        /// Gets or sets the UI mode Id of the terminal
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int UIModeId
        { get; set; }

        /// <summary>
        /// Gets or sets the last support tools activity
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LinkedToDevice
        { get; set; }

        /// <summary>
        /// Gets or sets if this terminal has a master tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool HasMasterTab 
        { get; set; }

        /// <summary>
        /// Gets or sets the pincode for this terminal
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Pincode
        { get; set; }

        /// <summary>
        /// Gets or sets the pincode for the superuser for this terminal
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PincodeSU
        { get; set; }

        /// <summary>
        /// Gets or sets the pincode for godmode for this terminal
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PincodeGM
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int UseHardKeyboard { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MaxVolume { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PmsTerminalOfflineNotificationEnabled { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PmsActive { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PmsTerminalOnline { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LastPmsSync
        { get; set; }

        /// <summary>
        /// Gets or sets the announcement yes button actions
        /// </summary>
        [XmlArray("AnnouncementActions")]
        [XmlArrayItem("AnnouncementAction")]
        [IncludeInCodeGeneratorForAndroid]
        public AnnouncementAction[] AnnouncementActions
        { get; set; }

        /// <summary>
        /// Gets or sets the messagegroups
        /// </summary>
        [XmlArray("Messagegroups")]
        [XmlArrayItem("Messagegroup")]
        [IncludeInCodeGeneratorForAndroid]
        public Messagegroup[] Messagegroups
        { get; set; }

        /// <summary>
        /// Gets or sets the message templates of the terminal
        /// </summary>
        [XmlArray("MessageTemplates")]
        [XmlArrayItem("MessageTemplate")]
        [IncludeInCodeGeneratorForAndroid]
        public MessageTemplate[] MessageTemplates
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Terminal Clone()
        {
            return this.Clone<Terminal>();
        }

        #endregion
    }
}
