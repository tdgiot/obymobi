using System;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Model class which represents a guest group
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "GuestGroup"), IncludeInCodeGeneratorForAndroid]
    public class GuestGroup : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.GuestGroup type
        /// </summary>
        public GuestGroup()
        {
        }        

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the name of the guest group
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public GuestGroup Clone()
        {
            return this.Clone<GuestGroup>();
        }

        #endregion
    }
}
