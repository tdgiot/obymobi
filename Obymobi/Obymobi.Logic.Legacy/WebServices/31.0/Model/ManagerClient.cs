using Newtonsoft.Json;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// ManagerClient model
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ManagerClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManagerClient"/> class.
        /// </summary>
        public ManagerClient()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ManagerClient"/> class.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="name">The name.</param>
        /// <param name="operationMode">The operation mode.</param>
        /// <param name="online">if set to <c>true</c> [online].</param>
        /// <param name="batteryPercentage">The battery percentage.</param>
        public ManagerClient(int clientId, string name, int operationMode, bool online, int batteryPercentage)
        {
            this.ClientId = clientId;
            this.ClientName = name;
            this.OperationMode = operationMode;
            this.Online = online;
            this.BatteryPercentage = batteryPercentage;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        /// <value>
        /// The client id.
        /// </value>
        [JsonProperty]
        public int ClientId { get; set; }

        /// <summary>
        /// Gets or sets the name of the client.
        /// </summary>
        /// <value>
        /// The name of the client.
        /// </value>
        [JsonProperty]
        public string ClientName { get; set; }

        /// <summary>
        /// Gets or sets the operation mode.
        /// </summary>
        /// <value>
        /// The operation mode.
        /// </value>
        [JsonProperty]
        public int OperationMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ManagerClient"/> is online.
        /// </summary>
        /// <value>
        ///   <c>true</c> if online; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty]
        public bool Online { get; set; }

        /// <summary>
        /// Gets or sets the battery percentage.
        /// </summary>
        /// <value>
        /// The battery percentage.
        /// </value>
        [JsonProperty]
        public int BatteryPercentage { get; set; }

        #endregion
    }
}
