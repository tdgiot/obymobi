using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Model class which represents a company currency
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CompanyCurrency"), IncludeInCodeGeneratorForAndroid]
    public class CompanyCurrency : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CompanyCurrency type
        /// </summary>
        public CompanyCurrency()
        {

        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the company currency
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int CompanyCurrencyId
        { get; set; }

        /// <summary>
        /// Gets or sets the currency code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string CurrencyCode
        { get; set; }

        /// <summary>
        /// Gets or sets the currency symbol
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Symbol
        { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the exchange rate
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public decimal ExchangeRate
        { get; set; }        

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CompanyCurrency Clone()
        {
            return this.Clone<CompanyCurrency>();
        }

        #endregion
    }
}
