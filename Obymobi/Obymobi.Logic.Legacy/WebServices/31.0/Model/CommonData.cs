using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Model class which represents common data
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CommonData"), IncludeInCodeGeneratorForXamarin]
    public class CommonData : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CommonData type
        /// </summary>
        public CommonData()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the Ticks of the last moment the common data was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public long LastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the venue categories
        /// </summary>
        [XmlArray("VenueCategories")]
        [XmlArrayItem("VenueCategory")]
        [IncludeInCodeGeneratorForXamarin]
        public VenueCategory[] VenueCategories
        { get; set; }

        /// <summary>
        /// Gets or sets the amenities
        /// </summary>
        [XmlArray("Amenities")]
        [XmlArrayItem("Amenity")]
        [IncludeInCodeGeneratorForXamarin]
        public Amenity[] Amenities
        { get; set; }

        /// <summary>
        /// Gets or sets the action button
        /// </summary>
        [XmlArray("ActionButtons")]
        [XmlArrayItem("ActionButton")]
        [IncludeInCodeGeneratorForXamarin]
        public ActionButton[] ActionButtons
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CommonData Clone()
        {
            return this.Clone<CommonData>();
        }

        #endregion
    }
}
