using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Model class which represents a DeliveryTime
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "DeliveryTime"), IncludeInCodeGeneratorForAndroid]
    public class DeliveryTime : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.DeliveryTime type
        /// </summary>
        public DeliveryTime()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the deliverypointgroup id
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]        
        [IncludeInCodeGeneratorForAndroid]
        public int DeliverypointgroupId
        { get; set; }

        /// <summary>
        /// Gets or sets the time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Time
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public DeliveryTime Clone()
        {
            return this.Clone<DeliveryTime>();
        }

        #endregion
    }
}
