using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Model class which represents a company culture
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CompanyCulture"), IncludeInCodeGeneratorForAndroid]
    public class CompanyCulture : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CompanyCulture type
        /// </summary>
        public CompanyCulture()
        {

        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the company culture
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int CompanyCultureId
        { get; set; }

        /// <summary>
        /// Gets or sets the culture code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string CultureCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }
        
        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CompanyCulture Clone()
        {
            return this.Clone<CompanyCulture>();
        }

        #endregion
    }
}
