using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Model class which represents a media ratio item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "MediaRatioTypeMedia"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class MediaRatioTypeMedia : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.MediaRatioTypeMedia type
        /// </summary>
        public MediaRatioTypeMedia()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.MediaRatio type using the
        /// </summary>
        public MediaRatioTypeMedia(int MediaRatioTypeMediaId, int MediaId, string MediaRatioTypeSystemName, int Width, int Height)
        {
            this.MediaRatioTypeMediaId = MediaRatioTypeMediaId;
            this.MediaId = MediaId;
            this.MediaRatioTypeSystemName = MediaRatioTypeSystemName;
            this.Width = Width;
            this.Height = Height;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the media ratio item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int MediaRatioTypeMediaId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the media item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int MediaId
        { get; set; }

        /// <summary>
        /// Gets or sets the system name of the media ratio type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string MediaRatioTypeSystemName
        { get; set; }

        /// <summary>
        /// Gets or sets the width of the media ratio item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int Width
        { get; set; }

        /// <summary>
        /// Gets or sets the filepath relative to the media path
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int Height
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the media item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int MediaType
        { get; set; }

        /// <summary>
        /// Parent of this MediaRatioTypeMedia
        /// </summary>
        [XmlAttribute("Parent")]
        public string Parent 
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public MediaRatioTypeMedia Clone()
        {
            return this.Clone<MediaRatioTypeMedia>();            
        }

        #endregion
    }
}
