namespace Obymobi.Logic.Model.v31.Converters
{
	public class WakeUpStatusConverter : ModelConverterBase<Obymobi.Logic.Model.v31.WakeUpStatus, Obymobi.Logic.Model.WakeUpStatus>
	{
        public WakeUpStatusConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.WakeUpStatus ConvertModelToLegacyModel(Obymobi.Logic.Model.WakeUpStatus source)
        {
            Obymobi.Logic.Model.v31.WakeUpStatus target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.WakeUpStatus();
                target.OnSiteServerWebserviceJobType = source.OnSiteServerWebserviceJobType;
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.AccountNumber = source.AccountNumber;
                target.WakeUpDate = source.WakeUpDate;
                target.WakeUpTime = source.WakeUpTime;
                target.Status = source.Status;
                target.Info = source.Info;
            }

            return target;
        }

        public override Obymobi.Logic.Model.WakeUpStatus ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.WakeUpStatus source)
        {
            Obymobi.Logic.Model.WakeUpStatus target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.WakeUpStatus();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
