namespace Obymobi.Logic.Model.v31.Converters
{
	public class RoomDataConverter : ModelConverterBase<Obymobi.Logic.Model.v31.RoomData, Obymobi.Logic.Model.RoomData>
	{
        public RoomDataConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.RoomData ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomData source)
        {
            Obymobi.Logic.Model.v31.RoomData target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.RoomData();
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.Error = source.Error;
                target.VoicemailMessageSet = source.VoicemailMessageSet;
                target.VoicemailMessage = source.VoicemailMessage;
                target.ClassOfServiceSet = source.ClassOfServiceSet;
                target.ClassOfService = source.ClassOfService;
                target.DoNotDisturbSet = source.DoNotDisturbSet;
                target.DoNotDisturb = source.DoNotDisturb;
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomData ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.RoomData source)
        {
            Obymobi.Logic.Model.RoomData target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomData();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
