namespace Obymobi.Logic.Model.v31.Converters
{
	public class CategorySuggestionConverter : ModelConverterBase<Obymobi.Logic.Model.v31.CategorySuggestion, Obymobi.Logic.Model.CategorySuggestion>
	{
        public CategorySuggestionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.CategorySuggestion ConvertModelToLegacyModel(Obymobi.Logic.Model.CategorySuggestion source)
        {
            Obymobi.Logic.Model.v31.CategorySuggestion target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.CategorySuggestion();
                target.CategorySuggestionId = source.CategorySuggestionId;
                target.CategoryId = source.CategoryId;
                target.ProductId = source.ProductId;
                target.Checkout = source.Checkout;
            }

            return target;
        }

        public override Obymobi.Logic.Model.CategorySuggestion ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.CategorySuggestion source)
        {
            Obymobi.Logic.Model.CategorySuggestion target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.CategorySuggestion();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
