namespace Obymobi.Logic.Model.v31.Converters
{
	public class OrderRoutestephandlerSaveStatusConverter : ModelConverterBase<Obymobi.Logic.Model.v31.OrderRoutestephandlerSaveStatus, Obymobi.Logic.Model.OrderRoutestephandlerSaveStatus>
	{
        public OrderRoutestephandlerSaveStatusConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add(nameof(Model.OrderRoutestephandlerSaveStatus.ErrorMessage));

            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.OrderRoutestephandlerSaveStatus ConvertModelToLegacyModel(Obymobi.Logic.Model.OrderRoutestephandlerSaveStatus source)
        {
            Obymobi.Logic.Model.v31.OrderRoutestephandlerSaveStatus target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.OrderRoutestephandlerSaveStatus();
                target.OrderRoutestephandlerId = source.OrderRoutestephandlerId;
                target.OrderRoutestephandlerGuid = source.OrderRoutestephandlerGuid;
                target.Status = source.Status;
                target.Error = source.Error;
                target.OrderId = source.OrderId;
                target.OrderGuid = source.OrderGuid;
            }

            return target;
        }

        public override Obymobi.Logic.Model.OrderRoutestephandlerSaveStatus ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.OrderRoutestephandlerSaveStatus source)
        {
            Obymobi.Logic.Model.OrderRoutestephandlerSaveStatus target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.OrderRoutestephandlerSaveStatus();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
