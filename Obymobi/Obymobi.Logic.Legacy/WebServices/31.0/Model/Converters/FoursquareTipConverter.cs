namespace Obymobi.Logic.Model.v31.Converters
{
	public class FoursquareTipConverter : ModelConverterBase<Obymobi.Logic.Model.v31.FoursquareTip, Obymobi.Logic.Model.FoursquareTip>
	{
        public FoursquareTipConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.FoursquareTip ConvertModelToLegacyModel(Obymobi.Logic.Model.FoursquareTip source)
        {
            Obymobi.Logic.Model.v31.FoursquareTip target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.FoursquareTip();
                target.Name = source.Name;
                target.PostedOn = source.PostedOn;
                target.Text = source.Text;
            }

            return target;
        }

        public override Obymobi.Logic.Model.FoursquareTip ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.FoursquareTip source)
        {
            Obymobi.Logic.Model.FoursquareTip target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.FoursquareTip();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
