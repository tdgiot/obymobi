namespace Obymobi.Logic.Model.v31.Converters
{
	public class ActionButtonConverter : ModelConverterBase<Obymobi.Logic.Model.v31.ActionButton, Obymobi.Logic.Model.ActionButton>
	{
        public ActionButtonConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.ActionButton ConvertModelToLegacyModel(Obymobi.Logic.Model.ActionButton source)
        {
            Obymobi.Logic.Model.v31.ActionButton target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.ActionButton();
                target.ActionButtonId = source.ActionButtonId;
                target.Name = source.Name;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v31.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.ActionButton ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.ActionButton source)
        {
            Obymobi.Logic.Model.ActionButton target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.ActionButton();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
