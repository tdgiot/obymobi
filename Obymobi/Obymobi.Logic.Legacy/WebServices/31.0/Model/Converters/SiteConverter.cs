namespace Obymobi.Logic.Model.v31.Converters
{
	public class SiteConverter : ModelConverterBase<Obymobi.Logic.Model.v31.Site, Obymobi.Logic.Model.Site>
	{
        public SiteConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.Site ConvertModelToLegacyModel(Obymobi.Logic.Model.Site source)
        {
            Obymobi.Logic.Model.v31.Site target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.Site();
                target.SiteId = source.SiteId;
                target.Version = source.Version;
                target.Name = source.Name;
                target.SiteType = source.SiteType;
                target.LastModifiedTicks = source.LastModifiedTicks;

                if (source.Pages != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.PageConverter pagesConverter = new Obymobi.Logic.Model.v31.Converters.PageConverter();
                    target.Pages = (Page[])pagesConverter.ConvertArrayToLegacyArray(source.Pages);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v31.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v31.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Site ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.Site source)
        {
            Obymobi.Logic.Model.Site target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Site();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
