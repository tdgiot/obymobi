namespace Obymobi.Logic.Model.v31.Converters
{
	public class CustomTextConverter : ModelConverterBase<Obymobi.Logic.Model.v31.CustomText, Obymobi.Logic.Model.CustomText>
	{
        public CustomTextConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.CustomText ConvertModelToLegacyModel(Obymobi.Logic.Model.CustomText source)
        {
            Obymobi.Logic.Model.v31.CustomText target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.CustomText();
                target.CustomTextId = source.CustomTextId;
                target.LanguageCode = source.LanguageCode;
                target.Type = source.Type;
                target.Text = source.Text;
                target.CultureCode = source.CultureCode;
                target.ForeignKey = source.ForeignKey;
                target.Parent = source.Parent;
            }

            return target;
        }

        public override Obymobi.Logic.Model.CustomText ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.CustomText source)
        {
            Obymobi.Logic.Model.CustomText target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.CustomText();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
