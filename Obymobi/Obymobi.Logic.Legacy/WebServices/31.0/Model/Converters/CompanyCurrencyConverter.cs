namespace Obymobi.Logic.Model.v31.Converters
{
	public class CompanyCurrencyConverter : ModelConverterBase<Obymobi.Logic.Model.v31.CompanyCurrency, Obymobi.Logic.Model.CompanyCurrency>
	{
        public CompanyCurrencyConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.CompanyCurrency ConvertModelToLegacyModel(Obymobi.Logic.Model.CompanyCurrency source)
        {
            Obymobi.Logic.Model.v31.CompanyCurrency target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.CompanyCurrency();
                target.CompanyCurrencyId = source.CompanyCurrencyId;
                target.CurrencyCode = source.CurrencyCode;
                target.Symbol = source.Symbol;
                target.Name = source.Name;
                target.ExchangeRate = source.ExchangeRate;
            }

            return target;
        }

        public override Obymobi.Logic.Model.CompanyCurrency ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.CompanyCurrency source)
        {
            Obymobi.Logic.Model.CompanyCurrency target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.CompanyCurrency();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
