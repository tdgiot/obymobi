namespace Obymobi.Logic.Model.v31.Converters
{
	public class ActionConverter : ModelConverterBase<Obymobi.Logic.Model.v31.Action, Obymobi.Logic.Model.Action>
	{
        public ActionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.Action ConvertModelToLegacyModel(Obymobi.Logic.Model.Action source)
        {
            Obymobi.Logic.Model.v31.Action target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.Action();
                target.DeliverypointgroupId = source.DeliverypointgroupId;
                target.EntertainmentId = source.EntertainmentId;
                target.CategoryId = source.CategoryId;
                target.ProductId = source.ProductId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Action ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.Action source)
        {
            Obymobi.Logic.Model.Action target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Action();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
