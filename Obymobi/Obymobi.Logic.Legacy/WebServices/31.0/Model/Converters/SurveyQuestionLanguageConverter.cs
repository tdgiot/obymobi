namespace Obymobi.Logic.Model.v31.Converters
{
	public class SurveyQuestionLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v31.SurveyQuestionLanguage, Obymobi.Logic.Model.SurveyQuestionLanguage>
	{
        public SurveyQuestionLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.SurveyQuestionLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.SurveyQuestionLanguage source)
        {
            Obymobi.Logic.Model.v31.SurveyQuestionLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.SurveyQuestionLanguage();
                target.SurveyQuestionLanguageId = source.SurveyQuestionLanguageId;
                target.SurveyQuestionId = source.SurveyQuestionId;
                target.LanguageCode = source.LanguageCode;
                target.Question = source.Question;
            }

            return target;
        }

        public override Obymobi.Logic.Model.SurveyQuestionLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.SurveyQuestionLanguage source)
        {
            Obymobi.Logic.Model.SurveyQuestionLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SurveyQuestionLanguage();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
