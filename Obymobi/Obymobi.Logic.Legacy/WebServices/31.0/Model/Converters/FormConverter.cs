namespace Obymobi.Logic.Model.v31.Converters
{
	public class FormConverter : ModelConverterBase<Obymobi.Logic.Model.v31.Form, Obymobi.Logic.Model.Form>
	{
        public FormConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.Form ConvertModelToLegacyModel(Obymobi.Logic.Model.Form source)
        {
            Obymobi.Logic.Model.v31.Form target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.Form();
                target.FormId = source.FormId;
                target.Name = source.Name;
                target.Title = source.Title;
                target.Subtitle = source.Subtitle;
                target.SavingTitle = source.SavingTitle;
                target.SavingMessage = source.SavingMessage;
                target.ProcessedTitle = source.ProcessedTitle;
                target.ProcessedMessage = source.ProcessedMessage;

                if (source.Fields != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.FormFieldConverter fieldsConverter = new Obymobi.Logic.Model.v31.Converters.FormFieldConverter();
                    target.Fields = (FormField[])fieldsConverter.ConvertArrayToLegacyArray(source.Fields);
                }

                if (source.FormLanguages != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.FormLanguageConverter formLanguagesConverter = new Obymobi.Logic.Model.v31.Converters.FormLanguageConverter();
                    target.FormLanguages = (FormLanguage[])formLanguagesConverter.ConvertArrayToLegacyArray(source.FormLanguages);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v31.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Form ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.Form source)
        {
            Obymobi.Logic.Model.Form target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Form();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
