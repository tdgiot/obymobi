namespace Obymobi.Logic.Model.v31.Converters
{
	public class UIThemeTextSizeConverter : ModelConverterBase<Obymobi.Logic.Model.v31.UIThemeTextSize, Obymobi.Logic.Model.UIThemeTextSize>
	{
        public UIThemeTextSizeConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.UIThemeTextSize ConvertModelToLegacyModel(Obymobi.Logic.Model.UIThemeTextSize source)
        {
            Obymobi.Logic.Model.v31.UIThemeTextSize target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.UIThemeTextSize();
                target.UIThemeTextSizeId = source.UIThemeTextSizeId;
                target.UIThemeId = source.UIThemeId;
                target.Type = source.Type;
                target.TextSize = source.TextSize;
            }

            return target;
        }

        public override Obymobi.Logic.Model.UIThemeTextSize ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.UIThemeTextSize source)
        {
            Obymobi.Logic.Model.UIThemeTextSize target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UIThemeTextSize();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
