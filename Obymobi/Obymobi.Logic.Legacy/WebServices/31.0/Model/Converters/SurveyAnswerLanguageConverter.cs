namespace Obymobi.Logic.Model.v31.Converters
{
	public class SurveyAnswerLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v31.SurveyAnswerLanguage, Obymobi.Logic.Model.SurveyAnswerLanguage>
	{
        public SurveyAnswerLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.SurveyAnswerLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.SurveyAnswerLanguage source)
        {
            Obymobi.Logic.Model.v31.SurveyAnswerLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.SurveyAnswerLanguage();
                target.SurveyAnswerLanguageId = source.SurveyAnswerLanguageId;
                target.SurveyAnswerId = source.SurveyAnswerId;
                target.LanguageCode = source.LanguageCode;
                target.Answer = source.Answer;
            }

            return target;
        }

        public override Obymobi.Logic.Model.SurveyAnswerLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.SurveyAnswerLanguage source)
        {
            Obymobi.Logic.Model.SurveyAnswerLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SurveyAnswerLanguage();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
