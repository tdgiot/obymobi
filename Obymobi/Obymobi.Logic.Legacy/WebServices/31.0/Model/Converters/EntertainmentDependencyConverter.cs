namespace Obymobi.Logic.Model.v31.Converters
{
	public class EntertainmentDependencyConverter : ModelConverterBase<Obymobi.Logic.Model.v31.EntertainmentDependency, Obymobi.Logic.Model.EntertainmentDependency>
	{
        public EntertainmentDependencyConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.EntertainmentDependency ConvertModelToLegacyModel(Obymobi.Logic.Model.EntertainmentDependency source)
        {
            Obymobi.Logic.Model.v31.EntertainmentDependency target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.EntertainmentDependency();
                target.EntertainmentId = source.EntertainmentId;
                target.Name = source.Name;
                target.PackageName = source.PackageName;
                target.ClassName = source.ClassName;
                target.Filename = source.Filename;
                target.FileVersion = source.FileVersion;
            }

            return target;
        }

        public override Obymobi.Logic.Model.EntertainmentDependency ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.EntertainmentDependency source)
        {
            Obymobi.Logic.Model.EntertainmentDependency target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.EntertainmentDependency();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
