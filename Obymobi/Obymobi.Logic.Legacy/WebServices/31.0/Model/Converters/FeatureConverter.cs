namespace Obymobi.Logic.Model.v31.Converters
{
	public class FeatureConverter : ModelConverterBase<Obymobi.Logic.Model.v31.Feature, Obymobi.Logic.Model.Feature>
	{
        public FeatureConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.Feature ConvertModelToLegacyModel(Obymobi.Logic.Model.Feature source)
        {
            Obymobi.Logic.Model.v31.Feature target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.Feature();
                target.FeatureId = source.FeatureId;
                target.CompanyId = source.CompanyId;
                target.Name = source.Name;
                target.Code = source.Code;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Feature ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.Feature source)
        {
            Obymobi.Logic.Model.Feature target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Feature();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
