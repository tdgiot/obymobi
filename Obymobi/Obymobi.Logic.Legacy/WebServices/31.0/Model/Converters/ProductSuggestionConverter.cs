namespace Obymobi.Logic.Model.v31.Converters
{
	public class ProductSuggestionConverter : ModelConverterBase<Obymobi.Logic.Model.v31.ProductSuggestion, Obymobi.Logic.Model.ProductSuggestion>
	{
        public ProductSuggestionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.ProductSuggestion ConvertModelToLegacyModel(Obymobi.Logic.Model.ProductSuggestion source)
        {
            Obymobi.Logic.Model.v31.ProductSuggestion target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.ProductSuggestion();
                target.ProductSuggestionId = source.ProductSuggestionId;
                target.ProductId = source.ProductId;
                target.SuggestedProductId = source.SuggestedProductId;
                target.Direct = source.Direct;
                target.Checkout = source.Checkout;
                target.CategoryId = source.CategoryId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.ProductSuggestion ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.ProductSuggestion source)
        {
            Obymobi.Logic.Model.ProductSuggestion target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.ProductSuggestion();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
