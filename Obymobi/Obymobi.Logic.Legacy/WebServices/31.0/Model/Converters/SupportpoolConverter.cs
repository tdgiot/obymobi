namespace Obymobi.Logic.Model.v31.Converters
{
	public class SupportpoolConverter : ModelConverterBase<Obymobi.Logic.Model.v31.Supportpool, Obymobi.Logic.Model.Supportpool>
	{
        public SupportpoolConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.Supportpool ConvertModelToLegacyModel(Obymobi.Logic.Model.Supportpool source)
        {
            Obymobi.Logic.Model.v31.Supportpool target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.Supportpool();
                target.SupportpoolId = source.SupportpoolId;
                target.Name = source.Name;
                target.Phonenumber = source.Phonenumber;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Supportpool ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.Supportpool source)
        {
            Obymobi.Logic.Model.Supportpool target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Supportpool();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
