namespace Obymobi.Logic.Model.v31.Converters
{
	public class PriceLevelItemConverter : ModelConverterBase<Obymobi.Logic.Model.v31.PriceLevelItem, Obymobi.Logic.Model.PriceLevelItem>
	{
        public PriceLevelItemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.PriceLevelItem ConvertModelToLegacyModel(Obymobi.Logic.Model.PriceLevelItem source)
        {
            Obymobi.Logic.Model.v31.PriceLevelItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.PriceLevelItem();
                target.PriceLevelItemId = source.PriceLevelItemId;
                target.Price = source.Price;
                target.ProductId = source.ProductId;
                target.AlterationoptionId = source.AlterationoptionId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.PriceLevelItem ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.PriceLevelItem source)
        {
            Obymobi.Logic.Model.PriceLevelItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.PriceLevelItem();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
