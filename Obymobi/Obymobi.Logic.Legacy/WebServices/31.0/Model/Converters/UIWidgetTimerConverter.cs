namespace Obymobi.Logic.Model.v31.Converters
{
	public class UIWidgetTimerConverter : ModelConverterBase<Obymobi.Logic.Model.v31.UIWidgetTimer, Obymobi.Logic.Model.UIWidgetTimer>
	{
        public UIWidgetTimerConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.UIWidgetTimer ConvertModelToLegacyModel(Obymobi.Logic.Model.UIWidgetTimer source)
        {
            Obymobi.Logic.Model.v31.UIWidgetTimer target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.UIWidgetTimer();
                target.UIWidgetTimerId = source.UIWidgetTimerId;
                target.CountToTime = source.CountToTime;
                target.CountToDate = source.CountToDate;
            }

            return target;
        }

        public override Obymobi.Logic.Model.UIWidgetTimer ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.UIWidgetTimer source)
        {
            Obymobi.Logic.Model.UIWidgetTimer target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UIWidgetTimer();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
