namespace Obymobi.Logic.Model.v31.Converters
{
	public class OrderHourConverter : ModelConverterBase<Obymobi.Logic.Model.v31.OrderHour, Obymobi.Logic.Model.OrderHour>
	{
        public OrderHourConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.OrderHour ConvertModelToLegacyModel(Obymobi.Logic.Model.OrderHour source)
        {
            Obymobi.Logic.Model.v31.OrderHour target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.OrderHour();
                target.OrderHourId = source.OrderHourId;
                target.DayOfWeek = source.DayOfWeek;
                target.TimeStart = source.TimeStart;
                target.TimeEnd = source.TimeEnd;
            }

            return target;
        }

        public override Obymobi.Logic.Model.OrderHour ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.OrderHour source)
        {
            Obymobi.Logic.Model.OrderHour target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.OrderHour();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
