namespace Obymobi.Logic.Model.v31.Converters
{
	public class SocialmediaConverter : ModelConverterBase<Obymobi.Logic.Model.v31.Socialmedia, Obymobi.Logic.Model.Socialmedia>
	{
        public SocialmediaConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.Socialmedia ConvertModelToLegacyModel(Obymobi.Logic.Model.Socialmedia source)
        {
            Obymobi.Logic.Model.v31.Socialmedia target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.Socialmedia();
                target.SocialmediaId = source.SocialmediaId;
                target.SocialmediaType = source.SocialmediaType;
                target.CompanySocialmediaId = source.CompanySocialmediaId;
                target.Name = source.Name;
                target.Title = source.Title;
                target.Subtitle = source.Subtitle;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;
                target.Livestream = source.Livestream;
                target.SavingTitle = source.SavingTitle;
                target.SavingMessage = source.SavingMessage;
                target.ProcessedTitle = source.ProcessedTitle;
                target.ProcessedMessage = source.ProcessedMessage;

                if (source.SocialmediaLanguages != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.SocialmediaLanguageConverter socialmediaLanguagesConverter = new Obymobi.Logic.Model.v31.Converters.SocialmediaLanguageConverter();
                    target.SocialmediaLanguages = (SocialmediaLanguage[])socialmediaLanguagesConverter.ConvertArrayToLegacyArray(source.SocialmediaLanguages);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v31.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Socialmedia ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.Socialmedia source)
        {
            Obymobi.Logic.Model.Socialmedia target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Socialmedia();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
