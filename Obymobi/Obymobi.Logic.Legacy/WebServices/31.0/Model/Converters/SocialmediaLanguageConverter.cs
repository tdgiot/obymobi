namespace Obymobi.Logic.Model.v31.Converters
{
	public class SocialmediaLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v31.SocialmediaLanguage, Obymobi.Logic.Model.SocialmediaLanguage>
	{
        public SocialmediaLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.SocialmediaLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.SocialmediaLanguage source)
        {
            Obymobi.Logic.Model.v31.SocialmediaLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.SocialmediaLanguage();
                target.SocialmediaLanguageId = source.SocialmediaLanguageId;
                target.SocialmediaId = source.SocialmediaId;
                target.LanguageCode = source.LanguageCode;
                target.Name = source.Name;
                target.Title = source.Title;
                target.Subtitle = source.Subtitle;
                target.SavingTitle = source.SavingTitle;
                target.SavingMessage = source.SavingMessage;
                target.ProcessedTitle = source.ProcessedTitle;
                target.ProcessedMessage = source.ProcessedMessage;
            }

            return target;
        }

        public override Obymobi.Logic.Model.SocialmediaLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.SocialmediaLanguage source)
        {
            Obymobi.Logic.Model.SocialmediaLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SocialmediaLanguage();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
