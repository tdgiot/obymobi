namespace Obymobi.Logic.Model.v31.Converters
{
	public class PointOfInterestSynopsisConverter : ModelConverterBase<Obymobi.Logic.Model.v31.PointOfInterestSynopsis, Obymobi.Logic.Model.PointOfInterestSynopsis>
	{
        public PointOfInterestSynopsisConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.PointOfInterestSynopsis ConvertModelToLegacyModel(Obymobi.Logic.Model.PointOfInterestSynopsis source)
        {
            Obymobi.Logic.Model.v31.PointOfInterestSynopsis target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.PointOfInterestSynopsis();
                target.PointOfInterestId = source.PointOfInterestId;
                target.Name = source.Name;
                target.DescriptionSingleLine = source.DescriptionSingleLine;
                target.Latitude = source.Latitude;
                target.Longitude = source.Longitude;
                target.Addressline1 = source.Addressline1;
                target.Zipcode = source.Zipcode;
                target.City = source.City;
                target.BusinesshoursIntermediate = source.BusinesshoursIntermediate;
                target.Floor = source.Floor;
                target.DisplayDistance = source.DisplayDistance;

                if (source.VenueCategories != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.VenueCategoryConverter venueCategoriesConverter = new Obymobi.Logic.Model.v31.Converters.VenueCategoryConverter();
                    target.VenueCategories = (VenueCategory[])venueCategoriesConverter.ConvertArrayToLegacyArray(source.VenueCategories);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v31.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v31.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.PointOfInterestSynopsis ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.PointOfInterestSynopsis source)
        {
            Obymobi.Logic.Model.PointOfInterestSynopsis target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.PointOfInterestSynopsis();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
