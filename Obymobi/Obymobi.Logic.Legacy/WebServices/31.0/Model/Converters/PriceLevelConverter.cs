namespace Obymobi.Logic.Model.v31.Converters
{
	public class PriceLevelConverter : ModelConverterBase<Obymobi.Logic.Model.v31.PriceLevel, Obymobi.Logic.Model.PriceLevel>
	{
        public PriceLevelConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.PriceLevel ConvertModelToLegacyModel(Obymobi.Logic.Model.PriceLevel source)
        {
            Obymobi.Logic.Model.v31.PriceLevel target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.PriceLevel();
                target.PriceLevelId = source.PriceLevelId;
                target.Name = source.Name;

                if (source.PriceLevelItems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.PriceLevelItemConverter priceLevelItemsConverter = new Obymobi.Logic.Model.v31.Converters.PriceLevelItemConverter();
                    target.PriceLevelItems = (PriceLevelItem[])priceLevelItemsConverter.ConvertArrayToLegacyArray(source.PriceLevelItems);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.PriceLevel ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.PriceLevel source)
        {
            Obymobi.Logic.Model.PriceLevel target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.PriceLevel();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
