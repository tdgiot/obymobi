namespace Obymobi.Logic.Model.v31.Converters
{
	public class UIScheduleItemOccurrenceConverter : ModelConverterBase<Obymobi.Logic.Model.v31.UIScheduleItemOccurrence, Obymobi.Logic.Model.UIScheduleItemOccurrence>
	{
        public UIScheduleItemOccurrenceConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.UIScheduleItemOccurrence ConvertModelToLegacyModel(Obymobi.Logic.Model.UIScheduleItemOccurrence source)
        {
            Obymobi.Logic.Model.v31.UIScheduleItemOccurrence target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.UIScheduleItemOccurrence();
                target.UIScheduleItemOccurrenceId = source.UIScheduleItemOccurrenceId;
                target.StartTime = source.StartTime;
                target.EndTime = source.EndTime;
                target.Recurring = source.Recurring;
                target.RecurrenceType = source.RecurrenceType;
                target.RecurrenceRange = source.RecurrenceRange;
                target.RecurrenceStart = source.RecurrenceStart;
                target.RecurrenceEnd = source.RecurrenceEnd;
                target.RecurrenceOccurenceCount = source.RecurrenceOccurenceCount;
                target.RecurrencePeriodicity = source.RecurrencePeriodicity;
                target.RecurrenceDayNumber = source.RecurrenceDayNumber;
                target.RecurrenceWeekDays = source.RecurrenceWeekDays;
                target.RecurrenceWeekOfMonth = source.RecurrenceWeekOfMonth;
                target.RecurrenceMonth = source.RecurrenceMonth;
                target.RecurrenceIndex = source.RecurrenceIndex;
                target.Type = source.Type;
                target.ParentUIScheduleItemOccurrenceId = source.ParentUIScheduleItemOccurrenceId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.UIScheduleItemOccurrence ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.UIScheduleItemOccurrence source)
        {
            Obymobi.Logic.Model.UIScheduleItemOccurrence target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UIScheduleItemOccurrence();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
