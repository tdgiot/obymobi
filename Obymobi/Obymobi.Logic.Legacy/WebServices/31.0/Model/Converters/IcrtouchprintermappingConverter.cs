namespace Obymobi.Logic.Model.v31.Converters
{
	public class IcrtouchprintermappingConverter : ModelConverterBase<Obymobi.Logic.Model.v31.Icrtouchprintermapping, Obymobi.Logic.Model.Icrtouchprintermapping>
	{
        public IcrtouchprintermappingConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.Icrtouchprintermapping ConvertModelToLegacyModel(Obymobi.Logic.Model.Icrtouchprintermapping source)
        {
            Obymobi.Logic.Model.v31.Icrtouchprintermapping target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.Icrtouchprintermapping();
                target.IcrtouchprintermappingId = source.IcrtouchprintermappingId;
                target.TerminalId = source.TerminalId;
                target.Name = source.Name;

                if (source.IcrtouchprintermappingDeliverypoints != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.IcrtouchprintermappingDeliverypointConverter icrtouchprintermappingDeliverypointsConverter = new Obymobi.Logic.Model.v31.Converters.IcrtouchprintermappingDeliverypointConverter();
                    target.IcrtouchprintermappingDeliverypoints = (IcrtouchprintermappingDeliverypoint[])icrtouchprintermappingDeliverypointsConverter.ConvertArrayToLegacyArray(source.IcrtouchprintermappingDeliverypoints);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Icrtouchprintermapping ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.Icrtouchprintermapping source)
        {
            Obymobi.Logic.Model.Icrtouchprintermapping target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Icrtouchprintermapping();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
