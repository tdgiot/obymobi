namespace Obymobi.Logic.Model.v31.Converters
{
	public class AlterationoptionConverter : ModelConverterBase<Obymobi.Logic.Model.v31.Alterationoption, Obymobi.Logic.Model.Alterationoption>
	{
        public AlterationoptionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.Alterationoption ConvertModelToLegacyModel(Obymobi.Logic.Model.Alterationoption source)
        {
            Obymobi.Logic.Model.v31.Alterationoption target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.Alterationoption();
                target.AlterationoptionId = source.AlterationoptionId;
                target.Name = source.Name;
                target.PriceIn = source.PriceIn;
                target.Description = source.Description;
                target.PosproductId = source.PosproductId;
                target.IsProductRelated = source.IsProductRelated;

                if (source.Posalterationoption != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v31.Converters.PosalterationoptionConverter posalterationoptionConverter = new Obymobi.Logic.Model.v31.Converters.PosalterationoptionConverter();
                    target.Posalterationoption = (Posalterationoption)posalterationoptionConverter.ConvertModelToLegacyModel(source.Posalterationoption);
                }

                target.Type = source.Type;
                target.StartTime = source.StartTime;
                target.EndTime = source.EndTime;
                target.MinLeadMinutes = source.MinLeadMinutes;
                target.MaxLeadHours = source.MaxLeadHours;
                target.IntervalMinutes = source.IntervalMinutes;
                target.ShowDatePicker = source.ShowDatePicker;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v31.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Alterationoption ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.Alterationoption source)
        {
            Obymobi.Logic.Model.Alterationoption target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Alterationoption();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
