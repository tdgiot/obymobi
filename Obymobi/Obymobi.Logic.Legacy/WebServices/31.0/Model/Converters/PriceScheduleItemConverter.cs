namespace Obymobi.Logic.Model.v31.Converters
{
	public class PriceScheduleItemConverter : ModelConverterBase<Obymobi.Logic.Model.v31.PriceScheduleItem, Obymobi.Logic.Model.PriceScheduleItem>
	{
        public PriceScheduleItemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.PriceScheduleItem ConvertModelToLegacyModel(Obymobi.Logic.Model.PriceScheduleItem source)
        {
            Obymobi.Logic.Model.v31.PriceScheduleItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.PriceScheduleItem();
                target.PriceScheduleItemId = source.PriceScheduleItemId;
                target.PriceLevelId = source.PriceLevelId;

                if (source.PriceScheduleItemOccurrences != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.PriceScheduleItemOccurrenceConverter priceScheduleItemOccurrencesConverter = new Obymobi.Logic.Model.v31.Converters.PriceScheduleItemOccurrenceConverter();
                    target.PriceScheduleItemOccurrences = (PriceScheduleItemOccurrence[])priceScheduleItemOccurrencesConverter.ConvertArrayToLegacyArray(source.PriceScheduleItemOccurrences);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.PriceScheduleItem ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.PriceScheduleItem source)
        {
            Obymobi.Logic.Model.PriceScheduleItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.PriceScheduleItem();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
