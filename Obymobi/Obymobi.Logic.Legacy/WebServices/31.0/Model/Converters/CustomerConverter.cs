namespace Obymobi.Logic.Model.v31.Converters
{
	public class CustomerConverter : ModelConverterBase<Obymobi.Logic.Model.v31.Customer, Obymobi.Logic.Model.Customer>
	{
        public CustomerConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.Customer ConvertModelToLegacyModel(Obymobi.Logic.Model.Customer source)
        {
            Obymobi.Logic.Model.v31.Customer target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.Customer();
                target.CustomerId = source.CustomerId;
                target.Phonenumber = source.Phonenumber;
                target.Email = source.Email;
                target.Password = source.Password;
                target.Verified = source.Verified;
                target.Firstname = source.Firstname;
                target.Lastname = source.Lastname;
                target.LastnamePrefix = source.LastnamePrefix;
                target.Birthdate = source.Birthdate;
                target.GenderSingleLetter = source.GenderSingleLetter;
                target.Addressline1 = source.Addressline1;
                target.Addressline2 = source.Addressline2;
                target.Zipcode = source.Zipcode;
                target.City = source.City;
                target.FacebookId = source.FacebookId;
                target.FacebookToken = source.FacebookToken;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Customer ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.Customer source)
        {
            Obymobi.Logic.Model.Customer target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Customer();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
