namespace Obymobi.Logic.Model.v31.Converters
{
	public class ClientEntertainmentConverter : ModelConverterBase<Obymobi.Logic.Model.v31.ClientEntertainment, Obymobi.Logic.Model.ClientEntertainment>
	{
        public ClientEntertainmentConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.ClientEntertainment ConvertModelToLegacyModel(Obymobi.Logic.Model.ClientEntertainment source)
        {
            Obymobi.Logic.Model.v31.ClientEntertainment target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.ClientEntertainment();
                target.ClientId = source.ClientId;
                target.EntertainmentId = source.EntertainmentId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.ClientEntertainment ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.ClientEntertainment source)
        {
            Obymobi.Logic.Model.ClientEntertainment target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.ClientEntertainment();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
