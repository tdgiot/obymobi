namespace Obymobi.Logic.Model.v31.Converters
{
	public class DeliverypointgroupLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v31.DeliverypointgroupLanguage, Obymobi.Logic.Model.DeliverypointgroupLanguage>
	{
        public DeliverypointgroupLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.DeliverypointgroupLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.DeliverypointgroupLanguage source)
        {
            Obymobi.Logic.Model.v31.DeliverypointgroupLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.DeliverypointgroupLanguage();
                target.LanguageCode = source.LanguageCode;
                target.DeliverypointCaption = source.DeliverypointCaption;
            }

            return target;
        }

        public override Obymobi.Logic.Model.DeliverypointgroupLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.DeliverypointgroupLanguage source)
        {
            Obymobi.Logic.Model.DeliverypointgroupLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.DeliverypointgroupLanguage();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
