namespace Obymobi.Logic.Model.v31.Converters
{
	public class SocialmediaMessagePublicationConverter : ModelConverterBase<Obymobi.Logic.Model.v31.SocialmediaMessagePublication, Obymobi.Logic.Model.SocialmediaMessagePublication>
	{
        public SocialmediaMessagePublicationConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.SocialmediaMessagePublication ConvertModelToLegacyModel(Obymobi.Logic.Model.SocialmediaMessagePublication source)
        {
            Obymobi.Logic.Model.v31.SocialmediaMessagePublication target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.SocialmediaMessagePublication();
                target.SocialmediaMessagePublicationId = source.SocialmediaMessagePublicationId;
                target.SocialmediaMessageId = source.SocialmediaMessageId;
                target.CompanySocialmediaId = source.CompanySocialmediaId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.SocialmediaMessagePublication ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.SocialmediaMessagePublication source)
        {
            Obymobi.Logic.Model.SocialmediaMessagePublication target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SocialmediaMessagePublication();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
