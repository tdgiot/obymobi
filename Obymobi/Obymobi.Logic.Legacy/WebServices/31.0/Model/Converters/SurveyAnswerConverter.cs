namespace Obymobi.Logic.Model.v31.Converters
{
	public class SurveyAnswerConverter : ModelConverterBase<Obymobi.Logic.Model.v31.SurveyAnswer, Obymobi.Logic.Model.SurveyAnswer>
	{
        public SurveyAnswerConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.SurveyAnswer ConvertModelToLegacyModel(Obymobi.Logic.Model.SurveyAnswer source)
        {
            Obymobi.Logic.Model.v31.SurveyAnswer target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.SurveyAnswer();
                target.SurveyAnswerId = source.SurveyAnswerId;
                target.Answer = source.Answer;
                target.SortOrder = source.SortOrder;
                target.TargetSurveyQuestionId = source.TargetSurveyQuestionId;

                if (source.SurveyAnswerLanguages != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.SurveyAnswerLanguageConverter surveyAnswerLanguagesConverter = new Obymobi.Logic.Model.v31.Converters.SurveyAnswerLanguageConverter();
                    target.SurveyAnswerLanguages = (SurveyAnswerLanguage[])surveyAnswerLanguagesConverter.ConvertArrayToLegacyArray(source.SurveyAnswerLanguages);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.SurveyAnswer ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.SurveyAnswer source)
        {
            Obymobi.Logic.Model.SurveyAnswer target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SurveyAnswer();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
