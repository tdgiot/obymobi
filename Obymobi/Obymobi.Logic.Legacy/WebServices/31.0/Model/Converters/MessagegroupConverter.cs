namespace Obymobi.Logic.Model.v31.Converters
{
	public class MessagegroupConverter : ModelConverterBase<Obymobi.Logic.Model.v31.Messagegroup, Obymobi.Logic.Model.Messagegroup>
	{
        public MessagegroupConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.Messagegroup ConvertModelToLegacyModel(Obymobi.Logic.Model.Messagegroup source)
        {
            Obymobi.Logic.Model.v31.Messagegroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.Messagegroup();
                target.MessagegroupId = source.MessagegroupId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.GroupName = source.GroupName;
                target.DeliverypointNumbers = source.DeliverypointNumbers;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Messagegroup ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.Messagegroup source)
        {
            Obymobi.Logic.Model.Messagegroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Messagegroup();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
