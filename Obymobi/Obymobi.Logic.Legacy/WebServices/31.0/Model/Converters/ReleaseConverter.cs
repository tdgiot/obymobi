namespace Obymobi.Logic.Model.v31.Converters
{
	public class ReleaseConverter : ModelConverterBase<Obymobi.Logic.Model.v31.Release, Obymobi.Logic.Model.Release>
	{
        public ReleaseConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.Release ConvertModelToLegacyModel(Obymobi.Logic.Model.Release source)
        {
            Obymobi.Logic.Model.v31.Release target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.Release();
                target.ReleaseId = source.ReleaseId;
                target.Version = source.Version;
                target.Filename = source.Filename;
                target.Remarks = source.Remarks;
                target.Crc32 = source.Crc32;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Release ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.Release source)
        {
            Obymobi.Logic.Model.Release target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Release();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
