namespace Obymobi.Logic.Model.v31.Converters
{
	public class CapturedFormConverter : ModelConverterBase<Obymobi.Logic.Model.v31.CapturedForm, Obymobi.Logic.Model.CapturedForm>
	{
        public CapturedFormConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.CapturedForm ConvertModelToLegacyModel(Obymobi.Logic.Model.CapturedForm source)
        {
            Obymobi.Logic.Model.v31.CapturedForm target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.CapturedForm();
                target.CapturedFormId = source.CapturedFormId;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;
            }

            return target;
        }

        public override Obymobi.Logic.Model.CapturedForm ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.CapturedForm source)
        {
            Obymobi.Logic.Model.CapturedForm target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.CapturedForm();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
