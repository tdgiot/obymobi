namespace Obymobi.Logic.Model.v31.Converters
{
	public class MessageTemplateConverter : ModelConverterBase<Obymobi.Logic.Model.v31.MessageTemplate, Obymobi.Logic.Model.MessageTemplate>
	{
        public MessageTemplateConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.MessageTemplate ConvertModelToLegacyModel(Obymobi.Logic.Model.MessageTemplate source)
        {
            Obymobi.Logic.Model.v31.MessageTemplate target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.MessageTemplate();
                target.MessageTemplateId = source.MessageTemplateId;
                target.Name = source.Name;
                target.Title = source.Title;
                target.Message = source.Message;
                target.CompanyId = source.CompanyId;
                target.Duration = source.Duration;
                target.MediaId = source.MediaId;
                target.CategoryId = source.CategoryId;
                target.EntertainmentId = source.EntertainmentId;
                target.ProductId = source.ProductId;
                target.MessageLayoutType = source.MessageLayoutType;
            }

            return target;
        }

        public override Obymobi.Logic.Model.MessageTemplate ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.MessageTemplate source)
        {
            Obymobi.Logic.Model.MessageTemplate target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.MessageTemplate();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
