namespace Obymobi.Logic.Model.v31.Converters
{
	public class PageConverter : ModelConverterBase<Obymobi.Logic.Model.v31.Page, Obymobi.Logic.Model.Page>
	{
        public PageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.Page ConvertModelToLegacyModel(Obymobi.Logic.Model.Page source)
        {
            Obymobi.Logic.Model.v31.Page target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.Page();
                target.PageId = source.PageId;
                target.SiteId = source.SiteId;
                target.PageTemplateId = source.PageTemplateId;
                target.Name = source.Name;
                target.ParentPageId = source.ParentPageId;
                target.PageType = source.PageType;
                target.SortOrder = source.SortOrder;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v31.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.PageElements != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.PageElementConverter pageElementsConverter = new Obymobi.Logic.Model.v31.Converters.PageElementConverter();
                    target.PageElements = (PageElement[])pageElementsConverter.ConvertArrayToLegacyArray(source.PageElements);
                }

                if (source.Attachments != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.AttachmentConverter attachmentsConverter = new Obymobi.Logic.Model.v31.Converters.AttachmentConverter();
                    target.Attachments = (Attachment[])attachmentsConverter.ConvertArrayToLegacyArray(source.Attachments);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v31.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Page ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.Page source)
        {
            Obymobi.Logic.Model.Page target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Page();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
