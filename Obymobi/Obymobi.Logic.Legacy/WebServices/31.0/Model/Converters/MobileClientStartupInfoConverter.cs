namespace Obymobi.Logic.Model.v31.Converters
{
	public class MobileClientStartupInfoConverter : ModelConverterBase<Obymobi.Logic.Model.v31.MobileClientStartupInfo, Obymobi.Logic.Model.MobileClientStartupInfo>
	{
        public MobileClientStartupInfoConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.MobileClientStartupInfo ConvertModelToLegacyModel(Obymobi.Logic.Model.MobileClientStartupInfo source)
        {
            Obymobi.Logic.Model.v31.MobileClientStartupInfo target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.MobileClientStartupInfo();
                target.RequiresUpdate = source.RequiresUpdate;
                target.UpdateUrl = source.UpdateUrl;
                target.UpdateTitle = source.UpdateTitle;
                target.UpdateText = source.UpdateText;
                target.MotdTitle = source.MotdTitle;
                target.MotdText = source.MotdText;
            }

            return target;
        }

        public override Obymobi.Logic.Model.MobileClientStartupInfo ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.MobileClientStartupInfo source)
        {
            Obymobi.Logic.Model.MobileClientStartupInfo target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.MobileClientStartupInfo();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
