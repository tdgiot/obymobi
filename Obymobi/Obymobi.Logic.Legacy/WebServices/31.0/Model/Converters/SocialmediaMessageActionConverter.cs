namespace Obymobi.Logic.Model.v31.Converters
{
	public class SocialmediaMessageActionConverter : ModelConverterBase<Obymobi.Logic.Model.v31.SocialmediaMessageAction, Obymobi.Logic.Model.SocialmediaMessageAction>
	{
        public SocialmediaMessageActionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.SocialmediaMessageAction ConvertModelToLegacyModel(Obymobi.Logic.Model.SocialmediaMessageAction source)
        {
            Obymobi.Logic.Model.v31.SocialmediaMessageAction target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.SocialmediaMessageAction();
                target.SocialmediaMessageId = source.SocialmediaMessageId;
                target.Action = source.Action;
            }

            return target;
        }

        public override Obymobi.Logic.Model.SocialmediaMessageAction ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.SocialmediaMessageAction source)
        {
            Obymobi.Logic.Model.SocialmediaMessageAction target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SocialmediaMessageAction();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
