namespace Obymobi.Logic.Model.v31.Converters
{
	public class RoomControlConfigurationConverter : ModelConverterBase<Obymobi.Logic.Model.v31.RoomControlConfiguration, Obymobi.Logic.Model.RoomControlConfiguration>
	{
        public RoomControlConfigurationConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.RoomControlConfiguration ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomControlConfiguration source)
        {
            Obymobi.Logic.Model.v31.RoomControlConfiguration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.RoomControlConfiguration();
                target.RoomControlConfigurationId = source.RoomControlConfigurationId;
                target.Name = source.Name;

                if (source.RoomControlAreas != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.RoomControlAreaConverter roomControlAreasConverter = new Obymobi.Logic.Model.v31.Converters.RoomControlAreaConverter();
                    target.RoomControlAreas = (RoomControlArea[])roomControlAreasConverter.ConvertArrayToLegacyArray(source.RoomControlAreas);
                }

                if (source.InfraredConfigurations != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v31.Converters.InfraredConfigurationConverter infraredConfigurationsConverter = new Obymobi.Logic.Model.v31.Converters.InfraredConfigurationConverter();
                    target.InfraredConfigurations = (InfraredConfiguration[])infraredConfigurationsConverter.ConvertArrayToLegacyArray(source.InfraredConfigurations);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomControlConfiguration ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.RoomControlConfiguration source)
        {
            Obymobi.Logic.Model.RoomControlConfiguration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomControlConfiguration();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
