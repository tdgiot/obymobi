namespace Obymobi.Logic.Model.v31.Converters
{
	public class FormFieldLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v31.FormFieldLanguage, Obymobi.Logic.Model.FormFieldLanguage>
	{
        public FormFieldLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            //COPY_DEFAULT_FIELDS_IN_COPY_DEFAULT_FIELDS
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v31.FormFieldLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.FormFieldLanguage source)
        {
            Obymobi.Logic.Model.v31.FormFieldLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v31.FormFieldLanguage();
                target.FormFieldLanguageId = source.FormFieldLanguageId;
                target.FormFieldId = source.FormFieldId;
                target.LanguageCode = source.LanguageCode;
                target.Name = source.Name;
            }

            return target;
        }

        public override Obymobi.Logic.Model.FormFieldLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v31.FormFieldLanguage source)
        {
            Obymobi.Logic.Model.FormFieldLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.FormFieldLanguage();

                // Copy default values from new version
                //COPY_DEFAULT_FIELDS_IN_CONVERT_LEGACY_TO_MODEL

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
