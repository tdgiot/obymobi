using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Model class which represents a room control section item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "RoomControlSectionItem"), IncludeInCodeGeneratorForAndroid]
    public class RoomControlSectionItem : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.RoomControlSectionItem type
        /// </summary>
        public RoomControlSectionItem()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the room control section item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlSectionItemId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue1
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue2
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue3
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue4
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue5
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Visible
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int InfraredConfigurationId
        { get; set; }

        [IncludeInCodeGeneratorForAndroid]
        [XmlArray("StationLists")]
        [XmlArrayItem("StationList")]
        public StationList[] StationLists
        { get; set; }        

        /// <summary>
        /// Gets or sets the room control widgets
        /// </summary>
        [XmlArray("RoomControlWidgets")]
        [XmlArrayItem("RoomControlWidget")]
        [IncludeInCodeGeneratorForAndroid]
        public RoomControlWidget[] RoomControlWidgets
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the room control section item
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public RoomControlSectionItem Clone()
        {
            return this.Clone<RoomControlSectionItem>();
        }

        #endregion
    }
}
