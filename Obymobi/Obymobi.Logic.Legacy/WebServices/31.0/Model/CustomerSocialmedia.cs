using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Model class which represents a customerSocialmedia
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CustomerSocialmedia"), IncludeInCodeGeneratorForXamarin]
    public class CustomerSocialmedia : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CustomerSocialmedia type 
        /// </summary>
        public CustomerSocialmedia()
        {

        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the customerSocialmedia
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int CustomerSocialmediaId
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the customerSocialmedia
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int SocialmediaType
        { get; set; }

        /// <summary>
        /// Gets or sets the emailaddress of the customerSocialmedia
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Email
        { get; set; }

        /// <summary>
        /// Gets or sets the externalId of the customerSocialmedia
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string ExternalId
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CustomerSocialmedia Clone()
        {
            return this.Clone<CustomerSocialmedia>();
        }

        #endregion
    }
}
