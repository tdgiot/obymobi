using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Model class which represents a collection of information about a company on FourSquare
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "FoursquareCompany")]
    public class FoursquareCompany : ModelBase 
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.FoursquareCompany type
        /// </summary>
        public FoursquareCompany()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the name of this company
        /// </summary>
        [XmlElement]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the complete address line of this company
        /// </summary>
        [XmlElement]
        public string Address
        { get; set; }

        /// <summary>
        /// Gets or sets the total amount of checkins done to this company
        /// </summary>
        [XmlElement]
        public int TotalCheckins
        { get; set; }

        /// <summary>
        /// Gets or sets the total amount of people that checked in to this company
        /// </summary>
        [XmlElement]
        public int TotalPeople
        { get; set; }

        /// <summary>
        /// Gets or sets the total amount of checkins that the acting user has done to this company
        /// </summary>
        [XmlElement]
        public int OwnCheckins
        { get; set; }

        /// <summary>
        /// Gets or sets the company's categories
        /// </summary>
        [XmlArray("Categories")]
        [XmlArrayItem("Category")]
        public string[] Categories
        { get; set; }

        /// <summary>
        /// Gets or sets the last 5 tips posted to this company
        /// </summary>
        [XmlArray("Tips")]
        [XmlArrayItem("Tip")]
        public FoursquareTip[] Tips
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public FoursquareCompany Clone()
        {
            return this.Clone<FoursquareCompany>();
        }

        #endregion
    }
}
