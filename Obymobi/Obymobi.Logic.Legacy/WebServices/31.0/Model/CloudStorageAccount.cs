using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Model class which represents an cloud storage account
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CloudStorageAccount"), IncludeInCodeGeneratorForAndroid]
    public class CloudStorageAccount : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CloudStorageAccount type
        /// </summary>
        public CloudStorageAccount()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the cloud storage account
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int CloudStorageAccountId
        { get; set; }

        /// <summary>
        /// Gets or sets the type of this cloud storage account
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the name cloud storage account
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string AccountName
        { get; set; }

        /// <summary>
        /// Gets or sets the password cloud storage account
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string AccountPassword
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CloudStorageAccount Clone()
        {
            return this.Clone<CloudStorageAccount>();
        }

        #endregion
    }
}
