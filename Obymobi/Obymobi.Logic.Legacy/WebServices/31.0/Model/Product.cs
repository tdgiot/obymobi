using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Model class which represents a product
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Product"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForJavascript, IncludeInCodeGeneratorForXamarin]
    public class Product : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Product type
        /// </summary>
        public Product()
        {
            // GK If these rows cause performance issues, i am sorry, i was just to lazy to search how to init an emtpy array..
            //this.Alterations = new List<Alteration>().ToArray();
            //this.ProductSuggestions = new List<ProductSuggestion>().ToArray();
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Product type using the
        /// </summary>
        public Product(int ProductId, int GenericproductId, int CategoryId, string Name, string Description, int Type, decimal PriceIn, int VatPercentage)
        {
            this.ProductId = ProductId;
            this.GenericproductId = GenericproductId;
            this.CategoryId = CategoryId;
            this.Name = Name;
            this.Description = Description;
            this.Type = Type;
            this.PriceIn = PriceIn;
            this.VatPercentage = VatPercentage;
            //this.Alterations = new List<Alteration>().ToArray();
            //this.ProductSuggestions = new List<ProductSuggestion>().ToArray();				
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the generic product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForAndroid]
        public int GenericproductId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the brand product
        /// </summary>
        [XmlElement]        
        [IncludeInCodeGeneratorForAndroid]
        public int BrandProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int CategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the sub type of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        public int SubType
        { get; set; }

        /// <summary>
        /// Gets or sets the price of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public decimal PriceIn
        { get; set; }

        /// <summary>
        /// Gets or sets the price of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int VatPercentage
        { get; set; }

        /// <summary>
        /// Gets or sets the text color
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string TextColor
        { get; set; }

        /// <summary>
        /// Gets or sets the background color
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string BackgroundColor
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether the product should be displayed on the homepage
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        public bool DisplayOnHomepage
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether the product is rateable or not
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public bool Rateable
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether the product or service item can have free text comments
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public bool AllowFreeText
        { get; set; }

        /// <summary>
        /// Gets or sets the color of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Color
        { get; set; }

        /// <summary>
        /// Gets or sets the custom button text for this product
        /// </summary>
        [XmlElement]
        public string ButtonText 
        { get; set; }

        /// <summary>
        /// Gets or sets the customize button text for this product
        /// </summary>
        [XmlElement]
        public string CustomizeButtonText
        { get; set; }

        [XmlElement]        
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string WebTypeTabletUrl
        { get; set; }

        [XmlElement]        
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string WebTypeSmartphoneUrl
        { get; set; }

        /// <summary>
        /// Gets or sets the schedule id of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ScheduleId
        { get; set; }

        /// <summary>
        /// Gets or sets the view layout type
        /// </summary>
        [XmlElement]
        public int ViewLayoutType
        { get; set; }

        [XmlElement]
        public int DeliveryLocationType 
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not the price should be hidden
        /// </summary>
        [XmlElement]
        public bool HidePrice
        { get; set; }

        /// <summary>
        /// Gets or sets the visibility type
        /// </summary>
        [XmlElement]
        public int VisibilityType
        { get; set; }

        /// <summary>
        /// Gets or sets the productgroup of the product
        /// </summary>
        [XmlArray("Productgroups")]
        [XmlArrayItem("Productgroup")]
        [IncludeInCodeGeneratorForAndroid]
        public Productgroup[] Productgroup
        { get; set; }

        /// <summary>
        /// Gets or sets the alterations of the product
        /// </summary>
        [XmlArray("Alterations")]
        [XmlArrayItem("Alteration")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public Alteration[] Alterations
        { get; set; }

        /// <summary>
        /// Gets or sets the alterations of the product
        /// </summary>
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        public Media[] Media
        { get; set; }

        /// <summary>
        /// Gets or sets the suggested products of the product
        /// </summary>
        [XmlArray("ProductSuggestions")]
        [XmlArrayItem("ProductSuggestion")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public ProductSuggestion[] ProductSuggestions
        { get; set; }

        /// <summary>
        /// Gets or sets the suggested product ids.
        /// </summary>
        /// <value>
        /// The suggested product ids.
        /// </value>
        [XmlArray("SuggestedProductIds")]
        [XmlArrayItem("SuggestedProductId")]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        public int[] SuggestedProductIds
        { get; set; }

        /// <summary>
        /// Gets or sets the tags of the advertisement
        /// </summary>
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [XmlArray("AdvertisementTags")]
        [XmlArrayItem("AdvertisementTag")]
        public AdvertisementTag[] AdvertisementTags
        { get; set; }

        /// <summary>
        /// Gets or sets the ratings
        /// </summary>
        [XmlArray("Ratings")]
        [XmlArrayItem("Rating")]
        public Rating[] Ratings
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [XmlArray("Attachments")]
        [XmlArrayItem("Attachment")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public Attachment[] Attachments
        { get; set; }

        /// <summary>
        /// Gets or sets the posproduct.
        /// </summary>
        /// <value>
        /// The posproduct.
        /// </value>
        [XmlElement]
        public Posproduct Posproduct
        { get; set; }

        /// <summary>
        /// Gets or sets the category ids.
        /// </summary>
        /// <value>
        /// The category ids.
        /// </value>
        public int[] CategoryIds
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Product Clone()
        {
            return this.Clone<Product>();
        }

        #endregion
    }
}
