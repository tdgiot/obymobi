using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v31
{
    /// <summary>
    /// Model class which represents an itemschedule
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "UIScheduleItem"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile]
    public class UIScheduleItem : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.ItemSchedule type
        /// </summary>
        public UIScheduleItem()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the schedule item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int UIScheduleItemId
        { get; set; }

        /// <summary>
        /// Gets or sets the widget id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int UIWidgetId
        { get; set; }

        /// <summary>
        /// Gets or sets the media id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MediaId
        { get; set; }

        /// <summary>
        /// Gets or sets the schedule items
        /// </summary>
        [XmlArray("UIScheduleItemOccurrences")]
        [XmlArrayItem("UIScheduleItemOccurrence")]
        [IncludeInCodeGeneratorForAndroid]
        public UIScheduleItemOccurrence[] UIScheduleItemOccurrences
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UIScheduleItem Clone()
        {
            return this.Clone<UIScheduleItem>();
        }

        #endregion
    }
}
