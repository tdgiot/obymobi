using System;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v25
{
    /// <summary>
    /// Model class which represents an Otoucho client
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Client"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForJavascript]
    public class Client : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Client type
        /// </summary>
        public Client()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Client type using the specified parameters
        /// </summary>
        public Client(int ClientId, int CompanyId)
        {
            this.ClientId = ClientId;
            this.CompanyId = CompanyId;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the Otoucho client
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForJavascript]
        public int ClientId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the company owner username
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string CompanyOwnerUsername
        { get; set; }

        /// <summary>
        /// Gets or sets the company owner password
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string CompanyOwnerPassword
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForJavascript]
        public int CustomerId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string CustomerHashedPassword
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string CompanyName
        { get; set; }

        /// <summary>
        /// Gets or sets the date and time of the last request
        /// </summary>
        [XmlElement]
        public DateTime LastRequestUTC
        { get; set; }

        /// <summary>
        /// Gets or sets the date and time of the last request notified by SMS
        /// </summary>
        [XmlElement]
        public DateTime LastRequestNotifiedBySMSUTC
        { get; set; }


        /// <summary>
        /// Determines that a Sms is required about the status of this Terminal
        /// </summary>
        [XmlElement]
        public bool SmsNotificationRequired
        { get; set; }

        /// <summary>
        /// Gets or sets the last status code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public int LastStatus
        { get; set; }

        /// <summary>
        /// Gets or sets the last status text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string LastStatusText
        { get; set; }

        /// <summary>
        /// Gets or sets the current state
        /// </summary>
        [IncludeInCodeGeneratorForFlex]
        public string CurrentState
        { get; set; }

        /// <summary>
        /// Gets or sets the last status code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OperationMode
        { get; set; }

        /// <summary>
        /// Gets or sets the related Customer Fullname
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForJavascript]
        public string RelatedCustomerFullname
        { get; set; }

        /// <summary>
        /// Gets or sets the ping of life interval
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public int PingOfLifeInterval
        { get; set; }

        /// <summary>
        /// Gets or sets the interval after which the screen should turn off
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int ScreenTimeoutInteval
        { get; set; }

        /// <summary>
        /// Gets or sets if the device has to be activated manually before use
        /// </summary>
        [XmlElement]
        public bool DeviceActivationRequired
        { get; set; }

        /// <summary>
        /// Gets or sets if the a reorder notification should be displayed
        /// </summary>
        [XmlElement]
        public bool ReorderNotificationEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the reorder notification interval
        /// </summary>
        [XmlElement]
        public int ReorderNotificationInterval
        { get; set; }

        /// <summary>
        /// Gets or sets the reorder notification announcement id
        /// </summary>
        [XmlElement]
        public int ReorderNotificationAnnouncementId
        { get; set; }

        /// <summary>
        /// Gets or sets the locale
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public string Locale
        { get; set; }

        /// <summary>
        /// Gets or sets if the client should log to a file
        /// </summary>
        [XmlElement]
        public bool LogToFile
        { get; set; }

        /// <summary>
        /// Gets or sets the pincode
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public string Pincode
        { get; set; }

        /// <summary>
        /// Gets or sets the pincode
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PincodeSU
        { get; set; }

        /// <summary>
        /// Gets or sets the pincode
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PincodeGM
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates if deliverypoints are entered manually
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public bool UseManualDeliverypoint
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates if deliverypoints are encrypted
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public bool UseManualDeliverypointEncryption
        { get; set; }

        /// <summary>
        /// Gets or sets the pincode
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int EncryptionSalt
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates if the deliverypoint number is hidden
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public bool HideDeliverypointNumbers
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates if the prices are hidden
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public bool HidePrices
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates if the session is cleared on timeout
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public bool ClearSessionOnTimeout
        { get; set; }

        /// <summary>
        /// Gets or sets the timeout in minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int ResetTimeout
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates if categories should open in product_view by default
        /// </summary>
        [XmlElement]
        public bool DefaultProductFullView
        { get; set; }

        /// <summary>
        /// Gets or sets (SurveyMonkey) URL for marketing mode
        /// </summary>
        [XmlElement]
        public string MarketingSurveyUrl
        { get; set; }

        /// <summary>
        /// Gets or sets 1st URL for the hotel mode tabs
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string HotelUrl1
        { get; set; }

        /// <summary>
        /// Gets or sets the caption for the 1st URL for the hotel mode tabs 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string HotelUrl1Caption
        { get; set; }

        /// <summary>
        /// Gets or sets the zoom level for the 1st URL for the hotel mode tabs 
        /// </summary>
        [XmlElement]
        public int HotelUrl1Zoom
        { get; set; }

        /// <summary>
        /// Gets or sets 2nd URL for the hotel mode tabs
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string HotelUrl2
        { get; set; }

        /// <summary>
        /// Gets or sets the caption for the 2nd URL for the hotel mode tabs 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string HotelUrl2Caption
        { get; set; }

        /// <summary>
        /// Gets or sets the zoom level for the 2nd URL for the hotel mode tabs 
        /// </summary>
        [XmlElement]
        public int HotelUrl2Zoom
        { get; set; }

        /// <summary>
        /// Gets or sets 3rd URL for the hotel mode tabs
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string HotelUrl3
        { get; set; }

        /// <summary>
        /// Gets or sets the caption for the 3rd URL for the hotel mode tabs 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string HotelUrl3Caption
        { get; set; }

        /// <summary>
        /// Gets or sets the zoom level for the 3rd URL for the hotel mode tabs 
        /// </summary>
        [XmlElement]
        public int HotelUrl3Zoom
        { get; set; }

        /// <summary>
        /// Gets or sets the dull dim level in %
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DimLevelDull
        { get; set; }

        /// <summary>
        /// Gets or sets the medium dim level in %
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DimLevelMedium
        { get; set; }

        /// <summary>
        /// Gets or sets the bright dim level in %
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DimLevelBright
        { get; set; }

        /// <summary>
        /// Gets or sets the dull dim level in %
        /// </summary>
        [XmlElement]
        public int DockedDimLevelDull
        { get; set; }

        /// <summary>
        /// Gets or sets the medium dim level in %
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DockedDimLevelMedium
        { get; set; }

        /// <summary>
        /// Gets or sets the bright dim level in %
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DockedDimLevelBright
        { get; set; }

        /// <summary>
        /// Gets or sets the power save timeout in minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PowerSaveTimeout
        { get; set; }

        /// <summary>
        /// Gets or sets the power save dim level in %
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PowerSaveLevel
        { get; set; }

        /// <summary>
        /// Gets or sets battery out of charge notification level in %
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OutOfChargeLevel
        { get; set; }

        /// <summary>
        /// Gets or sets the out of charge message title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OutOfChargeTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the out of charge message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OutOfChargeMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the order processed message title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderProcessedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the order processed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderProcessedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the order failed message title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderFailedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the order failed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderFailedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the order saving title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderSavingTitle
        { get; set; }
        /// <summary>
        /// Gets or sets the order saving message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderSavingMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the order completed title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderCompletedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the order completed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderCompletedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the order completed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool OrderCompletedEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the service saving title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ServiceSavingTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the service saving message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ServiceSavingMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the service processed title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ServiceProcessedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the service processed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ServiceProcessedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the service failed message title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ServiceFailedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the service failed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ServiceFailedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the rating saving title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RatingSavingTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the rating saving message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RatingSavingMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the rating processed title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RatingProcessedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the rating processed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RatingProcessedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the ordering not available message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderingNotAvailableMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the daily order reset time (format: HHMM)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string DailyOrderReset
        { get; set; }

        /// <summary>
        /// Gets or sets the daily sleep time (format: HHMM)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string SleepTime
        { get; set; }

        /// <summary>
        /// Gets or sets the daily wake up time (format: HHMM)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string WakeUpTime
        { get; set; }

        /// <summary>
        /// Gets or sets if the order on the case message should be displayed or not
        /// </summary>
        [XmlElement]
        public bool OrderStatusOnthecaseEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the order on the case title
        /// </summary>
        [XmlElement]
        public string OrderStatusOnthecaseTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the order on the case message
        /// </summary>
        [XmlElement]
        public string OrderStatusOnthecaseMessage
        { get; set; }

        /// <summary>
        /// Gets or sets if the order complete message should be displayed or not
        /// </summary>
        [XmlElement]
        public bool OrderStatusCompleteEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the order complete title
        /// </summary>
        [XmlElement]
        public string OrderStatusCompleteTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the order complete message
        /// </summary>
        [XmlElement]
        public string OrderStatusCompleteMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the service request on the case title
        /// </summary>
        [XmlElement]
        public string ServiceRequestStatusOnTheCaseTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the service request on the case message
        /// </summary>
        [XmlElement]
        public string ServiceRequestStatusOnTheCaseMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the service request complete title
        /// </summary>
        [XmlElement]
        public string ServiceRequestStatusCompleteTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the service request complete message
        /// </summary>
        [XmlElement]
        public string ServiceRequestStatusCompleteMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the freeform message title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FreeformMessageTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the MAC address
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string MacAddress
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the deliverypoint 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DeliverypointId
        { get; set; }

        /// <summary>
        /// Gets or sets the number of the deliverypoint 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string DeliverypointNumber
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the deliverypoint group
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DeliverypointgroupId
        { get; set; }

        /// <summary>
        /// Gets or sets the WiFi security method
        /// </summary>
        [XmlElement]
        public int WifiSecurityMethod
        { get; set; }

        /// <summary>
        /// Gets or sets the WiFi SSID
        /// </summary>
        [XmlElement]
        public string WifiSsid
        { get; set; }

        /// <summary>
        /// Gets or sets the WiFi password
        /// </summary>
        [XmlElement]
        public string WifiPassword
        { get; set; }

        /// <summary>
        /// Gets or sets the WiFi password
        /// </summary>
        [XmlElement]
        public int WifiEapMode
        { get; set; }

        /// <summary>
        /// Gets or sets the WiFi Phase 2 Authentication
        /// </summary>
        [XmlElement]
        public int WifiPhase2Authentication
        { get; set; }

        /// <summary>
        /// Gets or sets the WiFi Identity
        /// </summary>
        [XmlElement]
        public string WifiIdentity
        { get; set; }

        /// <summary>
        /// Gets or sets the WiFi Anonymous Identify
        /// </summary>
        [XmlElement]
        public string WifiAnonymousIdentity
        { get; set; }

        /// <summary>
        /// Gets or sets the homepage slideshow interval
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int HomepageSlideshowInterval
        { get; set; }

        /// <summary>
        /// Gets or sets the default announcement duration
        /// </summary>
        [XmlElement]
        public int AnnouncementDuration
        { get; set; }

        /// <summary>
        /// Gets or sets the deliverypoint caption
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string DeliverypointCaption
        { get; set; }

        /// <summary>
        /// Gets or sets the UI mode for clients in this deliverypointgroup
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int UIModeId
        { get; set; }

        /// <summary>
        /// Gets or sets if clients in this deliverypointgroup can print
        /// </summary>
        [XmlElement]
        public bool PrintingEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the last communication method
        /// </summary>
        [XmlElement]
        public int LastCommunicationMethod
        { get; set; }

        /// <summary>
        /// Gets or sets the latest support tools version
        /// </summary>
        [XmlElement]
        public string LastSupportToolsVersion
        { get; set; }

        /// <summary>
        /// Gets or sets the latest agent version
        /// </summary>
        [XmlElement]
        public string LastAgentVersion
        { get; set; }

        /// <summary>
        /// Gets or sets the last cloud environment this client was connected to
        /// </summary>
        [XmlElement]
        public CloudEnvironment LastCloudEnvironment
        { get; set; }

        /// <summary>
        /// Gets or sets the last support tools activity
        /// </summary>
        [XmlElement]
        public DateTime LastSupportToolsRequest
        { get; set; }

        /// <summary>
        /// Gets or sets if ChargerRemovedDialog is enabled
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool IsChargerRemovedDialogEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the ChargerRemovedDialog title
        /// </summary>
        [XmlElement]
        public string ChargerRemovedDialogTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the ChargerRemovedDialog text
        /// </summary>
        [XmlElement]
        public string ChargerRemovedDialogText
        { get; set; }

        /// <summary>
        /// Gets or sets if ChargerRemovedReminderDialog is enabled
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool IsChargerRemovedReminderDialogEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the ChargerRemovedReminderDialog title
        /// </summary>
        [XmlElement]
        public string ChargerRemovedReminderDialogTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the ChargerRemovedDialog text
        /// </summary>
        [XmlElement]
        public string ChargerRemovedReminderDialogText
        { get; set; }

        [XmlElement]
        public bool UpdateEmenuDownloaded
        { get; set; }

        [XmlElement]
        public bool UpdateAgentDownloaded
        { get; set; }

        [XmlElement]
        public bool UpdateSupportToolsDownloaded
        { get; set; }

        /// <summary>
        /// Gets or sets PMS device locked dialog title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsDeviceLockedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets PMS device locked dialog text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsDeviceLockedText
        { get; set; }

        /// <summary>
        /// Gets or sets PMS checkin failed dialog title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsCheckinFailedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets PMS checkin failed dialog text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsCheckinFailedText
        { get; set; }

        /// <summary>
        /// Gets or sets PMS restart dialog title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsRestartTitle
        { get; set; }

        /// <summary>
        /// Gets or sets PMS restart dialog text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsRestartText
        { get; set; }

        /// <summary>
        /// Gets or sets PMS welcome dialog title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsWelcomeTitle
        { get; set; }

        /// <summary>
        /// Gets or sets PMS welcome dialog text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsWelcomeText
        { get; set; }

        /// <summary>
        /// Gets or sets PMS checkout approve text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsCheckoutApproveText
        { get; set; }

        /// <summary>
        /// Gets or sets the time before the PMS welcome dialog automatically closes in minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PmsWelcomeTimeoutMinutes
        { get; set; }

        /// <summary>
        /// Gets or sets PMS checkout complete dialog title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsCheckoutCompleteTitle
        { get; set; }

        /// <summary>
        /// Gets or sets PMS checkout complete dialog text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsCheckoutCompleteText
        { get; set; }                

        /// <summary>
        /// Gets or sets the google printer id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string GooglePrinterId
        { get; set; }

        /// <summary>
        /// Gets or sets the time before the PMS checkout complete dialog automatically closes in minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PmsCheckoutCompleteTimeoutMinutes
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ScreenOffMode { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PowerButtonSoftBehaviour { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PowerButtonHardBehaviour { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public decimal RoomserviceCharge { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool EmailDocumentEnabled { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string DefaultRoomControlAreaName { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ScreensaverMode { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string TurnOffPrivacyTitle { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string TurnOffPrivacyText { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ItemCurrentlyUnavailableText { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string AlarmSetWhileNotChargingTitle
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string AlarmSetWhileNotChargingText
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string DeliveryLocationMismatchTitle
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string DeliveryLocationMismatchText
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PriceScheduleId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool IsOrderitemAddedDialogEnabled
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool BrowserAgeVerificationEnabled { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int BrowserAgeVerificationLayout { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool RestartApplicationDialogEnabled { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool RebootDeviceDialogEnabled { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RestartTimeoutSeconds { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool CraveAnalytics { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string CraveAnalyticsSas { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string CraveAnalyticsUrl { get; set; }

        /// <summary>
        /// Gets or sets the media of the deliverypoint
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        /// <summary>
        /// Gets or sets the entertainment categories of the deliverypoint group
        /// </summary>
        [XmlArray("Entertainmentcategorys")]
        [XmlArrayItem("Entertainmentcategory")]
        [IncludeInCodeGeneratorForAndroid]
        public Entertainmentcategory[] Entertainmentcategories
        { get; set; }

        /// <summary>
        /// Gets or sets the UI Theme for this deliverypoint group
        /// </summary>
        [XmlArray("UIThemes")]
        [XmlArrayItem("UITheme")]
        [IncludeInCodeGeneratorForAndroid]
        public UITheme[] UIThemes
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]       
        public CustomText[] CustomTexts
        { get; set; }        

        #endregion

        #region Methods

        [XmlIgnore]
        public ClientCommunicationMethod LastCommunicationMethodEnum
        {
            get { return EnumUtil.ToEnum<ClientCommunicationMethod>(LastCommunicationMethod); }
            set { LastCommunicationMethod = (int)value; }
        }

        [XmlIgnore]
        public ScreenOffMode ScreenOffModeEnum
        {
            get { return ScreenOffMode.ToEnum<ScreenOffMode>(); }
            set { ScreenOffMode = (int)value; }
        }

        [XmlIgnore]
        public PowerButtonMode PowerButtonSoftModeEnum
        {
            get { return PowerButtonSoftBehaviour.ToEnum<PowerButtonMode>(); }
            set { PowerButtonSoftBehaviour = (int)value; }
        }

        [XmlIgnore]
        public PowerButtonMode PowerButtonHardModeEnum
        {
            get { return PowerButtonHardBehaviour.ToEnum<PowerButtonMode>(); }
            set { PowerButtonHardBehaviour = (int)value; }
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Client Clone()
        {
            return this.Clone<Client>();
        }

        #endregion
    }
}
