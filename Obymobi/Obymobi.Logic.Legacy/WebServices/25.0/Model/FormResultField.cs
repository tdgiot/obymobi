using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v25
{
    /// <summary>
    /// Model class which represents a form result field
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "FormResultField")]
    public class FormResultField : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.FormField type
        /// </summary>
        public FormResultField()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets of sets the id of the formr result field
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int FormResultFieldId
        { get; set;}

        /// <summary>
        /// Gets or sets the form result fk id of the field
        /// </summary>
        [XmlElement]
        public int FormResultId
        { get; set; }

        /// <summary>
        /// Gets or sets the form field fk id of the field
        /// </summary>
        [XmlElement]
        public int FormFieldId
        { get; set; }

        /// <summary>
        /// Gets or sets the value of the field
        /// </summary>
        [XmlElement]
        public string Value
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public FormResultField Clone()
        {
            return this.Clone<FormResultField>();
        }

        #endregion
    }
}
