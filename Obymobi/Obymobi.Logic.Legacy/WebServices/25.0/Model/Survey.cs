using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v25
{
    /// <summary>
    /// Model class which represents an survey
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Survey"), IncludeInCodeGeneratorForFlex]
    public class Survey : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Survey type
        /// </summary>
        public Survey()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the survey
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the saving title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string SavingTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the saving message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string SavingMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the processed title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ProcessedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the processed message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ProcessedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the answer required title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string AnswerRequiredTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the answer required message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string AnswerRequiredMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the pages of the survey
        /// </summary>
        [XmlArray("SurveyPages")]
        [XmlArrayItem("SurveyPage")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public SurveyPage[] SurveyPages
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        [XmlArray("SurveyLanguages")]
        [XmlArrayItem("SurveyLanguage")]
        public SurveyLanguage[] SurveyLanguages
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the survey
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Survey Clone()
        {
            return this.Clone<Survey>();
        }

        #endregion
    }
}
