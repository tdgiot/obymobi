using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v25
{
    /// <summary>
    /// Model class which represents a productgroup
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Productgroup"), IncludeInCodeGeneratorForAndroid]
    public class Productgroup : ModelBase
    {
        #region Constructors

        public Productgroup()
        { }

        #endregion

        #region Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int ProductgroupId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]        
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ColumnHeader
        { get; set; }

        [XmlArray("ProductgroupItems")]
        [XmlArrayItem("ProductgroupItem")]
        [IncludeInCodeGeneratorForAndroid]
        public ProductgroupItem[] ProductgroupItems
        { get; set; }

        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }

        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Productgroup Clone()
        {
            return this.Clone<Productgroup>();
        }

        #endregion
    }
}
