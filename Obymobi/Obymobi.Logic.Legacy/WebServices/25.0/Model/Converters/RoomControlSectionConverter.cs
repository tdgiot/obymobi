namespace Obymobi.Logic.Model.v25.Converters
{
	public class RoomControlSectionConverter : ModelConverterBase<Obymobi.Logic.Model.v25.RoomControlSection, Obymobi.Logic.Model.RoomControlSection>
	{
        public RoomControlSectionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.RoomControlSectionConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.RoomControlSection ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomControlSection source)
        {
            Obymobi.Logic.Model.v25.RoomControlSection target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.RoomControlSection();
                target.RoomControlSectionId = source.RoomControlSectionId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.Visible = source.Visible;

                if (source.RoomControlSectionItems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v25.Converters.RoomControlSectionItemConverter roomControlSectionItemsConverter = new Obymobi.Logic.Model.v25.Converters.RoomControlSectionItemConverter();
                    target.RoomControlSectionItems = (RoomControlSectionItem[])roomControlSectionItemsConverter.ConvertArrayToLegacyArray(source.RoomControlSectionItems);
                }

                if (source.RoomControlComponents != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v25.Converters.RoomControlComponentConverter roomControlComponentsConverter = new Obymobi.Logic.Model.v25.Converters.RoomControlComponentConverter();
                    target.RoomControlComponents = (RoomControlComponent[])roomControlComponentsConverter.ConvertArrayToLegacyArray(source.RoomControlComponents);
                }

                if (source.RoomControlWidgets != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v25.Converters.RoomControlWidgetConverter roomControlWidgetsConverter = new Obymobi.Logic.Model.v25.Converters.RoomControlWidgetConverter();
                    target.RoomControlWidgets = (RoomControlWidget[])roomControlWidgetsConverter.ConvertArrayToLegacyArray(source.RoomControlWidgets);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v25.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v25.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v25.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v25.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomControlSection ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.RoomControlSection source)
        {
            Obymobi.Logic.Model.RoomControlSection target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomControlSection();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.RoomControlSectionConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
