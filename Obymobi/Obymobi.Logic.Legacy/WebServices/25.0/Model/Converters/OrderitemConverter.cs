namespace Obymobi.Logic.Model.v25.Converters
{
	public class OrderitemConverter : ModelConverterBase<Obymobi.Logic.Model.v25.Orderitem, Obymobi.Logic.Model.Orderitem>
	{
        public OrderitemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.OrderitemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.Orderitem ConvertModelToLegacyModel(Obymobi.Logic.Model.Orderitem source)
        {
            Obymobi.Logic.Model.v25.Orderitem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.Orderitem();
                target.OrderitemId = source.OrderitemId;
                target.OrderId = source.OrderId;
                target.Guid = source.Guid;
                target.CategoryId = source.CategoryId;
                target.ProductId = source.ProductId;
                target.ProductName = source.ProductName;
                target.Quantity = source.Quantity;
                target.ProductPriceIn = source.ProductPriceIn;
                target.VatPercentage = source.VatPercentage;
                target.Notes = source.Notes;
                target.BenchmarkInformation = source.BenchmarkInformation;
                target.Color = source.Color;
                target.OrderSource = source.OrderSource;

                if (source.Alterationitems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v25.Converters.AlterationitemConverter alterationitemsConverter = new Obymobi.Logic.Model.v25.Converters.AlterationitemConverter();
                    target.Alterationitems = (Alterationitem[])alterationitemsConverter.ConvertArrayToLegacyArray(source.Alterationitems);
                }

                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;

                if (source.Posorderitem != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v25.Converters.PosorderitemConverter posorderitemConverter = new Obymobi.Logic.Model.v25.Converters.PosorderitemConverter();
                    target.Posorderitem = (Posorderitem)posorderitemConverter.ConvertModelToLegacyModel(source.Posorderitem);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Orderitem ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.Orderitem source)
        {
            Obymobi.Logic.Model.Orderitem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Orderitem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.OrderitemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
