namespace Obymobi.Logic.Model.v25.Converters
{
	public class RoomControlWidgetConverter : ModelConverterBase<Obymobi.Logic.Model.v25.RoomControlWidget, Obymobi.Logic.Model.RoomControlWidget>
	{
        public RoomControlWidgetConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.RoomControlWidgetConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.RoomControlWidget ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomControlWidget source)
        {
            Obymobi.Logic.Model.v25.RoomControlWidget target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.RoomControlWidget();
                target.RoomControlWidgetId = source.RoomControlWidgetId;
                target.Caption = source.Caption;
                target.Type = source.Type;
                target.SortOrder = source.SortOrder;
                target.RoomControlComponentId1 = source.RoomControlComponentId1;
                target.RoomControlComponentId2 = source.RoomControlComponentId2;
                target.RoomControlComponentId3 = source.RoomControlComponentId3;
                target.RoomControlComponentId4 = source.RoomControlComponentId4;
                target.RoomControlComponentId5 = source.RoomControlComponentId5;
                target.RoomControlComponentId6 = source.RoomControlComponentId6;
                target.RoomControlComponentId7 = source.RoomControlComponentId7;
                target.RoomControlComponentId8 = source.RoomControlComponentId8;
                target.RoomControlComponentId9 = source.RoomControlComponentId9;
                target.RoomControlComponentId10 = source.RoomControlComponentId10;
                target.InfraredConfigurationId = source.InfraredConfigurationId;
                target.Visible = source.Visible;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;
                target.Parent = source.Parent;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v25.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v25.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomControlWidget ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.RoomControlWidget source)
        {
            Obymobi.Logic.Model.RoomControlWidget target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomControlWidget();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.RoomControlWidgetConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
