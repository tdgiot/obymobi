namespace Obymobi.Logic.Model.v25.Converters
{
	public class RatingConverter : ModelConverterBase<Obymobi.Logic.Model.v25.Rating, Obymobi.Logic.Model.Rating>
	{
        public RatingConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.RatingConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.Rating ConvertModelToLegacyModel(Obymobi.Logic.Model.Rating source)
        {
            Obymobi.Logic.Model.v25.Rating target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.Rating();
                target.RatingId = source.RatingId;
                target.ProductId = source.ProductId;
                target.Value = source.Value;
                target.Visible = source.Visible;
                target.Comments = source.Comments;
                target.Created = source.Created;
                target.CreatedTime = source.CreatedTime;
                target.DeliverypointId = source.DeliverypointId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Rating ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.Rating source)
        {
            Obymobi.Logic.Model.Rating target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Rating();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.RatingConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
