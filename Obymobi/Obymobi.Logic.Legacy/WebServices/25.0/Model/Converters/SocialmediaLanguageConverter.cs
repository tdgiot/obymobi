namespace Obymobi.Logic.Model.v25.Converters
{
	public class SocialmediaLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v25.SocialmediaLanguage, Obymobi.Logic.Model.SocialmediaLanguage>
	{
        public SocialmediaLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.SocialmediaLanguageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.SocialmediaLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.SocialmediaLanguage source)
        {
            Obymobi.Logic.Model.v25.SocialmediaLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.SocialmediaLanguage();
                target.SocialmediaLanguageId = source.SocialmediaLanguageId;
                target.SocialmediaId = source.SocialmediaId;
                target.LanguageCode = source.LanguageCode;
                target.Name = source.Name;
                target.Title = source.Title;
                target.Subtitle = source.Subtitle;
                target.SavingTitle = source.SavingTitle;
                target.SavingMessage = source.SavingMessage;
                target.ProcessedTitle = source.ProcessedTitle;
                target.ProcessedMessage = source.ProcessedMessage;
            }

            return target;
        }

        public override Obymobi.Logic.Model.SocialmediaLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.SocialmediaLanguage source)
        {
            Obymobi.Logic.Model.SocialmediaLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SocialmediaLanguage();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.SocialmediaLanguageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
