namespace Obymobi.Logic.Model.v25.Converters
{
	public class UIThemeConverter : ModelConverterBase<Obymobi.Logic.Model.v25.UITheme, Obymobi.Logic.Model.UITheme>
	{
        public UIThemeConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.UIThemeConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.UITheme ConvertModelToLegacyModel(Obymobi.Logic.Model.UITheme source)
        {
            Obymobi.Logic.Model.v25.UITheme target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.UITheme();
                target.UIThemeId = source.UIThemeId;

                if (source.UIThemeColors != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v25.Converters.UIThemeColorConverter uIThemeColorsConverter = new Obymobi.Logic.Model.v25.Converters.UIThemeColorConverter();
                    target.UIThemeColors = (UIThemeColor[])uIThemeColorsConverter.ConvertArrayToLegacyArray(source.UIThemeColors);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v25.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v25.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.UITheme ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.UITheme source)
        {
            Obymobi.Logic.Model.UITheme target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UITheme();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.UIThemeConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
