namespace Obymobi.Logic.Model.v25.Converters
{
	public class GuestGroupConverter : ModelConverterBase<Obymobi.Logic.Model.v25.GuestGroup, Obymobi.Logic.Model.GuestGroup>
	{
        public GuestGroupConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.GuestGroupConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.GuestGroup ConvertModelToLegacyModel(Obymobi.Logic.Model.GuestGroup source)
        {
            Obymobi.Logic.Model.v25.GuestGroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.GuestGroup();
                target.Name = source.Name;
            }

            return target;
        }

        public override Obymobi.Logic.Model.GuestGroup ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.GuestGroup source)
        {
            Obymobi.Logic.Model.GuestGroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.GuestGroup();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.GuestGroupConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
