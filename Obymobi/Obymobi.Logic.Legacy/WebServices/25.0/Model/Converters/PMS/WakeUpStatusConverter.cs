namespace Obymobi.Logic.Model.v25.Converters
{
	public class WakeUpStatusConverter : ModelConverterBase<Obymobi.Logic.Model.v25.WakeUpStatus, Obymobi.Logic.Model.WakeUpStatus>
	{
        public WakeUpStatusConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.WakeUpStatusConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.WakeUpStatus ConvertModelToLegacyModel(Obymobi.Logic.Model.WakeUpStatus source)
        {
            Obymobi.Logic.Model.v25.WakeUpStatus target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.WakeUpStatus();
                target.OnSiteServerWebserviceJobType = source.OnSiteServerWebserviceJobType;
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.AccountNumber = source.AccountNumber;
                target.WakeUpDate = source.WakeUpDate;
                target.WakeUpTime = source.WakeUpTime;
                target.Status = source.Status;
                target.Info = source.Info;
            }

            return target;
        }

        public override Obymobi.Logic.Model.WakeUpStatus ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.WakeUpStatus source)
        {
            Obymobi.Logic.Model.WakeUpStatus target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.WakeUpStatus();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.WakeUpStatusConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
