namespace Obymobi.Logic.Model.v25.Converters
{
	public class FolioItemConverter : ModelConverterBase<Obymobi.Logic.Model.v25.FolioItem, Obymobi.Logic.Model.FolioItem>
	{
        public FolioItemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.FolioItemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.FolioItem ConvertModelToLegacyModel(Obymobi.Logic.Model.FolioItem source)
        {
            Obymobi.Logic.Model.v25.FolioItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.FolioItem();
                target.Code = source.Code;
                target.Description = source.Description;
                target.PriceIn = source.PriceIn;
                target.CurrencyCode = source.CurrencyCode;
                target.Created = source.Created;
                target.ReferenceId = source.ReferenceId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.FolioItem ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.FolioItem source)
        {
            Obymobi.Logic.Model.FolioItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.FolioItem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.FolioItemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
