namespace Obymobi.Logic.Model.v25.Converters
{
	public class FormFieldConverter : ModelConverterBase<Obymobi.Logic.Model.v25.FormField, Obymobi.Logic.Model.FormField>
	{
        public FormFieldConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.FormFieldConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.FormField ConvertModelToLegacyModel(Obymobi.Logic.Model.FormField source)
        {
            Obymobi.Logic.Model.v25.FormField target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.FormField();
                target.FormFieldId = source.FormFieldId;
                target.FormId = source.FormId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.Required = source.Required;
                target.Width = source.Width;

                if (source.FormFieldValues != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v25.Converters.FormFieldValueConverter formFieldValuesConverter = new Obymobi.Logic.Model.v25.Converters.FormFieldValueConverter();
                    target.FormFieldValues = (FormFieldValue[])formFieldValuesConverter.ConvertArrayToLegacyArray(source.FormFieldValues);
                }

                if (source.FormFieldLanguages != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v25.Converters.FormFieldLanguageConverter formFieldLanguagesConverter = new Obymobi.Logic.Model.v25.Converters.FormFieldLanguageConverter();
                    target.FormFieldLanguages = (FormFieldLanguage[])formFieldLanguagesConverter.ConvertArrayToLegacyArray(source.FormFieldLanguages);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.FormField ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.FormField source)
        {
            Obymobi.Logic.Model.FormField target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.FormField();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.FormFieldConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
