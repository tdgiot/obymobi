namespace Obymobi.Logic.Model.v25.Converters
{
	public class DiscountConverter : ModelConverterBase<Obymobi.Logic.Model.v25.Discount, Obymobi.Logic.Model.Discount>
	{
        public DiscountConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.DiscountConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.Discount ConvertModelToLegacyModel(Obymobi.Logic.Model.Discount source)
        {
            Obymobi.Logic.Model.v25.Discount target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.Discount();
                target.DiscountId = source.DiscountId;
                target.CompanyId = source.CompanyId;
                target.DiscountType = source.DiscountType;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;
                target.Description = source.Description;
                target.CategoryIds = source.CategoryIds;
                target.ProductIds = source.ProductIds;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Discount ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.Discount source)
        {
            Obymobi.Logic.Model.Discount target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Discount();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.DiscountConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
