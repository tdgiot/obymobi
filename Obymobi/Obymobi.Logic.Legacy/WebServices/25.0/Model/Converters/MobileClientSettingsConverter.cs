namespace Obymobi.Logic.Model.v25.Converters
{
	public class MobileClientSettingsConverter : ModelConverterBase<Obymobi.Logic.Model.v25.MobileClientSettings, Obymobi.Logic.Model.MobileClientSettings>
	{
        public MobileClientSettingsConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.MobileClientSettingsConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.MobileClientSettings ConvertModelToLegacyModel(Obymobi.Logic.Model.MobileClientSettings source)
        {
            Obymobi.Logic.Model.v25.MobileClientSettings target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.MobileClientSettings();
                target.MobileClientSettingsId = source.MobileClientSettingsId;
                target.SavedPhonenumber = source.SavedPhonenumber;
                target.SavedHashedPassword = source.SavedHashedPassword;
                target.RandomValue = source.RandomValue;
                target.HasSignedOnBefore = source.HasSignedOnBefore;
                target.LastTrace = source.LastTrace;
                target.LastPaymentMethodId = source.LastPaymentMethodId;
                target.LastDeliverypointNumber = source.LastDeliverypointNumber;
                target.LastCompanyId = source.LastCompanyId;
                target.KeepConnectionAlive = source.KeepConnectionAlive;
            }

            return target;
        }

        public override Obymobi.Logic.Model.MobileClientSettings ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.MobileClientSettings source)
        {
            Obymobi.Logic.Model.MobileClientSettings target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.MobileClientSettings();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.MobileClientSettingsConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
