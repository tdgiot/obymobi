namespace Obymobi.Logic.Model.v25.Converters
{
	public class FeatureConverter : ModelConverterBase<Obymobi.Logic.Model.v25.Feature, Obymobi.Logic.Model.Feature>
	{
        public FeatureConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.FeatureConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.Feature ConvertModelToLegacyModel(Obymobi.Logic.Model.Feature source)
        {
            Obymobi.Logic.Model.v25.Feature target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.Feature();
                target.FeatureId = source.FeatureId;
                target.CompanyId = source.CompanyId;
                target.Name = source.Name;
                target.Code = source.Code;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Feature ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.Feature source)
        {
            Obymobi.Logic.Model.Feature target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Feature();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.FeatureConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
