namespace Obymobi.Logic.Model.v25.Converters
{
	public class NetmessageConverter : ModelConverterBase<Obymobi.Logic.Model.v25.Netmessage, Obymobi.Logic.Model.Netmessage>
	{
        public NetmessageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v26.Converters.NetmessageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v25.Netmessage ConvertModelToLegacyModel(Obymobi.Logic.Model.Netmessage source)
        {
            Obymobi.Logic.Model.v25.Netmessage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v25.Netmessage();
                target.SaveToDatabase = source.SaveToDatabase;
                target.Submitted = source.Submitted;
                target.NetmessageId = source.NetmessageId;
                target.Guid = source.Guid;
                target.MessageVersion = source.MessageVersion;
                target.ReceiverIdentifier = source.ReceiverIdentifier;
                target.SenderIdentifier = source.SenderIdentifier;
                target.MessageType = source.MessageType;
                target.MessageTypeInt = source.MessageTypeInt;
                target.Status = source.Status;
                target.SenderCompanyId = source.SenderCompanyId;
                target.ReceiverCompanyId = source.ReceiverCompanyId;
                target.SenderClientId = source.SenderClientId;
                target.ReceiverClientId = source.ReceiverClientId;
                target.SenderTerminalId = source.SenderTerminalId;
                target.ReceiverTerminalId = source.ReceiverTerminalId;
                target.SenderDeliverypointId = source.SenderDeliverypointId;
                target.ReceiverDeliverypointId = source.ReceiverDeliverypointId;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;
                target.Created = source.Created;
                target.MessageLog = source.MessageLog;
                target.FieldValue11 = source.FieldValue11;
                target.FieldValue12 = source.FieldValue12;
                target.FieldValue13 = source.FieldValue13;
                target.FieldValue14 = source.FieldValue14;
                target.FieldValue15 = source.FieldValue15;
                target.FieldValue16 = source.FieldValue16;
                target.FieldValue17 = source.FieldValue17;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Netmessage ConvertLegacyModelToModel(Obymobi.Logic.Model.v25.Netmessage source)
        {
            Obymobi.Logic.Model.Netmessage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Netmessage();

                // Copy default values from new version
                new Obymobi.Logic.Model.v26.Converters.NetmessageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
