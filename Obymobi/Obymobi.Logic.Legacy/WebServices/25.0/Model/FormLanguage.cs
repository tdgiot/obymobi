using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v25
{
    /// <summary>
    /// Model class which represents a form language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "FormLanguage")]
    public class FormLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.FormLanguage type
        /// </summary>
        public FormLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the form language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int FormLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the form
        /// </summary>
        [XmlElement]
        public int FormId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the form
        /// </summary>
        [XmlElement]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the title of the form
        /// </summary>
        [XmlElement]
        public string Title
        { get; set; }

        /// <summary>
        /// Gets or sets the subtitle of the form
        /// </summary>
        [XmlElement]
        public string Subtitle
        { get; set; }

        /// <summary>
        /// Gets or sets the saving title
        /// </summary>
        [XmlElement]
        public string SavingTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the saving message
        /// </summary>
        [XmlElement]
        public string SavingMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the processed title
        /// </summary>
        [XmlElement]
        public string ProcessedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the processed message
        /// </summary>
        [XmlElement]
        public string ProcessedMessage
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public FormLanguage Clone()
        {
            return this.Clone<FormLanguage>();
        }

        #endregion
    }
}
