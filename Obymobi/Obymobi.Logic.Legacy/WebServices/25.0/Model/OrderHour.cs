using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v25
{
    /// <summary>
    /// Model class which represents an order hour
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "OrderHour"), IncludeInCodeGeneratorForFlex]
    public class OrderHour : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.OrderHour type
        /// </summary>
        public OrderHour()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the producthour
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        public int OrderHourId
        { get; set; }

        /// <summary>
        /// Gets or sets the day number
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public int DayOfWeek
        { get; set; }

        /// <summary>
        /// Gets or sets the start time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string TimeStart
        { get; set; }

        /// <summary>
        /// Gets or sets the end time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string TimeEnd
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public OrderHour Clone()
        {
            return this.Clone<OrderHour>();
        }

        #endregion
    }
}
