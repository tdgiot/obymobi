using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v25
{
	/// <summary>
	/// Model class which represents a category
	/// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Category"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForJavascript, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
	public class Category : ModelBase
	{
		#region Constructors

		/// <summary>
		/// Constructs an instance of the Obymobi.Logic.Model.Category type
		/// </summary>
		public Category()
		{
			this.ParentCategoryId = -1;
			this.GenericcategoryId = -1;
			this.ProductId = 0;
		}

		/// <summary>
		/// Constructs an instance of the Obymobi.Logic.Model.Category type using the specified parameters
		/// </summary>
		public Category(int CategoryId, int ParentCategoryId, int GenericcategoryId, string Name)
		{
			this.CategoryId = CategoryId;
			this.ParentCategoryId = ParentCategoryId;
			this.GenericcategoryId = GenericcategoryId;
			this.Name = Name;
			this.ProductId = 0;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets of sets the id of the category
		/// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public int CategoryId
		{ get; set; }

		/// <summary>
		/// Gets of sets the id of the parent category
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForJavascript]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public int ParentCategoryId
		{ get; set; }

		/// <summary>
		/// Gets of sets the id of the generic category
		/// </summary>
		[XmlElement]
		public int GenericcategoryId
		{ get; set; }

		/// <summary>
		/// Gets of sets the name of the category
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForJavascript]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public string Name
		{ get; set; }

        /// <summary>
        /// Gets or sets the description of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

		/// <summary>
		/// Gets of sets the sort order of the category
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public int SortOrder
		{ get; set; }

		/// <summary>
		/// Gets or sets the flag which indicates whether the category is rateable or not
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForMobile]
		public bool Rateable
		{ get; set; }

		/// <summary>
		/// Gets or sets the flag which indicates whether the category can be used as announcement action
		/// </summary>
		[XmlElement]
		public bool AnnouncementAction
		{ get; set; }

		/// <summary>
		/// Gets or sets the media of the category
		/// </summary>
		[XmlArray("Categories")]
		[XmlArrayItem("Category")]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public Category[] Categories
		{ get; set; }

		/// <summary>
		/// Gets or sets the media of the category
		/// </summary>
		[XmlArray("Products")]
		[XmlArrayItem("Product")]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public Product[] Products
		{ get; set; }

		/// <summary>
		/// Gets or sets the media of the category
		/// </summary>
		[XmlArray("Media")]
		[XmlArrayItem("Media")]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public Media[] Media
		{ get; set; }

		/// <summary>
		/// Gets or sets the tags of the category
		/// </summary>
		[XmlArray("AdvertisementTags")]
		[XmlArrayItem("AdvertisementTag")]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForMobile]
		public AdvertisementTag[] AdvertisementTags
		{ get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

		/// <summary>
		/// Gets or sets the poscategory.
		/// </summary>
		/// <value>
		/// The poscategory.
		/// </value>
		[XmlElement]
		public Poscategory Poscategory
		{ get; set; }

		/// <summary>
		/// Gets or sets the contained product ids.
		/// </summary>
		/// <value>
		/// The contained product ids.
		/// </value>
		[XmlArray("ContainedProductIds")]
		[XmlArrayItem("int")]
		public int[] ContainedProductIds
		{ get; set; }

		/// <summary>
		/// Gets or sets the product id.
		/// </summary>
		/// <value>
		/// The product id.
		/// </value>
		[XmlElement]
		public int ProductId
		{ get; set; }

		/// <summary>
		/// Gets or sets the type.
		/// </summary>
		/// <value>
		/// The type.
		/// </value>
		[XmlElement]
		[IncludeInCodeGeneratorForAndroid]
		[IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
		public int Type
		{ get; set; }

		/// <summary>
		/// Gets or sets the type.
		/// </summary>
		/// <value>
		/// The type.
		/// </value>
		[XmlElement]
		[IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
		public bool HidePrices
		{ get; set; }

        /// <summary>
        /// Gets or sets the view layout type
        /// </summary>
        [XmlElement]
        public int ViewLayoutType
        { get; set; }

        /// <summary>
        /// Gets or sets the delivery location type
        /// </summary>
        [XmlElement]
        public int DeliveryLocationType
        { get; set; }

        /// <summary>
        /// Gets or sets the visibility type
        /// </summary>
        [XmlElement]
        public int VisibilityType
        { get; set; }

        /// <summary>
        /// Gets or sets the schedule ID
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ScheduleId
        { get; set; }

		#endregion

		#region Methods

		/// <summary>
		/// Clones this instance.
		/// </summary>
		/// <returns></returns>
		public Category Clone()
		{
			return this.Clone<Category>();
		}

		#endregion
	}
}
