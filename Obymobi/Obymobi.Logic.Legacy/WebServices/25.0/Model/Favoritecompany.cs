using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v25
{
    /// <summary>
    /// Model class which represents a favorite company
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Favoritecompany"), IncludeInCodeGeneratorForMobile]
    public class Favoritecompany : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Favoritecompany type
        /// </summary>
        public Favoritecompany()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the favorite company
        /// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
        public int FavoritecompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the customer
        /// </summary>
        [XmlElement]
        public int CustomerId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public int CompanyId
        { get; set; }

		/// <summary>
		/// Gets or sets the id of the company
		/// </summary>
		[XmlElement]
        [IncludeInCodeGeneratorForJavascript]
		public string CompanyName
		{ get; set; }

		/// <summary>
		/// Gets or sets the id of the company
		/// </summary>
		[XmlElement]
		public string Code
		{ get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Favoritecompany Clone()
        {
            return this.Clone<Favoritecompany>();
        }

        #endregion
    }
}
