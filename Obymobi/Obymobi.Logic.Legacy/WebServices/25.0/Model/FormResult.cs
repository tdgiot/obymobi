using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v25
{
    /// <summary>
    /// Model class which represents a form result
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "FormResult")]
    public class FormResult : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Form type
        /// </summary>
        public FormResult()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets of sets the id of the form result
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int FormResultId
        { get; set;}

        /// <summary>
        /// Gets or sets the form fk id of the form result
        /// </summary>
        [XmlElement]
        public int FormId
        { get; set; }

        /// <summary>
        /// Gets or sets the result fields of the form result
        /// </summary>
        [XmlArray("ResultFields")]
        [XmlArrayItem("FormResultField")]
        public FormResultField[] ResultFields
        { get; set; }

    
        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public FormResult Clone()
        {
            return this.Clone<FormResult>();
        }

        #endregion
    }
}
