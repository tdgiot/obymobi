using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v25
{
    /// <summary>
    /// Model class which represents an survey page
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "SurveyPage"), IncludeInCodeGeneratorForFlex]
    public class SurveyPage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.SurveyPage type
        /// </summary>
        public SurveyPage()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the survey page
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyPageId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the survey page
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of the survey page
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets the questions of the survey page
        /// </summary>
        [XmlArray("SurveyQuestions")]
        [XmlArrayItem("SurveyQuestion")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public SurveyQuestion[] SurveyQuestions
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the survey page
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public SurveyPage Clone()
        {
            return this.Clone<SurveyPage>();
        }

        #endregion
    }
}
