using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v25
{
	/// <summary>
	/// Model class for the Order State
	/// </summary>
	[Serializable, XmlRootAttribute(ElementName = "OrderState")]
    public class OrderState : ModelBase
	{
        /// <summary>
        /// Gets or sets the order id.
        /// </summary>
        /// <value>The order id.</value>
		[XmlElement]
		public int OrderId
		{ get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
		[XmlElement]
		public int Status
		{ get; set; }

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public OrderState Clone()
        {
            return this.Clone<OrderState>();
        }

        #endregion
	}
}
