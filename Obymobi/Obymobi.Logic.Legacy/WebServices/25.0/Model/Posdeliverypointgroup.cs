using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v25
{
    /// <summary>
    /// Model class which represents a point-of-sale deliverypointgroup
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Posdeliverypointgroup")]
    public class Posdeliverypointgroup : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Posdeliverypointgroup type
        /// </summary>
        public Posdeliverypointgroup()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Posdeliverypoint type using the specified parameters
        /// </summary>
        public Posdeliverypointgroup(int PosdeliverypointgroupId, int CompanyId, string ExternalId, string Name)
        {
            this.PosdeliverypointgroupId = PosdeliverypointgroupId;
            this.CompanyId = CompanyId;
            this.ExternalId = ExternalId;
            this.Name = Name;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the deliverypointgroup
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int PosdeliverypointgroupId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the external id of the pos deliverypoint
        /// </summary>
        [XmlElement]
        public string ExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the pos deliverypoint
        /// </summary>
        [XmlElement]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }


        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Posdeliverypointgroup Clone()
        {
            return this.Clone<Posdeliverypointgroup>();
        }

        #endregion
    }
}
