using Newtonsoft.Json;

namespace Obymobi.Logic.Model.v25
{
    /// <summary>
    /// ManagerMessage model
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ManagerMessage
    {
        #region Properties

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        [JsonProperty]
        public string Title 
        { get; set; }

        /// <summary>
        /// Gets or sets the subtitle.
        /// </summary>
        /// <value>
        /// The subtitle.
        /// </value>
        [JsonProperty]
        public string Subtitle 
        { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        [JsonProperty]
        public string Message 
        { get; set; }

        /// <summary>
        /// Gets or sets the client ids.
        /// </summary>
        /// <value>
        /// The client ids.
        /// </value>
        [JsonProperty]
        public int[] ClientIds
        { get; set; }

        #endregion
    }
}
