using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v25
{
    /// <summary>
    /// Model class which represents a schedule
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Schedule"), IncludeInCodeGeneratorForXamarin]
    public class Schedule : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Schedule type
        /// </summary>
        public Schedule()
        {
        }        

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the schedule
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int ScheduleId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the schedule
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the items of the schedule
        /// </summary>
        [XmlArray("Scheduleitems")]
        [XmlArrayItem("Scheduleitem")]
        [IncludeInCodeGeneratorForXamarin]
        public Scheduleitem[] Scheduleitems
        { get; set; }    

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Schedule Clone()
        {
            return this.Clone<Schedule>();
        }

        #endregion
    }
}
