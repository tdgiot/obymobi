using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v25
{
    /// <summary>
    /// Model class which represents a company currency
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CompanyCurrency"), IncludeInCodeGeneratorForXamarin]
    public class CompanyCurrency : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CompanyCurrency type
        /// </summary>
        public CompanyCurrency()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the company currency
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int CompanyCurrencyId
        { get; set; }

        /// <summary>
        /// Gets or sets the currency code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string CurrencyCode
        { get; set; }

        /// <summary>
        /// Gets or sets the symbol
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Symbol
        { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the exchange rate
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public decimal ExchangeRate
        { get; set; }
      
        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CompanyCurrency Clone()
        {
            return this.Clone<CompanyCurrency>();
        }

        #endregion
    }
}
