using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.Mobile.v25
{
    /// <summary>
    /// Model class which represents a company for the mobile solution
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Site"), IncludeInCodeGeneratorForXamarin]
    public class Site : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Site type
        /// </summary>
        public Site()
        {
        }

        #endregion

        #region Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int SiteId
        { get; set; }

        [IncludeInCodeGeneratorForXamarin]
        public int Version
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SiteType
        { get; set; }

        [XmlArray("Pages")]
        [XmlArrayItem("Page")]
        [IncludeInCodeGeneratorForXamarin]
        public Page[] Pages
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public long LastModifiedTicks { get; set; }

        [IncludeInCodeGeneratorForXamarin]
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        public Media[] Media
        { get; set; }        

        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        public Site Clone()
        {
            return this.Clone<Site>();
        }

        public Site CloneUsingBinaryFormatter()
        {
            return this.CloneUsingBinaryFormatter<Site>();
        }

        #endregion
    }
}
