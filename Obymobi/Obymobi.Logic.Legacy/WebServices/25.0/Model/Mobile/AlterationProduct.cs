using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v25
{
    /// <summary>
    /// Model class which represents an alteration product
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "AlterationProduct"), IncludeInCodeGeneratorForXamarin]
    public class AlterationProduct : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.AlterationProduct type
        /// </summary>
        public AlterationProduct()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the alteration product
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the alteration id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationId
        { get; set; }

        /// <summary>
        /// Gets or sets the visible flag for the alteration product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool Visible
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the category
        /// </summary>
        [XmlArray("Products")]
        [XmlArrayItem("Product")]
        [IncludeInCodeGeneratorForXamarin]
        public Product[] Products
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public AlterationProduct Clone()
        {
            return this.Clone<AlterationProduct>();
        }

        #endregion
    }
}
