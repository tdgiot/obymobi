using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v25.Converters
{
	public class PageElementConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v25.PageElement, Obymobi.Logic.Model.Mobile.PageElement>
	{
        public PageElementConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v26.Converters.PageElementConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v25.PageElement ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.PageElement source)
        {
            Obymobi.Logic.Model.Mobile.v25.PageElement target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v25.PageElement();
                target.PageElementId = source.PageElementId;
                target.PageId = source.PageId;
                target.SystemName = source.SystemName;
                target.LanguageCode = source.LanguageCode;
                target.PageElementType = source.PageElementType;
                target.PageType = source.PageType;
                target.SortOrder = source.SortOrder;
                target.StringValue1 = source.StringValue1;
                target.StringValue2 = source.StringValue2;
                target.StringValue3 = source.StringValue3;
                target.StringValue4 = source.StringValue4;
                target.StringValue5 = source.StringValue5;
                target.IntValue1 = source.IntValue1;
                target.IntValue2 = source.IntValue2;
                target.IntValue3 = source.IntValue3;
                target.IntValue4 = source.IntValue4;
                target.IntValue5 = source.IntValue5;
                target.BoolValue1 = source.BoolValue1;
                target.BoolValue2 = source.BoolValue2;
                target.BoolValue3 = source.BoolValue3;
                target.BoolValue4 = source.BoolValue4;
                target.BoolValue5 = source.BoolValue5;
                target.CultureCode = source.CultureCode;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v25.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v25.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.PageElement ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v25.PageElement source)
        {
            Obymobi.Logic.Model.Mobile.PageElement target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.PageElement();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v26.Converters.PageElementConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
