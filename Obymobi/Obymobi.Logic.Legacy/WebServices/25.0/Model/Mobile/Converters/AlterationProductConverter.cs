using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v25.Converters
{
	public class AlterationProductConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v25.AlterationProduct, Obymobi.Logic.Model.Mobile.AlterationProduct>
	{
        public AlterationProductConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v26.Converters.AlterationProductConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v25.AlterationProduct ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.AlterationProduct source)
        {
            Obymobi.Logic.Model.Mobile.v25.AlterationProduct target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v25.AlterationProduct();
                target.AlterationProductId = source.AlterationProductId;
                target.AlterationId = source.AlterationId;
                target.Visible = source.Visible;
                target.SortOrder = source.SortOrder;

                if (source.Products != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v25.Converters.ProductConverter productsConverter = new Obymobi.Logic.Model.Mobile.v25.Converters.ProductConverter();
                    target.Products = (Product[])productsConverter.ConvertArrayToLegacyArray(source.Products);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.AlterationProduct ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v25.AlterationProduct source)
        {
            Obymobi.Logic.Model.Mobile.AlterationProduct target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.AlterationProduct();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v26.Converters.AlterationProductConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
