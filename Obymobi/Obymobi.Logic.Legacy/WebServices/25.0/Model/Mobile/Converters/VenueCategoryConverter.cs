using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v25.Converters
{
	public class VenueCategoryConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v25.VenueCategory, Obymobi.Logic.Model.Mobile.VenueCategory>
	{
        public VenueCategoryConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v26.Converters.VenueCategoryConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v25.VenueCategory ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.VenueCategory source)
        {
            Obymobi.Logic.Model.Mobile.v25.VenueCategory target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v25.VenueCategory();
                target.VenueCategoryId = source.VenueCategoryId;
                target.Name = source.Name;
                target.MarkerIcon = source.MarkerIcon;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v25.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.Mobile.v25.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.VenueCategory ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v25.VenueCategory source)
        {
            Obymobi.Logic.Model.Mobile.VenueCategory target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.VenueCategory();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v26.Converters.VenueCategoryConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
