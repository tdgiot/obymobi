using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v25.Converters
{
	public class ProductSuggestionConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v25.ProductSuggestion, Obymobi.Logic.Model.Mobile.ProductSuggestion>
	{
        public ProductSuggestionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v26.Converters.ProductSuggestionConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v25.ProductSuggestion ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.ProductSuggestion source)
        {
            Obymobi.Logic.Model.Mobile.v25.ProductSuggestion target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v25.ProductSuggestion();
                target.ProductId = source.ProductId;
                target.SuggestedProductId = source.SuggestedProductId;
                target.SortOrder = source.SortOrder;
                target.Checkout = source.Checkout;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.ProductSuggestion ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v25.ProductSuggestion source)
        {
            Obymobi.Logic.Model.Mobile.ProductSuggestion target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.ProductSuggestion();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v26.Converters.ProductSuggestionConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
