using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v25.Converters
{
	public class UITabConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v25.UITab, Obymobi.Logic.Model.Mobile.UITab>
	{
        public UITabConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v26.Converters.UITabConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v25.UITab ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.UITab source)
        {
            Obymobi.Logic.Model.Mobile.v25.UITab target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v25.UITab();
                target.UITabId = source.UITabId;
                target.UIModeId = source.UIModeId;
                target.Caption = source.Caption;
                target.Type = source.Type;
                target.CategoryId = source.CategoryId;
                target.EntertainmentId = source.EntertainmentId;
                target.SiteId = source.SiteId;
                target.URL = source.URL;
                target.Zoom = source.Zoom;
                target.SortOrder = source.SortOrder;
                target.Visible = source.Visible;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v25.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.Mobile.v25.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.UITab ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v25.UITab source)
        {
            Obymobi.Logic.Model.Mobile.UITab target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.UITab();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v26.Converters.UITabConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
