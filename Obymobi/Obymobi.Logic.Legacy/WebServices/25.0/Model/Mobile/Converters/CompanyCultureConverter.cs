using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v25.Converters
{
	public class CompanyCultureConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v25.CompanyCulture, Obymobi.Logic.Model.Mobile.CompanyCulture>
	{
        public CompanyCultureConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v26.Converters.CompanyCultureConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v25.CompanyCulture ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.CompanyCulture source)
        {
            Obymobi.Logic.Model.Mobile.v25.CompanyCulture target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v25.CompanyCulture();
                target.CompanyCultureId = source.CompanyCultureId;
                target.CultureCode = source.CultureCode;
                target.Name = source.Name;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.CompanyCulture ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v25.CompanyCulture source)
        {
            Obymobi.Logic.Model.Mobile.CompanyCulture target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.CompanyCulture();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v26.Converters.CompanyCultureConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
