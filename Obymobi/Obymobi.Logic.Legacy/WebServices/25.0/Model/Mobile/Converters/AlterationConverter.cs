using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v25.Converters
{
	public class AlterationConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v25.Alteration, Obymobi.Logic.Model.Mobile.Alteration>
	{
        public AlterationConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v26.Converters.AlterationConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v25.Alteration ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Alteration source)
        {
            Obymobi.Logic.Model.Mobile.v25.Alteration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v25.Alteration();
                target.AlterationId = source.AlterationId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.DefaultAlterationoptionId = source.DefaultAlterationoptionId;
                target.MinOptions = source.MinOptions;
                target.MaxOptions = source.MaxOptions;
                target.StartTime = source.StartTime;
                target.EndTime = source.EndTime;
                target.MinLeadMinutes = source.MinLeadMinutes;
                target.MaxLeadHours = source.MaxLeadHours;
                target.IntervalMinutes = source.IntervalMinutes;
                target.ShowDatePicker = source.ShowDatePicker;
                target.Value = source.Value;
                target.OrderLevelEnabled = source.OrderLevelEnabled;
                target.ParentAlterationId = source.ParentAlterationId;
                target.Visible = source.Visible;
                target.Description = source.Description;

                if (source.Alterationoptions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v25.Converters.AlterationoptionConverter alterationoptionsConverter = new Obymobi.Logic.Model.Mobile.v25.Converters.AlterationoptionConverter();
                    target.Alterationoptions = (Alterationoption[])alterationoptionsConverter.ConvertArrayToLegacyArray(source.Alterationoptions);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v25.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v25.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.Products != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v25.Converters.ProductConverter productsConverter = new Obymobi.Logic.Model.Mobile.v25.Converters.ProductConverter();
                    target.Products = (Product[])productsConverter.ConvertArrayToLegacyArray(source.Products);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v25.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.Mobile.v25.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Alteration ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v25.Alteration source)
        {
            Obymobi.Logic.Model.Mobile.Alteration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Alteration();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v26.Converters.AlterationConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
