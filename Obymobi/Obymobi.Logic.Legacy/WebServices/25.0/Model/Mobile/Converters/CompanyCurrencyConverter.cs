using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v25.Converters
{
	public class CompanyCurrencyConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v25.CompanyCurrency, Obymobi.Logic.Model.Mobile.CompanyCurrency>
	{
        public CompanyCurrencyConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v26.Converters.CompanyCurrencyConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v25.CompanyCurrency ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.CompanyCurrency source)
        {
            Obymobi.Logic.Model.Mobile.v25.CompanyCurrency target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v25.CompanyCurrency();
                target.CompanyCurrencyId = source.CompanyCurrencyId;
                target.CurrencyCode = source.CurrencyCode;
                target.Symbol = source.Symbol;
                target.Name = source.Name;
                target.ExchangeRate = source.ExchangeRate;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.CompanyCurrency ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v25.CompanyCurrency source)
        {
            Obymobi.Logic.Model.Mobile.CompanyCurrency target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.CompanyCurrency();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v26.Converters.CompanyCurrencyConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
