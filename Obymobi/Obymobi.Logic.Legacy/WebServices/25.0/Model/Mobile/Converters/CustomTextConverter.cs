using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v25.Converters
{
	public class CustomTextConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v25.CustomText, Obymobi.Logic.Model.Mobile.CustomText>
	{
        public CustomTextConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v26.Converters.CustomTextConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v25.CustomText ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.CustomText source)
        {
            Obymobi.Logic.Model.Mobile.v25.CustomText target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v25.CustomText();
                target.CustomTextId = source.CustomTextId;
                target.Type = source.Type;
                target.Text = source.Text;
                target.CultureCode = source.CultureCode;
                target.ForeignKey = source.ForeignKey;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.CustomText ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v25.CustomText source)
        {
            Obymobi.Logic.Model.Mobile.CustomText target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.CustomText();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v26.Converters.CustomTextConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
