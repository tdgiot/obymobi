using System;
using System.Linq;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v25
{
    /// <summary>
    /// Model class which represents an order
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Order"), IncludeInCodeGeneratorForXamarin]
    public class Order : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Order type
        /// </summary>
        public Order()
        {
        }

        #endregion

        #region Methods

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the order
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int OrderId
        { get; set; }

        /// <summary>
        /// Gets or sets a unique identifier
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Guid
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the customer
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int CustomerId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the DeliverypointId
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int DeliverypointId
        { get; set; }

        /// <summary>
        /// Gets or sets the number of the deliverypoint
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int DeliverypointNumber
        { get; set; }

        /// <summary>
        /// Get or sets the name of the deliverypoint caption (Table, Lane, Room, etc.).		 
        /// Not a great fan of this name, since it's called DeliverypointCaption on other places
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string DeliverypointName
        { get; set; }

        /// <summary>
        /// Gets or sets the status of the order
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Status
        { get; set; }

        /// <summary>
        /// Gets or sets the text of the order status
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string StatusText
        { get; set; }

        /// <summary>
        /// Gets or sets the ErrorCode of the order 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ErrorCode
        { get; set; }

        /// <summary>
        /// Gets or sets the notes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Notes
        { get; set; }

        /// <summary>
        /// Gets or sets the full name of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string CustomerNameFull
        { get; set; }

        /// <summary>
        /// Gets or sets the phonenumber
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string CustomerPhonenumber
        { get; set; }

        /// <summary>
        /// Gets or sets the type of order
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the PendingPaymentmethodandTransactionId
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string PendingPaymentmethodAndTransactionId
        { get; set; }

        /// <summary>
        /// Gets or sets the time the order was created
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Created
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the time the order was created
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public DateTime CreatedAsDateTime
        { get; set; }

        /// <summary>
        /// Gets or sets items of the order
        /// </summary>
        [XmlArray("Orderitems")]
        [XmlArrayItem("Orderitem")]
        [IncludeInCodeGeneratorForXamarin]
        public Orderitem[] Orderitems
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Order Clone()
        {
            return this.Clone<Order>();
        }

        #endregion
    }
}