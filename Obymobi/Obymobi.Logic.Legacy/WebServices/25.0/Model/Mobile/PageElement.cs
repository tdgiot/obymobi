using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.Mobile.v25
{
    /// <summary>
    /// Model class which represents a company for the mobile solution
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "PageElement"), IncludeInCodeGeneratorForXamarin]
    public class PageElement : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Company type
        /// </summary>
        public PageElement()
        {
        }

        #endregion

        #region Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int PageElementId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int PageId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string SystemName
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int PageElementType
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int PageType
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SortOrder
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string StringValue1 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string StringValue2 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string StringValue3 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string StringValue4 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string StringValue5 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int? IntValue1 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int? IntValue2 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int? IntValue3 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int? IntValue4 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int? IntValue5 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool? BoolValue1 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool? BoolValue2 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool? BoolValue3 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool? BoolValue4 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool? BoolValue5 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string CultureCode
        { get; set; }

        /// <summary>
        /// Gets or sets the alterations of the product
        /// </summary>
        [IncludeInCodeGeneratorForXamarin]
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        public Media[] Media
        { get; set; }


        #endregion

        #region Methods

        public PageElement Clone()
        {
            return this.Clone<PageElement>();
        }

        public PageElement CloneUsingBinaryFormatter()
        {
            return this.CloneUsingBinaryFormatter<PageElement>();
        }

        #endregion
    }
}
