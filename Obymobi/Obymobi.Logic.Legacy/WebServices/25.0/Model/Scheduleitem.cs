using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v25
{
    /// <summary>
    /// Model class which represents an order hour
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Scheduleitem"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile]
    public class Scheduleitem : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Scheduleitem type
        /// </summary>
        public Scheduleitem()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the schedule item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int ScheduleitemId
        { get; set; }

        /// <summary>
        /// Gets or sets the day number
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DayOfWeek
        { get; set; }

        /// <summary>
        /// Gets or sets the start time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string TimeStart
        { get; set; }

        /// <summary>
        /// Gets or sets the end time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string TimeEnd
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Scheduleitem Clone()
        {
            return this.Clone<Scheduleitem>();
        }

        #endregion
    }
}
