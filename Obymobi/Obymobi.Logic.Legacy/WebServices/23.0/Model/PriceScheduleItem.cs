using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v23
{
    /// <summary>
    /// Model class which represents an price schedule item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "PriceScheduleItem"), IncludeInCodeGeneratorForAndroid]
    public class PriceScheduleItem : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.PriceScheduleItem type
        /// </summary>
        public PriceScheduleItem()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the price schedule item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int PriceScheduleItemId
        { get; set; }

        /// <summary>
        /// Gets or sets the widget id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PriceLevelId
        { get; set; }        

        /// <summary>
        /// Gets or sets the schedule items
        /// </summary>
        [XmlArray("PriceScheduleItemOccurrences")]
        [XmlArrayItem("PriceScheduleItemOccurrence")]
        [IncludeInCodeGeneratorForAndroid]
        public PriceScheduleItemOccurrence[] PriceScheduleItemOccurrences
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public PriceScheduleItem Clone()
        {
            return this.Clone<PriceScheduleItem>();
        }

        #endregion
    }
}
