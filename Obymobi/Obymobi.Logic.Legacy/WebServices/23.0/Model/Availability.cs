using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v23
{
    /// <summary>
    /// Model class which represents an Availability item
    /// </summary>
    [Serializable, XmlRoot(ElementName = "Availability"), IncludeInCodeGeneratorForAndroid]
    public class Availability : ModelBase
    {
        /// <summary>
        /// Gets or sets the id of the availability
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int AvailabilityId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the availability
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the status as int
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Status
        { get; set; }

        /// <summary>
        /// Gets or sets the status as int
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionEntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the action category id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionCategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the action product id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the action site id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionSiteId
        { get; set; }
        /// <summary>
        /// Gets or sets the action page id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionPageId
        { get; set; }

        /// <summary>
        /// Gets or sets the action url
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Url
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }
    }
}