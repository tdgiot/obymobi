using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v23
{
    /// <summary>
    /// Model class which represents a company for the mobile solution
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Site"), 
    IncludeInCodeGeneratorForXamarin, 
    IncludeInCodeGeneratorForAndroid]
    public class Site : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Company type
        /// </summary>
        public Site()
        {
        }

        #endregion

        #region Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int SiteId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int SiteType
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public long LastModifiedTicks { get; set; }

        [XmlArray("Pages")]
        [XmlArrayItem("Page")]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public Page[] Pages
        { get; set; }

        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        public Media[] Media
        { get; set; }        

        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        public Site Clone()
        {
            return this.Clone<Site>();
        }

        #endregion
    }
}
