using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v23
{
	/// <summary>
	/// Model class which represents a company
	/// </summary>
	[Serializable, XmlRootAttribute(ElementName = "ReiswijzerStop"), IncludeInCodeGeneratorForFlex]
	public class ReiswijzerStop
	{
		#region Constructors

		/// <summary>
		/// Constructs an instance of the Obymobi.Logic.Model.Company type
		/// </summary>
		public ReiswijzerStop()
		{
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the id of the company
		/// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
		public int ReiswijzerStopId
		{ get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string Type
		{ get; set; }

        /// <summary>
        /// Gets or sets the stop place.
        /// </summary>
        /// <value>
        /// The stop place.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string StopPlace
		{ get; set; }

        /// <summary>
        /// Gets or sets the name of the stop.
        /// </summary>
        /// <value>
        /// The name of the stop.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string StopName
		{ get; set; }

        /// <summary>
        /// Gets or sets the arrival time.
        /// </summary>
        /// <value>
        /// The arrival time.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ArrivalTime
		{ get; set; }

        /// <summary>
        /// Gets or sets the departure time.
        /// </summary>
        /// <value>
        /// The departure time.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string DepartureTime
		{ get; set; }

        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        /// <value>
        /// The time.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string Time
		{ get; set; }

        /// <summary>
        /// Gets or sets the platform.
        /// </summary>
        /// <value>
        /// The platform.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string Platform
		{ get; set; }

        /// <summary>
        /// Gets or sets the mode.
        /// </summary>
        /// <value>
        /// The mode.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string Mode
		{ get; set; }

        /// <summary>
        /// Gets or sets the mode id.
        /// </summary>
        /// <value>
        /// The mode id.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ModeId
		{ get; set; }

        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>
        /// The name of the company.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string CompanyName
		{ get; set; }

        /// <summary>
        /// Gets or sets the service id.
        /// </summary>
        /// <value>
        /// The service id.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ServiceId
		{ get; set; }

        /// <summary>
        /// Gets or sets the name of the service.
        /// </summary>
        /// <value>
        /// The name of the service.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ServiceName
		{ get; set; }

        /// <summary>
        /// Gets or sets the walktime.
        /// </summary>
        /// <value>
        /// The walktime.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public int Walktime
		{ get; set; }

		#endregion
	}
}
