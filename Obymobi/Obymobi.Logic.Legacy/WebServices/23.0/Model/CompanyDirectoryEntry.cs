using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v23
{
    [Serializable, XmlRootAttribute(ElementName = "OrderSaveStatus"), IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class CompanyDirectoryEntry : ModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyDirectoryEntry"/> class.
        /// </summary>
        public CompanyDirectoryEntry()
        {
        }

        /// <summary>
        /// Gets or sets the company id
        /// </summary>
        /// <value>The order id.</value>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the order guid.
        /// </summary>
        /// <value>The order id.</value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Map cooridates, latitude
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public double? Latitude
        { get; set; }

        /// <summary>
        /// Map cooridates, longitude
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public double? Longitude
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Data (not the menu) was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public long CompanyDataLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Media (not the menu) was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public long CompanyMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Menu was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public long MenuDataLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Media for the Menu was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public long MenuMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the deliverypoint groups of the company
        /// </summary>
        [XmlArray("Deliverypointgroups")]
        [XmlArrayItem("Deliverypointgroup")]
        public DeliverypointgroupDirectoryEntry[] Deliverypointgroups
        { get; set; }

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CompanyDirectoryEntry Clone()
        {
            return this.Clone<CompanyDirectoryEntry>();
        }

        #endregion
    }
}
