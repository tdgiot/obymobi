using Obymobi.Security;
using Dionysos;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Net;
using System.IO;
using System.Data.Common;
using Obymobi.Logic.HelperClasses;
using Obymobi.Enums;
using System.Data.SqlClient;

namespace Obymobi.Logic.Model.v23
{
    [DataContract]
    public class DatabaseConnectionInformation
    {
        // timestamp, hash, parameters

        private const string HASH_APPENDIX = "MakingConnectionsWithTheOtherWorld";

        public DatabaseConnectionInformation()
        {
        }

        public DatabaseConnectionInformation(string connectionString)
        {
            ConnectionString = connectionString;
        }

        #region Methods

        /// <summary>
        /// Check if the ConnectionString is valid voor usage
        /// </summary>
        /// <param name="cloudEnvironment">For which Cloud Environment is it checked, as a database will be checked if it's configured for that environment</param>
        /// <param name="currentCatalogName">Supply the current CatalogName (Obymobi.Data.DbUtils.GetCurrentCatalogName()) because the Catalog Overwrite will overwrite the Initial Database of the connection string</param>
        /// <param name="resultDetails"></param>
        /// <returns></returns>
        public bool ValidateConnectionString(CloudEnvironment cloudEnvironment, string currentCatalogName, out string resultDetails)
        {
            bool success = false;
            resultDetails = "Unknown error";            

            // Check if connection string is valid            
            SqlConnectionStringBuilder scb;
            try
            {
                scb = new SqlConnectionStringBuilder(this.ConnectionString);
            }
            catch
            {
                resultDetails = "ConnectionString invalid syntax";
                return false;
            }

            if(currentCatalogName.IsNullOrWhiteSpace() && scb != null)
            {
                // We do allow for empty CurrentCatalogName and fall back to the initial catalog in that case because the NocService has no
                // idea of which database is the overwrite.
                currentCatalogName = scb.InitialCatalog;
            }

            // Try to connect
            try
            {
                string result = DatabaseHelper.GetDatabaseStatus(this.ConnectionString, true);
                if (result != "ONLINE")
                    resultDetails = "Datasebase not online: " + result;
                else
                {
                    // Verify it's for the correct Cloud Environment
                    CloudEnvironment environmentInDatabase = DatabaseHelper.GetEnvironmentOfDatabase(this.ConnectionString, currentCatalogName);
                    if (environmentInDatabase != cloudEnvironment)
                    {
                        resultDetails = "Database environment is '{0}', while it was being verified on '{1}'".FormatSafe(environmentInDatabase, cloudEnvironment);
                        success = false;
                    }
                    else
                    {
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                resultDetails = "Connection failed: " + ex.Message;
            }

            return success;
        }

        public override string ToString()
        {
            string encryptedConnectionString = Dionysos.Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(this.ConnectionString);
            long timestamp = DateTime.UtcNow.ToUnixTime();
            string hash = Hasher.GetHash(HASH_APPENDIX, string.Format("{0}{1}{2}", timestamp, encryptedConnectionString, this.Database));
            return string.Format("{0},{1},{2},{3}", hash, timestamp, encryptedConnectionString, Database);
        }

        #endregion

        #region Static Methods

        public static bool TryRetrieveFromNocService(List<string> nocServiceConnectiongStringTxtUrls, out DatabaseConnectionInformation connectionInformationObject)
        {
            string dummy;
            return DatabaseConnectionInformation.TryRetrieveFromNocService(nocServiceConnectiongStringTxtUrls, out connectionInformationObject, out dummy);
        }

        public static bool TryRetrieveFromNocService(List<string> nocServiceConnectiongStringTxtUrls, out DatabaseConnectionInformation connectionInformationObject, out string exceptionMessage)
        {
            bool success = false;
            exceptionMessage = string.Empty;
            connectionInformationObject = null;
            List<string> urls = nocServiceConnectiongStringTxtUrls;

            // Iterate over NocStatusServices
            foreach (var url in urls)
            {
                // Try to get the file with the status from the Noc Status Service and check it's contents
                try
                {
                    string cacheSaveUrl = string.Format("{0}?update={1}", url, DateTime.Now.Ticks);
                    WebRequest request = WebRequest.Create(cacheSaveUrl);
                    request.Proxy = null;
                    var response = (HttpWebResponse)request.GetResponse();

                    if (response != null && response.StatusCode == HttpStatusCode.OK)
                    {
                        // Validate it's the correct type of content
                        var sr = new StreamReader(response.GetResponseStream());
                        string content = sr.ReadToEnd();

                        if (DatabaseConnectionInformation.TryParse(content, out connectionInformationObject))
                            success = true;
                    }

                    if (response != null)
                        response.Close();
                }
                catch (Exception ex)
                {
                    exceptionMessage = "TryRetrieveFromNocService failed for '{0}': '{1}'".FormatSafe(url, ex.Message);
                }

                if (success)
                    break;
            }

            return success;
        }

        public static bool TryParse(string connectionInformationText, out DatabaseConnectionInformation connectionInformationObject)
        {
            connectionInformationObject = null;

            try
            {
                string[] elements;
                if (DatabaseConnectionInformation.ValidateConnectionInformationString(connectionInformationText, out elements))
                {
                    connectionInformationObject = new DatabaseConnectionInformation();

                    // Calculate Timestamp to TimeSpan
                    connectionInformationObject.Age = (DateTime.UtcNow - DateTimeUtil.FromUnixTime(Convert.ToInt64(elements[1])));

                    // Decrypt Connection String
                    connectionInformationObject.ConnectionString = Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(elements[2]);

                    // Set database number
                    connectionInformationObject.Database = Convert.ToInt32(elements[3]);

                    return true;
                }
            }
            catch
            {
                // Returns false.
            }

            return false;
        }

        public static bool ValidateConnectionInformationString(string connectionInformation, out string[] elements)
        {
            // Must be 3 elements Hash,Timestamp,ConnectionString
            if (connectionInformation.IsNullOrWhiteSpace())
            {
                elements = new string[] { };
                return false;
            }

            elements = connectionInformation.Split(new char[] { ',' }, StringSplitOptions.None);

            if (elements.Length >= 4)
            {
                // Validate Hash
                StringBuilder sb = new StringBuilder();
                for (int i = 1; i < elements.Length; i++)
                {
                    sb.Append(elements[i]);
                }

                string suppliedHash = elements[0];

                if (Hasher.IsHashValid(elements[0], DatabaseConnectionInformation.HASH_APPENDIX, sb.ToString()))
                {
                    long dummyLong;
                    if (!long.TryParse(elements[1], out dummyLong))
                        return false;

                    int dummyInt;
                    if (!int.TryParse(elements[3], out dummyInt))
                        return false;

                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the database (1 = primary, 2 is secondary).
        /// </summary>
        /// <value>
        /// The database.
        /// </value>
        [DataMember]
        public int Database { get; set; }

        [DataMember]
        public string ConnectionString { get; set; }

        [DataMember]
        public TimeSpan Age { get; set; }

        #endregion

    }
}
