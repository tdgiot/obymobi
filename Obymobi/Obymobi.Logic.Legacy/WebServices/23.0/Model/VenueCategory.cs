using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v23
{
    /// <summary>
    /// Model class which represents a venue category
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "VenueCategory"), IncludeInCodeGeneratorForXamarin, IncludeInCodeGeneratorForAndroid]
    public class VenueCategory : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.VenueCategory type
        /// </summary>
        public VenueCategory()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the venue category
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        [IncludeInCodeGeneratorForAndroid]
        public int VenueCategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the venue category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }        

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public VenueCategory Clone()
        {
            return this.Clone<VenueCategory>();
        }

        #endregion
    }
}
