namespace Obymobi.Logic.Model.v23.Converters
{
	public class MessagegroupConverter : ModelConverterBase<Obymobi.Logic.Model.v23.Messagegroup, Obymobi.Logic.Model.Messagegroup>
	{
        public MessagegroupConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v24.Converters.MessagegroupConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v23.Messagegroup ConvertModelToLegacyModel(Obymobi.Logic.Model.Messagegroup source)
        {
            Obymobi.Logic.Model.v23.Messagegroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v23.Messagegroup();
                target.MessagegroupId = source.MessagegroupId;
                target.Name = source.Name;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Messagegroup ConvertLegacyModelToModel(Obymobi.Logic.Model.v23.Messagegroup source)
        {
            Obymobi.Logic.Model.Messagegroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Messagegroup();

                // Copy default values from new version
                new Obymobi.Logic.Model.v24.Converters.MessagegroupConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
