namespace Obymobi.Logic.Model.v23.Converters
{
	public class FoursquareCompanyConverter : ModelConverterBase<Obymobi.Logic.Model.v23.FoursquareCompany, Obymobi.Logic.Model.FoursquareCompany>
	{
        public FoursquareCompanyConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v24.Converters.FoursquareCompanyConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v23.FoursquareCompany ConvertModelToLegacyModel(Obymobi.Logic.Model.FoursquareCompany source)
        {
            Obymobi.Logic.Model.v23.FoursquareCompany target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v23.FoursquareCompany();
                target.Name = source.Name;
                target.Address = source.Address;
                target.TotalCheckins = source.TotalCheckins;
                target.TotalPeople = source.TotalPeople;
                target.OwnCheckins = source.OwnCheckins;
                target.Categories = source.Categories;

                if (source.Tips != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v23.Converters.FoursquareTipConverter tipsConverter = new Obymobi.Logic.Model.v23.Converters.FoursquareTipConverter();
                    target.Tips = (FoursquareTip[])tipsConverter.ConvertArrayToLegacyArray(source.Tips);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.FoursquareCompany ConvertLegacyModelToModel(Obymobi.Logic.Model.v23.FoursquareCompany source)
        {
            Obymobi.Logic.Model.FoursquareCompany target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.FoursquareCompany();

                // Copy default values from new version
                new Obymobi.Logic.Model.v24.Converters.FoursquareCompanyConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
