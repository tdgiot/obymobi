namespace Obymobi.Logic.Model.v23.Converters
{
	public class UIModeConverter : ModelConverterBase<Obymobi.Logic.Model.v23.UIMode, Obymobi.Logic.Model.UIMode>
	{
        public UIModeConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v24.Converters.UIModeConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v23.UIMode ConvertModelToLegacyModel(Obymobi.Logic.Model.UIMode source)
        {
            Obymobi.Logic.Model.v23.UIMode target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v23.UIMode();
                target.UIModeId = source.UIModeId;
                target.Name = source.Name;
                target.ShowServiceOptions = source.ShowServiceOptions;
                target.ShowRequestBill = source.ShowRequestBill;
                target.ShowDeliverypoint = source.ShowDeliverypoint;
                target.ShowClock = source.ShowClock;
                target.ShowBattery = source.ShowBattery;
                target.ShowBrightness = source.ShowBrightness;
                target.ShowFullscreenEyecatcher = source.ShowFullscreenEyecatcher;
                target.ShowFooterLogo = source.ShowFooterLogo;
                target.Type = source.Type;
                target.DefaultUITabId = source.DefaultUITabId;

                if (source.UITabs != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v23.Converters.UITabConverter uITabsConverter = new Obymobi.Logic.Model.v23.Converters.UITabConverter();
                    target.UITabs = (UITab[])uITabsConverter.ConvertArrayToLegacyArray(source.UITabs);
                }

                if (source.UIFooterItems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v23.Converters.UIFooterItemConverter uIFooterItemsConverter = new Obymobi.Logic.Model.v23.Converters.UIFooterItemConverter();
                    target.UIFooterItems = (UIFooterItem[])uIFooterItemsConverter.ConvertArrayToLegacyArray(source.UIFooterItems);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.UIMode ConvertLegacyModelToModel(Obymobi.Logic.Model.v23.UIMode source)
        {
            Obymobi.Logic.Model.UIMode target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UIMode();

                // Copy default values from new version
                new Obymobi.Logic.Model.v24.Converters.UIModeConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
