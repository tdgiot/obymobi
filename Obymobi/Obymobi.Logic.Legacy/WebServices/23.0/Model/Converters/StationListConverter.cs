namespace Obymobi.Logic.Model.v23.Converters
{
	public class StationListConverter : ModelConverterBase<Obymobi.Logic.Model.v23.StationList, Obymobi.Logic.Model.StationList>
	{
        public StationListConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v24.Converters.StationListConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v23.StationList ConvertModelToLegacyModel(Obymobi.Logic.Model.StationList source)
        {
            Obymobi.Logic.Model.v23.StationList target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v23.StationList();
                target.StationListId = source.StationListId;
                target.Name = source.Name;

                if (source.Stations != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v23.Converters.StationConverter stationsConverter = new Obymobi.Logic.Model.v23.Converters.StationConverter();
                    target.Stations = (Station[])stationsConverter.ConvertArrayToLegacyArray(source.Stations);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.StationList ConvertLegacyModelToModel(Obymobi.Logic.Model.v23.StationList source)
        {
            Obymobi.Logic.Model.StationList target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.StationList();

                // Copy default values from new version
                new Obymobi.Logic.Model.v24.Converters.StationListConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
