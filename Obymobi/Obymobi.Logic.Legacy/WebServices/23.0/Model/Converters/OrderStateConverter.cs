namespace Obymobi.Logic.Model.v23.Converters
{
	public class OrderStateConverter : ModelConverterBase<Obymobi.Logic.Model.v23.OrderState, Obymobi.Logic.Model.OrderState>
	{
        public OrderStateConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v24.Converters.OrderStateConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v23.OrderState ConvertModelToLegacyModel(Obymobi.Logic.Model.OrderState source)
        {
            Obymobi.Logic.Model.v23.OrderState target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v23.OrderState();
                target.OrderId = source.OrderId;
                target.Status = source.Status;
            }

            return target;
        }

        public override Obymobi.Logic.Model.OrderState ConvertLegacyModelToModel(Obymobi.Logic.Model.v23.OrderState source)
        {
            Obymobi.Logic.Model.OrderState target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.OrderState();

                // Copy default values from new version
                new Obymobi.Logic.Model.v24.Converters.OrderStateConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
