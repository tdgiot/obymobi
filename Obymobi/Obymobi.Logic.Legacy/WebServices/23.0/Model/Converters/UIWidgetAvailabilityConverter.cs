namespace Obymobi.Logic.Model.v23.Converters
{
	public class UIWidgetAvailabilityConverter : ModelConverterBase<Obymobi.Logic.Model.v23.UIWidgetAvailability, Obymobi.Logic.Model.UIWidgetAvailability>
	{
        public UIWidgetAvailabilityConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v24.Converters.UIWidgetAvailabilityConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v23.UIWidgetAvailability ConvertModelToLegacyModel(Obymobi.Logic.Model.UIWidgetAvailability source)
        {
            Obymobi.Logic.Model.v23.UIWidgetAvailability target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v23.UIWidgetAvailability();
                target.UIWidgetAvailabilityId = source.UIWidgetAvailabilityId;
                target.UIWidgetId = source.UIWidgetId;
                target.AvailabilityId = source.AvailabilityId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.UIWidgetAvailability ConvertLegacyModelToModel(Obymobi.Logic.Model.v23.UIWidgetAvailability source)
        {
            Obymobi.Logic.Model.UIWidgetAvailability target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UIWidgetAvailability();

                // Copy default values from new version
                new Obymobi.Logic.Model.v24.Converters.UIWidgetAvailabilityConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
