namespace Obymobi.Logic.Model.v23.Converters
{
	public class MediaRatioTypeMediaConverter : ModelConverterBase<Obymobi.Logic.Model.v23.MediaRatioTypeMedia, Obymobi.Logic.Model.MediaRatioTypeMedia>
	{
        public MediaRatioTypeMediaConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v24.Converters.MediaRatioTypeMediaConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v23.MediaRatioTypeMedia ConvertModelToLegacyModel(Obymobi.Logic.Model.MediaRatioTypeMedia source)
        {
            Obymobi.Logic.Model.v23.MediaRatioTypeMedia target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v23.MediaRatioTypeMedia();
                target.MediaRatioTypeMediaId = source.MediaRatioTypeMediaId;
                target.MediaId = source.MediaId;
                target.MediaRatioTypeSystemName = source.MediaRatioTypeSystemName;
                target.Width = source.Width;
                target.Height = source.Height;
                target.MediaType = source.MediaType;
                target.Parent = source.Parent;
            }

            return target;
        }

        public override Obymobi.Logic.Model.MediaRatioTypeMedia ConvertLegacyModelToModel(Obymobi.Logic.Model.v23.MediaRatioTypeMedia source)
        {
            Obymobi.Logic.Model.MediaRatioTypeMedia target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.MediaRatioTypeMedia();

                // Copy default values from new version
                new Obymobi.Logic.Model.v24.Converters.MediaRatioTypeMediaConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
