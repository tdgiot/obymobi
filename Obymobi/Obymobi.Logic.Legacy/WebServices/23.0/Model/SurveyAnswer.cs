using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v23
{
    /// <summary>
    /// Model class which represents an survey answer
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "SurveyAnswer"), IncludeInCodeGeneratorForFlex]
    public class SurveyAnswer : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.SurveyAnswer type
        /// </summary>
        public SurveyAnswer()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the survey
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyAnswerId
        { get; set; }

        /// <summary>
        /// Gets or sets the question
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public string Answer
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of the answer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the target question
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int TargetSurveyQuestionId
        { get; set; }


        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        [XmlArray("SurveyAnswerLanguages")]
        [XmlArrayItem("SurveyAnswerLanguage")]
        public SurveyAnswerLanguage[] SurveyAnswerLanguages
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public SurveyAnswer Clone()
        {
            return this.Clone<SurveyAnswer>();
        }

        #endregion
    }
}
