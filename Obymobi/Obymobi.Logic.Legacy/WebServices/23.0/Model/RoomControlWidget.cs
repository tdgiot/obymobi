using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v23
{
    /// <summary>
    /// Model class which represents a room control widget
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "RoomControlWidget"), IncludeInCodeGeneratorForAndroid]
    public class RoomControlWidget : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.RoomControlWidget
        /// </summary>
        public RoomControlWidget()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the room control widget
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlWidgetId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Caption
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlComponentId1
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlComponentId2
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlComponentId3
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlComponentId4
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlComponentId5
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlComponentId6
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlComponentId7
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlComponentId8
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlComponentId9
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlComponentId10
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Visible
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue1
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue2
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue3
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue4
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue5
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue6
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue7
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue8
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue9
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue10
        { get; set; }
        
        [XmlAttribute("Parent")]
        public string Parent
        { get; set; }        

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public RoomControlWidget Clone()
        {
            return this.Clone<RoomControlWidget>();
        }

        #endregion
    }
}
