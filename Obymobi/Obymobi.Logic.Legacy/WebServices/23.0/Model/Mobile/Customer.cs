using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v23
{
    /// <summary>
    /// Model class which represents a customer for the mobile solution
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Customer"), IncludeInCodeGeneratorForXamarin]
    public class Customer : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Customer type
        /// </summary>
        public Customer()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the customer
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int CustomerId
        { get; set; }

        /// <summary>
        /// Gets or sets the password of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Salt
        { get; set; }

        /// <summary>
        /// Gets or sets the password of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string EmailAddress
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Phonenumber
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Firstname
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Lastname
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string LastnamePrefix
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string BirthdateTimestamp
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Gender
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string AddressLine1
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string AddressLine2
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Zipcode
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string City
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string CountryCode
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool AnonymousAccount
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Guid 
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SocialmediaType
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string ExternalId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool Verified
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string CampaignName
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string CampaignSource
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string CampaignMedium
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string CampaignContent
        { get; set; }

        /// <summary>
        /// Gets or sets the CustomerSocialmedia of the customer
        /// </summary>
        [XmlArray("CustomerSocialmedia")]
        [XmlArrayItem("CustomerSocialmedia")]
        [IncludeInCodeGeneratorForXamarin]
        public CustomerSocialmedia[] CustomerSocialmedia
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Customer Clone()
        {
            return this.Clone<Customer>();
        }

        #endregion
    }
}
