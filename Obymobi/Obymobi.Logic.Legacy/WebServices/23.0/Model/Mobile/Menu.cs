using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v23
{
    /// <summary>
    /// Model class which represents a payment method
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Menu"), IncludeInCodeGeneratorForXamarin]
    public class Menu : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Menu type
        /// </summary>
        public Menu()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the menu
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int MenuId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the menu
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the version of the menu
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public long Ticks
        { get; set; }

        /// <summary>
        /// Gets or sets the root categories of the menu
        /// </summary>
        [XmlArray("Categories")]
        [XmlArrayItem("Category")]
        [IncludeInCodeGeneratorForXamarin]
        public Category[] Categories
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Menu Clone()
        {
            return this.Clone<Menu>();
        }

        #endregion
    }
}
