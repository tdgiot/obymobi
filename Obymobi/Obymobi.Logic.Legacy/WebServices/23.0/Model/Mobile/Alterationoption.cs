using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v23
{
    /// <summary>
    /// Model class which represents an alteration option
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Alterationoption"), IncludeInCodeGeneratorForXamarin]
    public class Alterationoption : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Alterationoption type
        /// </summary>
        public Alterationoption()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the alteration option
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationoptionId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the price addition of the alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public decimal PriceIn
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Additional properties

        /// <summary>
        /// Gets or sets a flag indicating whether this alteration option is selected
        /// </summary>
        public bool IsSelected
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Alterationoption Clone()
        {
            return this.Clone<Alterationoption>();
        }

        #endregion
    }
}
