using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v23.Converters
{
	public class SiteConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v23.Site, Obymobi.Logic.Model.Mobile.Site>
	{
        public SiteConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v24.Converters.SiteConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v23.Site ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Site source)
        {
            Obymobi.Logic.Model.Mobile.v23.Site target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v23.Site();
                target.SiteId = source.SiteId;
                target.Name = source.Name;
                target.SiteType = source.SiteType;

                if (source.Pages != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v23.Converters.PageConverter pagesConverter = new Obymobi.Logic.Model.Mobile.v23.Converters.PageConverter();
                    target.Pages = (Page[])pagesConverter.ConvertArrayToLegacyArray(source.Pages);
                }

                target.LastModifiedTicks = source.LastModifiedTicks;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v23.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v23.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v23.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.Mobile.v23.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Site ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v23.Site source)
        {
            Obymobi.Logic.Model.Mobile.Site target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Site();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v24.Converters.SiteConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
