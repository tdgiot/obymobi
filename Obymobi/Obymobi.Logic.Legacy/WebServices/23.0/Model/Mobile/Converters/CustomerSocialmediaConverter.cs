using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v23.Converters
{
	public class CustomerSocialmediaConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v23.CustomerSocialmedia, Obymobi.Logic.Model.Mobile.CustomerSocialmedia>
	{
        public CustomerSocialmediaConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v24.Converters.CustomerSocialmediaConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v23.CustomerSocialmedia ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.CustomerSocialmedia source)
        {
            Obymobi.Logic.Model.Mobile.v23.CustomerSocialmedia target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v23.CustomerSocialmedia();
                target.CustomerSocialmediaId = source.CustomerSocialmediaId;
                target.SocialmediaType = source.SocialmediaType;
                target.Email = source.Email;
                target.ExternalId = source.ExternalId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.CustomerSocialmedia ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v23.CustomerSocialmedia source)
        {
            Obymobi.Logic.Model.Mobile.CustomerSocialmedia target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.CustomerSocialmedia();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v24.Converters.CustomerSocialmediaConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
