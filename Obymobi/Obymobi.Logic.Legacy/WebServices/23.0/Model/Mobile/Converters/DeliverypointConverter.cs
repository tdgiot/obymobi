using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v23.Converters
{
	public class DeliverypointConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v23.Deliverypoint, Obymobi.Logic.Model.Mobile.Deliverypoint>
	{
        public DeliverypointConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v24.Converters.DeliverypointConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v23.Deliverypoint ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Deliverypoint source)
        {
            Obymobi.Logic.Model.Mobile.v23.Deliverypoint target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v23.Deliverypoint();
                target.DeliverypointId = source.DeliverypointId;
                target.Number = source.Number;
                target.CompanyId = source.CompanyId;
                target.Name = source.Name;
                target.DeliverypointgroupId = source.DeliverypointgroupId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Deliverypoint ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v23.Deliverypoint source)
        {
            Obymobi.Logic.Model.Mobile.Deliverypoint target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Deliverypoint();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v24.Converters.DeliverypointConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
