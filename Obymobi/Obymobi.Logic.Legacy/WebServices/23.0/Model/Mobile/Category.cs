using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v23
{
    /// <summary>
    /// Model class which represents a category
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Category"), IncludeInCodeGeneratorForXamarin]
    public class Category : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Category type
        /// </summary>
        public Category()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets of sets the id of the category
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int CategoryId
        { get; set; }

        /// <summary>
        /// Gets of sets the id of the parent category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ParentCategoryId
        { get; set; }

        /// <summary>
        /// Gets of sets the name of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets of sets the name of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets of sets the sort order of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets the geofencing type of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool Geofencing
        { get; set; }

        /// <summary>
        /// Gets of sets the schedule id of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ScheduleId
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the category
        /// </summary>
        [XmlArray("Categories")]
        [XmlArrayItem("Category")]
        [IncludeInCodeGeneratorForXamarin]
        public Category[] Categories
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the category
        /// </summary>
        [XmlArray("Products")]
        [XmlArrayItem("Product")]
        [IncludeInCodeGeneratorForXamarin]
        public Product[] Products
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the category
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForXamarin]
        public Media[] Media
        { get; set; }        

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool HidePrices
        { get; set; }

        /// <summary>
        /// Gets or sets the suggested products of the category
        /// </summary>
        [XmlArray("CategorySuggestions")]
        [XmlArrayItem("CategorySuggestion")]
        [IncludeInCodeGeneratorForXamarin]
        public CategorySuggestion[] CategorySuggestions
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Category Clone()
        {
            return this.Clone<Category>();
        }

        #endregion
    }
}
