using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v23
{
    /// <summary>
    /// Model class which represents a collection of analytics for Crave Console
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Analytics"), IncludeInCodeGeneratorForAndroid]
    public class Analytics : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Analytics type
        /// </summary>
        public Analytics()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the amount of orders for today
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OrderCount
        { get; set; }

        /// <summary>
        /// Gets or sets the amount of orders for the last hour
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OrderCountHour
        { get; set; }

        /// <summary>
        /// Gets or sets the amount of orders for yesterday
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OrderCountLastDay
        { get; set; }

        /// <summary>
        /// Gets or sets the amount of orders for this week
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OrderCountWeek
        { get; set; }

        /// <summary>
        /// Gets or sets the amount of orders for last week
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OrderCountLastWeek
        { get; set; }

        /// <summary>
        /// Gets or sets the amount of orders for this month
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OrderCountMonth
        { get; set; }

        /// <summary>
        /// Gets or sets the amount of orders for last month
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OrderCountLastMonth
        { get; set; }

        /// <summary>
        /// Gets or sets the revenue of today
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public decimal Revenue
        { get; set; }

        /// <summary>
        /// Gets or sets the totals for today
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public decimal Total
        { get; set; }

        /// <summary>
        /// Gets or sets the list of best selling products (names)
        /// </summary>
        [XmlArray("Bestsellers")]
        [XmlArrayItem("Bestseller")]
        public string[] Bestsellers
        { get; set; }

        /// <summary>
        /// Gets or sets the number of males that ordered
        /// </summary>
        [XmlElement]
        public int MaleCount
        { get; set; }

        /// <summary>
        /// Gets or sets the number of females that ordered
        /// </summary>
        [XmlElement]
        public int FemaleCount
        { get; set; }

        /// <summary>
        /// Gets or sets the average age
        /// </summary>
        [XmlElement]
        public int AverageAge
        { get; set; }

        /// <summary>
        /// Gets or sets the top list of visitor's cities
        /// </summary>
        [XmlArray("Cities")]
        [XmlArrayItem("City")]
        public string[] Cities
        { get; set; }

        /// <summary>
        /// Gets or sets the average serving time
        /// </summary>
        [XmlElement]
        public int AverageServingTime
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Analytics Clone()
        {
            return this.Clone<Analytics>();
        }

        #endregion
    }
}
