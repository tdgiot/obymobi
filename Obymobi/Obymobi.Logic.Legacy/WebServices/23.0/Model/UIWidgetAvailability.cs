using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v23
{
    /// <summary>
    /// Model class which represents an Availability item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "UIWidgetAvailability"), IncludeInCodeGeneratorForAndroid]
    public class UIWidgetAvailability : ModelBase
    {
        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the UIWidgetAvailability
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int UIWidgetAvailabilityId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the UIWidget this availability belongs to
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int UIWidgetId
        { get; set; }        

        /// <summary>
        /// Gets or sets the id of the availability
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AvailabilityId
        { get; set; }        

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UIWidgetAvailability Clone()
        {
            return this.Clone<UIWidgetAvailability>();
        }

        #endregion
    }
}
