using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v23
{
    /// <summary>
    /// Model class which represents a deliverypoint
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Deliverypoint"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class Deliverypoint : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Deliverypoint type
        /// </summary>
        public Deliverypoint()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Deliverypoint type using the specified parameters
        /// </summary>
        public Deliverypoint(int DeliverypointId, int Number, int CompanyId, string Name)
        {
            this.DeliverypointId = DeliverypointId;
            this.Number = Number;
            this.CompanyId = CompanyId;
            this.Name = Name;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the deliverypoint
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int DeliverypointId
        { get; set; }

        /// <summary>
        /// Gets or sets the number of the deliverypoint
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int Number
        { get; set; }

        /// <summary>
        /// Gets or sets the company id of the deliverypoint
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the deliverypoint
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the posdeliverypoint.
        /// </summary>
        /// <value>
        /// The posdeliverypoint.
        /// </value>
        [XmlElement]
        public Posdeliverypoint Posdeliverypoint
        { get; set; }

		/// <summary>
		/// Gets or sets the deliverypointgroup id
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
		public int DeliverypointgroupId
		{ get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControllerType { get; set; }

        /// <summary>
        /// Gets or sets the room controller IP
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RoomControllerIp
        { get; set; }

        /// <summary>
        /// Get or set the modbus slave id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControllerModbusSlaveId { get; set; }

        /// <summary>
        /// Gets or sets the google printer id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string GooglePrinterId
        { get; set; }

        /// <summary>
        /// Gets or sets the mac address allowed to print
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PrintFromMacAddress
        { get; set; }

        /// <summary>
        /// Gets or sets whether this DP should track analytics data
        /// </summary>
        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public bool EnableAnalytics { get; set; }        

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlConfigurationId 
        { get; set; }        

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Deliverypoint Clone()
        {
            return this.Clone<Deliverypoint>();
        }

        #endregion
    }
}
