using System;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using Dionysos.Security.Cryptography;
using Dionysos;
using Obymobi.Enums;
using System.Xml;

namespace Obymobi.Logic.Model.v23
{
    [DataContract]
    public class NocStatusReport : NocStatusReportBase
    {
        private const string HASH_APPENDIX = "TheresNotStatusLikeANocStatus";

        private const int INDEX_ENVIRONMENT = 0;
        private const int INDEX_ALTERNATIVE_URL = 1;
        private const int INDEX_CREATED = 2;
        private const int INDEX_SYSTEM_STATE = 3;
        private const int INDEX_CLIENTCOMMUNICATION_GLOBAL = 4;
        private const int INDEX_CLIENTCOMMUNICATION_COMPANY = 5;
        private const int INDEX_HTTPS_REQUIRED = 6;

        #region Methods

        public NocStatusReport()
        {
            this.Environment = 0;
            this.Age = 0;
            this.Created = DateTime.MinValue;
            this.AlternativeUrl = string.Empty;
            this.Hash = string.Empty;
            this.SystemState = (int)NocStatusSystemState.Normal;
            this.ClientCommunication = (int)ClientCommunicationMethod.All;
            this.HttpsRequired = 0;
        }

        public NocStatusReport(string statusString)
        {
            string[] elements;
            if (ValidateStatusString(statusString, out elements))
            {                
                // Environment                
                if (elements.Length > INDEX_ENVIRONMENT)
                {
                    int environmentInt;
                    if (!int.TryParse(elements[INDEX_ENVIRONMENT], out environmentInt))
                        this.Environment = -999;
                    else
                        this.Environment = environmentInt;
                }

                // Alternative Url
                if (elements.Length > INDEX_ALTERNATIVE_URL)
                {
                    this.AlternativeUrl = elements[INDEX_ALTERNATIVE_URL];
                }

                // Created
                if (elements.Length > INDEX_CREATED)
                {
                    long createdLong;
                    if (!long.TryParse(elements[INDEX_CREATED], out createdLong))
                        this.Created = DateTime.MinValue;
                    else
                        this.Created = createdLong.FromUnixTime();

                    // Estimated age (since we don't have server time)
                    this.Age = Convert.ToInt32((DateTime.UtcNow - this.Created).TotalMinutes);
                }

                // System State
                if (elements.Length > INDEX_SYSTEM_STATE)
                {
                    int systemStateInt;
                    NocStatusSystemState systemStateEnum;

                    if (!int.TryParse(elements[INDEX_SYSTEM_STATE], out systemStateInt))
                    {
                        this.SystemState = (int)NocStatusSystemState.Normal; // Normal
                    }
                    else if (!EnumUtil.TryParse(systemStateInt, out systemStateEnum))
                    {
                        this.SystemState = (int)NocStatusSystemState.Normal; // Normal
                    }
                    else
                    {
                        this.SystemState = (int)systemStateEnum;
                    }
                }

                // Client Communication Method
                if (elements.Length > INDEX_CLIENTCOMMUNICATION_GLOBAL)
                {
                    #region ClientCommunicationMethod specific for company

                    int companyClientCommunicationInt = -1;
                    ClientCommunicationMethod companyClientCommunicationEnum = ClientCommunicationMethod.All;

                    if (elements.Length > INDEX_CLIENTCOMMUNICATION_COMPANY)
                    {
                        if (!int.TryParse(elements[INDEX_CLIENTCOMMUNICATION_COMPANY], out companyClientCommunicationInt))
                        {
                            // Company ClientCommunication int could not be parsed
                            companyClientCommunicationEnum = ClientCommunicationMethod.All;
                        }
                        else if (!EnumUtil.TryParse(companyClientCommunicationInt, out companyClientCommunicationEnum))
                        {
                            // Company ClientCommunication int could not be parsed to an enum
                            companyClientCommunicationEnum = ClientCommunicationMethod.All;
                        }
                        else if (companyClientCommunicationInt > (int)ClientCommunicationMethod.All)
                        {
                            // Company ClientCommunication int is bigger than maximum allowed value
                            companyClientCommunicationEnum = ClientCommunicationMethod.All;
                        }
                    }

                    #endregion

                    #region ClientCommunicationMethod globally

                    int globalClientCommunicationInt = -1;
                    ClientCommunicationMethod globalClientCommunicationEnum = ClientCommunicationMethod.All;

                    if (!int.TryParse(elements[INDEX_CLIENTCOMMUNICATION_GLOBAL], out globalClientCommunicationInt))
                    {
                        // Global ClientCommunication int could not be parsed
                        globalClientCommunicationEnum = ClientCommunicationMethod.All;
                    }
                    else if (!EnumUtil.TryParse(globalClientCommunicationInt, out globalClientCommunicationEnum))
                    {
                        // Global ClientCommunication int could not be parsed to an enum
                        globalClientCommunicationEnum = ClientCommunicationMethod.All;
                    }
                    else if (globalClientCommunicationInt > (int)ClientCommunicationMethod.All)
                    {
                        // Global ClientCommunication int is bigger than maximum allowed value
                        globalClientCommunicationEnum = ClientCommunicationMethod.All;
                    }

                    #endregion

                    this.ClientCommunication = ((int)globalClientCommunicationEnum & (int)companyClientCommunicationEnum);
                }

                // HTTPS required flag
                if (elements.Length > INDEX_HTTPS_REQUIRED)
                {
                    int httpsRequiredInt;
                    if (!int.TryParse(elements[INDEX_HTTPS_REQUIRED], out httpsRequiredInt))
                    {
                        this.HttpsRequired = 0;
                    }
                    else
                    {
                        this.HttpsRequired = httpsRequiredInt;
                    }
                }
            }
        }        

        #endregion

        #region Properties

        /// <summary>
        /// Environment (enum: CloudEnvironment)
        /// </summary>
        [DataMember]
        public new int Environment { get; set; }

        /// <summary>
        /// Age of status message in minutes        
        /// </summary>        
        [DataMember]
        public new int Age { get; set; }

        /// <summary>
        /// Age of status message in minutes        
        /// </summary>
        [DataMember]
        public new DateTime Created { get; set; }

        /// <summary>
        /// Url of an alternative server if env == 2. Set to 0 when none.
        /// </summary>
        [DataMember]
        public new string AlternativeUrl { get; set; }

        /// <summary>
        /// Value of the NocStatusSystemState Enum
        /// </summary>
        [DataMember]
        public new int SystemState { get; set; }

		/// <summary>
		/// Value of the ClientCommunicationMethod enum
		/// </summary>
		[DataMember]
		public new int ClientCommunication { get; set; }

        /// <summary>
        /// Value of the HttpsRequired flag
        /// </summary>
        [DataMember]
        public new int HttpsRequired { get; set; }

        /// <summary>
        /// Hash of the env, age and alt + secret sauce
        /// </summary>
        [DataMember]
        public new string Hash { get; set; }

        [XmlAnyAttribute]
        public new XmlAttribute[] AnyAttributes;

        [XmlAnyElement]
        public new XmlElement[] AnyElements;

        #endregion

        #region Helper Methods

        /// <summary>
        /// Set the Age property based on DateTime.Now - this.Created
        /// </summary>
        public void SetAge()
        {
            this.Age = Convert.ToInt32((DateTime.UtcNow - this.Created).TotalMinutes);
        }

        /// <summary>
        /// Sets the Hash property based on this object it's contents
        /// </summary>
        public void SetHash()
        {
            this.Hash = this.GetHash();
        }

        /// <summary>
        /// Validate if the current value of .Hash is a valid has for this instance it's properties
        /// </summary>
        /// <returns></returns>
        public bool ValidateHash()
        {
            string shouldBeHash = this.GetHash();

            return (this.Hash == shouldBeHash);
        }

        /// <summary>
        /// Get the Hash based on the properties of this instance
        /// </summary>
        /// <returns></returns>
        private string GetHash()
        {
            return Hasher.GetHash(this.ToString() + NocStatusReport.HASH_APPENDIX, HashType.SHA512);
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3},{4},{5},{6}",
                this.Environment,
                this.AlternativeUrl,
                this.Created.ToUnixTime(),
                this.SystemState,
                this.ClientCommunication,
                this.ClientCommunication,
                this.HttpsRequired);
        }

        #endregion
    }
}