using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v23
{
    /// <summary>
    /// Model class which represents an language
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Language"), IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class Language : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Language type
        /// </summary>
        public Language()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the name of the language
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the code of the code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Code
        { get; set; }

        /// <summary>
        /// Gets or sets the localized name of the language
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string LocalizedName
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets the deliverypointCaption of the deliverypointgroup
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string DeliverypointCaption
        { get; set; }

        /// <summary>
        /// Gets or sets the venue page description
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string VenuePageDescription
        { get; set; }

        /// <summary>
        /// Gets or sets the browser age verification title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string BrowserAgeVerificationTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the browser age verification text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string BrowserAgeVerificationText
        { get; set; }

        /// <summary>
        /// Parent of this language
        /// </summary>
        [XmlAttribute("Parent")]
        public string Parent
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Language Clone()
        {
            return this.Clone<Language>();
        }

        #endregion
    }
}
