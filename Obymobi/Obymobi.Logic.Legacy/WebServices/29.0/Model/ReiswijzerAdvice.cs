using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v29
{
	/// <summary>
	/// Model class which represents a company
	/// </summary>
    [Serializable, XmlRootAttribute(ElementName = "ReiswijzerAdvice"), IncludeInCodeGeneratorForFlex]
	public class ReiswijzerAdvice
	{
		#region Constructors

		/// <summary>
		/// Constructs an instance of the Obymobi.Logic.Model.Company type
		/// </summary>
		public ReiswijzerAdvice()
		{
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the id of the company
		/// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
		public int ReiswijzerAdviceId
		{ get; set; }

        /// <summary>
        /// Gets or sets from place.
        /// </summary>
        /// <value>
        /// From place.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string FromPlace
		{ get; set; }

        /// <summary>
        /// Gets or sets from street.
        /// </summary>
        /// <value>
        /// From street.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string FromStreet
		{ get; set; }

        /// <summary>
        /// Gets or sets from house.
        /// </summary>
        /// <value>
        /// From house.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string FromHouse
		{ get; set; }

        /// <summary>
        /// Gets or sets to place.
        /// </summary>
        /// <value>
        /// To place.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ToPlace
		{ get; set; }

        /// <summary>
        /// Gets or sets to street.
        /// </summary>
        /// <value>
        /// To street.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ToStreet
		{ get; set; }

        /// <summary>
        /// Gets or sets to house.
        /// </summary>
        /// <value>
        /// To house.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ToHouse
		{ get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string Date
		{ get; set; }

        /// <summary>
        /// Gets or sets the reiswijzer solutions.
        /// </summary>
        /// <value>
        /// The reiswijzer solutions.
        /// </value>
		[XmlArray("ReiswijzerSolutions")]
		[XmlArrayItem("ReiswijzerSolutions")]
        [IncludeInCodeGeneratorForFlex]
		public ReiswijzerSolution[] ReiswijzerSolutions
		{ get; set; }

		#endregion
	}

}
