using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v29
{ 
	[Serializable, XmlRootAttribute(ElementName = "InfraredCommand"), IncludeInCodeGeneratorForAndroid]
    public class InfraredCommand : ModelBase
    {
        #region Constructors
        
        public InfraredCommand()
        {
        }

        #endregion

        #region Xml Properties

        [XmlElement, PrimaryKeyFieldOfModel]        
        [IncludeInCodeGeneratorForAndroid]
        public int InfraredCommandId
        { get; set; }
        
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Hex
        { get; set; }

        #endregion

        #region Methods

        public InfraredCommand Clone()
        {
            return this.Clone<InfraredCommand>();
        }

        #endregion
    }
}
