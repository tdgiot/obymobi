using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v29
{
    /// <summary>
    /// Model class which represents an entertainment item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Entertainment"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid]
    public class Entertainment : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Entertainment type
        /// </summary>
        public Entertainment()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the entertainment
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the entertainment item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the package name of the entertainment item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PackageName
        { get; set; }

        /// <summary>
        /// Gets or sets the class name of the entertainment item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ClassName
        { get; set; }

        /// <summary>
        /// Gets or sets the text color of the entertainment item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string TextColor
        { get; set; }

        /// <summary>
        /// Gets or sets the background color of the entertainment item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string BackgroundColor
        { get; set; }

        /// <summary>
        /// Gets or sets the default url for the entertainment item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string DefaultEntertainmenturl
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the entertainment item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentType
        { get; set; }

        /// <summary>
        /// Gets or sets the code of the feature
        /// </summary>
        [XmlElement]
        public int FeatureCode
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the social media
        /// </summary>
        [XmlElement]
        public int SocialmediaType
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the survey related to this entertainment
        /// </summary>
        [XmlElement]
        public string SurveyName
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the form related to this entertainment
        /// </summary>
        [XmlElement]
        public string FormName
        { get; set; }

        /// <summary>
        /// Gets or sets if this entertainment should show a back button
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool BackButton
        { get; set; }

        /// <summary>
        /// Gets or sets the of the category that contains this entertainment
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentcategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the description of this entertainment
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForFlex]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether the category can be used as announcement action
        /// </summary>
        [XmlElement]
        public bool AnnouncementAction
        { get; set; }

        /// <summary>
        /// Gets or sets the order id of the entertainment
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether to prevent the browserview from caching data or not
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PreventCaching
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether to postfix the requests with a timestamp
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PostfixWithTimestamp
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether to show the home button
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool HomeButton
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether to show the menu container
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool MenuContainer
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether to show the navigate button
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool NavigateButton
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether to show the navigation bar
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool NavigationBar
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether to show the title in the header
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool TitleAsHeader
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether to navigate to urls within restricted access
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool RestrictedAccess
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Filename
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FileVersion
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AppDataClearInterval { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AppCloseInterval { get; set; }

        [XmlElement]                
        [IncludeInCodeGeneratorForAndroid]        
        public int SiteId { get; set; }

        /// <summary>
        /// Gets or sets the alterations of the product
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        /// <summary>
        /// Gets or sets the tags of the entertainment item
        /// </summary>
        [XmlArray("AdvertisementTags")]
        [XmlArrayItem("AdvertisementTag")]
        [IncludeInCodeGeneratorForFlex]
        public AdvertisementTag[] AdvertisementTags
        { get; set; }

        /// <summary>
        /// Gets or sets the accessable urls for this entertainment item
        /// </summary>
        [XmlArray("Entertainmenturls")]
        [XmlArrayItem("Entertainmenturl")]
        [IncludeInCodeGeneratorForAndroid]
        public Entertainmenturl[] Entertainmenturls
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Entertainment Clone()
        {
            return this.Clone<Entertainment>();
        }

        #endregion
    }
}
