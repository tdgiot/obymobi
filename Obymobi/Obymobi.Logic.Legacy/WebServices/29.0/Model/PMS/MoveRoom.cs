using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v29
{
    /// <summary>
    /// Folio model
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "MoveRoom"), IncludeInCodeGeneratorForAndroid]
    public class MoveRoom : ModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MoveRoom"/> class.
        /// </summary>        
        public MoveRoom()
        {
        }

        /// <summary>
        /// The new DeliverypointNumber, i.e. RoomNumber
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string NewDeliverypointNumber { get; set; }

        /// <summary>
        /// The old DeliverypointNumber, i.e. RoomNumber
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string OldDeliverypointNumber { get; set; }

        /// <summary>
        /// Account number
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string AccountNumber { get; set; }       
    }
}
