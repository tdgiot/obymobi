namespace Obymobi.Logic.Model.v29.Converters
{
	public class UIScheduleItemConverter : ModelConverterBase<Obymobi.Logic.Model.v29.UIScheduleItem, Obymobi.Logic.Model.UIScheduleItem>
	{
        public UIScheduleItemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.UIScheduleItemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.UIScheduleItem ConvertModelToLegacyModel(Obymobi.Logic.Model.UIScheduleItem source)
        {
            Obymobi.Logic.Model.v29.UIScheduleItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.UIScheduleItem();
                target.UIScheduleItemId = source.UIScheduleItemId;
                target.UIWidgetId = source.UIWidgetId;
                target.MediaId = source.MediaId;

                if (source.UIScheduleItemOccurrences != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v29.Converters.UIScheduleItemOccurrenceConverter uIScheduleItemOccurrencesConverter = new Obymobi.Logic.Model.v29.Converters.UIScheduleItemOccurrenceConverter();
                    target.UIScheduleItemOccurrences = (UIScheduleItemOccurrence[])uIScheduleItemOccurrencesConverter.ConvertArrayToLegacyArray(source.UIScheduleItemOccurrences);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.UIScheduleItem ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.UIScheduleItem source)
        {
            Obymobi.Logic.Model.UIScheduleItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UIScheduleItem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.UIScheduleItemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
