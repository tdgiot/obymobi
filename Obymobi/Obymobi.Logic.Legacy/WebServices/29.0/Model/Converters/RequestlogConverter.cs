namespace Obymobi.Logic.Model.v29.Converters
{
	public class RequestlogConverter : ModelConverterBase<Obymobi.Logic.Model.v29.Requestlog, Obymobi.Logic.Model.Requestlog>
	{
        public RequestlogConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.RequestlogConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.Requestlog ConvertModelToLegacyModel(Obymobi.Logic.Model.Requestlog source)
        {
            Obymobi.Logic.Model.v29.Requestlog target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.Requestlog();
                target.RequestlogId = source.RequestlogId;
                target.ServerName = source.ServerName;
                target.UserAgent = source.UserAgent;
                target.Identifier = source.Identifier;
                target.CustomerId = source.CustomerId;
                target.TerminalId = source.TerminalId;
                target.Phonenumber = source.Phonenumber;
                target.MethodName = source.MethodName;
                target.ResultCode = source.ResultCode;
                target.ResultMessage = source.ResultMessage;
                target.Parameter1 = source.Parameter1;
                target.Parameter2 = source.Parameter2;
                target.Parameter3 = source.Parameter3;
                target.Parameter4 = source.Parameter4;
                target.Parameter5 = source.Parameter5;
                target.Parameter6 = source.Parameter6;
                target.ResultBody = source.ResultBody;
                target.ErrorMessage = source.ErrorMessage;
                target.ErrorStackTrace = source.ErrorStackTrace;
                target.Xml = source.Xml;
                target.RawRequest = source.RawRequest;
                target.TraceFromMobile = source.TraceFromMobile;
                target.MobileIdentifier = source.MobileIdentifier;
                target.MobileName = source.MobileName;
                target.MobileOS = source.MobileOS;
                target.MobilePlatform = source.MobilePlatform;
                target.MobileVendor = source.MobileVendor;
                target.DeviceInfo = source.DeviceInfo;
                target.Log = source.Log;
                target.ResultEnumTypeName = source.ResultEnumTypeName;
                target.ResultEnumValueName = source.ResultEnumValueName;
                target.SourceApplication = source.SourceApplication;
                target.Created = source.Created;
                target.Updated = source.Updated;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Requestlog ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.Requestlog source)
        {
            Obymobi.Logic.Model.Requestlog target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Requestlog();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.RequestlogConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
