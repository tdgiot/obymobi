namespace Obymobi.Logic.Model.v29.Converters
{
	public class SurveyLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v29.SurveyLanguage, Obymobi.Logic.Model.SurveyLanguage>
	{
        public SurveyLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.SurveyLanguageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.SurveyLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.SurveyLanguage source)
        {
            Obymobi.Logic.Model.v29.SurveyLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.SurveyLanguage();
                target.SurveyLanguageId = source.SurveyLanguageId;
                target.SurveyId = source.SurveyId;
                target.LanguageCode = source.LanguageCode;
                target.SavingTitle = source.SavingTitle;
                target.SavingMessage = source.SavingMessage;
                target.ProcessedTitle = source.ProcessedTitle;
                target.ProcessedMessage = source.ProcessedMessage;
            }

            return target;
        }

        public override Obymobi.Logic.Model.SurveyLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.SurveyLanguage source)
        {
            Obymobi.Logic.Model.SurveyLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SurveyLanguage();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.SurveyLanguageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
