namespace Obymobi.Logic.Model.v29.Converters
{
	public class FormLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v29.FormLanguage, Obymobi.Logic.Model.FormLanguage>
	{
        public FormLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.FormLanguageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.FormLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.FormLanguage source)
        {
            Obymobi.Logic.Model.v29.FormLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.FormLanguage();
                target.FormLanguageId = source.FormLanguageId;
                target.FormId = source.FormId;
                target.LanguageCode = source.LanguageCode;
                target.Name = source.Name;
                target.Title = source.Title;
                target.Subtitle = source.Subtitle;
                target.SavingTitle = source.SavingTitle;
                target.SavingMessage = source.SavingMessage;
                target.ProcessedTitle = source.ProcessedTitle;
                target.ProcessedMessage = source.ProcessedMessage;
            }

            return target;
        }

        public override Obymobi.Logic.Model.FormLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.FormLanguage source)
        {
            Obymobi.Logic.Model.FormLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.FormLanguage();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.FormLanguageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
