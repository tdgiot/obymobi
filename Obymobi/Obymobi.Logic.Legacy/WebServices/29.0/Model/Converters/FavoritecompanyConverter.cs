namespace Obymobi.Logic.Model.v29.Converters
{
	public class FavoritecompanyConverter : ModelConverterBase<Obymobi.Logic.Model.v29.Favoritecompany, Obymobi.Logic.Model.Favoritecompany>
	{
        public FavoritecompanyConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.FavoritecompanyConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.Favoritecompany ConvertModelToLegacyModel(Obymobi.Logic.Model.Favoritecompany source)
        {
            Obymobi.Logic.Model.v29.Favoritecompany target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.Favoritecompany();
                target.FavoritecompanyId = source.FavoritecompanyId;
                target.CustomerId = source.CustomerId;
                target.CompanyId = source.CompanyId;
                target.CompanyName = source.CompanyName;
                target.Code = source.Code;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Favoritecompany ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.Favoritecompany source)
        {
            Obymobi.Logic.Model.Favoritecompany target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Favoritecompany();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.FavoritecompanyConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
