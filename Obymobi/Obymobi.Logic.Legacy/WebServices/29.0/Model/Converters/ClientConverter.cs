namespace Obymobi.Logic.Model.v29.Converters
{
	public class ClientConverter : ModelConverterBase<Obymobi.Logic.Model.v29.Client, Obymobi.Logic.Model.Client>
	{
        public ClientConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.ClientConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.Client ConvertModelToLegacyModel(Obymobi.Logic.Model.Client source)
        {
            Obymobi.Logic.Model.v29.Client target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.Client();
                target.ClientId = source.ClientId;
                target.CompanyId = source.CompanyId;
                target.CompanyOwnerUsername = source.CompanyOwnerUsername;
                target.CompanyOwnerPassword = source.CompanyOwnerPassword;
                target.CustomerId = source.CustomerId;
                target.CustomerHashedPassword = source.CustomerHashedPassword;
                target.CompanyName = source.CompanyName;
                target.LastRequestUTC = source.LastRequestUTC;
                target.LastRequestNotifiedBySMSUTC = source.LastRequestNotifiedBySMSUTC;
                target.SmsNotificationRequired = source.SmsNotificationRequired;
                target.LastStatus = source.LastStatus;
                target.LastStatusText = source.LastStatusText;
                target.CurrentState = source.CurrentState;
                target.OperationMode = source.OperationMode;
                target.RelatedCustomerFullname = source.RelatedCustomerFullname;
                target.PingOfLifeInterval = source.PingOfLifeInterval;
                target.ScreenTimeoutInteval = source.ScreenTimeoutInteval;
                target.DeviceActivationRequired = source.DeviceActivationRequired;
                target.ReorderNotificationEnabled = source.ReorderNotificationEnabled;
                target.ReorderNotificationInterval = source.ReorderNotificationInterval;
                target.ReorderNotificationAnnouncementId = source.ReorderNotificationAnnouncementId;
                target.Locale = source.Locale;
                target.LogToFile = source.LogToFile;
                target.Pincode = source.Pincode;
                target.PincodeSU = source.PincodeSU;
                target.PincodeGM = source.PincodeGM;
                target.UseManualDeliverypoint = source.UseManualDeliverypoint;
                target.UseManualDeliverypointEncryption = source.UseManualDeliverypointEncryption;
                target.EncryptionSalt = source.EncryptionSalt;
                target.HideDeliverypointNumbers = source.HideDeliverypointNumbers;
                target.HidePrices = source.HidePrices;
                target.ClearSessionOnTimeout = source.ClearSessionOnTimeout;
                target.ResetTimeout = source.ResetTimeout;
                target.DefaultProductFullView = source.DefaultProductFullView;
                target.MarketingSurveyUrl = source.MarketingSurveyUrl;
                target.HotelUrl1 = source.HotelUrl1;
                target.HotelUrl1Caption = source.HotelUrl1Caption;
                target.HotelUrl1Zoom = source.HotelUrl1Zoom;
                target.HotelUrl2 = source.HotelUrl2;
                target.HotelUrl2Caption = source.HotelUrl2Caption;
                target.HotelUrl2Zoom = source.HotelUrl2Zoom;
                target.HotelUrl3 = source.HotelUrl3;
                target.HotelUrl3Caption = source.HotelUrl3Caption;
                target.HotelUrl3Zoom = source.HotelUrl3Zoom;
                target.DimLevelDull = source.DimLevelDull;
                target.DimLevelMedium = source.DimLevelMedium;
                target.DimLevelBright = source.DimLevelBright;
                target.DockedDimLevelDull = source.DockedDimLevelDull;
                target.DockedDimLevelMedium = source.DockedDimLevelMedium;
                target.DockedDimLevelBright = source.DockedDimLevelBright;
                target.PowerSaveTimeout = source.PowerSaveTimeout;
                target.PowerSaveLevel = source.PowerSaveLevel;
                target.OutOfChargeLevel = source.OutOfChargeLevel;
                target.OutOfChargeTitle = source.OutOfChargeTitle;
                target.OutOfChargeMessage = source.OutOfChargeMessage;
                target.OrderProcessedTitle = source.OrderProcessedTitle;
                target.OrderProcessedMessage = source.OrderProcessedMessage;
                target.OrderFailedTitle = source.OrderFailedTitle;
                target.OrderFailedMessage = source.OrderFailedMessage;
                target.OrderSavingTitle = source.OrderSavingTitle;
                target.OrderSavingMessage = source.OrderSavingMessage;
                target.OrderCompletedTitle = source.OrderCompletedTitle;
                target.OrderCompletedMessage = source.OrderCompletedMessage;
                target.OrderCompletedEnabled = source.OrderCompletedEnabled;
                target.ServiceSavingTitle = source.ServiceSavingTitle;
                target.ServiceSavingMessage = source.ServiceSavingMessage;
                target.ServiceProcessedTitle = source.ServiceProcessedTitle;
                target.ServiceProcessedMessage = source.ServiceProcessedMessage;
                target.ServiceFailedTitle = source.ServiceFailedTitle;
                target.ServiceFailedMessage = source.ServiceFailedMessage;
                target.RatingSavingTitle = source.RatingSavingTitle;
                target.RatingSavingMessage = source.RatingSavingMessage;
                target.RatingProcessedTitle = source.RatingProcessedTitle;
                target.RatingProcessedMessage = source.RatingProcessedMessage;
                target.OrderingNotAvailableMessage = source.OrderingNotAvailableMessage;
                target.DailyOrderReset = source.DailyOrderReset;
                target.SleepTime = source.SleepTime;
                target.WakeUpTime = source.WakeUpTime;
                target.OrderStatusOnthecaseEnabled = source.OrderStatusOnthecaseEnabled;
                target.OrderStatusOnthecaseTitle = source.OrderStatusOnthecaseTitle;
                target.OrderStatusOnthecaseMessage = source.OrderStatusOnthecaseMessage;
                target.OrderStatusCompleteEnabled = source.OrderStatusCompleteEnabled;
                target.OrderStatusCompleteTitle = source.OrderStatusCompleteTitle;
                target.OrderStatusCompleteMessage = source.OrderStatusCompleteMessage;
                target.ServiceRequestStatusOnTheCaseTitle = source.ServiceRequestStatusOnTheCaseTitle;
                target.ServiceRequestStatusOnTheCaseMessage = source.ServiceRequestStatusOnTheCaseMessage;
                target.ServiceRequestStatusCompleteTitle = source.ServiceRequestStatusCompleteTitle;
                target.ServiceRequestStatusCompleteMessage = source.ServiceRequestStatusCompleteMessage;
                target.FreeformMessageTitle = source.FreeformMessageTitle;
                target.MacAddress = source.MacAddress;
                target.DeliverypointId = source.DeliverypointId;
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.DeliverypointgroupId = source.DeliverypointgroupId;
                target.WifiSecurityMethod = source.WifiSecurityMethod;
                target.WifiSsid = source.WifiSsid;
                target.WifiPassword = source.WifiPassword;
                target.WifiEapMode = source.WifiEapMode;
                target.WifiPhase2Authentication = source.WifiPhase2Authentication;
                target.WifiIdentity = source.WifiIdentity;
                target.WifiAnonymousIdentity = source.WifiAnonymousIdentity;
                target.HomepageSlideshowInterval = source.HomepageSlideshowInterval;
                target.AnnouncementDuration = source.AnnouncementDuration;
                target.DeliverypointCaption = source.DeliverypointCaption;
                target.UIModeId = source.UIModeId;
                target.PrintingEnabled = source.PrintingEnabled;
                target.LastCommunicationMethod = source.LastCommunicationMethod;
                target.LastSupportToolsVersion = source.LastSupportToolsVersion;
                target.LastAgentVersion = source.LastAgentVersion;
                target.LastCloudEnvironment = source.LastCloudEnvironment;
                target.LastSupportToolsRequest = source.LastSupportToolsRequest;
                target.IsChargerRemovedDialogEnabled = source.IsChargerRemovedDialogEnabled;
                target.ChargerRemovedDialogTitle = source.ChargerRemovedDialogTitle;
                target.ChargerRemovedDialogText = source.ChargerRemovedDialogText;
                target.IsChargerRemovedReminderDialogEnabled = source.IsChargerRemovedReminderDialogEnabled;
                target.ChargerRemovedReminderDialogTitle = source.ChargerRemovedReminderDialogTitle;
                target.ChargerRemovedReminderDialogText = source.ChargerRemovedReminderDialogText;
                target.UpdateEmenuDownloaded = source.UpdateEmenuDownloaded;
                target.UpdateAgentDownloaded = source.UpdateAgentDownloaded;
                target.UpdateSupportToolsDownloaded = source.UpdateSupportToolsDownloaded;
                target.PmsDeviceLockedTitle = source.PmsDeviceLockedTitle;
                target.PmsDeviceLockedText = source.PmsDeviceLockedText;
                target.PmsCheckinFailedTitle = source.PmsCheckinFailedTitle;
                target.PmsCheckinFailedText = source.PmsCheckinFailedText;
                target.PmsRestartTitle = source.PmsRestartTitle;
                target.PmsRestartText = source.PmsRestartText;
                target.PmsWelcomeTitle = source.PmsWelcomeTitle;
                target.PmsWelcomeText = source.PmsWelcomeText;
                target.PmsCheckoutApproveText = source.PmsCheckoutApproveText;
                target.PmsWelcomeTimeoutMinutes = source.PmsWelcomeTimeoutMinutes;
                target.PmsCheckoutCompleteTitle = source.PmsCheckoutCompleteTitle;
                target.PmsCheckoutCompleteText = source.PmsCheckoutCompleteText;
                target.GooglePrinterId = source.GooglePrinterId;
                target.PmsCheckoutCompleteTimeoutMinutes = source.PmsCheckoutCompleteTimeoutMinutes;
                target.ScreenOffMode = source.ScreenOffMode;
                target.PowerButtonSoftBehaviour = source.PowerButtonSoftBehaviour;
                target.PowerButtonHardBehaviour = source.PowerButtonHardBehaviour;
                target.RoomserviceCharge = source.RoomserviceCharge;
                target.EmailDocumentEnabled = source.EmailDocumentEnabled;
                target.DefaultRoomControlAreaName = source.DefaultRoomControlAreaName;
                target.ScreensaverMode = source.ScreensaverMode;
                target.TurnOffPrivacyTitle = source.TurnOffPrivacyTitle;
                target.TurnOffPrivacyText = source.TurnOffPrivacyText;
                target.ItemCurrentlyUnavailableText = source.ItemCurrentlyUnavailableText;
                target.AlarmSetWhileNotChargingTitle = source.AlarmSetWhileNotChargingTitle;
                target.AlarmSetWhileNotChargingText = source.AlarmSetWhileNotChargingText;
                target.DeliveryLocationMismatchTitle = source.DeliveryLocationMismatchTitle;
                target.DeliveryLocationMismatchText = source.DeliveryLocationMismatchText;
                target.PriceScheduleId = source.PriceScheduleId;
                target.IsOrderitemAddedDialogEnabled = source.IsOrderitemAddedDialogEnabled;
                target.IsClearBasketDialogEnabled = source.IsClearBasketDialogEnabled;
                target.ClearBasketTitle = source.ClearBasketTitle;
                target.ClearBasketText = source.ClearBasketText;
                target.BrowserAgeVerificationEnabled = source.BrowserAgeVerificationEnabled;
                target.BrowserAgeVerificationLayout = source.BrowserAgeVerificationLayout;
                target.RestartApplicationDialogEnabled = source.RestartApplicationDialogEnabled;
                target.RebootDeviceDialogEnabled = source.RebootDeviceDialogEnabled;
                target.RestartTimeoutSeconds = source.RestartTimeoutSeconds;
                target.CraveAnalytics = source.CraveAnalytics;
                target.CraveAnalyticsSas = source.CraveAnalyticsSas;
                target.CraveAnalyticsUrl = source.CraveAnalyticsUrl;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v29.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v29.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.Entertainmentcategories != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v29.Converters.EntertainmentcategoryConverter entertainmentcategoriesConverter = new Obymobi.Logic.Model.v29.Converters.EntertainmentcategoryConverter();
                    target.Entertainmentcategories = (Entertainmentcategory[])entertainmentcategoriesConverter.ConvertArrayToLegacyArray(source.Entertainmentcategories);
                }

                if (source.UIThemes != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v29.Converters.UIThemeConverter uIThemesConverter = new Obymobi.Logic.Model.v29.Converters.UIThemeConverter();
                    target.UIThemes = (UITheme[])uIThemesConverter.ConvertArrayToLegacyArray(source.UIThemes);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v29.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v29.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }

                target.LastCommunicationMethodEnum = source.LastCommunicationMethodEnum;
                target.ScreenOffModeEnum = source.ScreenOffModeEnum;
                target.PowerButtonSoftModeEnum = source.PowerButtonSoftModeEnum;
                target.PowerButtonHardModeEnum = source.PowerButtonHardModeEnum;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Client ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.Client source)
        {
            Obymobi.Logic.Model.Client target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Client();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.ClientConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
