namespace Obymobi.Logic.Model.v29.Converters
{
	public class CategorySuggestionConverter : ModelConverterBase<Obymobi.Logic.Model.v29.CategorySuggestion, Obymobi.Logic.Model.CategorySuggestion>
	{
        public CategorySuggestionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.CategorySuggestionConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.CategorySuggestion ConvertModelToLegacyModel(Obymobi.Logic.Model.CategorySuggestion source)
        {
            Obymobi.Logic.Model.v29.CategorySuggestion target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.CategorySuggestion();
                target.CategorySuggestionId = source.CategorySuggestionId;
                target.CategoryId = source.CategoryId;
                target.ProductId = source.ProductId;
                target.Checkout = source.Checkout;
            }

            return target;
        }

        public override Obymobi.Logic.Model.CategorySuggestion ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.CategorySuggestion source)
        {
            Obymobi.Logic.Model.CategorySuggestion target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.CategorySuggestion();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.CategorySuggestionConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
