namespace Obymobi.Logic.Model.v29.Converters
{
	public class PosIntegrationInformationConverter : ModelConverterBase<Obymobi.Logic.Model.v29.PosIntegrationInformation, Obymobi.Logic.Model.PosIntegrationInformation>
	{
        public PosIntegrationInformationConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.PosIntegrationInformationConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.PosIntegrationInformation ConvertModelToLegacyModel(Obymobi.Logic.Model.PosIntegrationInformation source)
        {
            Obymobi.Logic.Model.v29.PosIntegrationInformation target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.PosIntegrationInformation();

                if (source.Products != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v29.Converters.ProductConverter productsConverter = new Obymobi.Logic.Model.v29.Converters.ProductConverter();
                    target.Products = (Product[])productsConverter.ConvertArrayToLegacyArray(source.Products);
                }

                if (source.Categories != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v29.Converters.CategoryConverter categoriesConverter = new Obymobi.Logic.Model.v29.Converters.CategoryConverter();
                    target.Categories = (Category[])categoriesConverter.ConvertArrayToLegacyArray(source.Categories);
                }

                if (source.Deliverypoints != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v29.Converters.DeliverypointConverter deliverypointsConverter = new Obymobi.Logic.Model.v29.Converters.DeliverypointConverter();
                    target.Deliverypoints = (Deliverypoint[])deliverypointsConverter.ConvertArrayToLegacyArray(source.Deliverypoints);
                }

                if (source.Terminal != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v29.Converters.TerminalConverter terminalConverter = new Obymobi.Logic.Model.v29.Converters.TerminalConverter();
                    target.Terminal = (Terminal)terminalConverter.ConvertModelToLegacyModel(source.Terminal);
                }

                target.POSConnectorType = source.POSConnectorType;
                target.UnlockDeliverypointProductId = source.UnlockDeliverypointProductId;
                target.BatteryLowProductId = source.BatteryLowProductId;
                target.ClientDisconnectedProductId = source.ClientDisconnectedProductId;
                target.SystemMessagesDeliverypointId = source.SystemMessagesDeliverypointId;
                target.AltSystemMessagesDeliverypointId = source.AltSystemMessagesDeliverypointId;

                if (source.Icrtouchprintermapping != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v29.Converters.IcrtouchprintermappingConverter icrtouchprintermappingConverter = new Obymobi.Logic.Model.v29.Converters.IcrtouchprintermappingConverter();
                    target.Icrtouchprintermapping = (Icrtouchprintermapping)icrtouchprintermappingConverter.ConvertModelToLegacyModel(source.Icrtouchprintermapping);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.PosIntegrationInformation ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.PosIntegrationInformation source)
        {
            Obymobi.Logic.Model.PosIntegrationInformation target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.PosIntegrationInformation();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.PosIntegrationInformationConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
