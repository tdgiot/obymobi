namespace Obymobi.Logic.Model.v29.Converters
{
	public class DeliverypointgroupConverter : ModelConverterBase<Obymobi.Logic.Model.v29.Deliverypointgroup, Obymobi.Logic.Model.Deliverypointgroup>
	{
        public DeliverypointgroupConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.DeliverypointgroupConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.Deliverypointgroup ConvertModelToLegacyModel(Obymobi.Logic.Model.Deliverypointgroup source)
        {
            Obymobi.Logic.Model.v29.Deliverypointgroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.Deliverypointgroup();
                target.DeliverypointgroupId = source.DeliverypointgroupId;
                target.CompanyId = source.CompanyId;
                target.Name = source.Name;
                target.OrderHistoryDialogEnabled = source.OrderHistoryDialogEnabled;
                target.PmsIntegration = source.PmsIntegration;
                target.PmsAllowShowBill = source.PmsAllowShowBill;
                target.PmsAllowExpressCheckout = source.PmsAllowExpressCheckout;
                target.PmsLockClientWhenNotCheckedIn = source.PmsLockClientWhenNotCheckedIn;
                target.DeliverypointCaption = source.DeliverypointCaption;
                target.UseHardKeyboard = source.UseHardKeyboard;
                target.PmsAllowShowGuestName = source.PmsAllowShowGuestName;
                target.UIModeId = source.UIModeId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Deliverypointgroup ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.Deliverypointgroup source)
        {
            Obymobi.Logic.Model.Deliverypointgroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Deliverypointgroup();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.DeliverypointgroupConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
