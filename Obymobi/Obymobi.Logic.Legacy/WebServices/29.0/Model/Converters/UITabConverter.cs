namespace Obymobi.Logic.Model.v29.Converters
{
	public class UITabConverter : ModelConverterBase<Obymobi.Logic.Model.v29.UITab, Obymobi.Logic.Model.UITab>
	{
        public UITabConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.UITabConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.UITab ConvertModelToLegacyModel(Obymobi.Logic.Model.UITab source)
        {
            Obymobi.Logic.Model.v29.UITab target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.UITab();
                target.UITabId = source.UITabId;
                target.UIModeId = source.UIModeId;
                target.Caption = source.Caption;
                target.Type = source.Type;
                target.CategoryId = source.CategoryId;
                target.EntertainmentId = source.EntertainmentId;
                target.SiteId = source.SiteId;
                target.URL = source.URL;
                target.Zoom = source.Zoom;
                target.Width = source.Width;
                target.SortOrder = source.SortOrder;
                target.Visible = source.Visible;
                target.RestrictedAccess = source.RestrictedAccess;
                target.AllCategoryVisible = source.AllCategoryVisible;
                target.ShowPmsMessagegroups = source.ShowPmsMessagegroups;
                target.MapId = source.MapId;

                if (source.UIWidgets != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v29.Converters.UIWidgetConverter uIWidgetsConverter = new Obymobi.Logic.Model.v29.Converters.UIWidgetConverter();
                    target.UIWidgets = (UIWidget[])uIWidgetsConverter.ConvertArrayToLegacyArray(source.UIWidgets);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v29.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v29.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.UITab ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.UITab source)
        {
            Obymobi.Logic.Model.UITab target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UITab();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.UITabConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
