namespace Obymobi.Logic.Model.v29.Converters
{
	public class PriceScheduleItemOccurrenceConverter : ModelConverterBase<Obymobi.Logic.Model.v29.PriceScheduleItemOccurrence, Obymobi.Logic.Model.PriceScheduleItemOccurrence>
	{
        public PriceScheduleItemOccurrenceConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.PriceScheduleItemOccurrenceConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.PriceScheduleItemOccurrence ConvertModelToLegacyModel(Obymobi.Logic.Model.PriceScheduleItemOccurrence source)
        {
            Obymobi.Logic.Model.v29.PriceScheduleItemOccurrence target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.PriceScheduleItemOccurrence();
                target.PriceScheduleItemOccurrenceId = source.PriceScheduleItemOccurrenceId;
                target.StartTime = source.StartTime;
                target.EndTime = source.EndTime;
                target.Recurring = source.Recurring;
                target.RecurrenceType = source.RecurrenceType;
                target.RecurrenceRange = source.RecurrenceRange;
                target.RecurrenceStart = source.RecurrenceStart;
                target.RecurrenceEnd = source.RecurrenceEnd;
                target.RecurrenceOccurenceCount = source.RecurrenceOccurenceCount;
                target.RecurrencePeriodicity = source.RecurrencePeriodicity;
                target.RecurrenceDayNumber = source.RecurrenceDayNumber;
                target.RecurrenceWeekDays = source.RecurrenceWeekDays;
                target.RecurrenceWeekOfMonth = source.RecurrenceWeekOfMonth;
                target.RecurrenceMonth = source.RecurrenceMonth;
            }

            return target;
        }

        public override Obymobi.Logic.Model.PriceScheduleItemOccurrence ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.PriceScheduleItemOccurrence source)
        {
            Obymobi.Logic.Model.PriceScheduleItemOccurrence target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.PriceScheduleItemOccurrence();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.PriceScheduleItemOccurrenceConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
