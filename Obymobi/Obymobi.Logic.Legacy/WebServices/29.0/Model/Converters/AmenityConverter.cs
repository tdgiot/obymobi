namespace Obymobi.Logic.Model.v29.Converters
{
	public class AmenityConverter : ModelConverterBase<Obymobi.Logic.Model.v29.Amenity, Obymobi.Logic.Model.Amenity>
	{
        public AmenityConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.AmenityConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.Amenity ConvertModelToLegacyModel(Obymobi.Logic.Model.Amenity source)
        {
            Obymobi.Logic.Model.v29.Amenity target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.Amenity();
                target.AmenityId = source.AmenityId;
                target.Name = source.Name;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v29.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v29.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Amenity ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.Amenity source)
        {
            Obymobi.Logic.Model.Amenity target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Amenity();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.AmenityConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
