namespace Obymobi.Logic.Model.v29.Converters
{
	public class AnalyticsConverter : ModelConverterBase<Obymobi.Logic.Model.v29.Analytics, Obymobi.Logic.Model.Analytics>
	{
        public AnalyticsConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.AnalyticsConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.Analytics ConvertModelToLegacyModel(Obymobi.Logic.Model.Analytics source)
        {
            Obymobi.Logic.Model.v29.Analytics target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.Analytics();
                target.OrderCount = source.OrderCount;
                target.OrderCountHour = source.OrderCountHour;
                target.OrderCountLastDay = source.OrderCountLastDay;
                target.OrderCountWeek = source.OrderCountWeek;
                target.OrderCountLastWeek = source.OrderCountLastWeek;
                target.OrderCountMonth = source.OrderCountMonth;
                target.OrderCountLastMonth = source.OrderCountLastMonth;
                target.Revenue = source.Revenue;
                target.Total = source.Total;
                target.Bestsellers = source.Bestsellers;
                target.MaleCount = source.MaleCount;
                target.FemaleCount = source.FemaleCount;
                target.AverageAge = source.AverageAge;
                target.Cities = source.Cities;
                target.AverageServingTime = source.AverageServingTime;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Analytics ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.Analytics source)
        {
            Obymobi.Logic.Model.Analytics target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Analytics();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.AnalyticsConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
