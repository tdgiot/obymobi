namespace Obymobi.Logic.Model.v29.Converters
{
	public class CompanyTimestampsUpdateConverter : ModelConverterBase<Obymobi.Logic.Model.v29.CompanyTimestampsUpdate, Obymobi.Logic.Model.CompanyTimestampsUpdate>
	{
        public CompanyTimestampsUpdateConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v30.Converters.CompanyTimestampsUpdateConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v29.CompanyTimestampsUpdate ConvertModelToLegacyModel(Obymobi.Logic.Model.CompanyTimestampsUpdate source)
        {
            Obymobi.Logic.Model.v29.CompanyTimestampsUpdate target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v29.CompanyTimestampsUpdate();
                target.CompanyId = source.CompanyId;
                target.CompanyDataLastModifiedTicks = source.CompanyDataLastModifiedTicks;
                target.CompanyMediaLastModifiedTicks = source.CompanyMediaLastModifiedTicks;
                target.MenuDataLastModifiedTicks = source.MenuDataLastModifiedTicks;
                target.MenuMediaLastModifiedTicks = source.MenuMediaLastModifiedTicks;
                target.PosIntegrationInformationLastModifiedTicks = source.PosIntegrationInformationLastModifiedTicks;
                target.DeliverypointDataLastModifiedTicks = source.DeliverypointDataLastModifiedTicks;
                target.SurveyDataLastModifiedTicks = source.SurveyDataLastModifiedTicks;
                target.SurveyMediaLastModifiedTicks = source.SurveyMediaLastModifiedTicks;
                target.AnnouncementDataLastModifiedTicks = source.AnnouncementDataLastModifiedTicks;
                target.AnnouncementMediaLastModifiedTicks = source.AnnouncementMediaLastModifiedTicks;
                target.EntertainmentDataLastModifiedTicks = source.EntertainmentDataLastModifiedTicks;
                target.EntertainmentMediaLastModifiedTicks = source.EntertainmentMediaLastModifiedTicks;
                target.AdvertisementDataLastModifiedTicks = source.AdvertisementDataLastModifiedTicks;
                target.AdvertisementMediaLastModifiedTicks = source.AdvertisementMediaLastModifiedTicks;
            }

            return target;
        }

        public override Obymobi.Logic.Model.CompanyTimestampsUpdate ConvertLegacyModelToModel(Obymobi.Logic.Model.v29.CompanyTimestampsUpdate source)
        {
            Obymobi.Logic.Model.CompanyTimestampsUpdate target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.CompanyTimestampsUpdate();

                // Copy default values from new version
                new Obymobi.Logic.Model.v30.Converters.CompanyTimestampsUpdateConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
