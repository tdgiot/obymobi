using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v29
{
    /// <summary>
    /// Model class which represents an advertisement
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Advertisement"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid]
    public class Advertisement : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Advertisement type
        /// </summary>
        public Advertisement()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the advertisement
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int AdvertisementId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the advertisement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the advertisement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
		[XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the entertainment
        /// </summary>
		[XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the action category id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionCategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the action entertainment id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionEntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the action entertainmentcategory id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionEntertainmentCategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the action url
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ActionUrl
        { get; set; }

        /// <summary>
        /// Gets or sets the action site id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionSiteId
        { get; set; }

        /// <summary>
        /// Gets or sets the action page id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionPageId
        { get; set; }

        /// <summary>
        /// Gets or sets the tags of the advertisement
        /// </summary>
        [XmlArray("AdvertisementTags")]
        [XmlArrayItem("AdvertisementTag")]
        [IncludeInCodeGeneratorForFlex]
        public AdvertisementTag[] AdvertisementTags
        { get; set; }        

        /// <summary>
        /// Gets or sets the media of the advertisement
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Advertisement Clone()
        {
            return this.Clone<Advertisement>();
        }

        #endregion
    }
}
