using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v29
{
    /// <summary>
    /// Model class which represents a point-of-sale alterationitem which is a link between alteration and alterationoptions
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Posalterationitem")]
    public class Posalterationitem : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Posorder type
        /// </summary>
        public Posalterationitem()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the pos alteration item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int PosalterationitemId
        { get; set; }

        /// <summary>
        /// Gets or sets the ExternalId as used in the POS system
        /// </summary>
        [XmlElement]
        public string ExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the ExternalId of the Posalteration
        /// </summary>
        [XmlElement]
        public string ExternalPosalterationId
        { get; set; }

        /// <summary>
        /// Gets or sets the ExternalId of the Posalterationoption
        /// </summary>
        [XmlElement]
        public string ExternalPosalterationoptionId
        { get; set; }
        /// <summary>
        /// Gets or sets the flag which indicates whether to select this Posalterationitem
        /// </summary>
        [XmlElement]
        public bool SelectOnDefault
        { get; set; }
        /// <summary>
        /// Gets or sets the sort order of the pos alteration item
        /// </summary>
        [XmlElement]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Posalterationitem Clone()
        {
            return this.Clone<Posalterationitem>();
        }

        #endregion
    }
}
