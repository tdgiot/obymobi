using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v29
{
	/// <summary>
	/// Model class for the Client Status
	/// </summary>
	[Serializable, XmlRootAttribute(ElementName = "ClientStatus"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid]
	public class ClientStatus : ModelBase
	{
		/// <summary>
		/// Primary Key is required in a Model, but this one is not used.
		/// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
		[IncludeInCodeGeneratorForFlex]
		public int ClientStatusId
		{ get; set; }

		/// <summary>
		/// Indicates if the database is working
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		public bool Database
		{ get; set; }

		/// <summary>
		/// Gets or sets the processed order ids.
		/// This field is always filled by the webservice, not by the emenu
		/// </summary>
		[XmlArray("ProcessedOrderIds")]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		public int[] ProcessedOrderIds
		{ get; set; }

		/// <summary>
		/// Gets or sets the failed order ids.
		/// This field is always filled by the webservice, not by the emenu
		/// </summary>
		[XmlArray("FailedOrderIds")]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		public int[] FailedOrderIds
		{ get; set; }

		/// <summary>
		/// Gets or sets the processed order item ids.
		/// This field is always filled by the webservice, not by the emenu
		/// </summary>
		[XmlArray("ProcessedOrderitemIds")]
		[IncludeInCodeGeneratorForAndroid]
		public int[] ProcessedOrderitemIds
		{ get; set; }

		/// <summary>
		/// Gets or sets the failed order item ids.
		/// This field is always filled by the webservice, not by the emenu
		/// </summary>
		[XmlArray("FailedOrderitemIds")]
		[IncludeInCodeGeneratorForAndroid]
		public int[] FailedOrderitemIds
		{ get; set; }

		/// <summary>
		/// Gets or sets the OrderIds of the orders which are pending emenu
		/// And on which to retrieve the orderStatus
		/// This field is always filled by the emenu, not by the webservice
		/// </summary>
		[XmlArray("PendingOrderIdsOnEmenu")]
		[IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
		public int[] PendingOrderIdsOnEmenu
		{ get; set; }

		/// <summary>
		/// Gets or sets the ClientStatusCode as defined in enum ClientStatusCoede
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		public int ClientStatusCode
		{ get; set; }

		/// <summary>
		/// Gets or sets the ClientOperationMode as defined in enum ClientOperationMode
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
		[IncludeInCodeGeneratorForAndroid]
		public int ClientOperationMode
		{ get; set; }

		/// <summary>
		/// Gets or sets the version of the client
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
		public string ApplicationVersion
		{ get; set; }

		/// <summary>
		/// Gets or sets the version of the device OS
		/// </summary>
		[XmlElement]
        [IncludeInCodeGeneratorForAndroid]
		public string OsVersion
		{ get; set; }

		/// <summary>
		/// Gets or sets the processed order ids.
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForAndroid]
		public bool RequestToSendLog
		{ get; set; }

		/// <summary>
		/// Gets or sets the processed order ids.
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForAndroid]
		public string Log
		{ get; set; }

		/// <summary>
		/// Current IP address of the client
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForAndroid]
		public string IpAddress
		{ get; set; }

		/// <summary>
		/// Current battery level of the client
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForAndroid]
		public int BatteryLevel
		{ get; set; }

		/// <summary>
		/// Current charging the battery
		/// </summary>
		[XmlElement]
        [IncludeInCodeGeneratorForAndroid]
		public bool IsCharging
		{ get; set; }

		/// <summary>
		/// Gets or sets the last known deliverypoint number of the client
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForAndroid]
		public string DeliverypointNumber
		{ get; set; }

        /// <summary>
        /// Gets or sets the last known deliverypoint number of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DeliverypointId
        { get; set; }

		/// <summary>
		/// Tells the emenu to display the dialog oder nicht
		/// </summary>
		[XmlElement]
		public bool ShowBatteryLowDialog
		{ get; set; }

		[XmlElement]
		public string TestField
		{ get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string WifiSsid
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int WifiStrength
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool AgentIsRunning
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool SupportToolsIsRunning
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool BluetoothKeyboardConnected 
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool BluetoothPrinterConnected { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool IsDevelopmentDevice { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool RoomControlConnected { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool DoNotDisturbActive { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool ServiceRoomActive { get; set; }

		#region Methods

		/// <summary>
		/// Clones this instance.
		/// </summary>
		/// <returns></returns>
		public ClientStatus Clone()
		{
			return this.Clone<ClientStatus>();
		}

		#endregion
	}
}
