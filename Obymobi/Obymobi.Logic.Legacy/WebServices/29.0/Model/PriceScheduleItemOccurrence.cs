using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v29
{
    /// <summary>
    /// Model class which represents a price scheduled item occurrence
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "PriceScheduleItemOccurrence"), IncludeInCodeGeneratorForAndroid]
    public class PriceScheduleItemOccurrence : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.PriceScheduleItemOccurrence type
        /// </summary>
        public PriceScheduleItemOccurrence()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the priceScheduleItemOccurrence
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int PriceScheduleItemOccurrenceId
        { get; set; }

        /// <summary>
        /// Gets or sets the start time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string StartTime
        { get; set; }

        /// <summary>
        /// Gets or sets the end time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string EndTime
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not the item is recurring
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Recurring
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceType
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence range
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceRange
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence start time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RecurrenceStart
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence end time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RecurrenceEnd
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence occurence count
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceOccurenceCount
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence periodicity
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrencePeriodicity
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence day number
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceDayNumber
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence week days
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceWeekDays
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence week of month
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceWeekOfMonth
        { get; set; }

        /// <summary>
        /// Gets or sets the recurrence month
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RecurrenceMonth
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public PriceScheduleItemOccurrence Clone()
        {
            return this.Clone<PriceScheduleItemOccurrence>();
        }

        #endregion
    }
}
