using Newtonsoft.Json;

namespace Obymobi.Logic.Model.v29
{
    /// <summary>
    /// ManagerProduct model
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ManagerProduct
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManagerProduct"/> class.
        /// </summary>
        public ManagerProduct()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ManagerProduct"/> class.
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <param name="name">The name.</param>
        /// <param name="quantity">The quantity.</param>
        /// <param name="price">The price.</param>
        /// <param name="total">The total.</param>
        public ManagerProduct(int productId, string name, int quantity, decimal price, decimal total)
        {
            this.ProductId = productId;
            this.Name = name;
            this.Quantity = quantity;
            this.Price = price;
            this.Total = total;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the product id.
        /// </summary>
        /// <value>
        /// The product id.
        /// </value>
        [JsonProperty]
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        [JsonProperty]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        /// <value>
        /// The quantity.
        /// </value>
        [JsonProperty]
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the price.
        /// </summary>
        /// <value>
        /// The price.
        /// </value>
        [JsonProperty]
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        /// <value>
        /// The total.
        /// </value>
        [JsonProperty]
        public decimal Total { get; set; }

        #endregion
    }
}
