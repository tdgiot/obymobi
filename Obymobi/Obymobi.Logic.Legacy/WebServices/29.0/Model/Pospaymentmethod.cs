using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v29
{
    /// <summary>
    /// Model class which represents a point-of-sale paymentmethod
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Pospaymentmethod")]
    public class Pospaymentmethod : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Pospaymentmethod type
        /// </summary>
        public Pospaymentmethod()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Pospaymentmethod type using the specified parameters
        /// </summary>
        public Pospaymentmethod(int PospaymentmethodId, int CompanyId, string ExternalId, string ExternalPospaymentmethodgroupId, string Name, string Number)
        {
            this.PospaymentmethodId = PospaymentmethodId;
            this.CompanyId = CompanyId;
            this.ExternalId = ExternalId;
            this.Name = Name;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the paymentmethod
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int PospaymentmethodId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the external id of the pos paymentmethod
        /// </summary>
        [XmlElement]
        public string ExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the pos paymentmethod
        /// </summary>
        [XmlElement]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }


        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Pospaymentmethod Clone()
        {
            return this.Clone<Pospaymentmethod>();
        }

        #endregion
    }
}
