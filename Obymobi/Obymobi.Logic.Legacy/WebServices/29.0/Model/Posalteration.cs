using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v29
{
    /// <summary>
    /// Model class which represents a point-of-sale alteration
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Posalteration")]
    [DataContract]
    public class Posalteration : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Posorder type
        /// </summary>
        public Posalteration()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the posalteration
        /// </summary>
        /// <value>
        /// The posalteration id.
        /// </value>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int PosalterationId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the ExternalId as used in the POS system
        /// </summary>
        [XmlElement]
        [DataMember]
        public string ExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the Name
        /// </summary>
        [XmlElement]
        [DataMember]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the minimum amount of options needed
        /// </summary>
        [XmlElement]
        [DataMember]
        public int? MinOptions
        { get; set; }

        /// <summary>
        /// Gets or sets the maximum amount of options possible
        /// </summary>
        [XmlElement]
        [DataMember]
        public int? MaxOptions
        { get; set; }

        /// <summary>
        /// Gets or sets the posalterationoptions.
        /// </summary>
        /// <value>The posalterationoptions.</value>
        [XmlArray("Posalterationoptions")]
        [XmlArrayItem("Posalterationoption")]
        [DataMember]
        public Posalterationoption[] Posalterationoptions
        { get; set; }


        /// <summary>
        /// Gets or sets the posalterationitems.
        /// </summary>
        /// <value>The posalterationitems.</value>
        [XmlArray("Posalterationitems")]
        [XmlArrayItem("Posalterationitem")]
        public Posalterationitem[] Posalterationitems
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }


        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Posalteration Clone()
        {
            return this.Clone<Posalteration>();
        }

        #endregion
    }
}
