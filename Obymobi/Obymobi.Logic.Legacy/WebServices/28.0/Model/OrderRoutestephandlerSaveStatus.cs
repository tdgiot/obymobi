using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v28
{
    /// <summary>
    /// OrderRoutestephandlerSaveStatus model
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "OrderRoutestephandlerSaveStatus"), IncludeInCodeGeneratorForAndroid]
    public class OrderRoutestephandlerSaveStatus : ModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrderRoutestephandlerSaveStatus"/> class.
        /// </summary>
        public OrderRoutestephandlerSaveStatus()
        {
            OrderRoutestephandlerId = 0;
        }

        /// <summary>
        /// Gets or sets the order routestephandler id.
        /// </summary>
        /// <value>
        /// The order id.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OrderRoutestephandlerId
        { get; set; }

        // GK-ROUTING: Todo, make a Database field for this as well
        /// <summary>
        /// Gets or sets the order routestephandler guid.
        /// </summary>
        [XmlElement]
        public string OrderRoutestephandlerGuid
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Status
        { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Error
        { get; set; }

        /// <summary>
        /// Gets or sets the order routestephandler id.
        /// </summary>
        /// <value>
        /// The order id.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OrderId
        { get; set; }

        // GK-ROUTING: Todo, make a Database field for this as well
        /// <summary>
        /// Gets or sets the order routestephandler guid.
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderGuid
        {
            get;
            set;
        }

        #region Methods

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public new string ToString()
        {
            return StringUtilLight.FormatSafe("OrderRoutestephandlerId: '{0}', Guid: '{1}', Status: '{2}', Error: '{3}'",
                this.OrderRoutestephandlerId,
                this.OrderRoutestephandlerGuid,
                this.Status.ToEnum<OrderRoutestephandlerStatus>().ToString(),
                this.Error.ToEnum<OrderProcessingError>().ToString());
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public OrderRoutestephandlerSaveStatus Clone()
        {
            return this.Clone<OrderRoutestephandlerSaveStatus>();
        }

        #endregion
    }
}
