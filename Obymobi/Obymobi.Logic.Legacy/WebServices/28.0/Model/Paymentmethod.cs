using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v28
{
    /// <summary>
    /// Model class which represents a payment method
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Paymentmethod"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForXamarin]
    public class Paymentmethod : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Paymentmethod type
        /// </summary>
        public Paymentmethod()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Paymentmethod type
        /// </summary>
        public Paymentmethod(int PaymentmethodId, string Name, int CompanyId)
        {
            this.PaymentmethodId = PaymentmethodId;
            this.Name = Name;
            this.CompanyId = CompanyId;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the payment method
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int PaymentmethodId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the the payment method type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int PaymentmethodType
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the title of the thank you message for this payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string ThankTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the body of the thank you message for this payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string ThankMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the minimum limit of this payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public decimal MinimumLimit
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Paymentmethod Clone()
        {
            return this.Clone<Paymentmethod>();
        }

        #endregion
    }
}
