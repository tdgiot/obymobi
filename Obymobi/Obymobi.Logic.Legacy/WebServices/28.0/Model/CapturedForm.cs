using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v28
{
    /// <summary>
    /// Model class which represents a category
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CapturedForm")]
    public class CapturedForm : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CapturedForm type
        /// </summary>
        public CapturedForm()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets of sets the id of the captured form
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int CapturedFormId
        { get; set; }

        /// <summary>
        /// Gets of sets the field value of the captured form
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets of sets the field value of the captured form
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets of sets the field value of the captured form
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets of sets the field value of the captured form
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets of sets the field value of the captured form
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets of sets the field value of the captured form
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets of sets the field value of the captured form
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets of sets the field value of the captured form
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets of sets the field value of the captured form
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets of sets the field value of the captured form
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CapturedForm Clone()
        {
            return this.Clone<CapturedForm>();
        }

        #endregion
    }
}
