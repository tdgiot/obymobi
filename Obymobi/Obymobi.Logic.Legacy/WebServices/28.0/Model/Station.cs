using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v28
{
    /// <summary>
    /// Model class which represents a station
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Station"), IncludeInCodeGeneratorForAndroid]
    public class Station : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Station type
        /// </summary>
        public Station()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the Station
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int StationId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Caption
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Number
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Scene
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string SuccessMessage
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Description
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Url
        { get; set; }

        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Station Clone()
        {
            return this.Clone<Station>();
        }

        #endregion
    }
}
