using Newtonsoft.Json;

namespace Obymobi.Logic.Model.v28
{
    /// <summary>
    /// ManagerOrder model
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ManagerOrder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ManagerOrder"/> class.
        /// </summary>
        public ManagerOrder()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ManagerOrder"/> class.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <param name="deliverypointId">The deliverypoint id.</param>
        /// <param name="date">The date.</param>
        /// <param name="time">The time.</param>
        /// <param name="status">The status.</param>
        /// <param name="products">The products.</param>
        /// <param name="totalPrice">The total price.</param>
        public ManagerOrder(string orderId, int deliverypointId, string date, string time, int status, ManagerProduct[] products, decimal totalPrice)
        {
            this.OrderId = orderId;
            this.DeliverypointId = deliverypointId;
            this.Date = date;
            this.Time = time;
            this.Status = status;
            this.Products = products;
            this.PriceIn = totalPrice;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the order id.
        /// </summary>
        /// <value>
        /// The order id.
        /// </value>
        [JsonProperty]
        public string OrderId { get; set; }

        /// <summary>
        /// Gets or sets the deliverypoint id.
        /// </summary>
        /// <value>
        /// The deliverypoint id.
        /// </value>
        [JsonProperty]
        public int DeliverypointId { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
        [JsonProperty]
        public string Date { get; set; }

        /// <summary>
        /// Gets or sets the time.
        /// </summary>
        /// <value>
        /// The time.
        /// </value>
        [JsonProperty]
        public string Time { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        [JsonProperty]
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets the price in.
        /// </summary>
        /// <value>
        /// The price in.
        /// </value>
        [JsonProperty]
        public decimal PriceIn { get; set; }

        /// <summary>
        /// Gets or sets the products.
        /// </summary>
        /// <value>
        /// The products.
        /// </value>
        [JsonProperty]
        public ManagerProduct[] Products { get; set; }

        #endregion
    }
}
