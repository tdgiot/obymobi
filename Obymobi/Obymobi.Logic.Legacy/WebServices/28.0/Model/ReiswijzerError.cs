using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v28
{
	/// <summary>
	/// Model class which represents a company
	/// </summary>
	[Serializable, XmlRootAttribute(ElementName = "ReiswijzerError"), IncludeInCodeGeneratorForFlex]
	public class ReiswijzerError
	{
		#region Constructors

		/// <summary>
		/// Constructs an instance of the Obymobi.Logic.Model.Company type
		/// </summary>
		public ReiswijzerError()
		{
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the id of the company
		/// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
		public int ReiswijzerErrorId
		{ get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string Code
		{ get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string Text
		{ get; set; }


		#endregion
	}

}
