using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v28
{    
    [Serializable, XmlRootAttribute(ElementName = "Action"), IncludeInCodeGeneratorForAndroid]
    public class Action : ModelBase
    {
        #region Constructors        

        public Action()
        {
        }

        #endregion

        #region Properties
        
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]        
        public int DeliverypointgroupId
        { get; set; }
        
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int CategoryId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ProductId
        { get; set; }

        #endregion

        #region Methods
        
        public Action Clone()
        {
            return this.Clone<Action>();
        }

        #endregion
    }
}
