using Obymobi.Security;
using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Obymobi.Constants;
using System.Net;
using System.IO;

namespace Obymobi.Logic.Model.v28
{
    [DataContract]
    public class ConnectionInformation
    {
        // timestamp, hash, parameters

        private const string HASH_APPENDIX = "MakingConnectionsWithTheOtherWorld";

        public ConnectionInformation()
        { 
        }

        #region Methods

        public static bool TryRetrieveFromCloud(out ConnectionInformation connectionInformationObject)
        {
            bool success = false;
            connectionInformationObject = null;
            List<string> urls = new List<string>();
            urls.Add(ObymobiConstants.NocStatusConnectionInformationFileUrl1);
            urls.Add(ObymobiConstants.NocStatusConnectionInformationFileUrl2);

            // Iterate over NocStatusServices
            foreach (var url in urls)
            {
                // Try to get the file with the status from the Noc Status Service and check it's contents
                HttpWebResponse response;
                try
                {
                    string cacheSaveUrl = string.Format("{0}?update={1}", url, DateTime.Now.Ticks);
                    WebRequest request = WebRequest.Create(cacheSaveUrl);
                    request.Proxy = null;
                    response = (HttpWebResponse)request.GetResponse();

                    if (response != null && response.StatusCode == HttpStatusCode.OK)
                    {
                        // Validate it's the correct type of content
                        StreamReader sr = new StreamReader(response.GetResponseStream());
                        string content = sr.ReadToEnd();

                        if (ConnectionInformation.TryParse(content, out connectionInformationObject))
                            success = true;
                    }

                    if (response != null)
                        response.Close();
                }
                catch
                {
                    if (TestUtil.IsPcDeveloper)
                        throw;
                }

                if (success)
                    break;
            }

            return success;
        }

        public static bool TryParse(string connectionInformationText, out ConnectionInformation connectionInformationObject)
        {
            connectionInformationObject = null;

            try
            {
                string[] elements;
                if (ConnectionInformation.ValidateConnectionInformationString(connectionInformationText, out elements))
                {
                    connectionInformationObject = new ConnectionInformation();

                    // Connection string is encrypted
                    connectionInformationObject.ConnectionString = Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(elements[2]);

                    connectionInformationObject.Age = (DateTime.UtcNow - DateTimeUtil.FromUnixTime(Convert.ToInt64(elements[1])));
                    return true;
                }
            }
            catch
            { 
                // Returns false.
            }

            return false;
        }         

        public static bool ValidateConnectionInformationString(string connectionInformation, out string[] elements)
        {
            // Must be 3 elements Hash,Timestamp,ConnectionString
            if (connectionInformation.IsNullOrWhiteSpace())
            {
                elements = new string[] { };
                return false;
            }

            elements = connectionInformation.Split(new char[] { ',' }, StringSplitOptions.None);

            if (elements.Length >= 3)
            {
                // Validate hash
                StringBuilder sb = new StringBuilder();
                for(int i = 1; i < elements.Length; i++)
                {
                    sb.Append(elements[i]);
                }
                    
                string suppliedHash = elements[0];

                if (Hasher.IsHashValid(elements[0], ConnectionInformation.HASH_APPENDIX, sb.ToString()))
                {
                    long dummy;
                    if(!long.TryParse(elements[2], out dummy))
                        return false;

                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        public override string ToString()
        { 
            string encryptedConnectionString = Dionysos.Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(this.ConnectionString);
            long timestamp = DateTimeUtil.ToUnixTime(DateTime.UtcNow);
            string hash = Hasher.GetHash(ConnectionInformation.HASH_APPENDIX, string.Format("{0}{1}", timestamp, encryptedConnectionString));
            return string.Format("{0},{1},{2}", hash, timestamp, encryptedConnectionString);
        }

        #endregion

        #region Properties

        [DataMember]
        public string ConnectionString { get; set; }

        [DataMember]
        public TimeSpan Age { get; set; }

        #endregion

    }
}
