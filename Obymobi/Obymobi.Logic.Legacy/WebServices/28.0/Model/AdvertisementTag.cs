using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v28
{
    /// <summary>
    /// Model class which represents an advertisement tag
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "AdvertisementTag"), IncludeInCodeGeneratorForFlex]
    public class AdvertisementTag : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.AdvertisementTag type
        /// </summary>
        public AdvertisementTag()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the advertisement tag
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        public int AdvertisementTagId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the advertisement tag
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string Name
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public AdvertisementTag Clone()
        {
            return this.Clone<AdvertisementTag>();
        }

        #endregion
    }
}
