using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v28
{
    /// <summary>
    /// Model class which represents an deliverypointgroup language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "DeliverypointgroupLanguage"), IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class DeliverypointgroupLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.DeliverypointgroupLanguage type
        /// </summary>
        public DeliverypointgroupLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the deliverypointgroup
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string DeliverypointCaption
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public DeliverypointgroupLanguage Clone()
        {
            return this.Clone<DeliverypointgroupLanguage>();
        }

        #endregion
    }
}
