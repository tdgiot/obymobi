namespace Obymobi.Logic.Model.v28.Converters
{
	public class FolioConverter : ModelConverterBase<Obymobi.Logic.Model.v28.Folio, Obymobi.Logic.Model.Folio>
	{
        public FolioConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v29.Converters.FolioConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v28.Folio ConvertModelToLegacyModel(Obymobi.Logic.Model.Folio source)
        {
            Obymobi.Logic.Model.v28.Folio target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v28.Folio();
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.AccountNumber = source.AccountNumber;
                target.Error = source.Error;
                target.Balance = source.Balance;

                if (source.FolioItems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.FolioItemConverter folioItemsConverter = new Obymobi.Logic.Model.v28.Converters.FolioItemConverter();
                    target.FolioItems = (FolioItem[])folioItemsConverter.ConvertArrayToLegacyArray(source.FolioItems);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Folio ConvertLegacyModelToModel(Obymobi.Logic.Model.v28.Folio source)
        {
            Obymobi.Logic.Model.Folio target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Folio();

                // Copy default values from new version
                new Obymobi.Logic.Model.v29.Converters.FolioConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
