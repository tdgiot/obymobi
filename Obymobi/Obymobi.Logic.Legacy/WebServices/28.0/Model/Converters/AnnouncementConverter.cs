namespace Obymobi.Logic.Model.v28.Converters
{
	public class AnnouncementConverter : ModelConverterBase<Obymobi.Logic.Model.v28.Announcement, Obymobi.Logic.Model.Announcement>
	{
        public AnnouncementConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v29.Converters.AnnouncementConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v28.Announcement ConvertModelToLegacyModel(Obymobi.Logic.Model.Announcement source)
        {
            Obymobi.Logic.Model.v28.Announcement target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v28.Announcement();
                target.AnnouncementId = source.AnnouncementId;
                target.Title = source.Title;
                target.Subtitle = source.Subtitle;
                target.Text = source.Text;
                target.DateToShow = source.DateToShow;
                target.TimeToShow = source.TimeToShow;
                target.Recurring = source.Recurring;
                target.RecurringPeriod = source.RecurringPeriod;
                target.RecurringBegin = source.RecurringBegin;
                target.RecurringEnd = source.RecurringEnd;
                target.RecurringAmount = source.RecurringAmount;
                target.RecurringMinutes = source.RecurringMinutes;
                target.DialogType = source.DialogType;
                target.OnYes = source.OnYes;
                target.OnNo = source.OnNo;
                target.OnYesCategory = source.OnYesCategory;
                target.OnNoCategory = source.OnNoCategory;
                target.OnYesEntertainmentCategory = source.OnYesEntertainmentCategory;
                target.OnNoEntertainmentCategory = source.OnNoEntertainmentCategory;
                target.OnYesEntertainment = source.OnYesEntertainment;
                target.OnYesProduct = source.OnYesProduct;
                target.MediaId = source.MediaId;
                target.Duration = source.Duration;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v28.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Announcement ConvertLegacyModelToModel(Obymobi.Logic.Model.v28.Announcement source)
        {
            Obymobi.Logic.Model.Announcement target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Announcement();

                // Copy default values from new version
                new Obymobi.Logic.Model.v29.Converters.AnnouncementConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
