namespace Obymobi.Logic.Model.v28.Converters
{
	public class FoursquareTipConverter : ModelConverterBase<Obymobi.Logic.Model.v28.FoursquareTip, Obymobi.Logic.Model.FoursquareTip>
	{
        public FoursquareTipConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v29.Converters.FoursquareTipConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v28.FoursquareTip ConvertModelToLegacyModel(Obymobi.Logic.Model.FoursquareTip source)
        {
            Obymobi.Logic.Model.v28.FoursquareTip target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v28.FoursquareTip();
                target.Name = source.Name;
                target.PostedOn = source.PostedOn;
                target.Text = source.Text;
            }

            return target;
        }

        public override Obymobi.Logic.Model.FoursquareTip ConvertLegacyModelToModel(Obymobi.Logic.Model.v28.FoursquareTip source)
        {
            Obymobi.Logic.Model.FoursquareTip target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.FoursquareTip();

                // Copy default values from new version
                new Obymobi.Logic.Model.v29.Converters.FoursquareTipConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
