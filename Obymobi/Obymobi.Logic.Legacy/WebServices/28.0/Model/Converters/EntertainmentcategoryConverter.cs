namespace Obymobi.Logic.Model.v28.Converters
{
	public class EntertainmentcategoryConverter : ModelConverterBase<Obymobi.Logic.Model.v28.Entertainmentcategory, Obymobi.Logic.Model.Entertainmentcategory>
	{
        public EntertainmentcategoryConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v29.Converters.EntertainmentcategoryConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v28.Entertainmentcategory ConvertModelToLegacyModel(Obymobi.Logic.Model.Entertainmentcategory source)
        {
            Obymobi.Logic.Model.v28.Entertainmentcategory target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v28.Entertainmentcategory();
                target.EntertainmentcategoryId = source.EntertainmentcategoryId;
                target.Name = source.Name;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v28.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Entertainmentcategory ConvertLegacyModelToModel(Obymobi.Logic.Model.v28.Entertainmentcategory source)
        {
            Obymobi.Logic.Model.Entertainmentcategory target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Entertainmentcategory();

                // Copy default values from new version
                new Obymobi.Logic.Model.v29.Converters.EntertainmentcategoryConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
