namespace Obymobi.Logic.Model.v28.Converters
{
	public class ProductgroupItemConverter : ModelConverterBase<Obymobi.Logic.Model.v28.ProductgroupItem, Obymobi.Logic.Model.ProductgroupItem>
	{
        public ProductgroupItemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v29.Converters.ProductgroupItemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v28.ProductgroupItem ConvertModelToLegacyModel(Obymobi.Logic.Model.ProductgroupItem source)
        {
            Obymobi.Logic.Model.v28.ProductgroupItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v28.ProductgroupItem();
                target.ProductgroupItemId = source.ProductgroupItemId;
                target.SortOrder = source.SortOrder;
            }

            return target;
        }

        public override Obymobi.Logic.Model.ProductgroupItem ConvertLegacyModelToModel(Obymobi.Logic.Model.v28.ProductgroupItem source)
        {
            Obymobi.Logic.Model.ProductgroupItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.ProductgroupItem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v29.Converters.ProductgroupItemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
