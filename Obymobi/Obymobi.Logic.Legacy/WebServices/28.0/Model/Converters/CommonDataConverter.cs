namespace Obymobi.Logic.Model.v28.Converters
{
	public class CommonDataConverter : ModelConverterBase<Obymobi.Logic.Model.v28.CommonData, Obymobi.Logic.Model.CommonData>
	{
        public CommonDataConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v29.Converters.CommonDataConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v28.CommonData ConvertModelToLegacyModel(Obymobi.Logic.Model.CommonData source)
        {
            Obymobi.Logic.Model.v28.CommonData target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v28.CommonData();
                target.LastModifiedTicks = source.LastModifiedTicks;

                if (source.VenueCategories != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.VenueCategoryConverter venueCategoriesConverter = new Obymobi.Logic.Model.v28.Converters.VenueCategoryConverter();
                    target.VenueCategories = (VenueCategory[])venueCategoriesConverter.ConvertArrayToLegacyArray(source.VenueCategories);
                }

                if (source.Amenities != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.AmenityConverter amenitiesConverter = new Obymobi.Logic.Model.v28.Converters.AmenityConverter();
                    target.Amenities = (Amenity[])amenitiesConverter.ConvertArrayToLegacyArray(source.Amenities);
                }

                if (source.ActionButtons != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.ActionButtonConverter actionButtonsConverter = new Obymobi.Logic.Model.v28.Converters.ActionButtonConverter();
                    target.ActionButtons = (ActionButton[])actionButtonsConverter.ConvertArrayToLegacyArray(source.ActionButtons);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.CommonData ConvertLegacyModelToModel(Obymobi.Logic.Model.v28.CommonData source)
        {
            Obymobi.Logic.Model.CommonData target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.CommonData();

                // Copy default values from new version
                new Obymobi.Logic.Model.v29.Converters.CommonDataConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
