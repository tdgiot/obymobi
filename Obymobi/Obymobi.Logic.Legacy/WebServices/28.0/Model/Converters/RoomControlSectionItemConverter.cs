namespace Obymobi.Logic.Model.v28.Converters
{
	public class RoomControlSectionItemConverter : ModelConverterBase<Obymobi.Logic.Model.v28.RoomControlSectionItem, Obymobi.Logic.Model.RoomControlSectionItem>
	{
        public RoomControlSectionItemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v29.Converters.RoomControlSectionItemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v28.RoomControlSectionItem ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomControlSectionItem source)
        {
            Obymobi.Logic.Model.v28.RoomControlSectionItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v28.RoomControlSectionItem();
                target.RoomControlSectionItemId = source.RoomControlSectionItemId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.Visible = source.Visible;
                target.InfraredConfigurationId = source.InfraredConfigurationId;

                if (source.StationLists != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.StationListConverter stationListsConverter = new Obymobi.Logic.Model.v28.Converters.StationListConverter();
                    target.StationLists = (StationList[])stationListsConverter.ConvertArrayToLegacyArray(source.StationLists);
                }

                if (source.RoomControlWidgets != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.RoomControlWidgetConverter roomControlWidgetsConverter = new Obymobi.Logic.Model.v28.Converters.RoomControlWidgetConverter();
                    target.RoomControlWidgets = (RoomControlWidget[])roomControlWidgetsConverter.ConvertArrayToLegacyArray(source.RoomControlWidgets);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v28.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v28.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomControlSectionItem ConvertLegacyModelToModel(Obymobi.Logic.Model.v28.RoomControlSectionItem source)
        {
            Obymobi.Logic.Model.RoomControlSectionItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomControlSectionItem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v29.Converters.RoomControlSectionItemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
