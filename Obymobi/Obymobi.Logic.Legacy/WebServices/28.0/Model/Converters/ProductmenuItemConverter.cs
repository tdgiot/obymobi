namespace Obymobi.Logic.Model.v28.Converters
{
	public class ProductmenuItemConverter : ModelConverterBase<Obymobi.Logic.Model.v28.ProductmenuItem, Obymobi.Logic.Model.ProductmenuItem>
	{
        public ProductmenuItemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v29.Converters.ProductmenuItemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v28.ProductmenuItem ConvertModelToLegacyModel(Obymobi.Logic.Model.ProductmenuItem source)
        {
            Obymobi.Logic.Model.v28.ProductmenuItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v28.ProductmenuItem();
                target.Id = source.Id;
                target.Name = source.Name;
                target.PriceIn = source.PriceIn;
                target.ParentProductmenuItemId = source.ParentProductmenuItemId;
                target.ItemType = source.ItemType;
                target.SortOrder = source.SortOrder;
                target.IsFavorite = source.IsFavorite;
                target.OrderedQuantity = source.OrderedQuantity;
                target.TextColor = source.TextColor;
                target.BackgroundColor = source.BackgroundColor;
                target.Description = source.Description;
                target.DisplayOnHomepage = source.DisplayOnHomepage;
                target.Rateable = source.Rateable;
                target.Type = source.Type;
                target.SubType = source.SubType;
                target.AllowFreeText = source.AllowFreeText;
                target.AnnouncementAction = source.AnnouncementAction;
                target.HidePrices = source.HidePrices;
                target.Color = source.Color;
                target.ButtonText = source.ButtonText;
                target.CustomizeButtonText = source.CustomizeButtonText;
                target.WebTypeTabletUrl = source.WebTypeTabletUrl;
                target.WebTypeSmartphoneUrl = source.WebTypeSmartphoneUrl;
                target.ScheduleId = source.ScheduleId;
                target.ViewLayoutType = source.ViewLayoutType;
                target.DeliveryLocationType = source.DeliveryLocationType;
                target.VisibilityType = source.VisibilityType;
                target.Instructions = source.Instructions;

                if (source.Alterations != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.AlterationConverter alterationsConverter = new Obymobi.Logic.Model.v28.Converters.AlterationConverter();
                    target.Alterations = (Alteration[])alterationsConverter.ConvertArrayToLegacyArray(source.Alterations);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v28.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.ProductSuggestions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.ProductSuggestionConverter productSuggestionsConverter = new Obymobi.Logic.Model.v28.Converters.ProductSuggestionConverter();
                    target.ProductSuggestions = (ProductSuggestion[])productSuggestionsConverter.ConvertArrayToLegacyArray(source.ProductSuggestions);
                }

                if (source.AdvertisementTags != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.AdvertisementTagConverter advertisementTagsConverter = new Obymobi.Logic.Model.v28.Converters.AdvertisementTagConverter();
                    target.AdvertisementTags = (AdvertisementTag[])advertisementTagsConverter.ConvertArrayToLegacyArray(source.AdvertisementTags);
                }

                if (source.Ratings != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.RatingConverter ratingsConverter = new Obymobi.Logic.Model.v28.Converters.RatingConverter();
                    target.Ratings = (Rating[])ratingsConverter.ConvertArrayToLegacyArray(source.Ratings);
                }

                if (source.Attachments != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.AttachmentConverter attachmentsConverter = new Obymobi.Logic.Model.v28.Converters.AttachmentConverter();
                    target.Attachments = (Attachment[])attachmentsConverter.ConvertArrayToLegacyArray(source.Attachments);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v28.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v28.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.ProductmenuItem ConvertLegacyModelToModel(Obymobi.Logic.Model.v28.ProductmenuItem source)
        {
            Obymobi.Logic.Model.ProductmenuItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.ProductmenuItem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v29.Converters.ProductmenuItemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
