using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v28
{
    /// <summary>
    /// Model class which represents an entertainmentDependency item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "EntertainmentDependency")]
    public class EntertainmentDependency : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.EntertainmentDependency type
        /// </summary>
        public EntertainmentDependency()
        {
        }

        #endregion

        #region Xml Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        public int EntertainmentId
        { get; set; }

        [XmlElement]
        public string Name
        { get; set; }

        [XmlElement]
        public string PackageName
        { get; set; }

        [XmlElement]
        public string ClassName
        { get; set; }

        [XmlElement]
        public string Filename
        { get; set; }

        [XmlElement]
        public int FileVersion
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public EntertainmentDependency Clone()
        {
            return this.Clone<EntertainmentDependency>();
        }

        #endregion
    }
}
