using System;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v28
{
    /// <summary>
    /// Folio model
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "WakeUp"), IncludeInCodeGeneratorForAndroid]
    public class WakeUpStatus : ModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WakeUpStatus"/> class.
        /// </summary>        
        public WakeUpStatus()
        {
        }

        [XmlIgnore]
        public OnSiteServerWebserviceJobType OnSiteServerWebserviceJobType { get; set; }

        /// <summary>
        /// The DeliverypointNumber, i.e. RoomNumber
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string DeliverypointNumber { get; set; }

        /// <summary>
        /// Account number
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Wake up date
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public DateTime WakeUpDate { get; set; }

        /// <summary>
        /// Wake up time
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public DateTime WakeUpTime { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Status { get; set; }

        /// <summary>
        /// Info
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Info { get; set; }
    }
}
