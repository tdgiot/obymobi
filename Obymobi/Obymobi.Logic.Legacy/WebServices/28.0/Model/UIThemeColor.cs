using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v28
{
    /// <summary>
    /// Model class which represents theme color
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "UIThemeColor"), IncludeInCodeGeneratorForAndroid]
    public class UIThemeColor : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.UIThemeColor type
        /// </summary>
        public UIThemeColor()
        {
        }

        #endregion

        #region Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int UIThemeColorId
        { get; set; }

        [XmlElement]
        public int UIThemeId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Color
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UIThemeColor Clone()
        {
            return this.Clone<UIThemeColor>();
        }

        #endregion
    }
}
