using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v28
{
    /// <summary>
    /// Model class which represents a UIWidgetLanguage item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "UIWidgetLanguage"), IncludeInCodeGeneratorForAndroid]    
    public class UIWidgetLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.UIWidgetLanguage type
        /// </summary>
        public UIWidgetLanguage()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the widget language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int UIWidgetLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the caption of the widget
        /// </summary>
        [XmlElement]
        public string Caption
        { get; set; }

        /// <summary>
        /// Gets or sets fieldvalue
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets fieldvalue
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets fieldvalue
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets fieldvalue
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets fieldvalue
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UIWidgetLanguage Clone()
        {
            return this.Clone<UIWidgetLanguage>();
        }

        #endregion
    }
}
