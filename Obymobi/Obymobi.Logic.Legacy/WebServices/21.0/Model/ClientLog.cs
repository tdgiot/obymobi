using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// ClientLog model
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ClientLog
    {
        #region Properties

        /// <summary>
        /// Gets or sets the log.
        /// </summary>
        /// <value>
        /// The log.
        /// </value>
        [JsonProperty]
        public string Log { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonProperty]
        public long LastModified { get; set; }

        #endregion
    }
}
