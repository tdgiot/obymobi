using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Model class which represents a room control component language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "RoomControlComponentLanguage"), IncludeInCodeGeneratorForAndroid]
    public class RoomControlComponentLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.RoomControlComponentLanguage type
        /// </summary>
        public RoomControlComponentLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the room control component language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlComponentLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LanguageCode
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public RoomControlComponentLanguage Clone()
        {
            return this.Clone<RoomControlComponentLanguage>();
        }

        #endregion
    }
}
