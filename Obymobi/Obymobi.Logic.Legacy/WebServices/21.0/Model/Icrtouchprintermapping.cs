using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Icrtouchprintermapping model
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Icrtouchprintermapping")]
    public class Icrtouchprintermapping : ModelBase
    {
        /// <summary>
        /// Gets or sets the id of the Otoucho client
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int IcrtouchprintermappingId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        public int TerminalId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the icrtouchprintermapping deliverypoints.
        /// </summary>
        /// <value>
        /// The icrtouchprintermapping deliverypoints.
        /// </value>
        [XmlArray("IcrtouchprintermappingDeliverypoints")]
        [XmlArrayItem("IcrtouchprintermappingDeliverypoint")]
        public IcrtouchprintermappingDeliverypoint[] IcrtouchprintermappingDeliverypoints
        { get; set; }

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Icrtouchprintermapping Clone()
        {
            return this.Clone<Icrtouchprintermapping>();
        }

        #endregion
    }
}
