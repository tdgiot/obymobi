using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Model class which represents a surveyAnswer language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "SurveyAnswerLanguage")]
    public class SurveyAnswerLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.SurveyAnswerLanguage type
        /// </summary>
        public SurveyAnswerLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the surveyAnswer language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyAnswerLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the surveyAnswer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyAnswerId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the surveyAnswer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Answer
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public SurveyAnswerLanguage Clone()
        {
            return this.Clone<SurveyAnswerLanguage>();
        }

        #endregion
    }
}
