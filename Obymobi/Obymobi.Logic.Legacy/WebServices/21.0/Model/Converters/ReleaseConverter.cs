namespace Obymobi.Logic.Model.v21.Converters
{
	public class ReleaseConverter : ModelConverterBase<Obymobi.Logic.Model.v21.Release, Obymobi.Logic.Model.Release>
	{
        public ReleaseConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.ReleaseConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.Release ConvertModelToLegacyModel(Obymobi.Logic.Model.Release source)
        {
            Obymobi.Logic.Model.v21.Release target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.Release();
                target.ReleaseId = source.ReleaseId;
                target.Version = source.Version;
                target.Filename = source.Filename;
                target.Remarks = source.Remarks;
                target.Crc32 = source.Crc32;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Release ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.Release source)
        {
            Obymobi.Logic.Model.Release target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Release();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.ReleaseConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
