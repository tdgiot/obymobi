using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v21.Converters
{
	public class RoomControlComponentConverter : ModelConverterBase<Obymobi.Logic.Model.v21.RoomControlComponent, Obymobi.Logic.Model.RoomControlComponent>
	{
        public RoomControlComponentConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.RoomControlComponentConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.RoomControlComponent ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomControlComponent source)
        {
            Obymobi.Logic.Model.v21.RoomControlComponent target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.RoomControlComponent();
                target.RoomControlComponentId = source.RoomControlComponentId;
                target.Name = source.Name;
                target.NameSystem = source.NameSystem;
                target.Type = source.Type;
                target.SortOrder = source.SortOrder;
                target.Visible = source.Visible;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<RoomControlComponentLanguage> languages = new System.Collections.Generic.List<RoomControlComponentLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        RoomControlComponentLanguage model = new RoomControlComponentLanguage();
                        model.RoomControlComponentLanguageId = -1;
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.RoomControlComponentName);
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();

                        languages.Add(model);
                    }

                    target.RoomControlComponentLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomControlComponent ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.RoomControlComponent source)
        {
            Obymobi.Logic.Model.RoomControlComponent target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomControlComponent();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.RoomControlComponentConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
