namespace Obymobi.Logic.Model.v21.Converters
{
	public class DeliverypointgroupLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v21.DeliverypointgroupLanguage, Obymobi.Logic.Model.DeliverypointgroupLanguage>
	{
        public DeliverypointgroupLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.DeliverypointgroupLanguageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.DeliverypointgroupLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.DeliverypointgroupLanguage source)
        {
            Obymobi.Logic.Model.v21.DeliverypointgroupLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.DeliverypointgroupLanguage();
                target.LanguageCode = source.LanguageCode;
                target.DeliverypointCaption = source.DeliverypointCaption;
            }

            return target;
        }

        public override Obymobi.Logic.Model.DeliverypointgroupLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.DeliverypointgroupLanguage source)
        {
            Obymobi.Logic.Model.DeliverypointgroupLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.DeliverypointgroupLanguage();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.DeliverypointgroupLanguageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
