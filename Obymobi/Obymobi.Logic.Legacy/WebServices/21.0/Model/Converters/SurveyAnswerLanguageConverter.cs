namespace Obymobi.Logic.Model.v21.Converters
{
	public class SurveyAnswerLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v21.SurveyAnswerLanguage, Obymobi.Logic.Model.SurveyAnswerLanguage>
	{
        public SurveyAnswerLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.SurveyAnswerLanguageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.SurveyAnswerLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.SurveyAnswerLanguage source)
        {
            Obymobi.Logic.Model.v21.SurveyAnswerLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.SurveyAnswerLanguage();
                target.SurveyAnswerLanguageId = source.SurveyAnswerLanguageId;
                target.SurveyAnswerId = source.SurveyAnswerId;
                target.LanguageCode = source.LanguageCode;
                target.Answer = source.Answer;
            }

            return target;
        }

        public override Obymobi.Logic.Model.SurveyAnswerLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.SurveyAnswerLanguage source)
        {
            Obymobi.Logic.Model.SurveyAnswerLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SurveyAnswerLanguage();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.SurveyAnswerLanguageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
