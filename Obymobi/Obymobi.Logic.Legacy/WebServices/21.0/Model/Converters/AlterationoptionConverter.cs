using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v21.Converters
{
	public class AlterationoptionConverter : ModelConverterBase<Obymobi.Logic.Model.v21.Alterationoption, Obymobi.Logic.Model.Alterationoption>
	{
        public AlterationoptionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.AlterationoptionConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.Alterationoption ConvertModelToLegacyModel(Obymobi.Logic.Model.Alterationoption source)
        {
            Obymobi.Logic.Model.v21.Alterationoption target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.Alterationoption();
                target.AlterationoptionId = source.AlterationoptionId;
                target.Name = source.Name;
                target.PriceIn = source.PriceIn;
                target.Description = source.Description;
                target.PosproductId = source.PosproductId;
                target.IsProductRelated = source.IsProductRelated;

                if (source.Posalterationoption != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v21.Converters.PosalterationoptionConverter posalterationoptionConverter = new Obymobi.Logic.Model.v21.Converters.PosalterationoptionConverter();
                    target.Posalterationoption = (Posalterationoption)posalterationoptionConverter.ConvertModelToLegacyModel(source.Posalterationoption);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<AlterationoptionLanguage> languages = new System.Collections.Generic.List<AlterationoptionLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        AlterationoptionLanguage model = new AlterationoptionLanguage();
                        model.AlterationoptionLanguageId = -1;
                        model.AlterationoptionId = source.AlterationoptionId;
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.AlterationoptionName);                 

                        languages.Add(model);
                    }

                    target.AlterationoptionLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Alterationoption ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.Alterationoption source)
        {
            Obymobi.Logic.Model.Alterationoption target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Alterationoption();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.AlterationoptionConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
