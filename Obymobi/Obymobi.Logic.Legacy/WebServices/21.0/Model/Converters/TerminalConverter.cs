namespace Obymobi.Logic.Model.v21.Converters
{
	public class TerminalConverter : ModelConverterBase<Obymobi.Logic.Model.v21.Terminal, Obymobi.Logic.Model.Terminal>
	{
        public TerminalConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.TerminalConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.Terminal ConvertModelToLegacyModel(Obymobi.Logic.Model.Terminal source)
        {
            Obymobi.Logic.Model.v21.Terminal target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.Terminal();
                target.TerminalId = source.TerminalId;
                target.Name = source.Name;
                target.DeliverypointgroupId = source.DeliverypointgroupId;
                target.CompanyId = source.CompanyId;
                target.CompanyName = source.CompanyName;
                target.ReceiptTypes = source.ReceiptTypes;
                target.LanguageCode = source.LanguageCode;
                target.RequestInterval = source.RequestInterval;
                target.DiagnoseInterval = source.DiagnoseInterval;
                target.PrinterName = source.PrinterName;
                target.CultureString = source.CultureString;
                target.LastRequest = source.LastRequest;
                target.LastRequestNotifiedBySMS = source.LastRequestNotifiedBySMS;
                target.SmsNotificationRequired = source.SmsNotificationRequired;
                target.LastStatus = source.LastStatus;
                target.LastStatusText = source.LastStatusText;
                target.LastStatusMessage = source.LastStatusMessage;
                target.CurrentState = source.CurrentState;
                target.TeamviewerId = source.TeamviewerId;
                target.MaxDaysLogHistory = source.MaxDaysLogHistory;
                target.HandlingMethod = source.HandlingMethod;
                target.SuspendService = source.SuspendService;
                target.POSConnectorType = source.POSConnectorType;
                target.PMSConnectorType = source.PMSConnectorType;
                target.SynchronizeWithPOSOnStart = source.SynchronizeWithPOSOnStart;
                target.MaxStatusReports = source.MaxStatusReports;
                target.MaxProcessTime = source.MaxProcessTime;
                target.Active = source.Active;
                target.UseMonitoring = source.UseMonitoring;
                target.NotificationForNewOrder = source.NotificationForNewOrder;
                target.NotificationForOverdueOrder = source.NotificationForOverdueOrder;
                target.AnnouncementDuration = source.AnnouncementDuration;
                target.PosValue1 = source.PosValue1;
                target.PosValue2 = source.PosValue2;
                target.PosValue3 = source.PosValue3;
                target.PosValue4 = source.PosValue4;
                target.PosValue5 = source.PosValue5;
                target.PosValue6 = source.PosValue6;
                target.PosValue7 = source.PosValue7;
                target.PosValue8 = source.PosValue8;
                target.PosValue9 = source.PosValue9;
                target.PosValue10 = source.PosValue10;
                target.PosValue11 = source.PosValue11;
                target.PosValue12 = source.PosValue12;
                target.PosValue13 = source.PosValue13;
                target.PosValue14 = source.PosValue14;
                target.PosValue15 = source.PosValue15;
                target.PosValue16 = source.PosValue16;
                target.PosValue17 = source.PosValue17;
                target.PosValue18 = source.PosValue18;
                target.PosValue19 = source.PosValue19;
                target.PosValue20 = source.PosValue20;
                target.SupportpoolId = source.SupportpoolId;
                target.ResetTimeout = source.ResetTimeout;
                target.Browser1EntertainmentId = source.Browser1EntertainmentId;
                target.Browser2EntertainmentId = source.Browser2EntertainmentId;
                target.CmsPageEntertainmentId = source.CmsPageEntertainmentId;
                target.ForwardToTerminalId = source.ForwardToTerminalId;
                target.SurveyResultNotifications = source.SurveyResultNotifications;
                target.PrintingEnabled = source.PrintingEnabled;
                target.LastCommunicationMethod = source.LastCommunicationMethod;
                target.LastSupportToolsVersion = source.LastSupportToolsVersion;
                target.LastAgentVersion = source.LastAgentVersion;
                target.LastSupportToolsRequest = source.LastSupportToolsRequest;
                target.UIModeId = source.UIModeId;
                target.LinkedToDevice = source.LinkedToDevice;
                target.HasMasterTab = source.HasMasterTab;
                target.Pincode = source.Pincode;
                target.PincodeSU = source.PincodeSU;
                target.PincodeGM = source.PincodeGM;
                target.UseHardKeyboard = source.UseHardKeyboard;
                target.MaxVolume = source.MaxVolume;

                if (source.AnnouncementActions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v21.Converters.AnnouncementActionConverter announcementActionsConverter = new Obymobi.Logic.Model.v21.Converters.AnnouncementActionConverter();
                    target.AnnouncementActions = (AnnouncementAction[])announcementActionsConverter.ConvertArrayToLegacyArray(source.AnnouncementActions);
                }

                if (source.Messagegroups != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v21.Converters.MessagegroupConverter messagegroupsConverter = new Obymobi.Logic.Model.v21.Converters.MessagegroupConverter();
                    target.Messagegroups = (Messagegroup[])messagegroupsConverter.ConvertArrayToLegacyArray(source.Messagegroups);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Terminal ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.Terminal source)
        {
            Obymobi.Logic.Model.Terminal target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Terminal();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.TerminalConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
