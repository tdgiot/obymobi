namespace Obymobi.Logic.Model.v21.Converters
{
	public class SimpleWebserviceResultConverter : ModelConverterBase<Obymobi.Logic.Model.v21.SimpleWebserviceResult, Obymobi.Logic.Model.SimpleWebserviceResult>
	{
        public SimpleWebserviceResultConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.SimpleWebserviceResultConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.SimpleWebserviceResult ConvertModelToLegacyModel(Obymobi.Logic.Model.SimpleWebserviceResult source)
        {
            Obymobi.Logic.Model.v21.SimpleWebserviceResult target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.SimpleWebserviceResult();
                target.Succes = source.Succes;
                target.Ticks = source.Ticks;
            }

            return target;
        }

        public override Obymobi.Logic.Model.SimpleWebserviceResult ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.SimpleWebserviceResult source)
        {
            Obymobi.Logic.Model.SimpleWebserviceResult target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SimpleWebserviceResult();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.SimpleWebserviceResultConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
