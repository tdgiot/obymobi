namespace Obymobi.Logic.Model.v21.Converters
{
	public class SurveyResultConverter : ModelConverterBase<Obymobi.Logic.Model.v21.SurveyResult, Obymobi.Logic.Model.SurveyResult>
	{
        public SurveyResultConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.SurveyResultConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.SurveyResult ConvertModelToLegacyModel(Obymobi.Logic.Model.SurveyResult source)
        {
            Obymobi.Logic.Model.v21.SurveyResult target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.SurveyResult();
                target.SurveyResultId = source.SurveyResultId;
                target.SurveyId = source.SurveyId;
                target.CustomerId = source.CustomerId;
                target.ClientId = source.ClientId;

                if (source.SurveyResultAnswers != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v21.Converters.SurveyResultAnswerConverter surveyResultAnswersConverter = new Obymobi.Logic.Model.v21.Converters.SurveyResultAnswerConverter();
                    target.SurveyResultAnswers = (SurveyResultAnswer[])surveyResultAnswersConverter.ConvertArrayToLegacyArray(source.SurveyResultAnswers);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.SurveyResult ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.SurveyResult source)
        {
            Obymobi.Logic.Model.SurveyResult target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SurveyResult();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.SurveyResultConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
