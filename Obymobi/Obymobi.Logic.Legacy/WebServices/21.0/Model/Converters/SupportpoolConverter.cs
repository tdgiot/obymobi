namespace Obymobi.Logic.Model.v21.Converters
{
	public class SupportpoolConverter : ModelConverterBase<Obymobi.Logic.Model.v21.Supportpool, Obymobi.Logic.Model.Supportpool>
	{
        public SupportpoolConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.SupportpoolConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.Supportpool ConvertModelToLegacyModel(Obymobi.Logic.Model.Supportpool source)
        {
            Obymobi.Logic.Model.v21.Supportpool target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.Supportpool();
                target.SupportpoolId = source.SupportpoolId;
                target.Name = source.Name;
                target.Phonenumber = source.Phonenumber;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Supportpool ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.Supportpool source)
        {
            Obymobi.Logic.Model.Supportpool target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Supportpool();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.SupportpoolConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
