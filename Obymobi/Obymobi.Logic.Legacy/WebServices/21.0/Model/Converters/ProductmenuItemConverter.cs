using System.Linq;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v21.Converters
{
	public class ProductmenuItemConverter : ModelConverterBase<Obymobi.Logic.Model.v21.ProductmenuItem, Obymobi.Logic.Model.ProductmenuItem>
	{
        public ProductmenuItemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.ProductmenuItemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.ProductmenuItem ConvertModelToLegacyModel(Obymobi.Logic.Model.ProductmenuItem source)
        {
            Obymobi.Logic.Model.v21.ProductmenuItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.ProductmenuItem();
                target.Id = source.Id;
                target.Name = source.Name;
                target.PriceIn = source.PriceIn;
                target.ParentProductmenuItemId = source.ParentProductmenuItemId;
                target.ItemType = source.ItemType;
                target.SortOrder = source.SortOrder;
                target.IsFavorite = source.IsFavorite;
                target.OrderedQuantity = source.OrderedQuantity;
                target.TextColor = source.TextColor;
                target.BackgroundColor = source.BackgroundColor;
                target.Description = source.Description;
                target.DisplayOnHomepage = source.DisplayOnHomepage;
                target.Rateable = source.Rateable;
                target.Type = source.Type;
                target.SubType = source.SubType;
                target.AllowFreeText = source.AllowFreeText;
                target.AnnouncementAction = source.AnnouncementAction;
                target.HidePrices = source.HidePrices;
                target.Color = source.Color;
                target.ButtonText = source.ButtonText;
                target.WebTypeTabletUrl = source.WebTypeTabletUrl;
                target.WebTypeSmartphoneUrl = source.WebTypeSmartphoneUrl;
                target.ScheduleId = source.ScheduleId;
                target.ViewLayoutType = source.ViewLayoutType;
                target.DeliveryLocationType = source.DeliveryLocationType;
                target.Visible = source.VisibilityType != (int)VisibilityType.Never;

                if (source.Alterations != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v21.Converters.AlterationConverter alterationsConverter = new Obymobi.Logic.Model.v21.Converters.AlterationConverter();
                    target.Alterations = (Alteration[])alterationsConverter.ConvertArrayToLegacyArray(source.Alterations);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v21.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v21.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.ProductSuggestions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v21.Converters.ProductSuggestionConverter productSuggestionsConverter = new Obymobi.Logic.Model.v21.Converters.ProductSuggestionConverter();
                    target.ProductSuggestions = (ProductSuggestion[])productSuggestionsConverter.ConvertArrayToLegacyArray(source.ProductSuggestions);
                }

                if (source.AdvertisementTags != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v21.Converters.AdvertisementTagConverter advertisementTagsConverter = new Obymobi.Logic.Model.v21.Converters.AdvertisementTagConverter();
                    target.AdvertisementTags = (AdvertisementTag[])advertisementTagsConverter.ConvertArrayToLegacyArray(source.AdvertisementTags);
                }

                if (source.Ratings != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v21.Converters.RatingConverter ratingsConverter = new Obymobi.Logic.Model.v21.Converters.RatingConverter();
                    target.Ratings = (Rating[])ratingsConverter.ConvertArrayToLegacyArray(source.Ratings);
                }

                if (source.Attachments != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v21.Converters.AttachmentConverter attachmentsConverter = new Obymobi.Logic.Model.v21.Converters.AttachmentConverter();
                    target.Attachments = (Attachment[])attachmentsConverter.ConvertArrayToLegacyArray(source.Attachments);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<ProductmenuItemLanguage> languages = new System.Collections.Generic.List<ProductmenuItemLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        ProductmenuItemLanguage model = new ProductmenuItemLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductName);
                        model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductDescription);
                        model.ButtonText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductButtonText);                 
                        model.OrderProcessedTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductOrderProcessedTitle);
                        model.OrderProcessedMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductOrderProcessedText);                        

                        languages.Add(model);
                    }

                    target.ProductmenuItemLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.ProductmenuItem ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.ProductmenuItem source)
        {
            Obymobi.Logic.Model.ProductmenuItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.ProductmenuItem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.ProductmenuItemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
