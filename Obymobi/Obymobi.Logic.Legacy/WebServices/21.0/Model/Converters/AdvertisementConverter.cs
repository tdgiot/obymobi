using System.Linq;

namespace Obymobi.Logic.Model.v21.Converters
{
	public class AdvertisementConverter : ModelConverterBase<Obymobi.Logic.Model.v21.Advertisement, Obymobi.Logic.Model.Advertisement>
	{
        public AdvertisementConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.AdvertisementConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.Advertisement ConvertModelToLegacyModel(Obymobi.Logic.Model.Advertisement source)
        {
            Obymobi.Logic.Model.v21.Advertisement target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.Advertisement();
                target.AdvertisementId = source.AdvertisementId;
                target.CompanyId = source.CompanyId;
                target.Name = source.Name;
                target.Description = source.Description;
                target.ProductId = source.ProductId;
                target.EntertainmentId = source.EntertainmentId;
                target.ActionCategoryId = source.ActionCategoryId;
                target.ActionEntertainmentId = source.ActionEntertainmentId;
                target.ActionEntertainmentCategoryId = source.ActionEntertainmentCategoryId;
                target.ActionUrl = source.ActionUrl;
                target.ActionSiteId = source.ActionSiteId;
                target.ActionPageId = source.ActionPageId;

                if (source.AdvertisementTags != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v21.Converters.AdvertisementTagConverter advertisementTagsConverter = new Obymobi.Logic.Model.v21.Converters.AdvertisementTagConverter();
                    target.AdvertisementTags = (AdvertisementTag[])advertisementTagsConverter.ConvertArrayToLegacyArray(source.AdvertisementTags);
                }                

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v21.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v21.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<AdvertisementLanguage> languages = new System.Collections.Generic.List<AdvertisementLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        AdvertisementLanguage model = new AdvertisementLanguage();
                        model.AdvertisementLanguageId = -1;
                        model.AdvertisementId = source.AdvertisementId;
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = Dionysos.DictionaryUtil.GetValueOrDefault(customTexts, Obymobi.Enums.CustomTextType.AdvertisementName);
                        model.Description = Dionysos.DictionaryUtil.GetValueOrDefault(customTexts, Obymobi.Enums.CustomTextType.AdvertisementDescription);

                        languages.Add(model);
                    }

                    target.AdvertisementLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Advertisement ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.Advertisement source)
        {
            Obymobi.Logic.Model.Advertisement target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Advertisement();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.AdvertisementConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
