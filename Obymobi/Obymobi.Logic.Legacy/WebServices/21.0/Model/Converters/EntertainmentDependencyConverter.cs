namespace Obymobi.Logic.Model.v21.Converters
{
	public class EntertainmentDependencyConverter : ModelConverterBase<Obymobi.Logic.Model.v21.EntertainmentDependency, Obymobi.Logic.Model.EntertainmentDependency>
	{
        public EntertainmentDependencyConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.EntertainmentDependencyConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.EntertainmentDependency ConvertModelToLegacyModel(Obymobi.Logic.Model.EntertainmentDependency source)
        {
            Obymobi.Logic.Model.v21.EntertainmentDependency target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.EntertainmentDependency();
                target.EntertainmentId = source.EntertainmentId;
                target.Name = source.Name;
                target.PackageName = source.PackageName;
                target.ClassName = source.ClassName;
                target.Filename = source.Filename;
                target.FileVersion = source.FileVersion;
            }

            return target;
        }

        public override Obymobi.Logic.Model.EntertainmentDependency ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.EntertainmentDependency source)
        {
            Obymobi.Logic.Model.EntertainmentDependency target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.EntertainmentDependency();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.EntertainmentDependencyConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
