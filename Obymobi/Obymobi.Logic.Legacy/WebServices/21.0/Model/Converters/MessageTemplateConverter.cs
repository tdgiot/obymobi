namespace Obymobi.Logic.Model.v21.Converters
{
	public class MessageTemplateConverter : ModelConverterBase<Obymobi.Logic.Model.v21.MessageTemplate, Obymobi.Logic.Model.MessageTemplate>
	{
        public MessageTemplateConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.MessageTemplateConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.MessageTemplate ConvertModelToLegacyModel(Obymobi.Logic.Model.MessageTemplate source)
        {
            Obymobi.Logic.Model.v21.MessageTemplate target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.MessageTemplate();
                target.MessageTemplateId = source.MessageTemplateId;
                target.Name = source.Name;
                target.Title = source.Title;
                target.Message = source.Message;
                target.CompanyId = source.CompanyId;
                target.Duration = source.Duration;
                target.MediaId = source.MediaId;
                target.CategoryId = source.CategoryId;
                target.EntertainmentId = source.EntertainmentId;
                target.ProductId = source.ProductId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.MessageTemplate ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.MessageTemplate source)
        {
            Obymobi.Logic.Model.MessageTemplate target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.MessageTemplate();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.MessageTemplateConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
