namespace Obymobi.Logic.Model.v21.Converters
{
	public class FormFieldValueLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v21.FormFieldValueLanguage, Obymobi.Logic.Model.FormFieldValueLanguage>
	{
        public FormFieldValueLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v22.Converters.FormFieldValueLanguageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v21.FormFieldValueLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.FormFieldValueLanguage source)
        {
            Obymobi.Logic.Model.v21.FormFieldValueLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v21.FormFieldValueLanguage();
                target.FormFieldValueLanguageId = source.FormFieldValueLanguageId;
                target.FormFieldValueId = source.FormFieldValueId;
                target.LanguageCode = source.LanguageCode;
                target.Value = source.Value;
            }

            return target;
        }

        public override Obymobi.Logic.Model.FormFieldValueLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v21.FormFieldValueLanguage source)
        {
            Obymobi.Logic.Model.FormFieldValueLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.FormFieldValueLanguage();

                // Copy default values from new version
                new Obymobi.Logic.Model.v22.Converters.FormFieldValueLanguageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
