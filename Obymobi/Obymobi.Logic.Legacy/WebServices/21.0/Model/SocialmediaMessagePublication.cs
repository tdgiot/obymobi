using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Model class which represents a social media message item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "SocialmediaMessagePublication")]
    public class SocialmediaMessagePublication : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.SocialmediaMessage type
        /// </summary>
        public SocialmediaMessagePublication()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the social media message item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int SocialmediaMessagePublicationId
        { get; set; }

        /// <summary>
        /// Gets or sets the id social media message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SocialmediaMessageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company social media data
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int CompanySocialmediaId
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public SocialmediaMessagePublication Clone()
        {
            return this.Clone<SocialmediaMessagePublication>();
        }

        #endregion
    }
}
