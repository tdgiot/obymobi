using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Model class which represents a room control area language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "RoomControlAreaLanguage"), IncludeInCodeGeneratorForAndroid]
    public class RoomControlAreaLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.RoomControlAreaLanguage type
        /// </summary>
        public RoomControlAreaLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the room control area language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlAreaLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LanguageCode
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public RoomControlAreaLanguage Clone()
        {
            return this.Clone<RoomControlAreaLanguage>();
        }

        #endregion
    }
}
