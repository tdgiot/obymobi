using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Model class which represents an alteration
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "AlterationProduct"), IncludeInCodeGeneratorForAndroid]
    public class AlterationProduct : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.AlterationProduct type
        /// </summary>
        public AlterationProduct()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the alterationProduct
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the visible flag for the alteration product
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool Visible
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public AlterationProduct Clone()
        {
            return this.Clone<AlterationProduct>();
        }

        #endregion
    }
}
