using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Model class which represents a orderitem
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Orderitem"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class Orderitem : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Orderitem type
        /// </summary>
        public Orderitem()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Orderitem type using the specified parameters
        /// </summary>
        public Orderitem(int OrderitemId, int OrderId, int ProductId, string ProductName, int Quantity, int ProductPriceIn, int VatPercentage, string Notes)
        {
            this.OrderitemId = OrderitemId;
            this.OrderId = OrderId;
            this.ProductId = ProductId;
            this.ProductName = ProductName;
            this.Quantity = Quantity;
            this.ProductPriceIn = ProductPriceIn;
            this.VatPercentage = VatPercentage;
            this.Notes = Notes;
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the orderitem
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int OrderitemId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the order
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int OrderId
        { get; set; }

        /// <summary>
        /// Gets or sets a unique identifier
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Guid
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the category
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public int CategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string ProductName
        { get; set; }

        /// <summary>
        /// Gets or sets the quantity of the orderitem
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int Quantity
        { get; set; }

        /// <summary>
        /// Gets or sets the price including vat of the orderitem
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public decimal ProductPriceIn
        { get; set; }

        /// <summary>
        /// Gets or sets the vat percentage of the orderitem
        /// </summary>
        [XmlElement]
        public double VatPercentage
        { get; set; }

        /// <summary>
        /// Gets or sets the quantity of the orderitem
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Notes
        { get; set; }

        /// <summary>
        /// Gets or sets the benchmarkinformation
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public string BenchmarkInformation
        { get; set; }

        /// <summary>
        /// Gets or sets the color this orderitem will be on the console
        /// </summary>		
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Color
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int OrderSource { get; set; }

        /// <summary>
        /// Gets or sets items of the order
        /// </summary>
        [XmlArray("Alterationitems")]
        [XmlArrayItem("Alterationitem")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public Alterationitem[] Alterationitems
        { get; set; }

        #endregion

        #region Custom Properties

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }

        /// <summary>
        /// Gets or sets the posorderitem.
        /// </summary>
        /// <value>
        /// The posorderitem.
        /// </value>
        [XmlElement]
        public Posorderitem Posorderitem
        { get; set; }

        #endregion

        #region Calculated Properties

        /// <summary>
        /// This is the calculated price for the Orderitem per piece (this.ProductPriceIn + alterationitemPriceInTotal + discountPriceInTotal)
        /// </summary>
        public decimal PriceInPerPiece
        {
            get
            {
                decimal alterationitemPriceInTotal = this.Alterationitems != null ? this.Alterationitems.Sum(a => a.AlterationoptionPriceIn) : 0;
                return this.ProductPriceIn + alterationitemPriceInTotal;
    }
        }

        /// <summary>
        /// This is the calculated price for the Orderitem (this.Quantity * (this.ProductPriceIn + alterationitemPriceInTotal + discountPriceInTotal))
        /// </summary>
        public decimal PriceIn
        {
            get
            {
                return this.Quantity * this.PriceInPerPiece;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Orderitem Clone()
        {
            return this.Clone<Orderitem>();
        }

        #endregion
    }
}
