using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v21
{
	/// <summary>
	/// Model class which represents a company
	/// </summary>
	[Serializable, XmlRootAttribute(ElementName = "ReiswijzerRouteSummary"), IncludeInCodeGeneratorForFlex]
	public class ReiswijzerRouteSummary
	{
		#region Constructors

		/// <summary>
		/// Constructs an instance of the Obymobi.Logic.Model.Company type
		/// </summary>
		public ReiswijzerRouteSummary()
		{
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the id of the company
		/// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
		public int ReiswijzerRouteId
		{ get; set; }

        /// <summary>
        /// Gets or sets the arrival time.
        /// </summary>
        /// <value>
        /// The arrival time.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ArrivalTime
		{ get; set; }

        /// <summary>
        /// Gets or sets the departure time.
        /// </summary>
        /// <value>
        /// The departure time.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string DepartureTime
		{ get; set; }

        /// <summary>
        /// Gets or sets the interchanges.
        /// </summary>
        /// <value>
        /// The interchanges.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public int Interchanges
		{ get; set; }

        /// <summary>
        /// Gets or sets the travel time.
        /// </summary>
        /// <value>
        /// The travel time.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public int TravelTime
		{ get; set; }

        /// <summary>
        /// Gets or sets the modes.
        /// </summary>
        /// <value>
        /// The modes.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string Modes
		{ get; set; }		

		#endregion
	}
}
