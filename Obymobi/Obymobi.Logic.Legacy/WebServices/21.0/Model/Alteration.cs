using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Model class which represents an alteration
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Alteration"),
        IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForJavascript, IncludeInCodeGeneratorForXamarin]
    public class Alteration : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Alteration type
        /// </summary>
        public Alteration()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the alteration
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the type of this alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the default alteration option
        /// </summary>
        [XmlArray("DefaultAlterationoptionIds")]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int[] DefaultAlterationoptionIds
        { get; set; }

        /// <summary>
        /// Gets or sets the minimal amount of options needed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int MinOptions
        { get; set; }

        /// <summary>
        /// Gets or sets the maximal amount of options possible
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int MaxOptions
        { get; set; }

        /// <summary>
        /// Gets or sets the posalteration.
        /// </summary>
        /// <value>
        /// The posalteration.
        /// </value>
        [XmlElement]
        public Posalteration Posalteration
        { get; set; }

        /// <summary>
        /// Gets or sets the start time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string StartTime
        { get; set; }

        /// <summary>
        /// Gets or sets the end time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string EndTime
        { get; set; }

        /// <summary>
        /// Gets or sets the minimum amount of leading minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int MinLeadMinutes
        { get; set; }

        /// <summary>
        /// Gets or sets the maximum amount of leading minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int MaxLeadHours
        { get; set; }

        /// <summary>
        /// Gets or sets the time intervals in minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int IntervalMinutes
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not to show a date picker
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public bool ShowDatePicker
        { get; set; }

        /// <summary>
        /// Gets or sets the free text value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Value
        { get; set; }

        /// <summary>
        /// Gets or sets wether or not the alteration should be chosen from the order view
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public bool OrderLevelEnabled
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SortOrder 
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ParentAlterationId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int LayoutType
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Description
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Visible
        { get; set; }

        /// <summary>
        /// Gets or sets the options of the alteration
        /// </summary>
        [XmlArray("Alterationoptions")]
        [XmlArrayItem("Alterationoption")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public Alterationoption[] Alterationoptions
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [XmlArray("AlterationLanguages")]
        [XmlArrayItem("AlterationLanguage")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public AlterationLanguage[] AlterationLanguages
        { get; set; }

        /// <summary>
        /// Gets or sets the alterations of the product
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public Media[] Media
        { get; set; }

        /// <summary>
        /// Gets or sets the alteration products
        /// </summary>
        [XmlArray("AlterationProducts")]
        [XmlArrayItem("AlterationProduct")]
        [IncludeInCodeGeneratorForAndroid]
        public AlterationProduct[] AlterationProducts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Alteration Clone()
        {
            return this.Clone<Alteration>();
        }

        #endregion
    }
}
