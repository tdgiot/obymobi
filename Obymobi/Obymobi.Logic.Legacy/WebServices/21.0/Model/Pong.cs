using Newtonsoft.Json;

namespace Obymobi.Logic.Model.v21
{

    /*
	public static final int STATE_INITIALIZING = -1;
	public static final int STATE_CONNECT = 0;
	public static final int STATE_LOGIN = 1;
	public static final int STATE_GETCLIENTS = 2;
	public static final int STATE_GETDELIVERYPOINTS = 3;
	public static final int STATE_GETCOMPANY = 4;
	public static final int STATE_GETMENU = 5;
	public static final int STATE_GETSURVEYS = 6;
	public static final int STATE_GETANNOUNCEMENTS = 7;
	public static final int STATE_GETENTERTAINMENT = 8;
	public static final int STATE_GETFORMS = 9;
	public static final int STATE_ORDER = 10;
	public static final int STATE_GETMEDIA = 11;
	public static final int STATE_IDLE = 100;
     */

    /// <summary>
    /// Ping/Pong
    /// </summary>
	[JsonObject(MemberSerialization.OptIn)]
	public class Pong
    {
        #region Enum

        /// <summary>
        /// State enums
        /// </summary>
        public enum State
        {
            /// <summary>
            /// Pong state is initializing
            /// </summary>
            INITIALIZING = -1,
            /// <summary>
            /// Pong state is connected
            /// </summary>
            CONNECTED = 0,
            /// <summary>
            /// Pong state is login
            /// </summary>
            LOGIN = 1,
            /// <summary>
            /// Pong state is getting clients
            /// </summary>
            GETCLIENTS = 2,
            /// <summary>
            /// Pong state is getting deliverypoints
            /// </summary>
            GETDELIVERYPOINTS = 3,
            /// <summary>
            /// Pong state is getting company
            /// </summary>
            GETCOMPANY = 4,
            /// <summary>
            /// Pong state is getting menu
            /// </summary>
            GETMENU = 5,
            /// <summary>
            /// Pong state is getting surveys
            /// </summary>
            GETSURVEYS = 6,
            /// <summary>
            /// Pong state is getting announcements
            /// </summary>
            GETANNOUNCEMENTS = 7,
            /// <summary>
            /// Pong state is getting entertainment
            /// </summary>
            GETENTERTAINMENT = 8,
            /// <summary>
            /// Pong state is getting forms
            /// </summary>
            GETFORMS = 9,
            /// <summary>
            /// Pong state is order
            /// </summary>
            ORDER = 10,
            /// <summary>
            /// Pong state is getting media
            /// </summary>
            GETMEDIA = 11,
            /// <summary>
            /// Pong state is idle
            /// </summary>
            IDLE = 100
        }

        #endregion 

        #region Send to client Properties

        /// <summary>
        /// Boolean if internet is available
        /// </summary>
		[JsonProperty]
		public bool InternetAlive { get; set; }

        /// <summary>
        /// Boolean if webservices are online
        /// </summary>
		[JsonProperty]
		public bool WebserviceAlive { get; set; }

        /// <summary>
        /// List of online deliverypoints
        /// </summary>
		[JsonProperty]
		public Deliverypoint[] OnlineDeliverypoints { get; set; }

        /// <summary>
        /// Last update Date/Time
        /// </summary>
		[JsonProperty]
		public string LastUpdate { get; set; }

        /// <summary>
        /// If client should display low battery dialog
        /// </summary>
        [JsonProperty]
        public bool ShowBatteryLowDialog { get; set; }

		#endregion

		#region Receive from client Properties

        /// <summary>
        /// Operation mode the client is in
        /// </summary>
		[JsonProperty]
		public int ClientOperationMode { get; set; }

        /// <summary>
        /// Percentage of battery left
        /// </summary>
        [JsonProperty]
        public int BatteryPercentage { get; set; }

        /// <summary>
        /// True is the device is charging
        /// </summary>
        [JsonProperty]
        public bool IsCharging { get; set; }

        /// <summary>
        /// State of the client
        /// </summary>
        [JsonProperty]
        public int LoadingState { get; set; }

		#endregion
	}
}
