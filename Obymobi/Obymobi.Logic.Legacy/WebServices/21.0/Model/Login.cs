using Newtonsoft.Json;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Login model
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class Login
    {
        /// <summary>
        /// Constructs an instance of the CraveOnsiteServer.Logic.Model.Login type
        /// </summary>
        public Login()
        {
        }

        /// <summary>
        /// Constructs an instance of the CraveOnsiteServer.Logic.Model.Login type using specified parameters
        /// </summary>
        public Login(string CompanyOwner, string CompanyPassword, int ClientVersion, int OSVersion, bool IsReconnect)
        {
            this.CompanyOwner = CompanyOwner;
            this.CompanyPassword = CompanyPassword;
            this.ClientVersion = ClientVersion;
            this.IsReconnect = IsReconnect;
            this.OSVersion = OSVersion;
        }

        #region Properties

        /// <summary>
        /// Gets or sets the company owner.
        /// </summary>
        /// <value>
        /// The company owner.
        /// </value>
        [JsonProperty]
        public string CompanyOwner { get; set; }

        /// <summary>
        /// Gets or sets the company password.
        /// </summary>
        /// <value>
        /// The company password.
        /// </value>
        [JsonProperty]
        public string CompanyPassword { get; set; }

        /// <summary>
        /// Gets or sets the client version.
        /// </summary>
        /// <value>
        /// The client version.
        /// </value>
        [JsonProperty]
        public int ClientVersion { get; set; }

        /// <summary>
        /// Gets or sets the OS version.
        /// </summary>
        /// <value>
        /// The OS version.
        /// </value>
        [JsonProperty]
        public int OSVersion { get; set; }

        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        /// <value>
        /// The client id.
        /// </value>
        [JsonProperty]
        public int ClientId { get; set; }

        /// <summary>
        /// Gets or sets the deliverypoint id.
        /// </summary>
        /// <value>
        /// The deliverypoint id.
        /// </value>
        [JsonProperty]
        public int DeliverypointId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is reconnect.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is reconnect; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty]
        public bool IsReconnect { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [config connect].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [config connect]; otherwise, <c>false</c>.
        /// </value>
        [JsonProperty]
        public bool ConfigConnect { get; set; }

        /// <summary>
        /// Gets or sets the mac address.
        /// </summary>
        /// <value>
        /// The mac address.
        /// </value>
        [JsonProperty]
        public string MacAddress { get; set; }


        /// <summary>
        /// Gets or sets the mac address.
        /// </summary>
        /// <value>
        /// The mac address.
        /// </value>
        [JsonProperty]
        public int DeviceType { get; set; }
        #endregion
    }
}
