using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Model class which represents a client language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "ClientLanguage"), IncludeInCodeGeneratorForAndroid]
    public class ClientLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.ClientLanguage type
        /// </summary>
        public ClientLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the out of charge message title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OutOfChargeTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the out of charge message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OutOfChargeMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the order processed message title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderProcessedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the order processed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderProcessedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the order failed message title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderFailedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the order failed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderFailedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the order saving title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderSavingTitle
        { get; set; }
        /// <summary>
        /// Gets or sets the order saving message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderSavingMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the order completed title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderCompletedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the order completed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderCompletedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the order completed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool OrderCompletedEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the service saving title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ServiceSavingTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the service saving message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ServiceSavingMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the service processed title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ServiceProcessedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the service processed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ServiceProcessedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the rating saving title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RatingSavingTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the rating saving message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RatingSavingMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the rating processed title of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RatingProcessedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the rating processed message of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RatingProcessedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets 1st URL for the hotel mode tabs
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string HotelUrl1
        { get; set; }

        /// <summary>
        /// Gets or sets the caption for the 1st URL for the hotel mode tabs 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string HotelUrl1Caption
        { get; set; }

        /// <summary>
        /// Gets or sets the zoom level for the 1st URL for the hotel mode tabs 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int HotelUrl1Zoom
        { get; set; }

        /// <summary>
        /// Gets or sets 2nd URL for the hotel mode tabs
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string HotelUrl2
        { get; set; }

        /// <summary>
        /// Gets or sets the caption for the 2nd URL for the hotel mode tabs 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string HotelUrl2Caption
        { get; set; }

        /// <summary>
        /// Gets or sets the zoom level for the 2nd URL for the hotel mode tabs 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int HotelUrl2Zoom
        { get; set; }

        /// <summary>
        /// Gets or sets 3rd URL for the hotel mode tabs
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string HotelUrl3
        { get; set; }

        /// <summary>
        /// Gets or sets the caption for the 3rd URL for the hotel mode tabs 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string HotelUrl3Caption
        { get; set; }

        /// <summary>
        /// Gets or sets the zoom level for the 3rd URL for the hotel mode tabs 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int HotelUrl3Zoom
        { get; set; }

        /// <summary>
        /// Gets or sets the deliverypoint caption
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string DeliverypointCaption
        { get; set; }

        /// <summary>
        /// Gets or sets PMS device locked dialog title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsDeviceLockedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets PMS device locked dialog text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsDeviceLockedText
        { get; set; }

        /// <summary>
        /// Gets or sets PMS checkin failed dialog title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsCheckinFailedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets PMS checkin failed dialog text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsCheckinFailedText
        { get; set; }

        /// <summary>
        /// Gets or sets PMS restart dialog title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsRestartTitle
        { get; set; }

        /// <summary>
        /// Gets or sets PMS restart dialog text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsRestartText
        { get; set; }

        /// <summary>
        /// Gets or sets PMS welcome dialog title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsWelcomeTitle
        { get; set; }

        /// <summary>
        /// Gets or sets PMS welcome dialog text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsWelcomeText
        { get; set; }

        /// <summary>
        /// Gets or sets PMS checkout approved text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsCheckoutApproveText
        { get; set; }

        /// <summary>
        /// Gets or sets PMS checkout complete dialog title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsCheckoutCompleteTitle
        { get; set; }

        /// <summary>
        /// Gets or sets PMS checkout complete dialog text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PmsCheckoutCompleteText
        { get; set; }

        /// <summary>
        /// Gets or sets the roomservice charge text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string RoomserviceChargeText { get; set; }

        /// <summary>
        /// Gets or sets the suggestions caption
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string SuggestionsCaption { get; set; }

        /// <summary>
        /// Gets or sets the printing confirmation title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PrintingConfirmationTitle { get; set; }

        /// <summary>
        /// Gets or sets the printing confirmation text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PrintingConfirmationText { get; set; }

        /// <summary>
        /// Gets or sets the printing succeeded title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PrintingSucceededTitle { get; set; }

        /// <summary>
        /// Gets or sets the printing succeeded text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PrintingSucceededText { get; set; }

        /// <summary>
        /// Gets or sets the charger removed title 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ChargerRemovedTitle { get; set; }

        /// <summary>
        /// Gets or sets the charger removed text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ChargerRemovedText { get; set; }

        /// <summary>
        /// Gets or sets the charger removed reminder title
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ChargerRemovedReminderTitle { get; set; }

        /// <summary>
        /// Gets or sets the charger removed reminder text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ChargerRemovedReminderText { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string TurnOffPrivacyTitle { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string TurnOffPrivacyText { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public ClientLanguage Clone()
        {
            return this.Clone<ClientLanguage>();
        }

        #endregion
    }
}
