using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Model class which represents the business hours of a company
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Businesshours")]
    public class Businesshours : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Businesshours type
        /// </summary>
        public Businesshours()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of this business hours entry
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int BusinesshoursId
        { get; set; }

        /// <summary>
        /// Gets or sets the opening day (Sunday 0 - Saturday 6)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public int OpeningDay
        { get; set; }

        /// <summary>
        /// Gets or sets the opening time (HHmm)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public int OpeningTime
        { get; set; }

        /// <summary>
        /// Gets or sets the closing day (Sunday 0 - Saturday 6)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public int ClosingDay
        { get; set; }

        /// <summary>
        /// Gets or sets the closing time (HHmm)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public int ClosingTime
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Businesshours Clone()
        {
            return this.Clone<Businesshours>();
        }

        #endregion
    }
}
