using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Model class which represents a deliverypoint group
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Deliverypointgroup"), IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class Deliverypointgroup : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Deliverypointgroup type
        /// </summary>
        public Deliverypointgroup()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Deliverypointgroup type using the specified parameters
        /// </summary>
        public Deliverypointgroup(int DeliverypointgroupId, int CompanyId, string Name)
        {
            this.DeliverypointgroupId = DeliverypointgroupId;
            this.CompanyId = CompanyId;
            this.Name = Name;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the deliverypoint group
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public int DeliverypointgroupId
        { get; set; }

        /// <summary>
        /// Gets or sets the company id of the deliverypoint group
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the deliverypoint group
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets if the order history dialog should be displayed on the emenu
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool OrderHistoryDialogEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets if a PmsIntegration is used for this group
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PmsIntegration
        { get; set; }

        /// <summary>
        /// Gets or sets if the user is allowed to view the bill (can be overriden by PMS) per room
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PmsAllowShowBill
        { get; set; }        

        /// <summary>
        /// Gets or sets if the user is allowed to express checkout (can be overriden by PMS) per room
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PmsAllowExpressCheckout
        { get; set; }

        /// <summary>
        /// Gets or sets if the client should be locked when the room is not checked in.
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PmsLockClientWhenNotCheckedIn
        { get; set; }

        /// <summary>
        /// Gets or sets the deliverypoint caption
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string DeliverypointCaption
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int UseHardKeyboard { get; set; }

        /// <summary>
        /// Gets or sets if the user is allowed to view the guest name
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PmsAllowShowGuestName
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Deliverypointgroup Clone()
        {
            return this.Clone<Deliverypointgroup>();
        }

        #endregion
    }
}
