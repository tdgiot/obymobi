using System;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// WriteToLog model
    /// </summary>
    [Serializable]
    public class WriteToLog : ModelBase
    {
        public WriteToLog()
        {
            this.OrderGuid = string.Empty;
            this.Status = string.Empty;
            this.Log = string.Empty;
            this.Message = string.Empty;            
        }

        /// <summary>
        /// Gets or sets the type of the terminal log.
        /// </summary>
        /// <value>
        /// The type of the terminal log.
        /// </value>
        public int TerminalLogType { get; set; }
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; set; }
        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public string Status { get; set; }
        /// <summary>
        /// Gets or sets the log.
        /// </summary>
        /// <value>
        /// The log.
        /// </value>
        public string Log { get; set; }
        /// <summary>
        /// Gets or sets the order id.
        /// </summary>
        /// <value>
        /// The order id.
        /// </value>
        public int? orderId { get; set; }
        /// <summary>
        /// Gets or sets the order GUID.
        /// </summary>
        /// <value>
        /// The order GUID.
        /// </value>
        public string OrderGuid { get; set; }
    }
}
