using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// Model class which represents a room control area
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "RoomControlArea"), IncludeInCodeGeneratorForAndroid]
    public class RoomControlArea : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.RoomControlArea type
        /// </summary>
        public RoomControlArea()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the room control area
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlAreaId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string NameSystem
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Visible
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the languages of the room control section
        /// </summary>
        [XmlArray("RoomControlAreaLanguages")]
        [XmlArrayItem("RoomControlAreaLanguage")]
        [IncludeInCodeGeneratorForAndroid]
        public RoomControlAreaLanguage[] RoomControlAreaLanguages
        { get; set; }

        /// <summary>
        /// Gets or sets the items of the room control section
        /// </summary>
        [XmlArray("RoomControlSections")]
        [XmlArrayItem("RoomControlSection")]
        [IncludeInCodeGeneratorForAndroid]
        public RoomControlSection[] RoomControlSections
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public RoomControlArea Clone()
        {
            return this.Clone<RoomControlArea>();
        }

        #endregion
    }
}
