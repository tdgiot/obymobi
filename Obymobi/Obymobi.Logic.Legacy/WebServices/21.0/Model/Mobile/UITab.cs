using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v21
{
    /// <summary>
    /// Model class which represents a UITab item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "UITab"), IncludeInCodeGeneratorForXamarin]
    public class UITab : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Mobile.UITab type
        /// </summary>
        public UITab()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the tab
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int UITabId
        { get; set; }

        /// Gets or sets the id of the UIMode this tab belongs to
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int UIModeId
        { get; set; }

        /// <summary>
        /// Gets or sets the caption of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Caption
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the categoryId of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int CategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the entertainmentId of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int EntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the entertainmentId of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SiteId
        { get; set; }

        /// <summary>
        /// Gets or sets the URL of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string URL
        { get; set; }

        /// <summary>
        /// Gets or sets the zoom level of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Zoom
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets if the tab should be visible or not
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool Visible
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [XmlArray("UITabLanguage")]
        [XmlArrayItem("UITabLanguage")]
        [IncludeInCodeGeneratorForXamarin]
        public UITabLanguage[] UITabLanguages
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UITab Clone()
        {
            return this.Clone<UITab>();
        }

        #endregion
    }
}
