using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v21
{
    /// <summary>
    /// Model class which represents a product suggestion
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "AttachmentLanguage"), IncludeInCodeGeneratorForXamarin]
    public class AttachmentLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.ProductSuggestion type
        /// </summary>
        public AttachmentLanguage()
        {
        }

        #endregion

        #region Xml Properties

         /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }


        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public AttachmentLanguage Clone()
        {
            return this.Clone<AttachmentLanguage>();
        }

        #endregion
    }
}
