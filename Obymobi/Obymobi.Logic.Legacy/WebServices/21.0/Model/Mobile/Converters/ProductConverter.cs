using Obymobi.Logic.Mobile;
using System.Linq;
using Dionysos;
namespace Obymobi.Logic.Model.Mobile.v21.Converters
{
	public class ProductConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v21.Product, Obymobi.Logic.Model.Mobile.Product>
	{
        public ProductConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v22.Converters.ProductConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v21.Product ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Product source)
        {
            Obymobi.Logic.Model.Mobile.v21.Product target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v21.Product();
                target.ProductId = source.ProductId;
                target.CategoryId = source.CategoryId;
                target.Name = source.Name;
                target.Description = source.Description;
                target.Type = source.Type;
                target.SubType = source.SubType;
                target.PriceIn = source.PriceIn;
                target.VatPercentage = source.VatPercentage;
                target.SortOrder = source.SortOrder;
                target.Geofencing = source.Geofencing;
                target.AllowFreeText = source.AllowFreeText;
                target.ScheduleId = source.ScheduleId;

                if (source.Alterations != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v21.Converters.AlterationConverter alterationsConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.AlterationConverter();
                    target.Alterations = (Alteration[])alterationsConverter.ConvertArrayToLegacyArray(source.Alterations);
                }

                target.ButtonText = source.ButtonText;
                target.WebTypeTabletUrl = source.WebTypeTabletUrl;
                target.WebTypeSmartphoneUrl = source.WebTypeSmartphoneUrl;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v21.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }                

                if (source.Attachments != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v21.Converters.AttachmentConverter attachmentsConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.AttachmentConverter();
                    target.Attachments = (Attachment[])attachmentsConverter.ConvertArrayToLegacyArray(source.Attachments);
                }

                if (source.ProductSuggestions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v21.Converters.ProductSuggestionConverter productSuggestionsConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.ProductSuggestionConverter();
                    target.ProductSuggestions = (ProductSuggestion[])productSuggestionsConverter.ConvertArrayToLegacyArray(source.ProductSuggestions);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<ProductLanguage> languages = new System.Collections.Generic.List<ProductLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        ProductLanguage model = new ProductLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.ProductLanguageId = -1;
                        model.ProductId = source.ProductId;
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductName);
                        model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductDescription);
                        model.ButtonText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductButtonText);

                        languages.Add(model);
                    }

                    target.ProductLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Product ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v21.Product source)
        {
            Obymobi.Logic.Model.Mobile.Product target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Product();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v22.Converters.ProductConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
