using Obymobi.Logic.Mobile;
using System.Linq;
using Dionysos;
namespace Obymobi.Logic.Model.Mobile.v21.Converters
{
	public class CompanyConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v21.Company, Obymobi.Logic.Model.Mobile.Company>
	{
        public CompanyConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v22.Converters.CompanyConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v21.Company ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Company source)
        {
            Obymobi.Logic.Model.Mobile.v21.Company target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v21.Company();
                target.CompanyId = source.CompanyId;
                target.Currency = source.Currency;
                target.Name = source.Name;
                target.Addressline1 = source.Addressline1;
                target.Addressline2 = source.Addressline2;
                target.Addressline3 = source.Addressline3;
                target.Zipcode = source.Zipcode;
                target.City = source.City;
                target.Country = source.Country;
                target.Latitude = source.Latitude;
                target.Longitude = source.Longitude;
                target.GeoFencingEnabled = source.GeoFencingEnabled;
                target.GeoFencingRadius = source.GeoFencingRadius;
                target.AllowFreeText = source.AllowFreeText;
                target.LanguageCode = Obymobi.Culture.Mappings[source.CultureCode].Language.CodeAlpha2.ToUpperInvariant();
                target.Telephone = source.Telephone;
                target.GoogleAnalyticsId = source.GoogleAnalyticsId;
                target.IsHero = source.IsHero;

                if (source.Businesshours != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v21.Converters.BusinesshoursConverter businesshoursConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.BusinesshoursConverter();
                    target.Businesshours = (Businesshours[])businesshoursConverter.ConvertArrayToLegacyArray(source.Businesshours);
                }

                target.BusinesshoursIntermediate = source.BusinesshoursIntermediate;
                target.ActionButtonId = source.ActionButtonId;
                target.ActionButtonUrlTablet = source.ActionButtonUrlTablet;
                target.ActionButtonUrlMobile = source.ActionButtonUrlMobile;
                target.CostIndicationType = source.CostIndicationType;
                target.CostIndicationValue = source.CostIndicationValue;
                target.CompanyDataLastModifiedTicks = source.CompanyDataLastModifiedTicks;
                target.CompanyMediaLastModifiedTicks = source.CompanyMediaLastModifiedTicks;
                target.MenuDataLastModifiedTicks = source.MenuDataLastModifiedTicks;
                target.MenuMediaLastModifiedTicks = source.MenuMediaLastModifiedTicks;
                target.DeliverypointDataLastModifiedTicks = source.DeliverypointDataLastModifiedTicks;
                target.AzureNotificationTag = source.AzureNotificationTag;
                target.TimeZone = source.TimeZone;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v21.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }                

                if (source.Deliverypointgroups != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v21.Converters.DeliverypointgroupConverter deliverypointgroupsConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.DeliverypointgroupConverter();
                    target.Deliverypointgroups = (Deliverypointgroup[])deliverypointgroupsConverter.ConvertArrayToLegacyArray(source.Deliverypointgroups);
                }

                if (source.Paymentmethods != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v21.Converters.PaymentmethodConverter paymentmethodsConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.PaymentmethodConverter();
                    target.Paymentmethods = (Paymentmethod[])paymentmethodsConverter.ConvertArrayToLegacyArray(source.Paymentmethods);
                }

                target.VenueCategoryIds = source.VenueCategoryIds;
                target.AmenityIds = source.AmenityIds;

                if (source.Schedules != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v21.Converters.ScheduleConverter schedulesConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.ScheduleConverter();
                    target.Schedules = (Schedule[])schedulesConverter.ConvertArrayToLegacyArray(source.Schedules);
                }

                target.IsPointOfInterest = source.IsPointOfInterest;

                if (source.PointOfInterestUIMode != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.Mobile.v21.Converters.UIModeConverter pointOfInterestUIModeConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.UIModeConverter();
                    target.PointOfInterestUIMode = (UIMode)pointOfInterestUIModeConverter.ConvertModelToLegacyModel(source.PointOfInterestUIMode);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<Language> languages = new System.Collections.Generic.List<Language>();

                    if (source.CompanyCultures != null && source.CompanyCultures.Length > 0)
                    {
                        // Company, use the company culture entities to fill the language models
                        foreach (CompanyCulture companyCulture in source.CompanyCultures)
                        {
                            System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, companyCulture.CultureCode);

                            Language model = new Language();
                            model.Code = Obymobi.Culture.Mappings[companyCulture.CultureCode].Language.CodeAlpha2.ToUpperInvariant();
                            model.Name = Obymobi.Culture.Mappings[companyCulture.CultureCode].Language.Name;
                            model.LocalizedName = Obymobi.Culture.Mappings[companyCulture.CultureCode].Language.Name;
                            model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.CompanyDescription);
                            model.DescriptionSingleLine = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.CompanyDescriptionShort);

                            languages.Add(model);
                        }
                    }
                    else
                    {
                        // PointOfInterest, use the custom texts to fill the language models
                        System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                        foreach (string cultureCode in cultureCodes)
                        {
                            System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                            Language model = new Language();
                            model.Code = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                            model.Name = Obymobi.Culture.Mappings[cultureCode].Language.Name;
                            model.LocalizedName = Obymobi.Culture.Mappings[cultureCode].Language.Name;
                            model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.PointOfInterestDescription);
                            model.DescriptionSingleLine = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.PointOfInterestDescriptionSingleLine);

                            languages.Add(model);
                        }
                    }

                    target.Languages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Company ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v21.Company source)
        {
            Obymobi.Logic.Model.Mobile.Company target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Company();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v22.Converters.CompanyConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
