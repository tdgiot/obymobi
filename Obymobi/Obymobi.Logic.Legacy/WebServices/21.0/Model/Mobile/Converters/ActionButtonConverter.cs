using Obymobi.Logic.Mobile;
using System.Linq;

namespace Obymobi.Logic.Model.Mobile.v21.Converters
{
	public class ActionButtonConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v21.ActionButton, Obymobi.Logic.Model.Mobile.ActionButton>
	{
        public ActionButtonConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v22.Converters.ActionButtonConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v21.ActionButton ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.ActionButton source)
        {
            Obymobi.Logic.Model.Mobile.v21.ActionButton target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v21.ActionButton();
                target.ActionButtonId = source.ActionButtonId;
                target.Name = source.Name;

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<ActionButtonLanguage> actionButtonLanguages = new System.Collections.Generic.List<ActionButtonLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        ActionButtonLanguage model = new ActionButtonLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = Dionysos.DictionaryUtil.GetValueOrDefault(customTexts, Obymobi.Enums.CustomTextType.ActionButtonName);

                        actionButtonLanguages.Add(model);
                    }

                    target.ActionButtonLanguages = actionButtonLanguages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.ActionButton ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v21.ActionButton source)
        {
            Obymobi.Logic.Model.Mobile.ActionButton target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.ActionButton();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v22.Converters.ActionButtonConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
