using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v21.Converters
{
	public class CustomerConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v21.Customer, Obymobi.Logic.Model.Mobile.Customer>
	{
        public CustomerConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v22.Converters.CustomerConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v21.Customer ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Customer source)
        {
            Obymobi.Logic.Model.Mobile.v21.Customer target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v21.Customer();
                target.CustomerId = source.CustomerId;
                target.Salt = source.Salt;
                target.EmailAddress = source.EmailAddress;
                target.Phonenumber = source.Phonenumber;
                target.Firstname = source.Firstname;
                target.Lastname = source.Lastname;
                target.LastnamePrefix = source.LastnamePrefix;
                target.BirthdateTimestamp = source.BirthdateTimestamp;
                target.Gender = source.Gender;
                target.AddressLine1 = source.AddressLine1;
                target.AddressLine2 = source.AddressLine2;
                target.Zipcode = source.Zipcode;
                target.City = source.City;
                target.CountryCode = source.CountryCode;
                target.AnonymousAccount = source.AnonymousAccount;
                target.Guid = source.Guid;
                target.SocialmediaType = source.SocialmediaType;
                target.ExternalId = source.ExternalId;
                target.Verified = source.Verified;
                target.CampaignName = source.CampaignName;
                target.CampaignSource = source.CampaignSource;
                target.CampaignMedium = source.CampaignMedium;
                target.CampaignContent = source.CampaignContent;

                if (source.CustomerSocialmedia != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v21.Converters.CustomerSocialmediaConverter customerSocialmediaConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.CustomerSocialmediaConverter();
                    target.CustomerSocialmedia = (CustomerSocialmedia[])customerSocialmediaConverter.ConvertArrayToLegacyArray(source.CustomerSocialmedia);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Customer ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v21.Customer source)
        {
            Obymobi.Logic.Model.Mobile.Customer target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Customer();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v22.Converters.CustomerConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
