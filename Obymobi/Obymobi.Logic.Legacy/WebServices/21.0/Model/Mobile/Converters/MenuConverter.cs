using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v21.Converters
{
	public class MenuConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v21.Menu, Obymobi.Logic.Model.Mobile.Menu>
	{
        public MenuConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v22.Converters.MenuConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v21.Menu ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Menu source)
        {
            Obymobi.Logic.Model.Mobile.v21.Menu target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v21.Menu();
                target.MenuId = source.MenuId;
                target.Name = source.Name;
                target.Ticks = source.Ticks;

                if (source.Categories != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v21.Converters.CategoryConverter categoriesConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.CategoryConverter();
                    target.Categories = (Category[])categoriesConverter.ConvertArrayToLegacyArray(source.Categories);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Menu ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v21.Menu source)
        {
            Obymobi.Logic.Model.Mobile.Menu target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Menu();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v22.Converters.MenuConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
