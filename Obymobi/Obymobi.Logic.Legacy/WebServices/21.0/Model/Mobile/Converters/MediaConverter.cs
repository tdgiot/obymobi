using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v21.Converters
{
	public class MediaConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v21.Media, Obymobi.Logic.Model.Mobile.Media>
	{
        public MediaConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v22.Converters.MediaConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v21.Media ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Media source)
        {
            Obymobi.Logic.Model.Mobile.v21.Media target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v21.Media();
                target.MediaId = source.MediaId;
                target.Url = source.Url;
                target.Filename = source.Filename;
                target.MediaType = (int)MediaHelper.GetCorrectMediaTypeIfGeneric((MediaType)source.MediaType);
                target.CdnPath = source.CdnPath;
                target.ActionProductId = source.ActionProductId;
                target.ActionCategoryId = source.ActionCategoryId;
                target.ActionEntertainmentId = source.ActionEntertainmentId;
                target.ActionEntertainmentcategoryId = source.ActionEntertainmentcategoryId;
                target.ActionUrl = source.ActionUrl;
                target.SizeMode = source.SizeMode;
                target.ZoomLevel = source.ZoomLevel;
                target.Parent = source.Parent;
                target.AgnosticMediaId = source.AgnosticMediaId;
                target.ActionSiteId = source.ActionSiteId;
                target.ActionPageId = source.ActionPageId;

                if (source.MediaCultures != null)
                {
                    System.Collections.Generic.List<MediaLanguage> languages = new System.Collections.Generic.List<MediaLanguage>();

                    foreach (MediaCulture mediaCulture in source.MediaCultures)
                    {
                        MediaLanguage mediaLanguage = new MediaLanguage();
                        mediaLanguage.MediaLanguageId = -1;
                        mediaLanguage.LanguageCode = Obymobi.Culture.Mappings[mediaCulture.CultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        mediaLanguage.MediaId = mediaCulture.MediaId;
                        mediaLanguage.Parent = mediaCulture.Parent;

                        languages.Add(mediaLanguage);
                    }

                    target.MediaLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Media ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v21.Media source)
        {
            Obymobi.Logic.Model.Mobile.Media target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Media();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v22.Converters.MediaConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
