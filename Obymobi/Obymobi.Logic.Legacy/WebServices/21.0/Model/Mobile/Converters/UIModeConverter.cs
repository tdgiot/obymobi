using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v21.Converters
{
	public class UIModeConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v21.UIMode, Obymobi.Logic.Model.Mobile.UIMode>
	{
        public UIModeConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v22.Converters.UIModeConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v21.UIMode ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.UIMode source)
        {
            Obymobi.Logic.Model.Mobile.v21.UIMode target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v21.UIMode();
                target.UIModeId = source.UIModeId;
                target.Name = source.Name;

                if (source.UITabs != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v21.Converters.UITabConverter uITabsConverter = new Obymobi.Logic.Model.Mobile.v21.Converters.UITabConverter();
                    target.UITabs = (UITab[])uITabsConverter.ConvertArrayToLegacyArray(source.UITabs);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.UIMode ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v21.UIMode source)
        {
            Obymobi.Logic.Model.Mobile.UIMode target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.UIMode();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v22.Converters.UIModeConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
