using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v21
{
    /// <summary>
    /// Model class which represents a alterationoption language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "AlterationoptionLanguage"), IncludeInCodeGeneratorForXamarin]
    public class AlterationoptionLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.AlterationoptionLanguage type
        /// </summary>
        public AlterationoptionLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the alterationoption language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationoptionLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the alterationoption
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationoptionId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the alterationoption
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public AlterationoptionLanguage Clone()
        {
            return this.Clone<AlterationoptionLanguage>();
        }

        #endregion
    }
}
