using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Attributes;
using System.Xml.Serialization;

namespace Obymobi.Logic.Model.v21
{
    /// <summary>
    /// FolioItem model
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "FolioItem"), IncludeInCodeGeneratorForAndroid]
    public class FolioItem : ModelBase
    {
        /// <summary>
        /// Code
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Code { get; set; }

        /// <summary>
        /// Human-readable description of this particular charge item
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Description { get; set; }

        /// <summary>
        /// Credit or debit amount of this particular folio item. Negative
        /// amounts will be treated as credits to the account.
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public decimal PriceIn { get; set; }

        /// <summary>
        /// Date and time on which this credit / charge was posted to the folio
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public DateTime Created { get; set; }
    }
}
