using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v20
{
    /// <summary>
    /// Folio model
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "WakeUp"), IncludeInCodeGeneratorForAndroid]
    public class WakeUpStatus : ModelBase
    {
        public enum OnSiteServerWebserviceJobTypeEnum : int
        {
            PmsWakeUpSet = 1,
            PmsWakeUpCleared = 2,
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WakeUpStatus"/> class.
        /// </summary>        
        public WakeUpStatus()
        {
        }

        [XmlIgnore]
        public OnSiteServerWebserviceJobTypeEnum OnSiteServerWebserviceJobType { get; set; }

        /// <summary>
        /// The DeliverypointNumber, i.e. RoomNumber
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string DeliverypointNumber { get; set; }

        /// <summary>
        /// Account number
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Wake up date
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public DateTime WakeUpDate { get; set; }

        /// <summary>
        /// Wake up time
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public DateTime WakeUpTime { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Status { get; set; }

        /// <summary>
        /// Info
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Info { get; set; }
    }
}
