using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v20
{
    /// <summary>
    /// Checkout class
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Checkout"), IncludeInCodeGeneratorForAndroid]
    public class Checkout : ModelBase
    {
        /// <summary>
        /// The DeliverypointNumber, i.e. RoomNumber
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string DeliverypointNumber { get; set; }

        /// <summary>
        /// Unique identifier used for charging this guest for goods and services purchased
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Amount of money agreed to be charged to the customer (including VAT)
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public decimal ChargePriceIn { get; set; }

        /// <summary>
        /// Indicates if the amount to be charged has been paid directly via Crave, otherwise
        /// it will be charged on the payment method registered at check in (mostly credit card)
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool ChargePaid { get; set; }

        /// <summary>
        /// Will be set by the On-site Server for processing in the webservice queu
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool CheckoutSuccessful { get; set; }
    }
}
