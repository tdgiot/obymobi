using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v20
{
    /// <summary>
    /// Model class which represents an advertisement language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "AdvertisementLanguage"), IncludeInCodeGeneratorForAndroid]
    public class AdvertisementLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.AdvertisementLanguage type
        /// </summary>
        public AdvertisementLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the advertisement language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int AdvertisementLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the advertisement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AdvertisementId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the title of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Description
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public AdvertisementLanguage Clone()
        {
            return this.Clone<AdvertisementLanguage>();
        }

        #endregion
    }
}
