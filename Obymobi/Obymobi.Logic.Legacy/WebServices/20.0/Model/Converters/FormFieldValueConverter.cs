namespace Obymobi.Logic.Model.v20.Converters
{
	public class FormFieldValueConverter : ModelConverterBase<Obymobi.Logic.Model.v20.FormFieldValue, Obymobi.Logic.Model.FormFieldValue>
	{
        public FormFieldValueConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.FormFieldValueConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.FormFieldValue ConvertModelToLegacyModel(Obymobi.Logic.Model.FormFieldValue source)
        {
            Obymobi.Logic.Model.v20.FormFieldValue target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.FormFieldValue();
                target.FormFieldValueId = source.FormFieldValueId;
                target.FormFieldId = source.FormFieldId;
                target.Value = source.Value;

                if (source.FormFieldValueLanguages != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.FormFieldValueLanguageConverter formFieldValueLanguagesConverter = new Obymobi.Logic.Model.v20.Converters.FormFieldValueLanguageConverter();
                    target.FormFieldValueLanguages = (FormFieldValueLanguage[])formFieldValueLanguagesConverter.ConvertArrayToLegacyArray(source.FormFieldValueLanguages);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.FormFieldValue ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.FormFieldValue source)
        {
            Obymobi.Logic.Model.FormFieldValue target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.FormFieldValue();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.FormFieldValueConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
