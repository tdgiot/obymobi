using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v20.Converters
{
	public class RoomControlWidgetConverter : ModelConverterBase<Obymobi.Logic.Model.v20.RoomControlWidget, Obymobi.Logic.Model.RoomControlWidget>
	{
        public RoomControlWidgetConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.RoomControlWidgetConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.RoomControlWidget ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomControlWidget source)
        {
            Obymobi.Logic.Model.v20.RoomControlWidget target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.RoomControlWidget();
                target.RoomControlWidgetId = source.RoomControlWidgetId;
                target.Caption = source.Caption;
                target.Type = source.Type;
                target.SortOrder = source.SortOrder;
                target.RoomControlComponentId1 = source.RoomControlComponentId1;
                target.RoomControlComponentId2 = source.RoomControlComponentId2;
                target.RoomControlComponentId3 = source.RoomControlComponentId3;
                target.RoomControlComponentId4 = source.RoomControlComponentId4;
                target.RoomControlComponentId5 = source.RoomControlComponentId5;
                target.RoomControlComponentId6 = source.RoomControlComponentId6;
                target.RoomControlComponentId7 = source.RoomControlComponentId7;
                target.RoomControlComponentId8 = source.RoomControlComponentId8;
                target.RoomControlComponentId9 = source.RoomControlComponentId9;
                target.RoomControlComponentId10 = source.RoomControlComponentId10;
                target.Visible = source.Visible;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;
                target.Parent = source.Parent;

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<RoomControlWidgetLanguage> languages = new System.Collections.Generic.List<RoomControlWidgetLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        RoomControlWidgetLanguage model = new RoomControlWidgetLanguage();
                        model.RoomControlWidgetLanguageId = -1;
                        model.Caption = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.RoomControlWidgetCaption);
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();

                        languages.Add(model);
                    }

                    target.RoomControlWidgetLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomControlWidget ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.RoomControlWidget source)
        {
            Obymobi.Logic.Model.RoomControlWidget target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomControlWidget();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.RoomControlWidgetConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
