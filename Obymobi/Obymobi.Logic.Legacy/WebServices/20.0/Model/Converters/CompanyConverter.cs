using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v20.Converters
{
	public class CompanyConverter : ModelConverterBase<Obymobi.Logic.Model.v20.Company, Obymobi.Logic.Model.Company>
	{
        public CompanyConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.CompanyConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.Company ConvertModelToLegacyModel(Obymobi.Logic.Model.Company source)
        {
            Obymobi.Logic.Model.v20.Company target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.Company();
                target.CompanyId = source.CompanyId;
                target.Currency = source.Currency;
                target.CurrencySymbol = source.CurrencySymbol;
                target.ShowCurrencySymbol = source.ShowCurrencySymbol;
                target.Name = source.Name;
                target.Code = source.Code;
                target.Addressline1 = source.Addressline1;
                target.Addressline2 = source.Addressline2;
                target.Addressline3 = source.Addressline3;
                target.Zipcode = source.Zipcode;
                target.City = source.City;
                target.Country = source.Country;
                target.CountryCode = source.CountryCode;
                target.CountryCodeAlpha3 = source.CountryCodeAlpha3;
                target.Latitude = source.Latitude;
                target.Longitude = source.Longitude;
                target.GeoFencingEnabled = source.GeoFencingEnabled;
                target.GeoFencingRadius = source.GeoFencingRadius;
                target.FacebookPlaceId = source.FacebookPlaceId;
                target.FoursquareVenueId = source.FoursquareVenueId;
                target.LanguageCode = source.LanguageCode;
                target.RelatedDataLoaded = source.RelatedDataLoaded;
                target.Telephone = source.Telephone;
                target.Fax = source.Fax;
                target.Website = source.Website;
                target.Email = source.Email;
                target.Description = source.Description;
                target.DescriptionSingleLine = source.DescriptionSingleLine;
                target.GoogleAnalyticsId = source.GoogleAnalyticsId;
                target.CompanyDataLastModifiedTicks = source.CompanyDataLastModifiedTicks;
                target.CompanyMediaLastModifiedTicks = source.CompanyMediaLastModifiedTicks;
                target.MenuDataLastModifiedTicks = source.MenuDataLastModifiedTicks;
                target.MenuMediaLastModifiedTicks = source.MenuMediaLastModifiedTicks;
                target.PosIntegrationInformationLastModifiedTicks = source.PosIntegrationInformationLastModifiedTicks;
                target.DeliverypointDataLastModifiedTicks = source.DeliverypointDataLastModifiedTicks;
                target.SurveyDataLastModifiedTicks = source.SurveyDataLastModifiedTicks;
                target.SurveyMediaLastModifiedTicks = source.SurveyMediaLastModifiedTicks;
                target.AnnouncementDataLastModifiedTicks = source.AnnouncementDataLastModifiedTicks;
                target.AnnouncementMediaLastModifiedTicks = source.AnnouncementMediaLastModifiedTicks;
                target.EntertainmentDataLastModifiedTicks = source.EntertainmentDataLastModifiedTicks;
                target.EntertainmentMediaLastModifiedTicks = source.EntertainmentMediaLastModifiedTicks;
                target.AdvertisementDataLastModifiedTicks = source.AdvertisementDataLastModifiedTicks;
                target.AdvertisementMediaLastModifiedTicks = source.AdvertisementMediaLastModifiedTicks;
                target.HandlingMethod = source.HandlingMethod;
                target.SupportpoolId = source.SupportpoolId;
                target.FreeText = source.FreeText;
                target.UseBillButton = source.UseBillButton;
                target.Salt = source.Salt;
                target.DeviceRebootMethod = source.DeviceRebootMethod;

                if (source.ActionButton != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v20.Converters.ActionButtonConverter actionButtonConverter = new Obymobi.Logic.Model.v20.Converters.ActionButtonConverter();
                    target.ActionButton = (ActionButton)actionButtonConverter.ConvertModelToLegacyModel(source.ActionButton);
                }

                target.ActionButtonUrlTablet = source.ActionButtonUrlTablet;
                target.ActionButtonUrlMobile = source.ActionButtonUrlMobile;
                target.CostIndicationType = source.CostIndicationType;
                target.CostIndicationValue = source.CostIndicationValue;
                target.CometHandlerType = source.CometHandlerType;
                target.BusinesshoursIntermediate = source.BusinesshoursIntermediate;
                target.SystemPassword = source.SystemPassword;
                target.AzureNotificationTag = source.AzureNotificationTag;
                target.TemperatureUnit = source.TemperatureUnit;
                target.ClockMode = source.ClockMode;
                target.WeatherClockWidgetMode = source.WeatherClockWidgetMode;
                target.TimeZone = source.TimeZone;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v20.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }                

                if (source.Deliverypointgroups != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.DeliverypointgroupConverter deliverypointgroupsConverter = new Obymobi.Logic.Model.v20.Converters.DeliverypointgroupConverter();
                    target.Deliverypointgroups = (Deliverypointgroup[])deliverypointgroupsConverter.ConvertArrayToLegacyArray(source.Deliverypointgroups);
                }

                if (source.Terminals != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.TerminalConverter terminalsConverter = new Obymobi.Logic.Model.v20.Converters.TerminalConverter();
                    target.Terminals = (Terminal[])terminalsConverter.ConvertArrayToLegacyArray(source.Terminals);
                }

                if (source.Businesshours != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.BusinesshoursConverter businesshoursConverter = new Obymobi.Logic.Model.v20.Converters.BusinesshoursConverter();
                    target.Businesshours = (Businesshours[])businesshoursConverter.ConvertArrayToLegacyArray(source.Businesshours);
                }

                if (source.MessageTemplates != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.MessageTemplateConverter messageTemplatesConverter = new Obymobi.Logic.Model.v20.Converters.MessageTemplateConverter();
                    target.MessageTemplates = (MessageTemplate[])messageTemplatesConverter.ConvertArrayToLegacyArray(source.MessageTemplates);
                }

                if (source.UIModes != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.UIModeConverter uIModesConverter = new Obymobi.Logic.Model.v20.Converters.UIModeConverter();
                    target.UIModes = (UIMode[])uIModesConverter.ConvertArrayToLegacyArray(source.UIModes);
                }

                if (source.VenueCategories != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.VenueCategoryConverter venueCategoriesConverter = new Obymobi.Logic.Model.v20.Converters.VenueCategoryConverter();
                    target.VenueCategories = (VenueCategory[])venueCategoriesConverter.ConvertArrayToLegacyArray(source.VenueCategories);
                }

                if (source.Amenities != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.AmenityConverter amenitiesConverter = new Obymobi.Logic.Model.v20.Converters.AmenityConverter();
                    target.Amenities = (Amenity[])amenitiesConverter.ConvertArrayToLegacyArray(source.Amenities);
                }

                if (source.Schedules != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.ScheduleConverter schedulesConverter = new Obymobi.Logic.Model.v20.Converters.ScheduleConverter();
                    target.Schedules = (Schedule[])schedulesConverter.ConvertArrayToLegacyArray(source.Schedules);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<Language> languages = new System.Collections.Generic.List<Language>();

                    if (source.CompanyCultures != null && source.CompanyCultures.Length > 0)
                    {
                        // Company, use the company culture entities to fill the language models
                        foreach (CompanyCulture companyCulture in source.CompanyCultures)
                        {
                            System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, companyCulture.CultureCode);

                            Language model = new Language();
                            model.Code = Obymobi.Culture.Mappings[companyCulture.CultureCode].Language.CodeAlpha2.ToUpperInvariant();
                            model.Name = Obymobi.Culture.Mappings[companyCulture.CultureCode].Language.Name;
                            model.LocalizedName = Obymobi.Culture.Mappings[companyCulture.CultureCode].Language.Name;
                            model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.CompanyDescription);
                            model.VenuePageDescription = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.CompanyVenuePageDescription);                            

                            languages.Add(model);
                        }
                    }
                    else
                    {
                        // PointOfInterest, use the custom texts to fill the language models
                        System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                        foreach (string cultureCode in cultureCodes)
                        {
                            System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                            Language model = new Language();
                            model.Code = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                            model.Name = Obymobi.Culture.Mappings[cultureCode].Language.Name;
                            model.LocalizedName = Obymobi.Culture.Mappings[cultureCode].Language.Name;
                            model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.PointOfInterestDescription);
                            model.VenuePageDescription = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.PointOfInterestVenuePageDescription);
                            
                            languages.Add(model);
                        }
                    }

                    target.Languages = languages.ToArray();
                }

                target.UseAgeCheckInOtoucho = source.UseAgeCheckInOtoucho;
                target.UseManualDeliverypointsEntryInOtoucho = source.UseManualDeliverypointsEntryInOtoucho;
                target.UseStatelessInOtoucho = source.UseStatelessInOtoucho;
                target.SkipOrderOverviewAfterOrderSubmitInOtoucho = source.SkipOrderOverviewAfterOrderSubmitInOtoucho;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Company ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.Company source)
        {
            Obymobi.Logic.Model.Company target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Company();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.CompanyConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
