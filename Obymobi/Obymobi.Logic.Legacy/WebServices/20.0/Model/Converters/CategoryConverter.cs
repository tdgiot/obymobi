using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v20.Converters
{
	public class CategoryConverter : ModelConverterBase<Obymobi.Logic.Model.v20.Category, Obymobi.Logic.Model.Category>
	{
        public CategoryConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.CategoryConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.Category ConvertModelToLegacyModel(Obymobi.Logic.Model.Category source)
        {
            Obymobi.Logic.Model.v20.Category target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.Category();
                target.CategoryId = source.CategoryId;
                target.ParentCategoryId = source.ParentCategoryId;
                target.GenericcategoryId = source.GenericcategoryId;
                target.Name = source.Name;
                target.Description = source.Description;
                target.SortOrder = source.SortOrder;
                target.Rateable = source.Rateable;
                target.AnnouncementAction = source.AnnouncementAction;

                if (source.Categories != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.CategoryConverter categoriesConverter = new Obymobi.Logic.Model.v20.Converters.CategoryConverter();
                    target.Categories = (Category[])categoriesConverter.ConvertArrayToLegacyArray(source.Categories);
                }

                if (source.Products != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.ProductConverter productsConverter = new Obymobi.Logic.Model.v20.Converters.ProductConverter();
                    target.Products = (Product[])productsConverter.ConvertArrayToLegacyArray(source.Products);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v20.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.AdvertisementTags != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.AdvertisementTagConverter advertisementTagsConverter = new Obymobi.Logic.Model.v20.Converters.AdvertisementTagConverter();
                    target.AdvertisementTags = (AdvertisementTag[])advertisementTagsConverter.ConvertArrayToLegacyArray(source.AdvertisementTags);
                }                

                if (source.Poscategory != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v20.Converters.PoscategoryConverter poscategoryConverter = new Obymobi.Logic.Model.v20.Converters.PoscategoryConverter();
                    target.Poscategory = (Poscategory)poscategoryConverter.ConvertModelToLegacyModel(source.Poscategory);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<CategoryLanguage> languages = new System.Collections.Generic.List<CategoryLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        CategoryLanguage model = new CategoryLanguage();
                        model.CategoryLanguageId = -1;
                        model.CategoryId = source.CategoryId;
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.CategoryName);
                        model.OrderProcessedTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.CategoryOrderProcessedTitle);
                        model.OrderProcessedMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.CategoryOrderProcessedText);
                        model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.CategoryDescription);                        

                        languages.Add(model);
                    }

                    target.CategoryLanguages = languages.ToArray();
                }

                target.ContainedProductIds = source.ContainedProductIds;
                target.ProductId = source.ProductId;
                target.Type = source.Type;
                target.HidePrices = source.HidePrices;
                target.ViewLayoutType = source.ViewLayoutType;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Category ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.Category source)
        {
            Obymobi.Logic.Model.Category target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Category();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.CategoryConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
