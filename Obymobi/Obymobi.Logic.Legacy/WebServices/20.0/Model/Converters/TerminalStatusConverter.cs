namespace Obymobi.Logic.Model.v20.Converters
{
	public class TerminalStatusConverter : ModelConverterBase<Obymobi.Logic.Model.v20.TerminalStatus, Obymobi.Logic.Model.TerminalStatus>
	{
        public TerminalStatusConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.TerminalStatusConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.TerminalStatus ConvertModelToLegacyModel(Obymobi.Logic.Model.TerminalStatus source)
        {
            Obymobi.Logic.Model.v20.TerminalStatus target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.TerminalStatus();
                target.TerminalStatusId = source.TerminalStatusId;
                target.PrivateIpAddresses = source.PrivateIpAddresses;
                target.PublicIpAddress = source.PublicIpAddress;
                target.BatteryLevel = source.BatteryLevel;
                target.IsCharging = source.IsCharging;
                target.StatusCode = source.StatusCode;
                target.ApplicationVersion = source.ApplicationVersion;
                target.OsVersion = source.OsVersion;
                target.WifiStrength = source.WifiStrength;
                target.AgentIsRunning = source.AgentIsRunning;
                target.SupportToolsIsRunning = source.SupportToolsIsRunning;
                target.PrinterConnected = source.PrinterConnected;
            }

            return target;
        }

        public override Obymobi.Logic.Model.TerminalStatus ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.TerminalStatus source)
        {
            Obymobi.Logic.Model.TerminalStatus target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.TerminalStatus();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.TerminalStatusConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
