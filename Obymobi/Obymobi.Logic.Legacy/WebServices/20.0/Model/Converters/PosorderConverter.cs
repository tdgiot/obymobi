namespace Obymobi.Logic.Model.v20.Converters
{
	public class PosorderConverter : ModelConverterBase<Obymobi.Logic.Model.v20.Posorder, Obymobi.Logic.Model.Posorder>
	{
        public PosorderConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.PosorderConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.Posorder ConvertModelToLegacyModel(Obymobi.Logic.Model.Posorder source)
        {
            Obymobi.Logic.Model.v20.Posorder target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.Posorder();
                target.PosorderId = source.PosorderId;
                target.PosdeliverypointExternalId = source.PosdeliverypointExternalId;
                target.PospaymentmethodExternalId = source.PospaymentmethodExternalId;
                target.Type = source.Type;

                if (source.Posorderitems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.PosorderitemConverter posorderitemsConverter = new Obymobi.Logic.Model.v20.Converters.PosorderitemConverter();
                    target.Posorderitems = (Posorderitem[])posorderitemsConverter.ConvertArrayToLegacyArray(source.Posorderitems);
                }

                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Posorder ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.Posorder source)
        {
            Obymobi.Logic.Model.Posorder target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Posorder();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.PosorderConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
