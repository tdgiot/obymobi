using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v20.Converters
{
	public class ClientConverter : ModelConverterBase<Obymobi.Logic.Model.v20.Client, Obymobi.Logic.Model.Client>
	{
        public ClientConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.ClientConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.Client ConvertModelToLegacyModel(Obymobi.Logic.Model.Client source)
        {
            Obymobi.Logic.Model.v20.Client target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.Client();
                target.ClientId = source.ClientId;
                target.CompanyId = source.CompanyId;
                target.CompanyOwnerUsername = source.CompanyOwnerUsername;
                target.CompanyOwnerPassword = source.CompanyOwnerPassword;
                target.CustomerId = source.CustomerId;
                target.CustomerHashedPassword = source.CustomerHashedPassword;
                target.CompanyName = source.CompanyName;
                target.LastRequest = source.LastRequestUTC.UtcToLocalTime();
                target.LastRequestNotifiedBySMS = source.LastRequestNotifiedBySMSUTC.UtcToLocalTime();
                target.SmsNotificationRequired = source.SmsNotificationRequired;
                target.LastStatus = source.LastStatus;
                target.LastStatusText = source.LastStatusText;
                target.CurrentState = source.CurrentState;
                target.OperationMode = source.OperationMode;
                target.RelatedCustomerFullname = source.RelatedCustomerFullname;
                target.PingOfLifeInterval = source.PingOfLifeInterval;
                target.ScreenTimeoutInteval = source.ScreenTimeoutInteval;
                target.DeviceActivationRequired = source.DeviceActivationRequired;
                target.ReorderNotificationEnabled = source.ReorderNotificationEnabled;
                target.ReorderNotificationInterval = source.ReorderNotificationInterval;
                target.ReorderNotificationAnnouncementId = source.ReorderNotificationAnnouncementId;
                target.Locale = source.Locale;
                target.LogToFile = source.LogToFile;
                target.Pincode = source.Pincode;
                target.PincodeSU = source.PincodeSU;
                target.PincodeGM = source.PincodeGM;
                target.UseManualDeliverypoint = source.UseManualDeliverypoint;
                target.UseManualDeliverypointEncryption = source.UseManualDeliverypointEncryption;
                target.EncryptionSalt = source.EncryptionSalt;
                target.HideDeliverypointNumbers = source.HideDeliverypointNumbers;
                target.HidePrices = source.HidePrices;
                target.ClearSessionOnTimeout = source.ClearSessionOnTimeout;
                target.ResetTimeout = source.ResetTimeout;
                target.DefaultProductFullView = source.DefaultProductFullView;
                target.MarketingSurveyUrl = source.MarketingSurveyUrl;
                target.HotelUrl1 = source.HotelUrl1;
                target.HotelUrl1Caption = source.HotelUrl1Caption;
                target.HotelUrl1Zoom = source.HotelUrl1Zoom;
                target.HotelUrl2 = source.HotelUrl2;
                target.HotelUrl2Caption = source.HotelUrl2Caption;
                target.HotelUrl2Zoom = source.HotelUrl2Zoom;
                target.HotelUrl3 = source.HotelUrl3;
                target.HotelUrl3Caption = source.HotelUrl3Caption;
                target.HotelUrl3Zoom = source.HotelUrl3Zoom;
                target.DimLevelDull = source.DimLevelDull;
                target.DimLevelMedium = source.DimLevelMedium;
                target.DimLevelBright = source.DimLevelBright;
                target.DockedDimLevelDull = source.DockedDimLevelDull;
                target.DockedDimLevelMedium = source.DockedDimLevelMedium;
                target.DockedDimLevelBright = source.DockedDimLevelBright;
                target.PowerSaveTimeout = source.PowerSaveTimeout;
                target.PowerSaveLevel = source.PowerSaveLevel;
                target.OutOfChargeLevel = source.OutOfChargeLevel;
                target.OutOfChargeTitle = source.OutOfChargeTitle;
                target.OutOfChargeMessage = source.OutOfChargeMessage;
                target.OrderProcessedTitle = source.OrderProcessedTitle;
                target.OrderProcessedMessage = source.OrderProcessedMessage;
                target.OrderFailedTitle = source.OrderFailedTitle;
                target.OrderFailedMessage = source.OrderFailedMessage;
                target.OrderSavingTitle = source.OrderSavingTitle;
                target.OrderSavingMessage = source.OrderSavingMessage;
                target.OrderCompletedTitle = source.OrderCompletedTitle;
                target.OrderCompletedMessage = source.OrderCompletedMessage;
                target.OrderCompletedEnabled = source.OrderCompletedEnabled;
                target.ServiceSavingTitle = source.ServiceSavingTitle;
                target.ServiceSavingMessage = source.ServiceSavingMessage;
                target.ServiceProcessedTitle = source.ServiceProcessedTitle;
                target.ServiceProcessedMessage = source.ServiceProcessedMessage;
                target.RatingSavingTitle = source.RatingSavingTitle;
                target.RatingSavingMessage = source.RatingSavingMessage;
                target.RatingProcessedTitle = source.RatingProcessedTitle;
                target.RatingProcessedMessage = source.RatingProcessedMessage;
                target.OrderingNotAvailableMessage = source.OrderingNotAvailableMessage;
                target.DailyOrderReset = source.DailyOrderReset;
                target.SleepTime = source.SleepTime;
                target.WakeUpTime = source.WakeUpTime;
                target.OrderStatusOnthecaseEnabled = source.OrderStatusOnthecaseEnabled;
                target.OrderStatusOnthecaseTitle = source.OrderStatusOnthecaseTitle;
                target.OrderStatusOnthecaseMessage = source.OrderStatusOnthecaseMessage;
                target.OrderStatusCompleteEnabled = source.OrderStatusCompleteEnabled;
                target.OrderStatusCompleteTitle = source.OrderStatusCompleteTitle;
                target.OrderStatusCompleteMessage = source.OrderStatusCompleteMessage;
                target.ServiceRequestStatusOnTheCaseTitle = source.ServiceRequestStatusOnTheCaseTitle;
                target.ServiceRequestStatusOnTheCaseMessage = source.ServiceRequestStatusOnTheCaseMessage;
                target.ServiceRequestStatusCompleteTitle = source.ServiceRequestStatusCompleteTitle;
                target.ServiceRequestStatusCompleteMessage = source.ServiceRequestStatusCompleteMessage;
                target.FreeformMessageTitle = source.FreeformMessageTitle;
                target.MacAddress = source.MacAddress;
                target.DeliverypointId = source.DeliverypointId;
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.DeliverypointgroupId = source.DeliverypointgroupId;
                target.WifiSecurityMethod = source.WifiSecurityMethod;
                target.WifiSsid = source.WifiSsid;
                target.WifiPassword = source.WifiPassword;
                target.WifiEapMode = source.WifiEapMode;
                target.WifiPhase2Authentication = source.WifiPhase2Authentication;
                target.WifiIdentity = source.WifiIdentity;
                target.WifiAnonymousIdentity = source.WifiAnonymousIdentity;
                target.HomepageSlideshowInterval = source.HomepageSlideshowInterval;
                target.AnnouncementDuration = source.AnnouncementDuration;
                target.DeliverypointCaption = source.DeliverypointCaption;
                target.UIModeId = source.UIModeId;
                target.PrintingEnabled = source.PrintingEnabled;
                target.LastCommunicationMethod = source.LastCommunicationMethod;
                target.LastSupportToolsVersion = source.LastSupportToolsVersion;
                target.LastAgentVersion = source.LastAgentVersion;
                target.LastCloudEnvironment = source.LastCloudEnvironment;
                target.LastSupportToolsRequest = source.LastSupportToolsRequest;
                target.IsChargerRemovedDialogEnabled = source.IsChargerRemovedDialogEnabled;
                target.ChargerRemovedDialogTitle = source.ChargerRemovedDialogTitle;
                target.ChargerRemovedDialogText = source.ChargerRemovedDialogText;
                target.IsChargerRemovedReminderDialogEnabled = source.IsChargerRemovedReminderDialogEnabled;
                target.ChargerRemovedReminderDialogTitle = source.ChargerRemovedReminderDialogTitle;
                target.ChargerRemovedReminderDialogText = source.ChargerRemovedReminderDialogText;
                target.UpdateEmenuDownloaded = source.UpdateEmenuDownloaded;
                target.UpdateAgentDownloaded = source.UpdateAgentDownloaded;
                target.UpdateSupportToolsDownloaded = source.UpdateSupportToolsDownloaded;
                target.PmsDeviceLockedTitle = source.PmsDeviceLockedTitle;
                target.PmsDeviceLockedText = source.PmsDeviceLockedText;
                target.PmsCheckinFailedTitle = source.PmsCheckinFailedTitle;
                target.PmsCheckinFailedText = source.PmsCheckinFailedText;
                target.PmsRestartTitle = source.PmsRestartTitle;
                target.PmsRestartText = source.PmsRestartText;
                target.PmsWelcomeTitle = source.PmsWelcomeTitle;
                target.PmsWelcomeText = source.PmsWelcomeText;
                target.PmsCheckoutApproveText = source.PmsCheckoutApproveText;
                target.PmsWelcomeTimeoutMinutes = source.PmsWelcomeTimeoutMinutes;
                target.PmsCheckoutCompleteTitle = source.PmsCheckoutCompleteTitle;
                target.PmsCheckoutCompleteText = source.PmsCheckoutCompleteText;
                target.GooglePrinterId = source.GooglePrinterId;
                target.PmsCheckoutCompleteTimeoutMinutes = source.PmsCheckoutCompleteTimeoutMinutes;
                target.ScreenOffMode = source.ScreenOffMode;
                target.PowerButtonSoftBehaviour = source.PowerButtonSoftBehaviour;
                target.PowerButtonHardBehaviour = source.PowerButtonHardBehaviour;
                target.RoomserviceCharge = source.RoomserviceCharge;
                target.EmailDocumentEnabled = source.EmailDocumentEnabled;
                target.DefaultRoomControlAreaName = source.DefaultRoomControlAreaName;
                target.ScreensaverMode = source.ScreensaverMode;                

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v20.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.Entertainmentcategories != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.EntertainmentcategoryConverter entertainmentcategoriesConverter = new Obymobi.Logic.Model.v20.Converters.EntertainmentcategoryConverter();
                    target.Entertainmentcategories = (Entertainmentcategory[])entertainmentcategoriesConverter.ConvertArrayToLegacyArray(source.Entertainmentcategories);
                }

                if (source.UIThemes != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.UIThemeConverter uIThemesConverter = new Obymobi.Logic.Model.v20.Converters.UIThemeConverter();
                    target.UIThemes = (UITheme[])uIThemesConverter.ConvertArrayToLegacyArray(source.UIThemes);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<ClientLanguage> languages = new System.Collections.Generic.List<ClientLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        ClientLanguage model = new ClientLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.OutOfChargeTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupOutOfChargeTitle);
                        model.OutOfChargeMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupOutOfChargeText);
                        model.OrderProcessedTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupOrderProcessedTitle);
                        model.OrderProcessedMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupOrderProcessedText);
                        model.OrderFailedTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupOrderFailedTitle);
                        model.OrderFailedMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupOrderFailedText);
                        model.OrderSavingTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupOrderSavingTitle);
                        model.OrderSavingMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupOrderSavingText);
                        model.OrderCompletedTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupOrderCompletedTitle);
                        model.OrderCompletedMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupOrderCompletedText);
                        model.OrderCompletedEnabled = source.OrderCompletedEnabled;
                        model.ServiceSavingTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupServiceSavingTitle);
                        model.ServiceSavingMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupServiceSavingText);
                        model.ServiceProcessedTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupServiceProcessedTitle);
                        model.ServiceProcessedMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupServiceProcessedText);                        
                        model.RatingSavingTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupRatingSavingTitle);
                        model.RatingSavingMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupRatingSavingText);
                        model.RatingProcessedTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupRatingProcessedTitle);
                        model.RatingProcessedMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupRatingProcessedText);
                        model.HotelUrl1 = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupHotelUrl1);
                        model.HotelUrl1Caption = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupHotelUrl1Caption);
                        model.HotelUrl1Zoom = source.HotelUrl1Zoom;
                        model.HotelUrl2 = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupHotelUrl2);
                        model.HotelUrl2Caption = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupHotelUrl2Caption);
                        model.HotelUrl2Zoom = source.HotelUrl2Zoom;
                        model.HotelUrl3 = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupHotelUrl3);
                        model.HotelUrl3Caption = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupHotelUrl3Caption);
                        model.HotelUrl3Zoom = source.HotelUrl3Zoom;
                        model.DeliverypointCaption = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupDeliverypointCaption);
                        model.PmsDeviceLockedTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPmsDeviceLockedTitle);
                        model.PmsDeviceLockedText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPmsDeviceLockedText);
                        model.PmsCheckinFailedTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPmsCheckinFailedTitle);
                        model.PmsCheckinFailedText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPmsCheckinFailedText);
                        model.PmsRestartTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPmsRestartTitle);
                        model.PmsRestartText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPmsRestartText);
                        model.PmsWelcomeTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPmsWelcomeTitle);
                        model.PmsWelcomeText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPmsWelcomeText);
                        model.PmsCheckoutApproveText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPmsCheckoutApproveText);
                        model.PmsCheckoutCompleteTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPmsCheckoutCompleteTitle);
                        model.PmsCheckoutCompleteText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPmsCheckoutCompleteText);
                        model.RoomserviceChargeText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupRoomserviceChargeText);
                        model.SuggestionsCaption = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupSuggestionsCaption);
                        model.PrintingConfirmationTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPrintingConfirmationTitle);
                        model.PrintingConfirmationText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPrintingConfirmationText);
                        model.PrintingSucceededTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPrintingSucceededTitle);
                        model.PrintingSucceededText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupPrintingSucceededText);
                        model.ChargerRemovedTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupChargerRemovedTitle);
                        model.ChargerRemovedText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupChargerRemovedText);
                        model.ChargerRemovedReminderTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupChargerRemovedReminderTitle);
                        model.ChargerRemovedReminderText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupChargerRemovedReminderText);                        

                        languages.Add(model);
                    }

                    target.ClientLanguages = languages.ToArray();
                }

                target.LastCommunicationMethodEnum = source.LastCommunicationMethodEnum;
                target.ScreenOffModeEnum = source.ScreenOffModeEnum;
                target.PowerButtonSoftModeEnum = source.PowerButtonSoftModeEnum;
                target.PowerButtonHardModeEnum = source.PowerButtonHardModeEnum;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Client ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.Client source)
        {
            Obymobi.Logic.Model.Client target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Client();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.ClientConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
