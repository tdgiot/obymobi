namespace Obymobi.Logic.Model.v20.Converters
{
	public class OrderRoutestephandlerConverter : ModelConverterBase<Obymobi.Logic.Model.v20.OrderRoutestephandler, Obymobi.Logic.Model.OrderRoutestephandler>
	{
        public OrderRoutestephandlerConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.OrderRoutestephandlerConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.OrderRoutestephandler ConvertModelToLegacyModel(Obymobi.Logic.Model.OrderRoutestephandler source)
        {
            Obymobi.Logic.Model.v20.OrderRoutestephandler target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.OrderRoutestephandler();
                target.OrderRoutestephandlerId = source.OrderRoutestephandlerId;
                target.Guid = source.Guid;
                target.OrderId = source.OrderId;
                target.TerminalId = source.TerminalId;
                target.Number = source.Number;
                target.HandlerType = source.HandlerType;
                target.PrintReportType = source.PrintReportType;
                target.Status = source.Status;
                target.Error = source.Error;
                target.Completed = source.Completed;
                target.Timeout = source.Timeout;
                target.TimeoutExpires = source.TimeoutExpires;
                target.ForwardedFromTerminalId = source.ForwardedFromTerminalId;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;
            }

            return target;
        }

        public override Obymobi.Logic.Model.OrderRoutestephandler ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.OrderRoutestephandler source)
        {
            Obymobi.Logic.Model.OrderRoutestephandler target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.OrderRoutestephandler();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.OrderRoutestephandlerConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
