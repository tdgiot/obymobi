namespace Obymobi.Logic.Model.v20.Converters
{
	public class PosalterationoptionConverter : ModelConverterBase<Obymobi.Logic.Model.v20.Posalterationoption, Obymobi.Logic.Model.Posalterationoption>
	{
        public PosalterationoptionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.PosalterationoptionConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.Posalterationoption ConvertModelToLegacyModel(Obymobi.Logic.Model.Posalterationoption source)
        {
            Obymobi.Logic.Model.v20.Posalterationoption target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.Posalterationoption();
                target.PosalterationoptionId = source.PosalterationoptionId;
                target.CompanyId = source.CompanyId;
                target.ExternalId = source.ExternalId;
                target.PosproductExternalId = source.PosproductExternalId;
                target.Name = source.Name;
                target.PriceIn = source.PriceIn;
                target.Description = source.Description;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Posalterationoption ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.Posalterationoption source)
        {
            Obymobi.Logic.Model.Posalterationoption target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Posalterationoption();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.PosalterationoptionConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
