namespace Obymobi.Logic.Model.v20.Converters
{
	public class SurveyResultAnswerConverter : ModelConverterBase<Obymobi.Logic.Model.v20.SurveyResultAnswer, Obymobi.Logic.Model.SurveyResultAnswer>
	{
        public SurveyResultAnswerConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.SurveyResultAnswerConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.SurveyResultAnswer ConvertModelToLegacyModel(Obymobi.Logic.Model.SurveyResultAnswer source)
        {
            Obymobi.Logic.Model.v20.SurveyResultAnswer target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.SurveyResultAnswer();
                target.SurveyResultAnswerId = source.SurveyResultAnswerId;
                target.SurveyQuestionId = source.SurveyQuestionId;
                target.SurveyAnswerId = source.SurveyAnswerId;
                target.Comment = source.Comment;
            }

            return target;
        }

        public override Obymobi.Logic.Model.SurveyResultAnswer ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.SurveyResultAnswer source)
        {
            Obymobi.Logic.Model.SurveyResultAnswer target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SurveyResultAnswer();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.SurveyResultAnswerConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
