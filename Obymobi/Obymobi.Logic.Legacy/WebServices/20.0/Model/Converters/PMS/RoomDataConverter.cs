namespace Obymobi.Logic.Model.v20.Converters
{
	public class RoomDataConverter : ModelConverterBase<Obymobi.Logic.Model.v20.RoomData, Obymobi.Logic.Model.RoomData>
	{
        public RoomDataConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.RoomDataConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.RoomData ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomData source)
        {
            Obymobi.Logic.Model.v20.RoomData target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.RoomData();
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.Error = source.Error;
                target.VoicemailMessageSet = source.VoicemailMessageSet;
                target.VoicemailMessage = source.VoicemailMessage;
                target.ClassOfServiceSet = source.ClassOfServiceSet;
                target.ClassOfService = source.ClassOfService;
                target.DoNotDisturbSet = source.DoNotDisturbSet;
                target.DoNotDisturb = source.DoNotDisturb;
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomData ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.RoomData source)
        {
            Obymobi.Logic.Model.RoomData target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomData();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.RoomDataConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
