using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v20.Converters
{
	public class AnnouncementConverter : ModelConverterBase<Obymobi.Logic.Model.v20.Announcement, Obymobi.Logic.Model.Announcement>
	{
        public AnnouncementConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.AnnouncementConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.Announcement ConvertModelToLegacyModel(Obymobi.Logic.Model.Announcement source)
        {
            Obymobi.Logic.Model.v20.Announcement target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.Announcement();
                target.AnnouncementId = source.AnnouncementId;
                target.Title = source.Title;
                target.Subtitle = source.Subtitle;
                target.Text = source.Text;
                target.DateToShow = source.DateToShow;
                target.TimeToShow = source.TimeToShow;
                target.Recurring = source.Recurring;
                target.RecurringPeriod = source.RecurringPeriod;
                target.RecurringBegin = source.RecurringBegin;
                target.RecurringEnd = source.RecurringEnd;
                target.RecurringAmount = source.RecurringAmount;
                target.RecurringMinutes = source.RecurringMinutes;
                target.DialogType = source.DialogType;
                target.OnYes = source.OnYes;
                target.OnNo = source.OnNo;
                target.OnYesCategory = source.OnYesCategory;
                target.OnNoCategory = source.OnNoCategory;
                target.OnYesEntertainmentCategory = source.OnYesEntertainmentCategory;
                target.OnNoEntertainmentCategory = source.OnNoEntertainmentCategory;
                target.OnYesEntertainment = source.OnYesEntertainment;
                target.OnYesProduct = source.OnYesProduct;
                target.MediaId = source.MediaId;
                target.Duration = source.Duration;

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<AnnouncementLanguage> languages = new System.Collections.Generic.List<AnnouncementLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        AnnouncementLanguage model = new AnnouncementLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.AnnouncementLanguageId = -1;
                        model.AnnouncementId = source.AnnouncementId;
                        model.Title = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.AnnouncementTitle);
                        model.Text = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.AnnouncementText);

                        languages.Add(model);
                    }

                    target.AnnouncementLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Announcement ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.Announcement source)
        {
            Obymobi.Logic.Model.Announcement target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Announcement();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.AnnouncementConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
