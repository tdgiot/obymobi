using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
namespace Obymobi.Logic.Model.v20.Converters
{
	public class UIThemeConverter : ModelConverterBase<Obymobi.Logic.Model.v20.UITheme, Obymobi.Logic.Model.UITheme>
	{
        public UIThemeConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.UIThemeConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.UITheme ConvertModelToLegacyModel(Obymobi.Logic.Model.UITheme source)
        {
            Obymobi.Logic.Model.v20.UITheme target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.UITheme();
                target.UIThemeId = source.UIThemeId;
                target.ListCategoryBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListCategoryBackgroundColor);
                target.ListCategoryL2BackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListCategoryL2BackgroundColor);
                target.ListCategoryL3BackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListCategoryL3BackgroundColor);
                target.ListCategoryL4BackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListCategoryL4BackgroundColor);
                target.ListCategoryL5BackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListCategoryL5BackgroundColor);
                target.ListCategoryDividerColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListCategoryDividerColor);
                target.ListCategoryTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListCategoryTextColor);
                target.ListCategorySelectedBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListCategorySelectedBackgroundColor);
                target.ListCategorySelectedDividerColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListCategorySelectedDividerColor);
                target.ListCategorySelectedTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListCategorySelectedTextColor);
                target.ListItemBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListItemBackgroundColor);
                target.ListItemDividerColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListItemDividerColor);
                target.ListItemTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListItemTextColor);
                target.ListItemSelectedBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListItemSelectedBackgroundColor);
                target.ListItemSelectedDividerColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListItemSelectedDividerColor);
                target.ListItemSelectedTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ListItemSelectedTextColor);
                target.WidgetBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.WidgetBackgroundColor);
                target.WidgetBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.WidgetBorderColor);
                target.WidgetTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.WidgetTextColor);
                target.HeaderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.HeaderColor);
                target.TabBackgroundBottomColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.TabBackgroundBottomColor);
                target.TabBackgroundTopColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.TabBackgroundTopColor);
                target.TabBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.TabBorderColor);
                target.TabDividerColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.TabDividerColor);
                target.TabActiveTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.TabActiveTextColor);
                target.TabInactiveTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.TabInactiveTextColor);
                target.PageBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.PageBackgroundColor);
                target.PageBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.PageBorderColor);
                target.PageTitleTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.PageTitleTextColor);
                target.PageDescriptionTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.PageDescriptionTextColor);
                target.PageErrorTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.PageErrorTextColor);
                target.PagePriceTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.PagePriceTextColor);
                target.PageInstructionsTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.PageInstructionsTextColor);
                target.PageDividerTopColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.PageDividerTopColor);
                target.PageDividerBottomColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.PageDividerBottomColor);
                target.PageFooterColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.PageFooterColor);
                target.FooterBackgroundBottomColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.FooterBackgroundBottomColor);
                target.FooterBackgroundTopColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.FooterBackgroundTopColor);
                target.FooterDividerColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.FooterDividerColor);
                target.FooterTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.FooterTextColor);
                target.FooterButtonBackgroundTopColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.FooterButtonBackgroundTopColor);
                target.FooterButtonBackgroundBottomColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.FooterButtonBackgroundBottomColor);
                target.FooterButtonBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.FooterButtonBorderColor);
                target.FooterButtonTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.FooterButtonTextColor);
                target.FooterButtonDisabledBackgroundTopColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.FooterButtonDisabledBackgroundTopColor);
                target.FooterButtonDisabledBackgroundBottomColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.FooterButtonDisabledBackgroundBottomColor);
                target.FooterButtonDisabledBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.FooterButtonDisabledBorderColor);
                target.FooterButtonDisabledTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.FooterButtonDisabledTextColor);
                target.ButtonBackgroundTopColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ButtonBackgroundTopColor);
                target.ButtonBackgroundBottomColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ButtonBackgroundBottomColor);
                target.ButtonBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ButtonBorderColor);
                target.ButtonTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ButtonTextColor);
                target.ButtonPositiveBackgroundTopColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ButtonPositiveBackgroundTopColor);
                target.ButtonPositiveBackgroundBottomColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ButtonPositiveBackgroundBottomColor);
                target.ButtonPositiveBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ButtonPositiveBorderColor);
                target.ButtonPositiveTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ButtonPositiveTextColor);
                target.ButtonDisabledBackgroundTopColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ButtonDisabledBackgroundTopColor);
                target.ButtonDisabledBackgroundBottomColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ButtonDisabledBackgroundBottomColor);
                target.ButtonDisabledBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ButtonDisabledBorderColor);
                target.ButtonDisabledTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.ButtonDisabledTextColor);
                target.DialogBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.DialogBackgroundColor);
                target.DialogBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.DialogBorderColor);
                target.DialogPanelBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.DialogPanelBackgroundColor);
                target.DialogPanelBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.DialogPanelBorderColor);
                target.DialogTitleTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.DialogTitleTextColor);
                target.DialogPrimaryTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.DialogPrimaryTextColor);
                target.DialogSecondaryTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.DialogSecondaryTextColor);
                target.TextboxBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.TextboxBackgroundColor);
                target.TextboxBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.TextboxBorderColor);
                target.TextboxTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.TextboxTextColor);
                target.TextboxCursorColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.TextboxCursorColor);
                target.SpinnerBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.SpinnerBackgroundColor);
                target.SpinnerBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.SpinnerBorderColor);
                target.SpinnerTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.SpinnerTextColor);
                target.RoomControlButtonOuterBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlButtonOuterBorderColor);
                target.RoomControlButtonInnerBorderTopColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlButtonInnerBorderTopColor);
                target.RoomControlButtonInnerBorderBottomColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlButtonInnerBorderBottomColor);
                target.RoomControlButtonBackgroundTopColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlButtonBackgroundTopColor);
                target.RoomControlButtonBackgroundBottomColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlButtonBackgroundBottomColor);
                target.RoomControlDividerTopColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlDividerTopColor);
                target.RoomControlDividerBottomColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlDividerBottomColor);
                target.RoomControlButtonTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlButtonTextColor);
                target.RoomControlTitleColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlTitleColor);
                target.RoomControlTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlTextColor);
                target.RoomControlPageBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlPageBackgroundColor);
                target.RoomControlSpinnerBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlSpinnerBorderColor);
                target.RoomControlSpinnerBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlSpinnerBackgroundColor);
                target.RoomControlSpinnerTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlSpinnerTextColor);
                target.RoomControlButtonIndicatorColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlButtonIndicatorColor);
                target.RoomControlButtonIndicatorBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlButtonIndicatorBackgroundColor);
                target.RoomControlHeaderButtonBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlHeaderButtonBackgroundColor);
                target.RoomControlHeaderButtonSelectorColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlHeaderButtonSelectorColor);
                target.RoomControlHeaderButtonTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlHeaderButtonTextColor);
                target.RoomControlStationNumberColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlStationNumberColor);
                target.RoomControlSliderMinColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlSliderMinColor);
                target.RoomControlSliderMaxColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlSliderMaxColor);
                target.RoomControlToggleOnTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlToggleOnTextColor);
                target.RoomControlToggleOffTextColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlToggleOffTextColor);
                target.RoomControlToggleBackgroundColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlToggleBackgroundColor);
                target.RoomControlToggleBorderColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlToggleBorderColor);
                target.RoomControlThermostatComponentColor = UIThemeHelper.GetCustomColorOrDefault(source, UIColorType.RoomControlThermostatComponentColor);

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v20.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.UITheme ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.UITheme source)
        {
            Obymobi.Logic.Model.UITheme target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UITheme();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.UIThemeConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
