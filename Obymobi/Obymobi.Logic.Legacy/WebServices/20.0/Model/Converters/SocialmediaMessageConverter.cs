namespace Obymobi.Logic.Model.v20.Converters
{
	public class SocialmediaMessageConverter : ModelConverterBase<Obymobi.Logic.Model.v20.SocialmediaMessage, Obymobi.Logic.Model.SocialmediaMessage>
	{
        public SocialmediaMessageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.SocialmediaMessageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.SocialmediaMessage ConvertModelToLegacyModel(Obymobi.Logic.Model.SocialmediaMessage source)
        {
            Obymobi.Logic.Model.v20.SocialmediaMessage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.SocialmediaMessage();
                target.SocialmediaMessageId = source.SocialmediaMessageId;
                target.CompanyId = source.CompanyId;
                target.Message = source.Message;
                target.PostedBy = source.PostedBy;
                target.PostedOn = source.PostedOn;
                target.SocialmediaType = source.SocialmediaType;

                if (source.SocialmediaMessagePublications != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v20.Converters.SocialmediaMessagePublicationConverter socialmediaMessagePublicationsConverter = new Obymobi.Logic.Model.v20.Converters.SocialmediaMessagePublicationConverter();
                    target.SocialmediaMessagePublications = (SocialmediaMessagePublication[])socialmediaMessagePublicationsConverter.ConvertArrayToLegacyArray(source.SocialmediaMessagePublications);
                }

                target.CreatedOn = source.CreatedOn;
                target.PostedByDeliverypoint = source.PostedByDeliverypoint;
            }

            return target;
        }

        public override Obymobi.Logic.Model.SocialmediaMessage ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.SocialmediaMessage source)
        {
            Obymobi.Logic.Model.SocialmediaMessage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SocialmediaMessage();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.SocialmediaMessageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
