using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v20.Converters
{
	public class VenueCategoryConverter : ModelConverterBase<Obymobi.Logic.Model.v20.VenueCategory, Obymobi.Logic.Model.VenueCategory>
	{
        public VenueCategoryConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v21.Converters.VenueCategoryConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v20.VenueCategory ConvertModelToLegacyModel(Obymobi.Logic.Model.VenueCategory source)
        {
            Obymobi.Logic.Model.v20.VenueCategory target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v20.VenueCategory();
                target.VenueCategoryId = source.VenueCategoryId;
                target.Name = source.Name;

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<VenueCategoryLanguage> languages = new System.Collections.Generic.List<VenueCategoryLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        VenueCategoryLanguage model = new VenueCategoryLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.VenueCategoryName);

                        languages.Add(model);
                    }

                    target.VenueCategoryLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.VenueCategory ConvertLegacyModelToModel(Obymobi.Logic.Model.v20.VenueCategory source)
        {
            Obymobi.Logic.Model.VenueCategory target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.VenueCategory();

                // Copy default values from new version
                new Obymobi.Logic.Model.v21.Converters.VenueCategoryConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
