using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v20
{
    /// <summary>
    /// Model class which represents a company for the mobile solution
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "PageElement"), 
    IncludeInCodeGeneratorForXamarin,
    IncludeInCodeGeneratorForAndroid]
    public class PageElement : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Company type
        /// </summary>
        public PageElement()
        {
        }

        #endregion

        #region Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int PageElementId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int PageId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public string SystemName
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public string LanguageCode
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int PageElementType
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int PageType
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public string StringValue1 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public string StringValue2 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public string StringValue3 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public string StringValue4 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public string StringValue5 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int? IntValue1 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int? IntValue2 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int? IntValue3 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int? IntValue4 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int? IntValue5 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public bool? BoolValue1 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public bool? BoolValue2 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public bool? BoolValue3 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public bool? BoolValue4 { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public bool? BoolValue5 { get; set; }

        /// <summary>
        /// Gets or sets the alterations of the product
        /// </summary>
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        public Media[] Media
        { get; set; }


        #endregion

        #region Methods

        public PageElement Clone()
        {
            return this.Clone<PageElement>();
        }

        #endregion
    }
}
