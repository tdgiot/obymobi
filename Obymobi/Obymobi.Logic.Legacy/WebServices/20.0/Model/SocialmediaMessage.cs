using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v20
{
    /// <summary>
    /// Model class which represents a social media message item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "SocialmediaMessage"), IncludeInCodeGeneratorForMobile]
    public class SocialmediaMessage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.SocialmediaMessage type
        /// </summary>
        public SocialmediaMessage()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the social media message item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public int SocialmediaMessageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company this message belongs to
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets message text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Message
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the poster
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string PostedBy
        { get; set; }

        /// <summary>
        /// Gets or sets the post date/time 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PostedOn
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company this message belongs to
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SocialmediaType
        { get; set; }

        /// <summary>
        /// Gets or sets the publications
        /// </summary>
        [XmlArray("SocialmediaMessagePublications")]
        [XmlArrayItem("SocialmediaMessagePublication")]
        [IncludeInCodeGeneratorForAndroid]
        public SocialmediaMessagePublication[] SocialmediaMessagePublications
        { get; set; }

        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        /// <value>
        /// The created on.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string CreatedOn 
        { get; set; }

        /// <summary>
        /// Gets or sets the posted by deliverypoint.
        /// </summary>
        /// <value>
        /// The posted by deliverypoint.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PostedByDeliverypoint 
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public SocialmediaMessage Clone()
        {
            return this.Clone<SocialmediaMessage>();
        }

        #endregion
    }
}
