using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v20
{
	/// <summary>
	/// Model class which represents a company
	/// </summary>
	[Serializable, XmlRootAttribute(ElementName = "ReiswijzerAdviceSummary"), IncludeInCodeGeneratorForFlex]
	public class ReiswijzerAdviceSummary
	{
		#region Constructors

		/// <summary>
		/// Constructs an instance of the Obymobi.Logic.Model.Company type
		/// </summary>
		public ReiswijzerAdviceSummary()
		{
		}

		#endregion

		#region Properties

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        /// <value>
        /// The reiswijzer advice summary id.
        /// </value>
		[XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
		public int ReiswijzerAdviceSummaryId
		{ get; set; }

        /// <summary>
        /// Gets or sets from place.
        /// </summary>
        /// <value>
        /// From place.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string FromPlace
		{ get; set; }

        /// <summary>
        /// Gets or sets from street.
        /// </summary>
        /// <value>
        /// From street.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string FromStreet
		{ get; set; }

        /// <summary>
        /// Gets or sets the via place.
        /// </summary>
        /// <value>
        /// The via place.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ViaPlace
		{ get; set; }

        /// <summary>
        /// Gets or sets the via street.
        /// </summary>
        /// <value>
        /// The via street.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ViaStreet
		{ get; set; }

        /// <summary>
        /// Gets or sets to place.
        /// </summary>
        /// <value>
        /// To place.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ToPlace
		{ get; set; }

        /// <summary>
        /// Gets or sets to street.
        /// </summary>
        /// <value>
        /// To street.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ToStreet
		{ get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string Date
		{ get; set; }

        /// <summary>
        /// Gets or sets the reiswijzer route summaries.
        /// </summary>
        /// <value>
        /// The reiswijzer route summaries.
        /// </value>
		[XmlArray("ReiswijzerRouteSummaries")]
		[XmlArrayItem("ReiswijzerRouteSummaries")]
        [IncludeInCodeGeneratorForFlex]
		public ReiswijzerRouteSummary[] ReiswijzerRouteSummaries
		{ get; set; }

		#endregion
	}
}
