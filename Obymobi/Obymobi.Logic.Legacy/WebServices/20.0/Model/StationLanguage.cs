using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v20
{
    /// <summary>
    /// Model class which represents a stationLanguage
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "StationLanguage"), IncludeInCodeGeneratorForAndroid]
    public class StationLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.StationLanguage type
        /// </summary>
        public StationLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the StationLanguage
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int StationLanguageId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }
        
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LanguageCode
        { get; set; }        

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string SuccessMessage
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Description
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public StationLanguage Clone()
        {
            return this.Clone<StationLanguage>();
        }

        #endregion
    }
}
