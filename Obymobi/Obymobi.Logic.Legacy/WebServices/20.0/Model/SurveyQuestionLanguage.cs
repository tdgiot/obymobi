using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v20
{
    /// <summary>
    /// Model class which represents a surveyQuestion language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "SurveyQuestionLanguage")]
    public class SurveyQuestionLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.SurveyQuestionLanguage type
        /// </summary>
        public SurveyQuestionLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the surveyQuestion language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyQuestionLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the surveyQuestion
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyQuestionId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the surveyQuestion
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Question
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public SurveyQuestionLanguage Clone()
        {
            return this.Clone<SurveyQuestionLanguage>();
        }

        #endregion
    }
}
