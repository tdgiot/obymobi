using Newtonsoft.Json;

namespace Obymobi.Logic.Model.v20
{
	/// <summary>
	/// 
	/// </summary>
	[JsonObject(MemberSerialization.OptIn)]
    public class ClientInfo
	{
	    #region Properties

        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        /// <value>
        /// The client id.
        /// </value>
		[JsonProperty]
		public int ClientId { get; set; }

        /// <summary>
        /// Gets or sets the deliverypoint id.
        /// </summary>
        /// <value>
        /// The deliverypoint id.
        /// </value>
		[JsonProperty]
		public int DeliverypointId { get; set; }

        /// <summary>
        /// Gets or sets the operation mode.
        /// </summary>
        /// <value>
        /// The operation mode.
        /// </value>
		[JsonProperty]
		public int OperationMode { get; set; }

		#endregion
	}
}
