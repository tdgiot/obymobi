using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v20
{
    /// <summary>
    /// Model class which represents a category or entertainment announcement action
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "AnnouncementAction"), IncludeInCodeGeneratorForAndroid]
    public class AnnouncementAction : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.AnnouncementAction type
        /// </summary>
        public AnnouncementAction()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the action type (AnnouncementActionType)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the category id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int CategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the entertainment id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the product id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public String Name
        { get; set; }

        #endregion
    }
}
