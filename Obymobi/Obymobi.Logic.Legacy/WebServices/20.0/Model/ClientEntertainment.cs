using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v20
{
    /// <summary>
    /// Model class which represents a client-entertainment link
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "ClientEntertainment"), IncludeInCodeGeneratorForAndroid]
    public class ClientEntertainment : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.ClientEntertainment type
        /// </summary>
        public ClientEntertainment()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ClientId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the entertainment
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentId
        { get; set; }
    
        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public ClientEntertainment Clone()
        {
            return this.Clone<ClientEntertainment>();
        }

        #endregion
    }
}
