using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.Mobile.v20
{
    /// <summary>
    /// Model class which represents a company for the mobile solution
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "SiteLanguage"), IncludeInCodeGeneratorForXamarin]
    public class SiteLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Company type
        /// </summary>
        public SiteLanguage()
        {
        }

        #endregion

        #region Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int SiteLanguageId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SiteId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        #endregion

        #region Methods

        public SiteLanguage Clone()
        {
            return this.Clone<SiteLanguage>();
        }

        public SiteLanguage CloneUsingBinaryFormatter()
        {
            return this.CloneUsingBinaryFormatter<SiteLanguage>();
        }

        #endregion
    }
}
