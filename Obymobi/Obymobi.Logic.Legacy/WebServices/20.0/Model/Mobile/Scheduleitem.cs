using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v20
{
    /// <summary>
    /// Model class which represents a schedule item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Scheduleitem"), IncludeInCodeGeneratorForXamarin]
    public class Scheduleitem : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Scheduleitem type
        /// </summary>
        public Scheduleitem()
        {
        }        

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the schedule item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int ScheduleitemId
        { get; set; }

        /// <summary>
        /// Gets or sets the day of week
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int DayOfweek
        { get; set; }

        /// <summary>
        /// Gets or sets the start time of the schedule item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string TimeStart
        { get; set; }

        /// <summary>
        /// Gets or sets the end time of the schedule item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string TimeEnd
        { get; set; }     

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Scheduleitem Clone()
        {
            return this.Clone<Scheduleitem>();
        }

        #endregion
    }
}
