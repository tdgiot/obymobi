using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v20.Converters
{
	public class ScheduleConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v20.Schedule, Obymobi.Logic.Model.Mobile.Schedule>
	{
        public ScheduleConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v21.Converters.ScheduleConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v20.Schedule ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Schedule source)
        {
            Obymobi.Logic.Model.Mobile.v20.Schedule target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v20.Schedule();
                target.ScheduleId = source.ScheduleId;
                target.Name = source.Name;

                if (source.Scheduleitems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v20.Converters.ScheduleitemConverter scheduleitemsConverter = new Obymobi.Logic.Model.Mobile.v20.Converters.ScheduleitemConverter();
                    target.Scheduleitems = (Scheduleitem[])scheduleitemsConverter.ConvertArrayToLegacyArray(source.Scheduleitems);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Schedule ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v20.Schedule source)
        {
            Obymobi.Logic.Model.Mobile.Schedule target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Schedule();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v21.Converters.ScheduleConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
