using Obymobi.Logic.Mobile;
using System.Linq;
using Dionysos;
namespace Obymobi.Logic.Model.Mobile.v20.Converters
{
	public class CategoryConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v20.Category, Obymobi.Logic.Model.Mobile.Category>
	{
        public CategoryConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v21.Converters.CategoryConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v20.Category ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Category source)
        {
            Obymobi.Logic.Model.Mobile.v20.Category target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v20.Category();
                target.CategoryId = source.CategoryId;
                target.ParentCategoryId = source.ParentCategoryId;
                target.Name = source.Name;
                target.Description = source.Description;
                target.SortOrder = source.SortOrder;
                target.Geofencing = source.Geofencing;
                target.ScheduleId = source.ScheduleId;

                if (source.Categories != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v20.Converters.CategoryConverter categoriesConverter = new Obymobi.Logic.Model.Mobile.v20.Converters.CategoryConverter();
                    target.Categories = (Category[])categoriesConverter.ConvertArrayToLegacyArray(source.Categories);
                }

                if (source.Products != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v20.Converters.ProductConverter productsConverter = new Obymobi.Logic.Model.Mobile.v20.Converters.ProductConverter();
                    target.Products = (Product[])productsConverter.ConvertArrayToLegacyArray(source.Products);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v20.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v20.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }                

                target.Type = source.Type;
                target.HidePrices = source.HidePrices;

                if (source.CategorySuggestions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v20.Converters.CategorySuggestionConverter categorySuggestionsConverter = new Obymobi.Logic.Model.Mobile.v20.Converters.CategorySuggestionConverter();
                    target.CategorySuggestions = (CategorySuggestion[])categorySuggestionsConverter.ConvertArrayToLegacyArray(source.CategorySuggestions);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<CategoryLanguage> languages = new System.Collections.Generic.List<CategoryLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        CategoryLanguage model = new CategoryLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.CategoryLanguageId = -1;
                        model.CategoryId = source.CategoryId;
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.CategoryName);
                        model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.CategoryDescription);

                        languages.Add(model);
                    }

                    target.CategoryLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Category ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v20.Category source)
        {
            Obymobi.Logic.Model.Mobile.Category target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Category();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v21.Converters.CategoryConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
