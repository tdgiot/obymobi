using Obymobi.Logic.Mobile;
using System.Linq;
using Dionysos;
namespace Obymobi.Logic.Model.Mobile.v20.Converters
{
	public class UITabConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v20.UITab, Obymobi.Logic.Model.Mobile.UITab>
	{
        public UITabConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v21.Converters.UITabConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v20.UITab ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.UITab source)
        {
            Obymobi.Logic.Model.Mobile.v20.UITab target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v20.UITab();
                target.UITabId = source.UITabId;
                target.UIModeId = source.UIModeId;
                target.Caption = source.Caption;
                target.Type = source.Type;
                target.CategoryId = source.CategoryId;
                target.EntertainmentId = source.EntertainmentId;
                target.SiteId = source.SiteId;
                target.URL = source.URL;
                target.Zoom = source.Zoom;
                target.SortOrder = source.SortOrder;
                target.Visible = source.Visible;

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<UITabLanguage> languages = new System.Collections.Generic.List<UITabLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        UITabLanguage model = new UITabLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.UITabLanguageId = -1;
                        model.Caption = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.UITabCaption);
                        model.URL = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.UITabUrl);
                        model.Zoom = source.Zoom;

                        languages.Add(model);
                    }

                    target.UITabLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.UITab ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v20.UITab source)
        {
            Obymobi.Logic.Model.Mobile.UITab target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.UITab();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v21.Converters.UITabConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
