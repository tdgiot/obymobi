using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v20.Converters
{
	public class AccessCodeConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v20.AccessCode, Obymobi.Logic.Model.Mobile.AccessCode>
	{
        public AccessCodeConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v21.Converters.AccessCodeConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v20.AccessCode ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.AccessCode source)
        {
            Obymobi.Logic.Model.Mobile.v20.AccessCode target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v20.AccessCode();
                target.AccessCodeId = source.AccessCodeId;
                target.Code = source.Code;
                target.Type = source.Type;
                target.AnalyticsEnabled = source.AnalyticsEnabled;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.AccessCode ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v20.AccessCode source)
        {
            Obymobi.Logic.Model.Mobile.AccessCode target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.AccessCode();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v21.Converters.AccessCodeConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
