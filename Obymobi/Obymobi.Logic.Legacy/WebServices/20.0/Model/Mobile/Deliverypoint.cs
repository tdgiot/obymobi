using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v20
{
    /// <summary>
    /// Model class which represents a deliverypoint
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Deliverypoint"), IncludeInCodeGeneratorForXamarin]
    public class Deliverypoint : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Deliverypoint type
        /// </summary>
        public Deliverypoint()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the deliverypoint
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int DeliverypointId
        { get; set; }

        /// <summary>
        /// Gets or sets the number of the deliverypoint
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForXamarin]
        public int Number
        { get; set; }

        /// <summary>
        /// Gets or sets the company id of the deliverypoint
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the deliverypoint
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the deliverypointgroup id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int DeliverypointgroupId
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Deliverypoint Clone()
        {
            return this.Clone<Deliverypoint>();
        }

        #endregion
    }
}
