using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v20
{
    /// <summary>
    /// Model class which represents a payment method
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "OrderHour"), IncludeInCodeGeneratorForXamarin]
    public class OrderHour : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Paymentmethod type
        /// </summary>
        public OrderHour()
        {
        }        

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the payment method
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int OrderHourId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int DayOfweek
        { get; set; }

        /// <summary>
        /// Gets or sets the the payment method type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string TimeStart
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string TimeEnd
        { get; set; }     

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Paymentmethod Clone()
        {
            return this.Clone<Paymentmethod>();
        }

        #endregion
    }
}
