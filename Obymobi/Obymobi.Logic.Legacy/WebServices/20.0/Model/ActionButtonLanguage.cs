using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v20
{
    /// <summary>
    /// Model class which represents an action button language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "ActionButtonLanguage"), IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class ActionButtonLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.ActionButtonLanguage type
        /// </summary>
        public ActionButtonLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the action button
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public ActionButtonLanguage Clone()
        {
            return this.Clone<ActionButtonLanguage>();
        }

        #endregion
    }
}
