using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents an survey
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "SurveyQuestion"), IncludeInCodeGeneratorForFlex]
    public class SurveyQuestion : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.SurveyQuestion type
        /// </summary>
        public SurveyQuestion()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the survey question
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyQuestionId
        { get; set; }

        /// <summary>
        /// Gets or sets the question
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public string Question
        { get; set; }

        /// <summary>
        /// Gets or sets the type of question (multiple choice, open)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets if the answer is required
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Required
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of the question
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the parent question
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int ParentSurveyQuestionId
        { get; set; }

        /// <summary>
        /// Gets or sets the notes enabled
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public bool NotesEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets the field value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue10
        { get; set; }
        
        /// <summary>
        /// Gets or sets the answers of the survey question
        /// </summary>
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [XmlArray("SurveyAnswers")]
        [XmlArrayItem("SurveyAnswer")]
        public SurveyAnswer[] SurveyAnswers
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        [XmlArray("SurveyQuestionLanguages")]
        [XmlArrayItem("SurveyQuestionLanguage")]
        public SurveyQuestionLanguage[] SurveyQuestionLanguages
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public SurveyQuestion Clone()
        {
            return this.Clone<SurveyQuestion>();
        }

        #endregion
    }
}
