using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
	/// <summary>
	/// Model class which represents a company
	/// </summary>
	[Serializable, XmlRootAttribute(ElementName = "ReiswijzerSolution"), IncludeInCodeGeneratorForFlex]
	public class ReiswijzerSolution
	{
		#region Constructors

		/// <summary>
		/// Constructs an instance of the Obymobi.Logic.Model.Company type
		/// </summary>
		public ReiswijzerSolution()
		{
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the id of the company
		/// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
		public int ReiswijzerSolutionId
		{ get; set; }

        /// <summary>
        /// Gets or sets the arrival date.
        /// </summary>
        /// <value>
        /// The arrival date.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ArrivalDate
		{ get; set; }

        /// <summary>
        /// Gets or sets the arrival time.
        /// </summary>
        /// <value>
        /// The arrival time.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ArrivalTime
		{ get; set; }

        /// <summary>
        /// Gets or sets the departure date.
        /// </summary>
        /// <value>
        /// The departure date.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string DepartureDate
		{ get; set; }

        /// <summary>
        /// Gets or sets the departure time.
        /// </summary>
        /// <value>
        /// The departure time.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string DepartureTime
		{ get; set; }

        /// <summary>
        /// Gets or sets the travel time.
        /// </summary>
        /// <value>
        /// The travel time.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string TravelTime
		{ get; set; }

        /// <summary>
        /// Gets or sets the interchanges.
        /// </summary>
        /// <value>
        /// The interchanges.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public int Interchanges
		{ get; set; }

        /// <summary>
        /// Gets or sets the name of from stop.
        /// </summary>
        /// <value>
        /// The name of from stop.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string FromStopName
		{ get; set; }

        /// <summary>
        /// Gets or sets the name of to stop.
        /// </summary>
        /// <value>
        /// The name of to stop.
        /// </value>
		[XmlElement]
        [IncludeInCodeGeneratorForFlex]
		public string ToStopName
		{ get; set; }

        /// <summary>
        /// Gets or sets the reiswijzer stops.
        /// </summary>
        /// <value>
        /// The reiswijzer stops.
        /// </value>
		[XmlArray("ReiswijzerStops")]
		[XmlArrayItem("ReiswijzerStops")]
        [IncludeInCodeGeneratorForFlex]
		public ReiswijzerStop[] ReiswijzerStops
		{ get; set; }

		#endregion
	}
}
