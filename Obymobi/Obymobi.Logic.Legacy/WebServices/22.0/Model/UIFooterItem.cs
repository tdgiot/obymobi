using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    [Serializable, XmlRootAttribute(ElementName = "UIFooterItem"), IncludeInCodeGeneratorForAndroid]
    public class UIFooterItem : ModelBase
    {
        [XmlElement, PrimaryKeyFieldOfModel, IncludeInCodeGeneratorForAndroid]
        public int UIFooterItemId { get; set; }

        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public string Name { get; set; }

        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public int Type { get; set; }

        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public int Position { get; set; }

        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public int SortOrder { get; set; }

        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public int ActionIntent { get; set; }

        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public bool Visible { get; set; }

        [XmlArray("UIFooterItemLanguage"), XmlArrayItem("UIFooterItemLanguage"), IncludeInCodeGeneratorForAndroid]
        public UIFooterItemLanguage[] UIFooterItemLanguages { get; set; }

        [XmlArray("Media"), XmlArrayItem("Media"), IncludeInCodeGeneratorForAndroid]
        public Media[] Media { get; set; }
    }
}