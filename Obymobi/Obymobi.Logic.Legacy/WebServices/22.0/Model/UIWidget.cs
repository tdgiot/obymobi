using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents a UIWidget item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "UIWidget"), IncludeInCodeGeneratorForAndroid]
    public class UIWidget : ModelBase
    {
        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the widget
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int UIWidgetId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the UITab this widget belongs to
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int UITabId
        { get; set; }

        /// <summary>
        /// UIWidget name, used for Analytics
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the caption of the widget
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Caption
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the widget
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of the widget
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets the advertisement id of the advertisement linked to the widget
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AdvertisementId
        { get; set; }

        /// <summary>
        /// Gets or sets the product id of the product category linked to the widget
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the category id of the product category linked to the widget
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int CategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the entertainment id of the entertainment linked to the widget
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the entertainment category id of the entertainment category linked to the widget
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentcategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the site id of the site linked to the widget
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SiteId
        { get; set; }

        /// <summary>
        /// Gets or sets the page id of the page linked to the widget
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PageId
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the tab linked to the widget
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int UITabType
        { get; set; }

        /// <summary>
        /// Gets or sets the url of the url linked to the widget
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Url
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlSectionType { get; set; }

        /// <summary>
        /// Gets or sets fieldvalue
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets fieldvalue
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets fieldvalue
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets fieldvalue
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets fieldvalue
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets message layout type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MessageLayoutType
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [XmlArray("UIWidgetLanguages")]
        [XmlArrayItem("UIWidgetLanguage")]
        [IncludeInCodeGeneratorForAndroid]
        public UIWidgetLanguage[] UIWidgetLanguages
        { get; set; }

        /// <summary>
        /// Gets or sets the timers
        /// </summary>
        [XmlArray("UIWidgetTimers")]
        [XmlArrayItem("UIWidgetTimer")]
        [IncludeInCodeGeneratorForAndroid]
        public UIWidgetTimer[] UIWidgetTimers
        { get; set; }

        /// <summary>
        /// Gets or sets the availabilities
        /// </summary>
        [XmlArray("UIWidgetAvailabilities")]
        [XmlArrayItem("UIWidgetAvailability")]
        [IncludeInCodeGeneratorForAndroid]
        public UIWidgetAvailability[] UIWidgetAvailabilitys
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the widget
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UIWidget Clone()
        {
            return this.Clone<UIWidget>();
        }

        #endregion
    }
}
