using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents a media item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Media"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class Media : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Media type
        /// </summary>
        public Media()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Media type using the
        /// </summary>
        public Media(int MediaId, string Name, string FilePathRelativeToMediaPath, int MediaType)
        {
            this.MediaId = MediaId;
            this.Name = Name;
            this.FilePathRelativeToMediaPath = FilePathRelativeToMediaPath;
            this.MediaType = MediaType;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the media item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int MediaId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the filepath relative to the media path
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        //[Obsolete("Is replaced by CdnFilePathRelativeToMediaPath, but still in use for backwards compatability")]
        public string FilePathRelativeToMediaPath
        { get; set; }

        /// <summary>
        /// Gets or sets the filepath relative to the media path
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string CdnFilePathRelativeToMediaPath
        { get; set; }

        /// <summary>
        /// Gets or sets if tit's a generic file (not company specific)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool GenericFile
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the media item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int MediaType
        { get; set; }

        /// <summary>
        /// Gets or sets the ratios of the media item
        /// </summary>
        [XmlArray("MediaRatioTypeMedias")]
        [XmlArrayItem("MediaRatioTypeMedia")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public MediaRatioTypeMedia[] MediaRatioTypeMedias
        { get; set; }

        /// <summary>
        /// Parent of this media
        /// </summary>
        [XmlAttribute("Parent")]
        public string Parent 
        { get; set; }

        /// <summary>
        /// Gets or sets the action product id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the action category id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionCategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the action entertainment id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionEntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the action entertainmentcategory id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ActionEntertainmentcategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the action url
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ActionUrl
        { get; set; }

        /// <summary>
        /// Linked media id for when image is linked to specific company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AgnosticMediaId { get; set; }

        /// <summary>
        /// Gets or sets the size mode
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SizeMode
        { get; set; }

        /// <summary>
        /// Gets or sets the zoom level
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ZoomLevel
        { get; set; }
        
        /// <summary>
        /// Gets or sets the action site id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public int ActionSiteId
        { get; set; }

        /// <summary>
        /// Gets or sets the action page id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public int ActionPageId
        { get; set; }

        /// <summary>
        /// Array of languages this image can be used for. If none, image is used as language agnotic
        /// </summary>
        [XmlArray("MediaLanguages")]
        [XmlArrayItem("MediaLanguage")]
        [IncludeInCodeGeneratorForAndroid]
        public MediaLanguage[] MediaLanguages { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Media Clone()
        {
            return this.Clone<Media>();
        }

        #endregion

    }
}
