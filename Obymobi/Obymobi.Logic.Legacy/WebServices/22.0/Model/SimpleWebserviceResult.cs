using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Generic Webservice Result for methods only required to return a bool
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "SimpleWebserviceResult"), IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForFlex]
    public class SimpleWebserviceResult : ModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleWebserviceResult"/> class.
        /// </summary>
        public SimpleWebserviceResult()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleWebserviceResult"/> class.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        public SimpleWebserviceResult(bool success)
        {
            this.Succes = success;
        }

        /// <summary>
        /// Gets of sets if the call was succesfull
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForFlex]
        public bool Succes
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public long Ticks { get; set; }

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public SimpleWebserviceResult Clone()
        {
            return this.Clone<SimpleWebserviceResult>();
        }

        #endregion

    }
}
