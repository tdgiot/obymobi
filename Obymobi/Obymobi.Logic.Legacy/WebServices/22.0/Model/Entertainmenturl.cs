using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents an entertainment item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Entertainmenturl"), IncludeInCodeGeneratorForAndroid]
    public class Entertainmenturl : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Entertainment type
        /// </summary>
        public Entertainmenturl()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the entertainment url
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmenturlId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the entertainment
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the url
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Url
        { get; set; }

        /// <summary>
        /// Gets or sets if the whole domain may be accessed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool AccessFullDomain
        { get; set; }

        /// <summary>
        /// Gets or sets the initial zoom level of the browser
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int InitialZoom
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Entertainmenturl Clone()
        {
            return this.Clone<Entertainmenturl>();
        }

        #endregion
    }
}
