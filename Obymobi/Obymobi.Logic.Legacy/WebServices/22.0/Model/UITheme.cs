using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents theme
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "UITheme"), IncludeInCodeGeneratorForAndroid]
    public class UITheme : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.UITheme type
        /// </summary>
        public UITheme()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the theme
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int UIThemeId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListCategoryBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListCategoryL2BackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListCategoryL3BackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListCategoryL4BackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListCategoryL5BackgroundColor
        { get; set; }

         [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListCategoryDividerColor
        { get; set; }

         [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListCategoryTextColor
        { get; set; }

         [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListCategorySelectedBackgroundColor
        { get; set; }

         [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListCategorySelectedDividerColor
        { get; set; }

         [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListCategorySelectedTextColor
        { get; set; }

         [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListItemBackgroundColor
        { get; set; }

         [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListItemDividerColor
        { get; set; }

         [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListItemTextColor
        { get; set; }

         [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListItemSelectedBackgroundColor
        { get; set; }

         [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListItemSelectedDividerColor
        { get; set; }

         [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ListItemSelectedTextColor
        { get; set; }

         [XmlElement]
         [IncludeInCodeGeneratorForAndroid]
         public int ListItemPriceTextColor
         { get; set; }

         [XmlElement]
         [IncludeInCodeGeneratorForAndroid]
         public int ListItemSelectedPriceTextColor
         { get; set; }

         [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int WidgetBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int WidgetBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int WidgetTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int HeaderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TabBackgroundBottomColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TabBackgroundTopColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TabBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TabDividerColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TabActiveTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TabInactiveTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PageBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PageBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PageTitleTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PageDescriptionTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PageErrorTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PagePriceTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PageInstructionsTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PageDividerTopColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PageDividerBottomColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PageFooterColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FooterBackgroundBottomColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FooterBackgroundTopColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FooterDividerColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FooterTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FooterButtonBackgroundTopColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FooterButtonBackgroundBottomColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FooterButtonBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FooterButtonTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FooterButtonDisabledBackgroundTopColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FooterButtonDisabledBackgroundBottomColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FooterButtonDisabledBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FooterButtonDisabledTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ButtonBackgroundTopColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ButtonBackgroundBottomColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ButtonBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ButtonTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ButtonPositiveBackgroundTopColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ButtonPositiveBackgroundBottomColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ButtonPositiveBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ButtonPositiveTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ButtonDisabledBackgroundTopColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ButtonDisabledBackgroundBottomColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ButtonDisabledBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ButtonDisabledTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DialogBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DialogBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DialogPanelBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DialogPanelBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DialogTitleTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DialogPrimaryTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DialogSecondaryTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TextboxBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TextboxBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TextboxTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TextboxCursorColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SpinnerBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SpinnerBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SpinnerTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlButtonOuterBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlButtonInnerBorderTopColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlButtonInnerBorderBottomColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlButtonBackgroundTopColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlButtonBackgroundBottomColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlDividerTopColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlDividerBottomColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlButtonTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlTitleColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlPageBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlSpinnerBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlSpinnerBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlSpinnerTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlButtonIndicatorColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlButtonIndicatorBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlHeaderButtonBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlHeaderButtonSelectorColor
        { get; set; }
    
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlHeaderButtonTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlStationNumberColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlSliderMinColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlSliderMaxColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlToggleOnTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlToggleOffTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlToggleBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlToggleBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlThermostatComponentColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogBorderColor
        { get; set; }        

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogListItemBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogListItemSelectedBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogListItemSelectedBorderColor 
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogPrimaryTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogSecondaryTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogInputBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogInputBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogInputTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogInputHintTextColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogPanelBackgroundColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogPanelBorderColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogRadioButtonColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogCheckBoxColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogCloseButtonColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogListViewArrowColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogListViewDividerColor
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AreaTabActiveText
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AreaTabInactiveText
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the widget
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UITheme Clone()
        {
            return this.Clone<UITheme>();
        }

        #endregion
    }
}
