using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents a UIWidgetTimer item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "UIWidgetTimer"), IncludeInCodeGeneratorForAndroid]
    public class UIWidgetTimer : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.UIWidgetTimer type
        /// </summary>
        public UIWidgetTimer()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the widget timer
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int UIWidgetTimerId
        { get; set; }

        /// <summary>
        /// Gets or sets the time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string CountToTime
        { get; set; }

        /// <summary>
        /// Gets or sets the date
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string CountToDate
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UIWidgetTimer Clone()
        {
            return this.Clone<UIWidgetTimer>();
        }

        #endregion
    }
}
