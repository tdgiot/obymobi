using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    [Serializable, XmlRoot(ElementName = "UIFooterItemLanguage"), IncludeInCodeGeneratorForAndroid]
    public class UIFooterItemLanguage : ModelBase
    {
        [XmlElement, PrimaryKeyFieldOfModel, IncludeInCodeGeneratorForAndroid]
        public int UIFooterItemLanguageId { get; set; }

        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public string LanguageCode { get; set; }

        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public string Name { get; set; }
    }
}