using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents a category language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CategoryLanguage"), IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class CategoryLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CategoryLanguage type
        /// </summary>
        public CategoryLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the category language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int CategoryLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int CategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the order processed title for this category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderProcessedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the order processed message for this category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OrderProcessedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets the order confirmation title for this category
        /// </summary>
        [XmlElement]
        public string OrderConfirmationTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the order confirmation message for this category
        /// </summary>
        [XmlElement]
        public string OrderConfirmationMessage
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CategoryLanguage Clone()
        {
            return this.Clone<CategoryLanguage>();
        }

        #endregion
    }
}
