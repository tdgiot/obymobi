using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents a point-of-sale orderitem
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Posorderitem")]
    public class Posorderitem : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Posorderitem type
        /// </summary>
        public Posorderitem()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the orderitem
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int PosorderitemId
        { get; set; }

        /// <summary>
        /// Gets or sets the external id of the pos product
        /// </summary>
        [XmlElement]
        public string PosproductExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the quantity
        /// </summary>
        [XmlElement]
        public int Quantity
        { get; set; }

        /// <summary>
        /// Gets or sets the price of the product
        /// </summary>
        [XmlElement]
        public decimal ProductPriceIn
        { get; set; }

        /// <summary>
        /// Gets or sets the extra description
        /// </summary>
        [XmlElement]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets pos alteration items of the order item
        /// </summary>
        [XmlArray("Posalterationitems")]
        [XmlArrayItem("Posalterationitem")]
        public Posalterationitem[] Posalterationitems
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }


        #endregion

        #region Calculated Properties

        /// <summary>
        /// This is the calculated price for the Posorderitem (Quantity * ProductPriceIn)
        /// </summary>
        public decimal PriceIn
        {
            get
            {
                return this.Quantity * this.ProductPriceIn;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Posorderitem Clone()
        {
            return this.Clone<Posorderitem>();
        }

        #endregion
    }
}
