using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v22.Converters
{
	public class PageConverter : ModelConverterBase<Obymobi.Logic.Model.v22.Page, Obymobi.Logic.Model.Page>
	{
        public PageConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.PageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.Page ConvertModelToLegacyModel(Obymobi.Logic.Model.Page source)
        {
            Obymobi.Logic.Model.v22.Page target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.Page();
                target.PageId = source.PageId;
                target.SiteId = source.SiteId;
                target.Name = source.Name;
                target.ParentPageId = source.ParentPageId;
                target.PageType = source.PageType;
                target.SortOrder = source.SortOrder;                

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v22.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.PageElements != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.PageElementConverter pageElementsConverter = new Obymobi.Logic.Model.v22.Converters.PageElementConverter();
                    target.PageElements = (PageElement[])pageElementsConverter.ConvertArrayToLegacyArray(source.PageElements);
                }

                if (source.Attachments != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.AttachmentConverter attachmentsConverter = new Obymobi.Logic.Model.v22.Converters.AttachmentConverter();
                    target.Attachments = (Attachment[])attachmentsConverter.ConvertArrayToLegacyArray(source.Attachments);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<PageLanguage> languages = new System.Collections.Generic.List<PageLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        PageLanguage model = new PageLanguage();
                        model.PageLanguageId = -1;
                        model.PageId = source.PageId;
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.PageName);

                        languages.Add(model);
                    }

                    target.PageLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Page ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.Page source)
        {
            Obymobi.Logic.Model.Page target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Page();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.PageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
