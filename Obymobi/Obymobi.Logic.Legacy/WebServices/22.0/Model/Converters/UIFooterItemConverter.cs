using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v22.Converters
{
	public class UIFooterItemConverter : ModelConverterBase<Obymobi.Logic.Model.v22.UIFooterItem, Obymobi.Logic.Model.UIFooterItem>
	{
        public UIFooterItemConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.UIFooterItemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.UIFooterItem ConvertModelToLegacyModel(Obymobi.Logic.Model.UIFooterItem source)
        {
            Obymobi.Logic.Model.v22.UIFooterItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.UIFooterItem();
                target.UIFooterItemId = source.UIFooterItemId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.Position = source.Position;
                target.SortOrder = source.SortOrder;
                target.ActionIntent = source.ActionIntent;
                target.Visible = source.Visible;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v22.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<UIFooterItemLanguage> languages = new System.Collections.Generic.List<UIFooterItemLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);
                        
                        UIFooterItemLanguage model = new UIFooterItemLanguage();
                        model.UIFooterItemLanguageId = -1;
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.UIFooterItemName);

                        languages.Add(model);
                    }

                    target.UIFooterItemLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.UIFooterItem ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.UIFooterItem source)
        {
            Obymobi.Logic.Model.UIFooterItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UIFooterItem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.UIFooterItemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
