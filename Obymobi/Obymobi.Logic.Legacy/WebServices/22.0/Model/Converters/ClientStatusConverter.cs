namespace Obymobi.Logic.Model.v22.Converters
{
	public class ClientStatusConverter : ModelConverterBase<Obymobi.Logic.Model.v22.ClientStatus, Obymobi.Logic.Model.ClientStatus>
	{
        public ClientStatusConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.ClientStatusConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.ClientStatus ConvertModelToLegacyModel(Obymobi.Logic.Model.ClientStatus source)
        {
            Obymobi.Logic.Model.v22.ClientStatus target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.ClientStatus();
                target.ClientStatusId = source.ClientStatusId;
                target.Database = source.Database;
                target.ProcessedOrderIds = source.ProcessedOrderIds;
                target.FailedOrderIds = source.FailedOrderIds;
                target.ProcessedOrderitemIds = source.ProcessedOrderitemIds;
                target.FailedOrderitemIds = source.FailedOrderitemIds;
                target.PendingOrderIdsOnEmenu = source.PendingOrderIdsOnEmenu;
                target.ClientStatusCode = source.ClientStatusCode;
                target.ClientOperationMode = source.ClientOperationMode;
                target.ApplicationVersion = source.ApplicationVersion;
                target.OsVersion = source.OsVersion;
                target.RequestToSendLog = source.RequestToSendLog;
                target.Log = source.Log;
                target.IpAddress = source.IpAddress;
                target.BatteryLevel = source.BatteryLevel;
                target.IsCharging = source.IsCharging;
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.DeliverypointId = source.DeliverypointId;
                target.ShowBatteryLowDialog = source.ShowBatteryLowDialog;
                target.TestField = source.TestField;
                target.WifiStrength = source.WifiStrength;
                target.AgentIsRunning = source.AgentIsRunning;
                target.SupportToolsIsRunning = source.SupportToolsIsRunning;
                target.BluetoothKeyboardConnected = source.BluetoothKeyboardConnected;
                target.BluetoothPrinterConnected = source.BluetoothPrinterConnected;
                target.IsDevelopmentDevice = source.IsDevelopmentDevice;
                target.RoomControlConnected = source.RoomControlConnected;
                target.DoNotDisturbActive = source.DoNotDisturbActive;
                target.ServiceRoomActive = source.ServiceRoomActive;
            }

            return target;
        }

        public override Obymobi.Logic.Model.ClientStatus ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.ClientStatus source)
        {
            Obymobi.Logic.Model.ClientStatus target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.ClientStatus();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.ClientStatusConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
