namespace Obymobi.Logic.Model.v22.Converters
{
	public class CompanyDirectoryEntryConverter : ModelConverterBase<Obymobi.Logic.Model.v22.CompanyDirectoryEntry, Obymobi.Logic.Model.CompanyDirectoryEntry>
	{
        public CompanyDirectoryEntryConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.CompanyDirectoryEntryConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.CompanyDirectoryEntry ConvertModelToLegacyModel(Obymobi.Logic.Model.CompanyDirectoryEntry source)
        {
            Obymobi.Logic.Model.v22.CompanyDirectoryEntry target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.CompanyDirectoryEntry();
                target.CompanyId = source.CompanyId;
                target.Name = source.Name;
                target.Latitude = source.Latitude;
                target.Longitude = source.Longitude;
                target.CompanyDataLastModifiedTicks = source.CompanyDataLastModifiedTicks;
                target.CompanyMediaLastModifiedTicks = source.CompanyMediaLastModifiedTicks;
                target.MenuDataLastModifiedTicks = source.MenuDataLastModifiedTicks;
                target.MenuMediaLastModifiedTicks = source.MenuMediaLastModifiedTicks;

                if (source.Deliverypointgroups != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.DeliverypointgroupDirectoryEntryConverter deliverypointgroupsConverter = new Obymobi.Logic.Model.v22.Converters.DeliverypointgroupDirectoryEntryConverter();
                    target.Deliverypointgroups = (DeliverypointgroupDirectoryEntry[])deliverypointgroupsConverter.ConvertArrayToLegacyArray(source.Deliverypointgroups);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.CompanyDirectoryEntry ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.CompanyDirectoryEntry source)
        {
            Obymobi.Logic.Model.CompanyDirectoryEntry target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.CompanyDirectoryEntry();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.CompanyDirectoryEntryConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
