namespace Obymobi.Logic.Model.v22.Converters
{
	public class CustomTextConverter : ModelConverterBase<Obymobi.Logic.Model.v22.CustomText, Obymobi.Logic.Model.CustomText>
	{
        public CustomTextConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CultureCode");
            this.FieldsToExclude.Add("ForeignKey");

            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.CustomTextConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.CustomText ConvertModelToLegacyModel(Obymobi.Logic.Model.CustomText source)
        {
            Obymobi.Logic.Model.v22.CustomText target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.CustomText();
                target.CustomTextId = source.CustomTextId;
                target.LanguageCode = Obymobi.Culture.Mappings[source.CultureCode].Language.CodeAlpha2.ToUpperInvariant();
                target.Type = source.Type;
                target.Text = source.Text;
            }

            return target;
        }

        public override Obymobi.Logic.Model.CustomText ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.CustomText source)
        {
            Obymobi.Logic.Model.CustomText target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.CustomText();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.CustomTextConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
