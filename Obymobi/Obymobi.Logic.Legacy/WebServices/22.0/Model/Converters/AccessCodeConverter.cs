namespace Obymobi.Logic.Model.v22.Converters
{
	public class AccessCodeConverter : ModelConverterBase<Obymobi.Logic.Model.v22.AccessCode, Obymobi.Logic.Model.AccessCode>
	{
        public AccessCodeConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.AccessCodeConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.AccessCode ConvertModelToLegacyModel(Obymobi.Logic.Model.AccessCode source)
        {
            Obymobi.Logic.Model.v22.AccessCode target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.AccessCode();
                target.AccessCodeId = source.AccessCodeId;
                target.Code = source.Code;
                target.Type = source.Type;
                target.AnalyticsEnabled = source.AnalyticsEnabled;
            }

            return target;
        }

        public override Obymobi.Logic.Model.AccessCode ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.AccessCode source)
        {
            Obymobi.Logic.Model.AccessCode target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.AccessCode();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.AccessCodeConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
