using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v22.Converters
{
	public class ProductConverter : ModelConverterBase<Obymobi.Logic.Model.v22.Product, Obymobi.Logic.Model.Product>
	{
        public ProductConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.ProductConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.Product ConvertModelToLegacyModel(Obymobi.Logic.Model.Product source)
        {
            Obymobi.Logic.Model.v22.Product target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.Product();
                target.ProductId = source.ProductId;
                target.GenericproductId = source.GenericproductId;
                target.CategoryId = source.CategoryId;
                target.Name = source.Name;
                target.Description = source.Description;
                target.Type = source.Type;
                target.SubType = source.SubType;
                target.PriceIn = source.PriceIn;
                target.VatPercentage = source.VatPercentage;
                target.TextColor = source.TextColor;
                target.BackgroundColor = source.BackgroundColor;
                target.DisplayOnHomepage = source.DisplayOnHomepage;
                target.SortOrder = source.SortOrder;
                target.Rateable = source.Rateable;
                target.AllowFreeText = source.AllowFreeText;
                target.Color = source.Color;
                target.ButtonText = source.ButtonText;
                target.CustomizeButtonText = source.CustomizeButtonText;
                target.WebTypeTabletUrl = source.WebTypeTabletUrl;
                target.WebTypeSmartphoneUrl = source.WebTypeSmartphoneUrl;
                target.ScheduleId = source.ScheduleId;
                target.ViewLayoutType = source.ViewLayoutType;
                target.DeliveryLocationType = source.DeliveryLocationType;
                target.HidePrice = source.HidePrice;
                target.VisibilityType = source.VisibilityType;

                if (source.Alterations != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.AlterationConverter alterationsConverter = new Obymobi.Logic.Model.v22.Converters.AlterationConverter();
                    target.Alterations = (Alteration[])alterationsConverter.ConvertArrayToLegacyArray(source.Alterations);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v22.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.ProductSuggestions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.ProductSuggestionConverter productSuggestionsConverter = new Obymobi.Logic.Model.v22.Converters.ProductSuggestionConverter();
                    target.ProductSuggestions = (ProductSuggestion[])productSuggestionsConverter.ConvertArrayToLegacyArray(source.ProductSuggestions);
                }

                target.SuggestedProductIds = source.SuggestedProductIds;

                if (source.AdvertisementTags != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.AdvertisementTagConverter advertisementTagsConverter = new Obymobi.Logic.Model.v22.Converters.AdvertisementTagConverter();
                    target.AdvertisementTags = (AdvertisementTag[])advertisementTagsConverter.ConvertArrayToLegacyArray(source.AdvertisementTags);
                }

                if (source.Ratings != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.RatingConverter ratingsConverter = new Obymobi.Logic.Model.v22.Converters.RatingConverter();
                    target.Ratings = (Rating[])ratingsConverter.ConvertArrayToLegacyArray(source.Ratings);
                }                

                if (source.Attachments != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.AttachmentConverter attachmentsConverter = new Obymobi.Logic.Model.v22.Converters.AttachmentConverter();
                    target.Attachments = (Attachment[])attachmentsConverter.ConvertArrayToLegacyArray(source.Attachments);
                }

                if (source.Posproduct != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v22.Converters.PosproductConverter posproductConverter = new Obymobi.Logic.Model.v22.Converters.PosproductConverter();
                    target.Posproduct = (Posproduct)posproductConverter.ConvertModelToLegacyModel(source.Posproduct);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<ProductLanguage> languages = new System.Collections.Generic.List<ProductLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        ProductLanguage model = new ProductLanguage();
                        model.ProductLanguageId = -1;
                        model.ProductId = source.ProductId;
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();                                                
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductName);
                        model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductDescription);
                        model.ButtonText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductButtonText);
                        model.CustomizeButtonText = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductCustomizeButtonText);
                        model.OrderProcessedTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductOrderProcessedTitle);
                        model.OrderProcessedMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductOrderProcessedText);
                        model.OrderConfirmationTitle = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductOrderConfirmationTitle);
                        model.OrderConfirmationMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.ProductOrderConfirmationText);

                        languages.Add(model);
                    }

                    target.ProductLanguages = languages.ToArray();
                }

                target.CategoryIds = source.CategoryIds;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Product ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.Product source)
        {
            Obymobi.Logic.Model.Product target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Product();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.ProductConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
