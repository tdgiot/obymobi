namespace Obymobi.Logic.Model.v22.Converters
{
	public class RoomControlConfigurationConverter : ModelConverterBase<Obymobi.Logic.Model.v22.RoomControlConfiguration, Obymobi.Logic.Model.RoomControlConfiguration>
	{
        public RoomControlConfigurationConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.RoomControlConfigurationConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.RoomControlConfiguration ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomControlConfiguration source)
        {
            Obymobi.Logic.Model.v22.RoomControlConfiguration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.RoomControlConfiguration();
                target.RoomControlConfigurationId = source.RoomControlConfigurationId;
                target.Name = source.Name;

                if (source.RoomControlAreas != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.RoomControlAreaConverter roomControlAreasConverter = new Obymobi.Logic.Model.v22.Converters.RoomControlAreaConverter();
                    target.RoomControlAreas = (RoomControlArea[])roomControlAreasConverter.ConvertArrayToLegacyArray(source.RoomControlAreas);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomControlConfiguration ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.RoomControlConfiguration source)
        {
            Obymobi.Logic.Model.RoomControlConfiguration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomControlConfiguration();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.RoomControlConfigurationConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
