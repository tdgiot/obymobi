using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v22.Converters
{
	public class EntertainmentcategoryConverter : ModelConverterBase<Obymobi.Logic.Model.v22.Entertainmentcategory, Obymobi.Logic.Model.Entertainmentcategory>
	{
        public EntertainmentcategoryConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.EntertainmentcategoryConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.Entertainmentcategory ConvertModelToLegacyModel(Obymobi.Logic.Model.Entertainmentcategory source)
        {
            Obymobi.Logic.Model.v22.Entertainmentcategory target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.Entertainmentcategory();
                target.EntertainmentcategoryId = source.EntertainmentcategoryId;
                target.Name = source.Name;

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<EntertainmentcategoryLanguage> languages = new System.Collections.Generic.List<EntertainmentcategoryLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        EntertainmentcategoryLanguage model = new EntertainmentcategoryLanguage();
                        model.EntertainmentcategoryLanguageId = -1;
                        model.EntertainmentcategoryId = source.EntertainmentcategoryId;
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.EntertainmentcategoryName);

                        languages.Add(model);
                    }

                    target.EntertainmentcategoryLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Entertainmentcategory ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.Entertainmentcategory source)
        {
            Obymobi.Logic.Model.Entertainmentcategory target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Entertainmentcategory();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.EntertainmentcategoryConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
