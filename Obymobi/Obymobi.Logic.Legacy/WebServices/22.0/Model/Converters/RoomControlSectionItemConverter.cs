using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v22.Converters
{
	public class RoomControlSectionItemConverter : ModelConverterBase<Obymobi.Logic.Model.v22.RoomControlSectionItem, Obymobi.Logic.Model.RoomControlSectionItem>
	{
        public RoomControlSectionItemConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.RoomControlSectionItemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.RoomControlSectionItem ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomControlSectionItem source)
        {
            Obymobi.Logic.Model.v22.RoomControlSectionItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.RoomControlSectionItem();
                target.RoomControlSectionItemId = source.RoomControlSectionItemId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.Visible = source.Visible;                

                if (source.StationLists != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.StationListConverter stationListsConverter = new Obymobi.Logic.Model.v22.Converters.StationListConverter();
                    target.StationLists = (StationList[])stationListsConverter.ConvertArrayToLegacyArray(source.StationLists);
                }

                if (source.RoomControlWidgets != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.RoomControlWidgetConverter roomControlWidgetsConverter = new Obymobi.Logic.Model.v22.Converters.RoomControlWidgetConverter();
                    target.RoomControlWidgets = (RoomControlWidget[])roomControlWidgetsConverter.ConvertArrayToLegacyArray(source.RoomControlWidgets);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v22.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<RoomControlSectionItemLanguage> languages = new System.Collections.Generic.List<RoomControlSectionItemLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        RoomControlSectionItemLanguage model = new RoomControlSectionItemLanguage();
                        model.RoomControlSectionItemLanguageId = -1;
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.RoomControlSectionItemName);
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();

                        languages.Add(model);
                    }

                    target.RoomControlSectionItemLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomControlSectionItem ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.RoomControlSectionItem source)
        {
            Obymobi.Logic.Model.RoomControlSectionItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomControlSectionItem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.RoomControlSectionItemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
