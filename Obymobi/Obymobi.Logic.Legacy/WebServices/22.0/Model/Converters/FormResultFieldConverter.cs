namespace Obymobi.Logic.Model.v22.Converters
{
	public class FormResultFieldConverter : ModelConverterBase<Obymobi.Logic.Model.v22.FormResultField, Obymobi.Logic.Model.FormResultField>
	{
        public FormResultFieldConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.FormResultFieldConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.FormResultField ConvertModelToLegacyModel(Obymobi.Logic.Model.FormResultField source)
        {
            Obymobi.Logic.Model.v22.FormResultField target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.FormResultField();
                target.FormResultFieldId = source.FormResultFieldId;
                target.FormResultId = source.FormResultId;
                target.FormFieldId = source.FormFieldId;
                target.Value = source.Value;
            }

            return target;
        }

        public override Obymobi.Logic.Model.FormResultField ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.FormResultField source)
        {
            Obymobi.Logic.Model.FormResultField target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.FormResultField();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.FormResultFieldConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
