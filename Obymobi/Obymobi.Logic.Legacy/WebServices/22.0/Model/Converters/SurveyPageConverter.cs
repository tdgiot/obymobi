namespace Obymobi.Logic.Model.v22.Converters
{
	public class SurveyPageConverter : ModelConverterBase<Obymobi.Logic.Model.v22.SurveyPage, Obymobi.Logic.Model.SurveyPage>
	{
        public SurveyPageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.SurveyPageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.SurveyPage ConvertModelToLegacyModel(Obymobi.Logic.Model.SurveyPage source)
        {
            Obymobi.Logic.Model.v22.SurveyPage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.SurveyPage();
                target.SurveyPageId = source.SurveyPageId;
                target.Name = source.Name;
                target.SortOrder = source.SortOrder;

                if (source.SurveyQuestions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.SurveyQuestionConverter surveyQuestionsConverter = new Obymobi.Logic.Model.v22.Converters.SurveyQuestionConverter();
                    target.SurveyQuestions = (SurveyQuestion[])surveyQuestionsConverter.ConvertArrayToLegacyArray(source.SurveyQuestions);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v22.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.SurveyPage ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.SurveyPage source)
        {
            Obymobi.Logic.Model.SurveyPage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SurveyPage();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.SurveyPageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
