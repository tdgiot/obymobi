using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v22.Converters
{
	public class RoomControlAreaConverter : ModelConverterBase<Obymobi.Logic.Model.v22.RoomControlArea, Obymobi.Logic.Model.RoomControlArea>
	{
        public RoomControlAreaConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.RoomControlAreaConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.RoomControlArea ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomControlArea source)
        {
            Obymobi.Logic.Model.v22.RoomControlArea target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.RoomControlArea();
                target.RoomControlAreaId = source.RoomControlAreaId;
                target.Name = source.Name;
                target.NameSystem = source.NameSystem;
                target.SortOrder = source.SortOrder;
                target.Visible = source.Visible;
                target.Type = source.Type;                

                if (source.RoomControlSections != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.RoomControlSectionConverter roomControlSectionsConverter = new Obymobi.Logic.Model.v22.Converters.RoomControlSectionConverter();
                    target.RoomControlSections = (RoomControlSection[])roomControlSectionsConverter.ConvertArrayToLegacyArray(source.RoomControlSections);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<RoomControlAreaLanguage> languages = new System.Collections.Generic.List<RoomControlAreaLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        RoomControlAreaLanguage model = new RoomControlAreaLanguage();
                        model.RoomControlAreaLanguageId = -1;
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.RoomControlAreaName);
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();                       

                        languages.Add(model);
                    }

                    target.RoomControlAreaLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomControlArea ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.RoomControlArea source)
        {
            Obymobi.Logic.Model.RoomControlArea target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomControlArea();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.RoomControlAreaConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
