namespace Obymobi.Logic.Model.v22.Converters
{
	public class PosalterationConverter : ModelConverterBase<Obymobi.Logic.Model.v22.Posalteration, Obymobi.Logic.Model.Posalteration>
	{
        public PosalterationConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.PosalterationConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.Posalteration ConvertModelToLegacyModel(Obymobi.Logic.Model.Posalteration source)
        {
            Obymobi.Logic.Model.v22.Posalteration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.Posalteration();
                target.PosalterationId = source.PosalterationId;
                target.CompanyId = source.CompanyId;
                target.ExternalId = source.ExternalId;
                target.Name = source.Name;
                target.MinOptions = source.MinOptions;
                target.MaxOptions = source.MaxOptions;

                if (source.Posalterationoptions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.PosalterationoptionConverter posalterationoptionsConverter = new Obymobi.Logic.Model.v22.Converters.PosalterationoptionConverter();
                    target.Posalterationoptions = (Posalterationoption[])posalterationoptionsConverter.ConvertArrayToLegacyArray(source.Posalterationoptions);
                }

                if (source.Posalterationitems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.PosalterationitemConverter posalterationitemsConverter = new Obymobi.Logic.Model.v22.Converters.PosalterationitemConverter();
                    target.Posalterationitems = (Posalterationitem[])posalterationitemsConverter.ConvertArrayToLegacyArray(source.Posalterationitems);
                }

                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Posalteration ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.Posalteration source)
        {
            Obymobi.Logic.Model.Posalteration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Posalteration();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.PosalterationConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
