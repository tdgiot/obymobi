namespace Obymobi.Logic.Model.v22.Converters
{
	public class PaymentmethodConverter : ModelConverterBase<Obymobi.Logic.Model.v22.Paymentmethod, Obymobi.Logic.Model.Paymentmethod>
	{
        public PaymentmethodConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.PaymentmethodConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.Paymentmethod ConvertModelToLegacyModel(Obymobi.Logic.Model.Paymentmethod source)
        {
            Obymobi.Logic.Model.v22.Paymentmethod target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.Paymentmethod();
                target.PaymentmethodId = source.PaymentmethodId;
                target.Name = source.Name;
                target.PaymentmethodType = source.PaymentmethodType;
                target.CompanyId = source.CompanyId;
                target.ThankTitle = source.ThankTitle;
                target.ThankMessage = source.ThankMessage;
                target.MinimumLimit = source.MinimumLimit;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Paymentmethod ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.Paymentmethod source)
        {
            Obymobi.Logic.Model.Paymentmethod target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Paymentmethod();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.PaymentmethodConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
