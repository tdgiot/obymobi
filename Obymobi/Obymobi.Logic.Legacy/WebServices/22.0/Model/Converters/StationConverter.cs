using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.v22.Converters
{
	public class StationConverter : ModelConverterBase<Obymobi.Logic.Model.v22.Station, Obymobi.Logic.Model.Station>
	{
        public StationConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.StationConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.Station ConvertModelToLegacyModel(Obymobi.Logic.Model.Station source)
        {
            Obymobi.Logic.Model.v22.Station target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.Station();
                target.StationId = source.StationId;
                target.Caption = source.Caption;
                target.Scene = source.Scene;
                target.SuccessMessage = source.SuccessMessage;
                target.Description = source.Description;

                if (int.TryParse(source.Channel, out int number))
                {
                    target.Number = number;
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v22.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<StationLanguage> languages = new System.Collections.Generic.List<StationLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        StationLanguage model = new StationLanguage();
                        model.StationLanguageId = -1;
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.StationName);
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.SuccessMessage = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.StationSuccessMessage);
                        model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.StationDescription);

                        languages.Add(model);
                    }

                    target.StationLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Station ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.Station source)
        {
            Obymobi.Logic.Model.Station target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Station();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.StationConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
