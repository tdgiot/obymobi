namespace Obymobi.Logic.Model.v22.Converters
{
	public class IcrtouchprintermappingDeliverypointConverter : ModelConverterBase<Obymobi.Logic.Model.v22.IcrtouchprintermappingDeliverypoint, Obymobi.Logic.Model.IcrtouchprintermappingDeliverypoint>
	{
        public IcrtouchprintermappingDeliverypointConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.IcrtouchprintermappingDeliverypointConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.IcrtouchprintermappingDeliverypoint ConvertModelToLegacyModel(Obymobi.Logic.Model.IcrtouchprintermappingDeliverypoint source)
        {
            Obymobi.Logic.Model.v22.IcrtouchprintermappingDeliverypoint target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.IcrtouchprintermappingDeliverypoint();
                target.IcrtouchprintermappingDeliverypointId = source.IcrtouchprintermappingDeliverypointId;
                target.IcrtouchprintermappingId = source.IcrtouchprintermappingId;
                target.DeliverypointId = source.DeliverypointId;
                target.Printer1 = source.Printer1;
                target.Printer2 = source.Printer2;
                target.Printer3 = source.Printer3;
                target.Printer4 = source.Printer4;
                target.Printer5 = source.Printer5;
                target.Printer6 = source.Printer6;
                target.Printer7 = source.Printer7;
                target.Printer8 = source.Printer8;
                target.Printer9 = source.Printer9;
                target.Printer10 = source.Printer10;
            }

            return target;
        }

        public override Obymobi.Logic.Model.IcrtouchprintermappingDeliverypoint ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.IcrtouchprintermappingDeliverypoint source)
        {
            Obymobi.Logic.Model.IcrtouchprintermappingDeliverypoint target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.IcrtouchprintermappingDeliverypoint();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.IcrtouchprintermappingDeliverypointConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
