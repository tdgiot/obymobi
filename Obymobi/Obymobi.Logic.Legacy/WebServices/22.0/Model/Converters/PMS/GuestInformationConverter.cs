namespace Obymobi.Logic.Model.v22.Converters
{
	public class GuestInformationConverter : ModelConverterBase<Obymobi.Logic.Model.v22.GuestInformation, Obymobi.Logic.Model.GuestInformation>
	{
        public GuestInformationConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.GuestInformationConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.GuestInformation ConvertModelToLegacyModel(Obymobi.Logic.Model.GuestInformation source)
        {
            Obymobi.Logic.Model.v22.GuestInformation target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.GuestInformation();
                target.OnSiteServerWebserviceJobType = source.OnSiteServerWebserviceJobType;
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.Occupied = source.Occupied;
                target.Title = source.Title;
                target.AccountNumber = source.AccountNumber;
                target.GroupReference = source.GroupReference;
                target.GroupName = source.GroupName;
                target.Company = source.Company;
                target.CustomerFirstname = source.CustomerFirstname;
                target.CustomerLastname = source.CustomerLastname;
                target.GuestId = source.GuestId;
                target.CustomerLastnamePrefix = source.CustomerLastnamePrefix;
                target.Zipcode = source.Zipcode;
                target.Password = source.Password;
                target.AllowFolioPosting = source.AllowFolioPosting;
                target.AllowViewFolioSet = source.AllowViewFolioSet;
                target.AllowViewFolio = source.AllowViewFolio;
                target.AllowExpressCheckoutSet = source.AllowExpressCheckoutSet;
                target.AllowExpressCheckout = source.AllowExpressCheckout;
                target.CreditLimit = source.CreditLimit;
                target.LanguageCode = source.LanguageCode;
                target.Vip = source.Vip;
                target.Error = source.Error;
                target.VoicemailMessage = source.VoicemailMessage;
                target.Arrival = source.Arrival;
                target.Departure = source.Departure;
                target.ClassOfService = source.ClassOfService;
                target.DoNotDisturb = source.DoNotDisturb;
                target.TvSetting = source.TvSetting;
                target.MinibarSetting = source.MinibarSetting;
            }

            return target;
        }

        public override Obymobi.Logic.Model.GuestInformation ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.GuestInformation source)
        {
            Obymobi.Logic.Model.GuestInformation target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.GuestInformation();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.GuestInformationConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
