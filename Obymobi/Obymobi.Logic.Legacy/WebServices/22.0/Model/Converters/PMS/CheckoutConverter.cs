namespace Obymobi.Logic.Model.v22.Converters
{
	public class CheckoutConverter : ModelConverterBase<Obymobi.Logic.Model.v22.Checkout, Obymobi.Logic.Model.Checkout>
	{
        public CheckoutConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.CheckoutConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.Checkout ConvertModelToLegacyModel(Obymobi.Logic.Model.Checkout source)
        {
            Obymobi.Logic.Model.v22.Checkout target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.Checkout();
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.AccountNumber = source.AccountNumber;
                target.ChargePriceIn = source.ChargePriceIn;
                target.ChargePaid = source.ChargePaid;
                target.CheckoutSuccessful = source.CheckoutSuccessful;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Checkout ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.Checkout source)
        {
            Obymobi.Logic.Model.Checkout target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Checkout();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.CheckoutConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
