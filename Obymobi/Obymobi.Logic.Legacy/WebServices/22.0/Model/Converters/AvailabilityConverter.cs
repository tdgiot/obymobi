using System.Collections.Generic;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Logic.Model.v22.Converters
{
	public class AvailabilityConverter : ModelConverterBase<Obymobi.Logic.Model.v22.Availability, Obymobi.Logic.Model.Availability>
	{
        public AvailabilityConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");

            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.AvailabilityConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.Availability ConvertModelToLegacyModel(Obymobi.Logic.Model.Availability source)
        {
            Obymobi.Logic.Model.v22.Availability target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.Availability();
                target.AvailabilityId = source.AvailabilityId;
                target.Name = source.Name;
                target.Status = source.Status;
                target.ActionEntertainmentId = source.ActionEntertainmentId;
                target.ActionCategoryId = source.ActionCategoryId;
                target.ActionProductId = source.ActionProductId;
                target.ActionSiteId = source.ActionSiteId;
                target.ActionPageId = source.ActionPageId;
                target.Url = source.Url;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v22.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Availability ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.Availability source)
        {
            Obymobi.Logic.Model.Availability target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Availability();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.AvailabilityConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                if (source.CustomTexts != null && source.AvailabilityId > 0)
                {
                    AvailabilityEntity availabilityEntity = new AvailabilityEntity(source.AvailabilityId);
                    if (!availabilityEntity.IsNew)
                    {
                        List<Obymobi.Logic.Model.CustomText> customTexts = new List<Obymobi.Logic.Model.CustomText>();
                        foreach (CustomText customText in source.CustomTexts)
                        {
                            if (customText.LanguageCode.IsNullOrWhiteSpace())
                                continue;

                            Obymobi.Culture culture = CompanyCultureHelper.GetCultureByCompanyLanguageCode(availabilityEntity.CompanyId, customText.LanguageCode);

                            Obymobi.Logic.Model.CustomText model = new Obymobi.Logic.Model.CustomText();
                            model.CustomTextId = customText.CustomTextId;
                            model.Type = customText.Type;
                            model.Text = customText.Text;
                            model.ForeignKey = source.AvailabilityId;
                            model.CultureCode = culture.Code;

                            customTexts.Add(model);
                        }
                        target.CustomTexts = customTexts.ToArray();
                    }
                }

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);                
            }

            return target;
        }
	}
}
