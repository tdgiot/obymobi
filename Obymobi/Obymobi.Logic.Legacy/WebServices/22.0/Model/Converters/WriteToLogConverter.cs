namespace Obymobi.Logic.Model.v22.Converters
{
	public class WriteToLogConverter : ModelConverterBase<Obymobi.Logic.Model.v22.WriteToLog, Obymobi.Logic.Model.WriteToLog>
	{
        public WriteToLogConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.WriteToLogConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.WriteToLog ConvertModelToLegacyModel(Obymobi.Logic.Model.WriteToLog source)
        {
            Obymobi.Logic.Model.v22.WriteToLog target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.WriteToLog();
                target.TerminalLogType = source.TerminalLogType;
                target.Message = source.Message;
                target.Status = source.Status;
                target.Log = source.Log;
                target.orderId = source.orderId;
                target.OrderGuid = source.OrderGuid;
            }

            return target;
        }

        public override Obymobi.Logic.Model.WriteToLog ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.WriteToLog source)
        {
            Obymobi.Logic.Model.WriteToLog target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.WriteToLog();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.WriteToLogConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
