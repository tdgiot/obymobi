using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Logic.Model.v22.Converters
{
	public class UIWidgetConverter : ModelConverterBase<Obymobi.Logic.Model.v22.UIWidget, Obymobi.Logic.Model.UIWidget>
	{
        public UIWidgetConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");

            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v23.Converters.UIWidgetConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v22.UIWidget ConvertModelToLegacyModel(Obymobi.Logic.Model.UIWidget source)
        {
            Obymobi.Logic.Model.v22.UIWidget target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v22.UIWidget();
                target.UIWidgetId = source.UIWidgetId;
                target.UITabId = source.UITabId;
                target.Name = source.Name;
                target.Caption = source.Caption;
                target.Type = source.Type;
                target.SortOrder = source.SortOrder;
                target.AdvertisementId = source.AdvertisementId;
                target.ProductId = source.ProductId;
                target.CategoryId = source.CategoryId;
                target.EntertainmentId = source.EntertainmentId;
                target.EntertainmentcategoryId = source.EntertainmentcategoryId;
                target.SiteId = source.SiteId;
                target.PageId = source.PageId;
                target.UITabType = source.UITabType;
                target.Url = source.Url;
                target.RoomControlSectionType = source.RoomControlSectionType;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.MessageLayoutType = source.MessageLayoutType;

                if (source.UIWidgetTimers != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.UIWidgetTimerConverter uIWidgetTimersConverter = new Obymobi.Logic.Model.v22.Converters.UIWidgetTimerConverter();
                    target.UIWidgetTimers = (UIWidgetTimer[])uIWidgetTimersConverter.ConvertArrayToLegacyArray(source.UIWidgetTimers);
                }

                if (source.UIWidgetAvailabilitys != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.UIWidgetAvailabilityConverter uIWidgetAvailabilitysConverter = new Obymobi.Logic.Model.v22.Converters.UIWidgetAvailabilityConverter();
                    target.UIWidgetAvailabilitys = (UIWidgetAvailability[])uIWidgetAvailabilitysConverter.ConvertArrayToLegacyArray(source.UIWidgetAvailabilitys);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v22.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v22.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<UIWidgetLanguage> languages = new System.Collections.Generic.List<UIWidgetLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        UIWidgetLanguage model = new UIWidgetLanguage();
                        model.UIWidgetLanguageId = -1;
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Caption = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.UIWidgetCaption);
                        model.FieldValue1 = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.UIWidgetFieldValue1);
                        model.FieldValue2 = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.UIWidgetFieldValue2);
                        model.FieldValue3 = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.UIWidgetFieldValue3);
                        model.FieldValue4 = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.UIWidgetFieldValue4);
                        model.FieldValue5 = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.UIWidgetFieldValue5);

                        languages.Add(model);
                    }

                    target.UIWidgetLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.UIWidget ConvertLegacyModelToModel(Obymobi.Logic.Model.v22.UIWidget source)
        {
            Obymobi.Logic.Model.UIWidget target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UIWidget();

                // Copy default values from new version
                new Obymobi.Logic.Model.v23.Converters.UIWidgetConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                if (source.UIWidgetId > 0)
                {
                    UIWidgetEntity entity = new UIWidgetEntity(source.UIWidgetId);
                    if (!entity.IsNew)
                    {
                        List<Model.CustomText> customTexts = new List<Model.CustomText>();
                        foreach (UIWidgetLanguage language in source.UIWidgetLanguages)
                        {
                            if (!entity.ParentCompanyId.HasValue)
                                continue;

                            Obymobi.Culture culture = CompanyCultureHelper.GetCultureByCompanyLanguageCode(entity.ParentCompanyId.Value, language.LanguageCode);

                            Model.CustomText caption = new Model.CustomText();
                            caption.CustomTextId = -1;
                            caption.Type = (int)Obymobi.Enums.CustomTextType.UIWidgetCaption;
                            caption.ForeignKey = source.UIWidgetId;
                            caption.CultureCode = culture.Code;
                            caption.Text = language.Caption;
                            customTexts.Add(caption);

                            Model.CustomText fieldvalue1 = new Model.CustomText();
                            fieldvalue1.CustomTextId = -1;
                            fieldvalue1.Type = (int)Obymobi.Enums.CustomTextType.UIWidgetFieldValue1;
                            fieldvalue1.ForeignKey = source.UIWidgetId;
                            fieldvalue1.CultureCode = culture.Code;
                            fieldvalue1.Text = language.FieldValue1;
                            customTexts.Add(fieldvalue1);

                            Model.CustomText fieldvalue2 = new Model.CustomText();
                            fieldvalue2.CustomTextId = -1;
                            fieldvalue2.Type = (int)Obymobi.Enums.CustomTextType.UIWidgetFieldValue2;
                            fieldvalue2.ForeignKey = source.UIWidgetId;
                            fieldvalue2.CultureCode = culture.Code;
                            fieldvalue2.Text = language.FieldValue2;
                            customTexts.Add(fieldvalue2);

                            Model.CustomText fieldValue3 = new Model.CustomText();
                            fieldValue3.CustomTextId = -1;
                            fieldValue3.Type = (int)Obymobi.Enums.CustomTextType.UIWidgetFieldValue3;
                            fieldValue3.ForeignKey = source.UIWidgetId;
                            fieldValue3.CultureCode = culture.Code;
                            fieldValue3.Text = language.FieldValue3;
                            customTexts.Add(fieldValue3);

                            Model.CustomText fieldValue4 = new Model.CustomText();
                            fieldValue4.CustomTextId = -1;
                            fieldValue4.Type = (int)Obymobi.Enums.CustomTextType.UIWidgetFieldValue4;
                            fieldValue4.ForeignKey = source.UIWidgetId;
                            fieldValue4.CultureCode = culture.Code;
                            fieldValue4.Text = language.FieldValue4;
                            customTexts.Add(fieldValue4);

                            Model.CustomText fieldValue5 = new Model.CustomText();
                            fieldValue5.CustomTextId = -1;
                            fieldValue5.Type = (int)Obymobi.Enums.CustomTextType.UIWidgetFieldValue5;
                            fieldValue5.ForeignKey = source.UIWidgetId;
                            fieldValue5.CultureCode = culture.Code;
                            fieldValue5.Text = language.FieldValue5;
                            customTexts.Add(fieldValue5);
                        }
                        target.CustomTexts = customTexts.ToArray();
                    }
                }

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
