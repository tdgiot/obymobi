using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents a entertainmentcategory language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "EntertainmentcategoryLanguage"), IncludeInCodeGeneratorForAndroid]
    public class EntertainmentcategoryLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.EntertainmentcategoryLanguage type
        /// </summary>
        public EntertainmentcategoryLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the entertainmentcategory language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentcategoryLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentcategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the title of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public EntertainmentcategoryLanguage Clone()
        {
            return this.Clone<EntertainmentcategoryLanguage>();
        }

        #endregion
    }
}
