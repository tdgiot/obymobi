using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents a customer payment method
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CustomerPaymentmethod"), IncludeInCodeGeneratorForMobile]
    public class CustomerPaymentmethod : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CustomerPaymentmethod type
        /// </summary>
        public CustomerPaymentmethod()
        {

        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CustomerPaymentmethod type
        /// </summary>
        public CustomerPaymentmethod(int CustomerPaymentmethodId, int CustomerId, int PaymentmethodId, string FieldValue1, string FieldValue2, string FieldValue3, string FieldValue4, string FieldValue5, string FieldValue6, string FieldValue7, string FieldValue8, string FieldValue9, string FieldValue10)
        {
            this.CustomerPaymentmethodId = CustomerPaymentmethodId;
            this.CustomerId = CustomerId;
            this.PaymentmethodId = PaymentmethodId;
            this.FieldValue1 = FieldValue1;
            this.FieldValue2 = FieldValue2;
            this.FieldValue3 = FieldValue3;
            this.FieldValue4 = FieldValue4;
            this.FieldValue5 = FieldValue5;
            this.FieldValue6 = FieldValue6;
            this.FieldValue7 = FieldValue7;
            this.FieldValue8 = FieldValue8;
            this.FieldValue9 = FieldValue9;
            this.FieldValue10 = FieldValue10;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the customer payment method
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForMobile]
        public int CustomerPaymentmethodId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the customer of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForMobile]
        public int CustomerId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the paymentmethod of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public int PaymentmethodId
        { get; set; }

        /// <summary>
        /// Gets or sets the fieldvalue1 of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets the fieldvalue2 of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets the fieldvalue3 of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets the fieldvalue4 of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets the fieldvalue5 of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets the fieldvalue6 of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets the fieldvalue7 of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets the fieldvalue8 of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets the fieldvalue9 of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets the fieldvalue10 of the payment method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string FieldValue10
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CustomerPaymentmethod Clone()
        {
            return this.Clone<CustomerPaymentmethod>();
        }

        #endregion
    }
}
