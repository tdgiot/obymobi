using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents a point-of-sale product
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Posorder")]
    public class Posorder : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Posorder type
        /// </summary>
        public Posorder()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the order
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int PosorderId
        { get; set; }

        /// <summary>
        /// Gets or sets the external id of the pos deliverypoint
        /// </summary>
        [XmlElement]
        public string PosdeliverypointExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the external id of the pos paymentmethod
        /// </summary>
        [XmlElement]
        public string PospaymentmethodExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the order
        /// </summary>
        [XmlElement]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the orderitems of the pos order
        /// </summary>
        [XmlArray]
        public Posorderitem[] Posorderitems
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }



        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Posorder Clone()
        {
            return this.Clone<Posorder>();
        }

        #endregion
    }
}
