using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Webservice result used by Service project, Backend webservice
    /// </summary>
    [DataContract]
    public class ServiceBackendWebserviceResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceBackendWebserviceResult"/> class.
        /// </summary>
        public ServiceBackendWebserviceResult()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceBackendWebserviceResult"/> class.
        /// </summary>
        /// <param name="success">if set to <c>true</c> [success].</param>
        public ServiceBackendWebserviceResult(bool success)
        {
            this.Success = success;
        }

        /// <summary>
        /// Gets of sets if the call was succesfull
        /// </summary>
        [DataMember]
        public bool Success { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public List<string> SupportpoolEmailaddresses { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public List<string> SupportpoolPhonenumbers { get; set; }
    }
}