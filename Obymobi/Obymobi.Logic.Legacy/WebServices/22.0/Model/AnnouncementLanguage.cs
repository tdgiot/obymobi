using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents a announcement language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "AnnouncementLanguage"), IncludeInCodeGeneratorForAndroid]
    public class AnnouncementLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.AnnouncementLanguage type
        /// </summary>
        public AnnouncementLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the announcement language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int AnnouncementLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AnnouncementId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the title of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Title
        { get; set; }

        /// <summary>
        /// Gets or sets the subtitle of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [Obsolete("MB: Field does not exists anymore in database")]
        public string Subtitle
        { get; set; }

        /// <summary>
        /// Gets or sets the text of the announcement
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Text
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public AnnouncementLanguage Clone()
        {
            return this.Clone<AnnouncementLanguage>();
        }

        #endregion
    }
}
