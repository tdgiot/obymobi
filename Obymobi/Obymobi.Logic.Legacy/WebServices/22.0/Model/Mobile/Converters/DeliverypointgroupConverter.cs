using Obymobi.Logic.Mobile;
using System.Linq;
using Dionysos;
namespace Obymobi.Logic.Model.Mobile.v22.Converters
{
	public class DeliverypointgroupConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v22.Deliverypointgroup, Obymobi.Logic.Model.Mobile.Deliverypointgroup>
	{
        public DeliverypointgroupConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v23.Converters.DeliverypointgroupConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v22.Deliverypointgroup ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Deliverypointgroup source)
        {
            Obymobi.Logic.Model.Mobile.v22.Deliverypointgroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v22.Deliverypointgroup();
                target.DeliverypointgroupId = source.DeliverypointgroupId;
                target.CompanyId = source.CompanyId;
                target.Name = source.Name;
                target.OrderHistoryDialogEnabled = source.OrderHistoryDialogEnabled;
                target.MenuId = source.MenuId;
                target.DeliverypointCaption = source.DeliverypointCaption;
                target.HomepageSlideshowInterval = source.HomepageSlideshowInterval;

                if (source.UIModes != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v22.Converters.UIModeConverter uIModesConverter = new Obymobi.Logic.Model.Mobile.v22.Converters.UIModeConverter();
                    target.UIModes = (UIMode[])uIModesConverter.ConvertArrayToLegacyArray(source.UIModes);
                }

                if (source.Deliverypoints != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v22.Converters.DeliverypointConverter deliverypointsConverter = new Obymobi.Logic.Model.Mobile.v22.Converters.DeliverypointConverter();
                    target.Deliverypoints = (Deliverypoint[])deliverypointsConverter.ConvertArrayToLegacyArray(source.Deliverypoints);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v22.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v22.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }                

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<DeliverypointgroupLanguage> languages = new System.Collections.Generic.List<DeliverypointgroupLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        DeliverypointgroupLanguage model = new DeliverypointgroupLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.DeliverypointCaption = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupDeliverypointCaption);
                        model.Title = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupTitle);
                        model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.DeliverypointgroupDescription);                        

                        languages.Add(model);
                    }

                    target.DeliverypointgroupLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Deliverypointgroup ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v22.Deliverypointgroup source)
        {
            Obymobi.Logic.Model.Mobile.Deliverypointgroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Deliverypointgroup();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v23.Converters.DeliverypointgroupConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
