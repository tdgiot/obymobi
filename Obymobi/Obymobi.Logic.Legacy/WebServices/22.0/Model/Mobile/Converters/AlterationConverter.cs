using MarkdownSharp.Extensions;
using Obymobi.Enums;
using Obymobi.Logic.Mobile;
using System.Linq;

namespace Obymobi.Logic.Model.Mobile.v22.Converters
{
	public class AlterationConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v22.Alteration, Obymobi.Logic.Model.Mobile.Alteration>
	{
        public AlterationConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v23.Converters.AlterationConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v22.Alteration ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Alteration source)
        {
            Obymobi.Logic.Model.Mobile.v22.Alteration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v22.Alteration();
                target.AlterationId = source.AlterationId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.DefaultAlterationoptionId = source.DefaultAlterationoptionId;
                target.MinOptions = source.MinOptions;
                target.MaxOptions = source.MaxOptions;
                target.StartTime = source.StartTime;
                target.EndTime = source.EndTime;
                target.MinLeadMinutes = source.MinLeadMinutes;
                target.MaxLeadHours = source.MaxLeadHours;
                target.IntervalMinutes = source.IntervalMinutes;
                target.ShowDatePicker = source.ShowDatePicker;
                target.Value = source.Value;
                target.OrderLevelEnabled = source.OrderLevelEnabled;
                target.ParentAlterationId = source.ParentAlterationId;
                target.Visible = source.Visible;
                target.Description = source.Description;

                if (source.Alterationoptions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v22.Converters.AlterationoptionConverter alterationoptionsConverter = new Obymobi.Logic.Model.Mobile.v22.Converters.AlterationoptionConverter();
                    target.Alterationoptions = (Alterationoption[])alterationoptionsConverter.ConvertArrayToLegacyArray(source.Alterationoptions);
                }
                
                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v22.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v22.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.Products != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v22.Converters.ProductConverter productsConverter = new Obymobi.Logic.Model.Mobile.v22.Converters.ProductConverter();
                    target.Products = (Product[])productsConverter.ConvertArrayToLegacyArray(source.Products);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<AlterationLanguage> languages = new System.Collections.Generic.List<AlterationLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        AlterationLanguage alterationLanguage = new AlterationLanguage();
                        alterationLanguage.AlterationLanguageId = -1;
                        alterationLanguage.AlterationId = source.AlterationId;
                        alterationLanguage.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        alterationLanguage.Name = Dionysos.DictionaryUtil.GetValueOrDefault(customTexts, Obymobi.Enums.CustomTextType.AlterationName);

                        string description = Dionysos.DictionaryUtil.GetValueOrDefault(customTexts, Obymobi.Enums.CustomTextType.AlterationDescription);
                        if (source.Type != (int)Obymobi.Enums.AlterationType.Instruction)
                        {
                            alterationLanguage.Description = description;
                        }
                        else
                        {
                            // Since Markdown is not thread safe and apparently creation cost is low, we spawn one (https://code.google.com/p/markdownsharp/issues/detail?id=48)
                            MarkdownSharp.Markdown markdown = MarkdownHelper.GetDefaultInstance();
                            alterationLanguage.Description = markdown.Transform(description);

                            if (alterationLanguage.Description.EndsWith("\n"))
                                alterationLanguage.Description = alterationLanguage.Description.Substring(0, alterationLanguage.Description.Length - 1);
                        }

                        languages.Add(alterationLanguage);
                    }

                    target.AlterationLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Alteration ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v22.Alteration source)
        {
            Obymobi.Logic.Model.Mobile.Alteration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Alteration();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v23.Converters.AlterationConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
