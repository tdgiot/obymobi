using Obymobi.Logic.Mobile;
using System.Linq;
using Dionysos;
namespace Obymobi.Logic.Model.Mobile.v22.Converters
{
	public class AttachmentConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v22.Attachment, Obymobi.Logic.Model.Mobile.Attachment>
	{
        public AttachmentConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v23.Converters.AttachmentConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v22.Attachment ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Attachment source)
        {
            Obymobi.Logic.Model.Mobile.v22.Attachment target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v22.Attachment();
                target.ProductId = source.ProductId;
                target.PageId = source.PageId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.Url = source.Url;
                target.AllowedDomains = source.AllowedDomains;
                target.AllowAllDomains = source.AllowAllDomains;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v22.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v22.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<AttachmentLanguage> languages = new System.Collections.Generic.List<AttachmentLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        AttachmentLanguage model = new AttachmentLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.AttachmentName);

                        languages.Add(model);
                    }

                    target.AttachmentLanguages = languages.ToArray();
                }                
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Attachment ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v22.Attachment source)
        {
            Obymobi.Logic.Model.Mobile.Attachment target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Attachment();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v23.Converters.AttachmentConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
