using Obymobi.Logic.Mobile;
using System.Linq;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Model.Mobile.v22.Converters
{
	public class SiteConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v22.Site, Obymobi.Logic.Model.Mobile.Site>
	{
        public SiteConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v23.Converters.SiteConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v22.Site ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Site source)
        {
            Obymobi.Logic.Model.Mobile.v22.Site target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v22.Site();
                target.SiteId = source.SiteId;
                target.Name = source.Name;
                target.SiteType = source.SiteType;

                if (source.Pages != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v22.Converters.PageConverter pagesConverter = new Obymobi.Logic.Model.Mobile.v22.Converters.PageConverter();
                    target.Pages = (Page[])pagesConverter.ConvertArrayToLegacyArray(source.Pages);
                }

                target.LastModifiedTicks = source.LastModifiedTicks;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v22.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v22.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<SiteLanguage> languages = new System.Collections.Generic.List<SiteLanguage>();

                    SiteCultureCollection siteCultures = new SiteCultureCollection();
                    siteCultures.GetMulti(new PredicateExpression(SiteCultureFields.SiteId == source.SiteId));

                    foreach (SiteCultureEntity siteCulture in siteCultures)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, siteCulture.CultureCode);

                        SiteLanguage model = new SiteLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[siteCulture.CultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.SiteLanguageId = -1;
                        model.SiteId = source.SiteId;
                        model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.SiteDescription);                        

                        languages.Add(model);
                    }

                    target.SiteLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Site ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v22.Site source)
        {
            Obymobi.Logic.Model.Mobile.Site target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Site();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v23.Converters.SiteConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
