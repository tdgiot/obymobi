using Obymobi.Logic.Mobile;
using System.Linq;
using Dionysos;

namespace Obymobi.Logic.Model.Mobile.v22.Converters
{
	public class AlterationoptionConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v22.Alterationoption, Obymobi.Logic.Model.Mobile.Alterationoption>
	{
        public AlterationoptionConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v23.Converters.AlterationoptionConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v22.Alterationoption ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Alterationoption source)
        {
            Obymobi.Logic.Model.Mobile.v22.Alterationoption target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v22.Alterationoption();
                target.AlterationoptionId = source.AlterationoptionId;
                target.Name = source.Name;
                target.PriceIn = source.PriceIn;
                target.Description = source.Description;

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<AlterationoptionLanguage> languages = new System.Collections.Generic.List<AlterationoptionLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        AlterationoptionLanguage model = new AlterationoptionLanguage();
                        model.AlterationoptionLanguageId = -1;
                        model.AlterationoptionId = source.AlterationoptionId;
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.AlterationoptionName);
                        model.Description = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.AlterationoptionDescription);

                        languages.Add(model);
                    }

                    target.AlterationoptionLanguages = languages.ToArray();
                }

                target.IsSelected = source.IsSelected;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Alterationoption ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v22.Alterationoption source)
        {
            Obymobi.Logic.Model.Mobile.Alterationoption target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Alterationoption();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v23.Converters.AlterationoptionConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
