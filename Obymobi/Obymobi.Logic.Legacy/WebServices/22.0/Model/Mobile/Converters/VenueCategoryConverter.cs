using Obymobi.Logic.Mobile;
using System.Linq;
using Dionysos;
namespace Obymobi.Logic.Model.Mobile.v22.Converters
{
	public class VenueCategoryConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v22.VenueCategory, Obymobi.Logic.Model.Mobile.VenueCategory>
	{
        public VenueCategoryConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v23.Converters.VenueCategoryConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v22.VenueCategory ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.VenueCategory source)
        {
            Obymobi.Logic.Model.Mobile.v22.VenueCategory target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v22.VenueCategory();
                target.VenueCategoryId = source.VenueCategoryId;
                target.Name = source.Name;
                target.MarkerIcon = source.MarkerIcon;                

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<VenueCategoryLanguage> languages = new System.Collections.Generic.List<VenueCategoryLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        VenueCategoryLanguage model = new VenueCategoryLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.VenueCategoryName);
                        model.NamePlural = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.VenueCategoryNamePlural);                        

                        languages.Add(model);
                    }

                    target.VenueCategoryLanguages = languages.ToArray();
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.VenueCategory ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v22.VenueCategory source)
        {
            Obymobi.Logic.Model.Mobile.VenueCategory target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.VenueCategory();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v23.Converters.VenueCategoryConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
