using Obymobi.Logic.Mobile;
using System.Linq;
using Dionysos;
namespace Obymobi.Logic.Model.Mobile.v22.Converters
{
	public class AmenityConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v22.Amenity, Obymobi.Logic.Model.Mobile.Amenity>
	{
        public AmenityConverter()
        {
            // Set the excluded fields
            this.FieldsToExclude.Add("CustomTexts");
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v23.Converters.AmenityConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v22.Amenity ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Amenity source)
        {
            Obymobi.Logic.Model.Mobile.v22.Amenity target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v22.Amenity();
                target.AmenityId = source.AmenityId;
                target.Name = source.Name;

                if (source.CustomTexts != null)
                {
                    System.Collections.Generic.List<AmenityLanguage> languages = new System.Collections.Generic.List<AmenityLanguage>();

                    System.Collections.Generic.List<string> cultureCodes = source.CustomTexts.Where(x => !string.IsNullOrWhiteSpace(x.CultureCode)).Select(x => x.CultureCode).Distinct().ToList();
                    foreach (string cultureCode in cultureCodes)
                    {
                        System.Collections.Generic.Dictionary<Obymobi.Enums.CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(source.CustomTexts, cultureCode);

                        AmenityLanguage model = new AmenityLanguage();
                        model.LanguageCode = Obymobi.Culture.Mappings[cultureCode].Language.CodeAlpha2.ToUpperInvariant();
                        model.Name = customTexts.GetValueOrDefault(Obymobi.Enums.CustomTextType.AmenityName);                        

                        languages.Add(model);
                    }

                    target.AmenityLanguages = languages.ToArray();
                }                
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Amenity ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v22.Amenity source)
        {
            Obymobi.Logic.Model.Mobile.Amenity target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Amenity();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v23.Converters.AmenityConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
