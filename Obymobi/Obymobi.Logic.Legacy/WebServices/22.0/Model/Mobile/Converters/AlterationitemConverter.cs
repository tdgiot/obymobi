using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v22.Converters
{
	public class AlterationitemConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v22.Alterationitem, Obymobi.Logic.Model.Mobile.Alterationitem>
	{
        public AlterationitemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v23.Converters.AlterationitemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v22.Alterationitem ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Alterationitem source)
        {
            Obymobi.Logic.Model.Mobile.v22.Alterationitem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v22.Alterationitem();
                target.AlterationitemId = source.AlterationitemId;
                target.AlterationId = source.AlterationId;
                target.Guid = source.Guid;
                target.AlterationName = source.AlterationName;
                target.AlterationType = source.AlterationType;
                target.AlterationoptionId = source.AlterationoptionId;
                target.AlterationoptionName = source.AlterationoptionName;
                target.AlterationoptionPriceIn = source.AlterationoptionPriceIn;
                target.SelectedOnDefault = source.SelectedOnDefault;
                target.SortOrder = source.SortOrder;
                target.Time = source.Time;
                target.Value = source.Value;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Alterationitem ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v22.Alterationitem source)
        {
            Obymobi.Logic.Model.Mobile.Alterationitem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Alterationitem();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v23.Converters.AlterationitemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
