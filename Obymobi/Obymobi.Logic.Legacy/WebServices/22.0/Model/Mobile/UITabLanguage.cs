using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v22
{
    /// <summary>
    /// Model class which represents a UITabLanguage item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "UITabLanguage"), IncludeInCodeGeneratorForXamarin]
    public class UITabLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Mobile.UITabLanguage type
        /// </summary>
        public UITabLanguage()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the tab language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int UITabLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the caption of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Caption
        { get; set; }

        /// <summary>
        /// Gets or sets the URL of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string URL
        { get; set; }

        /// <summary>
        /// Gets or sets the zoom level of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Zoom
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UITabLanguage Clone()
        {
            return this.Clone<UITabLanguage>();
        }

        #endregion
    }
}
