using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v22
{
    /// <summary>
    /// Model class which represents a venue category
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "VenueCategory"), IncludeInCodeGeneratorForXamarin]
    public class VenueCategory : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.VenueCategory type
        /// </summary>
        public VenueCategory()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the venue category
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int VenueCategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the marker icon of the venue category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int MarkerIcon
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [XmlArray("VenueCategoryLanguages")]
        [XmlArrayItem("VenueCategoryLanguage")]
        [IncludeInCodeGeneratorForXamarin]
        public VenueCategoryLanguage[] VenueCategoryLanguages
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public VenueCategory Clone()
        {
            return this.Clone<VenueCategory>();
        }

        #endregion
    }
}
