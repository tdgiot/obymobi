using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v22
{
    /// <summary>
    /// Model class which represents an amenity language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "AmenityLanguage"), IncludeInCodeGeneratorForXamarin]
    public class AmenityLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.AmenityLanguage type
        /// </summary>
        public AmenityLanguage()
        {
        }

        #endregion

        #region Properties

         /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the amenity
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public AmenityLanguage Clone()
        {
            return this.Clone<AmenityLanguage>();
        }

        #endregion
    }
}
