using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v22
{
    /// <summary>
    /// Model class which represents a company list for the mobile solution
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CompanyList"), IncludeInCodeGeneratorForXamarin]
    public class CompanyList : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Mobile.CompanyList type
        /// </summary>
        public CompanyList()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the ticks of the companylist
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public long Ticks
        { get; set; }

        /// <summary>
        /// Gets or sets the companies of the company list
        /// </summary>
        [XmlArray("Companies")]
        [XmlArrayItem("Company")]
        [IncludeInCodeGeneratorForXamarin]
        public Company[] Companies
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CompanyList Clone()
        {
            return this.Clone<CompanyList>();
        }

        #endregion
    }
}