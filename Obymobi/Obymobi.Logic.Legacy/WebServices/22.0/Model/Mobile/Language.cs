using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v22
{
    /// <summary>
    /// Model class which represents an language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Language"), IncludeInCodeGeneratorForXamarin]
    public class Language : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Language type
        /// </summary>
        public Language()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the name of the language
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the code of the code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Code
        { get; set; }

        /// <summary>
        /// Gets or sets the localized name of the language
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string LocalizedName
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the company or POI
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets the short description of the company or POI
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string DescriptionSingleLine
        { get; set; }

        /// <summary>
        /// Gets or sets the deliverypointCaption of the deliverypointgroup
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string DeliverypointCaption
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Language Clone()
        {
            return this.Clone<Language>();
        }

        #endregion
    }
}
