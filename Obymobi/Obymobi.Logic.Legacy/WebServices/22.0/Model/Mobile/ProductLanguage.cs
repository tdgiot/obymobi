using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v22
{
    /// <summary>
    /// Model class which represents a product language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "ProductLanguage"), IncludeInCodeGeneratorForXamarin]
    public class ProductLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.ProductLanguage type
        /// </summary>
        public ProductLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the product language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int ProductLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string ButtonText
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public ProductLanguage Clone()
        {
            return this.Clone<ProductLanguage>();
        }

        #endregion
    }
}
    