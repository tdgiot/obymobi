using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v22
{
    /// <summary>
    /// Model class which represents a product suggestion
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "ProductSuggestion"), IncludeInCodeGeneratorForXamarin]
    public class ProductSuggestion : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.ProductSuggestion type
        /// </summary>
        public ProductSuggestion()
        {
        }

        #endregion

        #region Xml Properties

         /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the suggested product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SuggestedProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of this suggestion
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets if the suggest should be shown at checkout
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool Checkout
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public ProductSuggestion Clone()
        {
            return this.Clone<ProductSuggestion>();
        }

        #endregion
    }
}
