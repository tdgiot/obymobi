using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v22
{
    /// <summary>
    /// Model class which represents a venue category language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "VenueCategoryLanguage"), IncludeInCodeGeneratorForXamarin]
    public class VenueCategoryLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.VenueCategoryLanguage type
        /// </summary>
        public VenueCategoryLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the venue category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the venue category
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string NamePlural
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public VenueCategoryLanguage Clone()
        {
            return this.Clone<VenueCategoryLanguage>();
        }

        #endregion
    }
}
