using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.Mobile.v22
{
    /// <summary>
    /// Model class which represents a company for the mobile solution
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "PageLanguage"), IncludeInCodeGeneratorForXamarin]
    public class PageLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Company type
        /// </summary>
        public PageLanguage()
        {
        }

        #endregion

        #region Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int PageLanguageId                
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int PageId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        #endregion

        #region Methods

        public PageLanguage Clone()
        {
            return this.Clone<PageLanguage>();
        }

        public PageLanguage CloneUsingBinaryFormatter()
        {
            return this.CloneUsingBinaryFormatter<PageLanguage>();
        }

        #endregion
    }
}
