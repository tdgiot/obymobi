using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents an entertainmentDependency item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "EntertainmentDependency"), IncludeInCodeGeneratorForAndroid]
    public class EntertainmentDependency : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.EntertainmentDependency type
        /// </summary>
        public EntertainmentDependency()
        {
        }

        #endregion

        #region Xml Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string PackageName
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ClassName
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Filename
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int FileVersion
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public EntertainmentDependency Clone()
        {
            return this.Clone<EntertainmentDependency>();
        }

        #endregion
    }
}
