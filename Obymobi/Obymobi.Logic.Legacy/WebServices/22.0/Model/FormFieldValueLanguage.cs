using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents a formFieldValue language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "FormFieldValueLanguage")]
    public class FormFieldValueLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.FormFieldValueLanguage type
        /// </summary>
        public FormFieldValueLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the formFieldValue language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int FormFieldValueLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the formFieldValue
        /// </summary>
        [XmlElement]
        public int FormFieldValueId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the formFieldValue
        /// </summary>
        [XmlElement]
        public string Value
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public FormFieldValueLanguage Clone()
        {
            return this.Clone<FormFieldValueLanguage>();
        }

        #endregion
    }
}
