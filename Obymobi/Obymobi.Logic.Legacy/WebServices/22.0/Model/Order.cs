using System;
using System.Linq;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents an order
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Order"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class Order : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Order type
        /// </summary>
        public Order()
        {
            this.CustomerNameFull = "Unknown";
            this.AgeVerificationType = 0;
            this.MobileOrder = false;
        }

        #endregion

        #region Methods

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the order
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int OrderId
        { get; set; }

        /// <summary>
        /// Gets or sets a unique identifier
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Guid
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the customer
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int CustomerId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the client
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public int ClientId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the DeliverypointId
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForMobile]
		[IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public int DeliverypointId
        { get; set; }

        /// <summary>
        /// Gets or sets the number of the deliverypoint
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int DeliverypointNumber
        { get; set; }

        /// <summary>
        /// Get or sets the name of the deliverypoint caption (Table, Lane, Room, etc.).		 
        /// Not a great fan of this name, since it's called DeliverypointCaption on other places
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string DeliverypointName
        { get; set; }

        /// <summary>
        /// Gets or sets the status of the order
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int Status
        { get; set; }

        /// <summary>
        /// Gets or sets the text of the order status
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string StatusText
        { get; set; }

        /// <summary>
        /// Gets or sets the ErrorCode of the order 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public int ErrorCode
        { get; set; }

        /// <summary>
        /// Gets or sets the notes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Notes
        { get; set; }

        /// <summary>
        /// Gets or sets the benchmarkinformation
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string BenchmarkInformation
        { get; set; }

        /// <summary>
        /// Gets or sets the order price total for use in the mobile client
        /// </summary>
        public int MobileClientHistoryOrderPriceInTotal
        { get; set; }

        /// <summary>
        /// Gets or sets the full name of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string CustomerNameFull
        { get; set; }

        /// <summary>
        /// Gets or sets the phonenumber
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string CustomerPhonenumber
        { get; set; }

        /// <summary>
        /// Gets or sets the MAC address
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string ClientMacAddress
        { get; set; }

        /// <summary>
        /// Gets or sets the full name of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string CompanyName
        { get; set; }

        /// <summary>
        /// Gets or sets the full name of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string CompanyObycode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the paymentmethod
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string PaymentmethodName
        { get; set; }

        /// <summary>
        /// Gets or sets the sum of the PriceIn's of the Orderitems
        /// </summary>
        [XmlElement]
        public decimal PriceIn
        {
            get
            {
                return this.Orderitems.Sum(oi => oi.PriceIn);
            }
        }

        /// <summary>
        /// Gets or sets the type of order
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the text of the order type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string TypeText
        { get; set; }

        /// <summary>
        /// Gets or sets the confirmation code of the order
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public string ConfirmationCode
        { get; set; }

        /// <summary>
        /// Gets or sets the confirmation code of the order
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public int ConfirmationCodeDeliveryType
        { get; set; }

        /// <summary>
        /// Gets or sets the last time the order was updated
        /// </summary>
        [XmlElement]
        public string Updated
        { get; set; }

        /// <summary>
        /// Gets or sets the time the order was created
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Created
        { get; set; }

        /// <summary>
        /// Gets or sets the time the order is pending if it's not processed
        /// </summary>
        [XmlElement]
        public bool ProcessingExpired
        { get; set; }

        /// <summary>
        /// Gets or sets the phone info from which the order was submitted
        /// </summary>
        [XmlElement]
        public string PhoneInfo
        { get; set; }

        /// <summary>
        /// Gets or sets if logic has been performed to verify the age, if required
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public int AgeVerificationType
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether an error has been sent via e-mail
        /// </summary>		
        [XmlElement]
        public bool ErrorSentByEmail
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether an error has been sent via SMS
        /// </summary>		
        [XmlElement]
        public bool ErrorSentBySMS
        { get; set; }

        /// <summary>
        /// Processing time
        /// </summary>		
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public DateTime? Processed
        { get; set; }

        /// <summary>
        /// Placing time
        /// </summary>		
        [XmlElement]
        public DateTime? PlaceTime
        { get; set; }

        /// <summary>
        /// Get or sets the id of the support pool for this order
        /// </summary>
        [XmlElement]
        public int SupportpoolId
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether the order has been printed by the terminal
        /// </summary>		
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        public bool Printed
        { get; set; }

        /// <summary>
        /// Gets or sets the time the order was created
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public bool MobileOrder
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the pdf file
        /// </summary>        
        public byte[] File { get; set; }

        /// <summary>
        /// Gets or sets wether the order is processed by a customer with vip status
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]        
        public bool IsCustomerVip
        { get; set; }

        /// <summary>
        /// Gets or sets items of the order
        /// </summary>
        [XmlArray("Orderitems")]
        [XmlArrayItem("Orderitem")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public Orderitem[] Orderitems
        { get; set; }

        /// <summary>
        /// Gets or sets orderRoutestephandlers of the order
        /// Must be made because android threats a property of OrderRoutestephandler as a collection
        /// So it will return a collection in the order model. This solution is for JSON read purposes (CraveOnSiteServer.Protocol.ProtocolData.ReceiveSaveOrder - Line 509)
        /// </summary>        
        [XmlArray("OrderRoutestephandlers")]
        [XmlArrayItem("OrderRoutestephandler")]
        [IncludeInCodeGeneratorForAndroid]
        public OrderRoutestephandler[] OrderRoutestephandlers
        { get; set; }

        #endregion

        #region Custom Properties

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }

        /// <summary>
        /// Gets or sets the posorder.
        /// </summary>
        /// <value>
        /// The posorder.
        /// </value>
        [XmlElement]
        public Posorder Posorder
        { get; set; }

        /// <summary>
        /// Gets or sets the auto id.
        /// </summary>
        /// <value>
        /// The auto id.
        /// </value>
        [XmlElement]
        public UInt32 AutoId
        { get; set; }

        /// <summary>
        /// Gets or sets the created on server.
        /// </summary>
        /// <value>
        /// The created on server.
        /// </value>
        [XmlElement]
        public DateTime CreatedOnServer
        { get; set; }

        /// <summary>
        /// Gets or sets the time the order was created as DateTime
        /// </summary>
        /// <value>
        /// The created as date time.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public DateTime CreatedAsDateTime
        { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is webservice order.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is webservice order; otherwise, <c>false</c>.
        /// </value>
        [XmlIgnore]
        public bool IsWebserviceOrder
        { get; set; }

        /// <summary>
        /// Property is only used in the OnsiteServer to check if the order is saved on the webservice
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is saved on webservice; otherwise, <c>false</c>.
        /// </value>
        [XmlElement]
        public bool IsSavedOnWebservice
        { get; set; }

        /// <summary>
        /// DateTime when the Order was validated to be ready to be converted to a OrderEntity
        /// </summary>
        /// <value>
        /// The validated by order processing helper.
        /// </value>
        [XmlIgnore]
        public DateTime? ValidatedByOrderProcessingHelper
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets orderRoutestephandler of the order
        /// </summary>        
        public OrderRoutestephandler OrderRoutestephandler
        {
            get
            {
                OrderRoutestephandler orderRoutestephandler = null;
                if (this.OrderRoutestephandlers != null && this.OrderRoutestephandlers.Length > 0)
                    orderRoutestephandler = this.OrderRoutestephandlers[0];
                return orderRoutestephandler;
            }
            set
            {
                //if (this.OrderRoutestephandlers == null)
                this.OrderRoutestephandlers = new OrderRoutestephandler[1];
                this.OrderRoutestephandlers[0] = value;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Order Clone()
        {
            return this.Clone<Order>();
        }

        #endregion
    }
}