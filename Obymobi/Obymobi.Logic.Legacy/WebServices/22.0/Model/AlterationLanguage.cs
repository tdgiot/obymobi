using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v22
{
    /// <summary>
    /// Model class which represents a alteration language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "AlterationLanguage"), IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    public class AlterationLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.AlterationLanguage type
        /// </summary>
        public AlterationLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the alteration language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public AlterationLanguage Clone()
        {
            return this.Clone<AlterationLanguage>();
        }

        #endregion
    }
}
