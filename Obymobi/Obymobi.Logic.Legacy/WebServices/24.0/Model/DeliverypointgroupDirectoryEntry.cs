using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v24
{
    [Serializable, XmlRootAttribute(ElementName = "DeliverypointgroupDirectoryEntry"), IncludeInCodeGeneratorForMobile]
    public class DeliverypointgroupDirectoryEntry : ModelBase
    {
        public DeliverypointgroupDirectoryEntry()
        {
        }

        /// <summary>
        /// Gets or sets the deliverypointgroup id
        /// </summary>
        /// <value>The deliverypointgroup id.</value>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public int DeliverypointgroupId
        { get; set; }

        /// <summary>
        /// Gets or sets the order guid.
        /// </summary>
        /// <value>The order id.</value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string Name
        { get; set; }

        #region Methods

        public DeliverypointgroupDirectoryEntry Clone()
        {
            return this.Clone<DeliverypointgroupDirectoryEntry>();
        }

        #endregion
    }
}
