namespace Obymobi.Logic.Model.v24.Converters
{
	public class DeliveryTimeConverter : ModelConverterBase<Obymobi.Logic.Model.v24.DeliveryTime, Obymobi.Logic.Model.DeliveryTime>
	{
        public DeliveryTimeConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v25.Converters.DeliveryTimeConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v24.DeliveryTime ConvertModelToLegacyModel(Obymobi.Logic.Model.DeliveryTime source)
        {
            Obymobi.Logic.Model.v24.DeliveryTime target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v24.DeliveryTime();
                target.DeliverypointgroupId = source.DeliverypointgroupId;
                target.Time = source.Time;
            }

            return target;
        }

        public override Obymobi.Logic.Model.DeliveryTime ConvertLegacyModelToModel(Obymobi.Logic.Model.v24.DeliveryTime source)
        {
            Obymobi.Logic.Model.DeliveryTime target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.DeliveryTime();

                // Copy default values from new version
                new Obymobi.Logic.Model.v25.Converters.DeliveryTimeConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
