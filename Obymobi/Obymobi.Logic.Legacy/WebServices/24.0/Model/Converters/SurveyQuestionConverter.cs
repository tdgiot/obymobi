namespace Obymobi.Logic.Model.v24.Converters
{
	public class SurveyQuestionConverter : ModelConverterBase<Obymobi.Logic.Model.v24.SurveyQuestion, Obymobi.Logic.Model.SurveyQuestion>
	{
        public SurveyQuestionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v25.Converters.SurveyQuestionConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v24.SurveyQuestion ConvertModelToLegacyModel(Obymobi.Logic.Model.SurveyQuestion source)
        {
            Obymobi.Logic.Model.v24.SurveyQuestion target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v24.SurveyQuestion();
                target.SurveyQuestionId = source.SurveyQuestionId;
                target.Question = source.Question;
                target.Type = source.Type;
                target.Required = source.Required;
                target.SortOrder = source.SortOrder;
                target.ParentSurveyQuestionId = source.ParentSurveyQuestionId;
                target.NotesEnabled = source.NotesEnabled;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;

                if (source.SurveyAnswers != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v24.Converters.SurveyAnswerConverter surveyAnswersConverter = new Obymobi.Logic.Model.v24.Converters.SurveyAnswerConverter();
                    target.SurveyAnswers = (SurveyAnswer[])surveyAnswersConverter.ConvertArrayToLegacyArray(source.SurveyAnswers);
                }

                if (source.SurveyQuestionLanguages != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v24.Converters.SurveyQuestionLanguageConverter surveyQuestionLanguagesConverter = new Obymobi.Logic.Model.v24.Converters.SurveyQuestionLanguageConverter();
                    target.SurveyQuestionLanguages = (SurveyQuestionLanguage[])surveyQuestionLanguagesConverter.ConvertArrayToLegacyArray(source.SurveyQuestionLanguages);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.SurveyQuestion ConvertLegacyModelToModel(Obymobi.Logic.Model.v24.SurveyQuestion source)
        {
            Obymobi.Logic.Model.SurveyQuestion target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SurveyQuestion();

                // Copy default values from new version
                new Obymobi.Logic.Model.v25.Converters.SurveyQuestionConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
