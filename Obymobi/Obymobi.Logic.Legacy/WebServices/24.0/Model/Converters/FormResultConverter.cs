namespace Obymobi.Logic.Model.v24.Converters
{
	public class FormResultConverter : ModelConverterBase<Obymobi.Logic.Model.v24.FormResult, Obymobi.Logic.Model.FormResult>
	{
        public FormResultConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v25.Converters.FormResultConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v24.FormResult ConvertModelToLegacyModel(Obymobi.Logic.Model.FormResult source)
        {
            Obymobi.Logic.Model.v24.FormResult target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v24.FormResult();
                target.FormResultId = source.FormResultId;
                target.FormId = source.FormId;

                if (source.ResultFields != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v24.Converters.FormResultFieldConverter resultFieldsConverter = new Obymobi.Logic.Model.v24.Converters.FormResultFieldConverter();
                    target.ResultFields = (FormResultField[])resultFieldsConverter.ConvertArrayToLegacyArray(source.ResultFields);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.FormResult ConvertLegacyModelToModel(Obymobi.Logic.Model.v24.FormResult source)
        {
            Obymobi.Logic.Model.FormResult target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.FormResult();

                // Copy default values from new version
                new Obymobi.Logic.Model.v25.Converters.FormResultConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
