namespace Obymobi.Logic.Model.v24.Converters
{
	public class AlterationProductConverter : ModelConverterBase<Obymobi.Logic.Model.v24.AlterationProduct, Obymobi.Logic.Model.AlterationProduct>
	{
        public AlterationProductConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v25.Converters.AlterationProductConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v24.AlterationProduct ConvertModelToLegacyModel(Obymobi.Logic.Model.AlterationProduct source)
        {
            Obymobi.Logic.Model.v24.AlterationProduct target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v24.AlterationProduct();
                target.AlterationProductId = source.AlterationProductId;
                target.AlterationId = source.AlterationId;
                target.ProductId = source.ProductId;
                target.Visible = source.Visible;
                target.SortOrder = source.SortOrder;
            }

            return target;
        }

        public override Obymobi.Logic.Model.AlterationProduct ConvertLegacyModelToModel(Obymobi.Logic.Model.v24.AlterationProduct source)
        {
            Obymobi.Logic.Model.AlterationProduct target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.AlterationProduct();

                // Copy default values from new version
                new Obymobi.Logic.Model.v25.Converters.AlterationProductConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
