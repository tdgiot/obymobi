namespace Obymobi.Logic.Model.v24.Converters
{
	public class ScheduleConverter : ModelConverterBase<Obymobi.Logic.Model.v24.Schedule, Obymobi.Logic.Model.Schedule>
	{
        public ScheduleConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v25.Converters.ScheduleConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v24.Schedule ConvertModelToLegacyModel(Obymobi.Logic.Model.Schedule source)
        {
            Obymobi.Logic.Model.v24.Schedule target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v24.Schedule();
                target.ScheduleId = source.ScheduleId;
                target.Name = source.Name;

                if (source.Scheduleitems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v24.Converters.ScheduleitemConverter scheduleitemsConverter = new Obymobi.Logic.Model.v24.Converters.ScheduleitemConverter();
                    target.Scheduleitems = (Scheduleitem[])scheduleitemsConverter.ConvertArrayToLegacyArray(source.Scheduleitems);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Schedule ConvertLegacyModelToModel(Obymobi.Logic.Model.v24.Schedule source)
        {
            Obymobi.Logic.Model.Schedule target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Schedule();

                // Copy default values from new version
                new Obymobi.Logic.Model.v25.Converters.ScheduleConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
