namespace Obymobi.Logic.Model.v24.Converters
{
	public class DeliverypointConverter : ModelConverterBase<Obymobi.Logic.Model.v24.Deliverypoint, Obymobi.Logic.Model.Deliverypoint>
	{
        public DeliverypointConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v25.Converters.DeliverypointConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v24.Deliverypoint ConvertModelToLegacyModel(Obymobi.Logic.Model.Deliverypoint source)
        {
            Obymobi.Logic.Model.v24.Deliverypoint target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v24.Deliverypoint();
                target.DeliverypointId = source.DeliverypointId;
                target.Number = source.Number;
                target.CompanyId = source.CompanyId;
                target.Name = source.Name;

                if (source.Posdeliverypoint != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v24.Converters.PosdeliverypointConverter posdeliverypointConverter = new Obymobi.Logic.Model.v24.Converters.PosdeliverypointConverter();
                    target.Posdeliverypoint = (Posdeliverypoint)posdeliverypointConverter.ConvertModelToLegacyModel(source.Posdeliverypoint);
                }

                target.DeliverypointgroupId = source.DeliverypointgroupId;
                target.RoomControllerType = source.RoomControllerType;
                target.RoomControllerIp = source.RoomControllerIp;
                target.RoomControllerModbusSlaveId = source.RoomControllerModbusSlaveId;
                target.GooglePrinterId = source.GooglePrinterId;
                target.PrintFromMacAddress = source.PrintFromMacAddress;
                target.EnableAnalytics = source.EnableAnalytics;
                target.RoomControlConfigurationId = source.RoomControlConfigurationId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Deliverypoint ConvertLegacyModelToModel(Obymobi.Logic.Model.v24.Deliverypoint source)
        {
            Obymobi.Logic.Model.Deliverypoint target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Deliverypoint();

                // Copy default values from new version
                new Obymobi.Logic.Model.v25.Converters.DeliverypointConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
