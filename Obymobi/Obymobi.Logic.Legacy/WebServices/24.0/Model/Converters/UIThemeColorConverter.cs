namespace Obymobi.Logic.Model.v24.Converters
{
	public class UIThemeColorConverter : ModelConverterBase<Obymobi.Logic.Model.v24.UIThemeColor, Obymobi.Logic.Model.UIThemeColor>
	{
        public UIThemeColorConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v25.Converters.UIThemeColorConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v24.UIThemeColor ConvertModelToLegacyModel(Obymobi.Logic.Model.UIThemeColor source)
        {
            Obymobi.Logic.Model.v24.UIThemeColor target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v24.UIThemeColor();
                target.UIThemeColorId = source.UIThemeColorId;
                target.UIThemeId = source.UIThemeId;
                target.Type = source.Type;
                target.Color = source.Color;
            }

            return target;
        }

        public override Obymobi.Logic.Model.UIThemeColor ConvertLegacyModelToModel(Obymobi.Logic.Model.v24.UIThemeColor source)
        {
            Obymobi.Logic.Model.UIThemeColor target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UIThemeColor();

                // Copy default values from new version
                new Obymobi.Logic.Model.v25.Converters.UIThemeColorConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
