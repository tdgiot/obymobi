namespace Obymobi.Logic.Model.v24.Converters
{
	public class SurveyQuestionLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v24.SurveyQuestionLanguage, Obymobi.Logic.Model.SurveyQuestionLanguage>
	{
        public SurveyQuestionLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v25.Converters.SurveyQuestionLanguageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v24.SurveyQuestionLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.SurveyQuestionLanguage source)
        {
            Obymobi.Logic.Model.v24.SurveyQuestionLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v24.SurveyQuestionLanguage();
                target.SurveyQuestionLanguageId = source.SurveyQuestionLanguageId;
                target.SurveyQuestionId = source.SurveyQuestionId;
                target.LanguageCode = source.LanguageCode;
                target.Question = source.Question;
            }

            return target;
        }

        public override Obymobi.Logic.Model.SurveyQuestionLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v24.SurveyQuestionLanguage source)
        {
            Obymobi.Logic.Model.SurveyQuestionLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.SurveyQuestionLanguage();

                // Copy default values from new version
                new Obymobi.Logic.Model.v25.Converters.SurveyQuestionLanguageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
