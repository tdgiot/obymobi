namespace Obymobi.Logic.Model.v24.Converters
{
	public class UIWidgetLanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v24.UIWidgetLanguage, Obymobi.Logic.Model.UIWidgetLanguage>
	{
        public UIWidgetLanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v25.Converters.UIWidgetLanguageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v24.UIWidgetLanguage ConvertModelToLegacyModel(Obymobi.Logic.Model.UIWidgetLanguage source)
        {
            Obymobi.Logic.Model.v24.UIWidgetLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v24.UIWidgetLanguage();
                target.UIWidgetLanguageId = source.UIWidgetLanguageId;
                target.LanguageCode = source.LanguageCode;
                target.Caption = source.Caption;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
            }

            return target;
        }

        public override Obymobi.Logic.Model.UIWidgetLanguage ConvertLegacyModelToModel(Obymobi.Logic.Model.v24.UIWidgetLanguage source)
        {
            Obymobi.Logic.Model.UIWidgetLanguage target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UIWidgetLanguage();

                // Copy default values from new version
                new Obymobi.Logic.Model.v25.Converters.UIWidgetLanguageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
