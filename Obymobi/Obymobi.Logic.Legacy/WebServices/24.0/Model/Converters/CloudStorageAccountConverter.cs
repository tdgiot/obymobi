namespace Obymobi.Logic.Model.v24.Converters
{
	public class CloudStorageAccountConverter : ModelConverterBase<Obymobi.Logic.Model.v24.CloudStorageAccount, Obymobi.Logic.Model.CloudStorageAccount>
	{
        public CloudStorageAccountConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v25.Converters.CloudStorageAccountConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v24.CloudStorageAccount ConvertModelToLegacyModel(Obymobi.Logic.Model.CloudStorageAccount source)
        {
            Obymobi.Logic.Model.v24.CloudStorageAccount target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v24.CloudStorageAccount();
                target.CloudStorageAccountId = source.CloudStorageAccountId;
                target.Type = source.Type;
                target.AccountName = source.AccountName;
                target.AccountPassword = source.AccountPassword;
            }

            return target;
        }

        public override Obymobi.Logic.Model.CloudStorageAccount ConvertLegacyModelToModel(Obymobi.Logic.Model.v24.CloudStorageAccount source)
        {
            Obymobi.Logic.Model.CloudStorageAccount target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.CloudStorageAccount();

                // Copy default values from new version
                new Obymobi.Logic.Model.v25.Converters.CloudStorageAccountConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
