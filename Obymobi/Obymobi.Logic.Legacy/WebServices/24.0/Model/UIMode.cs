using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Newtonsoft.Json;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// Model class which represents a UIMode item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "UIMode"), IncludeInCodeGeneratorForAndroid]
    public class UIMode : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.UIMode type
        /// </summary>
        public UIMode()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the UIMode
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
		[JsonProperty("uimodeid")]
        public int UIModeId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not to show the service options button
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool ShowServiceOptions
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not to show the request bill button
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool ShowRequestBill
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not to show the deliverypoint number
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool ShowDeliverypoint
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not to show the clock
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool ShowClock
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not to show the battery indicator
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool ShowBattery
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not to show the brightness button
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool ShowBrightness
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not to show the fullscreen eyecatcher
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool ShowFullscreenEyecatcher
        { get; set; }

        /// <summary>
        /// Gets or sets whether or not to show the logo in the footerbar (Emenu)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool ShowFooterLogo { get; set; }

        /// <summary>
        /// Gets or sets the type of the UI mode
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the default UI Tab Id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DefaultUITabId
        { get; set; }

        /// <summary>
        /// Gets or sets the tabs
        /// </summary>
        [XmlArray("UITabs")]
        [XmlArrayItem("UITab")]
        [IncludeInCodeGeneratorForAndroid]
		[JsonProperty("uitabs")]
        public UITab[] UITabs
        { get; set; }

        [XmlArray("UIFooterItems"), XmlArrayItem("UIFooterItem")]
        [IncludeInCodeGeneratorForAndroid]
        public UIFooterItem[] UIFooterItems { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UIMode Clone()
        {
            return this.Clone<UIMode>();
        }

        #endregion
    }
}
