using System;
using System.Xml.Serialization;
using Dionysos;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// Contains a collection of data required for the PosIntegration
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "PosIntegrationInformation")]
    public class PosIntegrationInformation : ModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PosIntegrationInformation"/> class.
        /// </summary>
        public PosIntegrationInformation()
        {
            this.BatteryLowProductId = string.Empty;
            this.UnlockDeliverypointProductId = string.Empty;
            this.ClientDisconnectedProductId = string.Empty;
            this.SystemMessagesDeliverypointId = string.Empty;
            this.AltSystemMessagesDeliverypointId = string.Empty;
            "test".FormatSafe("asdf");
            this.POSConnectorType = (int)Obymobi.Enums.POSConnectorType.Unknown;
        }

        /// <summary>
        /// Gets or sets the products.
        /// </summary>
        /// <value>
        /// The products.
        /// </value>
        [XmlArray("Products")]
        [XmlArrayItem("Product")]
        public Product[] Products { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>
        /// The category.
        /// </value>
        [XmlArray("Categories")]
        [XmlArrayItem("Category")]
        public Category[] Categories { get; set; }

        /// <summary>
        /// Gets or sets the deliverypoint groups.
        /// </summary>
        /// <value>
        /// The deliverypoint groups.
        /// </value>
        [XmlArray("Deliverypoints")]
        [XmlArrayItem("Deliverypoint")]
        public Deliverypoint[] Deliverypoints { get; set; }

        /// <summary>
        /// Gets or sets the terminal.
        /// </summary>
        /// <value>
        /// The terminal.
        /// </value>
        [XmlElement]
        public Terminal Terminal { get; set; }

        /// <summary>
        /// Gets or sets the int representing of the POS connector enum value.
        /// </summary>
        /// <value>
        /// The type of the POS connector.
        /// </value>
        [XmlElement]
        public int POSConnectorType { get; set; }

        /// <summary>
        /// Gets or sets the unlock deliverypoint product id.
        /// </summary>
        /// <value>
        /// The unlock deliverypoint product id.
        /// </value>
        [XmlElement]
        public string UnlockDeliverypointProductId { get; set; }

        /// <summary>
        /// Gets or sets the battery low product id.
        /// </summary>
        /// <value>
        /// The battery low product id.
        /// </value>
        [XmlElement]
        public string BatteryLowProductId { get; set; }

        /// <summary>
        /// Gets or sets the client disconnected product id.
        /// </summary>
        /// <value>
        /// The client disconnected product id.
        /// </value>
        [XmlElement]
        public string ClientDisconnectedProductId { get; set; }

        /// <summary>
        /// Gets or sets the system messages deliverypoint.
        /// </summary>
        /// <value>
        /// The system messages deliverypoint.
        /// </value>
        [XmlElement]
        public string SystemMessagesDeliverypointId { get; set; }

        /// <summary>
        /// Gets or sets the alternative system messages deliverypoint.
        /// </summary>
        /// <value>
        /// The alternative system messages deliverypoint.
        /// </value>
        [XmlElement]
        public string AltSystemMessagesDeliverypointId { get; set; }

        #region Icrtouch Specific

        /// <summary>
        /// Gets or sets the icrtouchprintermapping.
        /// </summary>
        /// <value>
        /// The icrtouchprintermapping.
        /// </value>
        [XmlElement]
        public Icrtouchprintermapping Icrtouchprintermapping { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public PosIntegrationInformation Clone()
        {
            return this.Clone<PosIntegrationInformation>();
        }

        #endregion
    }
}
