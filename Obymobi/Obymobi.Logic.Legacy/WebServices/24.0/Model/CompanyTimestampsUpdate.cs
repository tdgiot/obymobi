using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Attributes;
using System.Xml.Serialization;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// CompanyTimestampsUpdate model
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CompanyTimestampsUpdate"), IncludeInCodeGeneratorForMobile]
    public class CompanyTimestampsUpdate : ModelBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyTimestampsUpdate"/> class.
        /// </summary>
        public CompanyTimestampsUpdate()
        {
        }

        #region Properties

        /// <summary>
        /// Gets or sets the company id.
        /// </summary>
        /// <value>
        /// The company id.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public int CompanyId
        { get; set; }


        /// <summary>
        /// Gets or sets the company data last modified ticks.
        /// </summary>
        /// <value>
        /// The company data last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string CompanyDataLastModifiedTicks
        { get; set; }


        /// <summary>
        /// Gets or sets the company media last modified ticks.
        /// </summary>
        /// <value>
        /// The company media last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string CompanyMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the menu data last modified ticks.
        /// </summary>
        /// <value>
        /// The menu data last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string MenuDataLastModifiedTicks
        { get; set; }


        /// <summary>
        /// Gets or sets the menu media last modified ticks.
        /// </summary>
        /// <value>
        /// The menu media last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string MenuMediaLastModifiedTicks
        { get; set; }


        /// <summary>
        /// Gets or sets the pos integration information last modified ticks.
        /// </summary>
        /// <value>
        /// The pos integration information last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string PosIntegrationInformationLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the deliverypoint data last modified ticks.
        /// </summary>
        /// <value>
        /// The deliverypoint data last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string DeliverypointDataLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the survey data last modified ticks.
        /// </summary>
        /// <value>
        /// The survey data last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string SurveyDataLastModifiedTicks
        { get; set; }


        /// <summary>
        /// Gets or sets the survey media last modified ticks.
        /// </summary>
        /// <value>
        /// The survey media last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string SurveyMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the announcement data last modified ticks.
        /// </summary>
        /// <value>
        /// The announcement data last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string AnnouncementDataLastModifiedTicks
        { get; set; }


        /// <summary>
        /// Gets or sets the announcement media last modified ticks.
        /// </summary>
        /// <value>
        /// The announcement media last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string AnnouncementMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the entertainment data last modified ticks.
        /// </summary>
        /// <value>
        /// The menu entertainment last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string EntertainmentDataLastModifiedTicks
        { get; set; }


        /// <summary>
        /// Gets or sets the entertainment media last modified ticks.
        /// </summary>
        /// <value>
        /// The entertainment media last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string EntertainmentMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the advertisement data last modified ticks.
        /// </summary>
        /// <value>
        /// The advertisement data last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string AdvertisementDataLastModifiedTicks
        { get; set; }


        /// <summary>
        /// Gets or sets the advertisement media last modified ticks.
        /// </summary>
        /// <value>
        /// The advertisement media last modified ticks.
        /// </value>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        public string AdvertisementMediaLastModifiedTicks
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CompanyTimestampsUpdate Clone()
        {
            return this.Clone<CompanyTimestampsUpdate>();
        }

        #endregion
    }
}
