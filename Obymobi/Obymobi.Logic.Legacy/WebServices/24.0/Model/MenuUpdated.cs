using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// Model class which represents a menu updated
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "MenuUpdated"), IncludeInCodeGeneratorForAndroid]
    public class MenuUpdated : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.MenuUpdated type
        /// </summary>
        public MenuUpdated()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// The updated productmenu items
        /// </summary>
        [XmlArray("ProductmenuItems")]
        [XmlArrayItem("ProductmenuItem")]
        [IncludeInCodeGeneratorForAndroid]
        public ProductmenuItem[] ProductmenuItems
        { get; set; }

        /// <summary>
        /// The action to perform on the productmenu items
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MenuUpdatedAction
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public MenuUpdated Clone()
        {
            return this.Clone<MenuUpdated>();
        }

        #endregion
    }
}
