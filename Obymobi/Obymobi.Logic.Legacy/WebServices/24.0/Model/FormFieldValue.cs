using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// Model class which represents a form field
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "FormFieldValue")]
    public class FormFieldValue : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.FormField type
        /// </summary>
        public FormFieldValue()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets of sets the id of the form field value
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int FormFieldValueId
        { get; set;}

        /// <summary>
        /// Gets or sets the id of the form field
        /// </summary>
        [XmlElement]
        public int FormFieldId
        { get; set; }

        /// <summary>
        /// Gets or sets the value of the form field value
        /// </summary>
        [XmlElement]
        public string Value
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [XmlArray("FormFieldValueLanguages")]
        [XmlArrayItem("FormFieldValueLanguage")]
        public FormFieldValueLanguage[] FormFieldValueLanguages
        { get; set; }
    
        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public FormFieldValue Clone()
        {
            return this.Clone<FormFieldValue>();
        }

        #endregion
    }
}
