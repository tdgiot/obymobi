using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v24
{
    /// <summary>
    /// Model class which represents a product suggestion
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Attachment"), IncludeInCodeGeneratorForXamarin]
    public class Attachment : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.ProductSuggestion type
        /// </summary>
        public Attachment()
        {
        }

        #endregion

        #region Xml Properties

         /// <summary>
        /// Gets or sets the id of the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ProductId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int PageId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }
        
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int Type
        { get; set; }
        
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Url
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string AllowedDomains
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool AllowAllDomains
        { get; set; }

        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        [IncludeInCodeGeneratorForXamarin]
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        public Media[] Media
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Attachment Clone()
        {
            return this.Clone<Attachment>();
        }

        #endregion
    }
}
