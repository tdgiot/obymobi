using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v24
{
    /// <summary>
    /// Model class which represents an alteration item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Alterationitem"), IncludeInCodeGeneratorForXamarin]
    public class Alterationitem : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Alterationitem type
        /// </summary>
        public Alterationitem()
        {

        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the alteration item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationitemId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationId
        { get; set; }

        /// <summary>
        /// Gets or sets a unique identifier
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Guid
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string AlterationName
        { get; set; }

        /// <summary>
        /// Gets or sets the alteration type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationType
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationoptionId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string AlterationoptionName
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of the alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public decimal AlterationoptionPriceIn
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the alteration
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool SelectedOnDefault
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of the alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets selected time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public DateTime Time
        { get; set; }

        /// <summary>
        /// Gets or sets the free text value
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Value
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Alterationitem Clone()
        {
            return this.Clone<Alterationitem>();
        }

        #endregion
    }
}
