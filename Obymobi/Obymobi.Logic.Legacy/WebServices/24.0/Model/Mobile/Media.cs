using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v24
{
    /// <summary>
    /// Model class which represents a media item
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Media"), IncludeInCodeGeneratorForXamarin]
    public class Media : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Media type
        /// </summary>
        public Media()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the media item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int MediaId
        { get; set; }

        /// <summary>
        /// Gets or sets the url to the media path
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Url
        { get; set; }

        /// <summary>
        /// Gets or sets the filename of the media item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Filename
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the media item
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int MediaType
        { get; set; }

        /// <summary>
        /// Gets or sets the Path relative to the Media-path and Cdn-url (if you replace "\\" with "/")
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string CdnPath
        { get; set; }

        /// <summary>
        /// Gets or sets the action product id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ActionProductId
        { get; set; }

        public bool ShouldSerializeActionProductId() { return this.ActionProductId > 0; }

        /// <summary>
        /// Gets or sets the action category id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ActionCategoryId
        { get; set; }

        public bool ShouldSerializeActionCategoryId() { return this.ActionCategoryId > 0; }

        /// <summary>
        /// Gets or sets the action entertainment id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ActionEntertainmentId
        { get; set; }

        public bool ShouldSerializeActionEntertainmentId() { return this.ActionEntertainmentId > 0; }

        /// <summary>
        /// Gets or sets the action entertainmentcategory id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ActionEntertainmentcategoryId
        { get; set; }

        public bool ShouldSerializeActionEntertainmentcategoryId() { return this.ActionEntertainmentcategoryId > 0; }

        /// <summary>
        /// Gets or sets the action url
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string ActionUrl
        { get; set; }

        /// <summary>
        /// Gets or sets the size mode
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SizeMode
        { get; set; }

        /// <summary>
        /// Gets or sets the zoom level
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ZoomLevel
        { get; set; }

        /// <summary>
        /// Parent of this media
        /// </summary>
        [XmlAttribute("Parent")]
        public string Parent
        { get; set; }

        /// <summary>
        /// Gets or sets the agnostic media id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int AgnosticMediaId
        { get; set; }

        /// <summary>
        /// Gets or sets the action site id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ActionSiteId
        { get; set; }

        /// <summary>
        /// Gets or sets the action page id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ActionPageId
        { get; set; }

        /// <summary>
        /// Array of cultures this image can be used for. If none, image is used as language agnotic
        /// </summary>
        [XmlArray("MediaCultures")]
        [XmlArrayItem("MediaCulture")]
        [IncludeInCodeGeneratorForXamarin]
        public MediaCulture[] MediaCultures { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Media Clone()
        {
            return this.Clone<Media>();
        }

        #endregion

    }
}
