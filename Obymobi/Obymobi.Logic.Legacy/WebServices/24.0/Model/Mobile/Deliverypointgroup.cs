using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v24
{
    /// <summary>
    /// Model class which represents a deliverypoint group
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Deliverypointgroup"), IncludeInCodeGeneratorForXamarin]
    public class Deliverypointgroup : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Deliverypointgroup type
        /// </summary>
        public Deliverypointgroup()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the deliverypoint group
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int DeliverypointgroupId
        { get; set; }

        /// <summary>
        /// Gets or sets the company id of the deliverypoint group
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the deliverypoint group
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets if the order history dialog should be displayed on the emenu
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool OrderHistoryDialogEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets the menu id of the deliverypoint group
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int MenuId
        { get; set; }

        /// <summary>
        /// Gets or sets the caption of the deliverypoints i.e. Room or Table
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string DeliverypointCaption
        { get; set; }

        /// <summary>
        /// Gets or sets the homepage slideshow interval
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int HomepageSlideshowInterval
        { get; set; }

        /// <summary>
        /// Gets or sets if geofencing is enabled
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool GeoFencingEnabled
        { get; set; }

        /// <summary>
        /// Gets or sets if geofencing is enabled
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int GeoFencingRadius
        { get; set; }

        /// <summary>
        /// Gets or sets if geofencing is enabled
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool HideCompanyDetails
        { get; set; }

        /// <summary>
        /// Gets or sets the UIMode
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public UIMode[] UIModes
        { get; set; }

        /// <summary>
        /// Gets or sets the deliverypoints of the deliverypoint group
        /// </summary>
        [XmlArray("Deliverypoints")]
        [XmlArrayItem("Deliverypoint")]
        [IncludeInCodeGeneratorForXamarin]
        public Deliverypoint[] Deliverypoints
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the deliverypoint group
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForXamarin]
        public Media[] Media
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Deliverypointgroup Clone()
        {
            return this.Clone<Deliverypointgroup>();
        }

        #endregion
    }
}
