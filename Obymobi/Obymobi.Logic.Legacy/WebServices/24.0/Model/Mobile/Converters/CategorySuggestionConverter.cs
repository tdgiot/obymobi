using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v24.Converters
{
	public class CategorySuggestionConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v24.CategorySuggestion, Obymobi.Logic.Model.Mobile.CategorySuggestion>
	{
        public CategorySuggestionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v25.Converters.CategorySuggestionConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v24.CategorySuggestion ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.CategorySuggestion source)
        {
            Obymobi.Logic.Model.Mobile.v24.CategorySuggestion target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v24.CategorySuggestion();
                target.CategoryId = source.CategoryId;
                target.ProductId = source.ProductId;
                target.SortOrder = source.SortOrder;
                target.Checkout = source.Checkout;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.CategorySuggestion ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v24.CategorySuggestion source)
        {
            Obymobi.Logic.Model.Mobile.CategorySuggestion target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.CategorySuggestion();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v25.Converters.CategorySuggestionConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
