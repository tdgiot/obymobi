using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v24.Converters
{
	public class DeliverypointgroupConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v24.Deliverypointgroup, Obymobi.Logic.Model.Mobile.Deliverypointgroup>
	{
        public DeliverypointgroupConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v25.Converters.DeliverypointgroupConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v24.Deliverypointgroup ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Deliverypointgroup source)
        {
            Obymobi.Logic.Model.Mobile.v24.Deliverypointgroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v24.Deliverypointgroup();
                target.DeliverypointgroupId = source.DeliverypointgroupId;
                target.CompanyId = source.CompanyId;
                target.Name = source.Name;
                target.OrderHistoryDialogEnabled = source.OrderHistoryDialogEnabled;
                target.MenuId = source.MenuId;
                target.DeliverypointCaption = source.DeliverypointCaption;
                target.HomepageSlideshowInterval = source.HomepageSlideshowInterval;
                target.GeoFencingEnabled = source.GeoFencingEnabled;
                target.GeoFencingRadius = source.GeoFencingRadius;
                target.HideCompanyDetails = source.HideCompanyDetails;

                if (source.UIModes != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v24.Converters.UIModeConverter uIModesConverter = new Obymobi.Logic.Model.Mobile.v24.Converters.UIModeConverter();
                    target.UIModes = (UIMode[])uIModesConverter.ConvertArrayToLegacyArray(source.UIModes);
                }

                if (source.Deliverypoints != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v24.Converters.DeliverypointConverter deliverypointsConverter = new Obymobi.Logic.Model.Mobile.v24.Converters.DeliverypointConverter();
                    target.Deliverypoints = (Deliverypoint[])deliverypointsConverter.ConvertArrayToLegacyArray(source.Deliverypoints);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v24.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.Mobile.v24.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v24.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.Mobile.v24.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Deliverypointgroup ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v24.Deliverypointgroup source)
        {
            Obymobi.Logic.Model.Mobile.Deliverypointgroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Deliverypointgroup();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v25.Converters.DeliverypointgroupConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
