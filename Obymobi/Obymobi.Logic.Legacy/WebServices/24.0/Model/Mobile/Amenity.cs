using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v24
{
    /// <summary>
    /// Model class which represents an amenity
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Amenity"), IncludeInCodeGeneratorForXamarin]
    public class Amenity : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Amenity type
        /// </summary>
        public Amenity()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the amenity
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int AmenityId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the amenity
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Amenity Clone()
        {
            return this.Clone<Amenity>();
        }

        #endregion
    }
}
