using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.Mobile.v24
{
    /// <summary>
    /// Model class which represents a company for the mobile solution
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Page"), IncludeInCodeGeneratorForXamarin]
    public class Page : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Company type
        /// </summary>
        public Page()
        {
        }

        #endregion

        #region Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int PageId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SiteId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int ParentPageId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int PageType
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int SortOrder
        { get; set; }        

        [IncludeInCodeGeneratorForXamarin]
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        public Media[] Media
        { get; set; }

        [IncludeInCodeGeneratorForXamarin]
        [XmlArray("PageElements")]
        [XmlArrayItem("PageElement")]
        public PageElement[] PageElements
        { get; set; }

        [XmlArray("Attachments")]
        [XmlArrayItem("Attachment")]
        [IncludeInCodeGeneratorForXamarin]
        public Attachment[] Attachments
        { get; set; }

        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        public Company Clone()
        {
            return this.Clone<Company>();
        }

        public Company CloneUsingBinaryFormatter()
        {
            return this.CloneUsingBinaryFormatter<Company>();
        }

        #endregion
    }
}
