using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// Model class which represents an advertisement
    /// </summary>
    [Serializable, XmlRoot(ElementName = "MessageTemplate"), IncludeInCodeGeneratorForAndroid]
    public class MessageTemplate : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.MessageTemplate type
        /// </summary>
        public MessageTemplate()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the message template
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int MessageTemplateId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the message template
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the title of the message template
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Title 
        { get; set; }

        /// <summary>
        /// Gets or sets the message of the template
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Message 
        { get; set; }

        /// <summary>
        /// Gets or sets the company id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int CompanyId 
        { get; set; }

        /// <summary>
        /// Gets or sets the duration of the message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Duration
        { get; set; }

        /// <summary>
        /// Gets or sets the media id of the message image
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MediaId
        { get; set; }

        /// <summary>
        /// Gets or sets the category id of the message button event
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int CategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the entertainment id of the message button event
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the product id of the message button event
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets message layout type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MessageLayoutType
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public MessageTemplate Clone()
        {
            return this.Clone<MessageTemplate>();
        }

        #endregion

    }
}
