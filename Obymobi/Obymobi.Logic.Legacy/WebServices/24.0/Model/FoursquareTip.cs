using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// Model class which represents a collection of information about a tip posted on FourSquare
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "FoursquareTip")]
    public class FoursquareTip : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.FoursquareTip type
        /// </summary>
        public FoursquareTip()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the name of the user that published the tip
        /// </summary>
        [XmlElement]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the date/time of the tip
        /// </summary>
        [XmlElement]
        public string PostedOn
        { get; set; }

        /// <summary>
        /// Gets or sets the text body of the tip
        /// </summary>
        [XmlElement]
        public string Text
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public FoursquareTip Clone()
        {
            return this.Clone<FoursquareTip>();
        }

        #endregion
    }
}
