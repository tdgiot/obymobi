using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// Supportpool model
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Supportpool")]
    public class Supportpool : ModelBase
    {
        /// <summary>
        /// Gets or sets the id of the support pool
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int SupportpoolId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the support pool
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the phonenumber of the support pool
        /// </summary>
        [XmlElement]
        public string Phonenumber
        { get; set; }
        
        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Supportpool Clone()
        {
            return this.Clone<Supportpool>();
        }

        #endregion

    }
}
