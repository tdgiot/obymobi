using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// Model class which represents an orderroutestephandler
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "OrderRoutestephandler"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile]
    public class OrderRoutestephandler : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.OrderRoutestephandler type
        /// </summary> 
        public OrderRoutestephandler()
        {

        }

        #endregion

        #region Methods

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the order routestephandler
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int OrderRoutestephandlerId
        { get; set; }

        /// <summary>
        /// Gets or sets guid
        /// </summary>
        [XmlElement]
        public string Guid
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the order
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int OrderId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the terminal
        /// </summary>
        [XmlElement, ForeignKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int TerminalId
        { get; set; }

        /// <summary>
        /// Gets or sets the number of the order routestephandler
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int Number
        { get; set; }

        /// <summary>
        /// Gets or sets the handlerType of the order routestephandler
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int HandlerType
        { get; set; }

        /// <summary>
        /// Gets or sets the printReportType of the order routestephandler
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int PrintReportType
        { get; set; }

        /// <summary>
        /// Gets or sets the status of the order routestephandler
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int Status
        { get; set; }

        /// <summary>
        /// Gets or sets the error of the order routestephandler (OrderProcessingError)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int Error
        { get; set; }

        /// <summary>
        /// Completed datetime
        /// </summary>		
        [XmlElement]
        public DateTime? Completed
        { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[XmlElement]
		public int Timeout
		{ get; set; }

		/// <summary>
		/// 
		/// </summary>
		[XmlElement]
		public DateTime? TimeoutExpires
		{ get; set; }

        /// <summary>
        /// 
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ForwardedFromTerminalId
        { get; set; }
		
        #endregion

        #region Custom Properties

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }

        #endregion
    }
}
