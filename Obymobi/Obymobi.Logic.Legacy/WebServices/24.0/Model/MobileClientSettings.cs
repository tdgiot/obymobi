using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v24
{
	/// <summary>
	/// Class to be used for saving configuration settings on the client. 
	/// Could also be used to backup the settings to the server.
	/// </summary>
	[Serializable, XmlRootAttribute(ElementName = "MobileClientSettings")]
	public class MobileClientSettings : ModelBase
	{
		#region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.MobileClientSettings type
        /// </summary>
        public MobileClientSettings()
        {
        }

        #endregion

        #region Properties

		/// <summary>
		/// Gets or sets the id settings (should always be 1)
		/// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
		public int MobileClientSettingsId
		{ get; set; }

        /// <summary>
        /// Gets or sets saved phonenumber
        /// </summary>
        [XmlElement]
        public string SavedPhonenumber
        { get; set; }

		/// <summary>
		/// Gets or sets the saved password
		/// </summary>
		[XmlElement]
		public string SavedHashedPassword
		{ get; set; }

		/// <summary>
		/// Gets or sets a RandomValue
		/// </summary>
		[XmlElement]
		public string RandomValue
		{ get; set; }

		/// <summary>
		/// Gets or sets if the user has signed on before
		/// </summary>
		[XmlElement]
		public int HasSignedOnBefore
		{ get; set; }

		/// <summary>
		/// Gets or sets the last trace on the mobile
		/// </summary>
		[XmlElement]
		public string LastTrace
		{ get; set; }

		/// <summary>
		/// Gets or sets the last trace on the mobile
		/// </summary>
		[XmlElement]
		public int LastPaymentMethodId
		{ get; set; }

		/// <summary>
		/// Gets or sets the last trace on the mobile
		/// </summary>
		[XmlElement]
		public int LastDeliverypointNumber
		{ get; set; }

		/// <summary>
		/// Gets or sets the last trace on the mobile
		/// </summary>
		[XmlElement]
		public int LastCompanyId
		{ get; set; }

		/// <summary>
		/// Gets or sets the last trace on the mobile
		/// </summary>
		[XmlElement]
		public string KeepConnectionAlive
		{ get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public MobileClientSettings Clone()
        {
            return this.Clone<MobileClientSettings>();
        }

        #endregion
	}
}
