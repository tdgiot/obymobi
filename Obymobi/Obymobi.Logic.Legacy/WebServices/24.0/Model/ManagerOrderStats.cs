using Newtonsoft.Json;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// ManagerOrderStats model
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class ManagerOrderStats
    {
        #region Properties

        /// <summary>
        /// Gets or sets the total orders.
        /// </summary>
        /// <value>
        /// The total orders.
        /// </value>
        [JsonProperty]
        public int TotalOrders { get; set; }

        /// <summary>
        /// Gets or sets the total orders today.
        /// </summary>
        /// <value>
        /// The total orders today.
        /// </value>
        [JsonProperty]
        public int TotalOrdersToday { get; set; }

        /// <summary>
        /// Gets or sets the total orders today diff.
        /// </summary>
        /// <value>
        /// The total orders today diff.
        /// </value>
        [JsonProperty]
        public int TotalOrdersTodayDiff { get; set; }

        /// <summary>
        /// Gets or sets the total orders week.
        /// </summary>
        /// <value>
        /// The total orders week.
        /// </value>
        [JsonProperty]
        public int TotalOrdersWeek { get; set; }

        /// <summary>
        /// Gets or sets the total orders week diff.
        /// </summary>
        /// <value>
        /// The total orders week diff.
        /// </value>
        [JsonProperty]
        public int TotalOrdersWeekDiff { get; set; }

        /// <summary>
        /// Gets or sets the total orders month.
        /// </summary>
        /// <value>
        /// The total orders month.
        /// </value>
        [JsonProperty]
        public int TotalOrdersMonth { get; set; }

        /// <summary>
        /// Gets or sets the total orders month diff.
        /// </summary>
        /// <value>
        /// The total orders month diff.
        /// </value>
        [JsonProperty]
        public int TotalOrdersMonthDiff { get; set; }

        /// <summary>
        /// Gets or sets the revenue total day.
        /// </summary>
        /// <value>
        /// The revenue total day.
        /// </value>
        [JsonProperty]
        public double RevenueTotalDay { get; set; }

        /// <summary>
        /// Gets or sets the revenue total week.
        /// </summary>
        /// <value>
        /// The revenue total week.
        /// </value>
        [JsonProperty]
        public double RevenueTotalWeek { get; set; }

        #endregion
    }
}
