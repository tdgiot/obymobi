using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Interfaces;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// Folio model
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Folio"), IncludeInCodeGeneratorForAndroid]
    public class Folio : ModelBase, IPmsModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Folio"/> class.
        /// </summary>        
        public Folio()
        {
        }

        [XmlIgnore]
        public string Name { get { return this.GetType().Name; } }

        /// <summary>
        /// The DeliverypointNumber, i.e. RoomNumber
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string DeliverypointNumber { get; set; }

        /// <summary>
        /// Account number
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Error description in an error occured retrieving the folio
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Error { get; set; }

        /// <summary>
        /// Balance
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public decimal Balance { get; set; }

        /// <summary>
        /// Gets or sets the folio items.
        /// </summary>
        /// <value>
        /// The folio items.
        /// </value>
        [XmlArray("FolioItems")]
        [XmlArrayItem("FolioItem")]
        [IncludeInCodeGeneratorForAndroid]
        public FolioItem[] FolioItems { get; set; }
    }
}
