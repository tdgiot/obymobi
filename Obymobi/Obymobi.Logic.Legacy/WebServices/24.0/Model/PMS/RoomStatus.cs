using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// Folio model
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "RoomStatus"), IncludeInCodeGeneratorForAndroid]
    public class RoomStatus : ModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RoomStatus"/> class.
        /// </summary>        
        public RoomStatus()
        {
        }

        /// <summary>
        /// The DeliverypointNumber, i.e. RoomNumber
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string DeliverypointNumber { get; set; }

        /// <summary>
        /// Extension
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Extension { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public int CleaningStatus { get; set; }

        /// <summary>
        /// Account number
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string AccountNumber { get; set; }       
    }
}
