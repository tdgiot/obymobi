using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v24
{
    /// <summary>
    /// Model class which represents an alteration option
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Alterationoption"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForJavascript, IncludeInCodeGeneratorForXamarin]
    public class Alterationoption : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Alterationoption type
        /// </summary>
        public Alterationoption()
        {
            AlterationoptionId = 0;
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the alteration option
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int AlterationoptionId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the price addition of the alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public decimal PriceIn
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the alteration option
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets PosproductId
        /// </summary>
        [XmlElement]
        public int PosproductId
        { get; set; }

        /// <summary>
        /// Gets or sets PosproductId
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public bool IsProductRelated
        { get; set; }

        /// <summary>
        /// Gets or sets the posalterationoption.
        /// </summary>
        /// <value>
        /// The posalterationoption.
        /// </value>
        [XmlElement]
        public Posalterationoption Posalterationoption
        { get; set; }

        /// <summary>
        /// Gets or sets the type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the start time in utc
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string StartTimeUTC
        { get; set; }

        /// <summary>
        /// Gets or sets the end time in utc
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string EndTimeUTC
        { get; set; }

        /// <summary>
        /// Gets or sets the min lead minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MinLeadMinutes
        { get; set; }

        /// <summary>
        /// Gets or sets the max lead hours
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MaxLeadHours
        { get; set; }        

        /// <summary>
        /// Gets or sets the interval minutes
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int IntervalMinutes
        { get; set; }

        /// <summary>
        /// Gets or sets the show date picker
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool ShowDatePicker
        { get; set; }        

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Alterationoption Clone()
        {
            return this.Clone<Alterationoption>();
        }

        #endregion
    }
}
