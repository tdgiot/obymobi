using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v30
{
	[Serializable, XmlRootAttribute(ElementName = "InfraredConfiguration"), IncludeInCodeGeneratorForAndroid]
    public class InfraredConfiguration : ModelBase
    {
        #region Constructors

        public InfraredConfiguration()
        {
        }

        #endregion

        #region Xml Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int InfraredConfigurationId
        { get; set; }        
        
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MillisecondsBetweenCommands
        { get; set; }
        
        [XmlArray("InfraredCommands")]
        [XmlArrayItem("InfraredCommand")]        
        [IncludeInCodeGeneratorForAndroid]
        public InfraredCommand[] InfraredCommands
        { get; set; }                

        #endregion

        #region Methods

        public InfraredConfiguration Clone()
        {
            return this.Clone<InfraredConfiguration>();
        }

        #endregion
    }
}
