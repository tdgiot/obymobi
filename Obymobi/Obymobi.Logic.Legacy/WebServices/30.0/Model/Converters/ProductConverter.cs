namespace Obymobi.Logic.Model.v30.Converters
{
	public class ProductConverter : ModelConverterBase<Obymobi.Logic.Model.v30.Product, Obymobi.Logic.Model.Product>
	{
        public ProductConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.ProductConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.Product ConvertModelToLegacyModel(Obymobi.Logic.Model.Product source)
        {
            Obymobi.Logic.Model.v30.Product target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.Product();
                target.ProductId = source.ProductId;
                target.GenericproductId = source.GenericproductId;
                target.BrandProductId = source.BrandProductId;
                target.CategoryId = source.CategoryId;
                target.Name = source.Name;
                target.Description = source.Description;
                target.Type = source.Type;
                target.SubType = source.SubType;
                target.PriceIn = source.PriceIn;
                target.VatPercentage = source.VatPercentage;
                target.TextColor = source.TextColor;
                target.BackgroundColor = source.BackgroundColor;
                target.DisplayOnHomepage = source.DisplayOnHomepage;
                target.SortOrder = source.SortOrder;
                target.Rateable = source.Rateable;
                target.AllowFreeText = source.AllowFreeText;
                target.Color = source.Color;
                target.ButtonText = source.ButtonText;
                target.CustomizeButtonText = source.CustomizeButtonText;
                target.WebTypeTabletUrl = source.WebTypeTabletUrl;
                target.WebTypeSmartphoneUrl = source.WebTypeSmartphoneUrl;
                target.ScheduleId = source.ScheduleId;
                target.ViewLayoutType = source.ViewLayoutType;
                target.DeliveryLocationType = source.DeliveryLocationType;
                target.HidePrice = source.HidePrice;
                target.VisibilityType = source.VisibilityType;

                if (source.Productgroup != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.ProductgroupConverter productgroupConverter = new Obymobi.Logic.Model.v30.Converters.ProductgroupConverter();
                    target.Productgroup = (Productgroup[])productgroupConverter.ConvertArrayToLegacyArray(source.Productgroup);
                }

                if (source.Alterations != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.AlterationConverter alterationsConverter = new Obymobi.Logic.Model.v30.Converters.AlterationConverter();
                    target.Alterations = (Alteration[])alterationsConverter.ConvertArrayToLegacyArray(source.Alterations);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v30.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.ProductSuggestions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.ProductSuggestionConverter productSuggestionsConverter = new Obymobi.Logic.Model.v30.Converters.ProductSuggestionConverter();
                    target.ProductSuggestions = (ProductSuggestion[])productSuggestionsConverter.ConvertArrayToLegacyArray(source.ProductSuggestions);
                }

                target.SuggestedProductIds = source.SuggestedProductIds;

                if (source.AdvertisementTags != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.AdvertisementTagConverter advertisementTagsConverter = new Obymobi.Logic.Model.v30.Converters.AdvertisementTagConverter();
                    target.AdvertisementTags = (AdvertisementTag[])advertisementTagsConverter.ConvertArrayToLegacyArray(source.AdvertisementTags);
                }

                if (source.Ratings != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.RatingConverter ratingsConverter = new Obymobi.Logic.Model.v30.Converters.RatingConverter();
                    target.Ratings = (Rating[])ratingsConverter.ConvertArrayToLegacyArray(source.Ratings);
                }

                if (source.Attachments != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.AttachmentConverter attachmentsConverter = new Obymobi.Logic.Model.v30.Converters.AttachmentConverter();
                    target.Attachments = (Attachment[])attachmentsConverter.ConvertArrayToLegacyArray(source.Attachments);
                }

                if (source.Posproduct != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v30.Converters.PosproductConverter posproductConverter = new Obymobi.Logic.Model.v30.Converters.PosproductConverter();
                    target.Posproduct = (Posproduct)posproductConverter.ConvertModelToLegacyModel(source.Posproduct);
                }

                target.CategoryIds = source.CategoryIds;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v30.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Product ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.Product source)
        {
            Obymobi.Logic.Model.Product target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Product();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.ProductConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
