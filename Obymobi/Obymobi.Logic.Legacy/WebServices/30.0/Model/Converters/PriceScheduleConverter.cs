namespace Obymobi.Logic.Model.v30.Converters
{
	public class PriceScheduleConverter : ModelConverterBase<Obymobi.Logic.Model.v30.PriceSchedule, Obymobi.Logic.Model.PriceSchedule>
	{
        public PriceScheduleConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.PriceScheduleConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.PriceSchedule ConvertModelToLegacyModel(Obymobi.Logic.Model.PriceSchedule source)
        {
            Obymobi.Logic.Model.v30.PriceSchedule target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.PriceSchedule();
                target.PriceScheduleId = source.PriceScheduleId;
                target.Name = source.Name;

                if (source.PriceScheduleItems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.PriceScheduleItemConverter priceScheduleItemsConverter = new Obymobi.Logic.Model.v30.Converters.PriceScheduleItemConverter();
                    target.PriceScheduleItems = (PriceScheduleItem[])priceScheduleItemsConverter.ConvertArrayToLegacyArray(source.PriceScheduleItems);
                }

                if (source.PriceLevels != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.PriceLevelConverter priceLevelsConverter = new Obymobi.Logic.Model.v30.Converters.PriceLevelConverter();
                    target.PriceLevels = (PriceLevel[])priceLevelsConverter.ConvertArrayToLegacyArray(source.PriceLevels);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.PriceSchedule ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.PriceSchedule source)
        {
            Obymobi.Logic.Model.PriceSchedule target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.PriceSchedule();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.PriceScheduleConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
