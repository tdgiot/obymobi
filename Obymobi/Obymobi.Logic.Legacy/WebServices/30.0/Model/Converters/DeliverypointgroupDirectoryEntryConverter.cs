namespace Obymobi.Logic.Model.v30.Converters
{
	public class DeliverypointgroupDirectoryEntryConverter : ModelConverterBase<Obymobi.Logic.Model.v30.DeliverypointgroupDirectoryEntry, Obymobi.Logic.Model.DeliverypointgroupDirectoryEntry>
	{
        public DeliverypointgroupDirectoryEntryConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.DeliverypointgroupDirectoryEntryConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.DeliverypointgroupDirectoryEntry ConvertModelToLegacyModel(Obymobi.Logic.Model.DeliverypointgroupDirectoryEntry source)
        {
            Obymobi.Logic.Model.v30.DeliverypointgroupDirectoryEntry target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.DeliverypointgroupDirectoryEntry();
                target.DeliverypointgroupId = source.DeliverypointgroupId;
                target.Name = source.Name;
            }

            return target;
        }

        public override Obymobi.Logic.Model.DeliverypointgroupDirectoryEntry ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.DeliverypointgroupDirectoryEntry source)
        {
            Obymobi.Logic.Model.DeliverypointgroupDirectoryEntry target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.DeliverypointgroupDirectoryEntry();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.DeliverypointgroupDirectoryEntryConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
