namespace Obymobi.Logic.Model.v30.Converters
{
	public class CompanyConverter : ModelConverterBase<Obymobi.Logic.Model.v30.Company, Obymobi.Logic.Model.Company>
	{
        public CompanyConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.CompanyConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.Company ConvertModelToLegacyModel(Obymobi.Logic.Model.Company source)
        {
            Obymobi.Logic.Model.v30.Company target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.Company();
                target.CompanyId = source.CompanyId;
                target.Currency = source.Currency;
                target.CurrencySymbol = source.CurrencySymbol;
                target.ShowCurrencySymbol = source.ShowCurrencySymbol;
                target.Name = source.Name;
                target.Code = source.Code;
                target.Addressline1 = source.Addressline1;
                target.Addressline2 = source.Addressline2;
                target.Addressline3 = source.Addressline3;
                target.Zipcode = source.Zipcode;
                target.City = source.City;
                target.Country = source.Country;
                target.CountryCode = source.CountryCode;
                target.CountryCodeAlpha3 = source.CountryCodeAlpha3;
                target.Latitude = source.Latitude;
                target.Longitude = source.Longitude;
                target.GeoFencingEnabled = source.GeoFencingEnabled;
                target.GeoFencingRadius = source.GeoFencingRadius;
                target.FacebookPlaceId = source.FacebookPlaceId;
                target.FoursquareVenueId = source.FoursquareVenueId;
                target.LanguageCode = source.LanguageCode;
                target.RelatedDataLoaded = source.RelatedDataLoaded;
                target.Telephone = source.Telephone;
                target.Fax = source.Fax;
                target.Website = source.Website;
                target.Email = source.Email;
                target.Description = source.Description;
                target.DescriptionSingleLine = source.DescriptionSingleLine;
                target.GoogleAnalyticsId = source.GoogleAnalyticsId;
                target.CompanyDataLastModifiedTicks = source.CompanyDataLastModifiedTicks;
                target.CompanyMediaLastModifiedTicks = source.CompanyMediaLastModifiedTicks;
                target.MenuDataLastModifiedTicks = source.MenuDataLastModifiedTicks;
                target.MenuMediaLastModifiedTicks = source.MenuMediaLastModifiedTicks;
                target.PosIntegrationInformationLastModifiedTicks = source.PosIntegrationInformationLastModifiedTicks;
                target.DeliverypointDataLastModifiedTicks = source.DeliverypointDataLastModifiedTicks;
                target.SurveyDataLastModifiedTicks = source.SurveyDataLastModifiedTicks;
                target.SurveyMediaLastModifiedTicks = source.SurveyMediaLastModifiedTicks;
                target.AnnouncementDataLastModifiedTicks = source.AnnouncementDataLastModifiedTicks;
                target.AnnouncementMediaLastModifiedTicks = source.AnnouncementMediaLastModifiedTicks;
                target.EntertainmentDataLastModifiedTicks = source.EntertainmentDataLastModifiedTicks;
                target.EntertainmentMediaLastModifiedTicks = source.EntertainmentMediaLastModifiedTicks;
                target.AdvertisementDataLastModifiedTicks = source.AdvertisementDataLastModifiedTicks;
                target.AdvertisementMediaLastModifiedTicks = source.AdvertisementMediaLastModifiedTicks;
                target.HandlingMethod = source.HandlingMethod;
                target.SupportpoolId = source.SupportpoolId;
                target.FreeText = source.FreeText;
                target.UseBillButton = source.UseBillButton;
                target.Salt = source.Salt;
                target.SaltPms = source.SaltPms;
                target.DeviceRebootMethod = source.DeviceRebootMethod;

                if (source.ActionButton != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v30.Converters.ActionButtonConverter actionButtonConverter = new Obymobi.Logic.Model.v30.Converters.ActionButtonConverter();
                    target.ActionButton = (ActionButton)actionButtonConverter.ConvertModelToLegacyModel(source.ActionButton);
                }

                target.ActionButtonUrlTablet = source.ActionButtonUrlTablet;
                target.ActionButtonUrlMobile = source.ActionButtonUrlMobile;
                target.CostIndicationType = source.CostIndicationType;
                target.CostIndicationValue = source.CostIndicationValue;
                target.CometHandlerType = source.CometHandlerType;
                target.BusinesshoursIntermediate = source.BusinesshoursIntermediate;
                target.SystemPassword = source.SystemPassword;
                target.AzureNotificationTag = source.AzureNotificationTag;
                target.TemperatureUnit = source.TemperatureUnit;
                target.ClockMode = source.ClockMode;
                target.WeatherClockWidgetMode = source.WeatherClockWidgetMode;
                target.TimeZone = source.TimeZone;
                target.AlterationDialogMode = source.AlterationDialogMode;
                target.PriceFormatType = source.PriceFormatType;
                target.CultureCode = source.CultureCode;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v30.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.Deliverypointgroups != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.DeliverypointgroupConverter deliverypointgroupsConverter = new Obymobi.Logic.Model.v30.Converters.DeliverypointgroupConverter();
                    target.Deliverypointgroups = (Deliverypointgroup[])deliverypointgroupsConverter.ConvertArrayToLegacyArray(source.Deliverypointgroups);
                }

                if (source.Terminals != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.TerminalConverter terminalsConverter = new Obymobi.Logic.Model.v30.Converters.TerminalConverter();
                    target.Terminals = (Terminal[])terminalsConverter.ConvertArrayToLegacyArray(source.Terminals);
                }

                if (source.Businesshours != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.BusinesshoursConverter businesshoursConverter = new Obymobi.Logic.Model.v30.Converters.BusinesshoursConverter();
                    target.Businesshours = (Businesshours[])businesshoursConverter.ConvertArrayToLegacyArray(source.Businesshours);
                }

                if (source.MessageTemplates != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.MessageTemplateConverter messageTemplatesConverter = new Obymobi.Logic.Model.v30.Converters.MessageTemplateConverter();
                    target.MessageTemplates = (MessageTemplate[])messageTemplatesConverter.ConvertArrayToLegacyArray(source.MessageTemplates);
                }

                if (source.UIModes != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.UIModeConverter uIModesConverter = new Obymobi.Logic.Model.v30.Converters.UIModeConverter();
                    target.UIModes = (UIMode[])uIModesConverter.ConvertArrayToLegacyArray(source.UIModes);
                }

                if (source.VenueCategories != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.VenueCategoryConverter venueCategoriesConverter = new Obymobi.Logic.Model.v30.Converters.VenueCategoryConverter();
                    target.VenueCategories = (VenueCategory[])venueCategoriesConverter.ConvertArrayToLegacyArray(source.VenueCategories);
                }

                if (source.Amenities != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.AmenityConverter amenitiesConverter = new Obymobi.Logic.Model.v30.Converters.AmenityConverter();
                    target.Amenities = (Amenity[])amenitiesConverter.ConvertArrayToLegacyArray(source.Amenities);
                }

                if (source.Schedules != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.ScheduleConverter schedulesConverter = new Obymobi.Logic.Model.v30.Converters.ScheduleConverter();
                    target.Schedules = (Schedule[])schedulesConverter.ConvertArrayToLegacyArray(source.Schedules);
                }

                if (source.CloudStorageAccounts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.CloudStorageAccountConverter cloudStorageAccountsConverter = new Obymobi.Logic.Model.v30.Converters.CloudStorageAccountConverter();
                    target.CloudStorageAccounts = (CloudStorageAccount[])cloudStorageAccountsConverter.ConvertArrayToLegacyArray(source.CloudStorageAccounts);
                }

                if (source.Availabilitys != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.AvailabilityConverter availabilitysConverter = new Obymobi.Logic.Model.v30.Converters.AvailabilityConverter();
                    target.Availabilitys = (Availability[])availabilitysConverter.ConvertArrayToLegacyArray(source.Availabilitys);
                }

                if (source.CompanyCurrencys != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.CompanyCurrencyConverter companyCurrencysConverter = new Obymobi.Logic.Model.v30.Converters.CompanyCurrencyConverter();
                    target.CompanyCurrencys = (CompanyCurrency[])companyCurrencysConverter.ConvertArrayToLegacyArray(source.CompanyCurrencys);
                }

                if (source.CompanyCultures != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.CompanyCultureConverter companyCulturesConverter = new Obymobi.Logic.Model.v30.Converters.CompanyCultureConverter();
                    target.CompanyCultures = (CompanyCulture[])companyCulturesConverter.ConvertArrayToLegacyArray(source.CompanyCultures);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v30.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }

                target.UseAgeCheckInOtoucho = source.UseAgeCheckInOtoucho;
                target.UseManualDeliverypointsEntryInOtoucho = source.UseManualDeliverypointsEntryInOtoucho;
                target.UseStatelessInOtoucho = source.UseStatelessInOtoucho;
                target.SkipOrderOverviewAfterOrderSubmitInOtoucho = source.SkipOrderOverviewAfterOrderSubmitInOtoucho;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Company ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.Company source)
        {
            Obymobi.Logic.Model.Company target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Company();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.CompanyConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
