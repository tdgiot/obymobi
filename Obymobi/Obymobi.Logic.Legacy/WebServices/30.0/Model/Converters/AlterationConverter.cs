namespace Obymobi.Logic.Model.v30.Converters
{
	public class AlterationConverter : ModelConverterBase<Obymobi.Logic.Model.v30.Alteration, Obymobi.Logic.Model.Alteration>
	{
        public AlterationConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.AlterationConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.Alteration ConvertModelToLegacyModel(Obymobi.Logic.Model.Alteration source)
        {
            Obymobi.Logic.Model.v30.Alteration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.Alteration();
                target.AlterationId = source.AlterationId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.ColumnHeader = source.ColumnHeader;
                target.DefaultAlterationoptionIds = source.DefaultAlterationoptionIds;
                target.MinOptions = source.MinOptions;
                target.MaxOptions = source.MaxOptions;

                if (source.Posalteration != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v30.Converters.PosalterationConverter posalterationConverter = new Obymobi.Logic.Model.v30.Converters.PosalterationConverter();
                    target.Posalteration = (Posalteration)posalterationConverter.ConvertModelToLegacyModel(source.Posalteration);
                }

                target.StartTime = source.StartTime;
                target.EndTime = source.EndTime;
                target.MinLeadMinutes = source.MinLeadMinutes;
                target.MaxLeadHours = source.MaxLeadHours;
                target.IntervalMinutes = source.IntervalMinutes;
                target.ShowDatePicker = source.ShowDatePicker;
                target.Value = source.Value;
                target.OrderLevelEnabled = source.OrderLevelEnabled;
                target.SortOrder = source.SortOrder;
                target.ParentAlterationId = source.ParentAlterationId;
                target.LayoutType = source.LayoutType;
                target.Description = source.Description;
                target.Visible = source.Visible;

                if (source.Alterationoptions != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.AlterationoptionConverter alterationoptionsConverter = new Obymobi.Logic.Model.v30.Converters.AlterationoptionConverter();
                    target.Alterationoptions = (Alterationoption[])alterationoptionsConverter.ConvertArrayToLegacyArray(source.Alterationoptions);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v30.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.AlterationProducts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.AlterationProductConverter alterationProductsConverter = new Obymobi.Logic.Model.v30.Converters.AlterationProductConverter();
                    target.AlterationProducts = (AlterationProduct[])alterationProductsConverter.ConvertArrayToLegacyArray(source.AlterationProducts);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v30.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Alteration ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.Alteration source)
        {
            Obymobi.Logic.Model.Alteration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Alteration();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.AlterationConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
