namespace Obymobi.Logic.Model.v30.Converters
{
	public class EntertainmenturlConverter : ModelConverterBase<Obymobi.Logic.Model.v30.Entertainmenturl, Obymobi.Logic.Model.Entertainmenturl>
	{
        public EntertainmenturlConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.EntertainmenturlConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.Entertainmenturl ConvertModelToLegacyModel(Obymobi.Logic.Model.Entertainmenturl source)
        {
            Obymobi.Logic.Model.v30.Entertainmenturl target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.Entertainmenturl();
                target.EntertainmenturlId = source.EntertainmenturlId;
                target.EntertainmentId = source.EntertainmentId;
                target.Url = source.Url;
                target.AccessFullDomain = source.AccessFullDomain;
                target.InitialZoom = source.InitialZoom;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Entertainmenturl ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.Entertainmenturl source)
        {
            Obymobi.Logic.Model.Entertainmenturl target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Entertainmenturl();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.EntertainmenturlConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
