namespace Obymobi.Logic.Model.v30.Converters
{
	public class AnnouncementActionConverter : ModelConverterBase<Obymobi.Logic.Model.v30.AnnouncementAction, Obymobi.Logic.Model.AnnouncementAction>
	{
        public AnnouncementActionConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.AnnouncementActionConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.AnnouncementAction ConvertModelToLegacyModel(Obymobi.Logic.Model.AnnouncementAction source)
        {
            Obymobi.Logic.Model.v30.AnnouncementAction target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.AnnouncementAction();
                target.Type = source.Type;
                target.CategoryId = source.CategoryId;
                target.EntertainmentId = source.EntertainmentId;
                target.ProductId = source.ProductId;
                target.Name = source.Name;
                target.DeliverypointgroupIds = source.DeliverypointgroupIds;
            }

            return target;
        }

        public override Obymobi.Logic.Model.AnnouncementAction ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.AnnouncementAction source)
        {
            Obymobi.Logic.Model.AnnouncementAction target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.AnnouncementAction();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.AnnouncementActionConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
