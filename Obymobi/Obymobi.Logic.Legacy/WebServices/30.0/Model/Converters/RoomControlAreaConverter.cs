namespace Obymobi.Logic.Model.v30.Converters
{
	public class RoomControlAreaConverter : ModelConverterBase<Obymobi.Logic.Model.v30.RoomControlArea, Obymobi.Logic.Model.RoomControlArea>
	{
        public RoomControlAreaConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.RoomControlAreaConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.RoomControlArea ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomControlArea source)
        {
            Obymobi.Logic.Model.v30.RoomControlArea target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.RoomControlArea();
                target.RoomControlAreaId = source.RoomControlAreaId;
                target.Name = source.Name;
                target.NameSystem = source.NameSystem;
                target.SortOrder = source.SortOrder;
                target.Visible = source.Visible;
                target.Type = source.Type;
                target.IsNameSystemBaseId = source.IsNameSystemBaseId;

                if (source.RoomControlSections != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.RoomControlSectionConverter roomControlSectionsConverter = new Obymobi.Logic.Model.v30.Converters.RoomControlSectionConverter();
                    target.RoomControlSections = (RoomControlSection[])roomControlSectionsConverter.ConvertArrayToLegacyArray(source.RoomControlSections);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v30.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomControlArea ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.RoomControlArea source)
        {
            Obymobi.Logic.Model.RoomControlArea target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomControlArea();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.RoomControlAreaConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
