namespace Obymobi.Logic.Model.v30.Converters
{
	public class MapConverter : ModelConverterBase<Obymobi.Logic.Model.v30.Map, Obymobi.Logic.Model.Map>
	{
        public MapConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.MapConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.Map ConvertModelToLegacyModel(Obymobi.Logic.Model.Map source)
        {
            Obymobi.Logic.Model.v30.Map target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.Map();
                target.MapId = source.MapId;
                target.Name = source.Name;
                target.MapType = source.MapType;
                target.MyLocationEnabled = source.MyLocationEnabled;
                target.ZoomControlsEnabled = source.ZoomControlsEnabled;
                target.IndoorEnabled = source.IndoorEnabled;
                target.ZoomLevel = source.ZoomLevel;

                if (source.PointOfInterestSynopses != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.PointOfInterestSynopsisConverter pointOfInterestSynopsesConverter = new Obymobi.Logic.Model.v30.Converters.PointOfInterestSynopsisConverter();
                    target.PointOfInterestSynopses = (PointOfInterestSynopsis[])pointOfInterestSynopsesConverter.ConvertArrayToLegacyArray(source.PointOfInterestSynopses);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Map ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.Map source)
        {
            Obymobi.Logic.Model.Map target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Map();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.MapConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
