namespace Obymobi.Logic.Model.v30.Converters
{
	public class ProductgroupConverter : ModelConverterBase<Obymobi.Logic.Model.v30.Productgroup, Obymobi.Logic.Model.Productgroup>
	{
        public ProductgroupConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.ProductgroupConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.Productgroup ConvertModelToLegacyModel(Obymobi.Logic.Model.Productgroup source)
        {
            Obymobi.Logic.Model.v30.Productgroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.Productgroup();
                target.ProductgroupId = source.ProductgroupId;
                target.Name = source.Name;
                target.ColumnHeader = source.ColumnHeader;

                if (source.ProductgroupItems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.ProductgroupItemConverter productgroupItemsConverter = new Obymobi.Logic.Model.v30.Converters.ProductgroupItemConverter();
                    target.ProductgroupItems = (ProductgroupItem[])productgroupItemsConverter.ConvertArrayToLegacyArray(source.ProductgroupItems);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v30.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v30.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Productgroup ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.Productgroup source)
        {
            Obymobi.Logic.Model.Productgroup target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Productgroup();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.ProductgroupConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
