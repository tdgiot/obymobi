namespace Obymobi.Logic.Model.v30.Converters
{
	public class RoomControlComponentConverter : ModelConverterBase<Obymobi.Logic.Model.v30.RoomControlComponent, Obymobi.Logic.Model.RoomControlComponent>
	{
        public RoomControlComponentConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.RoomControlComponentConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.RoomControlComponent ConvertModelToLegacyModel(Obymobi.Logic.Model.RoomControlComponent source)
        {
            Obymobi.Logic.Model.v30.RoomControlComponent target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.RoomControlComponent();
                target.RoomControlComponentId = source.RoomControlComponentId;
                target.Name = source.Name;
                target.NameSystem = source.NameSystem;
                target.Type = source.Type;
                target.SortOrder = source.SortOrder;
                target.Visible = source.Visible;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.InfraredConfigurationId = source.InfraredConfigurationId;
                target.ControllerId = source.ControllerId;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v30.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.RoomControlComponent ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.RoomControlComponent source)
        {
            Obymobi.Logic.Model.RoomControlComponent target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.RoomControlComponent();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.RoomControlComponentConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
