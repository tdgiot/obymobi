namespace Obymobi.Logic.Model.v30.Converters
{
	public class MessageConverter : ModelConverterBase<Obymobi.Logic.Model.v30.Message, Obymobi.Logic.Model.Message>
	{
        public MessageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.MessageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.Message ConvertModelToLegacyModel(Obymobi.Logic.Model.Message source)
        {
            Obymobi.Logic.Model.v30.Message target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.Message();
                target.MessageId = source.MessageId;
                target.CustomerId = source.CustomerId;
                target.ClientId = source.ClientId;
                target.DeliverypointId = source.DeliverypointId;
                target.CompanyId = source.CompanyId;
                target.CompanyName = source.CompanyName;
                target.Title = source.Title;
                target.Text = source.Text;
                target.Duration = source.Duration;
                target.MediaId = source.MediaId;
                target.CategoryId = source.CategoryId;
                target.EntertainmentId = source.EntertainmentId;
                target.ProductId = source.ProductId;
                target.SiteId = source.SiteId;
                target.PageId = source.PageId;
                target.Url = source.Url;
                target.Urgent = source.Urgent;
                target.NotifyOnYes = source.NotifyOnYes;
                target.MessageLayoutType = source.MessageLayoutType;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v30.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Message ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.Message source)
        {
            Obymobi.Logic.Model.Message target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Message();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.MessageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
