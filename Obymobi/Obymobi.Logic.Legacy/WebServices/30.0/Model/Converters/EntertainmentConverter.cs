namespace Obymobi.Logic.Model.v30.Converters
{
	public class EntertainmentConverter : ModelConverterBase<Obymobi.Logic.Model.v30.Entertainment, Obymobi.Logic.Model.Entertainment>
	{
        public EntertainmentConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.EntertainmentConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.Entertainment ConvertModelToLegacyModel(Obymobi.Logic.Model.Entertainment source)
        {
            Obymobi.Logic.Model.v30.Entertainment target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.Entertainment();
                target.EntertainmentId = source.EntertainmentId;
                target.CompanyId = source.CompanyId;
                target.Name = source.Name;
                target.PackageName = source.PackageName;
                target.ClassName = source.ClassName;
                target.TextColor = source.TextColor;
                target.BackgroundColor = source.BackgroundColor;
                target.DefaultEntertainmenturl = source.DefaultEntertainmenturl;
                target.EntertainmentType = source.EntertainmentType;
                target.FeatureCode = source.FeatureCode;
                target.SocialmediaType = source.SocialmediaType;
                target.SurveyName = source.SurveyName;
                target.FormName = source.FormName;
                target.BackButton = source.BackButton;
                target.EntertainmentcategoryId = source.EntertainmentcategoryId;
                target.Description = source.Description;
                target.AnnouncementAction = source.AnnouncementAction;
                target.SortOrder = source.SortOrder;
                target.PreventCaching = source.PreventCaching;
                target.PostfixWithTimestamp = source.PostfixWithTimestamp;
                target.HomeButton = source.HomeButton;
                target.MenuContainer = source.MenuContainer;
                target.NavigateButton = source.NavigateButton;
                target.NavigationBar = source.NavigationBar;
                target.TitleAsHeader = source.TitleAsHeader;
                target.RestrictedAccess = source.RestrictedAccess;
                target.Filename = source.Filename;
                target.FileVersion = source.FileVersion;
                target.AppDataClearInterval = source.AppDataClearInterval;
                target.AppCloseInterval = source.AppCloseInterval;
                target.SiteId = source.SiteId;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v30.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.AdvertisementTags != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.AdvertisementTagConverter advertisementTagsConverter = new Obymobi.Logic.Model.v30.Converters.AdvertisementTagConverter();
                    target.AdvertisementTags = (AdvertisementTag[])advertisementTagsConverter.ConvertArrayToLegacyArray(source.AdvertisementTags);
                }

                if (source.Entertainmenturls != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.EntertainmenturlConverter entertainmenturlsConverter = new Obymobi.Logic.Model.v30.Converters.EntertainmenturlConverter();
                    target.Entertainmenturls = (Entertainmenturl[])entertainmenturlsConverter.ConvertArrayToLegacyArray(source.Entertainmenturls);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Entertainment ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.Entertainment source)
        {
            Obymobi.Logic.Model.Entertainment target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Entertainment();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.EntertainmentConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
