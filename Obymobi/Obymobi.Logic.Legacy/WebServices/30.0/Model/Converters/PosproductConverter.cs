namespace Obymobi.Logic.Model.v30.Converters
{
	public class PosproductConverter : ModelConverterBase<Obymobi.Logic.Model.v30.Posproduct, Obymobi.Logic.Model.Posproduct>
	{
        public PosproductConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v31.Converters.PosproductConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v30.Posproduct ConvertModelToLegacyModel(Obymobi.Logic.Model.Posproduct source)
        {
            Obymobi.Logic.Model.v30.Posproduct target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v30.Posproduct();
                target.PosproductId = source.PosproductId;
                target.CompanyId = source.CompanyId;
                target.ExternalId = source.ExternalId;
                target.ExternalPoscategoryId = source.ExternalPoscategoryId;
                target.Name = source.Name;
                target.PriceIn = source.PriceIn;
                target.VatTariff = source.VatTariff;
                target.SortOrder = source.SortOrder;
                target.Description = source.Description;

                if (source.Posalterations != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v30.Converters.PosalterationConverter posalterationsConverter = new Obymobi.Logic.Model.v30.Converters.PosalterationConverter();
                    target.Posalterations = (Posalteration[])posalterationsConverter.ConvertArrayToLegacyArray(source.Posalterations);
                }

                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Posproduct ConvertLegacyModelToModel(Obymobi.Logic.Model.v30.Posproduct source)
        {
            Obymobi.Logic.Model.Posproduct target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Posproduct();

                // Copy default values from new version
                new Obymobi.Logic.Model.v31.Converters.PosproductConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
