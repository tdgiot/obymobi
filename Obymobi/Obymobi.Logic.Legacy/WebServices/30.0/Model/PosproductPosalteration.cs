using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v30
{
    /// <summary>
    /// Model class which is a link between POS products and POS alterations
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "PosproductPosalteration")]
    public class PosproductPosalteration : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.PosproductPosalteration type
        /// </summary>
        public PosproductPosalteration()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the pos alteration item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int PosproductPosalterationId
        { get; set; }

        /// <summary>
        /// Gets or sets the ExternalId as used in the POS system
        /// </summary>
        [XmlElement]
        public string ExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the ExternalId of the Posproduct
        /// </summary>
        [XmlElement]
        public string ExternalPosproductId
        { get; set; }

        /// <summary>
        /// Gets or sets the ExternalId of the Posalteration
        /// </summary>
        [XmlElement]
        public string ExternalPosalterationId
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public PosproductPosalteration Clone()
        {
            return this.Clone<PosproductPosalteration>();
        }

        #endregion
    }
}
