using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v30
{
    /// <summary>
    /// Model class which represents a software release
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Release"), IncludeInCodeGeneratorForAndroid]
    public class Release : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Release type
        /// </summary>
        public Release()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the release
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int ReleaseId
        { get; set; }

        /// <summary>
        /// Gets or sets the version of the release
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Version
        { get; set; }

        /// <summary>
        /// Gets or sets the filename of the release
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Filename
        { get; set; }

        /// <summary>
        /// Gets or sets the remarks of the release
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Remarks
        { get; set; }

        /// <summary>
        /// Gets or sets the Crc32 checksum of the release
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Crc32 
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Release Clone()
        {
            return this.Clone<Release>();
        }

        #endregion
    }
}
