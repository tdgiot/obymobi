using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v30
{
    /// <summary>
    /// Model class which represents a point of interest synopsis
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "PointOfInterestSynopsis"), IncludeInCodeGeneratorForAndroid]
    public class PointOfInterestSynopsis : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.PointOfInterestSynopsis type
        /// </summary>
        public PointOfInterestSynopsis()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the point of interest id
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]        
        [IncludeInCodeGeneratorForAndroid]
        public int PointOfInterestId
        { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string DescriptionSingleLine
        { get; set; }

        /// <summary>
        /// Map cooridates, latitude
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public double? Latitude
        { get; set; }

        /// <summary>
        /// Map cooridates, longitude
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public double? Longitude
        { get; set; }

        /// <summary>
        /// Gets or sets the address
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Addressline1
        { get; set; }

        /// <summary>
        /// Gets or sets the zipcode
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Zipcode
        { get; set; }

        /// <summary> 
        /// Gets or sets the city
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string City
        { get; set; }

        /// <summary>
        /// Gets or sets the business hours
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string BusinesshoursIntermediate
        { get; set; }

        /// <summary>
        /// Gets or sets the floor level
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Floor
        { get; set; }

        /// <summary>
        /// Gets or sets wether the distance should be displayed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool DisplayDistance
        { get; set; }

        /// <summary>
        /// Gets or sets the venue category
        /// </summary>
        [XmlArray("VenueCategorys")]
        [XmlArrayItem("VenueCategory")]
        [IncludeInCodeGeneratorForAndroid]
        public VenueCategory[] VenueCategories
        { get; set; }

        /// <summary>
        /// Gets or sets the media
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        /// <summary>
        /// Gets or sets the custom texts
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Map Clone()
        {
            return this.Clone<Map>();
        }

        #endregion
    }
}