using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Interfaces;

namespace Obymobi.Logic.Model.v30
{
    /// <summary>
    /// Use this message to receive updates about a room 
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "RoomData"), IncludeInCodeGeneratorForAndroid]
    public class RoomData : ModelBase, IPmsModel
    {
        public RoomData()
        {

        }

        [XmlIgnore]
        public string Name { get { return this.GetType().Name; } }

        /// <summary>
        /// The DeliverypointNumber, i.e. RoomNumber
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string DeliverypointNumber { get; set; }

        /// <summary>
        /// An error in retrieving the Guest Information
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public string Error { get; set; }

        /// <summary>
        /// Indicates the voicemail has been set
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool VoicemailMessageSet { get; set; }

        /// <summary>
        /// Indicates wether the current guest has a voicemail message awaiting
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool VoicemailMessage { get; set; }

        /// <summary>
        /// Indicates if the classOfService is set
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool ClassOfServiceSet { get; set; }

        /// <summary>
        /// ClassOfService
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public int ClassOfService { get; set; }

        /// <summary>
        /// Indicates if the doNotDisturb is set
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool DoNotDisturbSet { get; set; }

        /// <summary>
        /// DoNotDisturb
        /// </summary>
        [IncludeInCodeGeneratorForAndroid]
        public bool DoNotDisturb { get; set; }

        public override string ToString()
        {
            return string.Format("DeliverypointNumber: {0}, Error: {1}, VoiceMailMessage: {2}, ClassOfService: {3}, DoNotDisturb: {4}",
                this.DeliverypointNumber,
                this.Error,
                this.VoicemailMessage,
                this.ClassOfService,
                this.DoNotDisturb);
        }
    }
}
