using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v30
{
    [Serializable, XmlRootAttribute(ElementName = "Messagegroup"), IncludeInCodeGeneratorForAndroid]
    public class Messagegroup : ModelBase
    {
        [XmlElement, PrimaryKeyFieldOfModel, IncludeInCodeGeneratorForAndroid]
        public int MessagegroupId
        { get; set; }

        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public string GroupName
        { get; set; }

        [XmlElement, IncludeInCodeGeneratorForAndroid]
        public int[] DeliverypointNumbers
        { get; set; }
    }
}