using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v30
{
    /// <summary>
    /// Model class which represents a form field
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "FormField")]
    public class FormField : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.FormField type
        /// </summary>
        public FormField()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets of sets the id of the form field
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int FormFieldId
        { get; set;}

        /// <summary>
        /// Gets or sets the id of the form
        /// </summary>
        [XmlElement]
        public int FormId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the form field
        /// </summary>
        [XmlElement]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the form field
        /// </summary>
        [XmlElement]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the flag which indicates whether this form is required
        /// </summary>
        [XmlElement]
        public bool Required
        { get; set; }

        /// <summary>
        /// Gets or sets the width of the form field
        /// </summary>
        [XmlElement]
        public int Width
        { get; set; }

        /// <summary>
        /// Gets or sets the fields of the form
        /// </summary>
        [XmlArray("FormFieldValues")]
        [XmlArrayItem("FormFieldValue")]
        public FormFieldValue[] FormFieldValues
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [XmlArray("FormFieldLanguages")]
        [XmlArrayItem("FormFieldLanguage")]
        public FormFieldLanguage[] FormFieldLanguages
        { get; set; }
    
        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public FormField Clone()
        {
            return this.Clone<FormField>();
        }

        #endregion
    }
}
