using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v30
{
    /// <summary>
    /// Model class which represents a custom text
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "CustomText"), IncludeInCodeGeneratorForAndroid]
    public class CustomText : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.CustomText type
        /// </summary>
        public CustomText()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the custom text
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int CustomTextId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code of the custom text
        /// </summary>
        [XmlElement]
        public string LanguageCode
        { get; set; }
        
        /// <summary>
        /// Gets or sets the type of the custom text
        /// </summary>
        [XmlElement]       
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the text of the custom text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Text
        { get; set; }

        /// <summary>
        /// Gets or sets the culture code of the custom text
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string CultureCode
        { get; set; }

        /// <summary>
        /// Gets or sets the foreign key this custom text is attached to
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ForeignKey
        { get; set; }

        /// <summary>
        /// Parent of this custom text
        /// </summary>
        [XmlAttribute("Parent")]
        public string Parent
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public CustomText Clone()
        {
            return this.Clone<CustomText>();
        }

        #endregion
    }
}
