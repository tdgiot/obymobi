using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v30
{
    /// <summary>
    /// Model class which represents an price level item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "PriceLevelItem"), IncludeInCodeGeneratorForAndroid]
    public class PriceLevelItem : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.PriceLevelItem type
        /// </summary>
        public PriceLevelItem()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the price level item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int PriceLevelItemId
        { get; set; }

        /// <summary>
        /// Gets or sets the price
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public decimal Price
        { get; set; }

        /// <summary>
        /// Gets or sets the product id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the alteration option id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationoptionId
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public PriceLevelItem Clone()
        {
            return this.Clone<PriceLevelItem>();
        }

        #endregion
    }
}
