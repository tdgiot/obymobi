using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v30
{
    /// <summary>
    /// ReiswijzerFailure model
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "ReiswijzerFailure"), IncludeInCodeGeneratorForFlex]
	public class ReiswijzerFailure
	{
		#region Constructors

		/// <summary>
		/// Constructs an instance of the Obymobi.Logic.Model.Company type
		/// </summary>
		public ReiswijzerFailure()
		{
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the id of the company
		/// </summary>
		[XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
		public int ReiswijzerFailureId
		{ get; set; }

        /// <summary>
        /// Gets or sets the reiswijzer errors.
        /// </summary>
        /// <value>
        /// The reiswijzer errors.
        /// </value>
		[XmlArray("ReiswijzerErrors")]
		[XmlArrayItem("ReiswijzerErrors")]
        [IncludeInCodeGeneratorForFlex]
		public ReiswijzerError[] ReiswijzerErrors
		{ get; set; }

		#endregion	
	}
}
