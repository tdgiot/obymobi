using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v30
{
    /// <summary>
    /// Model class which represents an price level
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "PriceLevel"), IncludeInCodeGeneratorForAndroid]
    public class PriceLevel : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.PriceLevel type
        /// </summary>
        public PriceLevel()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the price level
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int PriceLevelId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the price level
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the price level items
        /// </summary>
        [XmlArray("PriceLevelItems")]
        [XmlArrayItem("PriceLevelItem")]
        [IncludeInCodeGeneratorForAndroid]
        public PriceLevelItem[] PriceLevelItems
        { get; set; }        

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public PriceLevel Clone()
        {
            return this.Clone<PriceLevel>();
        }

        #endregion
    }
}
