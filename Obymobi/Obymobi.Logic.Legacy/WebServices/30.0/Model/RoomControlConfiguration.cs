using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v30
{
    /// <summary>
    /// Model class which represents a room control configuration
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "RoomControlConfiguration"), IncludeInCodeGeneratorForAndroid]
    public class RoomControlConfiguration : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.RoomControlConfiguration type
        /// </summary>
        public RoomControlConfiguration()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the room control configuration
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlConfigurationId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the room control areas
        /// </summary>
        [XmlArray("RoomControlAreas")]
        [XmlArrayItem("RoomControlArea")]
        [IncludeInCodeGeneratorForAndroid]
        public RoomControlArea[] RoomControlAreas
        { get; set; }

        /// <summary>
        /// Gets or sets the infra red configurations.
        /// </summary>
        [XmlArray("InfraredConfigurations")]
        [XmlArrayItem("InfraredConfiguration")]
        [IncludeInCodeGeneratorForAndroid]
        public InfraredConfiguration[] InfraredConfigurations
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public RoomControlConfiguration Clone()
        {
            return this.Clone<RoomControlConfiguration>();
        }

        #endregion
    }
}
