using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v30
{
    /// <summary>
    /// Model class which represents an alteration
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Discount"), IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForJavascript]
    public class Discount : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Offer type
        /// </summary>
        public Discount()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the offer
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int DiscountId
        { get; set; }

        /// <summary>
        /// Gets or sets the CompanyId of the offer
        /// </summary>
        [XmlElement]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the OfferType of hte offer
        /// </summary>
        [XmlElement]
        public int DiscountType
        { get; set; }

        /// <summary>
        /// Gets or sets a fieldvalue of the offer
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets a fieldvalue of the offer
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets a fieldvalue of the offer
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets a fieldvalue of the offer
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets a fieldvalue of the offer
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets a fieldvalue of the offer
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets a fieldvalue of the offer
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets a fieldvalue of the offer
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets a fieldvalue of the offer
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets a fieldvalue of the offer
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }

        /// <summary>
        /// Gets or sets a description of the offer
        /// </summary>
        [XmlElement]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets the category ids this offer is valid for
        /// </summary>
        [XmlArray("CategoryIds")]
        [XmlArrayItem("CategoryId")]
        public int[] CategoryIds
        { get; set; }

        /// <summary>
        /// Gets or sets the product ids this offer is valid for
        /// </summary>
        [XmlArray("ProductIds")]
        [XmlArrayItem("ProductId")]
        public int[] ProductIds
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Discount Clone()
        {
            return this.Clone<Discount>();
        }

        #endregion
    }
}