using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v30
{
	/// <summary>
	/// Model class for the Terminal Status
	/// </summary>
    [Serializable, XmlRootAttribute(ElementName = "TerminalStatus"), IncludeInCodeGeneratorForAndroid]
    public class TerminalStatus : ModelBase
    {
        /// <summary>
        /// Primary Key is required in a Model, but this one is not used.
        /// </summary> 
        [XmlElement, PrimaryKeyFieldOfModel]
        public int TerminalStatusId
        { get; set; }


        /// <summary>
        /// Primary Key is required in a Model, but this one is not used.
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public string PrivateIpAddresses
        { get; set; }

        /// <summary>
        /// Primary Key is required in a Model, but this one is not used.
        /// </summary>
        [XmlElement]       
        public string PublicIpAddress
        { get; set; }

        /// <summary>
        /// Primary Key is required in a Model, but this one is not used.
        /// </summary>
        [XmlElement]       
        [IncludeInCodeGeneratorForAndroid]
        public int BatteryLevel
        { get; set; }

        /// <summary>
        /// Primary Key is required in a Model, but this one is not used.
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool IsCharging
        { get; set; }

        /// <summary>
        /// Primary Key is required in a Model, but this one is not used.
        /// </summary>
        [XmlElement]
        public int StatusCode
        { get; set; }

        /// <summary>
        /// Primary Key is required in a Model, but this one is not used.
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string ApplicationVersion
        { get; set; }

        /// <summary>
        /// Primary Key is required in a Model, but this one is not used.
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string OsVersion
        { get; set; }

        /// <summary>
        /// Primary Key is required in a Model, but this one is not used.
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int WifiStrength
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool AgentIsRunning
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool SupportToolsIsRunning
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PrinterConnected 
        { get; set; }
        
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string LastPmsSync
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PmsActive
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool PmsTerminalOnline
        { get; set; }
    }
}