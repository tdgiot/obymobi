using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents an Otoucho client
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Requestlog")]
    public class Requestlog : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Requestlog type
        /// </summary>
        public Requestlog()
        {
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Requestlog type using the specified parameters
        /// </summary>
        public Requestlog(int RequestlogId)
        {
            this.RequestlogId = RequestlogId;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the request log item
        /// </summary>
        [XmlElement]
        public long RequestlogId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the current server
        /// </summary>
        [XmlElement]
        public string ServerName 
        { get; set; }

        /// <summary>
        /// Gets or sets the user agent
        /// </summary>
        [XmlElement]
        public string UserAgent
        { get; set; }

        /// <summary>
        /// Gets or sets the identifier
        /// </summary>
        [XmlElement]
        public string Identifier
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the customer
        /// </summary>
        [XmlElement]
        public int CustomerId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the terminal
        /// </summary>
        [XmlElement]
        public int TerminalId
        { get; set; }

        /// <summary>
        /// Gets or sets the the phone number
        /// </summary>
        [XmlElement]
        public string Phonenumber
        { get; set; }

        /// <summary>
        /// Gets or sets the method name
        /// </summary>
        [XmlElement]
        public string MethodName
        { get; set; }

        /// <summary>
        /// Gets or sets the result code
        /// </summary>
        [XmlElement]
        public string ResultCode
        { get; set; }

        /// <summary>
        /// Gets or sets the result message
        /// </summary>
        [XmlElement]
        public string ResultMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the first parameter
        /// </summary>
        [XmlElement]
        public string Parameter1
        { get; set; }

        /// <summary>
        /// Gets or sets the second parameter
        /// </summary>
        [XmlElement]
        public string Parameter2
        { get; set; }

        /// <summary>
        /// Gets or sets the third parameter
        /// </summary>
        [XmlElement]
        public string Parameter3
        { get; set; }

        /// <summary>
        /// Gets or sets the forth parameter
        /// </summary>
        [XmlElement]
        public string Parameter4
        { get; set; }

        /// <summary>
        /// Gets or sets the fifth parameter
        /// </summary>
        [XmlElement]
        public string Parameter5
        { get; set; }

        /// <summary>
        /// Gets or sets the sixth parameter
        /// </summary>
        [XmlElement]
        public string Parameter6
        { get; set; }

        /// <summary>
        /// Gets or sets the result body
        /// </summary>
        [XmlElement]
        public string ResultBody
        { get; set; }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        [XmlElement]
        public string ErrorMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the error stacktrace
        /// </summary>
        [XmlElement]
        public string ErrorStackTrace
        { get; set; }

        /// <summary>
        /// Gets or sets the xml
        /// </summary>
        [XmlElement]
        public string Xml
        { get; set; }

        /// <summary>
        /// Gets or sets the raw request
        /// </summary>
        [XmlElement]
        public string RawRequest
        { get; set; }

        /// <summary>
        /// Gets or sets the trace from mobile
        /// </summary>
        [XmlElement]
        public string TraceFromMobile
        { get; set; }

        /// <summary>
        /// Gets or sets the mobile identifier
        /// </summary>
        [XmlElement]
        public string MobileIdentifier
        { get; set; }

        /// <summary>
        /// Gets or sets the mobile name
        /// </summary>
        [XmlElement]
        public string MobileName
        { get; set; }

        /// <summary>
        /// Gets or sets the mobile operating system
        /// </summary>
        [XmlElement]
        public string MobileOS
        { get; set; }

        /// <summary>
        /// Gets or sets the MobilePlatform
        /// </summary>
        [XmlElement]
        public string MobilePlatform
        { get; set; }

        /// <summary>
        /// Gets or sets the mobile vendor
        /// </summary>
        [XmlElement]
        public string MobileVendor
        { get; set; }

        /// <summary>
        /// Gets or sets the device info
        /// </summary>
        [XmlElement]
        public string DeviceInfo
        { get; set; }

        /// <summary>
        /// Gets or sets the log
        /// </summary>
        [XmlElement]
        public string Log
        { get; set; }

        /// <summary>
        /// Gets or sets the result enum type name
        /// </summary>
        [XmlElement]
        public string ResultEnumTypeName
        { get; set; }

        /// <summary>
        /// Gets or sets the result enum value name
        /// </summary>
        [XmlElement]
        public string ResultEnumValueName
        { get; set; }

        /// <summary>
        /// Gets or sets the source application
        /// </summary>
        [XmlElement]
        public string SourceApplication
        { get; set; }

		/// <summary>
		/// Gets or sets the source application
		/// </summary>
		[XmlElement]
		public DateTime Created
		{ get; set; }

		/// <summary>
		/// Gets or sets the source application
		/// </summary>
		[XmlElement]
		public DateTime Updated
		{ get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Requestlog Clone()
        {
            return this.Clone<Requestlog>();
        }

        #endregion
    }
}
