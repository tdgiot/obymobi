using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents a station list
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "StationList"), IncludeInCodeGeneratorForAndroid]
    public class StationList : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.StationList type
        /// </summary>
        public StationList()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the staion list
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int StationListId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

         [XmlArray("Stations")]
         [XmlArrayItem("Station")]
         [IncludeInCodeGeneratorForAndroid]
         public Station[] Stations
         { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public StationList Clone()
        {
            return this.Clone<StationList>();
        }

        #endregion
    }
}
