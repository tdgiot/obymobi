using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents a company
    /// </summary>
    //[XmlRoot("Company", Namespace = "http://crave-emenu.com/webservices/tablet")]
    //[XmlType("Company", Namespace = "http://crave-emenu.com/webservices/tablet")]
    //[IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile, IncludeInCodeGeneratorForXamarin]
    //[XmlType(Namespace = "urn:baz")]
    //[XmlRoot(Namespace = "urn:baz")]
    [Serializable, XmlRootAttribute(ElementName = "Company"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile]
    public class Company : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Company type
        /// </summary>
        public Company()
        {
            // Default values
            this.UseAgeCheckInOtoucho = true;
            this.UseManualDeliverypointsEntryInOtoucho = false;
            this.UseStatelessInOtoucho = false;
            this.SkipOrderOverviewAfterOrderSubmitInOtoucho = false;
        }

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Company type using the specified parameters
        /// </summary>
        public Company(int CompanyId, string Currency, string Name, string Code, string Addressline1, string Addressline2, string Addressline3, string Zipcode, string City, string Country, string Telephone, string Fax, string Website, string Email, string Description)
        {
            this.CompanyId = CompanyId;
            this.Currency = Currency;
            this.Name = Name;
            this.Code = Code;
            this.Addressline1 = Addressline1;
            this.Addressline2 = Addressline2;
            this.Addressline3 = Addressline3;
            this.Zipcode = Zipcode;
            this.City = City;
            this.Country = Country;
            this.Telephone = Telephone;
            this.Fax = Fax;
            this.Website = Website;
            this.Email = Email;
            this.Description = Description;

            // Default values
            this.UseAgeCheckInOtoucho = true;
            this.UseManualDeliverypointsEntryInOtoucho = false;
            this.UseStatelessInOtoucho = false;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the currency of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Currency
        { get; set; }

        /// <summary>
        /// Gets or sets the currency symbol of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string CurrencySymbol
        { get; set; }

        /// <summary>
        /// Gets or sets the if the currency symbol should be visible for the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public bool ShowCurrencySymbol
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the code of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Code
        { get; set; }

        /// <summary>
        /// Gets or sets the addressline2 of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Addressline1
        { get; set; }

        /// <summary>
        /// Gets or sets the addressline2 of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Addressline2
        { get; set; }

        /// <summary>
        /// Gets or sets the addressline3 of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Addressline3
        { get; set; }

        /// <summary>
        /// Gets or sets the zipcode of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Zipcode
        { get; set; }

        /// <summary>
        /// Gets or sets the city of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string City
        { get; set; }

        /// <summary>
        /// Gets or sets the country of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Country
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string CountryCode { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string CountryCodeAlpha3 { get; set; }

        /// <summary>
        /// Map cooridates, latitude
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public double? Latitude
        { get; set; }

        /// <summary>
        /// Map cooridates, longitude
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public double? Longitude
        { get; set; }

        /// <summary>
        /// GeoFencing Enabled
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool GeoFencingEnabled { get; set; }

        /// <summary>
        /// GeoFencing Radius (in meters)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int GeoFencingRadius { get; set; }

        /// <summary>
        /// Facebook place ID
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string FacebookPlaceId
        { get; set; }

        /// <summary>
        /// FourSquare venue ID
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public string FoursquareVenueId
        { get; set; }

        /// <summary>
        /// Gets or sets the LanguageCode
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the flag whether the company is a favorite company
        /// </summary>
        [XmlElement]
        public string RelatedDataLoaded
        { get; set; }

        /// <summary>
        /// Gets or sets the telephone number of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Telephone
        { get; set; }

        /// <summary>
        /// Gets or sets the fax number of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        public string Fax
        { get; set; }

        /// <summary>
        /// Gets or sets the website address of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        public string Website
        { get; set; }

        /// <summary>
        /// Gets or sets the e-mail address of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public string Email
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets the description of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string DescriptionSingleLine
        { get; set; }

        /// <summary>
        /// Gets or sets the Google Analytics Tracker Id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public string GoogleAnalyticsId
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Data (not the menu) was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public long CompanyDataLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Media (not the menu) was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public long CompanyMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Menu was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public long MenuDataLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Media for the Menu was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public long MenuMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related PosIntegrationInformation was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForMobile]
        public long PosIntegrationInformationLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Deliverypoints was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public long DeliverypointDataLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Survey was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public long SurveyDataLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Media for the Survey was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public long SurveyMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Announcement was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public long AnnouncementDataLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Media for the Announcement was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public long AnnouncementMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Entertainment was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public long EntertainmentDataLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Media for the Entertainment was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public long EntertainmentMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Advertisement was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public long AdvertisementDataLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Media for the Advertisement was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForJavascript]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public long AdvertisementMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Get or sets the handling method of the terminal
        /// FO-ROUTING : Temporary excluded from android
        /// </summary>
        /// <remarks>This field is only for Emenu!!</remarks>
        [XmlElement]
        //[IncludeInCodeGeneratorForAndroid]
        public int HandlingMethod
        { get; set; }

        /// <summary>
        /// Get or sets the id of the support pool for this company
        /// </summary>
        [XmlElement]
        public int SupportpoolId
        { get; set; }

        /// <summary>
        /// Get or sets if free text is allowed for this company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public bool FreeText
        { get; set; }

        /// <summary>
        /// Get or sets if the bill button should be used
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool UseBillButton
        { get; set; }

		/// <summary>
		/// Gets or sets the company salt
		/// </summary>
		[XmlElement]
		[IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
		public string Salt 
		{ get; set; }

        /// <summary>
        /// Gets or sets the company salt
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string SaltPms
        { get; set; }

        /// <summary>
        /// Gets or sets the device reboot method
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int DeviceRebootMethod 
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public ActionButton ActionButton
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string ActionButtonUrlTablet
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string ActionButtonUrlMobile
        { get; set; }

        /// <summary>
        /// Gets or sets the cost indication type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int CostIndicationType
        { get; set; }

        /// <summary>
        /// Gets or sets the cost indication type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public decimal? CostIndicationValue
        { get; set; }

        /// <summary>
        /// Gets or sets the comet handler type
        /// </summary>
        /// <see cref="ClientCommunicationMethod"/>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int CometHandlerType
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        [IncludeInCodeGeneratorForAndroid]
        public string BusinesshoursIntermediate
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string SystemPassword { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string AzureNotificationTag { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TemperatureUnit { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ClockMode { get; set; }

        [XmlElement]
        public int WeatherClockWidgetMode { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string TimeZone { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int AlterationDialogMode { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PriceFormatType { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string CultureCode { get; set; }

        /// <summary>
        /// Gets or sets the media of the company
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public Media[] Media
        { get; set; }        

        /// <summary>
        /// Gets or sets the deliverypoint groups of the company
        /// </summary>
        [XmlArray("Deliverypointgroups")]
        [XmlArrayItem("Deliverypointgroup")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public Deliverypointgroup[] Deliverypointgroups
        { get; set; }

        /// <summary>
        /// Gets or sets the terminals of the company
        /// </summary>
        [XmlArray("Terminals")]
        [XmlArrayItem("Terminal")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        public Terminal[] Terminals
        { get; set; }

        /// <summary>
        /// Gets or sets the business hours of the company
        /// </summary>
        [XmlArray("Businesshourss")] // A bit weird, but the only way to follow the convention
        [XmlArrayItem("Businesshours")]
        [IncludeInCodeGeneratorForMobile]
        public Businesshours[] Businesshours
        { get; set; }

        /// <summary>
        /// Gets or sets the message templates of the company
        /// </summary>
        [XmlArray("MessageTemplates")]
        [XmlArrayItem("MessageTemplate")]
        [IncludeInCodeGeneratorForAndroid]
        public MessageTemplate[] MessageTemplates
        { get; set; }

        /// <summary>
        /// Gets or sets the ui modes of the company
        /// </summary>
        [XmlArray("UIModes")]
        [XmlArrayItem("UIMode")]
		[JsonProperty("uimodes")]
        [IncludeInCodeGeneratorForAndroid]
        public UIMode[] UIModes
        { get; set; }

        /// <summary>
        /// Gets or sets the VenueCategories relevant to this Company
        /// </summary>
        [XmlArray("VenueCategorys")]
        [XmlArrayItem("VenueCategory")]
        [IncludeInCodeGeneratorForAndroid]
        public VenueCategory[] VenueCategories
        { get; set; }

        /// <summary>
        /// Gets or sets the Amenities relevant to this Company
        /// </summary>
        [XmlArray("Amenitys")]
        [XmlArrayItem("Amenity")]
        public Amenity[] Amenities
        { get; set; }

        /// <summary>
        /// Gets or sets the schedules relevant to this Company
        /// </summary>
        [XmlArray("Schedules")]
        [XmlArrayItem("Schedule")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForXamarin]
        public Schedule[] Schedules
        { get; set; }

        /// <summary>
        /// Gets or sets the cloud storage accounts relevant to this Company
        /// </summary>
        [XmlArray("CloudStorageAccounts")]
        [XmlArrayItem("CloudStorageAccount")]
        [IncludeInCodeGeneratorForAndroid]
        public CloudStorageAccount[] CloudStorageAccounts
        { get; set; }

        /// <summary>
        /// Gets or sets the availabilities
        /// </summary>
        [XmlArray("Availabilities")]
        [XmlArrayItem("Availability")]
        [IncludeInCodeGeneratorForAndroid]
        public Availability[] Availabilitys
        { get; set; }

        /// <summary>
        /// Gets or sets the company currencies
        /// </summary>
        [XmlArray("CompanyCurrencies")]
        [XmlArrayItem("CompanyCurrency")]
        [IncludeInCodeGeneratorForAndroid]
        public CompanyCurrency[] CompanyCurrencys
        { get; set; }

        /// <summary>
        /// Gets or sets the company cultures
        /// </summary>
        [XmlArray("CompanyCultures")]
        [XmlArrayItem("CompanyCulture")]
        [IncludeInCodeGeneratorForAndroid]
        public CompanyCulture[] CompanyCultures
        { get; set; }

        /// <summary>
        /// Gets or sets the custom texts
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #region Otoucho configuration settings, should be refactored to seperate entity later

        /// <summary>
        /// Gets or sets if Age Check should be displayed in Otoucho
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public bool UseAgeCheckInOtoucho
        { get; set; }

        /// <summary>
        /// Gets or sets if the deliverypoint number should be entered manually
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public bool UseManualDeliverypointsEntryInOtoucho
        { get; set; }

        /// <summary>
        /// Gets or sets if Otoucho works stateless, which means no order history
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public bool UseStatelessInOtoucho
        { get; set; }

        /// <summary>
        /// Gets or sets if Otoucho should skip the OrderOverview page after an Order is submitted
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        public bool SkipOrderOverviewAfterOrderSubmitInOtoucho
        { get; set; }        

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Company Clone()
        {
            return this.Clone<Company>();
        }

        #endregion
    }
}
