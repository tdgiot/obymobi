using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents a productgroupItem
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "ProductgroupItem"), IncludeInCodeGeneratorForAndroid]
    public class ProductgroupItem : ModelBase
    {
        #region Constructors

        public ProductgroupItem()
        { }

        #endregion

        #region Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int ProductgroupItemId
        { get; set; }

        [IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }        

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public ProductgroupItem Clone()
        {
            return this.Clone<ProductgroupItem>();
        }

        #endregion
    }
}
