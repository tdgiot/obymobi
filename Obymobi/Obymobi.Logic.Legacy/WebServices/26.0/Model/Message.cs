using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents an advertisement
    /// </summary>
    [Serializable, XmlRoot(ElementName = "Message"), IncludeInCodeGeneratorForAndroid]
    public class Message : ModelBase
    {
        #region Constructor

        /// <summary>
        /// Constructor for message
        /// </summary>
        public Message()
        {
            
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the message id
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int MessageId 
        { get; set; }

        /// <summary>
        /// Gets or sets the customer id
        /// </summary>
        [XmlElement]
        public int CustomerId 
        { get; set; }

        /// <summary>
        /// Gets or sets the client id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ClientId
        { get; set; }

        /// <summary>
        /// Gets or sets the company id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int CompanyId 
        { get; set; }

        /// <summary>
        /// Gets or sets the company name
        /// </summary>
        [XmlElement]
        public string CompanyName 
        { get; set; }

        /// <summary>
        /// Gets or sets the title of the message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Title
        { get; set; }

        /// <summary>
        /// Gets or sets the text of the message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Text 
        { get; set; }

        /// <summary>
        /// Gets or sets the duration of the message
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Duration
        { get; set; }

        /// <summary>
        /// Gets or sets the media id of the message image
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MediaId
        { get; set; }

        /// <summary>
        /// Gets or sets the category id of the message button event
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int CategoryId
        { get; set; }        
        
        /// <summary>
        /// Gets or sets the entertainment id of the message button event
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the product id of the message button event
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int ProductId
        { get; set; }

        /// <summary>
        /// Gets or sets the site id of the message button event
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SiteId
        { get; set; }

        /// <summary>
        /// Gets or sets the page id of the message button event
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int PageId
        { get; set; }

        /// <summary>
        /// Gets or sets the url of the message button event
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Url
        { get; set; }

        /// <summary>
        /// Gets or sets if the message is urgent
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Urgent
        { get; set; }

        /// <summary>
        /// Gets or sets if the staff should be notified when the user clicks yes, but doesn't order the product
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool NotifyOnYes
        { get; set; }

        /// <summary>
        /// Gets or sets message layout type
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int MessageLayoutType
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Message Clone()
        {
            return this.Clone<Message>();
        }

        #endregion
    }
}
