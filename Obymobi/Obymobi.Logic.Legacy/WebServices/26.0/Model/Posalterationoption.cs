using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents a point-of-sale alterationoption
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Posalterationoption")]
    [DataContract]
    public class Posalterationoption : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Posorder type
        /// </summary>
        public Posalterationoption()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the company id
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int PosalterationoptionId
        { get; set; }

        /// <summary>
        /// Gets or sets the company id
        /// </summary>
        [XmlElement]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the ExternalId as used in the POS system
        /// </summary>
        [XmlElement]
        [DataMember]
        public string ExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the ExternalId of the Product to which the Posalterationoption is linked
        /// </summary>
        [XmlElement]
        public string PosproductExternalId
        { get; set; }

        /// <summary>
        /// Gets or sets the Name
        /// </summary>
        [XmlElement]
        [DataMember]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the price addition 
        /// </summary>
        [XmlElement]
        [DataMember]
        public decimal PriceIn
        { get; set; }

        /// <summary>
        /// Gets or sets the description
        /// </summary>
        public string Description
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue1
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue2
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue3
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue4
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue5
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue6
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue7
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue8
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue9
        { get; set; }

        /// <summary>
        /// Gets or sets an implementation specific custom-value
        /// </summary>
        [XmlElement]
        public string FieldValue10
        { get; set; }


        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Posalterationoption Clone()
        {
            return this.Clone<Posalterationoption>();
        }

        #endregion
    }
}
