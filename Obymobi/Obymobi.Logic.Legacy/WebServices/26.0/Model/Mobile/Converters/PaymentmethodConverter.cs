using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v26.Converters
{
	public class PaymentmethodConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v26.Paymentmethod, Obymobi.Logic.Model.Mobile.Paymentmethod>
	{
        public PaymentmethodConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v27.Converters.PaymentmethodConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v26.Paymentmethod ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Paymentmethod source)
        {
            Obymobi.Logic.Model.Mobile.v26.Paymentmethod target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v26.Paymentmethod();
                target.PaymentmethodId = source.PaymentmethodId;
                target.Name = source.Name;
                target.PaymentmethodType = source.PaymentmethodType;
                target.CompanyId = source.CompanyId;
                target.ThankTitle = source.ThankTitle;
                target.ThankMessage = source.ThankMessage;
                target.MinimumLimit = source.MinimumLimit;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Paymentmethod ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v26.Paymentmethod source)
        {
            Obymobi.Logic.Model.Mobile.Paymentmethod target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Paymentmethod();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v27.Converters.PaymentmethodConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
