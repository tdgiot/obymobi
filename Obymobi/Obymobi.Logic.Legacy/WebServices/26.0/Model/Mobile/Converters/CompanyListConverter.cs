using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v26.Converters
{
	public class CompanyListConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v26.CompanyList, Obymobi.Logic.Model.Mobile.CompanyList>
	{
        public CompanyListConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v27.Converters.CompanyListConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v26.CompanyList ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.CompanyList source)
        {
            Obymobi.Logic.Model.Mobile.v26.CompanyList target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v26.CompanyList();
                target.Ticks = source.Ticks;

                if (source.Companies != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v26.Converters.CompanyConverter companiesConverter = new Obymobi.Logic.Model.Mobile.v26.Converters.CompanyConverter();
                    target.Companies = (Company[])companiesConverter.ConvertArrayToLegacyArray(source.Companies);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.CompanyList ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v26.CompanyList source)
        {
            Obymobi.Logic.Model.Mobile.CompanyList target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.CompanyList();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v27.Converters.CompanyListConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
