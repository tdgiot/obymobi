using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v26.Converters
{
	public class BusinesshoursConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v26.Businesshours, Obymobi.Logic.Model.Mobile.Businesshours>
	{
        public BusinesshoursConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v27.Converters.BusinesshoursConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v26.Businesshours ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Businesshours source)
        {
            Obymobi.Logic.Model.Mobile.v26.Businesshours target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v26.Businesshours();
                target.BusinesshoursId = source.BusinesshoursId;
                target.OpeningDay = source.OpeningDay;
                target.OpeningTime = source.OpeningTime;
                target.ClosingDay = source.ClosingDay;
                target.ClosingTime = source.ClosingTime;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Businesshours ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v26.Businesshours source)
        {
            Obymobi.Logic.Model.Mobile.Businesshours target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Businesshours();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v27.Converters.BusinesshoursConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
