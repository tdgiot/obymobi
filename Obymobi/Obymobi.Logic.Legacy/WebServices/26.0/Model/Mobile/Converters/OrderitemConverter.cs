using Obymobi.Logic.Mobile;

namespace Obymobi.Logic.Model.Mobile.v26.Converters
{
	public class OrderitemConverter : MobileModelConverterBase<Obymobi.Logic.Model.Mobile.v26.Orderitem, Obymobi.Logic.Model.Mobile.Orderitem>
	{
        public OrderitemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.Mobile.v27.Converters.OrderitemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.Mobile.v26.Orderitem ConvertModelToLegacyModel(Obymobi.Logic.Model.Mobile.Orderitem source)
        {
            Obymobi.Logic.Model.Mobile.v26.Orderitem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.v26.Orderitem();
                target.OrderitemId = source.OrderitemId;
                target.OrderId = source.OrderId;
                target.Guid = source.Guid;
                target.CategoryId = source.CategoryId;
                target.ProductId = source.ProductId;
                target.ProductName = source.ProductName;
                target.Quantity = source.Quantity;
                target.ProductPriceIn = source.ProductPriceIn;
                target.Notes = source.Notes;

                if (source.Alterationitems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.Mobile.v26.Converters.AlterationitemConverter alterationitemsConverter = new Obymobi.Logic.Model.Mobile.v26.Converters.AlterationitemConverter();
                    target.Alterationitems = (Alterationitem[])alterationitemsConverter.ConvertArrayToLegacyArray(source.Alterationitems);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Mobile.Orderitem ConvertLegacyModelToModel(Obymobi.Logic.Model.Mobile.v26.Orderitem source)
        {
            Obymobi.Logic.Model.Mobile.Orderitem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Mobile.Orderitem();

                // Copy default values from new version
                new Obymobi.Logic.Model.Mobile.v27.Converters.OrderitemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
