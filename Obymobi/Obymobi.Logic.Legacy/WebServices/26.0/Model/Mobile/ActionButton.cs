using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.Mobile.v26
{
    /// <summary>
    /// Model class which represents an action button
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "ActionButton"), IncludeInCodeGeneratorForXamarin]
    public class ActionButton : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.ActionButton type
        /// </summary>
        public ActionButton()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the action button
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int ActionButtonId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the action button
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public ActionButton Clone()
        {
            return this.Clone<ActionButton>();
        }

        #endregion
    }
}
