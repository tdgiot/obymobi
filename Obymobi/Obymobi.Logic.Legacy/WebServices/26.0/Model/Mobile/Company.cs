using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.Mobile.v26
{
    /// <summary>
    /// Model class which represents a company for the mobile solution
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Company"), IncludeInCodeGeneratorForXamarin]
    public class Company : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Company type
        /// </summary>
        public Company()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the company (will be negative if it's a Point of interest (== PointOfInterestId * -1))
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the currency of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Currency
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the addressline2 of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Addressline1
        { get; set; }

        /// <summary>
        /// Gets or sets the addressline2 of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Addressline2
        { get; set; }

        /// <summary>
        /// Gets or sets the addressline3 of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Addressline3
        { get; set; }

        /// <summary>
        /// Gets or sets the zipcode of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Zipcode
        { get; set; }

        /// <summary>
        /// Gets or sets the city of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string City
        { get; set; }

        /// <summary>
        /// Gets or sets the country of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Country
        { get; set; }

        /// <summary>
        /// Map cooridates, latitude
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public double? Latitude
        { get; set; }

        /// <summary>
        /// Map cooridates, longitude
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public double? Longitude
        { get; set; }

        /// <summary>
        /// GeoFencing Enabled
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool? GeoFencingEnabled { get; set; }

        public bool ShouldSerializeGeoFencingEnabled() { return this.GeoFencingEnabled.HasValue; }

        /// <summary>
        /// GeoFencing Radius (in meters)
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int? GeoFencingRadius { get; set; }

        public bool ShouldSerializeGeoFencingRadius() { return this.GeoFencingRadius.HasValue; }

        /// <summary>
        /// Gets or sets the flag which indicates that free text is allowed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool AllowFreeText { get; set; }

        public bool ShouldSerializeAllowFreeText() { return this.AllowFreeText == true; }        

        /// <summary>
        /// Gets or sets the CultureCode
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string CultureCode
        { get; set; }

        /// <summary>
        /// Gets or sets the telephone number of the company
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string Telephone
        { get; set; }

        /// <summary>
        /// Gets or sets the Google Analytics Tracker Id
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string GoogleAnalyticsId
        { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the company should be displayed on the hero section of the carrousel view
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public bool IsHero
        { get; set; }

        [XmlArray("Businesshourss")] // A bit weird, but the only way to follow the convention
        [XmlArrayItem("Businesshours")]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public Businesshours[] Businesshours
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string BusinesshoursIntermediate
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int? ActionButtonId
        { get;set; }

        public bool ShouldSerializeActionButtonId() { return this.ActionButtonId.HasValue; }
        
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string ActionButtonUrlTablet
        { get;set; }
        
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string ActionButtonUrlMobile
        { get;set; }
        
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public int? CostIndicationType
        { get;set; }

        public bool ShouldSerializeCostIndicationType() { return this.CostIndicationType.HasValue; }
        
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public decimal? CostIndicationValue
        { get; set; }

        public bool ShouldSerializeCostIndicationValue() { return this.CostIndicationValue.HasValue; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Data (not the menu) was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public long CompanyDataLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Media (not the menu) was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public long CompanyMediaLastModifiedTicks
        { get; set; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Menu was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public long? MenuDataLastModifiedTicks
        { get; set; }

        public bool ShouldSerializeMenuDataLastModifiedTicks() { return this.MenuDataLastModifiedTicks.HasValue; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Media for the Menu was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public long? MenuMediaLastModifiedTicks
        { get; set; }

        public bool ShouldSerializeMenuMediaLastModifiedTicks() { return this.MenuMediaLastModifiedTicks.HasValue; }

        /// <summary>
        /// Gets or sets the Ticks of the last moment the Company related Deliverypoints was changed
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public long? DeliverypointDataLastModifiedTicks
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string AzureNotificationTag { get; set; }

        public bool ShouldSerializeDeliverypointDataLastModifiedTicks() { return this.DeliverypointDataLastModifiedTicks.HasValue; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin]
        public string TimeZone { get; set; }

        /// <summary>
        /// Gets or sets the media of the company
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForXamarin]
        public Media[] Media
        { get; set; }        

        /// <summary>
        /// Gets or sets the deliverypoint groups of the company
        /// </summary>
        [XmlArray("Deliverypointgroups")]
        [XmlArrayItem("Deliverypointgroup")]
        [IncludeInCodeGeneratorForXamarin]
        public Deliverypointgroup[] Deliverypointgroups
        { get; set; }

        /// <summary>
        /// Gets or sets the payment methods of the company
        /// </summary>
        [XmlArray("Paymentmethods")]
        [XmlArrayItem("Paymentmethods")]
        [IncludeInCodeGeneratorForXamarin]
        public Paymentmethod[] Paymentmethods
        { get; set; }

        /// <summary>
        /// Gets or sets the VenueCategories relevant to this Company
        /// </summary>
        [IncludeInCodeGeneratorForXamarin]
        public int[] VenueCategoryIds
        { get; set; }

        /// <summary>
        /// Gets or sets the Amenities relevant to this Company
        /// </summary>
        [IncludeInCodeGeneratorForXamarin]
        public int[] AmenityIds
        { get; set; }

        /// <summary>
        /// Gets or sets the schedules relevant to this Company
        /// </summary>
        [XmlArray("Schedules")]
        [XmlArrayItem("Schedule")]
        [IncludeInCodeGeneratorForXamarin]
        public Schedule[] Schedules
        { get; set; }

        /// <summary>
        /// Gets or sets the company cultures
        /// </summary>
        [XmlArray("CompanyCultures")]
        [XmlArrayItem("CompanyCulture")]
        [IncludeInCodeGeneratorForXamarin]
        public CompanyCulture[] CompanyCultures
        { get; set; }

        /// <summary>
        /// Gets or sets the company currencies
        /// </summary>
        [XmlArray("CompanyCurrencies")]
        [XmlArrayItem("CompanyCurrency")]
        [IncludeInCodeGeneratorForXamarin]
        public CompanyCurrency[] CompanyCurrencys
        { get; set; }

        #region PointOfInterestSpecificFeatures

        /// <summary>
        /// Determines if this is a Point of Interest (in contrast to a 'Normal' company)
        /// </summary>
        [IncludeInCodeGeneratorForXamarin]
        [XmlElement]
        public bool IsPointOfInterest
        {
            get;
            set;
        }

        ///// <summary>
        ///// Determines if this is external content (in contrast to a 'Normal' company)
        ///// </summary>
        //[IncludeInCodeGeneratorForXamarin]
        //[XmlElement("IsExternal")]
        //public bool IsExternalContent
        //{
        //    get;
        //    set;
        //}

        /// <summary>
        /// Mobile UI Mode
        /// </summary>
        [IncludeInCodeGeneratorForXamarin]
        public UIMode PointOfInterestUIMode
        { get; set; }

        /// <summary>
        /// Gets or sets the custom texts for this company
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Company Clone()
        {
            return this.Clone<Company>();
        }

        public Company CloneUsingBinaryFormatter()
        {
            return this.CloneUsingBinaryFormatter<Company>();
        }

        #endregion
    }
}
