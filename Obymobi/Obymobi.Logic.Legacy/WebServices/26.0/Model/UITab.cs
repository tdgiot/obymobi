using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents a UITab item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "UITab"), IncludeInCodeGeneratorForAndroid]
    public class UITab : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.UITab type
        /// </summary>
        public UITab()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the tab
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
		[JsonProperty("uitabid")]
        public int UITabId
        { get; set; }

        /// Gets or sets the id of the UIMode this tab belongs to
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
		[JsonProperty("uimodeid")]
        public int UIModeId
        { get; set; }

        /// <summary>
        /// Gets or sets the caption of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Caption
        { get; set; }

        /// <summary>
        /// Gets or sets the type of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        /// <summary>
        /// Gets or sets the categoryId of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int CategoryId
        { get; set; }

        /// <summary>
        /// Gets or sets the entertainmentId of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int EntertainmentId
        { get; set; }

        /// <summary>
        /// Gets or sets the siteId of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SiteId
        { get; set; }

        /// <summary>
        /// Gets or sets the URL of the tab
        /// </summary>
        [XmlElement]
		[JsonProperty("Url")]
        [IncludeInCodeGeneratorForAndroid]
        public string URL
        { get; set; }

        /// <summary>
        /// Gets or sets the zoom level of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Zoom
        { get; set; }

        /// <summary>
        /// Gets or sets the width of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Width
        { get; set; }

        /// <summary>
        /// Gets or sets the sort order of the tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }

        /// <summary>
        /// Gets or sets if the tab should be visible or not
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Visible
        { get; set; }

        /// <summary>
        /// Gets or sets if the urls should be monitored or not
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool RestrictedAccess
        { get; set; }

        /// <summary>
        /// Gets or sets if the All category should be visible on the More tab
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool AllCategoryVisible
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool ShowPmsMessagegroups { get; set; }

        /// <summary>
        /// Gets or sets the widgets
        /// </summary>
        [XmlArray("UIWidgets")]
        [XmlArrayItem("UIWidget")]
        [IncludeInCodeGeneratorForAndroid]
        public UIWidget[] UIWidgets
        { get; set; }        

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UITab Clone()
        {
            return this.Clone<UITab>();
        }

        #endregion
    }
}
