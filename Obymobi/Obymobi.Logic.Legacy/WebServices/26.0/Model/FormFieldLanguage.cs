using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents a formField language
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "FormFieldLanguage")]
    public class FormFieldLanguage : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.FormFieldLanguage type
        /// </summary>
        public FormFieldLanguage()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the formField language
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int FormFieldLanguageId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the formField
        /// </summary>
        [XmlElement]
        public int FormFieldId
        { get; set; }

        /// <summary>
        /// Gets or sets the language code
        /// </summary>
        [XmlElement]
        public string LanguageCode
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the formField
        /// </summary>
        [XmlElement]
        public string Name
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public FormFieldLanguage Clone()
        {
            return this.Clone<FormFieldLanguage>();
        }

        #endregion
    }
}
