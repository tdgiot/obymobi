using System;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents a theme text size.
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "UIThemeTextSize"), IncludeInCodeGeneratorForAndroid]
    public class UIThemeTextSize : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.UIThemeTextSize type
        /// </summary>
        public UIThemeTextSize()
        {
        }

        #endregion

        #region Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int UIThemeTextSizeId
        { get; set; }

        [XmlElement]
        public int UIThemeId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int TextSize
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public UIThemeTextSize Clone()
        {
            return this.Clone<UIThemeTextSize>();
        }

        #endregion
    }
}
