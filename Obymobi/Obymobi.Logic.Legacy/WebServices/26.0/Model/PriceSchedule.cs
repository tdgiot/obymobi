using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents an price schedule
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "PriceSchedule"), IncludeInCodeGeneratorForAndroid]
    public class PriceSchedule : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.PriceSchedule type
        /// </summary>
        public PriceSchedule()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the price schedule
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int PriceScheduleId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the schedule
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }        

        /// <summary>
        /// Gets or sets the schedule items
        /// </summary>
        [XmlArray("PriceScheduleItems")]
        [XmlArrayItem("PriceScheduleItem")]
        [IncludeInCodeGeneratorForAndroid]
        public PriceScheduleItem[] PriceScheduleItems
        { get; set; }

        /// <summary>
        /// Gets or sets the price levels
        /// </summary>
        [XmlArray("PriceLevels")]
        [XmlArrayItem("PriceLevel")]
        [IncludeInCodeGeneratorForAndroid]
        public PriceLevel[] PriceLevels
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public PriceSchedule Clone()
        {
            return this.Clone<PriceSchedule>();
        }

        #endregion
    }
}
