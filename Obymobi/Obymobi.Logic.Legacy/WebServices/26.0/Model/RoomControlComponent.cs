using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents a room control component
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "RoomControlComponent"), IncludeInCodeGeneratorForAndroid]
    public class RoomControlComponent : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.RoomControlComponent type
        /// </summary>
        public RoomControlComponent()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id of the room control component
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int RoomControlComponentId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string NameSystem
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int Type
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public bool Visible
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue1
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue2
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue3
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue4
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string FieldValue5
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public int InfraredConfigurationId
        { get; set; }

        /// <summary>
        /// Gets or sets the custom text collection
        /// </summary>
        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public RoomControlComponent Clone()
        {
            return this.Clone<RoomControlComponent>();
        }

        #endregion
    }
}
