using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents a schedule item
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "Schedule"), IncludeInCodeGeneratorForFlex, IncludeInCodeGeneratorForAndroid, IncludeInCodeGeneratorForMobile]
    public class Schedule : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Schedule type
        /// </summary>
        public Schedule()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the schedule item
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForAndroid]
        public int ScheduleId
        { get; set; }

        /// <summary>
        /// Gets or sets the start time
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the schedule items
        /// </summary>
        [XmlArray("Scheduleitems")]
        [XmlArrayItem("Scheduleitem")]
        [IncludeInCodeGeneratorForAndroid]
        public Scheduleitem[] Scheduleitems
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Schedule Clone()
        {
            return this.Clone<Schedule>();
        }

        #endregion
    }
}
