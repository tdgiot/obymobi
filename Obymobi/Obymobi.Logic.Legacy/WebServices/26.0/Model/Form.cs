using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents a form
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Form")]
    public class Form : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Form type
        /// </summary>
        public Form()
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets of sets the id of the form
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int FormId
        { get; set;}

        /// <summary>
        /// Gets or sets the name of the form
        /// </summary>
        [XmlElement]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the title of the form
        /// </summary>
        [XmlElement]
        public string Title
        { get; set; }

        /// <summary>
        /// Gets or sets the subtitle of the form
        /// </summary>
        [XmlElement]
        public string Subtitle
        { get; set; }

        /// <summary>
        /// Gets or sets the saving title
        /// </summary>
        [XmlElement]
        public string SavingTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the saving message
        /// </summary>
        [XmlElement]
        public string SavingMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the processed title
        /// </summary>
        [XmlElement]
        public string ProcessedTitle
        { get; set; }

        /// <summary>
        /// Gets or sets the processed message
        /// </summary>
        [XmlElement]
        public string ProcessedMessage
        { get; set; }

        /// <summary>
        /// Gets or sets the fields of the form
        /// </summary>
        [XmlArray("Fields")]
        [XmlArrayItem("FormField")]
        public FormField[] Fields
        { get; set; }

        /// <summary>
        /// Gets or sets the translations
        /// </summary>
        [XmlArray("FormLanguages")]
        [XmlArrayItem("FormLanguage")]
        public FormLanguage[] FormLanguages
        { get; set; }

        /// <summary>
        /// Gets or sets the media of the form
        /// </summary>
        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        public Media[] Media
        { get; set; }
    
        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Form Clone()
        {
            return this.Clone<Form>();
        }

        #endregion
    }
}
