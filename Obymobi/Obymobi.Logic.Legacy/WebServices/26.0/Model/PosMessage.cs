using Newtonsoft.Json;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// PosMessage model
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class PosMessage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PosMessage"/> class.
        /// </summary>
        public PosMessage()
        {
        }

        #region Properties

        /// <summary>
        /// Gets or sets the type of the pos service message.
        /// </summary>
        /// <value>
        /// The type of the pos service message.
        /// </value>
        [JsonProperty]
        public int PosServiceMessageType 
        { get; set; }

        /// <summary>
        /// Gets or sets the pos service message.
        /// </summary>
        /// <value>
        /// The pos service message.
        /// </value>
        [JsonProperty]
        public string PosServiceMessage
        { get; set; }

        #endregion
    }
}
