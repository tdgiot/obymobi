using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents an feature
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Feature")]
    public class Feature : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Feature type
        /// </summary>
        public Feature()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the feature
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        public int FeatureId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the company
        /// </summary>
        [XmlElement]
        public int CompanyId
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the feature
        /// </summary>
        [XmlElement]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the code of the feature
        /// </summary>
        [XmlElement]
        public int Code
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public Feature Clone()
        {
            return this.Clone<Feature>();
        }

        #endregion
    }
}
