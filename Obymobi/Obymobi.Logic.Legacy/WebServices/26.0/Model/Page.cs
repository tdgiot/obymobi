using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;
using Obymobi.Enums;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents a company for the mobile solution
    /// </summary>
    [Serializable, XmlRootAttribute(ElementName = "Page"),
    IncludeInCodeGeneratorForXamarin,
    IncludeInCodeGeneratorForAndroid]
    public class Page : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.Company type
        /// </summary>
        public Page()
        {
        }

        #endregion

        #region Properties

        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int PageId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int SiteId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public string Name
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int ParentPageId
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int PageType
        { get; set; }

        [XmlElement]
        [IncludeInCodeGeneratorForXamarin,
        IncludeInCodeGeneratorForAndroid]
        public int SortOrder
        { get; set; }        

        [XmlArray("Media")]
        [XmlArrayItem("Media")]
        [IncludeInCodeGeneratorForXamarin]
        [IncludeInCodeGeneratorForAndroid]
        public Media[] Media
        { get; set; }

        [XmlArray("PageElements")]
        [XmlArrayItem("PageElement")]
        [IncludeInCodeGeneratorForXamarin]
        [IncludeInCodeGeneratorForAndroid]
        public PageElement[] PageElements
        { get; set; }

        [XmlArray("Attachments")]
        [XmlArrayItem("Attachment")]
        [IncludeInCodeGeneratorForAndroid]
        [IncludeInCodeGeneratorForMobile]
        [IncludeInCodeGeneratorForXamarin]
        public Attachment[] Attachments
        { get; set; }

        [XmlArray("CustomTexts")]
        [XmlArrayItem("CustomText")]
        [IncludeInCodeGeneratorForXamarin]
        [IncludeInCodeGeneratorForAndroid]
        public CustomText[] CustomTexts
        { get; set; }

        #endregion

        #region Methods

        public Company Clone()
        {
            return this.Clone<Company>();
        }

        #endregion
    }
}
