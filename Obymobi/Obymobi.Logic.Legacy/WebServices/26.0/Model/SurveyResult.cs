using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Obymobi.Attributes;

namespace Obymobi.Logic.Model.v26
{
    /// <summary>
    /// Model class which represents an survey result
    /// </summary>
	[Serializable, XmlRootAttribute(ElementName = "SurveyResult"), IncludeInCodeGeneratorForFlex]
    public class SurveyResult : ModelBase
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.Model.SurveyResult type
        /// </summary>
        public SurveyResult()
        {
        }

        #endregion

        #region Xml Properties

        /// <summary>
        /// Gets or sets the id of the survey
        /// </summary>
        [XmlElement, PrimaryKeyFieldOfModel]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyResultId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the survey question
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int SurveyId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the customer
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int CustomerId
        { get; set; }

        /// <summary>
        /// Gets or sets the id of the client
        /// </summary>
        [XmlElement]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public int ClientId
        { get; set; }

        /// <summary>
        /// Gets or sets the survey result answers of the survey
        /// </summary>
        [XmlArray("SurveyResultAnswers")]
        [XmlArrayItem("SurveyResultAnswer")]
        [IncludeInCodeGeneratorForFlex]
        [IncludeInCodeGeneratorForAndroid]
        public SurveyResultAnswer[] SurveyResultAnswers
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns></returns>
        public SurveyResult Clone()
        {
            return this.Clone<SurveyResult>();
        }

        #endregion
    }
}
