namespace Obymobi.Logic.Model.v26.Converters
{
	public class InfraredConfigurationConverter : ModelConverterBase<Obymobi.Logic.Model.v26.InfraredConfiguration, Obymobi.Logic.Model.InfraredConfiguration>
	{
        public InfraredConfigurationConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v27.Converters.InfraredConfigurationConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v26.InfraredConfiguration ConvertModelToLegacyModel(Obymobi.Logic.Model.InfraredConfiguration source)
        {
            Obymobi.Logic.Model.v26.InfraredConfiguration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v26.InfraredConfiguration();
                target.InfraredConfigurationId = source.InfraredConfigurationId;
                target.Name = source.Name;
                target.MillisecondsBetweenCommands = source.MillisecondsBetweenCommands;

                if (source.InfraredCommands != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.InfraredCommandConverter infraredCommandsConverter = new Obymobi.Logic.Model.v26.Converters.InfraredCommandConverter();
                    target.InfraredCommands = (InfraredCommand[])infraredCommandsConverter.ConvertArrayToLegacyArray(source.InfraredCommands);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.InfraredConfiguration ConvertLegacyModelToModel(Obymobi.Logic.Model.v26.InfraredConfiguration source)
        {
            Obymobi.Logic.Model.InfraredConfiguration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.InfraredConfiguration();

                // Copy default values from new version
                new Obymobi.Logic.Model.v27.Converters.InfraredConfigurationConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
