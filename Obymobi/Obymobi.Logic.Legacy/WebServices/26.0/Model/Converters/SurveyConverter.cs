namespace Obymobi.Logic.Model.v26.Converters
{
	public class SurveyConverter : ModelConverterBase<Obymobi.Logic.Model.v26.Survey, Obymobi.Logic.Model.Survey>
	{
        public SurveyConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v27.Converters.SurveyConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v26.Survey ConvertModelToLegacyModel(Obymobi.Logic.Model.Survey source)
        {
            Obymobi.Logic.Model.v26.Survey target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v26.Survey();
                target.SurveyId = source.SurveyId;
                target.CompanyId = source.CompanyId;
                target.Name = source.Name;
                target.SavingTitle = source.SavingTitle;
                target.SavingMessage = source.SavingMessage;
                target.ProcessedTitle = source.ProcessedTitle;
                target.ProcessedMessage = source.ProcessedMessage;
                target.AnswerRequiredTitle = source.AnswerRequiredTitle;
                target.AnswerRequiredMessage = source.AnswerRequiredMessage;

                if (source.SurveyPages != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.SurveyPageConverter surveyPagesConverter = new Obymobi.Logic.Model.v26.Converters.SurveyPageConverter();
                    target.SurveyPages = (SurveyPage[])surveyPagesConverter.ConvertArrayToLegacyArray(source.SurveyPages);
                }

                if (source.SurveyLanguages != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.SurveyLanguageConverter surveyLanguagesConverter = new Obymobi.Logic.Model.v26.Converters.SurveyLanguageConverter();
                    target.SurveyLanguages = (SurveyLanguage[])surveyLanguagesConverter.ConvertArrayToLegacyArray(source.SurveyLanguages);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v26.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Survey ConvertLegacyModelToModel(Obymobi.Logic.Model.v26.Survey source)
        {
            Obymobi.Logic.Model.Survey target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Survey();

                // Copy default values from new version
                new Obymobi.Logic.Model.v27.Converters.SurveyConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
