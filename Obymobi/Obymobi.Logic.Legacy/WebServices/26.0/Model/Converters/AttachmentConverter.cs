namespace Obymobi.Logic.Model.v26.Converters
{
	public class AttachmentConverter : ModelConverterBase<Obymobi.Logic.Model.v26.Attachment, Obymobi.Logic.Model.Attachment>
	{
        public AttachmentConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v27.Converters.AttachmentConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v26.Attachment ConvertModelToLegacyModel(Obymobi.Logic.Model.Attachment source)
        {
            Obymobi.Logic.Model.v26.Attachment target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v26.Attachment();
                target.AttachmentId = source.AttachmentId;
                target.ProductId = source.ProductId;
                target.PageId = source.PageId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.Url = source.Url;
                target.AllowedDomains = source.AllowedDomains;
                target.AllowAllDomains = source.AllowAllDomains;

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v26.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v26.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Attachment ConvertLegacyModelToModel(Obymobi.Logic.Model.v26.Attachment source)
        {
            Obymobi.Logic.Model.Attachment target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Attachment();

                // Copy default values from new version
                new Obymobi.Logic.Model.v27.Converters.AttachmentConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
