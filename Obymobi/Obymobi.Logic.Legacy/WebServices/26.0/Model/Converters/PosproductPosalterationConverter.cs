namespace Obymobi.Logic.Model.v26.Converters
{
	public class PosproductPosalterationConverter : ModelConverterBase<Obymobi.Logic.Model.v26.PosproductPosalteration, Obymobi.Logic.Model.PosproductPosalteration>
	{
        public PosproductPosalterationConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v27.Converters.PosproductPosalterationConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v26.PosproductPosalteration ConvertModelToLegacyModel(Obymobi.Logic.Model.PosproductPosalteration source)
        {
            Obymobi.Logic.Model.v26.PosproductPosalteration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v26.PosproductPosalteration();
                target.PosproductPosalterationId = source.PosproductPosalterationId;
                target.ExternalId = source.ExternalId;
                target.ExternalPosproductId = source.ExternalPosproductId;
                target.ExternalPosalterationId = source.ExternalPosalterationId;
            }

            return target;
        }

        public override Obymobi.Logic.Model.PosproductPosalteration ConvertLegacyModelToModel(Obymobi.Logic.Model.v26.PosproductPosalteration source)
        {
            Obymobi.Logic.Model.PosproductPosalteration target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.PosproductPosalteration();

                // Copy default values from new version
                new Obymobi.Logic.Model.v27.Converters.PosproductPosalterationConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
