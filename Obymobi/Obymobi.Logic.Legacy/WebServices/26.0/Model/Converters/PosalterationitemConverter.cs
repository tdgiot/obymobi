namespace Obymobi.Logic.Model.v26.Converters
{
	public class PosalterationitemConverter : ModelConverterBase<Obymobi.Logic.Model.v26.Posalterationitem, Obymobi.Logic.Model.Posalterationitem>
	{
        public PosalterationitemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v27.Converters.PosalterationitemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v26.Posalterationitem ConvertModelToLegacyModel(Obymobi.Logic.Model.Posalterationitem source)
        {
            Obymobi.Logic.Model.v26.Posalterationitem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v26.Posalterationitem();
                target.PosalterationitemId = source.PosalterationitemId;
                target.ExternalId = source.ExternalId;
                target.ExternalPosalterationId = source.ExternalPosalterationId;
                target.ExternalPosalterationoptionId = source.ExternalPosalterationoptionId;
                target.SelectOnDefault = source.SelectOnDefault;
                target.SortOrder = source.SortOrder;
                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Posalterationitem ConvertLegacyModelToModel(Obymobi.Logic.Model.v26.Posalterationitem source)
        {
            Obymobi.Logic.Model.Posalterationitem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Posalterationitem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v27.Converters.PosalterationitemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
