namespace Obymobi.Logic.Model.v26.Converters
{
	public class MenuUpdatedConverter : ModelConverterBase<Obymobi.Logic.Model.v26.MenuUpdated, Obymobi.Logic.Model.MenuUpdated>
	{
        public MenuUpdatedConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v27.Converters.MenuUpdatedConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v26.MenuUpdated ConvertModelToLegacyModel(Obymobi.Logic.Model.MenuUpdated source)
        {
            Obymobi.Logic.Model.v26.MenuUpdated target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v26.MenuUpdated();

                if (source.ProductmenuItems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.ProductmenuItemConverter productmenuItemsConverter = new Obymobi.Logic.Model.v26.Converters.ProductmenuItemConverter();
                    target.ProductmenuItems = (ProductmenuItem[])productmenuItemsConverter.ConvertArrayToLegacyArray(source.ProductmenuItems);
                }

                target.MenuUpdatedAction = source.MenuUpdatedAction;
            }

            return target;
        }

        public override Obymobi.Logic.Model.MenuUpdated ConvertLegacyModelToModel(Obymobi.Logic.Model.v26.MenuUpdated source)
        {
            Obymobi.Logic.Model.MenuUpdated target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.MenuUpdated();

                // Copy default values from new version
                new Obymobi.Logic.Model.v27.Converters.MenuUpdatedConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
