namespace Obymobi.Logic.Model.v26.Converters
{
	public class AdvertisementConverter : ModelConverterBase<Obymobi.Logic.Model.v26.Advertisement, Obymobi.Logic.Model.Advertisement>
	{
        public AdvertisementConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v27.Converters.AdvertisementConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v26.Advertisement ConvertModelToLegacyModel(Obymobi.Logic.Model.Advertisement source)
        {
            Obymobi.Logic.Model.v26.Advertisement target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v26.Advertisement();
                target.AdvertisementId = source.AdvertisementId;
                target.CompanyId = source.CompanyId;
                target.Name = source.Name;
                target.Description = source.Description;
                target.ProductId = source.ProductId;
                target.EntertainmentId = source.EntertainmentId;
                target.ActionCategoryId = source.ActionCategoryId;
                target.ActionEntertainmentId = source.ActionEntertainmentId;
                target.ActionEntertainmentCategoryId = source.ActionEntertainmentCategoryId;
                target.ActionUrl = source.ActionUrl;
                target.ActionSiteId = source.ActionSiteId;
                target.ActionPageId = source.ActionPageId;

                if (source.AdvertisementTags != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.AdvertisementTagConverter advertisementTagsConverter = new Obymobi.Logic.Model.v26.Converters.AdvertisementTagConverter();
                    target.AdvertisementTags = (AdvertisementTag[])advertisementTagsConverter.ConvertArrayToLegacyArray(source.AdvertisementTags);
                }

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v26.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v26.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Advertisement ConvertLegacyModelToModel(Obymobi.Logic.Model.v26.Advertisement source)
        {
            Obymobi.Logic.Model.Advertisement target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Advertisement();

                // Copy default values from new version
                new Obymobi.Logic.Model.v27.Converters.AdvertisementConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
