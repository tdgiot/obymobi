namespace Obymobi.Logic.Model.v26.Converters
{
	public class LanguageConverter : ModelConverterBase<Obymobi.Logic.Model.v26.Language, Obymobi.Logic.Model.Language>
	{
        public LanguageConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v27.Converters.LanguageConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v26.Language ConvertModelToLegacyModel(Obymobi.Logic.Model.Language source)
        {
            Obymobi.Logic.Model.v26.Language target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v26.Language();
                target.Name = source.Name;
                target.Code = source.Code;
                target.LocalizedName = source.LocalizedName;
                target.Description = source.Description;
                target.DeliverypointCaption = source.DeliverypointCaption;
                target.VenuePageDescription = source.VenuePageDescription;
                target.BrowserAgeVerificationTitle = source.BrowserAgeVerificationTitle;
                target.BrowserAgeVerificationText = source.BrowserAgeVerificationText;
                target.Parent = source.Parent;
            }

            return target;
        }

        public override Obymobi.Logic.Model.Language ConvertLegacyModelToModel(Obymobi.Logic.Model.v26.Language source)
        {
            Obymobi.Logic.Model.Language target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Language();

                // Copy default values from new version
                new Obymobi.Logic.Model.v27.Converters.LanguageConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
