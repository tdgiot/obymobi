namespace Obymobi.Logic.Model.v26.Converters
{
	public class OrderConverter : ModelConverterBase<Obymobi.Logic.Model.v26.Order, Obymobi.Logic.Model.Order>
	{
        public OrderConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v27.Converters.OrderConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v26.Order ConvertModelToLegacyModel(Obymobi.Logic.Model.Order source)
        {
            Obymobi.Logic.Model.v26.Order target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v26.Order();
                target.OrderId = source.OrderId;
                target.Guid = source.Guid;
                target.CustomerId = source.CustomerId;
                target.ClientId = source.ClientId;
                target.CompanyId = source.CompanyId;
                target.DeliverypointId = source.DeliverypointId;
                target.DeliverypointNumber = source.DeliverypointNumber;
                target.DeliverypointName = source.DeliverypointName;
                target.Status = source.Status;
                target.StatusText = source.StatusText;
                target.ErrorCode = source.ErrorCode;
                target.Notes = source.Notes;
                target.BenchmarkInformation = source.BenchmarkInformation;
                target.MobileClientHistoryOrderPriceInTotal = source.MobileClientHistoryOrderPriceInTotal;
                target.CustomerNameFull = source.CustomerNameFull;
                target.CustomerPhonenumber = source.CustomerPhonenumber;
                target.ClientMacAddress = source.ClientMacAddress;
                target.CompanyName = source.CompanyName;
                target.CompanyObycode = source.CompanyObycode;
                target.PaymentmethodName = source.PaymentmethodName;
                target.Type = source.Type;
                target.TypeText = source.TypeText;
                target.ConfirmationCode = source.ConfirmationCode;
                target.ConfirmationCodeDeliveryType = source.ConfirmationCodeDeliveryType;
                target.Updated = source.Updated;
                target.Created = source.Created;
                target.ProcessingExpired = source.ProcessingExpired;
                target.PhoneInfo = source.PhoneInfo;
                target.AgeVerificationType = source.AgeVerificationType;
                target.ErrorSentByEmail = source.ErrorSentByEmail;
                target.ErrorSentBySMS = source.ErrorSentBySMS;
                target.Processed = source.Processed;
                target.PlaceTime = source.PlaceTime;
                target.SupportpoolId = source.SupportpoolId;
                target.Printed = source.Printed;
                target.MobileOrder = source.MobileOrder;
                target.Email = source.Email;
                target.File = source.File;
                target.IsCustomerVip = source.IsCustomerVip;
                target.PriceScheduleId = source.PriceScheduleId;
                target.AnalyticsPayLoad = source.AnalyticsPayLoad;
                target.AnalyticsTrackingIds = source.AnalyticsTrackingIds;
                target.HasPosRoutestephandler = source.HasPosRoutestephandler;
                target.OnTheCaseCaption = source.OnTheCaseCaption;
                target.CompleteCaption = source.CompleteCaption;
                target.ManuallyProcessOrderCaption = source.ManuallyProcessOrderCaption;

                if (source.Orderitems != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.OrderitemConverter orderitemsConverter = new Obymobi.Logic.Model.v26.Converters.OrderitemConverter();
                    target.Orderitems = (Orderitem[])orderitemsConverter.ConvertArrayToLegacyArray(source.Orderitems);
                }

                if (source.OrderRoutestephandlers != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.OrderRoutestephandlerConverter orderRoutestephandlersConverter = new Obymobi.Logic.Model.v26.Converters.OrderRoutestephandlerConverter();
                    target.OrderRoutestephandlers = (OrderRoutestephandler[])orderRoutestephandlersConverter.ConvertArrayToLegacyArray(source.OrderRoutestephandlers);
                }

                target.FieldValue1 = source.FieldValue1;
                target.FieldValue2 = source.FieldValue2;
                target.FieldValue3 = source.FieldValue3;
                target.FieldValue4 = source.FieldValue4;
                target.FieldValue5 = source.FieldValue5;
                target.FieldValue6 = source.FieldValue6;
                target.FieldValue7 = source.FieldValue7;
                target.FieldValue8 = source.FieldValue8;
                target.FieldValue9 = source.FieldValue9;
                target.FieldValue10 = source.FieldValue10;

                if (source.Posorder != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v26.Converters.PosorderConverter posorderConverter = new Obymobi.Logic.Model.v26.Converters.PosorderConverter();
                    target.Posorder = (Posorder)posorderConverter.ConvertModelToLegacyModel(source.Posorder);
                }

                target.AutoId = source.AutoId;
                target.CreatedOnServer = source.CreatedOnServer;
                target.CreatedAsDateTime = source.CreatedAsDateTime;
                target.IsWebserviceOrder = source.IsWebserviceOrder;
                target.IsSavedOnWebservice = source.IsSavedOnWebservice;
                target.ValidatedByOrderProcessingHelper = source.ValidatedByOrderProcessingHelper;

                if (source.OrderRoutestephandler != null)
                {
                    // Create the converter class for this and convert this model
                    Obymobi.Logic.Model.v26.Converters.OrderRoutestephandlerConverter orderRoutestephandlerConverter = new Obymobi.Logic.Model.v26.Converters.OrderRoutestephandlerConverter();
                    target.OrderRoutestephandler = (OrderRoutestephandler)orderRoutestephandlerConverter.ConvertModelToLegacyModel(source.OrderRoutestephandler);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.Order ConvertLegacyModelToModel(Obymobi.Logic.Model.v26.Order source)
        {
            Obymobi.Logic.Model.Order target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.Order();

                // Copy default values from new version
                new Obymobi.Logic.Model.v27.Converters.OrderConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
