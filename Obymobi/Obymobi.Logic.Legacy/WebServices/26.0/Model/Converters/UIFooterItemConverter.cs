namespace Obymobi.Logic.Model.v26.Converters
{
	public class UIFooterItemConverter : ModelConverterBase<Obymobi.Logic.Model.v26.UIFooterItem, Obymobi.Logic.Model.UIFooterItem>
	{
        public UIFooterItemConverter()
        {
            // Set the excluded fields
            
            // Set the field values
            
        }

        public override void CopyDefaultFields(System.Collections.Generic.List<string> fieldsToExclude, System.Collections.Generic.Dictionary<string, object> fieldValues)
        {
            // Copy fields from new version
            new Obymobi.Logic.Model.v27.Converters.UIFooterItemConverter().CopyDefaultFields(fieldsToExclude, fieldValues);
            
            // Copy fields to exclude
            foreach (string fieldToExclude in this.FieldsToExclude)
            {
               if (!fieldsToExclude.Contains(fieldToExclude))
                   fieldsToExclude.Add(fieldToExclude);
            }
            
            // Copy field values
            foreach (var fieldValue in this.FieldValues)
            {
               if (!fieldValues.ContainsKey(fieldValue.Key))
                   fieldValues.Add(fieldValue.Key, fieldValue.Value);
            }
        }

        public override Obymobi.Logic.Model.v26.UIFooterItem ConvertModelToLegacyModel(Obymobi.Logic.Model.UIFooterItem source)
        {
            Obymobi.Logic.Model.v26.UIFooterItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.v26.UIFooterItem();
                target.UIFooterItemId = source.UIFooterItemId;
                target.Name = source.Name;
                target.Type = source.Type;
                target.Position = source.Position;
                target.SortOrder = source.SortOrder;
                target.ActionIntent = source.ActionIntent;
                target.Visible = source.Visible;

                if (source.Media != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.MediaConverter mediaConverter = new Obymobi.Logic.Model.v26.Converters.MediaConverter();
                    target.Media = (Media[])mediaConverter.ConvertArrayToLegacyArray(source.Media);
                }

                if (source.CustomTexts != null)
                {
                    // Create the converter class for this and convert this array
                    Obymobi.Logic.Model.v26.Converters.CustomTextConverter customTextsConverter = new Obymobi.Logic.Model.v26.Converters.CustomTextConverter();
                    target.CustomTexts = (CustomText[])customTextsConverter.ConvertArrayToLegacyArray(source.CustomTexts);
                }
            }

            return target;
        }

        public override Obymobi.Logic.Model.UIFooterItem ConvertLegacyModelToModel(Obymobi.Logic.Model.v26.UIFooterItem source)
        {
            Obymobi.Logic.Model.UIFooterItem target = null;

            if (source != null)
            {
                target = new Obymobi.Logic.Model.UIFooterItem();

                // Copy default values from new version
                new Obymobi.Logic.Model.v27.Converters.UIFooterItemConverter().CopyDefaultFields(this.FieldsToExclude, this.FieldValues);

                // Copy the fields to the model
                this.CopyFieldsToModel(source, target);
            }

            return target;
        }
	}
}
