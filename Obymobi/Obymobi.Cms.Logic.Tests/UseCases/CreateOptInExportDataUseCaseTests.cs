﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NUnit.Framework;
using Obymobi.Cms.Logic.Models;
using Obymobi.Cms.Logic.Tests.Builders;
using Obymobi.Cms.Logic.UseCases;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Cms.Logic.Tests.UseCases
{
    [TestFixture, Explicit]
    [Category("UseCases")]
    public class CreateOptInExportDataUseCaseTests
    {
        private TestDataBuilder<OptInEntity> OptIn => TestDataBuilder<OptInEntity>.Create();

        [Test]
        public void Execute_MergeOptInsByCustomerName_WhenMaxTwoOptInsExistWithSameCustomerName()
        {
            OptInCollection optInCollection = new OptInCollection
            {
                OptIn.With(o => o.CustomerName = "Busta Rhymes", o => o.PhoneNumber = "1").Build(),
                OptIn.With(o => o.CustomerName = "Busta Rhymes", o => o.Email = "busta@crave-emenu.com").Build(),
                OptIn.With(o => o.CustomerName = "", o => o.PhoneNumber = "2").Build(),
                OptIn.With(o => o.CustomerName = "", o => o.Email = "Dr. Dre").Build(),
                OptIn.With(o => o.CustomerName = "Marshall Mathers", o => o.PhoneNumber = "4").Build(),
                OptIn.With(o => o.CustomerName = "Marshall Mathers", o => o.Email = "marshall@crave-emenu.com").Build(),
                OptIn.With(o => o.CustomerName = "Marshall Mathers", o => o.Email = "mathers@crave-emenu.com").Build(),
            };
            
            CreateOptInExportDataUseCase createOptInExportDataUseCase = new CreateOptInExportDataUseCase();
            ICollection<OptInRecord> result = createOptInExportDataUseCase.Execute(optInCollection, new DateTimeFormatInfo());

            Assert.AreEqual(6, result.Count);
            Assert.AreEqual(1, result.Count(x => x.CustomerName.Equals("Busta Rhymes")));
            Assert.AreEqual(2, result.Count(x => x.CustomerName.Equals("")));
            Assert.AreEqual(3, result.Count(x => x.CustomerName.Equals("Marshall Mathers")));
        }
    }
}
