﻿using NUnit.Framework;
using Obymobi.Cms.Logic.UseCases;

namespace Obymobi.Cms.Logic.Tests.UseCases
{
    [TestFixture, Explicit]
    [Category("Debug")]
    public class UpdateOptInsWithMissingCustomerNameUseCaseTests
    {
        [Test]
        [Explicit("Only for debug purposes")]
        public void Execute()
        {
            UpdateOptInsWithMissingCustomerNameUseCase useCase = new UpdateOptInsWithMissingCustomerNameUseCase();
            
            int result = useCase.Execute();
        }
    }
}
