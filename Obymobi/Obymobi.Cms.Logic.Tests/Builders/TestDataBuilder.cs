﻿using System;
using System.Collections.Generic;

namespace Obymobi.Cms.Logic.Tests.Builders
{
    public class TestDataBuilder
    {
        public static TestDataBuilder<TEntity> Create<TEntity>()
            where TEntity : new()
        {
            return new TestDataBuilder<TEntity>();
        }
    }

    public class TestDataBuilder<TEntity> where TEntity : new()
    {
        private readonly List<Func<TEntity, object>> setters = new List<Func<TEntity, object>>();

        public static TestDataBuilder<TEntity> Create() => new TestDataBuilder<TEntity>();

        public TestDataBuilder<TEntity> With(params Func<TEntity, object>[] props)
        {
            this.setters.AddRange(props);
            return this;
        }

        public TEntity Build()
        {
            TEntity model = new TEntity();
            this.setters.ForEach(s => s.Invoke(model));
            return model;
        }
    }
}
