﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Cms.Logic.Tests.Stubs
{
    public partial class Tests
    {
        public class CustomTextContainingEntityStub : ICustomTextContainingEntity
        {
            public CustomTextContainingEntityStub(string entityName, CustomTextCollection customTextCollection)
            {
                this.CustomTextCollection = customTextCollection;
                this.LLBLGenProEntityName = entityName;
            }

            public string LLBLGenProEntityName { get; }

            public CustomTextCollection CustomTextCollection { get; }

            public Task<bool> SaveAsync(IPredicate updateRestriction, bool recurse, CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }

            public IEntityFields Fields { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public bool IsSerializing { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public List<IEntityField> PrimaryKeyFields => throw new NotImplementedException();

            public bool IsNew { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public bool IsDirty { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public IValidator Validator { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public Guid ObjectID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public bool IsDeserializing { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public IConcurrencyPredicateFactory ConcurrencyPredicateFactoryToUse { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public int LLBLGenProEntityTypeValue => throw new NotImplementedException();



            public ITypeDefaultValue TypeDefaultValueProviderToUse { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public IAuthorizer AuthorizerToUse { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public IAuditor AuditorToUse { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public Dictionary<string, string> CustomPropertiesOfType => throw new NotImplementedException();

            public Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType => throw new NotImplementedException();

            public List<IFieldInfo> PrimaryKeyFieldInfos => throw new NotImplementedException();

            public InheritanceHierarchyType LLBLGenProIsInHierarchyOfType => throw new NotImplementedException();

            public bool IsUpdateOnlyEntity => throw new NotImplementedException();

            public bool MarkedForDeletion { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public Context ActiveContext { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
            public ITransaction Transaction { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            public bool ParticipatesInTransaction => throw new NotImplementedException();

            IEntityFieldsCore IEntityCore.Fields { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

            IList IEntityCore.PrimaryKeyFields => throw new NotImplementedException();

            public event EventHandler EntityContentsChanged;
            public event PropertyChangedEventHandler PropertyChanged;
            public event EventHandler AfterSave;
            public event EventHandler Initializing;
            public event EventHandler Initialized;

            public void AcceptChanges()
            {
                throw new NotImplementedException();
            }

            public void BeginEdit()
            {
                throw new NotImplementedException();
            }

            public void CancelEdit()
            {
                throw new NotImplementedException();
            }

            public bool CheckIfEntityHasPendingFkSyncs<TEntity>(Dictionary<Guid, TEntity> inQueue) where TEntity : IEntityCore
            {
                throw new NotImplementedException();
            }

            public bool CheckIfIsSubTypeOf(int typeOfEntity)
            {
                throw new NotImplementedException();
            }

            public bool Delete()
            {
                throw new NotImplementedException();
            }

            public bool Delete(IPredicate deleteRestriction)
            {
                throw new NotImplementedException();
            }

            public void DiscardSavedFields()
            {
                throw new NotImplementedException();
            }

            public void EndEdit()
            {
                throw new NotImplementedException();
            }

            public void FetchExcludedFields(ExcludeIncludeFieldsList excludedIncludedFields)
            {
                throw new NotImplementedException();
            }

            public Task<bool> DeleteAsync()
            {
                throw new NotImplementedException();
            }

            public Task<bool> DeleteAsync(CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }

            public Task<bool> DeleteAsync(IPredicate deleteRestriction)
            {
                throw new NotImplementedException();
            }

            public Task<bool> DeleteAsync(IPredicate deleteRestriction, CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }

            public Task<bool> SaveAsync()
            {
                throw new NotImplementedException();
            }

            public Task<bool> SaveAsync(CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }

            public Task<bool> SaveAsync(bool recurse, CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }

            public Task<bool> SaveAsync(bool recurse)
            {
                throw new NotImplementedException();
            }

            public Task<bool> SaveAsync(IPredicate updateRestriction)
            {
                throw new NotImplementedException();
            }

            public Task<bool> SaveAsync(IPredicate updateRestriction, CancellationToken cancellationToken)
            {
                throw new NotImplementedException();
            }

            public Task<bool> SaveAsync(IPredicate updateRestriction, bool recurse)
            {
                throw new NotImplementedException();
            }

            public void FlagAsSaved()
            {
                throw new NotImplementedException();
            }

            public void FlagMeAsChanged()
            {
                throw new NotImplementedException();
            }

            public List<IEntityRelation> GetAllRelations()
            {
                throw new NotImplementedException();
            }

            public IPredicateExpression GetConcurrencyPredicate(ConcurrencyPredicateType predicateTypeToCreate)
            {
                throw new NotImplementedException();
            }

            public object GetCurrentFieldValue(int fieldIndex)
            {
                throw new NotImplementedException();
            }

            public List<IEntity> GetDependentRelatedEntities()
            {
                throw new NotImplementedException();
            }

            public List<IEntity> GetDependingRelatedEntities()
            {
                throw new NotImplementedException();
            }

            public string GetEntityDescription(bool switchFlag)
            {
                throw new NotImplementedException();
            }

            public string GetEntityDescription(bool switchFlag, IEntityCore entity)
            {
                throw new NotImplementedException();
            }

            public IEntityFactory GetEntityFactory()
            {
                throw new NotImplementedException();
            }

            public IEntityFieldCore GetFieldByName(string fieldName)
            {
                throw new NotImplementedException();
            }

            public IInheritanceInfo GetInheritanceInfo()
            {
                throw new NotImplementedException();
            }

            public IInheritanceInfoProvider GetInheritanceInfoProvider()
            {
                throw new NotImplementedException();
            }

            public List<IEntityCollection> GetMemberEntityCollections()
            {
                throw new NotImplementedException();
            }

            public Dictionary<string, object> GetRelatedData()
            {
                throw new NotImplementedException();
            }

            public RelationCollection GetRelationsForFieldOfType(string fieldName)
            {
                throw new NotImplementedException();
            }

            public void ReadXml(XmlNode node)
            {
                throw new NotImplementedException();
            }

            public void ReadXml(string xmlData)
            {
                throw new NotImplementedException();
            }

            public bool Refetch()
            {
                throw new NotImplementedException();
            }

            public void RejectChanges()
            {
                throw new NotImplementedException();
            }

            public void RollbackFields(string name)
            {
                throw new NotImplementedException();
            }

            public bool Save()
            {
                throw new NotImplementedException();
            }

            public bool Save(bool recurse)
            {
                throw new NotImplementedException();
            }

            public bool Save(IPredicate updateRestriction)
            {
                throw new NotImplementedException();
            }

            public bool Save(IPredicate updateRestriction, bool recurse)
            {
                throw new NotImplementedException();
            }

            public void SaveFields(string name)
            {
                throw new NotImplementedException();
            }

            public void SetEntityError(string errorMessage)
            {
                throw new NotImplementedException();
            }

            public void SetEntityFieldError(string fieldName, string errorMessage, bool append)
            {
                throw new NotImplementedException();
            }

            public bool SetNewFieldValue(string fieldName, object value)
            {
                throw new NotImplementedException();
            }

            public bool SetNewFieldValue(int fieldIndex, object value)
            {
                throw new NotImplementedException();
            }

            public void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
            {
                throw new NotImplementedException();
            }

            public void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
            {
                throw new NotImplementedException();
            }

            public bool TestCurrentFieldValueForNull(Enum fieldIndex)
            {
                throw new NotImplementedException();
            }

            public bool TestOriginalFieldValueForNull(Enum fieldIndex)
            {
                throw new NotImplementedException();
            }

            public void TransactionCommit()
            {
                throw new NotImplementedException();
            }

            public void TransactionRollback()
            {
                throw new NotImplementedException();
            }

            public void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName)
            {
                throw new NotImplementedException();
            }

            public void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
            {
                throw new NotImplementedException();
            }

            public void ValidateEntity()
            {
                throw new NotImplementedException();
            }

            public void WriteXml(out string entityXml)
            {
                throw new NotImplementedException();
            }

            public void WriteXml(XmlDocument parentDocument, out XmlNode entityNode)
            {
                throw new NotImplementedException();
            }

            public void WriteXml(string rootNodeName, out string entityXml)
            {
                throw new NotImplementedException();
            }

            public void WriteXml(string rootNodeName, XmlDocument parentDocument, out XmlNode entityNode)
            {
                throw new NotImplementedException();
            }

            public void WriteXml(XmlFormatAspect aspects, out string entityXml)
            {
                throw new NotImplementedException();
            }

            public void WriteXml(XmlFormatAspect aspects, XmlDocument parentDocument, out XmlNode entityNode)
            {
                throw new NotImplementedException();
            }

            public void WriteXml(XmlFormatAspect aspects, string rootNodeName, out string entityXml)
            {
                throw new NotImplementedException();
            }

            public void WriteXml(XmlFormatAspect aspects, string rootNodeName, XmlDocument parentDocument, out XmlNode entityNode)
            {
                throw new NotImplementedException();
            }

            IEnumerable<IEntityCore> IEntityCore.GetDependentRelatedEntities()
            {
                throw new NotImplementedException();
            }

            IEnumerable<IEntityCore> IEntityCore.GetDependingRelatedEntities()
            {
                throw new NotImplementedException();
            }

            IEntityFactoryCore IEntityCore.GetEntityFactory()
            {
                throw new NotImplementedException();
            }

            IEnumerable<IEntityCollectionCore> IEntityCore.GetMemberEntityCollections()
            {
                throw new NotImplementedException();
            }
        }

    }
}