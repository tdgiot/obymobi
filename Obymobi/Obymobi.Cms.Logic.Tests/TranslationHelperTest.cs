using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NUnit.Framework;
using Obymobi.Cms.Logic.HelperClasses;
using Obymobi.Cms.Logic.Models;
using Obymobi.Cms.Logic.Tests.Resources;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using static Obymobi.Cms.Logic.Tests.Stubs.Tests;

namespace Obymobi.Cms.Logic.Tests
{
    public partial class TranslationHelperTest
    {
        private const string KnownEntityNameWithTranslations = "ApplicationConfigurationEntity";

        private const string TranslatedCulture = "nl";
        private const string UnTranslatedCulture = "ja-JP";

        [Test]
        public void GetTranslationDefaults_WithoutExactMatchingDefault_ShouldReturnParentCultureDefaults([Values("en-US", "en-MX", "nl", "nl-NL", "en")] string cultureInfoCode)
        {
            CultureInfo culture = CultureInfo.GetCultureInfo(cultureInfoCode);

            TranslationDefaults result = TranslationHelper.GetTranslationDefaults(TestResource.ResourceManager, culture, false);
            Assert.NotNull(result);
        }

        [Test]
        public void GetTranslationDefaults_WithoutDefaults_ShouldReturnNoTranslations([Values("fr", "fr-FR")] string cultureInfoCode)
        {
            CultureInfo culture = CultureInfo.GetCultureInfo(cultureInfoCode);

            TranslationDefaults result = TranslationHelper.GetTranslationDefaults(TestResource.ResourceManager, culture, false);
            Assert.Null(result);
        }

        [Test]
        public void GetTranslationDefaults_WithoutDefaults_WithFallback_ShouldReturnTranslations([Values("fr", "fr-FR")] string cultureInfoCode)
        {
            CultureInfo culture = CultureInfo.GetCultureInfo(cultureInfoCode);

            TranslationDefaults result = TranslationHelper.GetTranslationDefaults(TestResource.ResourceManager, culture, true);
            Assert.NotNull(result);
        }

        [Test]
        public void GetMissingTranslations_ReturnsUntranslatedItems()
        {
            // Setup
            var randomCulture = CultureInfo.GetCultureInfo(TranslatedCulture);
            int numberToTranslate = 16;

            var customTexts = new CustomTextCollection();
            CustomTextContainingEntityStub applicationConfigurationEntityStub = new CustomTextContainingEntityStub(KnownEntityNameWithTranslations, customTexts);
            IEnumerable<CustomTextType> typesWithoutTranslationBefore = TranslationHelper.GetMissingTranslationTypes(applicationConfigurationEntityStub, randomCulture);

            var expectedNumberOfUntranslated = typesWithoutTranslationBefore.Count() - numberToTranslate;

            foreach (var untranslatedType in typesWithoutTranslationBefore.Take(numberToTranslate))
            {
                customTexts.Add(new CustomTextEntity() { Type = untranslatedType, CultureCode = randomCulture.ToString() });
            }

            // Act
            IEnumerable<CustomTextType> typesWithoutTranslationAfter = TranslationHelper.GetMissingTranslationTypes(applicationConfigurationEntityStub, randomCulture);

            // Verify
            Assert.AreEqual(expectedNumberOfUntranslated, typesWithoutTranslationAfter.Count());
        }

        [Test]
        public void AddDefaultTranslationsToEntity_WithMatchingCulture_AndMatchingType_AddsSingleTranslation()
        {
            // The TestResource.ResourceManager also contains 1 part of the setup. "ThisIsRequired" is one of the expected Application Configuration text types.
            var randomCulture = CultureInfo.GetCultureInfo(TranslatedCulture);
            CustomTextContainingEntityStub applicationConfigurationEntityStub = new CustomTextContainingEntityStub(KnownEntityNameWithTranslations, new CustomTextCollection());

            TranslationHelper.AddDefaultTranslationsToEntity(TestResource.ResourceManager, applicationConfigurationEntityStub, randomCulture);

            Assert.AreEqual(1, applicationConfigurationEntityStub.CustomTextCollection.Count);
        }

        [Test]
        public void AddDefaultTranslationsToEntity_WithoutMatchingCulture__AddsNoTranslation()
        {
            // The TestResource.ResourceManager also contains 1 part of the setup. "ThisIsRequired" is one of the expected Application Configuration text types.
            var randomCulture = CultureInfo.GetCultureInfo(UnTranslatedCulture);
            CustomTextContainingEntityStub applicationConfigurationEntityStub = new CustomTextContainingEntityStub(KnownEntityNameWithTranslations, new CustomTextCollection());

            TranslationHelper.AddDefaultTranslationsToEntity(TestResource.ResourceManager, applicationConfigurationEntityStub, randomCulture);

            Assert.IsEmpty(applicationConfigurationEntityStub.CustomTextCollection);
        }

        [Test]
        public void AddDefaultTranslationsToEntity_WithoutMatchingCulture__WithFallBack_AddsSingleTranslation()
        {
            // The TestResource.ResourceManager also contains 1 part of the setup. "ThisIsRequired" is one of the expected Application Configuration text types.
            var randomCulture = CultureInfo.GetCultureInfo(UnTranslatedCulture);
            CustomTextContainingEntityStub applicationConfigurationEntityStub = new CustomTextContainingEntityStub(KnownEntityNameWithTranslations, new CustomTextCollection());

            TranslationHelper.AddDefaultTranslationsToEntity(TestResource.ResourceManager, applicationConfigurationEntityStub, randomCulture, true);

            Assert.AreEqual(1, applicationConfigurationEntityStub.CustomTextCollection.Count);
        }

        [Test]
        public void AddDefaultTranslationsToEntity_WithoutMatchingEntityName__AddsNoTranslation()
        {
            // The TestResource.ResourceManager also contains 1 part of the setup. "ThisIsRequired" is one of the expected Application Configuration text types.
            var randomCulture = CultureInfo.GetCultureInfo(TranslatedCulture);
            CustomTextContainingEntityStub applicationConfigurationEntityStub = new CustomTextContainingEntityStub("SomeUnknownEntityName", new CustomTextCollection());

            TranslationHelper.AddDefaultTranslationsToEntity(TestResource.ResourceManager, applicationConfigurationEntityStub, randomCulture);

            Assert.IsEmpty(applicationConfigurationEntityStub.CustomTextCollection);
        }
    }
}