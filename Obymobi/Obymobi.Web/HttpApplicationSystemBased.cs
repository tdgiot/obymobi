using Dionysos;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Status;
using SD.Tools.OrmProfiler.Interceptor;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Web;

// WARNING: If you change something here, change it in the HttpApplicationDionysosBased too!
// WARNING: If you change something here, change it in the HttpApplicationDionysosBased too!
// WARNING: If you change something here, change it in the HttpApplicationDionysosBased too!
// WARNING: If you change something here, change it in the HttpApplicationDionysosBased too!

namespace Obymobi.Web
{
    public abstract class HttpApplicationSystemBased : HttpApplication
    {
        #region  Fields

        protected readonly string ApplicationName;

        protected static bool ApplicationInitialized;
        protected static object InitializationLock = new object();
        protected static DateTime LastDatabaseCheck = DateTime.UtcNow;

        #endregion

        protected HttpApplicationSystemBased() : this("Unknown")
        { }

        protected HttpApplicationSystemBased(string applicationName)
        {
            this.ApplicationName = applicationName;
        }

        protected virtual void Application_Start(object sender, EventArgs e)
        {
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            // LLBLGen ORM Profiler
            InterceptorCore.Initialize(ApplicationName);
            InterceptorCore.DisableMessageSending();

            // Unhandled exception handler
            Error += HttpApplicationSystemBased_Error;

            // base.Application_Start(sender, e); > Not needed.
            this.DatabaseIndependentPreInitialization();

            // An entity has to be initiliazed to get the ActualConnectionString filled
            if (CommonDaoBase.ActualConnectionString.IsNullOrWhiteSpace())
            {
                try
                {
                    VattariffEntity l = new VattariffEntity(1);
                    l.Refetch();
                }
                catch
                {
                    // Might go wrong with invalid DB information.
                    // ignored
                }
            }

            if (HttpContext.Current != null)
            {
                HttpContext.Current.Application["APPLICATION_STARTED"] = DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss");
            }

            // Start ConnectionStringPoller            
            //ConnectionStringPollerManager.StartPollers();

            CommonDaoBase.CommandTimeOut = 300;

            if (DatabaseHelper.IsDatabaseReady(CommonDaoBase.ActualConnectionString))
            {
                this.DatabaseDependentInitialization();
                HttpApplicationSystemBased.ApplicationInitialized = true;
            }
            else
            {
                ConnectionStringPoller.Instance.IsDatabaseAvailable = false;
                Debug.WriteLine("WARNING: HttpApplication is not yet fully initialized due to unavailable database");
            }
        }

        private void HttpApplicationSystemBased_Error(object sender, EventArgs e)
        {
            Application_Error(this.Server.GetLastError());
        }

        /// <summary>
        ///     Used to initialize datase independent things:
        ///     DataProviders, AssemblyInformation, ConfigurationProvider, ApplicationInfo, ConfigurationInfo, CometConnection
        /// </summary>
        public abstract void DatabaseIndependentPreInitialization();

        public abstract void DatabaseDependentInitialization();

        protected virtual void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!HttpApplicationSystemBased.ApplicationInitialized)
            {
                // Try to initialize every 10 seconds on a seperate thread

                if (HttpApplicationSystemBased.LastDatabaseCheck.TotalSecondsToNow() > 10)
                {
                    HttpApplicationSystemBased.LastDatabaseCheck = DateTime.UtcNow;
                    Thread t = new Thread(this.TryInitialize);
                    t.Start();
                }
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Application_Error(this.Server.GetLastError());
        }

        protected virtual void Application_Error(Exception ex)
        {
        }

        protected void EnableDatabaseProfiler() => InterceptorCore.EnableMessageSending();

        private void TryInitialize()
        {
            Debug.WriteLine("INFORMATION: Global.TryInitialize - Start");

            // An entity has to be initiliazed to get the ActualConnectionString filled
            if (CommonDaoBase.ActualConnectionString.IsNullOrWhiteSpace())
            {
                VattariffEntity l = new VattariffEntity();
                l.Refetch();
            }

            if (!HttpApplicationSystemBased.ApplicationInitialized && DatabaseHelper.IsDatabaseReady(CommonDaoBase.ActualConnectionString))
            {
                lock (HttpApplicationSystemBased.InitializationLock)
                {
                    if (HttpApplicationSystemBased.ApplicationInitialized)
                    {
                        return;
                    }

                    this.DatabaseDependentInitialization();
                    Debug.WriteLine("INFORMATION: HttpApplicationSystemBased.IntializeApplication Completed");
                    HttpApplicationSystemBased.ApplicationInitialized = true;
                }
            }
        }
    }
}