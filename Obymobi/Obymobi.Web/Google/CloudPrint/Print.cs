﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Web.Google.CloudPrint
{
    public class Print
    {
        public bool success { get; set; }
        public string message { get; set; }

        public PrintJob job { get; set; }
    }
}
