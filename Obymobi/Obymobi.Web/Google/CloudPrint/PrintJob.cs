﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Web.Google.CloudPrint
{
    public class PrintJob
    {
        public string id { get; set; }
        public string printerid { get; set; }
        public string title { get; set; }
        public string contentType { get; set; }
        public string fileUrl { get; set; }
        public string ticketUrl { get; set; }
        public string createTime { get; set; }
        public string updateTime { get; set; }
        public string status { get; set; }
        public string errorCode { get; set; }
        public string message { get; set; }
    }
}
