﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Web.Google
{
    public class Printer
    {
        public string name { get; set; }
        public string displayName { get; set; }
        public string id { get; set; }
        public string connectionStatus { get; set; }
    }
}
