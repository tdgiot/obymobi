using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using DotNetOpenAuth.OAuth2;
using Google.Apis.Manual.Analytics.v3;
using Google.Apis.Manual.Analytics.v3.Data;
using Google.Apis.Manual.Authentication.OAuth2;
using Google.Apis.Manual.Authentication.OAuth2.DotNetOpenAuth;
using Google.Apis.Manual.Json;
using Obymobi.Web.Google.CloudPrint;
using Obymobi.Web.Google.Authorization;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Enums;
using Dionysos.Web;

namespace Obymobi.Web.Google
{
    public enum HttpVerb
    {
        GET,
        POST,
        DELETE
    }

    public enum GoogleAPIResult
    { 
        Failure,
        DimensionSetupIncorrect,
        MetricsSetupIncorrect
    }

    public class GoogleAPI
    {
        #region Properties

        // Crave credentials. Encoded to protect against decompiling baddies
        private static string REFRESH_TOKEN = "MS9wVkRoWWgwUEd5bGhuZV9fcUVGUThnYkhNdEh3TXlUdnV3dGxnNWtYUThv";
        private static string CLIENT_ID = "NTA4Nzg1NTk1ODYzLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29t";
        private static string CLIENT_SECRET = "MGhWUkp6RnhBTks1WVRRVTBYNUdoWE9Y";        

        public string RefreshToken { get; private set; }
        public AnalyticsService AnalyticsService { get; private set; }
        public Profile Profile { get; private set; }
        private OAuth2Authenticator<NativeApplicationClient> Authenticator { get; set; }
        
        #endregion

        #region Constructors

        public GoogleAPI()
        {
            this.RefreshToken = Base64UrlDecode(REFRESH_TOKEN);

            var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description);
            provider.ClientIdentifier = Base64UrlDecode(CLIENT_ID);
            provider.ClientCredentialApplicator = ClientCredentialApplicator.PostParameter(Base64UrlDecode(CLIENT_SECRET));
            this.Authenticator = new OAuth2Authenticator<NativeApplicationClient>(provider, GetAuthentication);

            this.AnalyticsService = new AnalyticsService(this.Authenticator);
        }

        public GoogleAPI(string clientId, string clientSecret, string refreshToken)
        {
            this.RefreshToken = refreshToken;

            var provider = new NativeApplicationClient(GoogleAuthenticationServer.Description);
            provider.ClientIdentifier = clientId;
            provider.ClientCredentialApplicator = ClientCredentialApplicator.PostParameter(clientSecret);
            this.Authenticator = new OAuth2Authenticator<NativeApplicationClient>(provider, GetAuthentication);

            this.AnalyticsService = new AnalyticsService(this.Authenticator);
        }

        #endregion

        #region Methods

        private IAuthorizationState GetAuthentication(NativeApplicationClient client)
        {
            IAuthorizationState state = new AuthorizationState(new[] { "https://www.googleapis.com/auth/analytics.readonly", "https://www.googleapis.com/auth/cloudprint" }) { RefreshToken = this.RefreshToken };
            client.RefreshAuthorization(state);
            return state;
        }

        public string GetPageViews(DateTime from, DateTime to)
        {
            string ret = "0";
            if (this.Profile != null)
            {
                var data = this.Get("ga:pageviews", from, to);
                if (data.Rows != null)
                {
                    ret = data.Rows[0][0];
                }
            }

            return ret;
        }

        public string GetTransactions(DateTime from, DateTime to)
        {
            string ret = "0";
            if (this.Profile != null)
            {
                var data = this.Get("ga:transactions", from, to);
                if (data.Rows != null)
                {
                    ret = data.Rows[0][0];
                }
            }

            return ret;
        }

        public GaData Get(string metrics, DateTime from, DateTime to)
        {
            return this.Get(metrics, null, null, null, null, from, to);
        }

        public GaData Get(string metrics, string dimensions, DateTime from, DateTime to)
        {
            return this.Get(metrics, dimensions, null, null, null, from, to);
        }

        public GaData Get(string metrics, string dimensions, string filters, DateTime from, DateTime to)
        {
            return this.Get(metrics, dimensions, filters, null, null, from, to);
        }

        public GaData Get(string metrics, string dimensions, string filters, string sort, DateTime from, DateTime to)
        {
            return this.Get(metrics, dimensions, filters, sort, null, from, to);
        }

        public GaData Get(string metrics, string dimensions, string filters, string sort, int? maxResults, DateTime from, DateTime to)
        {
            try
            {
                string profile = string.Format((string)"ga:{0}", (object)this.Profile.Id);
                string fromStr = from.ToString("yyyy-MM-dd");
                string toStr = to.ToString("yyyy-MM-dd");
                var query = this.AnalyticsService.Data.Ga.Get(profile, fromStr, toStr, metrics);

                if (dimensions != null)
                    query.Dimensions = dimensions;

                if (filters != null)
                    query.Filters = filters;

                if (sort != null)
                    query.Sort = sort;

                if (maxResults.HasValue)
                    query.MaxResults = maxResults.Value;

                query.Filters = filters;
                return query.Fetch();
            }
            catch(Exception ex)
            {
                if (ex.Message.Contains("No such dimension"))
                    throw new ObymobiException(GoogleAPIResult.DimensionSetupIncorrect, ex, ex.Message);
                if (ex.Message.Contains("Unknown metric"))
                    throw new ObymobiException(GoogleAPIResult.DimensionSetupIncorrect, ex, ex.Message);
                else
                    throw new ObymobiException(GoogleAPIResult.Failure, ex, ex.Message);             
            }
        }

        public bool SetProfile(string propertyId)
        {
            this.Profile = null;

            var profiles = this.AnalyticsService.Management.Profiles.List("~all", "~all").Fetch();
            foreach (var profile in profiles.Items)
            {
                if (profile != null && profile.WebPropertyId.Equals(propertyId))
                {
                    this.Profile = profile;
                    break;
                }
            }

            return this.Profile != null;
        }

        public bool SetProfileId(string profileId)
        {
            this.Profile = null;

            var profiles = this.AnalyticsService.Management.Profiles.List("~all", "~all").Fetch();
            foreach (var profile in profiles.Items)
            {
                if (profile != null && profile.Id.Equals(profileId))
                {
                    this.Profile = profile;
                    break;
                }
            }

            return this.Profile != null;
        }

        public static string Base64UrlEncode(String input)
        {
            var output = Convert.ToBase64String(Encoding.UTF8.GetBytes(input));
            output = output.Split('=')[0];
            output = output.Replace('+', '-');
            output = output.Replace('/', '_');
            return output;
        }

        public static string Base64UrlDecode(string input)
        {
            var output = input;
            output = output.Replace('-', '+');
            output = output.Replace('_', '/');
            switch (output.Length % 4)
            {
                case 0: break;
                case 2: output += "=="; break;
                case 3: output += "="; break;
                default: throw new System.Exception("Illegal base64url string!");
            }
            var converted = Convert.FromBase64String(output);
            return Encoding.UTF8.GetString(converted);
        }

        public GetPrinters GetPrinters()
        {
            string responseContent = this.PostCloudPrint("http://www.google.com/cloudprint/search?output=json", null);
            return NewtonsoftJsonSerializer.Instance.Deserialize<GetPrinters>(responseContent);
        }

        public GetPrintJobs GetPrintJobs(string printerId = "")
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            if (printerId != null && printerId.Length > 0)
            {
                data["printerid"] = printerId;
            }

            string responseContent = this.PostCloudPrint("http://www.google.com/cloudprint/jobs", data);
            return NewtonsoftJsonSerializer.Instance.Deserialize<GetPrintJobs>(responseContent);
        }

        public bool Print(string printerId, string title, string content, string contentType)
        {
            bool success = false;

            Dictionary<string, string> data = new Dictionary<string, string>();
            data["printerid"] = printerId;
            data["title"] = title;
            data["capabilities"] = "";
            data["content"] = content;
            data["contentType"] = contentType;

            string responseContent = this.PostCloudPrint("https://www.google.com/cloudprint/submit", data);
            Print print = NewtonsoftJsonSerializer.Instance.Deserialize<Print>(responseContent);
            if (print != null)
            {
                success = print.success;
            }
            return success;
        }

        public bool DeletePrintJob(string jobId)
        {
            bool success = false;

            Dictionary<string, string> data = new Dictionary<string, string>();
            data["jobid"] = jobId;

            string responseContent = this.PostCloudPrint("http://www.google.com/cloudprint/deletejob", data);
            DeletePrintJob deletePrintJob = NewtonsoftJsonSerializer.Instance.Deserialize<DeletePrintJob>(responseContent);
            if (deletePrintJob != null)
            {
                success = deletePrintJob.success;
            }
            return success;
        }

        public string PostCloudPrint(String url, Dictionary<string, string> data)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            // Add authorization header
            this.Authenticator.LoadAccessToken();
            request.Headers.Add("Authorization", "OAuth " + this.Authenticator.State.AccessToken);

            // Add post data
            string postData = this.EncodePostData(data);

            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] postDataBytes = encoding.GetBytes(postData);
            request.ContentLength = postDataBytes.Length;
            if (postDataBytes.Length > 0)
            {
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(postDataBytes, 0, postDataBytes.Length);
                requestStream.Close();
            }

            // Get the response as a string
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string responseContent = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return responseContent;
        }

        public string EncodePostData(Dictionary<string, string> data)
        {
            StringBuilder sb = new StringBuilder();
            bool isFirst = true;
            if (data != null && data.Count > 0)
            {
                foreach (KeyValuePair<string, string> entry in data)
                {
                    if (!isFirst)
                    {
                        sb.Append("&");
                    }
                    isFirst = false;

                    sb.Append(HttpUtility.UrlEncode(entry.Key));
                    sb.Append("=");
                    sb.Append(HttpUtility.UrlEncode(entry.Value));
                }
            }
            return sb.ToString();
        }

        public static TokenRequest RequestToken(string redirectUri, string code)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            // Add post data
            string postData = string.Empty;
            postData += string.Format("{0}={1}", HttpUtility.UrlEncode("client_id"), HttpUtility.UrlEncode("1056171545705-mjo1d3a239v257fi8mdrdvebcirpdf3v.apps.googleusercontent.com"));
            postData += string.Format("&{0}={1}", HttpUtility.UrlEncode("redirect_uri"), HttpUtility.UrlEncode(redirectUri));
            postData += string.Format("&{0}={1}", HttpUtility.UrlEncode("client_secret"), HttpUtility.UrlEncode("6YfaWf9e2BLx6usdSCt2cScL"));
            postData += string.Format("&{0}={1}", HttpUtility.UrlEncode("code"), HttpUtility.UrlEncode(code));
            postData += string.Format("&{0}={1}", HttpUtility.UrlEncode("grant_type"), HttpUtility.UrlEncode("authorization_code"));

            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] postDataBytes = encoding.GetBytes(postData);
            request.ContentLength = postDataBytes.Length;
            if (postDataBytes.Length > 0)
            {
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(postDataBytes, 0, postDataBytes.Length);
                requestStream.Close();
            }

            // Get the response as a string
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string responseContent = new StreamReader(response.GetResponseStream()).ReadToEnd();

            // Return the result as model
            return NewtonsoftJsonSerializer.Instance.Deserialize<TokenRequest>(responseContent);
        }

        public static Printer[] GetPrinters(int companyId)
        {
            Printer[] printers = null;

            ApiAuthenticationEntity googleApiAuthenticationEntity = ApiAuthenticationHelper.GetApiAuthenticationEntityByType(ApiAuthenticationType.Google, companyId);
            if (googleApiAuthenticationEntity != null && !string.IsNullOrWhiteSpace(googleApiAuthenticationEntity.FieldValue1))
            {
                string clientId = Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.GoogleClientId);
                string clientSecret = Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.GoogleClientSecret);

                GoogleAPI google = new GoogleAPI(clientId, clientSecret, googleApiAuthenticationEntity.FieldValue1);

                GetPrinters getPrinters = google.GetPrinters();
                if (getPrinters != null && getPrinters.printers != null)
                {
                    printers = getPrinters.printers;
                }
            }

            return printers;
        }              

        #endregion
    }
}
