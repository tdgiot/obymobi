﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Web.CDN
{
    public class UploadCompanyMediaRequest
    {
        #region Properties

        public int CompanyId = -1;

        // Company data
        public bool UploadCompany;
        public bool UploadProducts;
        public bool UploadCategories;
        public bool UploadDeliverypointgroups;
        public bool UploadAdvertisements;
        public bool UploadEntertainment;
        public bool UploadAlterations;
        public bool UploadAlterationoptions;
        public bool UploadSurveys;
        public bool UploadSites;
        public bool UploadUIWidgets;

        #endregion

        #region Constructors

        public UploadCompanyMediaRequest()
        {

        }

        public UploadCompanyMediaRequest(int companyId)
        {
            this.CompanyId = companyId;            
        }

        #endregion        
    }
}
