﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Web.CDN
{
    public class UploadGenericMediaRequest
    {
        #region Properties

        public bool UploadGenericProducts;
        public bool UploadGenericCategories;
        public bool UploadGenericEntertainment;
        public bool UploadGenericPointsOfInterest;
        public bool UploadGenericSites;
        public bool UploadGenericAdvertisements;
        public bool UploadGenericSiteTemplates;

        #endregion
    }
}
