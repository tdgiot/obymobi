﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Web.CDN
{
    public class UploadHelper
    {
        public static void UploadCompanyDataMedia(UploadCompanyMediaRequest uploadCompanyMediaRequest, bool forceSave, bool unpublishedOnly, bool preventTimestampUpdates)
        {
            var media = UploadHelper.GetCompanyMedia(uploadCompanyMediaRequest, unpublishedOnly);
            MediaRatioTypeMediaHelperWeb.ResetMediaRatioTypeMediaFiles(media, forceSave, preventTimestampUpdates);
        }

        public static void ConsistencyCheckCompanyDataMedia(UploadCompanyMediaRequest uploadCompanyMediaRequest)
        {
            var media = UploadHelper.GetCompanyMedia(uploadCompanyMediaRequest, false);
            MediaRatioTypeMediaHelperWeb.ResetMediaRatioTypeMediaFiles(media, false, true, true);
        }

        public static void UploadGenericMedia(UploadGenericMediaRequest uploadGenericMediaRequest, bool forceSave, bool unpublishedOnly)
        {
            var media = UploadHelper.GetGenericMedia(uploadGenericMediaRequest, unpublishedOnly);
            MediaRatioTypeMediaHelperWeb.ResetMediaRatioTypeMediaFiles(media, forceSave);
        }

        public static void ConsistencyCheckGenericMedia(UploadGenericMediaRequest uploadGenericMediaRequest)
        {
            var media = UploadHelper.GetGenericMedia(uploadGenericMediaRequest, false);
            MediaRatioTypeMediaHelperWeb.ResetMediaRatioTypeMediaFiles(media, false, true, true);
        }

        private static MediaCollection GetMediaForCompany(int companyId, bool unpublishedOnly)
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
            var mediaRatioTypeMediaPrefetch = MediaEntity.PrefetchPathMediaRatioTypeMediaCollection;
            if (unpublishedOnly)
            {
                mediaRatioTypeMediaPrefetch.Filter = UploadHelper.GetUnpublishedFilter();
            }
            prefetch.Add(mediaRatioTypeMediaPrefetch).SubPath.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaRatioTypeMediaFileEntity);

            var medias = MediaHelper.GetMediaForCompany(companyId, null, 0, null, null, prefetch, false);
            return medias;
        }

        private static PredicateExpression GetUnpublishedFilter()
        {
            var toReturn = new PredicateExpression();
            
            toReturn.Add(MediaRatioTypeMediaFields.LastDistributedVersionTicks == DBNull.Value);
            toReturn.AddWithOr(MediaRatioTypeMediaFields.LastDistributedVersionTicksAmazon == DBNull.Value);

            // Out of sync filter
            PredicateExpression outOfSync = new PredicateExpression();
            outOfSync.AddWithOr(MediaRatioTypeMediaFields.LastDistributedVersionTicksAmazon != MediaRatioTypeMediaFields.LastDistributedVersionTicks);
            toReturn.AddWithOr(outOfSync);

            return toReturn;
        }

        public static MediaCollection GetCompanyMedia(UploadCompanyMediaRequest uploadComanyMediaRequest, bool unpublishedOnly)
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
            var mediaRatioTypeMediaPrefetch = MediaEntity.PrefetchPathMediaRatioTypeMediaCollection;
            if (unpublishedOnly)
                mediaRatioTypeMediaPrefetch.Filter = UploadHelper.GetUnpublishedFilter();
            prefetch.Add(mediaRatioTypeMediaPrefetch).SubPath.Add(MediaRatioTypeMediaEntity.PrefetchPathMediaRatioTypeMediaFileEntity);

            var filter = new PredicateExpression();
            if (uploadComanyMediaRequest.UploadCompany)
                filter.AddWithOr(MediaFields.CompanyId == uploadComanyMediaRequest.CompanyId);
            if (uploadComanyMediaRequest.UploadProducts)
                filter.AddWithOr(ProductFields.CompanyId == uploadComanyMediaRequest.CompanyId);
            if (uploadComanyMediaRequest.UploadCategories)
                filter.AddWithOr(CategoryFields.CompanyId == uploadComanyMediaRequest.CompanyId);
            if (uploadComanyMediaRequest.UploadDeliverypointgroups)
                filter.AddWithOr(DeliverypointgroupFields.CompanyId == uploadComanyMediaRequest.CompanyId);
            if (uploadComanyMediaRequest.UploadAdvertisements)
                filter.AddWithOr(AdvertisementFields.CompanyId == uploadComanyMediaRequest.CompanyId);
            if (uploadComanyMediaRequest.UploadEntertainment)
                filter.AddWithOr(EntertainmentFields.CompanyId == uploadComanyMediaRequest.CompanyId);
            if (uploadComanyMediaRequest.UploadAlterations)
                filter.AddWithOr(AlterationFields.CompanyId == uploadComanyMediaRequest.CompanyId);
            if (uploadComanyMediaRequest.UploadAlterationoptions)
                filter.AddWithOr(AlterationoptionFields.CompanyId == uploadComanyMediaRequest.CompanyId);
            if (uploadComanyMediaRequest.UploadSurveys)
            {
                filter.AddWithOr(SurveyFields.CompanyId == uploadComanyMediaRequest.CompanyId);
                filter.AddWithOr(new FieldCompareValuePredicate(SurveyFields.CompanyId, ComparisonOperator.Equal, uploadComanyMediaRequest.CompanyId, "SurveyOfPage"));
            }
            if (uploadComanyMediaRequest.UploadSites)
            {
                filter.AddWithOr(SiteFields.CompanyId == uploadComanyMediaRequest.CompanyId);
                filter.AddWithOr(new FieldCompareValuePredicate(SiteFields.CompanyId, ComparisonOperator.Equal, uploadComanyMediaRequest.CompanyId, "SiteOfPage"));
                filter.AddWithOr(new FieldCompareValuePredicate(SiteFields.CompanyId, ComparisonOperator.Equal, uploadComanyMediaRequest.CompanyId, "SiteOfPageOfPageElement"));
            }
            if (uploadComanyMediaRequest.UploadUIWidgets)
                filter.AddWithOr(UIModeFields.CompanyId == uploadComanyMediaRequest.CompanyId);

            var relations = new RelationCollection();
            if (uploadComanyMediaRequest.UploadProducts)
                relations.Add(MediaEntity.Relations.ProductEntityUsingProductId, JoinHint.Left);
            if (uploadComanyMediaRequest.UploadCategories)
                relations.Add(MediaEntity.Relations.CategoryEntityUsingCategoryId, JoinHint.Left);
            if (uploadComanyMediaRequest.UploadDeliverypointgroups)
                relations.Add(MediaEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, JoinHint.Left);
            if (uploadComanyMediaRequest.UploadAdvertisements)
                relations.Add(MediaEntity.Relations.AdvertisementEntityUsingAdvertisementId, JoinHint.Left);
            if (uploadComanyMediaRequest.UploadEntertainment)
                relations.Add(MediaEntity.Relations.EntertainmentEntityUsingEntertainmentId, JoinHint.Left);
            if (uploadComanyMediaRequest.UploadAlterations)
                relations.Add(MediaEntity.Relations.AlterationEntityUsingAlterationId, JoinHint.Left);
            if (uploadComanyMediaRequest.UploadAlterationoptions)
                relations.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, JoinHint.Left);
            if (uploadComanyMediaRequest.UploadSurveys)
            {
                relations.Add(MediaEntity.Relations.SurveyEntityUsingSurveyId, JoinHint.Left);

                relations.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, JoinHint.Left);
                relations.Add(SurveyPageEntity.Relations.SurveyEntityUsingSurveyId, "SurveyOfPage", JoinHint.Left);
            }
            if (uploadComanyMediaRequest.UploadSites)
            {
                relations.Add(MediaEntity.Relations.SiteEntityUsingSiteId, JoinHint.Left);

                relations.Add(MediaEntity.Relations.PageEntityUsingPageId, JoinHint.Left);
                relations.Add(PageEntity.Relations.SiteEntityUsingSiteId, "SiteOfPage", JoinHint.Left);

                relations.Add(MediaEntity.Relations.PageElementEntityUsingPageElementId, JoinHint.Left);
                relations.Add(PageElementEntity.Relations.PageEntityUsingPageId, "PageOfPageElement", JoinHint.Left);
                relations.Add(PageEntity.Relations.SiteEntityUsingSiteId, "PageOfPageElement", "SiteOfPageOfPageElement", JoinHint.Left);
            }
            if (uploadComanyMediaRequest.UploadUIWidgets)
            {
                relations.Add(MediaEntity.Relations.UIWidgetEntityUsingUIWidgetId);
                relations.Add(UIWidgetEntity.Relations.UITabEntityUsingUITabId);
                relations.Add(UITabEntity.Relations.UIModeEntityUsingUIModeId);
            }

            MediaCollection media = new MediaCollection();
            media.GetMulti(filter, 0, null, relations, prefetch);

            return media;
        }

        public static MediaCollection GetGenericMedia(UploadGenericMediaRequest uploadGenericMediaRequest, bool unpublishedOnly)
        {            
            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
            var mediaRatioTypeMediaPrefetch = MediaEntity.PrefetchPathMediaRatioTypeMediaCollection;
            if (unpublishedOnly)
                mediaRatioTypeMediaPrefetch.Filter = UploadHelper.GetUnpublishedFilter();
            prefetch.Add(mediaRatioTypeMediaPrefetch);
            
            var filter = new PredicateExpression();
            var relations = new RelationCollection();

            // Filter
            if (uploadGenericMediaRequest.UploadGenericProducts)
                filter.AddWithOr(MediaFields.GenericproductId != DBNull.Value);
            if (uploadGenericMediaRequest.UploadGenericCategories)
                filter.AddWithOr(MediaFields.GenericcategoryId != DBNull.Value);
            if (uploadGenericMediaRequest.UploadGenericEntertainment)
            {
                var entertainmentSubFilter = new PredicateExpression();
                entertainmentSubFilter.Add(MediaFields.EntertainmentId != DBNull.Value);
                entertainmentSubFilter.Add(EntertainmentFields.CompanyId == DBNull.Value);
                filter.AddWithOr(entertainmentSubFilter);
            }
            if (uploadGenericMediaRequest.UploadGenericPointsOfInterest)
                filter.AddWithOr(MediaFields.PointOfInterestId != DBNull.Value);
            if (uploadGenericMediaRequest.UploadGenericSites)
            {
                var siteSubFilter = new PredicateExpression();
                siteSubFilter.Add(MediaFields.SiteId != DBNull.Value);
                siteSubFilter.Add(SiteFields.CompanyId == DBNull.Value);
                filter.AddWithOr(siteSubFilter);

                var pageSubFilter = new PredicateExpression();
                pageSubFilter.Add(MediaFields.PageId != DBNull.Value);
                pageSubFilter.Add(new FieldCompareValuePredicate(SiteFields.CompanyId, ComparisonOperator.Equal, DBNull.Value, "SiteOfPageGeneric"));
                filter.AddWithOr(pageSubFilter);

                var pageElementSubFilter = new PredicateExpression();
                pageElementSubFilter.Add(MediaFields.PageElementId != DBNull.Value);
                pageElementSubFilter.Add(new FieldCompareValuePredicate(SiteFields.CompanyId, ComparisonOperator.Equal, DBNull.Value, "SiteOfPageOfPageElementGeneric"));
                filter.AddWithOr(pageElementSubFilter);
            }
            if (uploadGenericMediaRequest.UploadGenericAdvertisements)
            {
                var advertisementSubFilter = new PredicateExpression();
                advertisementSubFilter.Add(MediaFields.AdvertisementId != DBNull.Value);
                advertisementSubFilter.Add(AdvertisementFields.CompanyId == DBNull.Value);
                filter.AddWithOr(advertisementSubFilter);
            }
            if (uploadGenericMediaRequest.UploadGenericSiteTemplates)
            {
                var pageTemplateSubFilter = new PredicateExpression();
                pageTemplateSubFilter.Add(MediaFields.PageTemplateId != DBNull.Value);
                filter.AddWithOr(pageTemplateSubFilter);

                var pageTemplateElementsSubFilter = new PredicateExpression();
                pageTemplateElementsSubFilter.Add(MediaFields.PageTemplateElementId != DBNull.Value);
                filter.AddWithOr(pageTemplateElementsSubFilter);
            }

            // Relations
            if (uploadGenericMediaRequest.UploadGenericEntertainment)
                relations.Add(MediaEntity.Relations.EntertainmentEntityUsingEntertainmentId, JoinHint.Left);

            if (uploadGenericMediaRequest.UploadGenericAdvertisements)
                relations.Add(MediaEntity.Relations.AdvertisementEntityUsingAdvertisementId, JoinHint.Left);

            if (uploadGenericMediaRequest.UploadGenericSites)
            {
                relations.Add(MediaEntity.Relations.SiteEntityUsingSiteId, JoinHint.Left);

                relations.Add(MediaEntity.Relations.PageEntityUsingPageId, JoinHint.Left);
                relations.Add(PageEntity.Relations.SiteEntityUsingSiteId, "SiteOfPageGeneric", JoinHint.Left);

                relations.Add(MediaEntity.Relations.PageElementEntityUsingPageElementId, JoinHint.Left);
                relations.Add(PageElementEntity.Relations.PageEntityUsingPageId, "PageOfPageElementGeneric", JoinHint.Left);
                relations.Add(PageEntity.Relations.SiteEntityUsingSiteId, "PageOfPageElementGeneric", "SiteOfPageOfPageElementGeneric", JoinHint.Left);
            }

            MediaCollection media = new MediaCollection();
            media.GetMulti(filter, 0, null, relations, prefetch);

            return media;
        }
    }
}
