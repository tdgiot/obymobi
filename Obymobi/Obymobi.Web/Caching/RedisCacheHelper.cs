﻿using Dionysos;

namespace Obymobi.Web.Caching
{
    public class RedisCacheHelper
    {
        private static RedisCacheHelper instance;

        public RedisCacheStore CacheStore { get; private set; }

        private RedisCacheHelper()
        {            
            instance = this;

            // Check if Redis values have been configured
            string redisHost = System.Configuration.ConfigurationManager.AppSettings["RedisHost"];
            string redisPassword = System.Configuration.ConfigurationManager.AppSettings["RedisPassword"];

            if (redisHost.IsNullOrWhiteSpace())
                return;

            int redisPort = 6379;
            if (redisHost.Contains(":"))
            {
                string[] splitValue = redisHost.Split(':');
                redisHost = splitValue[0];

                if (!int.TryParse(splitValue[1], out redisPort))
                    redisPort = 6379;
            }

            int redisDatabaseId = -1;
            string redisDatabaseIdStr = System.Configuration.ConfigurationManager.AppSettings["RedisDatabaseId"];
            if (!redisDatabaseIdStr.IsNullOrWhiteSpace())
            {
                if (!int.TryParse(redisDatabaseIdStr, out redisDatabaseId))
                {
                    redisDatabaseId = -1;
                }
            }

            CacheStore = new RedisCacheStore(redisHost, redisPort, redisPassword, redisDatabaseId);
        }

        public static RedisCacheHelper Instance
        {
            get { return RedisCacheHelper.instance ?? (RedisCacheHelper.instance = new RedisCacheHelper()); }
        }

        public bool RedisEnabled
        {
            get 
            { 
                return false; // CacheStore != null && CacheStore.HasCache;
            }
        }

        public void RemoveValue(string key)
        {
            if (CacheStore != null)
            {
                CacheStore.RemoveValue(key);
            }
        }

        public bool Add(string key, object value, int expireInXMinutes = -1)
        {
            if (CacheStore != null)
            {
                if (expireInXMinutes <= 0)
                {
                    return CacheStore.Add(key, value);
                }
                
                return CacheStore.AddAbsoluteExpire(key, value, expireInXMinutes);
            }

            return false;
        }
    }
}