﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Dionysos;
using Dionysos.Web.Caching;
using SD.LLBLGen.Pro.ORMSupportClasses;
using ServiceStack.Redis;

namespace Obymobi.Web.Caching
{
    public class RedisCacheStore : ICachingStore
    {
        private readonly BasicRedisClientManager clientsManager;

        public RedisCacheStore(string host, int port, string password, int defaultDb = 0)
        {
            string redisHost = string.Format("{0}:{1}", host, port);
            if (!password.IsNullOrWhiteSpace())
            {
                redisHost = string.Format("{0}@{1}", password, redisHost);
            }

            clientsManager = new BasicRedisClientManager(defaultDb, redisHost);
        }

        public CachingStoreType Type { get { return CachingStoreType.Redis; } }

        /// <summary>
        /// Gets whether HttpContext.Current.Cache is not null.
        /// </summary>
        public bool HasCache { get { return true; } }

        private IRedisClient Client { get { return this.clientsManager.GetClient(); } }

        public bool TryGetValue<T>(string key, out T value)
        {
            value = default(T);

            try
            {
                if (IsInstanceOfGenericType(typeof(T), typeof(EntityBase)))
                {
                    byte[] bytes = this.Client.As<byte[]>().GetValue(key);
                    if (bytes != null && bytes.Length > 0)
                    {
                        value = Deserialize<T>(bytes);
                    }
                }
                else
                {
                    value = this.Client.As<T>().GetValue(key);    
                }
                
                return (value != null);
            }
            catch (Exception)
            {
                // ignored
            }

            return false;
        }

        public T GetValue<T>(string key)
        {
            T value;
            if (!TryGetValue(key, out value))
            {
                value = default(T);
            }

            return value;
        }

        public bool HasValue(string key)
        {
            if (!HasCache)
                return false;

            string value = this.Client.GetValue(key);

            return !value.IsNullOrWhiteSpace();
        }

        public bool Add<T>(string key, T value)
        {
            if (HasCache)
            {
                if (value is EntityBase)
                {
                    this.Client.As<byte[]>().SetValue(key, Serialize(value));
                }
                else
                {
                    this.Client.As<T>().SetValue(key, value);
                }

                return true;
            }

            return false;
        }

        public bool AddAbsoluteExpire<T>(string key, T value, int expireInXMinutes)
        {
            if (HasCache)
            {
                if (value is EntityBase)
                {
                    this.Client.As<byte[]>().SetValue(key, Serialize(value), TimeSpan.FromMinutes(expireInXMinutes));   
                }
                else
                {
                    this.Client.As<T>().SetValue(key, value, TimeSpan.FromMinutes(expireInXMinutes));    
                }

                return true;
            }

            return false;
        }

        public void AddSlidingExpire<T>(string key, T value, int expireInXMinutes)
        {
            AddAbsoluteExpire(key, value, expireInXMinutes);
        }

        public bool RemoveValue(string key)
        {
            if (HasCache)
            {
                return this.Client.Remove(key);
            }

            return false;
        }

        public void RemoveWildcard(string keyWildcard)
        {
            if (HasCache)
            {
                this.Client.ExecLua(@"for _,k in ipairs(redis.call('keys', '" + keyWildcard + "')) do redis.call('del', k) end");
            }
        }

        public List<string> GetAllCacheKeys()
        {
            // Not supported
            return new List<string>();
        }

        public void Clear()
        {
            if (HasCache)
            {
                this.Client.FlushDb();
            }
        }

        private static byte[] Serialize(object o)
        {
            if (o == null)
            {
                return null;
            }

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, o);
                byte[] objectDataAsStream = memoryStream.ToArray();
                return objectDataAsStream;
            }
        }

        private static T Deserialize<T>(byte[] stream)
        {
            if (stream == null)
            {
                return default(T);
            }

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (MemoryStream memoryStream = new MemoryStream(stream))
            {
                T result = (T)binaryFormatter.Deserialize(memoryStream);
                return result;
            }
        }

        private static bool IsInstanceOfGenericType(Type genericType, Type baseType)
        {
            Type type = genericType;
            while (type != null)
            {
                if (type.Name.Equals(baseType.Name))
                {
                    return true;
                }
                type = type.BaseType;
            }
            return false;
        }
    }
}