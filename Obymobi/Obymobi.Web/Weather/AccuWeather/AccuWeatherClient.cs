﻿using Newtonsoft.Json;
using Obymobi.Web.Weather.AccuWeather.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Dionysos;
using Dionysos.Web;

namespace Obymobi.Web.Weather.AccuWeather
{
    internal class AccuWeatherClient
    {
        public enum AccuWeatherExceptions
        {
            [StringValue("FailedHttpRequest")]
            FailedHttpRequest,
            [StringValue("EmptyResponse")]
            EmptyResponse,
            [StringValue("FailedToDeserialize")]
            FailedToDeserialize,
            [StringValue("ApiError")]
            ApiError,
        }

        private const string SEARCH_GEOPOSITION = "http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey={0}&q={1}";
        private const string SEARCH_CURRENT = "http://dataservice.accuweather.com/currentconditions/v1/{0}?apikey={1}";
        private const string SEARCH_FORECASTS = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/{0}?apikey={1}";

        private HttpClient httpClient;
        private string apiKey;
        
        public AccuWeatherClient(string apiKey)
        {
            this.httpClient = new HttpClient();
            this.apiKey = apiKey;
        }

        public string SearchGeoposition(double latitude, double longitude)
        {
            string location = string.Format("{0:F},{1:F}", latitude, longitude);
            Uri uri = new Uri(string.Format(SEARCH_GEOPOSITION, this.apiKey, location));

            string responseString = CallApi(uri);

            GeopositionSearchResult response;
            try
            {
                response = JsonConvert.DeserializeObject<GeopositionSearchResult>(responseString, new JsonSerializerSettings
                {
                    Error = (sender, args) => args.ErrorContext.Handled = true
                });
            }
            catch (Exception ex)
            {
                throw new ObymobiException(AccuWeatherExceptions.FailedToDeserialize, ex);
            }

            return response.Key;
        }

        public CurrentWeatherModel GetCurrentWeather(string locationKey)
        {
            Uri uri = new Uri(string.Format(SEARCH_CURRENT, locationKey, this.apiKey));
            string responseString = CallApi(uri);

            CurrentWeatherModel[] response;
            try
            {
                response = JsonConvert.DeserializeObject<CurrentWeatherModel[]>(responseString, new JsonSerializerSettings
                {
                    Error = (sender, args) => args.ErrorContext.Handled = true
                });
            }
            catch (Exception ex)
            {
                throw new ObymobiException(AccuWeatherExceptions.FailedToDeserialize, ex);
            }

            if (response == null || response.Length == 0)
            {
                return null;
            }

            return response[0];
        }

        public ForecastModel GetForecast(string locationKey)
        {
            Uri uri = new Uri(string.Format(SEARCH_FORECASTS, locationKey, this.apiKey));
            string responseString = CallApi(uri);

            ForecastModel response;
            try
            {
                response = JsonConvert.DeserializeObject<ForecastModel>(responseString, new JsonSerializerSettings
                {
                    Error = (sender, args) => args.ErrorContext.Handled = true
                });
            }
            catch (Exception ex)
            {
                throw new ObymobiException(AccuWeatherExceptions.FailedToDeserialize, ex);
            }

            return response;
        }

        private string CallApi(Uri uri)
        {
            HttpResponseMessage response;
            try
            {
                response = httpClient.GetAsync(uri).Result;
            }
            catch (Exception ex)
            {
                throw new ObymobiException(AccuWeatherExceptions.FailedHttpRequest, ex);
            }

            if (!response.IsSuccessStatusCode)
            {
                throw new ObymobiException(AccuWeatherExceptions.FailedHttpRequest);
            }

            var responseString = response.Content.ReadAsStringAsync().Result;
            if (string.IsNullOrEmpty(responseString))
            {
                throw new ObymobiException(AccuWeatherExceptions.EmptyResponse);
            }

            return responseString;
        }
    }
}
