﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Weather.AccuWeather.Models
{
    public class GeopositionSearchResult
    {
        public int Version { get; set; }
        public string Key { get; set; }
        public string EnglishName { get; set; }
    }
}
