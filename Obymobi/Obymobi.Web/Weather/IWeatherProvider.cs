﻿using Obymobi.Logic.Model.Weather;

namespace Obymobi.Web.Weather
{
    public interface IWeatherProvider
    {
        string Name { get; set; }

        WeatherItem GetCurrentWeatherForLocation(double latitude, double longitude);
        ForecastData GetForecastForLocation(double latitude, double longitude, int forecastDays);

        WeatherItem GetCurrentWeatherForCity(string city);
        ForecastData GetForecastForCity(string city, int forcastDays);
    }
}
