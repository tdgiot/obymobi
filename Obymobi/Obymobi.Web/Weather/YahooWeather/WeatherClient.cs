﻿using System;
using Dionysos.Web;
using System.Net.Http;
using Newtonsoft.Json;
using Obymobi.Web.Weather.YahooWeather.Model;
using Dionysos;

namespace Obymobi.Web.Weather.YahooWeather
{
    internal class WeatherClient
    {
        public enum YahooWeatherExceptions
        {
            [StringValue("FailedHttpRequest")]
            FailedHttpRequest,
            [StringValue("EmptyResponse")]
            EmptyResponse,
            [StringValue("FailedToDeserialize")]
            FailedToDeserialize,
            [StringValue("ApiError")]
            ApiError,
        }

        private Uri BaseUri = new Uri("https://query.yahooapis.com/v1/public/yql?format=json");
        private HttpClient HttpClient;

        public WeatherClient()
        {
            this.HttpClient = new HttpClient();
        }

        internal YahooWeatherData GetByName(String location)
        {
            var selectString = string.Format("select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"{0}\")", location);
            var uri = this.BaseUri.AddQueryString("q", selectString);

            return Send(uri);
        }

        YahooWeatherData Send(Uri uri)
        {
            HttpResponseMessage response;
            try
            {
                response = this.HttpClient.GetAsync(uri).Result;
            }
            catch (Exception ex)
            {
                throw new ObymobiException(YahooWeatherExceptions.FailedHttpRequest, ex);
            }

            if (!response.IsSuccessStatusCode)
            {
                throw new ObymobiException(YahooWeatherExceptions.FailedHttpRequest);
            }

            var responseString = response.Content.ReadAsStringAsync().Result;
            if (string.IsNullOrEmpty(responseString))
            {
                throw new ObymobiException(YahooWeatherExceptions.EmptyResponse);
            }

            YahooWeatherData yahooWeatherData;
            try
            {
                yahooWeatherData = JsonConvert.DeserializeObject<YahooWeatherData>(responseString);
            }
            catch (Exception ex)
            {
                throw new ObymobiException(YahooWeatherExceptions.FailedToDeserialize, ex);
            }

            if (yahooWeatherData == null || yahooWeatherData.Query == null)
            {
                throw new ObymobiException(YahooWeatherExceptions.FailedToDeserialize);
            }

            if (yahooWeatherData.Query.Count == 0)
            {
                throw new ObymobiException(YahooWeatherExceptions.ApiError, "No results");
            }

            return yahooWeatherData;
        }
    }
}
