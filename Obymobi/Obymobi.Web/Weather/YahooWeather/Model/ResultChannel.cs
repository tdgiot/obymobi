﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Obymobi.Web.Weather.YahooWeather.Model
{
    [Serializable]
    public class ResultChannel
    {
        [JsonProperty]
        public string Title { get; set; }

        [JsonProperty]
        public ResultItem Item { get; set; }
    }

    [Serializable]
    public class ResultItem
    {
        public ResultItem()
        {
            Forecast = new List<Forecast>();
        }

        [JsonProperty]
        public string Title { get; set; }

        [JsonProperty]
        public Condition Condition { get; set; }

        [JsonProperty]
        public List<Forecast> Forecast { get; set; }
    }

    [Serializable]
    public class Condition
    {
        [JsonProperty]
        public string Code { get; set; }

        [JsonProperty]
        public string Date { get; set; }

        [JsonProperty]
        public string Temp { get; set; }
    }

    [Serializable]
    public class Forecast
    {
        [JsonProperty]
        public string Code { get; set; }

        [JsonProperty]
        public string Date { get; set; }

        [JsonProperty]
        public string Day { get; set; }

        [JsonProperty]
        public string High { get; set; }

        [JsonProperty]
        public string Low { get; set; }
    }
}
