﻿using System.Collections.Generic;
using Newtonsoft.Json;
using System;

namespace Obymobi.Web.Weather.YahooWeather.Model
{
    [Serializable]
    public class YahooWeatherData
    {
        [JsonProperty]
        public Query Query { get; set; }

        [JsonIgnore]
        public ResultItem ResultItem {
            get
            {
                if (Query.Results != null && Query.Results.Channel != null && Query.Results.Channel.Item != null)
                    return Query.Results.Channel.Item;

                return null;
            } 
        }

        [JsonIgnore]
        public List<Forecast> Forecasts
        {
            get
            {
                if (ResultItem != null)
                    return ResultItem.Forecast;

                return null;
            }
        }

        [JsonIgnore]
        public Forecast ForecastToday
        {
            get
            {
                if (Forecasts != null && Forecasts.Count > 0)
                    return Forecasts[0];

                return null;
            }
        }
    }

    [Serializable]
    public class Query
    {
        [JsonProperty]
        public DateTime Created { get; set; }

        public int Count { get; set; }

        [JsonProperty]
        public Result Results { get; set; }
    }

    [Serializable]
    public class Result
    {
        [JsonProperty]
        public ResultChannel Channel { get; set; }
    }
}
