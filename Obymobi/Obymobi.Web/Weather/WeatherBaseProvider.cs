﻿using Obymobi.Logic.Model.Weather;

namespace Obymobi.Web.Weather
{
    public abstract class WeatherBaseProvider : IWeatherProvider
    {
        public string Name { get; set; }

        protected WeatherBaseProvider(string providerName)
        {
            Name = providerName;
        }

        public abstract WeatherItem GetCurrentWeatherForLocation(double latitude, double longitude);
        public abstract ForecastData GetForecastForLocation(double latitude, double longitude, int forecastDays);
        public abstract WeatherItem GetCurrentWeatherForCity(string city);
        public abstract ForecastData GetForecastForCity(string city, int forcastDays);

        public double FahrenheitToKelvin(double fahrenheit)
        {
            return ((fahrenheit - 32) * ((double)5/9)) + 273.15;
        }

        public double CelsiusToKelvin(double celsius)
        {
            return celsius + 273.15;
        }
    }
}
