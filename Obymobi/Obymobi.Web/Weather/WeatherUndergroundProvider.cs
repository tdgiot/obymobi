﻿using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model.Weather;
using Obymobi.Web.Weather.WUnderground;
using Obymobi.Web.Weather.WUnderground.Models;

namespace Obymobi.Web.Weather
{
    public class WeatherUndergroundProvider : WeatherBaseProvider
    {
        private readonly Dictionary<string, WeatherResponse> LastWeatherData = new Dictionary<string, WeatherResponse>();
        private readonly Dictionary<string, DateTime> LastWeatherUpdate = new Dictionary<string, DateTime>();

        private readonly WUndergroundClient client;

        public WeatherUndergroundProvider(string apiKey) : base("WeatherUnderGround")
        {
            client = new WUndergroundClient(apiKey);
        }

        public override WeatherItem GetCurrentWeatherForLocation(double latitude, double longitude)
        {
            return GetCurrentWeatherForCity(string.Format("{0},{1}", latitude, longitude));
        }
        
        public override WeatherItem GetCurrentWeatherForCity(string city)
        {
            WeatherResponse item = DownloadWeather(city, true);
            if (item == null || item.current_observation == null)
            {
                return null;
            }

            var weather = new WeatherItem();
            weather.Date = DateTime.Parse(item.current_observation.local_time_rfc822);
            weather.Temperature = FahrenheitToKelvin(item.current_observation.temp_f);
            weather.TemperatureNight = weather.Temperature;
            weather.TemperatureMin = FahrenheitToKelvin(item.current_observation.dewpoint_f);

            double heatIndex;
            if (!double.TryParse(item.current_observation.heat_index_f, out heatIndex))
            {
                heatIndex = item.current_observation.temp_f;
            }
            weather.TemperatureMax = FahrenheitToKelvin(heatIndex);
            weather.WeatherType = ConvertWeatherType(item.current_observation.icon);

            return weather;
        }

        public override ForecastData GetForecastForLocation(double latitude, double longitude, int forecastDays)
        {
            return GetForecastForCity(string.Format("{0},{1}", latitude, longitude), forecastDays);
        }

        public override ForecastData GetForecastForCity(string city, int forcastDays)
        {
            WeatherResponse item = DownloadWeather(city);
            if (item == null)
            {
                return null;
            }

            var forecast = new ForecastData();
            foreach (var forecastData in item.forecast.simpleforecast.forecastday)
            {
                var weather = new WeatherItem();
                weather.Date = new DateTime(forecastData.date.year, forecastData.date.month, forecastData.date.day);
                weather.Temperature = FahrenheitToKelvin(forecastData.high.fahrenheit);
                weather.TemperatureNight = FahrenheitToKelvin(forecastData.low.fahrenheit);
                weather.TemperatureMin = FahrenheitToKelvin(forecastData.low.fahrenheit);
                weather.TemperatureMax = FahrenheitToKelvin(forecastData.high.fahrenheit);
                weather.WeatherType = ConvertWeatherType(forecastData.icon);

                forecast.Forecast.Add(weather);
            }

            return forecast;
        }

        private WeatherResponse DownloadWeather(string city, bool force = false)
        {
            WeatherResponse weatherData = null;

            if (!force)
            {
                DateTime lastUpdate;
                if (!LastWeatherUpdate.TryGetValue(city, out lastUpdate))
                {
                    lastUpdate = DateTime.MinValue;
                }

                if (DateTime.UtcNow.Subtract(lastUpdate).Seconds < 60)
                {
                    LastWeatherData.TryGetValue(city, out weatherData);
                }
            }

            if (weatherData == null)
            {
                weatherData = this.client.GetConditionsAndForecastForLocationAsync(city);

                if (weatherData != null)
                {
                    LastWeatherData[city] = weatherData;
                    LastWeatherUpdate[city] = DateTime.UtcNow;
                }
            }

            return weatherData;
        }

        private WeatherType ConvertWeatherType(string weatherCode)
        {
            if (weatherCode.IsNullOrWhiteSpace())
            {
                return WeatherType._Unknown;
            }

            weatherCode = weatherCode.Replace("nt_", string.Empty);

            WeatherType type;
            if (!weatherTypes.TryGetValue(weatherCode, out type))
            {
                type = WeatherType._Unknown;
            }

            return type;
        }

        private static readonly Dictionary<string, WeatherType> weatherTypes = new Dictionary<string, WeatherType>
                                                                               {
                                                                                   {"chanceflurries", WeatherType.Snow},
                                                                                   {"chancerain", WeatherType.Rain},
                                                                                   {"chancesleet", WeatherType.RainAndSleet},
                                                                                   {"chancesnow", WeatherType.Snow},
                                                                                   {"chancestorms", WeatherType.Storm},
                                                                                   {"clear", WeatherType.Clear},
                                                                                   {"cloudy", WeatherType.Cloudy},
                                                                                   {"flurries", WeatherType.Snow},
                                                                                   {"fog", WeatherType.Fog},
                                                                                   {"haze", WeatherType.Haze},
                                                                                   {"mostlycloudy", WeatherType.Cloudy},
                                                                                   {"partlycloudy", WeatherType.CloudyPartly},
                                                                                   {"mostlysunny", WeatherType.Clear},
                                                                                   {"partlysunny", WeatherType.Cloudy},
                                                                                   {"sleet", WeatherType.RainAndSleet},
                                                                                   {"rain", WeatherType.Rain},
                                                                                   {"snow", WeatherType.Snow},
                                                                                   {"sunny", WeatherType.Clear},
                                                                                   {"tstorms", WeatherType.Thunderstorm},
                                                                               };
    }
}