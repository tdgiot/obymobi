﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Obymobi.Web.Weather.OpenWeatherMap
{
    internal sealed class Request
    {
        /// <summary>
        /// Gets or sets the URI.
        /// </summary>
        /// <value>
        /// The URI.
        /// </value>
        public Uri Uri { get; set; }

        /// <summary>
        /// Gets or sets the application identifier.
        /// </summary>
        /// <value>
        /// The application identifier.
        /// </value>
        public string AppId { get; set; }

        /// <summary>
        /// Gets or sets the querystring parameters.
        /// </summary>
        /// <value>
        /// The parameters.
        /// </value>
        public IDictionary<string, string> Parameters { get; set; }

        /// <summary>
        /// Gets or sets the HTTP client.
        /// </summary>
        /// <value>
        /// The HTTP client.
        /// </value>
        public HttpClient HttpClient { get; set; }

        /// <summary>
        /// Gets or sets the request.
        /// </summary>
        /// <value>
        /// The request.
        /// </value>
        public HttpRequestMessage HttpRequest { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Request"/> class.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="httpClient">The HTTP client.</param>
        /// <param name="appId">The application identifier.</param>
        public Request(Uri uri, HttpClient httpClient, string appId)
        {
            this.Uri = uri;
            this.HttpClient = httpClient;
            this.Parameters = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(appId))
            {
                this.AppId = appId;
                this.Parameters.Add("APPID", appId);
            }
        }
    }
}
