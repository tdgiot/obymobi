﻿using System;
using Newtonsoft.Json;

namespace Obymobi.Web.Weather.OpenWeatherMap.Model
{
    [Serializable]
    public class Weather
    {
        [JsonProperty]
        private int id;
        [JsonProperty]
        private String icon;

        public int Id { get { return this.id; } }
    }
}
