﻿using System;
using System.Collections.Generic;
using Dionysos;
using Newtonsoft.Json;
using Obymobi.Enums;

namespace Obymobi.Web.Weather.OpenWeatherMap.Model
{
    [Serializable]
    public class Forecast
    {
        [JsonProperty]
        private long dt;

        [JsonProperty]
        private Temperature temp;
        
        [JsonProperty]
        private List<Weather> weather;

        [JsonIgnore]
        public DateTime Date { get { return dt.FromUnixTime(); } }

        [JsonIgnore]
        public Temperature Temperature { get { return this.temp; } }

        public int Weather
        {
            get { return weather.Count > 0 ? weather[0].Id : (int)WeatherType._Unknown; }
        }
    }
}
