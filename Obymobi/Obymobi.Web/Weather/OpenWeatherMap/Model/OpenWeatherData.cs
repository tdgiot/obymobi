﻿using System;
using System.Collections.Generic;
using Dionysos;
using Newtonsoft.Json;
using Obymobi.Enums;

namespace Obymobi.Web.Weather.OpenWeatherMap.Model
{
    [Serializable]
    public class OpenWeatherData
    {
        [JsonProperty]
        private String cod;
        [JsonProperty]
        private String message;

        [JsonProperty]
        private long dt;

        // Current temperature
        [JsonProperty]
        private CurrentTemperature main;
        [JsonProperty]
        private List<Weather> weather;

        // Forecast
        [JsonProperty]
        private List<Forecast> list = new List<Forecast>();

        [JsonIgnore]
        public string Result { get { return this.cod; } }

        [JsonIgnore]
        public string Message { get { return this.message; } }

        [JsonIgnore]
        public CurrentTemperature Current { get { return this.main; } }

        [JsonIgnore]
        public DateTime Date { get { return dt.FromUnixTime(); } }

        [JsonIgnore]
        public int CurrentWeather
        {
            get { return weather.Count > 0 ? weather[0].Id : (int)WeatherType._Unknown; }
        }

        [JsonIgnore]
        public List<Forecast> Forecasts { get { return this.list; } }
    }
}
