﻿using System;
using Newtonsoft.Json;

namespace Obymobi.Web.Weather.OpenWeatherMap.Model
{
    [Serializable]
    public class Temperature
    {
        [JsonProperty]
        private double day;
        [JsonProperty]
        private double night;
        [JsonProperty]
        private double min;
        [JsonProperty]
        private double max;

        [JsonIgnore]
        public double Day { get { return this.day; } }

        [JsonIgnore]
        public double Night { get { return this.night; } }

        [JsonIgnore]
        public double Min { get { return this.min; } }

        [JsonIgnore]
        public double Max { get { return this.max; } }
    }
}
