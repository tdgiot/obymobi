﻿using System;
using Newtonsoft.Json;

namespace Obymobi.Web.Weather.OpenWeatherMap.Model
{
    [Serializable]
    public class CurrentTemperature
    {
        [JsonProperty]
        private double temp;
        [JsonProperty]
        private double temp_min;
        [JsonProperty]
        private double temp_max;

        [JsonIgnore]
        public double Temperature { get { return this.temp; } }

        [JsonIgnore]
        public double Min { get { return this.temp_min; } }

        [JsonIgnore]
        public double Max { get { return this.temp_max; } }
    }
}
