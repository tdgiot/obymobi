﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using Dionysos;
using Dionysos.Web;
using Newtonsoft.Json;
using Obymobi.Web.Weather.OpenWeatherMap.Model;

namespace Obymobi.Web.Weather.OpenWeatherMap
{
    internal class ApiClientBase
    {
        public enum OpenWeatherMapExceptions
        {
            [StringValue("FailedHttpRequest")]
            FailedHttpRequest,
            [StringValue("EmptyResponse")]
            EmptyResponse,
            [StringValue("FailedToDeserialize")]
            FailedToDeserialize,
            [StringValue("ApiError")]
            ApiError,
        }

        public Request Request { get; set; }

        internal ApiClientBase(Request request, string segment)
        {
            request.Uri = request.Uri.AddSegment(segment);
            this.Request = request;
        }

        /// <summary>
        /// Get weather by city name.
        /// </summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="cityName">Name of the city.</param>
        /// <param name="count">The count.</param>
        /// <returns>
        /// The by name.
        /// </returns>
        internal OpenWeatherData GetByName(string cityName, int? count)
        {
            this.Request.Parameters.Add("q", cityName);

            if (count.HasValue)
            {
                this.Request.Parameters.Add("cnt", count.Value.ToString(CultureInfo.InvariantCulture));
            }

            return this.RunGetRequest();
        }

        /// <summary>
        /// Get weather by coordinates.
        /// </summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="lat">Latitude</param>
        /// <param name="lon">Longitude</param>
        /// <param name="count">The count.</param>
        /// <returns>
        /// The by name.
        /// </returns>
        internal OpenWeatherData GetByCoordinates(double lat, double lon, int? count)
        {
            this.Request.Parameters.Add("lat", lat.ToString("F"));
            this.Request.Parameters.Add("lon", lon.ToString("F"));

            if (count.HasValue)
            {
                this.Request.Parameters.Add("cnt", count.Value.ToString(CultureInfo.InvariantCulture));
            }

            return this.RunGetRequest();
        }

        /// <summary>
        /// Executes the get request operation.
        /// </summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <returns>
        /// A Task&lt;T&gt;
        /// </returns>
        OpenWeatherData RunGetRequest()
        {
            this.Request.Parameters.Add("mode", "json");
            var uri = this.Request.Uri.AddQueryString(ToUrlParameters(this.Request.Parameters));
            this.Request.HttpRequest = new HttpRequestMessage(HttpMethod.Get, uri);

            return this.Send();
        }

        /// <summary>
        /// Send this message.
        /// </summary>
        /// <returns>
        /// A Task&lt;T&gt;
        /// </returns>
        OpenWeatherData Send()
        {
            HttpResponseMessage response;
            try
            {
                response = this.Request.HttpClient.GetAsync(this.Request.HttpRequest.RequestUri).Result;
            }
            catch (Exception ex)
            {
                throw new ObymobiException(OpenWeatherMapExceptions.FailedHttpRequest, ex);
            }

            if (!response.IsSuccessStatusCode)
            {
                throw new ObymobiException(OpenWeatherMapExceptions.FailedHttpRequest);
            }

            var responseString = response.Content.ReadAsStringAsync().Result;
            if (string.IsNullOrEmpty(responseString))
            {
                throw new ObymobiException(OpenWeatherMapExceptions.EmptyResponse);
            }
            
            OpenWeatherData openWeatherData;
            try
            {
                openWeatherData = JsonConvert.DeserializeObject<OpenWeatherData>(responseString);
            }
            catch (Exception ex)
            {
                throw new ObymobiException(OpenWeatherMapExceptions.FailedToDeserialize, ex);
            }

            if (openWeatherData == null)
            {
                throw new ObymobiException(OpenWeatherMapExceptions.FailedToDeserialize);
            }

            if (!openWeatherData.Result.Equals("200"))
            {
                throw new ObymobiException(OpenWeatherMapExceptions.ApiError, "Code: {0}, Message: {1}", openWeatherData.Result, openWeatherData.Message);
            }

            return openWeatherData;
        }

        #region Helpers

        public static string ToUrlParameters(IEnumerable<KeyValuePair<string, string>> parameters)
        {
            var array = parameters.Select(x => string.Format("{0}={1}", Uri.EscapeUriString(x.Key), Uri.EscapeUriString(x.Value))).ToArray();
            return string.Join("&", array);
        }

        #endregion
    }
}
