﻿using System;
using System.Collections.Generic;
using Google.Apis.Manual.Util;
using Obymobi.Enums;
using Obymobi.Logic.Model.Weather;
using Obymobi.Web.Weather.AccuWeather;
using Obymobi.Web.Weather.AccuWeather.Models;

namespace Obymobi.Web.Weather
{
    public class AccuWeatherProvider : WeatherBaseProvider
    {
        private readonly AccuWeatherClient client;
        private readonly Dictionary<string, string> locationKeyDictionary = new Dictionary<string, string>();
        private readonly Dictionary<string, WeatherContainer> locationWeather = new Dictionary<string, WeatherContainer>();

        public AccuWeatherProvider(string apiKey)
            : base("AccuWeather")
        {
            client = new AccuWeatherClient(apiKey);
        }

        public override WeatherItem GetCurrentWeatherForLocation(double latitude, double longitude)
        {
            string locationKey = GetLocationKey(latitude, longitude);
            if (locationKey.IsNullOrEmpty())
            {
                return null;
            }

            WeatherContainer container = GenerateWeather(locationKey);
            if (container != null)
            {
                return container.CurrentWeather;
            }

            return null;
        }

        public override ForecastData GetForecastForLocation(double latitude, double longitude, int forecastDays)
        {
            string locationKey = GetLocationKey(latitude, longitude);
            if (locationKey.IsNullOrEmpty())
            {
                return null;
            }

            WeatherContainer container = GenerateWeather(locationKey);
            if (container != null)
            {
                return container.ForecastData;
            }

            return null;
        }

        public override WeatherItem GetCurrentWeatherForCity(string city)
        {
            return null;
        }

        public override ForecastData GetForecastForCity(string city, int forcastDays)
        {
            return null;
        }

        private string GetLocationKey(double latitude, double longitude)
        {
            string key = string.Format("{0},{1}", latitude, longitude);

            string locationKey;
            if (!locationKeyDictionary.TryGetValue(key, out locationKey))
            {
                try
                {
                    locationKey = client.SearchGeoposition(latitude, longitude);
                    if (locationKey.IsNotNullOrEmpty())
                    {
                        locationKeyDictionary.Add(key, locationKey);
                    }
                }
                catch (Exception)
                {
                    return "";
                }
            }

            return locationKey;
        }

        private WeatherContainer GenerateWeather(string locationKey)
        {
            WeatherContainer container;
            if (this.locationWeather.TryGetValue(locationKey, out container))
            {
                if (DateTime.UtcNow.Subtract(container.LastUpdated) <= TimeSpan.FromMinutes(1))
                {
                    return container;
                }
            }
            else
            {
                container = new WeatherContainer
                {
                    LastUpdated = DateTime.MinValue
                };
                this.locationWeather.Add(locationKey, container);
            }

            CurrentWeatherModel current = client.GetCurrentWeather(locationKey);
            ForecastModel forecast = client.GetForecast(locationKey);
            if (forecast.DailyForecasts.Count == 0)
            {
                return null;
            }

            DailyForecast forecastToday = forecast.DailyForecasts[0];

            var weather = new WeatherItem();
            weather.Date = DateTime.UtcNow;
            if (current.Temperature.Imperial != null)
            {
                weather.Temperature = FahrenheitToKelvin(current.Temperature.Imperial.Value);
            }
            else if (current.Temperature.Metric != null)
            {
                weather.Temperature = CelsiusToKelvin(current.Temperature.Metric.Value);
            }
            weather.TemperatureNight = weather.Temperature;
            weather.TemperatureMin = FahrenheitToKelvin(forecastToday.Temperature.Minimum.Value);
            weather.TemperatureMax = FahrenheitToKelvin(forecastToday.Temperature.Maximum.Value);
            weather.WeatherType = ConvertWeatherType(current.WeatherIcon);

            var forecastData = new ForecastData();
            foreach (var dailyForecast in forecast.DailyForecasts)
            {
                var w = new WeatherItem();
                w.Date = dailyForecast.Date.ToUniversalTime();
                w.TemperatureMin = FahrenheitToKelvin(dailyForecast.Temperature.Minimum.Value);
                w.TemperatureMax = FahrenheitToKelvin(dailyForecast.Temperature.Maximum.Value);
                w.Temperature = w.TemperatureMax;
                w.TemperatureNight = w.TemperatureMin;
                w.WeatherType = ConvertWeatherType(dailyForecast.Day.Icon);

                forecastData.Forecast.Add(w);
            }

            container = new WeatherContainer
            {
                LastUpdated = DateTime.UtcNow,
                CurrentWeather = weather,
                ForecastData = forecastData
            };

            this.locationWeather[locationKey] = container;

            return container;
        }

        /// <summary>
        /// https://developer.accuweather.com/weather-icons
        /// </summary>
        /// <param name="weatherId"></param>
        /// <returns></returns>
        private WeatherType ConvertWeatherType(int weatherId)
        {
            var type = WeatherType._Unknown;

            switch (weatherId)
            {
                case 1:
                case 2:
                case 3:
                case 33:
                case 34:
                    type = WeatherType.Clear;
                    break;
                case 4:
                case 35:
                case 36:
                    type = WeatherType.CloudyPartly;
                    break;
                case 5:
                case 37:
                    type = WeatherType.Haze;
                    break;
                case 6:
                case 7:
                case 8:
                case 38:
                    type = WeatherType.Cloudy;
                    break;
                case 11:
                    type = WeatherType.Fog;
                    break;
                case 12:
                case 13:
                case 14:
                case 39:
                case 40:
                    type = WeatherType.Shower;
                    break;
                case 15:
                case 16:
                case 17:
                case 41:
                case 42:
                    type = WeatherType.Thunderstorm;
                    break;
                case 18:
                    type = WeatherType.Rain;
                    break;
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 43:
                case 44:
                    type = WeatherType.Snow;
                    break;
                case 25:
                case 26:
                case 29:
                    type = WeatherType.RainAndSleet;
                    break;
                case 30: // hot
                case 31: // cold
                    type = WeatherType.Clear;
                    break;
                case 32:
                    type = WeatherType.Wind;
                    break;
            }


            return type;
        }

        private class WeatherContainer
        {
            public DateTime LastUpdated { get; set; }
            public WeatherItem CurrentWeather { get; set; }
            public ForecastData ForecastData { get; set; }
        }
    }
}