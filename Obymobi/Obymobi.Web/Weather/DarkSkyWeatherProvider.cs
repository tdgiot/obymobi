﻿using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model.Weather;
using Obymobi.Web.Weather.DarkSky;

namespace Obymobi.Web.Weather
{
    public class DarkSkyWeatherProvider : WeatherBaseProvider
    {
        private readonly Dictionary<string, WeatherContainer> locationWeather = new Dictionary<string, WeatherContainer>();
        private readonly DarkSkyClient client;

        public DarkSkyWeatherProvider(string secretKey) : base("DarkSky")
        {
            this.client = new DarkSkyClient(secretKey);
        }

        public override WeatherItem GetCurrentWeatherForLocation(double latitude, double longitude)
        {
            WeatherContainer container = GetWeather(latitude, longitude);
            if (container != null)
            {
                return container.CurrentWeather;
            }

            return null;
        }

        public override ForecastData GetForecastForLocation(double latitude, double longitude, int forecastDays)
        {
            WeatherContainer container = GetWeather(latitude, longitude);
            if (container != null)
            {
                return container.ForecastData;
            }

            return null;
        }

        public override WeatherItem GetCurrentWeatherForCity(string city)
        {
            return null;
        }

        public override ForecastData GetForecastForCity(string city, int forcastDays)
        {
            return null;
        }

        private WeatherContainer GetWeather(double latitude, double longitude)
        {
            string locationKey = string.Format("{0},{1}", latitude, longitude);

            WeatherContainer container;
            if (this.locationWeather.TryGetValue(locationKey, out container))
            {
                if (DateTime.UtcNow.Subtract(container.LastUpdated) <= TimeSpan.FromMinutes(1))
                {
                    return container;
                }
            }
            else
            {
                container = new WeatherContainer
                {
                    LastUpdated = DateTime.MinValue
                };
                this.locationWeather.Add(locationKey, container);
            }

            ForecastModel forecastModel = client.GetWeather(latitude, longitude);

            Datum forecastToday = forecastModel.Daily.Data[0];

            var weather = new WeatherItem();
            weather.Date = DateTime.UtcNow;
            weather.Temperature = FahrenheitToKelvin(forecastModel.Currently.Temperature);
            weather.TemperatureNight = weather.Temperature;
            weather.TemperatureMin = FahrenheitToKelvin(forecastToday.TemperatureMin);
            weather.TemperatureMax = FahrenheitToKelvin(forecastToday.TemperatureMax);
            weather.WeatherType = ConvertWeatherType(forecastModel.Currently.Icon);

            var forecastData = new ForecastData();
            foreach (var dailyForecast in forecastModel.Daily.Data)
            {
                var w = new WeatherItem();
                w.Date = dailyForecast.Time.FromUnixTime();
                w.TemperatureMin = FahrenheitToKelvin(dailyForecast.TemperatureMin);
                w.TemperatureMax = FahrenheitToKelvin(dailyForecast.TemperatureMax);
                w.Temperature = w.TemperatureMax;
                w.TemperatureNight = w.TemperatureMin;
                w.WeatherType = ConvertWeatherType(dailyForecast.Icon);

                forecastData.Forecast.Add(w);
            }

            container = new WeatherContainer
            {
                LastUpdated = DateTime.UtcNow,
                CurrentWeather = weather,
                ForecastData = forecastData
            };

            this.locationWeather[locationKey] = container;

            return container;
        }

        private WeatherType ConvertWeatherType(string icon)
        {
            //clear-day, clear-night, rain, snow, sleet, wind, fog, cloudy, partly-cloudy-day, or partly-cloudy-night

            if (icon.Contains("clear", StringComparison.OrdinalIgnoreCase))
            {
                return WeatherType.Clear;
            }
            if (icon.Contains("partly-cloudy", StringComparison.OrdinalIgnoreCase))
            {
                return WeatherType.CloudyPartly;
            }
            if (icon.Contains("cloudy", StringComparison.OrdinalIgnoreCase))
            {
                return WeatherType.Cloudy;
            }
            if (icon.Contains("rain", StringComparison.OrdinalIgnoreCase))
            {
                return WeatherType.Rain;
            }
            if (icon.Contains("snow", StringComparison.OrdinalIgnoreCase))
            {
                return WeatherType.Snow;
            }
            if (icon.Contains("sleet", StringComparison.OrdinalIgnoreCase))
            {
                return WeatherType.RainAndSleet;
            }
            if (icon.Contains("fog", StringComparison.OrdinalIgnoreCase))
            {
                return WeatherType.Fog;
            }
            if (icon.Contains("hail", StringComparison.OrdinalIgnoreCase))
            {
                return WeatherType.Hail;
            }
            if (icon.Contains("tornado", StringComparison.OrdinalIgnoreCase))
            {
                return WeatherType.Tornado;
            }
            if (icon.Contains("thunderstorm", StringComparison.OrdinalIgnoreCase))
            {
                return WeatherType.Thunderstorm;
            }
            return WeatherType._Unknown;
        }

        private class WeatherContainer
        {
            public DateTime LastUpdated { get; set; }
            public WeatherItem CurrentWeather { get; set; }
            public ForecastData ForecastData { get; set; }
        }
    }
}