﻿using System;
using System.Collections.Generic;
using Obymobi.Enums;
using Obymobi.Logic.Model.Weather;
using Obymobi.Web.Weather.YahooWeather;
using Obymobi.Web.Weather.YahooWeather.Model;

namespace Obymobi.Web.Weather
{
    public class YahooWeatherProvider : WeatherBaseProvider
    {
        public Dictionary<string, YahooWeatherData> LastWeatherData = new Dictionary<string, YahooWeatherData>();
        public Dictionary<string, DateTime> LastWeatherUpdate = new Dictionary<string, DateTime>();

        public YahooWeatherProvider() : base("YahooWeatherProvider")
        {
        }

        public override WeatherItem GetCurrentWeatherForCity(string city)
        {
            var yahooWeatherData = RequestDataForCity(city);

            // Convert Yahoo to Local model
            var weatherItem = new WeatherItem();
            weatherItem.Date = DateTime.UtcNow.Date;

            int currentTemp;
            if (int.TryParse(yahooWeatherData.ResultItem.Condition.Temp, out currentTemp))
            {
                weatherItem.Temperature = FahrenheitToKelvin(currentTemp);
                weatherItem.TemperatureNight = FahrenheitToKelvin(currentTemp);
            }

            if (yahooWeatherData.ForecastToday != null)
            {
                int temp;
                if (int.TryParse(yahooWeatherData.ForecastToday.Low, out temp))
                    weatherItem.TemperatureMin = FahrenheitToKelvin(currentTemp < temp ? currentTemp : temp);
                if (int.TryParse(yahooWeatherData.ForecastToday.High, out temp))
                    weatherItem.TemperatureMax = FahrenheitToKelvin(temp);
            }

            weatherItem.WeatherType = ConvertWeatherType(yahooWeatherData.ResultItem.Condition.Code);

            return weatherItem;
        }

        public override ForecastData GetForecastForCity(string city, int forcastDays)
        {
            var yahooWeatherData = RequestDataForCity(city);

            var forecasts = yahooWeatherData.Forecasts;
            if (forecasts == null)
                return null;

            var forecastData = new ForecastData();
            foreach (var forecast in forecasts)
            {
                var weatherItem = new WeatherItem();
                weatherItem.Date = DateTime.Parse(forecast.Date);
                weatherItem.TemperatureNight = 0;

                int temp;
                if (int.TryParse(forecast.Low, out temp))
                    weatherItem.TemperatureMin = FahrenheitToKelvin(temp);
                if (int.TryParse(forecast.High, out temp))
                    weatherItem.TemperatureMax = FahrenheitToKelvin(temp);

                weatherItem.WeatherType = ConvertWeatherType(yahooWeatherData.ResultItem.Condition.Code);

                forecastData.Forecast.Add(weatherItem);
            }

            return forecastData;
        }

        private YahooWeatherData RequestDataForCity(string city)
        {
            YahooWeatherData yahooWeatherData = null;

            if (LastWeatherUpdate.ContainsKey(city) && DateTime.UtcNow.Subtract(LastWeatherUpdate[city]).Seconds < 10)
            {
                if (LastWeatherData.ContainsKey(city))
                {
                    yahooWeatherData = LastWeatherData[city];
                }
            }

            if (yahooWeatherData == null)
            {
                yahooWeatherData = new WeatherClient().GetByName(city);
                if (yahooWeatherData != null)
                {
                    LastWeatherData[city] = yahooWeatherData;
                    LastWeatherUpdate[city] = DateTime.UtcNow;
                }
            }

            return yahooWeatherData;
        }

        private WeatherType ConvertWeatherType(string weatherCode)
        {
            int conditionCode;
            if (!int.TryParse(weatherCode, out conditionCode))
                return WeatherType._Unknown;

            if (conditionCode == 0)
                return WeatherType.Tornado;
            if (conditionCode == 1)
                return WeatherType.TropicalStorm;
            if (conditionCode == 2)
                return WeatherType.Hurricane;
            if (conditionCode == 3 || conditionCode == 4 || (conditionCode >= 37 && conditionCode <= 40) || conditionCode == 45 || conditionCode == 47)
                return WeatherType.Thunderstorm;
            if (conditionCode == 5)
                return WeatherType.Snow;
            if (conditionCode == 6 || conditionCode == 7 || conditionCode == 18)
                return WeatherType.RainAndSleet;
            if (conditionCode == 8 || conditionCode == 9 || conditionCode == 40)
                return WeatherType.Drizzle;
            if (conditionCode <= 12)
                return WeatherType.Shower;
            if (conditionCode <= 16 || (conditionCode >= 41 && conditionCode <= 43) || conditionCode == 46)
                return WeatherType.Snow;
            if (conditionCode == 17 || conditionCode == 35)
                return WeatherType.Hail;
            if (conditionCode == 19)
                return WeatherType.Dust;
            if (conditionCode == 20)
                return WeatherType.Fog;
            if (conditionCode == 21)
                return WeatherType.Haze;
            if (conditionCode == 22)
                return WeatherType.Smoke;
            if (conditionCode == 23)
                return WeatherType.Blustery;
            if (conditionCode == 24)
                return WeatherType.Wind;
            if (conditionCode == 25 || conditionCode == 36) // Cold, Hot
                return WeatherType.Clear;
            if (conditionCode <= 28)
                return WeatherType.Cloudy;
            if (conditionCode <= 30 || conditionCode == 44)
                return WeatherType.CloudyPartly;
            if (conditionCode <= 32)
                return WeatherType.Clear;
            if (conditionCode <= 34)
                return WeatherType.CloudyPartly;

            return WeatherType._Unknown;
        }

        public override WeatherItem GetCurrentWeatherForLocation(double latitude, double longitude)
        {
            return null;
        }

        public override ForecastData GetForecastForLocation(double latitude, double longitude, int forecastDays)
        {
            return null;
        }
    }
}
