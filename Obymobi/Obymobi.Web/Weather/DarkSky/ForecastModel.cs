﻿using System.Collections.Generic;

namespace Obymobi.Web.Weather.DarkSky
{
    public partial class ForecastModel
    {
        public long Latitude { get; set; }
        public double Longitude { get; set; }
        public string Timezone { get; set; }
        public Currently Currently { get; set; }
        public Daily Daily { get; set; }
        public long Offset { get; set; }
    }

    public partial class Currently
    {
        public long Time { get; set; }
        public string Summary { get; set; }
        public string Icon { get; set; }
        public long PrecipIntensity { get; set; }
        public long PrecipProbability { get; set; }
        public double Temperature { get; set; }
        public double ApparentTemperature { get; set; }
        public double DewPoint { get; set; }
        public double Humidity { get; set; }
        public double Pressure { get; set; }
        public double WindSpeed { get; set; }
        public double WindGust { get; set; }
        public long WindBearing { get; set; }
        public double CloudCover { get; set; }
        public long UvIndex { get; set; }
        public double Visibility { get; set; }
        public double Ozone { get; set; }
    }

    public partial class Daily
    {
        public string Summary { get; set; }
        public string Icon { get; set; }
        public List<Datum> Data { get; set; }
    }

    public partial class Datum
    {
        public long Time { get; set; }
        public string Summary { get; set; }
        public string Icon { get; set; }
        public long SunriseTime { get; set; }
        public long SunsetTime { get; set; }
        public double MoonPhase { get; set; }
        public double PrecipIntensity { get; set; }
        public double PrecipIntensityMax { get; set; }
        public long PrecipIntensityMaxTime { get; set; }
        public double PrecipProbability { get; set; }
        public string PrecipType { get; set; }
        public double TemperatureHigh { get; set; }
        public long TemperatureHighTime { get; set; }
        public double TemperatureLow { get; set; }
        public long TemperatureLowTime { get; set; }
        public double ApparentTemperatureHigh { get; set; }
        public long ApparentTemperatureHighTime { get; set; }
        public double ApparentTemperatureLow { get; set; }
        public long ApparentTemperatureLowTime { get; set; }
        public double DewPoint { get; set; }
        public double Humidity { get; set; }
        public double Pressure { get; set; }
        public double WindSpeed { get; set; }
        public double WindGust { get; set; }
        public long WindGustTime { get; set; }
        public long WindBearing { get; set; }
        public double CloudCover { get; set; }
        public long UvIndex { get; set; }
        public long UvIndexTime { get; set; }
        public double Visibility { get; set; }
        public double Ozone { get; set; }
        public double TemperatureMin { get; set; }
        public long TemperatureMinTime { get; set; }
        public double TemperatureMax { get; set; }
        public long TemperatureMaxTime { get; set; }
        public double ApparentTemperatureMin { get; set; }
        public long ApparentTemperatureMinTime { get; set; }
        public double ApparentTemperatureMax { get; set; }
        public long ApparentTemperatureMaxTime { get; set; }
    }

}