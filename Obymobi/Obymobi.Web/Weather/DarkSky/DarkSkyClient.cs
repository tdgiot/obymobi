﻿using System;
using System.Net;
using System.Net.Http;
using Dionysos;
using Newtonsoft.Json;

namespace Obymobi.Web.Weather.DarkSky
{
    public class DarkSkyClient
    {
        public enum DarkSkyExceptions
        {
            [StringValue("FailedHttpRequest")]
            FailedHttpRequest,
            [StringValue("EmptyResponse")]
            EmptyResponse,
            [StringValue("FailedToDeserialize")]
            FailedToDeserialize,
            [StringValue("ApiError")]
            ApiError,
        }

        private readonly HttpClient httpClient;
        private readonly string baseUrl;

        public DarkSkyClient(string secretKey)
        {
            this.httpClient = new HttpClient();
            this.httpClient.Timeout = TimeSpan.FromSeconds(10);
            this.baseUrl = string.Format("https://api.darksky.net/forecast/{0}", secretKey);
        }

        public ForecastModel GetWeather(double latitude, double longitude)
        {
            string url = string.Format("{0}/{1:F},{2:F}?exclude=[minutely,hourly,alerts,flags]", baseUrl, latitude, longitude);

            HttpResponseMessage httpResponse;
            try
            {
                httpResponse = httpClient.GetAsync(url).Result;
            }
            catch (Exception ex)
            {
                throw new ObymobiException(DarkSkyExceptions.FailedHttpRequest, ex);
            }

            if (!httpResponse.IsSuccessStatusCode)
            {
                throw new ObymobiException(DarkSkyExceptions.FailedHttpRequest);
            }

            var responseString = httpResponse.Content.ReadAsStringAsync().Result;
            if (string.IsNullOrEmpty(responseString))
            {
                throw new ObymobiException(DarkSkyExceptions.EmptyResponse);
            }

            ForecastModel response;
            try
            {
                response = JsonConvert.DeserializeObject<ForecastModel>(responseString, new JsonSerializerSettings
                {
                    Error = (sender, args) => args.ErrorContext.Handled = true
                });
            }
            catch (Exception ex)
            {
                throw new ObymobiException(DarkSkyExceptions.FailedToDeserialize, ex);
            }

            return response;
        }
    }
}