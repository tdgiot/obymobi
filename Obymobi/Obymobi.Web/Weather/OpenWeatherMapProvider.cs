﻿using System;
using System.Net.Http;
using System.Threading;
using Dionysos.Web;
using Obymobi.Enums;
using Obymobi.Logic.Model.Weather;
using Obymobi.Web.Weather.OpenWeatherMap;
using Obymobi.Web.Weather.OpenWeatherMap.Model;

namespace Obymobi.Web.Weather
{
    public class OpenWeatherMapProvider : WeatherBaseProvider
    {
        /// <summary>
        /// The open weather map URL.
        /// </summary>
        private static readonly Uri OpenWeatherMapUrl = new Uri("http://api.openweathermap.org/data/2.5");

        /// <summary>
        /// Gets or sets the application identifier.
        /// </summary>
        /// <value>
        /// The application identifier.
        /// </value>
        public string AppId { get; set; }

        /// <summary>
        /// Gets or sets the HTTP client.
        /// </summary>
        /// <value>
        /// The HTTP client.
        /// </value>
        private HttpClient HttpClient { get; set; }

        private DateTime LastRequest { get; set; }

        public OpenWeatherMapProvider(string appId = "") : base("OpenWeatherMapProvider")
        {
            this.HttpClient = new HttpClient(new HttpClientHandler());
            this.AppId = appId;
            this.LastRequest = DateTime.MinValue;
        }

        public override WeatherItem GetCurrentWeatherForCity(string city)
        {
            WeatherItem returnResult = null;

            ApplyRequestThrottling();

            var client = new ApiClientBase(new Request(OpenWeatherMapUrl, HttpClient, AppId), "weather");
            var result = client.GetByName(city, null);
            if (result != null)
            {
                returnResult = ConvertWeatherItem(result);
            }
            
            return returnResult;
        }

        public override ForecastData GetForecastForCity(string city, int forecastDays)
        {
            ForecastData returnResult = null;

            ApplyRequestThrottling();

            var client = new ApiClientBase(new Request(OpenWeatherMapUrl, HttpClient, AppId), "forecast");
            client.Request.Uri = client.Request.Uri.AddSegment("daily");

            var result = client.GetByName(city, forecastDays);
            if (result != null)
            {
                returnResult = ConvertForecastData(result);
            }

            return returnResult;
        }

        public override WeatherItem GetCurrentWeatherForLocation(double latitude, double longitude)
        {
            WeatherItem returnResult = null;

            ApplyRequestThrottling();

            var client = new ApiClientBase(new Request(OpenWeatherMapUrl, HttpClient, AppId), "weather");
            var result = client.GetByCoordinates(latitude, longitude, null);
            if (result != null)
            {
                returnResult = ConvertWeatherItem(result);
            }
            
            return returnResult;
        }

        public override ForecastData GetForecastForLocation(double latitude, double longitude, int forecastDays)
        {
            ForecastData returnResult = null;

            ApplyRequestThrottling();

            var client = new ApiClientBase(new Request(OpenWeatherMapUrl, HttpClient, AppId), "forecast");
            client.Request.Uri = client.Request.Uri.AddSegment("daily");

            var result = client.GetByCoordinates(latitude, longitude, forecastDays);
            if (result != null)
            {
                returnResult = ConvertForecastData(result);
            }

            return returnResult;
        }

        #region Converters

        private ForecastData ConvertForecastData(OpenWeatherData data)
        {
            var forecast = new ForecastData();
            foreach (var forecastData in data.Forecasts)
            {
                var weather = new WeatherItem();
                weather.Date = forecastData.Date.ToUniversalTime();
                weather.Temperature = forecastData.Temperature.Day;
                weather.TemperatureNight = forecastData.Temperature.Night;
                weather.TemperatureMin = forecastData.Temperature.Min;
                weather.TemperatureMax = forecastData.Temperature.Max;
                weather.WeatherType = ConvertWeatherType(forecastData.Weather);

                forecast.Forecast.Add(weather);
            }

            return forecast;
        }

        private WeatherItem ConvertWeatherItem(OpenWeatherData item)
        {
            var weather = new WeatherItem();
            weather.Date = item.Date;
            weather.Temperature = item.Current.Temperature;
            weather.TemperatureNight = 0;
            weather.TemperatureMin = item.Current.Min;
            weather.TemperatureMax = item.Current.Max;
            weather.WeatherType = ConvertWeatherType(item.CurrentWeather);

            return weather;
        }

        private WeatherType ConvertWeatherType(int weatherId)
        {
            var type = WeatherType._Unknown;

            if (weatherId <= 232)
            {
                type = WeatherType.Thunderstorm;
            }
            else if (weatherId <= 321)
            {
                type = weatherId >= 313 ? WeatherType.Shower : WeatherType.Drizzle;
            }
            else if (weatherId <= 531)
            {
                if (weatherId <= 501 || weatherId >= 520)
                    type = WeatherType.Shower;
                else
                    type = WeatherType.Rain;
            }
            else if (weatherId <= 622)
            {
                type = weatherId <= 602 ? WeatherType.Snow : WeatherType.RainAndSleet;
            }
            else if (weatherId <= 781)
            {
                switch (weatherId)
                {
                    case 701:
                        type = WeatherType.Mist;
                        break;
                    case 711:
                        type = WeatherType.Smoke;
                        break;
                    case 741:
                        type = WeatherType.Fog;
                        break;
                    case 762:
                    case 761:
                        type = WeatherType.Dust;
                        break;
                    default:
                        type = WeatherType.Haze;
                        break;
                }
            }
            else if (weatherId <= 804)
            {
                switch (weatherId)
                {
                    case 800:
                        type = WeatherType.Clear;
                        break;
                    case 801:
                        type = WeatherType.CloudyPartly;
                        break;
                    default:
                        type = WeatherType.Cloudy;
                        break;
                }
            }
            else if (weatherId <= 962)
            {
                switch (weatherId)
                {
                    case 900:
                        type = WeatherType.Tornado;
                        break;
                    case 901:
                        type = WeatherType.TropicalStorm;
                        break;
                    case 962:
                    case 902:
                        type = WeatherType.Hurricane;
                        break;
                    case 905:
                        type = WeatherType.Wind;
                        break;
                    case 906:
                        type = WeatherType.Hail;
                        break;
                    case 951:
                        type = WeatherType.Clear;
                        break;
                    default:
                        if (weatherId <= 959)
                            type = WeatherType.Blustery;
                        else if (weatherId == 960 || weatherId == 961)
                            type = WeatherType.Storm;
                        else
                            type = WeatherType.Clear;
                        break;
                }
            }

            return type;
        }

        #endregion

        private void ApplyRequestThrottling()
        {
            if (DateTime.UtcNow.Subtract(LastRequest).TotalSeconds < 1)
            {
                Thread.Sleep(1000);
            }

            LastRequest = DateTime.UtcNow;
        }
    }
}