﻿using System;
using System.Net.Http;
using Dionysos;
using Newtonsoft.Json;
using Obymobi.Web.Weather.WUnderground.Models;

namespace Obymobi.Web.Weather.WUnderground
{
    /// <summary>
    /// The client object to access the WUnderground API
    /// </summary>
    public class WUndergroundClient
    {
        public enum WUndergroundExceptions
        {
            [StringValue("FailedHttpRequest")]
            FailedHttpRequest,
            [StringValue("EmptyResponse")]
            EmptyResponse,
            [StringValue("FailedToDeserialize")]
            FailedToDeserialize,
            [StringValue("ApiError")]
            ApiError,
        }

        private const string GeolookupCurrentConditionsAndForecastUri = "http://api.wunderground.com/api/{0}/geolookup/conditions/forecast/q/{1}.json";

        private readonly string ApiKey;
        private readonly HttpClient httpClient;

        public WUndergroundClient(string apiKey)
        {
            this.ApiKey = apiKey;
            this.httpClient = new HttpClient();
        }

        private WeatherResponse GetResponse(Uri uri)
        {
            HttpResponseMessage response;
            try
            {
                response = httpClient.GetAsync(uri).Result;
            }
            catch (Exception ex)
            {
                throw new ObymobiException(WUndergroundExceptions.FailedHttpRequest, ex);
            }

            if (!response.IsSuccessStatusCode)
            {
                throw new ObymobiException(WUndergroundExceptions.FailedHttpRequest);
            }

            var responseString = response.Content.ReadAsStringAsync().Result;
            if (string.IsNullOrEmpty(responseString))
            {
                throw new ObymobiException(WUndergroundExceptions.EmptyResponse);
            }

            WeatherResponse weatherResponse;
            try
            {
                weatherResponse = JsonConvert.DeserializeObject<WeatherResponse>(responseString, new JsonSerializerSettings
                {
                    Error = (sender, args) => args.ErrorContext.Handled = true
                });
            }
            catch (Exception ex)
            {
                throw new ObymobiException(WUndergroundExceptions.FailedToDeserialize, ex);
            }

            if (weatherResponse == null || weatherResponse.response == null)
            {
                throw new ObymobiException(WUndergroundExceptions.FailedToDeserialize);
            }

            if (weatherResponse.response.error != null)
            {
                throw new ObymobiException(WUndergroundExceptions.ApiError, "Error: {0}", weatherResponse.response.error.description);
            }

            return weatherResponse;
        }

        /// <summary>
        /// Gets the current conditions and forecast of the specified coordinates
        /// </summary>
        /// <param name="city">The name of the city</param>
        /// <returns>The response object</returns>
        public WeatherResponse GetConditionsAndForecastForLocationAsync(string city)
        {
            city = city.Replace(" ", "_");

            Uri uri = new Uri(string.Format(GeolookupCurrentConditionsAndForecastUri, this.ApiKey, city));
            return GetResponse(uri);
        }
    }

    /// <summary>
    /// An exception thrown by the WUnderground service
    /// </summary>
    public class WUndergroundException : Exception
    {
        /// <summary>
        /// Creates a new WUnderground exception with the specified message
        /// </summary>
        /// <param name="message">The message</param>
        public WUndergroundException(string message) : base(message) { }
    }
}
