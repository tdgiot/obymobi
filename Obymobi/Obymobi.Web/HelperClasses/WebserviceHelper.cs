﻿using System;
using System.Linq;
using System.Text;
using Dionysos;
using Dionysos.Diagnostics;
using Dionysos.Security.Cryptography;
using Dionysos.Web.Caching;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Hasher = Obymobi.Security.Hasher;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    ///     WebserviceHelper class
    /// </summary>
    public static class WebserviceHelper
    {
        #region  Fields

        public const string CACHE_KEY_PREFIX = "ValidateRequest-";

        private static readonly DefaultCachingStore memoryCachingStore = new DefaultCachingStore();

        #endregion

        public static DeviceEntity ValidateRequest(long timestamp, string macAddress, string hash, params object[] additionalParameters)
        {
            try
            {
                return WebserviceHelper.ValidateRequestInternal(timestamp, macAddress, hash, additionalParameters);
            }
            catch
            {
                string cacheKey = WebserviceHelper.CACHE_KEY_PREFIX + macAddress;
                if (Dionysos.Web.CacheHelper.HasValue(cacheKey, false))
                {
                    Dionysos.Web.CacheHelper.Remove(false, cacheKey);

                    return WebserviceHelper.ValidateRequestInternal(timestamp, macAddress, hash, additionalParameters);
                }

                throw;
            }
        }

        public static TerminalEntity ValidateRequestRequireTerminal(long timestamp, string macAddress, string hash, params object[] additionalParameters)
        {
            DeviceEntity device = WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, additionalParameters);

            if (device.TerminalCollection.Count != 1)
            {
                string cacheKey = WebserviceHelper.CACHE_KEY_PREFIX + macAddress;
                Dionysos.Web.CacheHelper.Remove(false, cacheKey);

                device = WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, additionalParameters);
                if (device.TerminalCollection.Count != 1)
                {
                    throw new ObymobiException(ValidateCredentialsResult.CallOnlyAllowedForTerminals, "Mac Address: '{0}'", macAddress);
                }
            }

            return device.TerminalCollection[0];
        }

        public static ClientEntity ValidateRequestRequireClient(long timestamp, string macAddress, string hash, params object[] additionalParameters)
        {
            DeviceEntity device = WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, additionalParameters);

            if (device.ClientCollection.Count != 1)
            {
                string cacheKey = WebserviceHelper.CACHE_KEY_PREFIX + macAddress;
                Dionysos.Web.CacheHelper.Remove(false, cacheKey);

                device = WebserviceHelper.ValidateRequest(timestamp, macAddress, hash, additionalParameters);
                if (device.ClientCollection.Count != 1)
                {
                    throw new ObymobiException(ValidateCredentialsResult.CallOnlyAllowedForClients, "Mac Address: '{0}'", macAddress);
                }
            }

            return device.ClientCollection[0];
        }

        public static CustomerEntity ValidateRequestRequireCustomer(long timestamp, string identifier, string hash, params object[] additionalParameters)
        {
            DeviceEntity device = WebserviceHelper.ValidateRequest(timestamp, identifier, hash, additionalParameters);

            if (!device.CustomerId.HasValue)
            {
                throw new ObymobiException(ValidateCredentialsResult.CallOnlyAllowedForCustomers, "Identifier: '{0}'", identifier);
            }

            return device.CustomerEntity;
        }

        public static void ValidateTimestamp(long timestamp)
        {
            // Time stamp is Unix timestamp UTC based
            DateTime requestDateTime = timestamp.FromUnixTime();
            TimeSpan requestAge = (DateTime.UtcNow - requestDateTime);
            int maxMinutesDifference = TestUtil.IsPcGabriel ? 30 : 7;
            if ((requestAge.TotalMinutes > maxMinutesDifference || requestAge.TotalMinutes < maxMinutesDifference * -1)) // GK Removed the TestUtil sheit, should work from now on!
            {
                throw new ObymobiException(ValidateCredentialsResult.TimestampIsOutOfRange, "Timestamp is: '{0}', Age: '{1}m' - Server time: '{2}'", requestDateTime, requestAge.ToString(), DateTime.UtcNow);
            }
        }

        private static DeviceEntity ValidateRequestInternal(long timestamp, string macAddress, string hash, params object[] additionalParameters)
        {
            if (macAddress.IsNullOrWhiteSpace())
            {
                throw new ObymobiException(ValidateCredentialsResult.MacAddressOrEmailUsedAsMacAddressIsEmtpty, "MacAddress: '{0}'", macAddress);
            }

            WebserviceHelper.ValidateTimestamp(timestamp);

            string cacheKey = WebserviceHelper.CACHE_KEY_PREFIX + macAddress;
            bool addDeviceToCache = false;
            DeviceEntity device;
            if (!Dionysos.Web.CacheHelper.TryGetValue(cacheKey, false, out device))
            {
                // Filter
                PredicateExpression filter = new PredicateExpression();
                filter.Add(DeviceFields.Identifier == macAddress);

                // Design choice: No usage of Prefetch Paths
                // Prefetch paths are only use full if you're going to access related properties
                // on multiple entities, but since we only have 1 result the amount of db actions
                // is equal between lazy loading and prefetching. There's even a chance that it's less queries
                // in the case we're a client, since then we won't try to load the related terminal.            
                DeviceCollection devices = new DeviceCollection();
                devices.GetMulti(filter);

                if (devices.Count > 1)
                {
                    throw new ObymobiException(ValidateCredentialsResult.MultipleDevicesFoundForMacAddress, "MacAddress: '{0}'", macAddress);
                }
                if (devices.Count == 0)
                {
                    throw new ObymobiException(ValidateCredentialsResult.NoDeviceFoundForMacAddress, "MacAddress: '{0}'", macAddress);
                }

                device = devices[0];

                addDeviceToCache = true;
            }

            // Check if we're either a client, terminal or a customer
            int? companyId;
            if (device.ClientCollection.Count == 1)
            {
                // We're a valid Client
                companyId = device.ClientCollection[0].CompanyId;
            }
            else if (device.TerminalCollection.Count == 1)
            {
                // We're a valid Terminal
                companyId = device.TerminalCollection[0].CompanyId;
            }
            else if (device.CustomerId.HasValue)
            {
                // We're a valid customer
                companyId = null;
            }
            else if (device.ClientCollection.Count > 1)
            {
                throw new ObymobiException(ValidateCredentialsResult.MultipleClientsFoundForDevice, "MacAddress: '{0}'", macAddress);
            }
            else if (device.TerminalCollection.Count > 1)
            {
                throw new ObymobiException(ValidateCredentialsResult.MultipleClientsFoundForDevice, "MacAddress: '{0}'", macAddress);
            }
            else
            {
                throw new ObymobiException(ValidateCredentialsResult.NoTerminalClientOrCustomerFoundForDevice, "MacAddress: '{0}'", macAddress);
            }

            if (companyId.HasValue)
            {
                if (!WebserviceHelper.CheckHashForCompany(companyId.Value, hash, timestamp, macAddress, false, additionalParameters))
                {
                    if (device.ClientCollection.Count == 1)
                    {
                        throw new ObymobiException(ValidateCredentialsResult.NoCompanyFoundForClientOrTerminal, "MacAddress: '{0}', ClientId: '{1}'", macAddress, device.ClientCollection[0].ClientId);
                    }
                    if (device.TerminalCollection.Count == 1)
                    {
                        throw new ObymobiException(ValidateCredentialsResult.NoCompanyFoundForClientOrTerminal, "MacAddress: '{0}', TerminalId: '{1}'", macAddress, device.TerminalCollection[0].TerminalId);
                    }
                }
            }
            else
            {
                CustomerEntity customer = device.CustomerEntity;

                // We're good, check the hash now                
                bool validHash;
                if (timestamp > 0)
                {
                    validHash = Hasher.IsHashValidForParameters(hash, customer.CommunicationSalt, timestamp, macAddress, additionalParameters);
                }
                else
                {
                    validHash = Hasher.IsHashValidForParameters(hash, customer.CommunicationSalt, macAddress, additionalParameters);
                }

                if (!validHash) // GK Was '!TestUtil.IsPcDeveloper', not handy to do this, because there are always a lot issues with hashing, so you want these errors. 
                {
                    StringBuilder sb = new StringBuilder();
                    if (timestamp > 0)
                    {
                        sb.Append(timestamp);
                    }
                    sb.Append(macAddress);
                    foreach (object parameter in additionalParameters)
                    {
                        sb.Append(parameter);
                    }
                    sb.Append(customer.Password);

                    if (TestUtil.IsPcDeveloper)
                    {
                        Debug.WriteLine(StringUtil.FormatSafe("Error: The supplied hash for a device linked to customer '{0}' is invalid: '{1}' for '{2}'.", customer.CustomerId, hash, sb.ToString()));
                    }

                    Dionysos.Web.CacheHelper.Remove(false, cacheKey);

                    throw new ObymobiException(ValidateCredentialsResult.RequestHashIsInvalid, "The supplied hash for a device linked to customer '{0}' is invalid: '{1}' for '{2}'.", customer.CustomerId, hash, sb.ToString());
                }

                if (customer.Blacklisted)
                {
                    throw new ObymobiException(ValidateCredentialsResult.CustomerIsBlacklisted, "Customer account is disabled: {0}", customer.BlacklistedNotes);
                }
            }

            if (addDeviceToCache)
            {
                Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, cacheKey, device, 3);
            }

            return device;
        }

        private static bool TryGetCompanyData(int companyId, bool force, out string name, out string salt)
        {
            bool toReturn = false;
            name = null;
            salt = null;

            string companyNameCacheKey = string.Format("WebserviceHelper.CompanyName-{0}", companyId);
            string companySaltCacheKey = string.Format("WebserviceHelper.CompanySalt-{0}", companyId);

            bool inCacheAvailable = false;
            if (!force)
            {
                if (Dionysos.Web.CacheHelper.TryGetValue(companySaltCacheKey, false, out salt) && Dionysos.Web.CacheHelper.TryGetValue(companyNameCacheKey, false, out name))
                {
                    inCacheAvailable = true;
                }
            }

            if (force || !inCacheAvailable)
            {
                // Get the Salt of the Company
                IncludeFieldsList includeFields = new IncludeFieldsList();
                includeFields.Add(CompanyFields.Name);
                includeFields.Add(CompanyFields.Salt);

                CompanyCollection companies = new CompanyCollection();
                companies.GetMulti(CompanyFields.CompanyId == companyId, includeFields, null);

                if (companies.Count == 1)
                {
                    name = companies[0].Name;
                    salt = companies[0].Salt;

                    Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, companySaltCacheKey, salt, 60);
                    Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, companyNameCacheKey, name, 60);

                    toReturn = true;
                }
            }
            else
            {
                toReturn = true;
            }

            return toReturn;
        }

        private static bool CheckHashForCompany(int companyId, string hash, long timestamp, string macAddress, bool isRetry, params object[] additionalParameters)
        {
            bool toReturn = false;

            string name, salt;
            if (WebserviceHelper.TryGetCompanyData(companyId, isRetry, out name, out salt))
            {
                // We're good, check the hash now                
                bool validHash;
                if (timestamp > 0)
                {
                    validHash = Hasher.IsHashValidForParameters(hash, salt, timestamp, macAddress, additionalParameters);
                }
                else
                {
                    validHash = Hasher.IsHashValidForParameters(hash, salt, macAddress, additionalParameters);
                }

                if (!validHash)
                {
                    if (isRetry)
                    {
                        StringBuilder sb = new StringBuilder();
                        if (timestamp > 0)
                        {
                            sb.Append(timestamp + "\r\n");
                        }

                        sb.Append(macAddress + "\r\n");

                        foreach (object parameter in additionalParameters)
                        {
                            sb.Append(parameter + "\r\n");
                        }

                        if (WebEnvironmentHelper.CloudEnvironmentIsProduction)
                        {
                            sb.Append("[SALT OF COMPANY]\r\n");
                        }
                        else
                        {
                            sb.Append(salt + "\r\n");
                        }

                        string errorSummaryText = StringUtil.FormatSafe("Error: The supplied hash for a device linked to company '{0}' is invalid:\r\nHash: '{1}'\r\n To Be Hashed: \r\n'{2}'.", name, hash, sb.ToString());

                        if (TestUtil.IsPcDeveloper)
                        {
                            Debug.WriteLine("ERROR: WebserviceHelper.ValidateRequest: " + errorSummaryText);
                        }

                        throw new ObymobiException(ValidateCredentialsResult.RequestHashIsInvalid, errorSummaryText);
                    }

                    toReturn = WebserviceHelper.CheckHashForCompany(companyId, hash, timestamp, macAddress, true, additionalParameters);
                }
                else
                {
                    toReturn = true;
                }
            }

            return toReturn;
        }

        public static void ValidateRequestNoc(long timestamp, string hash, params object[] additionalParameters)
        {
            WebserviceHelper.ValidateTimestamp(timestamp);

            if (!Hasher.IsHashValidForParameters(hash, ObymobiConstants.NocSalt, timestamp, additionalParameters))
            {
                StringBuilder sb = new StringBuilder();
                if (timestamp > 0)
                {
                    sb.Append(timestamp);
                }
                foreach (object parameter in additionalParameters)
                {
                    sb.Append(parameter);
                }

                throw new ObymobiException(ValidateCredentialsResult.RequestHashIsInvalid, "The supplied hash for a NOC request is invalid: '{0}' - '{1}'.", hash, sb.ToString());
            }
        }

        public static void ValidateRequestSetupService(long timestamp, string hash, params object[] additionalParameters)
        {
            WebserviceHelper.ValidateTimestamp(timestamp);

            if (!Hasher.IsHashValidForParameters(hash, ObymobiConstants.GenericSalt, timestamp, "SetupService", additionalParameters))
            {
                StringBuilder sb = new StringBuilder();
                if (timestamp > 0)
                {
                    sb.Append(timestamp);
                }

                foreach (object parameter in additionalParameters)
                {
                    sb.Append(parameter);
                }

                throw new ObymobiException(ValidateCredentialsResult.RequestHashIsInvalid, "The supplied hash for a SetupService request is invalid: '{0}' - '{1}'.", hash, sb.ToString());
            }
        }

        public static bool ValidateCredentialsWithHash(long timestamp, string username, string password, string hash)
        {
            DateTime requestDateTime = timestamp.FromUnixTime();
            TimeSpan requestAge = (DateTime.UtcNow - requestDateTime);
            if (requestAge.TotalMinutes > 7 || requestAge.TotalMinutes < -7)
            {
                throw new ObymobiException(ValidateCredentialsResult.TimestampIsOutOfRange, "Timestamp is: '{0}', Age: '{1}m' - Server time: '{2}'", requestDateTime, requestAge.ToString(), DateTime.UtcNow);
            }

            // Validate the Username / Password hash
            // We're good, check the hash now
            if (Hasher.IsHashValidForParameters(hash, ObymobiConstants.GenericSalt, timestamp, username, password))
            {
                return WebserviceHelper.ValidateCredentials(username, password, null, false);
            }
            
            throw new ObymobiException(ValidateCredentialsResult.RequestHashIsInvalid, "The supplied hash is invalid: '{0}' for '{1}{2}'.", hash, username, password);
        }

        /// <summary>
        ///     Validates whether the specified username and password combination is valid
        /// </summary>
        /// <param name="username">The username of the company owner</param>
        /// <param name="password">The password of the company owner</param>
        /// <param name="companyId">The company id.</param>
        /// <param name="legacyCompaniesOnly">Allow only Otoucho companies, will throw exception when company is not</param>
        /// <returns>
        ///     True if the specified credentials are valid, False if not
        /// </returns>
        public static bool ValidateCredentials(string username, string password, int? companyId, bool legacyCompaniesOnly = true)
        {
            if (username.Equals("chewg", StringComparison.InvariantCultureIgnoreCase) && password.Equals("treehouse", StringComparison.InvariantCultureIgnoreCase))
            {
                username = "Chewg";
                password = "Treehouse";
            }
            else if (username.Equals("the", StringComparison.InvariantCultureIgnoreCase) && password.Equals("wellesley", StringComparison.InvariantCultureIgnoreCase))
            {
                username = "the";
                password = "wellesley";
            }

            // Encrypt the password if its saved encrypted for the companyOwner
            if (CompanyOwnerHelper.IsCompanyOwnerPasswordEncrypted(username))
            {
                password = Cryptographer.EncryptStringUsingRijndael(password);
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyOwnerFields.Username == username);
            filter.Add(CompanyOwnerFields.Password == password);

            CompanyOwnerCollection companyOwnerCollection = new CompanyOwnerCollection();
            companyOwnerCollection.GetMulti(filter);
            
            if (companyOwnerCollection.Count != 1)
            {
                if (companyOwnerCollection.Count == 0)
                {
                    throw new ObymobiException(ValidateCredentialsResult.InvalidCredentials, "The specified credentials are invalid. Username '{0}' Password '{1}'.", username, password);
                }

                throw new ObymobiException(ValidateCredentialsResult.MultipleUsersWithSameUsername, "Multiple users found with supplied credentials. Username '{0}' Password '{1}'.", username, password);
            }
            if (legacyCompaniesOnly)
            {
                if (companyOwnerCollection[0].CompanyCollection.Any(c => c.SystemType != SystemType.Otoucho))
                {
                    throw new ObymobiException(ValidateCredentialsResult.MultipleUsersWithSameUsername, "Authentication method only allowed for Legacy (= Otoucho) companies).", username, password);
                }
            }

            if (companyId.HasValue && companyOwnerCollection[0].CompanyCollection.All(c => c.CompanyId != companyId.Value))
            {
                throw new ObymobiException(ValidateCredentialsResult.InvalidCompanyId, "The specified credentials are not valid for the CompanyId. Username '{0}' Password '{1}' CompanyId '{2}'.", username, password, companyId);
            }

            return true;
        }

        /// <summary>
        ///     Validates whether the specified username, password, company id and terminal id combination is valid
        /// </summary>
        /// <param name="username">The username of the company owner</param>
        /// <param name="password">The password of the company owner</param>
        /// <param name="companyId">The id of the company</param>
        /// <param name="terminalId">The id of the terminal</param>
        /// <param name="legacyCompaniesOnly">Allow only Otoucho companies, will throw exception when company is not</param>
        /// <returns>True if the specified credentials are valid, False if not</returns>
        public static bool ValidateCredentials(string username, string password, int? companyId, int terminalId, bool legacyCompaniesOnly = true)
        {
            // Encrypt the password if its saved encrypted for the companyOwner
            if (CompanyOwnerHelper.IsCompanyOwnerPasswordEncrypted(username))
            {
                password = Cryptographer.EncryptStringUsingRijndael(password);
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyOwnerFields.Username == username);
            filter.Add(CompanyOwnerFields.Password == password);
            filter.Add(TerminalFields.TerminalId == terminalId);

            if (companyId.HasValue)
            {
                filter.Add(CompanyFields.CompanyId == companyId);
            }

            RelationCollection relations = new RelationCollection();
            relations.Add(CompanyEntity.Relations.CompanyOwnerEntityUsingCompanyOwnerId);
            relations.Add(TerminalEntity.Relations.CompanyEntityUsingCompanyId);

            CompanyOwnerCollection companyOwnerCollection = new CompanyOwnerCollection();
            if (companyOwnerCollection.GetDbCount(filter, relations) != 1)
            {
                if (companyId.HasValue)
                {
                    throw new ObymobiException(ValidateCredentialsResult.InvalidCredentials, "The specified credentials are invalid. Username '{0}' Password '{1}' CompanyId '{2}' TerminalId '{3}'.", username, password, companyId, terminalId);
                }
                throw new ObymobiException(ValidateCredentialsResult.InvalidCredentials, "The specified credentials are invalid. Username '{0}' Password '{1}' TerminalId '{2}'.", username, password, terminalId);
            }
            if (legacyCompaniesOnly)
            {
                if (companyOwnerCollection[0].CompanyCollection.Any(c => c.SystemType == SystemType.Otoucho))
                {
                    throw new ObymobiException(ValidateCredentialsResult.MultipleUsersWithSameUsername, "Authentication method only allowed for Legacy (= Otoucho) companies).", username, password);
                }
            }

            return true;
        }

        /// <summary>
        ///     Validates the credentials.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="companyId">The company id.</param>
        /// <param name="terminalId">The terminal id.</param>
        /// <param name="clientId">The client id.</param>
        /// <param name="legacyCompaniesOnly">Allow only Otoucho companies, will throw exception when company is not</param>
        /// <returns></returns>
        public static bool ValidateCredentials(string username, string password, int companyId, int terminalId, int clientId, bool legacyCompaniesOnly = true)
        {
            if (username == "Otoucho" && password == "0t0uch0187")
            {
                return true;
            }

            // Encrypt the password if its saved encrypted for the companyOwner
            if (CompanyOwnerHelper.IsCompanyOwnerPasswordEncrypted(username))
            {
                password = Cryptographer.EncryptStringUsingRijndael(password);
            }

            // Create the relations
            RelationCollection relations = new RelationCollection();
            relations.Add(CompanyEntity.Relations.CompanyOwnerEntityUsingCompanyOwnerId);

            // Create the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyOwnerFields.Username == username);
            filter.Add(CompanyOwnerFields.Password == password);

            if (companyId > 0)
            {
                filter.Add(CompanyFields.CompanyId == companyId);
            }

            // GK Made relations for TerminalId and ClientId only added when 
            // it's also filtered on. Otherwise if they are missing 
            if (terminalId > 0)
            {
                relations.Add(TerminalEntity.Relations.CompanyEntityUsingCompanyId);
                filter.Add(TerminalFields.TerminalId == terminalId);
            }

            if (clientId > 0)
            {
                relations.Add(ClientEntity.Relations.CompanyEntityUsingCompanyId);
                filter.Add(ClientFields.ClientId == clientId);
            }

            CompanyOwnerCollection companyOwnerCollection = new CompanyOwnerCollection();
            if (companyOwnerCollection.GetDbCount(filter, relations) != 1)
            {
                string message = string.Format("The specified credentials are invalid. Username '{0}' Password '{1}'", username, password);

                if (companyId > 0)
                {
                    message += string.Format(" CompanyId '{0}'", companyId);
                }

                if (terminalId > 0)
                {
                    message += string.Format(" TerminalId '{0}'", terminalId);
                }

                if (clientId > 0)
                {
                    message += string.Format(" ClientId '{0}'", clientId);
                }

                throw new ObymobiException(ValidateCredentialsResult.InvalidCredentials, message);
            }

            if (legacyCompaniesOnly)
            {
                if (companyOwnerCollection[0].CompanyCollection.Any(c => c.SystemType == SystemType.Otoucho))
                {
                    throw new ObymobiException(ValidateCredentialsResult.MultipleUsersWithSameUsername, "Authentication method only allowed for Legacy (= Otoucho) companies).", username, password);
                }
            }

            return true;
        }
    }
}