﻿using System;
using System.Collections.Generic;
using System.IO;
using Dionysos;
using Obymobi;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.CraveService;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Web.HelperClasses
{
    /// <summary>
    /// Summary description for PublishHelper
    /// </summary>
    public static class PublishHelper
    {
        #region Fields

        private static List<string> webserviceUrls = null;

        #endregion

        #region Public methods

        public static void Publish(int companyId, WebserviceCall webserviceCall)
        {
            PublishHelper.Publish(companyId, false, webserviceCall);
        }

        public static void Publish(int companyId, bool asTerminal, WebserviceCall webserviceCall)
        {
            string mac;
            string salt;
            if (!PublishHelper.TryAndGetMacAddressAndSaltForCompany(companyId, asTerminal, out mac, out salt))
            {
                throw new Exception(string.Format("Unable to retrieve mac address and salt for company with companyId '{0}'", companyId));
            }

            foreach (string webserviceUrl in PublishHelper.GetWebserviceUrls())
            {
                CraveService service = new CraveService();
                service.Timeout = int.MaxValue;
                service.Url = webserviceUrl;

                long timestamp = DateTime.UtcNow.ToUnixTime();

                webserviceCall.Invoke(service, timestamp, mac, salt);
            }
        }

        public delegate void WebserviceCall(CraveService service, long timestamp, string mac, string salt);

        #endregion

        #region Private methods

        private static List<string> GetWebserviceUrls()
        {
            if (PublishHelper.webserviceUrls == null)
            {
                PublishHelper.webserviceUrls = new List<string>();
                if (WebEnvironmentHelper.CloudEnvironmentIsProduction)
                {
                    PublishHelper.webserviceUrls.Add(string.Format("https://obymobi-api.prod.trueguest.tech/{0}/CraveService.asmx", ObymobiConstants.API_VERSION));
                }
                else if (!TestUtil.IsPcDeveloper)
                {
                    PublishHelper.webserviceUrls.Add(WebEnvironmentHelper.GetApiCraveServiceUrl());
                }
                else if (TestUtil.IsPcFloris)
                {
                    PublishHelper.webserviceUrls.Add("http://localhost/trunk/api/31.0/craveservice.asmx");
                }
                else if (TestUtil.IsPcMathieu)
                {
                    PublishHelper.webserviceUrls.Add("http://localhost/trunk/api/31.0/craveservice.asmx");
                }
                else if (TestUtil.IsPcDeveloper)
                {
                    PublishHelper.webserviceUrls.Add(WebEnvironmentHelper.GetApiCraveServiceUrl());
                }
            }

            return PublishHelper.webserviceUrls;
        }

        private static bool TryAndGetMacAddressAndSaltForCompany(int companyId, bool asTerminal, out string macAddress, out string salt)
        {
            bool result = false;
            try
            {
                // Salt
                salt = PublishHelper.GetCompanySalt(companyId);

                // Mac
                if (asTerminal)
                {
                    macAddress = PublishHelper.GetMacAddressFromTerminal(companyId);
                }
                else
                {
                    macAddress = PublishHelper.GetMacAddressFromClient(companyId);
                }

                result = !salt.IsNullOrWhiteSpace() && !macAddress.IsNullOrWhiteSpace();
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            return result;
        }

        private static string GetCompanySalt(int companyId)
        {
            CompanyEntity companyEntity = new CompanyEntity();
            companyEntity.FetchUsingPK(companyId, null, null, new IncludeFieldsList(CompanyFields.Salt));

            return companyEntity.Salt;
        }

        private static string GetMacAddressFromClient(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(ClientFields.DeviceId != DBNull.Value);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, 1);

            string mac = string.Empty;
            if (clientCollection.Count > 0)
            {
                ClientEntity client = clientCollection[0];
                mac = client.DeviceEntity.Identifier;
            }
            else
            {
                mac = "zz:zz:zz:zz:zz:zz";
                PredicateExpression deviceFilter = new PredicateExpression(DeviceFields.Identifier == mac);

                DeviceCollection deviceCollection = new DeviceCollection();
                deviceCollection.GetMulti(deviceFilter, 1);

                if (deviceCollection.Count == 0)
                {
                    DeviceEntity device = new DeviceEntity();
                    device.Identifier = mac;
                    device.Type = DeviceType.Unknown;
                    device.Save();
                }
            }
            return mac;
        }

        private static string GetMacAddressFromTerminal(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.CompanyId == companyId);
            filter.Add(TerminalFields.DeviceId != DBNull.Value);

            TerminalCollection terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter, 1);

            if (terminalCollection.Count > 0)
            {
                return terminalCollection[0].DeviceEntity.Identifier;
            }
            return string.Empty;
        }

        #endregion
    }
}
