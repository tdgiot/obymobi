using Dionysos;
using Dionysos.Diagnostics;
using Dionysos.Globalization;
using Dionysos.Net.Mail;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace Obymobi.Logic.HelperClasses
{
    public class ScheduledCommandTaskHelper
    {
        /// <summary>
        /// Gets the scheduled command tasks which are active and have status 'Pending' or 'Started'
        /// </summary>
        /// <returns>A ScheduledCommandTaskCollection instance</returns>
        public static ScheduledCommandTaskCollection GetActiveScheduledCommandTasks()
        {
            // Create the subfilter for the statuses
            var statusFilter = new PredicateExpression();
            statusFilter.Add(ScheduledCommandTaskFields.Status == ScheduledCommandTaskStatus.Pending);
            statusFilter.AddWithOr(ScheduledCommandTaskFields.Status == ScheduledCommandTaskStatus.Started);

            // Create the main filter
            var filter = new PredicateExpression();
            filter.Add(ScheduledCommandTaskFields.Active == true);
            filter.Add(ScheduledCommandTaskFields.StartUTC <= DateTime.UtcNow);
            filter.Add(statusFilter);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ScheduledCommandTaskEntity);
            prefetch.Add(ScheduledCommandTaskEntity.PrefetchPathCompanyEntity, new IncludeFieldsList(CompanyFields.MaxDownloadConnections, CompanyFields.TimeZoneOlsonId));

            // Get the scheduled command tasks
            var scheduledCommandTaskCollection = new ScheduledCommandTaskCollection();
            scheduledCommandTaskCollection.GetMulti(filter, prefetch);

            return scheduledCommandTaskCollection;
        }

        /// <summary>
        /// Sends an email with the results of the scheduled command task if notification email addresses were specified.
        /// </summary>
        /// <param name="scheduledCommandTask">The ScheduledCommandTaskEntity to send the email for.</param>
        public static void SendEmail(ScheduledCommandTaskEntity scheduledCommandTask)
        {
            if (scheduledCommandTask.NotificationEmailAddresses.IsNullOrWhiteSpace())
                return;

            List<string> emailRecipients = scheduledCommandTask.NotificationEmailAddresses.Split("\r\n", StringSplitOptions.RemoveEmptyEntries).ToList();

            string message = string.Empty;
            string subject = string.Empty;

            GetEmailSubjectAndMessage(scheduledCommandTask, ref subject, ref message);

            // Send E-mails            
            DateTime utcNow = DateTime.UtcNow;
            DateTime ukTime = utcNow.UtcToLocalTime(TimeZoneInformation.GetGMTStandardTime());
            DateTime nlTime = utcNow.UtcToLocalTime(TimeZoneInformation.GetWEuropeStandardTime());
            string times = "UTC: {0:HH:mm:ss}\r\nUK: {1:HH:mm:ss}\r\nNL: {2:HH:mm:ss}\r\n".FormatSafe(utcNow, ukTime, nlTime);

            DateTime companyTime;
            if (!scheduledCommandTask.CompanyEntity.TimeZoneOlsonId.IsNullOrWhiteSpace())
            {
                Obymobi.TimeZone timeZone = Obymobi.TimeZone.Mappings[scheduledCommandTask.CompanyEntity.TimeZoneOlsonId];
                companyTime = TimeZoneHelper.ConvertFromUtc(utcNow, timeZone);
                times += "{0}: {1:HH:mm:ss}\r\n".FormatSafe(timeZone.OlsonTimeZoneId, companyTime);
            }
            else
            {
                companyTime = utcNow;
            }

            string dateTimeString = companyTime.ToString("[dd-MM HH:mm:ss] ");

            const string emailSenderAddress = "no-reply@crave-emenu.com";
            string emailSenderName = "Non-Live Scheduler";
            switch (WebEnvironmentHelper.CloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    emailSenderName = "Live Scheduler";
                    break;
                case CloudEnvironment.Test:
                    emailSenderName = "Test Scheduler";
                    break;
                case CloudEnvironment.Development:
                    emailSenderName = "Dev Scheduler";
                    break;
                case CloudEnvironment.Staging:
                    emailSenderName = "Staging Scheduler";
                    break;
                case CloudEnvironment.Manual:
                    emailSenderName = "Manual Scheduler";
                    break;
            }

            const string disclaimer = "This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to whom they are addressed. If you have received this email in error please notify the system manager. This message contains confidential information and is intended only for the individual named. If you are not the named addressee you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system. If you are not the intended recipient you are notified that disclosing, copying, distributing or taking any action in reliance on the contents of this information is strictly prohibited.";
            var senderEmail = new MailAddress(emailSenderAddress, emailSenderName);
            foreach (var address in emailRecipients)
            {
                // Try to send the email
                try
                {
                    var recipientEmail = new MailAddress(address);

                    const int timeout = 30000;

                    var mailUtil = new MailUtilV2(senderEmail, recipientEmail, dateTimeString + subject);
                    mailUtil.SmtpClient.Timeout = timeout;
                    mailUtil.BodyPlainText = message;
                    mailUtil.BodyPlainText += "\n\n" + times;
                    mailUtil.BodyPlainText += "\n\n" + disclaimer;
                    mailUtil.SendMail().Wait(timeout);
                }
                catch (Exception ex)
                {
                    // Do some awesome loggin   
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// Getsh the subject and body message for the email with the results.
        /// </summary>
        /// <param name="scheduledCommandTask">The finished scheduled command task to get the results for.</param>
        /// <param name="subject">The subject of the email.</param>
        /// <param name="message">The message of the email.</param>
        private static void GetEmailSubjectAndMessage(ScheduledCommandTaskEntity scheduledCommandTask, ref string subject, ref string message)
        {
            var clientCommandsSucceeded = new Dictionary<ClientCommand, int>();
            var clientCommandsFailed = new Dictionary<ClientCommand, int>();
            var terminalCommandsSucceeded = new Dictionary<TerminalCommand, int>();
            var terminalCommandsFailed = new Dictionary<TerminalCommand, int>();

            GetCommandSucceededFailedStatistics(scheduledCommandTask, clientCommandsSucceeded, clientCommandsFailed, terminalCommandsSucceeded, terminalCommandsFailed);

            string clientCommandSucceededMessage = GetClientCommandSucceededMessage(clientCommandsSucceeded);
            string clientCommandFailedMessage = GetClientCommandFailedMessage(clientCommandsFailed);
            string terminalCommandSucceededMessage = GetTerminalCommandSucceededMessage(terminalCommandsSucceeded);
            string terminalCommandFailedMessage = GetTerminalCommandFailedMessage(terminalCommandsFailed);

            switch (scheduledCommandTask.Status)
            {
                case ScheduledCommandTaskStatus.Inactive:
                    subject = string.Format("Scheduled command task with ID '{0}' is inactive", scheduledCommandTask.ScheduledCommandTaskId);
                    message = string.Format("{0}\r\n\r\nThis message should never happen, report to development team when you receive this message", subject);
                    break;
                case ScheduledCommandTaskStatus.Pending:
                    subject = string.Format("Scheduled command task with ID '{0}' is pending", scheduledCommandTask.ScheduledCommandTaskId);
                    message = string.Format("{0}\r\n\r\nThis message should never happen, report to development team when you receive this message", subject);
                    break;
                case ScheduledCommandTaskStatus.Started:
                    subject = string.Format("Scheduled command task with ID '{0}' is started", scheduledCommandTask.ScheduledCommandTaskId);
                    message = string.Format("{0}\r\n\r\nThis message should never happen, report to development team when you receive this message", subject);
                    break;
                case ScheduledCommandTaskStatus.Completed:
                    subject = string.Format("Scheduled command task with ID '{0}' completed successfully", scheduledCommandTask.ScheduledCommandTaskId);
                    message = string.Format("{0}\r\n\r\n", subject);
                    if (!clientCommandSucceededMessage.IsNullOrWhiteSpace()) message += clientCommandSucceededMessage + "\r\n";
                    if (!clientCommandFailedMessage.IsNullOrWhiteSpace()) message += clientCommandFailedMessage + "\r\n";
                    if (!terminalCommandSucceededMessage.IsNullOrWhiteSpace()) message += terminalCommandSucceededMessage + "\r\n";
                    if (!terminalCommandFailedMessage.IsNullOrWhiteSpace()) message += terminalCommandFailedMessage + "\r\n";
                    break;
                case ScheduledCommandTaskStatus.Expired:
                    subject = string.Format("Scheduled command task with ID '{0}' expired", scheduledCommandTask.ScheduledCommandTaskId);
                    message = string.Format("{0}\r\n\r\n", subject);
                    if (!clientCommandSucceededMessage.IsNullOrWhiteSpace()) message += clientCommandSucceededMessage + "\r\n";
                    if (!clientCommandFailedMessage.IsNullOrWhiteSpace()) message += clientCommandFailedMessage + "\r\n";
                    if (!terminalCommandSucceededMessage.IsNullOrWhiteSpace()) message += terminalCommandSucceededMessage + "\r\n";
                    if (!terminalCommandFailedMessage.IsNullOrWhiteSpace()) message += terminalCommandFailedMessage + "\r\n";
                    break;
                case ScheduledCommandTaskStatus.CompletedWithErrors:
                    subject = string.Format("Scheduled command task with ID '{0}' completed with errors", scheduledCommandTask.ScheduledCommandTaskId);
                    message = string.Format("{0}\r\n\r\n", subject);
                    if (!clientCommandSucceededMessage.IsNullOrWhiteSpace()) message += clientCommandSucceededMessage + "\r\n";
                    if (!clientCommandFailedMessage.IsNullOrWhiteSpace()) message += clientCommandFailedMessage + "\r\n";
                    if (!terminalCommandSucceededMessage.IsNullOrWhiteSpace()) message += terminalCommandSucceededMessage + "\r\n";
                    if (!terminalCommandFailedMessage.IsNullOrWhiteSpace()) message += terminalCommandFailedMessage + "\r\n";
                    break;
            }
        }

        /// <summary>
        /// Gets the success/fail statistics for the commands that were executed.
        /// </summary>
        private static void GetCommandSucceededFailedStatistics(ScheduledCommandTaskEntity scheduledCommandTask, Dictionary<ClientCommand, int> clientCommandsSucceeded, Dictionary<ClientCommand, int> clientCommandsFailed, Dictionary<TerminalCommand, int> terminalCommandsSucceeded, Dictionary<TerminalCommand, int> terminalCommandsFailed)
        {
            var filter = new PredicateExpression();
            filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == scheduledCommandTask.ScheduledCommandTaskId);

            var fields = new IncludeFieldsList(ScheduledCommandFields.Status, ScheduledCommandFields.ClientCommand, ScheduledCommandFields.TerminalCommand);

            var scheduledCommandCollection = new ScheduledCommandCollection();
            scheduledCommandCollection.GetMulti(filter, fields, null);

            foreach (ScheduledCommandEntity scheduledCommandEntity in scheduledCommandCollection)
            {
                if (scheduledCommandEntity.ClientCommand.HasValue)
                {
                    if (scheduledCommandEntity.Status == ScheduledCommandStatus.Succeeded)
                    {
                        if (clientCommandsSucceeded.ContainsKey(scheduledCommandEntity.ClientCommand.Value)) clientCommandsSucceeded[scheduledCommandEntity.ClientCommand.Value]++;
                        else clientCommandsSucceeded.Add(scheduledCommandEntity.ClientCommand.Value, 1);
                    }
                    else
                    {
                        if (clientCommandsFailed.ContainsKey(scheduledCommandEntity.ClientCommand.Value)) clientCommandsFailed[scheduledCommandEntity.ClientCommand.Value]++;
                        else clientCommandsFailed.Add(scheduledCommandEntity.ClientCommand.Value, 1);
                    }
                }
                else if (scheduledCommandEntity.TerminalCommand.HasValue)
                {
                    if (scheduledCommandEntity.Status == ScheduledCommandStatus.Succeeded)
                    {
                        if (terminalCommandsSucceeded.ContainsKey(scheduledCommandEntity.TerminalCommand.Value)) terminalCommandsSucceeded[scheduledCommandEntity.TerminalCommand.Value]++;
                        else terminalCommandsSucceeded.Add(scheduledCommandEntity.TerminalCommand.Value, 1);
                    }
                    else
                    {
                        if (terminalCommandsFailed.ContainsKey(scheduledCommandEntity.TerminalCommand.Value)) terminalCommandsFailed[scheduledCommandEntity.TerminalCommand.Value]++;
                        else terminalCommandsFailed.Add(scheduledCommandEntity.TerminalCommand.Value, 1);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the message for the client commands that have succeeded.
        /// </summary>
        private static string GetClientCommandSucceededMessage(Dictionary<ClientCommand, int> clientCommandsSucceeded)
        {
            string message = string.Empty;

            foreach (ClientCommand clientCommand in clientCommandsSucceeded.Keys)
            {
                int count = clientCommandsSucceeded[clientCommand];
                bool multiple = clientCommandsSucceeded[clientCommand] > 1;

                switch (clientCommand)
                {
                    case ClientCommand.DoNothing:
                        break;
                    case ClientCommand.RestartApplication:
                        if (multiple) message += string.Format("{0} clients have restarted the application\r\n", count);
                        else message += "1 client has restarted the application\r\n";
                        break;
                    case ClientCommand.RestartDevice:
                        if (multiple) message += string.Format("{0} clients have restarted the device\r\n", count);
                        else message += "1 client has restarted the device\r\n";
                        break;
                    case ClientCommand.RestartInRecovery:
                        if (multiple) message += string.Format("{0} clients have restarted in recovery\r\n", count);
                        else message += "1 client has restarted in recovery\r\n";
                        break;
                    case ClientCommand.SendLogToWebservice:
                        if (multiple) message += string.Format("{0} clients have sent the log to the webservice\r\n", count);
                        else message += "1 client has sent the log to the webservice\r\n";
                        break;
                    case ClientCommand.DailyOrderReset:
                        if (multiple) message += string.Format("{0} clients have executed the daily order reset\r\n", count);
                        else message += "1 client has executed the daily order reset\r\n";
                        break;
                    case ClientCommand.DownloadEmenuUpdate:
                        if (multiple) message += string.Format("{0} clients have downloaded the Emenu update\r\n", count);
                        else message += "1 client has downloaded the Emenu update\r\n";
                        break;
                    case ClientCommand.DownloadAgentUpdate:
                        if (multiple) message += string.Format("{0} clients have downloaded the Agent update\r\n", count);
                        else message += "1 client has downloaded the Agent update\r\n";
                        break;
                    case ClientCommand.DownloadSupportToolsUpdate:
                        if (multiple) message += string.Format("{0} clients have downloaded the SupportTools update\r\n", count);
                        else message += "1 client has downloaded the SupportTools update\r\n";
                        break;
                    case ClientCommand.InstallEmenuUpdate:
                        if (multiple) message += string.Format("{0} clients have installed the Emenu update\r\n", count);
                        else message += "1 client has installed the Emenu update\r\n";
                        break;
                    case ClientCommand.InstallAgentUpdate:
                        if (multiple) message += string.Format("{0} clients have installed the Agent update\r\n", count);
                        else message += "1 client has installed the Agent update\r\n";
                        break;
                    case ClientCommand.InstallSupportToolsUpdate:
                        if (multiple) message += string.Format("{0} clients have installed the SupportTools update\r\n", count);
                        else message += "1 client has installed the SupportTools update\r\n";
                        break;
                    case ClientCommand.ClearBrowserCache:
                        if (multiple) message += string.Format("{0} clients have cleared the browser cache\r\n", count);
                        else message += "1 client has cleared the browser cache the Emenu update\r\n";
                        break;
                    case ClientCommand.DownloadOSUpdate:
                        if (multiple) message += string.Format("{0} clients have downloaded the OS update\r\n", count);
                        else message += "1 client has downloaded the OS update\r\n";
                        break;
                    case ClientCommand.InstallOSUpdate:
                        if (multiple) message += string.Format("{0} clients have installed the OS update\r\n", count);
                        else message += "1 client has installed the OS update\r\n";
                        break;
                    case ClientCommand.DownloadInstallMessagingServiceUpdate:
                        if (multiple) message += string.Format("{0} clients downloaded and installed the MessagingService update\r\n", count);
                        else message += "1 client downloaded and installed the MessagingService update\r\n";
                        break;
                    default:
                        message += "Unknown client command\r\n";
                        break;
                }
            }

            return message;
        }

        /// <summary>
        /// Gets the message for the client commands that have failed.
        /// </summary>
        private static string GetClientCommandFailedMessage(Dictionary<ClientCommand, int> clientCommandsFailed)
        {
            string message = string.Empty;

            foreach (ClientCommand clientCommand in clientCommandsFailed.Keys)
            {
                int count = clientCommandsFailed[clientCommand];
                bool multiple = clientCommandsFailed[clientCommand] > 1;

                switch (clientCommand)
                {
                    case ClientCommand.DoNothing:
                        break;
                    case ClientCommand.RestartApplication:
                        if (multiple) message += string.Format("{0} clients have failed to restart the application\r\n", count);
                        else message += "1 client has failed to restart the application\r\n";
                        break;
                    case ClientCommand.RestartDevice:
                        if (multiple) message += string.Format("{0} clients have failed to restart the device\r\n", count);
                        else message += "1 client has failed to restart the device\r\n";
                        break;
                    case ClientCommand.RestartInRecovery:
                        if (multiple) message += string.Format("{0} clients have failed to restart in recovery\r\n", count);
                        else message += "1 client has failed to restart in recovery\r\n";
                        break;
                    case ClientCommand.SendLogToWebservice:
                        if (multiple) message += string.Format("{0} clients have failed to sent the log to the webservice\r\n", count);
                        else message += "1 client has failed to sent the log to the webservice\r\n";
                        break;
                    case ClientCommand.DailyOrderReset:
                        if (multiple) message += string.Format("{0} clients have failed to execute the daily order reset\r\n", count);
                        else message += "1 client has failed to execute the daily order reset\r\n";
                        break;
                    case ClientCommand.DownloadEmenuUpdate:
                        if (multiple) message += string.Format("{0} clients have failed to download the Emenu update\r\n", count);
                        else message += "1 client has failed to download the Emenu update\r\n";
                        break;
                    case ClientCommand.DownloadAgentUpdate:
                        if (multiple) message += string.Format("{0} clients have failed to download the Agent update\r\n", count);
                        else message += "1 client has failed to download the Agent update\r\n";
                        break;
                    case ClientCommand.DownloadSupportToolsUpdate:
                        if (multiple) message += string.Format("{0} clients have failed to download the SupportTools update\r\n", count);
                        else message += "1 client has failed to download the SupportTools update\r\n";
                        break;
                    case ClientCommand.InstallEmenuUpdate:
                        if (multiple) message += string.Format("{0} clients have failed to install the Emenu update\r\n", count);
                        else message += "1 client has failed to install the Emenu update\r\n";
                        break;
                    case ClientCommand.InstallAgentUpdate:
                        if (multiple) message += string.Format("{0} clients have failed to install the Agent update\r\n", count);
                        else message += "1 client has failed to install the Agent update\r\n";
                        break;
                    case ClientCommand.InstallSupportToolsUpdate:
                        if (multiple) message += string.Format("{0} clients have failed to install the SupportTools update\r\n", count);
                        else message += "1 client has failed to install the SupportTools update\r\n";
                        break;
                    case ClientCommand.ClearBrowserCache:
                        if (multiple) message += string.Format("{0} clients have failed to cleare the browser cache\r\n", count);
                        else message += "1 client has failed to cleare the browser cache the Emenu update\r\n";
                        break;
                    case ClientCommand.DownloadOSUpdate:
                        if (multiple) message += string.Format("{0} clients have failed to download the OS update\r\n", count);
                        else message += "1 client has failed to download the OS update\r\n";
                        break;
                    case ClientCommand.InstallOSUpdate:
                        if (multiple) message += string.Format("{0} clients have failed to install the OS update\r\n", count);
                        else message += "1 client has failed to install the OS update\r\n";
                        break;
                    case ClientCommand.DownloadInstallMessagingServiceUpdate:
                        if (multiple) message += string.Format("{0} clients have failed to download and install the MessagingService update\r\n", count);
                        else message += "1 client has failed to download and installed the MessagingService update\r\n";
                        break;
                    default:
                        message += "Unknown client command\r\n";
                        break;
                }
            }

            return message;
        }

        /// <summary>
        /// Gets the message for the terminal commands that have succeeded.
        /// </summary>
        private static string GetTerminalCommandSucceededMessage(Dictionary<TerminalCommand, int> terminalCommandsSucceeded)
        {
            string message = string.Empty;

            foreach (TerminalCommand terminalCommand in terminalCommandsSucceeded.Keys)
            {
                int count = terminalCommandsSucceeded[terminalCommand];
                bool multiple = terminalCommandsSucceeded[terminalCommand] > 1;

                switch (terminalCommand)
                {
                    case TerminalCommand.DoNothing:
                        break;
                    case TerminalCommand.RestartApplication:
                        if (multiple) message += string.Format("{0} terminals have restarted the application\r\n", count);
                        else message += "1 terminal has restarted the application\r\n";
                        break;
                    case TerminalCommand.RestartDevice:
                        if (multiple) message += string.Format("{0} terminals have restarted the device\r\n", count);
                        else message += "1 terminal has restarted the device\r\n";
                        break;
                    case TerminalCommand.RestartInRecovery:
                        if (multiple) message += string.Format("{0} terminals have restarted in recovery\r\n", count);
                        else message += "1 terminal has restarted in recovery\r\n";
                        break;
                    case TerminalCommand.SendLogToWebservice:
                        if (multiple) message += string.Format("{0} terminals have sent the log to the webservice\r\n", count);
                        else message += "1 terminal has sent the log to the webservice\r\n";
                        break;
                    case TerminalCommand.DownloadConsoleUpdate:
                        if (multiple) message += string.Format("{0} terminals have downloaded the Console update\r\n", count);
                        else message += "1 terminal has downloaded the Emenu update\r\n";
                        break;
                    case TerminalCommand.DownloadAgentUpdate:
                        if (multiple) message += string.Format("{0} terminals have downloaded the Agent update\r\n", count);
                        else message += "1 terminal has downloaded the Agent update\r\n";
                        break;
                    case TerminalCommand.DownloadSupportToolsUpdate:
                        if (multiple) message += string.Format("{0} terminals have downloaded the SupportTools update\r\n", count);
                        else message += "1 terminal has downloaded the SupportTools update\r\n";
                        break;
                    case TerminalCommand.InstallConsoleUpdate:
                        if (multiple) message += string.Format("{0} terminals have installed the Console update\r\n", count);
                        else message += "1 terminal has installed the Emenu update\r\n";
                        break;
                    case TerminalCommand.InstallAgentUpdate:
                        if (multiple) message += string.Format("{0} terminals have installed the Agent update\r\n", count);
                        else message += "1 terminal has installed the Agent update\r\n";
                        break;
                    case TerminalCommand.InstallSupportToolsUpdate:
                        if (multiple) message += string.Format("{0} terminals have installed the SupportTools update\r\n", count);
                        else message += "1 terminal has installed the SupportTools update\r\n";
                        break;
                    case TerminalCommand.ClearBrowserCache:
                        if (multiple) message += string.Format("{0} terminals have cleared the browser cache\r\n", count);
                        else message += "1 terminal has cleared the browser cache the Emenu update\r\n";
                        break;
                    case TerminalCommand.DownloadOSUpdate:
                        if (multiple) message += string.Format("{0} terminals have downloaded the OS update\r\n", count);
                        else message += "1 terminal has downloaded the OS update\r\n";
                        break;
                    case TerminalCommand.InstallOSUpdate:
                        if (multiple) message += string.Format("{0} terminals have installed the OS update\r\n", count);
                        else message += "1 terminal has installed the OS update\r\n";
                        break;
                    case TerminalCommand.DownloadInstallMessagingServiceUpdate:
                        if (multiple) message += string.Format("{0} terminals have downloaded and installed the MessagingService update\r\n", count);
                        else message += "1 terminal has downloaded and installed the MessagingService update\r\n";
                        break;
                    default:
                        message += "Unknown terminal command\r\n";
                        break;
                }
            }

            return message;
        }

        /// <summary>
        /// Gets the message for the terminal commands that have failed.
        /// </summary>
        private static string GetTerminalCommandFailedMessage(Dictionary<TerminalCommand, int> terminalCommandsFailed)
        {
            string message = string.Empty;

            foreach (TerminalCommand terminalCommand in terminalCommandsFailed.Keys)
            {
                int count = terminalCommandsFailed[terminalCommand];
                bool multiple = terminalCommandsFailed[terminalCommand] > 1;

                switch (terminalCommand)
                {
                    case TerminalCommand.DoNothing:
                        break;
                    case TerminalCommand.RestartApplication:
                        if (multiple) message += string.Format("{0} terminals have failed to restart the application\r\n", count);
                        else message += "1 terminal has failed to restart the application\r\n";
                        break;
                    case TerminalCommand.RestartDevice:
                        if (multiple) message += string.Format("{0} terminals have failed to restart the device\r\n", count);
                        else message += "1 terminal has failed to restart the device\r\n";
                        break;
                    case TerminalCommand.RestartInRecovery:
                        if (multiple) message += string.Format("{0} terminals have failed to restart in recovery\r\n", count);
                        else message += "1 terminal has failed to restart in recovery\r\n";
                        break;
                    case TerminalCommand.SendLogToWebservice:
                        if (multiple) message += string.Format("{0} terminals have failed to sent the log to the webservice\r\n", count);
                        else message += "1 terminal has failed to sent the log to the webservice\r\n";
                        break;
                    case TerminalCommand.DownloadConsoleUpdate:
                        if (multiple) message += string.Format("{0} terminals have failed to download the Console update\r\n", count);
                        else message += "1 terminal has failed to download the Console update\r\n";
                        break;
                    case TerminalCommand.DownloadAgentUpdate:
                        if (multiple) message += string.Format("{0} terminals have failed to download the Agent update\r\n", count);
                        else message += "1 terminal has failed to download the Agent update\r\n";
                        break;
                    case TerminalCommand.DownloadSupportToolsUpdate:
                        if (multiple) message += string.Format("{0} terminals have failed to download the SupportTools update\r\n", count);
                        else message += "1 terminal has failed to download the SupportTools update\r\n";
                        break;
                    case TerminalCommand.InstallConsoleUpdate:
                        if (multiple) message += string.Format("{0} terminals have failed to install the Console update\r\n", count);
                        else message += "1 terminal has failed to install the Console update\r\n";
                        break;
                    case TerminalCommand.InstallAgentUpdate:
                        if (multiple) message += string.Format("{0} terminals have failed to install the Agent update\r\n", count);
                        else message += "1 terminal has failed to install the Agent update\r\n";
                        break;
                    case TerminalCommand.InstallSupportToolsUpdate:
                        if (multiple) message += string.Format("{0} terminals have failed to install the SupportTools update\r\n", count);
                        else message += "1 terminal has failed to install the SupportTools update\r\n";
                        break;
                    case TerminalCommand.ClearBrowserCache:
                        if (multiple) message += string.Format("{0} terminals have failed to cleare the browser cache\r\n", count);
                        else message += "1 terminal has failed to cleare the browser cache the Emenu update\r\n";
                        break;
                    case TerminalCommand.DownloadOSUpdate:
                        if (multiple) message += string.Format("{0} terminals have failed to download the OS update\r\n", count);
                        else message += "1 terminal has failed to download the OS update\r\n";
                        break;
                    case TerminalCommand.InstallOSUpdate:
                        if (multiple) message += string.Format("{0} terminals have failed to install the OS update\r\n", count);
                        else message += "1 terminal has failed to install the OS update\r\n";
                        break;
                    case TerminalCommand.DownloadInstallMessagingServiceUpdate:
                        if (multiple) message += string.Format("{0} terminals have failed to download and install the MessagingService update\r\n", count);
                        else message += "1 terminal has failed to download and install the MessagingService update\r\n";
                        break;
                    default:
                        message += "Unknown terminal command\r\n";
                        break;
                }
            }

            return message;
        }

        public static bool IsScheduledCommandTaskRunning(int companyId)
        {
            // Check if scheduledcommandtask is running
            PredicateExpression preStartedFilter = new PredicateExpression();
            preStartedFilter.Add(ScheduledCommandTaskFields.StartUTC >= DateTime.UtcNow);
            preStartedFilter.Add(ScheduledCommandTaskFields.StartUTC <= DateTime.UtcNow.AddMinutes(15));

            PredicateExpression startedFilter = new PredicateExpression();
            startedFilter.Add(ScheduledCommandTaskFields.StartUTC <= DateTime.UtcNow);
            startedFilter.Add(ScheduledCommandTaskFields.ExpiresUTC >= DateTime.UtcNow);

            PredicateExpression statusFilter = new PredicateExpression();
            statusFilter.Add(preStartedFilter); // Starts within 5 minutes
            statusFilter.AddWithOr(startedFilter); // Already started

            PredicateExpression taskFilter = new PredicateExpression();
            taskFilter.Add(ScheduledCommandTaskFields.CompanyId == companyId);
            taskFilter.Add(ScheduledCommandTaskFields.Active == true);
            taskFilter.Add(statusFilter);

            ScheduledCommandTaskCollection scheduledCommandTaskCollection = new ScheduledCommandTaskCollection();
            return scheduledCommandTaskCollection.GetDbCount(taskFilter) > 0;
        }

        public static ScheduledCommandCollection GetUnsuccessfullCommands(int scheduledCommandTaskId)
        {
            PredicateExpression filter = new PredicateExpression(ScheduledCommandFields.Status != ScheduledCommandStatus.Succeeded);
            filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == scheduledCommandTaskId);

            ScheduledCommandCollection scheduledCommands = new ScheduledCommandCollection();
            scheduledCommands.GetMulti(filter);

            return scheduledCommands;
        }

        public static bool GetHasUnsuccessfullCommands(int scheduledCommandTaskId)
        {
            PredicateExpression filter = new PredicateExpression(ScheduledCommandFields.Status != ScheduledCommandStatus.Succeeded);
            filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == scheduledCommandTaskId);

            ScheduledCommandCollection scheduledCommands = new ScheduledCommandCollection();
            return scheduledCommands.GetDbCount(filter) > 0;
        }
    }
}
