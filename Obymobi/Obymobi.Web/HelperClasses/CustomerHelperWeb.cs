﻿using Dionysos;
using Dionysos.Net.Mail;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Security;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.HelperClasses
{
    public static class CustomerHelperWeb
    {
        public enum CustomerHelperWebResult
        {
            NoCustomerFound = 300,
            CustomerNotVerified = 309,
            NewEmailEqualsOldEmail = 310

        }

        #region Authentication

        /// <summary>
        /// Authenticate customer 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="passwordTransferHash"></param>
        /// <param name="customer"></param>
        public static void AuthenticateCustomer(string emailAddress, string passwordTransferHash, out CustomerEntity customer)
        {
            if (emailAddress.IsNullOrWhiteSpace())
                throw new ObymobiException(AuthenticationResult.NoEmailAddressSupplied);

            // Get the customer collection using the specified emailAddress
            customer = null;
            CustomerCollection customerCollection = CustomerHelper.GetCustomersByEmail(emailAddress);

            if (customerCollection.Count == 0)
            {
                // No customer found        
                throw new ObymobiException(AuthenticationResult.EmailUnkown, "Emailaddress: '{0}'", emailAddress);
            }
            else if (customerCollection.Count > 1)
            {
                // Multiple customers found
                throw new ObymobiException(AuthenticationResult.MultipleCustomersFound, "Emailaddress: '{0}'", emailAddress);
            }
            else if (customerCollection.Count == 1)
            {
                // Sinlge customer found.                
                customer = customerCollection[0];

                // Validate the customer
                CustomerHelperWeb.ValidateCustomer(customer, customer.Password, customer.PasswordSalt, passwordTransferHash);
            }
        }

        /// <summary>
        /// Authenticate Customer based on customerId and hashedpassword
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="hashedPassword">Hashed Password</param>
        /// <param name="customer">The customer.</param>
        public static void AuthenticateCustomer(int customerId, string hashedPassword, ref CustomerEntity customer)
        {
            customer = new CustomerEntity(customerId);
            if (customer.IsNew)
                throw new ObymobiException(AuthenticationResult.CustomerIdUnknown, "CustomerId: {0}, Password: {1}", customerId, hashedPassword);
            else if (customer.ClientId.HasValue)
            {
                // Otoucho client
            }
            else
            {
                throw new NotImplementedException("This method is only useable for Otoucho clients, which mean they are linked to a Client.");
            }
        }

        /// <summary>
        /// Authenticate the credentials of a customer
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="storedPassword"></param>
        /// <param name="passwordTransferHash"></param>
        /// <returns></returns>
        public static bool AuthenticateCredentials(string storedPassword, string storedSalt, string passwordTransferHash)
        {
            bool valid = false;

            // Create the Storage Hash
            string passwordStorageHash = CustomerPasswordHelper.GetPasswordStorageHash(passwordTransferHash, storedSalt);

            // Check whether the specified password equals 
            // the customer's password in the database
            if (storedPassword.Equals(passwordStorageHash))
                valid = true;

            return valid;
        }

        #endregion

        #region Validation

        /// <summary>
        /// Performs a series of tests to validate the customer entity
        /// </summary>
        /// <param name="customer"></param>
        public static void ValidateCustomer(CustomerEntity customer, string storedPassword, string storedSalt, string passwordTransferHash)
        {
            // Customer is blacklisted, don't waste any more CPU cycles.
            if (customer.Blacklisted)
            {
                // Customer is blacklisted
                throw new ObymobiException(AuthenticationResult.Blacklisted, "Emailaddress: '{0}'", customer.Email);
            }

            // Mechanism to counter brute force by a max amount of tries per period (which grows)
            bool awaitingAttempts = false;
            int minutesToWaitTillNextAttempt = 0;
            if (customer.FailedPasswordAttemptCount >= 3 && customer.FailedPasswordAttemptCount % 3 == 0)
            {
                // 2^(attempts / 3)
                int waitMinutes = (int)System.Math.Pow(customer.FailedPasswordAttemptCount / 3, 2);

                DateTime waitCompleted = customer.LastFailedPasswordAttemptUTC.Value;
                waitCompleted = waitCompleted.AddMinutes(waitMinutes);

                if (waitCompleted > DateTime.UtcNow)
                {
                    awaitingAttempts = true;
                    minutesToWaitTillNextAttempt = Convert.ToInt32((waitCompleted - DateTime.UtcNow).TotalMinutes);
                }
            }

            // Customer must wait for next attempt?
            if (awaitingAttempts)
            {
                // DON'T change the text of this error, it's minutes are parsed in other code!
                throw new ObymobiException(AuthenticationResult.AwaitingAttempts, "Too many failed attempts, new attempt allowed in '{0}' minutes.", minutesToWaitTillNextAttempt);
            }

            if (AuthenticateCredentials(storedPassword, storedSalt, passwordTransferHash))
            {
                // Valid sign-on
                try
                {
                    if (customer.NewEmailAddressVerified)
                    {
                        // The customer has verified his new email address through the verification link in the email
                        // The new email email will be used from now on                        
                        customer.Email = customer.NewEmailAddress;
                        customer.NewEmailAddress = null;
                        customer.NewEmailAddressVerified = false;
                        customer.NewEmailAddressConfirmationGuid = null;
                    }

                    customer.FailedPasswordAttemptCount = 0;
                    customer.LastFailedPasswordAttemptUTC = null;
                    customer.Save();
                }
                catch (Exception ex)
                {
                    throw new ObymobiException(GenericWebserviceCallResult.UnspecifiedEntitySaveFalseFailure, ex, "Error during reset of FailedPasswordAttemptCount for customer: {0}", customer.Email);
                }
                
                //if (!customer.Verified && !customer.AnonymousAccount)
                //{
                //    // Customer isn't verified yet, send verification email and throw exception
                //    CustomerHelperWeb.SendConfirmationEmail(customer, customer.Email);
                //    throw new ObymobiException(AuthenticationResult.AccountNotVerified);
                //}
            }
            else
            {
                // Password is incorrect, update the failed password attempt information
                customer.FailedPasswordAttemptCount++;
                customer.LastFailedPasswordAttemptUTC = DateTime.UtcNow;
                customer.Save();
                throw new ObymobiException(AuthenticationResult.PasswordIncorrect, "Emailaddress: '{0}'", customer.Email);
            }
        }

        #endregion

        /// <summary>
        /// Sends an email to the registered customer to confirm his email address
        /// </summary>
        /// <param name="emailAddress"></param>
        public static void SendConfirmationEmail(CustomerEntity customer, string toEmail, Transaction transaction = null)
        {
            if (customer == null)
                throw new ObymobiException(SendEmailConfirmationResult.EmailAddressUnknown, "Email address: {0}", toEmail);
            else if (customer.Blacklisted)
                throw new ObymobiException(SendEmailConfirmationResult.Blacklisted, "Account is disabled: {0}", customer.BlacklistedNotes);
            else if (customer.AnonymousAccount)
                throw new ObymobiException(SendEmailConfirmationResult.NoConfirmationForAnonymousAccounts);
            else if (!Dionysos.Text.RegularExpressions.RegEx.IsEmail(toEmail))
                throw new ObymobiException(SendEmailConfirmationResult.EmailAddressIncorrectFormat, "Email address: {0}", customer.Email);
            else if (customer.InitialLinkIdentifier.IsNullOrWhiteSpace() && customer.NewEmailAddressConfirmationGuid.IsNullOrWhiteSpace())
                throw new ObymobiException(SendEmailConfirmationResult.NoInitialLinkIdentifierOrNewEmailGuidSet, "Email address: {0}", customer.Email);

            if (transaction == null) transaction = new Transaction(System.Data.IsolationLevel.ReadCommitted, customer.CustomerId.ToString() + "ConfirmCustomer");

            try
            {
                transaction.Add(customer);

                const int timeout = 30000;

                var mailUtil = new MailUtilV2(new System.Net.Mail.MailAddress("no-reply@crave-emenu.com", "Crave Mobile"), new System.Net.Mail.MailAddress(toEmail), "Confirm your e-mail address");
                mailUtil.SmtpClient.Timeout = timeout;
                mailUtil.ImagesSrcRootPath = "~/Templates";

                string templateBody = System.Web.Hosting.HostingEnvironment.MapPath("~/Templates/ConfirmRegistration.html");
                string emailBody = File.ReadAllText(templateBody);
                mailUtil.BodyHtml = emailBody;

                string initialLink = customer.InitialLinkIdentifier;
                if (!customer.NewEmailAddressConfirmationGuid.IsNullOrWhiteSpace()) initialLink = customer.NewEmailAddressConfirmationGuid;

                // Prepare variables to merge with
                StringDictionary templateVariables = new StringDictionary();
                templateVariables.Add("CONFIRMATIONLINK", StringUtil.FormatSafe("{0}?a={1}", WebEnvironmentHelper.GetApiUrl("App/ConfirmEmail.aspx"), initialLink));
                mailUtil.SendMail(templateVariables).Wait(timeout);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
            finally
            {
                transaction.Dispose();
            }
        }

        /// <summary>
        /// Changes the email for customer, causing an email to be send to the customer for a new verification.
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="newEmail"></param>
        public static void ChangeEmail(CustomerEntity customer, string newEmailAddress)
        {
            // Verify customer entity            
            //if (!customer.Verified)
            //{
            //    throw new ObymobiException(CustomerHelperWebResult.CustomerNotVerified);
            //}
            if (customer.Email.Equals(newEmailAddress))
            {
                throw new ObymobiException(CustomerHelperWebResult.NewEmailEqualsOldEmail);
            }
            else
            {
                customer.Email = newEmailAddress;
                customer.Save();
                
                // New transaction
                //Transaction transaction = new Transaction(System.Data.IsolationLevel.ReadCommitted, customer.CustomerId.ToString() + "ChangeCustomerEmail");

                // Set new email + Guid for verification
                //customer.NewEmailAddress = newEmailAddress;
                //customer.NewEmailAddressConfirmationGuid = Guid.NewGuid().ToString();

                // Send the confirmation email for the new email address
                //CustomerHelperWeb.SendConfirmationEmail(customer, newEmailAddress, transaction);
            }
        }

        /// <summary>
        /// Sets the Password Reset Link Identifier for the Customer
        /// We use this, instead of sending a new generated password, because this way
        /// we will never reset a password if the user didn't request it himself.
        /// </summary>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="byEmailFirst">if set to <c>true</c> [by email first].</param>
        /// <param name="byEmail">if set to <c>true</c> [by email].</param>
        /// <param name="byPhone">if set to <c>true</c> [by phone].</param>
        /// <exception cref="ObymobiException">
        /// CustomerId: '{0}', Phonenumber: '{1}'.
        /// or
        /// </exception>
        public static void ProcessResetPasswordRequest(string emailAddress)
        {
            CustomerEntity customer = null;

            CustomerCollection customers = CustomerHelper.GetCustomersByEmail(emailAddress);
            if (customers.Count == 0)
                throw new ObymobiException(RequestPasswordResetResult.EmailAddressUnknown, "Email address: {0}", emailAddress);
            else if (customers.Count > 1)
                throw new ObymobiException(RequestPasswordResetResult.MultipleCustomersWithEmailAddress, "Email address: {0}", emailAddress);
            else
                customer = customers[0];

            if (customer.Blacklisted)
                throw new ObymobiException(RequestPasswordResetResult.Blacklisted, "Account is disabled: {0}", customer.BlacklistedNotes);
            else if (customer.AnonymousAccount)
                throw new ObymobiException(RequestPasswordResetResult.NoPasswordResetForAnonymousAccounts);
            else if (!Dionysos.Text.RegularExpressions.RegEx.IsEmail(customer.Email))
                throw new ObymobiException(RequestPasswordResetResult.EmailAddressIncorrectFormat, "Email address: {0}", customer.Email);
            else if ((WebEnvironmentHelper.CloudEnvironmentIsProduction || TestUtil.IsPcGabriel) && customer.PasswordResetLinkGeneratedUTC.HasValue && (DateTime.UtcNow - customer.PasswordResetLinkGeneratedUTC.Value).TotalMinutes < 10)
                throw new ObymobiException(RequestPasswordResetResult.TooManyPasswordResetRequests, "Last request: {0}", customer.PasswordResetLinkGeneratedUTC);

            Transaction dbTransaction = new Transaction(System.Data.IsolationLevel.ReadCommitted, customer.CustomerId.ToString() + "ResetCustomerPassword");
            // Save using the transaction, if failed rollback
            try
            {
                dbTransaction.Add(customer);

                customer.PasswordResetLinkGeneratedUTC = DateTime.UtcNow;
                customer.PasswordResetLinkIdentifier = GuidUtil.CreateGuid();
                customer.PasswordResetLinkFailedAtttempts = 0;
                customer.Save();

                string emailToUse = (customer.NewEmailAddress.Equals(emailAddress, StringComparison.InvariantCultureIgnoreCase) ? customer.NewEmailAddress : customer.Email);

                const int timeout = 30000;

                var mailUtil = new MailUtilV2(new System.Net.Mail.MailAddress("no-reply@crave-emenu.com", "Crave World"), new System.Net.Mail.MailAddress(emailToUse), "Reset your password");
                mailUtil.SmtpClient.Timeout = timeout;
                mailUtil.ImagesSrcRootPath = "~/Templates";

                string templateBody = System.Web.Hosting.HostingEnvironment.MapPath("~/Templates/ForgotPassword.html");
                string emailBody = File.ReadAllText(templateBody);
                mailUtil.BodyHtml = emailBody;

                // Prepare variables to merge with
                StringDictionary templateVariables = new StringDictionary();
                templateVariables.Add("RESETLINK", StringUtil.FormatSafe("{0}?a={1}&b={2}", WebEnvironmentHelper.GetApiUrl("App/ResetPassword.aspx"), customer.CustomerId, customer.PasswordResetLinkIdentifier));
                mailUtil.SendMail(templateVariables).Wait(timeout);

                dbTransaction.Commit();
            }
            catch (Exception ex)
            {
                dbTransaction.Rollback();
                throw ex;
            }
            finally
            {
                dbTransaction.Dispose();
            }
        }

    }
}
