﻿using Dionysos;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Status;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Obymobi.Web.HelperClasses
{
    public static class MediaRatioTypeMediaHelperWeb
    {
        public static void ResetMediaRatioTypeMediaFiles(MediaCollection medias, bool forceSave = false, bool preventTimestampUpdates = false, bool consistencyCheck = false)
        {
            if (TestUtil.IsPcGabriel || TestUtil.IsPcMathieu)
            {
                // Don't do multiple thread for easier debugging
                if (!consistencyCheck)
                    MediaRatioTypeMediaHelperWeb.ProcessMedias(medias.ToList(), forceSave, preventTimestampUpdates);
                else
                    MediaRatioTypeMediaHelperWeb.CheckMedias(medias.ToList(), preventTimestampUpdates);
            }
            else
            {
                // Split work in 4
                int mediaPerBatch = Convert.ToInt32(System.Math.Ceiling(decimal.Divide(medias.Count, 4)));

                List<List<MediaEntity>> batches = new List<List<MediaEntity>>();
                while (medias.Count > 0)
                {
                    int amountInBatch = medias.Count > mediaPerBatch ? mediaPerBatch : medias.Count;
                    List<MediaEntity> batch = new List<MediaEntity>();
                    int added = 0;
                    while (added < amountInBatch)
                    {
                        batch.Add(medias[0]);
                        medias.RemoveAt(0);
                        added++;
                    }
                    batches.Add(batch);
                }

                List<Thread> threads = new List<Thread>();
                foreach (var batch in batches)
                {
                    // GK - For now a single thread without resizing is the solution
                    Thread t = null;

                    if (!consistencyCheck)
                        t = new Thread(() => ProcessMedias(batch, forceSave, preventTimestampUpdates));
                    else
                        t = new Thread(() => CheckMedias(batch, preventTimestampUpdates));

                    t.Priority = ThreadPriority.BelowNormal;
                    t.Start();

                    // Keep this method sync, thread is only to be able to set a lower priority
                    threads.Add(t);
                }

                foreach (var thread in threads)
                    thread.Join();
            }

            //Parallel.ForEach(medias, media =>
            //{                
            //    if (media.MediaFileEntity.File == null || media.MediaFileEntity.File.Length <= 0)
            //        return;

            //    // Set Thread Variable to prevent Authorization
            //    if (Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable) == null)
            //        Thread.AllocateNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable);
            //    Thread.SetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable), true);

            //    // Get a stream from the original media file
            //    using (MemoryStream memoryStream = new MemoryStream(media.MediaFileEntity.File))
            //    {
            //        byte[] imageData = new byte[memoryStream.Length];
            //        memoryStream.Read(imageData, 0, (int)memoryStream.Length);

            //        // Walk through the media ratio type media entities
            //        foreach (var mrtm in media.MediaRatioTypeMediaCollection)
            //        {
            //            using (var ms = new System.IO.MemoryStream((byte[])imageData.Clone()))
            //            {
            //                using (var image = Image.FromStream(ms))
            //                {
            //                    try
            //                    {
            //                        var size = image.Size;
            //                    }
            //                    catch
            //                    {
            //                        Debug.WriteLine("Failed loading image for: " + media.MediaId);
            //                    }

            //                    memoryStream.Close();
            //                    int width = image.Size.Width;

            //                    MediaRatioTypeMediaHelper.ResizeAndPublishFile(mrtm, image, true, forceSave);
            //                }
            //            }
            //        }
            //    }

            //    // Reset Thread Variable to prevent Authorization (not thread safe)
            //    // Thread.FreeNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable);
            //});
        }

        private static void ProcessMedias(List<MediaEntity> medias, bool forceSave, bool preventTimestampUpdates)
        {
            // Set Thread Variable to prevent Authorization
            if (Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable) == null)
                Thread.AllocateNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable);
            Thread.SetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable), true);

            // Prevent timestamp updates
            if (preventTimestampUpdates)
            {
                if (Thread.GetNamedDataSlot(ObymobiConstants.GeneralValidatorDisableUpdateVersions) == null)
                    Thread.AllocateNamedDataSlot(ObymobiConstants.GeneralValidatorDisableUpdateVersions);
                Thread.SetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralValidatorDisableUpdateVersions), true);
            }


            for (int i = 0; i < medias.Count; i++)
            {
                var media = medias[i];

                // Walk through the media ratio type media entities
                foreach (var mrtm in media.MediaRatioTypeMediaCollection)
                {
                    if (mrtm.MediaRatioTypeMediaFileEntity == null || mrtm.MediaRatioTypeMediaFileEntity.IsNew)
                        continue;

                    MediaRatioTypeMediaFileEntity mediaRatioFile = mrtm.MediaRatioTypeMediaFileEntity;
                    if (mediaRatioFile == null)
                        continue;

                    if (!forceSave)
                    {
                        if (mrtm.GetAmazonCdnStatus() == MediaRatioTypeMediaEntity.CdnStatus.UpToDate)
                        {
                            // No need.
                            continue;
                        }                        
                    }

                    mediaRatioFile.MediaProcessingTaskPriority = 25; // Default is 50, so a bit lower.
                    MediaHelper.QueueMediaRatioTypeMediaFileTask(mediaRatioFile, MediaProcessingTaskEntity.ProcessingAction.Upload, true, null);
                }
            }
        }

        private static void CheckMedias(List<MediaEntity> medias, bool preventTimestampUpdates)
        {
            // Set Thread Variable to prevent Authorization
            if (Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable) == null)
                Thread.AllocateNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable);
            Thread.SetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable), true);

            // Prevent timestamp updates
            if (preventTimestampUpdates)
            {
                if (Thread.GetNamedDataSlot(ObymobiConstants.GeneralValidatorDisableUpdateVersions) == null)
                    Thread.AllocateNamedDataSlot(ObymobiConstants.GeneralValidatorDisableUpdateVersions);
                Thread.SetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralValidatorDisableUpdateVersions), true);
            }


            for (int i = 0; i < medias.Count; i++)
            {
                var media = medias[i];

                // Walk through the media ratio type media entities
                foreach (var mrtm in media.MediaRatioTypeMediaCollection)
                {
                    if (mrtm.MediaRatioTypeMediaFileEntity == null || mrtm.MediaRatioTypeMediaFileEntity.IsNew)
                        continue;

                    MediaRatioTypeMediaFileEntity mediaRatioFile = mrtm.MediaRatioTypeMediaFileEntity;
                    if (mediaRatioFile == null)
                        continue;

                    bool outdated = false;
                    bool present = false;


                    if (mrtm.GetAmazonCdnStatus() != MediaRatioTypeMediaEntity.CdnStatus.UpToDate)
                    {
                        // One of the files is not up-to-date,
                        // these are skipped in this consistency check
                        // because they are uploaded using the upload function
                        outdated = true;
                    }


                    if (!outdated)
                    {
                        // Check whether the file is present on the CDN
                        foreach (string cdnUrl in WebEnvironmentHelper.GetCdnUrls(mrtm.GetFileName(MediaRatioTypeMediaEntity.FileNameType.Cdn)))
                        {
                            try
                            {
                                WebRequest request = WebRequest.Create(new Uri(cdnUrl));
                                request.Proxy = null;
                                request.Method = "HEAD";

                                using (WebResponse response = request.GetResponse())
                                {
                                    present = true;
                                }
                            }
                            catch { }
                        }
                    }

                    if (outdated || !present)
                    {
                        mediaRatioFile.MediaProcessingTaskPriority = 25; // Default is 50, so a bit lower.
                        MediaHelper.QueueMediaRatioTypeMediaFileTask(mediaRatioFile, MediaProcessingTaskEntity.ProcessingAction.Upload, true, null);
                    }
                }
            }
        }
    }
}
