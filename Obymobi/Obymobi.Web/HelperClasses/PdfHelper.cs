﻿using Dionysos.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Foxit.PDF;
using Foxit.PDF.Rasterizer;
using Obymobi.Data.EntityClasses;
using Dionysos.Drawing;
using System.Drawing;
using System.Drawing.Drawing2D;
using Obymobi.Logic.HelperClasses;
using Obymobi.Enums;
using Obymobi;
using System.Drawing.Imaging;
using Dionysos;
using Obymobi.Data;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Drawing;

/// <summary>
/// Summary description for PdfConverter
/// </summary>
public class PdfHelper
{
    private const string SessionKey = "PdfConverter.SessionKey";
    private static bool licenseValid = Foxit.PDF.Rasterizer.PdfRasterizer.AddLicense("FRS10NXSMP7CEDGnOTDF1rQ/Egq5FVCk6dAGvyLgQktAy/enN4r1CypqnIkRwQ7OQCH2f+MwsULZ1Q0ybZZf34kFucZi2EShfmjw");

    public enum PdfHelperResult
    { 
        PdfContainsZeroPages = 201,
    }

    private PdfHelper()
    {
    }

    /// <summary>
    /// This is not a Singleton, but single instance per Session class
    /// </summary>
    public static PdfHelper Instance
    {
        get
        {
            
            PdfHelper instance = SessionHelper.GetValue<PdfHelper>(PdfHelper.SessionKey);
            if (instance == null)
            {
                instance = new PdfHelper();
                SessionHelper.SetValue(PdfHelper.SessionKey, instance);
            }

            return instance;
        }
    }

    public void AttachPdfAsMedia(IMediaContainingEntity entity, string fileName, Stream pdfDocument, List<MediaRatioType> mediaRatioTypes, out int pages)
    {
        // Retrieve the Entity PK and PK Fieldname, so the entity can go out of scope during the processing.
        // these are used on the MediaEntity as FK.
        string fkFieldName = entity.PrimaryKeyFields[0].Name;
        int fkId = (int)entity.PrimaryKeyFields[0].CurrentValue;

        // Run in background.
        int sortOrder = 1;
        if(entity.MediaCollection.Count > 0)
            sortOrder = entity.MediaCollection.Max(x => x.SortOrder ?? 1);
        this.ProcessPdf(fkFieldName, fkId, fileName, sortOrder, pdfDocument, mediaRatioTypes, out pages);
    }

    public static byte[] GetFirstPageAsImage(byte[] pdf)
    {         
        var input = new InputPdf(pdf);
        var rasterizer = new Foxit.PDF.Rasterizer.PdfRasterizer(input);

        if (rasterizer.Pages.Count == 0)
            throw new Obymobi.ObymobiException(PdfHelperResult.PdfContainsZeroPages);

        byte[] toReturn = rasterizer.Pages[0].Draw(Foxit.PDF.Rasterizer.ImageFormat.Jpeg, Foxit.PDF.Rasterizer.ImageSize.Dpi96);

        return toReturn;            
    }

    private void ProcessPdf(string fkFieldName, int fkValue, string fileName, int sortOrderMax, Stream pdfDocument, List<MediaRatioType> mediaRatioTypes, out int pages)
    {
        MemoryStream bmpStream = null;
        Bitmap image = null;
        try
        {
            // Do PDF Magic.
            var input = new InputPdf(pdfDocument);
            var rasterizer = new Foxit.PDF.Rasterizer.PdfRasterizer(input);

            // JPG Quality Parameter
            ImageCodecInfo jpgEncoder = ImageCodecInfo.GetImageDecoders().FirstOrDefault(x => x.FormatID.Equals(System.Drawing.Imaging.ImageFormat.Jpeg.Guid));
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            EncoderParameters encoderParams = new EncoderParameters(1);

            EncoderParameter encoderParam = new EncoderParameter(myEncoder, 70L);
            encoderParams.Param[0] = encoderParam;

            // Get current highest Sort Order for Media of this entity


            // Add each page as a Media Entity            
            pages = rasterizer.Pages.Count;
            for (int i = 0; i < rasterizer.Pages.Count; i++)
            {
                var page = rasterizer.Pages[i];                

                // Create BMP at 1900 width      
                JpegImageFormat jpegformat = new JpegImageFormat(70);          
                byte[] bytes = page.Draw(jpegformat, Foxit.PDF.Rasterizer.ImageSize.Dpi150);                                
                bmpStream = new MemoryStream(bytes);
                image = new Bitmap(bmpStream);
                                                
                // Resize if required
                if (image.Size.Width > 4000 || image.Size.Height > 2000)
                {
                    // Crop is full area
                    Rectangle cropArea = new Rectangle(0, 0, image.Size.Width, image.Size.Height);
                    Size size = ImageUtil.GetFittingDimenions(image.Size.Width, image.Size.Height, 4000, 2000);

                    image = ImageUtil.Resize(image, cropArea, size, System.Drawing.Imaging.ImageFormat.Jpeg, Color.White);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        image.Save(ms, jpgEncoder, encoderParams);
                        bytes = ms.ToArray();
                    }
                }
                else
                { 
                    // Apply Jpeg compression
                    bytes = ImageUtil.ConvertToJpg(bytes, 70);
                }

                MediaEntity mediaEnt = new MediaEntity();
                mediaEnt.Name = Path.GetFileNameWithoutExtension(fileName) + " " + (i + 1);
                mediaEnt.Fields[fkFieldName].CurrentValue = fkValue;
                mediaEnt.FilePathRelativeToMediaPath = Path.GetFileNameWithoutExtension(fileName) + ".jpg"; // We convert it to a JPG.
                mediaEnt.MediaType = -1;
                mediaEnt.SetMimeTypeAndExtension(mediaEnt.FilePathRelativeToMediaPath);
                mediaEnt.SortOrder = sortOrderMax + i + 1;

                // Save the size of the image                
                if (bytes.Length > 0)
                    mediaEnt.SizeKb = Convert.ToInt32(bytes.Length) / 1024;

                // Save the media entity
                if (mediaEnt.Save())
                {
                    // Delete cached thumbnails                    
                    mediaEnt.Refetch();

                    // Get the MediaFile entity from the datasource
                    MediaFileEntity mediaFile = mediaEnt.MediaFileEntity;

                    // Get the bytes of the uploaded document
                    if (bytes.Length > 0)
                    {                                                
                        // Save the file
                        mediaFile.File = bytes;
                        mediaFile.Save();
                    }
                }    
                
                // Save MediaRatioTypes if any
                if (mediaRatioTypes != null)
                {
                    foreach (var mediaRatioType in mediaRatioTypes)
                    {
                        MediaRatioTypeMediaEntity mrtm = new MediaRatioTypeMediaEntity();
                        mediaEnt.MediaRatioTypeMediaCollection.Add(mrtm);
                        mrtm.MediaTypeAsEnum = mediaRatioType.MediaType;
                        mrtm.SetDefaultCrop(image);
                        mrtm.Save();
                        mrtm.ResizeAndPublishFile(true);
                    }
                }
            }
        }
        finally
        {
            if(bmpStream != null)
                bmpStream.Dispose();

            if(image != null)
                image.Dispose();
        }
    }

    public static byte[] MergeWithCoverPage(HttpPostedFile postedFile, ClientEntity client)
    {
        string roomCaption = !client.DeliverypointgroupEntity.DeliverypointCaption.IsNullOrWhiteSpace() ? client.DeliverypointgroupEntity.DeliverypointCaption : "Room";
        string roomAndNumberCaption = string.Format("{0} {1}", roomCaption, client.DeliverypointEntity.Number);
        
        string pagesCaption = "Pages";
        var defaultLanguages = client.DeliverypointgroupEntity.DeliverypointgroupLanguageCollection.Where(dpgl => dpgl.LanguageId == client.CompanyEntity.LanguageId).ToList();
        if (defaultLanguages.Count > 0 && !defaultLanguages[0].PagesCaption.IsNullOrWhiteSpace())
        {            
            pagesCaption = defaultLanguages[0].PagesCaption;
        }

        return PdfHelper.AddCoverPage(postedFile, roomAndNumberCaption, pagesCaption);
    }

    public static byte[] AddCoverPage(HttpPostedFile postedFile, string roomAndNumberCaption, string pagesCaption)
    {
        // Combined byte array
        byte[] combinedFile = null;

        // Pdf file        
        using (PdfDocument contentPdf = PdfReader.Open(HttpContext.Current.Request.Files[0].InputStream, PdfDocumentOpenMode.Import))
        using (PdfDocument newPdf = new PdfDocument())
        {
            string totalPages = string.Format("{0}: {1}", pagesCaption, contentPdf.PageCount);

            int width = 793;
            int height = 1122;

            Bitmap textImage = new Bitmap(width, height);

            StringFormat format = new StringFormat();
            format.Alignment = StringAlignment.Center;
            format.LineAlignment = StringAlignment.Center;

            Graphics g = Graphics.FromImage(textImage);
            g.SmoothingMode = SmoothingMode.AntiAlias;             
            g.DrawString(roomAndNumberCaption, new Font("Verdana", 18, FontStyle.Bold), Brushes.Black, new RectangleF(0, (height / 2) - 50, width, 50), format);
            g.DrawString(totalPages, new Font("Verdana", 12, FontStyle.Regular), Brushes.Black, new PointF((width / 2), (height / 2)), format);        
            g.Flush();

            XImage image = XImage.FromGdiPlusImage(textImage);

            // Front page
            PdfPage frontPage = new PdfPage();
            newPdf.AddPage(frontPage);

            XGraphics gfx = XGraphics.FromPdfPage(frontPage);
            gfx.SmoothingMode = XSmoothingMode.AntiAlias;
            gfx.DrawImage(image, 0, 0);            

            // Add the pages from the content pdf
            PdfHelper.CopyPages(contentPdf, newPdf);

            using (MemoryStream stream = new MemoryStream())
            {
                newPdf.Save(stream, true);
                combinedFile = stream.ToArray();
            }
        }

        return combinedFile;
    }

    private static void CopyPages(PdfDocument from, PdfDocument to)
    {
        for (int i = 0; i < from.PageCount; i++)
        {
            PdfPage toAdd = from.Pages[i];
            toAdd.Orientation = PdfSharp.PageOrientation.Landscape;

            to.AddPage(toAdd);            
        }
    }       
}