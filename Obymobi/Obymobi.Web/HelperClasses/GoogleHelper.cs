﻿using Dionysos.Web;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi;
using Dionysos;
using Obymobi.Web.Google;
using Obymobi.Logic.HelperClasses;

/// <summary>
/// Summary description for FileHelper
/// </summary>
public static class GoogleHelper
{
    public static bool PrintFile(ClientEntity client, string title, byte[] file)
    {         
        bool success = false;
        if (client != null)
        {
            if (client.DeliverypointId.HasValue && client.DeviceId.HasValue && client.DeliverypointEntity.DeviceId.HasValue)
            {
                if (client.DeviceId.Value != client.DeliverypointEntity.DeviceId.Value)
                {
                    // This client is not allowed to print to this printer, check the 'Print from' setting on the deliverypoint page in the CMS
                    throw new ObymobiException(PrintPdfResult.DeviceNotAllowedToPrint, "Device not allowed to print to this printer");
                }
            }

            // Get the printerId for the client
            string printerId = GoogleHelper.GetPrinterId(client);
            if (!printerId.IsNullOrWhiteSpace())
            {
                ApiAuthenticationEntity googleApiAuthenticationEntity = ApiAuthenticationHelper.GetApiAuthenticationEntityByType(ApiAuthenticationType.Google, client.CompanyId);
                if (googleApiAuthenticationEntity != null && !string.IsNullOrWhiteSpace(googleApiAuthenticationEntity.FieldValue1))
                {
                    string clientId = Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.GoogleClientId);
                    string clientSecret = Dionysos.ConfigurationManager.GetString(DionysosWebConfigurationConstants.GoogleClientSecret);

                    GoogleAPI google = new GoogleAPI(clientId, clientSecret, googleApiAuthenticationEntity.FieldValue1);
                    if (!google.Print(printerId, title, "data:application/pdf;base64," + Convert.ToBase64String(file), "dataUrl"))
                    {
                        throw new ObymobiException(GenericWebserviceCallResult.Failure, "File could not be printed");
                    }
                }
            }
        }        
        return success;
    }

    public static string GetPrinterId(ClientEntity client)
    {
        string printerId = string.Empty;
        if (client != null)
        {
            if (client.DeliverypointId.HasValue && !client.DeliverypointEntity.GooglePrinterId.IsNullOrWhiteSpace())
                printerId = client.DeliverypointEntity.GooglePrinterId;
            else if (client.DeliverypointGroupId.HasValue)
                printerId = client.DeliverypointgroupEntity.GooglePrinterId;
        }        
        return printerId;
    }
}