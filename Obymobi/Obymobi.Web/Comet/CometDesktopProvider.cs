﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Comet.Interfaces;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet
{
    /// <summary>
    /// CometDesktopProvider class for Web applications
    /// </summary>
	public class CometDesktopProvider : ICometProvider, ICometClientEventListener
    {
        // These are public enums, so why aren't they in separate files?
        #region Enums

        /// <summary>
		/// CometDesktopProviderResult enums
		/// </summary>
		public enum CometDesktopProviderResult
        {
            /// <summary>
            /// Not implemented for non httpApplications
            /// </summary>
            NotImplementedForNonHttpApplications = 200,
            /// <summary>
            /// Connection failed
            /// </summary>
            ConnectFailed = 201,
            /// <summary>
            /// Non-valid communication method has been supplied to CometDesktopProvider
            /// </summary>
            UnknownCommunicationMethod = 202,
        }

        public enum ConnectionState
        {
            Connected,
            Connecting,
            Reconnecting,
            Disconnected,
            Disconnecting
        }

        #endregion

        #region Fields

        private const int RECONNECT_INTERVAL = 5; // Seconds

        private CometClient cometClient;
        private readonly ClientCommunicationMethod communicationMethod;
        private readonly string appIdentifier;
        private readonly NetmessageClientType clientType;
        private readonly Dictionary<int, MessageHandlerContainer> messageHandlers = new Dictionary<int, MessageHandlerContainer>();

        protected readonly ReaderWriterLockSlim ConnectionLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        private ConnectionState connectionState;

        private bool isDestroy;
        private readonly object sendLock = new object();

        #endregion

        #region PingPong Checker

        readonly InfiniteLooper pingPongChecker = new InfiniteLooper(() =>
            {
                CometDesktopProvider provider = (CometDesktopProvider)CometHelper.GetCometProvider();
                TimeSpan timeout = provider.cometClient.IsAuthenticated ? new TimeSpan(0, 2, 0) : new TimeSpan(0, 1, 0);
                if (DateTime.UtcNow.Subtract(provider.cometClient.LastPong) >= timeout)
                {
                    provider.OnEventLog("No Ping/Pong for the last {0} seconds. Forcing disconnect.", timeout.TotalSeconds);

                    provider.SetConnectionState(ConnectionState.Disconnected);

                    if (provider.cometClient != null)
                        provider.cometClient.Dispose();
                }
            }, 30000, 30000, OnExceptionHandler);

        private static void OnExceptionHandler(Exception exception)
        {
            Debug.WriteLine("Ping/Pong Exception: {0}", exception.Message);
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="CometDesktopProvider"/> class.
        /// </summary>
        public CometDesktopProvider(ClientCommunicationMethod communicationMethod, string identifier, NetmessageClientType type)
        {
            this.cometClient = null;
            this.communicationMethod = communicationMethod;
            this.appIdentifier = identifier;
            this.clientType = type;
        }

        #endregion

        #region Public Methods

        public void AddHandler(int key, IMessageHandler handler, HttpContext context)
        {
            if (!this.messageHandlers.ContainsKey(key))
            {
                MessageHandlerContainer container = new MessageHandlerContainer();
                container.Context = context;
                container.Handler = handler;

                this.messageHandlers.Add(key, container);
            }
        }

        public void RemoveHandler(int key)
        {
            this.messageHandlers.Remove(key);

            if (this.messageHandlers.Count == 0)
            {
                //this.Destroy();
            }
        }

        public void Initialize()
        {
            OnEventLog("Initializing client '{0}'. cometClient is null: {1}", this.communicationMethod, (this.cometClient == null));

            if (this.cometClient != null)
            {
                this.cometClient.Dispose();
                this.cometClient = null;
            }

            OnEventLog("Setting connection state");
            SetConnectionState(ConnectionState.Connecting);

            if (this.communicationMethod == ClientCommunicationMethod.SignalR)
            {
                string signalRUrl = WebEnvironmentHelper.GetMessagingUrl();

                OnEventLog("Creating new SignalRClient - {0}", signalRUrl);
                this.cometClient = new SignalRClient(this, signalRUrl, appIdentifier, CometConstants.CometInternalSalt);
            }
            else if (this.communicationMethod == ClientCommunicationMethod.None)
            {
                string host = WebEnvironmentHelper.GetRedisHost();
                OnEventLog("Creating new MessagingRedisCometClient - {0}", host);
                this.cometClient = new MessagingRedisCometClient(this, host);
            }
            else
            {
                throw new ObymobiException(CometDesktopProviderResult.UnknownCommunicationMethod, "ClientCommunicationMethod '{0}' has nog been implemented in CometDesktopProvider.");
            }

            this.cometClient.Connect();
        }

        public void Destroy()
        {
            this.isDestroy = true;

            this.pingPongChecker.Stop();

            this.cometClient.Dispose();
            this.cometClient = null;
        }

        public bool IsConnectionReady()
        {
            return (GetConnectionState() == ConnectionState.Connected && this.cometClient != null && this.cometClient.IsAuthenticated && !isDestroy);
        }

        public void SendMessage(Netmessage netmessage)
        {
            lock (sendLock)
            {
                if (!IsConnectionReady() &&
                    GetConnectionState() != ConnectionState.Connecting &&
                    GetConnectionState() != ConnectionState.Reconnecting)
                {
                    Initialize();
                }

                if (this.cometClient != null)
                {
                    netmessage.Validate();
                    this.cometClient.SendMessage(netmessage);
                }
            }
        }

        #endregion

        #region Private Methods

        private void SetConnectionState(ConnectionState state)
        {
            bool reloadConnection = false;

            ConnectionLock.EnterWriteLock();
            try
            {
                if (connectionState != state)
                {
                    ConnectionState oldState = connectionState;
                    connectionState = state;

                    OnEventLog("CometDesktopProvider.SetConnectionState - Changing state from '{0}' to '{1}'", oldState, state);

                    if (state == ConnectionState.Connected && (oldState == ConnectionState.Connecting || oldState == ConnectionState.Reconnecting))
                    {
                        if (this.communicationMethod != ClientCommunicationMethod.None)
                        {
                            this.pingPongChecker.Start();
                        }
                    }
                    else if (state == ConnectionState.Disconnected && oldState != ConnectionState.Disconnecting)
                    {
                        reloadConnection = true;

                        if (this.pingPongChecker.IsRunning)
                            this.pingPongChecker.Stop();
                    }
                }
            }
            catch (Exception ex)
            {
                OnEventLog("SetConnectionState - Exception: " + ex.Message);
            }
            finally
            {
                ConnectionLock.ExitWriteLock();

                if (reloadConnection)
                {
                    ReloadConnection(RECONNECT_INTERVAL);
                }
            }
        }

        private ConnectionState GetConnectionState()
        {
            ConnectionLock.EnterWriteLock();
            ConnectionState ret = this.connectionState;
            ConnectionLock.ExitWriteLock();

            return ret;
        }

        private void ReloadConnection(int delay)
        {
            if (GetConnectionState() != ConnectionState.Reconnecting)
            {
                SetConnectionState(ConnectionState.Reconnecting);

                if (this.cometClient != null)
                {
                    this.cometClient.Dispose();
                    this.cometClient = null;
                }

                if (delay <= 0)
                {
                    OnEventLog("CometDesktopProvider.ReloadConnection - Reconnecting to CometServer", delay);
                    Initialize();
                }
                else
                {
                    OnEventLog("CometDesktopProvider.ReloadConnection - Reconnecting to CometServer in {0} seconds", delay);
                    Task.Factory.StartNew(() =>
                        {
                            OnEventLog("Commencing wait till initialize!");
                            try
                            {
                                Thread.Sleep(delay * 1000);
                            }
                            finally
                            {
                                Initialize();
                            }
                        });
                }
            }
        }

        #endregion

        public void OnConnectFailed()
        {
            OnEventLog("Failed to connect to server.");

            ReloadConnection(RECONNECT_INTERVAL);
        }

        public void OnConnectionChanged(bool isConnected)
        {
            if (isConnected)
            {
                SetConnectionState(ConnectionState.Connected);
            }
            else
            {
                if (GetConnectionState() == ConnectionState.Connected)
                {
                    SetConnectionState(ConnectionState.Disconnected);
                    ReloadConnection(RECONNECT_INTERVAL);
                }
                else if (GetConnectionState() == ConnectionState.Disconnecting)
                {
                    SetConnectionState(ConnectionState.Disconnected);
                }
            }
        }

        public void OnNetmessageReceived(Netmessage netmessage)
        {
            if (netmessage.GetMessageType() == NetmessageType.AuthenticateResult)
            {
                bool isAuthenticated;
                if (bool.TryParse(netmessage.FieldValue1, out isAuthenticated) && isAuthenticated)
                {
                    OnEventLog("Successfully authenticated. Sending client info.");

                    NetmessageSetClientType clientTypeMessage = new NetmessageSetClientType();
                    clientTypeMessage.ClientType = this.clientType;
                    this.cometClient.TransmitMessage(clientTypeMessage);

                    // This will also start the Netmessage procesing thread
                    this.cometClient.SetAuthenticated(true);
                }
                else
                {
                    this.cometClient.SetAuthenticated(false);
                    OnEventLog("Failed to autenticate. Message: {0}", netmessage.FieldValue2);
                }
            }
            else if (netmessage.GetMessageType() == NetmessageType.Ping)
            {
                if (this.cometClient.IsConnected())
                {
                    this.cometClient.Pong();
                }
            }
            else if (netmessage.GetMessageType() == NetmessageType.Disconnect)
            {
                if (this.cometClient.IsConnected())
                {
                    this.cometClient.Disconnect();
                }
            }
            else
            {
                foreach (MessageHandlerContainer messageHandler in messageHandlers.Values)
                {
                    if (messageHandler != null)
                    {
                        messageHandler.Handler.ReceivedMessage(netmessage, messageHandler.Context);
                    }
                }
            }
        }

        public void OnEventLog(string message, params object[] args)
        {
            if (args.Length > 0)
                message = string.Format(message, args);

            Debug.WriteLine("{0} - {1}.", this.appIdentifier, message);
            Log("{0} - {1}.", this.appIdentifier, message);
        }

        private static DateTime lastCleanup = DateTime.MinValue;
        private static readonly object LogLock = new object();
        private static void Log(string format, params object[] args)
        {
            string textToWrite = format.FormatSafe(args);
            textToWrite = DateTime.Now.ToString("[HH:mm:ss] ") + textToWrite;

            try
            {
                lock (LogLock)
                {
                    string fileName = "CometDesktopProvider-" + DateTime.Now.ToString("yyyyMMdd") + ".log";
                    string path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Logs/");
                    string filepath = Path.Combine(path, fileName);

                    using (StreamWriter writer = File.AppendText(filepath))
                    {
                        writer.Write(textToWrite);
                        writer.Write("\n");
                    }

                    if (lastCleanup.TotalSecondsToNow() > 600)
                    {
                        // Clean up
                        string[] files = Directory.GetFiles(path, "CometDesktopProvider-*");
                        foreach (string file in files)
                        {
                            FileInfo fi = new FileInfo(file);
                            if ((DateTime.Now - fi.LastWriteTime).Days > 14)
                                File.Delete(file);
                        }

                        lastCleanup = DateTime.Now;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Writing to log file failed:".FormatSafe(ex.Message));
            }
        }
    }
}