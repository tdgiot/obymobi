﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Amazon.ElasticMapReduce.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Obymobi.Logic.Comet.Interfaces;
using Obymobi.Logic.Model;
using ServiceStack.Redis;

namespace Obymobi.Logic.Comet
{
    public class MessagingRedisCometClient : CometClient
    {
        private readonly JsonSerializerSettings jsonSettingsCamelCase;
        private readonly string host;
        private BasicRedisClientManager redis;


        private BlockingCollection<NetmessageContainer> messageQueue = new BlockingCollection<NetmessageContainer>();

        public MessagingRedisCometClient(ICometClientEventListener eventListener, string host)
            : base(eventListener)
        {
            this.host = host;

            jsonSettingsCamelCase = new JsonSerializerSettings();
            jsonSettingsCamelCase.NullValueHandling = NullValueHandling.Ignore;
            jsonSettingsCamelCase.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }

        public override void Connect()
        {
            if (this.redis == null)
            {
                this.redis = new BasicRedisClientManager(this.host);
                using (IRedisClient client = this.redis.GetClient())
                {
                    client.GetClient();
                }

                SetConnected(true);
                IsAuthenticated = true;

                messageQueue = new BlockingCollection<NetmessageContainer>();

                Task.Factory.StartNew(ConsumeQueue, TaskCreationOptions.LongRunning);
            }
        }

        public override void Disconnect()
        {
            if (!this.messageQueue.IsAddingCompleted)
            {
                this.messageQueue.CompleteAdding();
            }

            if (this.redis != null)
            {
                SetConnected(false);

                this.redis = null;
            }
        }

        public override void Authenticate(string identifier, string salt)
        {
        }

        public override void Pong()
        {
        }

        public override void SetAuthenticated(bool isAuthenticated)
        {
        }

        public override void SendMessage(Netmessage message)
        {
            WriteToLog("SendMessage: {0}", message.MessageType);
            TransmitMessage(message);
        }

        public override void TransmitMessage(Netmessage message)
        {
            if (string.IsNullOrWhiteSpace(message.ReceiverIdentifier))
            {
                WriteToLog("Unable to transmit message. No receiver identifier specified.");
                return;
            }

            WriteToLog("Creating container: {0}", message.ToString());

            string jsonData = null;
            try
            {
                jsonData = JsonConvert.SerializeObject(message, this.jsonSettingsCamelCase);
            }
            catch (Exception e)
            {
                WriteToLog("Exception while serializing netmessage: {0}", e.ToString());
            }

            if (jsonData == null) 
                return;

            try
            {
                NetmessageContainer container = new NetmessageContainer
                {
                    Instance = "Extern",
                    Guid = string.IsNullOrWhiteSpace(message.Guid) ? Guid.NewGuid().ToString("D") : message.Guid,
                    Type = message.MessageType,
                    Receiver = message.ReceiverIdentifier,
                    Sender = message.SenderIdentifier,
                    Data = jsonData
                };

                WriteToLog("Adding to queue: {0}", container.ToString());

                this.messageQueue.Add(container);
            }
            catch (Exception e)
            {
                WriteToLog("Exception while transmitting: {0}", e.ToString());
            }
        }

        private void ConsumeQueue()
        {
            WriteToLog("ConsumeQueue Starting");

            while (!this.messageQueue.IsAddingCompleted)
            {
                NetmessageContainer container;
                if (this.messageQueue.TryTake(out container, -1) && container != null)
                {
                    try
                    {
                        string json = JsonConvert.SerializeObject(container);
                        WriteToLog("ConsumeQueue - Sending message: {0}", json);
                        using (IRedisClient client = this.redis.GetClient())
                        {
                            client.PublishMessage("scaleout:SendMessage", json);
                        }
                    }
                    catch (Exception ex)
                    {
                        WriteToLog("ConsumeQueue Exception: {0}", ex.Message);
                    }
                }
            }

            WriteToLog("ConsumeQueue Finished");
        }
    }
}