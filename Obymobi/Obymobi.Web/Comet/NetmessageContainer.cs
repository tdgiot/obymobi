﻿using System.Text;
using Obymobi.Enums;

namespace Obymobi.Logic.Comet
{
    /// <summary>
    /// Netmessage wrapper containing receiver information and the actual data to send
    /// </summary>
    public class NetmessageContainer
    {
        /// <summary>
        /// Unique identifier to verify message on sender
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// Gets or sets the sender of this netmessage
        /// </summary>
        public string Sender { get; set; }

        /// <summary>
        /// Gets or sets the receiver
        /// </summary>
        public string Receiver { get; set; }

        /// <summary>
        /// Gets or sets the group names to which to send this message
        /// </summary>
        public string[] Groups { get; set; }

        /// <summary>
        /// Netmessage type <see cref="NetmessageType"/>
        /// </summary>
        //[JilDirective(TreatEnumerationAs = typeof(int))]
        public NetmessageType Type { get; set; }

        /// <summary>
        /// Data to send to the receiver
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Server instance name from which message is send (only used for scale out messages). If this value is set messaging instances will not push this message to the scale-out back-plane
        /// </summary>
        public string Instance { get; set; }

        /// <inheritdoc />
        public override string ToString()
        {
            return new StringBuilder()
                   .AppendFormat("Sender={0}", this.Sender)
                   .AppendFormat("Receiver={0}", this.Receiver)
                   .AppendFormat("Groups={0}", this.Groups != null ? string.Join(",", this.Groups) : string.Empty)
                   .AppendFormat("Type={0}", this.Type)
                   .AppendFormat("Data={0}", this.Data)
                   .ToString();
        }
    }
}