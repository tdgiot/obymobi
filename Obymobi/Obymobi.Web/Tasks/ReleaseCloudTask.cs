﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Dionysos;
using Dionysos.Logging;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Web.Tasks
{
    public class ReleaseCloudTask : CloudBaseTask
    {
        private readonly ILoggingProvider loggingProvider;

        public ReleaseCloudTask(ILoggingProvider loggingProvider, CloudProcessingTaskEntity task) : base(CloudProcessingTaskType.Release, task)
        {
            this.loggingProvider = loggingProvider;
        }

        public override void Upload(IEnumerable<CloudStorage.CloudStorageProvider> storageProviders)
        {
            IsUploaded = false;
            if (TaskEntity.ReleaseId.HasValue)
            {
                ReleaseEntity releaseEntity = TaskEntity.ReleaseEntity;
                if (!releaseEntity.IsNew && releaseEntity.File != null)
                {
                    Log("ReleaseId {0}, File: {1}", releaseEntity.ReleaseId, releaseEntity.File.Length);


                    using (MemoryStream stream = new MemoryStream(releaseEntity.File))
                    {
                        foreach (CloudStorage.CloudStorageProvider provider in storageProviders)
                        {
                            try
                            {
                                provider.WriteFile(TaskEntity, stream, null);
                                IsUploaded = true;
                                Log("Success");
                            }
                            catch (Exception ex)
                            {
                                Log("Failed to upload: {0}", ex.Message);
                                AddError("Failed to upload: {0}", ex.Message);
                            }
                        }
                    }

                    if (IsUploaded)
                    {
                        Log("Emptying file");
                        releaseEntity.File = null;
                        releaseEntity.Save();
                    }
                }
                else
                {
                    AddError("Release {0} does not exist or does not have file linked.", releaseEntity.ReleaseId);
                }
            }
            else
            {
                AddError("No release linked to this task.");
            }
        }

        private void Log(string format, params object[] args)
        {
            Debug.WriteLine(format, args);
            this.loggingProvider.Verbose(format.FormatSafe(args));
        }
    }
}