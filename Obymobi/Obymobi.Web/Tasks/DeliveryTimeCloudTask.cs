﻿using System;
using System.Collections.Generic;
using System.IO;
using Google.Apis.Manual.Util;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Web.Tasks
{
    public class DeliveryTimeCloudTask : CloudBaseTask
    {
        public DeliveryTimeCloudTask(CloudProcessingTaskEntity task) : base(CloudProcessingTaskType.DeliveryTime, task)
        {
        }

        public override void Upload(IEnumerable<CloudStorage.CloudStorageProvider> storageProviders)
        {
            IsUploaded = false;
            if (!TaskEntity.FileContent.IsNullOrEmpty())
            {
                foreach (CloudStorage.CloudStorageProvider provider in storageProviders)
                {
                    try
                    {
                        provider.WriteFile(TaskEntity, TaskEntity.FileContent, null);
                        IsUploaded = true;
                    }
                    catch (Exception ex)
                    {
                        AddError("Failed to upload: {0}", ex.Message);
                    }
                }
            }
            else
            {
                AddError("No delivery time set for this task.");
            }
        }
    }
}