﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.CloudStorage;
using System;
using System.Collections.Generic;

namespace Obymobi.Web.Tasks
{
    public class CloudBaseTask
    {
        public CloudProcessingTaskType TaskType { get; private set; }
        public CloudProcessingTaskEntity TaskEntity { get; private set; }
        public bool IsCanceled { get; private set; }
        public bool IsDeleted { get; protected set; }
        public bool IsUploaded { get; protected set; }

        public CloudBaseTask(CloudProcessingTaskType type, CloudProcessingTaskEntity task)
        {
            TaskType = type;
            TaskEntity = task;
            IsCanceled = false;
        }

        public virtual void Upload(IEnumerable<CloudStorageProvider> storageProviders)
        {
            throw new NotImplementedException(string.Format("Upload method has not been implemented for type '{0}'", TaskType));
        }

        public virtual void Delete(IEnumerable<CloudStorageProvider> storageProviders)
        {
            IsDeleted = true;
            foreach (CloudStorageProvider storageProvider in storageProviders)
            {
                try
                {
                    storageProvider.DeleteFile(TaskEntity);
                }
                catch (Exception ex)
                {
                    IsDeleted = false;
                    AddError("Failed to delete. Exception: {0}", ex.Message);
                }
            }
        }

        public void Cancel(string message = "", params object[] args)
        {
            IsCanceled = true;
            TaskEntity.Attempts = 9000;
            if (message.Length > 0)
                AddError(message, args);
        }

        public void AddError(string format, params object[] args)
        {
            TaskEntity.Errors += string.Format(format, args) + Environment.NewLine;
        }
    }
}
