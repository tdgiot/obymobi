﻿using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.CloudStorage;
using System;

namespace Obymobi.Web.Tasks
{
    public class WeatherCloudTask : CloudBaseTask
    {
        public WeatherCloudTask(CloudProcessingTaskEntity task) : base(CloudProcessingTaskType.Weather, task)
        {
        }

        public override void Upload(IEnumerable<CloudStorageProvider> storageProviders)
        {
            if (TaskEntity.WeatherId.HasValue)
            {
                foreach (var provider in storageProviders)
                {
                    try
                    {
                        provider.WriteFile(TaskEntity, TaskEntity.WeatherEntity.LastWeatherData, null);
                        IsUploaded = true;
                    }
                    catch (Exception ex)
                    {
                        AddError("Failed to upload: {0}", ex.Message);
                    }
                }

                if (IsUploaded)
                {
                    TaskEntity.WeatherEntity.Filename = TaskEntity.PathFormat;
                    TaskEntity.WeatherEntity.Save();
                }
            }
            else
            {
                AddError("This CloudTask doesn't have any weather linked to it.");
            }
        }
    }
}
