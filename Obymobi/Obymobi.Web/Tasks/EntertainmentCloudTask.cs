﻿using System;
using System.Collections.Generic;
using System.IO;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.CloudStorage;

namespace Obymobi.Web.Tasks
{
    public class EntertainmentCloudTask : CloudBaseTask
    {
        public EntertainmentCloudTask(CloudProcessingTaskEntity task) : base(CloudProcessingTaskType.Entertainment, task)
        {
        }

        public override void Upload(IEnumerable<CloudStorageProvider> storageProviders)
        {
            IsUploaded = false;
            if (TaskEntity.EntertainmentFileId != null && TaskEntity.EntertainmentFileEntity != null)
            {
                var entertainmentEntity = TaskEntity.EntertainmentFileEntity;
                if (entertainmentEntity.Blob.Length != 0)
                {
                    var stream = new MemoryStream(entertainmentEntity.Blob);

                    foreach (var provider in storageProviders)
                    {
                        try
                        {
                            provider.WriteFile(TaskEntity, stream, null);
                            IsUploaded = true;
                        }
                        catch (Exception ex)
                        {
                            AddError("Failed to upload: {0}", ex.Message);
                        }
                    }
                }
                else
                {
                    AddError("EntertainmentFile with id '{0}' does not have a file assigned");
                }
            }
            else
            {
                AddError("No entertainment linked to this task.");
            }
        }

        public override void Delete(IEnumerable<CloudStorageProvider> storageProviders)
        {
            base.Delete(storageProviders);

            if (IsDeleted && TaskEntity.EntertainmentFileId.HasValue)
            {
                var fileEntity = new EntertainmentFileEntity(TaskEntity.EntertainmentFileId.Value);
                fileEntity.Delete();

                AddError("Success: Entertainment removed from cloud and database");
            }
        }
    }
}
