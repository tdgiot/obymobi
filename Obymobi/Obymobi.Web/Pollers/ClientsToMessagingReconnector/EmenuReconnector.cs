﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dionysos.Logging;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Web.Pollers.ClientsToMessagingReconnector
{
    public class EmenuReconnector : BaseReconnector
    {
        private const int BATCH_AMOUNT = 100;

        public EmenuReconnector(ILoggingProvider loggingProvider) : base(loggingProvider)
        {
            
        }

        public override void GetClients()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeviceFields.CommunicationMethod == ClientCommunicationMethod.Webservice);
            filter.Add(ClientFields.DeviceId > 0);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));

            SortExpression sort = new SortExpression(new SortClause(ClientFields.ClientId, SortOperator.Ascending));

            RelationCollection relation = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            this.clients = new ClientCollection();
            this.clients.GetMulti(filter, 0, sort, relation);
        }

        public override void Reconnect()
        {
            this.loggingProvider.Information("Reconnecting {0} Emenu's to signalR", this.clients.Count);

            for (int i = 0; i < this.clients.Count; i = i + EmenuReconnector.BATCH_AMOUNT)
            {
                this.loggingProvider.Information("Batch {0}", (i / EmenuReconnector.BATCH_AMOUNT) + 1);
                List<ClientEntity> batch = this.clients.Skip(i).Take(EmenuReconnector.BATCH_AMOUNT).ToList();
                this.SwitchClientsToWebservice(batch);
                Thread.Sleep(30000);
                this.SwitchClientsToSignalR(batch);
            }

            this.loggingProvider.Information("Finished reconnecting Emenu's");
        }

        private void SwitchClientsToWebservice(List<ClientEntity> batch)
        {
            foreach (ClientEntity client in batch)
            {
                this.loggingProvider.Information("Switching Emenu with client id '{0}' to webservice", client.ClientId);
                this.SwitchClientToWebservice(client);
            }
        }

        private void SwitchClientsToSignalR(List<ClientEntity> batch)
        {
            foreach (ClientEntity client in batch)
            {
                this.loggingProvider.Information("Switching Emenu with client id '{0}' to signalR", client.ClientId);
                this.SwitchClientToSignalR(client);
            }
        }

        private void SwitchClientToWebservice(ClientEntity client)
        {
            NetmessageSwitchCometHandler netmessage = new NetmessageSwitchCometHandler();
            netmessage.ReceiverClientId = client.ClientId;
            netmessage.Handler = ClientCommunicationMethod.Webservice;

            CometHelper.Instance.SendMessage(netmessage);
        }

        private void SwitchClientToSignalR(ClientEntity client)
        {
            NetmessageSwitchCometHandler netmessage = new NetmessageSwitchCometHandler();
            netmessage.ReceiverClientId = client.ClientId;
            netmessage.Handler = ClientCommunicationMethod.SignalR;

            CometHelper.Instance.SendMessage(netmessage);
        }
    }
}
