﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Logging;
using Obymobi.Data.CollectionClasses;

public abstract class BaseReconnector
{
    protected ILoggingProvider loggingProvider;
    protected ClientCollection clients;

    public BaseReconnector(ILoggingProvider loggingProvider)
    {
        this.loggingProvider = loggingProvider;
    }

    public abstract void GetClients();
    public abstract void Reconnect();
}