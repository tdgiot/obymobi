﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos.Logging;
using Obymobi.Web.Pollers.ClientsToMessagingReconnector;

public class ClientsToMessagingReconnector
{
    private readonly List<BaseReconnector> reconnectors;

    public ClientsToMessagingReconnector(ILoggingProvider loggingProvider)
    {
        this.reconnectors = new List<BaseReconnector>();
        this.reconnectors.Add(new EmenuReconnector(loggingProvider));
        this.reconnectors.Add(new SupportToolsReconnector(loggingProvider));
    }

    public void Reconnect()
    {
        foreach (BaseReconnector reconnector in this.reconnectors)
        {
            reconnector.GetClients();
            reconnector.Reconnect();
        }
    }    
}