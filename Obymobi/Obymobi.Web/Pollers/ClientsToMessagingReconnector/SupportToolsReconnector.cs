﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Dionysos.Logging;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Comet.Netmessages;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Web.Pollers.ClientsToMessagingReconnector
{
    public class SupportToolsReconnector : BaseReconnector
    {
        private const int BATCH_AMOUNT = 100;

        public SupportToolsReconnector(ILoggingProvider loggingProvider) : base(loggingProvider)
        {
            
        }

        public override void GetClients()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMinutes(-3));
            filter.Add(DeviceFields.LastSupportToolsRequestUTC < DateTime.UtcNow.AddMinutes(-3));
            filter.Add(ClientFields.DeviceId > 0);

            SortExpression sort = new SortExpression(new SortClause(ClientFields.ClientId, SortOperator.Ascending));

            RelationCollection relation = new RelationCollection(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            this.clients = new ClientCollection();
            this.clients.GetMulti(filter, 0, sort, relation);
        }

        public override void Reconnect()
        {
            this.loggingProvider.Information("Reconnecting total of {0} SupportTools to signalR", this.clients.Count);

            for (int i = 0; i < this.clients.Count; i = i + SupportToolsReconnector.BATCH_AMOUNT)
            {
                this.loggingProvider.Information("Batch {0}", (i / SupportToolsReconnector.BATCH_AMOUNT) + 1);
                List<ClientEntity> batch = this.clients.Skip(i).Take(SupportToolsReconnector.BATCH_AMOUNT).ToList();
                this.SwitchClients(batch);                
                Thread.Sleep(30000);
            }

            this.loggingProvider.Information("Finished reconnecting SupportTools");
        }

        private void SwitchClients(List<ClientEntity> batch)
        {
            foreach (ClientEntity client in batch)
            {
                this.loggingProvider.Information("Switching ST with client id '{0}' to signalR", client.ClientId);
                this.SwitchClient(client);
            }
        }

        private void SwitchClient(ClientEntity client)
        {
            NetmessageDeviceCommandExecute netmessage = new NetmessageDeviceCommandExecute();
            netmessage.ReceiverClientId = client.ClientId;
            netmessage.Command = "am force-stop net.craveinteractive.supporttools";

            CometHelper.Instance.SendMessage(netmessage);
        }
    }
}
