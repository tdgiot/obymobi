﻿using System.Collections.Generic;
using System.Net;
using Dionysos.Net;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Dionysos;
using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using Obymobi.Logic.HelperClasses;
using System.IO;

namespace Obymobi.Logic.Status
{
    public class ConnectionStringPoller
    {
        private static ConnectionStringPoller notifierInstance;
        public static DateTime LastStarted = DateTime.Now;
        public static DateTime LastCompleted = DateTime.Now;

        public static string RemoteIp = string.Empty;
        private DateTime lastIpRefresh;
        private SupportNotificationSendHelper supportNotificationSendHelper;

        /// <summary>
        /// Indicates if the database used as actual connectionstring was available during the last check
        /// </summary>
        public bool IsDatabaseAvailable = true; // Be postive, of course we assume all is good.

        private ConnectionStringPoller()
        {
            this.lastIpRefresh = DateTime.MinValue;

            string emailSenderAddress, emailSenderName, smsSender;
            WebEnvironmentHelper.GetSupportNotificationSenderInformation(out emailSenderAddress, out emailSenderName, out smsSender);
            this.supportNotificationSendHelper = new SupportNotificationSendHelper(emailSenderAddress, emailSenderName, smsSender);
        }

        /// <summary>
        /// Returns instance object of CometServer
        /// </summary>
        public static ConnectionStringPoller Instance
        {
            get { return notifierInstance ?? (notifierInstance = new ConnectionStringPoller()); }
        }

        public void DoWork()
        {
            // Only do work if a previous call is finished.
            if (Monitor.TryEnter(this, new TimeSpan(0, 0, 1)))
            {
                try
                {
                    if (TestUtil.IsPcGabriel)
                    {
                        Debug.WriteLine("ERROR: Skipping ConnectionPoller work!!");
                        LastStarted = DateTime.Now;
                        LastCompleted = DateTime.Now;
                        return;
                    }

                    this.Log("Start to Execute Work");
                    ConnectionStringPoller.LastStarted = DateTime.Now;

                    this.SetRemoteIp();

                    var setRemoveIpTime = DateTime.Now - ConnectionStringPoller.LastStarted;

                    this.ExecuteWork();

                    LastCompleted = DateTime.Now;

                    // Execution TimeSpan
                    TimeSpan executionTime = ConnectionStringPoller.LastCompleted - ConnectionStringPoller.LastStarted;

                    this.Log("Finished executing work in {0} milliseconds (SetRemoteIP: {1})", executionTime.TotalMilliseconds, setRemoveIpTime.TotalMilliseconds);

                    if (executionTime.TotalSeconds > 5)
                    {
                        // Report to RequestLog
                        try
                        {
                            if (this.IsDatabaseAvailable)
                            {
                                var requestLog = new RequestlogEntity();
                                requestLog.SourceApplication = Global.ApplicationInfo.ApplicationName;
                                requestLog.MethodName = "ConnectionStringPoller.DoWork";
                                requestLog.ResultEnumValueName = "LongExecution";
                                requestLog.ResultEnumTypeName = "LongExecution";
                                requestLog.ErrorMessage = string.Format("Execution took: {0} seconds.", executionTime.TotalSeconds);
                                requestLog.ResultCode = "200";
                                requestLog.ResultMessage = "Completed";
                                requestLog.Save();
                            }
                        }
                        catch
                        {
                            // The connection string poller might be running when there is no database, should continue then.
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.Log("ERROR: {0}\n{1}", ex.Message, ex.StackTrace);
                }
                finally
                {
                    Monitor.Exit(this);
                }
            }
            else
            {
                try
                {
                    if (this.IsDatabaseAvailable)
                    {
                        var requestLog = new RequestlogEntity();
                        requestLog.SourceApplication = Global.ApplicationInfo.ApplicationName;
                        requestLog.MethodName = "ConnectionStringPoller.DoWork";
                        requestLog.ResultEnumValueName = "FailedToGetLock";
                        requestLog.ResultEnumTypeName = "FailedToGetLock";
                        requestLog.ErrorMessage = string.Format("DoWork could not be performed: Previous iteration was still busy");
                        requestLog.ResultCode = "9999";
                        requestLog.ResultMessage = "Failed";
                        requestLog.Save();
                    }
                }
                catch
                {
                    // The connection string poller might be running when there is no database, should continue then.
                }
                this.Log("ConnectionStringPoller.DoWork was not yet finished when it was called again");
            }
            this.Log("Do Work Finished");
        }

        private void SetRemoteIp()
        {
            // IP won't change a lot, would it?
            if (this.lastIpRefresh < DateTime.Now.AddMinutes(-15))
            {
                List<string> nocserviceUrls = WebEnvironmentHelper.GetNocServiceIpCheckUrls();
                RemoteIp = NetUtil.GetRemoteIp(nocserviceUrls);
                if (RemoteIp.Length == 0)
                    this.Log("Failed to get remote IP address for server");
                else
                    this.lastIpRefresh = DateTime.Now;
            }
        }

        private void ExecuteWork()
        {
            // Retrieve ConnectionString from NOC
            bool? databaseAvailable = null;
            DatabaseConnectionInformation dci;
            string tryRetrieveFromNocServiceExceptions;
            List<string> urls = WebEnvironmentHelper.GetNocServiceConnectionStringTxtUrls();
            if (DatabaseConnectionInformation.TryRetrieveFromNocService(urls, out dci, out tryRetrieveFromNocServiceExceptions))
            {
                SqlConnectionStringBuilder sqlConnectionString;
                if (!ConvertStringToSqlConnection(dci.ConnectionString, out sqlConnectionString))
                {
                    this.Log("Failed to parse connectionstring received from NocService!");
                }

                if (sqlConnectionString != null)
                {
                    // Convert DNS datasource to IP
                    ConvertSqlDataSourceToIp(sqlConnectionString);

                    if (!sqlConnectionString.ConnectionString.Equals(Data.DaoClasses.CommonDaoBase.ActualConnectionString, StringComparison.InvariantCultureIgnoreCase))
                    {
                        // Connection string is different from the one currenlty in use. Try to switch!
                        string results;

                        // Switcheroo Attempt #1
                        bool isSwitched = this.SwitchDatabaseConnection(sqlConnectionString.ConnectionString, out results);
                        if (!isSwitched)
                        {
                            // Connecting to the new string failed.
                            this.Log("Failed to connect to connectionstring: {0}{1}{2}", sqlConnectionString.ConnectionString, Environment.NewLine, results);
                        }

                        // Switcheroo Attempt #2
                        if (!isSwitched && !dci.ConnectionString.Equals(sqlConnectionString.ConnectionString, StringComparison.InvariantCultureIgnoreCase))
                        {
                            this.Log("Switching to original connectionstring from NocService.");

                            // Failed to connect to modified connection string, try switching to the
                            // orginal connectionstring received from NocService
                            isSwitched = this.SwitchDatabaseConnection(dci.ConnectionString, out results);
                            if (!isSwitched)
                            {
                                this.Log("Failed to connect to orignal NocService connectionstring: {0}{1}{2}", dci.ConnectionString, Environment.NewLine, results);
                            }
                        }

                        if (!isSwitched)
                        {
                            // Switcheroo completely failed
                            this.Log("Failed to switch database connection, keep using orignal connectionstring: {0}", Data.DaoClasses.CommonDaoBase.ActualConnectionString);
                        }

                        databaseAvailable = isSwitched;
                    }
                }
            }
            else
            {
                this.Log("Could not retrieve DatabaseConnectionInformation from NocService");
            }

            if (!tryRetrieveFromNocServiceExceptions.IsNullOrWhiteSpace())
            {
                this.Log("Issues with retrieves the DatabaseConnectionInformation: {0}", tryRetrieveFromNocServiceExceptions);
            }

            // Always check the database
            if (!databaseAvailable.HasValue)
                databaseAvailable = DatabaseHelper.IsDatabaseReady(Data.DaoClasses.CommonDaoBase.ActualConnectionString);

            this.IsDatabaseAvailable = databaseAvailable.Value;
        }

        private void ConvertSqlDataSourceToIp(SqlConnectionStringBuilder sqlConnectionString)
        {
            string dataSourceHost = sqlConnectionString.DataSource;
            string dataSourcePort = string.Empty;
            if (sqlConnectionString.DataSource.Contains(","))
            {
                var split = sqlConnectionString.DataSource.Split(',');
                dataSourceHost = split[0];
                dataSourcePort = "," + split[1];
            }

            if (dataSourceHost.StartsWith("localhost", StringComparison.InvariantCultureIgnoreCase) ||
                dataSourceHost.StartsWith("tcp:localhost", StringComparison.InvariantCultureIgnoreCase) ||
                RemoteIp.Equals("127.0.0.1") || RemoteIp.Equals("::1"))
            {
                // Use (local) when connecting to localhost or local IP. 
                // This will make the SQL client connect using shared memory mode
                sqlConnectionString.DataSource = "tcp:localhost" + dataSourcePort;
            }
            else if (dataSourceHost.StartsWith("192.168.") || dataSourceHost.StartsWith("10.") || dataSourceHost.StartsWith("172.16.") ||
                     dataSourceHost.Equals("127.0.0.1") || dataSourceHost.StartsWith("169.254.") || dataSourceHost.Equals("(local)", StringComparison.InvariantCultureIgnoreCase))
            {
                // Using private ip-range to connect to database, leave it at that.
            }
            else if (RemoteIp.Length != 0)
            {
                try
                {
                    // We have our own remote IP. Try to resolve DataSource host to IP
                    var addressList = Dns.GetHostAddresses(dataSourceHost);
                    foreach (var ipAddress in addressList)
                    {
                        if (ipAddress.ToString().Equals(RemoteIp, StringComparison.InvariantCultureIgnoreCase) ||
                            ipAddress.ToString().Equals("127.0.0.1") || ipAddress.ToString().Equals("::1"))
                        {
                            // If DNS resolves to our own external or local IP, connect to database locally
                            sqlConnectionString.DataSource = "tcp:localhost" + dataSourcePort;
                            break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.Log("Failed to resolve IP address from DNS. Exception: {0}", ex.Message);
                }
            }
        }

        private bool SwitchDatabaseConnection(string connectionString, out string results)
        {
            bool toReturn = false;
            var dci = new DatabaseConnectionInformation(connectionString);

            // We've received another connection string, test if it works.
            if (dci.ValidateConnectionString(WebEnvironmentHelper.CloudEnvironment, Obymobi.Data.DbUtils.GetCurrentCatalogName(), out results))
            {
                string previousDatasource = string.Empty;
                string newDataSource = string.Empty;

                SqlConnectionStringBuilder sqlConnectionStringBuilder;
                if (ConvertStringToSqlConnection(Data.DaoClasses.CommonDaoBase.ActualConnectionString, out sqlConnectionStringBuilder))
                {
                    previousDatasource = sqlConnectionStringBuilder.DataSource;
                }

                if (ConvertStringToSqlConnection(dci.ConnectionString, out sqlConnectionStringBuilder))
                {
                    newDataSource = sqlConnectionStringBuilder.DataSource;
                }

                Data.DaoClasses.CommonDaoBase.ActualConnectionString = dci.ConnectionString;

                try
                {
                    if (this.IsDatabaseAvailable)
                    {
                        var requestLog = new RequestlogEntity();
                        requestLog.SourceApplication = Global.ApplicationInfo.ApplicationName;
                        requestLog.MethodName = "ConnectionStringPoller.DoWork";
                        requestLog.ResultEnumValueName = "ChangedConnectionString";
                        requestLog.ResultEnumTypeName = "ChangedConnectionString";
                        if (WebEnvironmentHelper.CloudEnvironment == Enums.CloudEnvironment.Manual)
                            requestLog.ErrorMessage = "Changing from {1} to {2}".FormatSafe(previousDatasource, newDataSource);
                        else
                            requestLog.ErrorMessage = "Changing from {1} to {2}".FormatSafe(previousDatasource, newDataSource);
                        requestLog.ResultCode = "200";
                        requestLog.ResultMessage = "Changed Connection String";
                        requestLog.Save();

                        if (WebEnvironmentHelper.CloudEnvironment != CloudEnvironment.Manual)
                        {
                            // Send a notification about this (db required, therefore in this if statement).
                            List<string> emailAddresses = new List<string>(); // SupportpoolHelper.GetSystemSupportpoolEmailaddresses();
                            List<string> phoneNumbers = new List<string>(); // SupportpoolHelper.GetSystemSupportpoolPhonenumbers();
                            string errorReport;
                            this.supportNotificationSendHelper.SendSupportNotification(phoneNumbers, emailAddresses, "ConnectionString changed for Application '{0}' on '{1}'".FormatSafe(Global.ApplicationInfo.ApplicationName, Environment.MachineName), out errorReport);

                            if (!errorReport.IsNullOrWhiteSpace())
                                this.Log("Support Notification about ConnectionString change failed: {0}", errorReport);
                        }
                    }

                    this.Log("Changed ConnectionString: {0}", "Changing from {1} to {2}".FormatSafe(previousDatasource, newDataSource));
                }
                catch (Exception ex)
                {
                    this.Log("SwitchDatabaseConnection - {0}", ex.Message);
                }

                toReturn = true;
            }

            return toReturn;
        }

        private bool ConvertStringToSqlConnection(string connectionString, out SqlConnectionStringBuilder builder)
        {
            builder = null;
            try
            {
                // Check if connectionstring from NocService is valid
                builder = new SqlConnectionStringBuilder(connectionString);
            }
            catch (Exception ex)
            {
                this.Log("Failed to parse connectionstring '{0}'.{1}Excpetion: {2}", connectionString, Environment.NewLine, ex.Message);
            }

            return (builder != null);
        }

        private DateTime lastCleanup = DateTime.MinValue;
        private void Log(string format, params object[] args)
        {
            string textToWrite = format.FormatSafe(args);
            textToWrite = DateTime.Now.ToString("[HH:mm:ss] ") + textToWrite;
            Debug.WriteLine("CONNECTIONSTRINGPOLLER: " + textToWrite);

            try
            {
                string fileName = "ConnectionStringPoller-" + DateTime.Now.ToString("yyyyMMdd") + ".log";
                string path = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/Logs/");
                if (path == null)
                    return;

                string filepath = Path.Combine(path, fileName);

                using (StreamWriter writer = File.AppendText(filepath))
                {
                    writer.Write(textToWrite);
                    writer.Write("\n");
                }

                if (lastCleanup.TotalSecondsToNow() > 600)
                {
                    // Clean up
                    var files = Directory.GetFiles(path, "ConnectionStringPoller*");
                    foreach (var file in files)
                    {
                        var fi = new FileInfo(file);
                        if ((DateTime.Now - fi.LastWriteTime).Days > 14)
                            File.Delete(file);
                    }

                    lastCleanup = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Writing to log file failed:".FormatSafe(ex.Message));
            }
        }
    }
}
