﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos.Logging;
using Obymobi.Web.Slack;
using Obymobi.Web.Slack.Interfaces;

namespace Obymobi.Web.Pollers
{
    public abstract class BasePoller : IPoller, ILoggingProvider
    {
        private const int DELAY_BETWEEN_SLACK_MESSAGES_MINUTES = 30;

        private readonly object workLock = new object();
        private bool isFirstRun = true;
        private DateTime lastNotificationAttemptUtc = DateTime.MinValue;

        protected readonly DateTime instanceCreated;
        protected readonly ISlackClient slackClient;

        protected CancellationToken CancellationToken { get; private set; }

        /// <summary>
        /// Interface for logging
        /// </summary>
        public abstract IMultiLoggingProvider LoggingProvider { get; }

        /// <summary>
        /// Enum type of the poller
        /// </summary>
        public abstract PollerType PollerType { get; }

        /// <summary>
        /// Logging type species to which file log lines are written
        /// </summary>
        public abstract Enum LoggingType { get; }

        /// <summary>
        /// Time when poller was last started
        /// </summary>
        public DateTime LastStarted { get; set; } = DateTime.MinValue;

        /// <summary>
        /// Time when poller last completed a full run
        /// </summary>
        public DateTime LastCompleted { get; set; } = DateTime.MinValue;

        /// <summary>
        /// Determines after how many seconds since lastexecution the Secondary Poller is allowed to start & when errors appear on the Noc
        /// </summary>
        public abstract int PrimaryPollerTimeoutSeconds { get; }

        /// <summary>
        /// TimeSpan after which this poller should run again
        /// </summary>
        protected abstract TimeSpan WorkInterval { get; }

        /// <summary>
        /// TimeSpan which indicates after how much time the first run should be done
        /// </summary>
        protected abstract TimeSpan FirstWorkInterval { get; }

        protected BasePoller(ISlackClient slackClient)
        {
            this.instanceCreated = DateTime.Now;
            this.slackClient = slackClient;
        }

        public virtual TimeSpan TimeTillNextWork()
        {
            DateTime lastStarted = LastStarted;
            if (lastStarted == DateTime.MinValue)
                lastStarted = instanceCreated;

            var nextWorkTime = lastStarted.Add(isFirstRun ? this.FirstWorkInterval : this.WorkInterval);
            var timeTillNextWork = nextWorkTime.Subtract(DateTime.Now);

            return timeTillNextWork;
        }

        public virtual void Initialize(CancellationToken cancelToken)
        {
            this.CancellationToken = cancelToken;

            LogInfo("Initialize", "Initialized");
        }

        public void DoWork(bool force = false)
        {
            isFirstRun = false;

            if (Monitor.TryEnter(this.workLock, 1000)) // 1 second timeout
            {
                try
                {
                    this.LastStarted = DateTime.Now;
                    var executionTimer = new Stopwatch();
                    executionTimer.Start();
                    
                    if (CancellationToken.IsCancellationRequested)
                    {
                        LogInfo("DoWork", "Cancellation is requestion");
                    }
                    else
                    {
                        this.LogVerbose("DoWork", "Begin");
                        this.ExecuteWork(force);
                        executionTimer.Stop();
                        this.LogVerbose("DoWork", $"End ({executionTimer.ElapsedMilliseconds}ms elapsed)");
                    }

                    executionTimer.Stop();

                    LastCompleted = DateTime.Now;
                }
                catch (Exception ex)
                {
                    try
                    {
                        this.LogError("DoWork", "Message: {0}\n\r{1}", ex.Message, ex.ProcessStackTrace(true));
                        this.SendNotificationOnSlack(ex);
                    }
                    catch
                    {
                        System.Diagnostics.Debug.WriteLine("Unable to write to log. {0}\n{1}", ex.Message, ex.StackTrace);
                        // Too bad.
                    }
                }
                finally
                {
                    Monitor.Exit(this.workLock);
                }
            }
            else
            {
                try
                {
                    var requestLog = new RequestlogEntity();
                    requestLog.SourceApplication = Global.ApplicationInfo.ApplicationName;
                    requestLog.MethodName = this.PollerType.ToString() + ".DoWork";
                    requestLog.ResultEnumValueName = "FailedToGetLock";
                    requestLog.ResultEnumTypeName = "FailedToGetLock";
                    requestLog.ErrorMessage = "DoWork could not be performed: Previous iteration was still busy";
                    requestLog.ResultMessage = "Failed";
                    requestLog.Save();
                }
                catch
                {
                    // The connection string poller might be running when there is no database, should continue then.
                    System.Diagnostics.Debug.WriteLine("The poller '{0}' might be running when there is no database. Continue...".FormatSafe(this.PollerType.ToString()));
                }

                this.LogWarning("DoWork", "DoWork was not yet finished when it was called again");
            }
        }

        protected abstract void ExecuteWork(bool force = false);

        public long SecondsSinceLastExecution => Convert.ToInt64((DateTime.Now - this.LastCompleted).TotalSeconds);

        protected void Log(LoggingLevel loggingLevel, string method, string message, params object[] args)
        {
            string messageFormatted;
            if (args.Length > 0) messageFormatted = message.FormatSafe(args);
            else messageFormatted = message;
            messageFormatted = method + " - " + messageFormatted;

            this.LoggingProvider.Log(this.LoggingType, loggingLevel, messageFormatted);
        }

        protected void LogVerbose(string method, string message, params object[] args)
        {
            Log(LoggingLevel.Verbose, method, message, args);
        }

        protected void LogInfo(string method, string message, params object[] args)
        {
            Log(LoggingLevel.Info, method, message, args);
        }

        protected void LogWarning(string method, string message, params object[] args)
        {
            Log(LoggingLevel.Warning, method, message, args);
        }

        protected void LogError(string method, string message, params object[] args)
        {
            Log(LoggingLevel.Error, method, message, args);
        }

        #region ILogginProvider

        public void Log(LoggingLevel level, string text, params object[] args)
        {
            string messageFormatted = args.Length > 0 ? text.FormatSafe(args) : text;

            LoggingProvider.Log(LoggingType, level, messageFormatted);
        }

        public void Verbose(string format, params object[] args)
        {
            Log(LoggingLevel.Verbose, format, args);
        }

        public void Debug(string format, params object[] args)
        {
            Log(LoggingLevel.Debug, format, args);
        }

        public void Information(string format, params object[] args)
        {
            Log(LoggingLevel.Info, format, args);
        }

        public void Warning(string format, params object[] args)
        {
            Log(LoggingLevel.Warning, format, args);
        }

        public void Error(string format, params object[] args)
        {
            Log(LoggingLevel.Error, format, args);
        }

        #endregion

        protected void SendNotificationOnSlack(Exception exception)
        {
            if (!IsEnoughTimePassedSinceLastNotification())
            {
                return;
            }

            LogInfo(nameof(BasePoller.SendNotificationOnSlack), "Sending notification on Slack");

            lastNotificationAttemptUtc = DateTime.UtcNow;
            
            SlackMessage message = CreateSlackMessage(exception);
            if (!slackClient.SendMessage(message))
            {
                LogError(nameof(BasePoller.SendNotificationOnSlack), "Failed to send notification on Slack");
            }
        }

        private bool IsEnoughTimePassedSinceLastNotification()
        {
            TimeSpan elapsedSinceLastMessage = DateTime.UtcNow - lastNotificationAttemptUtc;
            return elapsedSinceLastMessage.TotalMinutes > DELAY_BETWEEN_SLACK_MESSAGES_MINUTES;
        }

        private SlackMessage CreateSlackMessage(Exception exception)
        {
            string text = $"Services: An unhandled exception was thrown in the {GetType().Name}.\n";

            List<SlackAttachmentField> fields = new List<SlackAttachmentField>
            {
                new SlackAttachmentField("Server", Environment.MachineName),
                new SlackAttachmentField("Exception", exception.GetType().Name),
                new SlackAttachmentField("Message", exception.Message)
            };

            return new SlackMessage
            {
                Attachments = new List<SlackAttachment>
                {
                    new SlackAttachment(text, fields, text, null, SlackAttachmentColor.Danger)
                }
            };
        }
    }
}