﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Status
{
    public class ConnectionStringPollerManager
    {

        private static InfiniteLooper connectionStringPollerThread;
        private static bool pollersStarted = false;
        private static Object starterLock = new Object();

        public static void StartPollers()
        {
            lock (starterLock)
            {
                if (pollersStarted)
                    return;

                //if (ConnectionStringPollerManager.connectionStringPollerThread == null)
                //    ConnectionStringPollerManager.connectionStringPollerThread = InfiniteLooper.StartNew(() => ConnectionStringPoller.Instance.DoWork(), 7500, -1, ConnectionStringPollerExceptionHandler);

                ConnectionStringPollerManager.pollersStarted = true;
            }
        }

        static void ConnectionStringPollerExceptionHandler(Exception ex)
        {
            connectionStringPollerThread.Stop();
            connectionStringPollerThread.Start();
        }

    }
}
