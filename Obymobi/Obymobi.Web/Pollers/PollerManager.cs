﻿using System;
using System.Collections.Concurrent;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Web.Pollers
{
    /// <summary>
    /// Summary description for PollerManager
    /// </summary>
    public static class PollerManager
    {
        public static readonly string Identifier;

        public static ConcurrentDictionary<PollerType, IPoller> Pollers { get; private set; }        

        static PollerManager()
        {
            Identifier = GuidUtil.CreateGuid();
            Pollers = new ConcurrentDictionary<PollerType, IPoller>();
        }

        public static void SetPoller(IPoller poller)
        {
            poller.LoggingProvider.Log(LoggingLevel.Debug, "Registering Poller '{0}' to PollerManager '{1}'", poller.PollerType, Identifier);

            if (!Pollers.TryAdd(poller.PollerType, poller))
            {
                throw new Exception("Poller {0} couldn't be added.".FormatSafe(poller.PollerType));
            }
        }

        public static bool TryGetPoller<T>(PollerType pollerType, out T poller) where T : IPoller
        {
            IPoller p;
            bool toReturn = Pollers.TryGetValue(pollerType, out p);
            poller = (T)p;
            return toReturn;
        }

        public static long GetSecondsSinceLastExecution(PollerType pollerType)
        {
            IPoller poller;
            long toReturn = -1;
            if (TryGetPoller(pollerType, out poller))
            {
                toReturn = poller.SecondsSinceLastExecution;
            }
            return toReturn;
        }
    }
}