﻿using System;
using System.Threading;
using Obymobi.Enums;

namespace Obymobi.Web.Pollers
{
    /// <summary>
    /// Summary description for IPoller
    /// </summary>
    public interface IPoller
    {
        /// <summary>
        /// Interface for logging
        /// </summary>
        Dionysos.Logging.IMultiLoggingProvider LoggingProvider { get; }

        /// <summary>
        /// Enum type of the poller
        /// </summary>
        PollerType PollerType { get; }

        /// <summary>
        /// Time when poller was last started
        /// </summary>
        DateTime LastStarted { get; set; }

        /// <summary>
        /// Time when poller last completed a full run
        /// </summary>
        DateTime LastCompleted { get; set; }

        /// <summary>
        /// Amount of seconds since <see cref="LastCompleted"/>
        /// </summary>
        long SecondsSinceLastExecution { get; }

        /// <summary>
        /// Determines after how many seconds since lastexecution the Secondary Poller is allowed to start & when errors appear on the Noc
        /// </summary>
        int PrimaryPollerTimeoutSeconds { get; }

        /// <summary>
        /// Time till next work run
        /// </summary>
        /// <returns>TimeSpan till next exection</returns>
        TimeSpan TimeTillNextWork();

        /// <summary>
        /// Called once after poller intance is created
        /// </summary>
        void Initialize(CancellationToken cancellationToken);

        /// <summary>
        /// Main method, in which all work is done. For long running operations this method should be called inside a thread.
        /// </summary>
        void DoWork(bool force = false);
    }
}