using Dionysos;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Status;
using SD.Tools.OrmProfiler.Interceptor;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Web;

/// WARNING: If you change something here, change it in the HttpApplicationSystemBased too!
/// WARNING: If you change something here, change it in the HttpApplicationSystemBased too!
/// WARNING: If you change something here, change it in the HttpApplicationSystemBased too!
/// WARNING: If you change something here, change it in the HttpApplicationSystemBased too!

namespace Obymobi.Web
{
    public abstract class HttpApplicationDionysosBased : Dionysos.Web.HttpApplication
    {
        protected readonly string ApplicationName;

        protected static bool applicationInitialized = false;
        protected static Object initializationLock = new Object();
        protected static DateTime lastDatabaseCheck = DateTime.UtcNow;
        protected static string lastDatabaseStatus = string.Empty;

        protected HttpApplicationDionysosBased() : this("Unknown")
        { }

        protected HttpApplicationDionysosBased(string applicationName)
        {
            this.ApplicationName = applicationName;
        }

        protected override void Application_Start(object sender, EventArgs e)
        {
            HttpContext.Current.Application["APPLICATION_STARTED"] = DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss");
            ServicePointManager.SecurityProtocol |= SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            // LLBLGen ORM Profiler
            InterceptorCore.Initialize(ApplicationName);
            InterceptorCore.DisableMessageSending();

            // Database INdependent logic
            this.DatabaseIndependentPreInitialization();
            base.DatabaseIndependentInitializationLogic();

            // An entity has to be initiliazed to get the ActualConnectionString filled
            if (Obymobi.Data.DaoClasses.CommonDaoBase.ActualConnectionString.IsNullOrWhiteSpace())
            {
                try
                {
                    var l = new VattariffEntity(1);
                }
                catch
                { } // Might go wrong with invalid DB information.
            }

            CommonDaoBase.CommandTimeOut = 300;

            ConnectionStringPollerManager.StartPollers();

            if (DatabaseHelper.IsDatabaseReady(Obymobi.Data.DaoClasses.CommonDaoBase.ActualConnectionString))
            {
                this.DatabaseDependentInitialization();
                base.DatabaseDependentInitializationLogic();
                HttpApplicationDionysosBased.applicationInitialized = true;
            }
            else
            {
                ConnectionStringPoller.Instance.IsDatabaseAvailable = false;
                Debug.WriteLine("WARNING: HttpApplication is not yet fully initialized due to unavailable database");
                // Can't throw an Exception, would cause the HttpApplication to close and stop connection string poller.
                //throw new Exception("Application_Start did not complete: Database was unavailable. ConnectionStringPoller will try to get a working ConnectionString from the NocService");
            }
        }

        /// <summary>
        /// Used to initialize datase independent things:
        /// DataProviders, AssemblyInformation, ConfigurationProvider, ApplicationInfo, ConfigurationInfo, CometConnection
        /// </summary>
        public abstract void DatabaseIndependentPreInitialization();

        public abstract void DatabaseDependentInitialization();

        protected override void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!HttpApplicationDionysosBased.applicationInitialized)
            {
                // Try to initialize every 10 seconds on a seperate thread

                if (HttpApplicationDionysosBased.lastDatabaseCheck.TotalSecondsToNow() > 10)
                {
                    HttpApplicationDionysosBased.lastDatabaseCheck = DateTime.UtcNow;
                    var t = new Thread(this.TryInitialize);
                    t.Start();
                }
            }
        }

        protected void EnableDatabaseProfiler() => InterceptorCore.EnableMessageSending();

        private void TryInitialize()
        {
            Debug.WriteLine("INFORMATION: Global.TryInitialize - Start");

            string status;

            DatabaseHelper.IsDatabaseReady(Obymobi.Data.DaoClasses.CommonDaoBase.ActualConnectionString, out status);
            HttpApplicationDionysosBased.lastDatabaseStatus = status;

            if (!HttpApplicationDionysosBased.applicationInitialized && HttpApplicationDionysosBased.lastDatabaseStatus.Equals("ONLINE"))
            {
                lock (HttpApplicationDionysosBased.initializationLock)
                {
                    if (HttpApplicationDionysosBased.applicationInitialized)
                        return;

                    this.DatabaseDependentInitialization();
                    base.DatabaseDependentInitializationLogic();
                    Debug.WriteLine("INFORMATION: HttpApplicationSystemBased.IntializeApplication Completed");
                    HttpApplicationDionysosBased.applicationInitialized = true;
                }
            }
        }
    }
}
