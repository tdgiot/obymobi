﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Interfaces;
using Dionysos.SMS;
using Dionysos;

namespace Obymobi.Logic.Verifiers
{
    /// <summary>
    /// Verifier class which verifies if dependency injection is enabled
    /// </summary>
    public class SMSVerifier : IVerifier
    {
        #region Fields

        private string errorMessage = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.SMSVerifier class
        /// </summary>
        public SMSVerifier()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes the verifier and checks whether dependency injection is enabled
        /// </summary>
        /// <returns>True if verification was succesful, False if not</returns>
        public bool Verify()
        {
            bool succes = true;

            // Check whether SMS configuration values exists
            string username = ConfigurationManager.GetString(DionysosSMSConfigurationConstants.Username);
            if (username.Length == 0)
            {
                succes = false;
                this.errorMessage += "SMS username not specified in configuration!";
            }

            string password = ConfigurationManager.GetString(DionysosSMSConfigurationConstants.Password);
            if (password.Length == 0)
            {
                succes = false;
                this.errorMessage += "SMS password not specified in configuration!";
            }

            return succes;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the error message if verification failed
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion
    }
}
