﻿using Dionysos;
using Dionysos.Interfaces;
using Obymobi.Configuration;
using Obymobi.Data;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using System;

namespace Obymobi.Logic.Verifiers
{
    public class CloudEnvironmentConfigValidator : IVerifier
    {
        public string ErrorMessage
        {
            get;
            set;
        }

        public bool Verify()
        {
            this.ErrorMessage = "";
            bool valid = true;
            CloudEnvironment cloudEnvironmentFromAppSettings = WebEnvironmentHelper.CloudEnvironment;

            // GK Replaced by a generic setting for all applications using the datbase.
            // var cloudEnvironmentFromDbConfigSettings = Dionysos.ConfigurationManager.GetString(CraveCloudConfigConstants.CloudEnvironment).ToEnum<CloudEnvironment>();
            
            // To ensure the record is available, let's call for it
            Dionysos.ConfigurationManager.GetString(ObymobiConfigConstants.CraveCloudEnvironment);

            // Chosen to use THIS method to retrieve it, as it's the exact same method the ConnectionStringPoller uses and makes sure that that is working.
            CloudEnvironment cloudEnvironmentFromDbConfigSettings = DatabaseHelper.GetEnvironmentOfDatabase(Obymobi.Data.DaoClasses.CommonDaoBase.ActualConnectionString, DbUtils.GetCurrentCatalogName());

            if (cloudEnvironmentFromAppSettings <= CloudEnvironment.ProductionOverwrite && cloudEnvironmentFromDbConfigSettings <= CloudEnvironment.ProductionOverwrite)
            {
                // We're good, both in production
            }
            else if (cloudEnvironmentFromAppSettings != cloudEnvironmentFromDbConfigSettings)
            {
                // Not good, two different environments. AppSettings and Db should match.
                valid = false;
                this.ErrorMessage = "- Cloud Environments in Database.Configuration (Name: 'CraveCloudEnvironment', Section: 'Obymobi'): '{0}' and AppSettings.config: '{1}' mismatch. They must both be a variant of Production or be equal.\r\n".FormatSafe(
                    cloudEnvironmentFromDbConfigSettings, cloudEnvironmentFromAppSettings);
            }

            // GK Removed this as it didn't look like it was used anywhere
            //if (cloudEnvironmentFromAppSettings == CloudEnvironment.Manual && ConfigurationManager.GetString(CraveCloudConfigConstants.ManualWebserviceBaseUrl).IsNullOrWhiteSpace())
            //{
            //    valid = false;
            //    this.ErrorMessage = "- Manual Webservice Base Url is not configured in the Configuration table of your Database.";
            //}

            return valid;
        }
    }
}
