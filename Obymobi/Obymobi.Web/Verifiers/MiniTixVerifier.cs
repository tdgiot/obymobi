﻿using System.Collections.Generic;
using System.Text;
using Dionysos.Interfaces;
using Dionysos.SMS;
using Dionysos;

namespace Obymobi.Logic.Verifiers
{
    /// <summary>
    /// Verifier class which verifies if dependency injection is enabled
    /// </summary>
    public class MiniTixVerifier : IVerifier
    {
        #region Fields

        private string errorMessage = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.MiniTixVerifier class
        /// </summary>
        public MiniTixVerifier()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes the verifier and checks whether dependency injection is enabled
        /// </summary>
        /// <returns>True if verification was succesful, False if not</returns>
        public bool Verify()
        {
            bool succes = true;

            // Check whether MiniTix configuration values exists
            string returnUrl = ConfigurationManager.GetString(Obymobi.ObymobiConfigConstants.MiniTixReturnUrl, false);
            if (returnUrl.Length == 0)
            {
                succes = false;
                errorMessage += "MiniTix return url not specified in configuration!";
            }

            string errorUrl = ConfigurationManager.GetString(Obymobi.ObymobiConfigConstants.MiniTixErrorUrl, false);
            if (errorUrl.Length == 0)
            {
                succes = false;
                errorMessage += "MiniTix error url not specified in configuration!";
            }

            return succes;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the error message if verification failed
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion
    }
}
