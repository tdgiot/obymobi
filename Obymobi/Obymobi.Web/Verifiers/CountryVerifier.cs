﻿using Dionysos;
using Dionysos.Interfaces;
using Obymobi.Configuration;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using System;

namespace Obymobi.Logic.Cms
{
    public class CountryVerifier : IVerifier
    {
        public string ErrorMessage
        {
            get;
            set;
        }

        public bool Verify()
        {
            this.ErrorMessage = "";
            bool valid = true;

            CountryCollection countryCollection = new CountryCollection();
            countryCollection.GetMulti(null);

            foreach (CountryEntity countryEntity in countryCollection)
            {
                string issues = string.Empty;

                if (countryEntity.Name.IsNullOrWhiteSpace())
                    issues += "Name";

                if (countryEntity.Code.IsNullOrWhiteSpace())
                {
                    if (!issues.IsNullOrWhiteSpace())
                        issues += ", ";
                    issues += "Code";
                }

                if (countryEntity.CultureName.IsNullOrWhiteSpace())
                {
                    if (!issues.IsNullOrWhiteSpace())
                        issues += ", ";
                    issues += "CultureName";
                }

                if (countryEntity.CodeAlpha3.IsNullOrWhiteSpace())
                {
                    if (!issues.IsNullOrWhiteSpace())
                        issues += ", ";
                    issues += "CodeAlpha3";
                }

                if (!issues.IsNullOrWhiteSpace())
                {
                    valid = false;
                    this.ErrorMessage += string.Format("Country with ID {0}: {1}\r\n", countryEntity.CountryId, issues);
                }
            }

            if (!valid)
                this.ErrorMessage = "The following countries are missing values in the database:\r\n" + this.ErrorMessage;

            return valid;
        }
    }
}
