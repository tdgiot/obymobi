﻿using System;
using Dionysos.Interfaces;
using Obymobi.Logging;

namespace Obymobi.Logic.Verifiers
{
    /// <summary>
    /// LoggingVerifier class
    /// </summary>
	public class LoggingVerifier : IVerifier
	{
		private string errorMessage;
		private string stackTrace;

		#region IVerifier Members

		bool IVerifier.Verify()
		{
			bool toReturn = true;

			try
			{
				Exception testOne = new Exception("Test 1");
				Exception testTwo = new Exception("Test 2");

				Requestlogger.ExceptionToFileHandler(testOne, testTwo, true);
			}
			catch (Exception ex) 
			{
				this.errorMessage = ex.Message;
				this.stackTrace = ex.StackTrace;
				toReturn = false;
			}

			return toReturn;
		}

		string IVerifier.ErrorMessage
		{
			get
			{
				return string.Format("Requestlogger.ExceptionToFileHandler failed: {0}\r\n\r\n<br><br>{1}", this.errorMessage, this.stackTrace);
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		#endregion
	}
}
