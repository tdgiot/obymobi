﻿using Dionysos;
using Dionysos.Interfaces;
using Obymobi.Configuration;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using System;
using Dionysos.Data.LLBLGen;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Verifiers
{
    public class EntityVerifier : IVerifier
    {
        public string ErrorMessage
        {
            get;
            set;
        }

        public bool Verify()
        {
            this.ErrorMessage = "";
            bool valid = true;

            string entitiesWithoutInterface = string.Empty;

            foreach (string entityName in LLBLGenUtil.GetEntityNames())
            {
                IEntity entity = DataFactory.EntityFactory.GetEntity(entityName) as IEntity;
                if (entity != null)
                {
                    if (entity is ICompanyRelatedEntity)
                    {
                        // ICompanyRelatedEntity
                    }
                    else if (entity is INullableCompanyRelatedEntity)
                    {
                        // INullableCompanyRelatedEntity
                    }
                    else if (entity is ICompanyRelatedChildEntity)
                    {
                        // ICompanyRelatedChildEntity
                    }
                    else if (entity is INullableCompanyRelatedChildEntity)
                    {
                        // INullableCompanyRelatedChildEntity
                    }
                    else if (entity is INonCompanyRelatedEntity)
                    {
                        // INonCompanyRelatedEntity
                    }
                    else if (entity is ISystemEntity)
                    {
                        // ISystemEntity
                    }
                    else if (entity is ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity)
                    {
                        // ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity
                    }
                    else
                    {
                        if (!entitiesWithoutInterface.IsNullOrWhiteSpace())
                            entitiesWithoutInterface += ", ";
                        entitiesWithoutInterface += entityName;

                        valid = false;
                    }
                }
            }

            if (!valid)
                this.ErrorMessage = "The following entity classes do not have an interface defined:\r\n" + entitiesWithoutInterface;

            return valid;
        }
    }
}
