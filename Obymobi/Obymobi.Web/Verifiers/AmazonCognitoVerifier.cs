using Dionysos;
using Dionysos.Interfaces;
using Microsoft.WindowsAzure;

namespace Obymobi.Web.Verifiers
{
    /// <summary>
    /// Verifier class which verifies the amazon cognito configuration properties
    /// </summary>
    public class AmazonCognitoVerifier : IVerifier
    {
        public string ErrorMessage { get; set; } = string.Empty;

        public bool Verify()
        {
            string cognitoBaseUrl = CloudConfigurationManager.GetSetting(DionysosConfigurationConstants.CognitoBaseUrl);
            string cognitoClientId = CloudConfigurationManager.GetSetting(DionysosConfigurationConstants.CognitoClientId);
            string cognitoSecret = CloudConfigurationManager.GetSetting(DionysosConfigurationConstants.CognitoSecret);

            if (cognitoBaseUrl.IsNullOrWhiteSpace() || cognitoClientId.IsNullOrWhiteSpace() || cognitoSecret.IsNullOrWhiteSpace())
            {
                this.ErrorMessage = $"The '{DionysosConfigurationConstants.CognitoBaseUrl}', '{DionysosConfigurationConstants.CognitoClientId}' or '{DionysosConfigurationConstants.CognitoSecret}' setting for Amazon Cognito is missing. Should be in Web.config or Web.AppSettings.config.";
            }

            return this.ErrorMessage.IsNullOrWhiteSpace();
        }
    }
}