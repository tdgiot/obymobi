﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.WindowsAzure;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Amazon.S3.IO;
using System.Net;
using Amazon.S3.Util;
using Obymobi.Constants;
using System.Threading.Tasks;
using Amazon.Runtime;

namespace Obymobi.Web.CloudStorage
{
    public class AmazonCloudStorageProvider : CloudStorageProvider
    {
        private string RootContainer { get; set; }

        private IAmazonS3 s3Client;

        readonly Dictionary<string, ContainerInfo> containers = new Dictionary<string, ContainerInfo>();

        public struct ContainerInfo
        {
            public readonly string ContainerName;
            public string BaseUrlHttpWithContainer;
            public string BaseUrlHttpsWithContainer;
            public bool ContainerExists;

            public ContainerInfo(string name)
            {
                foreach (char c in name)
                {
                    if (char.IsUpper(c))
                        throw new Exception("Only lower case characters are allowed for container names");
                }

                this.ContainerName = name;
                this.BaseUrlHttpWithContainer = "";
                this.BaseUrlHttpsWithContainer = "";
                this.ContainerExists = false;
            }
        }

        public AmazonCloudStorageProvider(string awsAccessKey, string awsSecretAccessKey, string rootContainer, Dionysos.Logging.ILoggingProvider loggingProvider)
            : base(loggingProvider)
        {
            try
            {
                this.GetStorageAccount(awsAccessKey, awsSecretAccessKey);
                this.RootContainer = rootContainer;
            }
            catch (AmazonS3Exception ex)
            {
                if (WebEnvironmentHelper.CloudEnvironment != CloudEnvironment.Manual)
                {
                    this.Log("Error initializing Amazon S3: {0}", ex.Message);

                    throw new ObymobiException(CloudStorageProviderResult.ConfigurationMissingOrInvalid, ex);
                }
            }

            string dummy;
            this.IsStorageProviderAvailable(out dummy);
        }

        public AmazonCloudStorageProvider(Dionysos.Logging.ILoggingProvider loggingProvider)
            : base(loggingProvider)
        {
            try
            {
                this.GetStorageAccount();
            }
            catch (AmazonS3Exception ex)
            {
                if (WebEnvironmentHelper.CloudEnvironment != CloudEnvironment.Manual)
                {
                    this.Log("Error initializing Amazon S3: {0}", ex.Message);

                    throw new ObymobiException(CloudStorageProviderResult.ConfigurationMissingOrInvalid, ex);
                }
            }
            this.SetRootContainer();
        }

        private void SetRootContainer()
        {
            switch (WebEnvironmentHelper.CloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    this.RootContainer = ObymobiConstants.PrimaryAmazonRootContainer;
                    break;
                case CloudEnvironment.Test:
                    this.RootContainer = ObymobiConstants.TestAmazonRootContainer;
                    break;
                case CloudEnvironment.Development:
                    this.RootContainer = ObymobiConstants.DevelopmentAmazonRootContainer;
                    break;
                case CloudEnvironment.Staging:
                    this.RootContainer = ObymobiConstants.StagingAmazonRootContainer;
                    break;
                case CloudEnvironment.Manual:
                    this.RootContainer = string.Format("{0}", CloudConfigurationManager.GetSetting(CloudStorageProvider.CLOUD_MANUAL_CONTAINER_KEY));
                    break;
                default:
                    throw new NotImplementedException(WebEnvironmentHelper.CloudEnvironment.ToString());
            }

            // GK I found out there was some required initialization logic in that method, so let's just run in in the constructur
            string dummy;
            this.IsStorageProviderAvailable(out dummy);
        }

        #region Interface Implementation

        public override CloudStorageProviderIdentifier Identifier
        {
            get { return CloudStorageProviderIdentifier.Amazon; }
        }

        public override bool FileExists(string container, string path, bool publicAccess)
        {
            bool exists;

            using (IAmazonS3 client = this.GetStorageAccount())
            {
                try
                {
                    S3FileInfo fileInfo = new S3FileInfo(client, this.RootContainer, this.GetKey(container, path));
                    exists = fileInfo.Exists;
                }
                catch
                {
                    exists = false;
                }
            }

            return exists;
        }

        public override bool FileRequiresUpload(string container, string path, string md5)
        {
            bool toReturn;

            try
            {
                // GK If you have used multipart uploads the etag will not be the normal md5, so always fail.
                WebRequest webRequest = WebRequest.Create(this.GetUrl(container, path));
                webRequest.Method = "HEAD";

                using (WebResponse response = webRequest.GetResponse())
                {
                    if (response.Headers.AllKeys.Contains("ETag"))
                    {
                        string eTagMd5 = response.Headers["ETag"].Replace("\"", "");

                        // Check if it's equal, if not require upload.
                        toReturn = !eTagMd5.Equals(md5);
                    }
                    else
                    {
                        this.Log("AmazonCloudStorageProvider.FileRequiresUpload: File exists on path, unable to check ETag ({0}/{1})", container, path);
                        toReturn = true;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log("AmazonCloudStorageProvider.FileRequiresUpload: {0} ({1}/{2})", ex.Message, container, path);
                toReturn = true;
            }

            return toReturn;
        }

        public override bool ReadFile(string containerName, string path, out MemoryStream stream)
        {
            bool toReturn = false;
            stream = new MemoryStream();
            try
            {
                GetObjectRequest request = new GetObjectRequest();
                request.BucketName = this.RootContainer;
                request.Key = this.GetKey(containerName, path);

                this.Log("ReadFile - Reading file '{0}'", request.Key);

                using (IAmazonS3 client = this.GetStorageAccount())
                using (GetObjectResponse response = client.GetObject(request))
                {
                    if (response.ResponseStream != null && response.ResponseStream.Length > 0)
                    {
                        this.Log("ReadFile - Response for file '{0}' has length: {1}", request.Key, response.ResponseStream.Length);
                        response.ResponseStream.CopyTo(stream);
                        stream.Position = 0;

                        if (stream.Length > 0)
                        {
                            toReturn = true;
                        }
                    }
                    else
                    {
                        this.Log("ReadFile - Response for file '{0}' is empty.", request.Key);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log("AmazonCloudStorageProvider.ReadFile: {0}", ex.Message);
            }

            return toReturn;
        }

        public override string TestFileAccess(string container, string path)
        {
            throw new NotImplementedException();
        }

        public override bool WriteFile(string containerName, string path, Stream stream, int? maxAgeCache, bool privateAccess)
        {
            bool success = false;

            Stopwatch sw = new Stopwatch();
            sw.Start();

            try
            {
                // Create container if it doesn't already exists
                this.CreateBucket();

                try
                {
                    string fileMd5 = Dionysos.Security.Cryptography.Hasher.GetHash(stream, Dionysos.Security.Cryptography.HashType.MD5);

                    PutObjectRequest putObjectRequest = new PutObjectRequest
                    {
                        BucketName = this.RootContainer,
                        Key = this.GetKey(containerName, path),
                        InputStream = stream,
                        CannedACL = privateAccess ? S3CannedACL.AuthenticatedRead : S3CannedACL.PublicRead
                    };

                    using (IAmazonS3 client = this.GetStorageAccount())
                    {
                        PutObjectResponse response = client.PutObject(putObjectRequest);

                        string etag = response.ETag.Replace("\"", ""); // https://github.com/aws/aws-sdk-net/issues/815

                        if (response.HttpStatusCode != HttpStatusCode.OK || etag.IsNullOrWhiteSpace())
                        {
                            throw new Exception(string.Format("Response is not correct. HttpStatusCode: {0}, ETag: {1}", response.HttpStatusCode, etag));
                        }

                        if (!fileMd5.Equals(etag, StringComparison.InvariantCultureIgnoreCase))
                        {
                            throw new Exception(string.Format("MD5 hash {0} not matching with response ETag {1}", fileMd5, etag));
                        }

                        success = true;
                        sw.Stop();
                        this.Log("Uploaded file '{0}/{1}/{2}' to AmazonS3 in {3}ms (Md5: {4}, ETag: {5})", this.RootContainer, containerName, path, sw.ElapsedMilliseconds, fileMd5, etag);
                    }
                }
                catch (AmazonS3Exception ex)
                {
                    this.Log("Failed to write file to Amazon S3. Bucket: {0}, ErrorCode: {1}, Exception: {2}", this.RootContainer, ex.ErrorCode, ex.Message);
                    throw new ObymobiException(CloudStorageProviderResult.StorageOperationFailed, ex);
                }
            }
            catch (Exception ex)
            {
                this.Log("Failed to write file to Amazon S3. Exception: {0}", ex.Message);
                sw.Stop();
                throw;
            }

            return success;
        }

        public override bool WriteFile(string container, string path, string content, int? maxAgeCache)
        {
            return this.WriteFile(container, path, content, maxAgeCache, false);
        }

        public override bool WriteFile(string container, string path, string content, int? maxAgeCache, bool privateAccess)
        {
            byte[] contentInBytes = Encoding.UTF8.GetBytes(content);
            using (MemoryStream stream = new MemoryStream(contentInBytes))
            {
                return this.WriteFile(container, path, stream, maxAgeCache, privateAccess);
            }
        }

        public override void WriteFile(CloudProcessingTaskEntity task, Stream stream, int? maxAgeCache)
        {
            // Check if this task is already completed
            if (task.CompletedOnAmazonUTC.HasValue)
                return;

            this.WriteFile(task.Container, task.PathFormat, stream, maxAgeCache, false);
            task.CompletedOnAmazonUTC = DateTime.UtcNow;
        }

        public override void WriteFile(CloudProcessingTaskEntity task, string content, int? maxAgeCache)
        {
            // Check if this task is already completed
            if (task.CompletedOnAmazonUTC.HasValue)
                return;

            this.WriteFile(task.Container, task.PathFormat, content, maxAgeCache);
            task.CompletedOnAmazonUTC = DateTime.UtcNow;
        }

        public override bool IsStorageProviderAvailable(out string details)
        {
            details = string.Empty;

            try
            {
                GetObjectResponse getObjectResponse = null;
                using (IAmazonS3 client = this.GetStorageAccount())
                {
                    if (AmazonS3Util.DoesS3BucketExist(client, this.RootContainer))
                    {
                        ContainerInfo containerInfo;
                        if (!this.containers.TryGetValue(this.RootContainer, out containerInfo))
                        {
                            containerInfo = new ContainerInfo(this.RootContainer);
                            this.containers.Add(this.RootContainer, containerInfo);

                            //Log("Found bucket: {0}", bucket.BucketName);
                        }
                    }
                }

                this.CreateBucket();
            }
            catch (AmazonS3Exception ex)
            {
                details = string.Format("Error initializing Amazon S3. ErrorCode: {0}, Exception: {1}", ex.ErrorCode, ex.Message);

            }

            return (details == string.Empty);
        }

        public override void DeleteFile(string containerName, string path, bool isPrefix)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            try
            {
                // Create container if it doesn't already exists
                this.CreateBucket();

                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                {
                    BucketName = this.RootContainer,
                    Key = this.GetKey(containerName, path)
                };

                using (IAmazonS3 client = this.GetStorageAccount())
                    client.DeleteObject(deleteObjectRequest);

                sw.Stop();
                this.Log("Deleted file '{0}/{1}/{2}' from AmazonS3 in {3}ms", this.RootContainer, containerName, path, sw.ElapsedMilliseconds);
            }
            catch (Exception ex)
            {
                this.Log("Failed to delete file '{0}'. Exception: {1}", path, ex.Message);
                sw.Stop();

                throw;
            }
        }

        public override void DeleteFile(CloudProcessingTaskEntity task)
        {
            if (!task.CompletedOnAmazonUTC.HasValue)
            {
                this.DeleteFile(task.Container, task.PathFormat, false);
                task.CompletedOnAmazonUTC = DateTime.UtcNow;
            }
        }

        /// <summary>
        /// Retrieves the Media Files in a Dictionary and groups them by (key) MediaRatioTypeMediaId        
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        public override Dictionary<int, List<string>> GetMediaFilesForCleanup(string containerName)
        {
            // GK This code is duplicated in the AzureStorageProvider
            this.CreateBucket();

            Stopwatch swList = new Stopwatch();
            Stopwatch swParse = new Stopwatch();

            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = this.RootContainer;
            request.Prefix = containerName;
            Dictionary<int, List<string>> toReturn = new Dictionary<int, List<string>>();

            int returnedCount;
            int itemCount = 0;
            int subStringSetOff = containerName.Length + 1;
            // Strip the 'containerName' because upon deletion it would be used with 
            // it's containerName ending in [containerName]/[containerName]/
            using (IAmazonS3 client = this.GetStorageAccount())
            {
                do
                {
                    swList.Start();
                    ListObjectsResponse response = client.ListObjects(request);
                    swList.Stop();

                    returnedCount = response.S3Objects.Count;
                    itemCount += returnedCount;

                    swParse.Start();

                    foreach (S3Object item in response.S3Objects)
                    {
                        string path = item.Key.Substring(subStringSetOff);
                        string fileName = Path.GetFileName(path);
                        string mediaRatioTypeMediaIdString = StringUtil.GetAllBeforeFirstOccurenceOf(fileName, '-', true);
                        int mediaRatioTypeMediaId;
                        if (!mediaRatioTypeMediaIdString.IsNullOrWhiteSpace() && int.TryParse(mediaRatioTypeMediaIdString, out mediaRatioTypeMediaId))
                        {
                            // Got a valid id
                            if (!toReturn.ContainsKey(mediaRatioTypeMediaId))
                                toReturn.Add(mediaRatioTypeMediaId, new List<string>());

                            toReturn[mediaRatioTypeMediaId].Add(path);
                        }
                    }
                    swParse.Stop();

                    // If response is truncated, set the marker to get the next 
                    // set of keys.
                    if (response.IsTruncated)
                    {
                        request.Marker = response.NextMarker;
                    }
                    else
                    {
                        request = null;
                    }
                } while (request != null);
            }

            this.Log("List Blobs in: {0}", swList.Elapsed);
            this.Log("Parsed Blobs {0} (of which {1} unique) in: {2}", itemCount, toReturn.Keys.Count, swParse.Elapsed);

            return toReturn;
        }

        public override List<string> ListFiles(string containerName)
        {
            this.CreateBucket();

            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = this.RootContainer;
            request.Prefix = containerName;

            List<string> fileList = new List<string>();
            int subStringSetOff = containerName.Length + 1;
            using (IAmazonS3 client = this.GetStorageAccount())
            {
                ListObjectsResponse response = client.ListObjects(request);
                foreach (S3Object item in response.S3Objects)
                {
                    string path = item.Key.Substring(subStringSetOff);
                    string fileName = Path.GetFileName(path);

                    fileList.Add(fileName);
                }
            }

            return fileList;
        }

        public override string GetUrl(string containerName, string path)
        {
            return "https://{0}.s3.amazonaws.com/{1}/{2}".FormatSafe(this.RootContainer, containerName, path);
        }

        private string GetKey(string containerName, string path)
        {
            path = path.Replace("\\", "/"); // Required for MediaTaskPoller

            return containerName + "/" + path;
        }

        #endregion

        private void CreateBucket()
        {
            if (!this.containers.ContainsKey(this.RootContainer))
            {
                // Create new bucket on Amazon S3
                try
                {
                    PutBucketRequest putBucketRequest = new PutBucketRequest
                    {
                        BucketName = this.RootContainer,
                        BucketRegion = S3Region.EU
                    };

                    using (IAmazonS3 client = this.GetStorageAccount())
                        client.PutBucket(putBucketRequest);

                    this.Log("Created new bucket: {0}", this.RootContainer);

                    // Mark container as exists on cloud
                    ContainerInfo containerInfo = new ContainerInfo(this.RootContainer);
                    this.containers.Add(this.RootContainer, containerInfo);
                    containerInfo.ContainerExists = true;
                }
                catch (AmazonS3Exception ex)
                {
                    this.Log("Failed to create bucket. ErrorCode {0}, Exception: {1}", ex.ErrorCode, ex.Message);
                }
            }
        }

        private IAmazonS3 GetStorageAccount()
        {
            string awsAccessKey = CloudConfigurationManager.GetSetting(ObymobiConstants.AmazonS3AwsAccessKeySettingName);
            string awsSecretAccessKey = CloudConfigurationManager.GetSetting(ObymobiConstants.AmazonS3AwsSecretAccessKeySettingName);

            return this.GetStorageAccount(awsAccessKey, awsSecretAccessKey);
        }

        private IAmazonS3 GetStorageAccount(string awsAccessKey, string awsSecretAccessKey)
        {
            if (this.s3Client == null)
            {
                if ((awsAccessKey == string.Empty || awsSecretAccessKey == string.Empty) && WebEnvironmentHelper.CloudEnvironment != CloudEnvironment.Manual)
                {
                    throw new ObymobiException(CloudStorageProviderResult.ConfigurationMissingOrInvalid);
                }

                this.s3Client = AWSClientFactory.CreateAmazonS3Client(awsAccessKey, awsSecretAccessKey, RegionEndpoint.APSoutheast1);
            }

            return this.s3Client;
        }

        protected override sealed void Log(string format, params object[] args)
        {
            string log = "[Amazon] " + format;
            base.Log(log, args);
        }
    }
}
