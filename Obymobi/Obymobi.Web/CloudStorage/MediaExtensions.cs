﻿using CloudStorage;
using Dionysos;
using Obymobi.Constants;
using Obymobi.Enums;
using Obymobi.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Obymobi.Web.CloudStorage
{
    public static class MediaExtensions
    {
        public const string GenericFilesSubPath = "generic";
        public const string BrandFilesSubPath = "brand";
        public const string FILENAME_FORMAT_TICKS = "[ticks]"; // MUST BE LOWER CASE!

        public static byte[] Download(this IMediaEntity mediaEntity)
        {
            byte[] bytes = new byte[0];
            if (mediaEntity.LastDistributedVersionTicks.HasValue)
            {
                if (mediaEntity.LastDistributedVersionTicksAmazon.HasValue)
                {
                    bytes = DownloadImageFromCdn(MediaExtensions.GetCdnBaseUrl(CloudStorageProviderIdentifier.Amazon, mediaEntity.GetMediaPath(FileNameType.Cdn)));
                }
            }
            return bytes;
        }

        public async static Task<byte[]> DownloadAsync(this IMediaEntity mediaEntity)
        {
            byte[] bytes = new byte[0];
            if (mediaEntity.LastDistributedVersionTicks.HasValue)
            {
                if (mediaEntity.LastDistributedVersionTicksAmazon.HasValue)
                {
                    bytes = await DownloadImageFromCdnAsync(MediaExtensions.GetCdnBaseUrl(CloudStorageProviderIdentifier.Amazon, mediaEntity.GetMediaPath(FileNameType.Cdn)));
                }
            }
            return bytes;
        }

        public static bool Upload(this IMediaEntity mediaEntity, byte[] file)
        {
            string path = mediaEntity.GetMediaPath(FileNameType.Format);
            path = path.Replace(FILENAME_FORMAT_TICKS, mediaEntity.LastDistributedVersionTicks.Value.ToString(CultureInfo.InvariantCulture));

            return CloudUploader.UploadMedia(mediaEntity, ObymobiConstants.CloudContainerMediaFiles, path, file);
        }

        public static string GetCdnBaseUrl(CloudStorageProviderIdentifier storageLocation, string path)
        {
            string url = string.Empty;

            switch (WebEnvironmentHelper.CloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.PrimaryAmazonCdnBaseUrlFiles;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                case CloudEnvironment.Test:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.TestAmazonCdnBaseUrlFiles;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                case CloudEnvironment.Development:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.DevelopmentAmazonCdnBaseUrlFiles;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                case CloudEnvironment.Staging:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.StagingAmazonCdnBaseUrlFiles;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                case CloudEnvironment.Manual:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.ManualAmazonCdnBaseUrlFiles;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                default:
                    throw new NotImplementedException(WebEnvironmentHelper.CloudEnvironment.ToString());
            }

            if (!path.IsNullOrWhiteSpace())
                url = WebEnvironmentHelper.CombinePaths(url, path);

            return url;
        }

        public static string GetMediaPath(this IMediaEntity mediaEntity, FileNameType fileNameType)
        {
            string toReturn = string.Empty;

            List<string> pathComponents = new List<string>();

            if (mediaEntity.IsGeneric)
                pathComponents.Add(MediaExtensions.GenericFilesSubPath);
            else if (mediaEntity.GetRelatedBrandId().HasValue)
            {
                pathComponents.Add(MediaExtensions.BrandFilesSubPath);
                pathComponents.Add(mediaEntity.GetRelatedBrandId().ToString());
            }
            else
                pathComponents.Add(mediaEntity.GetRelatedCompanyId().ToString());

            pathComponents.Add("original");

            // Get the FileNameFormat
            pathComponents.Add(mediaEntity.GetFileName(fileNameType));

            toReturn = string.Join("\\", pathComponents);

            return toReturn;
        }

        public static byte[] DownloadImageFromCdn(string url)
        {
            using (MyWebClient webClient = new MyWebClient())
            {
                try
                {
                    return webClient.DownloadData(url);
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(ex.Message);
                    Trace.WriteLine(ex.ProcessStackTrace());
                    // Ignored
                }
            }
            return new byte[0];
        }

        public async static Task<byte[]> DownloadImageFromCdnAsync(string url)
        {
            using (MyWebClient webClient = new MyWebClient())
            {
                try
                {
                    return await webClient.DownloadDataTaskAsync(url);
                }
                catch (Exception ex)
                {
                    // Ignored
                }
            }
            return new byte[0];
        }

        private class MyWebClient : WebClient
        {
            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = 5 * 1000;
                return w;
            }
        }
    }
}
