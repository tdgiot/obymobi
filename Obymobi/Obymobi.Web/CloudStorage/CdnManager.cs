﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Dionysos.Security.Cryptography;
using Obymobi.Enums;
using Obymobi.Logging;
using Obymobi.Logic.Model;
using Obymobi.Web.CloudStorage;
using Obymobi.Web.CloudStorage.Queue;

public class CdnManager
{
    #region  Fields

    private static CdnManager sInstance;

    #endregion

    #region Properties

    private string RootContainer { get; set; }
    private List<CloudStorageProvider> CloudStorageProviders { get; set; }
    private CloudStorageProvider PrimaryCloudStorageProvider { get; set; }

    #endregion

    #region Methods

    #region Private methods

    private List<CloudStorageProvider> InitializeAndGetStorageProviders(List<CloudStorageAccount> cloudStorageAccounts)
    {
        List<CloudStorageProvider> storageProviders = new List<CloudStorageProvider>();
        foreach (CloudStorageAccount cloudStorageAccount in cloudStorageAccounts)
        {
            CloudStorageProvider storageProvider = null;
            try
            {
                if (cloudStorageAccount.Type == (int)CloudStorageAccountType.Amazon)
                {
                    string decryptedPassword = Cryptographer.DecryptUsingCBC(cloudStorageAccount.AccountPassword);
                    storageProvider = new AmazonCloudStorageProvider(cloudStorageAccount.AccountName.Trim(), decryptedPassword.Trim(), this.RootContainer, GlobalLight.LoggingProvider);
                }

                if (storageProvider != null)
                {
                    string details;
                    bool available = storageProvider.IsStorageProviderAvailable(out details);
                    ConsoleLogger.WriteToLog("InitializeAndGetStorageProviders - CloudStorageAccountId '{0}' - Storage Provider Available: {1} - {2}", cloudStorageAccount.CloudStorageAccountId, available, details);
                    if (available)
                    {
                        storageProviders.Add(storageProvider);

                        if (this.PrimaryCloudStorageProvider == null)
                        {
                            this.PrimaryCloudStorageProvider = storageProvider;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ConsoleLogger.WriteToLog("InitializeAndGetStorageProviders - Failed to initialize provider of type: {0}, CloudStorageId: {1}", cloudStorageAccount.Type, cloudStorageAccount.CloudStorageAccountId);
            }
        }
        return storageProviders;
    }

    #endregion

    #endregion

    #region Contructors

    private CdnManager()
    {
        CloudStorageProviders = new List<CloudStorageProvider>();
    }

    public static CdnManager Instance
    {
        get { return CdnManager.sInstance ?? (CdnManager.sInstance = new CdnManager()); }
    }

    #endregion

    #region Public methods

    public void Initialize(CloudStorageAccount[] cloudStorageAccounts, string rootContainer)
    {
        if (rootContainer.IsNullOrWhiteSpace())
        {                        
            ConsoleLogger.WriteToLog("CdnManager - Initialize - Unable to initialize storage providers. No CDN root container or salt setup");
            return;
        }

        try
        {
            this.RootContainer = rootContainer;            
            this.CloudStorageProviders = this.InitializeAndGetStorageProviders(cloudStorageAccounts.ToList());
        }
        catch (Exception ex)
        {
            ConsoleLogger.WriteToLog("Unable to initialize CDN manager. Exception: " + ex.Message);
        }
    }

    public bool IsProviderAvailable()
    {
        return this.CloudStorageProviders.Count > 0;
    }    

    public void UploadAsync(string container, string key, string content, bool privateAccess)
    {
        if (!this.IsProviderAvailable())
            return;

        try
        {
            CloudStorageQueue.Enqueue(new CloudStorageQueueUploadItem(this.CloudStorageProviders, container, key, content, privateAccess));
        }
        catch (Exception ex)
        {
            ConsoleLogger.WriteToLog("CdnManager - Upload", "Exception occured while uploading {0}", ex.Message);
        }
    }

    public bool UploadSync(string container, string key, string content)
    {
        if (!this.IsProviderAvailable())
            return false;
        
        bool result = false;
        try
        {
            foreach (CloudStorageProvider provider in this.CloudStorageProviders)
            {
                if (provider.WriteFile(container, key, content, null))
                {
                    result = true;
                }
            }
        }
        catch (Exception ex)
        {
            ConsoleLogger.WriteToLog("CdnManager - UploadSync", "Exception occured while uploading {0}", ex.Message);
        }        
        return result;
    }

    public List<string> GetUrl(string container, string key)
    {
        List<string> urls = new List<string>();
        try
        {
            foreach (CloudStorageProvider provider in this.CloudStorageProviders)
            {
                urls.Add(provider.GetUrl(container, key));
            }
        }
        catch (Exception ex)
        {
            ConsoleLogger.WriteToLog("CdnManager - GetUrl", "Exception occured while retrieving url {0}", ex.Message);
        }
        return urls;
    }

    #endregion
}