﻿using System;
using System.Collections.Generic;
using System.Linq;
using Amazon;
using Amazon.IdentityManagement;
using Amazon.IdentityManagement.Model;
using Dionysos;
using Dionysos.Security.Cryptography;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Web.CloudStorage
{
    public class AmazonCloudStorageHelper
    {
        public static void CreateAmazonUserPerCompany()
        {
            CompanyCollection companiesWithoutAmazonUser = AmazonCloudStorageHelper.GetCompaniesWithPrefetchedCloudStorageAccounts();
            AmazonCloudStorageHelper.CreateAmazonUserForCompanies(companiesWithoutAmazonUser);
        }

        private static void CreateAmazonUserForCompanies(CompanyCollection companies)
        {
            foreach (CompanyEntity company in companies)
            {
                CloudStorageAccountEntity amazonAccount = company.CloudStorageAccountCollection.SingleOrDefault(csa => csa.Type == CloudStorageAccountType.Amazon);
                if (amazonAccount == null)
                {
                    string username, accessKey, accessKeySecret;
                    if (AmazonCloudStorageHelper.CreateAmazonUser(company, out username, out accessKey, out accessKeySecret))
                    {
                        CloudStorageAccountEntity storageAccount = AmazonCloudStorageHelper.CreateCloudStorageAccount(CloudStorageAccountType.Amazon, company.CompanyId, username, accessKey, accessKeySecret);
                        storageAccount.Save();
                    }
                }
            }
        }

        public static CloudStorageAccountEntity CreateCloudStorageAccount(CloudStorageAccountType type, int companyId, string userName, string accessKey, string accessSecretKey)
        {
            CloudStorageAccountEntity cloudStorageAccount = new CloudStorageAccountEntity();
            cloudStorageAccount.Type = type;
            cloudStorageAccount.CompanyId = companyId;
            cloudStorageAccount.UserName = userName;
            cloudStorageAccount.AccountName = accessKey;
            cloudStorageAccount.AccountPassword = accessSecretKey;

            return cloudStorageAccount;
        }

        private static CompanyCollection GetCompaniesWithPrefetchedCloudStorageAccounts()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathCloudStorageAccountCollection);

            CompanyCollection companies = new CompanyCollection();
            companies.GetMulti(null, prefetch);

            return companies;
        }

        public static void CreateAmazonGroup()
        {
            AmazonIdentityManagementServiceClient client = AmazonCloudStorageHelper.CreateClient();

            string groupname = AmazonCloudStorageHelper.GetAmazonGroupName();
            string policyName = AmazonCloudStorageHelper.GetAmazonPolicyName();
            string policyDocument = AmazonCloudStorageHelper.GetAmazonPolicyDocument();

            CreateGroupRequest createGroupReq = new CreateGroupRequest();
            createGroupReq.GroupName = groupname;

            PutGroupPolicyRequest putGroupPolicyReq = new PutGroupPolicyRequest();
            putGroupPolicyReq.GroupName = groupname;
            putGroupPolicyReq.PolicyName = policyName;
            putGroupPolicyReq.PolicyDocument = policyDocument;

            CreateGroupResponse createGroupResponse = client.CreateGroup(createGroupReq);
            PutGroupPolicyResponse putGroupPolicyResponse = client.PutGroupPolicy(putGroupPolicyReq);
        }

        public static bool CreateAmazonUser(CompanyEntity company, out string userName, out string accessKey, out string accessKeySecret)
        {
            bool success;
            try
            {
                AmazonIdentityManagementServiceClient client = AmazonCloudStorageHelper.CreateClient();

                string newUsername = string.Format(ObymobiConstants.AmazonS3AwsCompanyUsername, WebEnvironmentHelper.GetEnvironmentPrefixForCdn(), company.CompanyId);
                string groupname = AmazonCloudStorageHelper.GetAmazonGroupName();

                CreateUserRequest createUserReq = new CreateUserRequest();
                createUserReq.UserName = newUsername;

                CreateAccessKeyRequest createAccessKeyReq = new CreateAccessKeyRequest();
                createAccessKeyReq.UserName = newUsername;

                AddUserToGroupRequest addUserToGroupReq = new AddUserToGroupRequest();
                addUserToGroupReq.UserName = newUsername;
                addUserToGroupReq.GroupName = groupname;

                CreateUserResponse createUserResponse = client.CreateUser(createUserReq);
                CreateAccessKeyResponse createAccessKeyResponse = client.CreateAccessKey(createAccessKeyReq);
                AddUserToGroupResponse addUserToGroupResposne = client.AddUserToGroup(addUserToGroupReq);

                accessKey = createAccessKeyResponse.AccessKey.AccessKeyId;
                accessKeySecret = createAccessKeyResponse.AccessKey.SecretAccessKey;
                userName = newUsername;

                success = true;
            }
            catch (Exception ex)
            {
                accessKey = "";
                accessKeySecret = "";
                userName = "";

                throw;
            }
            return success;
        }

        public static void RemoveUsersFromAmazon()
        {
            Group group;
            List<User> users;
            if (AmazonCloudStorageHelper.GetAmazonGroupAndUsers(out group, out users))
            {
                AmazonCloudStorageHelper.RemoveUsersFromGroup(users, group.GroupName);
                AmazonCloudStorageHelper.RemoveUsers(users);
                AmazonCloudStorageHelper.RemoveGroup(group);
            }
        }

        private static void RemoveUsersFromGroup(List<User> users, string groupName)
        {
            AmazonIdentityManagementServiceClient client = AmazonCloudStorageHelper.CreateClient();

            foreach (User user in users)
            {
                RemoveUserFromGroupRequest removeUserFromGroupReq = new RemoveUserFromGroupRequest();
                removeUserFromGroupReq.UserName = user.UserName;
                removeUserFromGroupReq.GroupName = groupName;

                client.RemoveUserFromGroup(removeUserFromGroupReq);
            }
        }

        private static void RemoveUsers(List<User> users)
        {
            AmazonIdentityManagementServiceClient client = AmazonCloudStorageHelper.CreateClient();

            foreach (User user in users)
            {
                DeleteUserRequest deleteUserReq = new DeleteUserRequest();
                deleteUserReq.UserName = user.UserName;

                client.DeleteUser(deleteUserReq);
            }
        }

        private static void RemoveGroup(Group group)
        {
            AmazonIdentityManagementServiceClient client = AmazonCloudStorageHelper.CreateClient();

            DeleteGroupRequest deleteGroupReq = new DeleteGroupRequest();
            deleteGroupReq.GroupName = group.GroupName;

            client.DeleteGroup(deleteGroupReq);
        }

        public static List<User> GetAmazonUsersToBeRemoved()
        {
            List<User> users = new List<User>();

            Group group;
            List<User> groupUsers;
            if (AmazonCloudStorageHelper.GetAmazonGroupAndUsers(out group, out groupUsers))
            {
                if (groupUsers.Count > 0)
                {
                    string environmentPrefix = WebEnvironmentHelper.GetEnvironmentPrefixForCdn();
                    if (!environmentPrefix.IsNullOrWhiteSpace())
                    {
                        CloudStorageAccountCollection cloudStorageAccounts = AmazonCloudStorageHelper.GetCloudStorageAccounts();
                        foreach (User user in groupUsers)
                        {
                            if (user.UserName.StartsWith(environmentPrefix, StringComparison.InvariantCultureIgnoreCase))
                            {
                                CloudStorageAccountEntity cloudStorageAccount = cloudStorageAccounts.SingleOrDefault(c => c.UserName.Equals(user.UserName, StringComparison.InvariantCultureIgnoreCase));
                                if (cloudStorageAccount == null)
                                {
                                    // The amazon user is prefixed with this environment, but its username is not in the database, should be removed from amazon manually
                                    users.Add(user);
                                }
                            }
                        }
                    }
                }
            }

            return users;
        }

        private static CloudStorageAccountCollection GetCloudStorageAccounts()
        {
            CloudStorageAccountCollection cloudStorageAccounts = new CloudStorageAccountCollection();
            cloudStorageAccounts.GetMulti(null);

            return cloudStorageAccounts;
        }

        private static bool GetAmazonGroupAndUsers(out Group group, out List<User> users)
        {
            bool success;
            try
            {
                AmazonIdentityManagementServiceClient client = AmazonCloudStorageHelper.CreateClient();

                string groupname = AmazonCloudStorageHelper.GetAmazonGroupName();

                GetGroupRequest getGroupReq = new GetGroupRequest();
                getGroupReq.GroupName = groupname;

                GetGroupResponse getGroupResponse = client.GetGroup(getGroupReq);

                group = getGroupResponse.Group;
                users = getGroupResponse.Users;

                success = true;
            }
            catch (Exception ex)
            {
                group = null;
                users = null;

                throw new Exception("Unable to retrieve group and users from Amazon for removal: ", ex);
            }
            return success;
        }

        private static AmazonIdentityManagementServiceClient CreateClient()
        {
            if (WebEnvironmentHelper.CloudEnvironmentIsProduction)
            {
                return new AmazonIdentityManagementServiceClient(
                    Cryptographer.DecryptStringUsingRijndael(ObymobiConstants.ProductionAmazonS3AwsAdminAccessKeyEncrypted),
                    Cryptographer.DecryptStringUsingRijndael(ObymobiConstants.ProductionAmazonS3AwsAdminSecretAccessKeyEncrypted),
                    RegionEndpoint.USEast1);
            }

            return new AmazonIdentityManagementServiceClient(
                Cryptographer.DecryptStringUsingRijndael(ObymobiConstants.DevelopmentAmazonS3AwsAdminAccessKeyEncrypted),
                Cryptographer.DecryptStringUsingRijndael(ObymobiConstants.DevelopmentAmazonS3AwsAdminSecretAccessKeyEncrypted),
                RegionEndpoint.USEast1);
        }

        private static string GetAmazonGroupName()
        {
            if (WebEnvironmentHelper.CloudEnvironmentIsProduction)
            {
                return ObymobiConstants.AmazonS3AwsGroupname;
            }
            else
            {
                return string.Format("{0}-{1}", WebEnvironmentHelper.GetEnvironmentPrefixForCdn(), ObymobiConstants.AmazonS3AwsGroupname);
            }
        }

        private static string GetAmazonPolicyName()
        {
            if (WebEnvironmentHelper.CloudEnvironmentIsProduction)
            {
                return ObymobiConstants.AmazonS3AwsPolicyName;
            }
            else
            {
                return string.Format("{0}-{1}", WebEnvironmentHelper.GetEnvironmentPrefixForCdn(), ObymobiConstants.AmazonS3AwsPolicyName);
            }
        }

        private static string GetAmazonPolicyDocument()
        {
            if (WebEnvironmentHelper.CloudEnvironmentIsProduction)
            {
                return AmazonCloudStorageHelper.ProductionPolicyDocument.Trim();
            }
            else
            {
                return AmazonCloudStorageHelper.ProductionNonPolicyDocument.Trim();
            }
        }

        #region Policy Documents

        private class PolicyDocument
        {
            public string Version { get; set; }
            public string Statement { get; set; }

        }

        private static string ProductionNonPolicyDocument
        {
            get { return @"
                { 
                    ""Version"": ""2012-10-17"", 
                    ""Statement"": [ 
                        { 
                            ""Action"": ""s3:*"", 
                            ""Effect"": ""Allow"", 
                            ""Resource"": [
                                ""arn:aws:s3:::cravecloud-demo/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-dev/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-dk/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-es/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-fo/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-gk/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-jh/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-mb/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-test/companies/${aws:username}/*""
                                        ] 
                        } 
                    ]
                }"; }
        }

        private static string ProductionPolicyDocument
        {
            get
            {
                return @"
                { 
                    ""Version"": ""2012-10-17"", 
                    ""Statement"": [ 
                        { 
                            ""Action"": ""s3:*"", 
                            ""Effect"": ""Allow"", 
                            ""Resource"": [
                                ""arn:aws:s3:::cravecloud-demo/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-dev/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-dk/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-es/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-fo/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-gk/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-jh/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-mb/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-test/companies/${aws:username}/*"",
                                ""arn:aws:s3:::cravecloud-prod/companies/${aws:username}/*""
                                        ] 
                        } 
                    ]
                }";
            }
        }

        #endregion
    }
}
