﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Logic.Model;

namespace Obymobi.Web.CloudStorage
{
    public class DataManager
    {
        #region Methods

        public static ProductmenuItem[] GetMenu()
        {
            ProductmenuItem[] productmenuItems = new ProductmenuItem[1];

            ProductmenuItem item = new ProductmenuItem(1, "test", 2, 1);
            productmenuItems[0] = item;

            return productmenuItems;
        }

        #endregion
    }
}
