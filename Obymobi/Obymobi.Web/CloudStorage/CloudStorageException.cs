﻿using System;

namespace Obymobi.Web.CloudStorage
{
    /// <summary>
    /// Exception class for when the communication with a POS fails
    /// </summary>
    public class CloudStorageException : ObymobiException
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.POS.POSException class
        /// </summary>	
        public CloudStorageException(CloudStorageProviderResult errorType, string message)
            : base(errorType, message)
        {
            this.errorType = errorType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliverypointLockedException"/> class.
        /// </summary>
        /// <param name="errorType">Type of the error.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public CloudStorageException(CloudStorageProviderResult errorType, string message, params object[] args)
            : base(errorType, message, args)
        {
            this.errorType = errorType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliverypointLockedException"/> class.
        /// </summary>
        /// <param name="errorType">Type of the error.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public CloudStorageException(CloudStorageProviderResult errorType, Exception innerException, string message, params object[] args)
            : base(errorType, innerException, message, args)
        {
            this.errorType = errorType;
        }

        private CloudStorageProviderResult errorType = CloudStorageProviderResult.UnspecifiedError;
        /// <summary>
        /// Gets or sets the type of the error.
        /// </summary>
        /// <value>
        /// The type of the error.
        /// </value>
        public CloudStorageProviderResult ErrorType
        {
            get
            {
                return this.errorType;
            }
            set
            {
                this.errorType = value;
            }
        }

        /// <summary>
        /// Throws the typed error for a POS exception (i.e. DeliverypointLocked, Connectivety, etc.), otherwise the generic is thrown.
        /// </summary>
        /// <param name="errorType">Type of the error.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public static void ThrowTyped(CloudStorageProviderResult errorType, Exception innerException, string message, params object[] args)
        {
            if (args == null)
                args = new string[1] { string.Empty };

            throw new CloudStorageException(errorType, innerException, message, args);
        }

        #endregion
    }
}
