﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.CloudStorage
{
    public class UploadResult
    {
        #region Constructor

        public UploadResult()
        {
            this.Success = false;
            this.Message = string.Empty;
            this.Exception = null;
        }

        #endregion

        #region Properties

        public bool Success { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }

        #endregion
    }
}
