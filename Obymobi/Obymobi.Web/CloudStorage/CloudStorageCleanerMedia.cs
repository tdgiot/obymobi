﻿using Dionysos;
using Dionysos.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Web.CloudStorage;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using SD.LLBLGen.Pro.QuerySpec.SelfServicing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

public class CloudStorageCleanerMedia
{
    CloudStorageAccount cloudStorageAccount;
    CloudStorageProvider provider; 

    private bool verboseLogging;
    private string containerName;
    private ILoggingProvider loggingProvider;

    public CloudStorageCleanerMedia(ILoggingProvider loggingProvider, bool verboseLogging)
    {
        this.verboseLogging = verboseLogging;        
        this.containerName = ObymobiConstants.CloudContainerMediaFiles;
        this.loggingProvider = loggingProvider;
    }

    public void Clean()
    {
        // Amazon
        this.provider = new AmazonCloudStorageProvider(this.loggingProvider);
        this.PerformCleaning();        
    }

    private void PerformCleaning()
    {
        // Fetch all the files / blobs
        this.GarbageCollect();
        long before = System.Diagnostics.Process.GetCurrentProcess().VirtualMemorySize64;
        Dictionary<int, List<string>> blobs = this.provider.GetMediaFilesForCleanup(this.containerName);
        long after = System.Diagnostics.Process.GetCurrentProcess().VirtualMemorySize64;
        this.loggingProvider.Information("Memory usage before (blobs): {0}, after: {1}, difference: {2}", before, after, (after - before) / 1024 / 1024);

        // Fetch all the MediaRatioTypeMediaIds
        this.GarbageCollect();
        before = System.Diagnostics.Process.GetCurrentProcess().VirtualMemorySize64;
        List<int> MediaRatioTypeMediaIds = this.GetMediaRatioTypeMediaIds();

        // Fetch all the MediaRatioTypeMediaIds waiting for deletion (no need to delete them now);
        MediaRatioTypeMediaIds.AddRange(this.GetMediaRatioTypeMediaIdsToBeDeleted());
        after = System.Diagnostics.Process.GetCurrentProcess().VirtualMemorySize64;
        this.loggingProvider.Information("Memory usage before (entities): {0}, after: {1}, difference: {2}", before, after, (after - before) / 1024 / 1024);

        // Clean up orphan files (no longer existing in database)
        this.DeleteOrphanFiles(blobs, MediaRatioTypeMediaIds);
    }

    private void GarbageCollect()
    {
        GC.Collect();
        GC.WaitForPendingFinalizers();
        GC.Collect();
    }

    private void DeleteOrphanFiles(Dictionary<int, List<string>> blobs, List<int> mediaRatioTypeMediaIds)
    {
        var keys = blobs.Keys.ToArray();
        for (int iKey = (keys.Length - 1); iKey >= 0; iKey--)
        {
            int mediaRatioTypeMediaId = keys[iKey];
            List<string> fileNames = blobs[mediaRatioTypeMediaId];
            if (!mediaRatioTypeMediaIds.Contains(mediaRatioTypeMediaId))
            {
                // Orphan found
                this.Log("Found Orphan: {0}", mediaRatioTypeMediaId);

                foreach (string name in fileNames)
                {
                    this.Log(" - Delete: {0}", name);
                    this.provider.DeleteFile(this.containerName, name, false);
                }

                // Remove, no need to check for previous versions, all is deleted
                blobs.Remove(mediaRatioTypeMediaId);
            }
            else if (fileNames.Count > 1)
            {
                this.Log("Found Previous Versions: {0}", mediaRatioTypeMediaId);

                Dictionary<string, long> filesAndTimestamps = new Dictionary<string, long>();
                foreach (string file in blobs[mediaRatioTypeMediaId])
                {
                    int lastDashIndex = file.LastIndexOf("-");
                    int extensionIndex = file.LastIndexOf(".");

                    if (lastDashIndex > 0 && extensionIndex > 0)
                    {
                        int timestampIndex = lastDashIndex+1;
                        string timestampString = file.Substring(timestampIndex, extensionIndex - timestampIndex);
                        long timestamp;

                        if (!timestampString.IsNullOrWhiteSpace() && long.TryParse(timestampString, out timestamp))
                        {
                            filesAndTimestamps.Add(file, timestamp);
                        }
                    }
                }

                // Remove it's previous versions if found
                List<string> filesSorted = filesAndTimestamps.OrderByDescending(x => x.Value).Select(x => x.Key).ToList();

                // Delete all but the first 2 (grace period for 1 version)
                for (int iFile = 0; iFile < filesSorted.Count; iFile++)
                {
                    if (iFile <= 1)
                    {
                        this.Log(" - Kept file: {0}", filesSorted[iFile]);
                    }
                    else
                    {
                        this.Log(" - Deleted file: {0}", filesSorted[iFile]);
                        this.provider.DeleteFile(this.containerName, filesSorted[iFile], false);
                    }
                }
            }
        }
    }

    private List<int> GetMediaRatioTypeMediaIds()
    {
        // Retrieve all ids of existing MediaRatioTypeMedia entities        
        Stopwatch sw = new Stopwatch();
        sw.Start();
        var qf = new QueryFactory();
        DynamicQuery query = qf.Create()
                   .Select(MediaRatioTypeMediaFields.MediaRatioTypeMediaId);

        TypedListDAO dao = new TypedListDAO();
        DataTable dt = dao.FetchAsDataTable(query);

        List<int> toReturn = new List<int>();

        if (dt != null)
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row[0] != null && row[0] != DBNull.Value)
                {
                    toReturn.Add((int)row[0]);
                }
            }
        }
        sw.Stop();
        this.Log("Retrieved existing MediaRatioTypeMediaIds in {0}", sw.Elapsed);
        return toReturn;
    }

    private List<int> GetMediaRatioTypeMediaIdsToBeDeleted()
    {
        Stopwatch sw = new Stopwatch();
        sw.Start();

        var qf = new QueryFactory();
        DynamicQuery query = qf.Create()
                            .Select(MediaProcessingTaskFields.MediaRatioTypeMediaIdNonRelationCopy)
                            .Where(MediaProcessingTaskFields.Action == MediaProcessingTaskEntity.ProcessingAction.Delete);

        TypedListDAO dao = new TypedListDAO();
        DataTable dt = dao.FetchAsDataTable(query);

        List<int> toReturn = new List<int>();

        if (dt != null)
        {
            foreach (DataRow row in dt.Rows)
            {
                if (row[0] != null && row[0] != DBNull.Value)
                {
                    toReturn.Add((int)row[0]);
                }
            }
        }

        sw.Stop();
        this.Log("Retrieved to be deleted MediaRatioTypeMediaIds in {0}", sw.Elapsed);

        return toReturn;
    }

    private void Log(string format, params object[] args)
    {
        if (this.verboseLogging)
            this.loggingProvider.Debug(format, args);
    }
}