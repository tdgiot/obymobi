﻿using Dionysos;
using Dionysos.Logging;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Obymobi.Web.CloudStorage
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class CloudStorageProvider : ICloudStorageProvider
    {
        public const string CLOUD_MANUAL_CONTAINER_KEY = "CloudManualContainer";

        private readonly ILoggingProvider loggingProvider;

        protected CloudStorageProvider(ILoggingProvider loggingProvider)
        {
            this.loggingProvider = loggingProvider;
        }

        /// <summary>
        /// Used to known which StorageProvider it is to update the relevant fields in the MediaProcessingTask
        /// </summary>
        /// <returns></returns>
        public abstract CloudStorageProviderIdentifier Identifier { get; }

        /// <summary>
        /// Check if the File Exists (either via the internal mechanisms of via the public url)
        /// </summary>
        /// <param name="container"></param>
        /// <param name="path">The path.</param>
        /// <param name="publicAccess">if set to <c>true</c> it's validated that the file is available on the public url, otherwise directly via the (internal) storage system.</param>
        /// <returns></returns>
        public abstract bool FileExists(string container, string path, bool publicAccess);

        /// <summary>
        /// Check if the File needs to be uploaded to the CloudStorageProvider
        /// </summary>
        /// <param name="container"></param>
        /// <param name="path">The path.</param>
        /// <param name="md5">The md5 hash of the file to check</param>
        /// <returns>Will return TRUE if it's either non-existent or the MD5 is different</returns>
        public abstract bool FileRequiresUpload(string container, string path, string md5);

        /// <summary>
        /// Reads the file.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="path">The path.</param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public abstract bool ReadFile(string container, string path, out MemoryStream stream);

        /// <summary>
        /// Tests the file access.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public abstract string TestFileAccess(string container, string path);

        /// <summary>
        /// Writes the file.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="path">The path.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="maxAgeCache">The max age cache (null == non-specifiec, 0 = don't cache, >= 1 is cache time in seconds).</param>
        /// <param name="privateAccess">Private access to the file to upload or not.</param>
        /// <returns></returns>
        public abstract bool WriteFile(string container, string path, Stream stream, int? maxAgeCache, bool privateAccess);

        /// <summary>
        /// Writes the file.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="path">The path.</param>
        /// <param name="content">The content.</param>
        /// <param name="maxAgeCache">The max age cache (null == non-specifiec, 0 = don't cache, >= 1 is cache time in seconds).</param>
        /// <returns></returns>
        public abstract bool WriteFile(string container, string path, string content, int? maxAgeCache);

        /// <summary>
        /// Writes the file.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="path">The path.</param>
        /// <param name="content">The content.</param>
        /// <param name="maxAgeCache">The max age cache (null == non-specifiec, 0 = don't cache, >= 1 is cache time in seconds).</param>
        /// <param name="privateAccess">Private access to the file to upload or not.</param>
        /// <returns></returns>
        public abstract bool WriteFile(string container, string path, string content, int? maxAgeCache, bool privateAccess);

        /// <summary>
        /// Write to cloud
        /// </summary>
        /// <param name="task"></param>
        /// <param name="stream">The stream.</param>
        /// <param name="maxAgeCache">The max age cache (null == non-specifiec, 0 = don't cache, >= 1 is cache time in seconds).</param>
        public abstract void WriteFile(CloudProcessingTaskEntity task, Stream stream, int? maxAgeCache);

        /// <summary>
        /// Write to cloud
        /// </summary>
        /// <param name="task"></param>
        /// <param name="content">The content.</param>
        /// <param name="maxAgeCache">The max age cache (null == non-specifiec, 0 = don't cache, >= 1 is cache time in seconds).</param>
        public abstract void WriteFile(CloudProcessingTaskEntity task, string content, int? maxAgeCache);

        /// <summary>
        /// Determines whether the storage provider is currentl;y available
        /// </summary>
        /// <param name="details">The details.</param>
        /// <returns>
        ///   <c>true</c> if [is storage provider available] [the specified details]; otherwise, <c>false</c>.
        /// </returns>
        public abstract bool IsStorageProviderAvailable(out string details);

        /// <summary>
        /// Deletes the file
        /// </summary>
        /// <param name="container"></param>
        /// <param name="path">The path.</param>
        /// <param name="isPrefix">If set to <c>true</c> all files that start with 'isPrefix' will be deleted. Don't include a * at the end.</param>
        public abstract void DeleteFile(string container, string path, bool isPrefix);

        /// <summary>
        /// Retrieves the Media Files in a Dictionary and groups them by (key) MediaRatioTypeMediaId
        /// It's indeed not uber-generic, but YAGNI/KISS.
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        public abstract Dictionary<int, List<string>> GetMediaFilesForCleanup(string container);

        /// <summary>
        /// Get list of files in a specific container
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        public abstract List<string> ListFiles(string container);

        /// <summary>
        /// Deletes the file
        /// </summary>
        public abstract void DeleteFile(CloudProcessingTaskEntity task);

        /// <summary>
        /// Gets the url of the file
        /// </summary>
        /// <param name="container"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public abstract string GetUrl(string container, string path);

        protected void ValidatePath(string path)
        {            
            if (path.IsNullOrWhiteSpace())
                throw new ObymobiException(CloudStorageProviderResult.InvalidPathName, "Path was empty");
            if (PathUtil.ContainsInvalidPathChars(path))
                throw new ObymobiException(CloudStorageProviderResult.InvalidPathName, "Invalid characters found in: '{0}'", path);
            if (path.StartsWith("\\\\"))
                throw new ObymobiException(CloudStorageProviderResult.InvalidPathName, "Path should not be a relative path, not a full network path: '{0}'", path);
            if (path.IndexOf(":", StringComparison.Ordinal) == 1)
                throw new ObymobiException(CloudStorageProviderResult.InvalidPathName, "Path should not be a relative path, not a drive path: '{0}'", path);
        }

        protected bool ValidateUrl(string url)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Proxy = null;
                request.Timeout = 5000; //set the timeout to 5 seconds to keep the user from waiting too long for the page to load
                request.Method = "HEAD"; //Get only the header information -- no need to download any content

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    int statusCode = (int)response.StatusCode;

                    if (statusCode >= 100 && statusCode < 400) //Good requests
                    {
                        return true;
                    }

                    if (statusCode >= 500 && statusCode <= 510) //Server Errors
                    {
                        Debug.WriteLine("The remote server has thrown an internal error. Url is not valid: {0}", url);
                        return false;
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError) //400 errors
                {
                    return false;
                }
                
                Debug.WriteLine("Unhandled status [{0}] returned for url: {1}", ex.Status, url);
            }
            catch
            {
                Debug.WriteLine("Could not test url {0}.", url);
            }

            return false;
        }

        protected virtual void Log(string format, params object[] args)
        {
            Debug.WriteLine(format, args);
            this.loggingProvider.Verbose(format.FormatSafe(args));
        }
    }
}
