﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Obymobi.Web.CloudStorage
{
    public interface ICloudStorageProvider
    {
        void DeleteFile(Obymobi.Data.EntityClasses.CloudProcessingTaskEntity task);
        void DeleteFile(string container, string path, bool isPrefix);
        bool FileExists(string container, string path, bool publicAccess);
        bool FileRequiresUpload(string container, string path, string md5);
        Obymobi.Enums.CloudStorageProviderIdentifier Identifier { get; }
        bool IsStorageProviderAvailable(out string details);
        bool ReadFile(string container, string path, out System.IO.MemoryStream stream);
        string TestFileAccess(string container, string path);
        void WriteFile(Obymobi.Data.EntityClasses.CloudProcessingTaskEntity task, System.IO.Stream stream, int? maxAgeCache);
        bool WriteFile(string container, string path, System.IO.Stream stream, int? maxAgeCache, bool privateAccess);
        bool WriteFile(string container, string path, string content, int? maxAgeCache, bool privateAccess);
        string GetUrl(string container, string path);
        
        /// <summary>
        /// Retrieves the Media Files in a Dictionary and groups them by (key) MediaRatioTypeMediaId
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        Dictionary<int, List<string>> GetMediaFilesForCleanup(string container);
    }
}
