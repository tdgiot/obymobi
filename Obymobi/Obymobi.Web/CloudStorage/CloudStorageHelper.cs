﻿//using Obymobi.Data;
//using Obymobi.Data.CollectionClasses;
//using Obymobi.Data.EntityClasses;
//using Obymobi.Data.HelperClasses;
//using Obymobi.Enums;
//using Obymobi.Logic.HelperClasses;
//using SD.LLBLGen.Pro.ORMSupportClasses;
//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Drawing;
//using System.IO;
//using System.Linq;

//namespace Obymobi.Web.CloudStorage
//{
//    public class CloudStorageHelper
//    {
//        public static void RefreshGenericMediaRatioTypeMediaEntities(bool forceSave)
//        {
//            Stopwatch sw = new Stopwatch();
//            sw.Start();
//            MediaRatioTypeMediaHelper.ResetMediaRatioTypeMediaFiles(CloudStorageHelper.GetGenericMedia(), forceSave);
//            sw.Stop();
//            Debug.WriteLine("COMPLETED RESIZE FOR GENERIC IN: " + sw.Elapsed.ToString());
//            Debug.WriteLine("COMPLETED RESIZE FOR GENERIC IN: " + sw.Elapsed.ToString());
//            Debug.WriteLine("COMPLETED RESIZE FOR GENERIC IN: " + sw.Elapsed.ToString());
//        }

//        public static void RefreshEntertainmentMediaRatioTypeMediaEntities(bool forceSave)
//        {
//            Stopwatch sw = new Stopwatch();
//            sw.Start();
//            MediaRatioTypeMediaHelper.ResetMediaRatioTypeMediaFiles(CloudStorageHelper.GetEntertainmentMedia(), forceSave);
//            Debug.WriteLine("COMPLETED RESIZE FOR ENTERTAINMENT IN: " + sw.Elapsed.ToString());
//            Debug.WriteLine("COMPLETED RESIZE FOR ENTERTAINMENT IN: " + sw.Elapsed.ToString());
//            Debug.WriteLine("COMPLETED RESIZE FOR ENTERTAINMENT IN: " + sw.Elapsed.ToString());
//        }

//        public static void RefreshMediaRatioTypeMediaEntities(int companyId, bool forceSave)
//        {
//            Stopwatch sw = new Stopwatch();
//            sw.Start();
//            MediaRatioTypeMediaHelper.ResetMediaRatioTypeMediaFiles(CloudStorageHelper.GetMediaForCompany(companyId, false), forceSave);
//            sw.Stop();
//            Debug.WriteLine("COMPLETED RESIZE FOR COMPANY " + companyId + "IN: " + sw.Elapsed.ToString());
//            Debug.WriteLine("COMPLETED RESIZE FOR COMPANY " + companyId + "IN: " + sw.Elapsed.ToString());
//            Debug.WriteLine("COMPLETED RESIZE FOR COMPANY " + companyId + "IN: " + sw.Elapsed.ToString());
//        }

//        public static void PublishGenericContentToCdn()
//        {
//            CloudStorageHelper.PublishMediaToCdn(CloudStorageHelper.GetGenericMedia());
//        }

//        public static void PublishEntertainmentMediaToCdn()
//        {
//            CloudStorageHelper.PublishMediaToCdn(CloudStorageHelper.GetEntertainmentMedia());
//        }

//        public static void PublishMediaToCdn(int companyId, bool unpublishedOnly = true)
//        { 
//            CloudStorageHelper.PublishMediaToCdn(CloudStorageHelper.GetMediaForCompany(companyId, unpublishedOnly));                       
//        }

//        private static void PublishMediaToCdn(MediaCollection medias)
//        {
//            foreach (var media in medias)
//            {
//                foreach (MediaRatioTypeMediaEntity ratio in media.MediaRatioTypeMediaCollection)
//                {
//                    try
//                    {
//                        MediaHelper.QueueMediaRatioTypeMediaFileTask(ratio, MediaProcessingTaskEntity.ProcessingAction.Upload, null);
//                    }
//                    catch (Exception ex)
//                    {
//                        Debug.WriteLine("WARNING: Failed for: {0}, {1}", ratio.MediaRatioTypeMediaId, ex.Message);
//                    }
//                }
//            } 
//        }

//        private static MediaCollection GetGenericMedia()
//        {
//            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
//            var mediaRatioTypeMediaPrefetch = MediaEntity.PrefetchPathMediaRatioTypeMediaCollection;
//            mediaRatioTypeMediaPrefetch.Filter = new PredicateExpression(MediaRatioTypeMediaFields.LastDistributedVersionTicks == DBNull.Value);

//            PredicateExpression filter = new PredicateExpression();
//            filter.Add(MediaFields.GenericcategoryId != DBNull.Value);
//            filter.AddWithOr(MediaFields.GenericproductId != DBNull.Value);
            
//            MediaCollection media = new MediaCollection();
//            media.GetMulti(filter, prefetch);
            
//            return media;
//        }

//        private static MediaCollection GetEntertainmentMedia()
//        {
//            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
//            var mediaRatioTypeMediaPrefetch = MediaEntity.PrefetchPathMediaRatioTypeMediaCollection;
//            mediaRatioTypeMediaPrefetch.Filter = new PredicateExpression(MediaRatioTypeMediaFields.LastDistributedVersionTicks == DBNull.Value);

//            PredicateExpression filter = new PredicateExpression();
//            filter.Add(MediaFields.EntertainmentId != DBNull.Value);

//            MediaCollection media = new MediaCollection();
//            media.GetMulti(filter, prefetch);

//            return media;
//        }

//        private static MediaCollection GetMediaForCompany(int companyId, bool unpublishedOnly)
//        {
//            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
//            var mediaRatioTypeMediaPrefetch = MediaEntity.PrefetchPathMediaRatioTypeMediaCollection;
//            if (unpublishedOnly)
//                mediaRatioTypeMediaPrefetch.Filter = new PredicateExpression(MediaRatioTypeMediaFields.LastDistributedVersionTicks == DBNull.Value);
//            prefetch.Add(mediaRatioTypeMediaPrefetch);

//            var medias = MediaHelper.GetMediaForCompany(companyId, null, 0, null, null, prefetch, false);

//            return medias;
//        }

//        public static void EnableMobileMediaRatioTypeMediaEntities(int companyId)
//        {
//            List<DeviceType> deviceTypes = new List<DeviceType>();
//            deviceTypes.Add(DeviceType.PhoneSmall);
//            deviceTypes.Add(DeviceType.PhoneNormal);
//            deviceTypes.Add(DeviceType.PhoneLarge);
//            deviceTypes.Add(DeviceType.PhoneXLarge);
//            deviceTypes.Add(DeviceType.TabletSmall);
//            deviceTypes.Add(DeviceType.TabletNormal);
//            deviceTypes.Add(DeviceType.TabletLarge);
//            deviceTypes.Add(DeviceType.TabletXLarge);
//            deviceTypes.Add(DeviceType.iPhone);
//            deviceTypes.Add(DeviceType.iPhoneRetina);
//            deviceTypes.Add(DeviceType.iPad);
//            deviceTypes.Add(DeviceType.iPadRetina);

//            List<MediaRatioType> mediaRatioTypes = MediaRatioTypes.GetMediaRatioTypesForDevice(deviceTypes.ToArray());

//            MediaCollection mediaCollection = GetMediaForCompany(companyId, false);

//            foreach (MediaEntity mediaEntity in mediaCollection)
//            {
//                var mediaTypes = from m in mediaRatioTypes 
//                                 where m.EntityType == string.Format("{0}Entity", mediaEntity.RelatedEntityName) 
//                                 select m.MediaType;

//                EntityView<MediaRatioTypeMediaEntity> mediaRatioTypeView = mediaEntity.MediaRatioTypeMediaCollection.DefaultView;
//                PredicateExpression filter = null;

//                foreach (MediaType mediaType in mediaTypes)
//                {
//                    filter = new PredicateExpression();
//                    filter.Add(MediaRatioTypeMediaFields.MediaType == mediaType);

//                    mediaRatioTypeView.Filter = filter;

//                    if (mediaRatioTypeView.Count == 0)
//                    {
//                        MemoryStream memoryStream = null;
//                        Image image = null;

//                        try
//                        {
//                            memoryStream = new MemoryStream(mediaEntity.MediaFileEntity.File);
//                            image = Image.FromStream(memoryStream);

//                            MediaRatioTypeMediaEntity mrtm = new MediaRatioTypeMediaEntity();
//                            //mrtm.AddToTransaction(transaction);
//                            mrtm.MediaId = mediaEntity.MediaId;
//                            mrtm.MediaTypeAsEnum = mediaType;
//                            mrtm.SetDefaultCrop(image);
//                            mrtm.Save();

//                            MediaRatioTypeMediaHelper.ResizeAndPublishFile(mrtm, mediaEntity, true);
//                            mediaRatioTypeView.RelatedCollection.Add(mrtm);
//                        }
//                        catch
//                        { }
//                        finally
//                        {
//                            if (memoryStream != null)
//                                memoryStream.Dispose();
//                            if (image != null)
//                                image.Dispose();
//                        }
//                    }
//                }
//            }
//        }
//    }
//}
