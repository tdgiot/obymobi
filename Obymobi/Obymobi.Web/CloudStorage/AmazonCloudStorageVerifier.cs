﻿using System;
using Dionysos;
using Dionysos.Interfaces;
using Microsoft.WindowsAzure;
using Obymobi.Constants;
using Obymobi.Enums;

namespace Obymobi.Web.CloudStorage
{
    public class AmazonCloudStorageVerifier : IVerifier
    {
        public string ErrorMessage { get; set; }
        
        public bool Verify()
        {
            bool toReturn = false;

            string awsAccessKey = CloudConfigurationManager.GetSetting(ObymobiConstants.AmazonS3AwsAccessKeySettingName);
            string awsSecretAccessKey = CloudConfigurationManager.GetSetting(ObymobiConstants.AmazonS3AwsSecretAccessKeySettingName);

            string manualContainer = CloudConfigurationManager.GetSetting(CloudStorageProvider.CLOUD_MANUAL_CONTAINER_KEY);

            if (awsAccessKey.IsNullOrWhiteSpace() || awsSecretAccessKey.IsNullOrWhiteSpace())
            {
                this.ErrorMessage = "The 'AWSAccessKey' or 'AWSSecretKey' setting for Amazon is missing. Should be in Web.config or Web.AppSettings.config.";
            }
            else if (WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.Manual && manualContainer.IsNullOrWhiteSpace())
            {
                this.ErrorMessage = "The 'CloudManualContainer' setting is missing. Should be in Web.AppSettings.config.";
            }
            else if (WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.Manual && manualContainer.Equals(ObymobiConstants.PrimaryAmazonRootContainer, StringComparison.InvariantCultureIgnoreCase))
            {
                this.ErrorMessage = "The 'CloudManualContainer' settings is configured to '{0}', which is only allowed on the production environment.".FormatSafe(ObymobiConstants.PrimaryAmazonRootContainer);
            }
            else
            {
                if (WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.Manual)
                {
                    // We need to set some settings
                    ObymobiConstants.ManualAmazonCdnBaseUrl = ObymobiConstants.AmazonStorageUrlFormat.FormatSafe(manualContainer);
                    ObymobiConstants.ManualAmazonCdnBaseUrlFiles = Dionysos.Web.WebPathCombiner.CombinePaths(ObymobiConstants.ManualAmazonCdnBaseUrl, ObymobiConstants.CloudContainerMediaFiles);
                    ObymobiConstants.ManualAmazonCdnBaseUrlPublished = Dionysos.Web.WebPathCombiner.CombinePaths(ObymobiConstants.ManualAmazonCdnBaseUrl, ObymobiConstants.CloudContainerPublished);
                }

                toReturn = true;
            }

            return toReturn;
        }
    }
}
