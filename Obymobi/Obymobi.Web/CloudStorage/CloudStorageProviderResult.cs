﻿namespace Obymobi.Web.CloudStorage
{
    public enum CloudStorageProviderResult
    {
        UnspecifiedError,
        InvalidPathName,
        ConfigurationMissingOrInvalid,
        StorageOperationFailed,
    }
}
