﻿using System;
using System.Collections.Generic;
using System.IO;
using Dionysos.Logging;
using Obymobi.Web.CloudStorage;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Interfaces;

namespace CloudStorage
{
    public static class CloudUploader
    {
        #region Constructor

        static CloudUploader()
        {
            StringBuilderLogger logger = new StringBuilderLogger();
            CloudUploader.AmazonCloudStorageProvider = new AmazonCloudStorageProvider(logger);        
            CloudUploader.CloudStorageProviders = new List<CloudStorageProvider>();
            CloudUploader.CloudStorageProviders.Add(CloudUploader.AmazonCloudStorageProvider);
        }

        #endregion

        #region Methods        

        public static bool UploadMedia(IMediaEntity mediaEntity, string container, string path, byte[] input)
        {
            return CloudUploader.UploadMedia((MediaEntity)mediaEntity, container, path, input);
        }

        public static bool UploadMedia(MediaEntity mediaEntity, string container, string path, byte[] input)
        {
            long lastDistributedVersionTicks = mediaEntity.LastDistributedVersionTicks.GetValueOrDefault(0);
            if (!mediaEntity.LastDistributedVersionTicks.HasValue)
            {
                lastDistributedVersionTicks = mediaEntity.UpdatedUTC.GetValueOrDefault(mediaEntity.CreatedUTC.GetValueOrDefault(DateTime.MinValue)).Ticks;
            }

            bool success = true;
            if (input == null || input.Length == 0)
            {
                return false;
            }

            foreach (CloudStorageProvider cloudStorageProvider in CloudStorageProviders)
            {
                Stream stream = new MemoryStream(input);
                if (cloudStorageProvider.Identifier == CloudStorageProviderIdentifier.Amazon && mediaEntity.LastDistributedVersionTicksAmazon.GetValueOrDefault(-1) < lastDistributedVersionTicks)
                {
                    if (cloudStorageProvider.WriteFile(container, path, stream, null, false))
                    {
                        mediaEntity.LastDistributedVersionTicksAmazon = lastDistributedVersionTicks;
                    }
                    else
                    {
                        success = false;
                    }
                }
            }

            mediaEntity.Save();

            return success;
        }

        public static void Delete(string container, string path, bool isPrefix)
        {
            foreach (CloudStorageProvider cloudStorageProvider in CloudStorageProviders)
            {
                cloudStorageProvider.DeleteFile(container, path, isPrefix);
            }
        }

        public static bool Exists(string container, string path, bool publicAccess)
        {
            return CloudUploader.AmazonCloudStorageProvider.FileExists(container, path, publicAccess);            
        }

        public static bool UploadReleaseAsync(string container, string path, byte[] input)
        {
            bool success = false;

            if (input == null || input.Length == 0)
            {
                return false;
            }

            foreach (CloudStorageProvider cloudStorageProvider in CloudStorageProviders)
            {
                Stream stream = new MemoryStream(input);
                if (cloudStorageProvider.Identifier == CloudStorageProviderIdentifier.Amazon)
                {
                    cloudStorageProvider.WriteFile(container, path, stream, null, false);
                    success = true;
                }
            }

            return success;
        }

        #endregion

        #region Properties

        private static List<CloudStorageProvider> CloudStorageProviders { get; set; }
        private static CloudStorageProvider AmazonCloudStorageProvider { get; set; }

        #endregion

    }
}