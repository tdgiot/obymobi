﻿using System.Collections.Generic;

namespace Obymobi.Web.CloudStorage.Queue
{
    public abstract class CloudStorageQueueItem
    {
        #region Constructors

        protected CloudStorageQueueItem(List<CloudStorageProvider> providers, string container, string path)
        {
            this.Providers = providers;
            this.Container = container;
            this.Path = path;
        }        

        #endregion

        #region Properties

        public List<CloudStorageProvider> Providers { get; set; }
        public string Container { get; set; }
        public string Path { get; set; }

        #endregion

        #region Methods

        #region Public methods

        public void Execute()
        {
            this.DoWork();
        }

        #endregion

        #region Abstract methods

        protected abstract void DoWork();

        #endregion

        #endregion
    }
}