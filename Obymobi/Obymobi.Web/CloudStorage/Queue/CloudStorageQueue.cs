﻿using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Obymobi.Web.CloudStorage.Queue
{
    public class CloudStorageQueue
    {
        #region Fields

        private static readonly object QueueLock = new object();
        private static CloudStorageQueue cloudStorageQueue;

        private const int PROCESS_THREADS = 1;

        private readonly BlockingCollection<CloudStorageQueueItem> queue = new BlockingCollection<CloudStorageQueueItem>();
        private readonly CancellationTokenSource queueCancelToken;

        #endregion

        #region Constructors

        private CloudStorageQueue(CancellationTokenSource cancellationToken)
        {
            this.queueCancelToken = cancellationToken;

            for (int i = 0; i < CloudStorageQueue.PROCESS_THREADS; i++)
            {
                new Thread((this.ProcessWork)).Start(this.queueCancelToken.Token);
            }
        }

        private static CloudStorageQueue Instance
        {
            get
            {
                lock (CloudStorageQueue.QueueLock)
                {
                    if (CloudStorageQueue.cloudStorageQueue == null)
                    {
                        CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
                        CloudStorageQueue.cloudStorageQueue = new CloudStorageQueue(cancellationTokenSource);
                    }
                }

                return CloudStorageQueue.cloudStorageQueue;
            }
        }

        #endregion

        #region Public methods

        public static void Enqueue(CloudStorageQueueItem item)
        {
            CloudStorageQueue.Instance.EnqueueInternal(item);
        }

        public static void Stop()
        {
            if (CloudStorageQueue.Instance.queueCancelToken != null && !CloudStorageQueue.Instance.queueCancelToken.IsCancellationRequested)
            {
                CloudStorageQueue.Instance.queueCancelToken.Cancel(false);
                CloudStorageQueue.Instance.queue.Add(null);
            }
        }

        #endregion

        #region Processing

        private void EnqueueInternal(CloudStorageQueueItem item)
        {
            this.queue.TryAdd(item);
        }

        private void ProcessWork(object cancellationToken)
        {
            CancellationToken token = (CancellationToken)cancellationToken;
            try
            {
                foreach (CloudStorageQueueItem cloudStorageQueueItem in this.queue.GetConsumingEnumerable(token))
                {
                    try
                    {
                        cloudStorageQueueItem.Execute();
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                    }
                }
            }
            catch (OperationCanceledException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        #endregion
    }
}
