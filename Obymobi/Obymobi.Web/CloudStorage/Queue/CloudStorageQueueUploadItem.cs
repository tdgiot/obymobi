﻿using System;
using System.Collections.Generic;

namespace Obymobi.Web.CloudStorage.Queue
{
    public class CloudStorageQueueUploadItem : CloudStorageQueueItem
    {
        #region Constructors

        public CloudStorageQueueUploadItem(List<CloudStorageProvider> providers, string container, string path, string content, bool privateAccess) : base(providers, container, path)
        {
            this.Content = content;
            this.PrivateAccess = privateAccess;                        
        }

        #endregion

        #region Methods

        protected override void DoWork()
        {
            try
            {
                foreach (CloudStorageProvider provider in this.Providers)
                {
                    // Upload the file, it's not equal.
                    provider.WriteFile(this.Container, this.Path, this.Content, null, this.PrivateAccess);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        #endregion

        #region Properties

        public string Content { get; set; }
        public bool PrivateAccess { get; set; }        

        #endregion
    }
}
