﻿using Dionysos.Interfaces;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Web.Azure.ServiceBus.Notifications
{
    public class AzureNotificationVerifier : IVerifier
    {
        public string ErrorMessage { get; set; }

        public bool Verify()
        {
            bool toReturn = true;

            var hubCollection = new AzureNotificationHubCollection();
            if (hubCollection.GetDbCount() == 0)
            {
                this.ErrorMessage = "AzureNotificationHubs are not configured.";
                toReturn = false;
            }

            return toReturn;
        }
    }
}
