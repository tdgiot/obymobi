﻿using System.Collections.Generic;
using System.Text;
using System.Web;
using Dionysos;
using Dionysos.Logging;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Web.Azure.ServiceBus.Notifications
{
    public class AzureNotificationProvider
    {
        private readonly IMultiLoggingProvider loggingProvider;

        public enum LoggingTypes
        {
            [StringValue("NotificationProvider")]
            NotificationProvider,
        }

        public AzureNotificationProvider(IMultiLoggingProvider loggingProvider)
        {
            this.loggingProvider = loggingProvider;
            Instance = this;

            this.loggingProvider.Information(LoggingTypes.NotificationProvider, "Created new AzureNotificationProvider instance");
        }

        public static AzureNotificationProvider Instance
        {
            get; private set;
        }

        public static void SendNotification(AzureNotificationContainer dataContainer, AzureNotificationHubCollection azureNotificationHubCollection = null)
        {
            /*List<NotificationHubClient> clients = GetNotificationClients(azureNotificationHubCollection);

            Instance.SendNotificationAndroid(clients, dataContainer);
            Instance.SendNotificationApple(clients, dataContainer);*/
        }

        /*private static List<NotificationHubClient> GetNotificationClients(AzureNotificationHubCollection azureNotificationHubCollection)
        {
            if (azureNotificationHubCollection == null)
            {
                azureNotificationHubCollection = new AzureNotificationHubCollection();
                azureNotificationHubCollection.GetMulti(null);
            }

            var clients = new List<NotificationHubClient>();
            foreach (var hubEntity in azureNotificationHubCollection)
            {
                if (hubEntity.ConnectionString.Length > 0 && hubEntity.HubPath.Length > 0)
                {
                    clients.Add(NotificationHubClient.CreateClientFromConnectionString(hubEntity.ConnectionString, hubEntity.HubPath));
                }
            }

            return clients;
        }

        private async void SendNotificationAndroid(IEnumerable<NotificationHubClient> clients, AzureNotificationContainer dataContainer)
        {
            var androidJson = dataContainer.GenerateAndroidNotification();
            this.loggingProvider.Verbose(LoggingTypes.NotificationProvider, "[Azure] Android: {0} | Tags: {1}", androidJson, dataContainer.TagExpression);
            foreach (NotificationHubClient client in clients)
            {
                try
                {
                    await client.SendGcmNativeNotificationAsync(androidJson, dataContainer.TagExpression);
                }
                catch (Exception ex)
                {
                    // Because we're running async with await we can still catch exceptions thrown from Azure Notification lib
                    this.loggingProvider.Error(LoggingTypes.NotificationProvider, "[Azure] SendNotificationAndroid - {0}", ex.Message);
                }
            }
        }

        private async void SendNotificationApple(IEnumerable<NotificationHubClient> clients, AzureNotificationContainer dataContainer)
        {
            var appleJson = dataContainer.GenerateAppleAlert();
            this.loggingProvider.Verbose(LoggingTypes.NotificationProvider, "[Azure] iOS: {0} | Tags: {1}", appleJson, dataContainer.TagExpression);
            foreach (NotificationHubClient client in clients)
            {
                try
                {
                    await client.SendAppleNativeNotificationAsync(dataContainer.GenerateAppleAlert(), dataContainer.TagExpression);
                }
                catch (Exception ex)
                {
                    // Because we're running async with await we can still catch exceptions thrown from Azure Notification lib
                    this.loggingProvider.Error(LoggingTypes.NotificationProvider, "[Azure] SendNotificationApple - {0}", ex.Message);
                }
            }
        }*/

        #region Data Container 

        public class AzureNotificationContainer : Dictionary<string, object>
        {
            /// <summary>
            /// Tag expressions can contain all Boolean operators, such as AND (&&), OR (||), and NOT (!). They can also contain parentheses.
            /// Tag expressions are limited to 20 tags if they contain only ORs; otherwise they are limited to 6 tags.
            /// </summary>
            /// <example>(tagA || tagB) && tagZ</example>
            /// <remarks>http://msdn.microsoft.com/en-us/library/azure/dn530749.aspx</remarks>
            public string TagExpression { get; set; }

            // Default fields
            private readonly string title = string.Empty;
            private readonly string message = string.Empty;

            /// <summary>
            /// This container is a basic Dictionary. Values have a string as key and object as value.
            /// </summary>
            /// <param name="tagExpression">Tag expressions can contain all Boolean operators, such as AND (&&), OR (||), and NOT (!). They can also contain parentheses. 
            /// Tag expressions are limited to 20 tags if they contain only ORs; otherwise they are limited to 6 tags.</param>
            /// <param name="title">Notification title</param>
            /// <param name="message">Notification body text</param>
            /// <example>(tagA || tagB) && tagZ</example>
            /// <remarks>http://msdn.microsoft.com/en-us/library/azure/dn530749.aspx</remarks>
            public AzureNotificationContainer(string tagExpression, string title, string message)
            {
                this.TagExpression = tagExpression;

                this.title = title;
                this.message = message;
            }

            internal string GenerateAndroidNotification()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("{\"data\":{");
                sb.AppendFormat("\"title\":\"{0}\",", HttpUtility.JavaScriptStringEncode(this.title));
                sb.AppendFormat("\"message\":\"{0}\",", HttpUtility.JavaScriptStringEncode(this.message));
                foreach (KeyValuePair<string, object> keyValuePair in this)
                {
                    string valueStr = HttpUtility.JavaScriptStringEncode(keyValuePair.Value.ToString());
                    sb.AppendFormat("\"{0}\":\"{1}\",", keyValuePair.Key, valueStr);
                }
                sb.Remove(sb.Length - 1, 1); // Remove last comma
                sb.Append("}}");
                return sb.ToString();
            }

            internal string GenerateAppleAlert()
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("{\"aps\":{");
                sb.AppendFormat("\"alert\":\"{0}\",", HttpUtility.JavaScriptStringEncode(this.message));
                sb.Append("\"sound\":\"default\"");
                sb.Append("}");
                if (this.Count > 0)
                {
                    sb.Append(",");
                    foreach (KeyValuePair<string, object> keyValuePair in this)
                    {
                        string valueStr = HttpUtility.JavaScriptStringEncode(keyValuePair.Value.ToString());
                        sb.AppendFormat("\"{0}\":\"{1}\",", keyValuePair.Key, valueStr);
                    }
                    sb.Remove(sb.Length - 1, 1); // Remove last comma
                }
                sb.Append("}");
                return sb.ToString();
            }
        }

        #endregion
    }
}
