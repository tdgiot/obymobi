﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Dionysos.Web;
using System.IO;

namespace Obymobi.Web.HttpHandlers
{
    public class HttpPutHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public void ProcessRequest(HttpContext httpContext)
        {
            string username = httpContext.Request.QueryString["user"];
            string password = httpContext.Request.QueryString["pass"];

            bool valid = false;

            if (String.IsNullOrEmpty(username))
            {
                // No username specified
            }
            else if (String.IsNullOrEmpty(password))
            {
                // No password specified
            }
            else if (username.ToLower() != "gast")
            {
                // Wrong username
            }
            else if (password.ToLower() != "welkom")
            {
                // Wrong password
            }
            else
            {
                valid = true;
            }

            if (valid)
            {
                File.WriteAllText(httpContext.Server.MapPath("~/result.txt"), "OK!");
            }
            else
            {
                File.WriteAllText(httpContext.Server.MapPath("~/result.txt"), "Failure :(");
            }
        }

        #endregion
    }
}
