﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Dionysos.Web;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Data.LLBLGen;

namespace Obymobi.Logic.Cms
{
    public class GlobalizationHelper : Dionysos.Web.GlobalizationHelper2
    {
        #region Master META Data Translation

        /// <summary>
        /// Retrieve the translation for a field on a Entity
        /// If none is found, the original text will be returned
        /// </summary>
        /// <param name="entity">Entity to search translation for</param>
        /// <param name="field">Field to find the translation for</param>
        /// <returns></returns>
        public static string GetTranslation(IEntity entity, IEntityField field)
        {
            string toReturn = "ERROR";
            if (Dionysos.Global.GlobalizationHelper.TranslationRequired)
            {
                TranslationEntity translation = GetTranslationEntity(entity, field);
                if (translation == null || translation.TranslationValue.Length == 0)
                    toReturn = entity.Fields[field.Name].CurrentValue.ToString();
                else
                    toReturn = translation.TranslationValue;
            }
            else
                toReturn = entity.Fields[field.Name].CurrentValue.ToString();

            return toReturn;
        }

        private static TranslationEntity GetTranslationEntity(IEntity entity, IEntityField field)
        {
            TranslationEntity toReturn = null;
            string translationKey = GetTranslationKey(entity, field);

            // First try from cache, otherwise retrieve
            if (!CacheHelper.TryGetValue(translationKey + CurrentUICultureLanguageCode, false, out toReturn))
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(TranslationFields.TranslationKey == translationKey);
                filter.Add(TranslationFields.LanguageCode == CurrentUICultureLanguageCode);
                toReturn = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<TranslationEntity>(filter);

                //toReturn = Dionysos.Global.TranslationProvider.GetTranslation(translationKey, true) as TranslationEntity;

                // In this case even NULL will be cached, as that's a result too
                if (toReturn == null)
                    toReturn = new TranslationEntity();

                CacheHelper.AddSlidingExpire(false, translationKey + CurrentUICultureLanguageCode, toReturn, 600);
            }

            return toReturn;
        }

        private static string GetTranslationKey(IEntity entity, IEntityField field)
        {
            string toReturn = string.Empty;
            if (entity is ModuleEntity)
            {
                ModuleEntity module = entity as ModuleEntity;
                toReturn = string.Format("Module.{0}", module.NameSystem);
            }
            else if (entity is UIElementEntity)
            {
                UIElementEntity uielement = entity as UIElementEntity;
                toReturn = string.Format("UIElement.{0}.{1}", uielement.ModuleNameSystem, uielement.NameSystem);
            }
            else
                throw new Dionysos.TechnicalException("GlobalizationHelper.GetTranslationKey has no implementation for Entities of the type: " + entity.GetType().ToString());

            toReturn += "." + field.Name;

            return toReturn;
        }

        #endregion

        public override bool TranslationRequired
        {
            get
            {
                return !Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName.Equals("nl", StringComparison.OrdinalIgnoreCase);
            }
        }
    }
}
