﻿using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
//using Obymobi.Logic.Configuration;
using Dionysos.Drawing;
using Dionysos.Web.UI.WebControls;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.RelationClasses;
using Obymobi.Data.StoredProcedureCallerClasses;
using Obymobi.Data;

using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;


namespace Obymobi.Logic.Cms
{
    /// <summary>
    /// Helper class for saving a file from a FileUpload control.
    /// </summary>
    public static class ImageSaveHelper
    {
        #region MediaEntity Methods

        /// <summary>
        /// Saves the file from the FileUpload to a MediaEntity.
        /// </summary>
        /// <param name="fu">The FileUpload control to get the file from.</param>
        /// <param name="media">The MediaEntity to save to.</param>
        /// <returns>true if the file is succesfuly saved; otherwise false.</returns>
        public static bool SaveMediaFromFileUpload(FileUpload fu, ref MediaEntity media)
        {
            if (!fu.HasFile)
            {
                throw new Dionysos.FunctionalException("Geen bestand gevonden bij het opslaan!");
            }

            return media.Attach(fu.PostedFile.InputStream, fu.PostedFile.FileName);
        }

        /// <summary>
        /// Saves the HttpPostedFile to a MediaEntity.
        /// </summary>
        /// <param name="file">The posted file from the FileUpload control.</param>
        /// <param name="subFolder">The subfolder to save the file in.</param>
        /// <param name="media">The MediaEntity to save to.</param>
        [Obsolete("This overload doesn't use the saving logic contained in the MediaEntity.")]
        public static void SaveMediaFromFileUpload(HttpPostedFile file, string subFolder, ref MediaEntity media)
        {
            string retval = string.Empty;

            HttpServerUtility server = HttpContext.Current.Server;
            // MDB TODO
            // string mediaPath = Dionysos.Global.ConfigurationProvider.GetString(ObymobiConfigConstants.MediaPathRelative);
            string mediaPath = "~/Media";
            string mediaPathMapped = server.MapPath(mediaPath);
            // Check if root path exisits, if not, create
            if (!Directory.Exists(mediaPathMapped))
                Directory.CreateDirectory(mediaPathMapped);

            // Add any subfolders that are supplied
            if (subFolder.Length > 0)
            {
                mediaPath = Path.Combine(mediaPath, subFolder);
                mediaPathMapped = Path.Combine(mediaPathMapped, subFolder);
            }

            // Check if path (now with appended subFolder) exisits, if not, create
            if (!Directory.Exists(mediaPathMapped))
                Directory.CreateDirectory(mediaPathMapped);

            // Check if file exists, if it does rename it
            string FileName = Path.GetFileName(file.FileName.Replace(' ', '-'));
            string FileNameWithoutExtension = Path.GetFileNameWithoutExtension(file.FileName);
            string FileExtension = Path.GetExtension(file.FileName); // FileExtenstion includes the period: ".jpg"
            string FullPathAndFileName = string.Empty;

            int i = 0;
            // Here we check if the file exists, if it does we try to append _000 untill _9999
            // Then we give up :)
            if (File.Exists(Path.Combine(mediaPathMapped, FileName)))
            {
                for (i = 0; i < 10000; i++)
                {
                    if (!File.Exists(Path.Combine(mediaPathMapped, (FileNameWithoutExtension + "_" + i.ToString() + FileExtension))))
                    {
                        FileName = FileNameWithoutExtension + "_" + i.ToString() + FileExtension;
                        break;
                    }
                }
            }

            FullPathAndFileName = Path.Combine(mediaPathMapped, FileName);

            if (i != 9999)
            {
                // We found a valid name to save the file.
                file.SaveAs(FullPathAndFileName);

                media.SizeKb = file.ContentLength / 1024;
                media.FilePathRelativeToMediaPath = Path.Combine(subFolder, FileName); // Here we use the relative instead of mapped path
                media.MimeType = Dionysos.MimeTypeHelper.GetMimeType(media.FilePathRelativeToMediaPath);
            }
            else
            {
                throw new Dionysos.FunctionalException("De opgegeven bestandsnaam bestaat al!");
            }
        }

        #endregion
    }
}
