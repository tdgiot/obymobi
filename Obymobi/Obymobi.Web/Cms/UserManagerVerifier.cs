﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos;
using Dionysos.Reflection;
using Dionysos.Interfaces;
using Dionysos.Data.LLBLGen;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;

namespace Obymobi.Logic.Cms
{
    /// <summary>
    /// Verifier to check if there's at least 1 administrator user in the system, if not 1 is added login/pass: system	
    /// </summary>
    public class UserManagementVerifier : IVerifier
    {
        #region Fields

        private string errorMessage = string.Empty;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ExtraVestiging.Logic.BackerVerifier class
        /// </summary>
        public UserManagementVerifier()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes the verifier and checks if a user is available, if not it's created and a error is shown telling the user is created
        /// </summary>
        /// <returns>True if a user was found, false if not found and added</returns>
        public bool Verify()
        {
            bool succes = false;

            RoleCollection roles = new RoleCollection();
            PredicateExpression filterRoles = new PredicateExpression(RoleFields.SystemRole == true);

            PrefetchPath pathRoles = new PrefetchPath(EntityType.RoleEntity);
            pathRoles.Add(RoleEntity.PrefetchPathUserCollectionViaUserRole);
            try
            {
                roles.GetMulti(filterRoles, pathRoles);
            }
            catch(Exception ex)
            {
                roles.GetMulti(filterRoles);
                throw new Exception("An error occurred while verifying the user management. POTENTIAL SOLUTION: Is assembly Obymobi.Data.Injectables (re)build?\r\n " + ex.Message);
            }

            for (int i = 0; i < roles.Count; i++)
            {
                RoleEntity current = roles[i];
                if (current.UserCollectionViaUserRole.Count > 0)
                {
                    succes = true;
                    break;
                }
            }

            // No user found in a SystemRole (= admin)
            if (!succes)
            {
                // If no system role was available add:
                RoleEntity systemRole;
                if (roles.Count == 0)
                {
                    systemRole = new RoleEntity();
                    systemRole.SystemRole = true;
                    systemRole.Name = "Administrator";
                    systemRole.Save();
                }
                else
                {
                    systemRole = roles[0];
                }

                UserEntity user = new UserEntity();
                user.Username = "system";
                user.IsApproved = true;
                user.Save();

                UserRoleEntity userRole = new UserRoleEntity();
                userRole.RoleId = systemRole.RoleId;
                userRole.UserId = user.UserId;
                userRole.Save();

                Dionysos.Web.Security.UserManager.Instance.SetPassword(user, "system");

                this.ErrorMessage = string.Format("Er was nog geen systeembeheerder in het systeem aanwezig, deze is nu aangemaakt met login en wachtwoord: 'system'");
            }

            return succes;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the error message if verification failed
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return this.errorMessage;
            }
            set
            {
                this.errorMessage = value;
            }
        }

        #endregion
    }
}
