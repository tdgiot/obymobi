﻿using Dionysos;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CmsSessionHelper
/// </summary>
public class CmsSessionHelper
{
    private const string CompaniesForUserCacheKeyTemplate = "companiesForUser-{0}";
    private const string RecentlyUsedCompaniesForUserCookieNameTemplate = "user-{0}";

    public static Role CurrentRole
    {
        get
        {
            Role currentRole = Role.Customer;

            UserEntity user = CmsSessionHelper.CurrentUser;
            if (user != null)
            {
                foreach (string roleName in user.Roles)
                {
                    if (roleName.Contains("GodMode", StringComparison.InvariantCultureIgnoreCase) && currentRole < Role.GodMode)
                        currentRole = Role.GodMode;
                    else if (roleName.Contains("Crave", StringComparison.InvariantCultureIgnoreCase) && currentRole < Role.Crave)
                        currentRole = Role.Crave;
                    else if (roleName.Contains("Administrator", StringComparison.InvariantCultureIgnoreCase) && currentRole < Role.Administrator)
                        currentRole = Role.Administrator;
                    else if (roleName.Contains("Reseller", StringComparison.InvariantCultureIgnoreCase) && currentRole < Role.Reseller)
                        currentRole = Role.Reseller;
                    else if (roleName.Contains("Console", StringComparison.InvariantCultureIgnoreCase) && currentRole < Role.Console)
                        currentRole = Role.Console;
                    else if (roleName.Contains("Supervisor", StringComparison.InvariantCultureIgnoreCase) && currentRole < Role.Supervisor)
                        currentRole = Role.Supervisor;
                }
            }

            return currentRole;
        }
    }

    public static UserEntity CurrentUser => Dionysos.Web.Security.UserManager.CurrentUser as UserEntity;

    public static CompanyCollection CompanyCollectionForUser
    {
        get
        {
            CompanyCollection companies = new CompanyCollection();

            if (Dionysos.Web.Security.UserManager.CurrentUser != null)
            {
                UserEntity user = Dionysos.Web.Security.UserManager.CurrentUser as UserEntity;
                if (user != null)
                {
                    string cacheKey = string.Format(CompaniesForUserCacheKeyTemplate, user.UserId);

                    if (!Dionysos.Web.CacheHelper.TryGetValue<CompanyCollection>(cacheKey, false, out companies))
                    {
                        companies = new CompanyCollection();

                        if (user.Role >= Role.Crave)
                        {
                            companies.GetMulti(null);
                        }
                        else if (user.AccountId.HasValue)
                        {
                            PredicateExpression filter = new PredicateExpression(AccountCompanyFields.AccountId == user.AccountId.Value);
                            RelationCollection relations = new RelationCollection(CompanyEntity.Relations.AccountCompanyEntityUsingCompanyId);
                            companies.GetMulti(filter, relations);
                        }
                        else if (user.IsAdministrator)
                        {
                            companies.GetMulti(null);
                        }

                        Dionysos.Web.CacheHelper.AddSlidingExpire(false, cacheKey, companies, 20);
                    }
                }
            }

            return companies;
        }
    }

    public static IEnumerable<int> CompanyIdsForUser
    {
        get { return CompanyCollectionForUser.Select(x => x.CompanyId); }
    }

    public static BrandRole? GetUserRoleForBrand(int brandId)
    {
        UserBrandEntity entity = CurrentUser.UserBrandCollection.FirstOrDefault(b => b.BrandId == brandId);

        if (entity == null && CurrentRole >= Role.Crave)
        {
            return BrandRole.Owner;
        }

        return (entity != null) ? entity.Role : (BrandRole?)null;
    }

    public static BrandCollection GetBrandCollectionForUser(BrandRole minimumRole = BrandRole.Viewer)
    {
        BrandCollection collection = new BrandCollection();

        UserEntity user = CmsSessionHelper.CurrentUser;
        if (user == null)
        {
            return collection;
        }

        if (user.Role >= Role.Crave)
        {
            collection.GetMulti(null);
        }
        else
        {
            PredicateExpression filter = new PredicateExpression(UserBrandFields.UserId == CmsSessionHelper.CurrentUser.UserId);
            filter.Add(UserBrandFields.Role >= minimumRole);
            RelationCollection relations = new RelationCollection(BrandEntity.Relations.UserBrandEntityUsingBrandId);
            collection.GetMulti(filter, relations);
        }

        return collection;
    }

    public static BrandCollection GetVisibleCompanyBrandsForUser(int companyId)
    {
        BrandCollection companyBrands = new BrandCollection();
        RelationCollection relations = new RelationCollection(BrandEntityBase.Relations.CompanyBrandEntityUsingBrandId);
        PredicateExpression filter = new PredicateExpression(CompanyBrandFields.CompanyId == companyId);
        companyBrands.GetMulti(filter, relations);

        if (companyBrands.Count == 0)
        {
            return new BrandCollection();
        }
        if (CurrentRole >= Role.Crave)
        {
            return companyBrands;
        }

        BrandCollection userBrands = CmsSessionHelper.GetBrandCollectionForUser();

        BrandCollection visibleBrands = new BrandCollection();
        foreach (BrandEntity companyBrand in companyBrands)
        {
            if (userBrands.Any(x => x.BrandId == companyBrand.BrandId))
            {
                visibleBrands.Add(companyBrand);
            }
        }

        return visibleBrands;
    }

    public static bool IsCompanyIdValidForCompanyBrandCollection(int companyId)
    {
        string key = $"CompanyBrandCollection-{companyId}";
        if (!Dionysos.Web.CacheHelper.TryGetValue(key, false, out CompanyBrandCollection companyBrandCollection))
        {
            companyBrandCollection = new CompanyBrandCollection();
            companyBrandCollection.GetMulti(CompanyBrandFields.CompanyId == companyId);

            Dionysos.Web.CacheHelper.AddAbsoluteExpire(false, key, companyBrandCollection, 5);
        }

        foreach (UserBrandEntity userBrandEntity in CurrentUser.UserBrandCollection)
        {
            if (companyBrandCollection.Any(x => x.BrandId == userBrandEntity.BrandId))
            {
                return true;
            }
        }

        return false;
    }

    public static void ClearCompaniesFromCacheForAccount(AccountEntity accountEntity)
    {
        if (accountEntity != null)
        {
            foreach (UserEntity userEntity in accountEntity.UserCollection)
            {
                ClearCompaniesFromCacheForUser(userEntity.UserId);
            }
        }
    }

    public static void ClearCompaniesFromCacheForUser(int userId)
    {
        string cacheKey = string.Format(CompaniesForUserCacheKeyTemplate, userId);
        Dionysos.Web.CacheHelper.Remove(false, cacheKey);
    }

    /// <summary>
    /// Gets / Sets the CultureInfo for a User
    /// </summary>
    public static CultureInfo CurrentCulture
    {
        get
        {
            CultureInfo toReturn;
            SessionHelper.TryGetValue("CmsSessionHelper.CurrentCulture", out toReturn);
            return toReturn;
        }
        set
        {
            SessionHelper.SetValue("CmsSessionHelper.CurrentCulture", value);
        }
    }

    private const string LastUsedCompanyIdCookieName = "LastUsedCompanyIdCookieName";
    /// <summary>
    /// Gets the id of the current company
    /// </summary>
    public static int CurrentCompanyId
    {
        get
        {
            int companyId = -1;
            if (!SessionHelper.TryGetValue("CompanyId", out companyId))
            {
                CookieHelper.TryGetValue(CmsSessionHelper.LastUsedCompanyIdCookieName, out companyId);
            }

            return companyId;
        }
        set
        {
            SessionHelper.SetValue("CompanyId", value);
            CookieHelper.AddCookie(CmsSessionHelper.LastUsedCompanyIdCookieName, value, DateTime.Now.AddDays(30));

            if (value > 0)
            {
                AddCompanyIdToTheRecentlyUsedCompanies(value);
            }
        }
    }

    private static void AddCompanyIdToTheRecentlyUsedCompanies(int companyId)
    {
        int? currentUserId = CurrentUser?.UserId;
        if (!currentUserId.HasValue)
        {
            return;
        }

        string userCookieName = string.Format(RecentlyUsedCompaniesForUserCookieNameTemplate, currentUserId);
        RecentlyUsedCompanies recentlyUsedCompanies = RecentlyUsedCompanies.Empty;

        // Check if the current user has a cookie
        if (CookieHelper.HasParameter(userCookieName))
        {
            string oldCompanyString = CookieHelper.GetParameter(userCookieName);

            recentlyUsedCompanies = RecentlyUsedCompanies.ParseFromCookieString(oldCompanyString);
            recentlyUsedCompanies.AddMostRecentlyUsedCompany(companyId);
        }

        string newCookieValue = recentlyUsedCompanies.ToCookieString();
        CookieHelper.AddCookie(userCookieName, newCookieValue, DateTime.Now.AddDays(60));
    }

    public static RecentlyUsedCompanies GetRecentlyUsedCompanies(int userId)
    {
        string userCookieName = string.Format(RecentlyUsedCompaniesForUserCookieNameTemplate, userId);

        // Check if the current user has a cookie
        if (!CookieHelper.HasParameter(userCookieName))
        {
            return RecentlyUsedCompanies.Empty;
        }
        else
        {
            string cookieValue = CookieHelper.GetParameter(userCookieName);
            return RecentlyUsedCompanies.ParseFromCookieString(cookieValue);
        }
    }

    #region Language Related

    // GK I wanted to remove these whole properties as I find that it should be based on the
    // CompanyId of the entity you're viewing, not what happens to be your current company. 
    // But since that's quite a bit more work (i.e. Panels must be added before the datasource 
    // is available I had to keep it for now - time constraint.)

    public static CompanyCultureCollection Cultures => CompanyCultureHelper.GetCompanyCulturesForCompany(CmsSessionHelper.CurrentCompanyId);

    public static string DefaultCurrencyCodeForCompany => CmsSessionHelper.GetCompanyWithField(CompanyFields.CurrencyCode).CurrencyCode;

    public static string DefaultCultureCodeForCompany => CmsSessionHelper.GetCompanyWithField(CompanyFields.CultureCode).CultureCode;

    public static string CurrentCompanyName => CmsSessionHelper.GetCompanyWithField(CompanyFields.Name).Name;

    public static TimeZoneInfo CompanyTimeZone => new CompanyEntity(CmsSessionHelper.CurrentCompanyId).TimeZoneInfo;

    public static AlterationDialogMode AlterationDialogMode => CmsSessionHelper.GetCompanyWithField(CompanyFields.AlterationDialogMode).AlterationDialogMode;

    public static ReleaseGroup CurrentCompanyReleaseGroup => CmsSessionHelper.GetCompanyWithField(CompanyFields.ReleaseGroup).ReleaseGroup.GetValueOrDefault(ReleaseGroup.All);

    public static CompanyEntity CurrentCompany => CmsSessionHelper.GetCompanyWithField(CompanyFields.CompanyId);

    private static CompanyEntity GetCompanyWithField(EntityField field)
    {
        string itemKey = string.Format("{0}-{1}", CmsSessionHelper.CurrentCompanyId, field.Name);

        CompanyEntity companyEntity = null;
        if (HttpContext.Current != null && HttpContext.Current.Items[itemKey] != null)
        {
            companyEntity = (CompanyEntity)HttpContext.Current.Items[itemKey];
        }

        if (companyEntity == null)
        {
            companyEntity = new CompanyEntity();
            companyEntity.FetchUsingPK(CmsSessionHelper.CurrentCompanyId, null, null, new IncludeFieldsList(field));
        }

        if (HttpContext.Current != null)
            HttpContext.Current.Items[itemKey] = companyEntity;

        return companyEntity;
    }

    public static CategoryCollection GetCategoriesForCompany()
    {
        CategoryCollection cats = new CategoryCollection();

        PredicateExpression filter = new PredicateExpression();
        filter.Add(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);
        filter.Add(CategoryFields.ParentCategoryId == DBNull.Value);

        SortExpression sort = new SortExpression();
        sort.Add(CategoryFields.SortOrder | SortOperator.Ascending);
        sort.Add(CategoryFields.Name | SortOperator.Ascending);

        PrefetchPath path = new PrefetchPath(EntityType.CategoryEntity);
        path.Add(CategoryEntity.PrefetchPathChildCategoryCollection).SubPath.Add(CategoryEntity.PrefetchPathChildCategoryCollection).SubPath.Add(CategoryEntity.PrefetchPathChildCategoryCollection).SubPath.Add(CategoryEntity.PrefetchPathChildCategoryCollection);

        cats.GetMulti(filter, 0, sort, null, path);

        return cats;
    }

    #endregion

    public static IEnumerable<CategoryEntity> GetCategoriesForCompanyFlat(bool hideCategoriesWithChilds)
    {
        CategoryCollection cats = new CategoryCollection();

        PredicateExpression filter = new PredicateExpression();
        filter.Add(CategoryFields.CompanyId == CmsSessionHelper.CurrentCompanyId);

        SortExpression sort = new SortExpression();
        sort.Add(CategoryFields.SortOrder | SortOperator.Ascending);
        sort.Add(CategoryFields.Name | SortOperator.Ascending);

        PrefetchPath path = new PrefetchPath(EntityType.CategoryEntity);
        path.Add(CategoryEntity.PrefetchPathChildCategoryCollection).SubPath.Add(CategoryEntity.PrefetchPathChildCategoryCollection).SubPath.Add(CategoryEntity.PrefetchPathChildCategoryCollection).SubPath.Add(CategoryEntity.PrefetchPathChildCategoryCollection);

        cats.GetMulti(filter, 0, sort, null, path);

        IEnumerable<CategoryEntity> categories = null;
        if (hideCategoriesWithChilds)
            categories = from cs in cats where cs.ChildCategoryCollection.Count == 0 select cs;
        else
            categories = from cs in cats select cs;

        IOrderedEnumerable<CategoryEntity> categoriesSorted = categories.OrderBy(c => c.FullCategoryName);

        return categoriesSorted;
    }

    #region Time Zone

    public static DateTime ConvertDateTimeToUserTimeZone(DateTime date)
    {
        return TimeZoneInfo.ConvertTime(date, TimeZoneInfo.Utc, CurrentUser.TimeZoneInfo);
    }

    #endregion

    public static bool ShouldHideCompany(CompanyEntity companyEntity)
    {
        if (companyEntity.IsDevelopmentCompany() && !CurrentUser.IsDeveloper)
        {
            return true;
        }

        if (companyEntity.IsTestingCompany() && (!IsDeveloperOrTester && CurrentRole != Role.GodMode))
        {
            return true;
        }

        return false;
    }

    public static bool IsDeveloperOrTester => CurrentUser.IsDeveloper || CurrentUser.IsTester;
}
