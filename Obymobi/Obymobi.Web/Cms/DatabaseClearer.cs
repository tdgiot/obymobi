﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Text.RegularExpressions;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Repositories;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Obymobi.Logic.Cms
{
    public class DatabaseClearer
    {
        private Transaction transaction;

        private readonly IActionRepository actionRepository;
        private readonly IOutletSellerInformationRepository outletSellerInformationRepository;
        private readonly IOutletRepository outletRepository;
        private readonly ICompanyRepository companyRepository;
        private readonly IPaymentIntegrationConfigurationRepository paymentIntegrationConfigurationRepository;
        private readonly IRoutestephandlerRepository routestepHandlerRepository;

        private const int KATALONIA_COMPANY_ID = 443;
        private const int KATA_V2_COMPANY_ID = 550;
        private const int TESTING_CO_COMPANY_ID = 222;
        private const int APPALONIA_COMPANY_ID = 595;
        private const int KATA_V2_ENTER_NOC_ACTION_ID = 11561;
        private const string TEST_EMAIL_ADDRESSES = "louisa.fitzgerald@craveinteractive.com, sa@craveinteractive.com";
        private const string TEST_EMAIL_FROM_ADDRESS = "crave@craveinteractive.com";
        private const string NOC_LINK_TEMPLATE = "https://{0}.crave-emenu.com/noc/Orders.aspx?embeddedMode=true";
        private const string PROD_ADYEN_MERCHANT_CODE = "CraveInteractiveLtdECOM";
        private const string TEST_ADYEN_MERCHANT_CODE = "CraveInteractiveLtdTest";
        private const int KATALONIA_EMAIL_DOCUMENT_HANDLER_ID = 2685;
        private const int KATALONIA_EMAIL_ROUTES_HANDLER_ID = 2682;
        private const int KATALONIA_EMAIL_ROUTE_WITH_HTML_HANDLER_ID = 2683;
        private const int KATALONIA_EMAIL_ACKNOWLEDGEMENT_HANDLER_ID = 2686;
        private const int KATA_V2_EMAIL_AND_CONSOLE_HANDLER_ID = 5006;
        private const int KATA_V2_EMAIL_HANDLER_ID = 5041;
        private const int KATA_V2_EMAIL_DEFAULT_ROUTE_HANDLER_ID = 7012;
        private const int KATA_V2_EMAIL_HTML_ROUTE_HANDLER_ID = 7011;

        public DatabaseClearer()
        {
            actionRepository = new ActionRepository();
            outletSellerInformationRepository = new OutletSellerInformationRepository();
            outletRepository = new OutletRepository();
            companyRepository = new CompanyRepository();
            paymentIntegrationConfigurationRepository = new PaymentIntegrationConfigurationRepository();
            routestepHandlerRepository = new RoutestephandlerRepository();
        }

        public DatabaseClearer(
            IActionRepository actionRepository,
            IOutletRepository outletRepository,
            IOutletSellerInformationRepository outletSellerInformationRepository,
            ICompanyRepository companyRepository,
            IPaymentIntegrationConfigurationRepository paymentIntegrationConfigurationRepository,
            IRoutestephandlerRepository routestepHandlerRepository)
        {
            this.actionRepository = actionRepository;
            this.outletSellerInformationRepository = outletSellerInformationRepository;
            this.outletRepository = outletRepository;
            this.companyRepository = companyRepository;
            this.paymentIntegrationConfigurationRepository = paymentIntegrationConfigurationRepository;
            this.routestepHandlerRepository = routestepHandlerRepository;
        }

        public static string NocLinkTemplate => NOC_LINK_TEMPLATE;
        public static string TestEmailAddresses => TEST_EMAIL_ADDRESSES;
        public static string TestEmailFromAddress => TEST_EMAIL_FROM_ADDRESS;
        public static string TestAdyenMerchantCode => TEST_ADYEN_MERCHANT_CODE;
        public static string ProdAdyenMerchantCode => PROD_ADYEN_MERCHANT_CODE;

        public void ClearDatabase()
        {
            if (WebEnvironmentHelper.CloudEnvironmentIsProduction || (DbUtils.GetCurrentCatalogName().Equals("ObymobiProduction") && !TestUtil.IsPcDeveloper))
            {
                throw new NotImplementedException("This method is not intended for use on the Production Environment or on databases called 'ObymobiProduction'!");
            }

            if (ConfigurationManager.GetBool(ObymobiDataConfigConstants.DatabaseClearedForNonLiveUsage))
            {
                throw new NotImplementedException("Database was already cleared.");
            }

            try
            {
                transaction = new Transaction(IsolationLevel.ReadUncommitted, $"{nameof(ClearDatabase)}{DateTime.Now.Ticks}");

                ChangeAllCredentials(WebEnvironmentHelper.CloudEnvironment);
                ReplaceAllMicrositesUrls();
                RemoveGoogleAnalyticsIdsAndChangeSalt();
                ClearPhoneNumberAndEmailOfSupportPools();
                ClearPhoneNumberAndEmailOfSupportAgents();
                ClearPhoneNumberAndEmailOfSupportpoolNotificationRecipients();
                ClearPhoneNumberAndEmailOfReceiptTemplates();
                ClearAzureNotificationHubs();
                DisarmRoutestephandlers();
                UpdatePaymentConfigurations(WebEnvironmentHelper.CloudEnvironment);
                UpdatePaymentProviders(WebEnvironmentHelper.CloudEnvironment);
                ClearDeliverectExternalSystems();
                UpdateDeliverectExternalSystems(WebEnvironmentHelper.CloudEnvironment);
                ClearOutletSellerInformation();
                ClearReportProcessingTasks();
                ClearReportProcessingTaskTemplates();
                ClearAppLessAnalyticSettings();
                UpdateEnterTheNocLink(WebEnvironmentHelper.CloudEnvironment);
                AddTestEmailsToOutlets(WebEnvironmentHelper.CloudEnvironment);
                SwitchOnMonitoring(WebEnvironmentHelper.CloudEnvironment);
                AddTestEmailsToRouteStepHandlers(WebEnvironmentHelper.CloudEnvironment);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();

                throw ex;
            }
            finally
            {
                transaction.Dispose();
            }

            ConfigurationManager.SetValue(ObymobiDataConfigConstants.DatabaseClearedForNonLiveUsage, true);
        }

        public void UpdatePaymentConfigurations(CloudEnvironment cloudEnvironment)
        {
            //https://craveinteractive.atlassian.net/wiki/spaces/DD/pages/1556774913/Adyen
            if (cloudEnvironment == CloudEnvironment.ProductionOverwrite ||
                cloudEnvironment == CloudEnvironment.ProductionPrimary ||
                cloudEnvironment == CloudEnvironment.ProductionSecondary)
            { return; }

            // Reserved for production only
            PaymentIntegrationConfigurationCollection paymentIntegrationConfigurations = paymentIntegrationConfigurationRepository.GetByAdyenMerchantCode(PROD_ADYEN_MERCHANT_CODE);

            foreach (var paymentIntegrationConfiguration in paymentIntegrationConfigurations)
            { paymentIntegrationConfiguration.AdyenMerchantCode = TEST_ADYEN_MERCHANT_CODE; }

            paymentIntegrationConfigurationRepository.Save(paymentIntegrationConfigurations, false);
        }

        public void UpdatePaymentProviders(CloudEnvironment cloudEnvironment)
        {
            if (cloudEnvironment == CloudEnvironment.ProductionOverwrite ||
                cloudEnvironment == CloudEnvironment.ProductionPrimary ||
                cloudEnvironment == CloudEnvironment.ProductionSecondary)
            {
                return;
            }

            PaymentProviderCollection paymentProviders = new PaymentProviderCollection();
            PredicateExpression filter = new PredicateExpression();

            filter.Add(PaymentProviderFields.Type == PaymentProviderType.Adyen);
            filter.Add(PaymentProviderFields.OriginDomain.Like("%appless.app%"));

            paymentProviders.GetMulti(filter);

            foreach (var paymentProvider in paymentProviders)
            {
                paymentProvider.OriginDomain = GetPaymentOriginDomain(cloudEnvironment);
                paymentProvider.AdyenUsername = string.Empty;
                paymentProvider.AdyenPassword = string.Empty;

                paymentProvider.Save();
            }
        }

        private void ClearDeliverectExternalSystems()
        {
            ExternalSystemEntity changes = new ExternalSystemEntity();
            changes.StringValue1 = "CLEARED";
            changes.StringValue2 = "CLEARED";
            changes.StringValue3 = "CLEARED";

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ExternalSystemFields.Type == ExternalSystemType.Deliverect);

            ExternalSystemCollection externalSystems = new ExternalSystemCollection();
            externalSystems.UpdateMulti(changes, filter);
        }

        private void UpdateDeliverectExternalSystems(CloudEnvironment cloudEnvironment)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ExternalSystemFields.Type == ExternalSystemType.Deliverect);
            filter.Add(new FieldCompareRangePredicate(ExternalSystemFields.CompanyId, new List<int> { TESTING_CO_COMPANY_ID, KATA_V2_COMPANY_ID }));

            ExternalSystemCollection externalSystems = new ExternalSystemCollection();
            externalSystems.GetMulti(filter);

            foreach (ExternalSystemEntity externalSystem in externalSystems)
            {
                bool isKataV2 = externalSystem.CompanyId == KATA_V2_COMPANY_ID;

                externalSystem.StringValue1 = "TZ9oO2vLXc4CBcCd";
                externalSystem.StringValue2 = "MarjqgwJsok2+RP+byUmmePBITF4A/9x20lmHTI/ukCcZfZ7PFWaw/gApobjlEHP";
                if (cloudEnvironment == CloudEnvironment.Development) externalSystem.StringValue3 = isKataV2 ? "6033dd388fe40ca01ab056a4" : "6203e6cb25513302f8947b4b";
                if (cloudEnvironment == CloudEnvironment.Test) externalSystem.StringValue3 = isKataV2 ? "607eb6dde8eb8149ea2f4496" : "6204ee29de8e1ea2dd536db9";
                if (cloudEnvironment == CloudEnvironment.Staging) externalSystem.StringValue3 = isKataV2 ? "608130dba53231b346f4d3f6" : "620bcda9764cea134858c0e1";

                externalSystem.Save();
            }
        }

        private void ClearOutletSellerInformation()
        {
            OutletSellerInformationCollection outletSellerInformationCollection = new OutletSellerInformationCollection();
            outletSellerInformationCollection.AddToTransaction(transaction);
            outletSellerInformationCollection.GetMulti(null);

            foreach (OutletSellerInformationEntity outletSellerInformationEntity in outletSellerInformationCollection)
            {
                outletSellerInformationEntity.AddToTransaction(transaction);
                outletSellerInformationEntity.Phonenumber = "CLEARED";
                outletSellerInformationEntity.Email = "CLEARED";
                outletSellerInformationEntity.ReceiptReceiversEmail = string.Empty;
                outletSellerInformationEntity.ReceiptReceiversSms = string.Empty;
            }

            outletSellerInformationCollection.SaveMulti();
        }

        private void ClearReportProcessingTasks()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ReportProcessingTaskFields.Email != DBNull.Value);

            ReportProcessingTaskCollection reportProcessingTaskCollection = new ReportProcessingTaskCollection();
            reportProcessingTaskCollection.AddToTransaction(transaction);
            reportProcessingTaskCollection.GetMulti(filter);

            foreach (ReportProcessingTaskEntity reportProcessingTaskEntity in reportProcessingTaskCollection)
            {
                reportProcessingTaskEntity.AddToTransaction(transaction);
                reportProcessingTaskEntity.Email = string.Empty;
            }

            reportProcessingTaskCollection.SaveMulti();
        }

        private void ClearReportProcessingTaskTemplates()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ReportProcessingTaskTemplateFields.Email != DBNull.Value);

            ReportProcessingTaskTemplateCollection reportProcessingTaskTemplateCollection = new ReportProcessingTaskTemplateCollection();
            reportProcessingTaskTemplateCollection.AddToTransaction(transaction);
            reportProcessingTaskTemplateCollection.GetMulti(filter);

            foreach (ReportProcessingTaskTemplateEntity reportProcessingTaskTemplateEntity in reportProcessingTaskTemplateCollection)
            {
                reportProcessingTaskTemplateEntity.AddToTransaction(transaction);
                reportProcessingTaskTemplateEntity.Email = string.Empty;
            }

            reportProcessingTaskTemplateCollection.SaveMulti();
        }

        private void ClearAppLessAnalyticSettings()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ApplicationConfigurationFields.AnalyticsEnvironment == AnalyticsEnvironment.Production);

            ApplicationConfigurationCollection applicationConfigurationCollection = new ApplicationConfigurationCollection();
            applicationConfigurationCollection.AddToTransaction(transaction);
            applicationConfigurationCollection.GetMulti(filter);

            foreach (ApplicationConfigurationEntity applicationConfigurationEntity in applicationConfigurationCollection)
            {
                applicationConfigurationEntity.GoogleAnalyticsId = string.Empty;
                applicationConfigurationEntity.GoogleTagManagerId = string.Empty;
            }

            applicationConfigurationCollection.SaveMulti();
        }

        public void ClearAzureNotificationHubs()
        {
            AzureNotificationHubCollection azureHubsCollection = new AzureNotificationHubCollection();
            azureHubsCollection.AddToTransaction(transaction);
            azureHubsCollection.DeleteMulti();
        }

        private void ClearPhoneNumberAndEmailOfSupportPools()
        {
            SupportpoolCollection pools = new SupportpoolCollection();
            pools.AddToTransaction(transaction);
            pools.GetMulti(null);

            foreach (SupportpoolEntity pool in pools)
            {
                pool.Phonenumber = string.Empty;
                pool.Email = string.Empty;
                pool.Validator = null;
            }

            pools.SaveMulti();
        }

        private void ClearPhoneNumberAndEmailOfReceiptTemplates()
        {
            ReceiptTemplateCollection receiptTemplates = new ReceiptTemplateCollection();
            receiptTemplates.AddToTransaction(transaction);
            receiptTemplates.GetMulti(null);

            foreach (var receiptTemplate in receiptTemplates)
            {
                receiptTemplate.Email = string.Empty;
                receiptTemplate.Phonenumber = string.Empty;
                receiptTemplate.SellerContactInfo = string.Empty;
                receiptTemplate.Validator = null;
            }

            receiptTemplates.SaveMulti();
        }

        private void ClearPhoneNumberAndEmailOfSupportAgents()
        {
            SupportagentCollection agents = new SupportagentCollection();
            agents.AddToTransaction(transaction);
            agents.GetMulti(null);

            foreach (SupportagentEntity agent in agents)
            {
                agent.Phonenumber = string.Empty;
                agent.Email = string.Empty;
                agent.Validator = null;
            }

            agents.SaveMulti();
        }

        private void ClearPhoneNumberAndEmailOfSupportpoolNotificationRecipients()
        {
            SupportpoolNotificationRecipientCollection recipients = new SupportpoolNotificationRecipientCollection();
            recipients.AddToTransaction(transaction);
            recipients.GetMulti(null);

            foreach (SupportpoolNotificationRecipientEntity recipient in recipients)
            {
                recipient.Phonenumber = string.Empty;
                recipient.Email = string.Empty;
                recipient.Validator = null;
            }

            recipients.SaveMulti();
        }

        private void DisarmRoutestephandlers()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(RoutestephandlerFields.HandlerType == (int)RoutestephandlerType.EmailDocument);
            filter.AddWithOr(RoutestephandlerFields.HandlerType == (int)RoutestephandlerType.EmailOrder);
            filter.AddWithOr(RoutestephandlerFields.HandlerType == (int)RoutestephandlerType.SMS);

            RoutestephandlerCollection routes = new RoutestephandlerCollection();
            routes.AddToTransaction(transaction);
            routes.GetMulti(filter);

            foreach (var routeStepHandler in routes)
            {
                routeStepHandler.FieldValue1 = string.Empty;
                routeStepHandler.FieldValue2 = string.Empty;
                routeStepHandler.Validator = null;
            }

            routes.SaveMulti();
        }

        private void RemoveGoogleAnalyticsIdsAndChangeSalt()
        {
            CompanyCollection companies = new CompanyCollection();
            companies.AddToTransaction(transaction);
            companies.GetMulti(null);

            foreach (CompanyEntity company in companies)
            {
                company.AddToTransaction(transaction);
                company.GoogleAnalyticsId = string.Empty;
                company.Salt = RandomUtil.GetRandomLowerCaseString(48);
                company.CorrespondenceEmail = string.Empty;
                company.Validator = null;
            }

            companies.SaveMulti();
        }

        private void ChangeAllCredentials(CloudEnvironment environment)
        {
            string prefix = string.Empty;
            switch (environment)
            {
                case CloudEnvironment.Test:
                    prefix = "test";
                    break;
                case CloudEnvironment.Development:
                    prefix = "dev";
                    break;
                case CloudEnvironment.Demo:
                    prefix = "demo";
                    break;
                case CloudEnvironment.Manual:
                    prefix = "man";
                    break;
                case CloudEnvironment.Staging:
                    prefix = "stag";
                    break;
                default:
                    break;
            }

            if (TestUtil.IsPcGabriel && DateTime.Now.Date.Equals(new DateTime(2014, 3, 3).Date))
            {
                prefix = "test";
            }

            // Change all Company salts
            CompanyCollection companies = new CompanyCollection();
            companies.AddToTransaction(transaction);
            companies.GetMulti(null);

            foreach (CompanyEntity company in companies)
            {
                try
                {
                    company.AddToTransaction(transaction);
                    company.Salt = RandomUtil.GetRandomLowerCaseString(48);
                    company.Validator = null;
                    company.Save();
                }
                catch (Exception ex)
                {
                    throw new Exception("Failed with Company: " + company.Name, ex);
                }
            }

            CompanyOwnerCollection companyOwners = new CompanyOwnerCollection();
            companyOwners.AddToTransaction(transaction);
            companyOwners.GetMulti(null);
            UserCollection users = new UserCollection();
            users.AddToTransaction(transaction);

            foreach (CompanyOwnerEntity owner in companyOwners)
            {
                try
                {
                    if (!owner.UserId.HasValue)
                    {
                        // Try to find it
                        PredicateExpression userOwnerFilter = new PredicateExpression();
                        userOwnerFilter.Add(UserFields.Username % owner.Username);
                        UserEntity user = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<UserEntity>(userOwnerFilter);
                        if (user != null)
                        {
                            owner.AddToTransaction(transaction);
                            owner.UserId = user.UserId;
                            owner.Validator = null;
                            owner.Save();
                        }
                    }

                    owner.AddToTransaction(transaction);
                    owner.Username = prefix + owner.Username.Replace(" ", "-");

                    PrecheckUsername(owner.Username);

                    owner.Validator = null;
                    owner.Save();
                }
                catch (Exception ex)
                {
                    throw new Exception("Failed with Company Owner: " + owner.Username, ex);
                }
            }

            PredicateExpression userFilter = new PredicateExpression();
            List<int?> userIds = companyOwners.Where(y => y.UserId.HasValue).Select(x => x.UserId).ToList();
            userFilter.Add(UserFields.UserId != userIds);
            users.GetMulti(userFilter);
            foreach (UserEntity user in users)
            {
                try
                {
                    Debug.WriteLine("{0} - {1}", user.UserId, user.Username);
                    user.AddToTransaction(transaction);
                    user.Username = prefix + user.Username;
                    PrecheckUsername(user.Username);
                    user.Validator = null;
                    user.Save();
                }
                catch (Exception ex)
                {
                    throw new Exception("Failed with User: " + user.Username, ex);
                }
            }

            CustomerCollection customers = new CustomerCollection();
            customers.AddToTransaction(transaction);
            PredicateExpression customerFilter = new PredicateExpression(CustomerFields.Email != DBNull.Value);
            customerFilter.Add(CustomerFields.Email != string.Empty);
            customerFilter.Add(CustomerFields.Password != DBNull.Value);
            customerFilter.Add(CustomerFields.Password != string.Empty);
            customers.GetMulti(customerFilter);
            foreach (CustomerEntity customer in customers)
            {
                try
                {
                    if (RegEx.IsEmail(customer.Email))
                    {
                        customer.AddToTransaction(transaction);
                        customer.Email = prefix + customer.Email;
                        customer.Validator = null;
                        customer.Save();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Failed with Customer: " + customer.Email, ex);
                }
            }
        }

        private void PrecheckUsername(string username)
        {
            UserCollection users = new UserCollection();
            users.AddToTransaction(transaction);
            PredicateExpression filter = new PredicateExpression();
            filter.Add(UserFields.Username % username);
            users.GetMulti(filter);

            if (users.Count > 0)
            {
                throw new Exception("Database Clearer can't be executed as there's a duplicate username issue about to happen for '{0}', because the user with UserId '{1}' already has that username. Change the username of that user manually and run again.".FormatSafe(username, users[0].UserId));
            }
        }

        private void ReplaceAllMicrositesUrls()
        {
            // UI Tabs
            UITabCollection uitabs = new UITabCollection();
            uitabs.AddToTransaction(transaction);
            uitabs.GetMulti(null);

            foreach (UITabEntity uitab in uitabs)
            {
                if (!uitab.URL.IsNullOrWhiteSpace())
                {
                    uitab.AddToTransaction(transaction);
                    uitab.URL = GetReplacementUrl(uitab.URL);
                    uitab.Validator = null;
                    uitab.Save();
                }
            }

            EntertainmenturlCollection urls = new EntertainmenturlCollection();
            urls.AddToTransaction(transaction);
            urls.GetMulti(null);

            // Entertainment Urls
            foreach (EntertainmenturlEntity url in urls)
            {
                if (!url.Url.IsNullOrWhiteSpace())
                {
                    url.AddToTransaction(transaction);
                    url.Url = GetReplacementUrl(url.Url);
                    url.Validator = null;
                    url.Save();
                }
            }
        }

        private string GetReplacementUrl(string url)
        {
            string toReturn = url;

            if (url.Contains("crave-emenu", StringComparison.InvariantCultureIgnoreCase) | url.Contains("wordpress", StringComparison.InvariantCultureIgnoreCase) | url.Contains("goldentours.com", StringComparison.InvariantCultureIgnoreCase))
            {
                if (url.Contains("newspaper"))
                {
                    toReturn = "http://www.crave-emenu.com/newspapersgrange/";
                }
                else if (url.Contains("golden"))
                {
                    toReturn = "http://www.crave-emenu.com/transportationgrange/";
                }
                else if (url.Contains("transport"))
                {
                    toReturn = "http://www.crave-emenu.com/transportationgrange/";
                }
                else if (url.Contains("ttd"))
                {
                    toReturn = "http://www.crave-emenu.com/grangestpaulslcg/";
                }
                else
                {
                    toReturn = "http://www.crave-emenu.com/grangestpauls/";
                }
            }

            return toReturn;
        }

        private static string GetPaymentOriginDomain(CloudEnvironment cloudEnvironment)
        {
            switch (cloudEnvironment)
            {
                case CloudEnvironment.Development:
                    return "https://dev.appless.app";
                case CloudEnvironment.Test:
                    return "https://test.appless.app";
                case CloudEnvironment.Staging:
                    return "https://stag.appless.app";
                default:
                    return "https://appless.app";
            }
        }

        public void UpdateEnterTheNocLink(CloudEnvironment cloudEnvironment)
        {
            if (cloudEnvironment == CloudEnvironment.ProductionOverwrite ||
                cloudEnvironment == CloudEnvironment.ProductionPrimary ||
                cloudEnvironment == CloudEnvironment.ProductionSecondary)
            { return; }

            string actionResource = string.Format(NOC_LINK_TEMPLATE, GetEnvironmentUrlName(cloudEnvironment));

            ActionEntity action = actionRepository.Get(KATA_V2_ENTER_NOC_ACTION_ID);
            action.AddToTransaction(transaction);
            action.Resource = actionResource;

            actionRepository.Save(action, false);
        }

        private string GetEnvironmentUrlName(CloudEnvironment cloudEnvironment)
        {
            switch (cloudEnvironment)
            {
                case CloudEnvironment.Development:
                    return "dev";
                case CloudEnvironment.Test:
                    return "test";
                case CloudEnvironment.Staging:
                    return "staging";
                default:
                    return string.Empty;
            }
        }

        public void SwitchOnMonitoring(CloudEnvironment cloudEnvironment)
        {
            if (cloudEnvironment == CloudEnvironment.ProductionOverwrite ||
                cloudEnvironment == CloudEnvironment.ProductionPrimary ||
                cloudEnvironment == CloudEnvironment.ProductionSecondary)
            { return; }

            CompanyCollection companies = new CompanyCollection();
            companies.AddRange(companyRepository.Get(KATALONIA_COMPANY_ID));
            companies.AddToTransaction(transaction);

            foreach (CompanyEntity company in companies)
            { company.UseMonitoring = true; }

            companyRepository.Save(companies, false);
        }

        public void AddTestEmailsToOutlets(CloudEnvironment cloudEnvironment)
        {
            if (cloudEnvironment == CloudEnvironment.ProductionOverwrite ||
                cloudEnvironment == CloudEnvironment.ProductionPrimary ||
                cloudEnvironment == CloudEnvironment.ProductionSecondary)
            { return; }

            OutletCollection outlets = GetTestCompanyOutlets();

            if (!outlets.Any())
            { return; }

            OutletSellerInformationCollection outletSellers = outletSellerInformationRepository.Get(outlets.Where(x => x.OutletSellerInformationId.HasValue).Select(x => x.OutletSellerInformationId.Value));
            outletSellers.AddToTransaction(transaction);

            foreach (OutletSellerInformationEntity seller in outletSellers)
            { seller.ReceiptReceiversEmail = TEST_EMAIL_ADDRESSES; }

            outletSellerInformationRepository.Save(outletSellers, false);
        }

        private OutletCollection GetTestCompanyOutlets() =>
            outletRepository.GetOutletsByCompanyId(new List<int>() { KATALONIA_COMPANY_ID, KATA_V2_COMPANY_ID, APPALONIA_COMPANY_ID, TESTING_CO_COMPANY_ID });

        public void AddTestEmailsToRouteStepHandlers(CloudEnvironment cloudEnvironment)
        {
            if (cloudEnvironment == CloudEnvironment.ProductionOverwrite ||
                cloudEnvironment == CloudEnvironment.ProductionPrimary ||
                cloudEnvironment == CloudEnvironment.ProductionSecondary)
            { return; }

            AddTestEmailsToKataRouteStepHandlers();
        }

        private void AddTestEmailsToKataRouteStepHandlers()
        {
            RoutestephandlerCollection handlerCollection = routestepHandlerRepository.Get(new List<int>() { 
                KATALONIA_EMAIL_ROUTES_HANDLER_ID, 
                KATALONIA_EMAIL_ROUTE_WITH_HTML_HANDLER_ID, 
                KATALONIA_EMAIL_ACKNOWLEDGEMENT_HANDLER_ID, 
                KATA_V2_EMAIL_AND_CONSOLE_HANDLER_ID, 
                KATA_V2_EMAIL_HANDLER_ID,
                KATALONIA_EMAIL_DOCUMENT_HANDLER_ID,
                KATA_V2_EMAIL_DEFAULT_ROUTE_HANDLER_ID,
                KATA_V2_EMAIL_HTML_ROUTE_HANDLER_ID
            });

            handlerCollection.AddToTransaction(transaction);

            foreach (RoutestephandlerEntity handler in handlerCollection)
            { 
                handler.FieldValue1 = TEST_EMAIL_ADDRESSES; 
                handler.FieldValue2 = TEST_EMAIL_FROM_ADDRESS;
            }

            routestepHandlerRepository.Save(handlerCollection, false);
        }
    }
}