﻿using System;
using System.Collections.Generic;
using System.Text;
using Obymobi.Data.EntityClasses;
using System.Web.UI;
using Dionysos.Data.LLBLGen;
using Dionysos;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Logic.Cms
{
    public class UIElementHelper
    {
        #region Fields

        static UIElementHelper instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the UIElementHelper class if the instance has not been initialized yet
        /// </summary>
        public UIElementHelper()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new UIElementHelper();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static UIElementHelper instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Gets the UIElementEntity for the specified control
        /// </summary>
        /// <param name="control">The Control to get the UIElementEntity for</param>
        /// <returns>An UIElementEntity instance</returns>
        public static UIElementEntity GetUIElementEntity(Control control)
        {
            UIElementEntity uiElementEntity = null;

            if (Instance.ArgumentIsEmpty(control, "control"))
            {
                // Parameter 'control' is empty
            }
            else
            {
                uiElementEntity = UIElementEntity.GetUIElementByTypeName(control.GetType().BaseType.FullName, true);
            }

            return uiElementEntity;
        }

        #endregion
    }
}