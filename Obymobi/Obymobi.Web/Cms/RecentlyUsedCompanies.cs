﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Cms
{
    public class RecentlyUsedCompanies
    {
        private const int MaximumNumberOfItems = 10;
        private const string Separator = "-";
        public static RecentlyUsedCompanies Empty { get; } = new RecentlyUsedCompanies();

        private readonly List<int> companyIds = new List<int>();

        private RecentlyUsedCompanies()
        {
        }

        public RecentlyUsedCompanies(IEnumerable<int> companyIds)
        {
            this.companyIds.AddRange(companyIds.Distinct());
        }

        public IEnumerable<int> CompanyIds
        {
            get
            {
                return this.companyIds;
            }
        }

        public void AddMostRecentlyUsedCompany(int companyId)
        {
            this.AddTheMostRecentCompanyAtTheFirstPosition(companyId);

            this.ApplyMaximumItemLimit();
        }

        public void RemoveRecentlyUsedCompany(int companyId)
        {
            if (this.companyIds.Contains(companyId))
            {
                this.companyIds.Remove(companyId);
            }
        }

        public string ToCookieString()
        {
            return string.Join(Separator, this.companyIds);
        }

        public static RecentlyUsedCompanies ParseFromCookieString(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return new RecentlyUsedCompanies();
            }

            List<int> parsedCompanyIds = new List<int>();

            string[] companyIdStrings = input.Split(new[] { Separator }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string id in companyIdStrings)
            {
                if (int.TryParse(id, out int companyId))
                {
                    parsedCompanyIds.Add(companyId);
                }
            }

            return new RecentlyUsedCompanies(parsedCompanyIds);
        }

        private void AddTheMostRecentCompanyAtTheFirstPosition(int companyId)
        {
            if (this.companyIds.Contains(companyId))
            {
                this.companyIds.Remove(companyId);
            }

            this.companyIds.Insert(0, companyId);
        }

        private void ApplyMaximumItemLimit()
        {
            if (this.companyIds.Count > MaximumNumberOfItems)
            {
                this.companyIds.RemoveRange(MaximumNumberOfItems, this.companyIds.Count - MaximumNumberOfItems);
            }
        }
    }
}
