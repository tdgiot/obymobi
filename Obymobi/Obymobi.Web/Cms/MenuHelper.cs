﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Web;
using Dionysos;
using Dionysos.Interfaces;
using Dionysos.Web.UI;

namespace Obymobi.Logic.Cms
{
    public class MenuHelper
    {
        #region Fields

        static MenuHelper instance = null;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the MenuHelper class if the instance has not been initialized yet
        /// </summary>
        public MenuHelper()
        {
            if (instance == null)
            {
                // first create the instance
                instance = new MenuHelper();
                // Then init the instance (the init needs the instance to fill it)
                instance.Init();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the static MenuHelper instance
        /// </summary>
        private void Init()
        {
        }

        /// <summary>
        /// Retrieves a collection of MenuItems which are available to the current user based on the current UIElementId
        /// </summary>
        /// <param name="loadAllChilderen">Determine if child MenuItems should be pre-loaded</param>
        /// <returns>Collection of available MenuItems</returns>
        public static Dionysos.Web.UI.MenuItemCollection GetMenuItemCollection(UIElementEntity currentUIElement, bool loadAllChilderen)
        {
            // Get allowed modules with childeren, because we use views.			
            List<ILicenseModule> modules = LicenseUtil.GetLicensedModules();

            if (currentUIElement == null)
            {
                throw new ApplicationException("The requested page is not yet available in the UiElement table and therefore can not be shown.");
            }
            else if (modules.Count <= 0)
            {
                throw new ApplicationException("Application doesn't have any modules available to the user");
            }
            else
            {
                Dionysos.Web.UI.MenuItemCollection menuitems = new Dionysos.Web.UI.MenuItemCollection(null);
                // Currently the system doesn't support Recursion. Will build when needed
                foreach (ILicenseModule licenseModule in modules)
                {
                    ModuleEntity module = licenseModule as ModuleEntity;

                    MenuItem item = new MenuItem();
                    item.Enabled = true;
                    item.ImageUrl = module.IconPath;
                    item.NavigateUrl = module.DefaultUIElement.Url;

                    // Apply Translations / Globalization
                    item.Text = GlobalizationHelper.GetTranslation(module, ModuleFields.NameFull);
                    item.ToolTip = GlobalizationHelper.GetTranslation(module, ModuleFields.NameFull);

                    item.Value = module.NameSystem;
                    item.DataItem = module;
                    menuitems.Add(item);

                    // If this module is parent of uielement make it selected
                    item.Selected = currentUIElement.ModuleNameSystem == module.NameSystem;

                    // We load the childeren if the item is selected, because then they'll be
                    // used as submenu items, or when the function parameter forces it to do so.
                    if (item.Selected || loadAllChilderen)
                    {
                        // Create view of the prefetched uielements
                        EntityView<UIElementEntity> uiElements = module.UIElementCollection.DefaultView;

                        uiElements.Filter = new PredicateExpression(UIElementFields.DisplayInMenu == true);
                        uiElements.Sorter = new SortExpression(new SortClause(UIElementFields.SortOrder, SortOperator.Ascending));

                        foreach (UIElementEntity element in uiElements)
                        {
                            if (!element.IsAllowedForUser())
                                continue;

                            ILicenseUIElement licenseUIElement = element;
                            if (Instance.Empty(licenseUIElement))
                            {
                                throw new EmptyException("Variable 'element' cannot be casted into type 'Dionysos.Interfaces.ILicenseUIElement.'");
                            }
                            else
                            {
                                MenuItem elementitem = new MenuItem();
                                elementitem.Enabled = true;

                                elementitem.Text = GlobalizationHelper.GetTranslation(element, UIElementFields.NameFull);
                                elementitem.ToolTip = GlobalizationHelper.GetTranslation(element, UIElementFields.NameFull);

                                elementitem.NavigateUrl = element.Url;
                                elementitem.DataItem = element;

                                elementitem.Value = element.TypeNameFull;
                                elementitem.Parent = item;
                                item.ChildItems.Add(elementitem);

                                // If this is the selected item mark the item and it's parent as selected
                                if (element.UIElementId == currentUIElement.UIElementId)
                                {
                                    elementitem.Selected = true;
                                    elementitem.Parent.Selected = true;
                                }
                            }
                        }
                    }
                }
                return menuitems;
            }
        }

        /// <summary>
        /// Get the PageMode for the current User for a certain UIElement
        /// </summary>
        /// <param name="UIElementId">UIElementId of the UIElement to check for</param>
        /// <returns>PageMode for the current User</returns>
        public static PageMode GetAllowedPageModeForUIElement(int UIElementId)
        {
            return PageMode.Add;
        }

        #endregion
    }
}