﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Web;
using Obymobi.Data.HelperClasses;
using Dionysos;
using Dionysos.Web.Security;
using System.Web;
using Obymobi.Enums;

namespace Obymobi.Logic.Cms
{
    public static class UserRightsHelper
    {
        private static readonly Dictionary<Role, int> RoleMappings = new Dictionary<Role, int>() 
        {
            {Role.GodMode, 8},
            {Role.Administrator, 5},
            {Role.Reseller, 7},
            {Role.Customer, 6},
            {Role.Console, 9},
            {Role.Supervisor, 10}
        };

        public static bool IsModuleAllowedForUser(this int moduleId)
        {
            return new ModuleEntity(moduleId).IsAllowedForUser();
        }

        public static bool IsAllowedForUser(this ModuleEntity module)
        {
            bool toReturn = false;

            if (module.UserRightsFree)
            {
                toReturn = true;
            }
            else if (UserManager.CurrentUser == null)
            {
                toReturn = false;
            }
            else
            {
                UserEntity user = UserManager.CurrentUser as UserEntity;
                
                toReturn = IsModuleAllowedForRole(user.Role, module);
            }

            return toReturn;
        }

        public static bool IsModuleAllowedForRole(Role role, ModuleEntity module)
        {
            bool moduleAllowed;

            if (module.UserRightsFree)
            {
                moduleAllowed = true;
            }
            else if (role >= Role.Administrator)
            {
                moduleAllowed = true;
            }
            else
            {
                string cacheKey = string.Format("Obymobi.Logic.Cms.RoleModuleRightsView.ModuleIsAllowed.{0}.{1}", role, module.ModuleId);

                if (!CacheHelper.TryGetValue<bool>(cacheKey, false, out moduleAllowed))
                {
                    EntityView<RoleModuleRightsEntity> view = GetRoleModuleRightsView(RoleMappings[role]);
                    view.Filter = new PredicateExpression(RoleModuleRightsFields.ModuleId == module.ModuleId);

                    moduleAllowed = view.Count == 1 && view[0].IsAllowed;

                    CacheHelper.AddSlidingExpire(false, cacheKey, moduleAllowed, 600);
                }
            }

            return moduleAllowed;
        }

        private static string GetUiElementRightsCacheKeyPrefix(Role role)
        {
            return string.Format("Obymobi.Logic.Cms.RoleModuleRightsView.{0}.", role);
        }

        /// <summary>
        /// Remove all cached UiElementsRights cache value. Should be used when user roles changes or sign's on again.
        /// </summary>
        /// <param name="userId"></param>
        public static void ClearUiElementRichtsCacheForUser(Role role)
        {
            if (HttpContext.Current != null && HttpContext.Current.Cache != null)
                CacheHelper.RemoveMultiple(false, UserRightsHelper.GetUiElementRightsCacheKeyPrefix(role));
        }

        public static bool IsUIElementAllowedForUser(this string uiElementTypeNameFull)
        {
            return UIElementEntity.GetUIElementByTypeName(uiElementTypeNameFull, false).IsAllowedForUser();
        }

        public static bool IsAllowedForUser(this UIElementEntity uiElement)
        {
            bool toReturn = false;

            if (uiElement.UserRightsFree)
            {
                toReturn = true;
            }
            else if (UserManager.CurrentUser == null)
            {
                toReturn = false;
            }
            else
            {
                UserEntity user = UserManager.CurrentUser as UserEntity;

                toReturn = IsUiElementAllowedForRole(user.Role, uiElement);
            }

            return toReturn;
        }

        public static bool IsUiElementAllowedForRole(Role role, UIElementEntity uiElement)
        {
            bool elementAllowed;

            if (uiElement.UserRightsFree)
            {
                elementAllowed = true;
            }
            else if (role >= Role.Administrator)
            {
                elementAllowed = true;
            }
            else
            {
                string cacheKey = GetUiElementRightsCacheKeyPrefix(role) + uiElement.TypeNameFull;

                if (!CacheHelper.TryGetValue<bool>(cacheKey, false, out elementAllowed))
                {
                    EntityView<RoleUIElementRightsEntity> view = GetRoleUiElementRightsView(RoleMappings[role]);
                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(RoleUIElementRightsFields.UiElementTypeNameFull == uiElement.TypeNameFull);
                    view.Filter = filter;

                    elementAllowed = view.Count == 1 && view[0].IsAllowed;

                    CacheHelper.AddSlidingExpire(false, cacheKey, elementAllowed, 600);
                }
            }

            return elementAllowed;
        }

        public static bool IsReadOnlyForUser(this UIElementEntity uiElement)
        {
            bool toReturn = false;

            if (uiElement.UserRightsFree)
            {
                toReturn = true;
            }
            else if (UserManager.CurrentUser == null)
            {
                toReturn = true;
            }
            else
            {
                UserEntity user = UserManager.CurrentUser as UserEntity;

                // Check if we have Cached Resuls
                string cacheKey = String.Format("Obymobi.Logic.Cms.RoleModuleRightsView.UIelementIsReadOnly.{0}.{1}", user.UserId, uiElement.TypeNameFull);
                if (!CacheHelper.TryGetValue<bool>(cacheKey, false, out toReturn))
                {
                    if (user.Role >= Enums.Role.Administrator)
                    {
                        toReturn = true;
                    }
                    else
                    {
                        EntityView<RoleUIElementRightsEntity> view = UserRightsHelper.GetRoleUiElementRightsView(RoleMappings[user.Role]);
                        PredicateExpression filter = new PredicateExpression();
                        filter.Add(RoleUIElementRightsFields.UiElementTypeNameFull == uiElement.TypeNameFull);
                        view.Filter = filter;

                        if (view.Count == 1)
                        {
                            toReturn = view[0].IsReadOnly;
                        }
                        else
                        {
                            toReturn = true;
                        }
                    }

                    CacheHelper.AddSlidingExpire(false, cacheKey, toReturn, 600);
                }
            }

            return toReturn;
        }

        private static EntityView<RoleModuleRightsEntity> GetRoleModuleRightsView(int roleId)
        {
            // Try Cache
            EntityView<RoleModuleRightsEntity> view;

            if (!CacheHelper.TryGetValue<EntityView<RoleModuleRightsEntity>>("Obymobi.Logic.Cms.RoleModuleRightsView." + roleId, false, out view))
            {
                PredicateExpression filter = new PredicateExpression(RoleModuleRightsFields.RoleId == roleId);
                RoleModuleRightsCollection rmrs = new RoleModuleRightsCollection();
                rmrs.GetMulti(filter);

                view = rmrs.DefaultView;
            }

            return view;
        }

        private static EntityView<RoleUIElementRightsEntity> GetRoleUiElementRightsView(int roleId)
        {
            // Try Cache
            EntityView<RoleUIElementRightsEntity> view;

            if (!CacheHelper.TryGetValue<EntityView<RoleUIElementRightsEntity>>("Obymobi.Logic.Cms.GetRoleUiElementRightsView." + roleId, false, out view))
            {
                PredicateExpression filter = new PredicateExpression(RoleUIElementRightsFields.RoleId == roleId);
                RoleUIElementRightsCollection rurs = new RoleUIElementRightsCollection();
                rurs.GetMulti(filter);

                view = rurs.DefaultView;
            }

            return view;
        }

    }

}
