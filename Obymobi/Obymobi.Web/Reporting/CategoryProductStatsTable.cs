﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using SD.LLBLGen.Pro.QuerySpec.SelfServicing;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Dionysos;
using Dionysos.Data;
using System.Data.SqlClient;

namespace Obymobi.Logic.Analytics
{
    /// <summary>
    /// Summary description for MenuStatsTable
    /// </summary>
    public class CategoryProductStatsTable : TypedStatsTable<CategoryProductStatsTable.CategoryProductStatsRow>
    {
        #region Data Table setup

        private Filter filter;
        private Dictionary<string, CategoryProductStatsRow> categoryIdProductIdLookup;
        private Dictionary<int, List<CategoryProductStatsRow>> productIdLookup;
        private Dictionary<int, int> quantityPerProductId;
        private Dictionary<int, decimal> revenuePerProductId;
        private Dictionary<int, int> quantityPerCategoryId;
        private Dictionary<int, decimal> revenuePerCategoryId;
        private List<CategoryProductStatsRow> deletedProductRows;

        public CategoryProductStatsTable(Filter filter)
        {
            this.filter = filter;

            // Initialize columns
            this.Columns.Add(new DataColumn(ColumnNames.MenuId, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.MenuName, typeof(string)));
            this.Columns.Add(new DataColumn(ColumnNames.CompanyId, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.CompanyName, typeof(string)));
            this.Columns.Add(new DataColumn(ColumnNames.ParentCategoryId, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.CategoryId, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.ProductId, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.CategoryName, typeof(string)));
            this.Columns.Add(new DataColumn(ColumnNames.ProductName, typeof(string)));
            this.Columns.Add(new DataColumn(ColumnNames.TotalQuantity, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.TotalRevenue, typeof(decimal)));

            // Init private variables
            this.productIdLookup = new Dictionary<int, List<CategoryProductStatsRow>>();
            this.quantityPerProductId = new Dictionary<int, int>();
            this.quantityPerCategoryId = new Dictionary<int, int>();
            this.revenuePerProductId = new Dictionary<int, decimal>();
            this.revenuePerCategoryId = new Dictionary<int, decimal>();
            this.categoryIdProductIdLookup = new Dictionary<string, CategoryProductStatsRow>();
            this.deletedProductRows = null;
        }

        protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
        {
            return new CategoryProductStatsRow(builder);
        }

        #endregion

        #region Retrieval Methods

        public void FetchMenuStats()
        {
            // Fetch the 'basic' data (meaning, no stats per Deliverypointgroup)
            TypedListDAO dao = new TypedListDAO();
            string sqlQuery = this.GetMenuStatsQuery();
            DataTable queryResult = new DataTable();
            SqlQueryHelper.ExecuteRetrievalQuery(queryResult, sqlQuery);

            // Conver result to typed DataRows
            foreach (DataRow sourceRow in queryResult.Rows)
            {
                // Copy/convert values
                CategoryProductStatsRow targetRow = this.NewRow();
                targetRow.MenuId = sourceRow[ColumnNames.MenuId] != DBNull.Value ? (int)sourceRow[ColumnNames.MenuId] : 0;
                targetRow.MenuName = sourceRow[ColumnNames.MenuName] != DBNull.Value ? (string)sourceRow[ColumnNames.MenuName] : string.Empty;
                targetRow.CompanyId = sourceRow[ColumnNames.CompanyId] != DBNull.Value ? (int)sourceRow[ColumnNames.CompanyId] : 0;
                targetRow.CompanyName = sourceRow[ColumnNames.CompanyName] != DBNull.Value ? (string)sourceRow[ColumnNames.CompanyName] : string.Empty;
                targetRow.CategoryId = sourceRow[ColumnNames.CategoryId] != DBNull.Value ? (int)sourceRow[ColumnNames.CategoryId] : 0;
                targetRow.ProductId = sourceRow[ColumnNames.ProductId] != DBNull.Value ? (int)sourceRow[ColumnNames.ProductId] : 0;
                targetRow.TotalQuantity = sourceRow[ColumnNames.TotalQuantity] != DBNull.Value ? (int)sourceRow[ColumnNames.TotalQuantity] : 0;
                targetRow.TotalRevenue = sourceRow[ColumnNames.TotalRevenue] != DBNull.Value ? (decimal)sourceRow[ColumnNames.TotalRevenue] : 0;

                // Add to lookup
                string key;
                if (targetRow.TryGetCategoryIdProductIdString(out key)) this.categoryIdProductIdLookup.Add(key, targetRow);

                // Product Quantity and Revenue stats
                if (!this.productIdLookup.ContainsKey(targetRow.ProductId))
                {
                    this.productIdLookup.Add(targetRow.ProductId, new List<CategoryProductStatsRow>());
                    this.quantityPerProductId.Add(targetRow.ProductId, 0);
                    this.revenuePerProductId.Add(targetRow.ProductId, 0);
                }
                this.productIdLookup[targetRow.ProductId].Add(targetRow);
                this.quantityPerProductId[targetRow.ProductId] += targetRow.TotalQuantity;
                this.revenuePerProductId[targetRow.ProductId] += targetRow.TotalRevenue;

                // Category Quantity and Revenue stats
                if (!this.quantityPerCategoryId.ContainsKey(targetRow.CategoryId))
                {
                    this.quantityPerCategoryId.Add(targetRow.CategoryId, 0);
                    this.revenuePerCategoryId.Add(targetRow.CategoryId, 0);
                }
                this.quantityPerCategoryId[targetRow.CategoryId] += targetRow.TotalQuantity;
                this.revenuePerCategoryId[targetRow.CategoryId] += targetRow.TotalRevenue;

                this.Rows.Add(targetRow);
            }

            return;
        }

        #endregion

        #region Product-based Reporting

        public void AppendProductsWorksheet(SLDocument spreadsheet, bool productOverview, bool productAndCategoryOverview)
        {
            // Load Products
            ProductCollection products = this.LoadProducts();

            // Append the requested views, i.e. Products and/or 'Products and Categories'
            if (productOverview) this.AppendProductsWorksheet(spreadsheet, products, false);
            if (productAndCategoryOverview) this.AppendProductsWorksheet(spreadsheet, products, true);
        }

        private void AppendProductsWorksheet(SLDocument spreadsheet, ProductCollection products, bool groupByCategory)
        {
            string worksheetName = groupByCategory ? "Products & Categories" : "Products";

            // Create and select worksheet
            spreadsheet.AddWorksheet(worksheetName);
            spreadsheet.SelectWorksheet(worksheetName);

            // Set Headings + styling
            spreadsheet.SetCellValue("A1", "Sort Order");
            if (groupByCategory) spreadsheet.SetCellValue("B1", "Product / Category");
            else spreadsheet.SetCellValue("B1", "Product");
            spreadsheet.SetCellValue("C1", "Product");
            spreadsheet.SetCellValue("D1", "Category");
            spreadsheet.SetCellValue("E1", "Quantity");
            spreadsheet.SetCellValue("F1", "Revenue");
            spreadsheet.SetStyle("A1", "F1", borders: Borders.Bottom, backgroundColor: SpreadsheetHelper.Green14);
            spreadsheet.SetStyle("E1", "F1", horizontalAlignment: DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Right);

            // ProductIds sorted by Quantity
            var productIdsSorted = this.quantityPerProductId.OrderByDescending(x => x.Value);

            // Show all products which are not deleted
            int currentRow = 2;
            foreach (var productId in productIdsSorted)
            {
                // Deleted products are dealt with later
                if (productId.Key == 0) continue;

                // Look up the product
                ProductEntity product = products.FirstOrDefault(x => x.ProductId == productId.Key);

                // Render the line if we have the details.
                if (product != null && this.productIdLookup.ContainsKey(product.ProductId))
                {
                    this.AppendRowForProduct(spreadsheet, product, this.productIdLookup[product.ProductId], ref currentRow, groupByCategory);
                }
                else
                {
                    this.AppendRowForNonExistingProduct(spreadsheet, "ProductId: " + productId.Key, this.productIdLookup[productId.Key], ref currentRow);
                }
            }

            // Show all deleted products 
            List<CategoryProductStatsRow> deletedProductRows = this.FetchDeletedProducts();
            if (deletedProductRows.Count > 0)
            {
                if (groupByCategory)
                {
                    // Create a header for the deleted products
                    spreadsheet.SetCellValue("B" + currentRow, "Deleted products");
                    spreadsheet.SetCellValue("E" + currentRow, deletedProductRows.Sum(x => x.TotalQuantity));
                    spreadsheet.SetCellValue("F" + currentRow, deletedProductRows.Sum(x => x.TotalRevenue));
                    spreadsheet.SetStyle("F" + currentRow, formatCode: SpreadsheetHelper.DecimalFormatCode);
                    if (groupByCategory) spreadsheet.SetStyle("A" + currentRow, "F" + currentRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green15, backgroundColor: SpreadsheetHelper.Green10);

                    currentRow++;
                }

                int deletedProductsStartRow = currentRow;
                foreach (CategoryProductStatsRow row in deletedProductRows)
                {
                    string productName = row.CategoryName + " - " + row.ProductName;
                    if (!groupByCategory)
                        productName += " (deleted)";
                    this.AppendRowForNonExistingProduct(spreadsheet, productName, new List<CategoryProductStatsRow>() { row }, ref currentRow);
                }

                if (groupByCategory) spreadsheet.Sort("A" + deletedProductsStartRow, "F" + currentRow, "E", false);
            }

            // Set column widths
            spreadsheet.SetColumnWidth("B", 76);
            spreadsheet.SetColumnWidth("E", 8.5);
            spreadsheet.SetColumnWidth("F", 11);

            // Hide: Columns Sort Order (A), Category (C), Product (D)
            spreadsheet.HideColumn("A");
            spreadsheet.HideColumn("C");
            spreadsheet.HideColumn("D");

            // Keep first line frozen
            spreadsheet.FreezePanes(1, 0);

            // Set filters (if you use the filter in Excel it will crash Excel - therefore disabled)
            //spreadsheet.Filter("A1", "F1");

            // Sort
            if (!groupByCategory) spreadsheet.Sort("A2", "F" + currentRow, "E", false);

            // Set printing to 1 page width
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 500);
            spreadsheet.SetPageSettings(pageSettings);
        }

        private List<CategoryProductStatsRow> FetchDeletedProducts()
        {
            if (this.deletedProductRows != null)
                return this.deletedProductRows;

            DataTable queryResult = new DataTable();
            string sqlQuery = this.GetMenuStatsQueryDeletedProducts();
            SqlQueryHelper.ExecuteRetrievalQuery(queryResult, sqlQuery);

            this.deletedProductRows = new List<CategoryProductStatsRow>();

            // Convert result to typed rows
            foreach (DataRow sourceRow in queryResult.Rows)
            {
                CategoryProductStatsRow targetRow = this.NewRow();
                targetRow.MenuId = -1;
                targetRow.MenuName = string.Empty;
                targetRow.CompanyId = sourceRow[ColumnNames.CompanyId] != DBNull.Value ? (int)sourceRow[ColumnNames.CompanyId] : 0;
                targetRow.CompanyName = sourceRow[ColumnNames.CompanyName] != DBNull.Value ? (string)sourceRow[ColumnNames.CompanyName] : string.Empty;
                targetRow.CategoryName = sourceRow[ColumnNames.CategoryName] != DBNull.Value ? (string)sourceRow[ColumnNames.CategoryName] : string.Empty;
                targetRow.ProductName = sourceRow[ColumnNames.ProductName] != DBNull.Value ? (string)sourceRow[ColumnNames.ProductName] : string.Empty;
                targetRow.TotalQuantity = sourceRow[ColumnNames.TotalQuantity] != DBNull.Value ? (int)sourceRow[ColumnNames.TotalQuantity] : 0;
                targetRow.TotalRevenue = sourceRow[ColumnNames.TotalRevenue] != DBNull.Value ? (decimal)sourceRow[ColumnNames.TotalRevenue] : 0;
                this.deletedProductRows.Add(targetRow);
            }

            return this.deletedProductRows;
        }

        private void AppendRowForNonExistingProduct(SLDocument spreadsheet, string productName, List<CategoryProductStatsRow> rows, ref int currentRow)
        {
            ProductEntity product = new ProductEntity();
            product.Name = productName;

            this.AppendRowForProduct(spreadsheet, product, rows, ref currentRow, false);
        }

        private void AppendRowForProduct(SLDocument spreadsheet, ProductEntity product, List<CategoryProductStatsRow> rows, ref int currentRow, bool groupByCategory)
        {
            int productRow = currentRow;
            currentRow++;
            // Columns: Sort Order, Product/Category, Product, Category, Quantity, Reveneu
            spreadsheet.SetCellValue("A" + productRow, currentRow);
            spreadsheet.SetCellValue("B" + productRow, product.Name);
            spreadsheet.SetCellValue("C" + productRow, product.Name);
            spreadsheet.SetCellValue("D" + productRow, string.Empty);

            int quantity = 0;
            decimal revenue = 0;
            System.Drawing.Color categoryBorderColor = SpreadsheetHelper.Green4;

            // Each Product can appear in multiple rows if it's orderable from different Categories
            foreach (CategoryProductStatsRow row in rows)
            {
                quantity += row.TotalQuantity;
                revenue += row.TotalRevenue;

                if (groupByCategory)
                {
                    // Get Category Name from Product
                    string categoryName = string.Empty;
                    if (row.CategoryId > 0)
                    {
                        ProductCategoryEntity productCategory = product.ProductCategoryCollection.FirstOrDefault(x => x.CategoryId == row.CategoryId);
                        CategoryEntity category;
                        if (productCategory != null) category = productCategory.CategoryEntity;
                        else category = new CategoryEntity(row.CategoryId);
                        categoryName = "{0} ({1})".FormatSafe(category.FullCategoryName, category.MenuEntity.Name);
                    }
                    else
                    {
                        categoryName = "Deleted category: " + row.CategoryId;
                    }

                    spreadsheet.SetCellValue("A" + currentRow, currentRow);
                    spreadsheet.SetCellValue("B" + currentRow, "".PadLeft(4, ' ') + categoryName);
                    spreadsheet.SetCellValue("C" + currentRow, string.Empty);
                    spreadsheet.SetCellValue("D" + currentRow, categoryName);
                    spreadsheet.SetCellValue("E" + currentRow, row.TotalQuantity);
                    spreadsheet.SetCellValue("F" + currentRow, row.TotalRevenue);
                    spreadsheet.SetStyle("A" + currentRow, "F" + currentRow, borders: Borders.Bottom, borderColor: categoryBorderColor);
                    spreadsheet.SetStyle("F" + currentRow, formatCode: SpreadsheetHelper.DecimalFormatCode);
                    currentRow++;
                }
            }

            spreadsheet.SetCellValue("E" + productRow, quantity);
            spreadsheet.SetCellValue("F" + productRow, revenue);

            // Set color to grey-ish for deleted products
            Nullable<System.Drawing.Color> textColor = null;
            if (product.IsNew) textColor = System.Drawing.Color.FromArgb(128, 128, 128); // IsNew would mean it's deleted                     

            // Only background when including categories, otherwise borders
            if (groupByCategory)
            {
                spreadsheet.SetStyle("A" + productRow, "F" + productRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green15, backgroundColor: SpreadsheetHelper.Green10, textColor: textColor);
            }
            else
            {
                spreadsheet.SetStyle("A" + productRow, "F" + productRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green4);
            }
            spreadsheet.SetStyle("F" + productRow, formatCode: SpreadsheetHelper.DecimalFormatCode);
        }

        private ProductCollection LoadProducts()
        {
            ProductCollection products = new ProductCollection();

            var productFilter = new PredicateExpression();
            productFilter.Add(ProductFields.CompanyId == this.filter.CompanyId);

            var sort = new SortExpression(ProductFields.Name | SortOperator.Ascending);

            var includeFields = new IncludeFieldsList();
            includeFields.Add(ProductFields.Name);

            var path = new PrefetchPath(EntityType.ProductEntity);
            path.Add(ProductEntity.PrefetchPathProductCategoryCollection).SubPath.Add(ProductCategoryEntity.PrefetchPathCategoryEntity);

            products.GetMulti(productFilter, 0, sort, null, path);

            return products;
        }

        #endregion

        #region Category-based Reporting

        private int duplicateWorksheets = 0;
        public void AppendCategoryAndProductsWorksheetPerMenu(SLDocument spreadsheet, bool categoryOverview, bool categoryAndProductOverview)
        {
            // Fetch the Menu's (which are sorted nicely!)
            MenuCollection menus = this.LoadMenus();

            duplicateWorksheets = 0;
            foreach (var menu in menus)
            {
                // Render the requested version, i.e. 'Category' and/or 'Category and Product' overview
                if (categoryOverview) this.AppendCategoryAndProductWorksheetForMenu(menu, false, spreadsheet);
                if (categoryAndProductOverview) this.AppendCategoryAndProductWorksheetForMenu(menu, true, spreadsheet);
            }
        }

        private void AppendCategoryAndProductWorksheetForMenu(MenuEntity menu, bool renderProducts, SLDocument spreadsheet)
        {
            // Prepare worksheet name (name is max 31 chars -/- 6 for ' - C&P')
            string worksheetSubName = menu.Name;
            if (worksheetSubName.Length > 24)
            {
                worksheetSubName = worksheetSubName.Substring(0, 24);
            }
            string worksheetName = worksheetSubName + (renderProducts ? " - C&P" : " - C");
            if (spreadsheet.GetWorksheetNames().Contains(worksheetName))
            {
                duplicateWorksheets++;
                worksheetName = string.Format("{0}{1}{2}", worksheetSubName, duplicateWorksheets, (renderProducts ? " - C&P" : " - C"));
            }

            // Create and select worksheet
            spreadsheet.AddWorksheet(worksheetName);
            spreadsheet.SelectWorksheet(worksheetName);

            // Set Headers + Styling 
            spreadsheet.SetCellValue("A1", "Sort Order");
            if (renderProducts) spreadsheet.SetCellValue("B1", "Category / Product");
            else spreadsheet.SetCellValue("B1", "Category");
            spreadsheet.SetCellValue("C1", "Category");
            spreadsheet.SetCellValue("D1", "Product");
            spreadsheet.SetCellValue("E1", "Quantity");
            spreadsheet.SetCellValue("F1", "Revenue");
            spreadsheet.SetStyle("A1", "F1", borders: Borders.Bottom, backgroundColor: SpreadsheetHelper.Green14);
            spreadsheet.SetStyle("E1", "F1", horizontalAlignment: DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Right);

            // Recursively add Categories & Products
            int row = 2;
            int indent = 0;
            this.AppendRowsToCategoryAndProductWorksheet(spreadsheet, menu.CategoryCollection, renderProducts, ref row, ref indent);

            // Add the deleted products
            this.AppendDeletedProductsToCategoryAndProductworksheetForMenu(spreadsheet, renderProducts, ref row);

            // Make first row filter row
            spreadsheet.SetColumnWidth("B", 76);
            spreadsheet.SetColumnWidth("E", 8.5);
            spreadsheet.SetColumnWidth("F", 11);

            // Hide: Columns Sort Order (A), Category (C), Product (D)
            spreadsheet.HideColumn("A");
            spreadsheet.HideColumn("C");
            spreadsheet.HideColumn("D");

            // Keep first line frozen
            spreadsheet.FreezePanes(1, 0);

            // Set printing to 1 page width
            var pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 45);
            spreadsheet.SetPageSettings(pageSettings);
        }

        private void AppendDeletedProductsToCategoryAndProductworksheetForMenu(SLDocument spreadsheet, bool renderProducts, ref int rowIndex)
        {
            // Fetch deleted products
            List<CategoryProductStatsRow> deletedProducts = this.FetchDeletedProducts();

            // No business here when there are no deleted products
            if (deletedProducts.Count == 0) return;

            // Render heading 'Deleted products'
            this.AppendRowToCategoryAndProductWorksheet(spreadsheet, 0, string.Empty, "Deleted products",
                deletedProducts.Sum(x => x.TotalQuantity),
                deletedProducts.Sum(x => x.TotalRevenue), rowIndex);
            rowIndex++;

            if (renderProducts)
            {
                // Render the products
                foreach (CategoryProductStatsRow row in deletedProducts)
                {
                    string productName = row.CategoryName + " - " + row.ProductName;
                    this.AppendRowToCategoryAndProductWorksheet(spreadsheet, 1, productName, string.Empty, row.TotalQuantity, row.TotalRevenue, rowIndex);
                    rowIndex++;
                }
            }
        }

        /// <summary>
        /// Helper class to throw around Quantity and Revenue numbers
        /// </summary>
        private struct QuantityRevenue
        {
            public int Quantity;
            public decimal Revenue;

            public QuantityRevenue(int quantity = 0, decimal revenue = 0)
            {
                this.Quantity = quantity;
                this.Revenue = revenue;
            }
        }

        private QuantityRevenue AppendRowsToCategoryAndProductWorksheet(SLDocument spreadsheet, CategoryCollection categories, bool renderProducts, ref int row, ref int indent)
        {
            QuantityRevenue toReturnTotals = new QuantityRevenue();
            foreach (var category in categories)
            {

                // At the zero indented level we only want parent categories.
                if (indent == 0 && category.ParentCategoryId.HasValue) continue;

                // Render this category first                
                int categoryRow = row; // It's rendered at the end when we have all the details
                row++;
                indent++;

                // Iterate over Products
                QuantityRevenue categoryTotals = new QuantityRevenue();
                var sortedProducts = category.ProductCategoryCollection.OrderBy(x => x.SortOrder);
                foreach (ProductCategoryEntity productCategory in sortedProducts)
                {
                    // Find the relevant line in the datatable
                    //string selectExpression = "{0} = {1} AND [{2}] = {3}".FormatSafe(ColumnNames.ProductId, productCategory.ProductId, ColumnNames.CategoryId, category.CategoryId);
                    //var productDetailsRow = this.GetRow(selectExpression) as CategoryProductStatsRow;
                    CategoryProductStatsRow productDetailsRow;
                    this.categoryIdProductIdLookup.TryGetValue(productCategory.CategoryId + "." + productCategory.ProductId, out productDetailsRow);
                    int productQuantity = 0;
                    decimal productRevenue = 0;

                    // Update the Totals for the Product and Category                  
                    if (productDetailsRow != null)
                    {
                        productQuantity = productDetailsRow.TotalQuantity;
                        productRevenue = productDetailsRow.TotalRevenue;
                        categoryTotals.Quantity += productQuantity;
                        categoryTotals.Revenue += productRevenue;
                    }

                    if (renderProducts)
                    {
                        int productRow = row;
                        row++;
                        this.AppendRowToCategoryAndProductWorksheet(spreadsheet, indent, productCategory.ProductEntity.Name, string.Empty, productQuantity, productRevenue, productRow);
                    }
                }

                // Render any child categories + get their totals
                QuantityRevenue totalsFromChilderen = this.AppendRowsToCategoryAndProductWorksheet(spreadsheet, category.ChildCategoryCollection, renderProducts, ref row, ref indent);
                categoryTotals.Quantity += totalsFromChilderen.Quantity;
                categoryTotals.Revenue += totalsFromChilderen.Revenue;

                indent--;

                // Render Category Heading
                this.AppendRowToCategoryAndProductWorksheet(spreadsheet, indent, string.Empty, category.Name, categoryTotals.Quantity, categoryTotals.Revenue, categoryRow);

                // Keep track of totals
                toReturnTotals.Quantity += categoryTotals.Quantity;
                toReturnTotals.Revenue += categoryTotals.Revenue;
            }

            return toReturnTotals;
        }

        private void AppendRowToCategoryAndProductWorksheet(SLDocument spreadsheet, int indent, string productName, string categoryName, int quantity, decimal revenue, int rowIndex)
        {
            if (!productName.IsNullOrWhiteSpace() && !categoryName.IsNullOrWhiteSpace())
            {
                throw new ArgumentException("ProductName and CategoryName can't be used at the same time, pick one, choose wisely...");
            }

            spreadsheet.SetCellValue("A" + rowIndex, rowIndex);
            spreadsheet.SetCellValue("B" + rowIndex, "".PadLeft((indent * 4) + 1, ' ') + categoryName + productName); // Can be done, because only 1 should be set.
            spreadsheet.SetCellValue("C" + rowIndex, categoryName);
            spreadsheet.SetCellValue("D" + rowIndex, productName);
            spreadsheet.SetCellValue("E" + rowIndex, quantity);
            spreadsheet.SetCellValue("F" + rowIndex, revenue);

            // Styling dependent on the Row Type
            if (!categoryName.IsNullOrWhiteSpace())
            {
                spreadsheet.SetCellBackground(indent, true, "A" + rowIndex, "F" + rowIndex);
                spreadsheet.SetStyle("A" + rowIndex, "F" + rowIndex, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green15);
            }
            else
            {
                spreadsheet.SetStyle("A" + rowIndex, "F" + rowIndex, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green4);
            }

            spreadsheet.SetStyle("F" + rowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode);
        }

        private MenuCollection LoadMenus()
        {
            MenuCollection menus = new MenuCollection();
            PredicateExpression filter = new PredicateExpression(MenuFields.CompanyId == this.filter.CompanyId);
            SortExpression sort = new SortExpression(MenuFields.Name | SortOperator.Ascending);
            RelationCollection joins = new RelationCollection();

            if (this.filter.DeliverypointgroupIds != null && this.filter.DeliverypointgroupIds.Count > 0)
            {
                joins.Add(MenuEntity.Relations.DeliverypointgroupEntityUsingMenuId);
                filter.Add(DeliverypointgroupFields.DeliverypointgroupId == this.filter.DeliverypointgroupIds);
            }

            SortExpression categorySort = new SortExpression(CategoryFields.SortOrder | SortOperator.Ascending);

            // Prefetch the Child categories up to 5 levels            
            var pathChildCategoryCollection1 = CategoryEntity.PrefetchPathChildCategoryCollection;
            pathChildCategoryCollection1.Sorter = categorySort;
            pathChildCategoryCollection1.SubPath.Add(CategoryEntity.PrefetchPathProductEntity);
            var pathChildCategoryCollection2 = CategoryEntity.PrefetchPathChildCategoryCollection;
            pathChildCategoryCollection2.Sorter = categorySort;
            pathChildCategoryCollection2.SubPath.Add(CategoryEntity.PrefetchPathProductEntity);
            var pathChildCategoryCollection3 = CategoryEntity.PrefetchPathChildCategoryCollection;
            pathChildCategoryCollection3.Sorter = categorySort;
            pathChildCategoryCollection3.SubPath.Add(CategoryEntity.PrefetchPathProductEntity);
            var pathChildCategoryCollection4 = CategoryEntity.PrefetchPathChildCategoryCollection;
            pathChildCategoryCollection4.Sorter = categorySort;
            pathChildCategoryCollection4.SubPath.Add(CategoryEntity.PrefetchPathProductEntity);
            var pathChildCategoryCollection5 = CategoryEntity.PrefetchPathChildCategoryCollection;
            pathChildCategoryCollection5.Sorter = categorySort;
            pathChildCategoryCollection5.SubPath.Add(CategoryEntity.PrefetchPathProductEntity);

            PrefetchPath path = new PrefetchPath(EntityType.MenuEntity);
            path.Add(MenuEntity.PrefetchPathCategoryCollection, 0, null, null, categorySort)
                .SubPath.Add(pathChildCategoryCollection1)
                .SubPath.Add(pathChildCategoryCollection2)
                .SubPath.Add(pathChildCategoryCollection3)
                .SubPath.Add(pathChildCategoryCollection4);

            menus.GetMulti(filter, 0, sort, joins, path);

            return menus;
        }


        #endregion

        #region Summary Reporting

        public void AppendToSummaryWorksheet(SLDocument spreadsheet, string worksheetName, ref int rowIndex)
        {
            spreadsheet.SelectWorksheet(worksheetName);
            int topX = 10;
            int startRowIndex = rowIndex;

            List<string> columnsStandard = new List<string>() { "A", "B", "C" };
            List<string> columnsRequests = new List<string>() { "F", "G", "H" };
            int currentRowIndex = startRowIndex;

            DataTable queryResult;

            // Standard: Render Product Top 10's by Quantity                        
            queryResult = this.FetchSummaryTableData(OrderType.Standard, EntityType.ProductEntity, ColumnNames.TotalQuantity, topX);
            this.RenderSummaryTable(spreadsheet, "Top 10 Products ordered by Quantity", columnsStandard, queryResult, topX, currentRowIndex);

            // Service Requests: Render Product Top 10's by Quantity            
            queryResult = this.FetchSummaryTableData(OrderType.RequestForService, EntityType.ProductEntity, ColumnNames.TotalQuantity, topX);
            this.RenderSummaryTable(spreadsheet, "Top 10 Products ordered by Quantity", columnsRequests, queryResult, topX, currentRowIndex);

            currentRowIndex += 13;
            // Standard: Render Category Top 10's by Quantity           
            queryResult = this.FetchSummaryTableData(OrderType.Standard, EntityType.CategoryEntity, ColumnNames.TotalQuantity, topX);
            this.RenderSummaryTable(spreadsheet, "Top 10 Categories ordered by Quantity", columnsStandard, queryResult, topX, currentRowIndex);

            // Service Requests: Category Top 10's by Quantity            
            queryResult = this.FetchSummaryTableData(OrderType.RequestForService, EntityType.CategoryEntity, ColumnNames.TotalQuantity, topX);
            this.RenderSummaryTable(spreadsheet, "Top 10 Categories orderded by Quantity", columnsRequests, queryResult, topX, currentRowIndex);

            currentRowIndex += 13;
            // Standard: Render Product Top 10's by Revenue
            queryResult = this.FetchSummaryTableData(OrderType.Standard, EntityType.ProductEntity, ColumnNames.TotalRevenue, topX);
            this.RenderSummaryTable(spreadsheet, "Top 10 Products orderd by Revenue", columnsStandard, queryResult, topX, currentRowIndex);

            // Service Requests: Render Product Top 10's by Revenue
            queryResult = this.FetchSummaryTableData(OrderType.RequestForService, EntityType.ProductEntity, ColumnNames.TotalRevenue, topX);
            this.RenderSummaryTable(spreadsheet, "Top 10 Products ordered by Revenue", columnsRequests, queryResult, topX, currentRowIndex);

            currentRowIndex += 13;
            // Standard: Render Category Top 10's by Revenue
            queryResult = this.FetchSummaryTableData(OrderType.Standard, EntityType.CategoryEntity, ColumnNames.TotalRevenue, topX);
            this.RenderSummaryTable(spreadsheet, "Top 10 Categories ordered by Revenue", columnsStandard, queryResult, topX, currentRowIndex);

            // Service Requests: Category Top 10's by Quantity            
            queryResult = this.FetchSummaryTableData(OrderType.RequestForService, EntityType.CategoryEntity, ColumnNames.TotalRevenue, topX);
            this.RenderSummaryTable(spreadsheet, "Top 10 Categories ordered by Revenue", columnsRequests, queryResult, topX, currentRowIndex);

            // Fixed amount of Rows: 4 x (Header + Header + topX + spacing)
            rowIndex += 4 * (1 + 1 + topX + 1);
        }

        private DataTable FetchSummaryTableData(OrderType orderType, EntityType entityType, string columnName, int topX)
        {
            DataTable queryResult = new DataTable();
            if (entityType != EntityType.ProductEntity && entityType != EntityType.CategoryEntity)
            {
                throw new NotImplementedException("EntityType: {0} is not implemented".FormatSafe(entityType));
            }

            TypedListDAO dao = new TypedListDAO();            
            if (entityType == EntityType.ProductEntity)
            {
                string query = this.GetTopXProductsQuery(topX, orderType, columnName);
                SqlQueryHelper.ExecuteRetrievalQuery(queryResult, query);                
            }
            else
            {
                string query = this.GetTopXCategoriesQuery(topX, orderType, columnName);
                SqlQueryHelper.ExecuteRetrievalQuery(queryResult, query);             
            }
            
            return queryResult; // DataTableUtil.Sort(queryResult, "{0} {1}".FormatSafe(columnName, "DESC")); - Sorting is now done in the DB
        }

       

        private void RenderSummaryTable(SLDocument spreadsheet, string title, List<string> columns, DataTable queryResult, int rowsToPrint, int currentRowIndex)
        {
            // Header for table
            spreadsheet.MergeWorksheetCells(columns[0] + currentRowIndex, columns[2] + currentRowIndex);
            spreadsheet.SetCellValue(columns[0] + currentRowIndex, title);
            spreadsheet.SetStyle(columns[0] + currentRowIndex, horizontalAlignment: DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);
            currentRowIndex++;

            // Column headings for table
            spreadsheet.SetCellValue(columns[0] + currentRowIndex, "Name");
            spreadsheet.SetCellValue(columns[1] + currentRowIndex, "Quantity");
            spreadsheet.SetCellValue(columns[2] + currentRowIndex, "Revenue");
            spreadsheet.SetStyle(columns[0] + currentRowIndex, columns[2] + currentRowIndex, backgroundColor: SpreadsheetHelper.Heading1);
            spreadsheet.SetStyle(columns[1] + currentRowIndex, columns[2] + currentRowIndex, horizontalAlignment: DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Right);
            currentRowIndex++;

            // Render the rows to be printed
            // We need to print as many rows as we need to (even if we wouldn't have the amount of rows) to render
            // the lines and keep currentRowIndex in sync.
            for (int i = 0; i < rowsToPrint; i++)
            {
                if (i < queryResult.Rows.Count)
                {
                    // Got row
                    DataRow row = queryResult.Rows[i];
                    if (row[ProductFields.Name.Name] == DBNull.Value)
                        spreadsheet.SetCellValue(columns[0] + currentRowIndex, "Deleted");
                    else
                        spreadsheet.SetCellValue(columns[0] + currentRowIndex, (string)row[ProductFields.Name.Name]);
                    spreadsheet.SetCellValue(columns[1] + currentRowIndex, (int)row[ColumnNames.TotalQuantity]);
                    decimal revenue = row[ColumnNames.TotalRevenue] == DBNull.Value ? 0 : (decimal)row[ColumnNames.TotalRevenue];
                    spreadsheet.SetCellValue(columns[2] + currentRowIndex, revenue);
                    spreadsheet.SetStyle(columns[1] + currentRowIndex, formatCode: SpreadsheetHelper.IntegerFormatCode);
                    spreadsheet.SetStyle(columns[2] + currentRowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode);
                }
                else
                {
                    // No row
                }

                // Border bottom
                spreadsheet.SetStyle(columns[0] + currentRowIndex, columns[2] + currentRowIndex, borders: Borders.Bottom, borderColor: SpreadsheetHelper.DividingBorderColor);

                currentRowIndex++;
            }
        }     
                
        #endregion

        #region Queries

        private DynamicQuery RetrieveMenuStatsQuery(bool deletedProducts)
        {
            var queryFactory = new QueryFactory();
            DynamicQuery query = queryFactory.Create();

            if (deletedProducts)
            {
                query.Select(CompanyFields.CompanyId, CompanyFields.Name.As(ColumnNames.CompanyName),
                    OrderitemFields.CategoryName.As(ColumnNames.CategoryName), OrderitemFields.ProductName.As(ColumnNames.ProductName),
                                (OrderitemFields.ProductPriceIn * OrderitemFields.Quantity).Sum().As(ColumnNames.TotalRevenue),
                                OrderitemFields.Quantity.Sum().As(ColumnNames.TotalQuantity))
                    .From(queryFactory.Orderitem
                                .LeftJoin(OrderitemEntity.Relations.OrderEntityUsingOrderId)
                                .LeftJoin(OrderEntity.Relations.CompanyEntityUsingCompanyId)
                                .LeftJoin(OrderEntity.Relations.DeliverypointEntityUsingDeliverypointId))
                    .GroupBy(CompanyFields.CompanyId, CompanyFields.Name, OrderitemFields.CategoryName, OrderitemFields.ProductName)
                    .Where(OrderitemFields.ProductId == DBNull.Value)
                    .OrderBy(OrderitemFields.CategoryName.Ascending(), OrderitemFields.ProductName.Ascending());
            }
            else
            {
                query.Select(CompanyFields.CompanyId, CompanyFields.Name.As(ColumnNames.CompanyName), MenuFields.MenuId.As(ColumnNames.MenuId), MenuFields.Name.As(ColumnNames.MenuName),
                    CategoryFields.CategoryId.As(ColumnNames.CategoryId), ProductFields.ProductId.As(ColumnNames.ProductId),
                                (OrderitemFields.ProductPriceIn * OrderitemFields.Quantity).Sum().As(ColumnNames.TotalRevenue),
                                OrderitemFields.Quantity.Sum().As(ColumnNames.TotalQuantity))
                    .From(queryFactory.Orderitem
                                .LeftJoin(OrderitemEntity.Relations.OrderEntityUsingOrderId)
                                .LeftJoin(OrderEntity.Relations.CompanyEntityUsingCompanyId)
                                .LeftJoin(OrderitemEntity.Relations.ProductEntityUsingProductId)
                                .LeftJoin(OrderitemEntity.Relations.CategoryEntityUsingCategoryId)
                                .LeftJoin(CategoryEntity.Relations.MenuEntityUsingMenuId)
                                .LeftJoin(OrderEntity.Relations.DeliverypointEntityUsingDeliverypointId))
                    .GroupBy(CompanyFields.CompanyId, CompanyFields.Name, MenuFields.MenuId, MenuFields.Name, CategoryFields.CategoryId, ProductFields.ProductId)
                    .OrderBy(CompanyFields.Name.Ascending(), MenuFields.Name.Ascending());
            }

            // Add Where clauses based on filter
            if (filter != null)
            {
                query.Where(OrderFields.CreatedUTC >= this.filter.FromUtc);
                query.Where(OrderFields.CreatedUTC <= this.filter.TillUtc);
                query.Where(OrderFields.CompanyId == this.filter.CompanyId);

                if (this.filter.DeliverypointIds != null && this.filter.DeliverypointIds.Count > 0)
                {
                    query.Where(OrderFields.DeliverypointId == this.filter.DeliverypointIds);
                }

                if (this.filter.DeliverypointgroupIds != null && this.filter.DeliverypointgroupIds.Count > 0)
                {
                    query.Where(DeliverypointFields.DeliverypointgroupId == this.filter.DeliverypointgroupIds);
                }

                if (!this.filter.IncludeFailedOrders)
                {
                    query.Where(OrderFields.ErrorCode == 0);
                }
            }

            return query;
        }

        private string GetMenuStatsQuery()
        {
            string sqlQuery = @"
                    SELECT 
	                    [CompanyId],
	                    [Company Name],
	                    [MenuId],
	                    [Menu Name],
	                    [CategoryId],
	                    [ProductId],					
	                    SUM([Revenue]) as [Revenue],
	                    SUM([Quantity]) as [Quantity]
                    FROM
	                    (		
		                    SELECT 
			                    [Company].[CompanyId] as [CompanyId],
			                    [Company].[Name] as [Company Name],
			                    [Menu].[MenuId] as [MenuId],
			                    [Menu].[Name] as [Menu Name],
			                    [Category].[CategoryId] as [CategoryId],
			                    [Product].[ProductId] as [ProductId],									
			                    [Orderitem].Quantity as [Quantity],
			                    (
				                    [Orderitem].[Quantity] * 
				                    (
					                    [Orderitem].[ProductPriceIn] + 
						                    ISNULL((
							                    SELECT 
								                    SUM([OrderitemAlterationitem].[AlterationoptionPriceIn]) AS [AlterationoptionPriceIn] 
							                    FROM 
								                    [OrderitemAlterationitem]  
							                    WHERE 
								                    ( 
									                    ( 
										                    [OrderitemAlterationitem].[OrderitemId] = [Orderitem].[OrderitemId]
									                    )
								                    )
							                    ),0)
						                    )
				                    ) 
			                    AS [Revenue] 
		                    FROM 
			                    [Orderitem] 
		                    LEFT JOIN [Order] on [Order].OrderId = [Orderitem].[OrderId]		
		                    LEFT JOIN [Product] on [Product].[ProductId] = [Orderitem].ProductId
		                    LEFT JOIN [Category] on [Category].[CategoryId] = [Orderitem].CategoryId
		                    LEFT JOIN [Menu] on [Menu].[MenuId] = [Category].[MenuId]		
		                    LEFT JOIN [Company] on [Company].[CompanyId] = [Order].[CompanyId]		
                            LEFT JOIN [Deliverypoint] on [Order].DeliverypointId = [Deliverypoint].[DeliverypointId]
		                    WHERE                                 
			                    [[WHERE]]
	                    ) as oiQuery
                    GROUP BY
	                    [CompanyId], [Company Name], [MenuId], [Menu Name], [CategoryId], [ProductId]
                    ORDER BY [Menu Name] DESC
                    ";

            List<string> whereClauses = SqlQueryHelper.GetDefaultWhereClause(this.filter);
            sqlQuery = sqlQuery.Replace("[[WHERE]]", string.Join(" AND ", whereClauses));

            return sqlQuery;
        }

        private string GetMenuStatsQueryDeletedProducts()
        {
            string sqlQuery = @"
                    SELECT 
	                    [CompanyId],
	                    [Company Name],
	                    [Category Name],
	                    [Product Name],					
	                    SUM([Revenue]) as [Revenue],
	                    SUM([Quantity]) as [Quantity]
                    FROM
	                    (		
		                    SELECT 
			                    [Orderitem].[CategoryName] as [Category Name],
			                    [Orderitem].[ProductName] as [Product Name],								
			                    [Company].[CompanyId] as [CompanyId],
			                    [Company].[Name] as [Company Name],
                                [Orderitem].Quantity as [Quantity],
			                    (
				                    [Orderitem].[Quantity] * 
				                    (
					                    [Orderitem].[ProductPriceIn] + 
					                    ISNULL((
						                    SELECT 
							                    SUM([OrderitemAlterationitem].[AlterationoptionPriceIn]) AS [AlterationoptionPriceIn] 
						                    FROM 
							                    [OrderitemAlterationitem]  
						                    WHERE 
							                    ( 
								                    ( 
									                    [OrderitemAlterationitem].[OrderitemId] = [Orderitem].[OrderitemId]
								                    )
							                    )
					                    ),0)
				                    ) 
			                    ) AS [Revenue] 
		                    FROM 
			                    [Orderitem] 
		                    LEFT JOIN [Order] on [Order].OrderId = [Orderitem].[OrderId]
		                    LEFT JOIN [Deliverypoint] on [Order].DeliverypointId = [Deliverypoint].[DeliverypointId] 
		                    LEFT JOIN [Company] on [Company].[CompanyId] = [Order].[CompanyId]		
		                    WHERE 
                                [Orderitem].[ProductId] IS NULL AND
			                    [[WHERE]]
	                    ) as oiQuery
                    GROUP BY
	                    [CompanyId], [Company Name], [Category Name], [Product Name]
                    ORDER BY 
	                    [Category Name], [Product Name]
                    ";

            List<string> whereClauses = SqlQueryHelper.GetDefaultWhereClause(this.filter);
            sqlQuery = sqlQuery.Replace("[[WHERE]]", string.Join(" AND ", whereClauses));

            return sqlQuery;
        }

        private string GetTopXProductsQuery(int maxResults, OrderType orderType, string orderByColumnName)
        {
            string sqlQuery = @"
                    SELECT 
	                    TOP([[TOP]]) 	
	                    [Name],	
	                    [ProductId],					
	                    SUM([Quantity]) as [Quantity],								
	                    SUM([Revenue]) as [Revenue]
                    FROM
	                    (
		                    SELECT 
			                    [Product].[Name] as [Name],	
			                    [Product].[ProductId] as [ProductId],															
			                    [Orderitem].Quantity as [Quantity],
			                    (
				                    [Orderitem].[Quantity] * 
				                    (
					                    [Orderitem].[ProductPriceIn] + 
					                        ISNULL((
						                        SELECT 
							                        SUM([OrderitemAlterationitem].[AlterationoptionPriceIn]) AS [AlterationoptionPriceIn] 
						                        FROM 
							                        [OrderitemAlterationitem]  
						                        WHERE 
							                        ( 
								                        ( 
									                        [OrderitemAlterationitem].[OrderitemId] = [Orderitem].[OrderitemId]
								                        )
							                        )
					                        ),0)
				                    )
			                    ) 
			                    AS [Revenue] 
		                    FROM 
			                    [Orderitem] 
		                    LEFT JOIN [Order] on [Order].OrderId = [Orderitem].[OrderId]		
		                    LEFT JOIN [Product] on [Product].[ProductId] = [Orderitem].ProductId
		                    LEFT JOIN [Deliverypoint] on [Order].DeliverypointId = [Deliverypoint].[DeliverypointId]
		                    LEFT JOIN [Deliverypointgroup] on [Deliverypoint].[DeliverypointgroupId] = [Deliverypointgroup].[DeliverypointgroupId]
		                    WHERE 
                                [[WHERE]]
	                    ) as oiQuery
                    GROUP BY 
	                    [Name],	
	                    [ProductId]
                    ORDER BY 
	                    [[[ORDERBY]]] DESC";

            List<string> whereClauses = SqlQueryHelper.GetDefaultWhereClause(this.filter);

            // Order Type
            whereClauses.Add("[Order].[Type] = {0}".FormatSafe((int)orderType));           

            sqlQuery = sqlQuery.Replace("[[WHERE]]", string.Join(" AND ", whereClauses));
            sqlQuery = sqlQuery.Replace("[[ORDERBY]]", orderByColumnName);
            sqlQuery = sqlQuery.Replace("[[TOP]]", maxResults.ToString());

            return sqlQuery;
        }

        private string GetTopXCategoriesQuery(int maxResults, OrderType orderType, string orderByColumnName)
        {
            string sqlQuery = @"
                    SELECT 
	                    TOP([[TOP]]) 	
	                    [Name],	
	                    [CategoryId],					
	                    SUM([Quantity]) as [Quantity],								
	                    SUM([Revenue]) as [Revenue]
                    FROM
	                    (
		                    SELECT 
			                    [Category].[Name] as [Name],	
			                    [Category].[CategoryId] as [CategoryId],															
			                    [Orderitem].Quantity as [Quantity],
			                    (
				                    [Orderitem].[Quantity] * 
				                    (
					                    [Orderitem].[ProductPriceIn] + 
					                        ISNULL((
						                        SELECT 
							                        SUM([OrderitemAlterationitem].[AlterationoptionPriceIn]) AS [AlterationoptionPriceIn] 
						                        FROM 
							                        [OrderitemAlterationitem]  
						                        WHERE 
							                        ( 
								                        ( 
									                        [OrderitemAlterationitem].[OrderitemId] = [Orderitem].[OrderitemId]
								                        )
							                        )
					                        ),0)
				                    )
			                    ) 
			                    AS [Revenue] 
		                    FROM 
			                    [Orderitem] 
		                    LEFT JOIN [Order] on [Order].OrderId = [Orderitem].[OrderId]		
		                    LEFT JOIN [Category] on [Category].[CategoryId] = [Orderitem].CategoryId
		                    LEFT JOIN [Deliverypoint] on [Order].DeliverypointId = [Deliverypoint].[DeliverypointId]
		                    LEFT JOIN [Deliverypointgroup] on [Deliverypoint].[DeliverypointgroupId] = [Deliverypointgroup].[DeliverypointgroupId]
		                    WHERE 
                                [[WHERE]]
	                    ) as oiQuery
                    GROUP BY 
	                    [Name],	
	                    [CategoryId]
                    ORDER BY 
	                    [[[ORDERBY]]] DESC";

            List<string> whereClauses = SqlQueryHelper.GetDefaultWhereClause(this.filter);

            // Order Type
            whereClauses.Add("[Order].[Type] = {0}".FormatSafe((int)orderType));

            sqlQuery = sqlQuery.Replace("[[WHERE]]", string.Join(" AND ", whereClauses));
            sqlQuery = sqlQuery.Replace("[[ORDERBY]]", orderByColumnName);
            sqlQuery = sqlQuery.Replace("[[TOP]]", maxResults.ToString());

            return sqlQuery;
        }


        #region QuerySpec approach

        private DynamicQuery RetrieveProductTopXStats(OrderType orderType, string orderBy, int maxResults)
        {
            var queryFactory = new QueryFactory();
            DynamicQuery query = queryFactory.Create();

            query.Select(CompanyFields.CompanyId, CompanyFields.Name.As("Company Name"), ProductFields.ProductId, ProductFields.Name,
                            Functions.CountRow().As("Orderitem Count"),
                            (OrderitemFields.ProductPriceIn * OrderitemFields.Quantity).Sum().As(ColumnNames.TotalRevenue),
                            OrderitemFields.Quantity.Sum().As(ColumnNames.TotalQuantity))
                .From(queryFactory.Orderitem
                            .LeftJoin(OrderitemEntity.Relations.OrderEntityUsingOrderId)
                            .LeftJoin(OrderEntity.Relations.CompanyEntityUsingCompanyId)
                            .LeftJoin(OrderitemEntity.Relations.ProductEntityUsingProductId)
                            .LeftJoin(OrderEntity.Relations.DeliverypointEntityUsingDeliverypointId))
                .Where(OrderFields.Type == (int)orderType)
                .GroupBy(CompanyFields.CompanyId, CompanyFields.Name, ProductFields.ProductId, ProductFields.Name)
                .Limit(maxResults);

            if (orderBy == ColumnNames.TotalQuantity)
                query.OrderBy(OrderitemFields.Quantity.Sum().Descending());
            else if (orderBy == ColumnNames.TotalRevenue)
                query.OrderBy((OrderitemFields.ProductPriceIn * OrderitemFields.Quantity).Sum().Descending());
            else
                throw new NotImplementedException("No sorting implemented for: " + orderBy);

            // Posted thread to get sorting from the DB: http://www.llblgen.com/tinyforum/Messages.aspx?ThreadID=22726
            //.OrderBy(ColumnNames.TotalRevenue.Ascending());

            if (this.filter != null)
            {
                query.Where(OrderFields.CreatedUTC >= this.filter.FromUtc);
                query.Where(OrderFields.CreatedUTC <= this.filter.TillUtc);
                query.Where(OrderFields.CompanyId == this.filter.CompanyId);

                if (this.filter.DeliverypointIds != null && this.filter.DeliverypointIds.Count > 0)
                    query.Where(OrderFields.DeliverypointId == this.filter.DeliverypointIds);

                if (this.filter.DeliverypointgroupIds != null && this.filter.DeliverypointgroupIds.Count > 0)
                    query.Where(DeliverypointFields.DeliverypointgroupId == this.filter.DeliverypointgroupIds);

                if (!this.filter.IncludeFailedOrders)
                    query.Where(OrderFields.ErrorCode == 0);
            }

            return query;
        }

        private DynamicQuery RetrieveCategoryTopXStats(OrderType orderType, string orderBy, int maxResults)
        {
            var queryFactory = new QueryFactory();
            DynamicQuery query = queryFactory.Create();

            query.Select(CompanyFields.CompanyId, CompanyFields.Name.As("Company Name"), CategoryFields.Name, CategoryFields.CategoryId,
                            Functions.CountRow().As("Orderitem Count"),

                            (OrderitemFields.ProductPriceIn * OrderitemFields.Quantity).Sum().As("Revenue"),
                            OrderitemFields.Quantity.Sum().As("Quantity"))
                .From(queryFactory.Orderitem
                            .LeftJoin(OrderitemEntity.Relations.OrderEntityUsingOrderId)
                            .LeftJoin(OrderEntity.Relations.CompanyEntityUsingCompanyId)
                            .LeftJoin(OrderitemEntity.Relations.CategoryEntityUsingCategoryId)
                            .LeftJoin(OrderEntity.Relations.DeliverypointEntityUsingDeliverypointId))
                .Where(OrderFields.Type == (int)orderType)
                .GroupBy(CompanyFields.CompanyId, CompanyFields.Name, CategoryFields.CategoryId, CategoryFields.Name)
                .Limit(maxResults);

            if (orderBy == ColumnNames.TotalQuantity)
                query.OrderBy(OrderitemFields.Quantity.Sum().Descending());
            else if (orderBy == ColumnNames.TotalRevenue)
                query.OrderBy((OrderitemFields.ProductPriceIn * OrderitemFields.Quantity).Sum().Descending());
            else
                throw new NotImplementedException("No sorting implemented for: " + orderBy);
            // Posted thread to get sorting from the DB: http://www.llblgen.com/tinyforum/Messages.aspx?ThreadID=22726
            // .OrderBy(ColumnNames.TotalRevenue.Ascending());                

            if (this.filter != null)
            {
                query.Where(OrderFields.CreatedUTC >= this.filter.FromUtc);
                query.Where(OrderFields.CreatedUTC <= this.filter.TillUtc);
                query.Where(OrderFields.CompanyId == this.filter.CompanyId);

                if (this.filter.DeliverypointIds != null && this.filter.DeliverypointIds.Count > 0)
                    query.Where(OrderFields.DeliverypointId == this.filter.DeliverypointIds);

                if (this.filter.DeliverypointgroupIds != null && this.filter.DeliverypointgroupIds.Count > 0)
                    query.Where(DeliverypointFields.DeliverypointgroupId == this.filter.DeliverypointgroupIds);

                if (!this.filter.IncludeFailedOrders)
                    query.Where(OrderFields.ErrorCode == 0);
            }

            return query;
        }

        #endregion

        #endregion

        #region Column Name definitions

        internal class ColumnNames
        {
            internal const string MenuId = "MenuId";
            internal const string MenuName = "Menu Name";
            internal const string CompanyId = "CompanyId";
            internal const string CompanyName = "Company Name";
            internal const string ParentCategoryId = "Parent CategoryId";
            internal const string CategoryId = "CategoryId";
            internal const string ProductId = "ProductId";
            internal const string CategoryName = "Category Name";
            internal const string ProductName = "Product Name";
            internal const string TotalQuantity = "Quantity";
            internal const string TotalRevenue = "Revenue";
        }

        #endregion

        #region Typed Data Row class

        public class CategoryProductStatsRow : DataRow
        {
            // GK All totals have been defined as DataTable columns instead of properties that just return 
            // the sum of Standard+RequestForService to keep it a simple data table structure to be used everywhere.
            internal CategoryProductStatsRow(DataRowBuilder builder)
                : base(builder)
            {
            }

            public int MenuId
            {
                get { return (int)base[ColumnNames.MenuId]; }
                set { base[ColumnNames.MenuId] = value; }
            }

            public string MenuName
            {
                get { return (string)base[ColumnNames.MenuName]; }
                set { base[ColumnNames.MenuName] = value; }
            }

            public int CompanyId
            {
                get { return (int)base[ColumnNames.CompanyId]; }
                set { base[ColumnNames.CompanyId] = value; }
            }

            public string CompanyName
            {
                get { return (string)base[ColumnNames.CompanyName]; }
                set { base[ColumnNames.CompanyName] = value; }
            }

            public int ParentCategoryId
            {
                get { return (int)base[ColumnNames.CategoryId]; }
                set { base[ColumnNames.CategoryId] = value; }
            }

            public int CategoryId
            {
                get { return (int)base[ColumnNames.CategoryId]; }
                set { base[ColumnNames.CategoryId] = value; }
            }


            public int ProductId
            {
                get { return (int)base[ColumnNames.ProductId]; }
                set { base[ColumnNames.ProductId] = value; }
            }

            /// <summary>
            /// The CategoryName is only used for Deleted Product Rows
            /// </summary>
            public string CategoryName
            {
                get { return (string)base[ColumnNames.CategoryName]; }
                set { base[ColumnNames.CategoryName] = value; }
            }

            /// <summary>
            /// The ProductName is only used for Deleted Product Rows
            /// </summary>
            public string ProductName
            {
                get { return (string)base[ColumnNames.ProductName]; }
                set { base[ColumnNames.ProductName] = value; }
            }

            public int TotalQuantity
            {
                get { return (int)base[ColumnNames.TotalQuantity]; }
                set { base[ColumnNames.TotalQuantity] = value; }
            }

            public decimal TotalRevenue
            {
                get { return (decimal)base[ColumnNames.TotalRevenue]; }
                set { base[ColumnNames.TotalRevenue] = value; }
            }

            public bool TryGetCategoryIdProductIdString(out string key)
            {
                bool toReturn = (this.CategoryId > 0 && this.ProductId > 0);

                if (toReturn) key = this.CategoryId + "." + this.ProductId;
                else key = string.Empty;

                return toReturn;
            }
        }

        #endregion
    }
}
