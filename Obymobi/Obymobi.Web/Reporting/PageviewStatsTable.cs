﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Spreadsheet;
using Google.Apis.Manual.Analytics.v3.Data;
using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Web.Google;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SpreadsheetLight;

namespace Obymobi.Logic.Analytics
{
    public class PageviewStatsTable : TypedStatsTable<PageviewStatsTable.PageviewStatsRow2>
    {
        private const string ANALYTICS_DIMENSION = "ga:eventCategory,ga:eventAction,ga:eventLabel,ga:dimension11,ga:dimension14";
        private const string ANALYTICS_METRIC = "ga:totalEvents,ga:eventValue,ga:avgEventValue,ga:users";
        private const string ANALYTICS_FILTER = "ga:dimension14=@[C-{0}];ga:eventAction=={1}";

        private Filter reportFilter;

        private readonly List<string> eventActionFilter;
        private readonly List<string> rootCategories;

        private MenuCollection menuCollection;
        private UIModeCollection uiModeCollection;
        private DeliverypointgroupCollection deliverypointgroupCollection;
        private EntertainmentCollection entertainmentCollection;
        private DataTable menuPageviews;

        // 29-09-14 - Temp Solution
        private CategoryCollection categoryCollection;
        private Dictionary<int, CategoryEntity> categoryMap;

        private CompanyEntity companyEntity;

        #region DataTable Setup

        public PageviewStatsTable()
        {
            eventActionFilter = new List<string>(10);
            eventActionFilter.AddRange(EventTypes2.ApplicationLostFocus.NameAndSynonyms);
            eventActionFilter.AddRange(EventTypes2.CompanyScreenView.NameAndSynonyms);
            eventActionFilter.AddRange(EventTypes2.CompanyScreenLeft.NameAndSynonyms);
            eventActionFilter.AddRange(EventTypes2.ProductViewed.NameAndSynonyms);
            eventActionFilter.AddRange(EventTypes2.CategoryViewed.NameAndSynonyms);
            eventActionFilter.AddRange(EventTypes2.EntertainmentView.NameAndSynonyms);
            eventActionFilter.AddRange(EventTypes2.BrowserScreenView.NameAndSynonyms);
            eventActionFilter.AddRange(EventTypes2.NativePageView.NameAndSynonyms);

            this.Columns.Add(new DataColumn(ColumnNames.EventCategory, typeof(string)));
            this.Columns.Add(new DataColumn(ColumnNames.EventAction, typeof(string)));
            this.Columns.Add(new DataColumn(ColumnNames.EventLabel, typeof(string)));
            this.Columns.Add(new DataColumn(ColumnNames.AppType, typeof(ApplicationType)));
            this.Columns.Add(new DataColumn(ColumnNames.PrimaryKeys, typeof(string)));
            this.Columns.Add(new DataColumn(ColumnNames.DeliverypointgroupId, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.TotalEvents, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.EventValue, typeof(long)));
            this.Columns.Add(new DataColumn(ColumnNames.AverageEventValue, typeof(double)));
            this.Columns.Add(new DataColumn(ColumnNames.Users, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.EventsPerUser, typeof(double)));
            this.Columns.Add(new DataColumn(ColumnNames.CategoryId, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.ProductId, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.TabId, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.EntertainmentId, typeof(int)));

            rootCategories = new List<string>(10);
            menuPageviews = new DataTable();
        }

        protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
        {
            return new PageviewStatsRow2(builder);
        }

        #endregion

        #region Data Fetching

        public void FetchData(GoogleAPI api, Filter filter)
        {
            this.reportFilter = filter;
            this.companyEntity = new CompanyEntity(filter.CompanyId);

            LoadMenus(filter);
            LoadUIModes(filter);
            LoadDeliverypointgroups(filter);
            LoadEntertainments(filter);

            // Temp
            LoadCategories(filter);

            FetchPageviewFromGoogleAnalytics(api, filter);
        }

        private void LoadCategories(Filter mainFilter)
        {
            var filter = new PredicateExpression();
            filter.Add(MenuFields.CompanyId == mainFilter.CompanyId);

            var relation = new RelationCollection();
            relation.Add(CategoryEntityBase.Relations.MenuEntityUsingMenuId);

            categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter, relation);

            categoryMap = new Dictionary<int, CategoryEntity>(categoryCollection.Count);
            foreach (var categoryEntity in categoryCollection)
            {
                categoryMap.Add(categoryEntity.CategoryId, categoryEntity);
            }
        }

        private void LoadMenus(Filter mainFilter)
        {
            menuCollection = new MenuCollection();
            var filter = new PredicateExpression(MenuFields.CompanyId == mainFilter.CompanyId);
            var sort = new SortExpression(MenuFields.Name | SortOperator.Ascending);

            var categorySort = new SortExpression(CategoryFields.SortOrder | SortOperator.Ascending);
            var pathChildCategoryCollection1 = CategoryEntityBase.PrefetchPathChildCategoryCollection;
            pathChildCategoryCollection1.Sorter = categorySort;
            pathChildCategoryCollection1.SubPath.Add(CategoryEntityBase.PrefetchPathProductEntity);
            var pathChildCategoryCollection2 = CategoryEntityBase.PrefetchPathChildCategoryCollection;
            pathChildCategoryCollection2.Sorter = categorySort;
            pathChildCategoryCollection2.SubPath.Add(CategoryEntityBase.PrefetchPathProductEntity);
            var pathChildCategoryCollection3 = CategoryEntityBase.PrefetchPathChildCategoryCollection;
            pathChildCategoryCollection3.Sorter = categorySort;
            pathChildCategoryCollection3.SubPath.Add(CategoryEntityBase.PrefetchPathProductEntity);
            var pathChildCategoryCollection4 = CategoryEntityBase.PrefetchPathChildCategoryCollection;
            pathChildCategoryCollection4.Sorter = categorySort;
            pathChildCategoryCollection4.SubPath.Add(CategoryEntityBase.PrefetchPathProductEntity);
            var pathChildCategoryCollection5 = CategoryEntityBase.PrefetchPathChildCategoryCollection;
            pathChildCategoryCollection5.Sorter = categorySort;
            pathChildCategoryCollection5.SubPath.Add(CategoryEntityBase.PrefetchPathProductEntity);

            // Prefetch categories for up to 5 levels
            var path = new PrefetchPath(EntityType.MenuEntity);
            path.Add(MenuEntityBase.PrefetchPathCategoryCollection, 0, null, null, categorySort)
                .SubPath.Add(pathChildCategoryCollection1)
                .SubPath.Add(pathChildCategoryCollection2)
                .SubPath.Add(pathChildCategoryCollection3)
                .SubPath.Add(pathChildCategoryCollection4);

            menuCollection.GetMulti(filter, 0, sort, null, path);
        }

        private void LoadUIModes(Filter mainFilter)
        {
            uiModeCollection = new UIModeCollection();

            var filter = new PredicateExpression(UIModeFields.CompanyId == mainFilter.CompanyId);
            var typeFilter = new PredicateExpression(UIModeFields.Type == UIModeType.VenueOwnedUserDevices);
            typeFilter.AddWithOr(UIModeFields.Type == UIModeType.GuestOwnedTabletDevices);
            typeFilter.AddWithOr(UIModeFields.Type == UIModeType.GuestOwnedMobileDevices);
            filter.Add(typeFilter);

            var uitModeSort = new SortExpression(UIModeFields.Name | SortOperator.Ascending);
            var uiTabSort = new SortExpression(UITabFields.Caption | SortOperator.Ascending);

            var path = new PrefetchPath(EntityType.UIModeEntity);
            path.Add(UIModeEntityBase.PrefetchPathUITabCollection, 0, null, null, uiTabSort);

            uiModeCollection.GetMulti(filter, 0, uitModeSort, null, path);
        }

        private void LoadDeliverypointgroups(Filter mainFilter)
        {
            deliverypointgroupCollection = new DeliverypointgroupCollection();

            var filter = new PredicateExpression(DeliverypointgroupFields.CompanyId == mainFilter.CompanyId);
            if (mainFilter.DeliverypointgroupIds != null && mainFilter.DeliverypointgroupIds.Count > 0)
            {
                filter.Add(DeliverypointgroupFields.DeliverypointgroupId == mainFilter.DeliverypointgroupIds);
            }

            var sort = new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending);
            var entertainmentSort = new SortExpression(EntertainmentFields.Name | SortOperator.Ascending);

            var occupancyFilter = new PredicateExpression(DeliverypointgroupOccupancyFields.Date >= mainFilter.FromUtc.UtcToLocalTime(this.companyEntity.TimeZoneInfo));
            occupancyFilter.Add(DeliverypointgroupOccupancyFields.Date <= mainFilter.TillUtc.UtcToLocalTime(this.companyEntity.TimeZoneInfo));

            var path = new PrefetchPath(EntityType.DeliverypointgroupEntity);
            path.Add(DeliverypointgroupEntityBase.PrefetchPathEntertainmentCollectionViaDeliverypointgroupEntertainment, 0, null, null, entertainmentSort);
            path.Add(DeliverypointgroupEntityBase.PrefetchPathDeliverypointgroupOccupancyCollection, 0, occupancyFilter);

            var relation = new RelationCollection(DeliverypointgroupEntityBase.Relations.DeliverypointEntityUsingDeliverypointgroupId);

            deliverypointgroupCollection.GetMulti(filter, 0, sort, relation, path);
        }

        private void LoadEntertainments(Filter mainFilter)
        {
            entertainmentCollection = new EntertainmentCollection();

            var filter = new PredicateExpression(DeliverypointgroupFields.CompanyId == mainFilter.CompanyId);
            var deliverypointgroupSort = new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending);
            var sort = new SortExpression(EntertainmentFields.Name | SortOperator.Ascending);

            var relation = new RelationCollection(EntertainmentEntityBase.Relations.DeliverypointgroupEntertainmentEntityUsingEntertainmentId);
            relation.Add(DeliverypointgroupEntertainmentEntityBase.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

            var path = new PrefetchPath(EntityType.EntertainmentEntity);
            path.Add(EntertainmentEntityBase.PrefetchPathDeliverypointgroupCollectionViaDeliverypointgroupEntertainment, 0, null, null, deliverypointgroupSort);

            entertainmentCollection.GetMulti(filter, 0, sort, relation, path);
        }

        private void FetchPageviewFromGoogleAnalytics(GoogleAPI api, Filter filter)
        {
            // Clone this datatable structure to menu pageviews DT
            menuPageviews = this.Clone();

            foreach (var eventAction in eventActionFilter)
            {
                var gaFilter = string.Format(ANALYTICS_FILTER, filter.CompanyId, eventAction);
                GaData gaData = api.Get(ANALYTICS_METRIC, ANALYTICS_DIMENSION, gaFilter, "", 10000, filter.FromUtc, filter.TillUtc);

                if (gaData != null && gaData.Rows != null)
                {
                    foreach (var row in gaData.Rows)
                    {
                        ParseGaDataRow(row);
                    }
                }
            }
        }

        private void ParseGaDataRow(IList<string> row)
        {
            PageviewStatsRow2 pageviewRow = this.NewRow();
            pageviewRow.EventCategory = row[(int)AnalyticsHeaders.EventCategory];
            pageviewRow.EventAction = row[(int)AnalyticsHeaders.EventAction];
            pageviewRow.EventLabel = row[(int)AnalyticsHeaders.EventLabel];
            pageviewRow.AppType = ApplicationTypeExt.FromString(row[(int)AnalyticsHeaders.AppType]);
            pageviewRow.PrimaryKeys = row[(int)AnalyticsHeaders.PrimaryKeys];
            pageviewRow.TotalEvents = int.Parse(row[(int)AnalyticsHeaders.TotalEvents]);
            pageviewRow.EventValue = long.Parse(row[(int)AnalyticsHeaders.EventValue]);
            pageviewRow.AverageEventValue = double.Parse(row[(int)AnalyticsHeaders.AvgEventValue]);
            pageviewRow.Users = int.Parse(row[(int)AnalyticsHeaders.Users]);
            pageviewRow.EventsPerUser = pageviewRow.TotalEvents / (double)pageviewRow.Users;

            this.Add(pageviewRow);

            if (EventTypes2.ProductViewed.NameAndSynonymsContains(pageviewRow.EventAction) ||
                EventTypes2.CategoryViewed.NameAndSynonymsContains(pageviewRow.EventAction))
            {
                if (pageviewRow.AppType == ApplicationType.IrtApp)
                {
                    // Temp solution - Create missing parent category path for IRT
                    CategoryEntity categoryEntity;
                    categoryMap.TryGetValue(pageviewRow.CategoryId, out categoryEntity);
                    while (categoryEntity != null)
                    {
                        // Prepend? parent category name
                        pageviewRow.EventLabel = categoryEntity.Name + "/" + pageviewRow.EventLabel;

                        if (!categoryEntity.ParentCategoryId.HasValue || !categoryMap.TryGetValue(categoryEntity.ParentCategoryId.Value, out categoryEntity))
                            categoryEntity = null;
                    }
                }

                if (pageviewRow.EventLabel.IndexOf('/') != 0)
                {
                    pageviewRow.EventLabel = "/" + pageviewRow.EventLabel;
                }

                if (pageviewRow.EventLabel.LastIndexOf('/') == 0)
                {
                    rootCategories.Add(pageviewRow.EventLabel.Substring(1));
                    pageviewRow.EventLabel += "/";
                }

                var rows = (PageviewStatsRow2[])menuPageviews.Select(string.Format("EventAction='{0}' AND PrimaryKeys='{1}' AND AppType={2}",
                                                                                  pageviewRow.EventAction, pageviewRow.PrimaryKeys, (int)pageviewRow.AppType));

                if (rows.Length == 1)
                {
                    rows[0].TotalEvents += pageviewRow.TotalEvents;
                    rows[0].EventValue += pageviewRow.EventValue;
                    rows[0].Users += pageviewRow.Users;
                    rows[0].EventsPerUser += pageviewRow.EventsPerUser;
                }
                else
                {
                    menuPageviews.ImportRow(pageviewRow);
                }
            }
        }

        #endregion

        #region Spreadsheet

        #region Totals

        public class PageviewIndexes
        {
            public const int COMPANY_VIEW = 20;
            public const int MENU_VIEW = 30;
            public const int NATIVESITE_VIEW = 40;
            public const int BROWSER_VIEW = 50;
            public const int ENTERTAINMENT_VIEW = 60;
        }

        public void AppendTotalPageviews(SLDocument spreadsheet, string title)
        {
            spreadsheet.AddWorksheet(title);
            spreadsheet.SelectWorksheet(title);

            var totalViews = new int[5];
            var companyViews = new int[5];
            var menuViews = new int[5];
            var browserViews = new int[5];
            var micrositeViews = new int[5];
            var entertainmentViews = new int[5];
            var totalUsers = new int[100];

            var viewsDeliverypointgroup = new Dictionary<int, int>();

            foreach (PageviewStatsRow2 statRow in this.Rows)
            {
                if ((statRow.AppType == ApplicationType.IrtApp && !this.reportFilter.IncludeInRoomTablets) ||
                    (statRow.AppType == ApplicationType.ByodApp && !this.reportFilter.IncludeByodApps))
                {
                    continue;
                }

                bool foundDpg = false;
                foreach (var dpg in deliverypointgroupCollection)
                {
                    if (dpg.DeliverypointgroupId == statRow.DeliverypointgroupId)
                    {
                        foundDpg = true;
                        break;
                    }
                }

                // DK - 15-12-2014: Temp hack due to mobile bugsie. When opening company and quickly switching to company page
                // the mobile hasn't finished loading yet - on the background - so it's unable to fetch company data
                // this will make it send DPG--1 for analytics events.
                if (statRow.DeliverypointgroupId == -1)
                    foundDpg = true;

                if (!foundDpg && statRow.AppType == ApplicationType.IrtApp)
                    continue;

                bool countEvent = true;
                string eventAction = statRow.EventAction;
                if (EventTypes2.CompanyScreenView.NameAndSynonymsContains(eventAction))
                {
                    companyViews[0] += statRow.TotalEvents;
                    companyViews[(int)statRow.AppType] += statRow.TotalEvents;

                    totalUsers[(int)statRow.AppType] += statRow.Users;
                    totalUsers[PageviewIndexes.COMPANY_VIEW] += statRow.Users;
                    totalUsers[PageviewIndexes.COMPANY_VIEW + (int)statRow.AppType] += statRow.Users;
                }
                else if (EventTypes2.ProductViewed.NameAndSynonymsContains(eventAction) || EventTypes2.CategoryViewed.NameAndSynonymsContains(eventAction))
                {
                    menuViews[0] += statRow.TotalEvents;
                    menuViews[(int)statRow.AppType] += statRow.TotalEvents;

                    totalUsers[(int)statRow.AppType] += statRow.Users;
                    totalUsers[PageviewIndexes.MENU_VIEW] += statRow.Users;
                    totalUsers[PageviewIndexes.MENU_VIEW + (int)statRow.AppType] += statRow.Users;
                }
                else if (EventTypes2.NativePageView.NameAndSynonymsContains(eventAction))
                {
                    micrositeViews[0] += statRow.TotalEvents;
                    micrositeViews[(int)statRow.AppType] += statRow.TotalEvents;

                    totalUsers[(int)statRow.AppType] += statRow.Users;
                    totalUsers[PageviewIndexes.NATIVESITE_VIEW] += statRow.Users;
                    totalUsers[PageviewIndexes.NATIVESITE_VIEW + (int)statRow.AppType] += statRow.Users;
                }
                else if (EventTypes2.EntertainmentView.NameAndSynonymsContains(eventAction))
                {
                    entertainmentViews[0] += statRow.TotalEvents;
                    entertainmentViews[(int)statRow.AppType] += statRow.TotalEvents;
                    totalUsers[(int)statRow.AppType] += statRow.Users;
                    totalUsers[PageviewIndexes.ENTERTAINMENT_VIEW] += statRow.Users;
                    totalUsers[PageviewIndexes.ENTERTAINMENT_VIEW + (int)statRow.AppType] += statRow.Users;
                }
                else if (EventTypes2.BrowserScreenView.NameAndSynonymsContains(eventAction))
                {
                    browserViews[0] += statRow.TotalEvents;
                    browserViews[(int)statRow.AppType] += statRow.TotalEvents;
                    totalUsers[(int)statRow.AppType] += statRow.Users;
                    totalUsers[PageviewIndexes.BROWSER_VIEW] += statRow.Users;
                    totalUsers[PageviewIndexes.BROWSER_VIEW + (int)statRow.AppType] += statRow.Users;
                }
                else
                {
                    countEvent = false;
                }

                if (countEvent)
                {
                    totalViews[(int)statRow.AppType] += statRow.TotalEvents;
                    totalViews[0] += statRow.TotalEvents; // Total
                    totalUsers[0] += statRow.Users;

                    if (!viewsDeliverypointgroup.ContainsKey(statRow.DeliverypointgroupId))
                        viewsDeliverypointgroup.Add(statRow.DeliverypointgroupId, 0);
                    viewsDeliverypointgroup[statRow.DeliverypointgroupId] += statRow.TotalEvents;
                }
            }

            int row = 1;
            spreadsheet.SetCellValue("A" + row, "Events Summary (Raw Analytics Data)");
            row++;

            spreadsheet.SetCellValue("C" + row, "Company");
            spreadsheet.SetCellValue("D" + row, "Menu");
            spreadsheet.SetCellValue("E" + row, "Browser");
            spreadsheet.SetCellValue("F" + row, "Native Site");
            spreadsheet.SetCellValue("G" + row, "Entertainment");
            spreadsheet.SetCellValue("H" + row, "Total");
            row++;

            #region Views

            spreadsheet.MergeWorksheetCells("A" + row, "A" + (row + 2));
            spreadsheet.SetCellValue("A" + row, "Views");
            spreadsheet.SetCellValue("B" + row, "IRT");
            spreadsheet.SetCellValue("C" + row, companyViews[(int)ApplicationType.IrtApp]); // Company
            spreadsheet.SetCellValue("D" + row, menuViews[(int)ApplicationType.IrtApp]); // Menu
            spreadsheet.SetCellValue("E" + row, browserViews[(int)ApplicationType.IrtApp]); // Browser
            spreadsheet.SetCellValue("F" + row, micrositeViews[(int)ApplicationType.IrtApp]); // Native Site
            spreadsheet.SetCellValue("G" + row, entertainmentViews[(int)ApplicationType.IrtApp]); // Entertainment
            spreadsheet.SetCellValue("H" + row, totalViews[(int)ApplicationType.IrtApp]); // Total
            row++;

            spreadsheet.SetCellValue("B" + row, "Byod");
            spreadsheet.SetCellValue("C" + row, companyViews[(int)ApplicationType.ByodApp]); // Company
            spreadsheet.SetCellValue("D" + row, menuViews[(int)ApplicationType.ByodApp]); // Menu
            spreadsheet.SetCellValue("E" + row, browserViews[(int)ApplicationType.ByodApp]); // Browser
            spreadsheet.SetCellValue("F" + row, micrositeViews[(int)ApplicationType.ByodApp]); // Native Site
            spreadsheet.SetCellValue("G" + row, entertainmentViews[(int)ApplicationType.ByodApp]); // Entertainment
            spreadsheet.SetCellValue("H" + row, totalViews[(int)ApplicationType.ByodApp]); // Total
            row++;

            spreadsheet.SetCellValue("B" + row, "Total");
            spreadsheet.SetCellValue("C" + row, companyViews[0]); // Company
            spreadsheet.SetCellValue("D" + row, menuViews[0]); // Menu
            spreadsheet.SetCellValue("E" + row, browserViews[0]); // Browser
            spreadsheet.SetCellValue("F" + row, micrositeViews[0]); // Native Site
            spreadsheet.SetCellValue("G" + row, entertainmentViews[0]); // Entertainment
            spreadsheet.SetCellValue("H" + row, totalViews[0]); // Total
            row++;

            #endregion

            #region Users

            spreadsheet.MergeWorksheetCells("A" + row, "A" + (row + 2));
            spreadsheet.SetCellValue("A" + row, "Users");
            spreadsheet.SetCellValue("B" + row, "IRT");
            spreadsheet.SetCellValue("C" + row, totalUsers[PageviewIndexes.COMPANY_VIEW + (int)ApplicationType.IrtApp]); // Company
            spreadsheet.SetCellValue("D" + row, totalUsers[PageviewIndexes.MENU_VIEW + (int)ApplicationType.IrtApp]); // Menu
            spreadsheet.SetCellValue("E" + row, totalUsers[PageviewIndexes.BROWSER_VIEW + (int)ApplicationType.IrtApp]); // Browser
            spreadsheet.SetCellValue("F" + row, totalUsers[PageviewIndexes.NATIVESITE_VIEW + (int)ApplicationType.IrtApp]); // Native Site
            spreadsheet.SetCellValue("G" + row, totalUsers[PageviewIndexes.ENTERTAINMENT_VIEW + (int)ApplicationType.IrtApp]); // Entertainment
            //spreadsheet.SetCellValue("H" + row, totalUsers[(int)ApplicationType.IrtApp]); // Total
            row++;

            spreadsheet.SetCellValue("B" + row, "Byod");
            spreadsheet.SetCellValue("C" + row, totalUsers[PageviewIndexes.COMPANY_VIEW + (int)ApplicationType.ByodApp]); // Company
            spreadsheet.SetCellValue("D" + row, totalUsers[PageviewIndexes.MENU_VIEW + (int)ApplicationType.ByodApp]); // Menu
            spreadsheet.SetCellValue("E" + row, totalUsers[PageviewIndexes.BROWSER_VIEW + (int)ApplicationType.ByodApp]); // Browser
            spreadsheet.SetCellValue("F" + row, totalUsers[PageviewIndexes.NATIVESITE_VIEW + (int)ApplicationType.ByodApp]); // Native Site
            spreadsheet.SetCellValue("G" + row, totalUsers[PageviewIndexes.ENTERTAINMENT_VIEW + (int)ApplicationType.ByodApp]); // Entertainment
            //spreadsheet.SetCellValue("H" + row, totalUsers[(int)ApplicationType.ByodApp]); // Total
            row++;

            spreadsheet.SetCellValue("B" + row, "Total");
            spreadsheet.SetCellValue("C" + row, totalUsers[PageviewIndexes.COMPANY_VIEW]); // Company
            spreadsheet.SetCellValue("D" + row, totalUsers[PageviewIndexes.MENU_VIEW]); // Menu
            spreadsheet.SetCellValue("E" + row, totalUsers[PageviewIndexes.BROWSER_VIEW]); // Browser
            spreadsheet.SetCellValue("F" + row, totalUsers[PageviewIndexes.NATIVESITE_VIEW]); // Native Site
            spreadsheet.SetCellValue("G" + row, totalUsers[PageviewIndexes.ENTERTAINMENT_VIEW]); // Entertainment
            //spreadsheet.SetCellValue("H" + row, totalUsers[0]); // Total
            row++;

            #endregion

            #region Page's per user

            spreadsheet.MergeWorksheetCells("A" + row, "A" + (row + 2));
            spreadsheet.SetCellValue("A" + row, "Page's per user");
            spreadsheet.SetCellValue("B" + row, "IRT");
            spreadsheet.SetCellValue("C" + row, "=IF(C6=0,C3,C3/C6)"); // Company
            spreadsheet.SetCellValue("D" + row, "=IF(D6=0,D3,D3/D6)"); // Menu
            spreadsheet.SetCellValue("E" + row, "=IF(E6=0,E3,E3/E6)"); // Browser
            spreadsheet.SetCellValue("F" + row, "=IF(F6=0,F3,F3/F6)"); // Native Site
            spreadsheet.SetCellValue("G" + row, "=IF(G6=0,G3,G3/G6)"); // Entertainment
            row++;

            spreadsheet.SetCellValue("B" + row, "Byod");
            spreadsheet.SetCellValue("C" + row, "=IF(C7=0,C4,C4/C7)"); // Company
            spreadsheet.SetCellValue("D" + row, "=IF(D7=0,D4,D4/D7)"); // Menu
            spreadsheet.SetCellValue("E" + row, "=IF(E7=0,E4,E4/E7)"); // Browser
            spreadsheet.SetCellValue("F" + row, "=IF(F7=0,F4,F4/F7)"); // Native Site
            spreadsheet.SetCellValue("G" + row, "=IF(G7=0,G4,G4/G7)"); // Entertainment
            row++;

            spreadsheet.SetCellValue("B" + row, "Total");
            spreadsheet.SetCellValue("C" + row, "=IF(C8=0,C5,C5/C8)"); // Company
            spreadsheet.SetCellValue("D" + row, "=IF(D8=0,D5,D5/D8)"); // Menu
            spreadsheet.SetCellValue("E" + row, "=IF(E8=0,E5,E5/E8)"); // Browser
            spreadsheet.SetCellValue("F" + row, "=IF(F8=0,F5,F5/F8)"); // Native Site
            spreadsheet.SetCellValue("G" + row, "=IF(G8=0,G5,G5/G8)"); // Entertainment
            row++;

            #endregion

            #region Occupation

            row++;
            var headerRowOccupation = row;
            spreadsheet.SetCellValue("C" + row, "Occupation");
            spreadsheet.SetCellValue("D" + row, "Views");
            spreadsheet.SetCellValue("E" + row, "Average Views");
            row++;

            int totalOccupated = 0;
            int totalDpgViews = 0;
            foreach (var deliverypointgroupEntity in deliverypointgroupCollection)
            {
                int count = 0;
                foreach (var occupancyEntity in deliverypointgroupEntity.DeliverypointgroupOccupancyCollection)
                {
                    count += occupancyEntity.Amount;
                }

                int dpgViews;
                if (!viewsDeliverypointgroup.TryGetValue(deliverypointgroupEntity.DeliverypointgroupId, out dpgViews))
                    dpgViews = 0;
                double averageViews = (count > 0) ? dpgViews / (double)count : 0;

                spreadsheet.SetCellValue("B" + row, deliverypointgroupEntity.Name);
                spreadsheet.SetCellValue("C" + row, count);
                spreadsheet.SetCellValue("D" + row, dpgViews);
                spreadsheet.SetCellValue("E" + row, averageViews);

                totalOccupated += count;
                totalDpgViews += dpgViews;
                row++;
            }

            double totalAverageViews = (totalOccupated > 0) ? totalDpgViews / (double)totalOccupated : 0;
            spreadsheet.SetCellValue("B" + row, "Total");
            spreadsheet.SetCellValue("C" + row, totalOccupated);
            spreadsheet.SetCellValue("D" + row, totalDpgViews);
            spreadsheet.SetCellValue("E" + row, totalAverageViews);

            // DPG Totals
            spreadsheet.SetStyle("A14", "B" + row, HorizontalAlignmentValues.Right, backgroundColor: SpreadsheetHelper.Green8);
            spreadsheet.SetStyle("E14", "E" + row, formatCode: SpreadsheetHelper.DecimalFormatCode);
            spreadsheet.SetStyle("A" + headerRowOccupation, "B" + headerRowOccupation, borders: Borders.Bottom);
            spreadsheet.SetStyle("A" + headerRowOccupation + 1, "A" + row, borders: Borders.Left);
            spreadsheet.SetStyle("C" + headerRowOccupation, "E" + headerRowOccupation, backgroundColor: SpreadsheetHelper.Green8, borders: Borders.All);
            spreadsheet.SetStyle("B" + headerRowOccupation, "E" + row, borders: Borders.Right);
            spreadsheet.SetStyle("A" + row, "E" + row, borders: Borders.Top, borderStyle: BorderStyleValues.Double); // Total border
            spreadsheet.SetStyle("A" + row, "E" + row, borders: Borders.Bottom); // Bottom border

            #endregion

            #region Root category totals per Deliverypointgroup

            row++;

            WriteRootCategoryTotalsPerDpg(ref row, spreadsheet);

            #endregion

            #region Styling

            // Title
            spreadsheet.MergeWorksheetCells("A1", "D1");
            spreadsheet.SetStyle("A1", fontSize: 14, fontStyle: FontStyle.Bold);

            // Top
            spreadsheet.SetStyle("C2", "H2", borders: Borders.Top);
            spreadsheet.SetStyle("B2", "H2", HorizontalAlignmentValues.Center, borders: Borders.Bottom);
            spreadsheet.SetStyle("C2", "H2", backgroundColor: SpreadsheetHelper.Green8);
            spreadsheet.SetStyle("C3", "H5", HorizontalAlignmentValues.Center);

            // Left
            spreadsheet.SetStyle("A3", "A11", HorizontalAlignmentValues.Center, VerticalAlignmentValues.Center, borders: Borders.All);
            spreadsheet.SetStyle("A3", "A8", borders: Borders.Bottom, borderStyle: BorderStyleValues.Medium);
            spreadsheet.SetStyle("A3", "B11", backgroundColor: SpreadsheetHelper.Green8);

            // All right borders
            spreadsheet.SetStyle("B2", "G11", borders: Borders.Right);
            spreadsheet.SetStyle("H2", "H5", borders: Borders.Right);

            // Horizontal split borders
            spreadsheet.SetStyle("B5", "H5", borders: Borders.Bottom, borderStyle: BorderStyleValues.Medium);
            spreadsheet.SetStyle("B8", "G8", borders: Borders.Bottom, borderStyle: BorderStyleValues.Medium);
            spreadsheet.SetStyle("B11", "G11", borders: Borders.Bottom);

            // Total borders
            spreadsheet.SetStyle("B5", "H5", borders: Borders.Top, borderStyle: BorderStyleValues.Double);
            spreadsheet.SetStyle("B8", "G8", borders: Borders.Top, borderStyle: BorderStyleValues.Double);
            spreadsheet.SetStyle("B11", "G11", borders: Borders.Top, borderStyle: BorderStyleValues.Double);

            spreadsheet.SetStyle("C9", "G11", formatCode: SpreadsheetHelper.DecimalFormatCode);

            spreadsheet.SetStyle("C6", "G11", HorizontalAlignmentValues.Center);

            spreadsheet.SetColumnWidth("A", 15);
            spreadsheet.SetColumnWidth("B", 10);
            spreadsheet.SetColumnWidth("C", "H", 15);

            // Set printing to 1 page width
            var pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 45);
            spreadsheet.SetPageSettings(pageSettings);

            #endregion
        }

        private void WriteRootCategoryTotalsPerDpg(ref int row, SLDocument spreadsheet)
        {
            row += 2;

            foreach (var dpgEntity in deliverypointgroupCollection)
            {
                var menuEntity = GetMenuById(dpgEntity.MenuId.GetValueOrDefault(0));
                if (menuEntity == null)
                    continue;

                spreadsheet.MergeWorksheetCells("A" + row, "E" + row);
                spreadsheet.SetCellValue("A" + row, dpgEntity.Name);
                spreadsheet.SetStyle("A" + row, "E" + row,
                                     fontStyle: FontStyle.Bold,
                                     fontSize: 14,
                                     borders: Borders.Top | Borders.Bottom,
                                     borderColor: SpreadsheetHelper.Green15,
                                     backgroundColor: SpreadsheetHelper.Green8);
                row++;

                spreadsheet.MergeWorksheetCells("A" + row, "C" + row);
                spreadsheet.SetCellValue("A" + row, "Menu: " + menuEntity.Name);
                spreadsheet.SetCellValue("D" + row, "Category");
                spreadsheet.SetCellValue("E" + row, "Product");
                spreadsheet.SetStyle("A" + row, "E" + row,
                                     fontStyle: FontStyle.Bold,
                                     borders: Borders.Bottom,
                                     borderColor: SpreadsheetHelper.Green15,
                                     backgroundColor: SpreadsheetHelper.Green8);
                spreadsheet.SetStyle("D" + row, "E" + row, HorizontalAlignmentValues.Right);
                row++;

                WriteCategoryProductPageviewSummary(spreadsheet, menuEntity.CategoryCollection, dpgEntity.DeliverypointgroupId, ref row, true);
                row++;
            }
        }

        private MenuEntity GetMenuById(int menuId)
        {
            return menuCollection.FirstOrDefault(menuEntity => menuEntity.MenuId == menuId);
        }

        private CategoryTotals WriteCategoryProductPageviewSummary(SLDocument spreadsheet, CategoryCollection categories, int deliverypointgroupId, ref int row, bool rootCategory)
        {
            categories.Sort((int)CategoryFieldIndex.SortOrder, ListSortDirection.Ascending);

            var totalCategoryViews = new CategoryTotals(0, 0);
            foreach (var category in categories)
            {
                if (rootCategory && category.ParentCategoryId.HasValue)
                    continue;
                if (rootCategory && category.ChildCategoryCollection.Count == 0 && !category.HasProducts)
                    continue;

                int categoryRow = row;

                if (rootCategory)
                {
                    row++;

                    spreadsheet.MergeWorksheetCells("A" + categoryRow, "C" + categoryRow);
                    spreadsheet.SetCellValue("A" + categoryRow, category.Name);
                    spreadsheet.SetCellValue("D" + categoryRow, 0);
                    spreadsheet.SetCellValue("E" + categoryRow, 0);

                    spreadsheet.SetStyle("A" + categoryRow, "E" + categoryRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green15);
                    spreadsheet.SetStyle("B" + categoryRow, "E" + categoryRow, HorizontalAlignmentValues.Right);
                }

                var categoryTotals = new CategoryTotals(0, 0);

                // Lookup category in GA data
                var categoryFilter = string.Format("{0} AND CategoryId='{1}' AND DeliverypointgroupId='{2}'", this.GetEventActionPredicate(EventTypes2.CategoryViewed), category.CategoryId, deliverypointgroupId);
                var categoryRows = (PageviewStatsRow2[])this.menuPageviews.Select(categoryFilter);
                foreach (var PageviewStatsRow2 in categoryRows)
                {
                    categoryTotals.Views += PageviewStatsRow2.TotalEvents;
                }

                foreach (var productEntity in category.ProductCategoryCollection)
                {
                    // Lookup products in GA data
                    var productFilter = string.Format("{0} AND CategoryId='{1}' AND DeliverypointgroupId='{2}' AND ProductId='{3}'", this.GetEventActionPredicate(EventTypes2.ProductViewed), category.CategoryId, deliverypointgroupId, productEntity.ProductId);
                    var productRows = (PageviewStatsRow2[])this.menuPageviews.Select(productFilter);
                    foreach (var PageviewStatsRow2 in productRows)
                    {
                        categoryTotals.ProductViews += PageviewStatsRow2.TotalEvents;
                    }
                }

                // Render any child categories + get their totals
                var totalFromChildren = WriteCategoryProductPageviewSummary(spreadsheet, category.ChildCategoryCollection, deliverypointgroupId, ref row, false);
                categoryTotals.Views += totalFromChildren.Views;
                categoryTotals.ProductViews += totalFromChildren.ProductViews;

                if (rootCategory)
                {
                    spreadsheet.SetCellValue("D" + categoryRow, categoryTotals.Views);
                    spreadsheet.SetCellValue("E" + categoryRow, categoryTotals.ProductViews);

                    if (categoryRow % 2 == 0)
                    {
                        spreadsheet.SetStyle("A" + categoryRow, "E" + categoryRow, backgroundColor: SpreadsheetHelper.Green2);
                    }
                }

                totalCategoryViews.Views += categoryTotals.Views;
                totalCategoryViews.ProductViews += categoryTotals.ProductViews;
            }

            return totalCategoryViews;
        }

        private string GetEventActionPredicate(EventType eventType)
        {
            string toReturn = string.Empty;
            if (eventType.NameAndSynonyms.Count == 0)
                throw new Exception("An event should at least have Name");
            else
            {
                toReturn = "(";

                for (int i = 0; i < eventType.NameAndSynonyms.Count; i++)
                {
                    if (i > 0)
                        toReturn += " OR ";
                    toReturn += string.Format("EventAction='{0}'", eventType.NameAndSynonyms[i]);
                }

                toReturn += ")";
            }

            return toReturn;

        }

        #endregion

        #region Menu Pageviews

        public void AppendPageviewsPerCategory(SLDocument spreadsheet)
        {
            int duplicateNameCounter = 0;
            foreach (var menu in menuCollection)
            {
                // Check if menu is linked to one of selected DPGs
                bool hasDeliverypointgroup = false;
                foreach (var menuDpg in menu.DeliverypointgroupCollection)
                {
                    if (deliverypointgroupCollection.Any(dpg => menuDpg.DeliverypointgroupId == dpg.DeliverypointgroupId))
                    {
                        hasDeliverypointgroup = true;
                        break;
                    }
                }
                if (!hasDeliverypointgroup)
                    continue;

                // Prepare worksheet name (name is max 31 chars +/- 12 for ' - Pageviews')
                string worksheetSubName = menu.Name;
                if (worksheetSubName.Length > 18)
                {
                    worksheetSubName = worksheetSubName.Substring(0, 18);
                }
                string worksheetName = worksheetSubName + " - Pageviews";

                if (spreadsheet.GetWorksheetNames().Contains(worksheetName))
                {
                    duplicateNameCounter++;
                    worksheetName = string.Format("{0}{1}-Pageviews", worksheetSubName, duplicateNameCounter);
                }

                // Create and select worksheet
                spreadsheet.AddWorksheet(worksheetName);
                spreadsheet.SelectWorksheet(worksheetName);

                // Menu name
                spreadsheet.SetCellValue("A1", menu.Name);
                spreadsheet.SetStyle("A1", fontStyle: FontStyle.Bold, fontSize: 18);

                // Set header cells
                spreadsheet.SetCellValue("A2", "Name");
                spreadsheet.SetCellValue("B2", "Category Views");
                spreadsheet.SetCellValue("C2", "Product Views");
                spreadsheet.SetStyle("A2", "C2", borders: Borders.Bottom, backgroundColor: SpreadsheetHelper.Green14);
                spreadsheet.SetStyle("B2", "C2", HorizontalAlignmentValues.Center);

                int row = 3;
                int indent = 0;
                WriteCategoryProductPageviewsToSheet(spreadsheet, menu.CategoryCollection, ref row, ref indent, true, false);

                spreadsheet.SetColumnWidth("A", 50);
                spreadsheet.SetColumnWidth("B", 15);
                spreadsheet.SetColumnWidth("C", 15);

                // Keep first line frozen
                spreadsheet.FreezePanes(2, 0);

                // Set printing to 1 page width
                var pageSettings = spreadsheet.GetPageSettings();
                pageSettings.ScalePage(1, 45);
                spreadsheet.SetPageSettings(pageSettings);
            }
        }

        private struct CategoryTotals
        {
            public int Views;
            public int ProductViews;

            public CategoryTotals(int views, int productViews)
            {
                this.Views = views;
                this.ProductViews = productViews;
            }
        }

        private CategoryTotals WriteCategoryProductPageviewsToSheet(SLDocument spreadsheet, CategoryCollection categories, ref int row, ref int indent, bool renderProducts, bool showEmpty)
        {
            categories.Sort((int)CategoryFieldIndex.SortOrder, ListSortDirection.Ascending);

            var totalCategoryViews = new CategoryTotals(0, 0);
            System.Drawing.Color productBorderColor = SpreadsheetHelper.Green4;
            foreach (var category in categories)
            {
                if (indent == 0 && category.ParentCategoryId.HasValue)
                    continue;

                int categoryRow = row;
                row++;

                spreadsheet.SetCellValue("A" + categoryRow, "".PadLeft((indent * 4) + 1, ' ') + category.Name);
                spreadsheet.SetCellValue("B" + categoryRow, 0);
                spreadsheet.SetCellValue("C" + categoryRow, 0);
                spreadsheet.SetCellBackground(indent, true, "A" + categoryRow, "C" + categoryRow);
                spreadsheet.SetStyle("A" + categoryRow, "C" + categoryRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green15);
                spreadsheet.SetStyle("B" + categoryRow, "C" + categoryRow, HorizontalAlignmentValues.Right);

                indent++;

                var categoryTotals = new CategoryTotals(0, 0);

                // Lookup category in GA data
                var categoryFilter = string.Format("{0} AND CategoryId='{1}'", this.GetEventActionPredicate(EventTypes2.CategoryViewed), category.CategoryId);
                var categoryRows = (PageviewStatsRow2[])this.menuPageviews.Select(categoryFilter);
                foreach (var PageviewStatsRow2 in categoryRows)
                {
                    categoryTotals.Views += PageviewStatsRow2.TotalEvents;
                }

                var sortedProducts = category.ProductCategoryCollection.OrderBy(x => x.SortOrder);
                foreach (ProductCategoryEntity productCategory in sortedProducts)
                {
                    int productViews = 0;

                    // Lookup Product in GA data
                    var productRows = (PageviewStatsRow2[])this.menuPageviews.Select(string.Format("ProductId={0} AND CategoryId={1}", productCategory.ProductId, category.CategoryId));
                    foreach (var productRow in productRows)
                    {
                        productViews += productRow.TotalEvents;
                        categoryTotals.ProductViews += productRow.TotalEvents;
                    }

                    if (renderProducts && (showEmpty || productViews > 0))
                    {
                        int productRow = row;
                        row++;

                        spreadsheet.SetCellValue("A" + productRow, "".PadLeft((indent * 4) + 1, ' ') + productCategory.ProductEntity.Name);
                        spreadsheet.SetCellValue("B" + productRow, string.Empty);
                        spreadsheet.SetCellValue("C" + productRow, productViews);
                        spreadsheet.SetStyle("A" + productRow, "C" + productRow, borders: Borders.Bottom, borderColor: productBorderColor);
                    }
                }

                // Render any child categories + get their totals
                var totalFromChildren = WriteCategoryProductPageviewsToSheet(spreadsheet, category.ChildCategoryCollection, ref row, ref indent, renderProducts, showEmpty);
                var ownCategoryTotalViews = categoryTotals.Views;
                categoryTotals.Views += totalFromChildren.Views;
                categoryTotals.ProductViews += totalFromChildren.ProductViews;

                indent--;

                if (categoryTotals.Views > ownCategoryTotalViews)
                    spreadsheet.SetCellValue("B" + categoryRow, string.Format("{0} ({1})", ownCategoryTotalViews, categoryTotals.Views));
                else
                    spreadsheet.SetCellValue("B" + categoryRow, categoryTotals.Views);

                spreadsheet.SetCellValue("C" + categoryRow, categoryTotals.ProductViews);

                totalCategoryViews.Views += categoryTotals.Views;
                totalCategoryViews.ProductViews += categoryTotals.ProductViews;
            }

            return totalCategoryViews;
        }

        #endregion

        #region Company Screen Time

        private struct ScreenInfo
        {
            public int TotalEvents;
            public long EventValue;
            public ApplicationType AppType;
        }

        public void AppendAverageCompanyScreenTime(SLDocument spreadsheet)
        {
            // Write data to spreadsheet
            const string worksheetName = "Company Tab Views";
            spreadsheet.AddWorksheet(worksheetName);
            spreadsheet.SelectWorksheet(worksheetName);

            int row = 1;
            foreach (var uiMode in uiModeCollection)
            {
                var sbDeliverypointgroups = new StringBuilder();

                // Check if menu is linked to one of selected DPGs
                bool hasDeliverypointgroup = false;
                foreach (var uiDpg in uiMode.DeliverypointgroupCollection)
                {
                    if (deliverypointgroupCollection.Any(dpg => uiDpg.DeliverypointgroupId == dpg.DeliverypointgroupId))
                    {                        
                        hasDeliverypointgroup = true;
                        break;
                    }
                }
                if (!hasDeliverypointgroup && (uiMode.Type == UIModeType.VenueOwnedStaffDevices || uiMode.Type == UIModeType.VenueOwnedUserDevices))
                    continue;

                var screenInfoMap = new Dictionary<string, List<ScreenInfo>>();
                foreach (var uiTab in uiMode.UITabCollection)
                {
                    List<ScreenInfo> screenCollection;
                    if (!screenInfoMap.TryGetValue(uiTab.CaptionOrType, out screenCollection))
                    {
                        screenCollection = new List<ScreenInfo>();
                        screenInfoMap.Add(uiTab.CaptionOrType, screenCollection);
                    }

                    // Get relevant info from datatable
                    var pageviewRows = (PageviewStatsRow2[])this.Select(string.Format("TabId='{0}'", uiTab.UITabId));
                    foreach (var pageviewRow in pageviewRows)
                    {
                        var screenInfo = new ScreenInfo();

                        if (EventTypes2.CompanyScreenView.NameAndSynonymsContains(pageviewRow.EventAction))
                        {
                            screenInfo.TotalEvents = pageviewRow.TotalEvents;
                            screenInfo.AppType = pageviewRow.AppType;
                        }
                        else if (EventTypes2.CompanyScreenLeft.NameAndSynonymsContains(pageviewRow.EventAction))
                        {
                            screenInfo.EventValue = pageviewRow.EventValue;
                            screenInfo.AppType = pageviewRow.AppType;
                        }

                        screenCollection.Add(screenInfo);
                    }
                }

                if (screenInfoMap.Count > 0)
                {
                    spreadsheet.MergeWorksheetCells("A" + row, "E" + row);
                    spreadsheet.SetStyle("A" + row, HorizontalAlignmentValues.Center, fontStyle: FontStyle.Bold);
                    spreadsheet.SetCellValue("A" + row, uiMode.Name);
                    row++;

                    WriteAverageCompanyScreenTime(spreadsheet, screenInfoMap, ref row);

                    row++;
                }
            }

            spreadsheet.SetColumnStyle(2, 5, HorizontalAlignmentValues.Center);
            spreadsheet.SetColumnWidth("A", 30);
            spreadsheet.SetColumnWidth("B", "D", 12);
            spreadsheet.SetColumnWidth("E", 14);

            // Set printing to 1 page width
            var pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 45);
            spreadsheet.SetPageSettings(pageSettings);
        }

        private void WriteAverageCompanyScreenTime(SLDocument spreadsheet, Dictionary<string, List<ScreenInfo>> screenInfoMap, ref int row)
        {
            // Header
            int headerRow = row;
            row++;

            spreadsheet.SetCellValue("A" + headerRow, "Name");
            spreadsheet.SetCellValue("B" + headerRow, "IRT");
            spreadsheet.SetCellValue("C" + headerRow, "Byod");
            spreadsheet.SetCellValue("D" + headerRow, "Total");
            spreadsheet.SetCellValue("E" + headerRow, "Avg Time (sec)");
            spreadsheet.SetStyle("A" + headerRow, "E" + headerRow, backgroundColor: SpreadsheetHelper.Heading1);

            foreach (var screenInfoList in screenInfoMap)
            {
                int totalEvents = 0;
                int totalEventsIrt = 0;
                int totalEventsByod = 0;
                long totalEventValue = 0;
                foreach (var screenInfo in screenInfoList.Value)
                {
                    if ((screenInfo.AppType == ApplicationType.IrtApp && !this.reportFilter.IncludeInRoomTablets) ||
                        (screenInfo.AppType == ApplicationType.ByodApp && !this.reportFilter.IncludeByodApps))
                    {
                        continue;
                    }

                    totalEvents += screenInfo.TotalEvents;
                    totalEventValue += screenInfo.EventValue;

                    if (screenInfo.AppType == ApplicationType.IrtApp)
                        totalEventsIrt += screenInfo.TotalEvents;
                    else if (screenInfo.AppType == ApplicationType.ByodApp)
                        totalEventsByod += screenInfo.TotalEvents;
                }

                double averageTime = (totalEvents > 0) ? System.Math.Round(totalEventValue / (double)totalEvents) : 0;

                spreadsheet.SetCellValue("A" + row, screenInfoList.Key);
                spreadsheet.SetCellValue("B" + row, totalEventsIrt);
                spreadsheet.SetCellValue("C" + row, totalEventsByod);
                spreadsheet.SetCellValue("D" + row, totalEvents);
                spreadsheet.SetCellValue("E" + row, averageTime);

                if (row % 2 != 0)
                {
                    spreadsheet.SetStyle("A" + row, "E" + row, backgroundColor: SpreadsheetHelper.Green2);
                }

                row++;
            }

            // Style
            spreadsheet.SetBorder(SpreadsheetHelper.Green8, Borders.Bottom, 1, 5, headerRow + 1, row - 1);
        }

        #endregion

        #region Most viewed products

        public void AppendMostViewedProducts(SLDocument spreadsheet, int amount)
        {
            int duplicateNameCounter = 0;
            foreach (var menu in menuCollection)
            {
                // Check if menu is linked to one of selected DPGs
                bool hasDeliverypointgroup = false;
                foreach (var menuDpg in menu.DeliverypointgroupCollection)
                {
                    if (deliverypointgroupCollection.Any(dpg => menuDpg.DeliverypointgroupId == dpg.DeliverypointgroupId))
                    {
                        hasDeliverypointgroup = true;
                        break;
                    }
                }
                if (!hasDeliverypointgroup)
                    continue;

                // Prepare worksheet name (name is max 31 chars +/- 14 for ' - Most viewed')
                string worksheetSubName = menu.Name;
                if (worksheetSubName.Length > 16)
                {
                    worksheetSubName = worksheetSubName.Substring(0, 16);
                }
                string worksheetName = worksheetSubName + " - Most viewed";

                if (spreadsheet.GetWorksheetNames().Contains(worksheetName))
                {
                    duplicateNameCounter++;
                    worksheetName = string.Format("{0}{1}-Most viewed", worksheetSubName, duplicateNameCounter);
                }

                spreadsheet.AddWorksheet(worksheetName);
                spreadsheet.SelectWorksheet(worksheetName);

                WriteMostViewedProductsForMenu(spreadsheet, menu, amount);

                spreadsheet.SetColumnWidth("A", 4);
                spreadsheet.SetColumnWidth("B", 60);
                spreadsheet.SetColumnWidth("C", 8);

                spreadsheet.SetColumnStyle(1, HorizontalAlignmentValues.Center);
                spreadsheet.SetColumnStyle(3, HorizontalAlignmentValues.Center);

                // Set printing to 1 page width
                var pageSettings = spreadsheet.GetPageSettings();
                pageSettings.ScalePage(1, 45);
                spreadsheet.SetPageSettings(pageSettings);
            }
        }

        private void WriteMostViewedProductsForMenu(SLDocument spreadsheet, MenuEntity menu, int amount)
        {
            int row = 1;
            spreadsheet.MergeWorksheetCells("A1", "B1");
            spreadsheet.SetCellValue("A1", menu.Name);
            spreadsheet.SetStyle("A1", fontStyle: FontStyle.Bold, fontSize: 18);
            row++;

            foreach (var category in menu.CategoryCollection)
            {
                if (category.ParentCategoryId.HasValue)
                    continue;

                spreadsheet.SetCellValue("A" + row, "#");
                spreadsheet.SetCellValue("B" + row, category.Name);
                spreadsheet.SetCellValue("C" + row, "Views");
                spreadsheet.SetStyle("A" + row, "C" + row, borders: Borders.Bottom, backgroundColor: SpreadsheetHelper.Heading1);
                row++;

                var filter = string.Format("{0} AND EventLabel LIKE '/{1}/*'", this.GetEventActionPredicate(EventTypes2.ProductViewed), category.Name.Replace("'", "''"));
                var rows = (PageviewStatsRow2[])this.menuPageviews.Select(filter, "TotalEvents DESC");
                if (rows.Length == 0)
                {
                    spreadsheet.SetCellValue("B" + row, "No views");
                    spreadsheet.SetBorder(SpreadsheetHelper.Green8, Borders.Bottom, 1, 3, row, row);
                    row++;
                }
                else
                {
                    for (int i = 0; i < (rows.Length < amount ? rows.Length : amount); i++)
                    {
                        spreadsheet.SetCellValue("A" + row, i + 1);
                        spreadsheet.SetCellValue("B" + row, rows[i].EventLabel.Replace("/" + category.Name + "/", ""));
                        spreadsheet.SetCellValue("C" + row, rows[i].TotalEvents);

                        if (i % 2 != 0)
                        {
                            spreadsheet.SetStyle("A" + row, "C" + row, backgroundColor: SpreadsheetHelper.Green2);
                        }
                        spreadsheet.SetBorder(SpreadsheetHelper.Green8, Borders.Bottom, 1, 3, row, row);

                        row++;
                    }
                }

                row++;
            }
        }

        #endregion

        #region Most viewed microsites

        private class MicrositePage
        {
            public string Name;
            public int Views;
            public int SubViews;
            public List<MicrositePage> Children;
            private MicrositePage Parent;

            public MicrositePage(string name, MicrositePage parent)
            {
                this.Name = name;
                this.Views = 0;
                this.SubViews = 0;
                this.Children = new List<MicrositePage>();
                this.Parent = parent;
            }

            public MicrositePage GetOrAddChild(string name)
            {
                MicrositePage microChild = null;
                foreach (var child in Children)
                {
                    if (child.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                    {
                        microChild = child;
                        break;
                    }
                }

                if (microChild == null)
                {
                    microChild = new MicrositePage(name, this);
                    Children.Add(microChild);
                }

                return microChild;
            }

            public bool IsRoot
            {
                get { return Parent == null; }
            }
        }

        public void AppendMostViewedSites(SLDocument spreadsheet)
        {
            const string worksheetName = "Sites";
            spreadsheet.AddWorksheet(worksheetName);
            spreadsheet.SelectWorksheet(worksheetName);

            bool writtenBrowserHeader = false;
            bool writtenNativeHeader = false;

            int browserRow = 1;
            int nativeRow = 1;
            const char browserColumn = 'A';
            const char nativeColumn = 'E';

            foreach (var deliverypointgroupEntity in deliverypointgroupCollection)
            {
                var browserSites = new Dictionary<string, MicrositePage>();
                var nativeSites = new Dictionary<string, MicrositePage>();

                var filter = string.Format("DeliverypointgroupId='{0}' AND ({1} OR {2})", deliverypointgroupEntity.DeliverypointgroupId, this.GetEventActionPredicate(EventTypes2.BrowserScreenView), this.GetEventActionPredicate(EventTypes2.NativePageView));
                var rows = (PageviewStatsRow2[])this.Select(filter, "EventLabel ASC");
                foreach (var dataRow in rows)
                {
                    var fullSite = dataRow.EventLabel;
                    if (fullSite.IndexOf('?') > 0)
                    {
                        // Strip parameters from end
                        fullSite = fullSite.Remove(fullSite.IndexOf('?'));
                    }

                    string[] sitePieces = fullSite.Split('/');
                    string sitePiece = sitePieces[0];

                    MicrositePage currentMicrosite = null;
                    if (EventTypes2.BrowserScreenView.NameAndSynonymsContains(dataRow.EventAction))
                    {
                        if (!browserSites.TryGetValue(sitePiece, out currentMicrosite))
                        {
                            currentMicrosite = new MicrositePage(sitePiece, null);
                            browserSites.Add(sitePiece, currentMicrosite);
                        }
                    }
                    else if (EventTypes2.NativePageView.NameAndSynonymsContains(dataRow.EventAction))
                    {
                        if (!nativeSites.TryGetValue(sitePiece, out currentMicrosite))
                        {
                            currentMicrosite = new MicrositePage(sitePiece, null);
                            nativeSites.Add(sitePiece, currentMicrosite);
                        }
                    }

                    if (currentMicrosite != null)
                    {
                        SaveMicrositeChildren(sitePieces, 1, currentMicrosite, dataRow.TotalEvents);
                    }
                }


                if (!writtenBrowserHeader)
                {
                    // Merge cells
                    var mergeCell = (char)((byte)browserColumn + 1);
                    spreadsheet.MergeWorksheetCells(browserColumn + "1", mergeCell + "1");
                    spreadsheet.SetCellValue(browserColumn + "1", "Browser Views");
                    spreadsheet.SetStyle(browserColumn + "1", HorizontalAlignmentValues.Center, fontStyle: FontStyle.Bold);

                    writtenBrowserHeader = true;
                    browserRow += 2;
                }

                WriteBrowserViews(spreadsheet, browserSites, deliverypointgroupEntity, browserColumn, ref browserRow);
                browserRow++;

                if (!writtenNativeHeader)
                {
                    var mergeCell = (char)((byte)nativeColumn + 1);
                    spreadsheet.MergeWorksheetCells(nativeColumn + "1", mergeCell + "1");
                    spreadsheet.SetCellValue(nativeColumn + "1", "Native Site Views");
                    spreadsheet.SetStyle(nativeColumn + "1", HorizontalAlignmentValues.Center, fontStyle: FontStyle.Bold);

                    writtenNativeHeader = true;
                    nativeRow += 2;
                }

                WriteBrowserViews(spreadsheet, nativeSites, deliverypointgroupEntity, nativeColumn, ref nativeRow);
                nativeRow++;
            }

            spreadsheet.SetColumnWidth("A", 40);
            spreadsheet.SetColumnWidth("B", 15);
            spreadsheet.SetColumnStyle(2, HorizontalAlignmentValues.Center);

            spreadsheet.SetColumnWidth("E", 40);
            spreadsheet.SetColumnWidth("F", 15);
            spreadsheet.SetColumnStyle(6, HorizontalAlignmentValues.Center);

            // Set printing to 1 page width
            var pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 45);
            spreadsheet.SetPageSettings(pageSettings);
        }

        private int SaveMicrositeChildren(string[] sitePieces, int currentIndex, MicrositePage currentMicrosite, int views)
        {
            int viewTotal = views;
            if (currentIndex < sitePieces.Length && sitePieces[currentIndex].Length > 0)
            {
                currentMicrosite.Views += views;

                var micrositeChild = currentMicrosite.GetOrAddChild(sitePieces[currentIndex]);
                currentIndex++;

                viewTotal = SaveMicrositeChildren(sitePieces, currentIndex, micrositeChild, views);
                currentMicrosite.SubViews += viewTotal;
            }
            else
            {
                currentMicrosite.Views += views;
            }

            return viewTotal;
        }

        private void WriteBrowserViews(SLDocument spreadsheet, Dictionary<string, MicrositePage> sites, DeliverypointgroupEntity dpg, char column, ref int row)
        {
            var secondColumn = (char)((byte)column + 1);
            spreadsheet.MergeWorksheetCells(column + "" + row, secondColumn + "" + row);
            spreadsheet.SetCellValue(column + "" + row, dpg.Name);
            spreadsheet.SetStyle(column + "" + row, HorizontalAlignmentValues.Center, fontStyle: FontStyle.Bold);
            row++;

            spreadsheet.SetCellValue(column + "" + row, "Name");
            spreadsheet.SetCellValue(secondColumn + "" + row, "Views (Total)");
            spreadsheet.SetStyle(column + "" + row, secondColumn + "" + row, backgroundColor: SpreadsheetHelper.Heading1);
            row++;

            foreach (var micrositePage in sites)
            {
                int indent = 0;
                WriteMicrositeChildren(spreadsheet, micrositePage.Value, column, secondColumn, ref row, ref indent);
            }

            if (sites.Count == 0)
            {
                spreadsheet.SetCellValue(column + "" + row, "No views");
                row++;
            }
        }

        private void WriteMicrositeChildren(SLDocument spreadsheet, MicrositePage page, char column, char secondColumn, ref int row, ref int indent)
        {
            spreadsheet.SetCellValue(column + "" + row, "".PadLeft((indent * 4), ' ') + page.Name);
            if (page.SubViews > 0)
                spreadsheet.SetCellValue(secondColumn + "" + row, string.Format("{0} ({1})", (page.Views - page.SubViews), page.Views));
            else
                spreadsheet.SetCellValue(secondColumn + "" + row, page.Views);
            spreadsheet.SetCellBackground(indent, true, column + "" + row, secondColumn + "" + row);

            row++;
            indent++;

            foreach (var child in page.Children)
            {
                WriteMicrositeChildren(spreadsheet, child, column, secondColumn, ref row, ref indent);
            }

            indent--;
        }

        #endregion

        #region Entertainment Views

        public void AppendEntertainmentViews(SLDocument spreadsheet)
        {
            const string worksheetName = "Entertainments";
            spreadsheet.AddWorksheet(worksheetName);
            spreadsheet.SelectWorksheet(worksheetName);

            int row = 1;
            char column = 'A';
            spreadsheet.SetCellValue(column + "1", "Name");

            // Map Deliverypointgroup to column
            var deliverypointgroupColumnMap = new Dictionary<int, char>();
            foreach (var deliverypointgroupEntity in deliverypointgroupCollection)
            {
                column++;
                deliverypointgroupColumnMap.Add(deliverypointgroupEntity.DeliverypointgroupId, column);

                spreadsheet.SetCellValue(column + "1", deliverypointgroupEntity.Name);
            }

            column++;
            spreadsheet.SetCellValue(column + "1", "Total");

            foreach (var entertainmentEntity in entertainmentCollection)
            {
                row++;
                spreadsheet.SetCellValue("A" + row, entertainmentEntity.Name);

                int total = 0;
                foreach (var dpg in deliverypointgroupColumnMap)
                {
                    int views = 0;
                    var pageviewRows = (PageviewStatsRow2[])this.Select(string.Format("EntertainmentId='{0}' AND DeliverypointgroupId='{1}'", entertainmentEntity.EntertainmentId, dpg.Key));
                    foreach (var pageviewRow in pageviewRows)
                    {
                        views += pageviewRow.TotalEvents;
                    }

                    total += views;
                    string cellKey = dpg.Value + "" + row;
                    spreadsheet.SetCellValue(cellKey, views);
                }

                spreadsheet.SetCellValue(column + "" + row, total);

                if (row % 2 != 0)
                {
                    spreadsheet.SetStyle("A" + row, column + "" + row, backgroundColor: SpreadsheetHelper.Green2);
                }
            }

            // Style it
            spreadsheet.SetStyle("A1", column + "1", backgroundColor: SpreadsheetHelper.Heading1);
            spreadsheet.SetColumnStyle(2, 1 + deliverypointgroupColumnMap.Count, HorizontalAlignmentValues.Center);
            spreadsheet.SetStyle("A2", column + "" + row, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green8);

            spreadsheet.SetColumnWidth(1, 40);
            spreadsheet.SetColumnWidth(2, 1 + deliverypointgroupColumnMap.Count, 25);

            // Set printing to 1 page width
            var pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 45);
            spreadsheet.SetPageSettings(pageSettings);
        }

        #endregion

        #endregion

        #region Column Name definitions

        internal enum AnalyticsHeaders
        {
            EventCategory,
            EventAction,
            EventLabel,
            AppType,
            PrimaryKeys,
            TotalEvents,
            EventValue,
            AvgEventValue,
            Users
        }

        internal class ColumnNames
        {
            internal const string EventCategory = "EventCategory";
            internal const string EventAction = "EventAction";
            internal const string EventLabel = "EventLabel";
            internal const string AppType = "AppType";
            internal const string PrimaryKeys = "PrimaryKeys";
            internal const string DeliverypointgroupId = "DeliverypointgroupId";
            internal const string TotalEvents = "TotalEvents";
            internal const string EventValue = "EventValue";
            internal const string AverageEventValue = "AverageEventValue";
            internal const string Users = "Users";
            internal const string EventsPerUser = "EventsPerUser";
            internal const string CategoryId = "CategoryId";
            internal const string ProductId = "ProductId";
            internal const string TabId = "TabId";
            internal const string EntertainmentId = "EntertainmentId";
        }

        #endregion

        #region Typed Data Row class

        public class PageviewStatsRow2 : DataRow
        {
            internal PageviewStatsRow2(DataRowBuilder builder)
                : base(builder)
            {
                DeliverypointgroupId = 0;
                CategoryId = 0;
                ProductId = 0;
                TabId = 0;
                EntertainmentId = 0;
            }

            public string EventCategory
            {
                get { return (string)this[ColumnNames.EventCategory]; }
                set { this[ColumnNames.EventCategory] = value; }
            }

            public string EventAction
            {
                get { return (string)this[ColumnNames.EventAction]; }
                set { this[ColumnNames.EventAction] = value; }
            }

            public string EventLabel
            {
                get { return (string)this[ColumnNames.EventLabel]; }
                set { this[ColumnNames.EventLabel] = value; }
            }

            public ApplicationType AppType
            {
                get { return (ApplicationType)this[ColumnNames.AppType]; }
                set { this[ColumnNames.AppType] = value; }
            }

            public int TotalEvents
            {
                get { return (int)this[ColumnNames.TotalEvents]; }
                set { this[ColumnNames.TotalEvents] = value; }
            }

            public long EventValue
            {
                get { return (long)this[ColumnNames.EventValue]; }
                set { this[ColumnNames.EventValue] = value; }
            }

            public int Users
            {
                get { return (int)this[ColumnNames.Users]; }
                set { this[ColumnNames.Users] = value; }
            }

            public double EventsPerUser
            {
                get { return (double)this[ColumnNames.EventsPerUser]; }
                set { this[ColumnNames.EventsPerUser] = value; }
            }

            public double AverageEventValue
            {
                get { return (double)this[ColumnNames.AverageEventValue]; }
                set { this[ColumnNames.AverageEventValue] = value; }
            }

            public string PrimaryKeys
            {
                get { return (string)this[ColumnNames.PrimaryKeys]; }
                set
                {
                    this[ColumnNames.PrimaryKeys] = value;

                    // Parse deliverypointgroup id
                    int dpgIndex = value.IndexOf("[DPG-", StringComparison.InvariantCultureIgnoreCase);
                    if (dpgIndex >= 0)
                    {
                        dpgIndex += 5;
                        int dpgIndexEnd = value.IndexOf("]", dpgIndex, StringComparison.InvariantCultureIgnoreCase);
                        string strDeliverypointgroupId = value.Substring(dpgIndex, dpgIndexEnd - dpgIndex);

                        int tempDpgId;
                        if (int.TryParse(strDeliverypointgroupId, out tempDpgId))
                        {
                            DeliverypointgroupId = tempDpgId;
                        }                        
                    }

                    // Parse category id
                    int categoryIndex = value.IndexOf("[CT-", StringComparison.InvariantCultureIgnoreCase);
                    if (categoryIndex >= 0)
                    {
                        categoryIndex += 4;
                        int categoryIndexEnd = value.IndexOf("]", categoryIndex, StringComparison.InvariantCultureIgnoreCase);
                        CategoryId = int.Parse(value.Substring(categoryIndex, categoryIndexEnd - categoryIndex));

                        if (EventTypes2.CategoryViewed.NameAndSynonymsContains(EventAction) && CategoryId > 100000000)
                        {
                            CategoryId -= 100000000;
                        }
                    }

                    // Parse product id
                    int productIndex = value.IndexOf("[P-", StringComparison.InvariantCultureIgnoreCase);
                    if (productIndex >= 0)
                    {
                        productIndex += 3;
                        int productIndexEnd = value.IndexOf("]", productIndex, StringComparison.InvariantCultureIgnoreCase);
                        ProductId = int.Parse(value.Substring(productIndex, productIndexEnd - productIndex));
                    }

                    // Parse tab id
                    int tabIndex = value.IndexOf("[T-", StringComparison.InvariantCultureIgnoreCase);
                    if (tabIndex >= 0)
                    {
                        tabIndex += 3;
                        int tabIndexEnd = value.IndexOf("]", tabIndex, StringComparison.InvariantCultureIgnoreCase);
                        TabId = int.Parse(value.Substring(tabIndex, tabIndexEnd - tabIndex));
                    }

                    // Parse entertainment id
                    int entertainmentIndex = value.IndexOf("[ETM-", StringComparison.InvariantCultureIgnoreCase);
                    if (entertainmentIndex >= 0)
                    {
                        entertainmentIndex += 5;
                        int entertainmentIndexEnd = value.IndexOf("]", entertainmentIndex, StringComparison.InvariantCultureIgnoreCase);
                        EntertainmentId = int.Parse(value.Substring(entertainmentIndex, entertainmentIndexEnd - entertainmentIndex));
                    }
                }
            }

            public int DeliverypointgroupId
            {
                get { return (int)this[ColumnNames.DeliverypointgroupId]; }
                private set { this[ColumnNames.DeliverypointgroupId] = value; }
            }

            public int CategoryId
            {
                get { return (int)this[ColumnNames.CategoryId]; }
                private set { this[ColumnNames.CategoryId] = value; }
            }

            public int ProductId
            {
                get { return (int)this[ColumnNames.ProductId]; }
                private set { this[ColumnNames.ProductId] = value; }
            }

            public int TabId
            {
                get { return (int)this[ColumnNames.TabId]; }
                private set { this[ColumnNames.TabId] = value; }
            }

            public int EntertainmentId
            {
                get { return (int)this[ColumnNames.EntertainmentId]; }
                private set { this[ColumnNames.EntertainmentId] = value; }
            }


        }

        #endregion
    }
}