﻿using System;
using Obymobi.Constants;
using Obymobi.Enums;
using Obymobi.Web.Google;
using SpreadsheetLight;

namespace Obymobi.Logic.Analytics
{
    public class PageviewsReport : IReport
    {
        public enum PageviewsErrorEnum
        {
            UnableToGetAnalyticsData
        }

        private readonly Filter filter;
        private readonly CloudEnvironment cloudEnvironment;


        public PageviewsReport(Filter filter, CloudEnvironment cloudEnvironment)
        {
            this.filter = filter;
            this.cloudEnvironment = cloudEnvironment;
        }

        public bool RunReport(out SLDocument sl)
        {
            var api = new GoogleAPI();
            sl = new SLDocument();

            try
            {
                string profile = ObymobiConstants.GetGoogleAnalyticsTrackingId(this.cloudEnvironment);
                if (TestUtil.IsPcBattleStationDanny)
                    profile = "UA-45902422-4";
                else if (TestUtil.IsPcFloris)
                    profile = "UA-45902422-4";
                else if (TestUtil.IsPcGabriel) // Let's join the league of extraordinary gentlemen
                    profile = "UA-45902422-4"; // 4 is live


                api.SetProfile(profile);
            }
            catch (Exception ex)
            {
                throw new ObymobiException(PageviewsErrorEnum.UnableToGetAnalyticsData, ex.Message);
            }

            if (api.Profile != null)
            {
                var stats = new PageviewStatsTable();
                stats.FetchData(api, this.filter);

                stats.AppendTotalPageviews(sl, "Summary");
                stats.AppendPageviewsPerCategory(sl);
                stats.AppendAverageCompanyScreenTime(sl);
                stats.AppendMostViewedProducts(sl, 10);
                stats.AppendMostViewedSites(sl);
                stats.AppendEntertainmentViews(sl);

                // Delete default worksheet
                sl.DeleteWorksheet(SLDocument.DefaultFirstSheetName);
                sl.SelectWorksheet("Summary");
            }

            return true;
        }
    }
}
