﻿using System;
using Obymobi.Constants;
using Obymobi.Enums;
using Obymobi.Logic.Analytics;
using Obymobi.Web.Google;
using SpreadsheetLight;

namespace Obymobi.Web.Reporting
{
    public class SummaryReport : IReport
    {
        private readonly Filter filter;
        private readonly CloudEnvironment cloudEnvironment;

        public SummaryReport(Filter filter, CloudEnvironment cloudEnvironment)
        {
            this.filter = filter;
            this.cloudEnvironment = cloudEnvironment;
        }

        public bool RunReport(out SLDocument sl)
        {
            var api = new GoogleAPI();
            sl = new SLDocument();

            try
            {
                string profile = ObymobiConstants.GetGoogleAnalyticsTrackingId(this.cloudEnvironment);
                api.SetProfile(profile);
            }
            catch (Exception ex)
            {
                throw new ObymobiException(PageviewsReport.PageviewsErrorEnum.UnableToGetAnalyticsData, ex.Message);
            }

            sl.DeleteWorksheet(SLDocument.DefaultFirstSheetName);

            TransactionsSummary(sl);

            if (api.Profile != null)
            {
                var stats = new PageviewStatsTable();
                stats.FetchData(api, this.filter);

                stats.AppendTotalPageviews(sl, "Pageviews");
            }

            // Set all print sizes to A4
            sl.SetPageSizeOfWorksheets(SLPaperSizeValues.A4Paper);

            return true;
        }

        private void TransactionsSummary(SLDocument sl)
        {
            string summaryWorkSheetName = "Transactions";

            // Prepare the Summary worksheet            
            int summaryRowIndex = 2; // (1 is already used below)
            sl.AddWorksheet(summaryWorkSheetName);

            // Headings: Left (Orders)
            sl.MergeWorksheetCells("A1", "C1");
            sl.SetCellValue("A1", "Orders");
            sl.SetStyle("A1", horizontalAlignment: DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center, fontSize: 16);

            // Headings: Right (Service Request)
            sl.MergeWorksheetCells("F1", "H1");
            sl.SetCellValue("F1", "Service Requests");
            sl.SetStyle("F1", horizontalAlignment: DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center, fontSize: 16);

            // Columns width
            sl.SetColumnWidth("A", 43.86);
            sl.SetColumnWidth("B", 10.86);
            sl.SetColumnWidth("C", 10.86);
            sl.SetColumnWidth("D", 2.29);
            sl.SetColumnWidth("E", 2.29);
            sl.SetColumnWidth("F", 43.86);
            sl.SetColumnWidth("G", 10.86);
            sl.SetColumnWidth("H", 10.86);

            // Get Deliverypointgroup information
            DeliverypointgroupStatsTable stats = new DeliverypointgroupStatsTable(this.filter);
            stats.FetchDeliverypointgroupBasedStatistics();

            // Render the Deliverypointgroup part of the Summary worksheet            
            stats.AppendToSummaryWorksheet(sl, summaryWorkSheetName, ref summaryRowIndex);

            // Get the Category / Products information
            CategoryProductStatsTable menuStats = new CategoryProductStatsTable(this.filter);
            menuStats.FetchMenuStats();

            // Render the Products/Categories part of the Summary worksheet
            menuStats.AppendToSummaryWorksheet(sl, summaryWorkSheetName, ref summaryRowIndex);

            // Draw dividing line in middle of summary
            sl.SetStyle("E1", "E" + summaryRowIndex, borders: Borders.Left);

            // Scale 1x1 Page
            sl.SelectWorksheet(summaryWorkSheetName);
            SLPageSettings pageSettings = sl.GetPageSettings();
            pageSettings.ScalePage(1, 1);
            sl.SetPageSettings(pageSettings);
        }
    }
}
