﻿using System;
using Obymobi.Constants;
using Obymobi.Enums;
using Obymobi.Web.Google;
using SpreadsheetLight;
using Obymobi.Logic.Analytics;

namespace Obymobi.Web.Reporting
{
    public class MarketingReport : IReport
    {
        public enum PageviewsErrorEnum
        {
            UnableToGetAnalyticsData
        }

        private readonly Filter filter;
        private readonly CloudEnvironment cloudEnvironment;

        public MarketingReport(Filter filter, CloudEnvironment cloudEnvironment)
        {
            this.filter = filter;
            this.cloudEnvironment = cloudEnvironment;
        }

        public bool RunReport(out SLDocument sl)
        {
            var api = new GoogleAPI();
            sl = new SLDocument();

            try
            {
                string profile = ObymobiConstants.GetGoogleAnalyticsTrackingId(this.cloudEnvironment);

                if (TestUtil.IsPcGabriel)
                    profile = "UA-45902422-2"; // == test

                api.SetProfile(profile);
            }
            catch (Exception ex)
            {
                throw new ObymobiException(PageviewsErrorEnum.UnableToGetAnalyticsData, ex.Message);
            }

            if (api.Profile != null)
            {
                var stats = new UpsellingStatsTable(this.filter, api);
                var messaging = new MessagingStatsTable(this.filter, api);

                messaging.LoadData();
                stats.LoadData(messaging.Rows.Count);

                stats.GenerateSummary(sl);
                stats.GenerateRevenuePerUpsellType(sl);
                stats.GenerateAdvertisementInfo(sl);
                messaging.GenerateAllData(sl);

                sl.SelectWorksheet("Summary");

                // Set all print sizes to A4
                sl.SetPageSizeOfWorksheets(SLPaperSizeValues.A4Paper);
            }

            return true;
        }
    }
}
