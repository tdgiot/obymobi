﻿using System;
using System.Collections.Generic;
using System.Net;
using Google.Apis.Manual.Util;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Analytics;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dionysos.Collections;
using Dionysos.Logging;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using PageFields = Obymobi.Data.HelperClasses.PageFields;

namespace Obymobi.Web.Reporting
{
    public class ExternalContentReport : IReport
    {
        readonly Dictionary<string, int> checkedUrls = new Dictionary<string, int>();

        private ILoggingProvider LoggingProvider { get; set; }

        public ExternalContentReport(ILoggingProvider loggingProvider)
        {
            LoggingProvider = loggingProvider;
        }

        public bool RunReport(out SLDocument sl)
        {
            var checkList = new List<ReportItem>();

            // Entertainments (Urls)
            CheckEntertainments(ref checkList);
            LoggingProvider.Information("ExternalContentReport - #1 Checklist Size: {0}", checkList.Count);

            // UITab (type = web)
            CheckUITabs(ref checkList);
            LoggingProvider.Information("ExternalContentReport - #2 Checklist Size: {0}", checkList.Count);

            // Attachmente (Urls)
            CheckAttachments(ref checkList);
            LoggingProvider.Information("ExternalContentReport - #3 Checklist Size: {0}", checkList.Count);

            // Site page (type = web view)
            CheckSitePages(ref checkList);
            LoggingProvider.Information("ExternalContentReport - #4 Checklist Size: {0}", checkList.Count);

            // Advertisement (url)
            CheckAdvertisements(ref checkList);
            LoggingProvider.Information("ExternalContentReport - #5 Checklist Size: {0}", checkList.Count);

            sl = null;
            if (checkList.Count > 0)
            {
                LoggingProvider.Information("ExternalContentReport - Writing output to spreadsheet");

                // Create spreadsheet
                sl = new SLDocument();
                sl.RenameWorksheet(SLDocument.DefaultFirstSheetName, "Summary");
                sl.SelectWorksheet("Summary");

                CreateReport(sl, checkList);

                // Set all print sizes to A4
                sl.SetPageSizeOfWorksheets(SLPaperSizeValues.A4Paper);
            }

            LoggingProvider.Information("ExternalContentReport - Done");

            return (checkList.Count > 0);
        }

        #region Worker Code

        private void CheckEntertainments(ref List<ReportItem> checkList)
        {
            var sort = new SortExpression(EntertainmenturlFields.EntertainmentId | SortOperator.Ascending);

            var relation = new RelationCollection(EntertainmenturlEntityBase.Relations.EntertainmentEntityUsingEntertainmentId);

            var prefetch = new PrefetchPath(EntityType.EntertainmenturlEntity);
            prefetch.Add(EntertainmenturlEntityBase.PrefetchPathEntertainmentEntity);

            var includeFields = new IncludeFieldsList();
            includeFields.Add(EntertainmenturlFields.EntertainmentId);
            includeFields.Add(EntertainmenturlFields.Url);
            includeFields.Add(EntertainmentFields.Name);

            var urlCollection = new EntertainmenturlCollection();
            urlCollection.GetMulti(null, 0, sort, relation, prefetch, includeFields, 0, 0);

            LoggingProvider.Verbose("ExternalContentReport - Entertainments UrlCollection size: {0}", urlCollection.Count);

            var collectedLinks = new List<ReportItem>();
            Parallel.ForEach(urlCollection,
                             new ParallelOptions { MaxDegreeOfParallelism = 4 },
                             () => new ConcurrentList<ReportItem>(),
                             (urlEntity, state, i, collection) =>
                             {
                                 if (urlEntity.Url.IsNotNullOrEmpty())
                                 {
                                     int statusCode;
                                     if (!UrlIsValid(urlEntity.Url, out statusCode))
                                     {
                                         collection.Add(new ReportItem("Entertainments", urlEntity.EntertainmentEntity.Name, urlEntity.EntertainmentId, urlEntity.Url, statusCode));
                                     }
                                 }
                                 else
                                 {
                                     collection.Add(new ReportItem("Entertainments", urlEntity.EntertainmentEntity.Name, urlEntity.EntertainmentId));
                                 }

                                 return collection;
                             },
                             collection =>
                             {
                                 lock (collectedLinks)
                                 {
                                     collectedLinks.AddRange(collection);
                                 }
                             });

            checkList.AddRange(collectedLinks);
        }

        private void CheckUITabs(ref List<ReportItem> checkList)
        {
            var filter = new PredicateExpression();
            filter.Add(UITabFields.Type == UITabType.Web);

            var sort = new SortExpression();
            sort.Add(CompanyFields.Name | SortOperator.Ascending);
            sort.Add(UIModeFields.Name | SortOperator.Ascending);

            var relation = new RelationCollection();
            relation.Add(UITabEntityBase.Relations.UIModeEntityUsingUIModeId);
            relation.Add(UIModeEntityBase.Relations.CompanyEntityUsingCompanyId);

            var prefetch = new PrefetchPath(EntityType.UITabEntity);
            prefetch.Add(UITabEntityBase.PrefetchPathUIModeEntity).SubPath.Add(UIModeEntityBase.PrefetchPathCompanyEntity);

            var includeFields = new IncludeFieldsList();
            includeFields.Add(UITabFields.Caption);
            includeFields.Add(UITabFields.UITabId);
            includeFields.Add(UITabFields.URL);
            includeFields.Add(UIModeFields.Name);
            includeFields.Add(CompanyFields.Name);

            var uiTabCollection = new UITabCollection();
            uiTabCollection.GetMulti(filter, 0, sort, relation, prefetch, includeFields, 0, 0);

            LoggingProvider.Verbose("ExternalContentReport - UITabs UrlCollection size: {0}", uiTabCollection.Count);

            var collectedLinks = new List<ReportItem>();
            Parallel.ForEach(uiTabCollection,
                             new ParallelOptions { MaxDegreeOfParallelism = 4 },
                             () => new ConcurrentList<ReportItem>(),
                             (uiTabEntity, state, i, collection) =>
                             {
                                 string name = string.Format("{0} / {1} / {2}",
                                                             uiTabEntity.UIModeEntity.CompanyEntity.Name,
                                                             uiTabEntity.UIModeEntity.Name,
                                                             uiTabEntity.Caption);

                                 if (uiTabEntity.URL.IsNotNullOrEmpty())
                                 {
                                     int statusCode;
                                     if (!UrlIsValid(uiTabEntity.URL, out statusCode))
                                     {
                                         collection.Add(new ReportItem("UITabs", name, uiTabEntity.UITabId, uiTabEntity.URL, statusCode));
                                     }
                                 }
                                 else
                                 {
                                     collection.Add(new ReportItem("UITabs", name, uiTabEntity.UITabId));
                                 }

                                 return collection;
                             },
                             collection =>
                             {
                                 lock (collectedLinks)
                                 {
                                     collectedLinks.AddRange(collection);
                                 }
                             });
            checkList.AddRange(collectedLinks);
        }

        private void CheckSitePages(ref List<ReportItem> checkList)
        {
            var filter = new PredicateExpression();
            filter.Add(PageFields.PageType == PageType.WebLink);
            filter.AddWithOr(PageFields.PageType == PageType.WebView);

            var attachmentFilter = new PredicateExpression();
            attachmentFilter.Add(AttachmentFields.Url != DBNull.Value);
            attachmentFilter.AddWithAnd(AttachmentFields.PageId > 0);
            filter.AddWithOr(attachmentFilter);

            var sort = new SortExpression();
            sort.Add(PageFields.SiteId | SortOperator.Ascending);
            sort.Add(PageFields.Name | SortOperator.Ascending);

            var relation = new RelationCollection();
            relation.Add(PageEntityBase.Relations.AttachmentEntityUsingPageId, JoinHint.Left);

            var prefetch = new PrefetchPath(EntityType.PageEntity);
            prefetch.Add(PageEntityBase.PrefetchPathPageElementCollection);

            var pageCollection = new PageCollection();
            pageCollection.GetMulti(filter, 0, sort, relation, prefetch);

            LoggingProvider.Verbose("ExternalContentReport - Site page UrlCollection size: {0}", pageCollection.Count);

            var collectedLinks = new List<ReportItem>();
            Parallel.ForEach(pageCollection,
                             new ParallelOptions { MaxDegreeOfParallelism = 4 },
                             () => new ConcurrentList<ReportItem>(),
                             (pageEntity, state, i, collection) =>
                             {
                                 CheckSitePageElements(pageEntity, ref collection);
                                 CheckSitePageAttachments(pageEntity, ref collection);

                                 return collection;
                             },
                             collection =>
                             {
                                 lock (collectedLinks)
                                 {
                                     collectedLinks.AddRange(collection);
                                 }
                             });

            checkList.AddRange(collectedLinks);
        }

        private void CheckAttachments(ref List<ReportItem> checkList)
        {

            PredicateExpression filter = new PredicateExpression
            {
                AttachmentFields.Type == AttachmentType.AffiliateBookOnline
            };

            var attachmentCollection = new AttachmentCollection();
            attachmentCollection.GetMulti(filter, 0, null, null, null);

            LoggingProvider.Verbose("ExternalContentReport - Site attachment collection size: {0}", attachmentCollection.Count);

            List<ReportItem> collectedLinks = attachmentCollection.AsParallel().Select(CheckAttachments).Where(x => x != null).ToList();

            checkList.AddRange(collectedLinks);
        }

        private void CheckSitePageElements(PageEntity pageEntity, ref ConcurrentList<ReportItem> collection)
        {
            foreach (var pageElementEntity in pageEntity.PageElementCollection.Where(e => e.PageElementTypeAsEnum == PageElementType.WebView))
            {
                if (pageElementEntity.StringValue1.IsNotNullOrEmpty())
                {
                    int statusCode;
                    if (!UrlIsValid(pageElementEntity.StringValue1, out statusCode))
                    {
                        var reportItem = new ReportItem("Site Pages", pageEntity.SiteNamePageName, pageEntity.PageId, pageElementEntity.StringValue1, statusCode);
                        collection.Add(reportItem);
                    }
                }
                else
                {
                    var reportItem = new ReportItem("Site Pages", pageEntity.SiteNamePageName, pageEntity.PageId);
                    collection.Add(reportItem);
                }
            }
        }        


        private void CheckSitePageAttachments(PageEntity pageEntity, ref ConcurrentList<ReportItem> collection)
        {
            foreach (var attachmentEntity in pageEntity.AttachmentCollection)
            {
                string name = string.Format("{0} / {1}", pageEntity.SiteNamePageName, attachmentEntity.Name);

                if (attachmentEntity.Url.IsNotNullOrEmpty())
                {
                    int statusCode;
                    if (!UrlIsValid(attachmentEntity.Url, out statusCode))
                    {
                        var reportItem = new ReportItem("Site Pages", name, pageEntity.PageId, attachmentEntity.Url, statusCode);
                        collection.Add(reportItem);
                    }
                }
                else
                {
                    var reportItem = new ReportItem("Site Pages", name, pageEntity.PageId);
                    collection.Add(reportItem);
                }
            }
        }

        private ReportItem CheckAttachments(AttachmentEntity attachmentEntity)
        {
            if (attachmentEntity.Url.IsNotNullOrEmpty())
            {
                int statusCode;
                string url = attachmentEntity.Url
                        .Replace("{language}'", "en")
                        .Replace("{campaign}", "campaign");                

                if (!UrlIsValid(url, out statusCode))
                {
                    (int id , string name) = GetAttachmentInfo(attachmentEntity);
                    
                    return new ReportItem("Attachments", name, id, url, statusCode);
                }
            }

            return null;
        }

        private (int,string) GetAttachmentInfo(AttachmentEntity attachmentEntity)
        {
            if(attachmentEntity.ProductId.HasValue)
            {
                return (attachmentEntity.ProductId ?? 0, attachmentEntity.ProductEntity.Name);
            }

            if (attachmentEntity.PageTemplateId.HasValue)
            {
                return (attachmentEntity.PageTemplateId ?? 0, attachmentEntity.PageTemplateEntity.Name);
            }

            if (attachmentEntity.PageId.HasValue)
            {
                return (attachmentEntity.PageId ?? 0, attachmentEntity.PageEntity.Name);
            }

            throw new NotImplementedException();
        }

        private void CheckAdvertisements(ref List<ReportItem> checkList)
        {
            var filter = new PredicateExpression();
            filter.Add(AdvertisementFields.ActionUrl != DBNull.Value);

            var advertisementCollection = new AdvertisementCollection();
            advertisementCollection.GetMulti(filter);

            LoggingProvider.Verbose("ExternalContentReport - Advertisements UrlCollection size: {0}", advertisementCollection.Count);

            var collectedLinks = new List<ReportItem>();
            Parallel.ForEach(advertisementCollection,
                             new ParallelOptions { MaxDegreeOfParallelism = 4 },
                             () => new ConcurrentList<ReportItem>(),
                             (advertisementEntity, state, i, collection) =>
                             {
                                 if (advertisementEntity.ActionUrl.Length > 0)
                                 {
                                     int statusCode;
                                     if (!UrlIsValid(advertisementEntity.ActionUrl, out statusCode))
                                     {
                                         collection.Add(new ReportItem("Advertisements", advertisementEntity.Name, advertisementEntity.AdvertisementId, advertisementEntity.ActionUrl, statusCode));
                                     }
                                 }
                                 else
                                 {
                                     collection.Add(new ReportItem("Advertisements", advertisementEntity.Name, advertisementEntity.AdvertisementId));
                                 }
                                 return collection;
                             },
                             collection =>
                             {
                                 lock (collectedLinks)
                                 {
                                     collectedLinks.AddRange(collection);
                                 }
                             });
            checkList.AddRange(collectedLinks);
        }

        private void CreateReport(SLDocument sl, List<ReportItem> checkList)
        {
            int row = 1;
            string lastCategory = string.Empty;

            foreach (var reportItem in checkList)
            {
                if (!reportItem.Catgory.Equals(lastCategory))
                {
                    if (!lastCategory.IsNullOrEmpty())
                        row++;

                    WriteHeader(sl, ref row, reportItem);
                    lastCategory = reportItem.Catgory;
                }

                sl.SetCellValue("A" + row, reportItem.Name);
                sl.SetCellValue("B" + row, reportItem.Id);
                sl.SetCellValue("C" + row, reportItem.StatusCode);
                sl.SetCellValue("D" + row, reportItem.Url);
                row++;
            }

            sl.SetColumnWidth("A", 60);
            sl.SetColumnWidth("B", 10);
            sl.SetColumnWidth("C", 10);
            sl.SetColumnWidth("D", 150);

            sl.SetColumnStyle(2, 3, HorizontalAlignmentValues.Center);
        }

        private void WriteHeader(SLDocument sl, ref int row, ReportItem reportItem)
        {
            sl.SetCellValue("A" + row, reportItem.Catgory);
            sl.SetStyle("A" + row, fontSize: 18, fontStyle: FontStyle.Bold);
            row++;

            sl.SetCellValue("A" + row, "Name");
            sl.SetCellValue("B" + row, "Id");
            sl.SetCellValue("C" + row, "Code");
            sl.SetCellValue("D" + row, "Url");
            sl.SetStyle("A" + row, "D" + row, fontStyle: FontStyle.Bold);
            sl.SetStyle("B" + row, "C" + row, HorizontalAlignmentValues.Center, fontStyle: FontStyle.Bold);
            row++;
        }

        #endregion

        /// <summary>
        /// This method will check a url to see that it does not return server or protocol errors
        /// </summary>
        /// <param name="url">The path to check</param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        private bool UrlIsValid(string url, out int statusCode)
        {
            bool toReturn = false;

            try
            {
                FixUrl(ref url);

                bool isNew = false;
                lock (checkedUrls)
                {
                    // 1. Check if URL is already (being) handled
                    if (!checkedUrls.TryGetValue(url, out statusCode))
                    {
                        isNew = true;
                        checkedUrls.Add(url, 0);
                    }
                }

                if (!isNew)
                {
                    // 2. If URL is already being handled by other thread, try get that result value
                    //      Waiting max 15 seconds
                    for (int count = 0; count < 15 && statusCode == 0; count++)
                    {
                        Thread.Sleep(1000);

                        lock (checkedUrls)
                        {
                            if (checkedUrls.TryGetValue(url, out statusCode))
                            {
                                break;
                            }
                        }
                    }
                }

                if (isNew || statusCode <= 0)
                {
                    // 3. Try fetch content for URL if it's new or previous attempt failed
                    var request = WebRequest.Create(url) as HttpWebRequest;
                    if (request != null)
                    {
                        request.Timeout = 15000;
                        request.Method = "GET";
                        request.AllowAutoRedirect = false;
                        request.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36";

                        HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                        if (response != null)
                        {
                            statusCode = (int)response.StatusCode;

                            lock (checkedUrls)
                            {
                                checkedUrls[url] = statusCode;
                            }

                            response.Close();
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                switch (ex.Status)
                {
                    case WebExceptionStatus.Success:
                        statusCode = 200;
                        break;
                    case WebExceptionStatus.ProtocolError:
                        statusCode = 404;
                        break;
                    case WebExceptionStatus.NameResolutionFailure:
                    case WebExceptionStatus.ConnectFailure:
                    case WebExceptionStatus.Timeout:
                        statusCode = 408;
                        break;
                    case WebExceptionStatus.ServerProtocolViolation:
                        statusCode = 505;
                        break;
                    default:
                        statusCode = (int)ex.Status;
                        break;
                }

                lock (checkedUrls)
                {
                    checkedUrls[url] = statusCode;
                }
            }
            catch (Exception ex)
            {
                LoggingProvider.Warning("UrlIsValid - Exception when processing url '{0}'. Message: {1}", url, ex.Message);
                statusCode = -1;

                lock (checkedUrls)
                {
                    checkedUrls[url] = statusCode;
                }
            }

            if (statusCode >= 100 && statusCode < 400) //Good requests
            {
                toReturn = true;
            }
            else if (statusCode > 0)
            {
                LoggingProvider.Warning("UrlIsValid - URL: {0}, StatusCode: {1}", url, statusCode);
            }

            return toReturn;
        }

        private void FixUrl(ref string input)
        {
            input = input.Trim();

            if (!input.StartsWith("http"))
                input = "http://" + input;
        }

        #region Internal Classes

        public class ReportItem
        {
            public string Catgory { get; set; }
            public string Name { get; set; }
            public int Id { get; set; }
            public string Url { get; set; }
            public int StatusCode { get; set; }

            public ReportItem(string category, string name, int id)
            {
                this.Catgory = category;
                this.Name = name;
                this.Id = id;
                this.Url = "No url specified";
                this.StatusCode = -9999;
            }

            public ReportItem(string category, string name, int id, string url, int statusCode)
            {
                this.Catgory = category;
                this.Name = name;
                this.Id = id;
                this.Url = url;
                this.StatusCode = statusCode;
            }
        }

        #endregion
    }
}
