﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using SD.LLBLGen.Pro.QuerySpec.SelfServicing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Dionysos;
using Dionysos.Data;
using SpreadsheetLight;
using System.Drawing;

namespace Obymobi.Logic.Analytics
{
    /// <summary>
    /// Summary description for DeliverypointgroupStatsTable
    /// </summary>
    public class DeliverypointgroupStatsTable : TypedStatsTable<DeliverypointgroupStatsTable.DeliverypointgroupStatsRow>
    {
        Filter filter;
        private const string OrderCountAlias = "Order Count";
        private const string OrderRevenueAlias = "Revenue";

        #region Data Table setup

        public DeliverypointgroupStatsTable(Filter filter)
        {
            // Initialize columns
            this.Columns.Add(new DataColumn(ColumnNames.DeliverypointgroupId, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.DeliverypointgroupName, typeof(string)));
            this.Columns.Add(new DataColumn(ColumnNames.DataType, typeof(DataType)));

            this.Columns.Add(new DataColumn(ColumnNames.IrtStandardQuantity, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.IrtRequestForServiceQuantity, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.IrtTotalQuantity, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.ByodStandardQuantity, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.ByodRequestforServiceQuantity, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.ByodTotalQuantity, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.TotalStandardQuantity, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.TotalRequestForServiceQuantity, typeof(int)));
            this.Columns.Add(new DataColumn(ColumnNames.TotalTotalQuantity, typeof(int)));

            this.Columns.Add(new DataColumn(ColumnNames.IrtStandardRevenue, typeof(decimal)));
            this.Columns.Add(new DataColumn(ColumnNames.IrtRequestForServiceRevenue, typeof(decimal)));
            this.Columns.Add(new DataColumn(ColumnNames.IrtTotalRevenue, typeof(decimal)));
            this.Columns.Add(new DataColumn(ColumnNames.ByodStandardRevenue, typeof(decimal)));
            this.Columns.Add(new DataColumn(ColumnNames.ByodRequestforServiceRevenue, typeof(decimal)));
            this.Columns.Add(new DataColumn(ColumnNames.ByodTotalRevenue, typeof(decimal)));
            this.Columns.Add(new DataColumn(ColumnNames.TotalStandardRevenue, typeof(decimal)));
            this.Columns.Add(new DataColumn(ColumnNames.TotalRequestForServiceRevenue, typeof(decimal)));
            this.Columns.Add(new DataColumn(ColumnNames.TotalTotalRevenue, typeof(decimal)));

            this.filter = filter;
        }

        protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
        {
            return new DeliverypointgroupStatsRow(builder);
        }

        #endregion

        #region Retrieval Methods

        public void FetchDeliverypointgroupBasedStatistics()
        {
            TypedListDAO dao = new TypedListDAO();
            DataTable byodOrderCounts, byodRevenues = new DataTable(), irtOrderCounts, irtRevenues = new DataTable();

            // Get stats for Byod
            // Order Counts
            DynamicQuery byodQuery = this.OrderCountPerDeliverypointgroupPerOrderTypeQuery();
            byodQuery.Where(OrderFields.MobileOrder == true);
            byodOrderCounts = dao.FetchAsDataTable(byodQuery);

            // Revenue
            string byodRevenueQuery = this.GetOrderRevenuePerDeliverypointgroupPerOrderTypeQuery(true);
            SqlQueryHelper.ExecuteRetrievalQuery(byodRevenues, byodRevenueQuery);

            // Get stats for Irt
            // Order Counts
            DynamicQuery irtQuery = this.OrderCountPerDeliverypointgroupPerOrderTypeQuery();
            irtQuery.Where(OrderFields.MobileOrder == false);
            irtOrderCounts = dao.FetchAsDataTable(irtQuery);

            // Revenue
            string irtRevenueQuery = this.GetOrderRevenuePerDeliverypointgroupPerOrderTypeQuery(false);
            SqlQueryHelper.ExecuteRetrievalQuery(irtRevenues, irtRevenueQuery);

            // Get all Deliverypointgroups as the basis (so that we also include those without any orders)
            var dpgs = new DeliverypointgroupCollection();
            var filterDpgs = new PredicateExpression(DeliverypointgroupFields.CompanyId == this.filter.CompanyId);

            if (this.filter.DeliverypointgroupIds != null && this.filter.DeliverypointgroupIds.Count > 0)
                filterDpgs.Add(DeliverypointgroupFields.DeliverypointgroupId == this.filter.DeliverypointgroupIds);

            var sortDpgs = new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending);                        
            dpgs.GetMulti(filterDpgs, 0, sortDpgs);

            List<OrderType> orderTypes = new List<OrderType> { OrderType.Standard, OrderType.RequestForService };
            // Get the stats for each Deliverypointgroup
            foreach (var dpg in dpgs)
            {
                // Prepare a row for Quantities 
                DeliverypointgroupStatsRow targetRow = this.NewRow();
                targetRow.DeliverypointgroupId = dpg.DeliverypointgroupId;
                targetRow.DeliverypointgroupName = dpg.Name;

                // Get the stats per OrderType
                foreach (var orderType in orderTypes)
                {
                    string orderTypeString = orderType.ToString();
                    string selectExpression = "[{0}] = {1} AND Type = {2}".FormatSafe(ColumnNames.DeliverypointgroupId, dpg.DeliverypointgroupId, orderType.ToIntString());

                    // Find Order Quantity Rows for Irt and Byod and set them
                    DataRow irtQuantitiesRow = irtOrderCounts.GetRow(selectExpression);
                    DataRow byodQuantitiesRow = byodOrderCounts.GetRow(selectExpression);
                                     
                    int irtOrderCount = irtQuantitiesRow != null ? (int)irtQuantitiesRow[DeliverypointgroupStatsTable.OrderCountAlias] : 0;
                    targetRow.SetIrtQuantity(orderType, irtOrderCount);

                    int byodOrderCount = byodQuantitiesRow != null ? (int)byodQuantitiesRow[DeliverypointgroupStatsTable.OrderCountAlias] : 0;
                    targetRow.SetByodQuantity(orderType, byodOrderCount);

                    // Find Revenue Rows for Irt and Byod and set them
                    DataRow irtRevenuesRow = irtRevenues.GetRow(selectExpression);
                    DataRow byodRevenuesRow = byodRevenues.GetRow(selectExpression);

                    decimal irtRevenue = irtRevenuesRow != null && irtRevenuesRow[DeliverypointgroupStatsTable.OrderRevenueAlias] != DBNull.Value ? (decimal)irtRevenuesRow[DeliverypointgroupStatsTable.OrderRevenueAlias] : 0;
                    targetRow.SetIrtRevenue(orderType, irtRevenue);

                    decimal byodRevenue = byodRevenuesRow != null && byodRevenuesRow[DeliverypointgroupStatsTable.OrderRevenueAlias] != DBNull.Value ? (decimal)byodRevenuesRow[DeliverypointgroupStatsTable.OrderRevenueAlias] : 0;
                    targetRow.SetByodRevenue(orderType, byodRevenue);
                }

                // Sum calculations
                targetRow.SetTotals();                

                // So easy to forget, as you would expect NewRow to also append it to the table... 
                this.Add(targetRow);                
            }

            return;
        }   

        #endregion

        #region Rendering Excel export

        public void AppendTableToSpreadsheet(SLDocument spreadsheet, bool includeOrderCount = true, bool includeRevenues = true)
        {
            // Add & select worksheet
            string worksheetName = "Deliverypointgroups";
            spreadsheet.AddWorksheet(worksheetName);
            spreadsheet.SelectWorksheet(worksheetName);

            Color headerColor = Color.FromArgb(216, 228, 188);
            Color revenueColor = Color.FromArgb(235, 241, 222);

            // Column A and B are used for Row Naming

            // Render High-level headings in merged cells
            spreadsheet.MergeWorksheetCells("C1", "E1"); // IRT
            spreadsheet.MergeWorksheetCells("F1", "H1"); // BYOD
            spreadsheet.MergeWorksheetCells("I1", "K1"); // TOTAL

            // Set Texts
            spreadsheet.SetCellValue("C1", "In-Room Tablet");
            spreadsheet.SetCellValue("F1", "Bring Your Own Device");
            spreadsheet.SetCellValue("I1", "Overall");

            // Render headings IRT
            spreadsheet.SetCellValue("C2", "Orders");
            spreadsheet.SetCellValue("D2", "Service Requests");
            spreadsheet.SetCellValue("E2", "Total");

            // Render headings BYOD
            spreadsheet.SetCellValue("F2", "Orders");
            spreadsheet.SetCellValue("G2", "Service Requests");
            spreadsheet.SetCellValue("H2", "Total");

            // Render headings Overall
            spreadsheet.SetCellValue("I2", "Orders");
            spreadsheet.SetCellValue("J2", "Service Requests");
            spreadsheet.SetCellValue("K2", "Total");
            spreadsheet.SetColumnWidth(3, 16);
            spreadsheet.SetColumnWidth(4, 16);
            spreadsheet.SetColumnWidth(5, 16);
            spreadsheet.SetColumnWidth(6, 16);
            spreadsheet.SetColumnWidth(7, 16);
            spreadsheet.SetColumnWidth(8, 16);
            spreadsheet.SetColumnWidth(9, 16);
            spreadsheet.SetColumnWidth(10, 16);
            spreadsheet.SetColumnWidth(11, 16);
            spreadsheet.SetStyle("C1", "K2", horizontalAlignment: DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center, backgroundColor: headerColor);

            // Render the Rows per Deliverypointgroup (which we expect to be ordered - since that's how the DataTable is filled)
            int currentRow = 3; // First two rows are the headers  
            decimal irtStandardQuantity = 0, irtRequestForServiceQuantity = 0, byodStandardQuantity = 0, byodRequestForServiceQuantity = 0;
            decimal irtStandardRevenue = 0, irtRequestForServiceRevenue = 0, byodStandardRevenue = 0, byodRequestForServiceRevenue = 0;
            List<int> rowsToGiveBottomBorder = new List<int>();            
            foreach (DataRow rowUntyped in this.Rows)
            {
                // We render 2 rows per Deliverypointgroup: Order Quantities and Reveneu
                DeliverypointgroupStatsRow row = (DeliverypointgroupStatsRow)rowUntyped;

                // Create a rowspan merge for the DPG name
                spreadsheet.MergeWorksheetCells("A" + currentRow, "A" + (currentRow + 1));
                spreadsheet.SetCellValue("A" + currentRow, row.DeliverypointgroupName);
                spreadsheet.SetStyle("A" + currentRow, verticalAlignment: DocumentFormat.OpenXml.Spreadsheet.VerticalAlignmentValues.Center, backgroundColor: headerColor);                
                rowsToGiveBottomBorder.Add(currentRow + 1);                

                // Render Order Quanties Row
                spreadsheet.SetCellValue("B" + currentRow, DataType.Quantity.ToString());
                spreadsheet.SetCellValue("C" + currentRow, row.IrtStandardQuantity);
                spreadsheet.SetCellValue("D" + currentRow, row.IrtRequestForServiceQuantity);
                spreadsheet.SetCellValue("E" + currentRow, row.IrtTotalQuantity);
                spreadsheet.SetCellValue("F" + currentRow, row.ByodStandardQuantity);
                spreadsheet.SetCellValue("G" + currentRow, row.ByodRequestforServiceQuantity);
                spreadsheet.SetCellValue("H" + currentRow, row.ByodTotalQuantity);
                spreadsheet.SetCellValue("I" + currentRow, row.TotalStandardQuantity);
                spreadsheet.SetCellValue("J" + currentRow, row.TotalRequestForServiceQuantity);
                spreadsheet.SetCellValue("K" + currentRow, row.TotalTotalQuantity);
                spreadsheet.SetStyle("B" + currentRow, "K" + currentRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green4, formatCode: SpreadsheetHelper.IntegerFormatCode);
                currentRow++;

                // Render Revenue Row
                spreadsheet.SetCellValue("B" + currentRow, DataType.Revenue.ToString());
                spreadsheet.SetCellValue("C" + currentRow, row.IrtStandardRevenue);
                spreadsheet.SetCellValue("D" + currentRow, row.IrtRequestForServiceRevenue);
                spreadsheet.SetCellValue("E" + currentRow, row.IrtTotalRevenue);
                spreadsheet.SetCellValue("F" + currentRow, row.ByodStandardRevenue);
                spreadsheet.SetCellValue("G" + currentRow, row.ByodRequestforServiceRevenue);
                spreadsheet.SetCellValue("H" + currentRow, row.ByodTotalRevenue);
                spreadsheet.SetCellValue("I" + currentRow, row.TotalStandardRevenue);
                spreadsheet.SetCellValue("J" + currentRow, row.TotalRequestForServiceRevenue);
                spreadsheet.SetCellValue("K" + currentRow, row.TotalTotalRevenue);
                spreadsheet.SetStyle("B" + currentRow, "K" + currentRow, borders: Borders.Bottom, formatCode: SpreadsheetHelper.DecimalFormatCode);
                currentRow++;

                // Update totals
                irtStandardQuantity += row.IrtStandardQuantity;
                irtStandardRevenue += row.IrtStandardRevenue;
                irtRequestForServiceQuantity += row.IrtRequestForServiceQuantity;
                irtRequestForServiceRevenue += row.IrtRequestForServiceRevenue;

                byodStandardQuantity += row.ByodStandardQuantity;
                byodStandardRevenue += row.ByodStandardRevenue;
                byodRequestForServiceQuantity += row.ByodRequestforServiceQuantity;
                byodRequestForServiceRevenue += row.ByodRequestforServiceRevenue;                          
                
            }

            // GK Failed for some reason
            // spreadsheet.AutoFitColumn("A");
            spreadsheet.SetColumnWidth("A", 30);

            // Render the total Rows (Quantity and Revenue)
            // Create a rowspan merge for the 'Total'-label
            spreadsheet.MergeWorksheetCells("A" + currentRow, "A" + (currentRow + 1));
            spreadsheet.SetCellValue("A" + currentRow, "Total");
            spreadsheet.SetStyle("A" + currentRow, verticalAlignment: DocumentFormat.OpenXml.Spreadsheet.VerticalAlignmentValues.Center, backgroundColor: headerColor);

            // Quantity Row
            spreadsheet.SetCellValue("B" + currentRow, DataType.Quantity.ToString());
            spreadsheet.SetCellValue("C" + currentRow, irtStandardQuantity);
            spreadsheet.SetCellValue("D" + currentRow, irtRequestForServiceQuantity);
            spreadsheet.SetCellValue("E" + currentRow, irtStandardQuantity + irtRequestForServiceQuantity);
            spreadsheet.SetCellValue("F" + currentRow, byodStandardQuantity);
            spreadsheet.SetCellValue("G" + currentRow, byodRequestForServiceQuantity);
            spreadsheet.SetCellValue("H" + currentRow, byodStandardQuantity + byodRequestForServiceQuantity);
            spreadsheet.SetCellValue("I" + currentRow, irtStandardQuantity + byodStandardQuantity);
            spreadsheet.SetCellValue("J" + currentRow, irtRequestForServiceQuantity + byodRequestForServiceQuantity);
            spreadsheet.SetCellValue("K" + currentRow, irtStandardQuantity + byodStandardQuantity + irtRequestForServiceQuantity + byodRequestForServiceQuantity);
            currentRow++;

            // Revenue Row
            spreadsheet.SetCellValue("B" + currentRow, DataType.Revenue.ToString());
            spreadsheet.SetCellValue("C" + currentRow, irtStandardRevenue);
            spreadsheet.SetCellValue("D" + currentRow, irtRequestForServiceRevenue);
            spreadsheet.SetCellValue("E" + currentRow, irtStandardRevenue + irtRequestForServiceRevenue);
            spreadsheet.SetCellValue("F" + currentRow, byodStandardRevenue);
            spreadsheet.SetCellValue("G" + currentRow, byodRequestForServiceRevenue);
            spreadsheet.SetCellValue("H" + currentRow, byodStandardRevenue + byodRequestForServiceRevenue);
            spreadsheet.SetCellValue("I" + currentRow, irtStandardRevenue + byodStandardRevenue);
            spreadsheet.SetCellValue("J" + currentRow, irtRequestForServiceRevenue + byodRequestForServiceRevenue);
            spreadsheet.SetCellValue("K" + currentRow, irtStandardRevenue + byodStandardRevenue + irtRequestForServiceRevenue + byodRequestForServiceRevenue);
            spreadsheet.SetStyle("B" + currentRow, "K" + currentRow, borders: Borders.Bottom, formatCode: SpreadsheetHelper.DecimalFormatCode);

            // Set Borders (at the end, since otherwise autofit column won't work)
            spreadsheet.SetBorder(Color.Black, Borders.Top, 1, 11, 1, 1);
            spreadsheet.SetBorder(Color.Black, Borders.Bottom, 1, 11, 2, 2);
            spreadsheet.SetBorder(Color.Black, Borders.Left, 1, 1, 1, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 2, 2, 1, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 3, 3, 3, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 4, 4, 3, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 5, 5, 1, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 6, 6, 3, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 7, 7, 3, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 8, 8, 1, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 9, 9, 3, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 10, 10, 3, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 11, 11, 1, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Bottom, 2, 11, currentRow, currentRow);
            rowsToGiveBottomBorder.Add(currentRow);

            // Update all borders in one iteration
            foreach (int row in rowsToGiveBottomBorder) spreadsheet.SetBorder(Color.Black, Borders.Bottom, 1, 1, row, row);

            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 1);
            pageSettings.Orientation = DocumentFormat.OpenXml.Spreadsheet.OrientationValues.Landscape;
            spreadsheet.SetPageSettings(pageSettings);

            // Top left cell, remove borders
            SLStyle style = spreadsheet.GetCellStyle("A1");
            style.Border.LeftBorder.BorderStyle = DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.None;
            style.Border.TopBorder.BorderStyle = DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.None;
            spreadsheet.SetCellStyle("A1", style);
            style = spreadsheet.GetCellStyle("B1");
            style.Border.TopBorder.BorderStyle = DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.None;
            spreadsheet.SetCellStyle("B1", style);
            style = spreadsheet.GetCellStyle("A2");
            style.Border.LeftBorder.BorderStyle = DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.None;
            spreadsheet.SetCellStyle("A2", style);

        }

        public void AppendToSummaryWorksheet(SLDocument spreadsheet, string worksheetName, ref int rowIndex)
        { 
            spreadsheet.SelectWorksheet(worksheetName);

            // Normal Headings
            spreadsheet.SetCellValue("A" + rowIndex, "Deliverypointgroup");
            spreadsheet.SetCellValue("B" + rowIndex, "Orders");
            spreadsheet.SetCellValue("c" + rowIndex, "Revenue");
            spreadsheet.SetCellValue("F" + rowIndex, "Deliverypointgroup");
            spreadsheet.SetCellValue("G" + rowIndex, "Orders");
            spreadsheet.SetCellValue("H" + rowIndex, "Revenue");
            spreadsheet.SetStyle("A" + rowIndex, "C" + rowIndex, backgroundColor: SpreadsheetHelper.Heading1);
            spreadsheet.SetStyle("F" + rowIndex, "H" + rowIndex, backgroundColor: SpreadsheetHelper.Heading1);
            rowIndex++;
 
            // Render per deliverypointgroup
            int totalOrderCount = 0, totalRequestCount = 0;
            decimal totalOrderRevenue = 0, totalRequestRevenue = 0;
            foreach (DataRow rowUntyped in this.Rows)
            {                
                DeliverypointgroupStatsRow row = (DeliverypointgroupStatsRow)rowUntyped;
             
                // Products / Orders                
                spreadsheet.SetCellValue("A" + rowIndex, row.DeliverypointgroupName);
                spreadsheet.SetCellValue("B" + rowIndex, row.TotalStandardQuantity);
                spreadsheet.SetCellValue("C" + rowIndex, row.TotalStandardRevenue);
                spreadsheet.SetStyle("B" + rowIndex, formatCode: SpreadsheetHelper.IntegerFormatCode);
                spreadsheet.SetStyle("C" + rowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode);                
                totalOrderCount += row.TotalStandardQuantity;
                totalOrderRevenue += row.TotalStandardRevenue;
                spreadsheet.SetStyle("A" + rowIndex, "C" + rowIndex, borders: Borders.Bottom, borderColor: SpreadsheetHelper.DividingBorderColor);

                // Service Requests
                spreadsheet.SetCellValue("F" + rowIndex, row.DeliverypointgroupName);
                spreadsheet.SetCellValue("G" + rowIndex, row.TotalRequestForServiceQuantity);
                spreadsheet.SetCellValue("H" + rowIndex, row.TotalRequestForServiceRevenue);
                spreadsheet.SetStyle("G" + rowIndex, formatCode: SpreadsheetHelper.IntegerFormatCode);
                spreadsheet.SetStyle("H" + rowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode);                
                totalRequestCount += row.TotalRequestForServiceQuantity;
                totalRequestRevenue += row.TotalRequestForServiceRevenue;

                // Border bottom
                spreadsheet.SetStyle("F" + rowIndex, "H" + rowIndex, borders: Borders.Bottom, borderColor: SpreadsheetHelper.DividingBorderColor);

                rowIndex++;
            }

            // Render totals
            // Products / Orders                
            //spreadsheet.SetCellValue("A" + rowIndex, "Total");
            spreadsheet.SetCellValue("B" + rowIndex, totalOrderCount);
            spreadsheet.SetCellValue("c" + rowIndex, totalOrderRevenue);
            spreadsheet.SetStyle("B" + rowIndex, formatCode: SpreadsheetHelper.IntegerFormatCode, borders: Borders.Top);
            spreadsheet.SetStyle("C" + rowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode, borders: Borders.Top);

            // Service Requests
            //spreadsheet.SetCellValue("F" + rowIndex, "Total");
            spreadsheet.SetCellValue("G" + rowIndex, totalRequestCount);
            spreadsheet.SetCellValue("H" + rowIndex, totalRequestRevenue);
            spreadsheet.SetStyle("G" + rowIndex, formatCode: SpreadsheetHelper.IntegerFormatCode, borders: Borders.Top);
            spreadsheet.SetStyle("H" + rowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode, borders: Borders.Top);

            rowIndex++;             

            // Spacing
            rowIndex++;             
        }

        #endregion

        #region Queries

        private DynamicQuery OrderCountPerDeliverypointgroupPerOrderTypeQuery()
        {
            var queryFactory = new QueryFactory();
            DynamicQuery query = queryFactory.Create()
                .Select(DeliverypointgroupFields.DeliverypointgroupId.As(ColumnNames.DeliverypointgroupId), DeliverypointgroupFields.Name.As(ColumnNames.DeliverypointgroupName), OrderFields.Type, Functions.CountRow().As(DeliverypointgroupStatsTable.OrderCountAlias))
                .From(queryFactory.Order
                        .LeftJoin(OrderEntity.Relations.DeliverypointEntityUsingDeliverypointId)
                        .LeftJoin(DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId)
                        .LeftJoin(OrderEntity.Relations.CompanyEntityUsingCompanyId))
                .GroupBy(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupFields.Name, OrderFields.Type)
                .OrderBy(DeliverypointgroupFields.Name.Ascending(), OrderFields.Type.Ascending());

            if (this.filter != null)
            {
                query.Where(OrderFields.CreatedUTC >= this.filter.FromUtc);
                query.Where(OrderFields.CreatedUTC <= this.filter.TillUtc);
                query.Where(OrderFields.CompanyId == this.filter.CompanyId);

                if (this.filter.DeliverypointIds != null && this.filter.DeliverypointIds.Count > 0)
                {
                    query.Where(OrderFields.DeliverypointId == this.filter.DeliverypointIds);
                }

                if (this.filter.DeliverypointgroupIds != null && this.filter.DeliverypointgroupIds.Count > 0)
                {
                    query.Where(DeliverypointFields.DeliverypointgroupId == this.filter.DeliverypointgroupIds);
                }

                if (!this.filter.IncludeFailedOrders)
                {
                    query.Where(OrderFields.ErrorCode == 0);
                }
            }

            return query;
        }

        private string GetOrderRevenuePerDeliverypointgroupPerOrderTypeQuery(bool mobileOrders)
        {
            string sqlQuery = @"
                    SELECT 
	                    [Deliverypointgroup Id], 
	                    [Deliverypointgroup Name], 
	                    [Type], 
	                    SUM([Revenue]) as [Revenue],
	                    SUM([Quantity]) as [Quantity]
                    FROM
	                    (
		                    SELECT 
			                    [Deliverypointgroup].[DeliverypointgroupId] as [Deliverypointgroup Id],
			                    [Deliverypointgroup].[Name] as [Deliverypointgroup Name],
			                    [Order].[Type] as [Type],			
			                    [Orderitem].Quantity as [Quantity],
			                    (
				                    [Orderitem].[Quantity] * 
				                    (
					                    [Orderitem].[ProductPriceIn] + 
						                    ISNULL((
							                    SELECT 
								                    SUM([OrderitemAlterationitem].[AlterationoptionPriceIn]) AS [AlterationoptionPriceIn] 
							                    FROM 
								                    [OrderitemAlterationitem]  
							                    WHERE 
								                    ( 
									                    ( 
										                    [OrderitemAlterationitem].[OrderitemId] = [Orderitem].[OrderitemId]
									                    )
								                    )
							                    ),0)
						                    )
				                    ) 
			                    AS [Revenue] 
		                    FROM 
			                    [Orderitem] 
		                    LEFT JOIN [Order] on [Order].OrderId = [Orderitem].[OrderId]
		                    LEFT JOIN [Deliverypoint] on [Order].DeliverypointId = [Deliverypoint].[DeliverypointId]
		                    LEFT JOIN [Deliverypointgroup] on [Deliverypoint].[DeliverypointgroupId] = [Deliverypointgroup].[DeliverypointgroupId]
		                    WHERE 
			                    [[WHERE]]
	                    ) AS oiQuery
                    GROUP BY
	                    [Deliverypointgroup Id], 
	                    [Deliverypointgroup Name], 
	                    [Type]
                    ORDER BY 
	                    [Deliverypointgroup Id] DESC               
            ";

            List<string> whereClauses = SqlQueryHelper.GetDefaultWhereClause(this.filter);

            // Order Type
            if (mobileOrders)
            {
                whereClauses.Add("[Order].[MobileOrder] = 1");
            }
            else
            {
                whereClauses.Add("[Order].[MobileOrder] = 0");
            }

            sqlQuery = sqlQuery.Replace("[[WHERE]]", string.Join(" AND ", whereClauses));

            return sqlQuery;
        }

        private DynamicQuery OrderDetailsPerDeliverypointgroupPerOrderTypeQuery()
        {
            var queryFactory = new QueryFactory();
            DynamicQuery query = queryFactory.Create()
                .Select(DeliverypointgroupFields.DeliverypointgroupId.As(ColumnNames.DeliverypointgroupId), DeliverypointgroupFields.Name.As(ColumnNames.DeliverypointgroupName),
                        OrderFields.Type,
                        (OrderitemFields.ProductPriceIn * OrderitemFields.Quantity).Sum().As(DeliverypointgroupStatsTable.OrderRevenueAlias),
                        OrderitemFields.Quantity.Sum())
                .From(queryFactory.Orderitem
                    .LeftJoin(OrderitemEntity.Relations.OrderEntityUsingOrderId)
                    .LeftJoin(OrderEntity.Relations.DeliverypointEntityUsingDeliverypointId)
                    .LeftJoin(OrderEntity.Relations.CompanyEntityUsingCompanyId)
                    .LeftJoin(DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId))
                .GroupBy(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupFields.Name, OrderFields.Type)
                .OrderBy(DeliverypointgroupFields.Name.Ascending(), OrderFields.Type.Ascending());

            if (filter != null)
            {
                query.Where(OrderFields.CreatedUTC >= this.filter.FromUtc);
                query.Where(OrderFields.CreatedUTC <= this.filter.TillUtc);
                query.Where(OrderFields.CompanyId == this.filter.CompanyId);

                if (this.filter.DeliverypointIds != null && this.filter.DeliverypointIds.Count > 0)
                {
                    query.Where(OrderFields.DeliverypointId == this.filter.DeliverypointIds);
                }

                if (this.filter.DeliverypointgroupIds != null && this.filter.DeliverypointgroupIds.Count > 0)
                {
                    query.Where(DeliverypointFields.DeliverypointgroupId == this.filter.DeliverypointgroupIds);
                }

                if (!this.filter.IncludeFailedOrders)
                {
                    query.Where(OrderFields.ErrorCode == 0);
                }
            }

            return query;
        }

        #endregion

        #region Column Name definitions

        internal class ColumnNames
        {
            internal const string DeliverypointgroupId = "Deliverypointgroup Id";
            internal const string DeliverypointgroupName = "Deliverypointgroup Name";
            internal const string DataType = "Data Type";

            internal const string IrtStandardQuantity = "IRT - Standard Quantity";
            internal const string IrtRequestForServiceQuantity = "IRT - RequestForService Quantity";
            internal const string IrtTotalQuantity = "IRT - Total Quantity";
            internal const string ByodStandardQuantity = "BYOD - Standard Quantity";
            internal const string ByodRequestforServiceQuantity = "BYOD - RequestForService Quantity";
            internal const string ByodTotalQuantity = "BYOD - Total Quantity";
            internal const string TotalStandardQuantity = "Total - Standard Quantity";
            internal const string TotalRequestForServiceQuantity = "Total - RequestForService Quantity";
            internal const string TotalTotalQuantity = "Total - Total Quantity";

            internal const string IrtStandardRevenue = "IRT - Standard Revenue";
            internal const string IrtRequestForServiceRevenue = "IRT - RequestForService Revenue";
            internal const string IrtTotalRevenue = "IRT - Total Revenue";
            internal const string ByodStandardRevenue = "BYOD - Standard Revenue";
            internal const string ByodRequestforServiceRevenue = "BYOD - RequestForService Revenue";
            internal const string ByodTotalRevenue = "BYOD - Total Revenue";
            internal const string TotalStandardRevenue = "Total - Standard Revenue";
            internal const string TotalRequestForServiceRevenue = "Total - RequestForService Revenue";
            internal const string TotalTotalRevenue = "Total - Total Revenue";
        }

        public enum DataType
        {
            Quantity,
            Revenue
        }

        #endregion

        #region Typed Data Row class

        public class DeliverypointgroupStatsRow : DataRow
        {
            // GK All totals have been defined as DataTable columns instead of properties that just return 
            // the sum of Standard+RequestForService to keep it a simple data table structure to be used everywhere.
            internal DeliverypointgroupStatsRow(DataRowBuilder builder)
                : base(builder)
            {
            }

            public int DeliverypointgroupId
            {
                get { return (int)base[ColumnNames.DeliverypointgroupId]; }
                set { base[ColumnNames.DeliverypointgroupId] = value; }
            }

            public string DeliverypointgroupName
            {
                get { return (string)base[ColumnNames.DeliverypointgroupName]; }
                set { base[ColumnNames.DeliverypointgroupName] = value; }
            }

            public DataType DataType
            {
                get { return (DataType)base[ColumnNames.DataType]; }
                set { base[ColumnNames.DataType] = value; }
            }

            public int IrtStandardQuantity
            {
                get { return (int)base[ColumnNames.IrtStandardQuantity]; }
                set { base[ColumnNames.IrtStandardQuantity] = value; }
            }

            public int IrtRequestForServiceQuantity
            {
                get { return (int)base[ColumnNames.IrtRequestForServiceQuantity]; }
                set { base[ColumnNames.IrtRequestForServiceQuantity] = value; }
            }

            public int IrtTotalQuantity
            {
                get { return (int)base[ColumnNames.IrtTotalQuantity]; }
                set { base[ColumnNames.IrtTotalQuantity] = value; }
            }

            public int ByodStandardQuantity
            {
                get { return (int)base[ColumnNames.ByodStandardQuantity]; }
                set { base[ColumnNames.ByodStandardQuantity] = value; }
            }

            public int ByodRequestforServiceQuantity
            {
                get { return (int)base[ColumnNames.ByodRequestforServiceQuantity]; }
                set { base[ColumnNames.ByodRequestforServiceQuantity] = value; }
            }

            public int ByodTotalQuantity
            {
                get { return (int)base[ColumnNames.ByodTotalQuantity]; }
                set { base[ColumnNames.ByodTotalQuantity] = value; }
            }

            public int TotalStandardQuantity
            {
                get { return (int)base[ColumnNames.TotalStandardQuantity]; }
                set { base[ColumnNames.TotalStandardQuantity] = value; }
            }

            public int TotalRequestForServiceQuantity
            {
                get { return (int)base[ColumnNames.TotalRequestForServiceQuantity]; }
                set { base[ColumnNames.TotalRequestForServiceQuantity] = value; }
            }

            public int TotalTotalQuantity
            {
                get { return (int)base[ColumnNames.TotalTotalQuantity]; }
                set { base[ColumnNames.TotalTotalQuantity] = value; }
            }

            public decimal IrtStandardRevenue
            {
                get { return (decimal)base[ColumnNames.IrtStandardRevenue]; }
                set { base[ColumnNames.IrtStandardRevenue] = value; }
            }

            public decimal IrtRequestForServiceRevenue
            {
                get { return (decimal)base[ColumnNames.IrtRequestForServiceRevenue]; }
                set { base[ColumnNames.IrtRequestForServiceRevenue] = value; }
            }

            public decimal IrtTotalRevenue
            {
                get { return (decimal)base[ColumnNames.IrtTotalRevenue]; }
                set { base[ColumnNames.IrtTotalRevenue] = value; }
            }

            public decimal ByodStandardRevenue
            {
                get { return (decimal)base[ColumnNames.ByodStandardRevenue]; }
                set { base[ColumnNames.ByodStandardRevenue] = value; }
            }

            public decimal ByodRequestforServiceRevenue
            {
                get { return (decimal)base[ColumnNames.ByodRequestforServiceRevenue]; }
                set { base[ColumnNames.ByodRequestforServiceRevenue] = value; }
            }

            public decimal ByodTotalRevenue
            {
                get { return (decimal)base[ColumnNames.ByodTotalRevenue]; }
                set { base[ColumnNames.ByodTotalRevenue] = value; }
            }

            public decimal TotalStandardRevenue
            {
                get { return (decimal)base[ColumnNames.TotalStandardRevenue]; }
                set { base[ColumnNames.TotalStandardRevenue] = value; }
            }

            public decimal TotalRequestForServiceRevenue
            {
                get { return (decimal)base[ColumnNames.TotalRequestForServiceRevenue]; }
                set { base[ColumnNames.TotalRequestForServiceRevenue] = value; }
            }

            public decimal TotalTotalRevenue
            {
                get { return (decimal)base[ColumnNames.TotalTotalRevenue]; }
                set { base[ColumnNames.TotalTotalRevenue] = value; }
            }

            public void SetIrtQuantity(OrderType orderType, int value)
            {
                switch (orderType)
                {
                    case OrderType.Standard:
                        this.IrtStandardQuantity = value;
                        break;
                    case OrderType.RequestForService:
                        this.IrtRequestForServiceQuantity = value;
                        break;
                    default:
                        throw new NotImplementedException("DeliverypointgroupStatsRow.SetIrtValue: " + orderType);
                }
            }

            public void SetByodQuantity(OrderType orderType, int value)
            {
                switch (orderType)
                {
                    case OrderType.Standard:
                        this.ByodStandardQuantity = value;
                        break;
                    case OrderType.RequestForService:
                        this.ByodRequestforServiceQuantity = value;
                        break;
                    default:
                        throw new NotImplementedException("DeliverypointgroupStatsRow.SetByodValue: " + orderType);
                }
            }

            public void SetIrtRevenue(OrderType orderType, decimal value)
            {
                switch (orderType)
                {
                    case OrderType.Standard:
                        this.IrtStandardRevenue = value;
                        break;
                    case OrderType.RequestForService:
                        this.IrtRequestForServiceRevenue = value;
                        break;
                    default:
                        throw new NotImplementedException("DeliverypointgroupStatsRow.SetIrtValue: " + orderType);
                }
            }

            public void SetByodRevenue(OrderType orderType, decimal value)
            {
                switch (orderType)
                {
                    case OrderType.Standard:
                        this.ByodStandardRevenue = value;
                        break;
                    case OrderType.RequestForService:
                        this.ByodRequestforServiceRevenue = value;
                        break;
                    default:
                        throw new NotImplementedException("DeliverypointgroupStatsRow.SetByodValue: " + orderType);
                }
            }

            public void SetTotals()
            {
                this.IrtTotalQuantity = this.IrtStandardQuantity + this.IrtRequestForServiceQuantity;
                this.ByodTotalQuantity = this.ByodStandardQuantity + this.ByodRequestforServiceQuantity;
                this.TotalStandardQuantity = this.IrtStandardQuantity + this.ByodStandardQuantity;
                this.TotalRequestForServiceQuantity = this.IrtRequestForServiceQuantity + this.ByodRequestforServiceQuantity;
                this.TotalTotalQuantity = this.TotalStandardQuantity + this.TotalRequestForServiceQuantity;

                this.IrtTotalRevenue = this.IrtStandardRevenue + this.IrtRequestForServiceRevenue;
                this.ByodTotalRevenue = this.ByodStandardRevenue + this.ByodRequestforServiceRevenue;
                this.TotalStandardRevenue = this.IrtStandardRevenue + this.ByodStandardRevenue;
                this.TotalRequestForServiceRevenue = this.IrtRequestForServiceRevenue + this.ByodRequestforServiceRevenue;
                this.TotalTotalRevenue = this.TotalStandardRevenue + this.TotalRequestForServiceRevenue;
            }

        }

        #endregion
    }
}