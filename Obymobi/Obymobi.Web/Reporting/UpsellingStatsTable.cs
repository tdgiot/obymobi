﻿using System.Collections.Generic;
using System.Drawing;
using Dionysos;
using DocumentFormat.OpenXml.Spreadsheet;
using Google.Apis.Manual.Analytics.v3.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.FactoryClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using System;
using System.Data;
using Obymobi.Web.Google;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using SD.LLBLGen.Pro.QuerySpec.SelfServicing;
using SpreadsheetLight;
using Math = System.Math;

namespace Obymobi.Logic.Analytics
{
    /// <summary>
    /// Summary description for UpsellingStatsTable
    /// </summary>
    public class UpsellingStatsTable : TypedStatsTable<UpsellingStatsTable.UpsellingStatsRow>
    {
        private const string ANALYTICS_DIMENSION = "ga:eventAction,ga:eventLabel,ga:dimension11,ga:dimension14";
        private const string ANALYTICS_METRIC = "ga:totalEvents";
        private const string ANALYTICS_FILTER = "ga:eventCategory=={0};ga:dimension14=@[C-{1}]";

        readonly Filter filter;
        private readonly GoogleAPI api;

        private readonly OrderSourceType[] upsellTypes =
    {
        OrderSourceType.Suggestion,
        OrderSourceType.Advertisement,
        OrderSourceType.AdvertisementHomepage,
        OrderSourceType.AdvertisementHomepageSlideshow,
        OrderSourceType.AdvertisementMessage
    };

        private int totalProductsUpselling;
        private int totalMessagesSend;

        readonly Dictionary<OrderSourceType, int> upsellingNumbers = new Dictionary<OrderSourceType, int>();
        readonly DataTable companyAdvertisements = new DataTable();
        readonly DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();
        readonly OrderCollection orderCollection = new OrderCollection();

        #region Ctor

        public UpsellingStatsTable(Filter f, GoogleAPI a)
        {
            this.filter = f;
            this.api = a;

            this.Columns.Add(ColumnNames.ADVERTISEMENT_ID, typeof(int));
            this.Columns.Add(ColumnNames.ADVERTISEMENT_NAME, typeof(string));
            this.Columns.Add(ColumnNames.DELIVERYPOINTGROUP_ID, typeof(int));
            this.Columns.Add(ColumnNames.DELIVERYPOINTGROUP_NAME, typeof(string));
            this.Columns.Add(ColumnNames.PRODUCT_ID, typeof(int));
            this.Columns.Add(ColumnNames.PRODUCT_NAME, typeof(string));
            this.Columns.Add(ColumnNames.ORDER_SOURCE, typeof(int));
            this.Columns.Add(ColumnNames.REVENUE, typeof(double));
            this.Columns.Add(ColumnNames.QUANTITY, typeof(int));

            companyAdvertisements.Columns.Add(ColumnNames.ADVERTISEMENT_ID, typeof(int));
            companyAdvertisements.Columns.Add(ColumnNames.MEDIA_ID, typeof(int));
            companyAdvertisements.Columns.Add(ColumnNames.ADVERTISEMENT_NAME, typeof(string));
            companyAdvertisements.Columns.Add(ColumnNames.DELIVERYPOINTGROUP_ID, typeof(int));
            companyAdvertisements.Columns.Add(ColumnNames.CLICKS, typeof(int));
            companyAdvertisements.Columns.Add(ColumnNames.VIEWS, typeof(int));
        }

        protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
        {
            return new UpsellingStatsRow(builder);
        }

        #endregion

        #region Queries

        public void LoadData(int totalMessaging)
        {
            this.totalMessagesSend = totalMessaging;

            FetchDataForCompany();

            FetchAllNonRegularOrderedProducts();
            FetchAdvertisementsForCompany();
            FetchAnalyticsAdvertisementData();

            PreProcessData();
        }

        private void FetchDataForCompany()
        {
            // Deliverypointgroups
            var dpgFilter = new PredicateExpression();
            dpgFilter.Add(DeliverypointgroupFields.CompanyId == this.filter.CompanyId);
            if (this.filter.DeliverypointgroupIds != null && this.filter.DeliverypointgroupIds.Count > 0)
            {
                dpgFilter.Add(DeliverypointgroupFields.DeliverypointgroupId == this.filter.DeliverypointgroupIds);
            }
            deliverypointgroupCollection.GetMulti(dpgFilter);

            // Orders
            var orderFilter = new PredicateExpression();
            orderFilter.Add(OrderFields.CompanyId == this.filter.CompanyId);
            orderFilter.AddWithAnd(OrderFields.CreatedUTC >= this.filter.FromUtc);
            orderFilter.AddWithAnd(OrderFields.CreatedUTC <= this.filter.TillUtc);

            if (this.filter.DeliverypointgroupIds != null && this.filter.DeliverypointgroupIds.Count > 0)
            {
                orderFilter.Add(DeliverypointgroupFields.DeliverypointgroupId == this.filter.DeliverypointgroupIds);
            }

            var relations = new RelationCollection();
            relations.Add(OrderEntityBase.Relations.CompanyEntityUsingCompanyId);
            relations.Add(CompanyEntityBase.Relations.DeliverypointgroupEntityUsingCompanyId);

            orderCollection.GetMulti(orderFilter, relations);
        }

        private void FetchAllNonRegularOrderedProducts()
        {
            string sqlQuery = @"SELECT 
	                            [Advertisement Id], 
	                            [Advertisement Name], 
	                            [Deliverypointgroup Id], 
	                            [Deliverypointgroup Name], 
	                            [Product Id],
	                            [Product Name],
	                            [OrderSource], 
	                            SUM([Revenue]) as [Revenue],
	                            SUM([Quantity]) as [Quantity]
                            FROM
	                            (
		                            SELECT 
			                            COALESCE([Advertisement].AdvertisementId, ([Media].MediaId*-1), 0) as [Advertisement Id],
			                            COALESCE([Advertisement].Name, [Media].Name, '') AS [Advertisement Name],
			                            [Deliverypointgroup].DeliverypointgroupId as [Deliverypointgroup Id],
			                            [Deliverypointgroup].[Name] as [Deliverypointgroup Name],
			                            [Product].ProductId as [Product Id],
			                            [Product].[Name] as [Product Name],
			                            [Orderitem].OrderSource as [OrderSource],			
			                            [Orderitem].Quantity as [Quantity],
			                            (
				                            [Orderitem].[Quantity] * 
				                            (
					                            [Orderitem].[ProductPriceIn] + 
						                            ISNULL((
							                            SELECT 
								                            SUM([OrderitemAlterationitem].[AlterationoptionPriceIn]) AS [AlterationoptionPriceIn] 
							                            FROM 
								                            [OrderitemAlterationitem]  
							                            WHERE 
								                            ( 
									                            ( 
										                            [OrderitemAlterationitem].[OrderitemId] = [Orderitem].[OrderitemId]
									                            )
								                            )
							                            ),0)
						                            )
				                            ) 
			                            AS [Revenue] 
		                            FROM 
			                            [Orderitem] 
		                            LEFT JOIN [Order] on [Order].OrderId = [Orderitem].[OrderId]
		                            LEFT JOIN [Deliverypoint] ON [Deliverypoint].DeliverypointId = [Order].DeliverypointId
                                    LEFT JOIN [Deliverypointgroup] ON [Deliverypointgroup].DeliverypointgroupId = [Deliverypoint].DeliverypointgroupId
		                            LEFT JOIN [Advertisement] on [Advertisement].ProductId = [Orderitem].ProductId
		                            LEFT JOIN [Product] ON [Product].ProductId = [Orderitem].ProductId
                                    LEFT JOIN [Media] ON [Media].ActionProductId = [Orderitem].ProductId
		                            WHERE 
			                            [Orderitem].OrderSource > 1 AND [[WHERE]]
	                            ) AS oiQuery
                            GROUP BY
	                            [Advertisement Id], 
	                            [Advertisement Name], 
	                            [Deliverypointgroup Id], 
	                            [Deliverypointgroup Name], 
	                            [Product Id],
	                            [Product Name],
	                            [OrderSource]
                            ORDER BY 
	                            [Quantity] DESC";

            List<string> whereClauses = SqlQueryHelper.GetDefaultWhereClause(this.filter);
            sqlQuery = sqlQuery.Replace("[[WHERE]]", string.Join(" AND ", whereClauses));

            var dtResults = new DataTable();
            SqlQueryHelper.ExecuteRetrievalQuery(dtResults, sqlQuery);

            foreach (DataRow dtRow in dtResults.Rows)
            {
                var row = this.NewRow();
                row.AdvertisementId = (int)dtRow["Advertisement Id"];
                row.AdvertisementName = (string)dtRow["Advertisement Name"];
                row.DeliverypointgroupId = (int)dtRow["Deliverypointgroup Id"];
                row.DeliverypointgroupName = (string)dtRow["Deliverypointgroup Name"];
                row.ProductId = (int)dtRow["Product Id"];
                row.ProductName = (string)dtRow["Product Name"];
                row.Quantity = (int)dtRow["Quantity"];
                row.OrderSource = (int)dtRow["OrderSource"];
                row.Revenue = double.Parse(dtRow["Revenue"].ToString());

                this.Rows.Add(row);
            }
        }

        private void FetchAdvertisementsForCompany()
        {
            // Get regular advertisements
            var advFilter = new PredicateExpression();
            advFilter.Add(AdvertisementFields.CompanyId == this.filter.CompanyId);
            advFilter.Add(AdvertisementFields.ProductId > 0);

            if (this.filter.DeliverypointgroupIds != null && this.filter.DeliverypointgroupIds.Count > 0)
            {
                advFilter.Add(DeliverypointgroupAdvertisementFields.DeliverypointgroupId == this.filter.DeliverypointgroupIds);
            }

            var relations = new RelationCollection();
            relations.Add(AdvertisementEntityBase.Relations.DeliverypointgroupAdvertisementEntityUsingAdvertisementId);

            var advertisementsCollection = new AdvertisementCollection();
            advertisementsCollection.GetMulti(advFilter, relations);

            foreach (var advertisementEntity in advertisementsCollection)
            {
                foreach (var deliverypointgroupEntity in advertisementEntity.DeliverypointgroupAdvertisementCollection)
                {
                    var row = companyAdvertisements.NewRow();
                    row[ColumnNames.ADVERTISEMENT_ID] = advertisementEntity.AdvertisementId;
                    row[ColumnNames.MEDIA_ID] = 0;
                    row[ColumnNames.ADVERTISEMENT_NAME] = advertisementEntity.Name;
                    row[ColumnNames.DELIVERYPOINTGROUP_ID] = deliverypointgroupEntity.DeliverypointgroupId;
                    row[ColumnNames.CLICKS] = 0;
                    row[ColumnNames.VIEWS] = 0;

                    companyAdvertisements.Rows.Add(row);
                }
            }

            // Get media advertisements (eg. homepage slidehow)
            var queryFactory = new QueryFactory();
            var subQuery = queryFactory.Create()
                                       .Select(DeliverypointgroupFields.DeliverypointgroupId)
                                       .Where(DeliverypointgroupFields.CompanyId == this.filter.CompanyId);

            if (this.filter.DeliverypointgroupIds != null && this.filter.DeliverypointgroupIds.Count > 0)
            {
                subQuery.AndWhere(DeliverypointgroupFields.DeliverypointgroupId == this.filter.DeliverypointgroupIds);
            }

            var query = queryFactory.Create()
                                    .Select(MediaFields.MediaId, MediaFields.Name, MediaFields.DeliverypointgroupId)
                                    .Where(MediaFields.ActionProductId > 0)
                                    .AndWhere(MediaFields.DeliverypointgroupId.In(subQuery));

            var mediaCollection = new DataTable();
            var dao = new TypedListDAO();
            dao.FetchAsDataTable(query, mediaCollection);
            foreach (DataRow mediaEntity in mediaCollection.Rows)
            {
                var row = companyAdvertisements.NewRow();
                row[ColumnNames.ADVERTISEMENT_ID] = 0;
                row[ColumnNames.MEDIA_ID] = Convert.ToInt32(mediaEntity.ItemArray[0]);
                row[ColumnNames.ADVERTISEMENT_NAME] = mediaEntity.ItemArray[1];
                row[ColumnNames.DELIVERYPOINTGROUP_ID] = Convert.ToInt32(mediaEntity.ItemArray[2]);
                row[ColumnNames.CLICKS] = 0;
                row[ColumnNames.VIEWS] = 0;

                companyAdvertisements.Rows.Add(row);
            }
        }

        private void FetchAnalyticsAdvertisementData()
        {
            var gaFilter = string.Format(ANALYTICS_FILTER, EventCategory.Advertising, filter.CompanyId);
            GaData gaData = api.Get(ANALYTICS_METRIC, ANALYTICS_DIMENSION, gaFilter, "", 10000, filter.FromUtc, filter.TillUtc);
            if (gaData == null || gaData.Rows == null)
                return;

            foreach (var gaRow in gaData.Rows)
            {
                var eventAction = gaRow[(int)AnalyticsHeaders.EventAction];
                var eventLabel = gaRow[(int)AnalyticsHeaders.EventLabel];
                var splitStart = eventAction.IndexOf("/", StringComparison.Ordinal);
                if (splitStart >= 0)
                {
                    eventAction = eventAction.Substring(splitStart + 1);
                }

                // With the introduction of Analytics vNext these events have changed to:
                // Event Action: 'Advertisement viewed' / 'Advertisement clicked'
                // Event Label: [Advertisement Reference]                
                bool isView = eventLabel.Equals("Viewed") || eventAction.Equals("Advertisement viewed");
                int totalEvents = int.Parse(gaRow[(int)AnalyticsHeaders.TotalEvents]);
                string primaryKeys = gaRow[(int)AnalyticsHeaders.PrimaryKeys];

                // Parse deliverypointgroup id
                int deliverypointgroupId = 0;
                int dpgIndex = primaryKeys.IndexOf("DPG-", StringComparison.InvariantCultureIgnoreCase);
                if (dpgIndex >= 0)
                {
                    dpgIndex += 4;
                    int dpgIndexEnd = primaryKeys.IndexOf("]", dpgIndex, StringComparison.InvariantCultureIgnoreCase);
                    deliverypointgroupId = int.Parse(primaryKeys.Substring(dpgIndex, dpgIndexEnd - dpgIndex));

                    if (this.filter.DeliverypointgroupIds != null && !this.filter.DeliverypointgroupIds.Contains(deliverypointgroupId))
                    {
                        continue;
                    }
                }

                // Two versions of the event
                string advertisementReferenceNormalized;
                if (eventAction.StartsWith("Advertisement ")) // New
                    advertisementReferenceNormalized = eventLabel.Replace("'", "''");
                else // Old
                    advertisementReferenceNormalized = eventAction.Replace("'", "''");

                string advFilter = string.Format("{0}='{1}' AND {2}={3}", ColumnNames.ADVERTISEMENT_NAME, advertisementReferenceNormalized, ColumnNames.DELIVERYPOINTGROUP_ID, deliverypointgroupId);
                DataRow[] dtRows = companyAdvertisements.Select(advFilter);
                foreach (var dtRow in dtRows)
                {
                    dtRow[ColumnNames.DELIVERYPOINTGROUP_ID] = deliverypointgroupId;

                    if (isView)
                        dtRow[ColumnNames.VIEWS] = totalEvents;
                    else
                        dtRow[ColumnNames.CLICKS] = totalEvents;

                }
            }
        }

        private void PreProcessData()
        {
            foreach (var upsellType in upsellTypes)
            {
                var quantitySum = this.Compute(string.Format("SUM({0})", ColumnNames.QUANTITY), string.Format("{0}={1}", ColumnNames.ORDER_SOURCE, (int)upsellType));
                int quantitySumInt = 0;
                if (quantitySum != DBNull.Value)
                    quantitySumInt = Convert.ToInt32(quantitySum);

                upsellingNumbers.Add(upsellType, quantitySumInt);
                totalProductsUpselling += quantitySumInt;
            }
        }

        #endregion

        #region Spreadsheet

        public void GenerateSummary(SLDocument spreadsheet)
        {
            var filterTimeSpan = DateTimeUtil.GetSpanBetweenDateTimes(this.filter.FromUtc, this.filter.TillUtc);

            spreadsheet.RenameWorksheet(SLDocument.DefaultFirstSheetName, "Summary");
            spreadsheet.SelectWorksheet("Summary");

            // Column Styling
            spreadsheet.SetColumnWidth("A", 50.0);
            spreadsheet.SetColumnWidth("B", 7);
            spreadsheet.SetColumnWidth("C", 8.5);
            spreadsheet.SetColumnWidth("D", 26.0);
            spreadsheet.SetColumnWidth("E", 50.0);
            spreadsheet.SetColumnWidth("F", 34.0);

            spreadsheet.SetColumnStyle(2, HorizontalAlignmentValues.Center);

            int row = 1;

            spreadsheet.SetCellValue("A" + row, "Filter");
            spreadsheet.SetCellValue("B" + row, "");
            spreadsheet.SetStyle("A" + row, "D" + row, fontStyle: FontStyle.Bold, backgroundColor: SpreadsheetHelper.Heading2, borders: Borders.Bottom | Borders.Top);
            row++;

            spreadsheet.SetCellValue("A" + row, "Period Time Zone");
            spreadsheet.SetCellValue("B" + row, this.filter.TimeZoneOlsonId);
            spreadsheet.SetStyle("A" + row, "D" + row, borders: Borders.Bottom, horizontalAlignment: HorizontalAlignmentValues.Left);
            row++;

            spreadsheet.SetCellValue("A" + row, "Period From");
            spreadsheet.SetCellValue("B" + row, this.filter.FromTimeZoned.ToLongDateString());
            spreadsheet.SetStyle("A" + row, "D" + row, borders: Borders.Bottom, horizontalAlignment: HorizontalAlignmentValues.Left);
            row++;

            spreadsheet.SetCellValue("A" + row, "Period Till");
            spreadsheet.SetCellValue("B" + row, this.filter.TillTimeZoned.ToLongDateString());
            spreadsheet.SetStyle("A" + row, "D" + row, borders: Borders.Bottom, horizontalAlignment: HorizontalAlignmentValues.Left);
            row++;

            spreadsheet.SetCellValue("A" + row, "Total Days");
            spreadsheet.SetCellValue("B" + row, Math.Ceiling(filterTimeSpan.TotalDays));
            spreadsheet.SetStyle("A" + row, "D" + row, borders: Borders.Bottom, horizontalAlignment: HorizontalAlignmentValues.Left);
            row++;
            row++;

            spreadsheet.SetCellValue("A" + row, "Product");
            spreadsheet.SetCellValue("B" + row, "Orders");
            spreadsheet.SetCellValue("C" + row, "Revenue");
            spreadsheet.SetCellValue("D" + row, "Deliverypointgroup");
            spreadsheet.SetCellValue("E" + row, "Advertisement");
            spreadsheet.SetCellValue("F" + row, "Order Source");

            spreadsheet.SetStyle("C" + row, HorizontalAlignmentValues.Center);
            spreadsheet.SetStyle("A" + row, "F" + row, fontStyle: FontStyle.Bold, backgroundColor: SpreadsheetHelper.Heading2, borders: Borders.Bottom | Borders.Top);
            row++;

            foreach (UpsellingStatsRow dtRow in this.Rows)
            {
                spreadsheet.SetCellValue("A" + row, dtRow.ProductName);
                spreadsheet.SetCellValue("B" + row, dtRow.Quantity);
                spreadsheet.SetCellValue("C" + row, dtRow.Revenue);
                spreadsheet.SetCellValue("D" + row, dtRow.DeliverypointgroupName);
                spreadsheet.SetCellValue("E" + row, dtRow.AdvertisementName);
                spreadsheet.SetCellValue("F" + row, dtRow.OrderSourceAsEnum.ToString());

                spreadsheet.SetStyle("C" + row, formatCode: SpreadsheetHelper.DecimalFormatCode);
                spreadsheet.SetStyle("A" + row, "F" + row, borders: Borders.Bottom);
                row++;
            }

            row++;

            spreadsheet.SetCellValue("A" + row, "Summary Orders");
            spreadsheet.SetCellValue("B" + row, "Orders");
            spreadsheet.SetCellValue("C" + row, "%");

            spreadsheet.SetStyle("C" + row, HorizontalAlignmentValues.Center);
            spreadsheet.SetStyle("A" + row, "C" + row, fontStyle: FontStyle.Bold, backgroundColor: SpreadsheetHelper.Heading2, borders: Borders.Bottom | Borders.Top);
            row++;

            spreadsheet.SetCellValue("A" + row, "Total Orders");
            spreadsheet.SetCellValue("B" + row, orderCollection.Count);

            spreadsheet.SetStyle("A" + row, "C" + row, borders: Borders.Bottom);
            row++;

            decimal upsellingPercentageRound = 0;
            if (orderCollection.Count > 0)
            {
                decimal upsellingPercentage = ((decimal)totalProductsUpselling / orderCollection.Count) * 100;
                upsellingPercentageRound = Math.Round(upsellingPercentage, 2);
            }

            spreadsheet.SetCellValue("A" + row, "Total upselling");
            spreadsheet.SetCellValue("B" + row, totalProductsUpselling);
            spreadsheet.SetCellValue("C" + row, upsellingPercentageRound);

            spreadsheet.SetStyle("C" + row, HorizontalAlignmentValues.Center);
            spreadsheet.SetStyle("A" + row, "C" + row, borders: Borders.Bottom);
            row++;
            row++;

            spreadsheet.SetCellValue("A" + row, "Summary Announcements/Messages");
            spreadsheet.SetCellValue("B" + row, "");

            spreadsheet.SetStyle("A" + row, "B" + row, fontStyle: FontStyle.Bold, backgroundColor: SpreadsheetHelper.Heading2, borders: Borders.Bottom | Borders.Top);
            row++;

            spreadsheet.SetCellValue("A" + row, "Total");
            spreadsheet.SetCellValue("B" + row, totalMessagesSend);
            spreadsheet.SetStyle("A" + row, "B" + row, borders: Borders.Bottom);
            row++;

            var totalDays = (int)filterTimeSpan.TotalDays;
            spreadsheet.SetCellValue("A" + row, "Avg send/day");
            spreadsheet.SetCellValue("B" + row, Math.Round((totalMessagesSend / (double)totalDays), 2, MidpointRounding.AwayFromZero));
            spreadsheet.SetStyle("A" + row, "B" + row, borders: Borders.Bottom);

            // Scale 1x1 Page
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 1);
            spreadsheet.SetPageSettings(pageSettings);
        }

        public void GenerateRevenuePerUpsellType(SLDocument spreadsheet)
        {
            int row = 1;

            spreadsheet.AddWorksheet("Upsell Revenue");
            spreadsheet.SelectWorksheet("Upsell Revenue");

            spreadsheet.SetColumnWidth("A", 34.0);
            spreadsheet.SetColumnWidth("B", 8.5);
            spreadsheet.SetColumnWidth("C", 8.5);
            spreadsheet.SetColumnWidth("D", 8.5);

            spreadsheet.SetColumnStyle(2, 3, HorizontalAlignmentValues.Center);

            // Header
            spreadsheet.SetCellValue("A" + row, "Totals");

            spreadsheet.SetStyle("A" + row, "D" + row, backgroundColor: SpreadsheetHelper.Heading1, fontStyle: FontStyle.Bold, borders: Borders.Bottom);
            row++;

            spreadsheet.SetCellValue("B" + row, "Items");
            spreadsheet.SetCellValue("C" + row, "%");
            spreadsheet.SetCellValue("D" + row, "Revenue");

            spreadsheet.SetStyle("A" + row, "D" + row, backgroundColor: SpreadsheetHelper.Heading2, fontStyle: FontStyle.Bold, borders: Borders.Bottom);
            row++;

            double totalRevenue = 0.0;
            foreach (var upsellType in upsellingNumbers)
            {
                // Fetch data from datatable
                var revenueSum = this.Compute(string.Format("SUM({0})", ColumnNames.REVENUE), string.Format("{0}={1}", ColumnNames.ORDER_SOURCE, (int)upsellType.Key));

                var revenueSumDouble = revenueSum as double?;
                totalRevenue += revenueSumDouble.GetValueOrDefault(0.0);

                decimal totalProductsUpsellingPerc = 0;
                if (totalProductsUpselling > 0)
                {
                    totalProductsUpsellingPerc = Math.Round(((decimal)upsellType.Value / totalProductsUpselling) * 100);
                }

                spreadsheet.SetCellValue("A" + row, upsellType.Key.ToString());
                spreadsheet.SetCellValue("B" + row, upsellType.Value);
                spreadsheet.SetCellValue("C" + row, totalProductsUpsellingPerc);
                spreadsheet.SetCellValue("D" + row, revenueSumDouble.GetValueOrDefault(0));

                spreadsheet.SetStyle("D" + row, formatCode: SpreadsheetHelper.DecimalFormatCode);
                spreadsheet.SetStyle("A" + row, "D" + row, borders: Borders.Bottom);
                row++;
            }

            spreadsheet.SetCellValue("A" + row, "Total");
            spreadsheet.SetCellValue("B" + row, totalProductsUpselling);
            spreadsheet.SetCellValue("D" + row, totalRevenue);

            spreadsheet.SetStyle("D" + row, formatCode: SpreadsheetHelper.DecimalFormatCode);
            spreadsheet.SetStyle("A" + row, borders: Borders.Top);
            spreadsheet.SetStyle("B" + row, "D" + row, borders: Borders.Top, borderStyle: BorderStyleValues.Double);
            spreadsheet.SetStyle("A" + row, "D" + row, borders: Borders.Bottom);
            row++;

            foreach (var deliverypointgroupEntity in deliverypointgroupCollection)
            {
                row++;

                spreadsheet.SetCellValue("A" + row, deliverypointgroupEntity.Name);
                spreadsheet.SetStyle("A" + row, "D" + row, backgroundColor: SpreadsheetHelper.Heading1, fontStyle: FontStyle.Bold, borders: Borders.Bottom | Borders.Top);
                row++;

                spreadsheet.SetCellValue("B" + row, "Items");
                spreadsheet.SetCellValue("C" + row, "%");
                spreadsheet.SetCellValue("D" + row, "Revenue");
                spreadsheet.SetStyle("A" + row, "D" + row, backgroundColor: SpreadsheetHelper.Heading2, fontStyle: FontStyle.Bold, borders: Borders.Bottom);
                row++;

                var subQuantityResult = this.Compute(string.Format("SUM({0})", ColumnNames.QUANTITY), string.Format("{0}={1}", ColumnNames.DELIVERYPOINTGROUP_ID, deliverypointgroupEntity.DeliverypointgroupId));
                int subTotalQuantity = 0;
                if (subQuantityResult != DBNull.Value)
                    subTotalQuantity = Convert.ToInt32(subQuantityResult);

                double subTotalRevenue = 0.0;
                foreach (var upsellType in upsellingNumbers)
                {
                    string dtFilter = string.Format("{0}={1} AND {2}={3}", ColumnNames.ORDER_SOURCE, (int)upsellType.Key, ColumnNames.DELIVERYPOINTGROUP_ID, deliverypointgroupEntity.DeliverypointgroupId);
                    var revenueSum = this.Compute(string.Format("SUM({0})", ColumnNames.REVENUE), dtFilter);
                    var quantitySum = this.Compute(string.Format("SUM({0})", ColumnNames.QUANTITY), dtFilter);

                    var revenueSumDouble = revenueSum as double?;
                    subTotalRevenue += revenueSumDouble.GetValueOrDefault(0);

                    int quantitySumInt = 0;
                    if (quantitySum != DBNull.Value)
                        quantitySumInt = Convert.ToInt32(quantitySum);

                    decimal quantityPercentage = (subTotalQuantity > 0) ? Math.Round(((decimal)quantitySumInt / subTotalQuantity) * 100) : 0;

                    spreadsheet.SetCellValue("A" + row, upsellType.Key.ToString());
                    spreadsheet.SetCellValue("B" + row, quantitySumInt);
                    spreadsheet.SetCellValue("C" + row, quantityPercentage);
                    spreadsheet.SetCellValue("D" + row, revenueSumDouble.GetValueOrDefault(0.0));

                    spreadsheet.SetStyle("D" + row, formatCode: SpreadsheetHelper.DecimalFormatCode);
                    spreadsheet.SetStyle("A" + row, "D" + row, borders: Borders.Bottom);
                    row++;
                }

                spreadsheet.SetCellValue("A" + row, "Total");
                spreadsheet.SetCellValue("B" + row, subTotalQuantity);
                spreadsheet.SetCellValue("D" + row, subTotalRevenue);

                spreadsheet.SetStyle("D" + row, formatCode: SpreadsheetHelper.DecimalFormatCode);
                spreadsheet.SetStyle("A" + row, borders: Borders.Top);
                spreadsheet.SetStyle("B" + row, "D" + row, borders: Borders.Top, borderStyle: BorderStyleValues.Double);
                spreadsheet.SetStyle("A" + row, "D" + row, borders: Borders.Bottom);
                row++;
            }

            // Scale 1x1 Page
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 1);
            spreadsheet.SetPageSettings(pageSettings);
        }


        public void GenerateAdvertisementInfo(SLDocument spreadsheet)
        {
            int row = 1;

            var dpgToRow = new Dictionary<int, int>();

            spreadsheet.AddWorksheet("Advertisements");
            spreadsheet.SelectWorksheet("Advertisements");

            spreadsheet.SetColumnWidth("A", 50.0);
            spreadsheet.SetColumnWidth("B", 8.5);
            spreadsheet.SetColumnWidth("C", 8.5);
            spreadsheet.SetColumnWidth("D", 8.5);
            spreadsheet.SetColumnWidth("E", 8.5);

            spreadsheet.SetColumnStyle(2, 4, HorizontalAlignmentValues.Center);

            spreadsheet.SetCellValue("A" + row, "Total");
            spreadsheet.SetStyle("A" + row, "E" + row, backgroundColor: SpreadsheetHelper.Heading1, fontStyle: FontStyle.Bold, borders: Borders.Bottom | Borders.Top);
            row++;

            spreadsheet.SetCellValue("A" + row, "Advertisement");
            spreadsheet.SetCellValue("B" + row, "Views");
            spreadsheet.SetCellValue("C" + row, "Clicks");
            spreadsheet.SetCellValue("D" + row, "Orders");
            spreadsheet.SetCellValue("E" + row, "Revenue");
            spreadsheet.SetStyle("A" + row, "E" + row, backgroundColor: SpreadsheetHelper.Heading2, fontStyle: FontStyle.Bold, borders: Borders.Bottom);
            row++;

            foreach (var deliverypointgroupEntity in deliverypointgroupCollection)
            {
                DataRow[] advertisements = companyAdvertisements.Select(string.Format("{0}={1}", ColumnNames.DELIVERYPOINTGROUP_ID, deliverypointgroupEntity.DeliverypointgroupId));
                if (advertisements.Length == 0)
                    continue;

                dpgToRow.Add(deliverypointgroupEntity.DeliverypointgroupId, row);
                row++;
            }
            row++;

            foreach (var deliverypointgroupEntity in deliverypointgroupCollection)
            {
                DataRow[] advertisements = companyAdvertisements.Select(string.Format("{0}={1}", ColumnNames.DELIVERYPOINTGROUP_ID, deliverypointgroupEntity.DeliverypointgroupId));
                if (advertisements.Length == 0)
                    continue;

                spreadsheet.SetCellValue("A" + row, deliverypointgroupEntity.Name);
                spreadsheet.SetStyle("A" + row, "E" + row, backgroundColor: SpreadsheetHelper.Heading1, fontStyle: FontStyle.Bold, borders: Borders.Bottom | Borders.Top);
                row++;

                spreadsheet.SetCellValue("A" + row, "Advertisement");
                spreadsheet.SetCellValue("B" + row, "Views");
                spreadsheet.SetCellValue("C" + row, "Clicks");
                spreadsheet.SetCellValue("D" + row, "Orders");
                spreadsheet.SetCellValue("E" + row, "Revenue");
                spreadsheet.SetStyle("A" + row, "E" + row, backgroundColor: SpreadsheetHelper.Heading2, fontStyle: FontStyle.Bold, borders: Borders.Bottom);
                row++;

                int totalViews = 0;
                int totalClicks = 0;
                int totalOrders = 0;
                double totalRevenue = 0.0;

                foreach (DataRow advRow in advertisements)
                {
                    int filterId = 0;
                    if ((int)advRow[ColumnNames.MEDIA_ID] > 0)
                    {
                        filterId = ((int)advRow[ColumnNames.MEDIA_ID]) * -1;
                    }
                    else if ((int)advRow[ColumnNames.ADVERTISEMENT_ID] > 0)
                    {
                        filterId = (int)advRow[ColumnNames.ADVERTISEMENT_ID];
                    }

                    double? advertisementRevenue = null;
                    if (filterId != 0)
                        advertisementRevenue = this.Compute(string.Format("SUM({0})", ColumnNames.REVENUE), string.Format("{0}={1} AND {2}={3}", ColumnNames.ADVERTISEMENT_ID, filterId, ColumnNames.DELIVERYPOINTGROUP_ID, (int)advRow[ColumnNames.DELIVERYPOINTGROUP_ID])) as double?;

                    int advOrdersCount = this.Select(string.Format("{0}={1} AND {2}={3}", ColumnNames.ADVERTISEMENT_ID, filterId, ColumnNames.DELIVERYPOINTGROUP_ID, (int)advRow[ColumnNames.DELIVERYPOINTGROUP_ID])).Length;

                    totalViews += (int)advRow[ColumnNames.VIEWS];
                    totalClicks += (int)advRow[ColumnNames.CLICKS];
                    totalOrders += advOrdersCount;
                    totalRevenue += advertisementRevenue.GetValueOrDefault(0);

                    spreadsheet.SetCellValue("A" + row, (string)advRow[ColumnNames.ADVERTISEMENT_NAME]);
                    spreadsheet.SetCellValue("B" + row, (int)advRow[ColumnNames.VIEWS]);
                    spreadsheet.SetCellValue("C" + row, (int)advRow[ColumnNames.CLICKS]);
                    spreadsheet.SetCellValue("D" + row, advOrdersCount);
                    spreadsheet.SetCellValue("E" + row, advertisementRevenue.GetValueOrDefault(0));

                    spreadsheet.SetStyle("E" + row, formatCode: SpreadsheetHelper.DecimalFormatCode);
                    spreadsheet.SetStyle("A" + row, "E" + row, borders: Borders.Bottom);
                    row++;
                }

                // DPG Summary
                int summaryRow = dpgToRow[deliverypointgroupEntity.DeliverypointgroupId];
                spreadsheet.SetCellValue("A" + summaryRow, deliverypointgroupEntity.Name);
                spreadsheet.SetCellValue("B" + summaryRow, totalViews);
                spreadsheet.SetCellValue("C" + summaryRow, totalClicks);
                spreadsheet.SetCellValue("D" + summaryRow, totalOrders);
                spreadsheet.SetCellValue("E" + summaryRow, totalRevenue);
                spreadsheet.SetStyle("E" + summaryRow, formatCode: SpreadsheetHelper.DecimalFormatCode);
                spreadsheet.SetStyle("A" + summaryRow, "E" + row, borders: Borders.Bottom);

                row++;
            }

            // Scale 1x1 Page
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 1);
            spreadsheet.SetPageSettings(pageSettings);
        }

        #endregion

        #region Typed Data Row

        internal enum AnalyticsHeaders
        {
            EventAction,
            EventLabel,
            AppType,
            PrimaryKeys,
            TotalEvents,
        }

        internal class ColumnNames
        {
            internal const string ADVERTISEMENT_ID = "AdvertisementId";
            internal const string ADVERTISEMENT_NAME = "AdvertisementName";
            internal const string DELIVERYPOINTGROUP_ID = "DeliverypointgroupId";
            internal const string DELIVERYPOINTGROUP_NAME = "DeliverypointgroupName";
            internal const string PRODUCT_ID = "ProductId";
            internal const string PRODUCT_NAME = "ProductName";
            internal const string ORDER_SOURCE = "OrderSource";
            internal const string REVENUE = "Revenue";
            internal const string QUANTITY = "Quantity";

            internal const string MEDIA_ID = "MediaId";
            internal const string CLICKS = "Clicks";
            internal const string VIEWS = "Views";
        }

        public class UpsellingStatsRow : DataRow
        {
            protected internal UpsellingStatsRow(DataRowBuilder builder)
                : base(builder)
            {
                AdvertisementId = 0;
                DeliverypointgroupId = 0;
                ProductId = 0;
                Quantity = 0;
                Revenue = 0.0;
            }

            public int AdvertisementId
            {
                get { return (int)this[ColumnNames.ADVERTISEMENT_ID]; }
                set { this[ColumnNames.ADVERTISEMENT_ID] = value; }
            }

            public string AdvertisementName
            {
                get { return (string)this[ColumnNames.ADVERTISEMENT_NAME]; }
                set { this[ColumnNames.ADVERTISEMENT_NAME] = value; }
            }

            public int DeliverypointgroupId
            {
                get { return (int)this[ColumnNames.DELIVERYPOINTGROUP_ID]; }
                set { this[ColumnNames.DELIVERYPOINTGROUP_ID] = value; }
            }

            public string DeliverypointgroupName
            {
                get { return (string)this[ColumnNames.DELIVERYPOINTGROUP_NAME]; }
                set { this[ColumnNames.DELIVERYPOINTGROUP_NAME] = value; }
            }

            public int ProductId
            {
                get { return (int)this[ColumnNames.PRODUCT_ID]; }
                set { this[ColumnNames.PRODUCT_ID] = value; }
            }

            public string ProductName
            {
                get { return (string)this[ColumnNames.PRODUCT_NAME]; }
                set { this[ColumnNames.PRODUCT_NAME] = value; }
            }

            public int OrderSource
            {
                get { return (int)this[ColumnNames.ORDER_SOURCE]; }
                set { this[ColumnNames.ORDER_SOURCE] = value; }
            }

            public OrderSourceType OrderSourceAsEnum
            {
                get { return OrderSource.ToEnum<OrderSourceType>(); }
                set { OrderSource = (int)value; }
            }

            public double Revenue
            {
                get { return (double)this[ColumnNames.REVENUE]; }
                set { this[ColumnNames.REVENUE] = value; }
            }

            public int Quantity
            {
                get { return (int)this[ColumnNames.QUANTITY]; }
                set { this[ColumnNames.QUANTITY] = value; }
            }
        }

        #endregion
    }
}