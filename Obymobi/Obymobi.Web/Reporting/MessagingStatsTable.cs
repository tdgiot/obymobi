﻿using System;
using System.Data;
using System.Drawing;
using DocumentFormat.OpenXml.Spreadsheet;
using Google.Apis.Manual.Analytics.v3.Data;
using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Web.Google;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SpreadsheetLight;

namespace Obymobi.Logic.Analytics
{
    /// <summary>
    /// Summary description for MessagingStatsTable
    /// </summary>
    public class MessagingStatsTable : TypedStatsTable<MessagingStatsTable.MessagingStatsRow>
    {
        private const string ANALYTICS_DIMENSION = "ga:eventAction,ga:eventLabel,ga:dimension11,ga:dimension14,ga:appVersion";
        private const string ANALYTICS_METRIC = "ga:totalEvents";
        private const string ANALYTICS_FILTER = "ga:eventCategory=={0};ga:dimension14=@[C-{1}]";

        private readonly Filter filter;
        private readonly GoogleAPI api;

        private readonly CompanyEntity companyEntity;
        private readonly DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();

        #region Ctor

        public MessagingStatsTable(Filter f, GoogleAPI a)
        {
            this.filter = f;
            this.api = a;

            this.Columns.Add(MessagingStatsRow.ANNOUNCEMENT_ID, typeof(int));
            this.Columns.Add(MessagingStatsRow.MESSAGING_ID, typeof(int));
            this.Columns.Add(MessagingStatsRow.DELIVERYPOINTGROUP_ID, typeof(int));
            this.Columns.Add(MessagingStatsRow.TITLE, typeof(string));
            this.Columns.Add(MessagingStatsRow.VIEWS, typeof(int));
            this.Columns.Add(MessagingStatsRow.RESPONSE_OK, typeof(int));
            this.Columns.Add(MessagingStatsRow.REPSONSE_YES, typeof(int));
            this.Columns.Add(MessagingStatsRow.RESPONSE_NO, typeof(int));

            this.companyEntity = new CompanyEntity(this.filter.CompanyId);
        }

        protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
        {
            return new MessagingStatsRow(builder);
        }

        #endregion

        #region Data Retrieval

        public void LoadData()
        {
            var dpgFilter = new PredicateExpression();
            dpgFilter.Add(DeliverypointgroupFields.CompanyId == this.filter.CompanyId);
            deliverypointgroupCollection.GetMulti(dpgFilter);

            LoadMessages();
            LoadAnnouncements();
            LoadAnalyticsData();
        }

        private void LoadMessages()
        {
            var messageFilter = new PredicateExpression();
            messageFilter.Add(MessageFields.CompanyId == this.filter.CompanyId);
            messageFilter.Add(MessageFields.CreatedUTC >= this.filter.FromUtc);
            messageFilter.Add(MessageFields.CreatedUTC <= this.filter.TillUtc);

            var messagePrefetch = new PrefetchPath(EntityType.MessageEntity);
            messagePrefetch.Add(MessageEntityBase.PrefetchPathDeliverypointEntity);

            var messageCollection = new MessageCollection();
            messageCollection.GetMulti(messageFilter, messagePrefetch);

            foreach (var messageEntity in messageCollection)
            {
                var row = this.NewRow();
                row.MessageId = messageEntity.MessageId;
                if (messageEntity.DeliverypointId > 0)
                    row.DeliverypointgroupId = messageEntity.DeliverypointEntity.DeliverypointgroupId;
                row.Title = messageEntity.Title;

                this.Add(row);
            }
        }

        private void LoadAnnouncements()
        {
            // (RecurringBeginDate >= '2014-01-01' AND RecurringEndDate <= '2014-08-19')
            var recurringDateSubFilter1 = new PredicateExpression();
            recurringDateSubFilter1.Add(AnnouncementFields.RecurringBeginDate >= this.filter.FromUtc.UtcToLocalTime(this.companyEntity.TimeZoneInfo));
            recurringDateSubFilter1.Add(AnnouncementFields.RecurringEndDate <= this.filter.TillUtc.UtcToLocalTime(this.companyEntity.TimeZoneInfo));

            // ((RecurringBeginDate >= '2014-01-01' AND RecurringEndDate <= '2014-08-19') OR RecurringEndDate >= '2014-08-19')
            var recurringDateSubFilter = new PredicateExpression();
            recurringDateSubFilter.Add(recurringDateSubFilter1);
            recurringDateSubFilter.AddWithOr(AnnouncementFields.RecurringEndDate >= this.filter.TillUtc.UtcToLocalTime(this.companyEntity.TimeZoneInfo));

            // (Recurring = 1 AND ((RecurringBeginDate >= '2014-01-01' AND RecurringEndDate <= '2014-08-19') OR RecurringEndDate >= '2014-08-19'))
            var recurringSubFilter = new PredicateExpression(AnnouncementFields.Recurring == 1);
            recurringSubFilter.AddWithAnd(recurringDateSubFilter);

            // (Recurring = 0 OR (Recurring = 1 AND ((RecurringBeginDate >= '2014-01-01' AND RecurringEndDate <= '2014-08-19') OR RecurringEndDate >= '2014-08-19')))
            var recurringFilter = new PredicateExpression(AnnouncementFields.Recurring == 0);
            recurringFilter.AddWithOr(recurringSubFilter);

            // CompanyId = 199 AND (Recurring = 0 OR (Recurring = 1 AND ((RecurringBeginDate >= '2014-01-01' AND RecurringEndDate <= '2014-08-19') OR RecurringEndDate >= '2014-08-19')))
            var announcementFilter = new PredicateExpression(AnnouncementFields.CompanyId == this.filter.CompanyId);
            announcementFilter.Add(recurringFilter);

            // .. AND DeliverypointgroupId = <array>
            if (this.filter.DeliverypointgroupIds != null && this.filter.DeliverypointgroupIds.Count > 0)
            {
                announcementFilter.Add(AnnouncementFields.DeliverypointgroupId == this.filter.DeliverypointgroupIds);
            }

            var announcementCollection = new AnnouncementCollection();
            announcementCollection.GetMulti(announcementFilter);

            foreach (var announcementEntity in announcementCollection)
            {
                var row = this.NewRow();
                row.AnnouncementId = announcementEntity.AnnouncementId;
                row.DeliverypointgroupId = announcementEntity.DeliverypointgroupId.GetValueOrDefault(0);
                row.Title = announcementEntity.Title;

                this.Add(row);
            }
        }

        private void LoadAnalyticsData()
        {
            var gaFilter = string.Format(ANALYTICS_FILTER, EventCategory.Messaging, filter.CompanyId);
            GaData gaData = api.Get(ANALYTICS_METRIC, ANALYTICS_DIMENSION, gaFilter, "", 10000, filter.FromUtc, filter.TillUtc);
            if (gaData == null || gaData.Rows == null)
                return;

            foreach (var gaRow in gaData.Rows)
            {
                var eventAction = gaRow[(int)AnalyticsHeaders.EventAction];
                var eventLabel = gaRow[(int)AnalyticsHeaders.EventLabel];
                string primaryKeys = gaRow[(int)AnalyticsHeaders.PrimaryKeys];
                string appVersion = gaRow[(int)AnalyticsHeaders.AppVersion];
                int totalEvents = int.Parse(gaRow[(int)AnalyticsHeaders.TotalEvents]);                

                string messageTitle;
                string messageResponse = "unknown";

                if (eventAction.StartsWith("Message ")) // New
                {

                    messageTitle = gaRow[(int)AnalyticsHeaders.EventLabel];
                    if (eventAction.StartsWith("Message shown") || eventAction.StartsWith("Message expired"))
                    {
                        // Not available in previous version, so skip
                        continue; 
                    }
                    else if (eventAction.StartsWith("Message viewed"))
                    {
                        messageResponse = "viewed";
                    }
                    else
                    {
                        int mrsIndex = primaryKeys.IndexOf("MRS-", StringComparison.InvariantCultureIgnoreCase);
                        if (mrsIndex >= 0)
                        {
                            mrsIndex += 4;
                            int mrsIndexEnd = primaryKeys.IndexOf("]", mrsIndex, StringComparison.InvariantCultureIgnoreCase);
                            messageResponse = primaryKeys.Substring(mrsIndex, mrsIndexEnd - mrsIndex);
                        }
                    }
                }
                else // Old
                {
                    messageTitle = gaRow[(int)AnalyticsHeaders.EventAction];
                    messageResponse = gaRow[(int)AnalyticsHeaders.EventLabel];
                }                                

                int deliverypointgroupId = 0;
                int dpgIndex = primaryKeys.IndexOf("DPG-", StringComparison.InvariantCultureIgnoreCase);
                if (dpgIndex >= 0)
                {
                    dpgIndex += 4;
                    int dpgIndexEnd = primaryKeys.IndexOf("]", dpgIndex, StringComparison.InvariantCultureIgnoreCase);
                    deliverypointgroupId = int.Parse(primaryKeys.Substring(dpgIndex, dpgIndexEnd - dpgIndex));

                    if (this.filter.DeliverypointgroupIds != null && !this.filter.DeliverypointgroupIds.Contains(deliverypointgroupId))
                    {
                        continue;
                    }
                }

                int messageId = 0;
                int msgIndex = primaryKeys.IndexOf("MSG-", StringComparison.InvariantCultureIgnoreCase);
                if (msgIndex >= 0)
                {
                    msgIndex += 4;
                    int msgIndexEnd = primaryKeys.IndexOf("]", msgIndex, StringComparison.InvariantCultureIgnoreCase);
                    messageId = int.Parse(primaryKeys.Substring(msgIndex, msgIndexEnd - msgIndex));
                }

                string dataFilter;
                if (messageId == 0)
                    dataFilter = string.Format("{0}='{1}'", MessagingStatsRow.TITLE, messageTitle);
                else
                {
                    if (messageId > 0)
                        dataFilter = string.Format("{0}={1}", MessagingStatsRow.MESSAGING_ID, messageId);
                    else
                        dataFilter = string.Format("{0}={1}", MessagingStatsRow.ANNOUNCEMENT_ID, (messageId * -1)); // Flip to positive
                }

                DataRow[] dtRows = this.Select(string.Format("{0} AND {1}={2}", dataFilter, MessagingStatsRow.DELIVERYPOINTGROUP_ID, deliverypointgroupId));
                if (dtRows.Length == 0)
                {
                    var row = this.NewRow();
                    row.MessageId = messageId;
                    row.Title = messageTitle;
                    row.DeliverypointgroupId = deliverypointgroupId;

                    if (messageResponse.Equals("viewed", StringComparison.InvariantCultureIgnoreCase))
                        row.Views += totalEvents;
                    else if (messageResponse.Equals("ok", StringComparison.InvariantCultureIgnoreCase))
                        row.ResponseOk += totalEvents;
                    else if (messageResponse.Equals("yes", StringComparison.InvariantCultureIgnoreCase))
                        row.ResponseYes += totalEvents;
                    else if (messageResponse.Equals("no", StringComparison.InvariantCultureIgnoreCase))
                        row.ResponseNo += totalEvents;

                    this.Add(row);
                }
                else
                {
                    foreach (var dtRow in dtRows)
                    {
                        var messagingRow = dtRow as MessagingStatsRow;
                        if (messagingRow == null)
                            continue;

                        if (messageResponse.Equals("viewed", StringComparison.InvariantCultureIgnoreCase))
                            messagingRow.Views += totalEvents;
                        else if (messageResponse.Equals("ok", StringComparison.InvariantCultureIgnoreCase))
                            messagingRow.ResponseOk += totalEvents;
                        else if (messageResponse.Equals("yes", StringComparison.InvariantCultureIgnoreCase))
                            messagingRow.ResponseYes += totalEvents;
                        else if (messageResponse.Equals("no", StringComparison.InvariantCultureIgnoreCase))
                            messagingRow.ResponseNo += totalEvents;
                    }
                }
            }
        }

        #endregion

        #region Spreadsheet

        public void GenerateAllData(SLDocument spreadsheet)
        {
            spreadsheet.AddWorksheet("Announcements & Messages");
            spreadsheet.SelectWorksheet("Announcements & Messages");

            int row = 1;

            spreadsheet.SetColumnWidth("A", 110);
            spreadsheet.SetColumnWidth("B", 6);
            spreadsheet.SetColumnWidth("C", 4);
            spreadsheet.SetColumnWidth("D", 4);
            spreadsheet.SetColumnWidth("E", 4);

            spreadsheet.SetColumnStyle(2, 5, HorizontalAlignmentValues.Center);

            foreach (var deliverypointgroupEntity in deliverypointgroupCollection)
            {
                spreadsheet.SetCellValue("A" + row, deliverypointgroupEntity.Name);
                spreadsheet.SetCellValue("C" + row, "Response");

                spreadsheet.MergeWorksheetCells("C" + row, "E" + row);
                spreadsheet.SetStyle("A" + row, "E" + row, backgroundColor: SpreadsheetHelper.Heading1, fontStyle: FontStyle.Bold, borders: Borders.Bottom | Borders.Top);
                row++;

                spreadsheet.SetCellValue("A" + row, "Title");
                spreadsheet.SetCellValue("B" + row, "Views");
                spreadsheet.SetCellValue("C" + row, "OK");
                spreadsheet.SetCellValue("D" + row, "Yes");
                spreadsheet.SetCellValue("E" + row, "No");
                spreadsheet.SetStyle("A" + row, "E" + row, backgroundColor: SpreadsheetHelper.Heading2, fontStyle: FontStyle.Bold, borders: Borders.Bottom);
                row++;

                DataRow[] dtRows = this.Select(string.Format("{0}={1}", MessagingStatsRow.DELIVERYPOINTGROUP_ID, deliverypointgroupEntity.DeliverypointgroupId));
                if (dtRows.Length == 0)
                {
                    spreadsheet.SetCellValue("A" + row, "No announcements/messages for this deliverypointgroup");
                    spreadsheet.SetStyle("A" + row, "E" + row, borders: Borders.Bottom);
                    row++;
                }

                foreach (var dtRow in dtRows)
                {
                    var statsRow = dtRow as MessagingStatsRow;
                    if (statsRow == null)
                        continue;

                    spreadsheet.SetCellValue("A" + row, statsRow.Title);
                    spreadsheet.SetCellValue("B" + row, statsRow.Views);
                    spreadsheet.SetCellValue("C" + row, statsRow.ResponseOk);
                    spreadsheet.SetCellValue("D" + row, statsRow.ResponseYes);
                    spreadsheet.SetCellValue("E" + row, statsRow.ResponseNo);

                    spreadsheet.SetStyle("A" + row, "E" + row, borders: Borders.Bottom);
                    row++;
                }

                row++;
            }

            // Scale 1x1 Page
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 1);
            spreadsheet.SetPageSettings(pageSettings);
        }

        #endregion

        #region DataRow

        internal enum AnalyticsHeaders
        {
            EventAction,
            EventLabel,
            AppType,
            PrimaryKeys,
            AppVersion,
            TotalEvents
        }

        public class MessagingStatsRow : DataRow
        {
            public const string MESSAGING_ID = "MessagingId";
            public const string ANNOUNCEMENT_ID = "AnnouncementId";
            public const string TITLE = "Title";
            public const string DELIVERYPOINTGROUP_ID = "DeliverypointgroupId";
            public const string VIEWS = "Views";
            public const string RESPONSE_OK = "ResponseOk";
            public const string REPSONSE_YES = "ResponseYes";
            public const string RESPONSE_NO = "ResponseNo";

            protected internal MessagingStatsRow(DataRowBuilder builder)
                : base(builder)
            {
                MessageId = 0;
                AnnouncementId = 0;
                DeliverypointgroupId = 0;
                Views = 0;
                ResponseNo = 0;
                ResponseOk = 0;
                ResponseYes = 0;
            }

            public int MessageId
            {
                get { return (int)this[MESSAGING_ID]; }
                set { this[MESSAGING_ID] = value; }
            }

            public int AnnouncementId
            {
                get { return (int)this[ANNOUNCEMENT_ID]; }
                set { this[ANNOUNCEMENT_ID] = value; }
            }

            public int DeliverypointgroupId
            {
                get { return (int)this[DELIVERYPOINTGROUP_ID]; }
                set { this[DELIVERYPOINTGROUP_ID] = value; }
            }

            public string Title
            {
                get { return (string)this[TITLE]; }
                set { this[TITLE] = value; }
            }

            public int Views
            {
                get { return (int)this[VIEWS]; }
                set { this[VIEWS] = value; }
            }

            public int ResponseOk
            {
                get { return (int)this[RESPONSE_OK]; }
                set { this[RESPONSE_OK] = value; }
            }

            public int ResponseYes
            {
                get { return (int)this[REPSONSE_YES]; }
                set { this[REPSONSE_YES] = value; }
            }

            public int ResponseNo
            {
                get { return (int)this[RESPONSE_NO]; }
                set { this[RESPONSE_NO] = value; }
            }
        }

        #endregion
    }
}