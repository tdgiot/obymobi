﻿using Dionysos;
using Obymobi.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Analytics
{
    public static class SqlQueryHelper
    {
        public static void ExecuteRetrievalQuery(System.Data.DataTable queryResult, string sqlQuery)
        {
            try
            {
                if (queryResult == null)
                    queryResult = new System.Data.DataTable();

                using (SqlConnection connection = new SqlConnection(Data.DaoClasses.CommonDaoBase.ActualConnectionString))
                {
                    connection.Open();
                    SqlCommand useDbCommand = new SqlCommand("USE " + DbUtils.GetCurrentCatalogName(), connection);
                    useDbCommand.ExecuteNonQuery();
                    SqlCommand command = new SqlCommand(sqlQuery, connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(command);
                    adapter.Fill(queryResult);
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Query failed with message: " + ex.Message + "\r\nQuery:\r\n" + sqlQuery, ex);
            }
        }

        public static List<string> GetDefaultWhereClause(Filter filter)
        {
            List<string> whereClauses = new List<string>();
            if (filter != null)
            {
                whereClauses.Add("[Order].[CreatedUTC] >= '{0}'".FormatSafe(filter.FromUtc.ToString("s")));
                whereClauses.Add("[Order].[CreatedUTC] <= '{0}'".FormatSafe(filter.TillUtc.ToString("s")));
                whereClauses.Add("[Order].[CompanyId] = {0}".FormatSafe(filter.CompanyId));

                if (filter.DeliverypointIds != null && filter.DeliverypointIds.Count > 0)
                    whereClauses.Add("[Order].[DeliverypointId] IN ({0})".FormatSafe(string.Join(", ", filter.DeliverypointIds)));

                if (filter.DeliverypointgroupIds != null && filter.DeliverypointgroupIds.Count > 0)
                    whereClauses.Add("[Deliverypoint].[DeliverypointgroupId] IN ({0})".FormatSafe(string.Join(", ", filter.DeliverypointgroupIds)));

                if (!filter.IncludeFailedOrders)
                    whereClauses.Add("[Order].[ErrorCode] = 0".FormatSafe(filter.CompanyId));
            }
            return whereClauses;
        }
    }
}
