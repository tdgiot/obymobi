using Dionysos;
using Obymobi.Constants;
using Obymobi.Enums;
using SD.Tools.OrmProfiler.Shared;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using System.Web.Configuration;

namespace Obymobi
{
    /// <summary>
    /// Enum o
    /// </summary>
    public class WebEnvironmentHelper
    {
        public enum WebEnvironmentHelperResult : int
        {
            HttpContextIsNull = 200,
            HttpContextRequestIsNull = 201,
            BaseUrlNotInAppSettings = 202,
            CloudEnvironmentNotInAppSettings = 203,
            InvalidCloudEnvironment = 204,
            DatabaseNameNotInAppSettings = 205,
            DatabaseNameCouldNotBeDetermined = 206,
            RedisHostNotInAppSettings = 207
        }

        #region Application Instance Characteristics


        /// <summary>
        /// Gets the current environment that this instance is running as
        /// </summary>
        /// <value>
        /// The current environment.
        /// </value>
        /// <exception cref="ObymobiException">
        /// </exception>
        public static CloudEnvironment CloudEnvironment
        {
            get
            {
                string cloudEnvironment = System.Configuration.ConfigurationManager.AppSettings["CloudEnvironment"];

                if (cloudEnvironment.IsNullOrWhiteSpace())
                    throw new ObymobiException(WebEnvironmentHelperResult.CloudEnvironmentNotInAppSettings);

                CloudEnvironment toReturn;
                if (!EnumUtil.TryParse(cloudEnvironment, out toReturn))
                    throw new ObymobiException(WebEnvironmentHelperResult.InvalidCloudEnvironment, cloudEnvironment);

                return toReturn;
            }
        }

        private static string googleApisJwt;
        public static string GoogleApisJwt
        {
            get
            {
                if (WebEnvironmentHelper.googleApisJwt.IsNullOrWhiteSpace())
                {
                    string base64 = WebConfigurationManager.AppSettings["GoogleApisJsonWebTokenBase64"];
                    if (base64.IsNullOrWhiteSpace())
                        throw new Exception("You need to have GoogleApisJsonWebTokenBase64 configured to be using this feature.");

                    WebEnvironmentHelper.googleApisJwt = Encoding.UTF8.GetString(Convert.FromBase64String(base64));
                }

                return WebEnvironmentHelper.googleApisJwt;
            }
        }

        public static bool ORMProfilerEnabled
        {
            get
            {
                string ormProfilerEnabled = System.Configuration.ConfigurationManager.AppSettings[ApplicationConstants.OrmProfilerEnableConfigSettingName];
                return !ormProfilerEnabled.IsNullOrWhiteSpace() && "True".EqualsIgnoreCase(ormProfilerEnabled);
            }
        }

        /// <summary>
        /// Gets if the CloudEnvironment is Production
        /// </summary>
        /// <returns></returns>
        public static bool CloudEnvironmentIsProduction => WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionPrimary ||
                                                           WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionSecondary ||
                                                           WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.ProductionOverwrite;

        /// <summary>
        /// Gets if the CloudEnvironment is Test
        /// </summary>
        /// <returns></returns>
        public static bool CloudEnvironmentIsTest => WebEnvironmentHelper.CloudEnvironment == CloudEnvironment.Test;

        public static string GetRedisHost()
        {
            string host = System.Configuration.ConfigurationManager.AppSettings["RedisHost"];
            if (host.IsNullOrWhiteSpace())
            {
                host = "localhost";
            }

            if (CloudEnvironment != CloudEnvironment.Manual && host.Equals("localhost"))
            {
                throw new ObymobiException(WebEnvironmentHelperResult.RedisHostNotInAppSettings);
            }

            return host;
        }

        ///// <summary>
        ///// This is the url on which the Application Suite is installed, i.e.: http://app.crave-emenu.com or http://192.168.57.75/trunk/
        ///// </summary>
        //public static string GetBaseUrl()
        //{
        //    string baseUrl = string.Empty;

        //    string path = System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath;
        //    baseUrl = System.Configuration.ConfigurationManager.AppSettings["BaseUrl"];

        //    if (baseUrl.IsNullOrWhiteSpace())
        //        throw new ObymobiException(WebEnvironmentHelperResult.BaseUrlNotInAppSettings);

        //    // Add the subpath if we have one:
        //    if (path != null && path.CountOccurencesOfChar('/') > 1)
        //    {
        //        // Running on a subpath - remove the end.
        //        string tmpPath = path;
        //        path = tmpPath.Substring(0, tmpPath.LastIndexOf('/'));
        //        baseUrl = WebEnvironmentHelper.CombinePaths(baseUrl, path);
        //    }

        //    return baseUrl;
        //}

        //public static string GetBaseDomain()
        //{
        //    string baseUrl = System.Configuration.ConfigurationManager.AppSettings["BaseUrl"];
        //    string baseDomain;

        //    if (baseUrl.StartsWith("https://"))
        //        baseDomain = baseUrl.Replace("https://", "");
        //    else
        //        baseDomain = baseUrl.Replace("http://", "");

        //    if (baseDomain.IndexOf('/') > 0)
        //    {
        //        baseDomain = baseDomain.Substring(baseDomain.IndexOf('/'));
        //    }

        //    return baseDomain;
        //}

        ///// <summary>
        ///// This is the url on which the Application Suite is installed, i.e.: http://app.crave-emenu.com or http://192.168.57.75/trunk/
        ///// </summary>
        //private static string GetBaseUrl(string combineWith)
        //{
        //    return WebEnvironmentHelper.CombinePaths(WebEnvironmentHelper.GetBaseUrl(), combineWith);
        //}

        #endregion

        #region Constants / Urls

        /// <summary>
        /// Returns the path for the Console Application: 'console'
        /// </summary>
        public static readonly string Console = ObymobiConstants.WebAppPathConsole;

        /// <summary>
        /// Returns the path for the Mobile Application: 'mobile'
        /// </summary>
        public static readonly string Mobile = ObymobiConstants.WebAppPathMobile;

        /// <summary>
        /// Returns the path for the Management Application: 'management'
        /// </summary>
        public static readonly string Management = ObymobiConstants.WebAppPathManagement;

        /// <summary>
        /// Returns the path for the Noc Application: 'noc'
        /// </summary>
        public static readonly string Noc = ObymobiConstants.WebAppPathNoc;

        /// <summary>
        /// Returns the path for the Messaging Application: 'messaging'
        /// </summary>
        public static readonly string Messaging = ObymobiConstants.WebAppPathMessaging;

        /// <summary>
        /// Returns the path for the Api Application: 'api'
        /// </summary>
        public static readonly string Api = ObymobiConstants.WebAppPathApi;

        /// <summary>
        /// Returns the path for the Noc Service Application: 'nocservice'
        /// </summary>
        public static readonly string NocService = ObymobiConstants.WebAppPathNocService;

        /// <summary>
        /// Returns the path for the Backend Service Application: 'backendservice'
        /// </summary>
        public static readonly string Services = ObymobiConstants.WebAppPathServices;

        /// <summary>
        /// Returns the Management url without a trailing slash (i.e. http://currentdomain.com/management)
        /// </summary>
        /// <returns></returns>
        public static string GetManagementUrl() => System.Configuration.ConfigurationManager.AppSettings[CraveConstants.CraveObymobiCmsUrl];

        /// <summary>
        /// Returns the Management Url + the path you want to append 
        /// </summary>
        /// <param name="pathToAppend">Path to append without starting slash (i.e. default.aspx), start slash will be added automatically</param>
        /// <returns></returns>
        public static string GetManagementUrl(string pathToAppend) => WebEnvironmentHelper.CombinePaths(WebEnvironmentHelper.GetManagementUrl(), pathToAppend);

        /// <summary>
        /// Returns the Noc url without a trailing slash (i.e. http://currentdomain.com/noc)
        /// </summary>
        /// <returns></returns>
        public static string GetNocUrl() => System.Configuration.ConfigurationManager.AppSettings[CraveConstants.CraveObymobiNocUrl];

        /// <summary>
        /// Returns the Noc Url + the path you want to append 
        /// </summary>
        /// <param name="pathToAppend">Path to append without starting slash (i.e. default.aspx), start slash will be added automatically</param>
        /// <returns></returns>
        public static string GetNocUrl(string pathToAppend) => WebEnvironmentHelper.CombinePaths(WebEnvironmentHelper.GetNocUrl(), pathToAppend);

        /// <summary>
        /// Returns the Messaging url without a trailing slash (i.e. http://currentdomain.com/messaging)
        /// </summary>
        /// <returns></returns>
        public static string GetMessagingUrl() => System.Configuration.ConfigurationManager.AppSettings[CraveConstants.CraveObymobiMessagingUrl];

        /// <summary>
        /// Returns the Messaging Url + the path you want to append 
        /// </summary>
        /// <param name="pathToAppend">Path to append without starting slash (i.e. default.aspx), start slash will be added automatically</param>
        /// <returns></returns>
        public static string GetMessagingUrl(string pathToAppend) => WebEnvironmentHelper.CombinePaths(WebEnvironmentHelper.GetMessagingUrl(), pathToAppend);

        /// <summary>
        /// Returns the Api url without a trailing slash (i.e. http://currentdomain.com/api)
        /// </summary>
        /// <returns></returns>
        public static string GetApiUrl() => System.Configuration.ConfigurationManager.AppSettings[CraveConstants.CraveObymobiApiUrl];

        /// <summary>
        /// Returns the Api Url + the path you want to append 
        /// </summary>
        /// <param name="pathToAppend">Path to append without starting slash (i.e. default.aspx), start slash will be added automatically</param>
        /// <returns></returns>
        public static string GetApiUrl(string pathToAppend) => WebEnvironmentHelper.CombinePaths(WebEnvironmentHelper.GetApiUrl(), pathToAppend);

        /// <summary>
        /// Gets the API crave service URL in the correct version (.
        /// </summary>
        /// <returns></returns>
        public static string GetApiCraveServiceUrl() => WebEnvironmentHelper.GetApiUrl(ObymobiConstants.API_VERSION + "/CraveService.asmx");

        /// <summary>
        /// Gets the API mobile service URL in the correct version (.
        /// </summary>
        /// <returns></returns>
        public static string GetApiMobileServiceUrl() => WebEnvironmentHelper.GetApiUrl(ObymobiConstants.API_VERSION + "/MobileService.asmx");

        /// <summary>
        /// Gets the new API url in the correct version. (e.g. http://api.local.cravecloud.com)
        /// </summary>
        public static string GetRestApiBaseUrl() => System.Configuration.ConfigurationManager.AppSettings[ObymobiConstants.RestApiBaseUrl];

        /// <summary>
        /// Returns the Backend service url without a trailing slash (i.e. http://currentdomain.com/services)
        /// </summary>
        /// <returns></returns>
        public static string GetServicesUrl() => System.Configuration.ConfigurationManager.AppSettings[CraveConstants.CraveObymobiServicesUrl];

        /// <summary>
        /// Get the NocServiceUrls for the current environment
        /// </summary>
        /// <returns></returns>
        public static List<string> GetNocServiceUrls() => new List<string>
        {
            ObymobiConstants.NocStatusServiceUrl1
        };

        /// <summary>
        /// Get NocService urls which can be used to get the remote ip
        /// </summary>
        /// <returns></returns>
        public static List<string> GetNocServiceIpCheckUrls()
        {
            var nocServiceUrls = GetNocServiceUrls();

            for (var i = 0; i < nocServiceUrls.Count; i++)
            {
                nocServiceUrls[i] = CombinePaths(nocServiceUrls[i], "ip.ashx");
            }

            return nocServiceUrls;
        }

        /// <summary>
        /// Get the url of the Txt file containing the CloudStatus
        /// </summary>
        /// <returns></returns>
        public static List<string> GetNocServiceLastStateTxtUrl()
        {
            List<string> urls = new List<string>();
            urls.Add(WebEnvironmentHelper.GetAmazonCdnBaseUrl(WebEnvironmentHelper.CloudEnvironment));

            for (int i = 0; i < urls.Count; i++)
            {
                urls[i] = WebEnvironmentHelper.CombinePaths(urls[i], "status/last-state.txt");
            }

            return urls;
        }

        /// <summary>
        /// Get the url of the Txt file containing the Connection String information
        /// </summary>
        /// <returns></returns>
        public static List<string> GetNocServiceConnectionStringTxtUrls()
        {
            List<string> urls = new List<string>();
            urls.Add(WebEnvironmentHelper.GetAmazonCdnBaseUrl(WebEnvironmentHelper.CloudEnvironment));

            for (int i = 0; i < urls.Count; i++)
            {
                urls[i] = WebEnvironmentHelper.CombinePaths(urls[i], "status/connection-string.txt");
            }

            return urls;
        }

        public static string GetAmazonCdnBaseUrl(CloudEnvironment cloudEnvironment)
        {
            string toReturn = string.Empty;
            if (WebEnvironmentHelper.CloudEnvironment == Enums.CloudEnvironment.ProductionPrimary || WebEnvironmentHelper.CloudEnvironment == Enums.CloudEnvironment.ProductionSecondary || WebEnvironmentHelper.CloudEnvironment == Enums.CloudEnvironment.ProductionOverwrite)
            {
                toReturn = string.Format(ObymobiConstants.AmazonBlobStorageUrlFormat, ObymobiConstants.PrimaryAmazonRootContainer, string.Empty);
            }
            else if (WebEnvironmentHelper.CloudEnvironment == Enums.CloudEnvironment.Staging)
            {
                toReturn = string.Format(ObymobiConstants.AmazonBlobStorageUrlFormat, ObymobiConstants.StagingAmazonRootContainer, string.Empty);
            }
            else if (WebEnvironmentHelper.CloudEnvironment == Enums.CloudEnvironment.Development)
            {
                toReturn = string.Format(ObymobiConstants.AmazonBlobStorageUrlFormat, ObymobiConstants.DevelopmentAmazonRootContainer, string.Empty);
            }
            else if (WebEnvironmentHelper.CloudEnvironment == Enums.CloudEnvironment.Test)
            {
                toReturn = string.Format(ObymobiConstants.AmazonBlobStorageUrlFormat, ObymobiConstants.TestAmazonRootContainer, string.Empty);
            }
            else
            {
                string containerName = System.Configuration.ConfigurationManager.AppSettings[ObymobiConstants.CloudManualContainerSettingName];

                if (containerName.IsNullOrWhiteSpace())
                    throw new Exception("Cloud Manual container is not set in appSettings.");

                toReturn = string.Format(ObymobiConstants.AmazonBlobStorageUrlFormat, containerName, string.Empty);
            }
            return toReturn;
        }

        /// <summary>
        /// This is the /files/ path on the CDN
        /// </summary>
        /// <param name="storageLocation"></param>
        /// <param name="path"></param>
        /// <param name="forwardSlashed"></param>
        /// <returns></returns>
        public static string GetCdnBaseUrlMedia(CloudStorageProviderIdentifier storageLocation, string path = "", bool forwardSlashed = false)
        {
            string url = string.Empty;

            switch (WebEnvironmentHelper.CloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.PrimaryAmazonCdnBaseUrlFiles;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                case CloudEnvironment.Test:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.TestAmazonCdnBaseUrlFiles;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                case CloudEnvironment.Development:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.DevelopmentAmazonCdnBaseUrlFiles;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                case CloudEnvironment.Staging:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.StagingAmazonCdnBaseUrlFiles;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                case CloudEnvironment.Manual:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.ManualAmazonCdnBaseUrlFiles;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                default:
                    throw new NotImplementedException(WebEnvironmentHelper.CloudEnvironment.ToString());
            }

            if (!path.IsNullOrWhiteSpace())
                url = WebEnvironmentHelper.CombinePaths(url, path);

            if (forwardSlashed)
                url = url.Replace("\\", "/");

            return url;
        }

        /// <summary>
        /// This is the /published/ path on the CDN
        /// </summary>
        public static string GetCdnBaseUrlPublished(CloudStorageProviderIdentifier storageLocation, string path = "", bool forwardSlashed = false)
        {
            string url = string.Empty;

            switch (WebEnvironmentHelper.CloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.PrimaryAmazonCdnBaseUrlPublished;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                case CloudEnvironment.Test:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.TestAmazonCdnBaseUrlPublished;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                case CloudEnvironment.Development:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.DevelopmentAmazonCdnBaseUrlPublished;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                case CloudEnvironment.Staging:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.StagingAmazonCdnBaseUrlPublished;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                case CloudEnvironment.Manual:
                    switch (storageLocation)
                    {
                        case CloudStorageProviderIdentifier.Amazon:
                            url = ObymobiConstants.ManualAmazonCdnBaseUrlPublished;
                            break;
                        default:
                            throw new NotImplementedException("Not implement GetCdnUrl for StorageLocation {0} on environment: {1}".FormatSafe(storageLocation, WebEnvironmentHelper.CloudEnvironment));
                    }
                    break;
                default:
                    throw new NotImplementedException(WebEnvironmentHelper.CloudEnvironment.ToString());
            }

            if (!path.IsNullOrWhiteSpace())
                url = WebEnvironmentHelper.CombinePaths(url, path);

            if (forwardSlashed)
                url = url.Replace("\\", "/");

            return url;
        }

        /// <summary>
        /// Retrieves a list of available Cdn addresses (i.e. http://cravefilesdevelopment.blob.core.windows.net/files, http://app.crave-emenu.com/api/files)
        /// </summary>
        /// <param name="path">Path you want to have combined with the urls</param>
        /// <returns></returns>
        public static List<string> GetCdnUrls(string path)
        {
            List<string> urls = new List<string>();

            // Urls should be added in order of priority in which the should be used!
            switch (WebEnvironmentHelper.CloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    urls.Add(ObymobiConstants.PrimaryAmazonCdnBaseUrlFiles);
                    break;
                case CloudEnvironment.Test:
                    urls.Add(ObymobiConstants.TestAmazonCdnBaseUrlFiles);
                    break;
                case CloudEnvironment.Development:
                    urls.Add(ObymobiConstants.DevelopmentAmazonCdnBaseUrlFiles);
                    break;
                case CloudEnvironment.Staging:
                    urls.Add(ObymobiConstants.StagingAmazonCdnBaseUrlFiles);
                    break;
                case CloudEnvironment.Manual:
                    if (!ObymobiConstants.ManualAmazonCdnBaseUrl.IsNullOrWhiteSpace())
                        urls.Add(ObymobiConstants.ManualAmazonCdnBaseUrl);
                    urls.Add(WebEnvironmentHelper.GetApiUrl(ObymobiConstants.CloudContainerMediaFiles));
                    break;
                default:
                    throw new NotImplementedException(WebEnvironmentHelper.CloudEnvironment.ToString());
            }

            if (!path.IsNullOrWhiteSpace())
            {
                for (int i = 0; i < urls.Count; i++)
                {
                    urls[i] = WebEnvironmentHelper.CombinePaths(urls[i], path);
                }
            }

            return urls;
        }

        private static string databaseName = null;

        public static string GetDatabaseName()
        {
            return GetDatabaseName(WebEnvironmentHelper.CloudEnvironment);
        }

        /// <summary>
        /// Get's the DatabaseName for all Environments
        /// </summary>
        /// <returns></returns>
        public static string GetDatabaseName(CloudEnvironment cloudEnvironment)
        {
            if (databaseName.IsNullOrWhiteSpace())
            {
                switch (cloudEnvironment)
                {
                    case CloudEnvironment.ProductionPrimary:
                    case CloudEnvironment.ProductionSecondary:
                    case CloudEnvironment.ProductionOverwrite:
                        if (TestUtil.IsPcGabriel)
                            return "ObymobiDevelopment2";
                        else
                            return "ObymobiProduction";
                    case CloudEnvironment.Test:
                        return "ObymobiTest";
                    case CloudEnvironment.Development:
                        return "ObymobiDevelopment";
                    case CloudEnvironment.Staging:
                        return "ObymobiStaging";
                    case CloudEnvironment.Manual:
                        // GK - Yes I went to great lengths to keep this to as low config as possible.                    
                        // Try to get from Overwrite
                        try
                        {
                            NameValueCollection section = (NameValueCollection)System.Web.Configuration.WebConfigurationManager.GetSection("sqlServerCatalogNameOverwrites");
                            if (section != null && !section["Obymobi"].IsNullOrWhiteSpace())
                                databaseName = section["Obymobi"];
                        }
                        catch
                        {
                        }

                        // Try to get from ConnectionString
                        if (databaseName.IsNullOrWhiteSpace())
                        {
                            try
                            {
                                string connectionString = System.Configuration.ConfigurationManager.AppSettings["ObymobiData.ConnectionString"];
                                if (!connectionString.IsNullOrWhiteSpace())
                                {
                                    SqlConnectionStringBuilder qstring = new SqlConnectionStringBuilder(connectionString);
                                    databaseName = qstring.InitialCatalog;
                                }
                            }
                            catch
                            {
                            }
                        }

                        // Try to get from DatabaseName in AppSettings
                        if (databaseName.IsNullOrWhiteSpace())
                        {
                            try
                            {
                                databaseName = System.Configuration.ConfigurationManager.AppSettings["DatabaseName"];
                            }
                            catch
                            {
                            }
                        }

                        break;
                    default:
                        break;
                }
            }

            if (databaseName.IsNullOrWhiteSpace())
                throw new ObymobiException(WebEnvironmentHelperResult.DatabaseNameCouldNotBeDetermined);

            return WebEnvironmentHelper.databaseName;
        }

        public static string GetAmazonRootContainer()
        {
            switch (CloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    return "cravecloud-tdg-production";
                case CloudEnvironment.Test:
                    return "cravecloud-tdg-test";
                case CloudEnvironment.Development:
                    return "cravecloud-tdg-development";
                case CloudEnvironment.Staging:
                    return "cravecloud-tdg-staging";
                case CloudEnvironment.Manual:
                    return string.Format("{0}", System.Configuration.ConfigurationManager.AppSettings["CloudManualContainer"]);
                default:
                    throw new NotImplementedException(CloudEnvironment.ToString());
            }
        }

        public static string GetEnvironmentPrefixForCdn()
        {
            return WebEnvironmentHelper.GetEnvironmentPrefixForCdn(WebEnvironmentHelper.CloudEnvironment);
        }

        public static string GetEnvironmentPrefixForCdn(CloudEnvironment environment)
        {
            switch (environment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    return string.Empty;
                case CloudEnvironment.Test:
                    return ObymobiConstants.TestAmazonRootContainer.Replace("cravecloud-tdg-", "");
                case CloudEnvironment.Development:
                    return ObymobiConstants.DevelopmentAmazonRootContainer.Replace("cravecloud-tdg-", "");
                case CloudEnvironment.Staging:
                    return ObymobiConstants.StagingAmazonRootContainer.Replace("cravecloud-tdg-", "");
                case CloudEnvironment.Manual:
                    string containerName = System.Configuration.ConfigurationManager.AppSettings[ObymobiConstants.CloudManualContainerSettingName];

                    if (containerName.IsNullOrWhiteSpace())
                    {
                        throw new Exception("Cloud manual container is not set in appSettings.");
                    }

                    return containerName.Replace("cravecloud-tdg-", "");
                default:
                    throw new NotImplementedException(environment.ToString());
            }
        }

        public static void GetSupportNotificationSenderInformation(out string emailSenderAddress, out string emailSenderName, out string smsSender)
        {
            emailSenderName = "Non-Live Support";
            emailSenderAddress = "no-reply@trueguest.tech";
            smsSender = "NonLiveSupp";

            switch (WebEnvironmentHelper.CloudEnvironment)
            {
                case CloudEnvironment.ProductionPrimary:
                case CloudEnvironment.ProductionSecondary:
                case CloudEnvironment.ProductionOverwrite:
                    emailSenderName = "Live Support";
                    smsSender = "LiveSupport";
                    break;
                case CloudEnvironment.Test:
                    emailSenderName = "Test Support";
                    smsSender = "TestSupport";
                    break;
                case CloudEnvironment.Development:
                    emailSenderName = "Dev Support";
                    smsSender = "DevSupport";
                    break;
                case CloudEnvironment.Staging:
                    emailSenderName = "Staging Support";
                    smsSender = "StagingSupport";
                    break;
                case CloudEnvironment.Manual:
                    emailSenderName = "Manual Support";
                    smsSender = "ManuSupport";
                    break;
            }
        }

        public static string GetOrderingApiBaseUrl()
        {
            return System.Configuration.ConfigurationManager.AppSettings["Crave.Ordering.ApiUrl"];
        }

        #endregion

        #region Physical Paths

        /// <summary>
        /// Get the physical path of the Api (i.e. D:\\Development\\dotnet\\Obymobi\\ObymobiWebService\\)
        /// </summary>
        /// <returns></returns>
        public static string GetApiPath()
        {
            return System.Web.Hosting.HostingEnvironment.MapPath("~/../{0}/".FormatSafe(ObymobiConstants.WebAppPathApi));
        }

        /// <summary>
        /// Get the physical path of the Management (i.e. D:\\Development\\dotnet\\Obymobi\\OybmobiCms\\)
        /// </summary>
        /// <returns></returns>
        public static string GetManagementPath()
        {
            return System.Web.Hosting.HostingEnvironment.MapPath("~/../{0}/".FormatSafe(ObymobiConstants.WebAppPathManagement));
        }

        /// <summary>
        /// Get the physical path of the Messaging (i.e. D:\\Development\\dotnet\\Obymobi\\CometServer\\)
        /// </summary>
        /// <returns></returns>
        public static string GetMessagingPath()
        {
            return System.Web.Hosting.HostingEnvironment.MapPath("~/../{0}/".FormatSafe(ObymobiConstants.WebAppPathMessaging));
        }

        /// <summary>
        /// Get the physical path of the Services (i.e. D:\\Development\\dotnet\\Obymobi\\Services\\)
        /// </summary>
        /// <returns></returns>
        public static string GetServicesPath()
        {
            return System.Web.Hosting.HostingEnvironment.MapPath("~/../{0}/".FormatSafe(ObymobiConstants.WebAppPathServices));
        }

        #endregion

        #region Utils

        public static string CombinePaths(params string[] paths)
        {
            return Dionysos.Web.WebPathCombiner.CombinePaths(paths);
        }

        #endregion
    }
}
