﻿using System;
using System.Collections.Generic;
using System.Linq;
using Amazon;
using Amazon.EC2;
using Amazon.EC2.Model;
using Instance = Amazon.EC2.Model.Instance;

namespace Obymobi.Web.CloudHosting
{
    public class AmazonCloudHostingProvider : CloudHostingProvider
    {
        private readonly AmazonEC2Client client;

        public AmazonCloudHostingProvider()
        {
            this.client = this.CreateClient();
        }

        public CloudInstance RequestInstance(int userId)
        {
            Instance instance = this.GetInstanceForUser(userId);
            if (instance != null)
            {
                this.StartInstance(instance);
            }
            return new CloudInstance(instance);
        }

        public CloudInstance StopInstance(int userId)
        {
            Instance instance = this.GetInstanceForUser(userId);
            if (instance != null)
            {
                this.StopInstance(instance);
            }
            return new CloudInstance(instance);
        }

        private Instance AttachInstanceToUser(int userId)
        {
            Instance instance = this.GetUnattachedInstance();
            if (instance != null)
            {
                this.CreateTagForInstance(instance.InstanceId, userId);
            }
            return instance;
        }

        private Instance GetInstanceForUser(int userId)
        {
            Instance instance = null;

            foreach (Instance tmp in this.RequestInstances())
            {
                Tag tag = tmp.Tags.SingleOrDefault(x => x.Key.Equals(userId.ToString(), StringComparison.InvariantCultureIgnoreCase));
                if (tag != null)
                {
                    instance = tmp;
                    break;
                }
            }

            if (instance == null)
            {
                instance = this.AttachInstanceToUser(userId);
            }

            return instance;
        }

        private Instance GetUnattachedInstance()
        {
            List<Instance> instances = this.RequestInstances();
            foreach (Instance instance in instances)
            {
                Tag tag = instance.Tags.SingleOrDefault(x => x.Key.Contains("userId"));
                if (tag == null)
                {
                    return instance;
                }
            }
            return null;
        }

        private List<Instance> RequestInstances()
        {
            Filter filter = new Filter();
            filter.Name = "tag-key";
            filter.Values.Add("demo");

            DescribeInstancesRequest request = new DescribeInstancesRequest();
            request.Filters.Add(filter);

            DescribeInstancesResponse result = this.client.DescribeInstances(request);
            List<Instance> instances = new List<Instance>();
            foreach (Reservation reservation in result.Reservations)
            {
                instances.AddRange(reservation.Instances);
            }
            return instances;
        }

        private void StartInstance(Instance instance)
        {
            if (instance.State.Code == 80)
                return;

            StartInstancesRequest request = new StartInstancesRequest();
            request.InstanceIds = new List<string>() { instance.InstanceId };

            this.client.StartInstances(request);
        }

        private void StopInstance(Instance instance)
        {
            StopInstancesRequest request = new StopInstancesRequest();
            request.InstanceIds = new List<string>() { instance.InstanceId };

            this.client.StopInstances(request);
        }

        private void CreateTagForInstance(string instanceId, int userId)
        {
            Tag tag = new Tag();
            tag.Key = "userId";
            tag.Value = userId.ToString();

            CreateTagsRequest request = new CreateTagsRequest();
            request.Resources.Add(instanceId);
            request.Tags.Add(tag);

            this.client.CreateTags(request);
        }

        private AmazonEC2Client CreateClient()
        {
            string awsAccessKey = "AKIAJEOSSSJNNPQ4FX6Q";
            string awsSecretAccessKey = "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";

            return new AmazonEC2Client(awsAccessKey, awsSecretAccessKey, RegionEndpoint.APSoutheast1);
        }

        public class CloudInstance
        {
            public bool IsRunning;
            public string Url;
            public string Username;
            public string Password;

            public CloudInstance(Instance instance)
            {
                if (instance != null)
                {
                    this.IsRunning = instance.State.Code == 80;
                    this.Url = string.Format("https://{0}", instance.PublicDnsName);
                    this.Username = "genymotion";
                    this.Password = instance.InstanceId;
                }
            }
        }
    }
}
