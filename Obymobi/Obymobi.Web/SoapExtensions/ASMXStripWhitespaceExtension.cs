﻿using System;
using System.IO;
using System.Web.Services.Protocols;
using System.Text;
using System.Xml;

namespace Obymobi.Web.SoapExtensions
{
    public class ASMXStripWhitespaceExtension : SoapExtension
    {
        // Fields
        private Stream newStream;
        private Stream oldStream;

        public MemoryStream YankIt(Stream streamToPrefix)
        {
            streamToPrefix.Position = 0L;
            XmlTextReader reader = new XmlTextReader(streamToPrefix);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = false;
            settings.NewLineChars = "";
            settings.NewLineHandling = NewLineHandling.None;
            settings.Encoding = Encoding.UTF8;
            MemoryStream outStream = new MemoryStream();
            using (XmlWriter writer = XmlWriter.Create(outStream, settings))
            {
                do
                {
                    writer.WriteNode(reader, true);
                }
                while (reader.Read());
                writer.Flush();
            }

            ////debug
            //outStream.Seek(0, SeekOrigin.Begin);
            ////outStream.Position = 0L;
            //StreamReader reader2 = new StreamReader(outStream);
            //string s = reader2.ReadToEnd();
            //System.Diagnostics.Debug.WriteLine(s);

            //outStream.Position = 0L;
            outStream.Seek(0, SeekOrigin.Begin);
            return outStream;
        }

        // Methods
        private void StripWhitespace()
        {
            this.newStream.Position = 0L;
            this.newStream = this.YankIt(this.newStream);
            this.Copy(this.newStream, this.oldStream);
        }

        private void Copy(Stream from, Stream to)
        {
            TextReader reader = new StreamReader(from);
            TextWriter writer = new StreamWriter(to);
            writer.WriteLine(reader.ReadToEnd());
            writer.Flush();
        }

        public override void ProcessMessage(SoapMessage message)
        {
            switch (message.Stage)
            {
                case SoapMessageStage.BeforeSerialize:
                case SoapMessageStage.AfterDeserialize:
                    return;

                case SoapMessageStage.AfterSerialize:
                    this.StripWhitespace();
                    return;
                case SoapMessageStage.BeforeDeserialize:
                    this.GetReady();
                    return;
            }
            throw new Exception("invalid stage");
        }

        public override Stream ChainStream(Stream stream)
        {
            this.oldStream = stream;
            this.newStream = new MemoryStream();
            return this.newStream;
        }

        private void GetReady()
        {
            this.Copy(this.oldStream, this.newStream);
            this.newStream.Position = 0L;
        }

        public override object GetInitializer(Type t)
        {
            return typeof(ASMXStripWhitespaceExtension);
        }

        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        {
            return attribute;
        }

        public override void Initialize(object initializer)
        {
            //You'd usually get the attribute here and pull whatever you need off it.
            ASMXStripWhitespaceAttribute attr = initializer as ASMXStripWhitespaceAttribute;
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class ASMXStripWhitespaceAttribute : SoapExtensionAttribute
    {
        // Fields
        private int priority;

        // Properties
        public override Type ExtensionType
        {
            get { return typeof(ASMXStripWhitespaceExtension); }
        }

        public override int Priority
        {
            get { return this.priority; }
            set { this.priority = value; }
        }
    }
}