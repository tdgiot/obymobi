﻿using System;
using Dionysos;
using Dionysos.Interfaces;
using Microsoft.WindowsAzure;
using Obymobi.Constants;
using Obymobi.Enums;
using Dionysos.Web;

namespace Obymobi.Web.GoogleApis
{
    public class GoogleApiKeyVerifier : IVerifier
    {
        public string ErrorMessage { get; set; }
        
        public bool Verify()
        {
            bool toReturn = true;
            this.ErrorMessage = string.Empty;
            string serverApiKey = Dionysos.Global.ConfigurationProvider.GetString(DionysosWebConfigurationConstants.GoogleGenericApiKey);
            string browserApiKey = Dionysos.Global.ConfigurationProvider.GetString(DionysosWebConfigurationConstants.GoogleMapsApiKey); 
            if (serverApiKey.IsNullOrWhiteSpace())
            {
                toReturn = false;
                this.ErrorMessage = "The Google - Server - API key is missing: DionysosWebConfigurationConstants.GoogleGenericApiKey == NULL or White Space. \r\n";
            }

            if(browserApiKey.IsNullOrWhiteSpace())
            {
                toReturn = false;                
                this.ErrorMessage += "The Google - Browser - API key is missing: DionysosWebConfigurationConstants.GoogleMapsApiKey == NULL or White Space. \r\n";
            }
            else if (browserApiKey.Equals("ABQIAAAAUqmPZC5xOrgKJoqE4HCKIRT0VdAE2y5c3Bk0LxWTyQ1qiRu6qBRlp0zzPXNhiKPaWRL3op-JMnjDcA"))
            {
                toReturn = false;
                this.ErrorMessage += "The Google - Browser - API key is wrong, it's the default key from code (for studio.amteam.nl). Update DionysosWebConfigurationConstants.GoogleMapsApiKey. \r\n";
            }

            if (!this.ErrorMessage.IsNullOrWhiteSpace())
            {
                this.ErrorMessage += "The correct API keys should be used (i.e. developer machine vs. dev, test, demo and live), which should stored in and can be retrieved from: O:\\Software Development\\Passwords\\Google API keys.txt \r\n";
            }

            return toReturn;
        }
    }
}
