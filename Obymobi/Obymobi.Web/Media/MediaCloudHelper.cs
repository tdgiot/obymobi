﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.CloudStorage;

namespace Obymobi.Web.Media
{
    public static class MediaCloudHelper
    {
        public static bool CopyMediaOnCdn(MediaEntity mediaToCopyFrom, MediaEntity mediaToCopyTo)
        {
            if (mediaToCopyFrom == null || mediaToCopyTo == null)
            {
                return false;
            }

            byte[] bytes = mediaToCopyFrom.Download();
            if (bytes.Length <= 0)
            {
                return false;
            }

            mediaToCopyTo.Refetch();
            mediaToCopyTo.LastDistributedVersionTicks = mediaToCopyTo.UpdatedUTC.GetValueOrDefault(mediaToCopyTo.CreatedUTC.GetValueOrDefault(mediaToCopyTo.CreatedUTC.GetValueOrDefault())).Ticks;

            return mediaToCopyTo.Upload(bytes);
        }
    }
}
