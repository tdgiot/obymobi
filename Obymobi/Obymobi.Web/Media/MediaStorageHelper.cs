﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Obymobi.Web.Media
{
    public class MediaStorageHelper
    {
        public static void PublishGenericContentToCdn()
        {
            MediaStorageHelper.PublishMediaToCdn(MediaStorageHelper.GetGenericMedia());
        }

        public static void PublishEntertainmentMediaToCdn()
        {
            MediaStorageHelper.PublishMediaToCdn(MediaStorageHelper.GetEntertainmentMedia());
        }

        public static void PublishMediaToCdn(int companyId, bool unpublishedOnly = true)
        { 
            MediaStorageHelper.PublishMediaToCdn(MediaStorageHelper.GetMediaForCompany(companyId, unpublishedOnly));                       
        }

        private static void PublishMediaToCdn(MediaCollection medias)
        {
            foreach (var media in medias)
            {
                foreach (MediaRatioTypeMediaEntity ratio in media.MediaRatioTypeMediaCollection)
                {
                    try
                    {
                        MediaHelper.QueueMediaRatioTypeMediaFileTask(ratio, MediaProcessingTaskEntity.ProcessingAction.Upload, true, 25, null);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("WARNING: Failed for: {0}, {1}", ratio.MediaRatioTypeMediaId, ex.Message);
                    }
                }
            } 
        }

        private static MediaCollection GetGenericMedia()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
            var mediaRatioTypeMediaPrefetch = MediaEntity.PrefetchPathMediaRatioTypeMediaCollection;
            mediaRatioTypeMediaPrefetch.Filter = new PredicateExpression(MediaRatioTypeMediaFields.LastDistributedVersionTicks == DBNull.Value);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaFields.GenericcategoryId != DBNull.Value);
            filter.AddWithOr(MediaFields.GenericproductId != DBNull.Value);
            
            MediaCollection media = new MediaCollection();
            media.GetMulti(filter, prefetch);
            
            return media;
        }

        private static MediaCollection GetEntertainmentMedia()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
            var mediaRatioTypeMediaPrefetch = MediaEntity.PrefetchPathMediaRatioTypeMediaCollection;
            mediaRatioTypeMediaPrefetch.Filter = new PredicateExpression(MediaRatioTypeMediaFields.LastDistributedVersionTicks == DBNull.Value);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaFields.EntertainmentId != DBNull.Value);

            MediaCollection media = new MediaCollection();
            media.GetMulti(filter, prefetch);

            return media;
        }

        private static MediaCollection GetMediaForCompany(int companyId, bool unpublishedOnly)
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.MediaEntity);
            var mediaRatioTypeMediaPrefetch = MediaEntity.PrefetchPathMediaRatioTypeMediaCollection;
            if (unpublishedOnly)
                mediaRatioTypeMediaPrefetch.Filter = new PredicateExpression(MediaRatioTypeMediaFields.LastDistributedVersionTicks == DBNull.Value);
            prefetch.Add(mediaRatioTypeMediaPrefetch);

            var medias = MediaHelper.GetMediaForCompany(companyId, null, 0, null, null, prefetch, false);

            return medias;
        }
    }
}
