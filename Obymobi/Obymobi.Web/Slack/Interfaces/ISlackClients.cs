﻿namespace Obymobi.Web.Slack.Interfaces
{
    public interface ISlackClients
    {
        ISlackClient GetDefaultSlackClient();
        ISlackClient GetDevelopmentNotificationsSlackClient();
        ISlackClient GetSupportNotificationsSlackClient();
    }
}
