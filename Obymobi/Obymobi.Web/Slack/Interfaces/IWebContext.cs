﻿using Obymobi.Enums;

namespace Obymobi.Web.Slack.Interfaces
{
    public interface IWebContext
    {
        CloudEnvironment Environment { get; }
    }
}
