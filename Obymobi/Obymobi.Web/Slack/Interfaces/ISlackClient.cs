﻿namespace Obymobi.Web.Slack.Interfaces
{
    public interface ISlackClient
    {
        bool SendMessage(SlackMessage message);
    }
}
