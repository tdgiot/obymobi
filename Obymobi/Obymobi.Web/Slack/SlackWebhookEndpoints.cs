﻿namespace Obymobi.Web.Slack
{
    public static class SlackWebhookEndpoints
    {
        public const string CraveCave = "https://hooks.slack.com/services/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";
        public const string DevSupportNotifications = "https://hooks.slack.com/services/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";
        public const string Test = "https://hooks.slack.com/services/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";
        public const string SlackSupportNotifications = "https://hooks.slack.com/services/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";
    }
}
