﻿using Obymobi.Enums;

namespace Obymobi.Web.Slack.Clients
{
    [SlackClient(CloudEnvironment.ProductionPrimary, SupportLevel.None)]
    [SlackClient(CloudEnvironment.ProductionSecondary, SupportLevel.None)]
    [SlackClient(CloudEnvironment.ProductionOverwrite, SupportLevel.None)]
    public class ProductionChannelSlackClient : SlackClient
    {
        public ProductionChannelSlackClient() : base(SlackWebhookEndpoints.CraveCave)
        {
        }
    }
}
