﻿using Obymobi.Enums;

namespace Obymobi.Web.Slack.Clients
{
    [SlackClient(CloudEnvironment.ProductionPrimary, SupportLevel.Development)]
    [SlackClient(CloudEnvironment.ProductionSecondary, SupportLevel.Development)]
    [SlackClient(CloudEnvironment.ProductionOverwrite, SupportLevel.Development)]
    [SlackClient(CloudEnvironment.Test, SupportLevel.CustomerService)]
    [SlackClient(CloudEnvironment.Test, SupportLevel.Development)]
    [SlackClient(CloudEnvironment.Development, SupportLevel.CustomerService)]
    [SlackClient(CloudEnvironment.Development, SupportLevel.Development)]
    public class DevSupportNotificationsSlackClient : SlackClient
    {
        public DevSupportNotificationsSlackClient() : base(SlackWebhookEndpoints.DevSupportNotifications)
        {
        }
    }
}
