﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Web.Slack.Extensions;
using Obymobi.Web.Slack.Interfaces;

namespace Obymobi.Web.Slack.Clients
{
    public class SlackClients : ISlackClients
    {
        private readonly IWebContext webContext;
        private readonly IEnumerable<ISlackClient> slackClients;

        public SlackClients(IWebContext webContext, IEnumerable<ISlackClient> slackClients)
        {
            this.webContext = webContext;
            this.slackClients = slackClients;
        }

        public ISlackClient GetDefaultSlackClient() => GetSlackClient(SupportLevel.None);

        public ISlackClient GetDevelopmentNotificationsSlackClient() => GetSlackClient(SupportLevel.Development);

        public ISlackClient GetSupportNotificationsSlackClient() => GetSlackClient(SupportLevel.CustomerService);

        private ISlackClient GetSlackClient(SupportLevel supportLevel) => CreateEmptyClientIfNoClientFound(this.slackClients.Filter(this.webContext.Environment, supportLevel)).Single();

        private IEnumerable<ISlackClient> CreateEmptyClientIfNoClientFound(IEnumerable<ISlackClient> slackClients) => slackClients.Any() ? slackClients : (new ISlackClient[] { new EmptySlackClient() });
    }
}
