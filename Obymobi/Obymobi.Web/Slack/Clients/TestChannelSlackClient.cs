﻿using Obymobi.Enums;

namespace Obymobi.Web.Slack.Clients
{
    [SlackClient(CloudEnvironment.Test, SupportLevel.None)]
    public class TestChannelSlackClient : SlackClient
    {
        public TestChannelSlackClient() : base(SlackWebhookEndpoints.Test)
        {
        }
    }
}
