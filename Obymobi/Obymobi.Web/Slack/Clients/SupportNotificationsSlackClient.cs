﻿using Obymobi.Enums;

namespace Obymobi.Web.Slack.Clients
{
    [SlackClient(CloudEnvironment.ProductionPrimary, SupportLevel.CustomerService)]
    [SlackClient(CloudEnvironment.ProductionSecondary, SupportLevel.CustomerService)]
    [SlackClient(CloudEnvironment.ProductionOverwrite, SupportLevel.CustomerService)]
    public class SupportNotificationsSlackClient : SlackClient
    {
        public SupportNotificationsSlackClient() : base(SlackWebhookEndpoints.SlackSupportNotifications)
        {
        }
    }
}
