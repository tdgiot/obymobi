﻿using Obymobi.Web.Slack.Interfaces;

namespace Obymobi.Web.Slack.Clients
{
    public class EmptySlackClient : ISlackClient
    {
        public bool SendMessage(SlackMessage message)
        {
            return true;
        }
    }
}
