﻿using System.IO;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Obymobi.Web.Slack.Interfaces;

namespace Obymobi.Web.Slack.Clients
{
    public class SlackClient : ISlackClient
    {
        private readonly string webhookUrl;

        /// <summary>
        /// Initializes a WebHookClient with a endpoint url created via Slack's Incoming WebHook integrations.
        /// </summary>
        public SlackClient(string webhookUrl)
        {
            this.webhookUrl = webhookUrl;
        }

        /// <summary>
        /// Sends a message to the client's WebHook endpoint.
        /// </summary>
        /// <returns>True if message was successfully submitted, false otherwise.</returns>
        public bool SendMessage(SlackMessage message)
        {
            var json = JsonConvert.SerializeObject(message);
            return SendPostRequest(json);
        }

        /// <summary>
        /// Asynchronously sends a message to the client's WebHook endpoint.
        /// </summary>
        /// <returns>True if message was successfully submitted, false otherwise.</returns>
        public Task<bool> SendMessageAsync(SlackMessage message)
        {
            var json = JsonConvert.SerializeObject(message);
            return SendPostRequestAsync(json);
        }

        private bool SendPostRequest(string json)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webhookUrl);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            return httpResponse.StatusCode == HttpStatusCode.Accepted;
        }

        private async Task<bool> SendPostRequestAsync(string json)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webhookUrl);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                await streamWriter.WriteAsync(json);
            }

            var httpResponse = (HttpWebResponse)await httpWebRequest.GetResponseAsync();
            return httpResponse.StatusCode == HttpStatusCode.Accepted;
        }
    }
}
