﻿using System.ComponentModel;
using System.Linq;
using Obymobi.Enums;
using Obymobi.Web.Slack.Interfaces;

namespace Obymobi.Web.Slack.Extensions
{
    public static class SlackClientTraits
    {
        public static bool Is(this ISlackClient self, CloudEnvironment cloudEnvironment, SupportLevel supportLevel)
            => TypeDescriptor.GetAttributes(self).OfType<SlackClientAttribute>().Any(attribute => attribute.CloudEnvironment == cloudEnvironment && attribute.SupportLevel == supportLevel);
    }
}
