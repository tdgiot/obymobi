﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Enums;
using Obymobi.Web.Slack.Interfaces;

namespace Obymobi.Web.Slack.Extensions
{
    public static class SlackClientsTraits
    {
        public static IEnumerable<ISlackClient> Filter(this IEnumerable<ISlackClient> self, CloudEnvironment cloudEnvironment, SupportLevel supportLevel)
            => self.Where(slackClient => slackClient.Is(cloudEnvironment, supportLevel));
    }
}
