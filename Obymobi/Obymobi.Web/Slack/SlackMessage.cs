﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Obymobi.Web.Slack
{
    public class SlackMessage
    {
        /// <summary>
        /// This is the text that will be posted to the channel.
        /// </summary>
        [JsonProperty(PropertyName = "text", NullValueHandling = NullValueHandling.Ignore, Required = Required.Always)]
        public virtual string Text { get; set; }

        /// <summary>
        /// The username you want your WebHook to post under.
        /// </summary>
        [JsonProperty(PropertyName = "username", NullValueHandling = NullValueHandling.Ignore)]
        public string Username { get; set; }

        /// <summary>
        /// The channel you want your WebHook to post the message in.
        /// </summary>
        [JsonProperty(PropertyName = "channel", NullValueHandling = NullValueHandling.Ignore)]
        public string Channel { get; set; }

        /// <summary>
        /// The channel you want your WebHook to post the message in.
        /// </summary>
        [JsonProperty(PropertyName = "link_names", NullValueHandling = NullValueHandling.Ignore)]
        public bool LinkNames { get; set; }

        /// <summary>
        /// Array of attachments to add to the message.
        /// </summary>
        [JsonProperty(PropertyName = "attachments", Required = Required.Always)]
        public IList<SlackAttachment> Attachments { get; set; }

        /// <summary>
        /// Initializes a message with the given parameters.
        /// </summary>
        public SlackMessage(string text, string username = null, string icon = null, string channel = null)
        {
            Text = text;
            Username = username;
            LinkNames = true;
            Attachments = new List<SlackAttachment>();

            // check channel
            if (!string.IsNullOrWhiteSpace(channel))
            {
                if (channel.IsValidChannel())
                {
                    Channel = channel;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("channel", "Channel must start with either '#' or '@'.");
                }
            }
            else
            {
                Channel = null;
            }
        }

        /// <summary>
        /// Initializes a message with the given parameters.
        /// </summary>
        public SlackMessage(IList<SlackAttachment> attachments = null, string username = null, string icon = null, string channel = null)
            : this(string.Empty, username, icon, channel)
        {
            Attachments = attachments ?? new List<SlackAttachment>();
        }
    }

    /// <summary>
    /// Attachment to add to the bottom of messages.
    /// </summary>
    public class SlackAttachment
    {
        /// <summary>
        /// Required text summary of the attachment that is shown by clients that understand attachments but choose not to show them.
        /// </summary>
        [JsonProperty(PropertyName = "fallback", Required = Required.Always)]
        public string Fallback { get; set; }

        /// <summary>
        /// Optional text that should appear within the attachment.
        /// </summary>
        [JsonProperty(PropertyName = "text", NullValueHandling = NullValueHandling.Ignore)]
        public string Text { get; set; }

        /// <summary>
        /// Optional text that should appear above the formatted data.
        /// </summary>
        [JsonProperty(PropertyName = "pretext", NullValueHandling = NullValueHandling.Ignore)]
        public string Pretext { get; set; }

        /// <summary>
        /// Can either be one of 'good', 'warning', 'danger', or any hex color code.
        /// </summary>
        [JsonProperty(PropertyName = "color", NullValueHandling = NullValueHandling.Ignore)]
        public string Color { get; set; }

        /// <summary>
        /// Fields are displayed in a table on the message.
        /// </summary>
        [JsonProperty(PropertyName = "fields", Required = Required.Always)]
        public IReadOnlyList<SlackAttachmentField> Fields { get; set; }

        /// <summary>
        /// Initializes an attachment with the given parameters.
        /// </summary>
        public SlackAttachment(string fallback, IReadOnlyList<SlackAttachmentField> fields, string text = null, string pretext = null, string color = null)
        {
            Fallback = fallback;
            Fields = fields;
            Text = text;
            Pretext = pretext;

            // check color
            if (!string.IsNullOrWhiteSpace(color))
            {
                if (SlackAttachmentColor.Good.Equals(color, StringComparison.InvariantCultureIgnoreCase))
                    Color = SlackAttachmentColor.Good;
                else if (SlackAttachmentColor.Warning.Equals(color, StringComparison.InvariantCultureIgnoreCase))
                    Color = SlackAttachmentColor.Warning;
                else if (SlackAttachmentColor.Danger.Equals(color, StringComparison.InvariantCultureIgnoreCase))
                    Color = SlackAttachmentColor.Danger;
                else if (color.IsValidHexColor())
                    Color = color;
                else
                    throw new ArgumentOutOfRangeException("color", "Color must be either 'good', 'warning', 'danger', or a hex color in the form of '#RRGGBB'.");
            }
            else
                Color = null;
        }
    }

    /// <summary>
    /// Field that displays text with a title on an attachment.
    /// </summary>
    public class SlackAttachmentField
    {
        /// <summary>
        /// Required Field Title. The title may not contain markup and will be escaped for you.
        /// </summary>
        [JsonProperty(PropertyName = "title", Required = Required.Always)]
        public string Title { get; set; }

        /// <summary>
        /// Text value of the field. May contain standard message markup and must be escaped as normal. May be multi-line.
        /// </summary>
        [JsonProperty(PropertyName = "value", Required = Required.Always)]
        public string Value { get; set; }

        /// <summary>
        /// Optional flag indicating whether the `value` is short enough to be displayed side-by-side with other values.
        /// </summary>
        [JsonProperty(PropertyName = "short", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Short { get; set; }

        /// <summary>
        /// Initializes a field with the given parameters.
        /// </summary>
        public SlackAttachmentField(string title, string value, bool? @short = null)
        {
            Title = title;
            Value = value;
            Short = @short;
        }
    }
}
