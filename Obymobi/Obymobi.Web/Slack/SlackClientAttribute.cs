﻿using System;
using Obymobi.Enums;

namespace Obymobi.Web.Slack
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class SlackClientAttribute : Attribute
    {
        public SlackClientAttribute(CloudEnvironment cloudEnvironment, SupportLevel supportLevel)
        {
            CloudEnvironment = cloudEnvironment;
            SupportLevel = supportLevel;
        }

        public CloudEnvironment CloudEnvironment { get; }

        public SupportLevel SupportLevel { get; }

        public override object TypeId => (CloudEnvironment, SupportLevel);
    }
}
