﻿namespace Obymobi.Web.Slack
{
    public class SlackAttachmentColor
    {
        public const string Good = "good";
        public const string Warning = "warning";
        public const string Danger = "danger";
    }
}
