﻿using Obymobi.Enums;
using Obymobi.Web.Slack.Interfaces;

namespace Obymobi.Web.Slack
{
    public class WebContext : IWebContext
    {
        public CloudEnvironment Environment
        {
            get
            {
                return WebEnvironmentHelper.CloudEnvironment;
            }
        }
    }
}
