﻿using Obymobi.Data.CollectionClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Analytics.Repositories
{
    public interface IOutletRepository
    {
        OutletCollection GetOutletsByCompanyId(IEnumerable<int> companyIds);
    }
}
