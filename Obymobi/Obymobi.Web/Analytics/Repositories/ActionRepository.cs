﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Repositories
{
    public class ActionRepository : IActionRepository
    {
        public ActionEntity Get(int? actionId)
        {
            ActionCollection actionCollection = new ActionCollection();

            if (!actionId.HasValue)
            { return default; }

            PredicateExpression filter = new PredicateExpression(ActionFields.ActionId.Equal(actionId));

            actionCollection.GetMulti(filter, 1);

            return actionCollection.FirstOrDefault();
        }

        public bool Save(ActionEntity action, bool recursive) => action.Save(recursive);
    }
}
