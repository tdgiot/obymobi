﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Repositories
{
    public class OutletRepository : IOutletRepository
    {
        public OutletCollection GetOutletsByCompanyId(IEnumerable<int> companyIds)
        {
            OutletCollection outletCollection = new OutletCollection();
            if (companyIds == null || !companyIds.Any())
            {
                return outletCollection;
            }

            PredicateExpression filter = new PredicateExpression(OutletFields.CompanyId.In(companyIds));

            outletCollection.GetMulti(filter, 0);

            return outletCollection;
        }
    }
}
