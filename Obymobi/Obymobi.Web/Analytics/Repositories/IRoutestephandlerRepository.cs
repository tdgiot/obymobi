﻿using Obymobi.Data.CollectionClasses;
using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Repositories
{
    public interface IRoutestephandlerRepository
    {
        RoutestephandlerCollection Get(IEnumerable<int> routestepHandlerIds);
        int Save(RoutestephandlerCollection routestepHandlers, bool recursive);
    }
}
