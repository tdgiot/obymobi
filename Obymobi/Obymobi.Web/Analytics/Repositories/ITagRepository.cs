﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Web.Analytics.Repositories
{
	public interface ITagRepository
	{
		TagCollection GetByCompanyId(int companyId);

        TagCollection GetTags(IEnumerable<int> tagIds);
    }
}
