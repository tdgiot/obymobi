﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Analytics.Repositories
{
    public class PaymentIntegrationConfigurationRepository : IPaymentIntegrationConfigurationRepository
    {
        public PaymentIntegrationConfigurationCollection GetByAdyenMerchantCode(string merchantCode)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PaymentIntegrationConfigurationFields.AdyenMerchantCode == merchantCode);

            RelationCollection relations = new RelationCollection();
            relations.Add(PaymentIntegrationConfigurationEntityBase.Relations.PaymentProviderEntityUsingPaymentProviderId);

            PaymentIntegrationConfigurationCollection paymentIntegrationConfigurations = new PaymentIntegrationConfigurationCollection();
            paymentIntegrationConfigurations.GetMulti(filter, relations);
            return paymentIntegrationConfigurations;
        }

        public int Save(PaymentIntegrationConfigurationCollection entities, bool recursive) => entities.SaveMulti(recursive);
    }
}
