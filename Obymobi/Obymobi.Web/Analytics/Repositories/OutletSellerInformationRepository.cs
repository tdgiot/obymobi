﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Analytics.Repositories
{
    public class OutletSellerInformationRepository : IOutletSellerInformationRepository
    {
        public OutletSellerInformationCollection Get(IEnumerable<int> ids)
        {
            OutletSellerInformationCollection sellerInfoCollection = new OutletSellerInformationCollection();
            if (ids == null || !ids.Any())
            {
                return sellerInfoCollection;
            }

            PredicateExpression filter = new PredicateExpression(OutletSellerInformationFields.OutletSellerInformationId.In(ids));

            sellerInfoCollection.GetMulti(filter, 0);

            return sellerInfoCollection;
        }

        public int Save(OutletSellerInformationCollection sellerInformations, bool recursive) => sellerInformations.SaveMulti(recursive);
    }
}
