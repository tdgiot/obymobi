﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Repositories
{
    public interface IActionRepository
    {
        ActionEntity Get(int? actionId);
        bool Save(ActionEntity action, bool recursive);
    }
}
