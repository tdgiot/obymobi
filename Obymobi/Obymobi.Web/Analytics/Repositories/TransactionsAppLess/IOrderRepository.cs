﻿using System;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Web.Analytics.Repositories.TransactionsAppLess
{
	public interface IOrderRepository
	{
        OrderCollection Get(OrdersRequest request);
	}

    public class OrdersRequest
    {
        public int CompanyId { get; set; }
        public DateTime UntilDateTimeUtc { get; set; }
        public DateTime FromDateTimeUtc { get; set; }
        public bool ShowProducts { get; set; }
        public bool ShowSystemProducts { get; set; }
        public bool ShowAlterations { get; set; }
        public bool ShowCustomerInfo { get; set; }
    }

}