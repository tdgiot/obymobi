﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Repositories.TransactionsAppLess
{
	public interface ICompanyRepository
	{
		CompanyEntity[] Get(int companyId);
		CompanyEntity[] Get(IEnumerable<int> ids);
		int Save(CompanyCollection companies, bool recursive);
	}
}