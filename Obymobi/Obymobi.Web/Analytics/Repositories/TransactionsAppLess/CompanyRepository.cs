﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using ServiceStack;

namespace Obymobi.Web.Analytics.Repositories.TransactionsAppLess
{
	public class CompanyRepository : ICompanyRepository
	{
		public CompanyEntity[] Get(int companyId)
		{
			var filter = CreateFilter(companyId);
			var includeFieldsList = CreateIncludeFieldsList();
			return FetchCompanies(filter, includeFieldsList);
		}

		public CompanyEntity[] Get(IEnumerable<int> ids)
		{
			CompanyCollection companyCollection = new CompanyCollection();
			if (ids == null || !ids.Any())
			{
				return companyCollection.ToArray();
			}

			PredicateExpression filter = new PredicateExpression(CompanyFields.CompanyId.In(ids));

			companyCollection.GetMulti(filter, 0);

			return companyCollection.ToArray();
		}

		private static CompanyEntity[] FetchCompanies(IPredicate filter, ExcludeIncludeFieldsList includeFieldsList)
		{
			var companyCollection = new CompanyCollection();
			// TODO: use datasource instead of the CompanyCollection to call GetMulti on.
			if (!companyCollection.GetMulti(filter, includeFieldsList, null))
			{
				throw new InvalidOperationException($"Failed to get the company from the database. Filter:{Environment.NewLine}{filter.ToJson()}");
			}
			return companyCollection.ToArray();
		}

		private static IncludeFieldsList CreateIncludeFieldsList() =>
			new IncludeFieldsList
			{
				CompanyFields.Name,
                CompanyFields.CultureCode,
                CompanyFields.CurrencyCode,
                CompanyFields.CurrencyId,
				CompanyFields.TimeZoneOlsonId
            };

		private static IPredicateExpression CreateFilter(int companyId) =>
			new PredicateExpression()
				.Add(CompanyFields.CompanyId == companyId);

		public int Save(CompanyCollection companies, bool recursive) => companies.SaveMulti(recursive);
	}
}