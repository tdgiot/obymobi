﻿using System;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Web.Analytics.Repositories.TransactionsAppLess
{
	public class OrderRepository : IOrderRepository
	{
		public OrderCollection Get(OrdersRequest request)
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();

			CreateRelationCollection(bucket);
			CreateFilter(request, bucket);
			IPrefetchPath prefetchPath = CreatePrefetchPath(request);
			ISortExpression sortExpression = CreateSortExpression();

			OrderCollection orderCollection = FetchOrders(bucket, sortExpression, prefetchPath);
			return orderCollection;
		}

		private static OrderCollection FetchOrders(IRelationPredicateBucket bucket, ISortExpression sort, IPrefetchPath prefetchPath)
		{
			OrderCollection orderCollection = new OrderCollection();
			orderCollection.GetMulti(bucket.PredicateExpression, 0, sort, bucket.Relations, prefetchPath);

			return orderCollection;
		}

		private static void CreateFilter(OrdersRequest request, IRelationPredicateBucket bucket)
		{
			// Basic filter
			IPredicateExpression filter = new PredicateExpression()
				.Add(OrderFields.CompanyId == request.CompanyId)
				.Add(OrderFields.CreatedUTC >= request.FromDateTimeUtc)
				.Add(OrderFields.CreatedUTC <= request.UntilDateTimeUtc);

			// Add order status filter
			filter.Add(CreateOrderStatusFilter());

			bucket.PredicateExpression.Add(filter);
		}

		private static IPredicateExpression CreateOrderStatusFilter()
		{
			// - Orders not linked to a payment, but are processed
			IPredicateExpression notPaidOrdersFilter = new PredicateExpression();
			notPaidOrdersFilter.Add(PaymentTransactionFields.PaymentTransactionId == DBNull.Value);
			notPaidOrdersFilter.Add(OrderFields.Status == OrderStatus.Processed);

			// - Orders linked to a payment transaction with the status PAID
			IPredicateExpression paidOrdersFilter = new PredicateExpression();
			paidOrdersFilter.Add(PaymentTransactionFields.Status == PaymentTransactionStatus.Paid);

			// - Orders linked to a master order. This filter is needed otherwise the relation filter doesn't work ¯\_(ツ)_/¯
			IPredicateExpression subOrdersFilter = new PredicateExpression();
			subOrdersFilter.Add(OrderFields.MasterOrderId != DBNull.Value);

			// Combine status filters with OR
			IPredicateExpression orderStatusFilter = new PredicateExpression();
			orderStatusFilter.Add(notPaidOrdersFilter);
			orderStatusFilter.AddWithOr(paidOrdersFilter);
			orderStatusFilter.AddWithOr(subOrdersFilter);

			return orderStatusFilter;
		}

		private static ISortExpression CreateSortExpression()
		{
			SortExpression sort = new SortExpression();
			sort.Add(OrderFields.OrderId | SortOperator.Ascending);

			return sort;
		}

		private static void CreateRelationCollection(IRelationPredicateBucket bucket)
		{
			bucket.Relations.Add(OrderEntityBase.Relations.OrderitemEntityUsingOrderId); // Order => Orderitem
			bucket.Relations.Add(OrderitemEntityBase.Relations.OrderitemAlterationitemEntityUsingOrderitemId, JoinHint.Left); // Orderitem => OrderitemAlterationitem
            bucket.Relations.Add(OrderEntityBase.Relations.PaymentTransactionEntityUsingOrderId, JoinHint.Left); // Order => PaymentTransaction
			bucket.Relations.Add(OrderEntityBase.Relations.DeliveryInformationEntityUsingDeliveryInformationId, JoinHint.Left); // Order => DeliveryInformation

			// Include sub-order where master order is processed
			PredicateExpression masterOrderFilter = new PredicateExpression();
			masterOrderFilter.Add(OrderFields.Status.SetObjectAlias("MasterOrder") == OrderStatus.Processed);
			bucket.Relations.Add(OrderEntityBase.Relations.OrderEntityUsingMasterOrderId, "MasterOrder", JoinHint.Left).CustomFilter = masterOrderFilter; // Order => Order
		}

		private static IPrefetchPath CreatePrefetchPath(OrdersRequest request)
		{
			PrefetchPath prefetch = new PrefetchPath(EntityType.OrderEntity);

			if (request.ShowProducts || request.ShowSystemProducts)
			{
				SortExpression orderitemSort = new SortExpression(OrderitemFields.Type | SortOperator.Ascending);

				IPrefetchPathElement orderitemPrefetchPath = prefetch.Add(OrderEntityBase.PrefetchPathOrderitemCollection, 0, null, null, orderitemSort);

				if (request.ShowAlterations)
				{
					orderitemPrefetchPath.SubPath.Add(OrderitemEntityBase.PrefetchPathOrderitemAlterationitemCollection)
                                         .SubPath.Add(OrderitemAlterationitemEntityBase.PrefetchPathOrderitemAlterationitemTagCollection, new IncludeFieldsList(OrderitemAlterationitemTagFields.TagId));
				}

				orderitemPrefetchPath.SubPath.Add(OrderitemEntityBase.PrefetchPathOrderitemTagCollection, new IncludeFieldsList(OrderitemTagFields.TagId));
			}

			if (request.ShowCustomerInfo)
			{
				prefetch.Add(OrderEntityBase.PrefetchPathDeliveryInformationEntity);
			}

			return prefetch.Count == 0 ? null : prefetch;
		}
	}
}
