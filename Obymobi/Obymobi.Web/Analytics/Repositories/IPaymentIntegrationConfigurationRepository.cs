﻿using Obymobi.Data.CollectionClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Analytics.Repositories
{
    public interface IPaymentIntegrationConfigurationRepository
    {
        PaymentIntegrationConfigurationCollection GetByAdyenMerchantCode(string merchantCode);
        int Save(PaymentIntegrationConfigurationCollection entities, bool recursive);
    }
}
