﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Obymobi.Web.Analytics.Repositories
{
	public class TagRepository : ITagRepository
	{
		public TagCollection GetByCompanyId(int companyId)
		{
			PredicateExpression filter = new PredicateExpression();
			filter.Add(TagFields.CompanyId == companyId);
			filter.AddWithOr(TagFields.CompanyId == DBNull.Value);

			SortExpression sort = new SortExpression();
			sort.Add(TagFields.CompanyId.Ascending());
			sort.Add(TagFields.Name.Ascending());

			TagCollection tagCollection = new TagCollection();
			tagCollection.GetMulti(filter, 0, sort);
			return tagCollection;
		}

        public TagCollection GetTags(IEnumerable<int> tagIds)
		{
			TagCollection tagCollection = new TagCollection();
			if (tagIds == null || !tagIds.Any())
            {
                return tagCollection;
            }

            PredicateExpression filter = new PredicateExpression(TagFields.TagId.In(tagIds));
            SortExpression sort = new SortExpression(TagFields.Name.Ascending());

            tagCollection.GetMulti(filter, 0, sort);

            return tagCollection;
        }
	}
}
