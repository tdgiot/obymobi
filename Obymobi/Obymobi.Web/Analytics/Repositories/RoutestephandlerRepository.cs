﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Analytics.Repositories
{
    public class RoutestephandlerRepository : IRoutestephandlerRepository
    {
        public RoutestephandlerCollection Get(IEnumerable<int> routestepHandlerIds)
        {
            RoutestephandlerCollection handlerCollection = new RoutestephandlerCollection();
            if (routestepHandlerIds == null || !routestepHandlerIds.Any())
            {
                return handlerCollection;
            }

            PredicateExpression filter = new PredicateExpression(RoutestephandlerFields.RoutestephandlerId.In(routestepHandlerIds));

            handlerCollection.GetMulti(filter, 0);

            return handlerCollection;
        }

        public int Save(RoutestephandlerCollection routestepHandlers, bool recursive) => routestepHandlers.SaveMulti(recursive);
    }
}
