﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Analytics.Repositories
{
    public interface IOutletSellerInformationRepository
    {
        OutletSellerInformationCollection Get(IEnumerable<int> ids);
        int Save(OutletSellerInformationCollection sellerInformations, bool recursive);
    }
}
