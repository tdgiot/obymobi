﻿namespace Obymobi.Web.Analytics.DomainTypes
{
    public readonly struct IncludedServiceMethods
	{
		private int[] IncludedIds { get; }

		private IncludedServiceMethods(int[] includedIds) => IncludedIds = includedIds;

		public static implicit operator int[](IncludedServiceMethods value) => value.IncludedIds;
		public static implicit operator IncludedServiceMethods(int[] value) => new IncludedServiceMethods(value);
	}
}
