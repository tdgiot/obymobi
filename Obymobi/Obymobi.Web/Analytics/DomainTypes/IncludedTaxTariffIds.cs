﻿namespace Obymobi.Web.Analytics.DomainTypes
{
	public readonly struct IncludedTaxTariffIds
	{
		private int[] IncludedIds { get; }

		private IncludedTaxTariffIds(int[] includedIds) => IncludedIds = includedIds;

		public static implicit operator int[](IncludedTaxTariffIds value) => value.IncludedIds;
		public static implicit operator IncludedTaxTariffIds(int[] value) => new IncludedTaxTariffIds(value);
	}
}
