﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct ShowProductsOption
    {
        private bool Value { get; }

        private ShowProductsOption(bool value) => Value = value;

        public static implicit operator bool(ShowProductsOption showProductsOption) => showProductsOption.Value;

        public static implicit operator ShowProductsOption(bool value) => new ShowProductsOption(value);
    }
}
