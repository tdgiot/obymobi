﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct OutletId
    {
        private int? Value { get; }

        private OutletId(int? value) => Value = value;

        public static implicit operator int?(OutletId showProductsOption) => showProductsOption.Value;

        public static implicit operator OutletId(int? value) => new OutletId(value);
    }
}
