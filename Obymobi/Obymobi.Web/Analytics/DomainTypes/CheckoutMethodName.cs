using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct CheckoutMethodName
    {
        private string Value { get; }

        private CheckoutMethodName(string value) => Value = value;

        public static implicit operator string(CheckoutMethodName serviceMethodName) => serviceMethodName.Value;

        public static implicit operator CheckoutMethodName(string value) => new CheckoutMethodName(value);
    }
}
