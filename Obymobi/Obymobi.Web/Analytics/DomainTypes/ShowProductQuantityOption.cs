﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
	[DebuggerDisplay("{Value}")]

	public readonly struct ShowProductQuantityOption
	{
		private bool Value { get; }

		private ShowProductQuantityOption(bool value) => Value = value;

		public static implicit operator bool(ShowProductQuantityOption showProductQuantityOption) => showProductQuantityOption.Value;

		public static implicit operator ShowProductQuantityOption(bool value) => new ShowProductQuantityOption(value);
	}
}