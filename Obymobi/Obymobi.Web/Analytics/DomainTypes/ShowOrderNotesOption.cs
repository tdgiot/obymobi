﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
	[DebuggerDisplay("{Value}")]
	public readonly struct ShowOrderNotesOption
	{
		private bool Value { get; }

		private ShowOrderNotesOption(bool value) => Value = value;

		public static implicit operator bool(ShowOrderNotesOption showOrderNotesOption) => showOrderNotesOption.Value;

		public static implicit operator ShowOrderNotesOption(bool value) => new ShowOrderNotesOption(value);
	}
}