﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct CategoryId
    {
        private int? Value { get; }

        private CategoryId(int? value) => Value = value;

        public static implicit operator int?(CategoryId categoryId) => categoryId.Value;

        public static implicit operator CategoryId(int? value) => new CategoryId(value);
    }
}
