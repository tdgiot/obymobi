using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct ShowCardSummary
    {
        private bool Value { get; }

        private ShowCardSummary(bool value) => this.Value = value;

        public static implicit operator bool(ShowCardSummary showCardSummary) => showCardSummary.Value;

        public static implicit operator ShowCardSummary(bool value) => new ShowCardSummary(value);
    }
}