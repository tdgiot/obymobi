﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
	[DebuggerDisplay("{Value}")]

	public readonly struct ShowSystemProductsOption
	{
		private bool Value { get; }

		private ShowSystemProductsOption(bool value) => Value = value;

		public static implicit operator bool(ShowSystemProductsOption showSystemProductsOption) => showSystemProductsOption.Value;

		public static implicit operator ShowSystemProductsOption(bool value) => new ShowSystemProductsOption(value);
	}
}