﻿namespace Obymobi.Web.Analytics.DomainTypes
{
    public readonly struct CategoryFilterIds
	{
		private int[] IncludedIds { get; }

		private CategoryFilterIds(int[] includedIds) => IncludedIds = includedIds;

		public static implicit operator int[](CategoryFilterIds value) => value.IncludedIds;
		public static implicit operator CategoryFilterIds(int[] value) => new CategoryFilterIds(value);
	}
}
