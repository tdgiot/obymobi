﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace Obymobi.Web.Analytics.DomainTypes
{
	[DebuggerDisplay("{Value}")]
	public readonly struct FromDateTime : IFormattable
	{
		private FromDateTime(DateTime value) => Value = value;

		private DateTime Value { get; }

		public static implicit operator DateTime(FromDateTime fromDateTime) => fromDateTime.Value;

		public static implicit operator FromDateTime(DateTime value) => new FromDateTime(value);

		public override string ToString() => Value.ToString(CultureInfo.InvariantCulture);

		public string ToString(string format, IFormatProvider formatProvider) => Value.ToString(format, formatProvider);
	}
}