using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct ShowPaymentMethod
    {
        private bool Value { get; }

        private ShowPaymentMethod(bool value) => this.Value = value;

        public static implicit operator bool(ShowPaymentMethod showPaymentMethod) => showPaymentMethod.Value;

        public static implicit operator ShowPaymentMethod(bool value) => new ShowPaymentMethod(value);
    }
}