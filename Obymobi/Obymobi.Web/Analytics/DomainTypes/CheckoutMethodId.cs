﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct CheckoutMethodId
    {
        private int Value { get; }

        private CheckoutMethodId(int value) => Value = value;

        public static implicit operator int(CheckoutMethodId showProductsOption) => showProductsOption.Value;

        public static implicit operator CheckoutMethodId(int value) => new CheckoutMethodId(value);
    }
}
