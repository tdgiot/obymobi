﻿using Obymobi.Web.Analytics.Model.Orders;

namespace Obymobi.Web.Analytics.DomainTypes
{
	public readonly struct IncludedSystemProductTypes
	{
		private int[] IncludedTypes { get; }

		private IncludedSystemProductTypes(int[] includedSystemProductTypes) => IncludedTypes = includedSystemProductTypes;

		public static implicit operator int[](IncludedSystemProductTypes value) => value.IncludedTypes;
		public static implicit operator IncludedSystemProductTypes(int[] value) => new IncludedSystemProductTypes(value);
	}
}
