﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
	[DebuggerDisplay("{Value}")]

	public readonly struct ShowProductPriceOption
	{
		private bool Value { get; }

		private ShowProductPriceOption(bool value) => Value = value;

		public static implicit operator bool(ShowProductPriceOption showProductPriceOption) => showProductPriceOption.Value;

		public static implicit operator ShowProductPriceOption(bool value) => new ShowProductPriceOption(value);
	}
}