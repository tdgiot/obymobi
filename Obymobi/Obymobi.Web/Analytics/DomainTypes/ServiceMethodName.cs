﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct ServiceMethodName
    {
        private string Value { get; }

        private ServiceMethodName(string value) => Value = value;

        public static implicit operator string(ServiceMethodName serviceMethodName) => serviceMethodName.Value;

        public static implicit operator ServiceMethodName(string value) => new ServiceMethodName(value);
    }
}
