using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct ShowCheckoutMethod
    {
        private bool Value { get; }

        private ShowCheckoutMethod(bool value) => Value = value;

        public static implicit operator bool(ShowCheckoutMethod showCheckoutMethod) => showCheckoutMethod.Value;

        public static implicit operator ShowCheckoutMethod(bool value) => new ShowCheckoutMethod(value);
    }
}
