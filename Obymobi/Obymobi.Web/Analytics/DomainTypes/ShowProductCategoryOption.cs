﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]

	public readonly struct ShowProductCategoryOption
	{
		private bool Value { get; }

		private ShowProductCategoryOption(bool value) => Value = value;

		public static implicit operator bool(ShowProductCategoryOption showProductsOption) => showProductsOption.Value;

		public static implicit operator ShowProductCategoryOption(bool value) => new ShowProductCategoryOption(value);
	}
}
