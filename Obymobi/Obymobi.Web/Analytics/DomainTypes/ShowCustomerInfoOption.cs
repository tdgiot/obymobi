﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
	[DebuggerDisplay("{Value}")]
	public readonly struct ShowCustomerInfoOption
	{
		private bool Value { get; }

		private ShowCustomerInfoOption(bool value) => Value = value;

		public static implicit operator bool(ShowCustomerInfoOption showCustomerInfoOption) => showCustomerInfoOption.Value;

		public static implicit operator ShowCustomerInfoOption(bool value) => new ShowCustomerInfoOption(value);
	}
}