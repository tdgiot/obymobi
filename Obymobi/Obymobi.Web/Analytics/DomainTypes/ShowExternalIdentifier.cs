﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]

	public readonly struct ShowExternalIdentifier
	{
		private bool Value { get; }

		private ShowExternalIdentifier(bool value) => Value = value;

		public static implicit operator bool(ShowExternalIdentifier showExternalIdentifier) => showExternalIdentifier.Value;

		public static implicit operator ShowExternalIdentifier(bool value) => new ShowExternalIdentifier(value);
	}
}
