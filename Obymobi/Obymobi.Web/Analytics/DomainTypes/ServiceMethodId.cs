﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct ServiceMethodId
    {
        private int Value { get; }

        private ServiceMethodId(int value) => Value = value;

        public static implicit operator int(ServiceMethodId serviceMethodId) => serviceMethodId.Value;

        public static implicit operator ServiceMethodId(int value) => new ServiceMethodId(value);
    }
}
