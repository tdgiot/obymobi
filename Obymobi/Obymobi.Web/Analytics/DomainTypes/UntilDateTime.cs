﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace Obymobi.Web.Analytics.DomainTypes
{
	[DebuggerDisplay("{Value}")]

	public readonly struct UntilDateTime : IFormattable
	{
		private UntilDateTime(DateTime value) => Value = value;

		private DateTime Value { get; }

		public static implicit operator DateTime(UntilDateTime untilDateTime) => untilDateTime.Value;

		public static implicit operator UntilDateTime(DateTime value) => new UntilDateTime(value);

		public override string ToString() => Value.ToString(CultureInfo.InvariantCulture);

		public string ToString(string format, IFormatProvider formatProvider) => Value.ToString(format, formatProvider);
	}
}