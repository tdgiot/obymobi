﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct CategoryName
    {
        private string Value { get; }

        private CategoryName(string value) => Value = value;

        public static implicit operator string(CategoryName categoryName) => categoryName.Value;

        public static implicit operator CategoryName(string value) => new CategoryName(value);
    }
}
