﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct DeliverypointName
    {
        private string Value { get; }

        private DeliverypointName(string value) => this.Value = value;

        public static implicit operator string(DeliverypointName serviceMethodName) => serviceMethodName.Value;

        public static implicit operator DeliverypointName(string value) => new DeliverypointName(value);
    }
}