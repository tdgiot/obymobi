﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct ShowServiceMethod
    {
        private bool Value { get; }

        private ShowServiceMethod(bool value) => Value = value;

        public static implicit operator bool(ShowServiceMethod showAlterationsOption) => showAlterationsOption.Value;

        public static implicit operator ShowServiceMethod(bool value) => new ShowServiceMethod(value);
    }
}
