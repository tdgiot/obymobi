﻿namespace Obymobi.Web.Analytics.DomainTypes
{
    public readonly struct IncludedCheckoutMethods
	{
		private int[] IncludedIds { get; }

		private IncludedCheckoutMethods(int[] includedIds) => IncludedIds = includedIds;

		public static implicit operator int[](IncludedCheckoutMethods value) => value.IncludedIds;
		public static implicit operator IncludedCheckoutMethods(int[] value) => new IncludedCheckoutMethods(value);
	}
}
