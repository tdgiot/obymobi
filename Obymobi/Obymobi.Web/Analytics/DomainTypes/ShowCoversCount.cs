﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct ShowCoversCount
    {
        private bool Value { get; }

        private ShowCoversCount(bool value) => this.Value = value;

        public static implicit operator bool(ShowCoversCount showDeliveryPoint) => showDeliveryPoint.Value;

        public static implicit operator ShowCoversCount(bool value) => new ShowCoversCount(value);
    }
}