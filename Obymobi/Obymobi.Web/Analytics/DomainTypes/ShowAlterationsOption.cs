﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
	[DebuggerDisplay("{Value}")]
	public readonly struct ShowAlterationsOption
	{
		private bool Value { get; }

		private ShowAlterationsOption(bool value) => Value = value;

		public static implicit operator bool(ShowAlterationsOption showAlterationsOption) => showAlterationsOption.Value;

		public static implicit operator ShowAlterationsOption(bool value) => new ShowAlterationsOption(value);
	}
}