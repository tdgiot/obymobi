﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.DomainTypes
{
    [DebuggerDisplay("{Value}")]
    public readonly struct ShowDeliveryPoint
    {
        private bool Value { get; }

        private ShowDeliveryPoint(bool value) => this.Value = value;

        public static implicit operator bool(ShowDeliveryPoint showDeliveryPoint) => showDeliveryPoint.Value;

        public static implicit operator ShowDeliveryPoint(bool value) => new ShowDeliveryPoint(value);
    }
}