﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Web.Analytics.Model.Orders
{
    public static class OrderitemAlterationitemEntityTraits
    {
        public static bool InheritsTaxTariff(this OrderitemAlterationitemEntity alterationitemEntity, OrderitemEntity parentOrderItem) 
            => alterationitemEntity.TaxTariffId == null && parentOrderItem.TaxTariffId.HasValue;
    }
}
