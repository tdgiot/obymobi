using System;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.Orders
{
    public class Order
    {
        private readonly List<int> subOrderNumbers = new List<int>();

        public Order(DateTime? orderDate,
            int orderNumber,
            string serviceMethodName,
            string checkoutMethodName,
            decimal orderTotal,
            OrderItemSale[] orderItems,
            string customerName,
            string customerPhoneNumber,
            string customerEmail,
            string notes,
            string building,
            string address,
            string postCode,
            string city,
            string instructions,
            string paymentMethod,
            string cardSummary,
            string deliveryPoint,
            string coversCount)
        {
            OrderDate = orderDate;
            OrderNumber = orderNumber;
            ServiceMethodName = serviceMethodName;
            CheckoutMethodName = checkoutMethodName;
            OrderTotal = orderTotal;
            OrderItems = orderItems;
            CustomerName = customerName;
            CustomerPhoneNumber = customerPhoneNumber;
            CustomerEmail = customerEmail;
            Notes = notes;
            Building = building;
            Address = address;
            PostCode = postCode;
            City = city;
            Instructions = instructions;
            PaymentMethod = paymentMethod;
            CardSummary = cardSummary;
            DeliveryPoint = deliveryPoint;
            CoversCount = coversCount;
        }

        public DateTime? OrderDate { get; }

        public int OrderNumber { get; }

        public string SubOrderNumbers => string.Join(", ", this.subOrderNumbers);

        public string ServiceMethodName { get; }

        public string CheckoutMethodName { get; }

        public decimal OrderTotal { get; }

        public OrderItemSale[] OrderItems { get; private set; }

        public string CustomerName { get; }

        public string CustomerPhoneNumber { get; }

        public string CustomerEmail { get; }

        public string Building { get; }

        public string Address { get; }

        public string PostCode { get; }

        public string City { get; }

        public string Notes { get; }

        public string Instructions { get; }

        public string PaymentMethod { get; }

        public string CardSummary { get; }

        public string DeliveryPoint { get; }

        public string CoversCount { get; }

        public void AddExtraOrderItems(int subOrderNumber, OrderItemSale[] orderItems)
        {
            this.subOrderNumbers.Add(subOrderNumber);
            OrderItems = OrderItems.Concat(orderItems).OrderBy(item => item.Type).ToArray();
        }
    }
}
