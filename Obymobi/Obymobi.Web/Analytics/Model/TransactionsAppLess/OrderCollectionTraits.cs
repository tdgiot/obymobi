﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.InventoryReport;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Model.ProductReport;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess
{
	public static class OrderCollectionTraits
	{
		public static Orders.Orders ToOrderModels(this OrderCollection self, TransactionsAppLessSheetFilter sheetFilter)
		{
			OrderCollection filteredOrderCollection = new OrderCollection(sheetFilter.CreateOrderCriteria().MeetCriteria(self));

			ICriteria<OrderitemEntity> orderItemCriteria = sheetFilter.CreateOrderItemCriteria();

			IEnumerable<Order> orderList = sheetFilter.MapOrderEntitiesToModels(filteredOrderCollection, orderItemCriteria)
				.Where(order => !((order.CustomerEmail ?? "").EndsWith("@crave-emenu.com", System.StringComparison.CurrentCultureIgnoreCase)));

			Orders.Orders orders = new Orders.Orders();
			orders.SetOrders(orderList);
			return orders;
		}

		public static IEnumerable<ProductSale> ToOrderModels(this OrderCollection self, ProductReportSheetFilter sheetFilter)
		{
			ICriteria<OrderEntity> orderCriteria = sheetFilter.CreateOrderCriteria();
			ICriteria<OrderitemEntity> orderItemCriteria = sheetFilter.CreateOrderItemCriteria();
			ICriteria<OrderitemAlterationitemEntity> alterationItemCriteria = sheetFilter.CreateOrderItemAlterationsCriteria();
			OrderCollection filteredOrderCollection = new OrderCollection(orderCriteria.MeetCriteria(self));

			return sheetFilter.MapOrderEntitiesToModels(filteredOrderCollection, orderCriteria, orderItemCriteria, alterationItemCriteria);
		}

		public static IEnumerable<InventoryItem> ToOrderModels(this OrderCollection self, InventoryReportSheetFilter sheetFilter)
		{
			ICriteria<OrderEntity> orderCriteria = sheetFilter.CreateOrderCriteria();
			ICriteria<OrderitemEntity> orderItemCriteria = sheetFilter.CreateOrderItemCriteria();
			ICriteria<OrderitemAlterationitemEntity> alterationItemCriteria = sheetFilter.CreateOrderItemAlterationsCriteria();
			OrderCollection filteredOrderCollection = new OrderCollection(orderCriteria.MeetCriteria(self));

			return sheetFilter.MapOrderEntitiesToModels(filteredOrderCollection, orderCriteria, orderItemCriteria, alterationItemCriteria);
		}
	}
}
