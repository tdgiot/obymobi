﻿using System;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;

namespace Obymobi.Web.Analytics.Model.Orders
{
	public class Company
	{
		public Company()
		{
		}

		public Company(int id, string name, string currencyCode, TimeZoneInfo timeZoneInfo)
		{
			Id = id;
			Name = name;
			CurrencyCode = currencyCode;
			TimeZoneInfo = timeZoneInfo;
		}

		public int Id { get; }
		public string Name { get; }

		public Currency Currency => (Currency) CurrencyCode;
		public string CurrencyCode { get; }
		public TimeZoneInfo TimeZoneInfo { get; }
	}

	public static class CompanyTraits
	{
		public static Company FetchCompany(this Company self, ICompanyRepository companyRepository, int companyId)
		{
			CompanyEntity companyEntity = companyRepository.Get(companyId).First();
			return new Company(companyEntity.CompanyId, companyEntity.Name, companyEntity.CurrencyCode, companyEntity.TimeZoneInfo);
		}
	}
}