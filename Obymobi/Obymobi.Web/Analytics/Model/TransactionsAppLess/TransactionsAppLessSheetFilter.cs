using Dionysos;
using Obymobi.Web.Analytics.Model.ModelBase;
using System.Collections.Generic;
using System.Text;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess
{
    public class TransactionsAppLessSheetFilter : SpreadSheetFilter
    {
        public bool ShowProducts { get; set; } = true;
        public bool ShowProductQuantity { get; set; } = true;
        public bool ShowProductPrice { get; set; } = true;
        public bool ShowProductCategory { get; set; }
        public bool ShowServiceMethod { get; set; } = true;
        public bool ShowCheckoutMethod { get; set; }
        public bool ShowCustomerInfo { get; set; }
        public bool ShowOrderNotes { get; set; } = true;
        public bool ShowPaymentMethod { get; set; }
        public bool ShowCardSummary { get; set; }
        public bool ShowDeliveryPoint { get; set; }
        public bool ShowCoversCount { get; set; }
        public IReadOnlyList<int> IncludedSystemProductTypes { get; set; } = new List<int>();
        public IReadOnlyList<int> IncludeTaxTariffIds { get; set; } = new List<int>();

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendFormatLine("=== Sheet Filter: {0} (id: {1})", Name, Id);
            sb.AppendFormatLine("Active: {0}", Active);
            sb.AppendFormatLine("Outlet Id: {0}", OutletId?.ToString() ?? "None");
            sb.AppendFormatLine("- Service methods: {0}", string.Join(", ", ServiceMethods));
            sb.AppendFormatLine("- Checkout methods: {0}", string.Join(", ", CheckoutMethods));
            sb.AppendFormatLine("Show products: {0}", ShowProducts);
            sb.AppendFormatLine("- Show product quantity: {0}", ShowProductQuantity);
            sb.AppendFormatLine("- Show product price: {0}", ShowProductPrice);
            sb.AppendFormatLine("- Show product category: {0}", ShowProductCategory);
            sb.AppendFormatLine("- Show alterations: {0}", ShowAlterations);
            sb.AppendFormatLine("Show service method: {0}", this.ShowServiceMethod);
            sb.AppendFormatLine("Show checkout method: {0}", this.ShowCheckoutMethod);
            sb.AppendFormatLine("Show order notes: {0}", ShowOrderNotes);
            sb.AppendFormatLine("Show payment method: {0}", ShowPaymentMethod);
            sb.AppendFormatLine("Show card summary: {0}", ShowCardSummary);
            sb.AppendFormatLine("Show delivery point: {0}", ShowDeliveryPoint);
            sb.AppendFormatLine("Show covers count: {0}", ShowCoversCount);
            sb.AppendFormatLine("Show customer info: {0}", ShowCustomerInfo);
            sb.AppendFormatLine("Show system products: {0}", ShowSystemProducts);
            sb.AppendFormatLine("- Product types: {0}", string.Join(", ", IncludedSystemProductTypes));
            sb.AppendFormatLine("Category filter: {0}", CategoryFilterType);
            sb.AppendFormatLine("- Selected category ids: {0}", string.Join(", ", CategoryFilterIds));
            sb.AppendFormatLine("Tag filter:");
            sb.AppendFormatLine("- Selected tags: {0}", string.Join(", ", IncludedTagIds));

            return sb.ToString();
        }
    }
}