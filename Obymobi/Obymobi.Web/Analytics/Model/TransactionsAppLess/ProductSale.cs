﻿namespace Obymobi.Web.Analytics.Model.Orders
{
	public class ProductSale : ProductSaleBase
	{
		public ProductSale(string name, string externalIdentifier, int quantity, decimal price) 
			: base(name, externalIdentifier, quantity, price)
		{
		}

		public decimal TotalPrice => Quantity * Price;
	}
}
