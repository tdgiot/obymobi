﻿namespace Obymobi.Web.Analytics.Model.Orders
{
	public abstract class ProductSaleBase
	{
		protected ProductSaleBase(string name, string externalIdentifier, int quantity, decimal price)
		{
			Name = name;
            ExternalIdentifier = externalIdentifier;
			Quantity = quantity;
			Price = price;
		}

		public string Name { get; }
		public string ExternalIdentifier { get; }
		public int Quantity { get; }
		public decimal Price { get; }
	}
}
