﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrderOutletIdCriteria : ICriteria<OrderEntity>
    {
        public OrderOutletIdCriteria(int outletId) => OutletId = outletId;

        private int OutletId { get; }

        public IEnumerable<OrderEntity> MeetCriteria(IEnumerable<OrderEntity> items) => items.Where(item => item.OutletId == OutletId);
    }
}
