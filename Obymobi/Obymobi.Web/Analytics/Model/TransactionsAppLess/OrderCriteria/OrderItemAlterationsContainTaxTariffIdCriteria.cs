﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrderItemAlterationsContainTaxTariffIdCriteria : ICriteria<OrderitemEntity>
    {
        private IEnumerable<int> IncludedTaxTariffIds { get; }

        public OrderItemAlterationsContainTaxTariffIdCriteria(IEnumerable<int> includedTaxTariffIds) => IncludedTaxTariffIds = includedTaxTariffIds;

        public IEnumerable<OrderitemEntity> MeetCriteria(IEnumerable<OrderitemEntity> items) => items.Where(orderItem => orderItem.OrderitemAlterationitemCollection.Any(alterationItem => IncludedTaxTariffIds.Contains(alterationItem.TaxTariffId)));
    }
}
