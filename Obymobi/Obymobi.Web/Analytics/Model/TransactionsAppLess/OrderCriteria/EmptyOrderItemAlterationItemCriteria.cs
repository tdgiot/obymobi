﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    [EmptyCriteria]
    public class EmptyOrderItemAlterationItemCriteria : ICriteria<OrderitemAlterationitemEntity>
    {
        public IEnumerable<OrderitemAlterationitemEntity> MeetCriteria(IEnumerable<OrderitemAlterationitemEntity> items) => items;
    }
}
