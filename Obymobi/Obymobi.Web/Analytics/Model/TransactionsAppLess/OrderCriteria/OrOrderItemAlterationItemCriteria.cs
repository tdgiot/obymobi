﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrOrderItemAlterationItemCriteria : OrCriteria<OrderitemAlterationitemEntity>
    {
        private OrOrderItemAlterationItemCriteria(ICriteria<OrderitemAlterationitemEntity> criteria, ICriteria<OrderitemAlterationitemEntity> otherCriteria) : base(criteria, otherCriteria)
        {
        }

        public static ICriteria<OrderitemAlterationitemEntity> Create(ICriteria<OrderitemAlterationitemEntity> criteria, ICriteria<OrderitemAlterationitemEntity> otherCriteria)
        {
            if (criteria != null && otherCriteria != null)
            {
                return new OrOrderItemAlterationItemCriteria(criteria, otherCriteria);
            }

            if (criteria != null)
            {
                return criteria;
            }
            else if (otherCriteria != null)
            {
                return otherCriteria;
            }

            return new EmptyOrderItemAlterationItemCriteria();
        }
    }
}
