﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    [EmptyCriteria]
    public class EmptyOrderCriteria : ICriteria<OrderEntity>
    {
        public IEnumerable<OrderEntity> MeetCriteria(IEnumerable<OrderEntity> items) => items;
    }
}
