﻿using System;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class EmptyCriteriaAttribute : Attribute
    {
    }
}
