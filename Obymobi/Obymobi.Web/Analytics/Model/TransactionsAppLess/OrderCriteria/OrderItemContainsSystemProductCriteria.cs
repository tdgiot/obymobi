﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrderItemContainsSystemProductCriteria : ICriteria<OrderitemEntity>
    {
        private IEnumerable<OrderitemType> IncludedSystemProductTypes { get; }

        public OrderItemContainsSystemProductCriteria(IEnumerable<OrderitemType> includedSystemProductTypes) => IncludedSystemProductTypes = includedSystemProductTypes;

        public IEnumerable<OrderitemEntity> MeetCriteria(IEnumerable<OrderitemEntity> items) => items.Where(orderItem => IncludedSystemProductTypes.Contains(orderItem.Type));
    }
}
