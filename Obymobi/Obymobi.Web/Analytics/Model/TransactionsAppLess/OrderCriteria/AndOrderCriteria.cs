﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class AndOrderCriteria : AndCriteria<OrderEntity>
    {
        private AndOrderCriteria(ICriteria<OrderEntity> criteria, ICriteria<OrderEntity> otherCriteria) : base(criteria, otherCriteria)
        {
        }

        public static ICriteria<OrderEntity> Create(ICriteria<OrderEntity> criteria, ICriteria<OrderEntity> otherCriteria)
        {
            if (criteria != null && otherCriteria != null)
            {
                return new AndOrderCriteria(criteria, otherCriteria);
            }

            if (criteria != null)
            {
                return criteria;
            }
            else if (otherCriteria != null)
            {
                return otherCriteria;
            }

            return new EmptyOrderCriteria();
        }
    }
}
