﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrderItemTagCriteria : ICriteria<OrderitemEntity>
    {
        public OrderItemTagCriteria(IEnumerable<int> includedTagIds) => IncludedTagIds = includedTagIds.ToArray();

        public int[] IncludedTagIds { get; }

        public IEnumerable<OrderitemEntity> MeetCriteria(IEnumerable<OrderitemEntity> items) 
            => items.Where(HasAnyIncludedTag);

        private bool HasAnyIncludedTag(OrderitemEntity orderItem) 
            => orderItem.OrderitemTagCollection
                .Any(tag => IncludedTagIds.Contains(tag.TagId));
    }
}
