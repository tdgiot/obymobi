﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrderServiceMethodCriteria : ICriteria<OrderEntity>
    {
        public OrderServiceMethodCriteria(IEnumerable<int> serviceMethodIds) => ServiceMethodIds = serviceMethodIds;

        private IEnumerable<int> ServiceMethodIds { get; }

        public IEnumerable<OrderEntity> MeetCriteria(IEnumerable<OrderEntity> items) => items.Where(item => ServiceMethodIds.Contains(item.ServiceMethodId));
    }
}
