﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrderItemAlterationItemTagCriteria : ICriteria<OrderitemAlterationitemEntity>
    {
        public OrderItemAlterationItemTagCriteria(IReadOnlyList<int> includedTagIds) => IncludedTagIds = includedTagIds;

        public IReadOnlyList<int> IncludedTagIds { get; }

        public IEnumerable<OrderitemAlterationitemEntity> MeetCriteria(IEnumerable<OrderitemAlterationitemEntity> alterationsItems)
            => alterationsItems.Where(HasAnyIncludedTag);

        private bool HasAnyIncludedTag(OrderitemAlterationitemEntity alterationItem) 
            => alterationItem.OrderitemAlterationitemTagCollection
                .Any(tag => IncludedTagIds.Contains(tag.TagId));
    }
}
