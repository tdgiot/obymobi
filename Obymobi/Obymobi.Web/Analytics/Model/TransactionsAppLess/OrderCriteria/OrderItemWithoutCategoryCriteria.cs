﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrderItemWithoutCategoryCriteria : ICriteria<OrderitemEntity>
    {
        public IEnumerable<OrderitemEntity> MeetCriteria(IEnumerable<OrderitemEntity> items) => items.Where(item => !item.CategoryId.HasValue);
    }
}
