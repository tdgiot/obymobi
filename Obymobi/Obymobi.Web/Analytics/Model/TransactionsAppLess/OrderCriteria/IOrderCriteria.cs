﻿using System.Collections.Generic;
using Obymobi.Web.Analytics.Model.Orders;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public interface IOrderCriteria
    {
        IEnumerable<Order> MeetCriteria(IEnumerable<Order> orders);
    }
}
