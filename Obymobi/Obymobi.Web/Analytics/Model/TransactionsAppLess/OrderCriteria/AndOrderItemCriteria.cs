﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class AndOrderItemCriteria : AndCriteria<OrderitemEntity>
    {
        private AndOrderItemCriteria(ICriteria<OrderitemEntity> criteria, ICriteria<OrderitemEntity> otherCriteria) : base(criteria, otherCriteria)
        {
        }

        public static ICriteria<OrderitemEntity> Create(ICriteria<OrderitemEntity> criteria, ICriteria<OrderitemEntity> otherCriteria)
        {
            if (criteria != null && otherCriteria != null)
            {
                return new AndOrderItemCriteria(criteria, otherCriteria);
            }

            if (criteria != null)
            {
                return criteria;
            }
            else if (otherCriteria != null)
            {
                return otherCriteria;
            }

            return new EmptyOrderItemCriteria();
        }
    }
}
