﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrderCheckoutMethodCriteria : ICriteria<OrderEntity>
    {
        public OrderCheckoutMethodCriteria(IEnumerable<int> serviceMethods) => CheckoutMethodIds = serviceMethods;

        private IEnumerable<int> CheckoutMethodIds { get; }

        public IEnumerable<OrderEntity> MeetCriteria(IEnumerable<OrderEntity> items) => items.Where(item => CheckoutMethodIds.Contains(item.CheckoutMethodId));
    }
}
