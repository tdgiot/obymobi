﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrderItemAlterationItemWithProductLinkCriteria : ICriteria<OrderitemAlterationitemEntity>
    {
        public IEnumerable<OrderitemAlterationitemEntity> MeetCriteria(IEnumerable<OrderitemAlterationitemEntity> alterationsItems)
            => alterationsItems.Where(alterationsItem => alterationsItem.AlterationoptionProductId.HasValue);
    }
}
