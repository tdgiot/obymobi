﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrOrderCriteria : OrCriteria<OrderEntity>
    {
        private OrOrderCriteria(ICriteria<OrderEntity> criteria, ICriteria<OrderEntity> otherCriteria) : base(criteria, otherCriteria)
        {
        }

        public static ICriteria<OrderEntity> Create(ICriteria<OrderEntity> criteria, ICriteria<OrderEntity> otherCriteria)
        {
            if (criteria != null && otherCriteria != null)
            {
                return new OrOrderCriteria(criteria, otherCriteria);
            }

            if (criteria != null)
            {
                return criteria;
            }
            else if (otherCriteria != null)
            {
                return otherCriteria;
            }

            return new EmptyOrderCriteria();
        }

    }
}
