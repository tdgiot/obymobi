﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.Orders;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrderItemDoesNotContainCategoryCriteria : OrderItemCategoryCriteria
    {
        public OrderItemDoesNotContainCategoryCriteria(IEnumerable<int> categoryFilterIds) : base(categoryFilterIds)
        {
        }

        public override IEnumerable<OrderitemEntity> MeetCriteria(IEnumerable<OrderitemEntity> items) => items.Where(orderItem => !CategoryFilterIds.Contains(orderItem.CategoryId));
    }
}
