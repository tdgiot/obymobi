﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.Orders;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrderItemContainsCategoryCriteria : OrderItemCategoryCriteria
    {
        public OrderItemContainsCategoryCriteria(IEnumerable<int> categoryFilterIds) : base(categoryFilterIds)
        {
        }

        public override IEnumerable<OrderitemEntity> MeetCriteria(IEnumerable<OrderitemEntity> items) => items.Where(orderItem => CategoryFilterIds.Contains(orderItem.CategoryId));
    }
}
