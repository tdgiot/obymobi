﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public abstract class OrderItemCategoryCriteria : ICriteria<OrderitemEntity>
    {
        protected int[] CategoryFilterIds { get; }

        protected OrderItemCategoryCriteria(IEnumerable<int> categoryFilterIds) => CategoryFilterIds = categoryFilterIds as int[] ?? categoryFilterIds.ToArray();

        public abstract IEnumerable<OrderitemEntity> MeetCriteria(IEnumerable<OrderitemEntity> items);

        public static OrderItemCategoryCriteria Create(IEnumerable<int> categoryFilterIds, Enums.IncludeExcludeFilter includeExcludeFilter) => includeExcludeFilter == Enums.IncludeExcludeFilter.Include
                ? (OrderItemCategoryCriteria)new OrderItemContainsCategoryCriteria(categoryFilterIds)
                : (OrderItemCategoryCriteria)new OrderItemDoesNotContainCategoryCriteria(categoryFilterIds);
    }
}
