﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class AndOrderItemAlterationItemCriteria : AndCriteria<OrderitemAlterationitemEntity>
    {
        private AndOrderItemAlterationItemCriteria(ICriteria<OrderitemAlterationitemEntity> criteria, ICriteria<OrderitemAlterationitemEntity> otherCriteria) : base(criteria, otherCriteria)
        {
        }

        public static ICriteria<OrderitemAlterationitemEntity> Create(ICriteria<OrderitemAlterationitemEntity> criteria, ICriteria<OrderitemAlterationitemEntity> otherCriteria)
        {
            if (criteria != null && otherCriteria != null)
            {
                return new AndOrderItemAlterationItemCriteria(criteria, otherCriteria);
            }

            if (criteria != null)
            {
                return criteria;
            }
            else if (otherCriteria != null)
            {
                return otherCriteria;
            }

            return new EmptyOrderItemAlterationItemCriteria();
        }
    }
}
