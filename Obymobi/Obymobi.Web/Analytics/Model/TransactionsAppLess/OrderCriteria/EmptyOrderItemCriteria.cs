﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    [EmptyCriteria]
    public class EmptyOrderItemCriteria : ICriteria<OrderitemEntity>
    {
        public IEnumerable<OrderitemEntity> MeetCriteria(IEnumerable<OrderitemEntity> items) => items;
    }
}
