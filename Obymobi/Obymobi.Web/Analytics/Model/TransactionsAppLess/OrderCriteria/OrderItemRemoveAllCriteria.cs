﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
	public class OrderItemRemoveAllCriteria : ICriteria<OrderitemEntity>
	{
		public IEnumerable<OrderitemEntity> MeetCriteria(IEnumerable<OrderitemEntity> items) => Enumerable.Empty<OrderitemEntity>();
	}
}
