﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria
{
    public class OrderItemWithAlterationLinkedToProductCriteria : ICriteria<OrderitemEntity>
    {
        public IEnumerable<OrderitemEntity> MeetCriteria(IEnumerable<OrderitemEntity> items)
            => items.Where(item => item.OrderitemAlterationitemCollection.Any(alterationItem => alterationItem.AlterationoptionProductId.HasValue));
    }
}
