﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.Orders
{
	public class Orders : IEnumerable<Order>
	{
		private Order[] Items { get; set; }

		public int Count => Items.Length;

		public decimal TotalRevenue { get; set; }
		public decimal TotalReportRevenue { get; set; }
		public int TotalReportQuantity { get; set; }
		public decimal TotalIndividualPrice { get; set; }
		public decimal TotalPrice { get; set; }

		public IEnumerator<Order> GetEnumerator() => ((IEnumerable<Order>) Items).GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		public void SetOrders(IEnumerable<Order> orders)
		{
			Items = orders as Order[] ?? orders.ToArray();

			TotalRevenue = Items.Sum(o => o.OrderTotal);

			TotalReportRevenue = Items.Sum(order => order.OrderItems.Sum(orderItem => orderItem.Price + orderItem.OrderItemAlterationItems.Sum(alterationItem => alterationItem.PriceInTax)));

			TotalReportQuantity = Items.Sum(order => order.OrderItems.Sum(orderItem => orderItem.Quantity));

			TotalIndividualPrice = Items.Sum(order => order.OrderItems.Sum(orderItem => orderItem.Price));
		}
	}
}
