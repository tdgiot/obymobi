﻿using Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria;
using System.ComponentModel;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern
{
    public static class CriteriaTraits
    {
        public static bool IsEmptyCriteria<T>(this ICriteria<T> self) => self != null && TypeDescriptor.GetAttributes(self).OfType<EmptyCriteriaAttribute>().Any();
    }
}
