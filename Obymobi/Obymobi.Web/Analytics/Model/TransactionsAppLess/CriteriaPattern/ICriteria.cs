﻿using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern
{
    public interface ICriteria<T>
    {
        IEnumerable<T> MeetCriteria(IEnumerable<T> items);
    }
}
