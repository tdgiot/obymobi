﻿using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern
{
    public class AndCriteria<T> : Criteria<T>
    {
        public AndCriteria(ICriteria<T> firstCriteria, ICriteria<T> otherCriteria) : base(firstCriteria, otherCriteria)
        {
        }

        public override IEnumerable<T> MeetCriteria(IEnumerable<T> items)
        {
            if (!FirstCriteria.IsEmptyCriteria() && !OtherCriteria.IsEmptyCriteria())
            {
                return OtherCriteria.MeetCriteria(FirstCriteria.MeetCriteria(items));
            }

            if (FirstCriteria.IsEmptyCriteria() && OtherCriteria.IsEmptyCriteria())
            {
                return Enumerable.Empty<T>();
            }

            return !FirstCriteria.IsEmptyCriteria() ? FirstCriteria.MeetCriteria(items) : OtherCriteria.MeetCriteria(items);
        }
    }
}
