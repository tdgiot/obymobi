﻿using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern
{
    public abstract class Criteria<T> : ICriteria<T>
    {
        protected ICriteria<T> FirstCriteria { get; }
        protected ICriteria<T> OtherCriteria { get; }

        protected Criteria(ICriteria<T> firstCriteria, ICriteria<T> otherCriteria)
        {
            FirstCriteria = firstCriteria;
            OtherCriteria = otherCriteria;
        }

        public abstract IEnumerable<T> MeetCriteria(IEnumerable<T> items);
    }
}
