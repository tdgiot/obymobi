﻿using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern
{
    public class OrCriteria<T> : Criteria<T>
    {
        public OrCriteria(ICriteria<T> firstCriteria, ICriteria<T> otherCriteria) : base(firstCriteria, otherCriteria)
        {
        }

        public override IEnumerable<T> MeetCriteria(IEnumerable<T> items)
        {
            IEnumerable<T> enumerable = items as T[] ?? items.ToArray();
            var firstCriteriaItems = !FirstCriteria.IsEmptyCriteria() ? FirstCriteria.MeetCriteria(enumerable) : Enumerable.Empty<T>();
            var otherCriteriaItems = !OtherCriteria.IsEmptyCriteria() ? OtherCriteria.MeetCriteria(enumerable) : Enumerable.Empty<T>();
            return firstCriteriaItems.Union(otherCriteriaItems);
        }
    }
}
