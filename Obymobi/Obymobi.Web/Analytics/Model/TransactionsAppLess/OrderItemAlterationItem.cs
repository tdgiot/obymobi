﻿namespace Obymobi.Web.Analytics.Model.Orders
{
	public class OrderItemAlterationItem
	{
		public OrderItemAlterationItem(string alterationName, string alterationOptionName, string alterationValue, decimal priceInTax, decimal priceExTax)
		{
			AlterationName = alterationName;
			AlterationOptionName = alterationOptionName;
			AlterationValue = alterationValue;
			PriceInTax = priceInTax;
			PriceExTax = priceExTax;
		}

		public string AlterationName { get; }

		public string AlterationOptionName { get; }

		public string AlterationValue { get; }

		public decimal PriceInTax { get; }

		public decimal PriceExTax { get; }
	}
}
