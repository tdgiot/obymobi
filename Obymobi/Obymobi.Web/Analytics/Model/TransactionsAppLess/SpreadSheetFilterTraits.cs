﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess
{
	public static class SpreadSheetFilterTraits
	{
		public static ICriteria<OrderEntity> CreateOrderCriteria(this SpreadSheetFilter sheetFilter)
		{
			ICriteria<OrderEntity> orderCriteria = new EmptyOrderCriteria();

			if (!sheetFilter.OutletId.HasValue)
			{
				return orderCriteria;
			}

			orderCriteria = new OrderOutletIdCriteria(sheetFilter.OutletId.Value);
			orderCriteria = orderCriteria.CreateCollectionBasedAndCriteria<OrderServiceMethodCriteria>(sheetFilter.ServiceMethods);
			orderCriteria = orderCriteria.CreateCollectionBasedAndCriteria<OrderCheckoutMethodCriteria>(sheetFilter.CheckoutMethods);

			return orderCriteria;
		}
    }
}
