﻿using Obymobi.Enums;

namespace Obymobi.Web.Analytics.Model.Orders
{
	public class OrderItemSale : ProductSaleBase
	{
		public OrderItemSale(string name, string externalIdentifier, int quantity, decimal price, OrderitemType type, string categoryName, OrderItemAlterationItem[] orderItemAlterationItems) 
			: base(name, externalIdentifier, quantity, price)
		{
			OrderItemAlterationItems = orderItemAlterationItems;
			CategoryName = categoryName;
			Type = type;
		}

		public OrderItemSale(string name, string externalIdentifier, int quantity, decimal price) 
			: this(name, externalIdentifier, quantity, price, OrderitemType.Product, string.Empty, new OrderItemAlterationItem[0])
		{
			
		}

		public string CategoryName { get; }

		public OrderitemType Type { get; }

		public OrderItemAlterationItem[] OrderItemAlterationItems { get; }
	}
}
