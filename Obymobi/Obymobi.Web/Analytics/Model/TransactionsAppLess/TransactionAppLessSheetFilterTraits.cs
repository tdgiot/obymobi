using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess
{
    public static class TransactionAppLessSheetFilterTraits
    {
        public static ICriteria<OrderitemEntity> CreateOrderItemCriteria(this TransactionsAppLessSheetFilter sheetFilter)
        {
            ICriteria<OrderitemEntity> orderItemCriteria = new EmptyOrderItemCriteria();

            if (sheetFilter.ShowProducts || sheetFilter.ShowSystemProducts)
            {
                if (sheetFilter.CategoryFilterType != IncludeExcludeFilter.None)
                {
                    ICriteria<OrderitemEntity> orderItemCategoryCriteria = OrderItemCategoryCriteria.Create(sheetFilter.CategoryFilterIds, sheetFilter.CategoryFilterType);

                    if (sheetFilter.ShowSystemProducts)
                    {
                        orderItemCategoryCriteria = OrOrderItemCriteria.Create(orderItemCategoryCriteria, new OrderItemWithoutCategoryCriteria());
                    }

                    orderItemCriteria = orderItemCategoryCriteria;
                }

                ICriteria<OrderitemEntity> systemProductCriteria = null;

                if (sheetFilter.ShowSystemProducts)
                {
                    if (sheetFilter.IncludedSystemProductTypes.Any())
                    {
                        systemProductCriteria = new OrderItemContainsSystemProductCriteria(sheetFilter.IncludedSystemProductTypes
                            .Where(x => Enum.IsDefined(typeof(OrderitemType), x))
                            .Select(x => (OrderitemType)Enum.ToObject(typeof(OrderitemType), x)));

                        if (sheetFilter.ShowProducts)
                        {
                            systemProductCriteria = OrOrderItemCriteria.Create(systemProductCriteria, CreateOrderitemProductCriteria(sheetFilter));
                        }
                    }
                    else if (!sheetFilter.ShowProducts)
                    {
                        systemProductCriteria = new OrderItemNotProductCriteria();
                    }
                }
                else if (sheetFilter.ShowProducts)
                {
                    systemProductCriteria = CreateOrderitemProductCriteria(sheetFilter);
                }

                orderItemCriteria = AndOrderItemCriteria.Create(orderItemCriteria, systemProductCriteria);
            }
            else
            {
                orderItemCriteria = new OrderItemNotProductAndNotSystemProductCriteria();
            }

            return orderItemCriteria;
        }

        public static IEnumerable<Order> MapOrderEntitiesToModels(this TransactionsAppLessSheetFilter sheetFilter, OrderCollection orders, ICriteria<OrderitemEntity> orderItemCriteria)
        {
            IReadOnlyList<int> taxTariffsToInclude = sheetFilter.IncludeTaxTariffIds;

            List<OrderEntity> orderEntityList = orders
                .Where(x => !x.MasterOrderId.HasValue && x.OrderitemCollection.Any())
                .ToList();

            List<Order> orderList = orderEntityList.Select(orderEntity => MapEntity(orderEntity, orderItemCriteria, taxTariffsToInclude))
                .Where(order => !SkipOrder(sheetFilter, order))
                .ToList();

            foreach (OrderEntity subOrderEntity in orders.Where(x => x.MasterOrderId.HasValue))
            {
                if (!subOrderEntity.OrderitemCollection.Any())
                {
                    continue;
                }

                Order masterOrder = orderList.FirstOrDefault(x => x.OrderNumber == subOrderEntity.MasterOrderId);

                if (masterOrder == null)
                {
                    // Create new master order without order items and then merge this subOrder with master order
                    masterOrder = MapEntity(subOrderEntity.OrderEntity, new OrderItemRemoveAllCriteria(), taxTariffsToInclude);
                    MergeEntity(subOrderEntity, masterOrder, orderItemCriteria, taxTariffsToInclude);

                    if (SkipOrder(sheetFilter, masterOrder))
                    {
                        continue;
                    }

                    orderList.Add(masterOrder);
                }
                else
                {
                    MergeEntity(subOrderEntity, masterOrder, orderItemCriteria, taxTariffsToInclude);
                }
            }

            return orderList.OrderBy(x => x.OrderDate);
        }

        private static Order MapEntity(OrderEntityBase orderEntity, ICriteria<OrderitemEntity> orderitemCriteria,
            IReadOnlyList<int> taxTariffsToInclude)
            => new Order(
                orderEntity.CreatedUTC,
                orderEntity.OrderId,
                orderEntity.ServiceMethodName,
                orderEntity.CheckoutMethodName,
                orderEntity.Total,
                orderitemCriteria.MeetCriteria(orderEntity.OrderitemCollection)
                    .Select(item => MapOrderItem(item, taxTariffsToInclude))
                    .ToArray(),
                orderEntity.CustomerLastname,
                orderEntity.CustomerPhonenumber,
                orderEntity.Email,
                orderEntity.Notes,
                orderEntity.DeliveryInformationEntity?.BuildingName ?? string.Empty,
                orderEntity.DeliveryInformationEntity?.Address ?? string.Empty,
                orderEntity.DeliveryInformationEntity?.Zipcode ?? string.Empty,
                orderEntity.DeliveryInformationEntity?.City ?? string.Empty,
                orderEntity.DeliveryInformationEntity?.Instructions ?? string.Empty,
                MapTransactionPaymentMethod(orderEntity),
                MapTransactionCardSummary(orderEntity),
                orderEntity.DeliverypointName,
                orderEntity.CoversCount != null ? orderEntity.CoversCount.ToString() : string.Empty);

        private static void MergeEntity(OrderEntityBase subOrder, Order masterOrder, ICriteria<OrderitemEntity> orderitemCriteria, IReadOnlyList<int> taxTariffsToInclude)
        {
            OrderItemSale[] orderItems = orderitemCriteria.MeetCriteria(subOrder.OrderitemCollection)
                .Select(orderItem => MapOrderItem(orderItem, taxTariffsToInclude))
                .ToArray();

            masterOrder.AddExtraOrderItems(subOrder.OrderId, orderItems);
        }

        private static bool SkipOrder(TransactionsAppLessSheetFilter sheetFilter, Order order) => (sheetFilter.ShowProducts || sheetFilter.ShowSystemProducts) && !order.OrderItems.Any();

        private static OrderItemSale MapOrderItem(OrderitemEntity orderItem, IReadOnlyList<int> taxTariffsToInclude) => new OrderItemSale(orderItem.ProductName,
            orderItem.ExternalIdentifier,
            orderItem.Quantity,
            orderItem.PriceTotalInTax,
            orderItem.Type,
            orderItem.CategoryPath,
            orderItem.OrderitemAlterationitemCollection
                .Where(alterationItem => IncludeAlterationItem(alterationItem, orderItem, taxTariffsToInclude))
                .Select(alterationItem =>
                    new OrderItemAlterationItem(
                        alterationItem.AlterationName,
                        alterationItem.AlterationoptionName,
                        alterationItem.Value,
                        alterationItem.PriceTotalInTax,
                        alterationItem.PriceTotalExTax))
                .ToArray()
            );

        private static string MapTransactionPaymentMethod(OrderEntityBase orderEntity) => orderEntity.PaymentTransactionCollection.FirstOrDefault(x => x.Status == PaymentTransactionStatus.Paid)?.PaymentMethod ?? string.Empty;

        private static string MapTransactionCardSummary(OrderEntityBase orderEntity) => orderEntity.PaymentTransactionCollection.FirstOrDefault(x => x.Status == PaymentTransactionStatus.Paid)?.CardSummary ?? string.Empty;

        private static bool IncludeAlterationItem(OrderitemAlterationitemEntity alterationitemEntity, OrderitemEntity orderitemEntity, IReadOnlyList<int> taxTariffsToInclude) =>
            IncludeTaxTariffId(alterationitemEntity.TaxTariffId, taxTariffsToInclude) ||
            alterationitemEntity.InheritsTaxTariff(orderitemEntity) && taxTariffsToInclude.Contains(orderitemEntity.TaxTariffId);

        private static bool IncludeTaxTariffId(int? taxTariffId, IReadOnlyList<int> taxTariffsToInclude) => !FilterOnTaxTariffId(taxTariffsToInclude) || taxTariffsToInclude.Contains(taxTariffId);

        private static bool FilterOnTaxTariffId(IEnumerable<int> taxTariffsToInclude) => taxTariffsToInclude.Any();

        private static ICriteria<OrderitemEntity> CreateOrderitemProductCriteria(TransactionsAppLessSheetFilter sheetFilter)
        {
            ICriteria<OrderitemEntity> orderItemProductCriteria = new OrderItemProductCriteria();

            if (!sheetFilter.IncludeTaxTariffIds.Any())
            {
                return orderItemProductCriteria;
            }

            ICriteria<OrderitemEntity> taxTariffCriteria = new OrderItemContainsTaxTariffIdCriteria(sheetFilter.IncludeTaxTariffIds);
            taxTariffCriteria = OrOrderItemCriteria.Create(taxTariffCriteria, new OrderItemAlterationsContainTaxTariffIdCriteria(sheetFilter.IncludeTaxTariffIds));

            if (sheetFilter.ShowSystemProducts)
            {
                taxTariffCriteria = OrOrderItemCriteria.Create(taxTariffCriteria, new OrderItemWithoutTaxTariffIdCriteria());
            }

            orderItemProductCriteria = AndOrderItemCriteria.Create(orderItemProductCriteria, taxTariffCriteria);

            return orderItemProductCriteria;
        }
    }
}
