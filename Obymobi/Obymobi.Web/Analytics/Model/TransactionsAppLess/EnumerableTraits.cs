﻿using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.Orders
{
    public static class EnumerableTraits
    {
        public static bool Contains(this IEnumerable<int> numberToInclude, int? number)
            => number.HasValue && Enumerable.Contains(numberToInclude, number.Value);
    }
}
