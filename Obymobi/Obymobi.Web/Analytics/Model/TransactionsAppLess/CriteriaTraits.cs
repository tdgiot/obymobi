﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria;

namespace Obymobi.Web.Analytics.Model.TransactionsAppLess
{
	public static class CriteriaTraits
	{
		public static ICriteria<OrderEntity> CreateCollectionBasedAndCriteria<TCriteria>(this ICriteria<OrderEntity> existingCriteria, IEnumerable<int> criteriaItems) where TCriteria : ICriteria<OrderEntity>
            => criteriaItems.Any()
                ? AndOrderCriteria.Create(existingCriteria, (ICriteria<OrderEntity>)Activator.CreateInstance(typeof(TCriteria), new object[] { criteriaItems }))
                : existingCriteria;
    }
}
