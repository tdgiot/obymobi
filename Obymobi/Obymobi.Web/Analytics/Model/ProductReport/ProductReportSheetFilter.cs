﻿using Dionysos;
using Obymobi.Web.Analytics.Model.ModelBase;
using System.Text;

namespace Obymobi.Web.Analytics.Model.ProductReport
{
	public class ProductReportSheetFilter : SpreadSheetFilter
	{
        public bool ShowExternalIdentifier { get; set; }

		public override string ToString() =>
			new StringBuilder()
				.AppendLine()
				.AppendFormatLine("=== Sheet Filter: {0} (id: {1})", Name, Id)
				.AppendFormatLine("Active: {0}", Active)
				.AppendFormatLine("Outlet Id: {0}", OutletId?.ToString() ?? "None")
				.AppendFormatLine("- Service methods: {0}", string.Join(", ", ServiceMethods))
				.AppendFormatLine("- Checkout methods: {0}", string.Join(", ", CheckoutMethods))
                .AppendFormatLine("Show external identifier: {0}", ShowExternalIdentifier)
				.AppendFormatLine("Category filter: {0}", CategoryFilterType)
				.AppendFormatLine("- Selected category ids: {0}", string.Join(", ", CategoryFilterIds))
				.AppendFormatLine("Tag filter:")
				.AppendFormatLine("- Selected tags: {0}", string.Join(", ", IncludedTagIds)).ToString();
	}
}
