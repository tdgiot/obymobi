﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Extensions;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria;

namespace Obymobi.Web.Analytics.Model.ProductReport
{
	public static class ProductReportSheetFilterTraits
	{
		public static ICriteria<OrderEntity> CreateOrderCriteria(this ProductReportSheetFilter sheetFilter)
		{
			ICriteria<OrderEntity> orderCriteria = new EmptyOrderCriteria();

			if (sheetFilter.OutletId.HasValue)
			{
				ICriteria<OrderEntity> outletCriteria = new OrderOutletIdCriteria(sheetFilter.OutletId.Value);
				orderCriteria = AndOrderCriteria.Create(orderCriteria, outletCriteria);
			}

			if (sheetFilter.ServiceMethods.Any())
			{
				ICriteria<OrderEntity> serviceMethodCriteria = new OrderServiceMethodCriteria(sheetFilter.ServiceMethods);
				orderCriteria = AndOrderCriteria.Create(orderCriteria, serviceMethodCriteria);
			}

			if (sheetFilter.CheckoutMethods.Any())
			{
				ICriteria<OrderEntity> checkoutMethodCriteria = new OrderCheckoutMethodCriteria(sheetFilter.CheckoutMethods);
				orderCriteria = AndOrderCriteria.Create(orderCriteria, checkoutMethodCriteria);
			}

			return orderCriteria;
		}

		public static ICriteria<OrderitemEntity> CreateOrderItemCriteria(this ProductReportSheetFilter sheetFilter)
		{
			var orderItemCriteria = AndOrderItemCriteria.Create(
				new OrderItemProductCriteria(),
				OrOrderItemCriteria.Create(
					new OrderItemWithAlterationLinkedToProductCriteria(),
					new OrderItemWithPriceCriteria())
			);

			if (sheetFilter.CategoryFilterType != IncludeExcludeFilter.None)
			{
				ICriteria<OrderitemEntity> orderItemCategoryCriteria = OrderItemCategoryCriteria.Create(sheetFilter.CategoryFilterIds, sheetFilter.CategoryFilterType);

				orderItemCriteria = AndOrderItemCriteria.Create(orderItemCriteria, orderItemCategoryCriteria);
			}

			if (sheetFilter.IncludedTagIds.Any())
			{
				ICriteria<OrderitemEntity> orderItemTagCriteria = new OrderItemTagCriteria(sheetFilter.IncludedTagIds);

				orderItemCriteria = AndOrderItemCriteria.Create(orderItemCriteria, orderItemTagCriteria);
			}

			return orderItemCriteria;
		}

		public static ICriteria<OrderitemAlterationitemEntity> CreateOrderItemAlterationsCriteria(this ProductReportSheetFilter sheetFilter)
		{
			var alterationItemCriteria = OrOrderItemAlterationItemCriteria.Create(
				new OrderItemAlterationItemWithPriceCriteria(),
				new OrderItemAlterationItemWithProductLinkCriteria()
			);

			if (sheetFilter.IncludedTagIds.Any())
			{
				alterationItemCriteria = AndOrderItemAlterationItemCriteria.Create(
					alterationItemCriteria,
					new OrderItemAlterationItemTagCriteria(sheetFilter.IncludedTagIds));
			}

			return alterationItemCriteria;
		}

		public static IEnumerable<ProductSale> MapOrderEntitiesToModels(this ProductReportSheetFilter sheetFilter,
			OrderCollection orders,
			ICriteria<OrderEntity> orderCriteria,
			ICriteria<OrderitemEntity> orderItemCriteria,
			ICriteria<OrderitemAlterationitemEntity> alterationItemCriteria)
		{
			OrderEntity[] filteredOrders = orderCriteria.MeetCriteria(orders).ToArray();

			List<ProductSale> alterationItems = filteredOrders
				.SelectMany(order => order.OrderitemCollection)
				.SelectMany(item => MapAlterationItem(item, alterationItemCriteria))
                .ToList();

			IEnumerable<OrderitemEntity> items = filteredOrders
				.SelectMany(orderEntity => orderItemCriteria.MeetCriteria(orderEntity.OrderitemCollection));

			OrderItemWithPriceCriteria orderItemWithPriceCriteria = new OrderItemWithPriceCriteria();
			List<ProductSale> products = orderItemWithPriceCriteria.MeetCriteria(items)
				.Select(item => item.ToProduct())
                .ToList();

            IEnumerable<ProductSale> productSales = products.Concat(alterationItems);

            return sheetFilter.ShowExternalIdentifier
                ? GroupByNameAndPriceAndExternalIdentifier(productSales)
                : GroupByNameAndPrice(productSales);
		}

		private static IEnumerable<ProductSale> GroupByNameAndPrice(IEnumerable<ProductSale> productSales) => 
            productSales
                .GroupBy(i => new 
                {
                    i.Name, 
                    i.Price
                })
                .Select(x => new ProductSale(x.Key.Name, string.Empty, x.Sum(k => k.Quantity), x.Key.Price))
                .OrderBy(i => i.Name)
                .ThenBy(i => i.TotalPrice)
                .ToArray();

        private static IEnumerable<ProductSale> GroupByNameAndPriceAndExternalIdentifier(IEnumerable<ProductSale> productSales) =>
            productSales
                .GroupBy(i => new
                {
                    i.Name,
                    i.Price,
					i.ExternalIdentifier
                })
                .Select(x => new ProductSale(x.Key.Name, x.Key.ExternalIdentifier, x.Sum(k => k.Quantity), x.Key.Price))
                .OrderBy(i => i.Name)
                .ThenBy(i => i.ExternalIdentifier)
				.ThenBy(i => i.TotalPrice)
                .ToArray();

		private static IEnumerable<ProductSale> MapAlterationItem(OrderitemEntityBase orderItem, ICriteria<OrderitemAlterationitemEntity> alterationItemCriteria)
		{
			IEnumerable<ProductSale> alterationItems = alterationItemCriteria.MeetCriteria(orderItem.OrderitemAlterationitemCollection)
				.Select(alterationItem =>
					new ProductSale(
						alterationItem.GetFriendlyNameOrName(),
                        alterationItem.ExternalIdentifier,
						orderItem.Quantity,
						alterationItem.PriceInTax));

			return alterationItems.ToArray();
		}
	}
}
