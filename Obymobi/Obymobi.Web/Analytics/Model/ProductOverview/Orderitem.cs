﻿using System;
using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Model.ProductOverview
{
    public class Orderitem
    {
        #region Fields

        private readonly ReportSettings settings;

        #endregion

        #region Constructors

        public Orderitem(ReportSettings settings)
        {
            this.settings = settings;
            this.Alterationitems = new List<Alterationitem>();
        }

        #endregion        

        #region Properties

        public DateTime Created { get; set; }

        public int OrderitemId { get; set; }

        public int OrderId { get; set; }

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public int ClientId { get; set; }

        public int DeliverypointId { get; set; }

        public string DeliverypointName { get; set; }

        public string DeliverypointNumber { get; set; }

        public string DeliverypointgroupName { get; set; }

        public string Status { get; set; }

        public DateTime Processed { get; set; }

        public string Notes { get; set; }

        public List<Alterationitem> Alterationitems { get; set; }

        public string CreatedAsString
        {
            get
            {
                return this.settings.ToClockModeString(this.Created);
            }
        }

        public string ProcessedAsString
        {
            get
            {
                return this.settings.ToClockModeString(this.Processed);
            }
        }

        #endregion        
    }
}
