﻿using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Model.ProductOverview
{
    public class Product
    {
        #region Constructors

        public Product()
        {
            this.Orderitems = new Dictionary<int, Orderitem>();
            this.Alterationitems = new Dictionary<int, Alterationitem>();
        }

        #endregion

        #region Properties

        public int ProductId { get; set; }

        public string Name { get; set; }

        public Dictionary<int, Orderitem> Orderitems { get; set; }

        public Dictionary<int, Alterationitem> Alterationitems { get; set; }

        #endregion        
    }
}
