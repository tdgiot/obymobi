﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using Obymobi.Logic.Analytics;
using Dionysos;
using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Model.ProductOverview
{
    public class ProductOverview
    {
        #region Fields

        private readonly Filter request;

        private OrderitemCollection orderitemCollection;

        #endregion

        #region Constructors

        public ProductOverview(Filter request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            this.request = request;
        }

        #endregion

        #region Properties

        public Product Product { get; set; }

        #endregion

        #region Methods

        public void FetchAndProcessData()
        {
            this.FetchProductWithOrderitems();
            this.ProcessData();
        }

        private void FetchProductWithOrderitems()
        {
            // Create the filter and the relations
            PredicateExpression filter = new PredicateExpression();
            RelationCollection relations = new RelationCollection();

            // Add filter on CompanyId and date range
            filter.Add(OrderFields.CompanyId == this.request.CompanyId);
            filter.Add(OrderFields.CreatedUTC >= this.request.FromUtc);
            filter.Add(OrderFields.CreatedUTC <= this.request.TillUtc);
            filter.Add(OrderitemFields.ProductId == this.request.ProductId);

            // Add relation Orderitem > Order
            relations.Add(OrderitemEntityBase.Relations.OrderEntityUsingOrderId, JoinHint.Left);

            // Add filter on DeliverypointIds
            if (this.request.DeliverypointIds != null && this.request.DeliverypointIds.Count > 0)
            {
                PredicateExpression subfilter = new PredicateExpression();

                if (this.request.DeliverypointsExclude)
                { 
                    subfilter.Add(OrderFields.DeliverypointId != this.request.DeliverypointIds);

                    if (this.request.DeliverypointgroupIds != null && this.request.DeliverypointgroupIds.Count > 0)
                    {
                        subfilter.Add(DeliverypointFields.DeliverypointgroupId == this.request.DeliverypointgroupIds);
                        relations.Add(OrderEntityBase.Relations.DeliverypointEntityUsingDeliverypointId); // Left join, so orders without deliverypoint are also included
                    }
                }
                else
                {
                    subfilter.Add(OrderFields.DeliverypointId == this.request.DeliverypointIds);
                }
                subfilter.AddWithOr(OrderFields.DeliverypointId == DBNull.Value);
                filter.Add(subfilter);
            }
            else if (this.request.DeliverypointgroupIds != null && this.request.DeliverypointgroupIds.Count > 0)
            {
                PredicateExpression subfilter = new PredicateExpression();
                subfilter.Add(DeliverypointFields.DeliverypointgroupId == this.request.DeliverypointgroupIds);
                subfilter.AddWithOr(OrderFields.DeliverypointId == DBNull.Value);
                filter.Add(subfilter);

                relations.Add(OrderEntityBase.Relations.DeliverypointEntityUsingDeliverypointId, JoinHint.Left); // Left join, so orders without deliverypoint are also included
            }

            // Create the included fields list for the orderitems
            IncludeFieldsList orderitemIncludes = new IncludeFieldsList();
            orderitemIncludes.Add(OrderitemFields.CreatedUTC);
            orderitemIncludes.Add(OrderitemFields.CategoryId);
            orderitemIncludes.Add(OrderitemFields.CategoryName);
            orderitemIncludes.Add(OrderitemFields.ProductId);
            orderitemIncludes.Add(OrderitemFields.ProductName);
            orderitemIncludes.Add(OrderitemFields.ProductPriceIn);

            IncludeFieldsList orderitemAlterationitemIncludeFields = new IncludeFieldsList
            {
                OrderitemAlterationitemFields.AlterationitemId, 
                OrderitemAlterationitemFields.AlterationName, 
                OrderitemAlterationitemFields.AlterationoptionName, 
                OrderitemAlterationitemFields.Time, 
                OrderitemAlterationitemFields.Value
            };

            IncludeFieldsList orderIncludeFields = new IncludeFieldsList
            {
                OrderFields.OrderId, 
                OrderFields.ClientId, 
                OrderFields.DeliverypointId, 
                OrderFields.DeliverypointName, 
                OrderFields.DeliverypointNumber, 
                OrderFields.DeliverypointgroupName, 
                OrderFields.Notes, 
                OrderFields.StatusText, 
                OrderFields.ProcessedUTC
            };

            IncludeFieldsList productIncludeFields = new IncludeFieldsList
            {
                ProductFields.Name
            };

            PrefetchPath prefetch = new PrefetchPath(EntityType.OrderitemEntity);
            IPrefetchPathElement prefetchOrderitemAlterationItem = prefetch.Add(OrderitemEntityBase.PrefetchPathOrderitemAlterationitemCollection, orderitemAlterationitemIncludeFields);
            IPrefetchPathElement prefetchOrderitemAlterationItemAlterationitem = prefetchOrderitemAlterationItem.SubPath.Add(OrderitemAlterationitemEntityBase.PrefetchPathAlterationitemEntity);
            IPrefetchPathElement prefetchOrderitemOrder = prefetch.Add(OrderitemEntityBase.PrefetchPathOrderEntity, orderIncludeFields).SubPath.Add(OrderEntityBase.PrefetchPathOrderitemCollection, orderitemIncludes);
            IPrefetchPathElement prefetchOrderitemProduct = prefetch.Add(OrderitemEntityBase.PrefetchPathProductEntity, productIncludeFields);

            // Create the sort
            SortExpression sort = new SortExpression(new SortClause(OrderitemFields.CreatedUTC, SortOperator.Descending));
            
            this.orderitemCollection = new OrderitemCollection();
            this.orderitemCollection.GetMulti(filter, 0, sort, relations, prefetch, orderitemIncludes, 0, 0);
        }

        #region Processing methods

        private void ProcessData()
        {
            this.Product = new Product();

            if (this.orderitemCollection.Count > 0)
            {
                this.Product.ProductId = this.orderitemCollection[0].ProductId ?? -1;
                this.Product.Name = this.orderitemCollection[0].ProductName;

                // Populate the orderitem models i.e. gather all info
                this.PopulateOrderitemModels();
            }
        }

        private void PopulateOrderitemModels()
        {
            ReportSettings settings = new ReportSettings(this.request.CompanyId);
            
            foreach (OrderitemEntity orderitemEntity in this.orderitemCollection)
            {
                // Create the orderitem model
                Orderitem orderitem = new Orderitem(settings);
                orderitem.Created = orderitemEntity.CreatedUTC.HasValue ? orderitemEntity.CreatedUTC.Value.UtcToLocalTime(settings.CompanyTimeZoneInfo) : new DateTime();
                orderitem.OrderitemId = orderitemEntity.OrderitemId;
                orderitem.OrderId = orderitemEntity.OrderId;
                orderitem.CategoryId = orderitemEntity.CategoryId ?? -1;
                orderitem.CategoryName = orderitemEntity.CategoryName;
                orderitem.ProductId = orderitemEntity.ProductId ?? -1;
                orderitem.ProductName = orderitemEntity.ProductName;
                orderitem.ClientId = orderitemEntity.OrderEntity.ClientId ?? -1;
                orderitem.DeliverypointId = orderitemEntity.OrderEntity.DeliverypointId ?? -1;
                orderitem.DeliverypointName = orderitemEntity.OrderEntity.DeliverypointName;
                orderitem.DeliverypointNumber = orderitemEntity.OrderEntity.DeliverypointNumber;
                orderitem.DeliverypointgroupName = orderitemEntity.OrderEntity.DeliverypointgroupName;
                orderitem.Status = orderitemEntity.OrderEntity.StatusText;
                orderitem.Processed = orderitemEntity.OrderEntity.ProcessedUTC.HasValue ? orderitemEntity.OrderEntity.ProcessedUTC.Value.UtcToLocalTime(settings.CompanyTimeZoneInfo) : new DateTime();
                orderitem.Notes = orderitemEntity.OrderEntity.Notes;

                OrderitemAlterationitemCollection orderitemAlterationitemCollection = new OrderitemAlterationitemCollection();
                orderitemAlterationitemCollection.AddRange(orderitemEntity.OrderitemAlterationitemCollection);
                
                if (this.request.ProductIdsFromAlterationProducts != null && this.request.ProductIdsFromAlterationProducts.Count > 0)
                {
                    foreach (OrderitemEntity tmpOrderitemEntity in orderitemEntity.OrderEntity.OrderitemCollection)
                    {
                        if (tmpOrderitemEntity.ProductId.GetValueOrDefault(0) > 0 && 
                            this.request.ProductIdsFromAlterationProducts.Contains(tmpOrderitemEntity.ProductId.Value))
                        {                            
                            orderitemAlterationitemCollection.AddRange(tmpOrderitemEntity.OrderitemAlterationitemCollection);
                        }
                    }
                }                

                foreach (OrderitemAlterationitemEntity orderitemAlterationitemEntity in orderitemAlterationitemCollection)
                {
                    if (orderitemAlterationitemEntity.AlterationitemEntity == null)
                        continue;

                    Alterationitem alterationitem = new Alterationitem(settings);
                    alterationitem.AlterationitemId = orderitemAlterationitemEntity.AlterationitemId ?? -1;
                    alterationitem.AlterationName = orderitemAlterationitemEntity.AlterationName;
                    alterationitem.AlterationoptionName = orderitemAlterationitemEntity.AlterationoptionName;
                    alterationitem.Time = orderitemAlterationitemEntity.Time.HasValue ? orderitemAlterationitemEntity.Time.Value.ToTimeZone(settings.DatabaseTimeZoneInfo, settings.CompanyTimeZoneInfo) : new DateTime();
                    alterationitem.Value = orderitemAlterationitemEntity.Value;

                    if (!this.Product.Alterationitems.ContainsKey(orderitemAlterationitemEntity.AlterationitemEntity.AlterationId))
                    {
                        this.Product.Alterationitems.Add(orderitemAlterationitemEntity.AlterationitemEntity.AlterationId, alterationitem);
                    }

                    orderitem.Alterationitems.Add(alterationitem);
                }

                this.Product.Orderitems.Add(orderitem.OrderitemId, orderitem);
            }
        }

        #endregion

        #endregion
    }
}
