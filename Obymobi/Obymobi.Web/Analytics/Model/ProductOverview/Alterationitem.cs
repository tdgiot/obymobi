﻿using System;
using Dionysos;

namespace Obymobi.Web.Analytics.Model.ProductOverview
{
    public class Alterationitem
    {
        #region Fields

        private readonly ReportSettings settings;

        #endregion

        #region Constructors

        public Alterationitem(ReportSettings settings)
        {
            this.settings = settings;
        }

        #endregion

        #region Properties

        public int AlterationitemId { get; set; }

        public string AlterationName { get; set; }

        public string AlterationoptionName { get; set; }

        public DateTime? Time { get; set; }

        public string Value { get; set; }

        public string TimeAsString
        {
            get
            {
                if (this.Time.HasValue)
                {
                    return this.settings.ToClockModeString(this.Time.Value);
                }
                return string.Empty;
            }
        }

        public string NameTimeOrValue
        {
            get
            {
                if (!this.AlterationoptionName.IsNullOrWhiteSpace())
                {
                    return this.AlterationoptionName;
                }
                else if (this.Time.HasValue)
                {
                    return this.TimeAsString;
                }
                else if (!this.Value.IsNullOrWhiteSpace())
                {
                    return this.Value;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        #endregion
    }
}
