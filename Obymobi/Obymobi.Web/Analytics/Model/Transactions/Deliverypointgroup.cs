﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Analytics.Model.Transactions
{
    public class Deliverypointgroup
    {
	    public Deliverypointgroup()
        {
            this.Menus = new Dictionary<int, Menu>();
            this.Orders = new Dictionary<int, Order>();
        }

	    public string Key { get; set; }

        public string Name { get; set; }

        public Dictionary<int, Menu> Menus { get; set; }

        public Dictionary<int, Order> Orders { get; set; }

        public int OrderCountStandardOrderInRoomTablet
        {
            get { return this.Orders.Values.Count(x => x.Type == OrderType.Standard && !x.MobileOrder); }
        }

        public int OrderCountRequestForServiceInRoomTablet
        {
            get { return this.Orders.Values.Count(x => x.Type == OrderType.RequestForService && !x.MobileOrder); }
        }

        public int OrderCountInRoomTablet
        {
            get { return this.OrderCountStandardOrderInRoomTablet + this.OrderCountRequestForServiceInRoomTablet; }
        }

        public decimal OrderRevenueStandardOrderInRoomTablet
        {
            get { return this.Orders.Values.Where(x => x.Type == OrderType.Standard && !x.MobileOrder).Sum(x => x.Revenue); }
        }

        public decimal OrderRevenueRequestForServiceInRoomTablet
        {
            get { return this.Orders.Values.Where(x => x.Type == OrderType.RequestForService && !x.MobileOrder).Sum(x => x.Revenue); }
        }

        public decimal OrderRevenueInRoomTablet
        {
            get { return this.OrderRevenueStandardOrderInRoomTablet + this.OrderRevenueRequestForServiceInRoomTablet; }
        }

        public int OrderCountStandardOrderByod
        {
            get { return this.Orders.Values.Count(x => x.Type == OrderType.Standard && x.MobileOrder); }
        }

        public int OrderCountRequestForServiceByod
        {
            get { return this.Orders.Values.Count(x => x.Type == OrderType.RequestForService && x.MobileOrder); }
        }

        public int OrderCountByod
        {
            get { return this.OrderCountStandardOrderByod + this.OrderCountRequestForServiceByod; }
        }

        public decimal OrderRevenueStandardOrderByod
        {
            get { return this.Orders.Values.Where(x => x.Type == OrderType.Standard && x.MobileOrder).Sum(x => x.Revenue); }
        }

        public decimal OrderRevenueRequestForServiceByod
        {
            get { return this.Orders.Values.Where(x => x.Type == OrderType.RequestForService && x.MobileOrder).Sum(x => x.Revenue); }
        }

        public decimal OrderRevenueByod
        {
            get { return this.OrderRevenueStandardOrderByod + this.OrderRevenueRequestForServiceByod; }
        }

        public int OrderCountStandardOrder
        {
            get { return this.OrderCountStandardOrderInRoomTablet + this.OrderCountStandardOrderByod; }
        }

        public int OrderCountRequestForService
        {
            get { return this.OrderCountRequestForServiceInRoomTablet + this.OrderCountRequestForServiceByod; }
        }

        public int OrderCount
        {
            get { return this.OrderCountStandardOrder + this.OrderCountRequestForService; }
        }

        public decimal OrderRevenueStandardOrder
        {
            get { return this.OrderRevenueStandardOrderInRoomTablet + this.OrderRevenueStandardOrderByod; }
        }

        public decimal OrderRevenueRequestForService
        {
            get { return this.OrderRevenueRequestForServiceInRoomTablet + this.OrderRevenueRequestForServiceByod; }
        }

        public decimal OrderRevenue
        {
            get { return this.OrderRevenueStandardOrder + this.OrderRevenueRequestForService; }
        }
    }
}
