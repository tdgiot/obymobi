﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Analytics.Model.Transactions
{
    public class Orderitem
    {
	    public int OrderitemId { get; set; }

        public int OrderId { get; set; }

        public Category Category { get; set; }

        public Product Product { get; set; }

        public Menu Menu { get; set; }

        public OrderType Type { get; set; }

        public Deliverypointgroup Deliverypointgroup { get; set; }

        public Order Order { get; set; }


        public string CategoryName { get; set; }

        public string CategoryPath { get; set; }

        public int Quantity { get; set; }

        public decimal PriceProduct { get; set; }

        public string ProductName { get; set; }

        public decimal PriceAlterations { get; set; }

        public decimal PricePerPiece 
        { 
            get
            {
                return (this.PriceProduct + this.PriceAlterations);
            }
        }

        public decimal Revenue
        {
            get
            {
                return (decimal)this.Quantity * this.PricePerPiece;
            }
        }

        public override string ToString()
        {
            return string.Format("{0} × {1} ({2}) = {3}", this.Quantity, this.ProductName, this.PricePerPiece, this.Revenue);
        }
    }
}
