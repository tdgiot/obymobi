﻿using Obymobi.Enums;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.Transactions
{
    public class Category
    {
	    public Category()
        {
            this.Categories = new Dictionary<string, Category>();
            this.Products = new Dictionary<string, Product>();
        }

	    public string Key { get; set; }

        public int? CategoryId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public OrderType? Type { get; set; } 
        public Dictionary<string, Category> Categories { get; set; }
        public Dictionary<string, Product> Products { get; set; }

        public decimal Revenue
        {
            get
            {
                if (this.Products.Count > 0)
                {
                    return this.Products.Values.Sum(x => x.Revenue);
                }
                return this.Categories.Values.Sum(x => x.Revenue);
            }
        }

        public int Quantity
        {
            get
            {
                if (this.Products.Count > 0)
                {
                    return this.Products.Values.Sum(x => x.Quantity);
                }
                return this.Categories.Values.Sum(x => x.Quantity);
            }
        }

        public override string ToString()
        {
            return string.Format("{0} ({1})", this.Path, this.Revenue);
        }
    }
}