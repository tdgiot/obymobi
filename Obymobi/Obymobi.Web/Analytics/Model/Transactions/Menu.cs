﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Analytics.Model.Transactions
{
    public class Menu
    {
	    public Menu()
        {
            this.Categories = new Dictionary<string, Category>();
        }

	    public int MenuId { get; set; }
        public string Name { get; set; }
        public Deliverypointgroup Deliverypointgroup { get; set; }
        public Dictionary<string, Category> Categories { get; set; }

        public decimal Revenue
        {
            get
            {
                return this.Categories.Values.Sum(x => x.Revenue);
            }
        }

        public override string ToString()
        {
            return string.Format("{0} ({1})", this.Name, this.Revenue);
        }
    }
}
