﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Web.Analytics.Requests;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;
using System;
using Dionysos;
using Google.Apis.Manual.Util;

namespace Obymobi.Web.Analytics.Model.Transactions
{
	public class Transactions
	{
		private readonly TransactionsRequest request;

		private OrderitemCollection orderitemCollection;
		private MenuCollection menuCollection;
		private DeliverypointgroupCollection deliverypointgroupCollection;

		public Transactions(TransactionsRequest request)
		{
			if (request == null)
				throw new ArgumentNullException("request");

			this.request = request;

			this.Orderitems = new Dictionary<int, Orderitem>();
			this.Categories = new Dictionary<string, Category>();
			this.Products = new Dictionary<string, Product>();
			this.Menus = new Dictionary<int, Menu>();
			this.Deliverypointgroups = new Dictionary<string, Deliverypointgroup>();
			this.Orders = new Dictionary<int, Order>();
		}

		private Dictionary<int, Orderitem> Orderitems { get; set; }

		public Dictionary<string, Category> Categories { get; set; }

		public Dictionary<string, Product> Products { get; set; }

		public Dictionary<int, Menu> Menus { get; set; }

		public Dictionary<string, Deliverypointgroup> Deliverypointgroups { get; set; }

		public Dictionary<int, Order> Orders { get; set; }

		public bool IncludeOrderitems { get { return this.request.IncludeOrderitems; } }

		public int OrderCountStandardOrderInRoomTablet
		{
			get { return this.Orders.Values.Count(x => x.Type == OrderType.Standard && !x.MobileOrder); }
		}

		public int OrderCountRequestForServiceInRoomTablet
		{
			get { return this.Orders.Values.Count(x => x.Type == OrderType.RequestForService && !x.MobileOrder); }
		}

		public int OrderCountInRoomTablet
		{
			get { return this.OrderCountStandardOrderInRoomTablet + this.OrderCountRequestForServiceInRoomTablet; }
		}

		public decimal OrderRevenueStandardOrderInRoomTablet
		{
			get { return this.Orders.Values.Where(x => x.Type == OrderType.Standard && !x.MobileOrder).Sum(x => x.Revenue); }
		}

		public decimal OrderRevenueRequestForServiceInRoomTablet
		{
			get { return this.Orders.Values.Where(x => x.Type == OrderType.RequestForService && !x.MobileOrder).Sum(x => x.Revenue); }
		}

		public decimal OrderRevenueInRoomTablet
		{
			get { return this.OrderRevenueStandardOrderInRoomTablet + this.OrderRevenueRequestForServiceInRoomTablet; }
		}

		public int OrderCountStandardOrderByod
		{
			get { return this.Orders.Values.Count(x => x.Type == OrderType.Standard && x.MobileOrder); }
		}

		public int OrderCountRequestForServiceByod
		{
			get { return this.Orders.Values.Count(x => x.Type == OrderType.RequestForService && x.MobileOrder); }
		}

		public int OrderCountByod
		{
			get { return this.OrderCountStandardOrderByod + this.OrderCountRequestForServiceByod; }
		}

		public decimal OrderRevenueStandardOrderByod
		{
			get { return this.Orders.Values.Where(x => x.Type == OrderType.Standard && x.MobileOrder).Sum(x => x.Revenue); }
		}

		public decimal OrderRevenueRequestForServiceByod
		{
			get { return this.Orders.Values.Where(x => x.Type == OrderType.RequestForService && x.MobileOrder).Sum(x => x.Revenue); }
		}

		public decimal OrderRevenueByod
		{
			get { return this.OrderRevenueStandardOrderByod + this.OrderRevenueRequestForServiceByod; }
		}

		public int OrderCountStandardOrder
		{
			get { return this.OrderCountStandardOrderInRoomTablet + this.OrderCountStandardOrderByod; }
		}

		public int OrderCountRequestForService
		{
			get { return this.OrderCountRequestForServiceInRoomTablet + this.OrderCountRequestForServiceByod; }
		}

		public int OrderCount
		{
			get { return this.OrderCountStandardOrder + this.OrderCountRequestForService; }
		}

		public decimal OrderRevenueStandardOrder
		{
			get { return this.OrderRevenueStandardOrderInRoomTablet + this.OrderRevenueStandardOrderByod; }
		}

		public decimal OrderRevenueRequestForService
		{
			get { return this.OrderRevenueRequestForServiceInRoomTablet + this.OrderRevenueRequestForServiceByod; }
		}

		public decimal OrderRevenue
		{
			get { return this.OrderRevenueStandardOrder + this.OrderRevenueRequestForService; }
		}

		public List<Product> TopProductsFromStandardOrdersByQuantity
		{
			get { return this.Products.Values.Where(x => x.Type == OrderType.Standard).OrderByDescending(x => x.Quantity).Take(10).ToList(); }
		}

		public List<Product> TopProductsFromRequestsForServiceByQuantity
		{
			get { return this.Products.Values.Where(x => x.Type == OrderType.RequestForService).OrderByDescending(x => x.Quantity).Take(10).ToList(); }
		}

		public List<Category> TopCategoriesFromStandardOrdersByQuantity
		{
			get { return this.Categories.Values.Where(x => x.Type == OrderType.Standard).OrderByDescending(x => x.Quantity).Take(10).ToList(); }
		}

		public List<Category> TopCategoriesFromRequestsForServiceByQuantity
		{
			get { return this.Categories.Values.Where(x => x.Type == OrderType.RequestForService).OrderByDescending(x => x.Quantity).Take(10).ToList(); }
		}

		public List<Product> TopProductsFromStandardOrdersByRevenue
		{
			get { return this.Products.Values.Where(x => x.Type == OrderType.Standard).OrderByDescending(x => x.Revenue).Take(10).ToList(); }
		}

		public List<Product> TopProductsFromRequestForServiceByRevenue
		{
			get { return this.Products.Values.Where(x => x.Type == OrderType.RequestForService).OrderByDescending(x => x.Revenue).Take(10).ToList(); }
		}

		public List<Category> TopCategoriesFromStandardOrdersByRevenue
		{
			get { return this.Categories.Values.Where(x => x.Type == OrderType.Standard).OrderByDescending(x => x.Revenue).Take(10).ToList(); }
		}

		public List<Category> TopCategoriesFromRequestsForServiceByRevenue
		{
			get { return this.Categories.Values.Where(x => x.Type == OrderType.RequestForService).OrderByDescending(x => x.Revenue).Take(10).ToList(); }
		}


		public void FetchAndProcessData()
		{
			this.FetchOrderitems();
			this.ProcessData();
		}

		private void FetchOrderitems()
		{
			// Create the filter and the relations
			PredicateExpression filter = new PredicateExpression();
			RelationCollection relations = new RelationCollection();

			// Add filter on CompanyId and date range
			filter.Add(OrderFields.CompanyId == this.request.CompanyId);
			filter.Add(OrderFields.CreatedUTC >= this.request.FromUtc);
			filter.Add(OrderFields.CreatedUTC <= this.request.TillUtc);

			if (this.request.ReportCategoryFilter == ReportCategoryFilter.IncludeCategories)
			{
				filter.Add(OrderitemFields.CategoryId == this.request.CategoryIds);
			}
			else if (this.request.ReportCategoryFilter == ReportCategoryFilter.ExcludeCategories)
			{
				filter.Add(OrderitemFields.CategoryId != this.request.CategoryIds);
			}

			if (this.request.OrderType.HasValue)
			{
				filter.Add(OrderFields.Type == this.request.OrderType);
			}

			if (!this.request.IncludeByodApps)
			{
				filter.Add(OrderFields.MobileOrder == false);
			}
			else if (!this.request.IncludeInRoomTablets)
			{
				filter.Add(OrderFields.MobileOrder == true);
			}

			// Add relation Orderitem > Order
			relations.Add(OrderitemEntityBase.Relations.OrderEntityUsingOrderId, JoinHint.Left);

			// Add filter on DeliverypointIds
			if (this.request.DeliverypointIds != null && this.request.DeliverypointIds.Count > 0)
			{
				PredicateExpression subfilter = new PredicateExpression();

				if (this.request.DeliverypointsExclude)
				{
					subfilter.Add(OrderFields.DeliverypointId != this.request.DeliverypointIds);

					if (this.request.DeliverypointgroupIds != null && this.request.DeliverypointgroupIds.Count > 0)
					{
						subfilter.Add(DeliverypointFields.DeliverypointgroupId == this.request.DeliverypointgroupIds);
						relations.Add(OrderEntityBase.Relations.DeliverypointEntityUsingDeliverypointId); // Left join, so orders without deliverypoint are also included
					}
				}
				else
				{
					subfilter.Add(OrderFields.DeliverypointId == this.request.DeliverypointIds);
				}
				subfilter.AddWithOr(OrderFields.DeliverypointId == DBNull.Value);
				filter.Add(subfilter);
			}
			else if (this.request.DeliverypointgroupIds != null && this.request.DeliverypointgroupIds.Count > 0)
			{
				PredicateExpression subfilter = new PredicateExpression();
				subfilter.Add(DeliverypointFields.DeliverypointgroupId == this.request.DeliverypointgroupIds);
				subfilter.AddWithOr(OrderFields.DeliverypointId == DBNull.Value);
				filter.Add(subfilter);

				relations.Add(OrderEntityBase.Relations.DeliverypointEntityUsingDeliverypointId, JoinHint.Left); // Left join, so orders without deliverypoint are also included
			}

			// Add filter on failed orders
			if (!this.request.IncludeFailedOrders)
			{
				filter.Add(OrderFields.ErrorCode == 0);
			}

			// Create the included fields list for the alterations
			IncludeFieldsList orderitemAlterationitemIncludeFields = new IncludeFieldsList(OrderitemAlterationitemFields.AlterationoptionPriceIn, OrderitemAlterationitemFields.AlterationName, OrderitemAlterationitemFields.AlterationoptionName, OrderitemAlterationitemFields.ParentCompanyId);
			IncludeFieldsList orderIncludeFields = new IncludeFieldsList(OrderFields.Type, OrderFields.DeliverypointgroupName, OrderFields.DeliverypointName, OrderFields.CreatedUTC, OrderFields.CompanyId, OrderFields.MobileOrder);
			IncludeFieldsList deliverypointIncludeFields = new IncludeFieldsList(DeliverypointFields.Number, DeliverypointFields.Name, DeliverypointFields.CompanyId);
			IncludeFieldsList deliverypointgroupIncludeFields = new IncludeFieldsList(DeliverypointgroupFields.Name, DeliverypointgroupFields.CompanyId);
			IncludeFieldsList categoryIncludeFields = new IncludeFieldsList(CategoryFields.Name, CategoryFields.CompanyId);
			IncludeFieldsList menuIncludeFields = new IncludeFieldsList(MenuFields.Name, MenuFields.CompanyId);
			IncludeFieldsList orderRoutestephandlerIncludeFields = new IncludeFieldsList(OrderRoutestephandlerFields.HandlerType, OrderRoutestephandlerFields.BeingHandledUTC, OrderRoutestephandlerFields.ParentCompanyId);
			IncludeFieldsList orderRoutestephandlerHistoryIncludeFields = new IncludeFieldsList(OrderRoutestephandlerHistoryFields.HandlerType, OrderRoutestephandlerHistoryFields.BeingHandledUTC, OrderRoutestephandlerHistoryFields.ParentCompanyId);

			// Create the prefetch
			PrefetchPath prefetch = new PrefetchPath(EntityType.OrderitemEntity);
			IPrefetchPathElement prefetchOrderitemAlterationItem = prefetch.Add(OrderitemEntityBase.PrefetchPathOrderitemAlterationitemCollection, orderitemAlterationitemIncludeFields);
			IPrefetchPathElement prefetchOrderitemOrder = prefetch.Add(OrderitemEntityBase.PrefetchPathOrderEntity, orderIncludeFields);
			IPrefetchPathElement prefetchOrderitemOrderDeliverypoint = prefetchOrderitemOrder.SubPath.Add(OrderEntityBase.PrefetchPathDeliverypointEntity, deliverypointIncludeFields);
			IPrefetchPathElement prefetchOrderitemOrderDeliverypointDeliverypointgroup = prefetchOrderitemOrderDeliverypoint.SubPath.Add(DeliverypointEntityBase.PrefetchPathDeliverypointgroupEntity, deliverypointgroupIncludeFields);
			IPrefetchPathElement prefetchOrderitemOrderDeliverypointDeliverypointgroupMenu = prefetchOrderitemOrderDeliverypointDeliverypointgroup.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathMenuEntity, menuIncludeFields);
			IPrefetchPathElement prefetchOrderitemOrderOrderRoutestephandler = prefetchOrderitemOrder.SubPath.Add(OrderEntityBase.PrefetchPathOrderRoutestephandlerCollection, orderRoutestephandlerIncludeFields);
			IPrefetchPathElement prefetchOrderitemOrderOrderRoutestephandlerHistory = prefetchOrderitemOrder.SubPath.Add(OrderEntityBase.PrefetchPathOrderRoutestephandlerHistoryCollection, orderRoutestephandlerHistoryIncludeFields);
			IPrefetchPathElement prefetchOrderitemCategory = prefetch.Add(OrderitemEntityBase.PrefetchPathCategoryEntity, categoryIncludeFields);
			IPrefetchPathElement prefetchOrderitemCategoryMenu = prefetchOrderitemCategory.SubPath.Add(CategoryEntityBase.PrefetchPathMenuEntity, menuIncludeFields);

			// Create the included fields list for the orderitems
			IncludeFieldsList orderitemIncludeFields = new IncludeFieldsList();
			orderitemIncludeFields.Add(OrderitemFields.ProductName);
			orderitemIncludeFields.Add(OrderitemFields.ProductPriceIn);
			orderitemIncludeFields.Add(OrderitemFields.Quantity);
			orderitemIncludeFields.Add(OrderitemFields.CategoryName);
			orderitemIncludeFields.Add(OrderitemFields.ParentCompanyId);

			// Create the sort
			SortExpression sort = new SortExpression(new SortClause(OrderitemFields.OrderitemId, SortOperator.Ascending));

			// Create the orderitem collection 
			// and retrieve the order items
			this.orderitemCollection = new OrderitemCollection();
			this.orderitemCollection.GetMulti(filter, 0, sort, relations, prefetch, orderitemIncludeFields, 0, 0);
		}

		private void ProcessData()
		{
			// Reset the dictionaries
			this.Orderitems = new Dictionary<int, Orderitem>();
			this.Categories = new Dictionary<string, Category>();
			this.Products = new Dictionary<string, Product>();
			this.Menus = new Dictionary<int, Menu>();
			this.Deliverypointgroups = new Dictionary<string, Deliverypointgroup>();
			this.Orders = new Dictionary<int, Order>();

			// Populate the orderitem models i.e. gather all info
			this.PopulateOrderitemModels();

			// Populate the menu model including full category structure
			this.PopulateMenus();

			// Add the products that are not present anymore
			this.AddProductsThatWereRemovedFromCategories();

			// Add the categories that have been deleted
			this.AddDeletedCategories();

			// Populate the menus per deliverypointgroup
			this.PopulateMenusPerDeliverypointgroup();
		}

		private void PopulateOrderitemModels()
		{
			foreach (OrderitemEntity orderitemEntity in this.orderitemCollection)
			{
				// Create the orderitem model
				Orderitem orderitem = new Orderitem();
				orderitem.OrderitemId = orderitemEntity.OrderitemId;
				orderitem.OrderId = orderitemEntity.OrderId;
				orderitem.PriceProduct = orderitemEntity.ProductPriceIn;
				orderitem.ProductName = orderitemEntity.ProductName;
				orderitem.Quantity = orderitemEntity.Quantity;
				orderitem.PriceAlterations = orderitemEntity.OrderitemAlterationitemCollection.Sum(x => x.AlterationoptionPriceIn);

				// Get or create the menu model 
				Menu menu = this.GetOrCreateMenuModel(orderitemEntity);

				// Get or create the product model
				// and add the orderitem to the product
				Product product = this.GetOrCreateProductModel(orderitemEntity.ProductId, orderitemEntity.ProductName);
				product.Orderitems.Add(orderitem.OrderitemId, orderitem);

				// Get or create the category model
				// and add the product model
				Category category = this.GetOrCreateCategoryModel(orderitemEntity.CategoryId, orderitemEntity.CategoryName);
				if (!product.Categories.ContainsKey(category.Key))
				{
					product.Categories.Add(category.Key, category);
				}

				// Get or create the deliverypointgroup model
				Deliverypointgroup deliverypointgroup = this.GetOrCreateDeliverypointgroupModel(orderitemEntity);

				// Get or create the order model
				// and set the deliverypointgroup of the order
				Order order = this.GetOrCreateOrderModel(orderitemEntity, orderitem);
				order.Deliverypointgroup = deliverypointgroup;

				// Add the order mode to the deliverypointgroup model
				if (deliverypointgroup != null && !deliverypointgroup.Orders.ContainsKey(order.OrderId))
				{
					deliverypointgroup.Orders.Add(order.OrderId, order);
				}

				// Set the model properties of the orderitemS
				orderitem.Type = order.Type;
				orderitem.Menu = menu;
				orderitem.Category = category;
				orderitem.Product = product;
				orderitem.Deliverypointgroup = deliverypointgroup;
				orderitem.Order = order;

				product.Type = order.Type;
				category.Type = order.Type;
				menu.Deliverypointgroup = deliverypointgroup;

				this.Orderitems.Add(orderitemEntity.OrderitemId, orderitem);
			}
		}

		private void PopulateMenus()
		{
			// Get a list of the menu ids
			List<int> menuIds = this.Orderitems.Values.Where(x => x.Menu != null).Select(x => x.Menu.MenuId).Distinct().ToList();

			// Fetch the categories using the menu ids
			CategoryCollection categoryCollection = this.FetchCategories(menuIds);

			// Process the menus
			foreach (int menuId in menuIds)
			{
				Menu menu = this.Menus[menuId];

				List<CategoryEntity> categoriesForMenu = categoryCollection.Where(x => x.MenuId.HasValue && x.MenuId.Value == menuId).ToList();
				List<CategoryEntity> rootCategoriesForMenu = categoriesForMenu.Where(x => x.IsRootCategory).ToList();

				foreach (CategoryEntity categoryEntity in rootCategoriesForMenu)
				{
					Category category = this.GetOrCreateCategoryModel(categoryEntity.CategoryId, categoryEntity.Name);
					category.Path = string.Format("{0}\\{1}", menu.Name, category.Name);

					// Add the child categories to the root category
					if (!menu.Categories.ContainsKey(category.Key))
					{
						menu.Categories.Add(category.Key, category);
					}

					// Add the products to the category (if any in a root category)
					foreach (ProductEntity productEntity in categoryEntity.ProductCollectionViaProductCategory)
					{
						if (!category.Products.ContainsKey(productEntity.ProductId.ToString()))
						{
							Product product = new Product();
							product.Key = productEntity.ProductId.ToString();
							product.ProductId = productEntity.ProductId;
							product.Name = productEntity.Name;

							category.Products.Add(product.Key, product);
						}
					}

					this.PopulateChildCategories(category, categoriesForMenu, category.Path);
				}
			}
		}

		private CategoryCollection FetchCategories(List<int> menuIds)
		{
			PredicateExpression filter = new PredicateExpression(MenuFields.MenuId == menuIds);

			RelationCollection categoryRelations = new RelationCollection(CategoryEntity.Relations.MenuEntityUsingMenuId);
			RelationCollection productRelations = new RelationCollection(ProductEntity.Relations.ProductCategoryEntityUsingProductId);

			// Create the included fields lists
			IncludeFieldsList categoryIncludeFields = new IncludeFieldsList(CategoryFields.Name, CategoryFields.SortOrder, CategoryFields.CompanyId);
			IncludeFieldsList productIncludeFields = new IncludeFieldsList(ProductFields.Name, ProductFields.CompanyId);

			SortExpression categorySort = new SortExpression(new SortClause(CategoryFields.SortOrder, SortOperator.Ascending));
			SortExpression productCategorySort = new SortExpression(new SortClause(ProductCategoryFields.SortOrder, SortOperator.Ascending));

			// Create the prefetch for the categories
			PrefetchPath prefetch = new PrefetchPath(EntityType.CategoryEntity);
			prefetch.Add(CategoryEntity.PrefetchPathProductCollectionViaProductCategory, 0, null, productRelations, productCategorySort, productIncludeFields);

			CategoryCollection categoryCollection = new CategoryCollection();
			categoryCollection.GetMulti(filter, 0, categorySort, categoryRelations, prefetch, categoryIncludeFields, 0, 0);

			return categoryCollection;
		}

		private void PopulateChildCategories(Category category, List<CategoryEntity> categoriesForMenu, string path)
		{
			List<CategoryEntity> childCategories = categoriesForMenu.Where(x => x.ParentCategoryId.HasValue && x.ParentCategoryId == category.CategoryId).ToList();
			foreach (CategoryEntity childCategoryEntity in childCategories)
			{
				// Create the child category model
				Category childCategory = this.GetOrCreateCategoryModel(childCategoryEntity.CategoryId, childCategoryEntity.Name);
				childCategory.Path = path + string.Format("\\{0}", childCategory.Name);

				// Add the child category to the parent category
				if (!category.Categories.ContainsKey(childCategory.Key))
				{
					category.Categories.Add(childCategory.Key, childCategory);
				}

				// Add the products to the category
				foreach (ProductEntity productEntity in childCategoryEntity.ProductCollectionViaProductCategory)
				{
					if (!childCategory.Products.ContainsKey(productEntity.ProductId.ToString()))
					{
						Product product = new Product();
						product.Key = productEntity.ProductId.ToString();
						product.ProductId = productEntity.ProductId;
						product.Name = productEntity.Name;

						childCategory.Products.Add(product.Key, product);
					}
				}

				this.PopulateChildCategories(childCategory, categoriesForMenu, path + string.Format("\\{0}", childCategory.Name));
			}
		}

		private void AddProductsThatWereRemovedFromCategories()
		{
			foreach (Orderitem orderitem in this.Orderitems.Values)
			{
				Product product;
				if (orderitem.Category.Products.ContainsKey(orderitem.Product.Key))
				{
					product = orderitem.Category.Products[orderitem.Product.Key];
				}
				else
				{
					product = new Product();
					product.Key = orderitem.Product.Key;
					product.ProductId = orderitem.Product.ProductId;
					product.Name = orderitem.Product.Name + " (Removed from category)";
					product.Type = orderitem.Type;

					orderitem.Category.Products.Add(product.Key, product);
				}

				if (!product.Orderitems.ContainsKey(orderitem.OrderitemId))
					product.Orderitems.Add(orderitem.OrderitemId, orderitem);
			}
		}

		private void AddDeletedCategories()
		{
			foreach (Orderitem orderitem in this.Orderitems.Values)
			{
				Category category = orderitem.Category;
				if (!category.CategoryId.HasValue) // Category was deleted
				{
					Category deletedCategory;
					if (orderitem.Menu.Categories.ContainsKey(category.Key)) // Check whether the deleted category is already in the menu
					{
						deletedCategory = orderitem.Menu.Categories[category.Key];
					}
					else
					{
						deletedCategory = category;
						deletedCategory.Name += " (Category has been deleted)";
						deletedCategory.Path = deletedCategory.Name;
						orderitem.Menu.Categories.Add(deletedCategory.Key, deletedCategory);
					}

					Product product;
					if (deletedCategory.Products.ContainsKey(orderitem.Product.Key))
					{
						product = deletedCategory.Products[orderitem.Product.Key];
					}
					else
					{
						product = new Product();
						product.Key = orderitem.Product.Key;
						product.ProductId = orderitem.Product.ProductId;
						product.Name = orderitem.Product.Name;
						product.Type = orderitem.Type;

						deletedCategory.Products.Add(product.Key, product);
					}

					if (!product.Orderitems.ContainsKey(orderitem.OrderitemId))
						product.Orderitems.Add(orderitem.OrderitemId, orderitem);
				}
			}
		}

		private void PopulateMenusPerDeliverypointgroup()
		{
			foreach (Orderitem orderitem in this.Orderitems.Values)
			{
				Deliverypointgroup deliverypointgroup = orderitem.Deliverypointgroup;
				if (deliverypointgroup != null && !deliverypointgroup.Menus.ContainsKey(orderitem.Menu.MenuId))
				{
					deliverypointgroup.Menus.Add(orderitem.Menu.MenuId, orderitem.Menu);
				}
			}
		}

		private Category GetOrCreateCategoryModel(int? categoryId, string categoryName)
		{
			Category category = null;

			string categoryKey = string.Empty;
			if (categoryId.HasValue)
			{
				categoryKey = categoryId.Value.ToString();
			}
			else if (!categoryName.IsNullOrWhiteSpace())
			{
				categoryKey = categoryName;
			}

			if (!categoryKey.IsNullOrWhiteSpace())
			{
				if (this.Categories.ContainsKey(categoryKey))
				{
					category = this.Categories[categoryKey];
				}
				else
				{
					category = new Category();
					category.Key = categoryKey;
					category.CategoryId = categoryId;

					if (categoryName.Contains("\\"))
						category.Name = categoryName.Substring(categoryName.LastIndexOf("\\")).Replace("\\", string.Empty);
					else
						category.Name = categoryName;

					this.Categories.Add(categoryKey, category);
				}
			}
			else
			{
				if (this.Categories.ContainsKey("-1"))
				{
					category = this.Categories["-1"];
				}
				else
				{
					category = new Category();
					category.Key = "-1";
					category.Name = "Unknown category";

					this.Categories.Add("-1", category);
				}                
			}

			return category;
		}

		private Product GetOrCreateProductModel(int? productId, string productName)
		{
			Product product = null;

			string productKey = string.Empty;
			if (productId.HasValue)
			{
				productKey = productId.Value.ToString();
			}
			else if (!productName.IsNullOrWhiteSpace())
			{
				productKey = productName;
			}

			if (!productKey.IsNullOrWhiteSpace())
			{
				if (this.Products.ContainsKey(productKey))
				{
					product = this.Products[productKey];
				}
				else
				{
					product = new Product();
					product.Key = productKey;
					product.ProductId = productId;
					product.Name = productName;

					this.Products.Add(productKey, product);
				}
			}

			return product;
		}

		private Deliverypointgroup GetOrCreateDeliverypointgroupModel(OrderitemEntity orderitemEntity)
		{
			Deliverypointgroup deliverypointgroup = null;

			string deliverypointgroupKey = string.Empty;
			string deliverypointgroupName = string.Empty;

			if (orderitemEntity.OrderEntity.DeliverypointId.HasValue)
			{
				deliverypointgroupKey = orderitemEntity.OrderEntity.DeliverypointEntity.DeliverypointgroupId.ToString();
				deliverypointgroupName = orderitemEntity.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.Name;
			}
			else if (!orderitemEntity.OrderEntity.DeliverypointgroupName.IsNullOrWhiteSpace())
			{
				deliverypointgroupKey = orderitemEntity.OrderEntity.DeliverypointgroupName;
				deliverypointgroupName = orderitemEntity.OrderEntity.DeliverypointgroupName;
			}

			if (!deliverypointgroupKey.IsNullOrWhiteSpace() && !deliverypointgroupName.IsNullOrWhiteSpace())
			{
				if (this.Deliverypointgroups.ContainsKey(deliverypointgroupKey))
				{
					deliverypointgroup = this.Deliverypointgroups[deliverypointgroupKey];
				}
				else if (this.Deliverypointgroups.Values.Any(x => x.Name.Equals(deliverypointgroupName)))
				{
					deliverypointgroup = this.Deliverypointgroups.Values.Single(x => x.Name.Equals(deliverypointgroupName));
				}
				else
				{
					deliverypointgroup = new Deliverypointgroup();
					deliverypointgroup.Key = deliverypointgroupKey;
					deliverypointgroup.Name = deliverypointgroupName;

					this.Deliverypointgroups.Add(deliverypointgroupKey, deliverypointgroup);
				}
			}

			return deliverypointgroup;
		}

		private Order GetOrCreateOrderModel(OrderitemEntity orderitemEntity, Orderitem orderitem)
		{
			int orderId = orderitemEntity.OrderId;

			Order order;
			if (this.Orders.ContainsKey(orderId))
			{
				order = this.Orders[orderId];
			}
			else
			{
				order = new Order();
				order.OrderId = orderId;
				order.Type = orderitemEntity.OrderEntity.TypeAsEnum;
				order.MobileOrder = orderitemEntity.OrderEntity.MobileOrder;
				order.Deliverypointgroup = orderitem.Deliverypointgroup;
				order.CreatedUtc = orderitemEntity.OrderEntity.CreatedUTC;
				order.DeliverypointName = orderitemEntity.OrderEntity.DeliverypointName;

				if (orderitemEntity.OrderEntity.CreatedUTC.HasValue)
				{
					// Calculate the difference between the time the order was submitted, and when someone pushed "on the case"
					if (orderitemEntity.OrderEntity.OrderRoutestephandlerCollection.Count > 0)
					{
						// Routestephandlers not archived yet, active order
						OrderRoutestephandlerEntity handledConsoleStep = orderitemEntity.OrderEntity.OrderRoutestephandlerCollection.FirstOrDefault(x => x.BeingHandledUTC != null && x.HandlerType == (int)RoutestephandlerType.Console);
						if (handledConsoleStep != null && handledConsoleStep.BeingHandledUTC.HasValue)
						{
							order.BeingHandledIn = handledConsoleStep.BeingHandledUTC.Value - orderitemEntity.OrderEntity.CreatedUTC.Value;
						}
					}
					else if (orderitemEntity.OrderEntity.OrderRoutestephandlerHistoryCollection.Count > 0)
					{
						// Archived routestephandlers
						OrderRoutestephandlerHistoryEntity handledConsoleStep = orderitemEntity.OrderEntity.OrderRoutestephandlerHistoryCollection.FirstOrDefault(x => x.BeingHandledUTC != null && x.HandlerType == (int)RoutestephandlerType.Console);
						if (handledConsoleStep != null && handledConsoleStep.BeingHandledUTC.HasValue)
						{
							order.BeingHandledIn = handledConsoleStep.BeingHandledUTC.Value - orderitemEntity.OrderEntity.CreatedUTC.Value;
						}
					}
				}
				
				this.Orders.Add(orderId, order);
			}

			if (order != null && order.NumberOfGuests.IsNullOrEmpty())
			{
				// FO: Yeah, I know. Ugly as fuck and won't work anymore when the name is changed but they need to have it today for Aria
				OrderitemAlterationitemEntity numberOfGuestsAlterationItem = orderitemEntity.OrderitemAlterationitemCollection.FirstOrDefault(x => x.AlterationName.Contains("Number of Guests", StringComparison.InvariantCultureIgnoreCase));
				order.NumberOfGuests = numberOfGuestsAlterationItem != null ? numberOfGuestsAlterationItem.AlterationoptionName : string.Empty;
			}

			order.Orderitems.Add(orderitem.OrderitemId, orderitem);

			return order;
		}

		private Menu GetOrCreateMenuModel(OrderitemEntity orderitemEntity)
		{
			Menu menu = null;

			int? menuId = null;
			MenuEntity menuEntity = null;
			if (orderitemEntity.CategoryId.HasValue && orderitemEntity.CategoryEntity.MenuId.HasValue)
			{
				// Get the MenuId via the category
				menuId = orderitemEntity.CategoryEntity.MenuId;
				menuEntity = orderitemEntity.CategoryEntity.MenuEntity;
			}
			else if (orderitemEntity.OrderEntity.DeliverypointId.HasValue && orderitemEntity.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.MenuId.HasValue)
			{
				// Get the MenuId via the deliverypointgroup
				menuId = orderitemEntity.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.MenuId;
				menuEntity = orderitemEntity.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.MenuEntity;
			}

			if (menuId.HasValue && menuEntity != null)
			{
				if (this.Menus.ContainsKey(menuId.Value))
				{
					menu = this.Menus[menuId.Value];
				}
				else
				{
					menu = new Menu();
					menu.MenuId = menuEntity.MenuId;
					menu.Name = menuEntity.Name;

					this.Menus.Add(menuId.Value, menu);
				}
			}
			else
			{
				if (this.Menus.ContainsKey(-1))
				{
					menu = this.Menus[-1];
				}
				else
				{
					menu = new Menu();
					menu.MenuId = -1;
					menu.Name = "(Order items of which menu could not be determined)";

					this.Menus.Add(-1, menu);
				}
			}

			return menu;
		}
	}
}
