﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Model.Transactions
{
    public class Order
    {
	    public Order() => Orderitems = new Dictionary<int, Orderitem>();

	    public int OrderId { get; set; }

        public OrderType Type { get; set; }

        public string NumberOfGuests { get; set; }

        public TimeSpan? BeingHandledIn { get; set; }

        public string DeliverypointName { get; set; }

        public DateTime? CreatedUtc { get; set; }

        public Deliverypointgroup Deliverypointgroup { get; set; }

        public Dictionary<int, Orderitem> Orderitems { get; set; }



        public int DeliverypointgroupId { get; set; }

        

        public bool MobileOrder { get; set; }

        

        public decimal Revenue => this.Orderitems.Values.Sum(x => x.Revenue);
    }
}
