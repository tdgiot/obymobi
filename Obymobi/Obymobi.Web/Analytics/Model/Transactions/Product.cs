﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Enums;

namespace Obymobi.Web.Analytics.Model.Transactions
{
    public class Product
    {
	    public Product()
        {
            Orderitems = new Dictionary<int, Orderitem>();
            Categories = new Dictionary<string, Category>();
        }

	    public string Key { get; set; }

        public int? ProductId { get; set; }

        public string Name { get; set; }

        public OrderType Type { get; set; }

        public Dictionary<int, Orderitem> Orderitems { get; set; }

        public Dictionary<string, Category> Categories { get; set; }  

        public decimal Revenue => Orderitems.Values.Sum(orderitem => orderitem.Revenue);

        public int Quantity => Orderitems.Values.Sum(orderitem => orderitem.Quantity);

        public override string ToString() => $"{Name} ({Revenue})";
    }
}
