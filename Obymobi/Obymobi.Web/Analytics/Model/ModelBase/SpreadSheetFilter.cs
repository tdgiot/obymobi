﻿using System.Collections.Generic;
using Obymobi.Enums;

namespace Obymobi.Web.Analytics.Model.ModelBase
{
    public abstract class SpreadSheetFilter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public int SortOrder { get; set; }
        public int? OutletId { get; set; }
        public IReadOnlyList<int> ServiceMethods { get; set; } = new List<int>();
        public IReadOnlyList<int> CheckoutMethods { get; set; } = new List<int>();
        public IReadOnlyList<int> IncludedTagIds { get; set; } = new List<int>();
        public bool ShowAlterations { get; set; }
        public IncludeExcludeFilter CategoryFilterType { get; set; } = IncludeExcludeFilter.None;
        public IReadOnlyList<int> CategoryFilterIds { get; set; } = new List<int>();
        public bool ShowSystemProducts { get; set; }
    }
}