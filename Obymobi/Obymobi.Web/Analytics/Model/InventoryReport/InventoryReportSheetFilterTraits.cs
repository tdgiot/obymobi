﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Extensions;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.CriteriaPattern;
using Obymobi.Web.Analytics.Model.TransactionsAppLess.OrderCriteria;

namespace Obymobi.Web.Analytics.Model.InventoryReport
{
	public static class InventoryReportSheetFilterTraits
	{
		public static ICriteria<OrderEntity> CreateOrderCriteria(this InventoryReportSheetFilter sheetFilter)
		{
			ICriteria<OrderEntity> orderCriteria = new EmptyOrderCriteria();

			if (sheetFilter.OutletId.HasValue)
			{
				ICriteria<OrderEntity> outletCriteria = new OrderOutletIdCriteria(sheetFilter.OutletId.Value);
				orderCriteria = AndOrderCriteria.Create(orderCriteria, outletCriteria);
			}

			if (sheetFilter.ServiceMethods.Any())
			{
				ICriteria<OrderEntity> serviceMethodCriteria = new OrderServiceMethodCriteria(sheetFilter.ServiceMethods);
				orderCriteria = AndOrderCriteria.Create(orderCriteria, serviceMethodCriteria);
			}

			if (sheetFilter.CheckoutMethods.Any())
			{
				ICriteria<OrderEntity> checkoutMethodCriteria = new OrderCheckoutMethodCriteria(sheetFilter.CheckoutMethods);
				orderCriteria = AndOrderCriteria.Create(orderCriteria, checkoutMethodCriteria);
			}

			return orderCriteria;
		}

		public static ICriteria<OrderitemEntity> CreateOrderItemCriteria(this InventoryReportSheetFilter sheetFilter)
		{
			ICriteria<OrderitemEntity> orderItemCriteria = AndOrderItemCriteria.Create(
				new OrderItemProductCriteria(),
				OrOrderItemCriteria.Create(
					new OrderItemWithAlterationLinkedToProductCriteria(),
					new OrderItemWithPriceCriteria()));

			if (sheetFilter.CategoryFilterType != IncludeExcludeFilter.None)
			{
				ICriteria<OrderitemEntity> orderItemCategoryCriteria = OrderItemCategoryCriteria.Create(sheetFilter.CategoryFilterIds, sheetFilter.CategoryFilterType);

				orderItemCriteria = AndOrderItemCriteria.Create(orderItemCriteria, orderItemCategoryCriteria);
			}

			if (sheetFilter.IncludedTagIds.Any())
			{
				ICriteria<OrderitemEntity> orderItemTagCriteria = new OrderItemTagCriteria(sheetFilter.IncludedTagIds);

				orderItemCriteria = AndOrderItemCriteria.Create(orderItemCriteria, orderItemTagCriteria);
			}

			return orderItemCriteria;
		}

		public static ICriteria<OrderitemAlterationitemEntity> CreateOrderItemAlterationsCriteria(this InventoryReportSheetFilter sheetFilter)
		{
			ICriteria<OrderitemAlterationitemEntity> alterationItemCriteria = new OrderItemAlterationItemWithProductLinkCriteria();

			if (sheetFilter.IncludedTagIds.Any())
			{
				alterationItemCriteria = AndOrderItemAlterationItemCriteria.Create(
					alterationItemCriteria,
					new OrderItemAlterationItemTagCriteria(sheetFilter.IncludedTagIds));
			}

			return alterationItemCriteria;
		}

		public static IEnumerable<InventoryItem> MapOrderEntitiesToModels(this InventoryReportSheetFilter sheetFilter,
			OrderCollection orders,
			ICriteria<OrderEntity> orderCriteria,
			ICriteria<OrderitemEntity> orderItemCriteria,
			ICriteria<OrderitemAlterationitemEntity> alterationItemCriteria)
		{
			OrderEntity[] filteredOrders = orderCriteria.MeetCriteria(orders).ToArray();

			List<InventoryItem> alterationItems = alterationItemCriteria.MeetCriteria(filteredOrders
					.SelectMany(order => order.OrderitemCollection)
					.SelectMany(item => item.OrderitemAlterationitemCollection))
				.Select(alterationItem => alterationItem.ToInventoryItem())
                .ToList();

			OrderitemEntity[] items = filteredOrders
				.SelectMany(orderEntity => orderItemCriteria.MeetCriteria(orderEntity.OrderitemCollection))
				.ToArray();

			OrderItemWithPriceCriteria orderItemWithPriceCriteria = new OrderItemWithPriceCriteria();
			List<InventoryItem> products = orderItemWithPriceCriteria.MeetCriteria(items)
				.Select(item => item.ToInventoryItem())
                .ToList();

            IEnumerable<InventoryItem> inventoryItems = products.Concat(alterationItems);

            return sheetFilter.ShowExternalIdentifier
                ? GroupByNameAndExternalIdentifier(inventoryItems)
                : GroupByName(inventoryItems);
        }

        private static IEnumerable<InventoryItem> GroupByName(IEnumerable<InventoryItem> inventoryItems) => 
            inventoryItems
                .GroupBy(i => new 
                {
                    i.Name
                })
                .Select(x => new InventoryItem(x.Key.Name, string.Empty, x.Sum(k => k.Quantity)))
                .OrderBy(i => i.Name)
                .ToArray();

        private static IEnumerable<InventoryItem> GroupByNameAndExternalIdentifier(IEnumerable<InventoryItem> inventoryItems) =>
			inventoryItems
                .GroupBy(i => new
                {
                    i.Name,
					i.ExternalIdentifier
                })
                .Select(x => new InventoryItem(x.Key.Name, x.Key.ExternalIdentifier, x.Sum(k => k.Quantity)))
                .OrderBy(i => i.Name)
                .ThenBy(i => i.ExternalIdentifier)
                .ToArray();
	}
}
