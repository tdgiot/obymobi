﻿namespace Obymobi.Web.Analytics.Model.InventoryReport
{
    public class InventoryItem
    {
        public InventoryItem(string name, string externalIdentifier, int quantity)
        {
            Name = name;
            ExternalIdentifier = externalIdentifier;
            Quantity = quantity;
        }

        public string Name { get; }
        public string ExternalIdentifier { get; }
        public int Quantity { get; }
    }
}
