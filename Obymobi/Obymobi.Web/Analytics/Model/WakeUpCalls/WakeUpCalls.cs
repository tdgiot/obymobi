﻿using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;
using System;
using Obymobi.Logic.Analytics;
using Dionysos;

namespace Obymobi.Web.Analytics.Model.WakeUpCalls
{
    public class WakeUpCalls
    {
        #region Fields

        private readonly Filter request;

        private OrderCollection orderCollection;

        #endregion

        #region Constructors

        public WakeUpCalls(Filter request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            this.request = request;
        }

        #endregion

        #region Properties

        public List<WakeUpCall> WakeUpCallCollection { get; set; }

        #endregion

        #region Methods

        public void FetchAndProcessData()
        {
            this.FetchWakeUpCalls();
            this.ProcessData();
        }

        private void FetchWakeUpCalls()
        {
            // Create the filter and the relations
            PredicateExpression filter = new PredicateExpression();
            RelationCollection relations = new RelationCollection();

            // Add filter on CompanyId and date range
            filter.Add(OrderFields.CompanyId == this.request.CompanyId);
            filter.Add(OrderFields.CreatedUTC >= this.request.FromUtc);
            filter.Add(OrderFields.CreatedUTC <= this.request.TillUtc);
            filter.Add(OrderFields.Type == OrderType.RequestForWakeUp);

            // Add filter on DeliverypointIds
            if (this.request.DeliverypointIds != null && this.request.DeliverypointIds.Count > 0)
            {
                PredicateExpression subfilter = new PredicateExpression();

                if (this.request.DeliverypointsExclude)
                {
                    subfilter.Add(OrderFields.DeliverypointId != this.request.DeliverypointIds);

                    if (this.request.DeliverypointgroupIds != null && this.request.DeliverypointgroupIds.Count > 0)
                    {
                        subfilter.Add(DeliverypointFields.DeliverypointgroupId == this.request.DeliverypointgroupIds);
                        relations.Add(OrderEntityBase.Relations.DeliverypointEntityUsingDeliverypointId); // Left join, so orders without deliverypoint are also included
                    }
                }
                else
                {
                    subfilter.Add(OrderFields.DeliverypointId == this.request.DeliverypointIds);
                }
                subfilter.AddWithOr(OrderFields.DeliverypointId == DBNull.Value);
                filter.Add(subfilter);
            }
            else if (this.request.DeliverypointgroupIds != null && this.request.DeliverypointgroupIds.Count > 0)
            {
                PredicateExpression subfilter = new PredicateExpression();
                subfilter.Add(DeliverypointFields.DeliverypointgroupId == this.request.DeliverypointgroupIds);
                subfilter.AddWithOr(OrderFields.DeliverypointId == DBNull.Value);
                filter.Add(subfilter);

                relations.Add(OrderEntityBase.Relations.DeliverypointEntityUsingDeliverypointId, JoinHint.Left); // Left join, so orders without deliverypoint are also included
            }

            // Create the prefetch
            PrefetchPath prefetch = new PrefetchPath(EntityType.OrderEntity);

            // Create the included fields list for the orderitems
            IncludeFieldsList orderIncludes = new IncludeFieldsList();
            orderIncludes.Add(OrderFields.CreatedUTC);
            orderIncludes.Add(OrderFields.OrderId);
            orderIncludes.Add(OrderFields.ClientId);
            orderIncludes.Add(OrderFields.DeliverypointId);
            orderIncludes.Add(OrderFields.DeliverypointName);
            orderIncludes.Add(OrderFields.DeliverypointNumber);
            orderIncludes.Add(OrderFields.DeliverypointgroupName);
            orderIncludes.Add(OrderFields.Notes);
            orderIncludes.Add(OrderFields.StatusText);
            orderIncludes.Add(OrderFields.ProcessedUTC);

            // Create the sort
            SortExpression sort = new SortExpression(new SortClause(OrderFields.CreatedUTC, SortOperator.Descending));
            
            this.orderCollection = new OrderCollection();
            this.orderCollection.GetMulti(filter, 0, sort, relations, prefetch, orderIncludes, 0, 0);
        }

        #region Processing methods

        private void ProcessData()
        {
            // Reset the dictionaries
            this.WakeUpCallCollection = new List<WakeUpCall>();

            // Populate the wakeupcall models i.e. gather all info
            this.PopulateWakeUpCallModels();
        }

        private void PopulateWakeUpCallModels()
        {
            ReportSettings settings = new ReportSettings(this.request.CompanyId);

            foreach (OrderEntity orderEntity in this.orderCollection)
            {
                // Create the wake up call model
                WakeUpCall wakeUpCall = new WakeUpCall(settings);
                wakeUpCall.Created = orderEntity.CreatedUTC.HasValue ? orderEntity.CreatedUTC.Value.UtcToLocalTime(settings.CompanyTimeZoneInfo) : new DateTime();
                wakeUpCall.OrderId = orderEntity.OrderId;
                wakeUpCall.ClientId = orderEntity.ClientId ?? -1;
                wakeUpCall.DeliverypointId = orderEntity.DeliverypointId ?? -1;
                wakeUpCall.DeliverypointName = orderEntity.DeliverypointName;
                wakeUpCall.DeliverypointNumber = orderEntity.DeliverypointNumber;
                wakeUpCall.DeliverypointgroupName = orderEntity.DeliverypointgroupName;
                wakeUpCall.WakeUpCallText = orderEntity.Notes;
                wakeUpCall.Status = orderEntity.StatusText;
                wakeUpCall.Processed = orderEntity.ProcessedUTC.HasValue ? orderEntity.ProcessedUTC.Value.UtcToLocalTime(settings.CompanyTimeZoneInfo) : new DateTime();

                this.WakeUpCallCollection.Add(wakeUpCall);
            }
        }

        #endregion

        #endregion
    }
}
