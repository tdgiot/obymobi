﻿using System;

namespace Obymobi.Web.Analytics.Model.WakeUpCalls
{
    public class WakeUpCall
    {
        #region Fields

        private readonly ReportSettings settings;

        #endregion

        #region Constructors

        public WakeUpCall(ReportSettings settings)
        {
            this.settings = settings;
        }

        #endregion

        #region Properties

        public DateTime? Created { get; set; }

        public int OrderId { get; set; }

        public int ClientId { get; set; }

        public int DeliverypointId { get; set; }

        public string DeliverypointName { get; set; }

        public string DeliverypointNumber { get; set; }

        public string DeliverypointgroupName { get; set; }

        public string WakeUpCallText { get; set; }

        public string Status { get; set; }

        public DateTime? Processed { get; set; }

        public string CreatedAsString
        {
            get
            {
                if (this.Created.HasValue)
                {
                    return this.settings.ToClockModeString(this.Created.Value);
                }
                return string.Empty;
            }
        }

        public string ProcessedAsString
        {
            get
            {
                if (this.Processed.HasValue)
                {
                    return this.settings.ToClockModeString(this.Processed.Value);
                }
                return string.Empty;
            }
        }

        #endregion
    }
}
