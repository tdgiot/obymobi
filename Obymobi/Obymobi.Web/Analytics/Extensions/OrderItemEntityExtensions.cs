﻿using Obymobi.Data.EntityClasses;
using Obymobi.Web.Analytics.Model.InventoryReport;
using Obymobi.Web.Analytics.Model.Orders;

namespace Obymobi.Web.Analytics.Extensions
{
    public static class OrderItemEntityExtensions
    {
        public static ProductSale ToProduct(this OrderitemEntity self) 
            => new ProductSale(self.ProductName, self.ExternalIdentifier, self.Quantity, self.PriceInTax);

        public static InventoryItem ToInventoryItem(this OrderitemEntity self)
            => new InventoryItem(self.ProductName, self.ExternalIdentifier, self.Quantity);

        public static InventoryItem ToInventoryItem(this OrderitemAlterationitemEntity self)
            => new InventoryItem(self.GetFriendlyNameOrName(), self.ExternalIdentifier, self.OrderitemEntity.Quantity);
    }
}
