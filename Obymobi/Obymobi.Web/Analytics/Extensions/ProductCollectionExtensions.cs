﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Web.Analytics.Model.Orders;

namespace Obymobi.Web.Analytics.Extensions
{
    public static class ProductCollectionExtensions
    {
        public static decimal SumIndividualPrices(this IEnumerable<ProductSale> self) => self.Sum(p => p.Price);

        public static decimal SumTotalPrices(this IEnumerable<ProductSale> self) => self.Sum(p => p.TotalPrice);
    }
}
