﻿using System;
using System.IO;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Extensions.SpreadsheetLight
{
    public static class SLDocumentExtensions
    {
        public static byte[] ToFile(this SLDocument spreadsheet)
        {
            byte[] file;

            using (MemoryStream stream = new MemoryStream())
            {
                spreadsheet.SaveAs(stream);
                stream.Position = 0;
                file = stream.ToArray();
            }

            return file;
        }
    }
}
