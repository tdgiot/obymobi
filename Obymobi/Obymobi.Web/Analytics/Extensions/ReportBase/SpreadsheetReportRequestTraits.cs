﻿using System;
using Dionysos;
using Obymobi.Web.Analytics.Requests.RequestBase;

namespace Obymobi.Web.Analytics.Extensions.ReportBase
{
    public static class SpreadsheetReportRequestTraits
	{
		public static DateTime GetFromDateLocal(this ReportRequest self, TimeZoneInfo timeZoneInfo) 
            => ToLocalTime(self.FromDateTimeUtc, timeZoneInfo);

		public static DateTime GetUntilDateLocal(this ReportRequest self, TimeZoneInfo timeZoneInfo)
            => ToLocalTime(self.UntilDateTimeUtc, timeZoneInfo);

		private static DateTime ToLocalTime(DateTime dateTime, TimeZoneInfo timeZoneInfo) => dateTime.UtcToLocalTime(timeZoneInfo);
	}
}
