﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Web.Analytics.Extensions
{
    public static class OrderItemAlterationItemEntityExtensions
    {
        public static string GetFriendlyNameOrName(this OrderitemAlterationitemEntity self)
            => string.IsNullOrWhiteSpace(self.AlterationitemEntity.AlterationoptionEntity.FriendlyName)
                ? self.AlterationoptionName 
                : self.AlterationitemEntity.AlterationoptionEntity.FriendlyName;
    }
}
