﻿using System.Linq;
using Obymobi.Web.Analytics.Model.InventoryReport;

namespace Obymobi.Web.Analytics.Extensions
{
    public static class InventoryItemExtensions
    {
        public static int SumQuantity(this InventoryItem[] self) => self.Sum(item => item.Quantity);
    }
}
