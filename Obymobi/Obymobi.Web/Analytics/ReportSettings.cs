﻿using System;
using System.Globalization;
using Obymobi.Constants;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Web.Analytics
{
    public class ReportSettings
    {
        #region Constructors

        public ReportSettings(int companyId)
        {
            this.CompanyId = companyId;

            this.Init();            
        }

        #endregion

        #region Methods

        private void Init()
        {
            CompanyEntity company = new CompanyEntity(this.CompanyId);

            this.CompanyTimeZoneInfo = company.TimeZoneInfo;
            this.CompanyClockMode = company.ClockMode;
            this.DatabaseTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(ObymobiConstants.DatabaseTimeZone);
        }

        public string ToClockModeString(DateTime dateTime)
        {
            if (this.CompanyClockMode == ClockMode.Hour24)
            {
                return string.Format("{0:yyyy-MM-dd HH:mm:ss}", dateTime);
            }
            else if (this.CompanyClockMode == ClockMode.AmPm)
            {
                return dateTime.ToString("yyyy-MM-dd hh:mm:ss tt", CultureInfo.InvariantCulture);
            }
            return string.Empty;
        }        

        #endregion

        #region Properties

        private int CompanyId { get; set; }

        public TimeZoneInfo DatabaseTimeZoneInfo { get; set; }
        public TimeZoneInfo CompanyTimeZoneInfo { get; set; }
        public ClockMode CompanyClockMode { get; set; }

        #endregion
    }
}
