﻿using System;
using Newtonsoft.Json;
using Obymobi.Web.Analytics.Model.ModelBase;

namespace Obymobi.Web.Analytics.Converters
{
	public abstract class SpreadSheetFilterConverter<T> : JsonConverter<SpreadSheetFilter> where T : SpreadSheetFilter
	{
		public override void WriteJson(JsonWriter writer, SpreadSheetFilter value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override SpreadSheetFilter ReadJson(JsonReader reader, Type objectType, SpreadSheetFilter existingValue, bool hasExistingValue, JsonSerializer serializer) 
			=> new JsonSerializer().Deserialize<T>(reader);
	}
}