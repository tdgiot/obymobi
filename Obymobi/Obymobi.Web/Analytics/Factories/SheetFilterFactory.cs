﻿using Obymobi.Web.Analytics.Model.ModelBase;

namespace Obymobi.Web.Analytics.Factories
{
	public abstract class SheetFilterFactory<T> : ISheetFilterFactory
		where T :  SpreadSheetFilter, new()
	{
		public SpreadSheetFilter CreateSpreadSheetFilter(int index, string name, int sortOrder) =>
			new T
			{
				Id = index,
				Name = name,
				SortOrder = sortOrder
			};

		public SpreadSheetFilter CreateSpreadSheetFilter() => new T();
	}
}