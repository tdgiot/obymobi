﻿using System;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Bridges.Implementers;
using Obymobi.Web.Analytics.Bridges.RefinedAbstractions;

namespace Obymobi.Web.Analytics.Factories
{
	public class ReportRequestBridgeFactory
	{
		public ReportRefinedAbstraction GetReportRequestRefinedAbstraction(AnalyticsReportType reportType)
			=> new ReportRefinedAbstraction(CreateReportRequestImplementor(reportType));

		public SheetFilterRefinedAbstraction GetSheetFilterRefinedAbstraction(AnalyticsReportType reportType)
			=> new SheetFilterRefinedAbstraction(CreateReportRequestImplementor(reportType));

		public SheetFilterControlRefinedAbstraction GetSheetFilterControlRefinedAbstraction(AnalyticsReportType reportType)
			=> new SheetFilterControlRefinedAbstraction(CreateReportRequestImplementor(reportType));

		private static IReportRequestImplementer CreateReportRequestImplementor(AnalyticsReportType reportType)
		{
			switch (reportType)
			{
				case AnalyticsReportType.TransactionsAppLess:
					return new TransactionAppLessReportRequestImplementer();
				case AnalyticsReportType.ProductSales:
					return new ProductSalesReportRequestImplementer();
				case AnalyticsReportType.Inventory:
					return new InventoryReportRequestImplementer();
				default:
					throw new ArgumentOutOfRangeException(nameof(reportType));
			}
		}
	}
}
