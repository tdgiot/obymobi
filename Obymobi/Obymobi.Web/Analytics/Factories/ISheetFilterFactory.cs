﻿using Obymobi.Web.Analytics.Model.ModelBase;

namespace Obymobi.Web.Analytics.Factories
{
	public interface ISheetFilterFactory
	{
		SpreadSheetFilter CreateSpreadSheetFilter(int index, string name, int sortOrder);
		SpreadSheetFilter CreateSpreadSheetFilter();
	}
}
