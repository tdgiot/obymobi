using Obymobi.Data.DaoClasses;
using Obymobi.Enums;
using System;
using System.Text;
using System.Web;

namespace Obymobi.Web
{
    public class MessageFromExceptionFactory
    {
        public string Create(HttpContext httpContext)
        {
            Exception lastError = httpContext.Server.GetLastError();
            if (lastError != null && lastError.GetBaseException() != null)
            {
                lastError = lastError.GetBaseException();
            }

            try
            {
                StringBuilder messageBuilder = new StringBuilder();
                messageBuilder.AppendLine($"Current company: {CmsSessionHelper.CurrentCompanyId}");
                messageBuilder.AppendLine($"User role: {CmsSessionHelper.CurrentRole}");
                messageBuilder.AppendLine($"Command timeout: {CommonDaoBase.CommandTimeOut}");
                if (CmsSessionHelper.CurrentRole < Role.Crave)
                {
                    messageBuilder.AppendLine($"Companies for user: {string.Join(", ", CmsSessionHelper.CompanyIdsForUser)}");
                }
                messageBuilder.AppendLine($"Page url: {httpContext.Request.Url.AbsoluteUri}");

                if (lastError != null)
                {
                    messageBuilder
                        .AppendLine()
                        .AppendLine(lastError.ToString());
                }

                return messageBuilder.ToString();
            }
            catch (Exception internalException)
            {
                if (lastError != null)
                {
                    return lastError.ToString();
                }

                return $"Failed to convert the exception to a Slack message. Reason: {Environment.NewLine}{internalException}";
            }
        }
    }
}