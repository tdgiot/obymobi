﻿using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml.Spreadsheet;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Extensions;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.ProductReport
{
	public class ProductSubView : HorizontalReportSubView
	{
		public ProductSubView(IEnumerable<ProductSale> products, Currency currency, ShowExternalIdentifier showExternalIdentifier)
			: base(
				new ProductSubViewHeader(showExternalIdentifier),
				CreateProducts(products, currency, showExternalIdentifier),
				new ProductSubViewTotals(products.SumIndividualPrices(), products.SumTotalPrices(), currency))
		{
		}

		private static ViewItem[] CreateProducts(IEnumerable<ProductSale> products, Currency currency, ShowExternalIdentifier showExternalIdentifier)
		{
			var viewItems = new List<ViewItem>();
            var values = new List<ViewValues>();
            foreach (var product in products)
            {
                var viewCells = new List<IViewCell>();

                viewCells.Add(new StringViewCell(product.Name));

                if (showExternalIdentifier)
                {
                    viewCells.Add(new StringViewCell(product.ExternalIdentifier));
                }

                viewCells.Add(new IntViewCell(product.Quantity, HorizontalAlignmentValues.Left));
                viewCells.Add(new CurrencyViewCell(product.Price, currency));
                viewCells.Add(new CurrencyViewCell(product.TotalPrice, currency));

                values.Add(new ViewValues(viewCells.ToArray()));
            }

            if (!values.Any())
            {
                // Add empty cells to align the cell correctly.
                int columnCount = showExternalIdentifier ? 5 : 4;
                IEnumerable<IViewCell> viewCells = Enumerable.Repeat(new StringViewCell(string.Empty), columnCount);
                values.Add(new ViewValues(viewCells.ToArray()));
            }

            viewItems.Add(new ViewItem(values.ToArray()));

			return viewItems.ToArray();
		}
	}
}
