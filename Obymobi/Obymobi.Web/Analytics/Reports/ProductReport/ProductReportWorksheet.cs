﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Web.Analytics.Model.ProductReport;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.ProductReport
{
	public class ProductReportWorksheet : Worksheet
	{
		public ProductReportWorksheet(ProductReportSheetFilter sheetFilter, string name) : base(name, CreateColumns(sheetFilter))
		{
		}

		private static Column[] CreateColumns(ProductReportSheetFilter sheetFilter)
        {
            IList<Column> columns = new List<Column>();
            columns.Add(new Column(Properties.Resources.ProductNameColumnWidth));

            if (sheetFilter.ShowExternalIdentifier)
            {
                columns.Add(new Column(Properties.Resources.ExternalIdentifierColumnWidth));
            }

			columns.Add(new Column(Properties.Resources.ProductReportQuantityColumnWidth));
			columns.Add(new Column(Properties.Resources.PriceColumnWidth));
			columns.Add(new Column(Properties.Resources.OrderTotalColumnWidth));

			return columns.ToArray();
		}
	}
}
