﻿using System.Collections.Generic;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.ProductReport
{
	public class ProductSubViewHeader : ViewHeader
	{
		public ProductSubViewHeader(ShowExternalIdentifier showExternalIdentifier) : base(CreateHeaderCells(showExternalIdentifier))
		{
		}

		private static HeaderCell[] CreateHeaderCells(ShowExternalIdentifier showExternalIdentifier)
		{
			var viewCellBuilder = new HeaderCellBuilder();

            var headerCells = new List<HeaderCell>();

            headerCells.Add(viewCellBuilder.With(Properties.Resources.Item).Build());

            if (showExternalIdentifier)
            {
                headerCells.Add(viewCellBuilder.With(Properties.Resources.ExternalIdentifier).Build());
			}

            headerCells.Add(viewCellBuilder.With(Properties.Resources.Quantity).Build());
			headerCells.Add(viewCellBuilder.With(Properties.Resources.IndividualPrice).Build());
			headerCells.Add(viewCellBuilder.With(Properties.Resources.TotalPrice).Build());

			return headerCells.ToArray();
		}
	}

	public class ProductSubViewTotals : ViewTotals
	{
		public ProductSubViewTotals(decimal individualPrice, decimal totalPrice, Currency currency) : base(CreateTotalCells(individualPrice, totalPrice, currency))
		{
		}

		private static IViewCell[] CreateTotalCells(decimal individualPrice, decimal totalPrice, Currency currency) =>
			new IViewCell[]
			{
				new CurrencyViewCell(individualPrice, currency),
				new CurrencyViewCell(totalPrice, currency)
			};
	}

	public class EmptyViewTotals : ViewTotals
	{
		public EmptyViewTotals() : base(new IViewCell[0])
		{
		}
	}
}
