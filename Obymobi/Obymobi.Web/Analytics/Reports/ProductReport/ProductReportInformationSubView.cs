﻿using System;
using DocumentFormat.OpenXml.Spreadsheet;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.ProductReport
{
	public class ProductReportInformationSubView : VerticalReportSubView
	{
		public ProductReportInformationSubView(string companyName, Currency currency, DateTime fromLocalDateTime, DateTime untilLocalUntilDateTime,  decimal totalProductRevenue)
			: base(new ProductReportInformationSubViewHeader(), CreateItems(companyName, currency, fromLocalDateTime, untilLocalUntilDateTime, totalProductRevenue))
		{
		}

		private static ViewItem[] CreateItems(string companyName, Currency currency, DateTime fromLocalDateTime, DateTime untilLocalUntilDateTime, decimal totalProductRevenue)
		{
			const string formatCode = "yyyy-mm-dd";
			const HorizontalAlignmentValues horizontalAlignmentValues = HorizontalAlignmentValues.Left;

			return new[]
			{
				new ViewItem(new[]
				{
					new ViewValues(new IViewCell[]
					{
						new StringViewCell(companyName),
						new DateTimeViewCell(fromLocalDateTime, formatCode, horizontalAlignmentValues),
						new DateTimeViewCell(untilLocalUntilDateTime, formatCode, horizontalAlignmentValues),
						new StringViewCell(string.Empty),
						new CurrencyViewCell(totalProductRevenue, currency, HorizontalAlignmentValues.Left)
					})
				})
			};
		}
	}
}
