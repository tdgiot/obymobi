﻿using System.Drawing;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.ProductReport
{
	public class ProductReportInformationSubViewHeader : ViewHeader
	{
		public ProductReportInformationSubViewHeader() : base(CreateHeaderCells())
		{
		}

		private static HeaderCell[] CreateHeaderCells()
		{
			var viewCellBuilder = new HeaderCellBuilder().With(FontStyle.Bold);
			return new[]
			{
				viewCellBuilder.With(Properties.Resources.VendorName).Build(),
				viewCellBuilder.With(Properties.Resources.ReportPeriodFrom).Build(),
				viewCellBuilder.With(Properties.Resources.ReportPeriodTo).Build(),
				viewCellBuilder.With(string.Empty).Build(),
				viewCellBuilder.With(Properties.Resources.SessionSalesProductRevenue).Build()
			};
		}
	}
}

