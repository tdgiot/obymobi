﻿using System;
using System.Collections.Generic;
using Obymobi.Interfaces;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Model.ProductReport;
using Obymobi.Web.Analytics.Reports.ProductReport;
using Obymobi.Web.Analytics.Reports.ReportBase;
using Obymobi.Web.Analytics.Reports.TransactionsAppLess;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports.ProductSummaryReport
{
	public class ProductReportBuilder : ReportBuilder
	{
		private readonly Company company;
		private readonly DateTime fromLocalDateTime;
		private readonly IEnumerable<ProductSale> products;
		private readonly ProductReportSheetFilter sheetFilter;
		private readonly SLDocument spreadsheet;
		private readonly IEnumerable<ITag> tags;
		private readonly decimal totalProductRevenue;
		private readonly DateTime untilLocalUntilDateTime;
		private CellLocation cellLocation;

		public ProductReportBuilder(
			SLDocument spreadsheet,
			ProductReportSheetFilter sheetFilter,
			Company company,
			IEnumerable<ProductSale> products,decimal totalProductRevenue,
			DateTime fromLocalDateTime,
			DateTime untilLocalUntilDateTime,
			IEnumerable<ITag> tags)
		{
			this.spreadsheet = spreadsheet;
			this.totalProductRevenue = totalProductRevenue;
			this.fromLocalDateTime = fromLocalDateTime;
			this.untilLocalUntilDateTime = untilLocalUntilDateTime;
			this.tags = tags;
			this.sheetFilter = sheetFilter;
			this.company = company;
			this.products = products;
			cellLocation = new CellLocation((SpreadsheetColumn) 1, (SpreadsheetRow) 1);
		}

		public void BuildWorksheet()
		{
			ProductReportWorksheet productReportWorksheet = new ProductReportWorksheet(sheetFilter, sheetFilter.Name);
			productReportWorksheet.Render(spreadsheet);
		}

		public void BuildProducts()
		{
			Currency currency = company.Currency;
			ProductSubView productSubView = new ProductSubView(products, currency, sheetFilter.ShowExternalIdentifier);
			CellLocation productSubViewLocation = cellLocation;

			RenderHeaderForHorizontalSubView(spreadsheet, productSubView, productSubViewLocation);

			ProductViewLineRenderingDirector productViewLineRenderingDirector = new ProductViewLineRenderingDirector();
			ProductViewLineBuilder productViewLineBuilder = new ProductViewLineBuilder(productSubView);

			CellLocation initialLocation = cellLocation.ChangeRow(GetNextRow(cellLocation.Row));
			SpreadsheetRow maximumRow = productViewLineRenderingDirector.Render(sheetFilter, spreadsheet, productViewLineBuilder, 0, initialLocation);
			initialLocation = new CellLocation(initialLocation.Column, GetNextRow(maximumRow));

            CellLocation totalCellLocation = !sheetFilter.ShowExternalIdentifier
                ? initialLocation.MoveColumn(2)
                : initialLocation.MoveColumn(3);

			RenderTotalsForHorizontalSubView(spreadsheet, productSubView, totalCellLocation);
		}

		public void BuildReportInformation()
		{
			CellLocation baseLocation = cellLocation;
			const int oneEmptyRow = 1;

			ProductReportInformationSubView reportInformationSubView = new ProductReportInformationSubView(
				company.Name,
				company.Currency,
				fromLocalDateTime,
				untilLocalUntilDateTime,
				totalProductRevenue);

			cellLocation = RestoreRow(SetLocationToNextColumn(reportInformationSubView.RenderHeader(spreadsheet, cellLocation)), baseLocation);
			cellLocation = RestoreColumn(SetLocationToNextRow(reportInformationSubView.RenderItem(spreadsheet, 0, cellLocation), oneEmptyRow), baseLocation);
		}

		public void BuildIncludeTagsSubView()
		{
			CellLocation baseLocation = cellLocation;
			const int oneEmptyRow = 1;

			IncludedTagsSubView includedTagsSubView = new IncludedTagsSubView(tags);

			cellLocation = RestoreRow(SetLocationToNextColumn(includedTagsSubView.RenderHeader(spreadsheet, cellLocation)), baseLocation);
			cellLocation = RestoreColumn(SetLocationToNextRow(includedTagsSubView.RenderItem(spreadsheet, 0, cellLocation), oneEmptyRow), baseLocation);
		}
	}
}
