﻿namespace Obymobi.Web.Analytics.Reports.ProductSummaryReport
{
	public class ProductReportBuilderDirector
	{
		public void Construct(ProductReportBuilder productReportBuilder)
		{
			productReportBuilder.BuildWorksheet();
			productReportBuilder.BuildReportInformation();
			productReportBuilder.BuildIncludeTagsSubView();
			productReportBuilder.BuildProducts();
		}
	}
}
