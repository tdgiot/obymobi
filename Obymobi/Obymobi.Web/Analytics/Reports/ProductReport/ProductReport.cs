using Obymobi.Data.CollectionClasses;
using Obymobi.Web.Analytics.Extensions;
using Obymobi.Web.Analytics.Extensions.ReportBase;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Model.ProductReport;
using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using Obymobi.Web.Analytics.Reports.ReportBase;
using Obymobi.Web.Analytics.Repositories;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using Obymobi.Web.Analytics.Requests;
using SpreadsheetLight;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Web.Analytics.Reports.ProductSummaryReport
{
    public sealed class ProductReport : SpreadsheetReport
    {
        private readonly IOrderRepository orderRepository;
        private readonly ProductReportRequest productReportRequest;
        private readonly ITagRepository tagRepository;

        public ProductReport(ProductReportRequest productReportRequest, IOrderRepository orderRepository, ICompanyRepository companyRepository, ITagRepository tagRepository)
            : base(companyRepository, productReportRequest.CompanyId)
        {
            this.productReportRequest = productReportRequest;
            this.orderRepository = orderRepository;
            this.tagRepository = tagRepository;
        }

        public override bool CreateReport(out SLDocument spreadsheet)
        {
            base.CreateReport(out spreadsheet);

            // Fetch order collection for request
            OrderCollection orderCollection = orderRepository.Get(productReportRequest.ToOrdersRequest());

            string firstSheetName = string.Empty;
            foreach (ProductReportSheetFilter sheetFilter in productReportRequest.GetActiveSheets())
            {
                if (string.IsNullOrEmpty(firstSheetName))
                {
                    firstSheetName = sheetFilter.Name;
                }

                CreateReportSheet(spreadsheet, orderCollection, sheetFilter);
            }

            // Make sure the first worksheet is selected when opening the document for the first time
            spreadsheet.SelectWorksheet(firstSheetName);
            return true;
        }

        private void CreateReportSheet(SLDocument spreadsheet, OrderCollection orderCollection, ProductReportSheetFilter sheetFilter)
        {
            IEnumerable<ProductSale> orderModels = orderCollection.ToOrderModels(sheetFilter);
            ProductSale[] filteredProducts = orderModels as ProductSale[] ?? orderModels.ToArray();

            decimal totalProductRevenue = filteredProducts.SumTotalPrices();

            TagCollection tagCollection = tagRepository.GetTags(sheetFilter.IncludedTagIds);

            ProductReportBuilderDirector reportBuilderDirector = new ProductReportBuilderDirector();
            reportBuilderDirector.Construct(
                new ProductReportBuilder(
                    spreadsheet,
                    sheetFilter,
                    Company,
                    filteredProducts,
                    totalProductRevenue,
                    productReportRequest.GetFromDateLocal(Company.TimeZoneInfo), productReportRequest.GetUntilDateLocal(Company.TimeZoneInfo),
                    tagCollection));
        }
    }
}
