﻿using System.Drawing;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.ProductReport
{
	public class TagsSubViewHeader : ViewHeader
	{
		public TagsSubViewHeader() : base(CreateHeaderCells())
		{
		}

		private static HeaderCell[] CreateHeaderCells()
		{
			var viewCellBuilder = new HeaderCellBuilder().With(FontStyle.Bold);
			return new[]
			{
				viewCellBuilder.With(Properties.Resources.TagsIncludedInReport).Build(),
			};
		}
	}
}
