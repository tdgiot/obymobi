﻿using System.Drawing;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.ProductReport
{
	public class IncludedTagsSubViewHeader : ViewHeader
	{
		public IncludedTagsSubViewHeader() : base(CreateHeaderCells())
		{
		}

		private static HeaderCell[] CreateHeaderCells()
		{
			HeaderCellBuilder viewCellBuilder = new HeaderCellBuilder().With(FontStyle.Bold);
			return new[]
			{
				viewCellBuilder.With(Properties.Resources.IncludedTags).Build()
			};

		}
	}
}
