﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Interfaces;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.ProductReport
{
	public class IncludedTagsSubView : VerticalReportSubView
	{
		public IncludedTagsSubView(IEnumerable<ITag> tags) : base(new IncludedTagsSubViewHeader(), CreateItems(tags))
		{
		}

		private static ViewItem[] CreateItems(IEnumerable<ITag> tags) =>
			new[]
			{
				new ViewItem(new[]
				{
					new ViewValues(new IViewCell[]
					{
						new StringViewCell(string.Join(", ", tags.Select(tag => tag.Name)))
					})
				})
			};
	}
}
