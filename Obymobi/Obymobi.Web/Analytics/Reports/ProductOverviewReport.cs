﻿using System.Linq;
using Obymobi.Web.Analytics.Model.ProductOverview;
using SpreadsheetLight;
using Obymobi.Logic.Analytics;

namespace Obymobi.Web.Analytics.Reports
{
    public class ProductOverviewReport : IReport
    {
        #region Constructors

        public ProductOverviewReport(Filter filter)
        {
            this.ProductOverview = new Obymobi.Web.Analytics.Model.ProductOverview.ProductOverview(filter);
            this.ProductOverview.FetchAndProcessData();            
        }

        #endregion

        #region Properties

        private readonly string[] alphabet = { "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

        private ProductOverview ProductOverview { get; set; }

        #endregion

        #region Methods

        public bool RunReport(out SLDocument spreadsheet)
        {
            spreadsheet = new SLDocument();
            string summaryWorkSheetName = "Product Overview";

            // Prepare the Summary worksheet            
            int summaryRowIndex = 2; // (1 is already used below)
            spreadsheet.RenameWorksheet(SLDocument.DefaultFirstSheetName, summaryWorkSheetName);

            // Render product overview
            this.RenderProductOverviewWorksheet(spreadsheet);
            
            // Scale 1x1 Page
            spreadsheet.SelectWorksheet(summaryWorkSheetName);
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 1);
            spreadsheet.SetPageSettings(pageSettings);

            // Set all print sizes to A4
            spreadsheet.SetPageSizeOfWorksheets(SLPaperSizeValues.A4Paper);

            return true;
        }

        private void RenderProductOverviewWorksheet(SLDocument spreadsheet)
        {
            // Create and select worksheet
            spreadsheet.AddWorksheet("Product Overview");
            spreadsheet.SelectWorksheet("Product Overview");

            // Set Headings + styling
            spreadsheet.SetCellValue("A1", "Created");
            spreadsheet.SetCellValue("B1", "OrderitemId");
            spreadsheet.SetCellValue("C1", "OrderId");
            spreadsheet.SetCellValue("D1", "CategoryId");
            spreadsheet.SetCellValue("E1", "CategoryName");
            spreadsheet.SetCellValue("F1", "ProductId");
            spreadsheet.SetCellValue("G1", "ProductName");
            spreadsheet.SetCellValue("H1", "ClientId");
            spreadsheet.SetCellValue("I1", "DeliverypointId");
            spreadsheet.SetCellValue("J1", "DeliverypointName");
            spreadsheet.SetCellValue("K1", "DeliverypointNumber");            

            int index = 0;
            for (int i = 0; i < this.ProductOverview.Product.Alterationitems.Values.Count; i++)
            {
                Alterationitem alterationitem = this.ProductOverview.Product.Alterationitems.Values.ElementAt(i);
                spreadsheet.SetCellValue(this.alphabet[i] + "1", alterationitem.AlterationName);
                index++;
            }

            spreadsheet.SetCellValue(this.alphabet[index] + "1", "DeliverypointgroupName");
            index++;

            spreadsheet.SetCellValue(this.alphabet[index] + "1", "Status");
            index++;

            spreadsheet.SetCellValue(this.alphabet[index] + "1", "Processed");
            index++;

            spreadsheet.SetCellValue(this.alphabet[index] + "1", "Notes");
            
            spreadsheet.SetStyle("A1", this.alphabet[index] + "1", borders: Borders.Bottom, backgroundColor: SpreadsheetHelper.Green14);
            spreadsheet.SetStyle("B1", "D1", DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);
            spreadsheet.SetStyle("F1", "K1", DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);

            int currentRow = 2;
            foreach (Orderitem orderitem in this.ProductOverview.Product.Orderitems.Values)
            {
                this.RenderRowForOrderitem(spreadsheet, orderitem, ref currentRow);
            }

            // Set column widths
            spreadsheet.SetColumnWidth("A", 22);
            spreadsheet.SetColumnWidth("B", 12);
            spreadsheet.SetColumnWidth("C", 8);
            spreadsheet.SetColumnWidth("D", 11);
            spreadsheet.SetColumnWidth("E", 33);
            spreadsheet.SetColumnWidth("F", 10);
            spreadsheet.SetColumnWidth("G", 21);
            spreadsheet.SetColumnWidth("H", 9);
            spreadsheet.SetColumnWidth("I", 15);
            spreadsheet.SetColumnWidth("J", 18);
            spreadsheet.SetColumnWidth("K", 20);

            index = 0;
            for (int i = 0; i < this.ProductOverview.Product.Alterationitems.Values.Count; i++)
            {
                spreadsheet.SetColumnWidth(this.alphabet[i], 15);
                index++;
            }

            spreadsheet.SetColumnWidth(this.alphabet[index], 23);
            index++;
            spreadsheet.SetColumnWidth(this.alphabet[index], 10);
            index++;
            spreadsheet.SetColumnWidth(this.alphabet[index], 22);
            index++;
            spreadsheet.SetColumnWidth(this.alphabet[index], 60);

            // Keep first line frozen
            spreadsheet.FreezePanes(1, 0);

            // Set printing to 1 page width
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 500);
            spreadsheet.SetPageSettings(pageSettings);
        }

        private void RenderRowForOrderitem(SLDocument spreadsheet, Orderitem orderitem, ref int currentRow)
        {
            int productRow = currentRow;
            currentRow++;

            spreadsheet.SetCellValue("A" + productRow, orderitem.CreatedAsString);
            spreadsheet.SetCellValue("B" + productRow, orderitem.OrderitemId);
            spreadsheet.SetCellValue("C" + productRow, orderitem.OrderId);
            spreadsheet.SetCellValue("D" + productRow, orderitem.CategoryId);
            spreadsheet.SetCellValue("E" + productRow, orderitem.CategoryName);
            spreadsheet.SetCellValue("F" + productRow, orderitem.ProductId);
            spreadsheet.SetCellValue("G" + productRow, orderitem.ProductName);
            spreadsheet.SetCellValue("H" + productRow, orderitem.ClientId);
            spreadsheet.SetCellValue("I" + productRow, orderitem.DeliverypointId);
            spreadsheet.SetCellValue("J" + productRow, orderitem.DeliverypointName);
            spreadsheet.SetCellValue("K" + productRow, orderitem.DeliverypointNumber);

            int index = 0;
            for (int i = 0; i < this.ProductOverview.Product.Alterationitems.Values.Count; i++)
            {
                if (orderitem.Alterationitems.Count > i)
                {
                    spreadsheet.SetCellValue(this.alphabet[index] + productRow, orderitem.Alterationitems[i].NameTimeOrValue);
                }                
                index++;
            }

            spreadsheet.SetCellValue(this.alphabet[index] + productRow, orderitem.DeliverypointgroupName);
            index++;

            spreadsheet.SetCellValue(this.alphabet[index] + productRow, orderitem.Status);
            index++;

            spreadsheet.SetCellValue(this.alphabet[index] + productRow, orderitem.ProcessedAsString);
            index++;

            spreadsheet.SetCellValue(this.alphabet[index] + productRow, orderitem.Notes);

            // Only background when including categories, otherwise borders
            spreadsheet.SetStyle("A" + productRow, this.alphabet[index] + productRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green15, backgroundColor: SpreadsheetHelper.Green10);
            spreadsheet.SetStyle("B" + productRow, "D" + productRow, DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);
            spreadsheet.SetStyle("F" + productRow, "K" + productRow, DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);
        }

        #endregion
    }
}
