﻿using System.Collections.Generic;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports
{
    public class InventoryItemSubViewHeader : ViewHeader
    {
        public InventoryItemSubViewHeader(ShowExternalIdentifier showExternalIdentifier) 
            : base(CreateHeaderCells(showExternalIdentifier))
        { }

        private static HeaderCell[] CreateHeaderCells(ShowExternalIdentifier showExternalIdentifier)
        {
            var viewCellBuilder = new HeaderCellBuilder();

            var headerCells = new List<HeaderCell>();

            headerCells.Add(viewCellBuilder.With(Properties.Resources.Item).Build());

            if (showExternalIdentifier)
            {
                headerCells.Add(viewCellBuilder.With(Properties.Resources.ExternalIdentifier).Build());
            }

            headerCells.Add(viewCellBuilder.With(Properties.Resources.Quantity).Build());

            return headerCells.ToArray();
        }
    }
}
