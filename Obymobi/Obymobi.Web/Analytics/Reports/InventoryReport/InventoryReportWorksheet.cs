﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Web.Analytics.Model.InventoryReport;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports
{
	public class InventoryReportWorksheet : Worksheet
	{
		public InventoryReportWorksheet(InventoryReportSheetFilter sheetFilter, string name) 
            : base(name, CreateColumns(sheetFilter))
		{ }

        private static Column[] CreateColumns(InventoryReportSheetFilter sheetFilter)
        {
            IList<Column> columns = new List<Column>();
            columns.Add(new Column(Properties.Resources.ProductNameColumnWidth));

            if (sheetFilter.ShowExternalIdentifier)
            {
                columns.Add(new Column(Properties.Resources.ExternalIdentifierColumnWidth));
            }

            columns.Add(new Column(Properties.Resources.OrderDateColumnWidth));

            return columns.ToArray();
        }
    }
}
