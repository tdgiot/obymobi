﻿using Obymobi.Web.Analytics.Model.InventoryReport;
using Obymobi.Web.Analytics.Reports.ReportBase;
using Obymobi.Web.Analytics.Reports.TransactionsAppLess;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports
{
	public class InventoryItemViewLineRenderingDirector : ViewLineRenderingDirector
	{
		public SpreadsheetRow Render(InventoryReportSheetFilter filter, SLDocument spreadsheet, InventoryItemViewLineBuilder builder, int orderIndex, CellLocation initialLocation)
		{
			var maximumRow = (SpreadsheetRow) 1;
			builder.RenderInventoryItems(spreadsheet, initialLocation, orderIndex, initialLocation, ref maximumRow);
			return maximumRow;
		}
	}
}
