﻿namespace Obymobi.Web.Analytics.Reports
{
	public class InventoryReportBuilderDirector
	{
		public void Construct(InventoryReportBuilder builder)
		{
			builder.BuildWorksheet();
			builder.BuildReportInformation();
			builder.BuildIncludeTagsSubView();
			builder.BuildInventoryItems();
		}
	}
}
