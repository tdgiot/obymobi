﻿using System.Collections.Generic;
using DocumentFormat.OpenXml.Spreadsheet;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Extensions;
using Obymobi.Web.Analytics.Model.InventoryReport;
using Obymobi.Web.Analytics.Reports.ReportBase;
using ServiceStack;

namespace Obymobi.Web.Analytics.Reports
{
	public class InventoryItemSubView : HorizontalReportSubView
	{
		public InventoryItemSubView(InventoryItem[] items, ShowExternalIdentifier showExternalIdentifier)
			: base(
				new InventoryItemSubViewHeader(showExternalIdentifier),
                CreateInventoryItems(items, showExternalIdentifier),
				new InventoryItemSubViewTotals(items.SumQuantity())
                )
		{ }

        private static ViewItem[] CreateInventoryItems(InventoryItem[] items, ShowExternalIdentifier showExternalIdentifier)
		{
            var values = new List<ViewValues>(items.Length);
            foreach (var product in items)
            {
                values.Add(CreateItemRow(product, showExternalIdentifier));
            }

            return new []
            {
                new ViewItem(values.ToArray())
            };
        }

        private static ViewValues CreateItemRow(InventoryItem item, ShowExternalIdentifier showExternalIdentifier)
        {
            IList<IViewCell> cells = new List<IViewCell>();

            cells.Add(new StringViewCell(item.Name));

            if (showExternalIdentifier)
            {
                cells.Add(new StringViewCell(item.ExternalIdentifier));
            }

            cells.Add(new IntViewCell(item.Quantity, HorizontalAlignmentValues.Left));

            return new ViewValues(cells.ToArray());
        }
    }
}
