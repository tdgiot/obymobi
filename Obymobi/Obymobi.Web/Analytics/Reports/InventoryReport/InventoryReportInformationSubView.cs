﻿using System;
using DocumentFormat.OpenXml.Spreadsheet;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports
{
	public class InventoryReportInformationSubView : VerticalReportSubView
	{
		public InventoryReportInformationSubView(string companyName, DateTime fromLocalDateTime, DateTime untilLocalUntilDateTime)
			: base(
                new InventoryReportInformationSubViewHeader(), 
                CreateItems(companyName, fromLocalDateTime, untilLocalUntilDateTime)
                )
		{ }

		private static ViewItem[] CreateItems(string companyName, DateTime fromLocalDateTime, DateTime untilLocalUntilDateTime)
		{
			const string formatCode = "yyyy-mm-dd";
			const HorizontalAlignmentValues horizontalAlignmentValues = HorizontalAlignmentValues.Left;

			return new[]
			{
				new ViewItem(new[]
				{
					new ViewValues(new IViewCell[]
					{
						new StringViewCell(companyName),
						new DateTimeViewCell(fromLocalDateTime, formatCode, horizontalAlignmentValues),
						new DateTimeViewCell(untilLocalUntilDateTime, formatCode, horizontalAlignmentValues)
					})
				})
			};
		}
	}
}
