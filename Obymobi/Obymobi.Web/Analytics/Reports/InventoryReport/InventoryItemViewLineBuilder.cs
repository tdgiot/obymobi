﻿using Obymobi.Web.Analytics.Reports.ReportBase;
using Obymobi.Web.Analytics.Reports.TransactionsAppLess;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports
{
    public class InventoryItemViewLineBuilder : ViewLineBuilder
    {
        private readonly InventoryItemSubView itemsSubView;

        public InventoryItemViewLineBuilder(InventoryItemSubView itemsSubView) => this.itemsSubView = itemsSubView;

        public CellLocation RenderInventoryItems(SLDocument spreadsheet, CellLocation currentLocation, int index, CellLocation initialLocation, ref SpreadsheetRow maximumRow)
            => RenderItem(spreadsheet, currentLocation, itemsSubView, index, initialLocation, ref maximumRow);
    }
}