﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Interfaces;
using Obymobi.Web.Analytics.Model.InventoryReport;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Reports.ProductReport;
using Obymobi.Web.Analytics.Reports.ReportBase;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports
{
	public class InventoryReportBuilder : ReportBuilder
	{
		private readonly Company company;
		private readonly DateTime fromLocalDateTime;
		private readonly IEnumerable<InventoryItem> items;
		private readonly InventoryReportSheetFilter sheetFilter;
		private readonly SLDocument spreadsheet;
		private readonly IEnumerable<ITag> tags;
		private readonly DateTime untilLocalUntilDateTime;
		private CellLocation cellLocation;

		public InventoryReportBuilder(
			SLDocument spreadsheet,
			InventoryReportSheetFilter sheetFilter,
			Company company,
			IEnumerable<InventoryItem> items,
			DateTime fromLocalDateTime,
			DateTime untilLocalUntilDateTime,
			IEnumerable<ITag> tags)
		{
			this.spreadsheet = spreadsheet;
			this.fromLocalDateTime = fromLocalDateTime;
			this.untilLocalUntilDateTime = untilLocalUntilDateTime;
			this.tags = tags;
			this.sheetFilter = sheetFilter;
			this.company = company;
			this.items = items;
			cellLocation = new CellLocation((SpreadsheetColumn) 1, (SpreadsheetRow) 1);
		}

		public void BuildWorksheet()
		{
			InventoryReportWorksheet worksheet = new InventoryReportWorksheet(sheetFilter, sheetFilter.Name);
			worksheet.Render(spreadsheet);
		}

		public void BuildReportInformation()
		{
			CellLocation baseLocation = cellLocation;
			const int oneEmptyRow = 1;

			InventoryReportInformationSubView reportInformationSubView = new InventoryReportInformationSubView(
				company.Name,
				fromLocalDateTime,
				untilLocalUntilDateTime
			);

			cellLocation = RestoreRow(SetLocationToNextColumn(reportInformationSubView.RenderHeader(spreadsheet, cellLocation)), baseLocation);
			cellLocation = RestoreColumn(SetLocationToNextRow(reportInformationSubView.RenderItem(spreadsheet, 0, cellLocation), oneEmptyRow), baseLocation);
		}

		public void BuildIncludeTagsSubView()
		{
			CellLocation baseLocation = cellLocation;
			const int oneEmptyRow = 1;

			IncludedTagsSubView includedTagsSubView = new IncludedTagsSubView(tags);

			cellLocation = RestoreRow(SetLocationToNextColumn(includedTagsSubView.RenderHeader(spreadsheet, cellLocation)), baseLocation);
			cellLocation = RestoreColumn(SetLocationToNextRow(includedTagsSubView.RenderItem(spreadsheet, 0, cellLocation), oneEmptyRow), baseLocation);
		}

		public void BuildInventoryItems()
		{
			InventoryItemSubView itemSubView = new InventoryItemSubView(items.ToArray(), sheetFilter.ShowExternalIdentifier);
			CellLocation itemSubViewLocation = cellLocation;

			RenderHeaderForHorizontalSubView(spreadsheet, itemSubView, itemSubViewLocation);

			InventoryItemViewLineRenderingDirector itemsViewLineRenderingDirector = new InventoryItemViewLineRenderingDirector();
			InventoryItemViewLineBuilder itemsViewLineBuilder = new InventoryItemViewLineBuilder(itemSubView);

			CellLocation initialLocation = cellLocation.ChangeRow(GetNextRow(cellLocation.Row));
			SpreadsheetRow maximumRow = itemsViewLineRenderingDirector.Render(sheetFilter, spreadsheet, itemsViewLineBuilder, 0, initialLocation);
			initialLocation = new CellLocation(initialLocation.Column, GetNextRow(maximumRow));

            CellLocation totalCellLocation = !sheetFilter.ShowExternalIdentifier
                ? initialLocation.MoveColumn(1)
                : initialLocation.MoveColumn(2);

			RenderTotalsForHorizontalSubView(spreadsheet, itemSubView, totalCellLocation);
		}
	}
}
