﻿using DocumentFormat.OpenXml.Spreadsheet;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports
{
    public class InventoryItemSubViewTotals : ViewTotals
    {
        public InventoryItemSubViewTotals(int totalQuantity) 
            : base(CreateTotalCells(totalQuantity))
        { }

        private static IViewCell[] CreateTotalCells(int totalQuantity) => new IViewCell[]
        {
            new IntViewCell(totalQuantity, HorizontalAlignmentValues.Left)
        };
    }
}
