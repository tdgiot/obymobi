﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Web.Analytics.Extensions.ReportBase;
using Obymobi.Web.Analytics.Model.InventoryReport;
using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using Obymobi.Web.Analytics.Reports.ReportBase;
using Obymobi.Web.Analytics.Repositories;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using Obymobi.Web.Analytics.Requests;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports
{
	public sealed class InventoryReport : SpreadsheetReport
	{
		private readonly InventoryReportRequest inventoryReportRequest;
		private readonly IOrderRepository orderRepository;
		private readonly ITagRepository tagRepository;

		public InventoryReport(InventoryReportRequest inventoryReportRequest, IOrderRepository orderRepository, ICompanyRepository companyRepository, ITagRepository tagRepository)
			: base(companyRepository, inventoryReportRequest.CompanyId)
		{
			this.inventoryReportRequest = inventoryReportRequest;
			this.orderRepository = orderRepository;
			this.tagRepository = tagRepository;
		}

		public override bool CreateReport(out SLDocument spreadsheet)
		{
			base.CreateReport(out spreadsheet);

			// Fetch order collection for request
			OrderCollection orderCollection = orderRepository.Get(inventoryReportRequest.ToOrdersRequest());

			string firstSheetName = string.Empty;
			foreach (InventoryReportSheetFilter sheetFilter in inventoryReportRequest.GetActiveSheets())
			{
				if (string.IsNullOrEmpty(firstSheetName))
				{
					firstSheetName = sheetFilter.Name;
				}

				CreateReportSheet(spreadsheet, orderCollection, sheetFilter);
			}

			// Make sure the first worksheet is selected when opening the document for the first time
			spreadsheet.SelectWorksheet(firstSheetName);

			return true;
		}

		private void CreateReportSheet(SLDocument spreadsheet, OrderCollection orderCollection, InventoryReportSheetFilter sheetFilter)
		{
			IEnumerable<InventoryItem> inventoryItems = orderCollection.ToOrderModels(sheetFilter);

			TagCollection tagCollection = tagRepository.GetTags(sheetFilter.IncludedTagIds);

			InventoryReportBuilderDirector reportBuilderDirector = new InventoryReportBuilderDirector();
			reportBuilderDirector.Construct(
				new InventoryReportBuilder(
					spreadsheet,
					sheetFilter,
					Company,
					inventoryItems,
					inventoryReportRequest.GetFromDateLocal(Company.TimeZoneInfo),
					inventoryReportRequest.GetUntilDateLocal(Company.TimeZoneInfo),
					tagCollection
				)
			);
		}
	}
}
