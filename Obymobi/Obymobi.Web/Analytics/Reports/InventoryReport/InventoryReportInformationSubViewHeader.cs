﻿using System.Drawing;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports
{
	public class InventoryReportInformationSubViewHeader : ViewHeader
	{
		public InventoryReportInformationSubViewHeader() 
            : base(CreateHeaderCells())
		{ }

		private static HeaderCell[] CreateHeaderCells()
		{
			var viewCellBuilder = new HeaderCellBuilder().With(FontStyle.Bold);
			return new[]
			{
				viewCellBuilder.With(Properties.Resources.VendorName).Build(),
				viewCellBuilder.With(Properties.Resources.ReportPeriodFrom).Build(),
				viewCellBuilder.With(Properties.Resources.ReportPeriodTo).Build()
			};
		}
	}
}
