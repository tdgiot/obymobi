﻿using Obymobi.Web.Analytics.Reports.ReportBase;
using Obymobi.Web.Analytics.Reports.TransactionsAppLess;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports
{
    public abstract class ReportBuilder
    {
        protected static CellLocation RestoreRow(CellLocation location, CellLocation baseLocation) => new CellLocation(location.Column, baseLocation.Row);
        protected static CellLocation RestoreColumn(CellLocation location, CellLocation baseLocation) => new CellLocation(baseLocation.Column, location.Row);
        protected static CellLocation SetLocationToNextRow(CellLocation location, int rowAmountToSkip = 0) =>
            location.ChangeRow(GetNextRow(location.Row, rowAmountToSkip));
        protected static CellLocation SetLocationToNextColumn(CellLocation location, int columnAmountToSkip = 0) =>
            location.ChangeColumn(GetNextColumn(location.Column, columnAmountToSkip));
        protected static SpreadsheetRow GetNextRow(SpreadsheetRow currentRow, int rowAmountToSkip = 0) => (SpreadsheetRow)((int)currentRow + 1 + rowAmountToSkip);
        protected static SpreadsheetColumn GetNextColumn(SpreadsheetColumn currentColumn, int columnAmountToSkip = 0) => (SpreadsheetColumn)((int)currentColumn + 1 + columnAmountToSkip);
        protected static void RenderHeaderForHorizontalSubView(SLDocument spreadsheet, ReportSubView subView, CellLocation location)
            => SetLocationToNextColumn(subView.RenderHeader(spreadsheet, location));
        protected static void RenderTotalsForHorizontalSubView(SLDocument spreadsheet, ReportSubView subView, CellLocation location)
            => SetLocationToNextColumn(subView.RenderTotals(spreadsheet, location));
	}
}
