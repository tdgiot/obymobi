﻿using System;
using DocumentFormat.OpenXml.Spreadsheet;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public class ReportInformationSubView : VerticalReportSubView
	{
		public ReportInformationSubView(string companyName, Currency currency, DateTime fromLocalDateTime, DateTime untilLocalUntilDateTime, Orders orders)
			: base(new ReportInformationSubViewHeader(), CreateItems(companyName, currency, fromLocalDateTime, untilLocalUntilDateTime, orders))
		{
		}

		private static ViewItem[] CreateItems(string companyName, Currency currency, DateTime fromLocalDateTime, DateTime untilLocalUntilDateTime, Orders orders)
		{
			const string formatCode = "yyyy-mm-dd";
			const HorizontalAlignmentValues horizontalAlignmentValues = HorizontalAlignmentValues.Left;

			return new[]
			{
				new ViewItem(new[]
				{
					new ViewValues(new IViewCell[]
					{
						new StringViewCell(companyName),
						new DateTimeViewCell(fromLocalDateTime, formatCode, horizontalAlignmentValues),
						new DateTimeViewCell(untilLocalUntilDateTime, formatCode, horizontalAlignmentValues),
						new StringViewCell(string.Empty),
						new IntViewCell(orders.Count, HorizontalAlignmentValues.Left),
                        new CurrencyViewCell(orders.TotalReportRevenue, currency, HorizontalAlignmentValues.Left)
					})
				})
			};
		}
	}
}
