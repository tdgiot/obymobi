﻿using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml.Spreadsheet;
using Obymobi.Enums;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Reports.ProductReport;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public class OrderItemSubView : HorizontalReportSubView
	{
		public OrderItemSubView(Orders orders, Currency currency, ShowProductCategoryOption showProductCategory, ShowProductsOption showProducts, ShowAlterationsOption includeAlterations,
			ShowProductQuantityOption showQuantities,
			ShowProductPriceOption showPrices)
			: base(
				new OrderItemSubViewHeader(showProductCategory, showQuantities, showPrices),
				CreateItems(orders, currency, showProductCategory, showProducts, includeAlterations, showQuantities, showPrices),
				new EmptyViewTotals())
		{
		}

		private static ViewItem[] CreateItems(Orders orders, Currency currency, ShowProductCategoryOption showProductCategory, ShowProductsOption showProducts,
			ShowAlterationsOption includeAlterations, ShowProductQuantityOption showQuantities,
			ShowProductPriceOption showPrices)
		{
			var viewItems = new List<ViewItem>();
			foreach (var order in orders)
			{
				var values = new List<ViewValues>();
				foreach (var orderItem in order.OrderItems)
				{
					if (!showProducts && !includeAlterations && orderItem.Type == OrderitemType.Product)
					{
						continue;
					}

					var viewCells = new List<IViewCell>();

					if (showProductCategory)
					{
						viewCells.Add(new StringViewCell(orderItem.CategoryName));
					}

					viewCells.Add(new StringViewCell(orderItem.Name));

					if (showQuantities)
					{
						if (orderItem.Type == OrderitemType.Product)
						{
							viewCells.Add(new IntViewCell(orderItem.Quantity, HorizontalAlignmentValues.Center));
						}
						else
						{
							viewCells.Add(new StringViewCell(string.Empty));
						}
					}

					if (showPrices)
					{
                        viewCells.Add(new CurrencyViewCell(orderItem.Price, currency, HorizontalAlignmentValues.Right));
					}

					values.Add(new ViewValues(viewCells.ToArray()));

					if (!includeAlterations || !orderItem.OrderItemAlterationItems.Any())
					{
						continue;
					}

					var lastAlterationItem = orderItem.OrderItemAlterationItems.Last();
					foreach (var alterationItem in orderItem.OrderItemAlterationItems)
					{
						var isLastAlterationItem = alterationItem.Equals(lastAlterationItem);
						var borderStyleValues = GetBorderStyleValues(isLastAlterationItem);
						var borders = GetBorders(isLastAlterationItem);


						var alterationValue = alterationItem.AlterationValue.Length > 0 ? $": {alterationItem.AlterationValue}" : string.Empty;

						var alterationViewCells = new List<IViewCell>();

						if (showProductCategory)

						{
							alterationViewCells.Add(new StringViewCell(string.Empty));
						}

						alterationViewCells.Add(new StringViewCell($"- {alterationItem.AlterationName} ({alterationItem.AlterationOptionName}{alterationValue})", null, null, null, null, borders, null,
							borderStyleValues));

						if (showQuantities)
						{
							alterationViewCells.Add(new StringViewCell(string.Empty, null, null, null, null, borders, null, borderStyleValues));
						}

						if (showPrices)
						{
							alterationViewCells.Add(new CurrencyViewCell(alterationItem.PriceInTax, currency, HorizontalAlignmentValues.Right, borders, borderStyleValues));
						}

						values.Add(new ViewValues(alterationViewCells.ToArray()));
					}
				}

				if (!values.Any())
				{
					// Add empty cells to align the cell correctly.
					var viewCells = new List<IViewCell>();
					if (showProductCategory)
					{
						viewCells.Add(new StringViewCell(string.Empty));
					}

					viewCells.Add(new StringViewCell(string.Empty));

					if (showQuantities)
					{
						viewCells.Add(new StringViewCell(string.Empty));
					}

					if (showPrices)
					{
						viewCells.Add(new StringViewCell(string.Empty));
					}


					values.Add(new ViewValues(viewCells.ToArray()));
				}

				viewItems.Add(new ViewItem(values.ToArray()));
			}

			return viewItems.ToArray();
		}

		private static Borders? GetBorders(bool lastAlterationItem) => lastAlterationItem ? (Borders?) Borders.Bottom : null;

		private static BorderStyleValues GetBorderStyleValues(bool lastAlterationItem) => lastAlterationItem ? BorderStyleValues.Dashed : BorderStyleValues.Thin;
	}
}
