﻿using System.Collections.Generic;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public class CustomerInformationSubView : HorizontalReportSubView
	{
		public CustomerInformationSubView(Orders orders) : base(new CustomerInformationSubViewHeader(), CreateItems(orders))
		{
		}

		private static ViewItem[] CreateItems(Orders orders)
		{
			var viewItems = new List<ViewItem>();
			foreach (var order in orders)
			{
				var emailPhone = CombineEmailAndPhoneNumber(order);
				var postCodeAndCity = $"{order.PostCode} {order.City}";

				var values = new List<ViewValues>();

				AddIfNotEmpty(values, order.CustomerName);
				AddIfNotEmpty(values, emailPhone);
				AddIfNotEmpty(values, order.Address);
				AddIfNotEmpty(values, postCodeAndCity);

				viewItems.Add(new ViewItem(values.ToArray()));
			}

			return viewItems.ToArray();
		}

		private static void AddIfNotEmpty(ICollection<ViewValues> values, string value)
		{
			if (!string.IsNullOrWhiteSpace(value))
			{
				values.Add(new ViewValues(new IViewCell[]
				{
					new StringViewCell(value)
				}));
			}
		}

		private static string CombineEmailAndPhoneNumber(Order order)
		{
			var emailPhone = order.CustomerPhoneNumber;

			if (!string.IsNullOrWhiteSpace(emailPhone) && !string.IsNullOrWhiteSpace(order.CustomerEmail))
			{
				emailPhone += @" / ";
			}

			if (!string.IsNullOrWhiteSpace(order.CustomerEmail))
			{
				emailPhone += order.CustomerEmail;
			}

			return emailPhone;
		}
	}
}
