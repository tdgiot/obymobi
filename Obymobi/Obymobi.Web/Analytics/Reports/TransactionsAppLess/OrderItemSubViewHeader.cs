﻿using System.Collections.Generic;
using System.Drawing;
using DocumentFormat.OpenXml.Spreadsheet;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public class OrderItemSubViewHeader : ViewHeader
	{
		public OrderItemSubViewHeader(ShowProductCategoryOption showProductCategory, ShowProductQuantityOption showQuantities, ShowProductPriceOption showPrices) : base(
			CreateHeaderCells(showProductCategory, showQuantities, showPrices))
		{
		}

		private static HeaderCell[] CreateHeaderCells(ShowProductCategoryOption showProductCategory, ShowProductQuantityOption showQuantities, ShowProductPriceOption showPrices)
		{
			var viewCellBuilder = new HeaderCellBuilder().With(FontStyle.Bold);

			var headerCells = new List<HeaderCell>();

			if (showProductCategory)
			{
				headerCells.Add(viewCellBuilder.With("Category").Build());
			}

			headerCells.Add(viewCellBuilder.With(Properties.Resources.Order).Build());

			if (showQuantities)
			{
				headerCells.Add(viewCellBuilder.With(Properties.Resources.Quantity).With(HorizontalAlignmentValues.Center).Build());
			}

			if (showPrices)
			{
				headerCells.Add(viewCellBuilder.With(Properties.Resources.Price).With(HorizontalAlignmentValues.Right).Build());
			}

			return headerCells.ToArray();
		}
	}
}
