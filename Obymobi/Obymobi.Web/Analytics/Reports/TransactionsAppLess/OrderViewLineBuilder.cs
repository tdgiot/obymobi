using Obymobi.Web.Analytics.Reports.ReportBase;
using SpreadsheetLight;
using System.Drawing;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
    public class OrderViewLineBuilder : ViewLineBuilder
    {
        public OrderViewLineBuilder(
            OrderSubView orderSubView,
            OrderItemSubView orderItemSubView,
            CustomerInformationSubView customerInformationSubView,
            NotesSubView notesSubView)
        {
            OrderSubView = orderSubView;
            OrderItemSubView = orderItemSubView;
            CustomerInformationSubView = customerInformationSubView;
            NotesSubView = notesSubView;

        }

        private OrderSubView OrderSubView { get; }

        private OrderItemSubView OrderItemSubView { get; }

        private CustomerInformationSubView CustomerInformationSubView { get; }

        private NotesSubView NotesSubView { get; }

        public CellLocation RenderOrder(SLDocument spreadsheet, CellLocation currentLocation, int orderIndex, CellLocation initialLocation, ref SpreadsheetRow maximumRow)
            => RenderItem(spreadsheet, currentLocation, OrderSubView, orderIndex, initialLocation, ref maximumRow);

        public CellLocation RenderOrderItems(SLDocument spreadsheet, CellLocation currentLocation, int orderIndex, CellLocation initialLocation, ref SpreadsheetRow maximumRow)
            => RenderItem(spreadsheet, currentLocation, OrderItemSubView, orderIndex, initialLocation, ref maximumRow);

        public CellLocation RenderCustomerInformation(SLDocument spreadsheet, CellLocation currentLocation, int orderIndex, CellLocation initialLocation, ref SpreadsheetRow maximumRow)
            => RenderItem(spreadsheet, currentLocation, CustomerInformationSubView, orderIndex, initialLocation, ref maximumRow);

        public CellLocation RenderNotes(SLDocument spreadsheet, CellLocation currentLocation, int orderIndex, CellLocation initialLocation, ref SpreadsheetRow maximumRow)
        {
            var previousLocation = currentLocation;
            currentLocation = RenderItem(spreadsheet, currentLocation, NotesSubView, orderIndex, initialLocation, ref maximumRow);

            if (currentLocation.IsEmpty)
            {
                currentLocation = previousLocation;
            }

            maximumRow = maximumRow.Max(currentLocation.Row);
            currentLocation = currentLocation.ChangeRow(maximumRow);

            if (!Equals(currentLocation, previousLocation) && !spreadsheet.MergeWorksheetCells(previousLocation, currentLocation))
            {
                throw new SpreadsheetException($"Failed merge the notes & instructions cell on spreadsheet. Start: {previousLocation} End: {currentLocation}");
            }

            return currentLocation;
        }

        public void RenderBorder(SLDocument spreadsheet, SpreadsheetColumn leftColumn, SpreadsheetColumn rightColumn, SpreadsheetRow row) =>
            spreadsheet.SetBorder(Color.Empty, Borders.Bottom, (int)leftColumn, (int)rightColumn, (int)row, (int)row);
    }
}
