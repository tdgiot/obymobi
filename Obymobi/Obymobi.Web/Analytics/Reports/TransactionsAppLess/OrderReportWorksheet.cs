using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using Obymobi.Web.Analytics.Reports.ReportBase;
using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
    public class OrderReportWorksheet : Worksheet
    {
        public OrderReportWorksheet(TransactionsAppLessSheetFilter request, string name) : base(name, CreateColumns(request))
        {
        }

        private static Column[] CreateColumns(TransactionsAppLessSheetFilter request)
        {
            var columns = new List<Column>(
                new[]
                {
					// Order view columns
					new Column(Properties.Resources.OrderDateColumnWidth),
                    new Column(Properties.Resources.OrderNumberColumnWidth)
                });

            if (request.ShowServiceMethod)
            {
                columns.Add(new Column(Properties.Resources.ServiceMethodColumnWidth));
            }

            if (request.ShowCheckoutMethod)
            {
                columns.Add(new Column(Properties.Resources.CheckoutMethodColumnWidth));
            }

            if (request.ShowPaymentMethod)
            {
                columns.Add(new Column(Properties.Resources.PaymentMethodColumnWidth));
            }

            if (request.ShowCardSummary)
            {
                columns.Add(new Column(Properties.Resources.CardSummaryColumnWidth));
            }

            if (request.ShowDeliveryPoint)
            {
                columns.Add(new Column(Properties.Resources.DeliveryPointColumnWidth));
            }

            if (request.ShowCoversCount)
            {
                columns.Add(new Column(Properties.Resources.CoversCountColumnWidth));
            }

            columns.Add(new Column(Properties.Resources.OrderTotalColumnWidth));
            columns.AddRange(CreateOrderItemColumns(request));
            columns.AddRange(CreateCustomerInformationColumns(request));
            columns.AddRange(CreateNotesColumns(request));

            return columns.ToArray();
        }

        private static IEnumerable<Column> CreateOrderItemColumns(TransactionsAppLessSheetFilter request)
        {
            var columns = new List<Column>();
            if (request.ShowProductCategory)
            {
                columns.Add(new Column(Properties.Resources.ProductCategoryColumnWidth));
            }

            columns.Add(new Column(Properties.Resources.OrderColumnWidth));

            if (request.ShowProductQuantity)
            {
                columns.Add(new Column(Properties.Resources.QuantityColumnWidth));
            }

            if (request.ShowProductPrice)
            {
                columns.Add(new Column(Properties.Resources.PriceColumnWidth));
            }

            return columns;
        }

        private static IEnumerable<Column> CreateCustomerInformationColumns(TransactionsAppLessSheetFilter request) =>
            request.ShowCustomerInfo ? new[] { new Column(Properties.Resources.CustomerInformationColumnWidth) } : new Column[0];

        private static IEnumerable<Column> CreateNotesColumns(TransactionsAppLessSheetFilter request) =>
            request.ShowOrderNotes ? new[] { new Column(Properties.Resources.NotesColumnWidth) } : new Column[0];
    }
}
