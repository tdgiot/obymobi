using DocumentFormat.OpenXml.Spreadsheet;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Reports.ReportBase;
using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
    public class OrderSubViewHeader : ViewHeader
    {
        public OrderSubViewHeader(ShowServiceMethod showServiceMethod, ShowCheckoutMethod showCheckoutMethod, ShowPaymentMethod showPaymentMethod, ShowCardSummary showCardSummary, ShowDeliveryPoint showDeliveryPoint, ShowCoversCount showCoversCount) :
            base(CreateHeaderCells(showServiceMethod, showCheckoutMethod, showPaymentMethod, showCardSummary, showDeliveryPoint, showCoversCount))
        {
        }

        private static HeaderCell[] CreateHeaderCells(ShowServiceMethod showServiceMethod, ShowCheckoutMethod showCheckoutMethod, ShowPaymentMethod showPaymentMethod, ShowCardSummary showCardSummary, ShowDeliveryPoint showDeliveryPoint, ShowCoversCount showCoversCount)
        {
            var viewCellBuilder = new HeaderCellBuilder();
            var headerCells = new List<HeaderCell>
            {
                viewCellBuilder.With(Properties.Resources.OrderDate).Build(),
                viewCellBuilder.With(Properties.Resources.OrderNumber).Build()
            };


            if (showServiceMethod)
            {
                headerCells.Add(viewCellBuilder.With(Properties.Resources.ServiceMethod).With(HorizontalAlignmentValues.Center).Build());
            }

            if (showCheckoutMethod)
            {
                headerCells.Add(viewCellBuilder.With(Properties.Resources.CheckoutMethod).With(HorizontalAlignmentValues.Center).Build());
            }

            if (showPaymentMethod)
            {
                headerCells.Add(viewCellBuilder.With(Properties.Resources.PaymentMethod).With(HorizontalAlignmentValues.Center).Build());
            }

            if (showCardSummary)
            {
                headerCells.Add(viewCellBuilder.With(Properties.Resources.CardSummary).With(HorizontalAlignmentValues.Center).Build());
            }

            if (showDeliveryPoint)
            {
                headerCells.Add(viewCellBuilder.With(Properties.Resources.DeliveryPoint).With(HorizontalAlignmentValues.Center).Build());
            }

            if (showCoversCount)
            {
                headerCells.Add(viewCellBuilder.With(Properties.Resources.CoverCount).With(HorizontalAlignmentValues.Center).Build());
            }

            headerCells.Add(viewCellBuilder.With(Properties.Resources.OrderTotal).With(HorizontalAlignmentValues.Right).Build());

            return headerCells.ToArray();
        }
    }
}
