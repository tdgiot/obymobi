﻿using System.Drawing;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public class CustomerInformationSubViewHeader : ViewHeader
	{
		public CustomerInformationSubViewHeader() : base(CreateHeaderCells())
		{
		}

		private static HeaderCell[] CreateHeaderCells()
		{
			var viewCellBuilder = new HeaderCellBuilder().With(FontStyle.Bold);
			return new[]
			{
				viewCellBuilder.With(Properties.Resources._3).Build(),
			};
		}

	}
}