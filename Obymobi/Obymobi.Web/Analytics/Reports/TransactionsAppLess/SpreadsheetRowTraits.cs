﻿using System;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public static class SpreadsheetRowTraits
	{
		public static SpreadsheetRow Max(this SpreadsheetRow self, SpreadsheetRow other) => (SpreadsheetRow)Math.Max((int)self, (int)other);
	}
}
