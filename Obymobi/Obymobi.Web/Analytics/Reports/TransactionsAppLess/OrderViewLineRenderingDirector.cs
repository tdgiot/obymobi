using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using Obymobi.Web.Analytics.Reports.ReportBase;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
    public class OrderViewLineRenderingDirector : ViewLineRenderingDirector
    {
        public SpreadsheetRow Render(TransactionsAppLessSheetFilter filter, SLDocument spreadsheet, OrderViewLineBuilder builder, int orderIndex,
            CellLocation initialLocation)
        {
            var maximumRow = (SpreadsheetRow)1;
            var currentLocation = builder.RenderOrder(spreadsheet, initialLocation, orderIndex, initialLocation, ref maximumRow);

            if (ShowProducts(filter))
            {
                currentLocation = builder.RenderOrderItems(spreadsheet, MoveToNextColumn(currentLocation), orderIndex, initialLocation, ref maximumRow);
            }

            if (filter.ShowCustomerInfo)
            {
                currentLocation = builder.RenderCustomerInformation(spreadsheet, MoveToNextColumn(currentLocation), orderIndex, initialLocation, ref maximumRow);
            }

            if (filter.ShowOrderNotes)
            {
                currentLocation = builder.RenderNotes(spreadsheet, MoveToNextColumn(currentLocation), orderIndex, initialLocation, ref maximumRow);
            }

            builder.RenderBorder(spreadsheet, initialLocation.Column, currentLocation.Column, maximumRow);

            return maximumRow;
        }

        private static bool ShowProducts(TransactionsAppLessSheetFilter request) => request.ShowProducts || request.ShowSystemProducts || request.ShowAlterations;
    }
}
