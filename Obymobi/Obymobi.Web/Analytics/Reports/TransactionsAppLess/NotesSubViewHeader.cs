using Obymobi.Web.Analytics.Reports.ReportBase;
using System.Drawing;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
    public class NotesSubViewHeader : ViewHeader
    {
        public NotesSubViewHeader() : base(CreateHeaderCells())
        {
        }

        private static HeaderCell[] CreateHeaderCells()
        {
            var viewCellBuilder = new HeaderCellBuilder().With(FontStyle.Bold);
            return new[]
            {
                viewCellBuilder.With(Properties.Resources.NotesAndInstructions).Build(),
            };
        }
    }
}
