﻿using Obymobi.Web.Analytics.Model.ProductReport;
using Obymobi.Web.Analytics.Reports.ReportBase;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public class ProductViewLineRenderingDirector : ViewLineRenderingDirector
	{
		public SpreadsheetRow Render(ProductReportSheetFilter filter, SLDocument spreadsheet, ProductViewLineBuilder builder, int orderIndex, CellLocation initialLocation)
		{
			var maximumRow = (SpreadsheetRow) 1;
			builder.RenderProducts(spreadsheet, initialLocation, orderIndex, initialLocation, ref maximumRow);
			return maximumRow;
		}
	}
}
