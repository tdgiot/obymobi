using DocumentFormat.OpenXml.Spreadsheet;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Reports.ReportBase;
using SpreadsheetLight;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
    public class NotesSubView : HorizontalReportSubView
    {
        public NotesSubView(Orders orders) : base(new NotesSubViewHeader(), CreateItems(orders))
        {
        }

        protected override CellLocation RenderValues(SLDocument spreadsheet, IEnumerable<ViewValues> viewValues, CellLocation cellLocation)
        {
            var rowIndex = cellLocation.Row;
            var maxColumn = cellLocation.Column;
            var maxRow = cellLocation.Row;
            var cellContent = new StringBuilder();
            foreach (var viewRow in viewValues)
            {
                var column = cellLocation.Column;
                foreach (var cell in viewRow.Cells)
                {
                    _ = cellContent
                        .AppendLine(cell.ToString())
                        .AppendLine();

                    maxColumn = maxColumn.Max(column);
                    column++;
                }

                maxRow = maxRow.Max(rowIndex);
                rowIndex += viewRow.RowCount;
            }

            if (!spreadsheet.SetCellValue(cellLocation, cellContent.ToString().Trim()))
            {
                throw new SpreadsheetException($"Failed to set the notes & instructions on spreadsheet: {cellContent}.", cellLocation);
            }

            var style = spreadsheet.CreateStyle();
            style.SetWrapText(true);

            if (!spreadsheet.SetCellStyle(cellLocation, style))
            {
                throw new SpreadsheetException("Failed to set notes & instructions to wrap text on spreadsheet.", cellLocation);
            }

            spreadsheet.SetCellStyleVerticalAlign(cellLocation, VerticalAlignmentValues.Top);

            return new CellLocation(maxColumn, maxRow);
        }

        private static ViewItem[] CreateItems(Orders orders)
        {
            var viewItems = new List<ViewItem>();
            foreach (var order in orders)
            {
                if (string.IsNullOrWhiteSpace(order.Notes) && string.IsNullOrWhiteSpace(order.Instructions))
                {
                    viewItems.Add(new ViewItem(new[]
                    {
                        new ViewValues(new IViewCell[]
                        {
                            new StringViewCell(string.Empty)
                        })
                    }));

                    continue;
                }

                var values = new List<ViewValues>();

                if (!string.IsNullOrWhiteSpace(order.Notes))
                {
                    values.Add(new ViewValues(new IViewCell[]
                    {
                        new StringViewCell(order.Notes)
                    }));
                }

                if (!string.IsNullOrWhiteSpace(order.Instructions))
                {
                    values.Add(new ViewValues(new IViewCell[]
                    {
                        new StringViewCell(order.Instructions)
                    }));
                }

                if (values.Any())
                {
                    viewItems.Add(new ViewItem(values.ToArray()));
                }
            }

            return viewItems.ToArray();
        }
    }
}
