using Obymobi.Data.CollectionClasses;
using Obymobi.Web.Analytics.Extensions.ReportBase;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using Obymobi.Web.Analytics.Reports.ReportBase;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using Obymobi.Web.Analytics.Requests;
using SpreadsheetLight;
using System;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
    public sealed class OrderReport : SpreadsheetReport
    {
        public OrderReport(TransactionsAppLessRequest request, IOrderRepository orderRepository, ICompanyRepository companyRepository)
            : base(companyRepository, request.CompanyId)
        {
            Request = request;
            OrderRepository = orderRepository;

            Title = string.Empty;
            SubTitle = Properties.Resources.OrderReport;
        }

        private TransactionsAppLessRequest Request { get; }

        private IOrderRepository OrderRepository { get; }

        private string Title { get; }

        private string SubTitle { get; }

        public override bool CreateReport(out SLDocument spreadsheet)
        {
            base.CreateReport(out spreadsheet);

            // Fetch order collection for request
            var orderCollection = OrderRepository.Get(Request.ToOrdersRequest());

            var firstSheetName = string.Empty;
            foreach (TransactionsAppLessSheetFilter sheetFilter in Request.GetActiveSheets())
            {
                if (string.IsNullOrEmpty(firstSheetName))
                {
                    firstSheetName = sheetFilter.Name;
                }

                CreateReportSheet(spreadsheet, orderCollection, sheetFilter);
            }

            // Make sure the first worksheet is selected when opening the document for the first time
            spreadsheet.SelectWorksheet(firstSheetName);

            return true;
        }

        public void CreateReportManual(SLDocument spreadsheet, TransactionsAppLessSheetFilter sheetFilter)
        {
            var request = new TransactionsAppLessRequest
            {
                CompanyId = Request.CompanyId,
                CompanyTimeZone = Request.CompanyTimeZone,
                UntilDateTimeUtc = Request.UntilDateTimeUtc,
                LeadTimeOffset = Request.LeadTimeOffset,

                OutletId = sheetFilter.OutletId,
                ShowProducts = sheetFilter.ShowProducts,
                ShowProductQuantity = sheetFilter.ShowProductQuantity,
                ShowProductPrice = sheetFilter.ShowProductPrice,
                ShowAlterations = sheetFilter.ShowAlterations,
                ShowSystemProducts = sheetFilter.ShowSystemProducts,
                ShowOrderNotes = sheetFilter.ShowOrderNotes,
                ShowCustomerInfo = sheetFilter.ShowCustomerInfo,
                ShowProductCategory = sheetFilter.ShowProductCategory,
                ShowPaymentMethod = sheetFilter.ShowPaymentMethod,
                ShowCardSummary = sheetFilter.ShowCardSummary,
                ShowDeliveryPoint = sheetFilter.ShowDeliveryPoint,
                ShowCoversCount = sheetFilter.ShowCoversCount,
                ServiceMethods = sheetFilter.ServiceMethods,
                CheckoutMethods = sheetFilter.CheckoutMethods,
                IncludedSystemProductTypes = sheetFilter.IncludedSystemProductTypes,
                CategoryFilterType = sheetFilter.CategoryFilterType,
                CategoryFilterIds = sheetFilter.CategoryFilterIds,
                IncludeTaxTariffIds = sheetFilter.IncludeTaxTariffIds
            };

            var orderCollection = OrderRepository.Get(request.ToOrdersRequest());

            CreateReportSheet(spreadsheet, orderCollection, sheetFilter);
        }

        private void CreateReportSheet(SLDocument spreadsheet, OrderCollection orderCollection, TransactionsAppLessSheetFilter sheetFilter)
        {
            var filteredOrders = orderCollection.ToOrderModels(sheetFilter);

            var orderReportWorksheet = new OrderReportWorksheet(sheetFilter, sheetFilter.Name);
            orderReportWorksheet.Render(spreadsheet);

            var reportTitleSubView = new ReportTitleSubView(Title, SubTitle);
            var currentLocation = reportTitleSubView.RenderItem(spreadsheet, 0, new CellLocation((SpreadsheetColumn)1, (SpreadsheetRow)1));

            const int whiteLines = 1;
            currentLocation = AddReportInformation(spreadsheet, filteredOrders, SetLocationToNextRow(currentLocation, whiteLines),
                Request.GetFromDateLocal(Company.TimeZoneInfo),
                Request.GetUntilDateLocal(Company.TimeZoneInfo));

            ShowOrders(spreadsheet, filteredOrders, currentLocation, sheetFilter);
        }

        private void ShowOrders(SLDocument spreadsheet, Orders orders, CellLocation currentLocation, TransactionsAppLessSheetFilter sheetFilter)
        {
            var currency = (Currency)Company.CurrencyCode;
            var orderSubView = new OrderSubView(orders, Company.TimeZoneInfo, currency, sheetFilter.ShowServiceMethod, sheetFilter.ShowCheckoutMethod, sheetFilter.ShowPaymentMethod, sheetFilter.ShowCardSummary, sheetFilter.ShowDeliveryPoint, sheetFilter.ShowCoversCount);
            var orderItemSubView = new OrderItemSubView(orders, currency, sheetFilter.ShowProductCategory, sheetFilter.ShowProducts, sheetFilter.ShowAlterations, sheetFilter.ShowProductQuantity, sheetFilter.ShowProductPrice);
            var customerInformationSubView = new CustomerInformationSubView(orders);
            var notesSubView = new NotesSubView(orders);

            var orderSubViewLocation = currentLocation;
            var nextHeaderLocation = RenderHeaderForHorizontalSubView(spreadsheet, orderSubView, orderSubViewLocation);

            var productsSubViewLocation = nextHeaderLocation;
            if (ShowProducts(sheetFilter))
            {
                nextHeaderLocation = RenderHeaderForHorizontalSubView(spreadsheet, orderItemSubView, productsSubViewLocation);
            }

            var customerInformationSubViewLocation = nextHeaderLocation;
            if (sheetFilter.ShowCustomerInfo)
            {
                nextHeaderLocation = RenderHeaderForHorizontalSubView(spreadsheet, customerInformationSubView, customerInformationSubViewLocation);
            }

            var orderNotesSubViewLocation = nextHeaderLocation;
            if (sheetFilter.ShowOrderNotes)
            {
                nextHeaderLocation = RenderHeaderForHorizontalSubView(spreadsheet, notesSubView, orderNotesSubViewLocation);
            }

            var orderViewLineRenderingDirector = new OrderViewLineRenderingDirector();
            var orderViewLineBuilder = new OrderViewLineBuilder(orderSubView, orderItemSubView, customerInformationSubView, notesSubView);

            var initialLocation = currentLocation.ChangeRow(GetNextRow(currentLocation.Row));
            for (var orderIndex = 0; orderIndex < orders.Count; orderIndex++)
            {
                var maximumRow = orderViewLineRenderingDirector.Render(sheetFilter, spreadsheet, orderViewLineBuilder, orderIndex, initialLocation);
                initialLocation = new CellLocation(initialLocation.Column, GetNextRow(maximumRow));
            }
        }

        private static bool ShowProducts(TransactionsAppLessSheetFilter request) => request.ShowProducts || request.ShowSystemProducts || request.ShowAlterations;

        private CellLocation AddReportInformation(SLDocument spreadsheet, Orders orders, CellLocation location, DateTime fromLocalDateTime, DateTime untilLocalUntilDateTime)
        {
            var baseLocation = location;
            const int oneEmptyRow = 1;

            var reportInformationSubView = new ReportInformationSubView(
                Company.Name,
                (Currency)Company.CurrencyCode,
                fromLocalDateTime,
                untilLocalUntilDateTime,
                orders);

            location = RestoreRow(SetLocationToNextColumn(reportInformationSubView.RenderHeader(spreadsheet, location)), baseLocation);
            return RestoreColumn(SetLocationToNextRow(reportInformationSubView.RenderItem(spreadsheet, 0, location), oneEmptyRow), baseLocation);
        }
    }
}
