﻿using Obymobi.Web.Analytics.Reports.ProductReport;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public class TagsSubView : VerticalReportSubView
	{
		public TagsSubView(string[] tags) : base(new TagsSubViewHeader(), CreateItems(tags))
		{
		}

		private static ViewItem[] CreateItems(string[] tags) =>
			new[]
			{
				new ViewItem(new[]
				{
					new ViewValues(new IViewCell[]
					{
						new StringViewCell(string.Join(", ", tags))
					})
				})
			};
	}
}
