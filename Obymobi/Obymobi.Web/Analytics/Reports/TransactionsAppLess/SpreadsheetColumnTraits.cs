﻿using System;
using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public static class SpreadsheetColumnTraits
	{
		public static SpreadsheetColumn Max(this SpreadsheetColumn self, SpreadsheetColumn other) => (SpreadsheetColumn)Math.Max((int)self, (int)other);
	}
}
