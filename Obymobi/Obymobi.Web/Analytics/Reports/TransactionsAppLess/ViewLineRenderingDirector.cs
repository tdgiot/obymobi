﻿using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public abstract class ViewLineRenderingDirector
	{
		protected static CellLocation MoveToNextColumn(CellLocation cellLocation) => cellLocation.ChangeColumn(cellLocation.Column + 1);
	}
}
