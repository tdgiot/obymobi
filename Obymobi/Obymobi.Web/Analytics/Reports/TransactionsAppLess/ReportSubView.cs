﻿using System.Collections.Generic;
using Obymobi.Web.Analytics.Reports.ReportBase;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public abstract class ReportSubView
	{
		protected ReportSubView(ViewHeader header, ViewItem[] valueItems, ViewTotals viewTotals)
		{
			Header = header;
			ValueItems = valueItems;
			ViewTotals = viewTotals;
		}

		protected ViewHeader Header { get; }

		protected ViewItem[] ValueItems { get; }

		public ViewTotals ViewTotals { get; }

		public virtual CellLocation RenderItem(SLDocument spreadsheet, int itemIndex, CellLocation cellLocation)
			=> itemIndex >= ValueItems.Length ? CellLocation.Empty() : RenderValues(spreadsheet, ValueItems[itemIndex].ViewValues, cellLocation);

		public abstract CellLocation RenderHeader(SLDocument spreadsheet, CellLocation cellLocation);

		protected abstract CellLocation RenderValues(SLDocument spreadsheet, IEnumerable<ViewValues> viewValues, CellLocation cellLocation);

		public abstract CellLocation RenderTotals(SLDocument spreadsheet, CellLocation cellLocation);
	}
}
