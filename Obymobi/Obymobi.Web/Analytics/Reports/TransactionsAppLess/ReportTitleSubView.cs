﻿using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public class ReportTitleSubView : VerticalReportSubView
	{
		public ReportTitleSubView(string title, string subTitle) : base(new EmptySubViewHeader(), CreateItems(title, subTitle))
		{
		}

		private static ViewItem[] CreateItems(string title, string subTitle) =>
			new[]
			{
				new ViewItem(new[]
				{
					new ViewValues(new IViewCell[]
					{
						new StringViewCell(title, System.Drawing.FontStyle.Bold, fontSize: 16d),
						new StringViewCell(subTitle, System.Drawing.FontStyle.Bold)
					})
				})
			};
	}
}
