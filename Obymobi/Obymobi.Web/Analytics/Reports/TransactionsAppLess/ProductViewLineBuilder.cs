﻿using Obymobi.Web.Analytics.Reports.ProductReport;
using Obymobi.Web.Analytics.Reports.ReportBase;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public class ProductViewLineBuilder : ViewLineBuilder
	{
		private readonly ProductSubView productSubView;

		public ProductViewLineBuilder(ProductSubView productSubView) => this.productSubView = productSubView;

		public CellLocation RenderProducts(SLDocument spreadsheet, CellLocation currentLocation, int index, CellLocation initialLocation, ref SpreadsheetRow maximumRow)
			=> RenderItem(spreadsheet, currentLocation, productSubView, index, initialLocation, ref maximumRow);
	}
}
