﻿using System;
using Obymobi.Web.Analytics.Reports.ReportBase;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public abstract class ViewLineBuilder
	{
		protected static CellLocation RenderItem(SLDocument spreadsheet, CellLocation currentLocation, ReportSubView subView, int index, CellLocation initialLocation, ref SpreadsheetRow maximumRow)
		{
			currentLocation = subView.RenderItem(spreadsheet, index, currentLocation);
			maximumRow = (SpreadsheetRow) Math.Max((int) maximumRow, (int) currentLocation.Row);
			return RestoreRow(currentLocation, initialLocation);
		}

		private static CellLocation RestoreRow(CellLocation location, CellLocation baseLocation) => new CellLocation(location.Column, baseLocation.Row);
	}
}
