using Dionysos;
using DocumentFormat.OpenXml.Spreadsheet;
using Obymobi.Web.Analytics.DomainTypes;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Reports.ReportBase;
using System;
using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
    public class OrderSubView : HorizontalReportSubView
    {
        public OrderSubView(Orders orders, TimeZoneInfo timeZoneInfo, Currency currency, ShowServiceMethod showServiceMethod, ShowCheckoutMethod showCheckoutMethod, ShowPaymentMethod showPaymentMethod, ShowCardSummary showCardSummary, ShowDeliveryPoint showDeliveryPoint, ShowCoversCount showCoversCount) : base(
            new OrderSubViewHeader(showServiceMethod, showCheckoutMethod, showPaymentMethod, showCardSummary, showDeliveryPoint, showCoversCount),
            CreateItems(orders, timeZoneInfo, currency, showServiceMethod, showCheckoutMethod, showPaymentMethod, showCardSummary, showDeliveryPoint, showCoversCount))
        {
        }

        private static ViewItem[] CreateItems(Orders orders, TimeZoneInfo timeZoneInfo, Currency currency, ShowServiceMethod showServiceMethod, ShowCheckoutMethod showCheckoutMethod, ShowPaymentMethod showPaymentMethod, ShowCardSummary showCardSummary, ShowDeliveryPoint showDeliveryPoint, ShowCoversCount showCoversCount)
        {
            var viewItems = new List<ViewItem>();

            foreach (var order in orders)
            {
                var viewCells = new List<IViewCell>();

                viewCells.Add(new DateTimeViewCell(ToLocalTime(order.OrderDate, timeZoneInfo)));
                viewCells.Add(new IdViewCell(order.OrderNumber));

                if (showServiceMethod)
                {
                    viewCells.Add(new StringViewCell(order.ServiceMethodName, HorizontalAlignmentValues.Center));
                }

                if (showCheckoutMethod)
                {
                    viewCells.Add(new StringViewCell(order.CheckoutMethodName, HorizontalAlignmentValues.Center));
                }

                if (showPaymentMethod)
                {
                    viewCells.Add(new StringViewCell(order.PaymentMethod, HorizontalAlignmentValues.Center));
                }

                if (showCardSummary)
                {
                    viewCells.Add(new StringViewCell(order.CardSummary, HorizontalAlignmentValues.Center));
                }

                if (showDeliveryPoint)
                {
                    viewCells.Add(new StringViewCell(order.DeliveryPoint, HorizontalAlignmentValues.Center));
                }

                if (showCoversCount)
                {
                    viewCells.Add(new StringViewCell(order.CoversCount, HorizontalAlignmentValues.Center));
                }

                viewCells.Add(new CurrencyViewCell(order.OrderTotal, currency));

                viewItems.Add(new ViewItem(new[] { new ViewValues(viewCells.ToArray()) }));
            }

            return viewItems.ToArray();
        }
        private static DateTime? ToLocalTime(DateTime? dateTime, TimeZoneInfo timeZoneInfo) => dateTime?.UtcToLocalTime(timeZoneInfo) ?? default;
    }
}
