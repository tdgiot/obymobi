﻿using Obymobi.Web.Analytics.Reports.ReportBase;

namespace Obymobi.Web.Analytics.Reports.TransactionsAppLess
{
	public class EmptySubViewHeader : ViewHeader
	{
		public EmptySubViewHeader() : base(new HeaderCell[0])
		{
		}
	}
}
