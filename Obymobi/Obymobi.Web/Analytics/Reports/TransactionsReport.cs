﻿using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Web.Analytics.Model.Transactions;
using System.Drawing;
using System.Globalization;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Requests;

namespace Obymobi.Web.Analytics.Reports
{
    public class TransactionsReport
    {
        private int duplicateWorksheets;

        private readonly TimeZoneInfo companyTimeZoneInfo;        
        private readonly ClockMode clockMode = ClockMode.Hour24;
        private readonly TransactionsRequest request;

        public TransactionsReport(TransactionsRequest request)
        {
            this.request = request;

            this.Transactions = new Obymobi.Web.Analytics.Model.Transactions.Transactions(request);
            this.Transactions.FetchAndProcessData();            
            CompanyEntity company = new CompanyEntity(request.CompanyId);
            if (!company.IsNew)
            {
                this.companyTimeZoneInfo = company.TimeZoneInfo;
                this.clockMode = company.ClockMode;
            }            
        }

        private Transactions Transactions { get; set; }

        public bool CreateReport(out SLDocument spreadsheet)
        {
            spreadsheet = new SLDocument();
            string summaryWorkSheetName = "Summary";

            // Prepare the Summary worksheet            
            int summaryRowIndex = 2; // (1 is already used below)
            spreadsheet.RenameWorksheet(SLDocument.DefaultFirstSheetName, summaryWorkSheetName);

            // Render the Deliverypointgroup part of the Summary worksheet            
            this.RenderSummaryWorksheet(spreadsheet, summaryWorkSheetName, ref summaryRowIndex);

            // Render Deliverypointgroups Worksheet
            this.RenderDeliverypointgroupsWorksheet(spreadsheet);

            // Render the Products worksheets
            this.RenderProductsWorksheet(spreadsheet, false);
            this.RenderProductsWorksheet(spreadsheet, true);

            // Render the Categories worksheets
            this.RenderCategoryAndProductsWorksheetPerMenu(spreadsheet);

            // Render orders
            this.RenderOrdersWorksheet(spreadsheet);
            
            // Scale 1x1 Page
            spreadsheet.SelectWorksheet(summaryWorkSheetName);
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 1);
            spreadsheet.SetPageSettings(pageSettings);

            // Set all print sizes to A4
            spreadsheet.SetPageSizeOfWorksheets(SLPaperSizeValues.A4Paper);

            return true;
        }

        private void RenderSummaryWorksheet(SLDocument spreadsheet, string worksheetName, ref int rowIndex)
        {
            // Headings: Left (Orders)
            spreadsheet.MergeWorksheetCells("A1", "C1");
            spreadsheet.SetCellValue("A1", "Orders");
            spreadsheet.SetStyle("A1", DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center, fontSize: 16);

            // Headings: Right (Service Request)
            spreadsheet.MergeWorksheetCells("F1", "H1");
            spreadsheet.SetCellValue("F1", "Service Requests");
            spreadsheet.SetStyle("F1", DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center, fontSize: 16);

            // Columns width
            spreadsheet.SetColumnWidth("A", 43.86);
            spreadsheet.SetColumnWidth("B", 10.86);
            spreadsheet.SetColumnWidth("C", 10.86);
            spreadsheet.SetColumnWidth("D", 2.29);
            spreadsheet.SetColumnWidth("E", 2.29);
            spreadsheet.SetColumnWidth("F", 43.86);
            spreadsheet.SetColumnWidth("G", 10.86);
            spreadsheet.SetColumnWidth("H", 10.86);

            spreadsheet.SelectWorksheet(worksheetName);

            // Normal Headings
            spreadsheet.SetCellValue("A" + rowIndex, "Deliverypointgroup");
            spreadsheet.SetCellValue("B" + rowIndex, "Orders");
            spreadsheet.SetCellValue("c" + rowIndex, "Revenue");
            spreadsheet.SetCellValue("F" + rowIndex, "Deliverypointgroup");
            spreadsheet.SetCellValue("G" + rowIndex, "Orders");
            spreadsheet.SetCellValue("H" + rowIndex, "Revenue");
            spreadsheet.SetStyle("A" + rowIndex, "C" + rowIndex, backgroundColor: SpreadsheetHelper.Heading1);
            spreadsheet.SetStyle("F" + rowIndex, "H" + rowIndex, backgroundColor: SpreadsheetHelper.Heading1);
            rowIndex++;

            // Render per deliverypointgroup
            foreach (Deliverypointgroup deliverypointgroup in this.Transactions.Deliverypointgroups.Values)
            {
                // Products / Orders                
                spreadsheet.SetCellValue("A" + rowIndex, deliverypointgroup.Name);
                spreadsheet.SetCellValue("B" + rowIndex, deliverypointgroup.OrderCountStandardOrder);
                spreadsheet.SetCellValue("C" + rowIndex, deliverypointgroup.OrderRevenueStandardOrder);
                spreadsheet.SetStyle("B" + rowIndex, formatCode: SpreadsheetHelper.IntegerFormatCode);
                spreadsheet.SetStyle("C" + rowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode);
                spreadsheet.SetStyle("A" + rowIndex, "C" + rowIndex, borders: Borders.Bottom, borderColor: SpreadsheetHelper.DividingBorderColor);

                // Service Requests
                spreadsheet.SetCellValue("F" + rowIndex, deliverypointgroup.Name);
                spreadsheet.SetCellValue("G" + rowIndex, deliverypointgroup.OrderCountRequestForService);
                spreadsheet.SetCellValue("H" + rowIndex, deliverypointgroup.OrderRevenueRequestForService);
                spreadsheet.SetStyle("G" + rowIndex, formatCode: SpreadsheetHelper.IntegerFormatCode);
                spreadsheet.SetStyle("H" + rowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode);

                // Border bottom
                spreadsheet.SetStyle("F" + rowIndex, "H" + rowIndex, borders: Borders.Bottom, borderColor: SpreadsheetHelper.DividingBorderColor);

                rowIndex++;
            }

            // Products / Orders                
            //spreadsheet.SetCellValue("A" + rowIndex, "Total");
            spreadsheet.SetCellValue("B" + rowIndex, this.Transactions.OrderCountStandardOrder);
            spreadsheet.SetCellValue("c" + rowIndex, this.Transactions.OrderRevenueStandardOrder);
            spreadsheet.SetStyle("B" + rowIndex, formatCode: SpreadsheetHelper.IntegerFormatCode, borders: Borders.Top);
            spreadsheet.SetStyle("C" + rowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode, borders: Borders.Top);

            // Service Requests
            //spreadsheet.SetCellValue("F" + rowIndex, "Total");
            spreadsheet.SetCellValue("G" + rowIndex, this.Transactions.OrderCountRequestForService);
            spreadsheet.SetCellValue("H" + rowIndex, this.Transactions.OrderRevenueRequestForService);
            spreadsheet.SetStyle("G" + rowIndex, formatCode: SpreadsheetHelper.IntegerFormatCode, borders: Borders.Top);
            spreadsheet.SetStyle("H" + rowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode, borders: Borders.Top);

            rowIndex++;

            // Spacing
            rowIndex++;

            int topX = 10;
            int startRowIndex = rowIndex;

            List<string> columnsStandard = new List<string>() { "A", "B", "C" };
            List<string> columnsRequests = new List<string>() { "F", "G", "H" };
            int currentRowIndex = startRowIndex;

            // Standard: Render Product Top 10's by Quantity
            this.RenderSummaryTable(spreadsheet, "Top 10 Products ordered by Quantity", columnsStandard, this.Transactions.TopProductsFromStandardOrdersByQuantity, null, topX, currentRowIndex);

            // Service Requests: Render Product Top 10's by Quantity            
            this.RenderSummaryTable(spreadsheet, "Top 10 Products ordered by Quantity", columnsRequests, this.Transactions.TopProductsFromRequestsForServiceByQuantity, null, topX, currentRowIndex);

            currentRowIndex += 13;

            // Standard: Render Category Top 10's by Quantity           
            this.RenderSummaryTable(spreadsheet, "Top 10 Categories ordered by Quantity", columnsStandard, null, this.Transactions.TopCategoriesFromStandardOrdersByQuantity, topX, currentRowIndex);

            // Service Requests: Category Top 10's by Quantity            
            this.RenderSummaryTable(spreadsheet, "Top 10 Categories ordered by Quantity", columnsRequests, null, this.Transactions.TopCategoriesFromRequestsForServiceByQuantity, topX, currentRowIndex);

            currentRowIndex += 13;

            // Standard: Render Product Top 10's by Revenue
            this.RenderSummaryTable(spreadsheet, "Top 10 Products ordered by Revenue", columnsStandard, this.Transactions.TopProductsFromStandardOrdersByRevenue, null, topX, currentRowIndex);

            // Service Requests: Render Product Top 10's by Revenue
            this.RenderSummaryTable(spreadsheet, "Top 10 Products ordered by Revenue", columnsRequests, this.Transactions.TopProductsFromRequestForServiceByRevenue, null, topX, currentRowIndex);

            currentRowIndex += 13;

            // Standard: Render Category Top 10's by Revenue
            this.RenderSummaryTable(spreadsheet, "Top 10 Categories ordered by Revenue", columnsStandard, null, this.Transactions.TopCategoriesFromStandardOrdersByRevenue, topX, currentRowIndex);

            // Service Requests: Category Top 10's by Quantity            
            this.RenderSummaryTable(spreadsheet, "Top 10 Categories ordered by Revenue", columnsRequests, null, this.Transactions.TopCategoriesFromRequestsForServiceByRevenue, topX, currentRowIndex);

            // Fixed amount of Rows: 4 x (Header + Header + topX + spacing)
            rowIndex += 4 * (1 + 1 + topX + 1);

            // Draw dividing line in middle of summary
            spreadsheet.SetStyle("E1", "E" + rowIndex, borders: Borders.Left);
        }

        private void RenderSummaryTable(SLDocument spreadsheet, string title, List<string> columns, List<Product> products, List<Category> categories, int rowsToPrint, int currentRowIndex)
        {
            // Header for table
            spreadsheet.MergeWorksheetCells(columns[0] + currentRowIndex, columns[2] + currentRowIndex);
            spreadsheet.SetCellValue(columns[0] + currentRowIndex, title);
            spreadsheet.SetStyle(columns[0] + currentRowIndex, horizontalAlignment: DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);
            currentRowIndex++;

            // Column headings for table
            spreadsheet.SetCellValue(columns[0] + currentRowIndex, "Name");
            spreadsheet.SetCellValue(columns[1] + currentRowIndex, "Quantity");
            spreadsheet.SetCellValue(columns[2] + currentRowIndex, "Revenue");
            spreadsheet.SetStyle(columns[0] + currentRowIndex, columns[2] + currentRowIndex, backgroundColor: SpreadsheetHelper.Heading1);
            spreadsheet.SetStyle(columns[1] + currentRowIndex, columns[2] + currentRowIndex, DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Right);
            currentRowIndex++;

            // Render the rows to be printed
            // We need to print as many rows as we need to (even if we wouldn't have the amount of rows) to render
            // the lines and keep currentRowIndex in sync.
            for (int i = 0; i < rowsToPrint; i++)
            {
                if (products != null && i < products.Count)
                {
                    // Got row
                    Product product = products[i];
                    spreadsheet.SetCellValue(columns[0] + currentRowIndex, !product.ProductId.HasValue ? string.Format("{0} (Product has been deleted)", product.Name) : product.Name);
                    spreadsheet.SetCellValue(columns[1] + currentRowIndex, product.Quantity);
                    spreadsheet.SetCellValue(columns[2] + currentRowIndex, product.Revenue);
                    spreadsheet.SetStyle(columns[1] + currentRowIndex, formatCode: SpreadsheetHelper.IntegerFormatCode);
                    spreadsheet.SetStyle(columns[2] + currentRowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode);
                }

                if (categories != null && i < categories.Count)
                {
                    // Got row
                    Category category = categories[i];
                    //if (!category.CategoryId.HasValue)
                    //{
                    //spreadsheet.SetCellValue(columns[0] + currentRowIndex, string.Format("{0} (Product has been deleted)", product.Name));
                    //}
                    //else
                    //{
                    spreadsheet.SetCellValue(columns[0] + currentRowIndex, category.Name);
                    //}
                    spreadsheet.SetCellValue(columns[1] + currentRowIndex, category.Quantity);
                    spreadsheet.SetCellValue(columns[2] + currentRowIndex, category.Revenue);
                    spreadsheet.SetStyle(columns[1] + currentRowIndex, formatCode: SpreadsheetHelper.IntegerFormatCode);
                    spreadsheet.SetStyle(columns[2] + currentRowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode);
                }

                // Border bottom
                spreadsheet.SetStyle(columns[0] + currentRowIndex, columns[2] + currentRowIndex, borders: Borders.Bottom, borderColor: SpreadsheetHelper.DividingBorderColor);

                currentRowIndex++;
            }
        }

        private void RenderDeliverypointgroupsWorksheet(SLDocument spreadsheet)
        {
            // Add & select worksheet
            string worksheetName = "Deliverypointgroups";
            spreadsheet.AddWorksheet(worksheetName);
            spreadsheet.SelectWorksheet(worksheetName);

            Color headerColor = Color.FromArgb(216, 228, 188);

            // Column A and B are used for Row Naming

            // Render High-level headings in merged cells
            spreadsheet.MergeWorksheetCells("C1", "E1"); // IRT
            spreadsheet.MergeWorksheetCells("F1", "H1"); // BYOD
            spreadsheet.MergeWorksheetCells("I1", "K1"); // TOTAL

            // Set Texts
            spreadsheet.SetCellValue("C1", "In-Room Tablet");
            spreadsheet.SetCellValue("F1", "Bring Your Own Device");
            spreadsheet.SetCellValue("I1", "Overall");

            // Render headings IRT
            spreadsheet.SetCellValue("C2", "Orders");
            spreadsheet.SetCellValue("D2", "Service Requests");
            spreadsheet.SetCellValue("E2", "Total");

            // Render headings BYOD
            spreadsheet.SetCellValue("F2", "Orders");
            spreadsheet.SetCellValue("G2", "Service Requests");
            spreadsheet.SetCellValue("H2", "Total");

            // Render headings Overall
            spreadsheet.SetCellValue("I2", "Orders");
            spreadsheet.SetCellValue("J2", "Service Requests");
            spreadsheet.SetCellValue("K2", "Total");
            spreadsheet.SetColumnWidth(3, 16);
            spreadsheet.SetColumnWidth(4, 16);
            spreadsheet.SetColumnWidth(5, 16);
            spreadsheet.SetColumnWidth(6, 16);
            spreadsheet.SetColumnWidth(7, 16);
            spreadsheet.SetColumnWidth(8, 16);
            spreadsheet.SetColumnWidth(9, 16);
            spreadsheet.SetColumnWidth(10, 16);
            spreadsheet.SetColumnWidth(11, 16);
            spreadsheet.SetStyle("C1", "K2", DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center, backgroundColor: headerColor);

            // Render the Rows per Deliverypointgroup (which we expect to be ordered - since that's how the DataTable is filled)
            int currentRow = 3; // First two rows are the headers  

            List<int> rowsToGiveBottomBorder = new List<int>();
            foreach (Deliverypointgroup deliverypointgroup in this.Transactions.Deliverypointgroups.Values)
            {
                // We render 2 rows per Deliverypointgroup: Order Quantities and Revenue
                // Create a rowspan merge for the DPG name
                spreadsheet.MergeWorksheetCells("A" + currentRow, "A" + (currentRow + 1));
                spreadsheet.SetCellValue("A" + currentRow, deliverypointgroup.Name);
                spreadsheet.SetStyle("A" + currentRow, verticalAlignment: DocumentFormat.OpenXml.Spreadsheet.VerticalAlignmentValues.Center, backgroundColor: headerColor);
                rowsToGiveBottomBorder.Add(currentRow + 1);

                // Render Order Quanties Row
                spreadsheet.SetCellValue("B" + currentRow, "Quantity");
                spreadsheet.SetCellValue("C" + currentRow, deliverypointgroup.OrderCountStandardOrderInRoomTablet);
                spreadsheet.SetCellValue("D" + currentRow, deliverypointgroup.OrderCountRequestForServiceInRoomTablet);
                spreadsheet.SetCellValue("E" + currentRow, deliverypointgroup.OrderCountInRoomTablet);
                spreadsheet.SetCellValue("F" + currentRow, deliverypointgroup.OrderCountStandardOrderByod);
                spreadsheet.SetCellValue("G" + currentRow, deliverypointgroup.OrderCountRequestForServiceByod);
                spreadsheet.SetCellValue("H" + currentRow, deliverypointgroup.OrderCountByod);
                spreadsheet.SetCellValue("I" + currentRow, deliverypointgroup.OrderCountStandardOrder);
                spreadsheet.SetCellValue("J" + currentRow, deliverypointgroup.OrderCountRequestForService);
                spreadsheet.SetCellValue("K" + currentRow, deliverypointgroup.OrderCount);
                spreadsheet.SetStyle("B" + currentRow, "K" + currentRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green4, formatCode: SpreadsheetHelper.IntegerFormatCode);
                currentRow++;

                // Render Revenue Row
                spreadsheet.SetCellValue("B" + currentRow, "Revenue");
                spreadsheet.SetCellValue("C" + currentRow, deliverypointgroup.OrderRevenueStandardOrderInRoomTablet);
                spreadsheet.SetCellValue("D" + currentRow, deliverypointgroup.OrderRevenueRequestForServiceInRoomTablet);
                spreadsheet.SetCellValue("E" + currentRow, deliverypointgroup.OrderRevenueInRoomTablet);
                spreadsheet.SetCellValue("F" + currentRow, deliverypointgroup.OrderRevenueStandardOrderByod);
                spreadsheet.SetCellValue("G" + currentRow, deliverypointgroup.OrderRevenueRequestForServiceByod);
                spreadsheet.SetCellValue("H" + currentRow, deliverypointgroup.OrderRevenueByod);
                spreadsheet.SetCellValue("I" + currentRow, deliverypointgroup.OrderRevenueStandardOrder);
                spreadsheet.SetCellValue("J" + currentRow, deliverypointgroup.OrderRevenueRequestForService);
                spreadsheet.SetCellValue("K" + currentRow, deliverypointgroup.OrderRevenue);
                spreadsheet.SetStyle("B" + currentRow, "K" + currentRow, borders: Borders.Bottom, formatCode: SpreadsheetHelper.DecimalFormatCode);
                currentRow++;
            }

            // GK Failed for some reason
            // spreadsheet.AutoFitColumn("A");
            spreadsheet.SetColumnWidth("A", 30);

            // Render the total Rows (Quantity and Revenue)
            // Create a rowspan merge for the 'Total'-label
            spreadsheet.MergeWorksheetCells("A" + currentRow, "A" + (currentRow + 1));
            spreadsheet.SetCellValue("A" + currentRow, "Total");
            spreadsheet.SetStyle("A" + currentRow, verticalAlignment: DocumentFormat.OpenXml.Spreadsheet.VerticalAlignmentValues.Center, backgroundColor: headerColor);

            // Quantity Row
            spreadsheet.SetCellValue("B" + currentRow, "Quantity");
            spreadsheet.SetCellValue("C" + currentRow, this.Transactions.OrderCountStandardOrderInRoomTablet);
            spreadsheet.SetCellValue("D" + currentRow, this.Transactions.OrderCountRequestForServiceInRoomTablet);
            spreadsheet.SetCellValue("E" + currentRow, this.Transactions.OrderCountInRoomTablet);
            spreadsheet.SetCellValue("F" + currentRow, this.Transactions.OrderCountStandardOrderByod);
            spreadsheet.SetCellValue("G" + currentRow, this.Transactions.OrderCountRequestForServiceByod);
            spreadsheet.SetCellValue("H" + currentRow, this.Transactions.OrderCountByod);
            spreadsheet.SetCellValue("I" + currentRow, this.Transactions.OrderCountStandardOrder);
            spreadsheet.SetCellValue("J" + currentRow, this.Transactions.OrderCountRequestForService);
            spreadsheet.SetCellValue("K" + currentRow, this.Transactions.OrderCount);
            currentRow++;

            // Revenue Row
            spreadsheet.SetCellValue("B" + currentRow, "Revenue");
            spreadsheet.SetCellValue("C" + currentRow, this.Transactions.OrderRevenueStandardOrderInRoomTablet);
            spreadsheet.SetCellValue("D" + currentRow, this.Transactions.OrderRevenueRequestForServiceInRoomTablet);
            spreadsheet.SetCellValue("E" + currentRow, this.Transactions.OrderRevenueInRoomTablet);
            spreadsheet.SetCellValue("F" + currentRow, this.Transactions.OrderRevenueStandardOrderByod);
            spreadsheet.SetCellValue("G" + currentRow, this.Transactions.OrderRevenueRequestForServiceByod);
            spreadsheet.SetCellValue("H" + currentRow, this.Transactions.OrderRevenueByod);
            spreadsheet.SetCellValue("I" + currentRow, this.Transactions.OrderRevenueStandardOrder);
            spreadsheet.SetCellValue("J" + currentRow, this.Transactions.OrderRevenueRequestForService);
            spreadsheet.SetCellValue("K" + currentRow, this.Transactions.OrderRevenue);
            spreadsheet.SetStyle("B" + currentRow, "K" + currentRow, borders: Borders.Bottom, formatCode: SpreadsheetHelper.DecimalFormatCode);

            // Set Borders (at the end, since otherwise autofit column won't work)
            spreadsheet.SetBorder(Color.Black, Borders.Top, 1, 11, 1, 1);
            spreadsheet.SetBorder(Color.Black, Borders.Bottom, 1, 11, 2, 2);
            spreadsheet.SetBorder(Color.Black, Borders.Left, 1, 1, 1, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 2, 2, 1, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 3, 3, 3, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 4, 4, 3, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 5, 5, 1, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 6, 6, 3, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 7, 7, 3, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 8, 8, 1, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 9, 9, 3, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 10, 10, 3, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Right, 11, 11, 1, currentRow);
            spreadsheet.SetBorder(Color.Black, Borders.Bottom, 2, 11, currentRow, currentRow);
            rowsToGiveBottomBorder.Add(currentRow);

            // Update all borders in one iteration
            foreach (int row in rowsToGiveBottomBorder) spreadsheet.SetBorder(Color.Black, Borders.Bottom, 1, 1, row, row);

            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 1);
            pageSettings.Orientation = DocumentFormat.OpenXml.Spreadsheet.OrientationValues.Landscape;
            spreadsheet.SetPageSettings(pageSettings);

            // Top left cell, remove borders
            SLStyle style = spreadsheet.GetCellStyle("A1");
            style.Border.LeftBorder.BorderStyle = DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.None;
            style.Border.TopBorder.BorderStyle = DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.None;
            spreadsheet.SetCellStyle("A1", style);
            style = spreadsheet.GetCellStyle("B1");
            style.Border.TopBorder.BorderStyle = DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.None;
            spreadsheet.SetCellStyle("B1", style);
            style = spreadsheet.GetCellStyle("A2");
            style.Border.LeftBorder.BorderStyle = DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.None;
            spreadsheet.SetCellStyle("A2", style);

        }

        private void RenderProductsWorksheet(SLDocument spreadsheet, bool groupByCategory)
        {
            string worksheetName = groupByCategory ? "Products & Categories" : "Products";

            // Create and select worksheet
            spreadsheet.AddWorksheet(worksheetName);
            spreadsheet.SelectWorksheet(worksheetName);

            // Set Headings + styling
            spreadsheet.SetCellValue("A1", "Sort Order");
            spreadsheet.SetCellValue("B1", groupByCategory ? "Product / Category" : "Product");
            spreadsheet.SetCellValue("C1", "Product");
            spreadsheet.SetCellValue("D1", "Category");
            spreadsheet.SetCellValue("E1", "Quantity");
            spreadsheet.SetCellValue("F1", "Revenue");
            spreadsheet.SetStyle("A1", "F1", borders: Borders.Bottom, backgroundColor: SpreadsheetHelper.Green14);
            spreadsheet.SetStyle("E1", "F1", DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Right);

            // ProductIds sorted by Quantity
            List<Product> productsSortedByQuantity = this.Transactions.Products.Values.OrderByDescending(x => x.Quantity).ToList();

            // Show all products which are not deleted
            int currentRow = 2;
            foreach (Product product in productsSortedByQuantity)
            {
                this.RenderRowForProduct(spreadsheet, product, ref currentRow, groupByCategory);
            }

            // Set column widths
            spreadsheet.SetColumnWidth("B", 76);
            spreadsheet.SetColumnWidth("E", 8.5);
            spreadsheet.SetColumnWidth("F", 11);

            // Hide: Columns Sort Order (A), Category (C), Product (D)
            spreadsheet.HideColumn("A");
            spreadsheet.HideColumn("C");
            spreadsheet.HideColumn("D");

            // Keep first line frozen
            spreadsheet.FreezePanes(1, 0);

            // Sort
            if (!groupByCategory) spreadsheet.Sort("A2", "F" + currentRow, "E", false);

            // Set printing to 1 page width
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 500);
            spreadsheet.SetPageSettings(pageSettings);
        }

        private void RenderRowForProduct(SLDocument spreadsheet, Product product, ref int currentRow, bool groupByCategory)
        {
            int productRow = currentRow;
            currentRow++;

            // Columns: Sort Order, Product/Category, Product, Category, Quantity, Reveneu
            spreadsheet.SetCellValue("A" + productRow, currentRow);
            spreadsheet.SetCellValue("B" + productRow, product.Name);
            spreadsheet.SetCellValue("C" + productRow, product.Name);
            spreadsheet.SetCellValue("D" + productRow, string.Empty);

            Color categoryBorderColor = SpreadsheetHelper.Green4;

            if (groupByCategory)
            {
                if (product.ProductId.HasValue)
                {
                    // Each Product can appear in multiple rows if it's orderable from different Categories
                    foreach (Category category in product.Categories.Values)
                    {
                        spreadsheet.SetCellValue("A" + currentRow, currentRow);
                        spreadsheet.SetCellValue("B" + currentRow, "".PadLeft(4, ' ') + category.Path);
                        spreadsheet.SetCellValue("C" + currentRow, string.Empty);
                        spreadsheet.SetCellValue("D" + currentRow, category.Path);
                        spreadsheet.SetCellValue("E" + currentRow, category.Products.Values.Single(x => x.Key == product.ProductId.ToString()).Quantity);
                        spreadsheet.SetCellValue("F" + currentRow, category.Products.Values.Single(x => x.Key == product.ProductId.ToString()).Revenue);
                        spreadsheet.SetStyle("A" + currentRow, "F" + currentRow, borders: Borders.Bottom, borderColor: categoryBorderColor);
                        spreadsheet.SetStyle("F" + currentRow, formatCode: SpreadsheetHelper.DecimalFormatCode);
                        currentRow++;
                    }
                }
                else
                {
                    spreadsheet.SetCellValue("A" + currentRow, currentRow);
                    spreadsheet.SetCellValue("B" + currentRow, "".PadLeft(4, ' ') + "(Product has been deleted)");
                    spreadsheet.SetCellValue("C" + currentRow, string.Empty);
                    spreadsheet.SetCellValue("D" + currentRow, "(Product has been deleted)");
                    spreadsheet.SetCellValue("E" + currentRow, product.Quantity);
                    spreadsheet.SetCellValue("F" + currentRow, product.Revenue);
                    spreadsheet.SetStyle("A" + currentRow, "F" + currentRow, borders: Borders.Bottom, borderColor: categoryBorderColor);
                    spreadsheet.SetStyle("F" + currentRow, formatCode: SpreadsheetHelper.DecimalFormatCode);
                    currentRow++;
                }
            }

            spreadsheet.SetCellValue("E" + productRow, product.Quantity);
            spreadsheet.SetCellValue("F" + productRow, product.Revenue);

            // Set color to grey-ish for deleted products
            Color? textColor = null;
            if (!product.ProductId.HasValue) textColor = Color.FromArgb(128, 128, 128);

            // Only background when including categories, otherwise borders
            if (groupByCategory)
            {
                spreadsheet.SetStyle("A" + productRow, "F" + productRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green15, backgroundColor: SpreadsheetHelper.Green10, textColor: textColor);
            }
            else
            {
                spreadsheet.SetStyle("A" + productRow, "F" + productRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green4);
            }
            spreadsheet.SetStyle("F" + productRow, formatCode: SpreadsheetHelper.DecimalFormatCode);
        }        

        private void RenderRowForOrder(SLDocument spreadsheet, Order order, ref int currentRow)
        {
            int productRow = currentRow;
            currentRow++;

            string orderIdentifier = order.OrderId.ToString();
            if (order.CreatedUtc != null)
            {
                DateTime created = order.CreatedUtc.Value.UtcToLocalTime(this.companyTimeZoneInfo);

                string createdStr = string.Empty;
                if (this.clockMode == ClockMode.Hour24)
                {
                    createdStr = string.Format("{0:yyyy-MM-dd HH:mm:ss}", created);
                }
                else if (this.clockMode == ClockMode.AmPm)
                {
                    createdStr = created.ToString("yyyy-MM-dd hh:mm:ss tt", CultureInfo.InvariantCulture);
                }

                orderIdentifier = string.Format("{0} (ID: {1})", createdStr, order.OrderId);
            }

            // Columns: Sort Order, Order/Product, Order, Product, Acknowledged In, Deliverypoint, Quantity, Revenue
            spreadsheet.SetCellValue("A" + productRow, currentRow);
            spreadsheet.SetCellValue("B" + productRow, orderIdentifier);
            spreadsheet.SetCellValue("C" + productRow, orderIdentifier);
            spreadsheet.SetCellValue("D" + productRow, string.Empty);
            spreadsheet.SetCellValue("E" + productRow, order.NumberOfGuests);
            spreadsheet.SetCellValue("F" + productRow, order.BeingHandledIn.HasValue ? order.BeingHandledIn.Value.ToString(@"hh\:mm\:ss") : "");
            spreadsheet.SetCellValue("G" + productRow, order.DeliverypointName);

            Color categoryBorderColor = SpreadsheetHelper.Green4;

            if (this.Transactions.IncludeOrderitems)
            {
                // Each Product can appear in multiple rows if it's orderable from different Categories
                foreach (Orderitem orderitem in order.Orderitems.Values)
                {
                    spreadsheet.SetCellValue("A" + currentRow, currentRow);
                    spreadsheet.SetCellValue("B" + currentRow, "".PadLeft(4, ' ') + orderitem.ProductName);
                    spreadsheet.SetCellValue("C" + currentRow, string.Empty);
                    spreadsheet.SetCellValue("D" + currentRow, orderitem.ProductName);
                    spreadsheet.SetCellValue("E" + currentRow, string.Empty);
                    spreadsheet.SetCellValue("F" + currentRow, string.Empty);
                    spreadsheet.SetCellValue("G" + currentRow, string.Empty);
                    spreadsheet.SetCellValue("H" + currentRow, orderitem.Quantity);
                    spreadsheet.SetCellValue("I" + currentRow, orderitem.Revenue);
                    spreadsheet.SetCellValue("J" + currentRow, string.Empty);
                    spreadsheet.SetStyle("A" + currentRow, "J" + currentRow, borders: Borders.Bottom, borderColor: categoryBorderColor);
                    spreadsheet.SetStyle("E" + currentRow, "H" + currentRow, DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);
                    spreadsheet.SetStyle("I" + currentRow, "J" + currentRow, formatCode: SpreadsheetHelper.DecimalFormatCode);
                    currentRow++;
                }
            }

            spreadsheet.SetCellValue("H" + productRow, string.Empty);
            spreadsheet.SetCellValue("I" + productRow, string.Empty);
            spreadsheet.SetCellValue("J" + productRow, order.Revenue);

            // Set color to grey-ish for deleted products
            Color? textColor = null;

            // Only background when including categories, otherwise borders
            spreadsheet.SetStyle("A" + productRow, "J" + productRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green15, backgroundColor: SpreadsheetHelper.Green10, textColor: textColor);
            spreadsheet.SetStyle("E" + productRow, "H" + productRow, DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);
            spreadsheet.SetStyle("I" + productRow, formatCode: SpreadsheetHelper.DecimalFormatCode);
            spreadsheet.SetStyle("J" + productRow, formatCode: SpreadsheetHelper.DecimalFormatCode);
        }

        private void RenderOrdersWorksheet(SLDocument spreadsheet)
        {
            // Create and select worksheet
            spreadsheet.AddWorksheet("Orders");
            spreadsheet.SelectWorksheet("Orders");

            // Set Headings + styling
            spreadsheet.SetCellValue("A1", "Sort Order");
            spreadsheet.SetCellValue("B1", "Order / Product");
            spreadsheet.SetCellValue("C1", "Order");
            spreadsheet.SetCellValue("D1", "Product");
            spreadsheet.SetCellValue("E1", "Number of Guests");
            spreadsheet.SetCellValue("F1", "On-The-Case");
            spreadsheet.SetCellValue("G1", "Deliverypoint");
            spreadsheet.SetCellValue("H1", "Quantity");
            spreadsheet.SetCellValue("I1", "Revenue");
            spreadsheet.SetCellValue("J1", "Total");
            spreadsheet.SetStyle("A1", "J1", borders: Borders.Bottom, backgroundColor: SpreadsheetHelper.Green14);
            spreadsheet.SetStyle("E1", "H1", DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);
            spreadsheet.SetStyle("I1", "J1", DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Right);

            // OrderIds sorted by Created
            List<Order> ordersSortedByCreated = this.Transactions.Orders.Values.OrderByDescending(x => x.CreatedUtc).ToList();

            // Show all orders
            int currentRow = 2;
            foreach (Order order in ordersSortedByCreated)
            {
                this.RenderRowForOrder(spreadsheet, order, ref currentRow);
            }

            // Set column widths
            spreadsheet.SetColumnWidth("B", 76);
            spreadsheet.SetColumnWidth("E", 17);
            spreadsheet.SetColumnWidth("F", 16);
            spreadsheet.SetColumnWidth("G", 14);
            spreadsheet.SetColumnWidth("H", 10);
            spreadsheet.SetColumnWidth("I", 11);
            spreadsheet.SetColumnWidth("J", 11);

            // Hide: Columns Sort Order (A), Category (C), Product (D)
            spreadsheet.HideColumn("A");
            spreadsheet.HideColumn("C");
            spreadsheet.HideColumn("D");

            if (this.request.CompanyId != 343)
            {
                // We have a special column for Aria 'Number of Guests'
                // Hide this column for every other company since it'll be empty anyway
                spreadsheet.HideColumn("E");
            }

            // Keep first line frozen
            spreadsheet.FreezePanes(1, 0);

            // Set printing to 1 page width
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 500);
            spreadsheet.SetPageSettings(pageSettings);
        }

        public void RenderCategoryAndProductsWorksheetPerMenu(SLDocument spreadsheet)
        {
            this.duplicateWorksheets = 0;
            foreach (Menu menu in this.Transactions.Menus.Values)
            {
                if (menu.MenuId > 0)
                {
                    Menu menu2 = menu.Deliverypointgroup.Menus.Values.SingleOrDefault(x => x.MenuId == -1);

                    // Render the requested version, i.e. 'Category' and/or 'Category and Product' overview
                    this.RenderCategoryAndProductWorksheetForMenu(spreadsheet, menu, menu2, false);
                    this.RenderCategoryAndProductWorksheetForMenu(spreadsheet, menu, menu2, true);
                }
            }
        }

        private void RenderCategoryAndProductWorksheetForMenu(SLDocument spreadsheet, Menu menu, Menu menu2, bool renderProducts)
        {
            // Prepare worksheet name (name is max 31 chars -/- 6 for ' - C&P')
            string worksheetSubName = menu.Name;
            if (worksheetSubName.Length > 24)
            {
                worksheetSubName = worksheetSubName.Substring(0, 24);
            }
            string worksheetName = worksheetSubName + (renderProducts ? " - C&P" : " - C");
            if (spreadsheet.GetWorksheetNames().Contains(worksheetName))
            {
                this.duplicateWorksheets++;
                worksheetName = string.Format("{0}{1}{2}", worksheetSubName, this.duplicateWorksheets, (renderProducts ? " - C&P" : " - C"));
            }

            // Create and select worksheet
            spreadsheet.AddWorksheet(worksheetName);
            spreadsheet.SelectWorksheet(worksheetName);

            // Set Headers + Styling 
            spreadsheet.SetCellValue("A1", "Sort Order");
            spreadsheet.SetCellValue("B1", renderProducts ? "Category / Product" : "Category");
            spreadsheet.SetCellValue("C1", "Category");
            spreadsheet.SetCellValue("D1", "Product");
            spreadsheet.SetCellValue("E1", "Quantity");
            spreadsheet.SetCellValue("F1", "Revenue");
            spreadsheet.SetStyle("A1", "F1", borders: Borders.Bottom, backgroundColor: SpreadsheetHelper.Green14);
            spreadsheet.SetStyle("E1", "F1", DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Right);

            // Recursively add Categories & Products
            int row = 2;
            int indent = 0;

            foreach (Category category in menu.Categories.Values)
            {
                this.RenderRowsToCategoryAndProductWorksheet(spreadsheet, category, renderProducts, ref row, ref indent);
            }

            if (menu2 != null)
            {
                foreach (Category category in menu2.Categories.Values)
                {
                    this.RenderRowsToCategoryAndProductWorksheet(spreadsheet, category, renderProducts, ref row, ref indent);
                }                
            }

            // Make first row filter row
            spreadsheet.SetColumnWidth("B", 76);
            spreadsheet.SetColumnWidth("E", 8.5);
            spreadsheet.SetColumnWidth("F", 11);

            // Hide: Columns Sort Order (A), Category (C), Product (D)
            spreadsheet.HideColumn("A");
            spreadsheet.HideColumn("C");
            spreadsheet.HideColumn("D");

            // Keep first line frozen
            spreadsheet.FreezePanes(1, 0);

            // Set printing to 1 page width
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 45);
            spreadsheet.SetPageSettings(pageSettings);
        }

        private void RenderRowsToCategoryAndProductWorksheet(SLDocument spreadsheet, Category category, bool renderProducts, ref int row, ref int indent)
        {
            this.RenderRowToCategoryAndProductWorksheet(spreadsheet, indent, string.Empty, category.Name, category.Quantity, category.Revenue, row);
            row++;

            if (category.Categories.Count > 0)
            {
                indent++;

                foreach (Category childCategory in category.Categories.Values)
                {
                    this.RenderRowsToCategoryAndProductWorksheet(spreadsheet, childCategory, renderProducts, ref row, ref indent);
                }

                indent--;
            }

            if (renderProducts && category.Products.Count > 0)
            {
                indent++;

                foreach (Product product in category.Products.Values)
                {
                    this.RenderRowToCategoryAndProductWorksheet(spreadsheet, indent, product.Name, string.Empty, product.Quantity, product.Revenue, row);
                    row++;
                }

                indent--;
            }
        }

        private void RenderRowToCategoryAndProductWorksheet(SLDocument spreadsheet, int indent, string productName, string categoryName, int quantity, decimal revenue, int rowIndex)
        {
            if (!productName.IsNullOrWhiteSpace() && !categoryName.IsNullOrWhiteSpace())
            {
                throw new ArgumentException("ProductName and CategoryName can't be used at the same time, pick one, choose wisely...");
            }

            spreadsheet.SetCellValue("A" + rowIndex, rowIndex);
            spreadsheet.SetCellValue("B" + rowIndex, "".PadLeft((indent * 4) + 1, ' ') + categoryName + productName); // Can be done, because only 1 should be set.
            spreadsheet.SetCellValue("C" + rowIndex, categoryName);
            spreadsheet.SetCellValue("D" + rowIndex, productName);
            spreadsheet.SetCellValue("E" + rowIndex, quantity);
            spreadsheet.SetCellValue("F" + rowIndex, revenue);

            // Styling dependent on the Row Type
            if (!categoryName.IsNullOrWhiteSpace())
            {
                spreadsheet.SetCellBackground(indent, true, "A" + rowIndex, "F" + rowIndex);
                spreadsheet.SetStyle("A" + rowIndex, "F" + rowIndex, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green15);
            }
            else
            {
                spreadsheet.SetStyle("A" + rowIndex, "F" + rowIndex, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green4);
            }

            spreadsheet.SetStyle("F" + rowIndex, formatCode: SpreadsheetHelper.DecimalFormatCode);
        }
    }
}
