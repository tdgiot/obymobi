﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	[DebuggerDisplay("Width = {Width}")]
	public readonly struct Column
	{
		public Column(double width) => Width = width;

		public double Width { get; }
	}
}