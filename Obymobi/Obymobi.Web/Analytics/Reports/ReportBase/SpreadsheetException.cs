﻿using System;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public class SpreadsheetException : Exception
	{
		public SpreadsheetException()
		{
		}

		public SpreadsheetException(string message)
			: base(message)
		{
		}

		public SpreadsheetException(string message, Exception inner)
			: base(message, inner)
		{
		}

		public SpreadsheetException(string message, string cellLocation)
			: this(message)
			=> CellReference = cellLocation;

		public string CellReference { get; } = CellLocation.Empty();
	}
}
