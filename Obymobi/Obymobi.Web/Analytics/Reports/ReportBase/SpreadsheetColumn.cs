﻿using System.Diagnostics;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	[DebuggerDisplay("{Value}")]
	public readonly struct SpreadsheetColumn
	{
		private int Value { get; }

		private SpreadsheetColumn(int value) => Value = value;

		private SpreadsheetColumn(string value) => Value = SLConvert.ToColumnIndex(value);

		public static explicit operator SpreadsheetColumn(int value) => new SpreadsheetColumn(value);
		public static explicit operator SpreadsheetColumn(string value) => new SpreadsheetColumn(value);
		public static explicit operator int(SpreadsheetColumn column) => column.Value;
		public static explicit operator string(SpreadsheetColumn column) => SLConvert.ToColumnName(column.Value);

		public static bool operator ==(SpreadsheetColumn lhs, SpreadsheetColumn rhs) => lhs.Equals(rhs);

		public static bool operator !=(SpreadsheetColumn lhs, SpreadsheetColumn rhs) => !(lhs == rhs);

		public static SpreadsheetColumn operator ++(SpreadsheetColumn column) => (SpreadsheetColumn)((int) column + 1);

		public static SpreadsheetColumn operator +(SpreadsheetColumn column, int increment) => (SpreadsheetColumn) ((int) column + increment);

		public static SpreadsheetColumn operator -(SpreadsheetColumn column, int decrement) => (SpreadsheetColumn) ((int) column - decrement);

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
			{
				return false;
			}

			var column = (SpreadsheetColumn)obj;
			return Equals(column);
		}

		public bool Equals(SpreadsheetColumn other) => Value == other.Value;

		public override int GetHashCode() => Value.GetHashCode();
	}
}
