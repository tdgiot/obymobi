﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public class CurrencyViewCell : DecimalViewCell
	{
		public CurrencyViewCell(decimal value, Currency currency) : this(value, $"{currency.Symbol}{SpreadsheetHelper.DecimalFormatCode}")
		{
		}

		public CurrencyViewCell(decimal value, string formatCode) : base(value, formatCode)
		{
		}

		public CurrencyViewCell(decimal value, Currency currency, HorizontalAlignmentValues horizontalAlignmentValues) 
			: this(value, currency, horizontalAlignmentValues, null)
		{
		}

		public CurrencyViewCell(decimal value, Currency currency, HorizontalAlignmentValues? horizontalAlignmentValues, Borders? borders, BorderStyleValues borderStyleValues = BorderStyleValues.Thin) 
			: base(value,
				$"{currency.Symbol}{SpreadsheetHelper.DecimalFormatCode}",
				horizontalAlignmentValues,
				null,
				null, 
				borders, 
				null, 
				borderStyleValues)
		{
		}
	}
}
