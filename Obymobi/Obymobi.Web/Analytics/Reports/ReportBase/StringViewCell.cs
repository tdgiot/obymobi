﻿using System.Drawing;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using Color = System.Drawing.Color;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
    public class StringViewCell : ViewCell<string>
	{
		public StringViewCell(string value) : base(value)
		{
		}

		public StringViewCell(string value, FontStyle fontStyle, double? fontSize = null)
			: this(value, fontStyle, null, null, null, null, null, null, fontSize: fontSize)
		{
			
		}

		public StringViewCell(string value, HorizontalAlignmentValues horizontalAlignmentValues)
			: this(value, FontStyle.Regular, null, horizontalAlignmentValues)
		{
		}

		public StringViewCell(string value, FontStyle fontStyle, string formatCode, HorizontalAlignmentValues? horizontalAlignmentValues)
			: base(value, fontStyle, formatCode, horizontalAlignmentValues, null, null, null, null)
		{
		}

		public StringViewCell(string value,
			string formatCode, 
			HorizontalAlignmentValues? horizontalAlignmentValues, 
			Color? textColour, 
			Color? backgroundColour, 
			Borders? borders, 
			Color? borderColour,
			BorderStyleValues borderStyleValues)
			: this(value, FontStyle.Regular, formatCode, horizontalAlignmentValues, textColour, backgroundColour, borders, borderColour, borderStyleValues)
		{
		}
		public StringViewCell(string value,
			FontStyle fontStyle, 
			string formatCode, 
			HorizontalAlignmentValues? horizontalAlignmentValues, 
			Color? textColour, 
			Color? backgroundColour, 
			Borders? borders, 
			Color? borderColour,
			BorderStyleValues borderStyleValues = BorderStyleValues.Thin,
			double? fontSize = null)
			: base(value, fontStyle, formatCode, horizontalAlignmentValues, textColour, backgroundColour, borders, borderColour, borderStyleValues, fontSize)
		{
		}

		public override void ToSpreadsheet(SLDocument spreadsheet, string cellReference)
		{
			base.ToSpreadsheet(spreadsheet, cellReference);
			if (!spreadsheet.SetCellValue(cellReference, Value))
			{
				throw NewSpreadsheetException(cellReference);
			}
		}

		public override string ToString() => Value;
	}
}
