﻿namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public abstract class ViewHeader
	{
		protected ViewHeader(HeaderCell[] headerCells) => HeaderCells = headerCells;

		public HeaderCell[] HeaderCells { get; }

		public int RequiredHeight => 1;
	}

	public abstract class ViewTotals
	{
		public IViewCell[] TotalsCells { get; }

		protected ViewTotals(IViewCell[] totalsCells) => TotalsCells = totalsCells;
	}
}
