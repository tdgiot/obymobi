﻿using System.Diagnostics;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	[DebuggerDisplay("{Value}")]
	public readonly struct SpreadsheetRow
	{
		private int Value { get; }

		private SpreadsheetRow(int value) => Value = value;

		public static explicit operator int(SpreadsheetRow row) => row.Value;
		public static explicit operator SpreadsheetRow(int value) => new SpreadsheetRow(value);

		public static bool operator ==(SpreadsheetRow lhs, SpreadsheetRow rhs) => lhs.Equals(rhs);

		public static bool operator !=(SpreadsheetRow lhs, SpreadsheetRow rhs) => !(lhs == rhs);

		public static SpreadsheetRow operator ++(SpreadsheetRow row) => (SpreadsheetRow)((int)row + 1);

		public static SpreadsheetRow operator +(SpreadsheetRow row, int increment) => (SpreadsheetRow)((int)row + increment);

		public static SpreadsheetRow operator -(SpreadsheetRow row, int decrement) => (SpreadsheetRow)((int)row - decrement);

		public override int GetHashCode() => Value.GetHashCode();

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
			{
				return false;
			}

			var row = (SpreadsheetRow)obj;
			return Equals(row);
		}

		public bool Equals(SpreadsheetRow other) => Value == other.Value;
	}
}
