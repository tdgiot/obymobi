﻿using System.Drawing;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using Color = System.Drawing.Color;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public class IntViewCell : ViewCell<int>
	{
		public IntViewCell(int value, HorizontalAlignmentValues horizontalAlignmentValues)
			: this(value, FontStyle.Regular, null, horizontalAlignmentValues)
		{

		}

		public IntViewCell(int value, FontStyle fontStyle, string formatCode, HorizontalAlignmentValues? horizontalAlignmentValues)
			: this(value, fontStyle, formatCode, horizontalAlignmentValues, null, null, null, null)
		{
		}

		public IntViewCell(int value,
			FontStyle fontStyle,
			string formatCode,
			HorizontalAlignmentValues? horizontalAlignmentValues,
			Color? textColour,
			Color? backgroundColour,
			Borders? borders,
			Color? borderColour)
			: base(value, fontStyle, formatCode, horizontalAlignmentValues, textColour, backgroundColour, borders, borderColour)

		{
		}

		public override void ToSpreadsheet(SLDocument spreadsheet, string cellReference)
		{
			base.ToSpreadsheet(spreadsheet, cellReference);
			if (!spreadsheet.SetCellValue(cellReference, Value))
			{
				throw NewSpreadsheetException(cellReference);
			}
		}
	}
}
