﻿using System.Collections.Generic;
using Obymobi.Web.Analytics.Reports.ProductReport;
using Obymobi.Web.Analytics.Reports.TransactionsAppLess;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public abstract class HorizontalReportSubView : ReportSubView
	{
		protected HorizontalReportSubView(ViewHeader header, ViewItem[] valueItems) : this(header, valueItems, new EmptyViewTotals())
		{
		}

		protected HorizontalReportSubView(ViewHeader header, ViewItem[] valueItems, ViewTotals viewTotals) : base(header, valueItems, viewTotals)
		{
		}

		public override CellLocation RenderHeader(SLDocument spreadsheet, CellLocation cellLocation)
		{
			var column = cellLocation.Column;

			foreach (var headerCell in Header.HeaderCells)
			{
				var cellReference = SLConvert.ToCellReference((int)cellLocation.Row, (int)column);
				headerCell.ToSpreadsheet(spreadsheet, cellReference);
				column++;
			}

			return new CellLocation(column - 1, cellLocation.Row);
		}

		protected override CellLocation RenderValues(SLDocument spreadsheet, IEnumerable<ViewValues> viewValues, CellLocation cellLocation)
		{
			var row = cellLocation.Row;
			var maxColumn = cellLocation.Column;
			var maxRow = cellLocation.Row;
			foreach (var viewRow in viewValues)
			{
				var columnIndex = cellLocation.Column;
				foreach (var cell in viewRow.Cells)
				{
					cell.ToSpreadsheet(spreadsheet, SLConvert.ToCellReference((int)row, (int)columnIndex));
					maxColumn = maxColumn.Max(columnIndex);
					columnIndex++;
				}

				maxRow = maxRow.Max(row);
				row += viewRow.RowCount;
			}

			return new CellLocation(maxColumn, maxRow);
		}

		public override CellLocation RenderTotals(SLDocument spreadsheet, CellLocation cellLocation)
		{
			var column = cellLocation.Column;

			foreach (var viewCell in ViewTotals.TotalsCells)
			{
				var cellReference = SLConvert.ToCellReference((int)cellLocation.Row, (int)column);
				viewCell.ToSpreadsheet(spreadsheet, cellReference);
				column++;
			}

			CellLocation endLocation = new CellLocation(column - 1, cellLocation.Row);

			SLStyle style = spreadsheet.CreateStyle();
			style.Border.TopBorder.BorderStyle = DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin;
			style.Border.BottomBorder.BorderStyle = DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Double;

			spreadsheet.SetCellStyle(SLConvert.ToCellReference((int) cellLocation.Row, (int) cellLocation.Column), SLConvert.ToCellReference((int) endLocation.Row, (int) endLocation.Column), style);

			return endLocation;
		}
	}
}
