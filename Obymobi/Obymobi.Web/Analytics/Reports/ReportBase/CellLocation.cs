﻿using System.Diagnostics;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	[DebuggerDisplay("Column = {(string)Column}, Row = {Row}")]
	public class CellLocation
	{
		public CellLocation(SpreadsheetColumn column, SpreadsheetRow row)
		{
			Row = row;
			Column = column;
		}

		public CellLocation ChangeRow(SpreadsheetRow newRowIndex) => new CellLocation(Column, newRowIndex);

		public CellLocation ChangeColumn(SpreadsheetColumn newColumnIndex) => new CellLocation(newColumnIndex, Row);

		public SpreadsheetRow Row { get; }

		public SpreadsheetColumn Column { get; }

		public CellLocation MoveRow(int amount) => new CellLocation(Column, Row + amount);

		public CellLocation MoveColumn(int amount) => new CellLocation(Column + amount, Row);

		public static implicit operator string(CellLocation cellLocation) => SLConvert.ToCellReference((int)cellLocation.Row, (int)cellLocation.Column);

		public override string ToString() => this;

		public static CellLocation Empty() => new CellLocation((SpreadsheetColumn)0, (SpreadsheetRow)0);

		public bool IsEmpty => Column.Equals((SpreadsheetColumn) 0) && Row.Equals((SpreadsheetRow) 0);

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
			{
				return false;
			}

			var cellLocation = (CellLocation)obj;
			return Row == cellLocation.Row && Column == cellLocation.Column;
		}

		public override int GetHashCode() => (Column, Row).GetHashCode();
	}
}
