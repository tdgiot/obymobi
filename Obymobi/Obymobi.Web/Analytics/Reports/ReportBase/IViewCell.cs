﻿using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public interface IViewCell
	{
		void ToSpreadsheet(SLDocument spreadsheet, string cellReference);
	}
}