﻿using System;
using System.Drawing;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using Color = System.Drawing.Color;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public class DateTimeViewCell : ViewCell<DateTime?>
	{
		public DateTimeViewCell(DateTime? value) : this(value, FontStyle.Regular)
		{
		}

		public DateTimeViewCell(DateTime? value, string formatCode, HorizontalAlignmentValues? horizontalAlignmentValues)
			: this(value, FontStyle.Regular, formatCode, horizontalAlignmentValues)
		{
		}

		public DateTimeViewCell(DateTime? value, FontStyle fontStyle)
			: this(value, fontStyle, @"yyyy-mm-dd h:mm:ss", DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Left, null, null, null, null)
		{
		}

		public DateTimeViewCell(DateTime? value, FontStyle fontStyle, string formatCode, HorizontalAlignmentValues? horizontalAlignmentValues)
			: this(value, fontStyle, formatCode, horizontalAlignmentValues, null, null, null, null)
		{
		}

		public DateTimeViewCell(DateTime? value, 
			FontStyle fontStyle, 
			string formatCode, 
			HorizontalAlignmentValues? horizontalAlignmentValues, 
			Color? textColour, 
			Color? backgroundColour, 
			Borders? borders, 
			Color? borderColour)
			: base(value, fontStyle, formatCode, horizontalAlignmentValues, textColour, backgroundColour, borders, borderColour)
		{
		}

		public override void ToSpreadsheet(SLDocument spreadsheet, string cellReference)
		{
			base.ToSpreadsheet(spreadsheet, cellReference);
			if (!(Value.HasValue ? spreadsheet.SetCellValue(cellReference, Value.Value) : spreadsheet.SetCellValue(cellReference, string.Empty)))
			{
				throw NewSpreadsheetException(cellReference);
			}
		}
	}
}
