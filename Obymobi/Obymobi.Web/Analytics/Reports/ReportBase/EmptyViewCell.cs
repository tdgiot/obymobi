﻿namespace Obymobi.Web.Analytics.Reports.ReportBase
{
    public class EmptyViewCell : ViewCell<string>
    {
        public EmptyViewCell() : base(string.Empty)
        {
        }
    }
}
