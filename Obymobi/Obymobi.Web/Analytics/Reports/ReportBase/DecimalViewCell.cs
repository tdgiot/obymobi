﻿using System.Drawing;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using Color = System.Drawing.Color;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public class DecimalViewCell : ViewCell<decimal>
	{
		public DecimalViewCell(decimal value, string formatCode) 
			: this(value, formatCode, DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Right)
		{
		}

		public DecimalViewCell(decimal value, string formatCode, HorizontalAlignmentValues horizontalAlignmentValues)
			: this(value, FontStyle.Regular, formatCode, horizontalAlignmentValues)
		{
		}

		public DecimalViewCell(decimal value, FontStyle fontStyle, string formatCode, HorizontalAlignmentValues? horizontalAlignmentValues)
			: base(value, fontStyle, formatCode, horizontalAlignmentValues, null, null, null, null)
		{
		}

		public DecimalViewCell(decimal value,
			string formatCode,
			HorizontalAlignmentValues? horizontalAlignmentValues,
			Color? textColour,
			Color? backgroundColour,
			Borders? borders,
			Color? borderColour,
			BorderStyleValues borderStyleValues)
			: this(value, FontStyle.Regular, formatCode, horizontalAlignmentValues, textColour, backgroundColour, borders, borderColour, borderStyleValues)
		{
		}

		public DecimalViewCell(decimal value,
			FontStyle fontStyle,
			string formatCode,
			HorizontalAlignmentValues? horizontalAlignmentValues,
			Color? textColour,
			Color? backgroundColour,
			Borders? borders,
			Color? borderColour,
			BorderStyleValues borderStyleValues)
			: base(value, fontStyle, formatCode, horizontalAlignmentValues, textColour, backgroundColour, borders, borderColour, borderStyleValues)
		{
		}

		public override void ToSpreadsheet(SLDocument spreadsheet, string cellReference)
		{
			base.ToSpreadsheet(spreadsheet, cellReference);
			if (!spreadsheet.SetCellValue(cellReference, Value))
			{
				throw NewSpreadsheetException(cellReference);
			}
		}
	}
}
