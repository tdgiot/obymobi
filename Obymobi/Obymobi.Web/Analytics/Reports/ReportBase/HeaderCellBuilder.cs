﻿using System.Drawing;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public class HeaderCellBuilder
	{
		public HeaderCellBuilder() : this(default, FontStyle.Bold, null)
		{

		}

		private HeaderCellBuilder(string value, FontStyle fontStyle, HorizontalAlignmentValues? horizontalAlignmentValues)
		{
			Value = value;
			FontStyle = fontStyle;
			HorizontalAlignmentValues = horizontalAlignmentValues;
		}

		private string Value { get; }

		private FontStyle FontStyle { get; }

		private HorizontalAlignmentValues? HorizontalAlignmentValues { get; }

		public HeaderCell Build() => new HeaderCell(Value, FontStyle, HorizontalAlignmentValues);

		public HeaderCellBuilder With(FontStyle fontStyle) => new HeaderCellBuilder(Value, fontStyle, HorizontalAlignmentValues);

		public HeaderCellBuilder With(string value) => new HeaderCellBuilder(value, FontStyle, HorizontalAlignmentValues);
		
		public HeaderCellBuilder With(HorizontalAlignmentValues horizontalAlignmentValues) => new HeaderCellBuilder(Value, FontStyle, horizontalAlignmentValues);
	}
}