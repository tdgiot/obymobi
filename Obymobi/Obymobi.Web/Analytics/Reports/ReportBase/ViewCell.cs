﻿using System.Drawing;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using Color = System.Drawing.Color;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public class ViewCell<TValue> : IViewCell
	{
		public ViewCell(TValue value) : this(value, FontStyle.Regular, null, null, null, null, null, null)
		{
		}

		public ViewCell(TValue value, FontStyle fontStyle, string formatCode, HorizontalAlignmentValues? horizontalAlignmentValues, Color? textColour, Color? backgroundColour, Borders? borders, Color? borderColour, BorderStyleValues borderStyleValues = BorderStyleValues.Thin, double? fontSize = null)
		{
			Value = value;
			FontStyle = fontStyle;
			FontSize = fontSize;
			FormatCode = formatCode;
			HorizontalAlignmentValues = horizontalAlignmentValues;
			TextColour = textColour;
			BackgroundColour = backgroundColour;
			Borders = borders;
			BorderColour = borderColour;
			BorderStyleValues = borderStyleValues;
		}

		public TValue Value { get; }

		public FontStyle FontStyle { get; }

		public string FormatCode { get; }

		private Color? TextColour { get; }

		private Color? BackgroundColour { get; }

		private Borders? Borders { get; }

		private Color? BorderColour { get; }

		public BorderStyleValues BorderStyleValues { get; }

		public HorizontalAlignmentValues? HorizontalAlignmentValues { get; }

		public double? FontSize { get; }

		public virtual void ToSpreadsheet(SLDocument spreadsheet, string cellReference)
			=> spreadsheet.SetStyle(cellReference, 
				fontStyle: FontStyle,
				fontSize: FontSize,
				horizontalAlignment: HorizontalAlignmentValues,
				formatCode: FormatCode,
				backgroundColor: BackgroundColour,
				textColor: TextColour,
				borderColor: BorderColour,
				borders: Borders,
				borderStyle: BorderStyleValues, 
				verticalAlignment: VerticalAlignmentValues.Top);

		protected SpreadsheetException NewSpreadsheetException(string cellReference)
			=> new SpreadsheetException($"Failed to set view cell value on spreadsheet: {Value}.", cellReference);
	}
}
