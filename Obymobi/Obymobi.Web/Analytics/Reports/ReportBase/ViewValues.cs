﻿namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public class ViewValues
	{
		public ViewValues(IViewCell[] cells) : this(cells, 1)
		{
		}

		public ViewValues(IViewCell[] cells, int rowCount)
		{
			Cells = cells;
			RowCount = rowCount;
		}

		public IViewCell[] Cells { get; }

		public int RowCount { get; }
	}
}