﻿using System;
using Obymobi.Web.Analytics.Model.Orders;
using Obymobi.Web.Analytics.Reports.TransactionsAppLess;
using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public abstract class SpreadsheetReport
	{
		private readonly Lazy<Company> company;
		private readonly int companyId;
		private readonly ICompanyRepository companyRepository;

		protected SpreadsheetReport(ICompanyRepository companyRepository, int companyId)
		{
			this.companyId = companyId;
			this.companyRepository = companyRepository;
			company = new Lazy<Company>(FetchCompany);
		}

		protected Company Company => company.Value;


		public virtual bool CreateReport(out SLDocument spreadsheet)
		{
			spreadsheet = CreateSpreadsheetDocument();
			return true;
		}

		protected static CellLocation RestoreRow(CellLocation location, CellLocation baseLocation) => new CellLocation(location.Column, baseLocation.Row);
		protected static CellLocation RestoreColumn(CellLocation location, CellLocation baseLocation) => new CellLocation(baseLocation.Column, location.Row);

		protected static CellLocation SetLocationToNextRow(CellLocation location, int rowAmountToSkip = 0) =>
			location.ChangeRow(GetNextRow(location.Row, rowAmountToSkip));

		protected static CellLocation SetLocationToNextColumn(CellLocation location, int columnAmountToSkip = 0) =>
			location.ChangeColumn(GetNextColumn(location.Column, columnAmountToSkip));

		protected static SpreadsheetRow GetNextRow(SpreadsheetRow currentRow, int rowAmountToSkip = 0) => (SpreadsheetRow) ((int) currentRow + 1 + rowAmountToSkip);

		private static SLDocument CreateSpreadsheetDocument() => new SLDocument();
		private static SpreadsheetColumn GetNextColumn(SpreadsheetColumn currentColumn, int columnAmountToSkip = 0) => (SpreadsheetColumn) ((int) currentColumn + 1 + columnAmountToSkip);
		private Company FetchCompany() => new Company().FetchCompany(companyRepository, companyId);

		protected static CellLocation RenderHeaderForHorizontalSubView(SLDocument spreadsheet, ReportSubView subView, CellLocation location)
			=> SetLocationToNextColumn(subView.RenderHeader(spreadsheet, location));

		protected static CellLocation RenderTotalsForHorizontalSubView(SLDocument spreadsheet, ReportSubView subView, CellLocation location)
			=> SetLocationToNextColumn(subView.RenderTotals(spreadsheet, location));
	}
}
