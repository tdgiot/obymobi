﻿using SpreadsheetLight;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
    public class Worksheet
    {
        public Worksheet(string name, Column[] columns)
        {
            Name = name;
            Columns = columns;
        }

        private string Name { get; }

        private Column[] Columns { get; }

        public void Render(SLDocument spreadsheet)
        {
            if (Name != SLDocument.DefaultFirstSheetName)
            {
                if (!spreadsheet.AddWorksheet(Name))
                {
                    throw new SpreadsheetException($"Failed to create new worksheet with name: {Name}.");
                }

                // Delete default worksheet (fail silently if it's already deleted)
                spreadsheet.DeleteWorksheet(SLDocument.DefaultFirstSheetName);
            }

            var columnIndex = 1;

            foreach (var column in Columns)
            {
                if (!spreadsheet.SetColumnWidth(columnIndex, column.Width))
                {
                    throw new SpreadsheetException($"Failed to change the width of column {columnIndex} to {column.Width}");
                }

                columnIndex++;
            }
        }
    }
}
