﻿namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public class SubViewLocation
	{
		public SubViewLocation(CellLocation cellLocation) => CellLocation = cellLocation;

		public CellLocation CellLocation { get; }

		public static implicit operator string(SubViewLocation subViewLocation) => subViewLocation.CellLocation;

		public override string ToString() => this;
	}
}