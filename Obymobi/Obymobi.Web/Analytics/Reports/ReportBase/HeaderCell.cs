﻿using System.Drawing;
using DocumentFormat.OpenXml.Spreadsheet;
using Color = System.Drawing.Color;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public class HeaderCell : StringViewCell
	{
		public HeaderCell(string value) : this(value, FontStyle.Bold, null)
		{

		}

		public HeaderCell(string value,
			FontStyle fontStyle,
			HorizontalAlignmentValues? horizontalAlignmentValues)
			: base(value, fontStyle, null, horizontalAlignmentValues, 
				null, Color.FromArgb(147, 196, 125), Borders.Top | Borders.Bottom, null, BorderStyleValues.Medium)
		{
		}

		public HeaderCell(string value,
			FontStyle fontStyle,
			HorizontalAlignmentValues? horizontalAlignmentValues,
			Color? textColour,
			Color? backgroundColour,
			Borders? borders,
			Color? borderColour,
			BorderStyleValues borderStyleValues)
			: base(value, fontStyle, null, horizontalAlignmentValues, textColour, backgroundColour, borders, borderColour, borderStyleValues)
		{
		}
	}
}