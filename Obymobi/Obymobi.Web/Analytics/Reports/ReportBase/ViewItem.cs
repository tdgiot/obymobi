﻿namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public class ViewItem
	{
		public ViewItem(ViewValues[] viewValues) => ViewValues = viewValues;

		public ViewValues[] ViewValues { get; }

		public int Length => ViewValues.Length;
	}
}
