﻿using Obymobi.Web.Analytics.Reports.ProductReport;
using Obymobi.Web.Analytics.Reports.TransactionsAppLess;
using SpreadsheetLight;
using System.Collections.Generic;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public abstract class VerticalReportSubView : ReportSubView
	{
		protected VerticalReportSubView(ViewHeader header, ViewItem[] valueItems) : base(header, valueItems, new EmptyViewTotals())
		{
		}

		public override CellLocation RenderHeader(SLDocument spreadsheet, CellLocation cellLocation)
		{
			var row = cellLocation.Row;
			var column = cellLocation.Column;

			foreach (var headerCell in Header.HeaderCells)
			{
				var cellReference = SLConvert.ToCellReference((int)row, (int)column);
				if (!spreadsheet.SetCellValue(cellReference, headerCell.Value))
				{
					throw new SpreadsheetException($"Failed to set header cell value on spreadsheet: {headerCell.Value}.", cellReference);
				}

				spreadsheet.SetStyle(cellReference, fontStyle: headerCell.FontStyle);
				row++;
			}

			return new CellLocation(column, row - 1);
		}

		protected override CellLocation RenderValues(SLDocument spreadsheet, IEnumerable<ViewValues> values, CellLocation cellLocation)
		{
			var column = cellLocation.Column;
			var maxColumn = cellLocation.Column;
			var maxRow = cellLocation.Row;
			foreach (var viewColumn in values)
			{
				var rowIndex = cellLocation.Row;
				foreach (var cell in viewColumn.Cells)
				{
					cell.ToSpreadsheet(spreadsheet, SLConvert.ToCellReference((int)rowIndex, (int)column));
					maxRow = maxRow.Max(rowIndex);
					rowIndex++;
				}

				maxColumn = maxColumn.Max(column);
				column++;
			}

			return new CellLocation(maxColumn, maxRow);

		}

		public override CellLocation RenderTotals(SLDocument spreadsheet, CellLocation cellLocation) => cellLocation;
	}
}
