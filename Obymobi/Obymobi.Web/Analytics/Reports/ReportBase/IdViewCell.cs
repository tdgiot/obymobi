﻿using System.Drawing;
using DocumentFormat.OpenXml.Spreadsheet;

namespace Obymobi.Web.Analytics.Reports.ReportBase
{
	public class IdViewCell : IntViewCell
	{
		public IdViewCell(int value) : this(value, FontStyle.Regular, null, DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Left)
		{
		}

		public IdViewCell(int value, FontStyle fontStyle, string formatCode, HorizontalAlignmentValues? horizontalAlignmentValues) 
			: base(value, fontStyle, formatCode, horizontalAlignmentValues)
		{
		}
	}
}