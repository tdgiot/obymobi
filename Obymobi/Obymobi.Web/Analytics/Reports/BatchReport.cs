﻿using Dionysos;
using Dionysos.IO;
using Newtonsoft.Json;
using Obymobi.Enums;
using Obymobi.Logic.Analytics;
using Obymobi.Logic.HelperClasses;
using Obymobi.Web.Analytics.Requests;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Obymobi.Web.Analytics.Reports
{
    public class BatchReport
    {
        private readonly DateTime fromUtc;
        private readonly DateTime tillUtc;
        private readonly Dictionary<string, byte[]> reportOutputFiles = new Dictionary<string, byte[]>();
        private readonly List<Tuple<AnalyticsReportType, object>> reportsToRun = new List<Tuple<AnalyticsReportType, object>>();
        private int reportsRan = 0;

        public BatchReport(DateTime fromUtc, DateTime tillUtc, BatchReportRequest batchReportMetadata)
        {
            // Combine the different collections to one to be able to easily iterate.
            foreach (TransactionsRequest report in batchReportMetadata.TransactionReports)
            {
                reportsToRun.Add(new Tuple<AnalyticsReportType, object>(AnalyticsReportType.Transactions, report));
            }

            foreach (Filter report in batchReportMetadata.PageViewReports)
            {
                reportsToRun.Add(new Tuple<AnalyticsReportType, object>(AnalyticsReportType.PageViews, report));
            }


            foreach (Filter report in batchReportMetadata.ProductOverviewReports)
            {
                reportsToRun.Add(new Tuple<AnalyticsReportType, object>(AnalyticsReportType.ProductOverview, report));
            }

            foreach (Filter report in batchReportMetadata.WakeUpCallReports)
            {
                reportsToRun.Add(new Tuple<AnalyticsReportType, object>(AnalyticsReportType.WakeUpCalls, report));
            }

            this.fromUtc = fromUtc;
            this.tillUtc = tillUtc;
        }

        /// <summary>
        /// You must iterate this method using while to run all reports.
        /// Reason for this choice is because it will be use primarily by the ReportingPoller 
        /// which needs in between updates to not cause a timeout.
        /// </summary>
        /// <returns></returns>
        public bool RunAReport()
        {
            Tuple<AnalyticsReportType, object> reportToRun = reportsToRun[reportsRan];
            switch (reportToRun.Item1)
            {
                case AnalyticsReportType.Transactions:
                    RunTransactionsReport(reportToRun.Item2 as TransactionsRequest);
                    break;
                case AnalyticsReportType.PageViews:                                        
                case AnalyticsReportType.WakeUpCalls:                                        
                case AnalyticsReportType.ProductOverview:
                    RunGenericReport(reportToRun.Item1, reportToRun.Item2 as Filter);
                    break;                                 
                default:
                    using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes("Unsupported report of type: {0}")))
                    {
                        reportOutputFiles.Add("Unsupported Report {0}.txt".FormatSafe(reportsRan + 1), ms.ToArray());
                    }
                    break;
            }

            reportsRan++;
            return reportsRan < reportsToRun.Count;
        }

        public void WriteZipFile(Stream stream)
        {
            ZipUtil.ZipByteArrays(this.reportOutputFiles, stream);
        }

        private void RunTransactionsReport(TransactionsRequest request)
        {

            request.FromUtc = fromUtc;
            request.TillUtc = tillUtc;
            
            TransactionsReport report = new TransactionsReport(request);
            request.OrderType = request.OrderType.HasValue ? request.OrderType : null;
            report.CreateReport(out SLDocument spreadsheet);
            string fileName = "TransactionsReport - {0}.xlsx".FormatSafe(request.Name);
            string fileNameMetadata = "TransactionsReport - {0} - Meta.txt".FormatSafe(request.Name);

            using (MemoryStream ms = new MemoryStream())
            {
                spreadsheet.SaveAs(ms);
                reportOutputFiles.Add(fileName, ms.ToArray());
            }

            AppendMetadata(fileNameMetadata, request);
        } 

        private void RunGenericReport(AnalyticsReportType reportType, Filter filter)
        {
            filter.FromUtc = fromUtc;
            filter.TillUtc = tillUtc;

            string fileName = "{0} - {1}.xlsx".FormatSafe(reportType, filter.Name);
            string fileNameMetadata = "{0} - {1} - Meta.txt".FormatSafe(reportType, filter.Name);

            // I know - switching again, but due to set up of other code I couldn't think of a nicer way quick enough for
            // a feature that's End of life on it's arrival.
            IReport report;
            switch (reportType)
            {
                case AnalyticsReportType.PageViews:
                    report = new PageviewsReport(filter, WebEnvironmentHelper.CloudEnvironment);
                    break;
                case AnalyticsReportType.WakeUpCalls:
                    report = new WakeUpCallsReport(filter);
                    break;
                case AnalyticsReportType.ProductOverview:
                    report = new ProductOverviewReport(filter);
                    break;
                default:
                    return;
            }

            report.RunReport(out SLDocument spreadsheet);

            using (MemoryStream ms = new MemoryStream())
            {
                spreadsheet.SaveAs(ms);
                reportOutputFiles.Add(fileName, ms.ToArray());
            }

            AppendMetadata(fileNameMetadata, filter);
        }

        private void AppendMetadata(string filename, object metadata)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Text");
            sb.AppendLine(metadata.ToString());
            sb.AppendLine("---------");
            sb.AppendLine("XML");
            sb.AppendLine(XmlHelper.Serialize(metadata));
            sb.AppendLine("---------");
            sb.AppendLine("Json");
            sb.AppendLine(JsonConvert.SerializeObject(metadata));
            sb.AppendLine("---------");

            using (MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(sb.ToString())))
            {
                reportOutputFiles.Add("{0}.txt".FormatSafe(filename), ms.ToArray());
            }
        }
    }
}
