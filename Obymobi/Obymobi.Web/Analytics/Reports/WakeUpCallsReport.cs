﻿using SpreadsheetLight;
using Obymobi.Web.Analytics.Model.WakeUpCalls;
using Obymobi.Logic.Analytics;

namespace Obymobi.Web.Analytics.Reports
{
    public class WakeUpCallsReport : IReport
    {
        #region Fields

        private int duplicateWorksheets;

        #endregion

        #region Constructors

        public WakeUpCallsReport(Filter filter)
        {
            this.WakeUpCalls = new Obymobi.Web.Analytics.Model.WakeUpCalls.WakeUpCalls(filter);
            this.WakeUpCalls.FetchAndProcessData();
        }

        #endregion

        #region Properties

        private WakeUpCalls WakeUpCalls { get; set; }

        #endregion

        #region Methods

        public bool RunReport(out SLDocument spreadsheet)
        {
            spreadsheet = new SLDocument();
            string summaryWorkSheetName = "Wake Up Calls";

            // Prepare the Summary worksheet            
            int summaryRowIndex = 2; // (1 is already used below)
            spreadsheet.RenameWorksheet(SLDocument.DefaultFirstSheetName, summaryWorkSheetName);

            // Render wake up calls
            this.RenderWakeUpCallsWorksheet(spreadsheet);
            
            // Scale 1x1 Page
            spreadsheet.SelectWorksheet(summaryWorkSheetName);
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 1);
            spreadsheet.SetPageSettings(pageSettings);

            // Set all print sizes to A4
            spreadsheet.SetPageSizeOfWorksheets(SLPaperSizeValues.A4Paper);

            return true;
        }

        private void RenderWakeUpCallsWorksheet(SLDocument spreadsheet)
        {
            // Create and select worksheet
            spreadsheet.AddWorksheet("Wake Up Calls");
            spreadsheet.SelectWorksheet("Wake Up Calls");

            // Set Headings + styling
            spreadsheet.SetCellValue("A1", "Created");
            spreadsheet.SetCellValue("B1", "OrderId");
            spreadsheet.SetCellValue("C1", "ClientId");
            spreadsheet.SetCellValue("D1", "DeliverypointId");
            spreadsheet.SetCellValue("E1", "DeliverypointName");
            spreadsheet.SetCellValue("F1", "DeliverypointNumber");
            spreadsheet.SetCellValue("G1", "DeliverypointgroupName");
            spreadsheet.SetCellValue("H1", "Wake Up Call");
            spreadsheet.SetCellValue("I1", "Status");
            spreadsheet.SetCellValue("J1", "Processed");
            spreadsheet.SetStyle("A1", "J1", borders: Borders.Bottom, backgroundColor: SpreadsheetHelper.Green14);
            spreadsheet.SetStyle("B1", "F1", DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);

            int currentRow = 2;
            foreach (WakeUpCall wakeUpCall in this.WakeUpCalls.WakeUpCallCollection)
            {
                this.RenderRowForWakeUpCall(spreadsheet, wakeUpCall, ref currentRow);
            }

            // Set column widths
            spreadsheet.SetColumnWidth("A", 22);
            spreadsheet.SetColumnWidth("B", 9);
            spreadsheet.SetColumnWidth("C", 9);
            spreadsheet.SetColumnWidth("D", 16);
            spreadsheet.SetColumnWidth("E", 19);
            spreadsheet.SetColumnWidth("F", 20);
            spreadsheet.SetColumnWidth("G", 25);
            spreadsheet.SetColumnWidth("H", 52);
            spreadsheet.SetColumnWidth("I", 12);
            spreadsheet.SetColumnWidth("J", 25);

            // Keep first line frozen
            spreadsheet.FreezePanes(1, 0);

            // Set printing to 1 page width
            SLPageSettings pageSettings = spreadsheet.GetPageSettings();
            pageSettings.ScalePage(1, 500);
            spreadsheet.SetPageSettings(pageSettings);
        }

        private void RenderRowForWakeUpCall(SLDocument spreadsheet, WakeUpCall wakeUpCall, ref int currentRow)
        {
            int productRow = currentRow;
            currentRow++;

            spreadsheet.SetCellValue("A" + productRow, wakeUpCall.CreatedAsString);
            spreadsheet.SetCellValue("B" + productRow, wakeUpCall.OrderId);
            spreadsheet.SetCellValue("C" + productRow, wakeUpCall.ClientId);
            spreadsheet.SetCellValue("D" + productRow, wakeUpCall.DeliverypointId);
            spreadsheet.SetCellValue("E" + productRow, wakeUpCall.DeliverypointName);
            spreadsheet.SetCellValue("F" + productRow, wakeUpCall.DeliverypointNumber);
            spreadsheet.SetCellValue("G" + productRow, wakeUpCall.DeliverypointgroupName);
            spreadsheet.SetCellValue("H" + productRow, wakeUpCall.WakeUpCallText);
            spreadsheet.SetCellValue("I" + productRow, wakeUpCall.Status);
            spreadsheet.SetCellValue("J" + productRow, wakeUpCall.ProcessedAsString);

            // Only background when including categories, otherwise borders
            spreadsheet.SetStyle("A" + productRow, "J" + productRow, borders: Borders.Bottom, borderColor: SpreadsheetHelper.Green15, backgroundColor: SpreadsheetHelper.Green10);
            spreadsheet.SetStyle("B" + productRow, "F" + productRow, DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Center);
        }

        #endregion
    }
}
