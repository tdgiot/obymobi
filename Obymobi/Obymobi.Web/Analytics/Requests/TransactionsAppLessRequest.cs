using Dionysos;
using Newtonsoft.Json;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Model.TransactionsAppLess;
using Obymobi.Web.Analytics.Requests.RequestBase;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Web.Analytics.Requests
{
    public class TransactionsAppLessRequest : ReportRequest
    {
        public TransactionsAppLessRequest()
        {
            ShowProducts = true;
            ShowProductQuantity = true;
            ShowProductPrice = true;
            ShowOrderNotes = true;
        }

        public int Version { get; } = 2;

        [JsonIgnore]
        public IEnumerable<TransactionsAppLessSheetFilter> TransactionsAppLessSheetFilters => SheetFilters.Cast<TransactionsAppLessSheetFilter>();

        #region Legacy Properties

        public int? OutletId { get; set; }
        public IReadOnlyList<int> ServiceMethods { get; set; } = new List<int>();
        public IReadOnlyList<int> CheckoutMethods { get; set; } = new List<int>();
        public bool ShowProductQuantity { get; set; }
        public bool ShowProductPrice { get; set; }
        public bool ShowProductCategory { get; set; }
        public bool ShowOrderNotes { get; set; }
        public bool ShowPaymentMethod { get; set; }
        public bool ShowCardSummary { get; set; }
        public bool ShowDeliveryPoint { get; set; }
        public bool ShowCoversCount { get; set; }
        public IncludeExcludeFilter CategoryFilterType { get; set; } = IncludeExcludeFilter.None;
        public IReadOnlyList<int> CategoryFilterIds { get; set; } = new List<int>();
        public IReadOnlyList<int> IncludedSystemProductTypes { get; set; } = new List<int>();
        public IReadOnlyList<int> IncludeTaxTariffIds { get; set; } = new List<int>();

        #endregion

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormatLine("CompanyId: {0}", CompanyId);
            sb.AppendFormatLine("Reporting Period: {0}", ReportingPeriod);
            sb.AppendFormatLine("Time UTC Until: {0}", UntilDateTimeUtc.ToString("G"));
            sb.AppendFormatLine("Lead Time Offset: {0}", LeadTimeOffset.ToString("g"));
            sb.AppendFormatLine("Time UTC From: {0}", FromDateTimeUtc.ToString("G"));

            foreach (SpreadSheetFilter filter in SheetFilters)
            {
                sb.Append(filter);
            }

            return sb.ToString();
        }
    }
}