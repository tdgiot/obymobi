﻿using Obymobi.Web.Analytics.Repositories.TransactionsAppLess;
using System.Linq;

namespace Obymobi.Web.Analytics.Requests
{
	public static class OrdersRequestConversion
	{
		public static OrdersRequest ToOrdersRequest(this TransactionsAppLessRequest self) =>
			new OrdersRequest
			{
				CompanyId = self.CompanyId,
				FromDateTimeUtc = self.FromDateTimeUtc,
				UntilDateTimeUtc = self.UntilDateTimeUtc,
				ShowProducts = self.ShowProducts,
				ShowSystemProducts = self.ShowSystemProducts,
				ShowAlterations = self.SheetFilters.Any(s => s.ShowAlterations),
				ShowCustomerInfo = self.ShowCustomerInfo
			};

		public static OrdersRequest ToOrdersRequest(this ProductReportRequest self) =>
			new OrdersRequest
			{
				CompanyId = self.CompanyId,
				FromDateTimeUtc = self.FromDateTimeUtc,
				UntilDateTimeUtc = self.UntilDateTimeUtc,
				ShowProducts = true,
				ShowSystemProducts = false,
				ShowAlterations = true,
				ShowCustomerInfo = false
			};

		public static OrdersRequest ToOrdersRequest(this InventoryReportRequest self) =>
			new OrdersRequest
			{
				CompanyId = self.CompanyId,
				FromDateTimeUtc = self.FromDateTimeUtc,
				UntilDateTimeUtc = self.UntilDateTimeUtc,
				ShowProducts = true,
				ShowSystemProducts = false,
				ShowAlterations = true,
				ShowCustomerInfo = false
			};
	}
}
