﻿using System.Collections.Generic;
using Obymobi.Enums;

namespace Obymobi.Web.Analytics.Requests
{
    public class ReportTemplateSheetFilter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public int SortOrder { get; set; }
        public int? OutletId { get; set; }
        public IReadOnlyList<int> ServiceMethods { get; set; } = new List<int>();
        public IReadOnlyList<int> CheckoutMethods { get; set; } = new List<int>();
        public bool ShowProducts { get; set; } = true;
        public bool ShowProductQuantity { get; set; } = true;
        public bool ShowProductPrice { get; set; } = true;
        public bool ShowProductCategory { get; set; }
        public bool ShowAlterations { get; set; }
        public bool ShowServiceMethod { get; set; } = true;
        public bool ShowCheckoutMethod { get; set; }
        public bool ShowCustomerInfo { get; set; }
        public bool ShowOrderNotes { get; set; } = true;
        public bool ShowSystemProducts { get; set; }
        public IncludeExcludeFilter CategoryFilterType { get; set; } = IncludeExcludeFilter.None;
        public IReadOnlyList<int> CategoryFilterIds { get; set; } = new List<int>();
        public IReadOnlyList<int> IncludedSystemProductTypes { get; set; } = new List<int>();
        public IReadOnlyList<int> IncludeTaxTariffIds { get; set; } = new List<int>();
    }
}