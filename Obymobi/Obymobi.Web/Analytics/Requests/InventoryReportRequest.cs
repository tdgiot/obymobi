﻿using Dionysos;
using Obymobi.Web.Analytics.Model.InventoryReport;
using Obymobi.Web.Analytics.Model.ModelBase;
using Obymobi.Web.Analytics.Requests.RequestBase;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Obymobi.Web.Analytics.Requests
{
	public class InventoryReportRequest : ReportRequest
	{
		public int Version { get; } = 2;
        
        [JsonIgnore]
		public IEnumerable<InventoryReportSheetFilter> InventoryReportSheetFilters => SheetFilters.Cast<InventoryReportSheetFilter>();

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendFormatLine("CompanyId: {0}", CompanyId);
			sb.AppendFormatLine("Reporting Period: {0}", ReportingPeriod);
			sb.AppendFormatLine("Time UTC Until: {0}", UntilDateTimeUtc.ToString("G"));
			sb.AppendFormatLine("Lead Time Offset: {0}", LeadTimeOffset.ToString("g"));
			sb.AppendFormatLine("Time UTC From: {0}", FromDateTimeUtc.ToString("G"));

			foreach (SpreadSheetFilter filter in SheetFilters)
			{
				sb.Append(filter);
			}

			return sb.ToString();
		}
	}
}
