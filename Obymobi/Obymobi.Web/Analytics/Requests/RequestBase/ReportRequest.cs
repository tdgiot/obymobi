﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Model.ModelBase;

namespace Obymobi.Web.Analytics.Requests.RequestBase
{
	public abstract class ReportRequest : IReportRequest
    {
        public int CompanyId { get; set; }
        public DateTime UntilDateTimeUtc { get; set; }
        public TimeSpan LeadTimeOffset { get; set; }
        public DateTime FromDateTimeUtc => ReportingPeriod != ReportingPeriod.Manual 
            ? UntilDateTimeUtc.Date.Add(LeadTimeOffset) 
            : UntilDateTimeUtc.Add(LeadTimeOffset);
        public ReportingPeriod ReportingPeriod { get; set; }
        public string CompanyTimeZone { get; set; }
        public IList<SpreadSheetFilter> SheetFilters { get; set; } = new List<SpreadSheetFilter>();
        
        #region Legacy Properties

        public bool ShowProducts { get; set; }
        public bool ShowAlterations { get; set; }
        public bool ShowCustomerInfo { get; set; }
        public bool ShowSystemProducts { get; set; }

        #endregion

        public IOrderedEnumerable<SpreadSheetFilter> GetActiveSheets() => SheetFilters
            .Where(x => x.Active)
            .OrderBy(x => x.SortOrder);

        public SpreadSheetFilter GetSheet(int sheetId) => SheetFilters.FirstOrDefault(x => x.Id == sheetId);

        public void UpdateSheet(SpreadSheetFilter sheetFilter)
        {
            SpreadSheetFilter filter = SheetFilters.FirstOrDefault(x => x.Id == sheetFilter.Id);
            if (filter != null)
            {
                sheetFilter.SortOrder = filter.SortOrder;
                
                SheetFilters.Remove(filter);
                SheetFilters.Add(sheetFilter);
            }
        }

        public void RemoveSheet(int sheetId)
        {
            SpreadSheetFilter filter = SheetFilters.FirstOrDefault(x => x.Id == sheetId);
            if (filter != null)
            {
                int sortOrder = filter.SortOrder;

                SheetFilters.Remove(filter);

                // Fix sort order
                foreach (SpreadSheetFilter sheetFilter in SheetFilters)
                {
                    if (sheetFilter.SortOrder > sortOrder)
                    {
                        sheetFilter.SortOrder--;
                    }
                }
            }
        }

        public string Serialize() => JsonConvert.SerializeObject(this, Formatting.Indented);
    }
}
