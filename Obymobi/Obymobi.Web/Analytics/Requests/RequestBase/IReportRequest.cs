﻿using System;
using System.Collections.Generic;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Model.ModelBase;

namespace Obymobi.Web.Analytics.Requests.RequestBase
{
	public interface IReportRequest
	{
        int CompanyId { get; set; }
        DateTime UntilDateTimeUtc { get; set; }
        TimeSpan LeadTimeOffset { get; set; }
        ReportingPeriod ReportingPeriod { get; set; }
        string CompanyTimeZone { get; set; }
		SpreadSheetFilter GetSheet(int sheetId);
		void UpdateSheet(SpreadSheetFilter sheetFilter);
		void RemoveSheet(int sheetId);
        IList<SpreadSheetFilter> SheetFilters { get; set; }
		string Serialize();
	}
}
