﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Web.Analytics.Requests
{
    public class BatchReportRequest
    {
        public BatchReportRequest()
        {
            TransactionReports = new List<TransactionsRequest>();
            PageViewReports = new List<Obymobi.Logic.Analytics.Filter>();
            WakeUpCallReports = new List<Obymobi.Logic.Analytics.Filter>();
            ProductOverviewReports = new List<Obymobi.Logic.Analytics.Filter>();
        }

        public string Name { get; set; }         
        public List<TransactionsRequest> TransactionReports { get; set; }
        public List<Obymobi.Logic.Analytics.Filter> PageViewReports { get; set; }
        public List<Obymobi.Logic.Analytics.Filter> WakeUpCallReports { get; set; }
        public List<Obymobi.Logic.Analytics.Filter> ProductOverviewReports { get; set; }
    }
}
