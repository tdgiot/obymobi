﻿using System;

namespace Obymobi.Web.Analytics.Requests
{
	[Serializable]
	public class OrdersRequest
	{
		public DateTime FromUtc { get; }
		public DateTime TillUtc { get; }
		public int CompanyId { get; }

		public OrdersRequest()
		{
			
		}

		public OrdersRequest(DateTime fromUtc, DateTime tillUtc, int companyId) : this()
		{
			FromUtc = fromUtc;
			TillUtc = tillUtc;
			CompanyId = companyId;
		}
	}
}