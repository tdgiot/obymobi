﻿using Obymobi.Web.Analytics.Bridges.Implementers;

namespace Obymobi.Web.Analytics.Bridges
{
	public class AnalyticsReportAbstraction
	{
		public AnalyticsReportAbstraction(IReportRequestImplementer reportRequestImplementer) => ReportRequestImplementer = reportRequestImplementer;
		protected IReportRequestImplementer ReportRequestImplementer { get; }
	}
}
