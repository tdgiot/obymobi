﻿using System;
using Obymobi.Web.Analytics.Converters;
using Obymobi.Web.Analytics.Factories;
using Obymobi.Web.Analytics.Requests;
using Obymobi.Web.Analytics.Requests.RequestBase;

namespace Obymobi.Web.Analytics.Bridges.Implementers
{
	public class ProductSalesReportRequestImplementer : ReportRequestImplementer<ProductReportSpreadSheetFilterConverter>
	{
		public ProductSalesReportRequestImplementer() : base(new Lazy<ProductReportSpreadSheetFilterConverter>(() => new ProductReportSpreadSheetFilterConverter()))
		{
		}

		public override string SheetFilterName => "ProductSalesReportSheetFilter";

		public override ReportRequest Create() => new ProductReportRequest();

		public override ReportRequest Deserialise(string value) => InternalDeserialise<ProductReportRequest>(value);

		public override ISheetFilterFactory CreateSheetFilterFactory() => new ProductReportSheetFactory();
	}
}
