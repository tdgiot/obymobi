﻿using System;
using Obymobi.Web.Analytics.Converters;
using Obymobi.Web.Analytics.Factories;
using Obymobi.Web.Analytics.Requests;
using Obymobi.Web.Analytics.Requests.RequestBase;

namespace Obymobi.Web.Analytics.Bridges.Implementers
{
	public class TransactionAppLessReportRequestImplementer : ReportRequestImplementer<TransactionsAppLessSpreadSheetFilterConverter>
	{
		public TransactionAppLessReportRequestImplementer() : base(new Lazy<TransactionsAppLessSpreadSheetFilterConverter>(() => new TransactionsAppLessSpreadSheetFilterConverter()))
		{
		}

		public override string SheetFilterName => "TransactionsAppLessReportSheetFilter";

		public override ReportRequest Create() => new TransactionsAppLessRequest();

		public override ReportRequest Deserialise(string value) => InternalDeserialise<TransactionsAppLessRequest>(value);

		public override ISheetFilterFactory CreateSheetFilterFactory() => new TransactionReportSheetFactory();
	}
}
