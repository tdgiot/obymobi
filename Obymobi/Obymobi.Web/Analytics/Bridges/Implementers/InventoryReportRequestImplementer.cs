﻿using System;
using Obymobi.Web.Analytics.Converters;
using Obymobi.Web.Analytics.Factories;
using Obymobi.Web.Analytics.Requests;
using Obymobi.Web.Analytics.Requests.RequestBase;

namespace Obymobi.Web.Analytics.Bridges.Implementers
{
	public class InventoryReportRequestImplementer : ReportRequestImplementer<InventoryReportSpreadSheetFilterConverter>
	{
		public InventoryReportRequestImplementer() : base(new Lazy<InventoryReportSpreadSheetFilterConverter>(() => new InventoryReportSpreadSheetFilterConverter()))
		{
		}

		public override string SheetFilterName => "InventoryReportSheetFilterControl";

		public override ReportRequest Create() => new InventoryReportRequest();

		public override ReportRequest Deserialise(string value) => InternalDeserialise<InventoryReportRequest>(value);

		public override ISheetFilterFactory CreateSheetFilterFactory() => new InventoryReportSheetFactory();
	}
}
