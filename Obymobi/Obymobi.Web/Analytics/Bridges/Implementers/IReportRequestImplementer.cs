﻿using Obymobi.Web.Analytics.Factories;
using Obymobi.Web.Analytics.Requests.RequestBase;

namespace Obymobi.Web.Analytics.Bridges.Implementers
{
	public interface IReportRequestImplementer
	{
		string SheetFilterName { get; }
		ReportRequest Create();
		ReportRequest Deserialise(string value);
		ISheetFilterFactory CreateSheetFilterFactory();
	}
}
