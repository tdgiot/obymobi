﻿using System;
using Newtonsoft.Json;
using Obymobi.Web.Analytics.Factories;
using Obymobi.Web.Analytics.Requests.RequestBase;

namespace Obymobi.Web.Analytics.Bridges.Implementers
{
	public abstract class ReportRequestImplementer<TJsonConverter> : IReportRequestImplementer
		where TJsonConverter : JsonConverter
	{
		private readonly Lazy<TJsonConverter> converter;

		protected ReportRequestImplementer(Lazy<TJsonConverter> converter) => this.converter = converter;

		protected JsonConverter Converter => converter.Value;

		public abstract ReportRequest Create();

		public abstract ReportRequest Deserialise(string value);

		public abstract ISheetFilterFactory CreateSheetFilterFactory();

		public abstract string SheetFilterName { get; }

		protected ReportRequest InternalDeserialise<TRequest>(string value)
			where TRequest : ReportRequest
			=> JsonConvert.DeserializeObject<TRequest>(value, Converter);
	}
}
