﻿using System.Web.UI;
using Obymobi.Web.Analytics.Bridges.Implementers;
using Page = Dionysos.Web.UI.Page;

namespace Obymobi.Web.Analytics.Bridges.RefinedAbstractions
{
	public class SheetFilterControlRefinedAbstraction : AnalyticsReportAbstraction
	{
		public SheetFilterControlRefinedAbstraction(IReportRequestImplementer reportRequestImplementer) : base(reportRequestImplementer)
		{
		}

		public UserControl CreateControl(Page page)
		{
			string controlName = $"~/Analytics/ReportTemplateSheets/{ReportRequestImplementer.SheetFilterName}.ascx";
			return (UserControl) page.LoadControl(controlName);
		}
	}
}
