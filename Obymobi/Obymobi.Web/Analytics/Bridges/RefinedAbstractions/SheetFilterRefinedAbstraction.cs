﻿using Obymobi.Web.Analytics.Bridges.Implementers;
using Obymobi.Web.Analytics.Factories;

namespace Obymobi.Web.Analytics.Bridges.RefinedAbstractions
{
	public class SheetFilterRefinedAbstraction : AnalyticsReportAbstraction
	{
		public SheetFilterRefinedAbstraction(IReportRequestImplementer reportRequestImplementer) : base(reportRequestImplementer)
		{
		}

		public ISheetFilterFactory CreateSheetFilterFactory() => ReportRequestImplementer.CreateSheetFilterFactory();
	}
}
