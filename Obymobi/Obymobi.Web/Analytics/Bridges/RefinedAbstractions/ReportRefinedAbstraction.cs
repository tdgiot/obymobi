﻿using Obymobi.Web.Analytics.Bridges.Implementers;
using Obymobi.Web.Analytics.Requests.RequestBase;

namespace Obymobi.Web.Analytics.Bridges.RefinedAbstractions
{
	public class ReportRefinedAbstraction : AnalyticsReportAbstraction
	{
		public ReportRequest CreateReportRequest() => ReportRequestImplementer.Create();

		public ReportRequest CreateReportRequest(string value) => ReportRequestImplementer.Deserialise(value);

		public ReportRefinedAbstraction(IReportRequestImplementer reportRequestImplementer) : base(reportRequestImplementer)
		{
		}
	}
}
