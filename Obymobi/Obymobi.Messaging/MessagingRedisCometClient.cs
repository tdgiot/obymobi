﻿//using System;
//using System.Collections.Concurrent;
//using System.Threading.Tasks;
//using Jil;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Serialization;
//using Obymobi.Logic.Comet;
//using Obymobi.Logic.Comet.Interfaces;
//using Obymobi.Logic.Model;
//using StackExchange.Redis;

//namespace Obymobi.Messaging
//{
//    public class MessagingRedisCometClient : CometClient
//    {
//        private readonly JsonSerializerSettings jsonSettingsCamelCase;
//        private readonly string host;
//        private ConnectionMultiplexer redis;
//        private ISubscriber sub;

//        private BlockingCollection<NetmessageContainer> messageQueue = new BlockingCollection<NetmessageContainer>();

//        public MessagingRedisCometClient(ICometClientEventListener eventListener, string host) : base(eventListener)
//        {
//            this.host = host;

//            jsonSettingsCamelCase = new JsonSerializerSettings();
//            jsonSettingsCamelCase.NullValueHandling = NullValueHandling.Ignore;
//            jsonSettingsCamelCase.ContractResolver = new CamelCasePropertyNamesContractResolver();
//        }

//        public override void Connect()
//        {
//            if (this.redis == null)
//            {
//                this.redis = ConnectionMultiplexer.Connect(this.host);
//                this.sub = this.redis.GetSubscriber();

//                SetConnected(true);
//                IsAuthenticated = true;
//            }

//            messageQueue = new BlockingCollection<NetmessageContainer>();

//            Task.Factory.StartNew(ConsumeQueue, TaskCreationOptions.LongRunning);
//        }

//        public override void Disconnect()
//        {
//            if (!this.messageQueue.IsAddingCompleted)
//            {
//                this.messageQueue.CompleteAdding();
//            }

//            if (this.redis.IsConnected)
//            {
//                SetConnected(false);

//                this.redis.Close();
//                this.redis = null;
//            }
//        }

//        public override void Authenticate(string identifier, string salt)
//        {   
//        }

//        public override void Pong()
//        {
//        }

//        public override void SetAuthenticated(bool isAuthenticated)
//        {
//        }

//        public override void TransmitMessage(Netmessage message)
//        {
//            if (string.IsNullOrWhiteSpace(message.ReceiverIdentifier))
//            {
//                WriteToLog("Unable to transmit message. No receiver identifier specified.");
//                return;
//            }

//            NetmessageContainer container = new NetmessageContainer
//                                            {
//                                                Instance = "Extern",
//                                                Guid = string.IsNullOrWhiteSpace(message.Guid) ? Guid.NewGuid().ToString("D") : message.Guid,
//                                                Type = message.MessageType,
//                                                Receiver = message.ReceiverIdentifier,
//                                                Sender = message.SenderIdentifier,
//                                                Data = JsonConvert.SerializeObject(message, this.jsonSettingsCamelCase)
//                                            };

//            this.messageQueue.Add(container);
//        }

//        private void ConsumeQueue()
//        {
//            WriteToLog("ConsumeQueue Starting");

//            while (!this.messageQueue.IsAddingCompleted)
//            {
//                NetmessageContainer container;
//                if (this.messageQueue.TryTake(out container) && container != null)
//                {
//                    try
//                    {
//                        this.sub.Publish("scaleout:SendMessage", JSON.Serialize(container, Options.IncludeInherited));
//                    }
//                    catch (Exception ex)
//                    {
//                        WriteToLog("ConsumeQueue Exception: {0}", ex.Message);
//                    }
//                }
//            }

//            WriteToLog("ConsumeQueue Finished");
//        }
//    }
//}
