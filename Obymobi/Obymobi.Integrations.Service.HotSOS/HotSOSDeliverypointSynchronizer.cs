﻿using System.Linq;
using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Integrations;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Integrations.Service.HotSOS
{
    public class HotSOSDeliverypointSynchronizer : ExternalDeliverypointSynchronizer
    {
        public HotSOSDeliverypointSynchronizer(ExternalSystemEntity externalSystemEntity) : base(externalSystemEntity)
        {
        }

        public void LinkToDeliverypoints()
        {
            ExternalDeliverypointCollection externalDeliverypointCollection = this.externalSystemEntity.ExternalDeliverypointCollection;

            foreach (ExternalDeliverypointEntity externalDeliverypointEntity in externalDeliverypointCollection)
            {
                if (externalDeliverypointEntity.Id.IsNullOrWhiteSpace())
                    continue;

                PredicateExpression filter = new PredicateExpression();
                filter.Add(DeliverypointFields.Number == externalDeliverypointEntity.Id);
                filter.Add(DeliverypointFields.CompanyId == this.externalSystemEntity.CompanyId);

                PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointEntity);
                prefetch.Add(DeliverypointEntity.PrefetchPathDeliverypointExternalDeliverypointCollection);

                DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
                deliverypointCollection.GetMulti(filter);

                foreach (DeliverypointEntity deliverypointEntity in deliverypointCollection)
                {
                    DeliverypointExternalDeliverypointEntity deliverypointExternalDeliverypointEntity = deliverypointEntity.DeliverypointExternalDeliverypointCollection.FirstOrDefault(x => x.ExternalDeliverypointId == externalDeliverypointEntity.ExternalDeliverypointId);
                    if (deliverypointExternalDeliverypointEntity == null)
                    {
                        deliverypointExternalDeliverypointEntity = new DeliverypointExternalDeliverypointEntity();
                        deliverypointExternalDeliverypointEntity.DeliverypointId = deliverypointEntity.DeliverypointId;
                        deliverypointExternalDeliverypointEntity.ExternalDeliverypointId = externalDeliverypointEntity.ExternalDeliverypointId;
                        deliverypointExternalDeliverypointEntity.Save();
                    }
                }
            }
        }
    }
}
