﻿using System;
using System.Data;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Integrations.Service.HotSOS;
using Obymobi.Integrations.Service.HotSOS.MTechAPI;
using Obymobi.Interfaces;
using Obymobi.Logging;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.Routing.Requests;
using Obymobi.Logic.Routing.UseCases;

namespace Obymobi.Integrations.Service.HotSOS.Routing
{
    public class HandleHotSOSServiceOrderRoutestepUseCase : HandleRoutestepUseCaseBase<HandleRoutestepRequest>
    {
        public override void HandleRoutestep(HandleRoutestepRequest request)
        {
            int orderRoutestephandlerEntityId = request.OrderRoutestephandlerEntityId;

            var orderRoutestephandlerEntity = new OrderRoutestephandlerEntity(orderRoutestephandlerEntityId);
            if (!orderRoutestephandlerEntity.IsNew)
            {
                try
                {
                    PrefetchPath prefetch = new PrefetchPath(EntityType.OrderEntity);
                    prefetch.Add(OrderEntity.PrefetchPathDeliverypointEntity);
                    prefetch.Add(OrderEntity.PrefetchPathOrderitemCollection).SubPath.Add(OrderitemEntity.PrefetchPathProductEntity);

                    var orderEntity = new OrderEntity(orderRoutestephandlerEntity.OrderId, prefetch);
                    if (!orderEntity.IsNew)
                    {
                        // Create a HotSOS service order based on the current order
                        HotSOSConnector connector = new HotSOSConnector(orderEntity.CompanyId);
                        ServiceOrder serviceOrder = connector.CreateServiceOrder(orderEntity);

                        orderEntity.HotSOSServiceOrderId = serviceOrder.ID;
                        orderEntity.Save();

                        orderRoutestephandlerEntity.StatusAsEnum = OrderRoutestephandlerStatus.Completed;
                    }
                }
                catch (ExternalSystemException esEx)
                {
                    orderRoutestephandlerEntity.StatusAsEnum = OrderRoutestephandlerStatus.Failed;
                    orderRoutestephandlerEntity.ErrorCodeAsEnum = esEx.ErrorType;
                    orderRoutestephandlerEntity.OrderEntity.ErrorText = string.Format("An external system exception was thrown whilst trying to create the service order. Exception {0}", esEx.Message);
                    Exceptionlogger.CreateExceptionlog(esEx);
                }
                catch (Exception ex)
                {
                    orderRoutestephandlerEntity.StatusAsEnum = OrderRoutestephandlerStatus.Failed;
                    orderRoutestephandlerEntity.OrderEntity.ErrorText = string.Format("An exception was thrown whilst trying to create the service order. Exception {0}", ex.Message);
                    Exceptionlogger.CreateExceptionlog(ex);
                }
                finally
                {
                    // Create a new transaction
                    var transaction = new Transaction(IsolationLevel.ReadCommitted, nameof(HandleHotSOSServiceOrderRoutestepUseCase));

                    // Start working with the transaction
                    try
                    {
                        orderRoutestephandlerEntity.AddToTransaction(transaction);
                        orderRoutestephandlerEntity.Save();

                        // Commit the transaction
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        // Something went wrong, rollback the full transaction
                        transaction.Rollback();

                        Exceptionlogger.CreateExceptionlog(ex);
                        throw ex;
                    }
                    finally
                    {
                        // Dispose the Transaction to free the DB connection
                        transaction.Dispose();
                    }
                }
            }
        }
    }
}
