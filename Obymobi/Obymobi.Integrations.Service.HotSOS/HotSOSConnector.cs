﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Integrations;
using Obymobi.Integrations.Interfaces;
using Obymobi.Integrations.Service.HotSOS.MTechAPI;
using Obymobi.Logging;

namespace Obymobi.Integrations.Service.HotSOS
{
    public class HotSOSConnector : ExternalSystemConnector, IExternalProductSynchronizer, IExternalDeliverypointSynchronizer
    {
        #region Fields

        private readonly MTechAPIClient client = null;
        
        private readonly ExternalProductSynchronizer externalProductSynchronizer;
        private readonly ExternalDeliverypointSynchronizer externalDeliverypointSynchronizer;

        #endregion

        #region Constructors

        public HotSOSConnector(int companyId) : base(companyId, ExternalSystemType.HotSOS)
        {
            this.externalProductSynchronizer = new ExternalProductSynchronizer(this.externalSystemEntity);
            this.externalDeliverypointSynchronizer = new HotSOSDeliverypointSynchronizer(this.externalSystemEntity);

            if (TEST_MODE)
            {
                this.client = new MTechAPIClient();
                this.client.ClientCredentials.UserName.UserName = "MBH_Crave";
                this.client.ClientCredentials.UserName.Password = "Montage2018!@";
                this.client.Endpoint.Address = new EndpointAddress("https://na4.m-tech.com/hotsos/app");
            }
            else
            {
                this.client = new MTechAPIClient();
                this.client.ClientCredentials.UserName.UserName = this.externalSystemEntity.StringValue1;
                this.client.ClientCredentials.UserName.Password = this.externalSystemEntity.StringValue2;
                this.client.Endpoint.Address = new EndpointAddress(this.externalSystemEntity.StringValue3);
            }
        }

        #endregion

        #region Properties

        public ExternalProductSynchronizer ExternalProductSynchronizer
        {
            get 
            { 
                return this.externalProductSynchronizer; 
            }
        }

        public ExternalDeliverypointSynchronizer ExternalDeliverypointSynchronizer
        {
            get
            {
                return this.externalDeliverypointSynchronizer;
            }
        }

        #endregion

        #region Methods

        public override bool HasValidConfiguration()
        {
            return (
                        !this.externalSystemEntity.StringValue1.IsNullOrWhiteSpace() &&
                        !this.externalSystemEntity.StringValue2.IsNullOrWhiteSpace() && 
                        !this.externalSystemEntity.StringValue3.IsNullOrWhiteSpace()
                   );
        }

        public IEnumerable<ExternalProduct> GetExternalProducts()
        {
            List<ExternalProduct> externalProducts = new List<ExternalProduct>();

            try
            {
                Dictionary<string, Issue> issues = new Dictionary<string, Issue>();
                ApiObjectCollection getIssueCollectionResponse = null;

                while (getIssueCollectionResponse == null || getIssueCollectionResponse.Items.Length == 20)
                {
                    // Create the request
                    GetIssueCollection getIssueCollectionRequest = new GetIssueCollection();

                    // See if we already have a response from an earlier iteration
                    // if so, use the ID of the previous response
                    if (getIssueCollectionResponse != null)
                        getIssueCollectionRequest.ID = getIssueCollectionResponse.ID;
                    else
                        getIssueCollectionRequest.nameLike = "%";

                    // Get the response from the HotSOS API
                    getIssueCollectionResponse = this.client.get(getIssueCollectionRequest) as ApiObjectCollection;

                    // Process the response
                    if (getIssueCollectionResponse != null && getIssueCollectionResponse.Items != null && getIssueCollectionResponse.Items.Length > 0)
                    {
                        foreach (Issue issue in getIssueCollectionResponse.Items)
                        {
                            if (!issues.ContainsKey(issue.ID))
                                issues.Add(issue.ID, issue);
                        }
                    }
                }

                foreach (string issueId in issues.Keys)
                {
                    Issue issue = issues[issueId];

                    ExternalProduct externalProduct = new ExternalProduct
                    {
                        Id = issue.ID,
                        Name = issue.Name,
                        Price = 0
                    };
                    externalProducts.Add(externalProduct);
                }
            }
            catch (FaultException<apiFault> faultException)
            {
                Exceptionlogger.CreateExceptionlog(faultException);
            }
            catch (Exception exception)
            {
                Exceptionlogger.CreateExceptionlog(exception);
            }

            return externalProducts;
        }

        /// <remarks>
        /// The GetRoomCollection API call from HotSOS only allows fuzzy search with the nameLike parameter and no pagination.
        /// The is not very convenient for us, because we would like to get the complete list of rooms to synchronize.
        /// A request for change has been posted at the HotSOS developers.
        /// </remarks>
        public IEnumerable<ExternalDeliverypoint> GetExternalDeliverypoints()
        {
            List<ExternalDeliverypoint> externalDeliverypoints = new List<ExternalDeliverypoint>();

            try
            {
                Dictionary<string, Room> rooms = new Dictionary<string, Room>();
                ApiObjectCollection getRoomCollectionResponse = null;

                // Create the request
                GetRoomCollection getRoomCollectionRequest = new GetRoomCollection();
                getRoomCollectionRequest.nameLike = "%";

                getRoomCollectionResponse = this.client.get(getRoomCollectionRequest) as ApiObjectCollection;

                // Process the response
                if (getRoomCollectionResponse != null && getRoomCollectionResponse.Items != null && getRoomCollectionResponse.Items.Length > 0)
                {
                    foreach (Room room in getRoomCollectionResponse.Items)
                    {
                        if (!rooms.ContainsKey(room.ID))
                            rooms.Add(room.ID, room);
                    }
                }

                foreach (string roomId in rooms.Keys)
                {
                    Room room = rooms[roomId];

                    ExternalDeliverypoint externalDeliverypoint = new ExternalDeliverypoint
                    {
                        Id = room.ID,
                        Name = room.Name,
                        StringValue1 = room.RoomNumber
                    };
                    externalDeliverypoints.Add(externalDeliverypoint);
                }
            }
            catch (FaultException<apiFault> faultException)
            {
                Exceptionlogger.CreateExceptionlog(faultException);
            }
            catch (Exception exception)
            {
                Exceptionlogger.CreateExceptionlog(exception);
            }

            return externalDeliverypoints;
        }

        public ServiceOrder CreateServiceOrder(OrderEntity orderEntity)
        {
            ExternalDeliverypointEntity externalDeliverypointEntity = this.GetExternalDeliverypointEntityForOrder(orderEntity);
            ExternalProductEntity externalProductEntity = this.GetExternalProductEntityForOrder(orderEntity);

            return this.CreateServiceOrder(externalDeliverypointEntity.Id, externalProductEntity.Id);
        }

        public ServiceOrder CreateServiceOrder(string roomId, string issueId)
        {
            ServiceOrder serviceOrder = null;

            try
            {
                Issue issue = new Issue();
                issue.ID = issueId;

                Room room = new Room();
                room.ID = roomId;

                // Optional
                //MTechAPI.User user = new MTechAPI.User();
                //user.ID = "1";

                // Optional
                //MTechAPI.Reservation reservation = new MTechAPI.Reservation();
                //reservation.ID = "1";

                serviceOrder = new ServiceOrder();
                serviceOrder.Issue = issue;
                serviceOrder.Location = room;
                //serviceOrder.ActionTime = DateTime.UtcNow;    // Optional
                //serviceOrder.AssignedTo = user;               // Optional
                //serviceOrder.ExtendedAttributes =             // Optional
                //serviceOrder.NewRemark =                      // Optional
                //serviceOrder.Priority = 1;                    // Optional
                //serviceOrder.Reservation = reservation;       // Optional
                //serviceOrder.Trade = "housekeeping";          // Optional

                serviceOrder = this.client.send(serviceOrder) as ServiceOrder;
            }
            catch (FaultException<apiFault> faultException)
            {
                Exceptionlogger.CreateExceptionlog(faultException);
                throw;
            }
            catch (Exception exception)
            {
                Exceptionlogger.CreateExceptionlog(exception);
                throw;
            }

            return serviceOrder;
        }

        #endregion
    }
}
