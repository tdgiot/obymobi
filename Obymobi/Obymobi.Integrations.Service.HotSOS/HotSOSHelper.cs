﻿using Obymobi.Data.EntityClasses;

namespace Obymobi.Integrations.Service.HotSOS
{
    public class HotSOSHelper
    {
        /// <summary>
        /// Processes the battery notification for Request service and Console
        /// </summary>
        /// <param name="client">The client.</param>
        public static bool WriteOutOfChargeNotification(ClientEntity clientEntity)
        {
            bool processed = false;

            if (!clientEntity.DeliverypointGroupId.HasValue)
            {
                // Client not connected to a deliverypointgroup
            }
            else if (!clientEntity.DeliverypointgroupEntity.HotSOSBatteryLowProductId.HasValue)
            {
                // No battery low product configured
            }
            else if (!clientEntity.DeliverypointId.HasValue)
            {
                // Client not connected to a deliverypoint
            }
            else
            {
                try
                {
                    HotSOSConnector hotSOSConnector = new HotSOSConnector(clientEntity.CompanyId);

                    ExternalDeliverypointEntity externalDeliverypointEntity = hotSOSConnector.GetExternalDeliverypointEntity(clientEntity.DeliverypointId.Value);
                    ExternalProductEntity externalProductEntity = hotSOSConnector.GetExternalProductEntity(clientEntity.DeliverypointgroupEntity.HotSOSBatteryLowProductEntity.ProductId);

                    if (hotSOSConnector.HasValidConfiguration())
                    {
                        hotSOSConnector.CreateServiceOrder(externalDeliverypointEntity.Id, externalProductEntity.Id);
                        processed = true;
                    }
                }
                catch { }
            }

            return processed;
        }
    }
}
