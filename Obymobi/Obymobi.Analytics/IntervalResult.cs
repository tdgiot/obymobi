﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics
{
    public class IntervalResult<TResultType>
    {
        public IntervalResult(DateTime fromUtc, DateTime tillUtc, TResultType result)
        {
            this.FromUtc = fromUtc;
            this.TillUtc = tillUtc;
            this.Result = result;
        }

        public DateTime FromUtc { get; private set;  }
        public DateTime TillUtc { get; private set; }
        public TResultType Result { get; private set; }
    }
}
