﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics.KpiFunctions
{
    class FunctionContainer<TFilter> where TFilter : IAnalyticsFilter
    {
        public string FunctionName;
        public TFilter Filter;
        public List<object[]> Result;        
    }
}
