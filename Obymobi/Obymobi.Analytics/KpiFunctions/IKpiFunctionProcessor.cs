﻿using System;
using System.Collections.Generic;

namespace Obymobi.Analytics.KpiFunctions
{
    public interface IKpiFunctionProcessor
    {
        bool CanHandleFunction(string functionName);
        List<object[]> PrepareAndExecuteFunction(string functionName, string parameterSets, DateTime fromUtc, DateTime tillUtc, out IAnalyticsFilter filter);
        IAnalyticsFilter PrepareFilter(string parameterSets, DateTime fromUtc, DateTime tillUtc);
    }
}