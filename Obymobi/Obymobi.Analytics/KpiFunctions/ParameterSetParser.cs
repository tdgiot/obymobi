﻿using Dionysos;
using Obymobi.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Obymobi.Analytics.KpiFunctions
{
    public static class ParameterSetParser
    {
        private static readonly string parameterSetPattern = @"\s?(?<ParameterName>.*?)\[(?<Values>.*?)\];?";

        public static void ParseFilterValues(IAnalyticsFilter filterObject, string parameterSets)
        {
            // Match ParameterSets
            try
            {
                MatchCollection matches = Regex.Matches(parameterSets, ParameterSetParser.parameterSetPattern);

                string name = string.Empty;
                foreach (Match match in matches)
                {
                    try
                    {
                        name = "Couldn't parse name";
                        name = match.Groups["ParameterName"].Value;
                        string values = match.Groups["Values"].Value;

                        if (name.Equals("FriendlyName"))
                            continue; // Was only implemented for the Temp Analytics Gatherer.

                        if (name.Equals("OrderTypesInt"))
                            throw new ArgumentException("OrderTypesInt can't be set manually, use OrderTypes with the string values of OrderTypes");

                        var property = filterObject.GetType().GetProperty(name);
                        if (property == null)
                            throw new ArgumentException("Unknown ParameterName (or invalid syntax): " + name);

                        var propertyValue = property.GetValue(filterObject);

                        if (propertyValue == null)
                            throw new Exception("All properties of a Filter object must be initialized, the following was null: " + name);

                        if (name.Equals("IncludeFailedOrders"))
                        {
                            int i = 9;
                        }

                        if (propertyValue is IList && propertyValue.GetType().IsGenericType)
                        {
                            Type listType = ListOfWhat(propertyValue);
                            if (listType == typeof(int))
                            {
                                AddValuesToList(values, (List<int>)propertyValue);
                            }
                            else if (listType == typeof(string))
                                AddValuesToList(values, (List<string>)propertyValue);
                            else if (listType == typeof(OrderType))
                                AddOrderTypesToList(values, (List<OrderType>)propertyValue);
                            else
                                throw new NotImplementedException(string.Format("Lists of type '{0}' are not supported.", listType.ToString()));
                        }
                        else
                        {
                            property.SetValue(filterObject, Convert.ChangeType(values, property.PropertyType, CultureInfo.InvariantCulture));
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Failed to parse ParameterSet '{0}': {1}".FormatSafe(name, ex.Message), ex);
                    }
                }

                // Add childeren of categories if requested (yes, the format isn't the most pretty, 
                // but being generic means paying a price here and there (could have extended the function format, but decided this)
                //if (filterObject.CategoryIds.Count > 0 && filterObject.CategoryIds.Contains(-1))
                //{
                //    List<int> childCategoryIds = new List<int>();
                //    foreach (int categoryId in filterObject.CategoryIds)
                //    {
                //        var category = new CategoryEntity(categoryId);
                //        childCategoryIds.AddRange(category.GetChildrenCategoryIds());
                //    }
                //    filterObject.CategoryIds.AddRange(childCategoryIds);
                //}


            }
            catch (Exception ex)
            {
                throw new Exception("Failed to SetFilterValues: " + ex.Message, ex);
            }
        }

        private static Type ListOfWhat(Object list)
        {
            return ListOfWhat2((dynamic)list);
        }

        private static Type ListOfWhat2<T>(IList<T> list)
        {
            return typeof(T);
        }

        private static void AddValuesToList<T>(string values, List<T> list)
        {
            if (values.Length == 0)
                return;

            var valuesSplit = values.Split(new char[] { ',' }).Select(x => x.Trim());
            Type type = list.GetType().GetGenericArguments()[0];
            foreach (var value in valuesSplit)
            {
                try
                {
                    list.Add((T)Convert.ChangeType(value, type, CultureInfo.InvariantCulture));
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("Value '{0}' can't be parsed to {1}: '{2}'", value, type, ex.Message));
                }
            }
        }

        private static void AddOrderTypesToList(string values, List<OrderType> list)
        {
            if (values.Length == 0)
                return;

            list.AddRange(values.Split(new char[] { ',' }).Select(x => (OrderType)Enum.Parse(typeof(OrderType), x))); // Don't you love one-liners?
        }
    }
}
