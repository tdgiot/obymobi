﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Obymobi.Analytics.KpiFunctions
{
    public abstract class KpiFunctionProcessorBase<TKpiRetriever> : IKpiFunctionProcessor
    {
        private IChildCategoryFetcher childCategoryFetcher;
        protected TKpiRetriever kpiRetriever;
        private int? companyIdCheck;
        private static Dictionary<string, Dictionary<string, MethodInfo>> CachedFunctions = new Dictionary<string, Dictionary<string, MethodInfo>>();        

        
        public KpiFunctionProcessorBase(TKpiRetriever kpiRetriever, IChildCategoryFetcher childCategoryFetcher, int? companyIdCheck = null)
        {
            // GAKR The Child Category Fetcher isn't very pretty - but since we need to be able to fetch them during
            // processing of all the Regex shit I couldn't think of a nicer way quick enough. Feel free to improve.
            this.childCategoryFetcher = childCategoryFetcher;
            this.companyIdCheck = companyIdCheck;
            this.kpiRetriever = kpiRetriever;
        }

        /// <summary>
        /// Determines if the Function Process can handle this Function
        /// </summary>
        /// <param name="functionName"></param>
        /// <returns></returns>
        public virtual bool CanHandleFunction(string functionName)
        {
            return this.GetFunctionsCached().ContainsKey(functionName);
        }

        /// <summary>
        /// Gives the result of a function (can be 1 or multiple values (columns) or lines
        /// As generic as possible, used this instead of DataTable
        /// We expected that you return multipline lines with string column headers in the first object[].
        /// </summary>
        /// <param name="functionName"></param>
        /// <returns></returns>
        public List<object[]> PrepareAndExecuteFunction(string functionName, string parameterSets, DateTime fromUtc, DateTime tillUtc, out IAnalyticsFilter filter)
        {
            filter = this.PrepareFilter(parameterSets, fromUtc, tillUtc);

            List<object[]> toReturn;
            if (!this.ExecuteFunctionNonGeneric(functionName, filter, out toReturn))
            {
                toReturn = ExecuteFunction(functionName, filter);
            }

            return toReturn;
        }

        protected List<object[]> ExecuteFunction(string functionName, IAnalyticsFilter filter)
        {
            List<object[]> toReturn;
            // Get the function execute
            var functions = this.GetFunctionsCached();
            MethodInfo method;
            if (functions.TryGetValue(functionName, out method))
            {
                toReturn = new List<object[]>();
                var result = method.Invoke(this.kpiRetriever, new object[] { filter });
                toReturn.Add(new object[] { result });
            }
            else
                throw new NotImplementedException(string.Format("Function not availabe: {0} on {1}", functionName, method));
            return toReturn;
        }

        public IAnalyticsFilter PrepareFilter(string parameterSets, DateTime fromUtc, DateTime tillUtc)
        {
            var filter = this.CreateBaseFilter();
            filter.FromUtc = fromUtc;
            filter.TillUtc = tillUtc;
            ParameterSetParser.ParseFilterValues(filter, parameterSets);

            // Validate Company Id if given
            this.ValidateCompanyId(filter);

            // Post-process CategoryId's (i.e. Add childeren if required)
            AppendChildCategoryIds(filter);

            return filter;
        }

        private void AppendChildCategoryIds(IAnalyticsFilter filter)
        {
            if (filter.CategoryIds.Contains(0))
            {
                filter.CategoryIds.Remove(0);
                filter.CategoryIds.AddRange(this.childCategoryFetcher.GetChildCategoryIds(filter.CategoryIds));
            }

            if (filter.ExcludeCategoryIds.Contains(0))
            {
                filter.CategoryIds.Remove(0);
                filter.CategoryIds.AddRange(this.childCategoryFetcher.GetChildCategoryIds(filter.ExcludeCategoryIds));
            }
        }

        private void ValidateCompanyId(IAnalyticsFilter filter)
        {
            if (this.companyIdCheck.HasValue)
            {
                string exceptionText = string.Empty;
                if (filter.CompanyIds == null || filter.CompanyIds.Count == 0)
                    exceptionText = "No CompanyIds were provided.";
                else if (filter.CompanyIds.Count > 1)
                    exceptionText = "More than 1 CompanyId was provided.";
                else if (!filter.CompanyIds.Contains(this.companyIdCheck.Value))
                    exceptionText = "An invalid CompanyId was provided.";

                if (exceptionText.Length > 0)
                {
                    string companyIds = (filter.CompanyIds != null && filter.CompanyIds.Count > 0) ? string.Join(",", filter.CompanyIds) : "None";
                    throw new Exception(string.Format("Filter parsing failed due to CompanyId restrictions. Expected: '{0}', Provided: '{1}'", this.companyIdCheck, companyIds));
                }
            }
        }

        protected abstract bool ExecuteFunctionNonGeneric(string functionName, IAnalyticsFilter filter, out List<object[]> result);

        protected abstract IAnalyticsFilter CreateBaseFilter();

        protected Dictionary<string, MethodInfo> GetFunctionsCached()
        {
            lock(CachedFunctions)
            {
                var kpiRetrieverType = this.kpiRetriever.GetType();
                string fullName = kpiRetrieverType.FullName;
                if (!CachedFunctions.ContainsKey(fullName))
                {
                    Dictionary<string, MethodInfo> functions = new Dictionary<string, MethodInfo>();
                    var methods = kpiRetrieverType.GetMethods(BindingFlags.Instance | BindingFlags.Public); // http://stackoverflow.com/a/5030548/2104 / http://msdn.microsoft.com/en-us/library/system.reflection.bindingflags.aspx
                    foreach (var method in methods)
                    {
                        if (method.Name.StartsWith("Get") && method.GetParameters().Length == 1)
                        {
                            // Remove 'Get'
                            functions.Add(method.Name.Substring(3), method);
                        }
                    }
                    CachedFunctions.Add(fullName, functions);
                }

                return CachedFunctions[fullName];
            }
        }



        ///// <summary>
        ///// Using this method you can queue function for execution (as some sources like Google work quicker when batched)
        ///// You need to call RunQueue to run the complete queue
        ///// </summary>
        ///// <param name="functionName"></param>
        ///// <param name="parameterSets"></param>
        //abstract object QueueFunction(string functionName, string parameterSets);

        ///// <summary>
        ///// Executes all queued functions
        ///// </summary>
        //abstract void RunQueue();
    }
}
