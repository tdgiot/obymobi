﻿using Dionysos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics
{
    public static class IntervalCreator
    {

        public static List<Tuple<DateTime, DateTime>> GetIntervals(IAnalyticsFilter filter, ReportingInterval interval)
        {
            var toReturn = new List<Tuple<DateTime, DateTime>>();

            DateTime from = filter.FromUtc;
            DateTime till = filter.TillUtc;
            DateTime currentFrom = from;

            switch (interval)
            {
                case ReportingInterval.None:
                    toReturn.Add(new Tuple<DateTime, DateTime>(from, till));
                    break;
                case ReportingInterval.Hourly:
                    currentFrom = new DateTime(currentFrom.Year, currentFrom.Month, currentFrom.Day, currentFrom.Hour, 0, 0);
                    while (currentFrom <= till)
                    {
                        DateTime currentTill = currentFrom.AddHours(1);
                        toReturn.Add(new Tuple<DateTime, DateTime>(currentFrom, currentTill));
                        currentFrom = currentTill;
                    }
                    break;
                case ReportingInterval.Daily:
                    currentFrom = DateTimeUtil.MakeBeginOfDay(currentFrom);
                    while (currentFrom <= till)
                    {
                        DateTime currentTill = currentFrom.AddDays(1);
                        toReturn.Add(new Tuple<DateTime, DateTime>(currentFrom, currentTill));
                        currentFrom = currentTill;
                    }
                    break;
                case ReportingInterval.Weekly:
                    currentFrom = DateTimeUtil.GetFirstDayOfWeek(currentFrom);
                    while (currentFrom <= till)
                    {
                        DateTime currentTill = currentFrom.AddDays(7);
                        toReturn.Add(new Tuple<DateTime, DateTime>(currentFrom, currentTill));
                        currentFrom = currentTill;
                    }
                    break;
                case ReportingInterval.Monthly:
                    currentFrom = DateTimeUtil.GetFirstDayOfMonth(currentFrom);
                    while (currentFrom <= till)
                    {                        
                        DateTime currentTill = currentFrom.AddMonths(1);
                        toReturn.Add(new Tuple<DateTime, DateTime>(currentFrom, currentTill));
                        currentFrom = currentTill;
                    }
                    break;
                default:
                    throw new NotImplementedException(interval.ToString());                    
            }

            return toReturn;
        }
    }
}
