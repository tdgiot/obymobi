﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics
{
    public interface IChildCategoryFetcher
    {
        List<int> GetChildCategoryIds(List<int> parentCategoryIds);        
    }
}
