﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics
{
    public enum ReportingInterval
    {
        /// <summary>
        /// All will be reported as one period - no intervals
        /// </summary>
        None,
        Hourly,
        Daily,
        Weekly,
        Monthly
    }
}
