﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Analytics
{
    public interface IAnalyticsFilter
    {
        DateTime FromUtc { get; set; }
        DateTime TillUtc { get; set; }
        List<int> CategoryIds { get; set; }
        List<int> ExcludeCategoryIds { get; set; }
        List<int> CompanyIds { get; set; }
    }
}
