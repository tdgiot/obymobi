﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;
using System.Globalization;
using SpreadsheetLight;
using System.IO;
using Obymobi.Analytics.GoogleAnalytics;

namespace Obymobi.Analytics.Reports
{
    public class KpiReport
    {
        KpiReportMetadata metadata;
        ReportProcessingTaskEntity reportProcessingTaskEntity;
        string googleApisJwt;

        public KpiReport(ReportProcessingTaskEntity reportProcessingTaskEntity, string googleApisJwt)
        {
            this.reportProcessingTaskEntity = reportProcessingTaskEntity;
            this.metadata = Newtonsoft.Json.JsonConvert.DeserializeObject<KpiReportMetadata>(reportProcessingTaskEntity.Filter);
            this.googleApisJwt = googleApisJwt;
        }

        public SLDocument RunReport(bool validationOnly)
        {
            if (this.reportProcessingTaskEntity.ReportProcessingTaskTemplateCollection.Count != 1)
                throw new Exception("One and only one Template has to be defined.");

            if(!this.reportProcessingTaskEntity.FromUTC.HasValue || !this.reportProcessingTaskEntity.TillUTC.HasValue)
                throw new Exception("The From and/or Till Utc dates are not defined.");

            using (MemoryStream ms = new MemoryStream(this.reportProcessingTaskEntity.ReportProcessingTaskTemplateCollection[0].TemplateFile))
            {
                SLDocument doc = null; ;
                // Try to open the file
                try
                {
                    doc = new SLDocument(ms);
                }
                catch (Exception ex)
                {
                    throw new Exception("Invalid file type: " + ex.Message);
                }

                if (validationOnly)
                    return null;

                AccountConfiguration gaConfig = AccountConfiguration.GetByCloudEnvironment(this.metadata.CloudEnvironment);

                GoogleAnalyticsKpiRetriever gaKpiRetriever = null;
                if (!this.googleApisJwt.IsNullOrWhiteSpace())
                    gaKpiRetriever = new GoogleAnalyticsKpiRetriever(this.googleApisJwt, gaConfig);

                KpiFunctionHelper functionHelper = new KpiFunctionHelper(this.metadata.CompanyId, this.metadata.IncludeChildCategories, gaKpiRetriever);

                // Validate if there are any formulas that we can use
                CultureInfo invariantCulture = CultureInfo.InvariantCulture;
                for (int iRow = 1; iRow < 100; iRow++)
                {
                    for (int iColumn = 1; iColumn < 100; iColumn++)
                    {
                        string value = doc.GetCellValueAsString(iRow, iColumn);
                        // Contains is the first simple check before we do the full blown RegEX
                        try
                        {
                            if (!value.IsNullOrWhiteSpace() && value.Contains("(") && KpiFunctionHelper.IsValid(value))
                            {
                                string actualFilter;
                                string result = functionHelper.GetValueAsString(value, this.reportProcessingTaskEntity.FromUTC.Value, this.reportProcessingTaskEntity.TillUTC.Value, out actualFilter);

                                if (result.Contains("~~"))
                                {
                                    // 'CSV'
                                    string[] lines = result.SplitLines();
                                    for (int iCsvLine = 0; iCsvLine < lines.Length; iCsvLine++)
                                    {
                                        string[] columns = lines[iCsvLine].Split("~~", StringSplitOptions.None);
                                        for (int iCsvColumn = 0; iCsvColumn < columns.Length; iCsvColumn++)
                                        {
                                            // Dirty hard coding. YAGNI - Make generic when required.
                                            string stringValue = columns[iCsvColumn];
                                            int intValue;
                                            decimal decimalValue;
                                            if (stringValue.Contains(".") && decimal.TryParse(stringValue, out decimalValue))
                                                doc.SetCellValue(iRow + iCsvLine, iColumn + iCsvColumn, Convert.ToDecimal(columns[iCsvColumn], invariantCulture));
                                            else if (int.TryParse(stringValue, out intValue))
                                                doc.SetCellValue(iRow + iCsvLine, iColumn + iCsvColumn, intValue);
                                            else
                                                doc.SetCellValue(iRow + iCsvLine, iColumn + iCsvColumn, stringValue);
                                        }
                                    }
                                }
                                else
                                {
                                    doc.SetCellValueNumeric(iRow, iColumn, result);
                                }

                                if (this.metadata.IncludeActualFilters)
                                {
                                    var comment = doc.CreateComment();
                                    comment.Author = "Auto generated";
                                    comment.AutoSize = true;
                                    StringBuilder sb = new StringBuilder();
                                    sb.AppendFormatLine("Function: {0}", value);
                                    sb.AppendFormatLine("Filter:");
                                    sb.AppendFormatLine(actualFilter);
                                    comment.SetText(sb.ToString());
                                    doc.InsertComment(iRow, iColumn, comment);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            doc.SetCellValue(iRow, iColumn, "FAILED");
                            // http://spreadsheetlight.com/downloads/samplecode/CellComments.cs
                            var comment = doc.CreateComment();
                            comment.Author = "Auto generated";
                            comment.AutoSize = true;
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormatLine("Failure for function: " + value);

                            if (this.metadata.IncludeStackTracesWithErrors)
                                sb.AppendFormatLine(ex.ProcessStackTrace(true));
                            else
                                sb.AppendFormatLine("Exception: " + ex.GetAllMessages());

                            SLStyle style = doc.CreateStyle();
                            style.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, System.Drawing.Color.DarkRed, System.Drawing.Color.DarkRed);
                            style.Font.FontColor = System.Drawing.Color.White;
                            doc.SetCellStyle(iRow, iColumn, style);

                            comment.SetText(sb.ToString());
                            doc.InsertComment(iRow, iColumn, comment);
                        }

                    }
                }

                // Validate that the Formula's are valid
                return doc;
            }
        }

        public bool RunReport(out SLDocument sl)
        {
            throw new NotImplementedException();
        }
    }
}
