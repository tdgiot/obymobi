﻿using Obymobi.Analytics.GoogleAnalytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickTestsConsole
{
    public class Program
    {
        static void Main(string[] args)
        {
            Task.Factory.StartNew(() => Test());
            Console.ReadLine();            
        }

        public async static void Test()
        {
            ScreenViewFilter filter = new ScreenViewFilter(222);
            filter.From = new DateTime(2016, 4, 10);
            filter.Till = new DateTime(2016, 5, 10);

            //var result = await GoogleAnalyticsKpiReporting.GetEventCount(filter, AccountConfiguration.Test);
            //Console.WriteLine(result);

            //result = await GoogleAnalyticsKpiReporting.GetScreenViewCount(filter, AccountConfiguration.Test);
            //Console.WriteLine(result);

            //filter.SiteIds.Add(97);
            //result = await GoogleAnalyticsKpiReporting.GetScreenViewCount(filter, AccountConfiguration.Test);
            //Console.WriteLine(result);

            //filter.SiteIds.Add(98);
            //result = await GoogleAnalyticsKpiReporting.GetScreenViewCount(filter, AccountConfiguration.Test);
            //Console.WriteLine(result);

            //filter.PageIds.Add(30);
            //filter.PageIds.Add(31);
            //result = await GoogleAnalyticsKpiReporting.GetScreenViewCount(filter, AccountConfiguration.Test);
            //Console.WriteLine(result);
            
            //filter.SiteIds.Clear();
            //filter.PageIds.Clear();
            //filter.CategoryIds.Add(13447); // 3334
            //result = await GoogleAnalyticsKpiReporting.GetScreenViewCount(filter, AccountConfiguration.Test);
            //Console.WriteLine(result);

            //filter.ProductIds.Add(83410); // 66
            //result = await GoogleAnalyticsKpiReporting.GetScreenViewCount(filter, AccountConfiguration.Test);
            //Console.WriteLine(result);

            //filter.ProductIds.Add(83409); // 19 == 75
            //result = await GoogleAnalyticsKpiReporting.GetScreenViewCount(filter, AccountConfiguration.Test);
            //Console.WriteLine(result);

            //filter.CategoryIds.Clear();
            //filter.ProductIds.Clear();

            //filter.ScreenNames.Add("Testing Co");
            //result = await GoogleAnalyticsKpiReporting.GetScreenViewCount(filter, AccountConfiguration.Test);
            //Console.WriteLine(result);

            //filter.ScreenNames.Add("Selina");
            //result = await GoogleAnalyticsKpiReporting.GetScreenViewCount(filter, AccountConfiguration.Test);
            //Console.WriteLine(result);

            //filter.ScreenNames.Clear();
            //filter.ScreenNames.Add("/Testing Co/advertisements/Advertisement viewed"); // 2195
            //result = await GoogleAnalyticsKpiReporting.GetScreenViewCount(filter, AccountConfiguration.Test);
            //Console.WriteLine(result);

            //filter.ScreenNames.Clear();
            //filter.TabIds.Add(344); // 85
            //result = await GoogleAnalyticsKpiReporting.GetScreenViewCount(filter, AccountConfiguration.Test);
            //Console.WriteLine(result);

            filter.TabIds.Clear();

            filter.EventCategories.Add("Advertising"); // 2225
            var result = await ReportingApiHelper.GetEventCount(filter, AccountConfiguration.Test);
            Console.WriteLine(result);

            filter.EventCategories.Add("Widgets"); // 204 = 2429
            result = await ReportingApiHelper.GetEventCount(filter, AccountConfiguration.Test);
            Console.WriteLine(result);

            filter.EventActions.Add("Advertisement clicked"); // 30
            result = await ReportingApiHelper.GetEventCount(filter, AccountConfiguration.Test);
            Console.WriteLine(result); 

            filter.EventActions.Clear();
            filter.EventActions.Add("Advertisement clicked"); // 30
            result = await ReportingApiHelper.GetEventCount(filter, AccountConfiguration.Test);
            Console.WriteLine(result);

            filter.EventCategories.Add("Advertising"); // 20
            result = await ReportingApiHelper.GetEventCount(filter, AccountConfiguration.Test);
            Console.WriteLine(result);

            filter.EventCategories.Clear();
            filter.EventActions.Clear();
            filter.EventLabels.Add("Advertisement Location/Advertisement 99-99"); // 20
            result = await ReportingApiHelper.GetEventCount(filter, AccountConfiguration.Test);
            Console.WriteLine(result);

        }

    }
}
