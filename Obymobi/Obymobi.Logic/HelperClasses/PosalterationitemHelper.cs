﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// PosalterationitemHelper class
    /// </summary>
    public class PosalterationitemHelper
    {
        /// <summary>
        /// Cleans up.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="cleanupReport">The cleanup report.</param>
        /// <param name="batchId">The batch id.</param>
        public static void CleanUp(int companyId, ref StringBuilder cleanupReport, out long batchId)
        {
            cleanupReport.AppendLine("*** START: Delete Posalterationitem entities ***");
            batchId = -1;
            // Prepate filter for company
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationitemFields.CompanyId == companyId);

            // Create collection and use for Scalar MAX
            PosalterationitemCollection pais = new PosalterationitemCollection();
            object maxBatchId = pais.GetScalar(Data.PosalterationitemFieldIndex.SynchronisationBatchId, null, AggregateFunction.Max, filter);

            // Only proceed when we have an actual maxBatchId
            if (maxBatchId != DBNull.Value && maxBatchId is long)
            {
                // Retrieve all order posalterationitems & delete them
                cleanupReport.AppendFormatLine("BatchId: {0}", maxBatchId);
                batchId = (long)maxBatchId;
                // Prepare filter for older posalterationitems
                filter = new PredicateExpression();
                filter.Add(PosalterationitemFields.CompanyId == companyId);
                filter.Add(PosalterationitemFields.SynchronisationBatchId < maxBatchId);

                pais.GetMulti(filter);

                // Delete per piece
                foreach (var pai in pais)
                {
                    cleanupReport.AppendFormatLine("ExternalPosalterationId: {0}, ExternalPosalterationoptionId: {1}",
                        pai.ExternalPosalterationId, pai.ExternalPosalterationoptionId);
                    pai.Delete();
                }
                cleanupReport.AppendFormatLine("{0} entities deleted", pais.Count);
            }
            else
                cleanupReport.AppendLine("No valid batch found");

            cleanupReport.AppendLine("*** FINISHED: Delete Posalterationitem entities ***");
        }

        /// <summary>
        /// Creates the posalterationitem model from entity.
        /// </summary>
        /// <param name="posalterationitemEntity">The posalterationitem entity.</param>
        /// <returns></returns>
        public static Posalterationitem CreatePosalterationitemModelFromEntity(PosalterationitemEntity posalterationitemEntity)
        {
            Posalterationitem posalterationitemModel = new Posalterationitem();

            posalterationitemModel.PosalterationitemId = posalterationitemEntity.PosalterationitemId;
            posalterationitemModel.ExternalId = posalterationitemEntity.ExternalId;
            posalterationitemModel.ExternalPosalterationId = posalterationitemEntity.ExternalPosalterationId;
            posalterationitemModel.ExternalPosalterationoptionId = posalterationitemEntity.ExternalPosalterationoptionId;
            posalterationitemModel.SelectOnDefault = posalterationitemEntity.SelectedOnDefault;
            posalterationitemModel.SortOrder = posalterationitemEntity.SortOrder;

            posalterationitemModel.FieldValue1 = posalterationitemEntity.FieldValue1;
            posalterationitemModel.FieldValue2 = posalterationitemEntity.FieldValue2;
            posalterationitemModel.FieldValue3 = posalterationitemEntity.FieldValue3;
            posalterationitemModel.FieldValue4 = posalterationitemEntity.FieldValue4;
            posalterationitemModel.FieldValue5 = posalterationitemEntity.FieldValue5;
            posalterationitemModel.FieldValue6 = posalterationitemEntity.FieldValue6;
            posalterationitemModel.FieldValue7 = posalterationitemEntity.FieldValue7;
            posalterationitemModel.FieldValue8 = posalterationitemEntity.FieldValue8;
            posalterationitemModel.FieldValue9 = posalterationitemEntity.FieldValue9;
            posalterationitemModel.FieldValue10 = posalterationitemEntity.FieldValue10;

            posalterationitemModel.RevenueCenter = posalterationitemEntity.RevenueCenter;

            return posalterationitemModel;
        }

        /// <summary>
        /// Gets the posalterationitems.
        /// </summary>
        /// <param name="posalterationEntity">The posalteration entity.</param>
        /// <returns></returns>
        public static PosalterationitemCollection GetPosalterationitems(PosalterationEntity posalterationEntity)
        {
            PosalterationitemCollection posalterationItems = new PosalterationitemCollection();
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationitemFields.ExternalPosalterationId == posalterationEntity.ExternalId);
            posalterationItems.GetMulti(filter);
            return posalterationItems;
        }

        /// <summary>
        /// Synchronizes the alterationitems from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="xml">The Xml string containing the serialized array of Posalterationitem instances</param>
        /// <param name="companyId">The id of the company to import the pos alterationitems for</param>
        /// <param name="batchId">The batch id.</param>
        public static void SynchronizePosalterationitems(string xml, int companyId, long batchId)
        {
            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPosalterationitemsResult.XmlIsEmpty, "Xml string containing serialized Posalterationitem array is empty.");
            else if (companyId <= 1)
                throw new ObymobiException(ImportPosalterationitemsResult.CompanyIdIsZeroOrLess, "The specified company id '{0}' is smaller than 1.", companyId);
            else
            {
                // Deserialize the xml in order to get the array of Posalterationitem instances
                Posalteration posalteration = XmlHelper.Deserialize<Posalteration>(xml);

                SynchronizePosalterationitems(posalteration, companyId, batchId);
            }
        }

        /// <summary>
        /// Synchronizes the alterationitems from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="posalteration">The posalteration</param>
        /// <param name="companyId">The id of the company to import the pos alterationitems for</param>
        /// <param name="batchId">The id of the batch</param>
        public static void SynchronizePosalterationitems(Posalteration posalteration, int companyId, long batchId, string revenueCenter = null)
        {
            // Get a PosalterationitemCollection for the specified company
            PosalterationitemCollection posalterationitemCollection = GetPosalterationitemCollection(companyId);

            // Create an EntityView for the PosalterationitemCollection
            EntityView<PosalterationitemEntity> posalterationitemView = posalterationitemCollection.DefaultView;

            // Populate the list of external id's
            var query = from c in posalterationitemView select c.ExternalId;
            List<string> posalterationitemExternalIdList = query.ToList<string>();

            // Create a filter
            PredicateExpression filter = null;

            // Walk through the Posalterationitem instances of the array
            for (int i = 0; i < posalteration.Posalterationoptions.Length; i++)
            {
                Posalterationoption posalterationoption = posalteration.Posalterationoptions[i];

                // Initialize the filter
                filter = new PredicateExpression();
                filter.Add(PosalterationitemFields.ExternalPosalterationId == posalteration.ExternalId);
                filter.Add(PosalterationitemFields.ExternalPosalterationoptionId == posalterationoption.ExternalId);
                filter.Add(PosalterationitemFields.CompanyId == companyId);

                if (!string.IsNullOrWhiteSpace(revenueCenter))
                {
                    filter.Add(PosalterationitemFields.RevenueCenter == revenueCenter);
                }

                // Set the filter to the EntityView
                posalterationitemView.Filter = filter;

                // Check whether items have been found
                if (posalterationitemView.Count == 0)
                {
                    // No pos alterationitem found in database, add

                    // Then, create a PosalterationitemEntity instance
                    // and save it to the persistent storage
                    PosalterationitemEntity posalterationitemEntity = new PosalterationitemEntity();
                    posalterationitemEntity.CompanyId = companyId;
                    posalterationitemEntity.ExternalId = string.Format("{0}-{1}", posalteration.ExternalId, posalterationoption.ExternalId);
                    posalterationitemEntity.ExternalPosalterationId = posalteration.ExternalId;
                    posalterationitemEntity.ExternalPosalterationoptionId = posalterationoption.ExternalId;
                    posalterationitemEntity.SelectedOnDefault = false;
                    posalterationitemEntity.SortOrder = 10000;
                    posalterationitemEntity.RevenueCenter = posalteration.RevenueCenter;
                    try
                    {
                        posalterationitemEntity.CreatedInBatchId = batchId;
                        posalterationitemEntity.SynchronisationBatchId = batchId;

                        if (posalterationitemEntity.Save())
                        {
                            posalterationitemEntity.Refetch();
                            posalterationitemView.RelatedCollection.Add(posalterationitemEntity);
                        }
                    }
                    catch
                    {
                        // Do not throw an exception here because it will stop the whole sync batch
                    }
                }
                else if (posalterationitemView.Count == 1)
                {
                    // Pos alterationitem found in database, update
                    PosalterationitemEntity posalterationitemEntity = posalterationitemView[0];
                    posalterationitemEntity.CompanyId = companyId;
                    posalterationitemEntity.ExternalId = string.Format("{0}-{1}", posalteration.ExternalId, posalterationoption.ExternalId);
                    posalterationitemEntity.ExternalPosalterationId = posalteration.ExternalId;
                    posalterationitemEntity.ExternalPosalterationoptionId = posalterationoption.ExternalId;
                    posalterationitemEntity.DeletedFromCms = false;
                    posalterationitemEntity.RevenueCenter = posalteration.RevenueCenter;

                    try
                    {
                        if (posalterationitemEntity.IsDirty)
                            posalterationitemEntity.UpdatedInBatchId = batchId;

                        if (!posalterationitemEntity.CreatedInBatchId.HasValue)
                            posalterationitemEntity.CreatedInBatchId = batchId;

                        posalterationitemEntity.SynchronisationBatchId = batchId;

                        if (posalterationitemEntity.Save() && posalterationitemExternalIdList.Contains(posalterationitemEntity.ExternalId))
                        {
                            // Remove the external id from the list
                            posalterationitemExternalIdList.Remove(posalterationitemEntity.ExternalId);
                        }
                    }
                    catch
                    {
                        // Do not throw an exception here because it will stop the whole sync batch
                    }
                }
                else if (posalterationitemView.Count > 1)
                {
                    // Multiple pos alterationitems found in database for the specified external id, error!
                    throw new ObymobiException(ImportPosalterationitemsResult.MultiplePosalterationitemsFound, "Multiple pos alterationitems found for the specified external pos alteration id '{0}' and pos alteration option id '{1}'.", posalteration.ExternalId, posalterationoption.ExternalId);
                }
            }

            //// Remove pos alterationitems which were not synchronized
            //if (posalterationitemExternalIdList.Count > 0)
            //{
            //    // There are some external id's which not were saved
            //    // Remove them from the database
            //    foreach (string externalId in posalterationitemExternalIdList)
            //    {
            //        PosalterationitemEntity posalterationitemEntity = GetPosalterationitemByExternalId(externalId, posalterationitemView);
            //        if (!posalterationitemEntity.IsNew)
            //        {
            //            posalterationitemEntity.Delete();
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Gets the external id of the pos alterationitem for the specified alterationitem id
        /// </summary>
        /// <param name="alterationitemId">The id of the alterationitem to get the pos alterationitem id for</param>
        /// <returns>A System.String instance containing the external id</returns>
        public static string GetPosalterationitemExternalId(int alterationitemId)
        {
            string externalId = string.Empty;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationitemFields.AlterationitemId == alterationitemId);

            RelationCollection relations = new RelationCollection();
            relations.Add(AlterationitemEntity.Relations.PosalterationitemEntityUsingPosalterationitemId);

            PosalterationitemCollection posalterationitemCollection = new PosalterationitemCollection();
            posalterationitemCollection.GetMulti(filter, relations);

            if (posalterationitemCollection.Count == 0)
            {
                // No pos alterationitem found, error
                throw new ObymobiException(GetPosalterationitemExternalIdResult.NoPosalterationitemFound, "No pos alterationitems found for alterationitem id '{0}'.", alterationitemId);
            }
            else if (posalterationitemCollection.Count == 1)
            {
                // Pos alterationitem found
                externalId = posalterationitemCollection[0].ExternalId;
            }
            else if (posalterationitemCollection.Count > 1)
            {
                // Multiple pos alterationitems found, error
                throw new ObymobiException(GetPosalterationitemExternalIdResult.MultiplePosalterationitemsFound, "Multiple pos alterationitems found for alterationitem id '{0}'.", alterationitemId);
            }

            return externalId;
        }

        /// <summary>
        /// Gets the alterationitem by posalterationitem id or a new one if none was found.
        /// </summary>
        /// <param name="posalterationitemId">The posalterationitem id.</param>
        /// <param name="alterationitemView">The alterationitem view.</param>
        /// <returns>
        /// A (new) <see cref="AlterationitemEntity"/> instance
        /// </returns>
        public static AlterationitemEntity GetAlterationitemByPosalterationitemId(int posalterationitemId, EntityView<AlterationitemEntity> alterationitemView)
        {
            // Create the AlterationitemEntity instance
            AlterationitemEntity alterationitem = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationitemFields.PosalterationitemId == posalterationitemId);

            // Set the filter to the alterationitem view
            alterationitemView.Filter = filter;

            // Check whether a alterationitem was found
            if (alterationitemView.Count == 0)
                alterationitem = new AlterationitemEntity();
            else if (alterationitemView.Count == 1)
                alterationitem = alterationitemView[0];
            else if (alterationitemView.Count > 1)
                throw new ObymobiException(SynchronizeAlterationitemsResult.MultipleAlterationitemsFound, "Multiple alterationitems found for the pos alterationitem id '{0}'.", posalterationitemId);

            return alterationitem;
        }

        /// <summary>
        /// Gets the posalterationitem by external id or a new one if none was found.
        /// </summary>
        /// <param name="externalId">The external id.</param>
        /// <param name="posalterationitemView">The posalterationitem view.</param>
        /// <returns>
        /// A (new) <see cref="PosalterationitemEntity"/> instance
        /// </returns>
        public static PosalterationitemEntity GetPosalterationitemByExternalId(string externalId, EntityView<PosalterationitemEntity> posalterationitemView)
        {
            // Create the PosalterationitemEntity instance
            PosalterationitemEntity posalterationitem = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationitemFields.ExternalId == externalId);

            // Set the filter to the POS alterationitem view
            posalterationitemView.Filter = filter;

            // Check whether a POS alterationitem was found
            if (posalterationitemView.Count == 0)
                posalterationitem = new PosalterationitemEntity();
            else if (posalterationitemView.Count == 1)
                posalterationitem = posalterationitemView[0];
            else if (posalterationitemView.Count > 1)
                throw new ObymobiException(ImportPosalterationitemsResult.MultiplePosalterationitemsFound, "Multiple pos alteration items found for external id '{0}'.", externalId);

            return posalterationitem;
        }

        /// <summary>
        /// Gets a PosalterationitemCollection for the specified company id
        /// </summary>
        /// <param name="companyId">The id of the company to get the PosalterationitemCollection for</param>
        /// <returns>An Obymobi.Data.PosalterationitemCollection instance</returns>
        public static PosalterationitemCollection GetPosalterationitemCollection(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationitemFields.CompanyId == companyId);

            PosalterationitemCollection posalterationitemCollection = new PosalterationitemCollection();
            posalterationitemCollection.GetMulti(filter);

            return posalterationitemCollection;
        }

        /// <summary>
        /// Creates a PosalterationitemEntity instance for the specified Posalterationitem instance
        /// </summary>
        /// <param name="posalterationitem">The Posalterationitem instance to create the PosalterationitemEntity instance for</param>
        /// <returns>An Obymobi.Data.EntityClasses.PosalterationitemEntity instance</returns>
        public static PosalterationitemEntity CreatePosalterationitemEntityFromModel(Posalterationitem posalterationitem)
        {
            PosalterationitemEntity posalterationitemEntity = new PosalterationitemEntity();
            posalterationitemEntity.ExternalId = posalterationitem.ExternalId;
            posalterationitemEntity.ExternalPosalterationId = posalterationitem.ExternalPosalterationId;
            posalterationitemEntity.ExternalPosalterationoptionId = posalterationitem.ExternalPosalterationoptionId;
            posalterationitemEntity.SelectedOnDefault = posalterationitem.SelectOnDefault;
            posalterationitemEntity.SortOrder = posalterationitem.SortOrder;
            posalterationitemEntity.RevenueCenter = posalterationitem.RevenueCenter;

            return posalterationitemEntity;
        }
    }
}
