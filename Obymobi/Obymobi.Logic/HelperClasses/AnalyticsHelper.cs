using Dionysos;
using Obymobi.Analytics;
using Obymobi.Analytics.Google;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Obymobi.Logic.HelperClasses
{
    public class AnalyticsHelper
    {
        public static Hit CreateHitForOrder(OrderEntity order, string eventAction, string eventLabel)
        {
            Hit hit = GetBaseHit(HitType.Event, order.AnalyticsPayLoad);

            hit.CompanyId = order.CompanyId;
            hit.CompanyName = order.CompanyName;

            hit.OrderId = order.OrderId;
            hit.OrderType = order.TypeAsEnum.ToString();

            hit.DeliverypointId = order.DeliverypointId ?? 0;
            hit.DeliverypointName = order.DeliverypointName ?? string.Empty;

            int.TryParse(order.DeliverypointNumber, out int deliveryPointNumber);
            hit.DeliverypointNumber = deliveryPointNumber;

            hit.DeliverypointGroupId = order?.DeliverypointEntity?.DeliverypointgroupId ?? 0;
            hit.DeliverypointGroupName = order.DeliverypointgroupName ?? string.Empty;

            hit.ClientId = order.ClientId ?? 0;

            hit.EventCategory = EventCategory.Transactions;
            hit.EventAction = eventAction;
            hit.EventLabel = "{0}/{1}".FormatSafe(hit.CompanyName, hit.DeliverypointGroupName);

            return hit;
        }

        public static List<string> GetTrackingIds(OrderEntity order)
        {
            if (order.AnalyticsTrackingIds.IsNullOrWhiteSpace())
            {
                return new List<string>(0);
            }

            return order.AnalyticsTrackingIds.Split(';', StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        private static Hit GetBaseHit(HitType type, string payload = "")
        {
            Hit hit = new Hit(type);
            hit.ApplicationName = Dionysos.Global.ApplicationInfo.ApplicationName;
            hit.ApplicationInstallerId = Dionysos.Global.ApplicationInfo.ApplicationName;
            hit.ApplicationVersion = Dionysos.Global.ApplicationInfo.ApplicationVersion;
            hit.TimeStamp = DateTimeUtil.ToUnixTime(DateTime.UtcNow).ToString();

            if (!payload.IsNullOrWhiteSpace())
            {
                EnrichHitWithPayload(hit, payload);
            }

            return hit;
        }

        private static void EnrichHitWithPayload(Hit hit, string payload)
        {
            // Get all properties to map payload on fields
            Dictionary<string, FieldInfo> fields = HitBase.GetFieldMappings();

            string[] items = payload.Split("|--|", StringSplitOptions.RemoveEmptyEntries);
            foreach (string item in items)
            {
                if (item.Contains("|-|"))
                {
                    string[] keyAndValue = StringUtil.Split(item, "|-|", StringSplitOptions.RemoveEmptyEntries);

                    // Find field for key & set value in the correct type
                    if (keyAndValue.Length > 1)
                    {
                        string value = keyAndValue[1];
                        if (fields.TryGetValue(keyAndValue[0], out FieldInfo fieldInfo))
                        {
                            if (fieldInfo.FieldType == typeof(string))
                            {
                                fieldInfo.SetValue(hit, value);
                            }
                            else if (fieldInfo.FieldType == typeof(int))
                            {
                                int intValue;
                                int.TryParse(value, out intValue);
                                fieldInfo.SetValue(hit, intValue);
                            }
                            else if (fieldInfo.FieldType == typeof(Double))
                            {
                                double doubleValue;
                                double.TryParse(value, out doubleValue);
                                fieldInfo.SetValue(hit, doubleValue);
                            }
                            else if (fieldInfo.FieldType == typeof(bool))
                            {
                                fieldInfo.SetValue(hit, (value.Equals("1") || value.Equals("true", StringComparison.InvariantCultureIgnoreCase)));
                            }
                            else if (fieldInfo.FieldType.IsEnum)
                            {
                                try
                                {
                                    fieldInfo.SetValue(hit, Enum.Parse(fieldInfo.FieldType, value, true));
                                }
                                catch
                                {
                                    // Skip
                                }
                            }
                            else
                            {
                                fieldInfo.SetValue(hit, value.ToString());
                            }
                        }
                    }
                }
            }
        }
    }
}
