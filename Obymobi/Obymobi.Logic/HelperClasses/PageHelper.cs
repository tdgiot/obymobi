﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Cms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    public class PageHelper
    {
        #region Enums

        public enum PageHelperResult
        {            
            UnknownPageTypeUsed = 200,
            PageTypeNotCompatibleWithPage = 201
        }

        #endregion

        #region Compatibility

        public static List<PageType> PageTypesCompatibleWithPageAndSite(PageType pageType, SiteType siteType)
        {
            List<PageType> toReturn = new List<PageType>();
            var sourcePageElements = PageTypeHelper.GetPageTypeInstance(pageType).PageTypeElements;

            foreach (var pageTypeBase in PageTypeHelper.GetPageTypes(siteType))
            {
                // Compare it's PageElements
                bool compatible = true;
                foreach (var sourcePageElement in sourcePageElements)
                {
                    var pageElement = pageTypeBase.PageTypeElements.FirstOrDefault(x => x.SystemName == sourcePageElement.SystemName);
                    if (pageElement == null ||
                        pageElement.PageTypeElementType != sourcePageElement.PageTypeElementType)
                    {
                        compatible = false;                        
                    }
                    else
                    {
                        switch (sourcePageElement.PageTypeElementType)
                        {
                            case PageElementType.SingleLineText:
                            case PageElementType.MultiLineText:
                            case PageElementType.WebView:
                            case PageElementType.YouTube:
                            case PageElementType.Map:
                            case PageElementType.Venue:
                                // Nothing to compare - They can't be configued on Template Level
                                break;
                            case PageElementType.Image:
                                ImageElement source = (ImageElement)sourcePageElement;
                                ImageElement element = (ImageElement)pageElement;
                                if (source.MediaTypeGroupPhone != element.MediaTypeGroupPhone || source.MediaTypeGroupTablet != element.MediaTypeGroupTablet)                                
                                    compatible = false;                                                                    
                                break;
                            default:
                                if (TestUtil.IsPcDeveloper)
                                    throw new NotImplementedException("Hee!! Apenkop! Ga eensch implementeren!!!!");
                                break;
                        }
                    }

                    if (!compatible)
                        break;                    
                }

                if (compatible)
                    toReturn.Add(pageTypeBase.PageType);
            }

            return toReturn;
        }

        public static bool IsPageTypeChangeCompatible(PageEntity page)
        {            
            // Get the PageType is was/is before this save                
            bool toReturn = false;
            if (page.Fields[PageFields.PageType.Name].DbValue != null)
            {
                PageType originalPageType;
                if (!EnumUtil.TryParse((int)page.Fields[PageFields.PageType.Name].DbValue, out originalPageType))
                    throw new ObymobiEntityException(PageHelperResult.UnknownPageTypeUsed, "Unknown Page Type: {0}", page.Fields[PageFields.PageType.Name].DbValue);

                var compatiblePageTypes = PageHelper.PageTypesCompatibleWithPageAndSite(originalPageType, page.SiteEntity.SiteTypeAsEnum);
                if (compatiblePageTypes.Contains(page.PageTypeAsEnum))
                {
                    toReturn = true;

                }              
            }

            return toReturn;
        }

        #endregion
    }
}
