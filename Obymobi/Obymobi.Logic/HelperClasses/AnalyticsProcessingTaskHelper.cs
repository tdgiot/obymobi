﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    public class AnalyticsProcessingTaskHelper
    {
        public static void CreateTask(OrderEntity orderEntity, string eventAction, string eventLabel)
        {
            if (orderEntity.AnalyticsTrackingIds.IsNullOrWhiteSpace())
            {
                return;
            }

            AnalyticsProcessingTaskEntity task = new AnalyticsProcessingTaskEntity();
            task.AddToTransaction(orderEntity);
            task.OrderEntity = orderEntity;
            task.EventAction = eventAction;
            task.EventLabel = eventLabel;
            task.Save();
        }

        public static AnalyticsProcessingTaskCollection GetTasks()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.AnalyticsProcessingTaskEntity);
            prefetch.Add(AnalyticsProcessingTaskEntityBase.PrefetchPathOrderEntity)
                    .SubPath.Add(OrderEntityBase.PrefetchPathDeliverypointEntity, new IncludeFieldsList(DeliverypointFields.DeliverypointgroupId));

            AnalyticsProcessingTaskCollection tasks = new AnalyticsProcessingTaskCollection();
            tasks.GetMulti(null, prefetch);

            return tasks;
        }
    }
}
