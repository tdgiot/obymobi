﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    public static class MessageTemplateCategoryHelper
    {
        public static MessageTemplateCategoryCollection GetMessageTemplateCategoryCollection(int companyId)
        {
            MessageTemplateCategoryCollection collection = new MessageTemplateCategoryCollection();
            PredicateExpression filter = new PredicateExpression(MessageTemplateCategoryFields.CompanyId == companyId);
            SortExpression sort = new SortExpression(MessageTemplateCategoryFields.Name | SortOperator.Ascending);

            collection.GetMulti(filter, 0, sort);

            return collection;
        }
    }
}