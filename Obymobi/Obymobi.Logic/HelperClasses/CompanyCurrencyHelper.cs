﻿using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.HelperClasses
{
    public class CompanyCurrencyHelper
    {
        public static CompanyCurrency[] ConvertCompanyCurrencyCollectionToArray(CompanyCurrencyCollection companyCurrencies)
        {
            List<CompanyCurrency> toReturn = new List<CompanyCurrency>();
            foreach (CompanyCurrencyEntity companyCurrencyEntity in companyCurrencies)
            {
                toReturn.Add(CompanyCurrencyHelper.ConvertCompanyCurrencyEntityToModel(companyCurrencyEntity));
            }
            return toReturn.OrderBy(x => x.Name).ToArray();
        }

        public static CompanyCurrency ConvertCompanyCurrencyEntityToModel(CompanyCurrencyEntity entity)
        {
            CompanyCurrency companyCurrency = new CompanyCurrency();
            companyCurrency.CompanyCurrencyId = entity.CompanyCurrencyId;
            companyCurrency.CurrencyCode = entity.CurrencyCode;

            if (!entity.OverwriteSymbol.IsNullOrWhiteSpace())
            {
                companyCurrency.Symbol = entity.OverwriteSymbol;
            }

            if (!entity.CurrencyCode.IsNullOrWhiteSpace())
            {
                Currency currency = Currency.Mappings[entity.CurrencyCode];

                if (companyCurrency.Symbol.IsNullOrWhiteSpace())
                {
                    companyCurrency.Symbol = currency.Symbol;
                }

                companyCurrency.Name = currency.Name;
            }
            
            companyCurrency.ExchangeRate = entity.ExchangeRate;

            return companyCurrency;
        }
    }
}