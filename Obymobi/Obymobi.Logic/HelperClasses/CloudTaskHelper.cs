﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Logic.HelperClasses
{
    public class CloudTaskHelper
    {
        public const string ENTERTAINMENT_CONTAINER = "entertainment";
        public const string WEATHER_CONTAINER = "weather";
        public const string RELEASE_CONTAINER = "builds"; // Capital B is needed for backwards compatibility 
        public const string DELIVERYTIME_CONTAINER = "deliverytime";

        public static void UploadEntertainmentFile(int entertainmentId, EntertainmentFileEntity entertainmentFileEntity)
        {
            CloudProcessingTaskEntity cloudProcessingTask = new CloudProcessingTaskEntity();
            cloudProcessingTask.ActionAsEnum = CloudProcessingTaskEntity.ProcessingAction.Upload;
            cloudProcessingTask.TypeAsEnum = CloudProcessingTaskType.Entertainment;
            cloudProcessingTask.EntertainmentId = entertainmentId;
            cloudProcessingTask.EntertainmentFileId = entertainmentFileEntity.EntertainmentFileId;
            cloudProcessingTask.PathFormat = entertainmentFileEntity.Filename;
            cloudProcessingTask.Container = ENTERTAINMENT_CONTAINER;
            cloudProcessingTask.Save();
        }

        public static void DeleteEntertainmentFile(int? entertainmentId, EntertainmentFileEntity entertainmentFileEntity)
        {
            CloudProcessingTaskEntity cloudProcessingTask = new CloudProcessingTaskEntity();
            cloudProcessingTask.ActionAsEnum = CloudProcessingTaskEntity.ProcessingAction.Delete;
            cloudProcessingTask.TypeAsEnum = CloudProcessingTaskType.Entertainment;
            cloudProcessingTask.EntertainmentId = entertainmentId;
            cloudProcessingTask.EntertainmentFileId = entertainmentFileEntity.EntertainmentFileId;
            cloudProcessingTask.PathFormat = entertainmentFileEntity.Filename;
            cloudProcessingTask.Container = ENTERTAINMENT_CONTAINER;
            cloudProcessingTask.Save();
        }

        /// <summary>
        /// Weather will be uploaded as Json with filename as city-countrycode.json
        /// </summary>
        /// <param name="weather"></param>
        public static void UploadWeather(WeatherEntity weather)
        {
            // Create json filename
            var filename = weather.City.ToLower() + ".json"; // Everything to lower + extension
            filename = filename.Replace(',', '-'); // Replace comma with dash
            filename = filename.Replace(" ", "_"); // Replace whitespace with underscore

            CloudProcessingTaskEntity cloudProcessingTask = new CloudProcessingTaskEntity();
            cloudProcessingTask.ActionAsEnum = CloudProcessingTaskEntity.ProcessingAction.Upload;
            cloudProcessingTask.TypeAsEnum = CloudProcessingTaskType.Weather;
            cloudProcessingTask.Container = WEATHER_CONTAINER;
            cloudProcessingTask.PathFormat = filename;
            cloudProcessingTask.WeatherId = weather.WeatherId;
            cloudProcessingTask.Save();
        }

        /// <summary>
        /// Upload Release file blob to cloud storage
        /// </summary>
        /// <param name="release"></param>
        public static void UploadRelease(ReleaseEntity release)
        {
            CloudProcessingTaskEntity cloudProcessingTask = new CloudProcessingTaskEntity();
            cloudProcessingTask.ActionAsEnum = CloudProcessingTaskEntity.ProcessingAction.Upload;
            cloudProcessingTask.TypeAsEnum = CloudProcessingTaskType.Release;
            cloudProcessingTask.ReleaseId = release.ReleaseId;
            cloudProcessingTask.PathFormat = release.Filename.Replace("builds/", "");
            cloudProcessingTask.Container = RELEASE_CONTAINER;
            cloudProcessingTask.Save();
        }

        public static void DeleteRelease(ReleaseEntity release)
        {
            CloudProcessingTaskEntity cloudProcessingTask = new CloudProcessingTaskEntity();
            cloudProcessingTask.ActionAsEnum = CloudProcessingTaskEntity.ProcessingAction.Delete;
            cloudProcessingTask.TypeAsEnum = CloudProcessingTaskType.Release;
            cloudProcessingTask.PathFormat = release.Filename.Replace("builds/", "");
            cloudProcessingTask.Container = RELEASE_CONTAINER;
            cloudProcessingTask.Save();
        }

        public static void UploadDeliveryTime(int deliverypointgroupId, int deliveryTime)
        {
            CloudProcessingTaskEntity cloudProcessingTask = new CloudProcessingTaskEntity();
            cloudProcessingTask.ActionAsEnum = CloudProcessingTaskEntity.ProcessingAction.Upload;
            cloudProcessingTask.TypeAsEnum = CloudProcessingTaskType.DeliveryTime;
            cloudProcessingTask.FileContent = deliveryTime.ToString();
            cloudProcessingTask.PathFormat = string.Format("{0}.txt", deliverypointgroupId);
            cloudProcessingTask.Container = DELIVERYTIME_CONTAINER;
            cloudProcessingTask.Save();
        }

        public static void DeleteDeliveryTime(int deliverypointgroupId)
        {
            CloudProcessingTaskEntity cloudProcessingTask = new CloudProcessingTaskEntity();
            cloudProcessingTask.ActionAsEnum = CloudProcessingTaskEntity.ProcessingAction.Delete;
            cloudProcessingTask.TypeAsEnum = CloudProcessingTaskType.DeliveryTime;
            cloudProcessingTask.PathFormat = string.Format("{0}.txt", deliverypointgroupId);
            cloudProcessingTask.Container = DELIVERYTIME_CONTAINER;
            cloudProcessingTask.Save();
        }
    }
}
