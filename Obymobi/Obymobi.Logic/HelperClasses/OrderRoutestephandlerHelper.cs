﻿using System.Linq;
using Dionysos;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Order = Obymobi.Logic.Model.Order;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// OrderRoutestephandlerHelper class
    /// </summary>
    public static class OrderRoutestephandlerHelper
    {
        #region Model / Entity Conversion

        /// <summary>
        /// Creates the routestephandler save status model from order model.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        public static OrderRoutestephandlerSaveStatus CreateRoutestephandlerSaveStatusModelFromOrderModel(Order order)
        {
            OrderRoutestephandlerSaveStatus model = new OrderRoutestephandlerSaveStatus();
            if (order.OrderRoutestephandler != null)
            {
                OrderRoutestephandler routestep = order.OrderRoutestephandler;
                model.OrderRoutestephandlerId = routestep.OrderRoutestephandlerId;
                model.OrderRoutestephandlerGuid = routestep.Guid;
                model.Status = routestep.Status;
                model.Error = routestep.Error;
            }
            model.OrderId = order.OrderId;
            model.OrderGuid = order.Guid;

            return model;
        }

        /// <summary>
        /// Creates the routestephandler save status model from entity.
        /// </summary>
        /// <param name="orderoutestephandlerEntity">The orderoutestephandler entity.</param>
        /// <param name="order">The order.</param>
        /// <returns></returns>
        public static OrderRoutestephandlerSaveStatus CreateRoutestephandlerSaveStatusModelFromEntity(OrderRoutestephandlerEntity orderoutestephandlerEntity, OrderEntity order)
        {
            OrderRoutestephandlerSaveStatus model = new OrderRoutestephandlerSaveStatus();
            model.OrderRoutestephandlerId = orderoutestephandlerEntity.OrderRoutestephandlerId;
            model.OrderRoutestephandlerGuid = orderoutestephandlerEntity.Guid;
            model.OrderId = order.OrderId;
            model.OrderGuid = order.Guid;
            model.Status = orderoutestephandlerEntity.Status;
            model.Error = orderoutestephandlerEntity.ErrorCode;
            return model;
        }

        /// <summary>
        /// Creates the order routestephandler entity from model.
        /// </summary>
        /// <param name="orderRoutestephandler">The order routestephandler.</param>
        /// <returns></returns>
        public static OrderRoutestephandlerEntity CreateOrderRoutestephandlerEntityFromModel(OrderRoutestephandler orderRoutestephandler)
        {
            OrderRoutestephandlerEntity orderRoutestephandlerEntity = new OrderRoutestephandlerEntity();
            orderRoutestephandlerEntity.OrderRoutestephandlerId = orderRoutestephandler.OrderRoutestephandlerId;
            orderRoutestephandlerEntity.Guid = orderRoutestephandler.Guid;
            orderRoutestephandlerEntity.OrderId = orderRoutestephandler.OrderId;
            orderRoutestephandlerEntity.TerminalId = orderRoutestephandler.TerminalId;
            orderRoutestephandlerEntity.Number = orderRoutestephandler.Number;
            orderRoutestephandlerEntity.HandlerType = orderRoutestephandler.HandlerType;
            orderRoutestephandlerEntity.PrintReportType = orderRoutestephandler.PrintReportType;
            orderRoutestephandlerEntity.Status = orderRoutestephandler.Status;
            orderRoutestephandlerEntity.CompletedUTC = orderRoutestephandler.Completed.LocalTimeToUtc();
            orderRoutestephandlerEntity.FieldValue1 = orderRoutestephandler.FieldValue1;
            orderRoutestephandlerEntity.FieldValue2 = orderRoutestephandler.FieldValue2;
            orderRoutestephandlerEntity.FieldValue3 = orderRoutestephandler.FieldValue3;
            orderRoutestephandlerEntity.FieldValue4 = orderRoutestephandler.FieldValue4;
            orderRoutestephandlerEntity.FieldValue5 = orderRoutestephandler.FieldValue5;
            orderRoutestephandlerEntity.FieldValue6 = orderRoutestephandler.FieldValue6;
            orderRoutestephandlerEntity.FieldValue7 = orderRoutestephandler.FieldValue7;
            orderRoutestephandlerEntity.FieldValue8 = orderRoutestephandler.FieldValue8;
            orderRoutestephandlerEntity.FieldValue9 = orderRoutestephandler.FieldValue9;
            orderRoutestephandlerEntity.FieldValue10 = orderRoutestephandler.FieldValue10;

            return orderRoutestephandlerEntity;
        }

        /// <summary>
        /// Create a Obymobi.Logic.Model.OrderRoutestephandler instance using the specified Obymobi.Data.EntityClasses.OrderRoutestephandler instance
        /// </summary>
        /// <param name="orderRoutestephandlerEntity">The OrderRoutestephandlerEntity instance to create the OrderRoutestephandler instance for</param>
        /// <returns>A OrderRoutestephandler instance</returns>
        public static OrderRoutestephandler CreateOrderRoutestephandlerModelFromEntity(OrderRoutestephandlerEntity orderRoutestephandlerEntity)
        {
            OrderRoutestephandler orderRoutestephandler = new OrderRoutestephandler();
            orderRoutestephandler.OrderRoutestephandlerId = orderRoutestephandlerEntity.OrderRoutestephandlerId;
            orderRoutestephandler.Guid = orderRoutestephandlerEntity.Guid;
            orderRoutestephandler.OrderId = orderRoutestephandlerEntity.OrderId;
            orderRoutestephandler.TerminalId = orderRoutestephandlerEntity.TerminalId ?? 0;
            orderRoutestephandler.Number = orderRoutestephandlerEntity.Number;
            orderRoutestephandler.HandlerType = orderRoutestephandlerEntity.HandlerType;
            orderRoutestephandler.PrintReportType = orderRoutestephandlerEntity.PrintReportType ?? 0;
            orderRoutestephandler.Status = orderRoutestephandlerEntity.Status;
            orderRoutestephandler.Completed = orderRoutestephandlerEntity.CompletedUTC.UtcToLocalTime();
            orderRoutestephandler.FieldValue1 = orderRoutestephandlerEntity.FieldValue1;
            orderRoutestephandler.FieldValue2 = orderRoutestephandlerEntity.FieldValue2;
            orderRoutestephandler.FieldValue3 = orderRoutestephandlerEntity.FieldValue3;
            orderRoutestephandler.FieldValue4 = orderRoutestephandlerEntity.FieldValue4;
            orderRoutestephandler.FieldValue5 = orderRoutestephandlerEntity.FieldValue5;
            orderRoutestephandler.FieldValue6 = orderRoutestephandlerEntity.FieldValue6;
            orderRoutestephandler.FieldValue7 = orderRoutestephandlerEntity.FieldValue7;
            orderRoutestephandler.FieldValue8 = orderRoutestephandlerEntity.FieldValue8;
            orderRoutestephandler.FieldValue9 = orderRoutestephandlerEntity.FieldValue9;
            orderRoutestephandler.FieldValue10 = orderRoutestephandlerEntity.FieldValue10;
        	orderRoutestephandler.Timeout = orderRoutestephandlerEntity.Timeout;
        	orderRoutestephandler.TimeoutExpires = orderRoutestephandlerEntity.TimeoutExpiresUTC.UtcToLocalTime();
            orderRoutestephandler.ForwardedFromTerminalId = orderRoutestephandlerEntity.ForwardedFromTerminalId ?? 0;

            return orderRoutestephandler;
        }

        #endregion
    }
}
