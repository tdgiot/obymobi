﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Logic.HelperClasses
{
    public class TerminalConfigurationHelper
    {
        public static void CreateFromTerminal(TerminalEntity terminalEntity)
        {
            TerminalConfigurationEntity terminalConfigurationEntity = new TerminalConfigurationEntity();
            terminalConfigurationEntity.AddToTransaction(terminalEntity);
            terminalConfigurationEntity.TerminalId = terminalEntity.TerminalId;
            terminalConfigurationEntity.CompanyId = terminalEntity.CompanyId;
            terminalConfigurationEntity.Name = terminalEntity.Name;
            terminalConfigurationEntity.UIModeId = terminalEntity.UIModeId;
            terminalConfigurationEntity.PrintingEnabled = terminalEntity.PrintingEnabled;
            terminalConfigurationEntity.UseHardKeyboard = (int)terminalEntity.UseHardKeyboard;
            terminalConfigurationEntity.MaxVolume = terminalEntity.MaxVolume;

            CompanyEntity companyEntity = new CompanyEntity(terminalEntity.CompanyId);
            terminalConfigurationEntity.Pincode = companyEntity.Pincode;
            terminalConfigurationEntity.PincodeSU = companyEntity.PincodeSU;
            terminalConfigurationEntity.PincodeGM = companyEntity.PincodeGM;

            terminalConfigurationEntity.Save();
            terminalConfigurationEntity.Refetch();

            terminalEntity.TerminalConfigurationId = terminalConfigurationEntity.TerminalConfigurationId;
            terminalEntity.Save();
        }
    }
}
