﻿using System;
using System.Collections.Generic;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// DeliverypointgroupHelper class
    /// </summary>
    public class DeliverypointgroupHelper
    {
        /// <summary>
        /// Creates the deliverypointgroup model from entity.
        /// </summary>
        /// <param name="deliverypointgroupEntity">The deliverypointgroup entity.</param>
        /// <returns></returns>
        public static Deliverypointgroup CreateDeliverypointgroupModelFromEntity(DeliverypointgroupEntity deliverypointgroupEntity)
        {
            Deliverypointgroup deliverypointgroup = new Deliverypointgroup();
            deliverypointgroup.DeliverypointgroupId = deliverypointgroupEntity.DeliverypointgroupId;
            deliverypointgroup.CompanyId = deliverypointgroupEntity.CompanyId;
            deliverypointgroup.Name = deliverypointgroupEntity.Name;
            deliverypointgroup.OrderHistoryDialogEnabled = deliverypointgroupEntity.OrderHistoryDialogEnabled;
            deliverypointgroup.PmsIntegration = deliverypointgroupEntity.PmsIntegration;
            deliverypointgroup.PmsLockClientWhenNotCheckedIn = deliverypointgroupEntity.PmsLockClientWhenNotCheckedIn;
            deliverypointgroup.PmsAllowShowBill = deliverypointgroupEntity.PmsAllowShowBill;
            deliverypointgroup.PmsAllowExpressCheckout = deliverypointgroupEntity.PmsAllowExpressCheckout;
            deliverypointgroup.PmsAllowShowGuestName = deliverypointgroupEntity.PmsAllowShowGuestName;
            deliverypointgroup.DeliverypointCaption = deliverypointgroupEntity.DeliverypointCaption;
            deliverypointgroup.UseHardKeyboard = (int)deliverypointgroupEntity.UseHardKeyboard;
            deliverypointgroup.UIModeId = deliverypointgroupEntity.UIModeId.GetValueOrDefault(0);

            return deliverypointgroup;
        }

        /// <summary>
        /// Gets the deliverypointgroup collection for company.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public static DeliverypointgroupCollection GetDeliverypointgroupCollectionForCompany(int companyId, PrefetchPath path)
        {
            DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointgroupFields.CompanyId == companyId);            

            deliverypointgroups.GetMulti(filter, path);

            return deliverypointgroups;
        }

        /// <summary>
        /// Gets the deliverypointgroup by deliverypoint id.
        /// </summary>
        /// <param name="deliverypointId">The deliverypoint id.</param>
        /// <returns></returns>
        public static DeliverypointgroupEntity GetDeliverypointgroupByDeliverypointId(int deliverypointId)
        {
            // GK - Untested... :S
            PredicateExpression filter = new PredicateExpression(DeliverypointFields.DeliverypointId == deliverypointId);
            RelationCollection joins = new RelationCollection();
            joins.Add(DeliverypointgroupEntity.Relations.DeliverypointEntityUsingDeliverypointgroupId);

            var dg = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<DeliverypointgroupEntity>(filter, joins);

            return dg;
        }

        /// <summary>
        /// Gets the deliverypointgroup by deliverypoint id.
        /// </summary>
        /// <param name="deliverypointgroupId">The deliverypointgroup id.</param>
        /// <returns>Deliverypointgroup model</returns>
        public static Deliverypointgroup GetDeliverypointgroupModelByDeliverypointgroupId(int deliverypointgroupId)
        {
            Deliverypointgroup deliverypointgroup = null;

            DeliverypointgroupEntity deliverypointgroupEntity = new DeliverypointgroupEntity(deliverypointgroupId);
            if (deliverypointgroupEntity != null)
                deliverypointgroup = CreateDeliverypointgroupModelFromEntity(deliverypointgroupEntity);

            return deliverypointgroup;
        }

        /// <summary>
        /// Gets if the deliverypointgroup currently has ordering enabled
        /// </summary>
        /// <param name="deliverypointgroupId">The deliverypointgroup id.</param>
        /// <returns>true if ordering is enabled</returns>
        public static bool GetOrderingEnabled(int deliverypointgroupId)
        {
            DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointgroupFields.DeliverypointgroupId == deliverypointgroupId);

            IncludeFieldsList includeFields = new IncludeFieldsList();
            includeFields.Add(DeliverypointgroupFields.OrderingEnabled);

            deliverypointgroups.GetMulti(filter, includeFields, 0);

            return deliverypointgroups.Count > 0 && deliverypointgroups[0].OrderingEnabled;
        }

    }
}
