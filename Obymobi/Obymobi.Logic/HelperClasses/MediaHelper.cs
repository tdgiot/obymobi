﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Web;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// MediaHelper class
    /// </summary>
    public static class MediaHelper
    {
        public const string GenericFilesSubPath = "generic";
        public const string BrandFilesSubPath = "brand";

        /// <summary>
        /// MediaHelperErrors enums
        /// </summary>
        public enum MediaHelperErrors : int
        {
            /// <summary>
            /// Company does not exists
            /// </summary>
            CompanyDoesNotExist = 200,

            /// <summary>
            /// No deliverypoint group id or terminal id was specified
            /// </summary>
            NoDeliverypointgroupIdOrTerminalIdSpecified = 201
        }

        public static List<Obymobi.Logic.Model.Media> CreateMediaModelsFromEntityCollection(IEnumerable<MediaEntity> medias)
        {
            List<Obymobi.Logic.Model.Media> toReturn = new List<Obymobi.Logic.Model.Media>();

            foreach (var mediaEntity in medias)
                toReturn.AddRange(MediaHelper.CreateMediaModelFromEntity(mediaEntity));

            return toReturn;
        }

        /// <summary>
        /// Creates a Obymobi.Logic.Model.Media instance using the specified Obymobi.Data.EntityClasses.MediaEntity instance
        /// </summary>
        /// <param name="mediaEntity">The MediaEntity instance to create the Media instance for</param>
        /// <param name="parent">Parent of this media</param>
        /// <returns>A Media instance</returns>
        public static List<Model.Media> CreateMediaModelFromEntity(MediaEntity mediaEntity, string parent = "", string mediaParent = "")
        {
            List<Model.Media> mediaList = new List<Model.Media>();

            foreach (MediaRatioTypeMediaEntity mediaRatio in mediaEntity.MediaRatioTypeMediaCollection)
            {
                Model.Media media = MediaHelper.CreateMediaModelFromMediaRatioTypeMedia(mediaEntity, mediaRatio, parent, mediaParent);
                if (media != null)
                {
                    media.GenericFile = mediaEntity.IsGeneric;
                    mediaList.Add(media);
                }
            }

            return mediaList;
        }

        public static Model.Media CreateMediaModelFromMediaRatioTypeMedia(MediaEntity mediaEntity, MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity, string parent = "", string mediaParent = "")
        {
            var media = new Model.Media();
            media.MediaId = mediaEntity.MediaId;
            media.Name = mediaEntity.Name;
            media.Parent = parent;

            string fileName = mediaRatioTypeMediaEntity.FileNameOnCdn;

            // Don't include images that don't have a filename.
            if (fileName.IsNullOrWhiteSpace())
                return null;

            media.MediaCultures = MediaCultureHelper.ConvertMediaCultureCollectionToArray(mediaEntity.MediaCultureCollection, mediaParent.IsNullOrWhiteSpace() ? "" : mediaParent);
            media.AgnosticMediaId = mediaEntity.AgnosticMediaId.GetValueOrDefault(0);

            // GK Seemed double code
            string subDirectoryName = mediaRatioTypeMediaEntity.MediaRatioType != null ? MediaHelper.GetEntitySubDirectoryName(mediaRatioTypeMediaEntity) : string.Empty;
            media.FilePathRelativeToMediaPath = Path.Combine(subDirectoryName, fileName);
            media.MediaType = mediaRatioTypeMediaEntity.MediaType.GetValueOrDefault(0);
            media.ActionProductId = mediaEntity.ActionProductId.HasValue ? mediaEntity.ActionProductId.Value : 0;
            media.ActionCategoryId = mediaEntity.ActionCategoryId.HasValue ? mediaEntity.ActionCategoryId.Value : 0;
            media.ActionEntertainmentId = mediaEntity.ActionEntertainmentId.HasValue ? mediaEntity.ActionEntertainmentId.Value : 0;
            media.ActionEntertainmentcategoryId = mediaEntity.ActionEntertainmentcategoryId.HasValue ? mediaEntity.ActionEntertainmentcategoryId.Value : 0;
            media.ActionSiteId = mediaEntity.ActionSiteId.HasValue ? mediaEntity.ActionSiteId.Value : 0;
            media.ActionPageId = mediaEntity.ActionPageId.HasValue ? mediaEntity.ActionPageId.Value : 0;
            media.ActionUrl = mediaEntity.ActionUrl;
            media.SizeMode = (int)mediaEntity.SizeMode;
            media.ZoomLevel = mediaEntity.ZoomLevel;
            media.RelatedCompanyId = mediaEntity.RelatedCompanyId.HasValue ? mediaEntity.RelatedCompanyId.Value : 0;
            media.RelatedBrandId = mediaEntity.ProductId.HasValue ? mediaEntity.ProductEntity.BrandId.GetValueOrDefault(0) : 0;

            if (mediaRatioTypeMediaEntity.MediaRatioType != null) // Null happens when the MediaRatioType no longer exists.
                media.CdnFilePathRelativeToMediaPath = MediaHelper.GetMediaRatioTypeMediaPath(mediaRatioTypeMediaEntity);

            return media;
        }

        public static MediaType GetCorrectMediaTypeIfGeneric(MediaType mediaType)
        {
            return MediaHelperLight.GetCorrectMediaTypeIfGeneric(mediaType);
        }

        static void RemoveDuplicates(string directoryPath)
        {
            if (Directory.Exists(directoryPath))
            {
                // Sort that the lastest files appear first
                var filesSorted = Directory.GetFiles(directoryPath).OrderByDescending(x => x);

                string lastFilePrefix = string.Empty;
                foreach (var file in filesSorted)
                {
                    string fileName = Path.GetFileName(file);
                    string filePrefix = fileName.Substring(0, fileName.IndexOf('-', fileName.IndexOf('-') + 1) + 1);
                    if (filePrefix.Equals(lastFilePrefix, StringComparison.InvariantCultureIgnoreCase))
                    {
                        // Delete file, it's a double
                        File.Delete(file);
                    }

                    lastFilePrefix = filePrefix;
                }

                var subDirs = Directory.GetDirectories(directoryPath);
                foreach (var dir in subDirs)
                    MediaHelper.RemoveDuplicates(dir);
            }

        }

        public static string GetEntitySubDirectoryName(MediaRatioTypeMediaEntity mediaRatio)
        {
            return GetEntitySubDirectoryName(mediaRatio.MediaRatioType.DeviceType, mediaRatio);
        }

        /// <summary>
        /// Gets the name of the subdirectory for the related entity of the media entity
        /// </summary>
        /// <param name="deviceType">The type of device</param>
        /// <param name="mediaRatio">The <see cref="MediaRatioTypeMediaEntity"/> instance to get the subdirectory name for</param>
        /// <returns>A <see cref="System.String"/> instance containing the name of the subdirectory</returns>
        public static string GetEntitySubDirectoryName(DeviceType deviceType, MediaRatioTypeMediaEntity mediaRatio)
        {
            string dir = string.Empty;

            List<DeviceType> craveDeviceTypes = new List<DeviceType>();
            craveDeviceTypes.Add(DeviceType.Archos70);
            craveDeviceTypes.Add(DeviceType.SamsungP5110);
            craveDeviceTypes.Add(DeviceType.AndroidTvBox);
            craveDeviceTypes.Add(DeviceType.PhoneSmall);
            craveDeviceTypes.Add(DeviceType.PhoneNormal);
            craveDeviceTypes.Add(DeviceType.PhoneLarge);
            craveDeviceTypes.Add(DeviceType.PhoneXLarge);
            craveDeviceTypes.Add(DeviceType.TabletSmall);
            craveDeviceTypes.Add(DeviceType.TabletNormal);
            craveDeviceTypes.Add(DeviceType.TabletLarge);
            craveDeviceTypes.Add(DeviceType.TabletXLarge);
            craveDeviceTypes.Add(DeviceType.iPhone);
            craveDeviceTypes.Add(DeviceType.iPhoneRetina);
            craveDeviceTypes.Add(DeviceType.iPhoneRetinaHD);
            craveDeviceTypes.Add(DeviceType.iPhoneRetinaHDPlus);
            craveDeviceTypes.Add(DeviceType.iPad);
            craveDeviceTypes.Add(DeviceType.iPadRetina);
            craveDeviceTypes.Add(DeviceType.Unknown); // Needed for Attachments

            if (deviceType == DeviceType.EM200)
            {
                // Related entity
                if (mediaRatio.MediaEntity.ProductId.HasValue || mediaRatio.MediaEntity.GenericproductId.HasValue)
                    dir = "products";
                else if (mediaRatio.MediaEntity.CategoryId.HasValue || mediaRatio.MediaEntity.GenericcategoryId.HasValue)
                    dir = "categories";
                else if (mediaRatio.MediaEntity.EntertainmentId.HasValue)
                    dir = "entertainment";
                else if (mediaRatio.MediaEntity.AdvertisementId.HasValue)
                    dir = "advertisements";
                else if (mediaRatio.MediaEntity.CompanyId.HasValue)
                    dir = "eye-catchers";
                else if (mediaRatio.MediaEntity.DeliverypointgroupId.HasValue)
                    dir = "deliverypointgroups";
                else if (mediaRatio.MediaEntity.SurveyId.HasValue || mediaRatio.MediaEntity.SurveyPageId.HasValue)
                    dir = "surveys";
                else
                    throw new ObymobiException(MediaSaveResult.MissingOrUnsupportedRelatedEntity, "Can't determine SubDirectoryName");
            }
            else if (craveDeviceTypes.Contains(deviceType))
            {
                // Related entity
                if (mediaRatio.MediaEntity.ProductId.HasValue || mediaRatio.MediaEntity.GenericproductId.HasValue)
                    dir = "products";
                else if (mediaRatio.MediaEntity.CategoryId.HasValue || mediaRatio.MediaEntity.GenericcategoryId.HasValue)
                    dir = "categories";
                else if (mediaRatio.MediaEntity.CompanyId.HasValue)
                    dir = "eye-catchers";
                else if (mediaRatio.MediaEntity.EntertainmentId.HasValue)
                    dir = "entertainment";
                else if (mediaRatio.MediaEntity.AlterationId.HasValue)
                    dir = "alterations";
                else if (mediaRatio.MediaEntity.DeliverypointgroupId.HasValue)
                    dir = "deliverypointgroups";
                else if (mediaRatio.MediaEntity.SurveyId.HasValue || mediaRatio.MediaEntity.SurveyPageId.HasValue)
                    dir = "surveys";
                else if (mediaRatio.MediaEntity.AdvertisementId.HasValue)
                    dir = "advertisements";
                else if (mediaRatio.MediaEntity.PointOfInterestId.HasValue)
                    dir = "pointsofinterest";
                else if (mediaRatio.MediaEntity.PageElementId.HasValue || mediaRatio.MediaEntity.PageTemplateElementId.HasValue)
                    dir = "pageelement";
                else if (mediaRatio.MediaEntity.PageId.HasValue || mediaRatio.MediaEntity.PageTemplateId.HasValue)
                    dir = "page";
                else if (mediaRatio.MediaEntity.SiteId.HasValue)
                    dir = "site";
                else if (mediaRatio.MediaEntity.RelatedEntityType == EntityType.AttachmentEntity)
                    dir = "attachments";
                else if (mediaRatio.MediaEntity.RoutestephandlerId.HasValue)
                    dir = "routestephandlers";
                else if (mediaRatio.MediaEntity.UIWidgetId.HasValue)
                    dir = "widgets";
                else if (mediaRatio.MediaEntity.UIThemeId.HasValue)
                    dir = "themes";
                else if (mediaRatio.MediaEntity.RoomControlSectionId.HasValue ||
                         mediaRatio.MediaEntity.RoomControlSectionItemId.HasValue ||
                         mediaRatio.MediaEntity.StationId.HasValue)
                    dir = "roomcontrol";
                else if (mediaRatio.MediaEntity.UIFooterItemId.HasValue)
                    dir = "footerbar";
                else if (mediaRatio.MediaEntity.ProductgroupId.HasValue)
                    dir = "productgroups";
                else if (mediaRatio.MediaEntity.LandingPageId.HasValue ||
                    mediaRatio.MediaEntity.WidgetId.HasValue ||
                    mediaRatio.MediaEntity.CarouselItemId.HasValue ||
                    mediaRatio.MediaEntity.ApplicationConfigurationId.HasValue ||
                    mediaRatio.MediaEntity.LandingPageId.HasValue)
                    dir = "appless";
                else
                    throw new ObymobiException(MediaSaveResult.MissingOrUnsupportedRelatedEntity, "Can't determine SubDirectoryName");
            }

            return dir;
        }

        /// <summary>
        /// Retrieves all related Media for a Company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static MediaCollection GetMediaForCompany(int companyId, PredicateExpression additionalFilter, int maxResults = 0, SortExpression sort = null, RelationCollection additionalRelations = null, PrefetchPath mediaPrefetchPath = null, bool includeGenericMedia = false)
        {
            PredicateExpression filter;
            RelationCollection relations;
            MediaHelper.GetRelationsAndFilterForMediaForCompany(companyId, out filter, out relations);

            if (additionalFilter != null)
                filter.Add(additionalFilter);

            if (additionalRelations != null)
                relations.AddRange(additionalRelations);

            MediaCollection media = new MediaCollection();
            media.GetMulti(filter, maxResults, sort, relations, mediaPrefetchPath);
            return media;
        }

        /// <summary>
        /// Get Media Count for company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static int GetMediaCountForCompany(int companyId, PredicateExpression additionalFilter = null, RelationCollection additionalRelations = null, bool includeGenericMedia = false)
        {
            PredicateExpression filter;
            RelationCollection relations;
            MediaHelper.GetRelationsAndFilterForMediaForCompany(companyId, out filter, out relations);

            if (additionalFilter != null)
                filter.Add(additionalFilter);

            if (additionalRelations != null)
                relations.AddRange(additionalRelations);

            MediaCollection media = new MediaCollection();
            return media.GetDbCount(filter, relations);
        }

        public static void GetRelationsAndFilterForMediaForCompany(int companyId, out PredicateExpression filter, out RelationCollection relations)
        {
            filter = new PredicateExpression();
            filter.AddWithOr(MediaFields.CompanyId == companyId);
            filter.AddWithOr(ProductFields.CompanyId == companyId);
            filter.AddWithOr(CategoryFields.CompanyId == companyId);
            filter.AddWithOr(AdvertisementFields.CompanyId == companyId);
            filter.AddWithOr(EntertainmentFields.CompanyId == companyId);
            filter.AddWithOr(AlterationoptionFields.CompanyId == companyId);
            filter.AddWithOr(AlterationFields.CompanyId == companyId);
            filter.AddWithOr(DeliverypointgroupFields.CompanyId == companyId);
            filter.AddWithOr(AnnouncementFields.CompanyId == companyId);
            filter.AddWithOr(SurveyFields.CompanyId == companyId);
            filter.AddWithOr(new FieldCompareValuePredicate(SurveyFields.CompanyId, ComparisonOperator.Equal, companyId, "SurveyOfPage"));
            filter.AddWithOr(new FieldCompareValuePredicate(SiteFields.CompanyId, ComparisonOperator.Equal, companyId, "SiteOfPage"));
            filter.AddWithOr(new FieldCompareValuePredicate(SiteFields.CompanyId, ComparisonOperator.Equal, companyId, "SiteOfPageOfPageElement"));
            filter.AddWithOr(new FieldCompareValuePredicate(UIModeFields.CompanyId, ComparisonOperator.Equal, companyId, "UIModeOfWidget"));

            relations = new RelationCollection();
            relations.Add(MediaEntity.Relations.ProductEntityUsingProductId, JoinHint.Left);
            relations.Add(MediaEntity.Relations.CategoryEntityUsingCategoryId, JoinHint.Left);
            relations.Add(MediaEntity.Relations.AdvertisementEntityUsingAdvertisementId, JoinHint.Left);
            relations.Add(MediaEntity.Relations.EntertainmentEntityUsingEntertainmentId, JoinHint.Left);
            relations.Add(MediaEntity.Relations.AlterationoptionEntityUsingAlterationoptionId, JoinHint.Left);
            relations.Add(MediaEntity.Relations.AlterationEntityUsingAlterationId, JoinHint.Left);
            relations.Add(MediaEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId, JoinHint.Left);
            relations.Add(MediaEntity.Relations.AnnouncementEntityUsingMediaId, JoinHint.Left);
            relations.Add(MediaEntity.Relations.SurveyEntityUsingSurveyId, JoinHint.Left);
            relations.Add(MediaEntity.Relations.SurveyPageEntityUsingSurveyPageId, JoinHint.Left);
            relations.Add(SurveyPageEntity.Relations.SurveyEntityUsingSurveyId, "SurveyOfPage", JoinHint.Left);

            relations.Add(MediaEntity.Relations.PageEntityUsingPageId, JoinHint.Left);
            relations.Add(PageEntity.Relations.SiteEntityUsingSiteId, "SiteOfPage", JoinHint.Left);

            relations.Add(MediaEntity.Relations.PageElementEntityUsingPageElementId, JoinHint.Left);
            relations.Add(PageElementEntity.Relations.PageEntityUsingPageId, "PageOfPageElement", JoinHint.Left);
            relations.Add(PageEntity.Relations.SiteEntityUsingSiteId, "SiteOfPageOfPageElement", JoinHint.Left);

            relations.Add(MediaEntity.Relations.UIWidgetEntityUsingUIWidgetId, JoinHint.Left);
            relations.Add(UIWidgetEntity.Relations.UITabEntityUsingUITabId, JoinHint.Left);
            relations.Add(UITabEntity.Relations.UIModeEntityUsingUIModeId, "UIModeOfWidget", JoinHint.Left);
        }

        public static MediaCollection GetMediaWithMediaType(IMediaContainingEntity mediaContainingEntity, MediaType mediaType)
        {
            MediaCollection medias = new MediaCollection();
            foreach (MediaEntity media in mediaContainingEntity.MediaCollection)
            {
                if (media.MediaRatioTypeMediaCollection.Where(mrtm => mrtm.MediaType == (int)mediaType).ToList().Count > 0)
                {
                    medias.Add(media);
                }
            }
            return medias;
        }

        public static bool IsPdf(this MediaEntity mediaEntity)
        {
            return mediaEntity.Extension.Equals(".pdf", StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsPdf(string fileName)
        {
            return System.IO.Path.GetExtension(fileName).Equals(".pdf", StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsGif(this MediaEntity mediaEntity)
        {
            return mediaEntity.Extension.Equals(".gif", StringComparison.InvariantCultureIgnoreCase);
        }

        private readonly static List<string> documentTypeExtensions = new List<string> { ".pdf", ".zip", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx" };
        public static bool IsDocument(this MediaEntity mediaEntity)
        {
            return MediaHelper.documentTypeExtensions.Contains(mediaEntity.Extension, true);
        }

        public static bool IsDocument(string fileName)
        {
            return MediaHelper.documentTypeExtensions.Contains(System.IO.Path.GetExtension(fileName), true);
        }

        // http://msdn.microsoft.com/en-us/library/stf701f5%28v=vs.110%29.aspx
        // Managed GDI+ has built-in encoders and decoders that support the following file types:    BMP    GIF    JPEG    PNG    TIFF 
        private readonly static List<string> supportedImageExtensions = new List<string> { ".jpg", ".jpeg", ".gif", ".bmp", ".png", ".tif", ".tiff" };
        public static bool IsSupportedImage(this MediaEntity mediaEntity)
        {
            return MediaHelper.supportedImageExtensions.Contains(mediaEntity.Extension, true);
        }

        public static bool IsSupportedImage(string fileName)
        {
            return MediaHelper.supportedImageExtensions.Contains(System.IO.Path.GetExtension(fileName), true);
        }

        private static readonly List<EntityType> entityTypesSupportingPng = new List<EntityType>
                                                                            {
                                                                                EntityType.RoomControlSectionEntity, EntityType.RoomControlSectionItemEntity,
                                                                                EntityType.UIWidgetEntity, EntityType.UIThemeEntity, EntityType.UIFooterItemEntity,
                                                                                EntityType.StationEntity, EntityType.ApplicationConfigurationEntity, EntityType.LandingPageEntity,
                                                                                EntityType.CarouselItemEntity, EntityType.WidgetActionBannerEntity
                                                                            };

        private static readonly List<EntityType> entityTypesSupportingGif = new List<EntityType>
                                                                            {
                                                                                EntityType.DeliverypointgroupEntity
                                                                            };

        public static bool IsPngAllowed(EntityType entityType)
        {
            return entityTypesSupportingPng.Contains(entityType);
        }

        public static bool IsGifAllowed(EntityType entityType)
        {
            return entityTypesSupportingGif.Contains(entityType);
        }

        public static void SetDefaultCutout(MediaEntity mediaEntity, MediaRatioTypeMediaEntity mrtm)
        {
            mrtm.SetDefaultCrop(MediaHelper.GetImageSize(mediaEntity));
        }

        public static void UploadToCdn(MediaRatioTypeMediaEntity mrtm, MediaEntity mediaEntity)
        {
            try
            {
                mrtm.ResizeAndPublishFile(mediaEntity, true);
            }
            catch (Exception exc)
            {
                ErrorLoggerWeb.LogError(exc, exc.Message);
            }
        }

        public static void OverwriteRatiosFromAgnosticMedia(MediaEntity mediaEntity)
        {
            // Linked to another agnostic media, Delete old Mrtms                
            if (mediaEntity.MediaRatioTypeMediaCollection.Count > 0)
            {
                mediaEntity.MediaRatioTypeMediaCollection.AddToTransaction(mediaEntity);
                mediaEntity.MediaRatioTypeMediaCollection.DeleteMulti();
            }

            //Overwrite the MediaRatioTypes from its parent
            MediaRatioTypeMediaCollection agnosticMrtm = new MediaRatioTypeMediaCollection();
            agnosticMrtm.GetMulti(new PredicateExpression(MediaRatioTypeMediaFields.MediaId == mediaEntity.AgnosticMediaId));

            foreach (MediaRatioTypeMediaEntity mrtm in agnosticMrtm)
            {
                MediaRatioTypeMediaEntity newMrtm = new MediaRatioTypeMediaEntity();
                newMrtm.MediaId = mediaEntity.MediaId;
                newMrtm.MediaType = mrtm.MediaType;

                MediaHelper.SetDefaultCutout(mediaEntity, newMrtm);

                newMrtm.AddToTransaction(mediaEntity);
                if (newMrtm.Save())
                {
                    MediaHelper.UploadToCdn(newMrtm, mediaEntity);
                    mediaEntity.MediaRatioTypeMediaCollection.Add(newMrtm);
                }
            }
        }

        public static MediaType GetMediaTypeByMessageLayoutType(MessageLayoutType type)
        {
            MediaType defaultType = MediaType.Gallery;

            if (type == MessageLayoutType.Small || type == MessageLayoutType.AutoSize)
            {
                defaultType = MediaType.GallerySmall;
            }
            else if (type == MessageLayoutType.Medium || type == MessageLayoutType.Large)
            {
                defaultType = MediaType.GalleryLarge;
            }

            return defaultType;
        }

        #region Media v2

        public static void BatchedMediaProcessing(Action<MediaEntity> mediaProcessing, int batchSize = 50, PredicateExpression filter = null, RelationCollection joins = null, PrefetchPath path = null)
        {
            SortExpression sort = new SortExpression(MediaFields.MediaId | SortOperator.Ascending);
            MediaCollection media = new MediaCollection();
            int mediaCount = media.GetDbCount(filter, joins);

            int batches = Decimal.ToInt32(Decimal.Ceiling(Decimal.Divide(mediaCount, batchSize)));

            Stopwatch sw = new Stopwatch();
            sw.Start();
            for (int i = 1; i <= batches; i++)
            {
                media.GetMulti(null, 0, sort, null, path, i, batchSize);

                foreach (var m in media)
                {
                    mediaProcessing(m);
                }

                Debug.WriteLine("Batch {0} of {1}, elapsed time: {2} (avg. time per batch: {3}ms)", i, batches, sw.Elapsed, Decimal.Round(Decimal.Divide(sw.ElapsedMilliseconds, i)));
            }
            sw.Stop();
        }

        public static void UpdateMimeTypesAndExtensions()
        {
            try
            {
                Thread.SetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralValidatorDisableUpdateVersions), true);
                MediaHelper.BatchedMediaProcessing(MediaHelper.UpdateMimeTypeAndExtensionLegacy);
            }
            finally
            {
                Thread.SetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralValidatorDisableUpdateVersions), false);
            }
        }

        public static void UpdateMimeTypeAndExtensionLegacy(MediaEntity m)
        {
            if (m.RelatedEntityId.HasValue && !m.FilePathRelativeToMediaPath.IsNullOrWhiteSpace())
            {
                m.Extension = Path.GetExtension(m.FilePathRelativeToMediaPath);
                m.MimeType = MimeTypeHelper.GetMimeType(m.FilePathRelativeToMediaPath);
                m.AuthorizerToUse = null;
                m.Validator = null;
                m.Save();
            }
            else if (!m.RelatedEntityId.HasValue)
            {
                // Go orphan - GO!
                m.Delete();
            }
        }

        public static void QueueMediaRatioTypeMediaFileTask(MediaRatioTypeMediaFileEntity file, MediaProcessingTaskEntity.ProcessingAction action, bool forceCdnUpload, ITransaction transaction)
        {
            // Sometimes it's null after the Save, fix that.
            if (file.MediaRatioTypeMediaEntity == null)
            {
                file.AlreadyFetchedMediaRatioTypeMediaEntity = false;
                if (file.MediaRatioTypeMediaEntity == null)
                    throw new ObymobiException(Obymobi.Enums.GenericResult.Failure, "file.MediaRatioTypeMediaEntity == null");
            }

            MediaHelper.QueueMediaRatioTypeMediaFileTask(file.MediaRatioTypeMediaEntity, action, forceCdnUpload, file.MediaProcessingTaskPriority, transaction);
        }

        public static void QueueMediaRatioTypeMediaFileTask(MediaRatioTypeMediaEntity mediaRatioTypeMedia, MediaProcessingTaskEntity.ProcessingAction action, bool forceCdnUpload, int? priority, ITransaction transaction)
        {
            MediaProcessingTaskEntity task = new MediaProcessingTaskEntity();
            task.AddToTransaction(transaction);

            try
            {
                if (!mediaRatioTypeMedia.MediaEntity.GetRelatedBrandId().HasValue)
                {
                    task.RelatedToCompanyId = mediaRatioTypeMedia.MediaEntity.GetRelatedCompanyId();
                }
            }
            catch
            {
                // Nothing to save here... 
                if (!mediaRatioTypeMedia.MediaEntity.RelatedEntityId.HasValue)
                    return;

                throw;
            }

            if (priority.HasValue)
                task.Priority = priority.Value;
            if (action == MediaProcessingTaskEntity.ProcessingAction.Delete)
            {
                task.MediaRatioTypeMediaIdNonRelationCopy = mediaRatioTypeMedia.MediaRatioTypeMediaId;
                task.PathFormat = MediaHelper.GetMediaRatioTypeMediaPath(mediaRatioTypeMedia, MediaRatioTypeMediaEntity.FileNameType.DeletionWildcard);
            }
            else
            {
                if (!forceCdnUpload &&
                    mediaRatioTypeMedia.LastDistributedVersionTicks != null &&
                    mediaRatioTypeMedia.LastDistributedVersionTicks > mediaRatioTypeMedia.UpdatedUTC.GetValueOrDefault().Ticks)
                {
                    return;
                }

                task.MediaRatioTypeMediaId = mediaRatioTypeMedia.MediaRatioTypeMediaId;
                task.PathFormat = MediaHelper.GetMediaRatioTypeMediaPath(mediaRatioTypeMedia, MediaRatioTypeMediaEntity.FileNameType.Format);
            }
            task.ActionAsEnum = action;
            task.Save();
        }

        /// <summary>
        /// Gets the media ratio type media path, if you convert the backslashed to forward slashed it's also the Cdn Url Path
        /// </summary>
        /// <param name="mediaRatioTypeMediaEntity">The media ratio type media entity.</param>
        /// <param name="fileNameType">Type of the file name.</param>
        /// <returns></returns>
        public static string GetMediaRatioTypeMediaPath(MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity, MediaRatioTypeMediaEntity.FileNameType fileNameType = MediaRatioTypeMediaEntity.FileNameType.Cdn)
        {
            string toReturn = string.Empty;

            List<string> pathComponents = new List<string>();

            if (mediaRatioTypeMediaEntity.MediaEntity.IsGeneric)
            {
                pathComponents.Add(MediaHelper.GenericFilesSubPath);
            }
            else if (mediaRatioTypeMediaEntity.MediaEntity.GetRelatedBrandId().HasValue)
            {
                pathComponents.Add(MediaHelper.BrandFilesSubPath);
                pathComponents.Add(mediaRatioTypeMediaEntity.MediaEntity.GetRelatedBrandId().ToString());
            }
            else
            {
                pathComponents.Add(mediaRatioTypeMediaEntity.MediaEntity.GetRelatedCompanyId().ToString());
            }

            if (mediaRatioTypeMediaEntity.MediaRatioType != null)
            {
                // Add DeviceType                       
                pathComponents.Add(mediaRatioTypeMediaEntity.MediaRatioType.DeviceType.ToString());

                // Entity Type Name            
                pathComponents.Add(MediaHelper.GetEntitySubDirectoryName(mediaRatioTypeMediaEntity));
            }

            // Get the FileNameFormat
            pathComponents.Add(mediaRatioTypeMediaEntity.GetFileName(fileNameType));

            toReturn = string.Join("\\", pathComponents);

            return toReturn;
        }

        /// <summary>
        /// Filters out MediaRatioTypeMedia that doesn't belong to the requested device type.
        /// </summary>
        /// <param name="media">Array of media for requested device type</param>
        public static Obymobi.Logic.Model.Media[] FilterMediaPerDevice(DeviceType deviceType, Model.Media[] mediaList)
        {
            List<Model.Media> toReturn = new List<Model.Media>();
            if (mediaList != null && mediaList.Length > 0)
            {
                if (deviceType == DeviceType.Unknown)
                {
                    // We don't know the device type, return all the media
                    toReturn.AddRange(mediaList);
                }
                else
                {
                    // We know the device type, get the allowed mediatypes for this device
                    int[] mediaTypesForDevice = MediaRatioTypes.GetMediaTypeArrayForDeviceTypeInts(DeviceType.Unknown, deviceType).ToArray();
                    for (int i = 0; i < mediaList.Length; i++)
                    {
                        Model.Media media = mediaList[i];
                        if (mediaTypesForDevice.Contains(media.MediaType))
                            toReturn.Add(media);
                    }
                }
            }
            return toReturn.ToArray();
        }

        /// <summary>
        /// Pushes a media model to terminals and clients
        /// </summary>
        /// <param name="mediaRatioTypeMediaEntity"></param>
        public static void PushMediaEntityToCompany(MediaEntity mediaEntity, MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity)
        {
            Model.Media model = MediaHelper.CreateMediaModelFromMediaRatioTypeMedia(mediaEntity, mediaRatioTypeMediaEntity);

            TerminalCollection terminals = new TerminalCollection();
            ClientCollection clients = new ClientCollection();

            if (mediaEntity.CompanyId.HasValue)
            {
                // Get terminals
                PredicateExpression terminalFilter = new PredicateExpression(TerminalFields.CompanyId == mediaEntity.CompanyId.Value);
                IncludeFieldsList terminalIncludes = new IncludeFieldsList(TerminalFields.TerminalId, TerminalFields.CompanyId);
                terminals = EntityCollection.GetMulti<TerminalCollection>(terminalFilter, null, null, null, terminalIncludes);

                // Get clients
                DateTime currentTimeUtcNow = DateTime.UtcNow;
                DateTime clientLastRequest = currentTimeUtcNow.AddMinutes(-3);

                RelationCollection relation = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

                PredicateExpression clientsFilter = new PredicateExpression(ClientFields.CompanyId == mediaEntity.CompanyId.Value);
                clientsFilter.Add(DeviceFields.LastRequestUTC > clientLastRequest);
                IncludeFieldsList clientIncludes = new IncludeFieldsList(ClientFields.ClientId, ClientFields.CompanyId);
                clients = EntityCollection.GetMulti<ClientCollection>(clientsFilter, null, relation, null, clientIncludes);
            }

            HostingEnvironment.QueueBackgroundWorkItem((_) =>
            {
                try
                {

                    string modelJson = model.ToJson();

                    foreach (ClientEntity client in clients)
                    {
                        try
                        {
                            CometHelper.PushModelToClient(client.ClientId, ModelType.Media, modelJson);
                        }
                        catch (Exception)
                        {
                            // ignored, too bad
                        }
                    }

                    foreach (TerminalEntity terminal in terminals)
                    {
                        try
                        {
                            CometHelper.PushModelToTerminal(terminal.TerminalId, ModelType.Media, modelJson);
                        }
                        catch (Exception)
                        {
                            // ignored, too bad
                        }

                    }
                }
                catch (Exception exc)
                {
                    ErrorLoggerWeb.LogError(exc, exc.Message);
                }

            });
        }

        public static Size GetImageSize(MediaEntity mediaEntity)
        {
            Size imageSize = default(Size);

            if (mediaEntity.SizeWidth.HasValue && mediaEntity.SizeHeight.HasValue)
            {
                imageSize = new Size(mediaEntity.SizeWidth.Value, mediaEntity.SizeHeight.Value);
            }
            else
            {
                byte[] fileBytes = mediaEntity.FileBytes;

                imageSize = MediaHelper.GetImageSize(fileBytes);
            }

            return imageSize;
        }

        public static Size GetImageSize(byte[] fileBytes)
        {
            Size imageSize = default(Size);

            if (fileBytes != null && fileBytes.Length > 0)
            {
                using (MemoryStream memoryStream = new MemoryStream(fileBytes))
                using (System.Drawing.Image image = System.Drawing.Image.FromStream(memoryStream))
                {
                    imageSize = image.Size;
                    memoryStream.Close();
                }
            }

            return imageSize;
        }

        public static string GetMediaCdnFileName(int mediaId)
        {
            string mediaCdnFilename = string.Empty;
            if (mediaId > 0)
            {
                MediaEntity mediaEntity = new MediaEntity(mediaId);
                if (!mediaEntity.IsNew)
                {
                    foreach (MediaRatioTypeMediaEntity mrt in mediaEntity.MediaRatioTypeMediaCollection)
                    {
                        if (mrt.MediaTypeAsEnum == MediaType.Gallery || mrt.MediaTypeAsEnum == MediaType.NotificationIconNormal)
                        {
                            mediaCdnFilename = MediaHelper.GetMediaRatioTypeMediaPath(mrt);
                            if (mrt.MediaTypeAsEnum == MediaType.NotificationIconNormal)
                                break;
                        }
                    }
                }
            }
            return mediaCdnFilename;
        }

        #endregion
    }
}
