﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.Comparers;
using Obymobi.Data.EntityClasses;
using Obymobi.Interfaces;

namespace Obymobi.Logic.HelperClasses
{
    public static class TagHelper
    {        
        public static string FormatTagName(string name)
        {
            // All characters should be lowercase
            name = name.ToLower();

            // Grab all word boundaries
            List<string> matches = new List<string>();
            foreach (Match match in Regex.Matches(name, @"(\b[\w]+)"))
            {
                matches.Add(match.Value);
            }

            // Separate words with dashes
            return string.Join("-", matches);
        }

        public static void UpdateTagsForEntity<TEntity, TTagEntity, TTagCollection>(TEntity entity, ICollection<ITag> currentTags)
            where TEntity : CommonEntityBase, ITagEntity
            where TTagEntity : CommonEntityBase, ITaggableEntity
            where TTagCollection : CommonEntityCollection<TTagEntity>
        {
            ICollection<ITag> oldTags = entity.GetTags();

            TTagCollection tagCollection = entity.TagCollection as TTagCollection;
            
            // Delete any removed tags
            DeleteTagsForEntity<TEntity, TTagEntity, TTagCollection>(entity, tagCollection, oldTags, currentTags);
            
            // Save & create new tags
            SaveNewTagsForEntity<TEntity, TTagEntity, TTagCollection>(entity, tagCollection, oldTags, currentTags);
        }

        private static void DeleteTagsForEntity<TEntity, TTagEntity, TTagCollection>(TEntity entity, TTagCollection tagCollection, ICollection<ITag> oldTags, ICollection<ITag> currentTags)
            where TEntity : CommonEntityBase, ITagEntity
            where TTagEntity : CommonEntityBase, ITaggableEntity
            where TTagCollection : CommonEntityCollection<TTagEntity>
        {
            ICollection<int> tagIdsToRemove = oldTags.Except(currentTags, new TagNameEqualityComparer()).Select(tag => tag.TagId).ToList();
            if (!tagIdsToRemove.Any()) return;

            string entityName = LLBLGenUtil.GetEntityName(entity);

            TTagCollection trackerCollection = DataFactory.EntityCollectionFactory.GetEntityCollection($"{entityName}Tag") as TTagCollection;
            tagCollection.RemovedEntitiesTracker = trackerCollection;

            foreach (int tagId in tagIdsToRemove)
            {
                TTagEntity tagEntity = tagCollection.FirstOrDefault(x => x.TagId == tagId);
                if (tagEntity == null || tagEntity.IsNew) continue;

                tagCollection.Remove(tagEntity);
            }

            trackerCollection.DeleteMulti();
        }

        private static void SaveNewTagsForEntity<TEntity, TTagEntity, TTagCollection>(TEntity entity, TTagCollection tagCollection, ICollection<ITag> oldTags, ICollection<ITag> currentTags)
            where TEntity : CommonEntityBase, ITagEntity
            where TTagEntity : CommonEntityBase, ITaggableEntity
            where TTagCollection : CommonEntityCollection<TTagEntity>
        {
            ICollection<ITag> newTags = currentTags.Except(oldTags, new TagNameEqualityComparer()).ToList();
            foreach (ITag tag in newTags)
            {
                TagEntity tagEntity = new TagEntity();
                if (tag.TagId > 0)
                {
                    tagEntity.TagId = tag.TagId;
                    tagEntity.IsNew = false;
                }
                else
                {
                    tagEntity.Name = tag.Name;
                    if (tag.CompanyId.GetValueOrDefault() <= 0 && entity.Fields["CompanyId"]?.CurrentValue != null)
                    {
                        tagEntity.CompanyId = (int?)entity.Fields["CompanyId"].CurrentValue;
                    }
                }

                TTagEntity taggableEntityTagEntity = tagCollection.AddNew();
                taggableEntityTagEntity.TagEntity = tagEntity;

                taggableEntityTagEntity.Save(recurse: tagEntity.IsNew);
            }
        }
    }
}
