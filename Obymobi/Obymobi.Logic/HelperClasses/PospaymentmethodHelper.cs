﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// PospaymentmethodHelper class
    /// </summary>
    public class PospaymentmethodHelper
    {
        /// <summary>
        /// Cleans up.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="cleanupReport">The cleanup report.</param>
        /// <param name="batchId">The batch id.</param>
        public static void CleanUp(int companyId, ref StringBuilder cleanupReport, out long batchId)
        {
            cleanupReport.AppendLine("*** START: Delete Pospaymentmethod entities ***");
            batchId = -1;

            // Prepate filter for company
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PospaymentmethodFields.CompanyId == companyId);

            // Create collection and use for Scalar MAX
            PospaymentmethodCollection pospaymentmethods = new PospaymentmethodCollection();
            object maxBatchId = pospaymentmethods.GetScalar(Data.PospaymentmethodFieldIndex.SynchronisationBatchId, null, AggregateFunction.Max, filter);

            // Only proceed when we have an actual maxBatchId
            if (maxBatchId != DBNull.Value && maxBatchId is long)
            {
                // Retrieve all order pospaymentmethods & delete them
                cleanupReport.AppendFormatLine("BatchId: {0}", maxBatchId);
                batchId = (long)maxBatchId;
                // Prepare filter for older pospaymentmethods
                filter = new PredicateExpression();
                filter.Add(PospaymentmethodFields.CompanyId == companyId);
                filter.Add(PospaymentmethodFields.SynchronisationBatchId < maxBatchId);

                pospaymentmethods.GetMulti(filter);

                // Delete per piece
                foreach (var pospaymentmethod in pospaymentmethods)
                {
                    cleanupReport.AppendFormatLine("ExternalId: {0}, Name: {1}",
                    pospaymentmethod.ExternalId, pospaymentmethod.Name);
                    pospaymentmethod.Delete();
                }
                cleanupReport.AppendFormatLine("{0} entities deleted", pospaymentmethods.Count);
            }

            cleanupReport.AppendLine("*** FINISHED: Delete Pospaymentmethod entities ***");
        }

        /// <summary>
        /// Import the paymentmethods from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="xml">The Xml string containing the serialized array of Pospaymentmethod instances</param>
        /// <param name="companyId">The id of the company to import the pos paymentmethods for</param>
        public static void ImportPospaymentmethods(string xml, int companyId, string revenueCenter = null)
        {
            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPospaymentmethodsResult.XmlIsEmpty, "Xml string containing serialized Pospaymentmethod array is empty.");
            else if (companyId <= 1)
                throw new ObymobiException(ImportPospaymentmethodsResult.CompanyIdIsZeroOrLess, "The specified company id '{0}' is smaller than 1.", companyId);
            else
            {
                // Deserialize the xml in order to get the array of Pospaymentmethod instances
                Pospaymentmethod[] pospaymentmethods = XmlHelper.Deserialize<Pospaymentmethod[]>(xml);

                // Get a PospaymentmethodCollection for the specified company
                PospaymentmethodCollection pospaymentmethodCollection = GetPospaymentmethodCollection(companyId);

                // Create an EntityView for the PospaymentmethodCollection
                EntityView<PospaymentmethodEntity> pospaymentmethodView = pospaymentmethodCollection.DefaultView;

                // Create a filter
                PredicateExpression filter = null;

                // Walk through the Pospaymentmethod instances of the array
                for (int i = 0; i < pospaymentmethods.Length; i++)
                {
                    Pospaymentmethod pospaymentmethod = pospaymentmethods[i];

                    // Initialize the filter
                    filter = new PredicateExpression(PospaymentmethodFields.ExternalId == pospaymentmethod.ExternalId);

                    if (!string.IsNullOrWhiteSpace(revenueCenter))
                    {
                        filter.Add(PospaymentmethodFields.RevenueCenter == revenueCenter);
                    }

                    // Set the filter to the EntityView
                    pospaymentmethodView.Filter = filter;

                    // Check whether items have been found
                    if (pospaymentmethodView.Count == 0)
                    {
                        // No pos paymentmethod found in database, add

                        // First, set the company id
                        pospaymentmethod.CompanyId = companyId;

                        // Then, create a PospaymentmethodEntity instance
                        // and save it to the persistent storage
                        PospaymentmethodEntity pospaymentmethodEntity = CreatePospaymentmethodEntityFromModel(pospaymentmethod);
                        try
                        {
                            pospaymentmethodEntity.Save();
                        }
                        catch (Exception ex)
                        {
                            throw new ObymobiException(GenericResult.EntitySaveException, ex);
                        }
                    }
                    else if (pospaymentmethodView.Count == 1)
                    {
                        // Pos paymentmethod found in database, update
                        PospaymentmethodEntity pospaymentmethodEntity = pospaymentmethodView[0];
                        pospaymentmethodEntity.CompanyId = companyId;
                        pospaymentmethodEntity.ExternalId = pospaymentmethod.ExternalId;
                        pospaymentmethodEntity.Name = pospaymentmethod.Name;

                        pospaymentmethodEntity.FieldValue1 = pospaymentmethod.FieldValue1;
                        pospaymentmethodEntity.FieldValue2 = pospaymentmethod.FieldValue2;
                        pospaymentmethodEntity.FieldValue3 = pospaymentmethod.FieldValue3;
                        pospaymentmethodEntity.FieldValue4 = pospaymentmethod.FieldValue4;
                        pospaymentmethodEntity.FieldValue5 = pospaymentmethod.FieldValue5;
                        pospaymentmethodEntity.FieldValue6 = pospaymentmethod.FieldValue6;
                        pospaymentmethodEntity.FieldValue7 = pospaymentmethod.FieldValue7;
                        pospaymentmethodEntity.FieldValue8 = pospaymentmethod.FieldValue8;
                        pospaymentmethodEntity.FieldValue9 = pospaymentmethod.FieldValue8;
                        pospaymentmethodEntity.FieldValue10 = pospaymentmethod.FieldValue10;

                        pospaymentmethodEntity.RevenueCenter = pospaymentmethod.RevenueCenter;

                        try
                        {
                            pospaymentmethodEntity.Save();
                        }
                        catch (Exception ex)
                        {
                            throw new ObymobiException(GenericResult.EntitySaveException, ex);
                        }
                    }
                    else if (pospaymentmethodView.Count > 1)
                    {
                        // Multiple pos paymentmethods found in database for the specified external id, error!
                        throw new ObymobiException(ImportPospaymentmethodsResult.MultiplePospaymentmethodsFound, "Multiple pos paymentmethods found for the specified external id '{0}'.", pospaymentmethod.ExternalId);
                    }
                }
            }
        }

        /// <summary>
        /// Gets a PospaymentmethodCollection for the specified company id
        /// </summary>
        /// <param name="companyId">The id of the company to get the PospaymentmethodCollection for</param>
        /// <returns>An Obymobi.Data.PospaymentmethodCollection instance</returns>
        public static PospaymentmethodCollection GetPospaymentmethodCollection(int companyId)
        {
            PospaymentmethodCollection pospaymentmethodCollection = new PospaymentmethodCollection();
            pospaymentmethodCollection.GetMulti(new PredicateExpression(PospaymentmethodFields.CompanyId == companyId));

            return pospaymentmethodCollection;
        }

        /// <summary>
        /// Creates a PospaymentmethodEntity instance for the specified Pospaymentmethod instance
        /// </summary>
        /// <param name="pospaymentmethod">The Pospaymentmethod instance to create the PospaymentmethodEntity instance for</param>
        /// <returns>An Obymobi.Data.EntityClasses.PospaymentmethodEntity instance</returns>
        public static PospaymentmethodEntity CreatePospaymentmethodEntityFromModel(Pospaymentmethod pospaymentmethod)
        {
            PospaymentmethodEntity pospaymentmethodEntity = new PospaymentmethodEntity();
            pospaymentmethodEntity.CompanyId = pospaymentmethod.CompanyId;
            pospaymentmethodEntity.ExternalId = pospaymentmethod.ExternalId;
            pospaymentmethodEntity.Name = pospaymentmethod.Name;

            pospaymentmethodEntity.FieldValue1 = pospaymentmethod.FieldValue1;
            pospaymentmethodEntity.FieldValue2 = pospaymentmethod.FieldValue2;
            pospaymentmethodEntity.FieldValue3 = pospaymentmethod.FieldValue3;
            pospaymentmethodEntity.FieldValue4 = pospaymentmethod.FieldValue4;
            pospaymentmethodEntity.FieldValue5 = pospaymentmethod.FieldValue5;
            pospaymentmethodEntity.FieldValue6 = pospaymentmethod.FieldValue6;
            pospaymentmethodEntity.FieldValue7 = pospaymentmethod.FieldValue7;
            pospaymentmethodEntity.FieldValue8 = pospaymentmethod.FieldValue8;
            pospaymentmethodEntity.FieldValue9 = pospaymentmethod.FieldValue8;
            pospaymentmethodEntity.FieldValue10 = pospaymentmethod.FieldValue10;

            pospaymentmethodEntity.RevenueCenter = pospaymentmethod.RevenueCenter;

            return pospaymentmethodEntity;
        }
    }
}
