﻿using System;
using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    public static class MessageHelper
    {
        public static MessageTemplate[] GetMessageTemplatesForTerminal(int terminalId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.AddWithOr(TerminalMessageTemplateCategoryFields.TerminalId == terminalId);

            RelationCollection relations = new RelationCollection();
            relations.Add(MessageTemplateEntityBase.Relations.MessageTemplateCategoryMessageTemplateEntityUsingMessageTemplateId);
            relations.Add(MessageTemplateCategoryMessageTemplateEntityBase.Relations.MessageTemplateCategoryEntityUsingMessageTemplateCategoryId);
            relations.Add(MessageTemplateCategoryEntityBase.Relations.TerminalMessageTemplateCategoryEntityUsingMessageTemplateCategoryId);

            MessageTemplateCollection messageTemplateCollection = new MessageTemplateCollection();
            messageTemplateCollection.GetMulti(filter, relations);

            List<MessageTemplate> messageTemplates = new List<MessageTemplate>();
            foreach (MessageTemplateEntity messageTemplateEntity in messageTemplateCollection)
            {
                MessageTemplate messageTemplate = MessageTemplateHelper.CreateMessageTemplateModelFromEntity(messageTemplateEntity);
                messageTemplates.Add(messageTemplate);
            }

            return messageTemplates.ToArray();
        }

        public static MessageRecipientEntity CreateMessageRecipientEntity(int customerId, int clientId, int deliverypointId, Model.Action action)
        {
            MessageRecipientEntity entity = new MessageRecipientEntity();
            entity.Queued = true;

            if (customerId > 0)
            {
                CustomerEntity customer = new CustomerEntity(customerId);
                if (customer.IsNew)
                {
                    throw new ObymobiException(GenericResult.Failure, "Customer with id {0} does not exists.", customerId);
                }
                else
                {
                    entity.CustomerId = customerId;
                }                
            }

            if (clientId > 0)
            {
                ClientEntity client = new ClientEntity(clientId);
                if (client.IsNew)
                {
                    throw new ObymobiException(GenericResult.Failure, "Client with id {0} does not exists.", clientId);
                }
                else
                {
                    entity.ClientId = clientId;
                }                
            }

            if (deliverypointId > 0)
            {
                // No validation to the database since all requests to this method because the deliverypointId is passed directly from entities
                entity.DeliverypointId = deliverypointId;                
            }

            if (action != null)
            {
                if (action.EntertainmentId > 0)
                {
                    entity.EntertainmentId = action.EntertainmentId;
                }
                if (action.CategoryId > 0)
                {
                    entity.CategoryId = action.CategoryId;
                }
                if (action.ProductId > 0)
                {
                    entity.ProductId = action.ProductId;
                }
            }

            return entity;
        }        

        public static Message CreateMessageModelFromEntity(MessageEntity messageEntity)
        {
            Message message = new Message();            
            message.CompanyId = messageEntity.CompanyId;
            message.Title = messageEntity.Title;
            message.Text = messageEntity.Message;
            message.Duration = messageEntity.Duration;
            message.MediaId = messageEntity.MediaId.GetValueOrDefault(0);
            message.NotifyOnYes = messageEntity.NotifyOnYes;
            message.MessageLayoutType = (int)messageEntity.MessageLayoutType;

            if (messageEntity.ProductCategoryId > 0)
            {
                message.ProductId = messageEntity.ProductCategoryEntity.ProductId;
                message.CategoryId = messageEntity.ProductCategoryEntity.CategoryId;
            }

            if (message.CategoryId <= 0)
            {
                if (messageEntity.CategoryId.GetValueOrDefault(0) > 0)
                {
                    message.CategoryId = messageEntity.CategoryId.GetValueOrDefault(0);
                }
            }

            if (message.ProductId <= 0)
            {
                message.ProductId = messageEntity.ProductId.GetValueOrDefault(0);
            }

            message.EntertainmentId = messageEntity.EntertainmentId.GetValueOrDefault(0);
            message.SiteId = messageEntity.SiteId.GetValueOrDefault(0);
            message.PageId = messageEntity.PageId.GetValueOrDefault(0);
            message.Url = messageEntity.Url;
            message.Urgent = messageEntity.Urgent;

            if (!messageEntity.MediaId.IsNullOrZero())
            {
                List<Model.Media> media = new List<Model.Media>();
                media.AddRange(MediaHelper.CreateMediaModelFromEntity(messageEntity.MediaEntity, string.Empty));
                foreach (MediaEntity mediaCulture in messageEntity.MediaEntity.MediaCollection)
                {
                    media.AddRange(MediaHelper.CreateMediaModelFromEntity(mediaCulture, string.Empty));
                }
                message.Media = media.ToArray();
            }

            return message;
        }

        public static Message SetMessageRecipientInfo(Message originalMessage, MessageRecipientEntity messageRecipientEntity)
        {
            Message message = originalMessage.Clone();

            message.MessageId = messageRecipientEntity.MessageRecipientId; // FO: Yes, we send the messageRecipientId as a messageId to keep the model backwards compatible
            message.CustomerId = messageRecipientEntity.CustomerId ?? 0;
            message.ClientId = messageRecipientEntity.ClientId ?? 0;
            message.DeliverypointId = messageRecipientEntity.DeliverypointId ?? 0;

            if (messageRecipientEntity.CategoryId.GetValueOrDefault(0) > 0)
            {
                message.CategoryId = messageRecipientEntity.CategoryId.GetValueOrDefault(0);
            }

            if (messageRecipientEntity.ProductId.GetValueOrDefault(0) > 0)
            {
                message.ProductId = messageRecipientEntity.ProductId.GetValueOrDefault(0);
            }

            if (messageRecipientEntity.EntertainmentId.GetValueOrDefault(0) > 0)
            {
                message.EntertainmentId = messageRecipientEntity.EntertainmentId.GetValueOrDefault(0);
            }

            return message;
        }
    }
}