﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;

namespace Obymobi.Logic.HelperClasses
{
    public static class TimeZoneHelper
    {
        public static DateTime ConvertFromUtc(DateTime dateTime, string timeZoneOlsonId)
        {
            return TimeZoneHelper.ConvertFromUtc(dateTime, Obymobi.TimeZone.Mappings[timeZoneOlsonId]);
        }

        public static DateTime ConvertFromUtc(DateTime dateTime, Obymobi.TimeZone timeZone)
        {
            return dateTime.UtcToLocalTime(TimeZoneHelper.GetTimeZoneInfo(timeZone));
        }

        public static TimeZoneInfo GetTimeZoneInfo(string timeZoneOlsonId)
        {
            return TimeZoneHelper.GetTimeZoneInfo(Obymobi.TimeZone.Mappings[timeZoneOlsonId]);
        }

        public static TimeZoneInfo GetTimeZoneInfo(Obymobi.TimeZone timeZone)
        {
            return TimeZoneInfo.FindSystemTimeZoneById(timeZone.WindowsTimeZoneId);
        }
    }
}
