﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;

namespace Obymobi.Logic.HelperClasses
{
    public static class InfraredCommandHelper
    {
        public static InfraredCommand[] ConvertInfraredCommandsToArray(InfraredCommandCollection entities)
        {
            List<InfraredCommand> toReturn = new List<InfraredCommand>();
            foreach (InfraredCommandEntity entity in entities)
            {
                toReturn.Add(InfraredCommandHelper.ConvertInfraredCommandEntityToModel(entity));
            }
            return toReturn.ToArray();
        }

        public static InfraredCommand ConvertInfraredCommandEntityToModel(InfraredCommandEntity entity)
        {
            InfraredCommand model = new InfraredCommand();
            model.InfraredCommandId = entity.InfraredCommandId;
            model.Type = (int)entity.Type;
            model.Name = entity.Name;
            model.Hex = entity.Hex;
            model.CustomTexts = CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection, "INFRAREDCONFIGURATIONS_INFRAREDCOMMANDS");

            return model;
        }

        public static InfraredCommandEntity ConvertInfraredCommandModelToEntity(InfraredCommand model)
        {
            InfraredCommandEntity entity = new InfraredCommandEntity();
            entity.InfraredCommandId = entity.InfraredCommandId;
            entity.Type = model.Type.ToEnum<InfraredCommandType>();
            entity.Name = model.Name;
            entity.Hex = model.Hex;

            return entity;
        }        
    }
}
