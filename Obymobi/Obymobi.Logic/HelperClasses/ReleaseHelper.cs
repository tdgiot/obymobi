﻿using Dionysos;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// Helper class for Application Releases
    /// </summary>
    public class ReleaseHelper
    {
        public enum ReleaseHelperErrors
        {
            /// <summary>
            /// No stable release is set for the application
            /// </summary>
            NoStableReleaseSetForApplication = 200,
            /// <summary>
            /// Release does not exist on disk
            /// </summary>
            ReleaseDoesNotExistsOnDisk = 300,
            /// <summary>
            /// Maximum download connections reached
            /// </summary>
            MaximumDownloadConnectionsReached = 400,
            /// <summary>
            /// No company id was specified
            /// </summary>
            NoCompanyIdSpecified = 500,
            /// <summary>
            /// No company id was specified
            /// </summary>
            NoReleaseIdSpecified = 600,
        }

        /// <summary>
        /// Gets the amount of devices that are currently downloading a release
        /// </summary>
        /// <param name="companyId">The id of the company.</param>
        /// <returns>An integer containing the number of devices currently downloading.</returns>
        public static int GetAmountOfDownloadingDevices(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));
            filter.Add(DeviceFields.UpdateStatus == UpdateStatus.DownloadStarted);
            filter.Add(DeviceFields.UpdateStatusChangedUTC > DateTime.UtcNow.AddMinutes(-60)); // Within an hour

            RelationCollection relations = new RelationCollection();
            relations.Add(DeviceEntityBase.Relations.ClientEntityUsingDeviceId, JoinHint.Inner);

            DeviceCollection devices = new DeviceCollection();
            return devices.GetDbCount(filter, relations);
        }

        /// <summary>
        /// Gets the company release entity (if set) for the specified application code and company id
        /// </summary>
        /// <param name="applicationCode">The code of the application.</param>
        /// <param name="companyId">The id of the company.</param>
        /// <param name="excludeFileField">The flag indicating whether the file field should be excluded.</param>
        /// <returns>A <see cref="CompanyReleaseEntity"/> instance if a company release was set, otherwise Null.</returns>
        public static ReleaseEntity GetCompanyRelease(string applicationCode, int companyId)
        {
            ReleaseEntity companyRelease = null;

            // Create the relations
            RelationCollection companyReleaseRelations = new RelationCollection();
            companyReleaseRelations.Add(CompanyReleaseEntityBase.Relations.ReleaseEntityUsingReleaseId);
            companyReleaseRelations.Add(ReleaseEntityBase.Relations.ApplicationEntityUsingApplicationId);
            companyReleaseRelations.Add(CompanyReleaseEntityBase.Relations.CompanyEntityUsingCompanyId);

            // Create the filter
            PredicateExpression companyReleasefilter = new PredicateExpression();
            companyReleasefilter.Add(ApplicationFields.Code == applicationCode);
            if (companyId > 0)
                companyReleasefilter.Add(CompanyFields.CompanyId == companyId);

            ExcludeFieldsList excludedFields = new ExcludeFieldsList(ReleaseFields.File);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyReleaseEntity);
            prefetch.Add(CompanyReleaseEntityBase.PrefetchPathReleaseEntity, excludedFields);

            // Get the collection
            CompanyReleaseCollection companyReleases = new CompanyReleaseCollection();
            companyReleases.GetMulti(companyReleasefilter, 0, null, companyReleaseRelations, prefetch, excludedFields, 0, 0);

            if (companyReleases.Count > 0)
                companyRelease = companyReleases[0].ReleaseEntity;

            return companyRelease;
        }

        /// Gets the company release entity (if set) for the specified application code and company id
        /// </summary>
        /// <param name="deviceModel">Device model for which a release should be fetched</param>
        /// <param name="companyId">The id of the company.</param>
        /// <param name="excludeFileField">The flag indicating whether the file field should be excluded.</param>
        /// <returns>A <see cref="CompanyReleaseEntity"/> instance if a company release was set, otherwise Null.</returns>
        public static ReleaseEntity GetCompanyRelease(DeviceModel deviceModel, int companyId)
        {
            string applicationCode = ReleaseHelper.GetApplicationCodeForDeviceModel(deviceModel);
            if (applicationCode.IsNullOrWhiteSpace())
            {
                return null;
            }

            return ReleaseHelper.GetCompanyRelease(applicationCode, companyId);
        }

        /// <summary>
        /// Get Application release code by <see cref="Obymobi.Enums.DeviceModel"/>
        /// </summary>
        /// <param name="deviceModel">Device Model type</param>
        /// <returns>Application code as string, empty if not found</returns>
        public static string GetApplicationCodeForDeviceModel(DeviceModel deviceModel)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ApplicationFields.DeviceModel == deviceModel);

            ApplicationCollection applicationCollection = new ApplicationCollection();
            applicationCollection.GetMulti(filter);

            if (applicationCollection.Count >= 1)
            {
                return applicationCollection[0].Code;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the stable release (if set) for the specified application code
        /// </summary>
        /// <param name="applicationCode">The code of the application.</param>
        /// <returns>A <see cref="ReleaseEntity"/> instance of a release was set, otherwise Null.</returns>
        public static ReleaseEntity GetStableReleaseByCode(string applicationCode)
        {
            ReleaseEntity stableRelease = null;

            PredicateExpression releaseFilter = new PredicateExpression();
            releaseFilter.Add(ApplicationFields.Code == applicationCode);
            releaseFilter.Add(ApplicationFields.ReleaseId != DBNull.Value);

            RelationCollection releaseRelations = new RelationCollection();
            releaseRelations.Add(ApplicationEntityBase.Relations.ReleaseEntityUsingReleaseId);

            ExcludeFieldsList excludedFields = new ExcludeFieldsList { ReleaseFields.File };

            ReleaseCollection releases = new ReleaseCollection();
            releases.GetMulti(releaseFilter, 0, null, releaseRelations, null, excludedFields, 0, 0);

            if (releases.Count > 0)
                stableRelease = releases[0];

            return stableRelease;
        }

        /// <summary>
        /// Gets the stable release (if set) for the specified application id
        /// </summary>
        /// <param name="applicationCode">The code of the application.</param>
        /// <returns>A <see cref="ReleaseEntity"/> instance of a release was set, otherwise Null.</returns>
        public static ReleaseEntity GetStableReleaseByApplicationId(int applicationId)
        {
            ReleaseEntity stableRelease = null;

            PredicateExpression releaseFilter = new PredicateExpression();
            releaseFilter.Add(ApplicationFields.ApplicationId == applicationId);
            releaseFilter.Add(ApplicationFields.ReleaseId != DBNull.Value);

            IncludeFieldsList includes = new IncludeFieldsList();
            includes.Add(ApplicationFields.ApplicationId);
            includes.Add(ApplicationFields.ReleaseId);
            includes.Add(ReleaseFields.Version);

            RelationCollection releaseRelations = new RelationCollection();
            releaseRelations.Add(ApplicationEntityBase.Relations.ReleaseEntityUsingReleaseId);

            ReleaseCollection releases = new ReleaseCollection();
            releases.GetMulti(releaseFilter, 0, null, releaseRelations, null, includes, 0, 0);

            if (releases.Count > 0)
                stableRelease = releases[0];

            return stableRelease;
        }

        /// <summary>
        /// Returns the release version for the supplied company.
        /// If the company doesn't have a stable version, the global stable will be returned
        /// </summary>
        /// <param name="applicationCode">Application name</param>
        /// <param name="companyId">Id of the company</param>
        /// <returns>Version number as string</returns>
        public static string GetVersionForCompany(string applicationCode, int companyId)
        {
            string toReturn = "0";

            ReleaseEntity companyRelease = null;
            ReleaseEntity stableRelease = null;

            int applicationId = GetApplicationIdForApplicationCode(applicationCode);
            if (applicationId > 0)
            {
                // Get the releases
                ReleaseCollection companyReleases = GetReleases(applicationId, companyId);

                if (companyReleases.Count > 0)
                    companyRelease = companyReleases[0];

                // Get the stable release
                stableRelease = GetStableReleaseByApplicationId(applicationId);
            }

            if (stableRelease != null)
            {
                // Create the object for the release to download
                ReleaseEntity releaseToDownload;

                if (companyRelease != null)
                {
                    int companyVersion;
                    int stableVersion;

                    if (int.TryParse(companyRelease.Version, out companyVersion) && int.TryParse(stableRelease.Version, out stableVersion) && companyVersion > stableVersion)
                        releaseToDownload = companyRelease;
                    else
                        releaseToDownload = stableRelease;
                }
                else
                    releaseToDownload = stableRelease;


                toReturn = releaseToDownload.Version;
            }

            return toReturn;
        }

        /// <summary>
        /// Get the applicationId for an application code for performance issues
        /// </summary>
        /// <param name="applicationCode">The code where to get the application id for</param>
        /// <returns>ApplicationId</returns>
        public static int GetApplicationIdForApplicationCode(string applicationCode)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ApplicationFields.Code == applicationCode);

            IncludeFieldsList includes = new IncludeFieldsList();
            includes.Add(ApplicationFields.ApplicationId);
            includes.Add(ApplicationFields.Code);

            ApplicationCollection applications = new ApplicationCollection();
            applications.GetMulti(filter, includes, null);

            int applicationId = -1;

            if (applications.Count > 0)
                applicationId = applications[0].ApplicationId;

            return applicationId;
        }

        /// <summary>
        /// Gets the releaseIds for a specific application id
        /// </summary>
        /// <param name="applicationId">The application id</param>
        /// <returns>ReleaseIds</returns>
        public static ReleaseCollection GetReleases(int applicationId, int? companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ReleaseFields.ApplicationId == applicationId);
            filter.Add(ReleaseFields.Filename != DBNull.Value);

            if (companyId != null)
                filter.Add(CompanyReleaseFields.CompanyId == companyId);

            IncludeFieldsList includes = new IncludeFieldsList();
            includes.Add(ReleaseFields.ReleaseId);
            includes.Add(ReleaseFields.ApplicationId);
            includes.Add(ReleaseFields.Version);
            includes.Add(CompanyReleaseFields.CompanyId);

            RelationCollection relations = new RelationCollection();
            relations.Add(ReleaseEntityBase.Relations.CompanyReleaseEntityUsingReleaseId);

            ReleaseCollection releases = new ReleaseCollection();
            releases.GetMulti(filter, 0, null, relations, null, includes, 0, 0);

            return releases;
        }

        public static void UpdateVersionForCompaniesWithoutCompanyRelease(ApplicationEntity application, int? releaseIdToExclude)
        {
            if (!application.ReleaseId.HasValue)
            {
                return;
            }

            // Update the companies that do not have 
            // a company release set for the specified application 
            PredicateExpression subfilter = new PredicateExpression();
            subfilter.Add(ReleaseFields.ApplicationId == application.ApplicationId);

            if (releaseIdToExclude.HasValue)
                subfilter.Add(ReleaseFields.ReleaseId != releaseIdToExclude.Value);

            RelationCollection subrelations = new RelationCollection();
            subrelations.Add(CompanyEntityBase.Relations.CompanyReleaseEntityUsingCompanyId);
            subrelations.Add(CompanyReleaseEntityBase.Relations.ReleaseEntityUsingReleaseId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(new FieldCompareSetPredicate(CompanyFields.CompanyId, CompanyFields.CompanyId, SetOperator.In, subfilter, subrelations, true));

            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(filter);

            foreach (CompanyEntity companyEntity in companyCollection)
            {
                ReleaseHelper.UpdateVersionOnCompanyForRelease(companyEntity, application.ReleaseEntity);
            }
        }

        public static void UpdateVersionForCompaniesWithCompanyRelease(ApplicationEntity application, int? releaseIdToExclude)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ReleaseFields.ApplicationId == application.ApplicationId);

            if (releaseIdToExclude.HasValue)
                filter.Add(ReleaseFields.ReleaseId != releaseIdToExclude.Value);

            RelationCollection relations = new RelationCollection();
            relations.Add(CompanyReleaseEntityBase.Relations.ReleaseEntityUsingReleaseId);

            CompanyReleaseCollection companyReleaseCollection = new CompanyReleaseCollection();
            companyReleaseCollection.GetMulti(filter, relations);

            foreach (CompanyReleaseEntity companyReleaseEntity in companyReleaseCollection)
            {
                ReleaseHelper.UpdateVersionOnCompanyForRelease(companyReleaseEntity.CompanyEntity, companyReleaseEntity.ReleaseEntity);
            }
        }

        public static void UpdateVersionOnCompanyForRelease(CompanyEntity company, ReleaseEntity release)
        {
            string applicationCode = release.ApplicationEntity.Code;

            if (applicationCode == ApplicationCode.Emenu)
            {
                company.ClientApplicationVersion = release.Version;
            }
            else if (applicationCode == ApplicationCode.Agent)
            {
                company.AgentVersion = release.Version;
            }
            else if (applicationCode == ApplicationCode.SupportTools)
            {
                company.SupportToolsVersion = release.Version;
            }
            else if (applicationCode == ApplicationCode.Console)
            {
                if (company.TerminalCollection.Count > 0 && (TerminalType)company.TerminalCollection[0].HandlingMethod == TerminalType.Console)
                {
                    company.TerminalApplicationVersion = release.Version;
                }
            }
            else if (applicationCode == ApplicationCode.OnsiteServer)
            {
                if (company.TerminalCollection.Count > 0 && (TerminalType)company.TerminalCollection[0].HandlingMethod == TerminalType.OnSiteServer)
                {
                    company.TerminalApplicationVersion = release.Version;
                }
            }
            else
            {
                return;
            }

            company.Save();
        }
    }
}
