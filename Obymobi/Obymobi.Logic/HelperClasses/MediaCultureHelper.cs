﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.HelperClasses
{
    public static class MediaCultureHelper
    {
        #region Conversion

        public static MediaCulture[] ConvertMediaCultureCollectionToArray(MediaCultureCollection collection, string parent)
        {
            List<MediaCulture> list = new List<MediaCulture>();
            foreach (MediaCultureEntity entity in collection)
            {
                list.Add(MediaCultureHelper.ConvertEntityToModel(entity, parent));
            }
            return list.ToArray();
        }

        public static MediaCulture ConvertEntityToModel(MediaCultureEntity entity, string parent)
        {
            MediaCulture model = new MediaCulture();
            model.MediaCultureId = entity.MediaCultureId;
            model.MediaId = entity.MediaId;
            model.CultureCode = entity.CultureCode;
            model.Parent = parent;

            return model;
        }

        #endregion
    }
}
