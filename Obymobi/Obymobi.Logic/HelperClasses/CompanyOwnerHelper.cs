﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Dionysos.Web;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// CompanyOwnerHelper class
    /// </summary>
    public class CompanyOwnerHelper
    {
        /// <summary>
        /// Checks if the password for this companyOwner is saved encrypted or not
        /// </summary>
        /// <param name="username">The username of the companyOwner</param>
        /// <returns>boolean</returns>
        public static Boolean IsCompanyOwnerPasswordEncrypted(string username)
        {
            bool encrypted = false;

            var companyOwners = new CompanyOwnerCollection();
            companyOwners.GetMulti(CompanyOwnerFields.Username == username);

            if (companyOwners.Count == 1 && companyOwners[0].IsPasswordEncrypted)
                encrypted = true;

            return encrypted;
        }
    }
}
