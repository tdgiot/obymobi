﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Logic.HelperClasses
{
    public class ClientStateHelper
    {
        /// <summary>
        /// Gets the latest client state entity for the specified client id
        /// </summary>
        /// <param name="clientId">The id of the client</param>
        /// <returns>The latest <see cref="ClientStateEntity"/> instance if it exists, otherwise null</returns>
        private static ClientStateEntity GetLatestClientStateEntity(int clientId, ITransaction transaction = null)
        {
            ClientStateEntity clientStateEntity = null;

            var filter = new PredicateExpression();
            filter.Add(ClientStateFields.ClientId == clientId);

            var sort = new SortExpression(new SortClause(ClientStateFields.ClientStateId, SortOperator.Descending));

            var clientStateCollection = new ClientStateCollection();
            clientStateCollection.AddToTransaction(transaction);
            clientStateCollection.GetMulti(filter, 1, sort);

            if (clientStateCollection.Count == 1)
                clientStateEntity = clientStateCollection[0];

            return clientStateEntity;
        }

        /// <summary>
        /// Updates the client state if the state of the client has changed
        /// </summary>
        /// <param name="client">The <see cref="ClientEntity"/> instance to update the client state for</param>
        /// <param name="online">The flag which indicates whether the client is online</param>
        public static void UpdateClientState(ClientEntity client, bool online, ITransaction transaction = null)
        {
            string message = string.Empty;
            ClientStateEntity lastClientStateEntity = null;

            if (HasClientStateChanged(client, online, out message, out lastClientStateEntity, transaction))
            {
                DateTime utcNow = DateTime.UtcNow;

                var clientStateEntity = new ClientStateEntity();
                clientStateEntity.AddToTransaction(transaction);
                clientStateEntity.ClientId = client.ClientId;
                clientStateEntity.Online = online;
                clientStateEntity.OperationMode = client.OperationMode;
                clientStateEntity.LastBatteryLevel = client.DeviceEntity.BatteryLevel;
                clientStateEntity.LastIsCharging = client.DeviceEntity.IsCharging;
                clientStateEntity.Message = message;
                clientStateEntity.CreatedUTC = utcNow;
                if (lastClientStateEntity != null && lastClientStateEntity.CreatedUTC.HasValue)
                    clientStateEntity.TimeSpan = (utcNow - lastClientStateEntity.CreatedUTC.Value);

                clientStateEntity.Save();
            }
        }

        /// <summary>
        /// Checks whether the client state has changed
        /// </summary>
        /// <param name="client">The <see cref="ClientEntity"/> instance to update the client state for</param>
        /// <param name="online">The flag which indicates whether the client is online</param>
        /// <param name="message">The message to add to the client state record</param>
        /// <returns>A flag which indicates whether the client state has changed</returns>
        public static bool HasClientStateChanged(ClientEntity client, bool online, out string message, out ClientStateEntity lastClientStateEntity, ITransaction transaction = null)
        {
            bool hasChanged = false;

            lastClientStateEntity = GetLatestClientStateEntity(client.ClientId, transaction);
            message = string.Empty;

            if (lastClientStateEntity == null) // No client state record existed yet
            {
                message = string.Format("No client state was known yet. Added initial client state.");
                hasChanged = true;
            }
            else if (lastClientStateEntity.Online != online) // Online status changed
            {
                if (online)
                    message = "Client came online.";
                else
                    message = "Client went offline.";

                hasChanged = true;
            }
            else if (lastClientStateEntity.OperationMode != client.LastOperationMode) // Operation mode changed
            {
                var clientOperationMode = ClientOperationMode.Unknown;
                if (client.LastOperationMode.HasValue)
                    clientOperationMode = (ClientOperationMode)client.LastOperationMode.Value;

                message = string.Format("Client operation mode changed from '{0}' to '{1}'.", lastClientStateEntity.OperationModeAsEnum.ToString(), clientOperationMode.ToString());
                hasChanged = true;
            }

            return hasChanged;
        }
    }
}
