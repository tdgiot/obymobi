﻿using System.Collections.Generic;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Logic.HelperClasses
{
    public static class InfraredConfigurationHelper
    {
        public static InfraredConfiguration ConvertInfraredConfigurationEntityToModel(InfraredConfigurationEntity entity)
        {
            InfraredConfiguration model = new InfraredConfiguration();
            model.InfraredConfigurationId = entity.InfraredConfigurationId;
            model.Name = entity.Name;
            model.MillisecondsBetweenCommands = entity.MillisecondsBetweenCommands;
            model.InfraredCommands = InfraredCommandHelper.ConvertInfraredCommandsToArray(entity.InfraredCommandCollection);
            return model;
        }

        public static InfraredConfigurationEntity ConvertInfraredConfigurationModelToEntity(InfraredConfiguration model)
        {
            InfraredConfigurationEntity entity = new InfraredConfigurationEntity();
            entity.InfraredConfigurationId = model.InfraredConfigurationId;
            entity.Name = model.Name;
            entity.MillisecondsBetweenCommands = model.MillisecondsBetweenCommands;

            foreach (InfraredCommand command in model.InfraredCommands)
            {
                entity.InfraredCommandCollection.Add(InfraredCommandHelper.ConvertInfraredCommandModelToEntity(command));
            }

            return entity;
        }

        public static List<int> GetInfraredConfigurationIds(int roomControlConfigurationId)
        {
            List<int> infraredConfigurationIds = new List<int>();

            // RoomControlComponent
            // RoomControlSectionItem
            // RoomControlWidget

            return infraredConfigurationIds;
        }

        public static InfraredConfiguration[] GetInfraredConfigurations(List<int> infraredConfigurationIds)
        {
            PredicateExpression filter = new PredicateExpression(InfraredConfigurationFields.InfraredConfigurationId == infraredConfigurationIds.ToArray());

            PrefetchPath prefetch = new PrefetchPath(EntityType.InfraredConfigurationEntity);
            prefetch.Add(InfraredConfigurationEntity.PrefetchPathInfraredCommandCollection);

            InfraredConfigurationCollection configurations = new InfraredConfigurationCollection();
            configurations.GetMulti(filter, prefetch);

            List<InfraredConfiguration> infraredConfigurations = new List<InfraredConfiguration>();
            foreach (InfraredConfigurationEntity infraredConfigurationEntity in configurations)
            {
                infraredConfigurations.Add(InfraredConfigurationHelper.ConvertInfraredConfigurationEntityToModel(infraredConfigurationEntity));
            }

            return infraredConfigurations.ToArray();
        }
    }
}
