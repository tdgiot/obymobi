﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    ///     TerminalHelper class
    /// </summary>
    public static class TerminalHelper
    {
        public enum TerminalHelperResult
        {
            MissingParameters = 200,
            TerminalAlreadyLinkedToAnotherDevice = 201,
            TerminalIdDoesNotExist = 202,
            ApplicationCodeNotFound = 300
        }

        /// <summary>
        ///     Creates (but does NOT save) a TerminalLog entity pre-filled with the information about
        ///     the device change in the supplied TerminalEntity (based on Current en DbValue of DeviceId field).
        /// </summary>
        /// <param name="terminal"></param>
        /// <returns></returns>
        public static TerminalLogEntity CreateLogForDeviceChange(this TerminalEntity terminal)
        {
            TerminalLogEntity log = new TerminalLogEntity();
            log.AddToTransaction(terminal);
            log.TerminalId = terminal.TerminalId;
            log.TypeAsEnum = TerminalLogType.DeviceChanged;
            log.Log = string.Empty;

            // Fetch previous device
            DeviceEntity previousDevice = null;
            DeviceCollection devices = new DeviceCollection();
            devices.AddToTransaction(terminal);

            if (terminal.Fields[TerminalFields.DeviceId.Name].DbValue != null)
            {
                int previousDeviceId = (int)terminal.Fields[TerminalFields.DeviceId.Name].DbValue;

                PredicateExpression filter = new PredicateExpression(DeviceFields.DeviceId == previousDeviceId);
                previousDevice = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<DeviceEntity>(filter, devices);
            }

            // Fetch current device
            DeviceEntity currentDevice = null;
            if (terminal.DeviceId.HasValue)
            {
                currentDevice = terminal.DeviceEntity;
            }

            if (previousDevice != null)
            {
                if (currentDevice != null)
                {
                    // We came from a device and go to one.
                    log.Message = StringUtil.FormatSafe("Changed from Device '{0}-{1}' to '{2}-{3}'", previousDevice.DeviceId, previousDevice.Identifier, currentDevice.DeviceId, currentDevice.Identifier);
                }
                else
                {
                    // We came from a device and go to none
                    log.Message = StringUtil.FormatSafe("Device is unlinked: '{0}-{1}'", previousDevice.DeviceId, previousDevice.Identifier);
                }
            }
            else
            {
                // We didn't have a device yet, so first linking
                log.Message = StringUtil.FormatSafe("Device is linked: '{0}-{1}'", currentDevice.DeviceId, currentDevice.Identifier);
            }

            return log;
        }

        /// <summary>
        ///     Gets the current terminal operation mode
        /// </summary>
        /// <param name="terminalId">The id of the terminal</param>
        /// <returns>Current terminal operation mode</returns>
        public static TerminalOperationMode GetTerminalOperationMode(int terminalId)
        {
            TerminalOperationMode toReturn = TerminalOperationMode.Unknown;

            TerminalEntity terminal = new TerminalEntity(terminalId);
            if (!terminal.IsNew)
            {
                toReturn = terminal.OperationMode.ToEnum<TerminalOperationMode>();
            }

            return toReturn;
        }

        /// <summary>
        ///     Updates the terminal operation state with the specified operation state
        /// </summary>
        /// <param name="terminalId">The id of the terminal</param>
        /// <param name="state">The new terminal state</param>
        public static void UpdateTerminalOperationState(int terminalId, TerminalOperationState state)
        {
            TerminalEntity terminalEntity = null;

            // Create and initialize a filter
            PredicateExpression filter = new PredicateExpression {TerminalFields.TerminalId == terminalId};

            // Get a TerminalCollection instance for the speciefied filter
            TerminalCollection terminalCollection = TerminalHelper.GetTerminalCollection(filter);
            if (terminalCollection.Count == 0)
            {
                throw new ObymobiException(UpdateLastTerminalRequestResult.TerminalUnknown, "TerminalId: {0}", terminalId);
            }
            if (terminalCollection.Count > 1)
            {
                throw new ObymobiException(UpdateLastTerminalRequestResult.MultipleTerminalsFound, "TerminalId: {0}", terminalId);
            }

            terminalEntity = terminalCollection[0];

            if (terminalEntity != null)
            {
                terminalEntity.OperationState = (int)state;
                terminalEntity.Save();
            }
        }

        //public static void UpdateLastRequest(int terminalId, TerminalStatusCode statusCode, string statusMessage, string applicationVersion, string osVersion)
        //{
        //    TerminalEntity terminalEntity = null;

        //    // Create and initialize a filter
        //    PredicateExpression filter = new PredicateExpression();
        //    filter.Add(TerminalFields.TerminalId == terminalId);

        //    // Get a TerminalCollection instance for the specified filter
        //    TerminalCollection terminalCollection = TerminalHelper.GetTerminalCollection(filter);
        //    if (terminalCollection.Count == 0)
        //    {
        //        throw new ObymobiException(UpdateLastTerminalRequestResult.TerminalUnknown, "TerminalId: {0}", terminalId);
        //    }
        //    if (terminalCollection.Count > 1)
        //    {
        //        throw new ObymobiException(UpdateLastTerminalRequestResult.MultipleTerminalsFound, "TerminalId: {0}", terminalId);
        //    }

        //    terminalEntity = terminalCollection[0];

        //    TerminalHelper.UpdateLastRequest(terminalEntity, statusCode, statusMessage, applicationVersion, osVersion);
        //}

        ///// <summary>
        /////     Updates the datetime value of the last request for the specified terminal
        ///// </summary>
        ///// <param name="terminalId">The id of the terminal</param>
        ///// <param name="statusCode">The status code of the terminal</param>
        ///// <param name="statusMessage">The status message of the terminal</param>
        ///// <param name="version">The version.</param>
        //public static void UpdateLastRequest(TerminalEntity terminalEntity, TerminalStatusCode statusCode, string statusMessage, string applicationVersion, string osVersion)
        //{
        //    if (terminalEntity == null)
        //        return;
            
        //    if (!terminalEntity.DeviceId.HasValue)
        //        return;

        //    if (terminalEntity.DeviceEntity.LastRequestUTC.HasValue && terminalEntity.DeviceEntity.LastRequestUTC.Value > DateTime.UtcNow)
        //        return;

        //    terminalEntity.DeviceEntity.LastRequestUTC = DateTime.UtcNow;
        //    terminalEntity.LastStatus = (int)statusCode;
        //    terminalEntity.LastStatusText = statusCode.ToString();

        //    // Status message
        //    if (!statusMessage.IsNullOrWhiteSpace())
        //    {
        //        terminalEntity.LastStatusMessage = statusMessage;
        //    }

        //    // Version
        //    if (!applicationVersion.IsNullOrWhiteSpace())
        //    {
        //        terminalEntity.DeviceEntity.ApplicationVersion = applicationVersion;
        //    }

        //    if (!osVersion.IsNullOrWhiteSpace())
        //    {
        //        terminalEntity.DeviceEntity.OsVersion = osVersion;
        //    }

        //    // Terminal state
        //    if (!terminalEntity.LastStateOnline.HasValue || !terminalEntity.LastStateOnline.Value) // LastStateOnline is null or false
        //    {
        //        // Update the terminal state to online
        //        TerminalStateHelper.UpdateTerminalState(terminalEntity, true);
        //    }

        //    terminalEntity.Save();
        //    terminalEntity.DeviceEntity.Save();
        //}

        ///// <summary>
        ///// Updates the last request of the specified terminal without fetching the terminal first.
        ///// </summary>
        ///// <param name="terminalId">The id of the terminal to update the last request for.</param>
        ///// <returns>True if the update was successful, False if not.</returns>
        //public static bool UpdateLastRequest(int terminalId)
        //{
        //    TerminalEntity terminalEntity = new TerminalEntity();
        //    terminalEntity.LastRequestUTC = DateTime.UtcNow;

        //    TerminalCollection terminalCollection = new TerminalCollection();
        //    return terminalCollection.UpdateMulti(terminalEntity, TerminalFields.TerminalId == terminalId) > 0;
        //}

        /// <summary>
        ///     Gets the terminal to forward to for the supplied terminal.
        /// </summary>
        /// <param name="terminal">The terminal.</param>
        /// <returns>TerminalEntity or null when no terminal is available</returns>
        public static TerminalEntity GetTerminalToForwardTo(TerminalEntity terminal)
        {
            List<TerminalEntity> forwardingTerminals = new List<TerminalEntity>();
            TerminalHelper.GetTerminalsForwardingFromThisTerminal(terminal, terminal.GetCurrentTransaction(), forwardingTerminals);

            if (forwardingTerminals.Count > 0)
            {
                return forwardingTerminals[forwardingTerminals.Count - 1];
            }
            return null;
        }

        /// <summary>
        ///     Gets all terminals to which this terminal is forwaring.
        ///     So if the Terminal forwards to Terminal A, and Terminal A forwards to Terminal X and Terminal X to Terminal Q you
        ///     get the id's of the terminals A, X and Q.
        /// </summary>
        /// <param name="terminal">The terminal.</param>
        /// <param name="transaction">The transaction.</param>
        /// <param name="terminalIds">The terminal ids.</param>
        public static void GetTerminalsForwardingFromThisTerminal(TerminalEntity terminal, ITransaction transaction, List<TerminalEntity> terminals)
        {
            terminal.AddToTransaction(terminal);

            if (terminal.ForwardToTerminalId.HasValue)
            {
                if (!terminals.Any(t => t.TerminalId == terminal.ForwardToTerminalId.Value))
                {
                    terminals.Add(terminal.ForwardToTerminalEntity);
                    TerminalHelper.GetTerminalsForwardingFromThisTerminal(terminal.ForwardToTerminalEntity, transaction, terminals);
                }
            }
        }

        ///// <summary>
        /////     Gets all terminals that forward to this terminal, so the one directly forwarding, but also the ones forwarding via
        /////     another one.
        ///// </summary>
        ///// <param name="terminalId"></param>
        ///// <param name="transaction"></param>
        ///// <param name="recursionLevel"></param>
        ///// <param name="maxRecursion"></param>
        ///// <param name="terminalIds"></param>
        //public static void GetTerminalsForwardingToTerminal(int terminalId, ITransaction transaction, ref int recursionLevel, int maxRecursion, List<int> terminalIds)
        //{
        //    // Prevent a full loop
        //    if (recursionLevel > maxRecursion)
        //    {
        //        return;
        //    }
        //    recursionLevel++;

        //    PredicateExpression filterForwardingToTerminal = new PredicateExpression();
        //    filterForwardingToTerminal.Add(TerminalFields.ForwardToTerminalId == terminalId);

        //    TerminalCollection terminals = new TerminalCollection();
        //    terminals.AddToTransaction(transaction);
        //    terminals.GetMulti(filterForwardingToTerminal);

        //    foreach (TerminalEntity terminal in terminals)
        //    {
        //        if (terminalIds.Contains(terminal.TerminalId))
        //        {
        //            // Not good, the fact the terminal is already in means a loop. - Don't continue
        //        }
        //        else
        //        {
        //            terminalIds.Add(terminal.TerminalId);
        //            TerminalHelper.GetTerminalsForwardingToTerminal(terminal.TerminalId, transaction, ref recursionLevel, maxRecursion, terminalIds);
        //        }
        //    }
        //}

        /// <summary>
        ///     Get a Obymobi.Data.CollectionClasses.TerminalCollection instance using the specified filter
        /// </summary>
        /// <param name="filter">The filter to use when retrieving the collection</param>
        /// <returns>A TerminalCollection instance</returns>
        public static TerminalCollection GetTerminalCollection(PredicateExpression filter)
        {
            TerminalCollection terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter);

            return terminalCollection;
        }

        public static TerminalCollection GetTerminalCollection(int companyId)
        {
            TerminalCollection collection = new TerminalCollection();
            PredicateExpression terminalFilter = new PredicateExpression(TerminalFields.CompanyId == companyId);
            SortExpression terminalSort = new SortExpression(TerminalFields.Name | SortOperator.Ascending);

            collection.GetMulti(terminalFilter, 0, terminalSort);

            return collection;
        }

        public static List<DeviceActivityLog> GetTerminalActivityLogs(this TerminalEntity terminal, int? orderId, bool includeNetmessages, bool includeTerminalLogs, bool includeTerminalStates, int? hours)
        {
            List<DeviceActivityLog> toReturn = new List<DeviceActivityLog>();
            DateTime from = DateTime.MinValue;

            if (hours.HasValue)
            {
                from = DateTime.UtcNow.AddHours(-hours.Value);
            }

            if (includeNetmessages)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(NetmessageFields.MessageType != NetmessageType.Ping);
                filter.Add(NetmessageFields.MessageType != NetmessageType.Pong);

                if (from != DateTime.MinValue)
                {
                    filter.Add(NetmessageFields.CreatedUTC >= from);
                }

                SortExpression sort = new SortExpression(NetmessageFields.NetmessageId | SortOperator.Descending);

                if (terminal != null)
                {
                    filter.Add(NetmessageFields.ReceiverTerminalId == terminal.TerminalId);
                }

                NetmessageCollection netmessages = new NetmessageCollection();
                netmessages.GetMulti(filter, 0, sort);

                // Convert Netmessages to ClientActivityLogs
                foreach (NetmessageEntity netmessage in netmessages)
                {
                    toReturn.Add(new DeviceActivityLog(netmessage));
                }
            }

            if (includeTerminalLogs)
            {
                PredicateExpression filter = new PredicateExpression();

                if (terminal != null)
                {
                    filter.Add(TerminalLogFields.TerminalId == terminal.TerminalId);
                }
                else if (orderId != null)
                {
                    filter.Add(TerminalLogFields.OrderId == orderId.Value);
                }

                if (from != DateTime.MinValue)
                {
                    filter.Add(TerminalLogFields.CreatedUTC >= from);
                }

                SortExpression sort = new SortExpression(TerminalLogFields.TerminalLogId | SortOperator.Descending);

                TerminalLogCollection terminallogs = new TerminalLogCollection();
                terminallogs.GetMulti(filter, 0, sort);

                foreach (TerminalLogEntity terminallog in terminallogs)
                {
                    toReturn.Add(new DeviceActivityLog(terminallog));
                }
            }

            if (includeTerminalStates && terminal == null)
            {
                throw new ObymobiException(TerminalHelperResult.MissingParameters, "Terminal required if includeTerminalStates == true");
            }
            if (includeTerminalStates)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(TerminalStateFields.TerminalId == terminal.TerminalId);

                if (from != DateTime.MinValue)
                {
                    filter.Add(TerminalStateFields.CreatedUTC >= from);
                }

                SortExpression sort = new SortExpression(TerminalStateFields.TerminalStateId | SortOperator.Descending);

                TerminalStateCollection terminalStates = new TerminalStateCollection();
                terminalStates.GetMulti(filter, 0, sort);

                foreach (TerminalStateEntity terminalState in terminalStates)
                {
                    toReturn.Add(new DeviceActivityLog(terminalState));
                }
            }

            toReturn = toReturn.OrderByDescending(x => x.DateTime ?? DateTime.MinValue).ToList();

            return toReturn;
        }

        public static List<DeviceActivityLog> GetTerminalActivityLogs(this TerminalEntity terminal, int? orderId, bool includeNetmessages, bool includeTerminalLogs, bool includeTerminalStates, int maxResults)
        {
            List<DeviceActivityLog> toReturn = new List<DeviceActivityLog>();
            DateTime lastnetMessageCreated = DateTime.MinValue;

            if (includeNetmessages)
            {
                NetmessageCollection netmessages = new NetmessageCollection();
                PredicateExpression filter = new PredicateExpression();
                SortExpression sort = new SortExpression(NetmessageFields.NetmessageId | SortOperator.Descending);

                if (terminal != null)
                {
                    filter.Add(NetmessageFields.ReceiverTerminalId == terminal.TerminalId);
                }

                filter.Add(NetmessageFields.MessageType != NetmessageType.Ping);
                filter.Add(NetmessageFields.MessageType != NetmessageType.Pong);

                netmessages.GetMulti(filter, maxResults, sort);

                // Convert Netmessages to ClientActivityLogs
                foreach (NetmessageEntity netmessage in netmessages)
                {
                    toReturn.Add(new DeviceActivityLog(netmessage));
                }

                if (netmessages.Count > 0)
                {
                    lastnetMessageCreated = netmessages.Min(x => x.CreatedUTC ?? DateTime.MinValue);
                }
            }

            if (includeTerminalLogs)
            {
                TerminalLogCollection terminallogs = new TerminalLogCollection();
                PredicateExpression filter = new PredicateExpression();
                SortExpression sort = new SortExpression(TerminalLogFields.TerminalLogId | SortOperator.Descending);

                if (terminal != null)
                {
                    filter.Add(TerminalLogFields.TerminalId == terminal.TerminalId);
                }
                else if (orderId != null)
                {
                    filter.Add(TerminalLogFields.OrderId == orderId.Value);
                }

                if (lastnetMessageCreated != DateTime.MinValue)
                {
                    PredicateExpression filterToMatchNetmessages = new PredicateExpression();
                    filterToMatchNetmessages.Add(filter);
                    filterToMatchNetmessages.Add(TerminalLogFields.CreatedUTC >= lastnetMessageCreated);

                    terminallogs.GetMulti(filterToMatchNetmessages, maxResults, sort);
                }
                else
                {
                    terminallogs.GetMulti(filter, maxResults, sort);
                }

                foreach (TerminalLogEntity terminallog in terminallogs)
                {
                    toReturn.Add(new DeviceActivityLog(terminallog));
                }

                // Check if we have enough results
                if (toReturn.Count < maxResults)
                {
                    // Add more
                    int moreRequired = maxResults - toReturn.Count;
                    if (terminallogs.Count > 0)
                    {
                        filter.Add(TerminalLogFields.TerminalLogId < terminallogs.Min(x => x.TerminalLogId));
                    }

                    terminallogs.GetMulti(filter, moreRequired, sort);

                    foreach (TerminalLogEntity terminallog in terminallogs)
                    {
                        toReturn.Add(new DeviceActivityLog(terminallog));
                    }
                }
            }

            if (includeTerminalStates && terminal == null)
            {
                throw new ObymobiException(TerminalHelperResult.MissingParameters, "Terminal required if includeTerminalStates == true");
            }
            if (includeTerminalStates)
            {
                TerminalStateCollection terminalStates = new TerminalStateCollection();
                PredicateExpression filter = new PredicateExpression();
                SortExpression sort = new SortExpression(TerminalStateFields.TerminalStateId | SortOperator.Descending);
                filter.Add(TerminalStateFields.TerminalId == terminal.TerminalId);

                if (lastnetMessageCreated != DateTime.MinValue)
                {
                    PredicateExpression filterToMatchNetmessages = new PredicateExpression();
                    filterToMatchNetmessages.Add(filter);
                    filterToMatchNetmessages.Add(TerminalStateFields.CreatedUTC >= lastnetMessageCreated);
                    terminalStates.GetMulti(filterToMatchNetmessages, maxResults, sort);
                }
                else
                {
                    terminalStates.GetMulti(filter, maxResults, sort);
                }

                foreach (TerminalStateEntity terminalState in terminalStates)
                {
                    toReturn.Add(new DeviceActivityLog(terminalState));
                }

                // Check if we have enough results
                if (toReturn.Count < maxResults)
                {
                    // Add more
                    int moreRequired = maxResults - toReturn.Count;

                    if (terminalStates.Count > 0)
                    {
                        filter.Add(TerminalStateFields.TerminalStateId < terminalStates.Min(x => x.TerminalStateId));
                    }

                    terminalStates.GetMulti(filter, moreRequired, sort);

                    foreach (TerminalStateEntity terminalState in terminalStates)
                    {
                        toReturn.Add(new DeviceActivityLog(terminalState));
                    }
                }
            }

            toReturn = toReturn.OrderByDescending(x => x.DateTime ?? DateTime.MinValue).ToList();

            return toReturn;
        }

        public static string GetLastSyncFromTerminal(TerminalEntity terminal)
        {
            if (!terminal.LastSyncUTC.HasValue)
            {
                return string.Empty;
            }
            TimeZoneInfo companyTimeZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZone.Mappings[terminal.CompanyEntity.TimeZoneOlsonId].WindowsTimeZoneId);
            DateTime lastSyncLocalTime = terminal.LastSyncUTC.GetValueOrDefault().UtcToLocalTime(companyTimeZone);
            return lastSyncLocalTime.ToString("dd-MM-yyyy HH:mm:ss");
        }
    }
}