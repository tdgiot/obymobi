﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Constants;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Logic.Routing;
using Obymobi.Analytics.Google.Recording;
using Obymobi.Analytics.Google;
using Dionysos;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// OrderProcessingHelper class
    /// </summary>
    public static class OrderProcessingHelper
    {
        /// <summary>
        /// OrderProcessingHelperResult enums
        /// </summary>
        public enum OrderProcessingHelperResult
        {
            /// <summary>
            /// PaymentmethodType is missing
            /// </summary>
            PaymentmethodTypeMissing = 200,
            /// <summary>
            /// Paymentmethod is not implemented
            /// </summary>
            PaymentMethodNotImplemented = 201,
        }

        /// <summary>
        /// Creates the new order.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="client">The client.</param>
        /// <param name="order">The order.</param>
        /// <param name="obyresult">The obyresult.</param>
        /// <param name="isOnsiteServer">if set to <c>true</c> [is onsite server].</param>
        /// <returns></returns>
        public static OrderEntity CreateNewOrder(CustomerEntity customer, ClientEntity client, Order order, ref ObyTypedResult<Order> obyresult, bool isOnsiteServer = false)
        {
            // First fully check the Order
            // Any check will throw errors if it can't continue, so not required to
            // check a return value (which are none either: void)

            CompanyEntity company;

            // Validate Company Checks and give CompanyEntity as an out parameter for later use.
            OrderProcessingHelper.ValidateCompanyChecks(customer, client, order, out company, isOnsiteServer);

            // Validate all Products & Product related data (Products, Alterations, Age)
            OrderProcessingHelper.ValidateOrderitems(client, order, false, true);

            // Set / validate Deliverypoint
            OrderProcessingHelper.ValidateDeliverypoint(ref order, isOnsiteServer);

            // Mark this order as checked (otherwise it won't covert to a Model)
            order.ValidatedByOrderProcessingHelper = DateTime.UtcNow;

            // Convert Model to Entity
            var orderEntity = OrderHelper.CreateOrderEntityFromModel(order, isOnsiteServer);

            // If not yet set, set the CurrencyId
            if (!orderEntity.CurrencyId.HasValue)
                orderEntity.CurrencyId = company.CurrencyId;

            // We require OrderItems if we're not requesting for service or checkout
            if (orderEntity.OrderitemCollection.Count == 0 && orderEntity.TypeAsEnum != OrderType.RequestForCheckout && orderEntity.TypeAsEnum != OrderType.RequestForService && orderEntity.TypeAsEnum != OrderType.RequestForBatteryCharge && orderEntity.TypeAsEnum != OrderType.RequestForPrint && orderEntity.TypeAsEnum != OrderType.RequestForWakeUp)
                throw new ObymobiException(OrderSaveResult.NoOrderitemsForOrder);

            // Save the Order transaction
            var transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, $"{nameof(CreateNewOrder)}-{DateTime.UtcNow.Ticks}");

            if (customer != null)
            {
                // Update customer last name
                if (!customer.Lastname.Equals(order.CustomerNameFull))
                {
                    customer.AddToTransaction(transaction);
                    customer.Lastname = order.CustomerNameFull;
                    customer.Save();
                }
            }

            // Save order
            try
            {
                // Add to transaction
                orderEntity.AddToTransaction(transaction);

                // Do recursive save
                if (!orderEntity.Save(true))
                {
                    throw new ObymobiException(OrderSaveResult.EntitySaveRecursiveFalse, XmlHelper.Serialize(order));
                }
                else
                {
                    bool directlyStartRoute = orderEntity.StatusAsEnum != OrderStatus.WaitingForPaymentCompleted;

                    // Add the OrderRoute                    
                    RoutingHelper.WriteRouteForOrder(orderEntity, directlyStartRoute);
                }

                string eventLabel = "{0}/{1}".FormatSafe(order.CompanyName, orderEntity.DeliverypointEntity.DeliverypointgroupEntity.Name);
                AnalyticsProcessingTaskHelper.CreateTask(orderEntity, "Order Saved", eventLabel);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new ObymobiException(OrderSaveResult.EntitySaveRecursiveException, ex, "Error: {0}\r\nStacktrace: {1}", ex.Message, ex.StackTrace);
            }
            finally
            {
                transaction.Dispose();
            }

            // Create a Model to Return & remove it's OrderItems because it's unnecessary load that won't be used by the consuming app.
            Order orderFromEntity = OrderHelper.CreateOrderModelFromEntity(orderEntity, false, true);
            //orderFromEntity.Orderitems = null;
            obyresult.ModelCollection.Add(orderFromEntity);

            return orderEntity;
        }

        /// <summary>
        /// Verify if an Order can be accepted in this time frame
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <param name="client">The client.</param>
        /// <param name="order">The order.</param>
        /// <param name="company">The company.</param>
        /// <param name="isOnsiteServer">if set to <c>true</c> [is onsite server].</param>
        private static void ValidateCompanyChecks(CustomerEntity customer, ClientEntity client, Order order, out CompanyEntity company, bool isOnsiteServer)
        {
            // Check CompanyHours
            DateTime orderTime = DateTime.UtcNow.UtcToLocalTime(CompanyHelper.GetCompanyTimeZone(order.CompanyId));

            if (order.PlaceTime.HasValue)
            {
                orderTime = order.PlaceTime.Value;
            }

            bool companyInBusinessHours = BusinesshoursHelper.IsTimeInCompanyBusinesshours(order.CompanyId, out company, orderTime);

            // Check if we need a Business Hours check, if so, check.
            // Do OnsiteServer check here because of CompanyEntity reference, filled by this method
            if (!isOnsiteServer && customer != null && !customer.ClientId.HasValue && client == null && !companyInBusinessHours)
                throw new ObymobiException(OrderSaveResult.CompanyIsClosed);

            if(order.MobileOrder && company.MobileOrderingDisabled)
                throw new ObymobiException(OrderSaveResult.MobileOrderingDisabled);
        }

        /// <summary>
        /// Verify all product related checks
        /// </summary>
        /// <param name="client">The client that is placing this order.</param>
        /// <param name="order">The order.</param>
        /// <param name="preCheck">if set to <c>true</c> [pre check].</param>
        /// <param name="throwExceptionIfInvalid">if set to <c>true</c> [throw exception if invalid].</param>
        /// <returns></returns>
        private static List<int> ValidateOrderitems(ClientEntity client, Order order, bool preCheck, bool throwExceptionIfInvalid)
        {
            List<int> invalidProducts = new List<int>();
            StringBuilder productProblems = new StringBuilder();
            PredicateExpression filter = new PredicateExpression();

            // Retrieve all products at once + prefetch required data.
            var productIds = from oi in order.Orderitems select oi.ProductId;
            filter.Add(ProductFields.ProductId == productIds.ToList());

            PrefetchPath path = new PrefetchPath(EntityType.ProductEntity);
            IPrefetchPathElement prefetchSchedule = path.Add(ProductEntityBase.PrefetchPathScheduleEntity);
            IPrefetchPathElement prefetchVattarrif =  path.Add(ProductEntityBase.PrefetchPathVattariffEntity);
            IPrefetchPathElement prefetchPriceLevelItems = path.Add(ProductEntityBase.PrefetchPathPriceLevelItemCollection);
            IPrefetchPathElement prefetchBrandProduct = path.Add(ProductEntityBase.PrefetchPathBrandProductEntity);

            ProductCollection products = new ProductCollection();
            products.GetMulti(filter, path);

            // Retrieve all relevant Alterationitems incl. prefetch at once for speed.
            AlterationitemCollection alterationItems = new AlterationitemCollection();
            if (order.Orderitems.Any(x => x.Alterationitems != null && x.Alterationitems.Any()))
            {
                PredicateExpression filterAlterationitems = new PredicateExpression();
                PredicateExpression filterOrderitemAlterationitems = new PredicateExpression();

                foreach (var orderItem in order.Orderitems)
                {
                    if (orderItem.Alterationitems != null)
                    {
                        foreach (var alterationItem in orderItem.Alterationitems)
                        {
                            PredicateExpression filterItemFilter = new PredicateExpression();
                            filterItemFilter.Add(AlterationitemFields.AlterationId == alterationItem.AlterationId);
                            filterItemFilter.Add(AlterationitemFields.AlterationoptionId == alterationItem.AlterationoptionId);
                            filterOrderitemAlterationitems.AddWithOr(filterItemFilter);
                        }
                    }
                }

                filterAlterationitems.Add(filterOrderitemAlterationitems);
                filterAlterationitems.Add(AlterationitemFields.Version == 1);

                RelationCollection relations = new RelationCollection();
                relations.Add(AlterationitemEntityBase.Relations.AlterationEntityUsingAlterationId);

                PrefetchPath pathAlterationItem = new PrefetchPath(EntityType.AlterationitemEntity);
                IPrefetchPathElement prefetchAlteration = pathAlterationItem.Add(AlterationitemEntityBase.PrefetchPathAlterationEntity);
                IPrefetchPathElement prefetchAlterationoption = pathAlterationItem.Add(AlterationitemEntityBase.PrefetchPathAlterationoptionEntity);
                IPrefetchPathElement prefetchAlterationoptionPriceLevelItems = prefetchAlterationoption.SubPath.Add(AlterationoptionEntityBase.PrefetchPathPriceLevelItemCollection);

                alterationItems.GetMulti(filterAlterationitems, 0, null, relations, pathAlterationItem);
            }

            TimeZoneInfo companyTimeZoneInfo = CompanyHelper.GetCompanyTimeZone(order.CompanyId);            
            DateTime companyLocalTime = DateTime.UtcNow.UtcToLocalTime(companyTimeZoneInfo);
            
            // Check for each orderItem if we've got the product, and if we have it
            // if it meets all criteria to be orderable.
            foreach (Orderitem orderitem in order.Orderitems)
            {
                bool canOrderProduct = true;
                ProductEntity product = products.SingleOrDefault(p => p.ProductId == orderitem.ProductId);

                if (product == null)
                {
                    // Non existing product
                    canOrderProduct = false;
                    productProblems.AppendFormat("The product '{0}' doesn't exist.\n", orderitem.ProductName);
                }
                else if (product.CompanyId != order.CompanyId)
                {
                    // Product of another company
                    productProblems.AppendFormat("The product '{0}' belongs to another company.\n", orderitem.ProductName);
                }
                else if (!product.Visible)
                {
                    // Non orderable product
                    canOrderProduct = false;
                    productProblems.AppendFormat("The product '{0}' is invisible.\n", orderitem.ProductName);
                }
                else if (product.ScheduleId.HasValue)
                {
                    // We have a schedule, requires us to check the schedule
                    bool timeIsOk = false;                    

                    TimeSpan nowTimeOfDay = companyLocalTime.TimeOfDay;

                    foreach (ScheduleitemEntity scheduleitem in product.ScheduleEntity.ScheduleitemCollection)
                    {
                        DateTime start = scheduleitem.TimeStartAsDateTime();
                        DateTime end = scheduleitem.TimeEndAsDateTime();

                        if (scheduleitem.DayOfWeek.HasValue)
                        {
                            // Check if day is OK
                            if (scheduleitem.DayOfWeek.Value == companyLocalTime.DayOfWeek)
                            {
                                // Check if time is OK
                                if (nowTimeOfDay >= start.TimeOfDay && nowTimeOfDay <= end.TimeOfDay)
                                {
                                    timeIsOk = true;
                                    break;
                                }
                            }
                        }
                        else if (nowTimeOfDay >= start.TimeOfDay && nowTimeOfDay <= end.TimeOfDay) // All days, only check time
                        {
                            timeIsOk = true;
                            break;
                        }
                    }

                    if (!timeIsOk)
                    {
                        canOrderProduct = false;
                        productProblems.AppendFormat("The product '{0}' can't be ordered due to its schedule.\n", orderitem.ProductName);
                    }
                }

                if (canOrderProduct)
                {
                    // Copy the values (we don't trust the input from the outside application)
                    orderitem.ProductName = product.Name;
                    orderitem.ProductPriceIn = OrderProcessingHelper.GetActivePriceForProduct(product, companyLocalTime, order.PriceScheduleId);
                    orderitem.VatPercentage = product.VattariffPercentage;

                    if (product.Color > 0)
                    {
                        orderitem.Color = product.Color;
                    }
                    else if(orderitem.CategoryId > 0)
                    {
                        orderitem.Color = CategoryHelper.GetColorFromParent(new CategoryEntity(orderitem.CategoryId));
                    }

                    if (!preCheck)
                    {
                        if (orderitem.Quantity <= 0)
                            throw new ObymobiException(OrderitemSaveResult.QuantityLessThanOrZero);
                    }

                    if (orderitem.ProductPriceIn < 0)
                        throw new ObymobiException(OrderitemSaveResult.PriceExLessThanZero);

                    if (orderitem.VatPercentage < 0)
                        throw new ObymobiException(OrderitemSaveResult.VatPercentageLessThanZero);

                    // Retrieve the AlterationitemIds  
                    // GK TODO: Add the price of the Alterations
                    decimal alterationItemsPriceIn = 0;
                    if (orderitem.Alterationitems != null)
                    {                        
                        foreach (Alterationitem alterationItem in orderitem.Alterationitems)
                        {
                            if (alterationItem.AlterationType != (int)AlterationType.Options)
                            {
                                AlterationEntity alterationEntity = new AlterationEntity(alterationItem.AlterationId);
                                if (!alterationEntity.IsNew)
                                {
                                    alterationItem.AlterationName = alterationEntity.Name;
                                }
                            }
                            else
                            {
                                AlterationitemEntity alterationItemEntity = alterationItems.SingleOrDefault(ai => ai.AlterationoptionId == alterationItem.AlterationoptionId &&
                                                                                                                  ai.AlterationId == alterationItem.AlterationId);

                                if (alterationItemEntity == null)
                                {
                                    productProblems.AppendFormat("{0} - Alterationitem not found for Alteration '{1}' and AlterationOption '{2}'\n",
                                                                 orderitem.ProductName, alterationItem.AlterationName, alterationItem.AlterationoptionName);
                                    canOrderProduct = false;
                                }
                                else
                                {
                                    alterationItem.AlterationitemId = alterationItemEntity.AlterationitemId;
                                    alterationItem.AlterationName = alterationItemEntity.AlterationEntity.Name;
                                    alterationItem.AlterationoptionName = alterationItemEntity.AlterationoptionEntity.Name;
                                    alterationItem.AlterationoptionPriceIn = OrderProcessingHelper.GetActivePriceForAlterationitem(alterationItemEntity, companyLocalTime, order.PriceScheduleId);
                                }
                            }

                            alterationItemsPriceIn += alterationItem.AlterationoptionPriceIn;
                        }
                    }
                }

                // Add product to non orderable list if not orderable
                if (!canOrderProduct)
                {
                    invalidProducts.Add(orderitem.ProductId);
                }
            }

            // throw exception if required
            if (invalidProducts.Count > 0 && throwExceptionIfInvalid)
            {
                throw new ObymobiException(OrderSaveResult.InvalidProducts, productProblems.ToString());
            }

            return invalidProducts;
        }

        private static decimal GetActivePriceForProduct(ProductEntity product, DateTime now, int priceScheduleId)
        {
            decimal price = product.PriceIn ?? 0;
            bool hasActivePriceLevel = false;

            if (priceScheduleId > 0 && product.PriceLevelItemCollection.Count > 0)
            {
                // Try to find out if a price level is active at the time
                PriceLevelItemEntity priceLevelItemEntity = PriceScheduleHelper.GetActivePriceLevelItem(now, new PredicateExpression(PriceLevelItemFields.ProductId == product.ProductId), priceScheduleId);
                if (priceLevelItemEntity != null)
                {
                    price = priceLevelItemEntity.Price;
                    hasActivePriceLevel = true;
                }
            }

            if (!hasActivePriceLevel && !product.PriceIn.HasValue && product.BrandProductId.HasValue)
            {
                price = product.BrandProductEntity.PriceIn ?? 0;
            }

            return price;
        }

        private static decimal GetActivePriceForAlterationitem(AlterationitemEntity alterationitem, DateTime now, int priceScheduleId)
        {
            decimal price = alterationitem.AlterationoptionEntity.PriceIn ?? 0;
            if (priceScheduleId > 0 && alterationitem.AlterationoptionEntity.PriceLevelItemCollection.Count > 0)
            {
                // Try to find out if a price level is active at the time
                PriceLevelItemEntity priceLevelItemEntity = PriceScheduleHelper.GetActivePriceLevelItem(now, new PredicateExpression(PriceLevelItemFields.AlterationoptionId == alterationitem.AlterationoptionEntity.AlterationoptionId), priceScheduleId);
                if (priceLevelItemEntity != null)
                {
                    price = priceLevelItemEntity.Price;
                }
                
            }
            return price;
        }

        /// <summary>
        /// Verify all Deliverypoint related checks
        /// </summary>
        /// <param name="order">The order, the DeliverypointId will be set based on CompanyId and DeliverypointNumber if not yet done.</param>
        /// <param name="isOnsiteServer">if set to <c>true</c> [is onsite server].</param>
        private static void ValidateDeliverypoint(ref Order order, bool isOnsiteServer = false)
        {
            // Check delivery point id            
            DeliverypointEntity dpoint;
            string descriptionForErrors = "";
            if (order.DeliverypointId <= 0)
            {
                dpoint = DeliverypointHelper.GetDeliverypoint(order.CompanyId, order.DeliverypointNumber, true);
                descriptionForErrors = string.Format("DeliverypointNumber: {0}, CompanyId: {1}", order.DeliverypointNumber, order.CompanyId);
            }
            else
            {
                dpoint = DeliverypointHelper.GetDeliverypoint(order.DeliverypointId, true);
                descriptionForErrors = string.Format("DeliverypointId: {0}", order.DeliverypointId);
            }

            if (dpoint != null && !dpoint.IsNew)
            {
                if (!dpoint.DeliverypointgroupEntity.Active)
                    throw new ObymobiException(OrderSaveResult.DeliverypointgroupDisabled, descriptionForErrors);

                order.DeliverypointId = dpoint.DeliverypointId;
            }
            else
            {
                // DK/GK - Temporary hack for mobile checkin
                // https://www.pivotaltracker.com/story/show/61875762                
                if (order.DeliverypointNumber == ObymobiConstants.MobileCheckinDeliverypointNumber)
                {
                    dpoint = DeliverypointHelper.GetDeliverypoint(order.CompanyId, order.DeliverypointNumber, false);
                    if (dpoint == null || dpoint.IsNew)
                    {
                        DeliverypointgroupEntity dpgEntity = null;
                        var deliverypointgroups = DeliverypointgroupHelper.GetDeliverypointgroupCollectionForCompany(order.CompanyId, null);
                        foreach (var deliverypointgroup in deliverypointgroups)
                        {
                            if (dpgEntity == null && deliverypointgroup.Active)
                                dpgEntity = deliverypointgroup;

                            if (deliverypointgroup.AvailableOnObymobi && deliverypointgroup.Active)
                            {
                                dpgEntity = deliverypointgroup;
                                break;
                            }
                        }

                        if (dpgEntity != null)
                        {
                            dpoint = new DeliverypointEntity();
                            dpoint.Name = "Check-In";
                            dpoint.Number = "" + ObymobiConstants.MobileCheckinDeliverypointNumber;
                            dpoint.DeliverypointgroupId = dpgEntity.DeliverypointgroupId;
                            dpoint.CompanyId = dpgEntity.CompanyId;
                            dpoint.Save();

                            order.DeliverypointId = dpoint.DeliverypointId;
                        }
                    }
                    else
                    {
                        order.DeliverypointId = dpoint.DeliverypointId;
                    }
                }

                if (dpoint == null)
                    throw new ObymobiException(OrderSaveResult.UnknownDeliverypoint, descriptionForErrors);
            }
        }
    }
}
