﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Dionysos.Data;
using Dionysos.Data.LLBLGen;
using Dionysos.Interfaces.Data;
using MarkdownSharp;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.Model;
using Obymobi.Data;
using MarkdownSharp.Extensions;

namespace Obymobi.Logic.HelperClasses
{
    public static class CustomTextHelper
    {
        #region Fields

        /// <summary>
        /// Key: EntityName, Value: Range of CustomTextTypes.        
        /// </summary>
        private static Dictionary<string, List<CustomTextType>> customTextTypesPerEntity = null;

        /// <summary>
        /// The default culture to use for generic entities.
        /// </summary>
        public static Obymobi.Culture DefaultCulture = Obymobi.Culture.English_United_Kingdom;

        #endregion

        #region Constructors

        static CustomTextHelper()
        {
            if (CustomTextHelper.customTextTypesPerEntity == null)
            {
                Array enumValues = Enum.GetValues(typeof(CustomTextType));

                CustomTextHelper.customTextTypesPerEntity = new Dictionary<string, List<CustomTextType>>();
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(CompanyEntity).Name, new CustomTextTypeRangeConverter(enumValues, 1, 1000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(DeliverypointgroupEntity).Name, new CustomTextTypeRangeConverter(enumValues, 1000, 2000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(CategoryEntity).Name, new CustomTextTypeRangeConverter(enumValues, 2000, 3000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(ProductEntity).Name, new CustomTextTypeRangeConverter(enumValues, 3000, 4000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(ActionButtonEntity).Name, new CustomTextTypeRangeConverter(enumValues, 4000, 5000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(AdvertisementEntity).Name, new CustomTextTypeRangeConverter(enumValues, 5000, 6000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(AlterationEntity).Name, new CustomTextTypeRangeConverter(enumValues, 6000, 7000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(AlterationoptionEntity).Name, new CustomTextTypeRangeConverter(enumValues, 7000, 8000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(AmenityEntity).Name, new CustomTextTypeRangeConverter(enumValues, 8000, 9000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(AnnouncementEntity).Name, new CustomTextTypeRangeConverter(enumValues, 9000, 10000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(AttachmentEntity).Name, new CustomTextTypeRangeConverter(enumValues, 10000, 11000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(EntertainmentcategoryEntity).Name, new CustomTextTypeRangeConverter(enumValues, 11000, 12000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(GenericcategoryEntity).Name, new CustomTextTypeRangeConverter(enumValues, 12000, 13000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(GenericproductEntity).Name, new CustomTextTypeRangeConverter(enumValues, 13000, 14000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(PageEntity).Name, new CustomTextTypeRangeConverter(enumValues, 14000, 15000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(PageTemplateEntity).Name, new CustomTextTypeRangeConverter(enumValues, 15000, 16000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(PointOfInterestEntity).Name, new CustomTextTypeRangeConverter(enumValues, 16000, 17000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(RoomControlAreaEntity).Name, new CustomTextTypeRangeConverter(enumValues, 17000, 18000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(RoomControlComponentEntity).Name, new CustomTextTypeRangeConverter(enumValues, 18000, 19000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(RoomControlSectionEntity).Name, new CustomTextTypeRangeConverter(enumValues, 19000, 20000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(RoomControlSectionItemEntity).Name, new CustomTextTypeRangeConverter(enumValues, 20000, 21000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(RoomControlWidgetEntity).Name, new CustomTextTypeRangeConverter(enumValues, 21000, 22000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(ScheduledMessageEntity).Name, new CustomTextTypeRangeConverter(enumValues, 22000, 23000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(SiteEntity).Name, new CustomTextTypeRangeConverter(enumValues, 23000, 24000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(SiteTemplateEntity).Name, new CustomTextTypeRangeConverter(enumValues, 24000, 25000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(StationEntity).Name, new CustomTextTypeRangeConverter(enumValues, 25000, 26000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(UIFooterItemEntity).Name, new CustomTextTypeRangeConverter(enumValues, 26000, 27000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(UITabEntity).Name, new CustomTextTypeRangeConverter(enumValues, 27000, 28000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(UIWidgetEntity).Name, new CustomTextTypeRangeConverter(enumValues, 28000, 29000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(VenueCategoryEntity).Name, new CustomTextTypeRangeConverter(enumValues, 29000, 30000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(AvailabilityEntity).Name, new CustomTextTypeRangeConverter(enumValues, 30000, 31000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(ClientConfigurationEntity).Name, new CustomTextTypeRangeConverter(enumValues, 31000, 32000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(ProductgroupEntity).Name, new CustomTextTypeRangeConverter(enumValues, 32000, 33000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(RoutestephandlerEntity).Name, new CustomTextTypeRangeConverter(enumValues, 33000, 34000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(InfraredCommandEntity).Name, new CustomTextTypeRangeConverter(enumValues, 34000, 35000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(CarouselItemEntity).Name, new CustomTextTypeRangeConverter(enumValues, 35000, 36000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(NavigationMenuItemEntity).Name, new CustomTextTypeRangeConverter(enumValues, 36000, 37000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(WidgetActionBannerEntity).Name, new CustomTextTypeRangeConverter(enumValues, 37000, 38000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(WidgetActionButtonEntity).Name, new CustomTextTypeRangeConverter(enumValues, 38000, 39000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(WidgetLanguageSwitcherEntity).Name, new CustomTextTypeRangeConverter(enumValues, 39000, 40000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(WidgetPageTitleEntity).Name, new CustomTextTypeRangeConverter(enumValues, 40000, 41000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(OutletEntity).Name, new CustomTextTypeRangeConverter(enumValues, 41000, 42000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(ServiceMethodEntity).Name, new CustomTextTypeRangeConverter(enumValues, 42000, 43000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(CheckoutMethodEntity).Name, new CustomTextTypeRangeConverter(enumValues, 43000, 44000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(WidgetWaitTimeEntity).Name, new CustomTextTypeRangeConverter(enumValues, 44000, 45000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(WidgetOpeningTimeEntity).Name, new CustomTextTypeRangeConverter(enumValues, 45000, 46000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(WidgetMarkdownEntity).Name, new CustomTextTypeRangeConverter(enumValues, 46000, 47000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(ApplicationConfigurationEntity).Name, new CustomTextTypeRangeConverter(enumValues, 47000, 48000).CustomTextTypes);
                CustomTextHelper.customTextTypesPerEntity.Add(typeof(LandingPageEntity).Name, new CustomTextTypeRangeConverter(enumValues, 48000, 49000).CustomTextTypes);
            }
        }

        #endregion

        #region Enums

        public enum CustomTextHelperResult
        {
            NoCultureCodeSetForCompany = 200
        }

        #endregion

        #region Copying

        public static CustomTextCollection Copy(this CustomTextCollection collection, EntityField fieldToExclude)
        {
            ArrayList excludedFields = new ArrayList() { fieldToExclude };
            CustomTextCollection newCustomTextCollection = new CustomTextCollection();
            foreach (CustomTextEntity oldCustomTextEntity in collection)
            {
                CustomTextEntity newCustomTextEntity = new CustomTextEntity();
                LLBLGenEntityUtil.CopyFields(oldCustomTextEntity, newCustomTextEntity, excludedFields);
                newCustomTextCollection.Add(newCustomTextEntity);
            }
            return newCustomTextCollection;
        }

        #endregion

        #region Conversion

        public static CustomText[] ConvertCustomTextCollectionToArray(CustomTextCollection collection, string parent = "")
        {
            List<CustomText> list = new List<CustomText>();
            foreach (CustomTextEntity entity in collection)
            {
                list.Add(CustomTextHelper.ConvertEntityToModel(entity, parent));
            }
            return list.ToArray();
        }

        public static CustomText ConvertEntityToModel(CustomTextEntity entity, string parent = "")
        {
            CustomText model = new CustomText();
            model.CustomTextId = entity.CustomTextId;
            model.CultureCode = entity.CultureCode;
            model.Type = (int)entity.Type;
            model.ForeignKey = entity.RelatedEntityId;
            model.Parent = parent;

            Culture culture;
            if (Obymobi.Culture.Mappings.TryGetValue(entity.CultureCode, out culture))
            {
                model.LanguageCode = culture.Language.CodeAlpha2.ToUpperInvariant();
            }

            if (!CustomTextHelper.ShouldApplyMarkDown(entity))
            {
                model.Text = entity.Text;
            }
            else
            {
                model.Text = CustomTextHelper.ApplyMarkDown(entity);
            }

            return model;
        }

        private static bool ShouldApplyMarkDown(CustomTextEntity entity)
        {
            return entity.AlterationId.HasValue && entity.Type == CustomTextType.AlterationDescription && entity.AlterationEntity.Type == AlterationType.Instruction;
        }

        private static string ApplyMarkDown(CustomTextEntity entity)
        {
            Markdown markdown = MarkdownHelper.GetDefaultInstance();
            string toReturn = markdown.Transform(entity.Text);

            if (toReturn.EndsWith("\n"))
                toReturn = toReturn.Substring(0, toReturn.Length - 1);

            return toReturn;
        }

        #endregion

        #region Collection Filtering

        public static Dictionary<CustomTextType, string> ToCultureCodeSpecificDictionary(this CustomTextCollection collection, int languageId)
        {
            Dictionary<CustomTextType, string> dictionary = new Dictionary<CustomTextType, string>();

            if (languageId > 0)
            {
                foreach (CustomTextEntity customTextEntity in collection.Where(x => x.LanguageId.HasValue && x.LanguageId == languageId))
                {
                    if (!dictionary.ContainsKey(customTextEntity.Type))
                    {
                        dictionary.Add(customTextEntity.Type, customTextEntity.Text);
                    }
                }
            }

            return dictionary;
        }

        public static Dictionary<CustomTextType, string> ToCultureCodeSpecificDictionary(this CustomTextCollection collection, string cultureCode)
        {
            Dictionary<CustomTextType, string> dictionary = new Dictionary<CustomTextType, string>();

            if (cultureCode != null)
            {
                foreach (CustomTextEntity customTextEntity in collection.Where(x => !x.CultureCode.IsNullOrWhiteSpace() && x.CultureCode == cultureCode))
                {
                    if (!dictionary.ContainsKey(customTextEntity.Type))
                    {
                        dictionary.Add(customTextEntity.Type, customTextEntity.Text);
                    }
                }
            }

            return dictionary;
        }

        public static Dictionary<CustomTextType, string> ToCultureCodeSpecificDictionary(CustomText[] array, string cultureCode)
        {
            Dictionary<CustomTextType, string> dictionary = new Dictionary<CustomTextType, string>();

            if (cultureCode != null)
            {
                foreach (CustomText customText in array.Where(x => !x.CultureCode.IsNullOrWhiteSpace() && x.CultureCode == cultureCode))
                {
                    if (!dictionary.ContainsKey(customText.Type.ToEnum<CustomTextType>()))
                    {
                        dictionary.Add(customText.Type.ToEnum<CustomTextType>(), customText.Text);
                    }
                }
            }

            return dictionary;
        }

        public static Dictionary<CustomTextType, string> ToCultureCodeSpecificDictionary(Obymobi.Logic.Model.Mobile.CustomText[] array, string cultureCode)
        {
            Dictionary<CustomTextType, string> dictionary = new Dictionary<CustomTextType, string>();

            if (cultureCode != null)
            {
                foreach (Model.Mobile.CustomText customText in array.Where(x => !x.CultureCode.IsNullOrWhiteSpace() && x.CultureCode == cultureCode))
                {
                    if (!dictionary.ContainsKey(customText.Type.ToEnum<CustomTextType>()))
                    {
                        dictionary.Add(customText.Type.ToEnum<CustomTextType>(), customText.Text);
                    }
                }
            }

            return dictionary;
        }

        public static List<CustomTextEntity> StartsWith(this CustomTextCollection collection, string prefix)
        {
            return collection.Where(x => x.GetType().Name.StartsWith(prefix)).ToList();
        }

        public static List<CustomTextType> GetCustomTextTypesForEntity(string entity, CustomTextType[] excludeCustomTextTypes = null)
        {
            List<CustomTextType> customTextTypes;
            if (!customTextTypesPerEntity.TryGetValue(entity, out customTextTypes))
            {
                return new List<CustomTextType>();
            }

            if (excludeCustomTextTypes != null)
            {
                customTextTypes = customTextTypes.Where(textType => !excludeCustomTextTypes.Contains(textType)).ToList();
            }

            var sortedTextTypes = customTextTypes.OrderBy(c =>
            {
                Attributes.DisplayOrderAttribute customSortOrderValue = EnumUtil.GetAttribute<Attributes.DisplayOrderAttribute>(c);
                return customSortOrderValue != null ? customSortOrderValue.DisplayOrder : (int)c;
            });

            return sortedTextTypes.ToList();
        }

        public static List<CustomTextType> GetCustomTextTypesForEntity(IEntity entity)
        {
            return GetCustomTextTypesForEntity(entity.LLBLGenProEntityName);
        }

        #endregion

        #region Creation & Updating

        public static CustomTextEntity CreateEntityForDeliverypointgroup(CustomTextType type, string cultureCode, string text, int deliverypointgroupId)
        {
            return CustomTextHelper.CreateEntity(type, cultureCode, text, DeliverypointgroupFields.DeliverypointgroupId, deliverypointgroupId);
        }

        public static CustomTextEntity CreateEntity(CustomTextType type, string cultureCode, string text, IEntityField primaryKey, int primaryValue)
        {
            return CustomTextHelper.CreateEntity(type, cultureCode, text, primaryKey.Name, primaryValue);
        }

        public static CustomTextEntity CreateEntity(CustomTextType type, string cultureCode, string text, string primaryKeyText, int primaryValue)
        {
            if (text.IsNullOrWhiteSpace())
                return null;

            CustomTextEntity entity = new CustomTextEntity();
            entity.CultureCode = cultureCode;
            entity.Text = text;
            entity.Type = type;
            entity.SetNewFieldValue(primaryKeyText, primaryValue);

            return entity;
        }

        public static void UpdateCustomTexts(IEntity entity)
        {
            CustomTextHelper.UpdateCustomTexts(entity, CustomTextHelper.DefaultCulture.Code);
        }

        public static void UpdateCustomTexts(IEntity entity, CompanyEntity company)
        {
            if (company.CultureCode.IsNullOrWhiteSpace())
                throw new ObymobiException(CustomTextHelperResult.NoCultureCodeSetForCompany, "CompanyId: {0}, CompanyName: {1}", company.CompanyId, company.Name);

            CustomTextHelper.UpdateCustomTexts(entity, company.CultureCode);
        }

        /// <summary>
        /// Updates the related CustomTextEntities for all mapped fields on the entity.
        /// </summary>
        /// <param name="entity">The entity which has a CustomTextCollection.</param>
        public static void UpdateCustomTexts(IEntity entity, string cultureCode, ITransaction transaction = null)
        {
            if (entity == null || cultureCode.IsNullOrWhiteSpace())
                return;

            // Use the transaction on the entity if the transaction is not explicitly passed on as a parameter
            transaction = transaction ?? entity.Transaction;

            IEntityField primaryKeyField = entity.PrimaryKeyFields[0]; // e.g. productId
            int primaryKeyValue; // e.g. 310

            if (primaryKeyField != null && primaryKeyField.CurrentValue != null && int.TryParse(primaryKeyField.CurrentValue.ToString(), out primaryKeyValue))
            {
                PredicateExpression filter = new PredicateExpression(CustomTextFields.CultureCode == cultureCode);
                filter.Add(LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression("CustomTextEntity", primaryKeyField.Name, primaryKeyValue));

                EntityView<CustomTextEntity> entityView = CustomTextHelper.GetCustomTexts(filter).DefaultView;

                Dictionary<int, CustomTextType> mappedCustomTextTypes = CustomTextHelper.GetMappedDictionaryForEntity(entity);
                if (mappedCustomTextTypes != null)
                {
                    CustomTextCollection customTextCollectionToDelete = new CustomTextCollection();

                    for (int i = 0; i < entity.Fields.Count; i++)
                    {
                        if (!mappedCustomTextTypes.ContainsKey(entity.Fields[i].FieldIndex))
                        {
                            // The field of the entity isn't mapped, can't create / update the custom text entity
                        }
                        else if (entity.Fields[i].CurrentValue == null)
                        {
                            // The field value is null, can't create / update the custom text entity
                        }
                        else
                        {
                            CustomTextType type = mappedCustomTextTypes[entity.Fields[i].FieldIndex];
                            string text = entity.Fields[i].CurrentValue.ToString();

                            entityView.Filter = new PredicateExpression(CustomTextFields.Type == type);

                            CustomTextEntity customTextEntity;
                            if (entityView.Count == 0)
                            {
                                customTextEntity = CustomTextHelper.CreateEntity(type, cultureCode, text, primaryKeyField.Name, primaryKeyValue);
                                if (customTextEntity != null)
                                {
                                    customTextEntity.AddToTransaction(transaction);
                                    customTextEntity.Save();

                                    customTextEntity.IsNew = true;
                                }
                            }
                            else
                            {
                                customTextEntity = entityView[0];
                                if (text.IsNullOrWhiteSpace())
                                {
                                    // Delete entry if new text is empty

                                    customTextEntity.AddToTransaction(transaction);
                                    customTextCollectionToDelete.Add(customTextEntity);

                                }
                                else
                                {
                                    // Only update entity if text has changed
                                    customTextEntity.Text = text;

                                    customTextEntity.AddToTransaction(transaction);
                                    customTextEntity.Save();

                                    customTextEntity.IsDirty = true;
                                }
                            }
                        }
                    }

                    customTextCollectionToDelete.AddToTransaction(transaction);
                    customTextCollectionToDelete.DeleteMulti();
                }
            }
        }

        /// <summary>
        /// Add or update custom text entity. When updating entity with empty text value the entity will be deleted.
        /// </summary>
        /// <returns>Returns null when language is not supported</returns>
        public static CustomTextEntity AddOrUpdateEntity(CustomTextType type, string cultureCode, string text, EntityField primaryKey, int primaryValue, ITransaction transaction = null)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CustomTextFields.Type == type);
            filter.Add(CustomTextFields.CultureCode == cultureCode);
            filter.Add(primaryKey == primaryValue);

            CustomTextEntity entity;
            CustomTextCollection collection = CustomTextHelper.GetCustomTexts(filter);
            if (collection.Count == 0)
            {
                entity = CustomTextHelper.CreateEntity(type, cultureCode, text, primaryKey, primaryValue);
                if (entity != null)
                {
                    entity.AddToTransaction(transaction);
                    entity.Save();

                    entity.IsNew = true;
                }
            }
            else
            {
                entity = collection[0];
                if (text.IsNullOrWhiteSpace())
                {
                    // Delete entry if new text is empty
                    entity.AddToTransaction(transaction);
                    entity.Delete();
                }
                else if (!entity.Text.Equals(text))
                {
                    // Only update entity if text has changed
                    entity.Text = text;

                    entity.AddToTransaction(transaction);
                    entity.Save();

                    entity.IsDirty = true;
                }
            }

            return entity;
        }

        #endregion

        #region Data Retrieval

        public static CustomTextEntity GetCustomText(int customTextId)
        {
            if (customTextId > 0)
                return new CustomTextEntity(customTextId);

            return new CustomTextEntity();
        }

        private static CustomTextCollection GetCustomTexts(PredicateExpression filter)
        {
            return EntityCollection.GetMulti<CustomTextCollection>(filter);
        }

        #endregion

        #region Mappings

        public static Dictionary<int, CustomTextType> GetMappedDictionaryForEntity(IEntity entity)
        {
            if (entity is ActionButtonEntity)
            {
                return CustomTextHelper.MappedActionButtonCustomTextTypes;
            }
            else if (entity is AdvertisementEntity)
            {
                return CustomTextHelper.MappedAdvertisementCustomTextTypes;
            }
            else if (entity is AlterationEntity)
            {
                return CustomTextHelper.MappedAlterationCustomTextTypes;
            }
            else if (entity is AlterationoptionEntity)
            {
                return CustomTextHelper.MappedAlterationoptionCustomTextTypes;
            }
            else if (entity is AmenityEntity)
            {
                return CustomTextHelper.MappedAmenityCustomTextTypes;
            }
            else if (entity is AnnouncementEntity)
            {
                return CustomTextHelper.MappedAnnouncementCustomTextTypes;
            }
            else if (entity is AttachmentEntity)
            {
                return CustomTextHelper.MappedAttachmentCustomTextTypes;
            }
            else if (entity is AvailabilityEntity)
            {
                return CustomTextHelper.MappedAvailabilityCustomTextTypes;
            }
            else if (entity is CategoryEntity)
            {
                return CustomTextHelper.MappedCategoryCustomTextTypes;
            }
            else if (entity is CompanyEntity)
            {
                return CustomTextHelper.MappedCompanyCustomTextTypes;
            }
            else if (entity is DeliverypointgroupEntity)
            {
                return CustomTextHelper.MappedDeliverypointgroupCustomTextTypes;
            }
            else if (entity is EntertainmentcategoryEntity)
            {
                return CustomTextHelper.MappedEntertainmentcategoryCustomTextTypes;
            }
            else if (entity is GenericcategoryEntity)
            {
                return CustomTextHelper.MappedGenericcategoryCustomTextTypes;
            }
            else if (entity is GenericproductEntity)
            {
                return CustomTextHelper.MappedGenericproductCustomTextTypes;
            }
            else if (entity is PageEntity)
            {
                return CustomTextHelper.MappedPageCustomTextTypes;
            }
            else if (entity is PageTemplateEntity)
            {
                return CustomTextHelper.MappedPageTemplateCustomTextTypes;
            }
            else if (entity is PointOfInterestEntity)
            {
                return CustomTextHelper.MappedPointOfInterestCustomTextTypes;
            }
            else if (entity is ProductEntity)
            {
                return CustomTextHelper.MappedProductCustomTextTypes;
            }
            else if (entity is RoomControlAreaEntity)
            {
                return CustomTextHelper.MappedRoomControlAreaCustomTextTypes;
            }
            else if (entity is RoomControlComponentEntity)
            {
                return CustomTextHelper.MappedRoomControlComponentCustomTextTypes;
            }
            else if (entity is RoomControlSectionEntity)
            {
                return CustomTextHelper.MappedRoomControlSectionCustomTextTypes;
            }
            else if (entity is RoomControlSectionItemEntity)
            {
                return CustomTextHelper.MappedRoomControlSectionItemCustomTextTypes;
            }
            else if (entity is RoomControlWidgetEntity)
            {
                return CustomTextHelper.MappedRoomControlWidgetCustomTextTypes;
            }
            else if (entity is SiteEntity)
            {
                return CustomTextHelper.MappedSiteCustomTextTypes;
            }
            else if (entity is SiteTemplateEntity)
            {
                return CustomTextHelper.MappedSiteTemplateCustomTextTypes;
            }
            else if (entity is StationEntity)
            {
                return CustomTextHelper.MappedStationCustomTextTypes;
            }
            else if (entity is UIFooterItemEntity)
            {
                return CustomTextHelper.MappedUiFooterItemCustomTextTypes;
            }
            else if (entity is UITabEntity)
            {
                return CustomTextHelper.MappedUiTabCustomTextTypes;
            }
            else if (entity is UIWidgetEntity)
            {
                return CustomTextHelper.MappedUiWidgetCustomTextTypes;
            }
            else if (entity is VenueCategoryEntity)
            {
                return CustomTextHelper.MappedVenueCategoryCustomTextTypes;
            }
            else if (entity is ProductgroupEntity)
            {
                return CustomTextHelper.MappedProductgroupCustomTextTypes;
            }
            else if (entity is InfraredCommandEntity)
            {
                return CustomTextHelper.MappedInfraredCommandCustomTextTypes;
            }
            else if (entity is CarouselItemEntity)
            {
                return CustomTextHelper.MappedApplessCarouselItemCustomTextTypes;
            }
            else if (entity is NavigationMenuItemEntity)
            {
                return CustomTextHelper.MappedApplessNavigationMenuItemCustomTextTypes;
            }
            else if (entity is WidgetActionButtonEntity)
            {
                return CustomTextHelper.MappedApplessWidgetActionButtonTextCustomTextTypes;
            }
            else if (entity is WidgetActionBannerEntity)
            {
                return CustomTextHelper.MappedApplessWidgetActionBannerCustomTextTypes;
            }
            else if (entity is WidgetLanguageSwitcherEntity)
            {
                return CustomTextHelper.MappedApplessWidgetLanguageSwitcherCustomTextTypes;
            }
            else if (entity is WidgetPageTitleEntity)
            {
                return CustomTextHelper.MappedApplessWidgetPageTitleCustomTextTypes;
            }
            else if (entity is ServiceMethodEntity)
            {
                return CustomTextHelper.MappedServiceMethodCustomTextTypes;
            }
            else if (entity is CheckoutMethodEntity)
            {
                return CustomTextHelper.MappedCheckoutMethodCustomTextTypes;
            }
            else if (entity is WidgetWaitTimeEntity)
            {
                return CustomTextHelper.MappedApplessWidgetWaitTimeCustomTextTypes;
            }
            else if (entity is WidgetOpeningTimeEntity)
            {
                return CustomTextHelper.MappedApplessWidgetOpeningTimeCustomTextTypes;
            }
            else if (entity is WidgetMarkdownEntity)
            {
                return CustomTextHelper.MappedApplessWidgetMarkdownCustomTextTypes;
            }
            else if (entity is LandingPageEntity)
            {
                return CustomTextHelper.MappedApplessLandingPageCustomTextTypes;
            }

            return null;
        }

        private static readonly Dictionary<int, CustomTextType> MappedCompanyCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)CompanyFieldIndex.Description, CustomTextType.CompanyDescription },
            { (int)CompanyFieldIndex.DescriptionSingleLine, CustomTextType.CompanyDescriptionShort },
        };

        private static readonly Dictionary<int, CustomTextType> MappedDeliverypointgroupCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)DeliverypointgroupFieldIndex.OrderProcessingNotificationTitle, CustomTextType.DeliverypointgroupOrderProcessingNotificationTitle },
            { (int)DeliverypointgroupFieldIndex.OrderProcessingNotification, CustomTextType.DeliverypointgroupOrderProcessingNotificationText },
            { (int)DeliverypointgroupFieldIndex.ServiceProcessingNotificationTitle, CustomTextType.DeliverypointgroupServiceProcessingNotificationTitle },
            { (int)DeliverypointgroupFieldIndex.ServiceProcessingNotification, CustomTextType.DeliverypointgroupServiceProcessingNotificationText },
            { (int)DeliverypointgroupFieldIndex.OrderProcessedNotificationTitle, CustomTextType.DeliverypointgroupOrderProcessedNotificationTitle },
            { (int)DeliverypointgroupFieldIndex.OrderProcessedNotification, CustomTextType.DeliverypointgroupOrderProcessedNotificationText },
            { (int)DeliverypointgroupFieldIndex.ServiceProcessedNotificationTitle, CustomTextType.DeliverypointgroupServiceProcessedNotificationTitle },
            { (int)DeliverypointgroupFieldIndex.ServiceProcessedNotification, CustomTextType.DeliverypointgroupServiceProcessedNotificationText },
            { (int)DeliverypointgroupFieldIndex.FreeformMessageTitle, CustomTextType.DeliverypointgroupFreeformMessageTitle },
            { (int)DeliverypointgroupFieldIndex.OutOfChargeTitle, CustomTextType.DeliverypointgroupOutOfChargeTitle },
            { (int)DeliverypointgroupFieldIndex.OutOfChargeMessage, CustomTextType.DeliverypointgroupOutOfChargeText },
            { (int)DeliverypointgroupFieldIndex.OrderProcessedTitle, CustomTextType.DeliverypointgroupOrderProcessedTitle },
            { (int)DeliverypointgroupFieldIndex.OrderProcessedMessage, CustomTextType.DeliverypointgroupOrderProcessedText },
            { (int)DeliverypointgroupFieldIndex.OrderFailedTitle, CustomTextType.DeliverypointgroupOrderFailedTitle },
            { (int)DeliverypointgroupFieldIndex.OrderFailedMessage, CustomTextType.DeliverypointgroupOrderFailedText },
            { (int)DeliverypointgroupFieldIndex.OrderSavingTitle, CustomTextType.DeliverypointgroupOrderSavingTitle },
            { (int)DeliverypointgroupFieldIndex.OrderSavingMessage, CustomTextType.DeliverypointgroupOrderSavingText },
            { (int)DeliverypointgroupFieldIndex.OrderCompletedTitle, CustomTextType.DeliverypointgroupOrderCompletedTitle },
            { (int)DeliverypointgroupFieldIndex.OrderCompletedMessage, CustomTextType.DeliverypointgroupOrderCompletedText },
            { (int)DeliverypointgroupFieldIndex.ServiceSavingTitle, CustomTextType.DeliverypointgroupServiceSavingTitle },
            { (int)DeliverypointgroupFieldIndex.ServiceSavingMessage, CustomTextType.DeliverypointgroupServiceSavingText },
            { (int)DeliverypointgroupFieldIndex.ServiceProcessedTitle, CustomTextType.DeliverypointgroupServiceProcessedTitle },
            { (int)DeliverypointgroupFieldIndex.ServiceProcessedMessage, CustomTextType.DeliverypointgroupServiceProcessedText },
            { (int)DeliverypointgroupFieldIndex.ServiceFailedTitle, CustomTextType.DeliverypointgroupServiceFailedTitle },
            { (int)DeliverypointgroupFieldIndex.ServiceFailedMessage, CustomTextType.DeliverypointgroupServiceFailedText },
            { (int)DeliverypointgroupFieldIndex.RatingSavingTitle, CustomTextType.DeliverypointgroupRatingSavingTitle },
            { (int)DeliverypointgroupFieldIndex.RatingSavingMessage, CustomTextType.DeliverypointgroupRatingSavingText },
            { (int)DeliverypointgroupFieldIndex.RatingProcessedTitle, CustomTextType.DeliverypointgroupRatingProcessedTitle },
            { (int)DeliverypointgroupFieldIndex.RatingProcessedMessage, CustomTextType.DeliverypointgroupRatingProcessedText },
            { (int)DeliverypointgroupFieldIndex.OrderingNotAvailableMessage, CustomTextType.DeliverypointgroupOrderingNotAvailableText },
            { (int)DeliverypointgroupFieldIndex.PmsDeviceLockedTitle, CustomTextType.DeliverypointgroupPmsDeviceLockedTitle },
            { (int)DeliverypointgroupFieldIndex.PmsDeviceLockedText, CustomTextType.DeliverypointgroupPmsDeviceLockedText },
            { (int)DeliverypointgroupFieldIndex.PmsCheckinFailedTitle, CustomTextType.DeliverypointgroupPmsCheckinFailedTitle },
            { (int)DeliverypointgroupFieldIndex.PmsCheckinFailedText, CustomTextType.DeliverypointgroupPmsCheckinFailedText },
            { (int)DeliverypointgroupFieldIndex.PmsRestartTitle, CustomTextType.DeliverypointgroupPmsRestartTitle },
            { (int)DeliverypointgroupFieldIndex.PmsRestartText, CustomTextType.DeliverypointgroupPmsRestartText },
            { (int)DeliverypointgroupFieldIndex.PmsWelcomeTitle, CustomTextType.DeliverypointgroupPmsWelcomeTitle },
            { (int)DeliverypointgroupFieldIndex.PmsWelcomeText, CustomTextType.DeliverypointgroupPmsWelcomeText },
            { (int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteTitle, CustomTextType.DeliverypointgroupPmsCheckoutCompleteTitle },
            { (int)DeliverypointgroupFieldIndex.PmsCheckoutCompleteText, CustomTextType.DeliverypointgroupPmsCheckoutCompleteText },
            { (int)DeliverypointgroupFieldIndex.PmsCheckoutApproveText, CustomTextType.DeliverypointgroupPmsCheckoutApproveText },
            { (int)DeliverypointgroupFieldIndex.ChargerRemovedDialogTitle, CustomTextType.DeliverypointgroupChargerRemovedTitle },
            { (int)DeliverypointgroupFieldIndex.ChargerRemovedDialogText, CustomTextType.DeliverypointgroupChargerRemovedText },
            { (int)DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogTitle, CustomTextType.DeliverypointgroupChargerRemovedReminderTitle },
            { (int)DeliverypointgroupFieldIndex.ChargerRemovedReminderDialogText, CustomTextType.DeliverypointgroupChargerRemovedReminderText },
            { (int)DeliverypointgroupFieldIndex.TurnOffPrivacyTitle, CustomTextType.DeliverypointgroupTurnOffPrivacyTitle },
            { (int)DeliverypointgroupFieldIndex.TurnOffPrivacyText, CustomTextType.DeliverypointgroupTurnOffPrivacyText },
            { (int)DeliverypointgroupFieldIndex.ItemCurrentlyUnavailableText, CustomTextType.DeliverypointgroupItemCurrentlyUnavailableText },
            { (int)DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingTitle, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingTitle },
            { (int)DeliverypointgroupFieldIndex.AlarmSetWhileNotChargingText, CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingText },
            { (int)DeliverypointgroupFieldIndex.HotelUrl1, CustomTextType.DeliverypointgroupHotelUrl1 },
            { (int)DeliverypointgroupFieldIndex.HotelUrl1Caption, CustomTextType.DeliverypointgroupHotelUrl1Caption },
            { (int)DeliverypointgroupFieldIndex.HotelUrl2, CustomTextType.DeliverypointgroupHotelUrl2 },
            { (int)DeliverypointgroupFieldIndex.HotelUrl2Caption, CustomTextType.DeliverypointgroupHotelUrl2Caption },
            { (int)DeliverypointgroupFieldIndex.HotelUrl3, CustomTextType.DeliverypointgroupHotelUrl3 },
            { (int)DeliverypointgroupFieldIndex.HotelUrl3Caption, CustomTextType.DeliverypointgroupHotelUrl3Caption },
            { (int)DeliverypointgroupFieldIndex.DeliverypointCaption, CustomTextType.DeliverypointgroupDeliverypointCaption },
            { (int)DeliverypointgroupFieldIndex.RoomserviceCharge, CustomTextType.DeliverypointgroupRoomserviceChargeText },
        };

        private static readonly Dictionary<int, CustomTextType> MappedCategoryCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)CategoryFieldIndex.Name, CustomTextType.CategoryName },
            { (int)CategoryFieldIndex.Description, CustomTextType.CategoryDescription },
        };

        private static readonly Dictionary<int, CustomTextType> MappedProductCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)ProductFieldIndex.Name, CustomTextType.ProductName },
            { (int)ProductFieldIndex.Description, CustomTextType.ProductDescription },
            { (int)ProductFieldIndex.ShortDescription, CustomTextType.ProductShortDescription },
            { (int)ProductFieldIndex.ButtonText, CustomTextType.ProductButtonText },
            { (int)ProductFieldIndex.CustomizeButtonText, CustomTextType.ProductCustomizeButtonText },
        };

        private static readonly Dictionary<int, CustomTextType> MappedActionButtonCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)ActionButtonFieldIndex.Name, CustomTextType.ActionButtonName }
        };

        private static readonly Dictionary<int, CustomTextType> MappedAdvertisementCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AdvertisementFieldIndex.Name, CustomTextType.AdvertisementName },
            { (int)AdvertisementFieldIndex.Description, CustomTextType.AdvertisementDescription },
        };

        private static readonly Dictionary<int, CustomTextType> MappedAlterationCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AlterationFieldIndex.Name, CustomTextType.AlterationName },
            { (int)AlterationFieldIndex.Description, CustomTextType.AlterationDescription },
        };

        private static readonly Dictionary<int, CustomTextType> MappedAlterationoptionCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AlterationoptionFieldIndex.Name, CustomTextType.AlterationoptionName },
            { (int)AlterationoptionFieldIndex.Description, CustomTextType.AlterationoptionDescription },
        };

        private static readonly Dictionary<int, CustomTextType> MappedAmenityCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AmenityFieldIndex.Name, CustomTextType.AmenityName }
        };

        private static readonly Dictionary<int, CustomTextType> MappedAnnouncementCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AnnouncementFieldIndex.Title, CustomTextType.AnnouncementTitle },
            { (int)AnnouncementFieldIndex.Text, CustomTextType.AnnouncementText },
        };

        private static readonly Dictionary<int, CustomTextType> MappedAttachmentCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AttachmentFieldIndex.Name, CustomTextType.AttachmentName }
        };

        private static readonly Dictionary<int, CustomTextType> MappedAvailabilityCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)AvailabilityFieldIndex.Name, CustomTextType.AvailabilityName },
            { (int)AvailabilityFieldIndex.Status, CustomTextType.AvailabilityStatus }
        };

        private static readonly Dictionary<int, CustomTextType> MappedEntertainmentcategoryCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)EntertainmentcategoryFieldIndex.Name, CustomTextType.EntertainmentcategoryName }
        };

        private static readonly Dictionary<int, CustomTextType> MappedGenericcategoryCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)GenericcategoryFieldIndex.Name, CustomTextType.GenericcategoryName }
        };

        private static readonly Dictionary<int, CustomTextType> MappedGenericproductCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)GenericproductFieldIndex.Name, CustomTextType.GenericproductName },
            { (int)GenericproductFieldIndex.Description, CustomTextType.GenericproductDescription },
        };

        private static readonly Dictionary<int, CustomTextType> MappedPageCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)PageFieldIndex.Name, CustomTextType.PageName }
        };

        private static readonly Dictionary<int, CustomTextType> MappedPageTemplateCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)PageTemplateFieldIndex.Name, CustomTextType.PageTemplateName }
        };

        private static readonly Dictionary<int, CustomTextType> MappedPointOfInterestCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)PointOfInterestFieldIndex.Description, CustomTextType.PointOfInterestDescription },
            { (int)PointOfInterestFieldIndex.DescriptionSingleLine, CustomTextType.PointOfInterestDescriptionSingleLine },
        };

        private static readonly Dictionary<int, CustomTextType> MappedRoomControlAreaCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)RoomControlAreaFieldIndex.Name, CustomTextType.RoomControlAreaName },
        };

        private static readonly Dictionary<int, CustomTextType> MappedRoomControlComponentCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)RoomControlComponentFieldIndex.Name, CustomTextType.RoomControlComponentName },
        };

        private static readonly Dictionary<int, CustomTextType> MappedRoomControlSectionCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)RoomControlSectionFieldIndex.Name, CustomTextType.RoomControlSectionName },
        };

        private static readonly Dictionary<int, CustomTextType> MappedRoomControlSectionItemCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)RoomControlSectionItemFieldIndex.Name, CustomTextType.RoomControlSectionItemName },
        };

        private static readonly Dictionary<int, CustomTextType> MappedRoomControlWidgetCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)RoomControlWidgetFieldIndex.Caption, CustomTextType.RoomControlWidgetCaption },
        };

        private static readonly Dictionary<int, CustomTextType> MappedSiteCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)SiteFieldIndex.Description, CustomTextType.SiteDescription },
        };

        private static readonly Dictionary<int, CustomTextType> MappedSiteTemplateCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)SiteTemplateFieldIndex.Name, CustomTextType.SiteTemplateName },
        };

        private static readonly Dictionary<int, CustomTextType> MappedStationCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)StationFieldIndex.Name, CustomTextType.StationName },
            { (int)StationFieldIndex.Description, CustomTextType.StationDescription },
            { (int)StationFieldIndex.SuccessMessage, CustomTextType.StationSuccessMessage },
        };

        private static readonly Dictionary<int, CustomTextType> MappedUiFooterItemCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)UIFooterItemFieldIndex.Name, CustomTextType.UIFooterItemName },
        };

        private static readonly Dictionary<int, CustomTextType> MappedUiTabCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
             { (int)UITabFieldIndex.Caption, CustomTextType.UITabCaption },
             { (int)UITabFieldIndex.URL, CustomTextType.UITabUrl },
        };

        private static readonly Dictionary<int, CustomTextType> MappedUiWidgetCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)UIWidgetFieldIndex.Caption, CustomTextType.UIWidgetCaption },
            { (int)UIWidgetFieldIndex.FieldValue1, CustomTextType.UIWidgetFieldValue1 },
            { (int)UIWidgetFieldIndex.FieldValue2, CustomTextType.UIWidgetFieldValue2 },
            { (int)UIWidgetFieldIndex.FieldValue3, CustomTextType.UIWidgetFieldValue3 },
            { (int)UIWidgetFieldIndex.FieldValue4, CustomTextType.UIWidgetFieldValue4 },
            { (int)UIWidgetFieldIndex.FieldValue5, CustomTextType.UIWidgetFieldValue5 },
        };

        private static readonly Dictionary<int, CustomTextType> MappedVenueCategoryCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)VenueCategoryFieldIndex.Name, CustomTextType.VenueCategoryName },
        };

        private static readonly Dictionary<int, CustomTextType> MappedProductgroupCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)ProductgroupFieldIndex.Name, CustomTextType.ProductgroupName }
        };

        private static readonly Dictionary<int, CustomTextType> MappedInfraredCommandCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)InfraredCommandFieldIndex.Name, CustomTextType.InfraredCommandName }
        };

        private static readonly Dictionary<int, CustomTextType> MappedApplessCarouselItemCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)CarouselItemFieldIndex.Message, CustomTextType.AppLessCarouselItemMessage }
        };

        private static readonly Dictionary<int, CustomTextType> MappedApplessNavigationMenuItemCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)NavigationMenuItemFieldIndex.Text, CustomTextType.AppLessNavigationMenuItemText }
        };

        private static readonly Dictionary<int, CustomTextType> MappedApplessWidgetActionBannerCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)WidgetActionBannerFieldIndex.Text, CustomTextType.AppLessWidgetActionBannerText }
        };

        private static readonly Dictionary<int, CustomTextType> MappedApplessWidgetActionButtonTextCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)WidgetActionButtonFieldIndex.Text, CustomTextType.AppLessWidgetActionButtonText }
        };

        private static readonly Dictionary<int, CustomTextType> MappedApplessWidgetLanguageSwitcherCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)WidgetLanguageSwitcherFieldIndex.Text, CustomTextType.AppLessWidgetLanguageSwitcherText }
        };

        private static readonly Dictionary<int, CustomTextType> MappedApplessWidgetPageTitleCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)WidgetPageTitleFieldIndex.Title, CustomTextType.AppLessWidgetPageTitle }
        };

        private static readonly Dictionary<int, CustomTextType> MappedServiceMethodCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)ServiceMethodFieldIndex.Label, CustomTextType.ServiceMethodLabel },
            { (int)ServiceMethodFieldIndex.Description, CustomTextType.ServiceMethodDescription }
        };

        private static readonly Dictionary<int, CustomTextType> MappedCheckoutMethodCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)CheckoutMethodFieldIndex.Label, CustomTextType.CheckoutMethodLabel },
            { (int)CheckoutMethodFieldIndex.Description, CustomTextType.CheckoutMethodDescription },
            { (int)CheckoutMethodFieldIndex.ConfirmationDescription, CustomTextType.CheckoutMethodConfirmationDescription },
            { (int)CheckoutMethodFieldIndex.TermsAndConditionsLabel, CustomTextType.CheckoutMethodTermsAndConditionsLabel }
        };

        private static readonly Dictionary<int, CustomTextType> MappedApplessWidgetWaitTimeCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)WidgetWaitTimeFieldIndex.Text, CustomTextType.AppLessWidgetWaitTimeText }
        };

        private static readonly Dictionary<int, CustomTextType> MappedApplessWidgetOpeningTimeCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)WidgetOpeningTimeFieldIndex.Title, CustomTextType.AppLessWidgetOpeningTimeTitle }
        };

        private static readonly Dictionary<int, CustomTextType> MappedApplessWidgetMarkdownCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)WidgetMarkdownFieldIndex.Title, CustomTextType.AppLessWidgetMarkdownTitle },
            { (int)WidgetMarkdownFieldIndex.Text, CustomTextType.AppLessWidgetMarkdownText }
        };

        private static readonly Dictionary<int, CustomTextType> MappedApplessLandingPageCustomTextTypes = new Dictionary<int, CustomTextType>()
        {
            { (int)LandingPageFieldIndex.Name, CustomTextType.ApplessPageName }
        };

        #endregion        

        #region CustomTextType Ranges

        private class CustomTextTypeRangeConverter
        {
            public readonly List<CustomTextType> CustomTextTypes = new List<CustomTextType>();

            public CustomTextTypeRangeConverter(Array enumValues, int start, int end)
            {
                for (int i = 0; i < enumValues.Length; i++)
                {
                    int enumValue = Convert.ToInt32(enumValues.GetValue(i));
                    if (enumValue >= start && enumValue < end)
                    {
                        this.CustomTextTypes.Add((CustomTextType)enumValue);
                    }
                }
            }
        }

        #endregion
    }
}
