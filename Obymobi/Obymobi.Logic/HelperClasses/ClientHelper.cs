﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Web;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Analytics;
using Obymobi.Logic.Model;
using Obymobi.Logic.Routing;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SpreadsheetLight;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// ClientHelper class
    /// </summary>
    public static class ClientHelper
    {
        public enum ClientHelperResult
        { 
            MultipleClientsForDevice = 200,
            ApplicationCodeNotFound = 300,
        }

        /// <summary>
        /// Gets a combined list of Netmessages and ClientLogs
        /// </summary>
        /// <returns></returns>
        public static List<DeviceActivityLog> GetClientActivityLogs(this ClientEntity client, bool includeNetmessages, bool includeClientLogs, bool includeClientStates, int? hours)
        {
            List<DeviceActivityLog> toReturn = new List<DeviceActivityLog>();
            DateTime from = DateTime.MinValue;

            if (hours.HasValue)
                from = DateTime.UtcNow.AddHours(-hours.Value);

            if (includeNetmessages)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(NetmessageFields.ReceiverClientId == client.ClientId);
                filter.Add(NetmessageFields.MessageType != NetmessageType.Ping);
                filter.Add(NetmessageFields.MessageType != NetmessageType.Pong);

                if (from != DateTime.MinValue)
                    filter.Add(NetmessageFields.CreatedUTC >= from);

                SortExpression sort = new SortExpression(NetmessageFields.NetmessageId | SortOperator.Descending);

                NetmessageCollection netmessages = new NetmessageCollection();
                netmessages.GetMulti(filter, 0, sort);

                // Convert Netmessages to ClientActivityLogs
                foreach (var netmessage in netmessages)
                    toReturn.Add(new DeviceActivityLog(netmessage));
            }

            if (includeClientLogs)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ClientLogFields.ClientId == client.ClientId);

                if (from != DateTime.MinValue)
                    filter.Add(ClientLogFields.CreatedUTC >= from);

                SortExpression sort = new SortExpression(ClientLogFields.ClientLogId | SortOperator.Descending);

                ClientLogCollection clientlogs = new ClientLogCollection();
                clientlogs.GetMulti(filter, 0, sort);

                foreach (var clientlog in clientlogs)
                    toReturn.Add(new DeviceActivityLog(clientlog));
            }

            if (includeClientStates)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ClientStateFields.ClientId == client.ClientId);

                if (from != DateTime.MinValue)
                    filter.Add(ClientStateFields.CreatedUTC >= from);

                SortExpression sort = new SortExpression(ClientStateFields.ClientStateId | SortOperator.Descending);

                ClientStateCollection clientStates = new ClientStateCollection();
                clientStates.GetMulti(filter, 0, sort);
                
                foreach (var clientState in clientStates)
                    toReturn.Add(new DeviceActivityLog(clientState));
            }

            toReturn = toReturn.OrderByDescending(x => x.DateTime ?? DateTime.MinValue).ToList();

            return toReturn;
        }

        /// <summary>
        /// Gets a combined list of Netmessages and ClientLogs
        /// </summary>
        /// <returns></returns>
        public static List<DeviceActivityLog> GetClientActivityLogs(this ClientEntity client, bool includeNetmessages, bool includeClientLogs, bool includeClientStates, int maxResults)
        {
            List<DeviceActivityLog> toReturn = new List<DeviceActivityLog>();
            DateTime lastnetMessageCreated = DateTime.MinValue;

            if (includeNetmessages)
            {
                NetmessageCollection netmessages = new NetmessageCollection();
                SortExpression sort = new SortExpression(NetmessageFields.NetmessageId | SortOperator.Descending);
                PredicateExpression filter = new PredicateExpression();
                filter.Add(NetmessageFields.ReceiverClientId == client.ClientId);                
                filter.Add(NetmessageFields.MessageType != NetmessageType.Ping);
                filter.Add(NetmessageFields.MessageType != NetmessageType.Pong);
                netmessages.GetMulti(filter, maxResults, sort);

                // Convert Netmessages to ClientActivityLogs
                foreach (var netmessage in netmessages)
                    toReturn.Add(new DeviceActivityLog(netmessage));                

                if(netmessages.Count > 0)
                    lastnetMessageCreated = netmessages.Min(x => x.CreatedUTC ?? DateTime.MinValue);
            }

            if (includeClientLogs)
            {
                ClientLogCollection clientlogs = new ClientLogCollection();
                SortExpression sort = new SortExpression(ClientLogFields.ClientLogId | SortOperator.Descending);
                PredicateExpression filter = new PredicateExpression();                                
                filter.Add(ClientLogFields.ClientId == client.ClientId);

                if (lastnetMessageCreated != DateTime.MinValue)
                {
                    // Try to get all ClietnLogs in the period that is span by the Netmessages
                    PredicateExpression filterToNetmessages = new PredicateExpression();
                    filterToNetmessages.Add(filter);
                    filterToNetmessages.Add(ClientLogFields.CreatedUTC >= filterToNetmessages);
                    clientlogs.GetMulti(filter, 0, sort);
                }
                else
                    clientlogs.GetMulti(filter, maxResults, sort);

                foreach (var clientlog in clientlogs)
                    toReturn.Add(new DeviceActivityLog(clientlog));
                
                // Check if we have enough results
                if (toReturn.Count < maxResults)
                { 
                    // Add more 
                    int moreRequired = maxResults - toReturn.Count;
                    if(clientlogs.Count > 0)
                        filter.Add(ClientLogFields.ClientLogId < clientlogs.Min(x => x.ClientLogId));
                    clientlogs.GetMulti(filter, moreRequired, sort);

                    foreach (var clientlog in clientlogs)
                        toReturn.Add(new DeviceActivityLog(clientlog));
                }
            }

            if (includeClientStates)
            {
                ClientStateCollection clientStates = new ClientStateCollection();
                SortExpression sort = new SortExpression(ClientStateFields.ClientStateId | SortOperator.Descending);
                PredicateExpression filter = new PredicateExpression();
                filter.Add(ClientStateFields.ClientId == client.ClientId);

                if (lastnetMessageCreated != DateTime.MinValue)
                {
                    // Try to get all ClientStates in the period that is span by the Netmessages
                    PredicateExpression filterToNetmessages = new PredicateExpression();
                    filterToNetmessages.Add(filter);
                    filterToNetmessages.Add(ClientStateFields.CreatedUTC >= filterToNetmessages);
                    clientStates.GetMulti(filter, 0, sort);
                }
                else
                    clientStates.GetMulti(filter, maxResults, sort);

                foreach (var clientState in clientStates)
                    toReturn.Add(new DeviceActivityLog(clientState));

                // Check if we have enough results
                if (toReturn.Count < maxResults)
                {
                    // Add more 
                    int moreRequired = maxResults - toReturn.Count;
                    if (clientStates.Count > 0)
                        filter.Add(ClientStateFields.ClientStateId < clientStates.Min(x => x.ClientStateId));
                    clientStates.GetMulti(filter, moreRequired, sort);

                    foreach (var clientState in clientStates)
                        toReturn.Add(new DeviceActivityLog(clientState));
                }
            }

            toReturn = toReturn.OrderByDescending(x => x.DateTime ?? DateTime.MinValue).ToList();

            if (toReturn.Count > maxResults)
                toReturn = toReturn.Take(maxResults).ToList();

            return toReturn;
        }

        /// <summary>
        /// Creates (but does NOT save) a TerminalLog entity pre-filled with the information about
        /// the device change in the supplied TerminalEntity (based on Current en DbValue of DeviceId field).
        /// </summary>
        /// <param name="terminal"></param>
        /// <returns></returns>
        public static ClientLogEntity CreateLogForDeviceChange(this ClientEntity client)
        {
            ClientLogEntity log = new ClientLogEntity();
            log.AddToTransaction(client);
            log.TypeEnum = ClientLogType.DeviceChanged;
            log.ClientId = client.ClientId;

            // Fetch previous device
            DeviceEntity previousDevice = null;
            DeviceCollection devices = new DeviceCollection();
            devices.AddToTransaction(client);

            if (client.Fields[ClientFields.DeviceId.Name].DbValue != null)
            {
                int previousDeviceId = (int)client.Fields[ClientFields.DeviceId.Name].DbValue;
                PredicateExpression filter = new PredicateExpression(DeviceFields.DeviceId == previousDeviceId);
                previousDevice = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<DeviceEntity>(filter, devices);
            }

            // Fetch current device
            DeviceEntity currentDevice = null;
            if (client.DeviceId.HasValue)
                currentDevice = client.DeviceEntity;

            if (previousDevice != null)
            {
                if (currentDevice != null)
                {
                    // We came from a device and go to one.
                    log.Message = StringUtil.FormatSafe("Changed from Device '{0}-{1}' to '{2}-{3}'",
                        previousDevice.DeviceId, previousDevice.Identifier, currentDevice.DeviceId, currentDevice.Identifier);
                }
                else
                {
                    // We came from a device and go to none
                    log.Message = StringUtil.FormatSafe("Device is unlinked: '{0}-{1}'",
                        previousDevice.DeviceId, previousDevice.Identifier);
                }
            }
            else
            {
                // We didn't have a device yet, so first linking
                log.Message = StringUtil.FormatSafe("Device is linked: '{0}-{1}'",
                    currentDevice.DeviceId, currentDevice.Identifier);
            }

            return log;
        }

        /// <summary>
        /// Get the state of the client
        /// </summary>
        /// <param name="clientId">Id of client to check for</param>
        /// <param name="maxMinutesSinceLastCall">The max minutes since last call.</param>
        /// <returns>
        /// state
        /// </returns>
        public static ClientState GetClientState(int clientId, int maxMinutesSinceLastCall)
        {
            ClientState toReturn = ClientState.Unknown;

            // Validate valid client
            ClientEntity client = new ClientEntity(clientId);
            if (client.IsNew)
                toReturn = ClientState.ClientIdIncorrect;

            // Check by last request if online
            if (toReturn == ClientState.Unknown)
            {
                if (client.DeviceEntity.LastRequestUTC.HasValue)
                {
                    if (client.DeviceEntity.LastRequestUTC.Value.AddMinutes(maxMinutesSinceLastCall) >= DateTime.UtcNow)
                    {
                        toReturn = ClientState.Online;
                    }
                }

                // When not online, check why
                if (toReturn != ClientState.Online)
                {
                    toReturn = ClientState.Offline;
                }
            }

            return toReturn;
        }

        /// <summary>
        /// Creates the client entity.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="companyId">The company id.</param>
        /// <param name="deliverypointgroupId">The deliverypointgroup id.</param>
        /// <param name="teamviewerId">The teamviewer id.</param>
        /// <param name="configuration">The configuration.</param>
        /// <param name="notes">The notes.</param>
        /// <returns></returns>
        public static ClientEntity CreateClientEntity(string serialNumber, int companyId, int? deliverypointgroupId, string teamviewerId, string configuration, string notes)
        {
            ClientEntity clientEntity = new ClientEntity();

            clientEntity.CompanyId = companyId;
            if (deliverypointgroupId.HasValue && deliverypointgroupId.Value > 0)
                clientEntity.DeliverypointGroupId = deliverypointgroupId.Value;
            clientEntity.Notes = notes;

            return clientEntity;
        }

        /// <summary>
        /// Gets the client using the specified client id
        /// </summary>
        /// <param name="clientId">The id of the client</param>
        /// <returns>A <see cref="ClientEntity"/> instance if the client was found, otherwise null</returns>
        public static ClientEntity GetClientEntityByClientId(int clientId)
        {
            ClientEntity clientEntity = null;

            if (clientId <= 0)
                throw new ObymobiException(GetClientResult.NoClientIdSpecified, "ClientId '{0}'", clientId);
            else
            {
                clientEntity = new ClientEntity(clientId);
                if (clientEntity.IsNew)
                    throw new ObymobiException(GetClientResult.NoClientFoundForClientId, "ClientId '{0}'", clientId);
            }

            return clientEntity;
        }        

        /// <summary>
        /// Gets / set operation mode automaticly. Can put the Client in KioskAutomatic mode in case of erros
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="customerId">The customer id.</param>
        /// <param name="clientId">The client id.</param>
        /// <param name="operationModeFromClient">The client operation mode</param>
        /// <returns>
        /// The ClientOperation mode at the end of the method
        /// </returns>
        public static ClientOperationMode GetSetOperationMode(ClientEntity client, int customerId, int clientId, ClientOperationMode operationModeFromClient) // GK-ROUTING client and Clientid?
        {
            CompanyEntity company = null;

            // Try to get the company from the cache
            if (!CacheHelper.TryGetEntity<CompanyEntity>(client.CompanyId, out company))
                company = new CompanyEntity(client.CompanyId);

            // Determine if the Company wants automatic change of operation mode;
            if (company.AutomaticClientOperationModes && client.OperationModeEnum != ClientOperationMode.KioskManual)
            {
                ClientLogEntity clientLog = new ClientLogEntity();
                clientLog.ClientId = client.ClientId;

                // Log the current operation mode
                clientLog.TypeEnum = ClientLogType.Unknown;
                clientLog.FromOperationMode = client.OperationMode;

				// Set the operation mode based on the result of the emenu
				client.OperationModeEnum = operationModeFromClient;

                #region Check for unprocessed expired orders

                // Check for unproccesed / expired orders
                int unprocessedOrders;
                if (RoutingHelper.HasClientExpiredOrders(client.ClientId, out unprocessedOrders) &&
                    client.OperationModeEnum != ClientOperationMode.KioskAutomaticUnprocessedOrders)
                {
                    // Switch to kiosk					
                    client.OperationModeEnum = ClientOperationMode.KioskAutomaticUnprocessedOrders;
                    clientLog.TypeEnum = ClientLogType.FailToKioskForUnprocessedOrders;
                }
                int failedOrderCount = unprocessedOrders;

                #endregion

                #region Check for unprocessable orders

                int unprocessableOrderCount = 0;
                if (failedOrderCount == 0)
                {
                    // Init Filter etc.
                    OrderCollection orders = new OrderCollection();
                    PredicateExpression filterUnprocessable = new PredicateExpression();
                    filterUnprocessable.Add(OrderFields.CompanyId == client.CompanyId);
                    if (customerId > 0)
                        filterUnprocessable.Add(OrderFields.CustomerId == customerId);
                    if (client.DeliverypointId.HasValue && client.DeliverypointId.Value > 0)
                        filterUnprocessable.Add(OrderFields.DeliverypointId == client.DeliverypointId.Value);
                    filterUnprocessable.Add(OrderFields.Status == OrderStatus.Unprocessable);

                    // Check for invalid orders:
                    unprocessableOrderCount = (int)orders.GetScalar(OrderFieldIndex.OrderId, null, AggregateFunction.Count, filterUnprocessable);  // MB Experimental
                    if (unprocessableOrderCount > 0 && client.OperationModeEnum != ClientOperationMode.KioskAutomaticUnprocessableOrders)
                    {
                        if (TestUtil.IsPcDeveloper)
                        {
                            OrderCollection orderCollection = new OrderCollection();
                            orderCollection.GetMulti(filterUnprocessable);

                            string orderString = string.Empty;

                            foreach (var order in orderCollection)
                            {
                                orderString += string.Format("Order ID: {0} Status: {1} Created (UTC): {2}\r\n", order.OrderId, order.Status, order.CreatedUTC);
                            }

                            InformationLoggerWeb.LogInformation("kak.txt", orderString);
                        }

                        // Switch to kiosk					
                        client.OperationModeEnum = ClientOperationMode.KioskAutomaticUnprocessableOrders;
                        clientLog.TypeEnum = ClientLogType.FailToKioskForUnprocessableOrders;
                    }
                }

                #endregion

                // Reset state:
                if (failedOrderCount == 0 && unprocessableOrderCount == 0 && 
                    client.OperationModeEnum != ClientOperationMode.Ordering && 
                    client.OperationModeEnum != ClientOperationMode.KioskNoDeliverypointConfigured &&
                    client.OperationModeEnum != ClientOperationMode.KioskDisconnectedFromPokeIn)
                {
                    // Return to normal, but only if we were in ClientOperationMode.KioskAutomatic
                    client.OperationModeEnum = ClientOperationMode.Ordering;
                    clientLog.TypeEnum = ClientLogType.AutomaticReturnToOrdering;
                }

                // Log the new operation mode
                clientLog.ToOperationMode = client.OperationMode;

                // Save changes & log
                if (client.IsDirty)
                {
                    clientLog.Save();
                    client.Save();
                }
            }

            return client.OperationModeEnum;
        }

        /// <summary>
        /// Updates the states of the clients
        /// </summary>
        /// <param name="clients">The System.Array instance containing the Client instances</param>
        /// <param name="maxMinutesSinceLastCall">The max minutes since last call.</param>
        public static void UpdateClientStates(ref Client[] clients, int maxMinutesSinceLastCall)
        {
            for (int i = 0; i < clients.Length; i++)
            {
                Client client = clients[i];
                ClientState state = GetClientState(client.ClientId, maxMinutesSinceLastCall);
                client.CurrentState = state.ToString();

                if (state != ClientState.Online && client.LastRequestNotifiedBySMSUTC != client.LastRequestUTC)
                {
                    // Maximum of 1 sms per day
                    if (client.LastRequestNotifiedBySMSUTC != DateTime.MinValue &&
                        (DateTime.UtcNow - client.LastRequestNotifiedBySMSUTC).TotalHours > 24 &&
                        (DateTime.UtcNow - client.LastRequestUTC).TotalHours > 24)
                    {
                        CompanyEntity c = new CompanyEntity(client.CompanyId);
                        if (c.UseMonitoring)
                        {
                            client.SmsNotificationRequired = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Updates the datetime value of the last request for the specified client
        /// </summary>
        /// <param name="customerId">The id of the customer</param>
        /// <param name="clientId">The id of the client</param>
        /// <param name="statusCode">The status code of the client</param>
        /// <param name="statusMessage">The status message of the client</param>
        /// <param name="ipAddress">The ip address of the client</param>
        [Obsolete("Use the overload with ClientEntity as parameter instead of ClientId")]
        public static void UpdateLastRequest(int customerId, int clientId, ClientStatusCode statusCode, string statusMessage, string ipAddress)
        {
            ClientEntity dummy;
            UpdateLastRequest(customerId, clientId, statusCode, statusMessage, ipAddress, out dummy, string.Empty);
        }

        /// <summary>
        /// Updates the datetime value of the last request for the specified client
        /// </summary>
        /// <param name="customerId">The id of the customer</param>
        /// <param name="clientId">The id of the client</param>
        /// <param name="statusCode">The status code of the client</param>
        /// <param name="statusMessage">The status message of the client</param>
        /// <param name="ipAddress">The ip address.</param>
        /// <param name="clientEntity">The client entity.</param>
        /// <param name="version">The version.</param>        
        [Obsolete("Use the overload with ClientEntity as parameter instead of ClientId")]
        public static void UpdateLastRequest(int customerId, int clientId, ClientStatusCode statusCode, string statusMessage, string ipAddress, out ClientEntity clientEntity, string version)
        {
            clientEntity = null;
            if (clientId > 0)
            {
                // Already prefetch the client for efficient ClientStat logic.
                clientEntity = new ClientEntity(clientId);
                if (clientEntity == null || clientEntity.IsNew)
                    throw new ObymobiException(UpdateLastClientRequestResult.ClientUnknown, "ClientId: {0}", clientId);
            }
            UpdateLastRequest(customerId, clientEntity, statusCode, statusMessage, ipAddress, version);
        }

        /// <summary>
        /// Updates the datetime value of the last request for the specified client
        /// </summary>
        /// <param name="customerId">The id of the customer</param>
        /// <param name="clientId">The id of the client</param>
        /// <param name="statusCode">The status code of the client</param>
        /// <param name="statusMessage">The status message of the client</param>
        /// <param name="ipAddress">The ip address.</param>        
        /// <param name="version">The version.</param>
        public static void UpdateLastRequest(int customerId, ClientEntity clientEntity, ClientStatusCode statusCode, string statusMessage, string ipAddress, string version)
        {
            if (clientEntity == null)
                return;
            
            if (!clientEntity.DeviceId.HasValue)
                return;

            if (clientEntity.DeviceEntity.LastRequestUTC.HasValue && clientEntity.DeviceEntity.LastRequestUTC.Value > DateTime.UtcNow)
                return;

            clientEntity.LastStatus = (int)statusCode;
            clientEntity.LastStatusText = statusCode.ToString();
            //clientEntity.LastStatusMessage = statusMessage;

            // Client state
            if (!clientEntity.LastStateOnline.GetValueOrDefault(false)) // LastStateOnline is null or false
            {
                // Update the client state to online
                ClientStateHelper.UpdateClientState(clientEntity, true);
            }
                
            // Save entity through database queue
            clientEntity.CanQueue = true;
            clientEntity.Save();

            DeviceEntity deviceEntity = clientEntity.DeviceEntity;
            deviceEntity.LastRequestUTC = DateTime.UtcNow;

            // Version
            if (!version.IsNullOrWhiteSpace())
                deviceEntity.ApplicationVersion = version;

            // IP address
            if (!ipAddress.IsNullOrWhiteSpace())
                deviceEntity.PrivateIpAddresses = ipAddress;

            deviceEntity.Save();
        }

        /// <summary>
        /// Retrieve offline clients
        /// </summary>
        /// <param name="maxMinutesSinceLastCall">Treshold when to mark a client as online</param>		
        /// <returns>An Obymobi.Data.CollectionClasses.ClientCollection instance</returns>
        public static ClientCollection GetOnlineClients(int maxMinutesSinceLastCall)
        {
            ClientCollection onlineClients = new ClientCollection();

            RelationCollection relation = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            // Get all offline terminals			
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddMinutes(-1 * maxMinutesSinceLastCall));
            onlineClients.GetMulti(filter, relation);

            return onlineClients;
        }

        /// <summary>
        /// Retrieve offline clients
        /// </summary>
        /// <param name="maxMinutesSinceLastCall">Treshold when to mark a client as offline</param>
        /// <param name="onlyWhenInBusinessHours">When set to true, only clients that are offline while the company is in business hours are returned</param>
        /// <returns>An Obymobi.Data.CollectionClasses.ClientCollection instance</returns>
        public static ClientCollection GetOfflineClients(int maxMinutesSinceLastCall, bool onlyWhenInBusinessHours, int? companyId = null)
        {
            ClientCollection offlineClients = new ClientCollection();

            RelationCollection relation = new RelationCollection(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            // Get all offline terminals
            ClientCollection clients = new ClientCollection();
            PredicateExpression filter = new PredicateExpression();            
            filter.Add(DeviceFields.LastRequestUTC < DateTime.UtcNow.AddMinutes(-1 * maxMinutesSinceLastCall));

            if (companyId.HasValue)
            {
                filter.Add(ClientFields.CompanyId == companyId);
            }

            clients.GetMulti(filter, relation);
            
            // GK I did this method and it's far from perfect, also because it did a IsCompanyInBusinessHours for each 
            // client instead of caching it. Also it should have been done with 1 fixed date time. So for now I 
            // just improved it a little, not a lot.

            Dictionary<int, bool> companyInBusinessHoursCache = new Dictionary<int, bool>();

            if (onlyWhenInBusinessHours)
            {
                for (int i = 0; i < clients.Count; i++)
                {
                    ClientEntity client = clients[i];

                    bool inBusinessHours;
                    if (!companyInBusinessHoursCache.TryGetValue(client.CompanyId, out inBusinessHours))
                    {
                        inBusinessHours = CompanyHelper.IsCompanyInBusinessHours(client.CompanyId);
                        companyInBusinessHoursCache.Add(client.CompanyId, inBusinessHours);
                    }

                    if (inBusinessHours)
                        offlineClients.Add(client);
                }
            }
            else
                offlineClients = clients;

            return offlineClients;
        }

        public static SLDocument GetOfflineClientsReport(int maxMinutesSinceLastCall, int companyId, List<int> deliverypointGroupIds)
        {
            SLDocument report = new SLDocument();            

            // Fetch company with DPGs preloaded (and filtered if specified)
            PrefetchPath path = new PrefetchPath(EntityType.CompanyEntity);
            PredicateExpression filterDpgs = new PredicateExpression();
            if(deliverypointGroupIds != null && deliverypointGroupIds.Count > 0)
                filterDpgs.Add(DeliverypointgroupFields.DeliverypointgroupId == deliverypointGroupIds);
            path.Add(CompanyEntity.PrefetchPathDeliverypointgroupCollection, 0, filterDpgs);

            var company = new CompanyEntity(companyId, path);

            // Device prefetch
            PrefetchPath devicePrefetch = new PrefetchPath(EntityType.ClientEntity);
            devicePrefetch.Add(ClientEntityBase.PrefetchPathDeviceEntity, new IncludeFieldsList {DeviceFields.Identifier, DeviceFields.LastRequestUTC, DeviceFields.BatteryLevel, DeviceFields.WifiStrength});

            RelationCollection relation = new RelationCollection();
            relation.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            // Worksheet per Deliverypointgroup
            DateTime utcNow = DateTime.UtcNow;
            bool firstWorksheet = true;
            PrefetchPath pathClient = new PrefetchPath(EntityType.ClientEntity);
            pathClient.Add(ClientEntity.PrefetchPathDeliverypointEntity);
            List<string> dpgNames = new List<string>();
            var dpgsSorted = company.DeliverypointgroupCollection.OrderBy(x => x.Name);
            foreach (DeliverypointgroupEntity dpg in dpgsSorted)
            {
                if (dpg.ClientCollection.Count <= 0)
                    continue;

                string dpgName = dpg.Name;
                if (!dpgNames.Contains(dpgName.ToLowerInvariant()))
                {
                    dpgNames.Add(dpgName.ToLowerInvariant());
                }
                else
                { 
                    // Already used that name (Excel will break on that)
                    dpgName = dpgName.Truncate(28);
                    dpgName += "-" + dpgNames.Count;
                }

                string dpCaption = dpg.DeliverypointCaption.IsNullOrWhiteSpace() ? "Room" : dpg.DeliverypointCaption;

                // Retrieve the Offline Clients for this company ordered by Deliverpointnumber
                ClientCollection clients = new ClientCollection();
                PredicateExpression filterClients = new PredicateExpression();
                filterClients.Add(ClientFields.DeliverypointGroupId == dpg.DeliverypointgroupId);
                filterClients.Add(DeviceFields.LastRequestUTC < utcNow.AddMinutes(-1 * maxMinutesSinceLastCall));
                clients.GetMulti(filterClients, 0, null, relation, devicePrefetch);

                ClientHelper.GetOfflineClientsReportWorksheet(report, company, utcNow, dpgName, dpCaption, ref firstWorksheet, clients);
            }

            // Worksheet with orphan devices     
            ClientCollection orphanClients = new ClientCollection();
            PredicateExpression filterOrphanClients = new PredicateExpression();
            filterOrphanClients.Add(ClientFields.DeliverypointGroupId == DBNull.Value);
            filterOrphanClients.Add(DeviceFields.LastRequestUTC < utcNow.AddMinutes(-1 * maxMinutesSinceLastCall));
            orphanClients.GetMulti(filterOrphanClients, 0, null, relation, devicePrefetch);
            
            if(orphanClients.Count > 0)
                ClientHelper.GetOfflineClientsReportWorksheet(report, company, utcNow, "Non-configured", "Room", ref firstWorksheet, orphanClients);                    

            report.SetPageSizeOfWorksheets(SLPaperSizeValues.A4Paper);
            
            return report;
            
        }

        private static void GetOfflineClientsReportWorksheet(SLDocument report, CompanyEntity company, DateTime utcNow, string dpgName, string dpCaption, ref bool firstWorksheet, ClientCollection clients)
        {
            if (firstWorksheet)
            {                    
                report.RenameWorksheet(SLDocument.DefaultFirstSheetName, dpgName);
                firstWorksheet = false;
            }
            else
                report.AddWorksheet(dpgName);

            // Create the fixed header row 
            // MacAddress | Deliverypoint (Caption) | Last seen | Offline time | Battery Level | Wifi Strength
            report.SetCellValue("A1", "Mac Address");
            report.SetCellValue("B1", dpCaption);
            report.SetCellValue("C1", "Last Online");
            report.SetCellValue("D1", "Offline period");
            report.SetCellValue("E1", "Battery Level");
            report.SetCellValue("F1", "Wifi Strength");

            report.SetColumnWidth("A", 16.2);
            report.SetColumnWidth("B", 10.2);
            report.SetColumnWidth("C", 18.2);
            report.SetColumnWidth("D", 13.2);
            report.SetColumnWidth("E", 12.2);
            report.SetColumnWidth("F", 12.2);

            report.SetStyle("A1", "A1", fontStyle: System.Drawing.FontStyle.Bold, borders: Borders.Bottom, borderColor: Color.Black);
            report.SetStyle("B1", "F1", fontStyle: System.Drawing.FontStyle.Bold, horizontalAlignment: DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Right, borders: Borders.Bottom, borderColor: Color.Black);            

            var clientsOrdered = clients.OrderBy(c => c.DeliverypointEntity.NumberAsInteger);

            // Render a line for each client
            int rowIndex = 2; // First was used by the header
            foreach (ClientEntity client in clientsOrdered)
            {
                string dpName;
                if (client.DeliverypointEntity.Name.Equals(client.DeliverypointEntity.Number))
                    dpName = client.DeliverypointEntity.Number;
                else if (!client.DeliverypointEntity.Name.IsNullOrWhiteSpace())
                    dpName = "{0} ({1})".FormatSafe(client.DeliverypointEntity.Name, client.DeliverypointEntity.Number);
                else
                    dpName = client.DeliverypointEntity.Number;

                // GK Since we don't have proper support yet for timezones, it's hacked here.
                DateTime? lastActivity = client.DeviceEntity.LastRequestUTC;
                string lastActivityString = "Never";
                string elapsedTimeString = "";
                if (lastActivity != null)
                {
                    if (company.CountryEntity.Code == "GB")
                        lastActivity = lastActivity.Value.AddHours(-1);

                    lastActivityString = lastActivity.ToString();
                    elapsedTimeString = DateTimeUtil.GetElapsedTimeString(lastActivity.Value, utcNow);
                }

                report.SetCellValue("A" + rowIndex, "'" + client.MacAddress);
                report.SetCellValue("B" + rowIndex, "'" + dpName);
                report.SetCellValue("C" + rowIndex, "'" + lastActivityString);
                report.SetCellValue("D" + rowIndex, "'" + elapsedTimeString);
                report.SetCellValue("E" + rowIndex, "'" + client.DeviceEntity.BatteryLevel.ToString());
                report.SetCellValue("F" + rowIndex, "'" + client.DeviceEntity.WifiStrength.ToString() + "dB");

                rowIndex++;

                report.FreezePanes(1, 0);
            }

            string rowIndexString = "F" + rowIndex.ToString();
            report.SetStyle("B2", rowIndexString, horizontalAlignment: DocumentFormat.OpenXml.Spreadsheet.HorizontalAlignmentValues.Right);

            // Auto fit columns
            report.AutoFitColumn(1, 6);

            // Fit on 1 page width, 2 pages height.
            SLPageSettings pageSettings = report.GetPageSettings();
            pageSettings.SetNarrowMargins();
            pageSettings.ScalePage(1, 5);
            report.SetPageSettings(pageSettings);
        }

        /// <summary>
        /// Checks if a out of charge notification is needed to be send to the console for a specific client
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="isCharging">if set to <c>true</c> [is charging].</param>
        /// <returns></returns>
        public static bool getBatteryNotificationNeeded(ClientEntity client, bool isCharging)
        {
            bool toReturn = false;

            // Check if the client isn't already charging
            if (!isCharging)
            {
                // Check if the battery level is beneath the outOfCharge level
                DeliverypointgroupEntity deliverypointgroup = client.DeliverypointgroupEntity;
                if (deliverypointgroup != null && deliverypointgroup.OutOfChargeLevel > 0)
                {
                    if (client.DeviceEntity.BatteryLevel <= deliverypointgroup.OutOfChargeLevel && client.OutOfChargeNotificationsSent < deliverypointgroup.OutOfChargeNotificationAmount)
                    {
                        // Battery level is below the notification amount
                        toReturn = true;
                    }
                    else if (client.DeviceEntity.BatteryLevel > deliverypointgroup.OutOfChargeLevel && client.OutOfChargeNotificationsSent > 0)
                    {
                        // Battery level is above the notification amount
                        client.OutOfChargeNotificationsSent = 0;
                        client.Save();
                    }
                }
            }

            return toReturn;
        }

        /// <summary>
        /// Processes the print notification for Request service and Console
        /// </summary>
        /// <param name="client">The client.</param>
        /// <param name="filename">The name of the file that needs to be printed.</param>
        public static bool WritePrintNotification(ClientEntity client, String filename)
        {
            Orderitem[] orderitems = new Orderitem[0];

            // Create an order
            Order order = new Order();
            order.Guid = Guid.NewGuid().ToString();
            order.Type = (int)OrderType.RequestForPrint;

            // Add the array to the order
            order.Orderitems = orderitems;
            order.CompanyId = client.CompanyId;
            order.CustomerId = 0;
            order.ClientId = client.ClientId;
            order.AgeVerificationType = 100;
            order.BenchmarkInformation = "None";
            order.Notes = "Filename: " + filename;

            if (client.DeliverypointEntity != null)
            {
                int deliverypointNumber;
                if (int.TryParse(client.DeliverypointEntity.Number, out deliverypointNumber))
                {
                    order.DeliverypointNumber = deliverypointNumber;
                }
                else
                {
                    order.DeliverypointNumber = 0;
                }
            }

            // Create an order result object
            ObyTypedResult<Order> orderResult = new ObyTypedResult<Order>();

            // Save the order for the print notification
            OrderProcessingHelper.CreateNewOrder(null, client, order, ref orderResult, false);

            return true;
        }

        /// <summary>
        /// Processes the wake up notification
        /// </summary>
        /// <param name="client">The client</param>
        public static bool WriteWakeUpNotification(ClientEntity client)
        {
            Orderitem[] orderitems = new Orderitem[0];

            // Create an order
            Order order = new Order();
            order.Guid = Guid.NewGuid().ToString();
            order.Type = (int)OrderType.RequestForWakeUp;

            // Add the array to the order
            order.Orderitems = orderitems;
            order.CompanyId = client.CompanyId;
            order.CustomerId = 0;
            order.ClientId = client.ClientId;
            order.AgeVerificationType = 100;
            order.BenchmarkInformation = "None";
            
            DateTime convertedDateTime = client.WakeUpTimeUtc.Value.UtcToLocalTime(client.CompanyEntity.TimeZoneInfo);
            order.Notes = "Wake up time: " + convertedDateTime.ToString("dd-MM-yyyy HH:mm:ss");

            if (client.DeliverypointEntity != null)
            {
                int deliverypointNumber;
                if (int.TryParse(client.DeliverypointEntity.Number, out deliverypointNumber))
                {
                    order.DeliverypointId = client.DeliverypointEntity.DeliverypointId;
                    order.DeliverypointNumber = deliverypointNumber;
                }
                else
                {
                    order.DeliverypointNumber = 0;
                }
            }

            // Create an order result object
            ObyTypedResult<Order> orderResult = new ObyTypedResult<Order>();

            // Save the order for the wake up notification
            OrderProcessingHelper.CreateNewOrder(null, client, order, ref orderResult, false);

            return true;
        }
    }
}
