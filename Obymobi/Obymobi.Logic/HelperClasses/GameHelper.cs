﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.HelperClasses
{
    public class GameHelper
    {
        public static GameEntity GetGameEntity(string url)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(GameFields.Url == url);

            GameCollection gameCollection = EntityCollection.GetMulti<GameCollection>(filter, null, null);

            if (gameCollection.Count == 0)
                throw new InvalidOperationException(string.Format("No game could be found for url '{0}'!", url));
            else if (gameCollection.Count > 1)
                throw new InvalidOperationException(string.Format("Multiple games were found for url '{0}'!", url));

            return gameCollection[0];
        }
    }
}
