﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// EntertainmentHelper class
    /// </summary>
    public class EntertainmentHelper
    {
        /// <summary>
        /// Gets an array of Obymobi.Logic.Model.Entertainmentcategory instances using the specified company id or deliverypointgroup id
        /// </summary>
        /// <param name="companyId">The id of the company</param>
        /// <param name="deliverypointgroupId">The id of the deliverypointgroup</param>
        /// <returns>An array of Entertainmentcategory instances</returns>
        public static Entertainmentcategory[] GetEntertainmentcategories(int companyId, int deliverypointgroupId)
        {
            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();

            // Create the relation collection
            RelationCollection joins = new RelationCollection();

            // Only use visible entertainment entries
            filter.Add(EntertainmentFields.Visible == true);

            // Add Company Filter
            if (companyId > 0)
            {
                filter.Add(CompanyEntertainmentFields.CompanyId == companyId);

                joins.Add(EntertainmentEntityBase.Relations.CompanyEntertainmentEntityUsingEntertainmentId);
            }

            // Add Deliverypointgroup filter
            if (deliverypointgroupId > 0)
            {
                filter.Add(DeliverypointgroupEntertainmentFields.DeliverypointgroupId == deliverypointgroupId);

                joins.Add(EntertainmentEntityBase.Relations.DeliverypointgroupEntertainmentEntityUsingEntertainmentId);
            }

            // Create and initialize a EntertainmentCollection instance and retrieve the items using the filter
            joins.Add(EntertainmentcategoryEntityBase.Relations.EntertainmentEntityUsingEntertainmentcategoryId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.EntertainmentcategoryEntity);
            prefetch.Add(EntertainmentcategoryEntityBase.PrefetchPathCustomTextCollection);

            EntertainmentcategoryCollection entertainmentcategoryCollection = new EntertainmentcategoryCollection();
            entertainmentcategoryCollection.GetMulti(filter, 0, null, joins, prefetch);

            // Initialize the array
            List<Entertainmentcategory> entertainmentcategories = new List<Entertainmentcategory>();
            for (int i = 0; i < entertainmentcategoryCollection.Count; i++)
            {
                EntertainmentcategoryEntity entertainmentcategoryEntity = entertainmentcategoryCollection[i];
                entertainmentcategories.Add(EntertainmentHelper.CreateEntertainmentcategoryModelFromEntity(entertainmentcategoryEntity));
            }

            // Convert to Array
            Entertainmentcategory[] entertainmentcategoryArray = new Entertainmentcategory[entertainmentcategories.Count];
            entertainmentcategories.CopyTo(entertainmentcategoryArray);

            return entertainmentcategoryArray;
        }

        /// <summary>
        /// Creates a Obymobi.Logic.Model.Entertainmentcategory instance using the specified Obymobi.Data.EntityClasses.EntertainmentcategoryEntity instance
        /// </summary>
        /// <param name="entertainmentcategoryEntity">The EntertainmentcategoryEntity instance to create the Entertainmentcategory instance for</param>
        /// <returns>An Entertainmentcategory instance</returns>
        public static Entertainmentcategory CreateEntertainmentcategoryModelFromEntity(EntertainmentcategoryEntity entertainmentcategoryEntity)
        {
            Entertainmentcategory entertainmentcategory = new Entertainmentcategory();
            entertainmentcategory.EntertainmentcategoryId = entertainmentcategoryEntity.EntertainmentcategoryId;
            entertainmentcategory.Name = entertainmentcategoryEntity.Name;
            entertainmentcategory.CustomTexts = CustomTextHelper.ConvertCustomTextCollectionToArray(entertainmentcategoryEntity.CustomTextCollection);

            return entertainmentcategory;
        }
    }
}
