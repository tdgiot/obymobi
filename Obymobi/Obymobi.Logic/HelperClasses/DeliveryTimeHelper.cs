﻿using System.Collections.Generic;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    public class DeliveryTimeHelper
    {
        public static List<DeliveryTime> GetDeliveryTimesForCompany(int companyId)
        {
            if (companyId > 0)
            {
                PredicateExpression filter = new PredicateExpression(DeliverypointgroupFields.CompanyId == companyId);
                DeliverypointgroupCollection deliverypointgroupCollection = EntityCollection.GetMulti<DeliverypointgroupCollection>(filter, null, DeliverypointgroupFields.EstimatedDeliveryTime);

                List<DeliveryTime> deliveryTimes = new List<DeliveryTime>();
                foreach (DeliverypointgroupEntity deliverypointgroupEntity in deliverypointgroupCollection)
                {
                    deliveryTimes.Add(DeliveryTimeHelper.CreateDeliveryTimeModelFromDeliverypointgroupEntity(deliverypointgroupEntity));
                }
                return deliveryTimes;
            }
            return new List<DeliveryTime>();
        }

        public static DeliveryTime CreateDeliveryTimeModelFromDeliverypointgroupEntity(DeliverypointgroupEntity deliverypointgroupEntity)
        {
            DeliveryTime deliveryTime = new DeliveryTime();
            deliveryTime.DeliverypointgroupId = deliverypointgroupEntity.DeliverypointgroupId;
            deliveryTime.Time = deliverypointgroupEntity.EstimatedDeliveryTime;

            return deliveryTime;
        }
    }
}
