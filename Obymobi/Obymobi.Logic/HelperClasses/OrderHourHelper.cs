﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// OrderHourHelper class
    /// </summary>
    public static class OrderHourHelper
    {
        /// <summary>
        /// Set the TimeStart of the OrderHour based on a DateTime containing the time
        /// </summary>
        /// <param name="orderHour"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public static string TimeStartFromDateTime(this OrderHourEntity orderHour, DateTime time)
        {
            string toReturn = time.ToString("HHmm");
            orderHour.TimeStart = toReturn;
            return toReturn;
        }


        /// <summary>
        /// Set the TimeEnd of the OrderHour based on a DateTime containing the time
        /// </summary>
        /// <param name="orderHour"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public static string TimeEndFromDateTime(this OrderHourEntity orderHour, DateTime time)
        {
            string toReturn = time.ToString("HHmm");
            orderHour.TimeEnd = toReturn;
            return toReturn;
        }


        /// <summary>
        /// Returns a DateTime instance for 01-01-2000 with the Start Time of the Order Hour
        /// </summary>
        /// <param name="orderHour"></param>
        /// <returns></returns>
        public static DateTime TimeStartAsDateTime(this OrderHourEntity orderHour)
        {
            return OrderHourHelper.GetDateTimeForTimeString(orderHour.TimeStart);
        }

        /// <summary>
        /// Returns a DateTime instance for 01-01-2000 with the End Time of the Order Hour
        /// </summary>
        /// <param name="orderHour"></param>
        /// <returns></returns>
        public static DateTime TimeEndAsDateTime(this OrderHourEntity orderHour)
        {
            return OrderHourHelper.GetDateTimeForTimeString(orderHour.TimeEnd);
        }

        /// <summary>
        /// Gets the date time for time string.
        /// </summary>
        /// <param name="time">The time.</param>
        /// <returns></returns>
        public static DateTime GetDateTimeForTimeString(string time)
        {
            int hours = Convert.ToInt32(time.Substring(0, 2));
            int minutes = Convert.ToInt32(time.Substring(2, 2));

            DateTime toReturn = new DateTime(2000, 1, 1, hours, minutes, 0);

            return toReturn;
        }
    }
}
