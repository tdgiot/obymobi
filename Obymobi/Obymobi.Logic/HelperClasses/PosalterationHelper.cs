﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos;
using Dionysos.Data.LLBLGen;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// PosalterationHelper class
    /// </summary>
    public class PosalterationHelper
    {
        /// <summary>
        /// Synchronizes the alterations from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="posalterations">The posalterations.</param>
        /// <param name="companyId">The id of the company to import the pos alterations for</param>
        /// <param name="batchId">The batch id.</param>
        public static void SynchronizePosalterations(Posalteration[] posalterations, int companyId, long batchId, string revenueCenter = null)
        {
            // Get a PosalterationCollection for the specified company
            PosalterationCollection posalterationCollection = GetPosalterationCollection(companyId);

            // Create an EntityView for the PosalterationCollection
            EntityView<PosalterationEntity> posalterationView = posalterationCollection.DefaultView;

            // Populate the list of external id's
            var query = from c in posalterationView select c.ExternalId;
            List<string> posalterationExternalIdList = query.ToList<string>();

            // Create a filter
            PredicateExpression filter = null;

            // Walk through the Posalteration instances of the array
            for (int i = 0; i < posalterations.Length; i++)
            {
                Posalteration posalteration = posalterations[i];

                // Initialize the filter
                filter = new PredicateExpression();
                filter.Add(PosalterationFields.ExternalId == posalteration.ExternalId);
                filter.Add(PosalterationFields.CompanyId == companyId);

                if (!string.IsNullOrWhiteSpace(revenueCenter))
                {
                    filter.Add(PosalterationFields.RevenueCenter == revenueCenter);
                }

                // Set the filter to the EntityView
                posalterationView.Filter = filter;

                // Check whether items have been found
                PosalterationEntity posalterationEntity = null;
                posalteration.CompanyId = companyId;
                if (posalterationView.Count == 0)
                {
                    // First, set the company id                    
                }
                else if (posalterationView.Count == 1)
                {
                    // Pos alteration found in database, update
                    posalterationEntity = posalterationView[0];
                }
                else if (posalterationView.Count > 1)
                {
                    // Multiple pos alterations found in database for the specified external id, error!
                    throw new ObymobiException(ImportPosalterationsResult.MultiplePosalterationsFound, "Multiple pos alterations found for the specified external id '{0}'.", posalteration.ExternalId);
                }

                // Update Fields
                posalterationEntity = CreateUpdatePosalterationEntityFromModel(posalteration, posalterationEntity);

                try
                {
                    bool wasNew = posalterationEntity.IsNew;

                    // Set Created or Updated batch to this batch.
                    if (wasNew)
                        posalterationEntity.CreatedInBatchId = batchId;
                    else if (posalterationEntity.IsDirty)
                        posalterationEntity.UpdatedInBatchId = batchId;

                    // Make this backwards compatible by filling it anyway.
                    if (!posalterationEntity.CreatedInBatchId.HasValue)
                        posalterationEntity.CreatedInBatchId = batchId;

                    posalterationEntity.SynchronisationBatchId = batchId;

                    if (posalterationEntity.Save())
                    {
                        if (wasNew)
                        {
                            posalterationEntity.Refetch();
                            posalterationView.RelatedCollection.Add(posalterationEntity);
                        }
                    }
                }
                catch
                {
                    // Do not throw an exception here because it will stop the whole sync batch
                }

                // Synchronize the alteration options
                if (posalteration.Posalterationoptions != null && posalteration.Posalterationoptions.Length > 0)
                {
                    PosalterationoptionHelper.SynchronizePosalterationoptions(posalteration.Posalterationoptions, companyId, batchId);
                    PosalterationitemHelper.SynchronizePosalterationitems(posalteration, companyId, batchId);
                }
            }


        }

        /// <summary>
        /// Gets the external id of the pos alteration for the specified alteration id
        /// </summary>
        /// <param name="alterationId">The id of the alteration to get the pos alteration id for</param>
        /// <returns>A System.String instance containing the external id</returns>
        public static string GetPosalterationExternalId(int alterationId)
        {
            string externalId = string.Empty;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationFields.AlterationId == alterationId);

            RelationCollection relations = new RelationCollection();
            relations.Add(AlterationEntity.Relations.PosalterationEntityUsingPosalterationId);

            PosalterationCollection posalterationCollection = new PosalterationCollection();
            posalterationCollection.GetMulti(filter, relations);

            if (posalterationCollection.Count == 0)
            {
                // No pos alteration found, error
                throw new ObymobiException(GetPosalterationExternalIdResult.NoPosalterationFound, "No pos alterations found for alteration id '{0}'.", alterationId);
            }
            else if (posalterationCollection.Count == 1)
            {
                // Pos alteration found
                externalId = posalterationCollection[0].ExternalId;
            }
            else if (posalterationCollection.Count > 1)
            {
                // Multiple pos alterations found, error
                throw new ObymobiException(GetPosalterationExternalIdResult.MultiplePosalterationsFound, "Multiple pos alterations found for alteration id '{0}'.", alterationId);
            }

            return externalId;
        }

        /// <summary>
        /// Gets the posalterations for posproduct.
        /// </summary>
        /// <param name="posproductEntity">The posproduct entity.</param>
        /// <returns></returns>
        public static PosalterationCollection GetPosalterationsForPosproduct(PosproductEntity posproductEntity)
        {
            PosproductPosalterationCollection posproductPosalterations = PosproductPosalterationHelper.GetPosproductPosalterationsForPosproduct(posproductEntity);

            PosalterationCollection posalterationCollection = new PosalterationCollection();

            var posaterationExternalIds = from pppa in posproductPosalterations select pppa.PosalterationExternalId;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationFields.ExternalId == posaterationExternalIds);

            posalterationCollection.GetMulti(filter);

            return posalterationCollection;
        }

        /// <summary>
        /// Gets the alteration by posalteration id or a new one if none was found.
        /// </summary>
        /// <param name="posalterationId">The posalteration id.</param>
        /// <param name="alterationView">The alteration view.</param>
        /// <returns>
        /// A (new) <see cref="AlterationEntity"/> instance
        /// </returns>
        public static AlterationEntity GetAlterationByPosalterationId(int posalterationId, EntityView<AlterationEntity> alterationView)
        {
            // Create the AlterationEntity instance
            AlterationEntity alteration = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationFields.PosalterationId == posalterationId);

            // Set the filter to the alteration view
            alterationView.Filter = filter;

            // Check whether a alteration was found
            if (alterationView.Count == 0)
                alteration = new AlterationEntity();
            else if (alterationView.Count == 1)
                alteration = alterationView[0];
            else if (alterationView.Count > 1)
                throw new ObymobiException(SynchronizeAlterationsResult.MultipleAlterationsFound, "Multiple alterations found for the pos alteration id '{0}'.", posalterationId);

            return alteration;
        }

        /// <summary>
        /// Gets the posalteration by external id.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="posalterationExternalId">The posalteration external id.</param>
        /// <param name="transaction">The transaction.</param>
        /// <returns>
        /// Null if not found
        /// </returns>
        public static PosalterationEntity GetPosalterationByExternalId(int companyId, string posalterationExternalId, ITransaction transaction)
        {
            PredicateExpression filterPosalteration = new PredicateExpression();
            filterPosalteration.Add(PosalterationFields.ExternalId == posalterationExternalId);
            filterPosalteration.Add(PosalterationFields.CompanyId == companyId);

            PosalterationCollection posalterations = new PosalterationCollection();
            if (transaction != null)
                transaction.Add(posalterations);

            return LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<PosalterationEntity>(filterPosalteration, posalterations);
        }

        /// <summary>
        /// Gets the posalteration by external id or a new one if none was found.
        /// </summary>
        /// <param name="externalId">The external id.</param>
        /// <param name="posalterationView">The posalteration view.</param>
        /// <returns>
        /// A (new) <see cref="PosalterationEntity"/> instance
        /// </returns>
        public static PosalterationEntity GetPosalterationByExternalId(string externalId, EntityView<PosalterationEntity> posalterationView)
        {
            // Create the PosalterationEntity instance
            PosalterationEntity posalteration = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationFields.ExternalId == externalId);

            // Set the filter to the POS alteration view
            posalterationView.Filter = filter;

            // Check whether a POS alteration was found
            if (posalterationView.Count == 0)
                posalteration = new PosalterationEntity();
            else if (posalterationView.Count == 1)
                posalteration = posalterationView[0];
            else if (posalterationView.Count > 1)
                throw new ObymobiException(ImportPosalterationsResult.MultiplePosalterationsFound, "Multiple pos alterations found for external id '{0}'.", externalId);

            return posalteration;
        }

        /// <summary>
        /// Gets a PosalterationCollection for the specified company id
        /// </summary>
        /// <param name="companyId">The id of the company to get the PosalterationCollection for</param>
        /// <returns>An Obymobi.Data.PosalterationCollection instance</returns>
        public static PosalterationCollection GetPosalterationCollection(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationFields.CompanyId == companyId);

            PosalterationCollection posalterationCollection = new PosalterationCollection();
            posalterationCollection.GetMulti(filter);

            return posalterationCollection;
        }


        /// <summary>
        /// Creates / Updates a PosalterationEntity instance for the specified Posalteration instance
        /// </summary>
        /// <param name="posalteration">The Posalteration instance to create the PosalterationEntity instance for</param>
        /// <param name="posalterationEntity">The PosalterationEntity entity to be updated / created.</param>
        /// <returns>
        /// A new PosalterationEntity, or the updated supplied one.
        /// </returns>
        public static PosalterationEntity CreateUpdatePosalterationEntityFromModel(Posalteration posalteration, PosalterationEntity posalterationEntity)
        {
            if (posalterationEntity == null)
                posalterationEntity = new PosalterationEntity();

            posalterationEntity.CompanyId = posalteration.CompanyId;
            posalterationEntity.ExternalId = posalteration.ExternalId;
            posalterationEntity.Name = posalteration.Name;
            if (posalteration.MinOptions.HasValue)
                posalterationEntity.MinOptions = posalteration.MinOptions.Value;
            if (posalteration.MaxOptions.HasValue)
                posalterationEntity.MaxOptions = posalteration.MaxOptions.Value;

            posalterationEntity.FieldValue1 = posalteration.FieldValue1;
            posalterationEntity.FieldValue2 = posalteration.FieldValue2;
            posalterationEntity.FieldValue3 = posalteration.FieldValue3;
            posalterationEntity.FieldValue4 = posalteration.FieldValue4;
            posalterationEntity.FieldValue5 = posalteration.FieldValue5;
            posalterationEntity.FieldValue6 = posalteration.FieldValue6;
            posalterationEntity.FieldValue7 = posalteration.FieldValue7;
            posalterationEntity.FieldValue8 = posalteration.FieldValue8;
            posalterationEntity.FieldValue9 = posalteration.FieldValue8;
            posalterationEntity.FieldValue10 = posalteration.FieldValue10;

            posalterationEntity.RevenueCenter = posalteration.RevenueCenter;

            return posalterationEntity;
        }


        /// <summary>
        /// Creates the posalteration model from entity.
        /// </summary>
        /// <param name="posalterationEntity">The posalteration entity.</param>
        /// <param name="addNestedModels">if set to <c>true</c> [add nested models].</param>
        /// <returns></returns>
        public static Posalteration CreatePosalterationModelFromEntity(PosalterationEntity posalterationEntity, bool addNestedModels)
        {
            Posalteration posalterationModel = new Posalteration();

            posalterationModel.PosalterationId = posalterationEntity.PosalterationId;
            posalterationModel.CompanyId = posalterationEntity.CompanyId;
            posalterationModel.ExternalId = posalterationEntity.ExternalId;
            posalterationModel.Name = posalterationEntity.Name;
            if (posalterationEntity.MinOptions.HasValue)
                posalterationModel.MinOptions = posalterationEntity.MinOptions.Value;
            if (posalterationEntity.MaxOptions.HasValue)
                posalterationModel.MaxOptions = posalterationEntity.MaxOptions.Value;

            posalterationModel.FieldValue1 = posalterationEntity.FieldValue1;
            posalterationModel.FieldValue2 = posalterationEntity.FieldValue2;
            posalterationModel.FieldValue3 = posalterationEntity.FieldValue3;
            posalterationModel.FieldValue4 = posalterationEntity.FieldValue4;
            posalterationModel.FieldValue5 = posalterationEntity.FieldValue5;
            posalterationModel.FieldValue6 = posalterationEntity.FieldValue6;
            posalterationModel.FieldValue7 = posalterationEntity.FieldValue7;
            posalterationModel.FieldValue8 = posalterationEntity.FieldValue8;
            posalterationModel.FieldValue9 = posalterationEntity.FieldValue9;
            posalterationModel.FieldValue10 = posalterationEntity.FieldValue10;

            posalterationModel.RevenueCenter = posalterationEntity.RevenueCenter;

            // Get all related PosalterationItems
            if (addNestedModels)
            {
                var posalterationitemEntities = PosalterationitemHelper.GetPosalterationitems(posalterationEntity);
                List<Posalterationitem> posalterationItems = new List<Posalterationitem>();

                foreach (var posalterationitemEntity in posalterationitemEntities)
                {
                    posalterationItems.Add(PosalterationitemHelper.CreatePosalterationitemModelFromEntity(posalterationitemEntity));
                }

                posalterationModel.Posalterationitems = posalterationItems.ToArray();

                // Get all related Posalterationoptions                                                                        
                var posalterationoptionEntities = PosalterationoptionHelper.GetPosalterationoptions(posalterationitemEntities);
                List<Posalterationoption> posalterationoptions = new List<Posalterationoption>();

                foreach (var posalterationoptionEntity in posalterationoptionEntities)
                {
                    posalterationoptions.Add(PosalterationoptionHelper.CreatePosalterationoptionModelFromEntity(posalterationoptionEntity));
                }

                posalterationModel.Posalterationoptions = posalterationoptions.ToArray();
            }

            return posalterationModel;
        }
    }
}
