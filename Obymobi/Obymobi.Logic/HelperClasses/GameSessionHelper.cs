﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.HelperClasses
{
    public static class GameSessionHelper
    {
        public static void RegisterGameSession(ClientEntity clientEntity, string url)
        {
            // Skip devices without a deliverypoint since we can not save without a valid room
            if (!clientEntity.DeliverypointId.HasValue)
                return;

            // Get the game based on the url
            GameEntity gameEntity = GameHelper.GetGameEntity(url);
            if (gameEntity == null)
                return;

            // Get the latest game session for the client and game
            GameSessionEntity gameSessionEntity = GetOrCreateGameSessionEntity(clientEntity, gameEntity);
            if (gameSessionEntity == null)
                return;

            // Update the game session
            gameSessionEntity.EndUTC = DateTime.UtcNow;
            gameSessionEntity.Length = (int)(gameSessionEntity.EndUTC - gameSessionEntity.StartUTC).TotalMinutes;
            gameSessionEntity.Save();
        }

        public static List<GameSessionEntity> GetGameSessions(int companyId, DateTime start, DateTime end)
        {
            List<GameSessionEntity> gameSessions = new List<GameSessionEntity>();

            PredicateExpression filter;
            RelationCollection relations;
            GameSessionCollection gameSessionCollection;

            #region Sessions that were already started before the start of the interval

            filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(GameSessionFields.StartUTC < start);
            filter.Add(GameSessionFields.EndUTC > start);
            filter.Add(GameSessionFields.EndUTC < end);

            relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.GameSessionEntityUsingClientId);

            gameSessionCollection = new GameSessionCollection();
            gameSessionCollection.GetMulti(filter, relations);

            foreach (GameSessionEntity gameSessionEntity in gameSessionCollection)
            {
                gameSessionEntity.StartUTC = start;
                gameSessions.Add(gameSessionEntity);
            }

            #endregion

            #region Sessions that were started after the start of the interval and ended before the end of the interval

            filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(GameSessionFields.StartUTC > start);
            filter.Add(GameSessionFields.EndUTC < end);

            relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.GameSessionEntityUsingClientId);

            gameSessionCollection = new GameSessionCollection();
            gameSessionCollection.GetMulti(filter, relations);

            foreach (GameSessionEntity gameSessionEntity in gameSessionCollection)
            {
                gameSessions.Add(gameSessionEntity);
            }

            #endregion

            #region Sessions that were ended after the end of the interval

            filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(GameSessionFields.StartUTC > start);
            filter.Add(GameSessionFields.StartUTC < end);
            filter.Add(GameSessionFields.EndUTC > end);

            relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.GameSessionEntityUsingClientId);

            gameSessionCollection = new GameSessionCollection();
            gameSessionCollection.GetMulti(filter, relations);

            foreach (GameSessionEntity gameSessionEntity in gameSessionCollection)
            {
                gameSessionEntity.EndUTC = end;
                gameSessions.Add(gameSessionEntity);
            }

            #endregion

            #region Sessions that were already started before the start of the interval and that were ended after the end of the interval

            filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(GameSessionFields.StartUTC < start);
            filter.Add(GameSessionFields.EndUTC > end);

            relations = new RelationCollection();
            relations.Add(ClientEntity.Relations.GameSessionEntityUsingClientId);

            gameSessionCollection = new GameSessionCollection();
            gameSessionCollection.GetMulti(filter, relations);

            foreach (GameSessionEntity gameSessionEntity in gameSessionCollection)
            {
                gameSessionEntity.StartUTC = start;
                gameSessionEntity.EndUTC = end;
                gameSessions.Add(gameSessionEntity);
            }

            #endregion

            return gameSessions;
        }

        private static GameSessionEntity GetOrCreateGameSessionEntity(ClientEntity clientEntity, GameEntity gameEntity)
        {
            GameSessionEntity latestGameSessionEntity = GetLatestGameSessionEntity(clientEntity, gameEntity.GameId);
            if (latestGameSessionEntity != null)
                return latestGameSessionEntity;

            latestGameSessionEntity = new GameSessionEntity();
            latestGameSessionEntity.ParentCompanyId = clientEntity.CompanyId;
            latestGameSessionEntity.GameId = gameEntity.GameId;
            latestGameSessionEntity.GameName = gameEntity.Name;

            latestGameSessionEntity.ClientId = clientEntity.ClientId;
            if (clientEntity.DeviceId.HasValue)
                latestGameSessionEntity.ClientIdentifier = clientEntity.DeviceEntity.Identifier;

            latestGameSessionEntity.DeliverypointId = clientEntity.DeliverypointId;
            latestGameSessionEntity.DeliverypointNumber = clientEntity.DeliverypointEntity.Number;
            latestGameSessionEntity.DeliverypointName = clientEntity.DeliverypointEntity.Name;

            DateTime utcNow = DateTime.UtcNow;
            latestGameSessionEntity.StartUTC = utcNow.AddMinutes(-1);
            latestGameSessionEntity.EndUTC = utcNow;
            latestGameSessionEntity.Length = 1;
            latestGameSessionEntity.Save();

            return latestGameSessionEntity;
        }

        private static GameSessionEntity GetLatestGameSessionEntity(ClientEntity clientEntity, int gameId)
        {
            GameSessionEntity latestGameSessionEntity = null;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(GameSessionFields.ClientId == clientEntity.ClientId);
            filter.Add(GameSessionFields.GameId == gameId);
            filter.Add(GameSessionFields.EndUTC >= DateTime.UtcNow.AddSeconds(-90));

            GameSessionCollection gameSessionCollection = new GameSessionCollection();
            gameSessionCollection.GetMulti(filter);

            if (gameSessionCollection.Count > 0)
                latestGameSessionEntity = gameSessionCollection[0];

            return latestGameSessionEntity;
        }
    }
}
