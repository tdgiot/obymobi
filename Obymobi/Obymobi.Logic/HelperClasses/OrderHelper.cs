﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Logic.Routing;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Order = Obymobi.Logic.Model.Order;
using Orderitem = Obymobi.Logic.Model.Orderitem;
using OrderStatus = Obymobi.Enums.OrderStatus;
using Terminal = Obymobi.Logic.Model.Terminal;

namespace Obymobi.Logic.HelperClasses
{
	/// <summary>
	/// OrderHelper class
	/// </summary>
	public static class OrderHelper
	{
		/// <summary>
		/// OrderHelperError enums
		/// </summary>
		public enum OrderHelperError
		{
			/// <summary>
			/// Terminal not found for this client
			/// </summary>
			NoTerminalFoundForClient = 100,
			/// <summary>
			/// No product is configured for the POS service message
			/// </summary>
			NoProductConfiguredForPosServiceMessage = 200,
			/// <summary>
			/// The POS service message isn't implemented
			/// </summary>
			PosServiceMessageNotImplemented = 300,
			/// <summary>
			/// TerminalSystemMessagesDeliverypointId is null
			/// </summary>
			TerminalSystemMessagesDeliverypointIdNull = 400,
			/// <summary>
			/// Customer couldn't be found
			/// </summary>
			CustomerNotFound = 500,
			/// <summary>
			/// The customer doesn't have a client
			/// </summary>
			CustomerHasNoClient = 600,
			/// <summary>
			/// Clien't can't be found
			/// </summary>
			ClientNotFound = 601,
			/// <summary>
			/// No identifier was supplied with an order status
			/// </summary>
			NoIdentifierSuppliedWithOrderStatus = 700,
			/// <summary>
			/// Guid does not exists that was supplied with the order status
			/// </summary>
			NonExistingGuidSuppliedWithOrderStatus = 701,
			/// <summary>
			/// OrderId does not exists that was supplied with the order status
			/// </summary>
			NonExistingOrderIdSuppliedWithOrderStatus = 702,
			/// <summary>
			/// Multiple orders were found for the Guid that was supplied with the order status
			/// </summary>
			MultipleOrdersFoundForGuidSuppliedWithOrderStatus = 703,
			/// <summary>
			/// Order can't be found
			/// </summary>
			OrderNotFound = 704,
			/// <summary>
			/// The integrity isn't checked off the order
			/// </summary>
			OrderIsNotIntegrityChecked = 705,
			/// <summary>
			/// The lists that were provided aren't equal size
			/// </summary>
			ProvidedListsHaveDontHaveEqualCount = 706,
		}

		#region Misc Util methods

		/// <summary>
		/// Gets the type of the confirmation code delivery.
		/// </summary>
		/// <param name="order">The order.</param>
		/// <returns></returns>
		public static ConfirmationCodeDeliveryType GetConfirmationCodeDeliveryType(OrderEntity order)
		{
			//if (order.DeliverypointEntity.DeliverypointgroupEntity.ConfirmationCodeDeliveryType.HasValue)
			//    return order.DeliverypointEntity.DeliverypointgroupEntity.ConfirmationCodeDeliveryType.Value.ToEnum<ConfirmationCodeDeliveryType>();
			//else
			return order.CompanyEntity.ConfirmationCodeDeliveryType.ToEnum<ConfirmationCodeDeliveryType>();
		}

		/// <summary>
		/// Generate a fake order (for live order view).
		/// The generated order object is NOT completely filled!
		/// </summary>
		/// <param name="companyId">The company id.</param>
		/// <param name="deliverypoints">The deliverypoints.</param>
		/// <returns></returns>
		public static Order GenerateFakeOrder(int companyId, string[] deliverypoints)
		{
			// Order
			var order = new Order();

			// Get Company
			PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
			prefetch.Add(CompanyEntity.PrefetchPathTerminalCollection);
			prefetch.Add(CompanyEntity.PrefetchPathDeliverypointCollection);
			var company = new CompanyEntity(companyId, prefetch);
			if (!company.IsNew)
			{
				// Create object
				order.CompanyName = company.Name;
				order.Created = DateTime.UtcNow.UtcToLocalTime().ToString("dd-MM-yyyy HH:mm:ss");

				// Get terminal for Deliverypoint title and number
				order.DeliverypointName = company.DeliverypointgroupCollection[0].DeliverypointCaption;

				string deliverypointString = "0";
				if (deliverypoints != null)
				{
					int deliverypointId = RandomUtil.NextInt(0, deliverypoints.Length - 1);
					deliverypointString = deliverypoints[deliverypointId];
				}
				else
				{
					int deliverypointId = RandomUtil.NextInt(0, company.DeliverypointCollection.Count - 1);
					var deliverypoint = company.DeliverypointCollection[deliverypointId];
					deliverypointString = deliverypoint.Number;
				}
				int deliverypointNumber;
				int.TryParse(deliverypointString, out deliverypointNumber);
				order.DeliverypointNumber = deliverypointNumber;

				// Get products
				ProductCollection productCollection = ProductHelper.GetOrderableProductCollectionForCompany(companyId, true);

				// Add products
				int productsToAdd = RandomUtil.NextInt(1, 5);
				order.Orderitems = new Orderitem[productsToAdd];
				List<ProductEntity> products = productCollection.ToList();
				for (int j = 0; j < productsToAdd; j++)
				{
					// Choose product and remove from list to prevent doubles on a order
					int productIndex = RandomUtil.GetRandomNumber(products.Count - 1);
					ProductEntity product = products[productIndex];
					products.Remove(product);

					var item = new Orderitem();
					item.ProductName = product.Name;
					item.Quantity = RandomUtil.NextInt(1, 5);

					// Add orderitem to order
					order.Orderitems[j] = item;
				}
			}

			return order;
		}

		#endregion

		#region Order Processing Methods

		/// <summary>
		/// Updates the order status.
		/// </summary>
		/// <param name="order">The order.</param>
		/// <param name="orderStatus">The order status.</param>
		public static void UpdateOrderStatus(OrderEntity order, OrderStatus orderStatus)
		{
			// Calculate processing time
			if (orderStatus == OrderStatus.Processed)
			{
				order.ProcessedUTC = DateTime.UtcNow;
			}

            order.Status = (int)orderStatus;
			order.Save();
		}

		// End of Order Processing Methods region
		#endregion

		#region Model / Entity Conversion

		/// <summary>
		/// Create a Obymobi.Logic.Model.Order instance using the specified Obymobi.Data.EntityClasses.OrderEntity instance
		/// </summary>
		/// <param name="orderEntity">The OrderEntity instance to create the Order instance for</param>
		/// <param name="applyPosSpecificTranslation">If set to <c>true</c> the pos logic is applied.</param>
		/// <returns>
		/// A Order instance
		/// </returns>
		public static Order CreateOrderModelFromEntity(OrderEntity orderEntity, bool applyPosSpecificTranslation, bool withChildOrders)
		{
			return OrderHelper.CreateOrderModelFromEntity(orderEntity, null, applyPosSpecificTranslation, withChildOrders);
		}

		/// <summary>
		/// Create a Obymobi.Logic.Model.Order instance using the specified Obymobi.Data.EntityClasses.OrderEntity instance
		/// </summary>
		/// <param name="orderEntity">The OrderEntity instance to create the Order instance for</param>
		/// <param name="orderRoutestephandlerEntity">The order routestephandler entity.</param>
		/// <param name="applyPosSpecificTranslation">If set to <c>true</c> the pos logic is applied.</param>
		/// <returns>
		/// A Order instance
		/// </returns>
		public static Order CreateOrderModelFromEntity(OrderEntity orderEntity, OrderRoutestephandlerEntity orderRoutestephandlerEntity, bool applyPosSpecificTranslation, bool withChildOrders)
		{
			Order order = new Order();
			order.OrderId = orderEntity.OrderId;
			order.Guid = orderEntity.Guid;
			order.CompanyId = orderEntity.CompanyId;
			order.CompanyName = (orderEntity.CompanyName.Length > 0 ? orderEntity.CompanyName : orderEntity.CompanyEntity.Name); // TODO 
			order.CompanyObycode = orderEntity.CompanyEntity.Code;
			order.BenchmarkInformation = orderEntity.BenchmarkInformation;
		    order.Processed = orderEntity.ProcessedUTC.HasValue ? orderEntity.ProcessedUTC.GetValueOrDefault().UtcToLocalTime() : DateTime.MinValue;
		    order.ErrorCode = orderEntity.ErrorCode;
		    order.Email = orderEntity.Email;
			order.ServiceMethodType = orderEntity.ServiceMethodType.HasValue ? (ServiceMethodType)orderEntity.ServiceMethodType : default;
			
			order.CoversCount = orderEntity.CoversCount ?? 1;

			PaymentTransactionEntity paymentTransaction = orderEntity.PaymentTransactionCollection.FirstOrDefault(p => p.Status == PaymentTransactionStatus.Paid);

			if (paymentTransaction != null)
			{
				order.PaymentmethodName = paymentTransaction.PaymentMethod;
				order.ServiceMethodName = orderEntity.ServiceMethodName;
				order.CheckoutMethodName = orderEntity.CheckoutMethodName;
				order.CardSummary = paymentTransaction.CardSummary;
			}

			// Set the client friendly name used by this order
			if (orderEntity.ClientId.HasValue)
			{
                order.CustomerNameFull = StringUtil.CombineWithSpace(orderEntity.CustomerFirstname, orderEntity.CustomerLastnamePrefix, orderEntity.CustomerLastname); ;
			}
			else if (orderEntity.CustomerId.HasValue)
			{
				order.CustomerId = orderEntity.CustomerId.Value;

				string firstName = (orderEntity.CustomerFirstname.Length > 0 ? orderEntity.CustomerFirstname : orderEntity.CustomerEntity.Firstname);
				string lastName = (orderEntity.CustomerLastname.Length > 0 ? orderEntity.CustomerLastname : orderEntity.CustomerEntity.Lastname);
				string lastNamePrefix = (orderEntity.CustomerLastnamePrefix.Length > 0 ? orderEntity.CustomerLastnamePrefix : orderEntity.CustomerEntity.LastnamePrefix);

				if (firstName.Length > 0 || lastName.Length > 0)
                {
					order.CustomerNameFull = StringUtil.CombineWithSpace(firstName, lastNamePrefix, lastName);
				}

				order.CustomerPhonenumber = (orderEntity.CustomerPhonenumber.Length > 0 ? orderEntity.CustomerPhonenumber : orderEntity.CustomerEntity.Phonenumber);
			}

			// Set client details
			if (orderEntity.ClientId.HasValue)
			{
				order.ClientId = orderEntity.ClientId.Value;
				//order.ClientMacAddress = (orderEntity.ClientMacAddress.Length > 0 ? orderEntity.ClientMacAddress : orderEntity.ClientEntity.MacAddress);
			}

            // Deliverypoint
            order.DeliverypointId = orderEntity.DeliverypointId.GetValueOrDefault(-1);
            if (string.IsNullOrWhiteSpace(orderEntity.DeliverypointNumber) || !int.TryParse(orderEntity.DeliverypointNumber, out int deliverypointNumber))
            {
                if (!orderEntity.DeliverypointId.HasValue || !int.TryParse(orderEntity.DeliverypointEntity.Number, out deliverypointNumber))
                {
                    deliverypointNumber = -1;
                }
            }
            order.DeliverypointNumber = deliverypointNumber;
            order.DeliverypointName = orderEntity.DeliverypointName;

			// Status
			order.Status = orderEntity.Status;
			order.StatusText = orderEntity.StatusText;

			// Determine the ProcessingExpired state
			if (orderEntity.CompanyEntity.UseMonitoring)
			{
				// GK-ROUTING            
				// Determine if the order is expired
				if (orderEntity.StatusAsEnum != OrderStatus.WaitingForPaymentCompleted)
				{
					if (RoutingHelper.IsOrderExpired(orderEntity, false))
					{
						order.StatusText = "! - " + orderEntity.StatusText;
						order.ProcessingExpired = true;
					}
				}
			}

			order.Notes = orderEntity.Notes;
			order.Type = orderEntity.Type;
			order.TypeText = orderEntity.TypeAsEnum.ToString();

            if(orderEntity.CreatedUTC.HasValue)
            {
                order.CreatedAsDateTime = orderEntity.CreatedUTC.Value.UtcToLocalTime();
                order.Created = order.CreatedAsDateTime.ToString("dd-MM-yyyy HH:mm:ss");
            }
			order.ConfirmationCode = orderEntity.ConfirmationCode;
			order.ConfirmationCodeDeliveryType = (int)OrderHelper.GetConfirmationCodeDeliveryType(orderEntity);
			order.AgeVerificationType = orderEntity.AgeVerificationType;
			order.ErrorSentByEmail = orderEntity.ErrorSentByEmail;
			order.ErrorSentBySMS = orderEntity.ErrorSentBySMS;

            if (orderEntity.UpdatedUTC.HasValue)
            {
                order.Updated = orderEntity.UpdatedUTC.Value.UtcToLocalTime().ToString("dd-MM-yyyy HH:mm:ss");
            }
            else if (orderEntity.CreatedUTC.HasValue)
            {
                order.Updated = orderEntity.CreatedUTC.Value.UtcToLocalTime().ToString("dd-MM-yyyy HH:mm:ss");
            }
			else
            {
                order.Updated = DateTime.UtcNow.UtcToLocalTime().ToString("dd-MM-yyyy HH:mm:ss");
            }
			
            if(orderEntity.PlaceTimeUTC.HasValue)
            {
                order.PlaceTime = orderEntity.PlaceTimeUTC.Value.UtcToLocalTime();
            }
			order.Printed = orderEntity.Printed;
			order.SupportpoolId = (orderEntity.CompanyEntity.SupportpoolId.HasValue ? orderEntity.CompanyEntity.SupportpoolId.Value : 0);
		    order.IsCustomerVip = orderEntity.IsCustomerVip;

			// Add the OrderRoutestephandler
			if (orderRoutestephandlerEntity != null)
			{
				var handler = OrderRoutestephandlerHelper.CreateOrderRoutestephandlerModelFromEntity(orderRoutestephandlerEntity);
				order.OrderRoutestephandler = handler;
			}
			else
			{
				orderEntity.AlreadyFetchedOrderRoutestephandlerCollection = false;
				if (orderEntity.OrderRoutestephandlerCollection.Count > 0)
					order.OrderRoutestephandler = OrderRoutestephandlerHelper.CreateOrderRoutestephandlerModelFromEntity(orderEntity.OrderRoutestephandlerCollection[0]);
			}

			// Do the OrderItems
			List<Orderitem> orderitems = new List<Orderitem>();

			for (int i = 0; i < orderEntity.OrderitemCollection.Count; i++)
			{
				OrderitemEntity orderitem = orderEntity.OrderitemCollection[i];
				orderitems.AddRange(OrderitemHelper.CreateOrderitemModelFromEntity(orderitem));
			}

			if (withChildOrders)
			{
				// Check if this order has child orders
				if (orderEntity.OrderCollection.Count > 0)
				{
					foreach (OrderEntity childOrder in orderEntity.OrderCollection)
					{
						foreach (OrderitemEntity orderitemEntity in childOrder.OrderitemCollection)
						{
							orderitems.AddRange(OrderitemHelper.CreateOrderitemModelFromEntity(orderitemEntity));
						}
					}
				}
			}

			order.Orderitems = orderitems.ToArray();

			// TODO Refactoring Apply POS specific logic:
			//if (applyPosSpecificTranslation)
			//	POS.PosHelperWrapper.PrepareOrderForRequestService(orderEntity, order, order.OrderRoutestephandler.TerminalId);

			return order;
		}

		/// <summary>
		/// Creates the order entity from model.
		/// </summary>
		/// <param name="order">The order.</param>
		/// <param name="isOnsiteServer">if set to <c>true</c> [is onsite server].</param>
		/// <returns></returns>
		public static OrderEntity CreateOrderEntityFromModel(Order order, bool isOnsiteServer = false)
		{
			if (!order.ValidatedByOrderProcessingHelper.HasValue)
			{
				throw new ObymobiException(OrderHelperError.OrderIsNotIntegrityChecked);
			}

			OrderEntity orderEntity = new OrderEntity();
			orderEntity.ValidatedByOrderProcessingHelper = true;

			// Set the order type
			if (order.Type == 0)
			{
				orderEntity.TypeAsEnum = OrderType.Standard;
			}
			else
			{
				orderEntity.Type = order.Type;
				orderEntity.TypeAsEnum = order.Type.ToEnum<OrderType>();
			}

			orderEntity.Guid = order.Guid;

			if (order.CustomerId > 0)
				orderEntity.CustomerId = order.CustomerId;

		    if (order.ClientId > 0)
		    {
                orderEntity.ClientId = order.ClientId;
		        
                if (!order.CustomerNameFull.IsNullOrWhiteSpace())
                {
                    string[] names = order.CustomerNameFull.Split(' ');
                    for (int i = 0; i < names.Length; i++)
                    {
                        if (i == names.Length - 1) // Last name
                            orderEntity.CustomerLastname = names[i];
                        else if (i == 0) // First name
                            orderEntity.CustomerFirstname = names[i];
                        else if (orderEntity.CustomerLastnamePrefix.IsNullOrWhiteSpace()) // Prefix
                            orderEntity.CustomerLastnamePrefix = names[i];
                        else
                            orderEntity.CustomerLastnamePrefix += " " + names[i];
                    }                    
                }		        
		    }		    

			orderEntity.CompanyId = order.CompanyId;
			orderEntity.AgeVerificationType = order.AgeVerificationType;            
            orderEntity.MobileOrder = order.MobileOrder;
            orderEntity.AnalyticsPayLoad = order.AnalyticsPayLoad;
            orderEntity.AnalyticsTrackingIds = order.AnalyticsTrackingIds;

            // NOTE: DO NOT SET ORDER STATUS WHEN ORDER IS PLACED BY ONSITESERVER BITCHES! AIGHT
            if (!isOnsiteServer)
				orderEntity.Status = (int)OrderStatus.NotRouted;
			else
				orderEntity.Status = order.Status;

			orderEntity.BenchmarkInformation = order.BenchmarkInformation;
			orderEntity.ProcessedUTC = order.Processed.HasValue ? order.Processed.Value.LocalTimeToUtc() : DateTime.MinValue;
			orderEntity.DeliverypointId = order.DeliverypointId;
		    orderEntity.Email = order.Email;
            orderEntity.CustomerPhonenumber = order.CustomerPhonenumber;
		    orderEntity.File = order.File;
		    orderEntity.IsCustomerVip = order.IsCustomerVip;            
            
            if (orderEntity.TypeAsEnum == OrderType.Document)
            {
                // We dont need to print document or wake up orders, only notify on console
                orderEntity.Printed = true;
            }

			// ROUTE
			if (order.OrderRoutestephandler != null && order.OrderRoutestephandler.HandlerType > 0)
			{
				OrderRoutestephandlerEntity stepEntity = OrderRoutestephandlerHelper.CreateOrderRoutestephandlerEntityFromModel(order.OrderRoutestephandler);
				orderEntity.OrderRoutestephandlerCollection.Add(stepEntity);
			}

            string orderNotes = string.Empty;
            if ((order.CompanyId == 343 || CompanyHelper.ShouldIncludeDeliveryTimeInOrderNotes(order.CompanyId)) && order.DeliverypointId > 0)
            {
                IncludeFieldsList includes = new IncludeFieldsList(DeliverypointgroupFields.EstimatedDeliveryTime);

                PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointEntity);
                prefetch.Add(DeliverypointEntityBase.PrefetchPathDeliverypointgroupEntity, includes);

                DeliverypointEntity deliverypoint = new DeliverypointEntity(order.DeliverypointId, prefetch);
                if (!deliverypoint.IsNew)
                {
                    orderNotes = string.Format("Wait Time: {0} minutes\n", deliverypoint.DeliverypointgroupEntity.EstimatedDeliveryTime);
                }
            }

		    ClockMode clockMode = ClockMode.Hour24;
		    if (order.CompanyId > 0)
		    {
		        CompanyEntity companyEntity = new CompanyEntity(order.CompanyId);
		        if (!companyEntity.IsNew)
		        {
		            clockMode = companyEntity.ClockMode;
		        }
		    }

		    for (int i = 0; i < order.Orderitems.Length; i++)
			{
				Orderitem orderitem = order.Orderitems[i];

			    string orderLevelAlterationNotes = string.Empty;

				OrderitemEntity orderitemEntity = new OrderitemEntity();
				orderitemEntity.ValidatedByOrderProcessingHelper = true;
                orderitemEntity.Guid = orderitem.Guid;
				orderitemEntity.ProductId = orderitem.ProductId;
				orderitemEntity.ProductName = orderitem.ProductName;
				orderitemEntity.ProductPriceIn = orderitem.ProductPriceIn;                
				orderitemEntity.Quantity = orderitem.Quantity;
				orderitemEntity.VatPercentage = orderitem.VatPercentage;
				orderitemEntity.Notes = orderitem.Notes;
                orderitemEntity.Color = orderitem.Color;
			    orderitemEntity.OrderSource = orderitem.OrderSource;
                if (orderitem.CategoryId > 0)
                {
                    orderitemEntity.CategoryId = orderitem.CategoryId;
                    CategoryEntity c = new CategoryEntity(orderitem.CategoryId);
                    if (!c.IsNew)
                    {
                        orderitemEntity.CategoryName = c.FullCategoryName;
                    }
                }
                else
                    orderitemEntity.CategoryId = null;

			    // Convert Alterationitems
				if (orderitem.Alterationitems != null)
				{
					foreach (Alterationitem alterationItemModel in orderitem.Alterationitems)
					{
						OrderitemAlterationitemEntity orderitemAlterationitemEntity = new OrderitemAlterationitemEntity();
						orderitemAlterationitemEntity.ValidatedByOrderProcessingHelper = true;
                        orderitemAlterationitemEntity.OrderLevelEnabled = alterationItemModel.AlterationOrderLevelEnabled;

                        if (alterationItemModel.AlterationitemId > 0) // Check whether we have a valid alteration item id
						    orderitemAlterationitemEntity.AlterationitemId = alterationItemModel.AlterationitemId;

						orderitemAlterationitemEntity.AlterationName = alterationItemModel.AlterationName;
                        orderitemAlterationitemEntity.AlterationType = alterationItemModel.AlterationType;

                        if (orderitemAlterationitemEntity.AlterationType == 0) // If no type has been specified, default to Options
                            orderitemAlterationitemEntity.AlterationType = (int)AlterationType.Options;

                        if (alterationItemModel.AlterationType == (int)AlterationType.DateTime || alterationItemModel.AlterationType == (int)AlterationType.Date)
                        {
                            orderitemAlterationitemEntity.Time = DateTime.ParseExact(alterationItemModel.Time, "dd-MM-yyyy HHmmss", CultureInfo.InvariantCulture);
                        }
                        else if (alterationItemModel.AlterationType == (int)AlterationType.Email || alterationItemModel.AlterationType == (int)AlterationType.Text || alterationItemModel.AlterationType == (int)AlterationType.Numeric)
                        {
                            orderitemAlterationitemEntity.Value = alterationItemModel.Value;
                        }
                        else
                        {
                            orderitemAlterationitemEntity.AlterationoptionName = alterationItemModel.AlterationoptionName;
                            orderitemAlterationitemEntity.AlterationoptionPriceIn = alterationItemModel.AlterationoptionPriceIn;
                        }

						orderitemEntity.OrderitemAlterationitemCollection.Add(orderitemAlterationitemEntity);
					}

                    // Include the order level alterations in the notes
				    List<int> alterationIdsProcessed = new List<int>();
				    foreach (Alterationitem alterationItemModel in orderitem.Alterationitems)
				    {
                        int alterationId = alterationItemModel.AlterationId;				        
				        if (!alterationIdsProcessed.Contains(alterationId))
				        {
                            AlterationEntity alteration = new AlterationEntity(alterationId);
				            if (!alteration.IsNew && (alterationItemModel.AlterationOrderLevelEnabled || alteration.OrderLevelEnabled))
				            {
                                alterationIdsProcessed.Add(alterationId);

                                string values = string.Empty;
                                if (alterationItemModel.AlterationType == (int)AlterationType.Options)
                                {
                                    foreach (Alterationitem siblingAlterationItem in orderitem.Alterationitems)
                                    {
                                        if (siblingAlterationItem.AlterationId == alterationId)
                                        {
                                            if (values.Length > 0)
                                            {
                                                values += ", ";
                                            }
                                            values += siblingAlterationItem.AlterationoptionName;                                            
                                        }
                                    }
                                }
                                else if (alterationItemModel.AlterationType == (int)AlterationType.Email || alterationItemModel.AlterationType == (int)AlterationType.Text || alterationItemModel.AlterationType == (int)AlterationType.Numeric)
                                {
                                    values += alterationItemModel.Value;
                                }
                                else if (alterationItemModel.AlterationType == (int)AlterationType.Date || alterationItemModel.AlterationType == (int)AlterationType.DateTime)
                                {
                                    DateTime datetime = DateTime.ParseExact(alterationItemModel.Time, "dd-MM-yyyy HHmmss", CultureInfo.InvariantCulture);
                                    if (alterationItemModel.AlterationType == (int)AlterationType.DateTime)
                                    {
                                        if (clockMode == ClockMode.Hour24)
                                        {
                                            values += string.Format("{0:d MMMM HH:mm}", datetime);
                                        }
                                        else if (clockMode == ClockMode.AmPm)
                                        {
                                            values += datetime.ToString("d MMMM hh:mm tt", CultureInfo.InvariantCulture);
                                        }
                                    }
                                    else
                                    {
                                        values += string.Format("{0:d MMMM}", datetime);
                                    }
                                }

				                if (!values.IsNullOrWhiteSpace())
				                {
                                    orderLevelAlterationNotes += string.Format("{0}: {1}\n", alterationItemModel.AlterationName, values);
				                }				                
				            }
				        }
				    }				    
				}

				orderEntity.OrderitemCollection.Add(orderitemEntity);
                orderNotes += orderLevelAlterationNotes;
			}

		    if (!orderNotes.IsNullOrWhiteSpace())
		    {
                orderNotes += "\n";
		    }

		    orderNotes += order.Notes;
            orderEntity.Notes = orderNotes;

			return orderEntity;
		}

		#endregion
	}
}
