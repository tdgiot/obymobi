﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Obymobi.Logic.HelperClasses
{
    public class CategoryTagHelper
    {
        public static void UpdateProductCategoryTags(CategoryEntity categoryEntity)
        {
            if (categoryEntity.ParentCategoryChanged)
            {
                // Parent category changed
                ICollection<ProductCategoryEntity> productCategoryEntities = GetProductCategoriesForChildCategories(categoryEntity).ToList();
                if (productCategoryEntities.Any())
                {
                    // delete all parent category tags from products
                    DeleteProductCategoryTagsForParentCategories(categoryEntity, productCategoryEntities);

                    // set all tags from new parent categories on products
                    CreateProductCategoryTagsForParentCategories(categoryEntity.ParentCategoryEntity, productCategoryEntities);
                }
            }

            // Check for any new/removed tags on category
            ICollection<int> activeTagIds = categoryEntity.CategoryTagCollection.Select(x => x.TagId).ToList();
            ICollection<int> productCategoryTagIds = categoryEntity.ProductCategoryTagCollection.Select(x => x.TagId).ToList();

            // Remove old tags
            ICollection<int> tagIdsToRemove = productCategoryTagIds.Except(activeTagIds).ToList();
            DeleteOldTags(categoryEntity, tagIdsToRemove);

            // Add new tags
            ICollection<int> tagIdsToAdd = activeTagIds.Except(productCategoryTagIds).ToList();
            if (tagIdsToAdd.Count > 0)
            {
                AddNewTags(categoryEntity, tagIdsToAdd, GetProductCategoriesForChildCategories(categoryEntity));
            }
        }
        
        /// <summary>
        /// Delete any parent category tags from sub-products
        /// </summary>
        /// <param name="categoryEntity">The category to use as base to get parent categories</param>
        public static void DeleteProductCategoryTagsForParentCategories(CategoryEntity categoryEntity, ICollection<ProductCategoryEntity> productCategoryEntities)
        {
            int? parentCategoryId = categoryEntity.OldParentCategoryId;
            if (!parentCategoryId.HasValue) return;

            CategoryEntity oldParentCategoryEntity = new CategoryEntity(parentCategoryId.Value);
            if (oldParentCategoryEntity.IsNew) return;

            // Get list of all products available in any sub-categories
            IEnumerable<int> productCategoryIds = productCategoryEntities.Select(x => x.ProductCategoryId);

            // Get list of all parent categories
            ICollection<int> oldParentCategoryIds = oldParentCategoryEntity.GetParentCategories().Select(x => x.CategoryId).ToList();
            oldParentCategoryIds.Add(oldParentCategoryEntity.CategoryId);

            // Delete all product category tags linked to the products in the tree and any old category
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductCategoryTagFields.ProductCategoryId.In(productCategoryIds));
            filter.AddWithAnd(ProductCategoryTagFields.CategoryId.In(oldParentCategoryIds));
            
            ProductCategoryTagCollection collection = new ProductCategoryTagCollection();
            collection.AddToTransaction(categoryEntity);
            collection.DeleteMulti(filter);
        }

        private static void DeleteOldTags(CategoryEntity useableEntity, IEnumerable<int> tagIdsToRemove)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductCategoryTagFields.CategoryId == useableEntity.CategoryId);
            filter.Add(ProductCategoryTagFields.TagId.In(tagIdsToRemove));

            ProductCategoryTagCollection productCategoryTagCollection = new ProductCategoryTagCollection();
            productCategoryTagCollection.AddToTransaction(useableEntity);
            productCategoryTagCollection.DeleteMulti(filter);
        }

        private static void AddNewTags(CategoryEntity useableEntity, ICollection<int> tagIdsToAdd, IEnumerable<ProductCategoryEntity> productCategories)
        {
            if (tagIdsToAdd.Count == 0) return;

            ProductCategoryTagCollection productCategoryTagsToAdd = new ProductCategoryTagCollection();

            // Loop through all product categories and add new tags
            foreach (ProductCategoryEntity productCategoryEntity in productCategories)
            {
                foreach (int tagId in tagIdsToAdd)
                {
                    ProductCategoryTagEntity productCategoryTagEntity = new ProductCategoryTagEntity();
                    productCategoryTagEntity.ProductCategoryEntity = productCategoryEntity;
                    productCategoryTagEntity.CategoryId = useableEntity.CategoryId;
                    productCategoryTagEntity.TagId = tagId;

                    productCategoryTagsToAdd.Add(productCategoryTagEntity);
                }
            }

            productCategoryTagsToAdd.AddToTransaction(useableEntity);
            productCategoryTagsToAdd.SaveMulti();
        }

        private static void CreateProductCategoryTagsForParentCategories(CategoryEntity parentCategoryEntity, ICollection<ProductCategoryEntity> productCategoryEntities)
        {
            if (parentCategoryEntity.IsNew) return;

            ICollection<int> tagIds = parentCategoryEntity.CategoryTagCollection.Select(x => x.TagId).ToList();
            if (tagIds.Count > 0)
            {
                AddNewTags(parentCategoryEntity, tagIds, productCategoryEntities);
            }

            if (parentCategoryEntity.ParentCategoryId.HasValue)
            {
                CreateProductCategoryTagsForParentCategories(parentCategoryEntity.ParentCategoryEntity, productCategoryEntities);
            }
        }

        private static IEnumerable<ProductCategoryEntity> GetProductCategoriesForChildCategories(CategoryEntity parentCategory)
        {
            if (parentCategory.HasProducts)
            {
                return parentCategory.ProductCategoryCollection.Where(productCategory => !productCategory.IsDeleted);
            }

            List<ProductCategoryEntity> productCategoryList = new List<ProductCategoryEntity>();
            foreach (CategoryEntity childCategory in parentCategory.ChildCategoryCollection)
            {
                productCategoryList.AddRange(GetProductCategoriesForChildCategories(childCategory));
            }

            return productCategoryList;
        }
    }
}