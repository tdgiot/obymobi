﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System;
using System.Collections.Generic;

namespace Obymobi.Logic.HelperClasses
{
    public class ClientConfigurationHelper
    {
        private static Dictionary<CustomTextType, CustomTextType> DeliverypointgroupCustomTextToClientConfig = new Dictionary<CustomTextType, CustomTextType>()
                                                                                        {
                                                                                            {CustomTextType.DeliverypointgroupOrderProcessingNotificationTitle, CustomTextType.ClientConfigurationOrderProcessingNotificationTitle},
                                                                                            {CustomTextType.DeliverypointgroupOrderProcessingNotificationText, CustomTextType.ClientConfigurationOrderProcessingNotificationText},
                                                                                            {CustomTextType.DeliverypointgroupServiceProcessingNotificationTitle, CustomTextType.ClientConfigurationServiceProcessingNotificationTitle},
                                                                                            {CustomTextType.DeliverypointgroupServiceProcessingNotificationText, CustomTextType.ClientConfigurationServiceProcessingNotificationText},
                                                                                            {CustomTextType.DeliverypointgroupOrderProcessedNotificationTitle, CustomTextType.ClientConfigurationOrderProcessedNotificationTitle},
                                                                                            {CustomTextType.DeliverypointgroupOrderProcessedNotificationText, CustomTextType.ClientConfigurationOrderProcessedNotificationText},
                                                                                            {CustomTextType.DeliverypointgroupServiceProcessedNotificationTitle, CustomTextType.ClientConfigurationServiceProcessedNotificationTitle},
                                                                                            {CustomTextType.DeliverypointgroupServiceProcessedNotificationText, CustomTextType.ClientConfigurationServiceProcessedNotificationText},
                                                                                            {CustomTextType.DeliverypointgroupFreeformMessageTitle, CustomTextType.ClientConfigurationFreeformMessageTitle},
                                                                                            {CustomTextType.DeliverypointgroupOutOfChargeTitle, CustomTextType.ClientConfigurationOutOfChargeTitle},
                                                                                            {CustomTextType.DeliverypointgroupOutOfChargeText, CustomTextType.ClientConfigurationOutOfChargeText},
                                                                                            {CustomTextType.DeliverypointgroupOrderProcessedTitle, CustomTextType.ClientConfigurationOrderProcessedTitle},
                                                                                            {CustomTextType.DeliverypointgroupOrderProcessedText, CustomTextType.ClientConfigurationOrderProcessedText},
                                                                                            {CustomTextType.DeliverypointgroupOrderFailedTitle, CustomTextType.ClientConfigurationOrderFailedTitle},
                                                                                            {CustomTextType.DeliverypointgroupOrderFailedText, CustomTextType.ClientConfigurationOrderFailedText},
                                                                                            {CustomTextType.DeliverypointgroupOrderSavingTitle, CustomTextType.ClientConfigurationOrderSavingTitle},
                                                                                            {CustomTextType.DeliverypointgroupOrderSavingText, CustomTextType.ClientConfigurationOrderSavingText},
                                                                                            {CustomTextType.DeliverypointgroupOrderCompletedTitle, CustomTextType.ClientConfigurationOrderCompletedTitle},
                                                                                            {CustomTextType.DeliverypointgroupOrderCompletedText, CustomTextType.ClientConfigurationOrderCompletedText},
                                                                                            {CustomTextType.DeliverypointgroupServiceSavingTitle, CustomTextType.ClientConfigurationServiceSavingTitle},
                                                                                            {CustomTextType.DeliverypointgroupServiceSavingText, CustomTextType.ClientConfigurationServiceSavingText},
                                                                                            {CustomTextType.DeliverypointgroupServiceProcessedTitle, CustomTextType.ClientConfigurationServiceProcessedTitle},
                                                                                            {CustomTextType.DeliverypointgroupServiceProcessedText, CustomTextType.ClientConfigurationServiceProcessedText},
                                                                                            {CustomTextType.DeliverypointgroupServiceFailedTitle, CustomTextType.ClientConfigurationServiceFailedTitle},
                                                                                            {CustomTextType.DeliverypointgroupServiceFailedText, CustomTextType.ClientConfigurationServiceFailedText},
                                                                                            {CustomTextType.DeliverypointgroupOrderingNotAvailableText, CustomTextType.ClientConfigurationOrderingNotAvailableText},
                                                                                            {CustomTextType.DeliverypointgroupPmsDeviceLockedTitle, CustomTextType.ClientConfigurationPmsDeviceLockedTitle},
                                                                                            {CustomTextType.DeliverypointgroupPmsDeviceLockedText, CustomTextType.ClientConfigurationPmsDeviceLockedText},
                                                                                            {CustomTextType.DeliverypointgroupPmsCheckinFailedTitle, CustomTextType.ClientConfigurationPmsCheckinFailedTitle},
                                                                                            {CustomTextType.DeliverypointgroupPmsCheckinFailedText, CustomTextType.ClientConfigurationPmsCheckinFailedText},
                                                                                            {CustomTextType.DeliverypointgroupPmsRestartTitle, CustomTextType.ClientConfigurationPmsRestartTitle},
                                                                                            {CustomTextType.DeliverypointgroupPmsRestartText, CustomTextType.ClientConfigurationPmsRestartText},
                                                                                            {CustomTextType.DeliverypointgroupPmsWelcomeTitle, CustomTextType.ClientConfigurationPmsWelcomeTitle},
                                                                                            {CustomTextType.DeliverypointgroupPmsWelcomeText, CustomTextType.ClientConfigurationPmsWelcomeText},
                                                                                            {CustomTextType.DeliverypointgroupPmsCheckoutCompleteTitle, CustomTextType.ClientConfigurationPmsCheckoutCompleteTitle},
                                                                                            {CustomTextType.DeliverypointgroupPmsCheckoutCompleteText, CustomTextType.ClientConfigurationPmsCheckoutCompleteText},
                                                                                            {CustomTextType.DeliverypointgroupPmsCheckoutApproveText, CustomTextType.ClientConfigurationPmsCheckoutApproveText},
                                                                                            {CustomTextType.DeliverypointgroupChargerRemovedTitle, CustomTextType.ClientConfigurationChargerRemovedTitle},
                                                                                            {CustomTextType.DeliverypointgroupChargerRemovedText, CustomTextType.ClientConfigurationChargerRemovedText},
                                                                                            {CustomTextType.DeliverypointgroupChargerRemovedReminderTitle, CustomTextType.ClientConfigurationChargerRemovedReminderTitle},
                                                                                            {CustomTextType.DeliverypointgroupChargerRemovedReminderText, CustomTextType.ClientConfigurationChargerRemovedReminderText},
                                                                                            {CustomTextType.DeliverypointgroupTurnOffPrivacyTitle, CustomTextType.ClientConfigurationTurnOffPrivacyTitle},
                                                                                            {CustomTextType.DeliverypointgroupTurnOffPrivacyText, CustomTextType.ClientConfigurationTurnOffPrivacyText},
                                                                                            {CustomTextType.DeliverypointgroupItemCurrentlyUnavailableText, CustomTextType.ClientConfigurationItemCurrentlyUnavailableText},
                                                                                            {CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingTitle, CustomTextType.ClientConfigurationAlarmSetWhileNotChargingTitle},
                                                                                            {CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingText, CustomTextType.ClientConfigurationAlarmSetWhileNotChargingText},
                                                                                            {CustomTextType.DeliverypointgroupDeliverypointCaption, CustomTextType.ClientConfigurationDeliverypointCaption},
                                                                                            {CustomTextType.DeliverypointgroupPrintingConfirmationTitle, CustomTextType.ClientConfigurationPrintingConfirmationTitle},
                                                                                            {CustomTextType.DeliverypointgroupPrintingConfirmationText, CustomTextType.ClientConfigurationPrintingConfirmationText},
                                                                                            {CustomTextType.DeliverypointgroupPrintingSucceededTitle, CustomTextType.ClientConfigurationPrintingSucceededTitle},
                                                                                            {CustomTextType.DeliverypointgroupPrintingSucceededText, CustomTextType.ClientConfigurationPrintingSucceededText},
                                                                                            {CustomTextType.DeliverypointgroupSuggestionsCaption, CustomTextType.ClientConfigurationSuggestionsCaption},
                                                                                            {CustomTextType.DeliverypointgroupRoomserviceChargeText, CustomTextType.ClientConfigurationRoomserviceChargeText},
                                                                                            {CustomTextType.DeliverypointgroupDeliveryLocationMismatchTitle, CustomTextType.ClientConfigurationDeliveryLocationMismatchTitle},
                                                                                            {CustomTextType.DeliverypointgroupDeliveryLocationMismatchText, CustomTextType.ClientConfigurationDeliveryLocationMismatchText},
                                                                                            {CustomTextType.DeliverypointgroupBrowserAgeVerificationTitle, CustomTextType.ClientConfigurationBrowserAgeVerificationTitle},
                                                                                            {CustomTextType.DeliverypointgroupBrowserAgeVerificationText, CustomTextType.ClientConfigurationBrowserAgeVerificationText},
                                                                                            {CustomTextType.DeliverypointgroupBrowserAgeVerificationButton, CustomTextType.ClientConfigurationBrowserAgeVerificationButton},
                                                                                            {CustomTextType.DeliverypointgroupRestartApplicationDialogTitle, CustomTextType.ClientConfigurationRestartApplicationDialogTitle},
                                                                                            {CustomTextType.DeliverypointgroupRestartApplicationDialogText, CustomTextType.ClientConfigurationRestartApplicationDialogText},
                                                                                            {CustomTextType.DeliverypointgroupRebootDeviceDialogTitle, CustomTextType.ClientConfigurationRebootDeviceDialogTitle},
                                                                                            {CustomTextType.DeliverypointgroupRebootDeviceDialogText, CustomTextType.ClientConfigurationRebootDeviceDialogText},
                                                                                            {CustomTextType.DeliverypointgroupEstimatedDeliveryTimeDialogTitle, CustomTextType.ClientConfigurationEstimatedDeliveryTimeDialogTitle},
                                                                                            {CustomTextType.DeliverypointgroupEstimatedDeliveryTimeDialogText, CustomTextType.ClientConfigurationEstimatedDeliveryTimeDialogText},
                                                                                            {CustomTextType.DeliverypointgroupClearBasketTitle, CustomTextType.ClientConfigurationClearBasketTitle},
                                                                                            {CustomTextType.DeliverypointgroupClearBasketText, CustomTextType.ClientConfigurationClearBasketText},
                                                                                        };

        public static void CreateFromDeliverypointgroup(DeliverypointgroupEntity deliverypointgroupEntity)
        {
            int? entertainmentConfigurationId = null;

            EntertainmentConfigurationEntity entertainmentConfigurationEntity = new EntertainmentConfigurationEntity();
            entertainmentConfigurationEntity.AddToTransaction(deliverypointgroupEntity);
            entertainmentConfigurationEntity.CompanyId = deliverypointgroupEntity.CompanyId;
            entertainmentConfigurationEntity.Name = deliverypointgroupEntity.Name;
            if (entertainmentConfigurationEntity.Save())
            {
                entertainmentConfigurationEntity.Refetch();
                entertainmentConfigurationId = entertainmentConfigurationEntity.EntertainmentConfigurationId;
            }

            if (entertainmentConfigurationId.HasValue)
            {
                foreach (EntertainmentEntity entertainmentEntity in deliverypointgroupEntity.EntertainmentCollectionViaDeliverypointgroupEntertainment)
                {
                    EntertainmentConfigurationEntertainmentEntity entertainmentConfigurationEntertainmentEntity = new EntertainmentConfigurationEntertainmentEntity();
                    entertainmentConfigurationEntertainmentEntity.AddToTransaction(deliverypointgroupEntity);
                    entertainmentConfigurationEntertainmentEntity.ParentCompanyId = deliverypointgroupEntity.CompanyId;
                    entertainmentConfigurationEntertainmentEntity.EntertainmentConfigurationId = entertainmentConfigurationId.Value;
                    entertainmentConfigurationEntertainmentEntity.EntertainmentId = entertainmentEntity.EntertainmentId;
                    entertainmentConfigurationEntertainmentEntity.Save();
                }
            }

            // Convert DPG advertisement to advertisement configuration
            int? advertisementConfigurationId = null;
            AdvertisementConfigurationEntity advertisementConfigurationEntity = new AdvertisementConfigurationEntity();
            advertisementConfigurationEntity.AddToTransaction(deliverypointgroupEntity);
            advertisementConfigurationEntity.CompanyId = deliverypointgroupEntity.CompanyId;
            advertisementConfigurationEntity.Name = deliverypointgroupEntity.Name;
            if (advertisementConfigurationEntity.Save())
            {
                advertisementConfigurationEntity.Refetch();
                advertisementConfigurationId = advertisementConfigurationEntity.AdvertisementConfigurationId;
            }

            if (advertisementConfigurationId.HasValue)
            {
                foreach (AdvertisementEntity advertisementEntity in deliverypointgroupEntity.AdvertisementCollectionViaDeliverypointgroupAdvertisement)
                {
                    AdvertisementConfigurationAdvertisementEntity advertisementConfigurationAdvertisementEntity = new AdvertisementConfigurationAdvertisementEntity();
                    advertisementConfigurationAdvertisementEntity.AddToTransaction(deliverypointgroupEntity);
                    advertisementConfigurationAdvertisementEntity.ParentCompanyId = deliverypointgroupEntity.CompanyId;
                    advertisementConfigurationAdvertisementEntity.AdvertisementConfigurationId = advertisementConfigurationId.Value;
                    advertisementConfigurationAdvertisementEntity.AdvertisementId = advertisementEntity.AdvertisementId;
                    advertisementConfigurationAdvertisementEntity.Save();
                }
            }

            ClientConfigurationEntity clientConfigurationEntity = new ClientConfigurationEntity();
            clientConfigurationEntity.AddToTransaction(deliverypointgroupEntity);
            clientConfigurationEntity.CompanyId = deliverypointgroupEntity.CompanyId;
            clientConfigurationEntity.Name = deliverypointgroupEntity.Name;
            clientConfigurationEntity.DeliverypointgroupId = deliverypointgroupEntity.DeliverypointgroupId;
            clientConfigurationEntity.MenuId = deliverypointgroupEntity.MenuId.Value;
            clientConfigurationEntity.UIModeId = deliverypointgroupEntity.UIModeId.Value;
            clientConfigurationEntity.UIThemeId = deliverypointgroupEntity.UIThemeId;
            clientConfigurationEntity.UIScheduleId = deliverypointgroupEntity.UIScheduleId;
            clientConfigurationEntity.EntertainmentConfigurationId = entertainmentConfigurationId;
            clientConfigurationEntity.AdvertisementConfigurationId = advertisementConfigurationId;
            clientConfigurationEntity.Pincode = deliverypointgroupEntity.Pincode;
            clientConfigurationEntity.PincodeSU = deliverypointgroupEntity.PincodeSU;
            clientConfigurationEntity.PincodeGM = deliverypointgroupEntity.PincodeGM;
            clientConfigurationEntity.PriceScheduleId = deliverypointgroupEntity.PriceScheduleId;
            clientConfigurationEntity.HidePrices = deliverypointgroupEntity.HidePrices;
            clientConfigurationEntity.ShowCurrencySymbol = deliverypointgroupEntity.CompanyEntity.ShowCurrencySymbol;
            clientConfigurationEntity.ClearSessionOnTimeout = deliverypointgroupEntity.ClearSessionOnTimeout;
            clientConfigurationEntity.ResetTimeout = deliverypointgroupEntity.ResetTimeout;
            clientConfigurationEntity.ScreenTimeoutInterval = deliverypointgroupEntity.ScreenTimeoutInterval;
            clientConfigurationEntity.DimLevelDull = deliverypointgroupEntity.DimLevelDull;
            clientConfigurationEntity.DimLevelMedium = deliverypointgroupEntity.DimLevelMedium;
            clientConfigurationEntity.DimLevelBright = deliverypointgroupEntity.DimLevelBright;
            clientConfigurationEntity.DockedDimLevelDull = deliverypointgroupEntity.DockedDimLevelDull;
            clientConfigurationEntity.DockedDimLevelMedium = deliverypointgroupEntity.DockedDimLevelMedium;
            clientConfigurationEntity.DockedDimLevelBright = deliverypointgroupEntity.DockedDimLevelBright;
            clientConfigurationEntity.PowerSaveTimeout = deliverypointgroupEntity.PowerSaveTimeout;
            clientConfigurationEntity.PowerSaveLevel = deliverypointgroupEntity.PowerSaveLevel;
            clientConfigurationEntity.OutOfChargeLevel = deliverypointgroupEntity.OutOfChargeLevel;
            clientConfigurationEntity.DailyOrderReset = deliverypointgroupEntity.DailyOrderReset;
            clientConfigurationEntity.SleepTime = deliverypointgroupEntity.SleepTime;
            clientConfigurationEntity.WakeUpTime = deliverypointgroupEntity.WakeUpTime;
            clientConfigurationEntity.HomepageSlideshowInterval = deliverypointgroupEntity.HomepageSlideshowInterval;
            clientConfigurationEntity.IsChargerRemovedDialogEnabled = deliverypointgroupEntity.IsChargerRemovedDialogEnabled;
            clientConfigurationEntity.IsChargerRemovedReminderDialogEnabled = deliverypointgroupEntity.IsChargerRemovedReminderDialogEnabled;
            clientConfigurationEntity.PowerButtonHardBehaviour = deliverypointgroupEntity.PowerButtonHardBehaviour;
            clientConfigurationEntity.PowerButtonSoftBehaviour = deliverypointgroupEntity.PowerButtonSoftBehaviour;
            clientConfigurationEntity.ScreenOffMode = deliverypointgroupEntity.ScreenOffMode;
            clientConfigurationEntity.ScreensaverMode = deliverypointgroupEntity.ScreensaverMode;
            clientConfigurationEntity.DeviceRebootMethod = deliverypointgroupEntity.CompanyEntity.DeviceRebootMethod;
            clientConfigurationEntity.RoomserviceCharge = deliverypointgroupEntity.RoomserviceCharge.GetValueOrDefault(0);
            clientConfigurationEntity.AllowFreeText = deliverypointgroupEntity.CompanyEntity.AllowFreeText;
            clientConfigurationEntity.BrowserAgeVerificationEnabled = deliverypointgroupEntity.BrowserAgeVerificationEnabled;
            clientConfigurationEntity.BrowserAgeVerificationLayout = deliverypointgroupEntity.BrowserAgeVerificationLayout;
            clientConfigurationEntity.RestartApplicationDialogEnabled = deliverypointgroupEntity.RestartApplicationDialogEnabled;
            clientConfigurationEntity.RebootDeviceDialogEnabled = deliverypointgroupEntity.RebootDeviceDialogEnabled;
            clientConfigurationEntity.RestartTimeoutSeconds = deliverypointgroupEntity.RestartTimeoutSeconds;
            clientConfigurationEntity.GooglePrinterId = deliverypointgroupEntity.GooglePrinterId;
            clientConfigurationEntity.PmsIntegration = deliverypointgroupEntity.PmsIntegration;
            clientConfigurationEntity.PmsAllowShowBill = deliverypointgroupEntity.PmsAllowShowBill;
            clientConfigurationEntity.PmsAllowExpressCheckout = deliverypointgroupEntity.PmsAllowExpressCheckout;
            clientConfigurationEntity.PmsAllowShowGuestName = deliverypointgroupEntity.PmsAllowShowGuestName;
            clientConfigurationEntity.PmsLockClientWhenNotCheckedIn = deliverypointgroupEntity.PmsLockClientWhenNotCheckedIn;
            clientConfigurationEntity.PmsWelcomeTimeoutMinutes = deliverypointgroupEntity.PmsWelcomeTimeoutMinutes;
            clientConfigurationEntity.PmsCheckoutCompleteTimeoutMinutes = deliverypointgroupEntity.PmsCheckoutCompleteTimeoutMinutes;
            clientConfigurationEntity.IsOrderitemAddedDialogEnabled = deliverypointgroupEntity.IsOrderitemAddedDialogEnabled;

            // Save the client configuration
            if (clientConfigurationEntity.Save())
            {
                clientConfigurationEntity.Refetch();

                deliverypointgroupEntity.ClientConfigurationId = clientConfigurationEntity.ClientConfigurationId;
                deliverypointgroupEntity.Validator = null;
                deliverypointgroupEntity.Save();

                if (deliverypointgroupEntity.RouteId.HasValue) // Default route
                {
                    ClientConfigurationRouteEntity clientConfigurationRouteEntity = new ClientConfigurationRouteEntity();
                    clientConfigurationRouteEntity.AddToTransaction(deliverypointgroupEntity);
                    clientConfigurationRouteEntity.ParentCompanyId = deliverypointgroupEntity.CompanyId;
                    clientConfigurationRouteEntity.ClientConfigurationId = clientConfigurationEntity.ClientConfigurationId;
                    clientConfigurationRouteEntity.RouteId = deliverypointgroupEntity.RouteId.Value;
                    clientConfigurationRouteEntity.Type = 0;
                    clientConfigurationRouteEntity.Save();
                }

                if (deliverypointgroupEntity.SystemMessageRouteId.HasValue) // System messages route
                {
                    ClientConfigurationRouteEntity clientConfigurationRouteEntity = new ClientConfigurationRouteEntity();
                    clientConfigurationRouteEntity.AddToTransaction(deliverypointgroupEntity);
                    clientConfigurationRouteEntity.ParentCompanyId = deliverypointgroupEntity.CompanyId;
                    clientConfigurationRouteEntity.ClientConfigurationId = clientConfigurationEntity.ClientConfigurationId;
                    clientConfigurationRouteEntity.RouteId = deliverypointgroupEntity.SystemMessageRouteId.Value;
                    clientConfigurationRouteEntity.Type = 1;
                    clientConfigurationRouteEntity.Save();
                }

                if (deliverypointgroupEntity.OrderNotesRouteId.HasValue) // Order notes route
                {
                    ClientConfigurationRouteEntity clientConfigurationRouteEntity = new ClientConfigurationRouteEntity();
                    clientConfigurationRouteEntity.AddToTransaction(deliverypointgroupEntity);
                    clientConfigurationRouteEntity.ParentCompanyId = deliverypointgroupEntity.CompanyId;
                    clientConfigurationRouteEntity.ClientConfigurationId = clientConfigurationEntity.ClientConfigurationId;
                    clientConfigurationRouteEntity.RouteId = deliverypointgroupEntity.OrderNotesRouteId.Value;
                    clientConfigurationRouteEntity.Type = 2;
                    clientConfigurationRouteEntity.Save();
                }

                if (deliverypointgroupEntity.EmailDocumentRouteId.HasValue) // Email document route
                {
                    ClientConfigurationRouteEntity clientConfigurationRouteEntity = new ClientConfigurationRouteEntity();
                    clientConfigurationRouteEntity.AddToTransaction(deliverypointgroupEntity);
                    clientConfigurationRouteEntity.ParentCompanyId = deliverypointgroupEntity.CompanyId;
                    clientConfigurationRouteEntity.ClientConfigurationId = clientConfigurationEntity.ClientConfigurationId;
                    clientConfigurationRouteEntity.RouteId = deliverypointgroupEntity.EmailDocumentRouteId.Value;
                    clientConfigurationRouteEntity.Type = 3;
                    clientConfigurationRouteEntity.Save();
                }

                // Custom Texts
                foreach (CustomTextEntity customTextEntity in deliverypointgroupEntity.CustomTextCollection)
                {
                    CustomTextType convertedType;
                    if (!ClientConfigurationHelper.DeliverypointgroupCustomTextToClientConfig.TryGetValue(customTextEntity.Type, out convertedType))
                    {
                        continue;
                    }

                    CustomTextEntity newText = new CustomTextEntity();
                    newText.AddToTransaction(deliverypointgroupEntity);
                    newText.ParentCompanyId = deliverypointgroupEntity.CompanyId;
                    newText.ClientConfigurationId = deliverypointgroupEntity.ClientConfigurationId;
                    newText.Type = convertedType;
                    newText.CultureCode = customTextEntity.CultureCode;
                    newText.Text = customTextEntity.Text;
                    newText.Save();
                }
            }
        }
    }
}
