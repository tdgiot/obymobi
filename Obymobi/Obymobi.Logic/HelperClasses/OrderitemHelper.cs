﻿using System;
using System.Collections.Generic;
using System.Xml;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// OrderitemHelper class
    /// </summary>
    public class OrderitemHelper
    {
        /// <summary>
        /// Create a Obymobi.Logic.Model.Orderitem instance using the specified Obymobi.Data.EntityClasses.OrderitemEntity instance
        /// </summary>
        /// <param name="orderitemEntity">The orderitem entity.</param>
        /// <returns>
        /// A Order instance
        /// </returns>
        public static List<Orderitem> CreateOrderitemModelFromEntity(OrderitemEntity orderitemEntity)
        {
            CompanyEntity company = orderitemEntity.OrderEntity.CompanyEntity;

            List<Orderitem> orderitems = new List<Orderitem>();

            Orderitem orderitem = new Orderitem();
            orderitem.OrderitemId = orderitemEntity.OrderitemId;
            orderitem.Guid = orderitemEntity.Guid;

            orderitem.ProductId = (orderitemEntity.ProductId.HasValue ? orderitemEntity.ProductId.Value : -1);
            orderitem.ProductName = orderitemEntity.ProductName;
            orderitem.ProductPriceIn = orderitemEntity.ProductPriceIn;
            orderitem.Quantity = orderitemEntity.Quantity;
            orderitem.VatPercentage = orderitemEntity.VatPercentage;
            orderitem.Notes = orderitemEntity.Notes;
            orderitem.Color = orderitemEntity.Color;
            orderitem.OrderSource = orderitemEntity.OrderSource;
            orderitem.Type = orderitemEntity.Type;

            List<Alterationitem> alterationItems = new List<Alterationitem>();
            List<Orderitem> alterationOrderitems = new List<Orderitem>();
            foreach (var alteration in orderitemEntity.OrderitemAlterationitemCollection)
            {
                if (alteration.AlterationitemId.HasValue && alteration.AlterationitemId.Value > 0)
                {
                    AlterationoptionEntity alterationoption = alteration.AlterationitemEntity.AlterationoptionEntity;

                    // Check whether the alteration option is connected to a POS product
                    // and whether the POS product is connected to a regular product
                    POSConnectorType posConnectorType = company.GetPOSConnectorType();

                    if (alterationoption.PosproductId.HasValue && posConnectorType != POSConnectorType.Unknown && alterationoption.PosproductEntity.ProductCollection.Count > 0)
                    {
                        // Alteration is connected to a posproduct
                        Orderitem alterationOrderitem = new Orderitem();
                        alterationOrderitem.OrderitemId = orderitem.OrderitemId + alteration.AlterationitemEntity.AlterationoptionId;
                        alterationOrderitem.ProductId = alteration.AlterationitemEntity.AlterationoptionEntity.PosproductEntity.ProductCollection[0].ProductId;
                        alterationOrderitem.ProductName = alteration.AlterationitemEntity.AlterationoptionEntity.PosproductEntity.ProductCollection[0].Name;
                        alterationOrderitem.Quantity = orderitemEntity.Quantity;
                        alterationOrderitem.ProductPriceIn = alteration.AlterationoptionPriceIn;

                        alterationOrderitem.Notes = string.Empty;

                        if (alterationoption.IsProductRelated) // For Vectron integration to group products in the correct order
                        {
                            alterationOrderitems.Insert(0, alterationOrderitem);
                        }
                        else
                        {
                            alterationOrderitems.Add(alterationOrderitem);
                        }
                    }
                    else
                    {
                        Alterationitem item = new Alterationitem();
                        item.AlterationId = alteration.AlterationitemEntity.AlterationId;
                        item.AlterationType = (int)alteration.AlterationitemEntity.AlterationEntity.Type;
                        item.AlterationoptionId = alteration.AlterationitemEntity.AlterationoptionId;
                        item.AlterationName = alteration.AlterationitemEntity.AlterationEntity.Name;
                        item.AlterationoptionName = alteration.AlterationitemEntity.AlterationoptionEntity.Name;
                        item.AlterationoptionPriceIn = alteration.AlterationoptionPriceIn;
                        item.AlterationOrderLevelEnabled = alteration.OrderLevelEnabled || alteration.AlterationitemEntity.AlterationEntity.OrderLevelEnabled;
                        item.Value = alteration.Value;

                        alterationItems.Add(item);
                    }
                }
                else if (alteration.AlterationType == (int)AlterationType.DateTime || alteration.AlterationType == (int)AlterationType.Date)
                {
                    Alterationitem item = new Alterationitem();
                    item.AlterationType = alteration.AlterationType;
                    item.AlterationName = alteration.AlterationName;
                    if (alteration.Time.HasValue)
                    {
                        item.Time = alteration.Time.Value.ToString("dd-MM-yyyy HH:mm:ss");
                    }
                    else if (alteration.TimeUTC.HasValue)
                    {
                        item.Time = alteration.TimeUTC.Value.ToTimeZone(TimeZoneInfo.Utc, company.TimeZoneInfo).ToString("dd-MM-yyyy HH:mm:ss");
                    }
                    alterationItems.Add(item);
                }
                else if (alteration.AlterationType == (int)AlterationType.Email || alteration.AlterationType == (int)AlterationType.Text || alteration.AlterationType == (int)AlterationType.Numeric)
                {
                    Alterationitem item = new Alterationitem();
                    item.AlterationType = alteration.AlterationType;
                    item.AlterationName = alteration.AlterationName;
                    item.Value = alteration.Value;
                    alterationItems.Add(item);
                }
            }

            orderitem.Alterationitems = alterationItems.ToArray();
            orderitems.Add(orderitem);
            if (alterationOrderitems.Count > 0)
                orderitems.AddRange(alterationOrderitems);

            return orderitems;
        }
    }
}
