﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// CompanyHelper class
    /// </summary>
    public static class CompanyHelper
    {
        /// <summary>
        /// MenuHelperErrors enums
        /// </summary>
        public enum CompanyHelperErrors : int
        {
            CompanyAndOrDeliverypointGroupNotAvailableForMobileOrdering = 200
        }

        public static List<int> GetCompanyIdListForObymobi()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyFields.AvailableOnObymobi == true);
            filter.Add(DeliverypointgroupFields.AvailableOnObymobi == true);

            RelationCollection relations = new RelationCollection();
            relations.Add(CompanyEntityBase.Relations.DeliverypointgroupEntityUsingCompanyId);

            IncludeFieldsList fields = new IncludeFieldsList(CompanyFields.CompanyId);

            CompanyCollection companies = new CompanyCollection();
            companies.GetMulti(filter, 0, null, relations, null, fields, 0, 0);

            return companies.Select(x => x.CompanyId).ToList();
        }

        public static List<int> GetCompanyIdListUsingAccessCodes(List<int> accessCodeIds)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AccessCodeFields.AccessCodeId == accessCodeIds);

            RelationCollection relations = new RelationCollection();
            relations.Add(CompanyEntityBase.Relations.AccessCodeCompanyEntityUsingCompanyId);
            relations.Add(AccessCodeCompanyEntityBase.Relations.AccessCodeEntityUsingAccessCodeId);

            IncludeFieldsList fields = new IncludeFieldsList(CompanyFields.CompanyId);

            CompanyCollection companies = new CompanyCollection();
            companies.GetMulti(filter, 0, null, relations, null, fields, 0, 0);

            return companies.Select(x => x.CompanyId).ToList();
        }

        public static List<int> GetCompanyIdList(List<int> accessCodeIds, AccessCodeType? accessCodeType)
        {
            List<int> companyIds = new List<int>();

            if (!accessCodeType.HasValue || accessCodeIds == null || accessCodeIds.Count == 0)
            {
                // Access code type could not be determined
                // because no access codes were supplied
                companyIds = GetCompanyIdListForObymobi();
            }
            else if (accessCodeType.Value == AccessCodeType.ShowOnly)
            {
                // Access code is of type ShowOnly, which means we are only
                // going to retrieve the companies specified by the access codes
                companyIds = GetCompanyIdListUsingAccessCodes(accessCodeIds);
            }
            else // Access code is of type Show, combine the results
            {
                companyIds = GetCompanyIdListForObymobi();
                companyIds = companyIds.Concat(GetCompanyIdListUsingAccessCodes(accessCodeIds)).ToList();
            }

            return companyIds;
        }

        /// <summary>
        /// Gets the timestamp in ticks for the company list or a specific company for the company-, menu- and/or deliverypoint data.
        /// </summary>
        /// <returns></returns>
        public static long GetCompanyTicksForObymobi(int? companyId, bool companyData, bool menuData, bool deliverypointData)
        {
            List<int> companyIds = new List<int>();
            if (companyId.HasValue)
                companyIds.Add(companyId.Value);

            return GetCompanyTicksForObymobi(companyIds, companyData, menuData, deliverypointData);
        }

        /// <summary>
        /// Gets the timestamp in ticks for the company list or a specific company for the company-, menu- and/or deliverypoint data.
        /// </summary>
        /// <returns></returns>
        public static long GetCompanyTicksForObymobi(List<int> companyIds, bool companyData, bool menuData, bool deliverypointData)
        {
            long companiesTicks = 0;

            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            if (companyIds != null && companyIds.Count > 0)
                filter.Add(CompanyFields.CompanyId == companyIds);

            filter.Add(DeliverypointgroupFields.AvailableOnObymobi == true);

            RelationCollection relations = new RelationCollection();
            relations.Add(CompanyEntity.Relations.DeliverypointgroupEntityUsingCompanyId);

            CompanyCollection companies = new CompanyCollection();

            int size = 0;
            if (companyData) size++;
            if (menuData) size++;
            if (deliverypointData) size++;

            ResultsetFields fields = new ResultsetFields(size);

            int counter = 0;
            if (companyData)
            {
                fields.DefineField(CompanyFields.CompanyDataLastModifiedUTC, counter, AggregateFunction.Max);
                counter++;
            }
            if (menuData)
            {
                fields.DefineField(CompanyFields.MenuDataLastModifiedUTC, counter, AggregateFunction.Max);
                counter++;
            }
            if (deliverypointData)
            {
                fields.DefineField(CompanyFields.DeliverypointDataLastModifiedUTC, counter, AggregateFunction.Max);
                counter++;
            }

            DataTable dynamicList = new DataTable();
            TypedListDAO dao = new TypedListDAO();
            dao.GetMultiAsDataTable(fields, dynamicList, 0, null, filter, relations, true, null, null, 0, 0);

            if (dynamicList != null && dynamicList.Rows != null && dynamicList.Rows.Count == 1 && dynamicList.Rows[0].ItemArray != null && dynamicList.Rows[0].ItemArray.Length == size)
            {
                List<DateTime> dateTimes = new List<DateTime>();
                for (int i = 0; i < dynamicList.Rows[0].ItemArray.Length; i++)
                {
                    object item = dynamicList.Rows[0].ItemArray.GetValue(i);

                    try
                    {
                        dateTimes.Add((DateTime)item);
                    }
                    catch
                    {
                        // only throw on a specific request for a Company
                        if(companyIds != null && companyIds.Count > 0)
                            throw new ObymobiException(CompanyHelperErrors.CompanyAndOrDeliverypointGroupNotAvailableForMobileOrdering, "Company '{0}' or it's deliverypointgroup is not available for mobile ordering.", companyIds[0]);
                    }
                }

                if (dateTimes.Count > 0)
                {
                    DateTime max = dateTimes.Max();
                    companiesTicks = max.Ticks;
                }
            }

            return companiesTicks;
        }

        /// <summary>
        /// Gets a company entity using the specified company id
        /// </summary>
        /// <param name="companyId">The id of the company to get</param>
        /// <returns>A <see cref="CompanyEntity"/> instance</returns>
        public static CompanyEntity GetCompanyEntityByCompanyId(int companyId, bool prefetch = true)
        {
            PrefetchPath path = null;
            if (prefetch)
            {
                path = new PrefetchPath(EntityType.CompanyEntity);
                                
                IPrefetchPathElement prefetchMedia = path.Add(CompanyEntityBase.PrefetchPathMediaCollection);
                prefetchMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
                prefetchMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);

                path.Add(CompanyEntityBase.PrefetchPathDeliverypointgroupCollection);
                path.Add(CompanyEntityBase.PrefetchPathCountryEntity);                
                path.Add(CompanyEntityBase.PrefetchPathTimeZoneEntity);
                path.Add(CompanyEntityBase.PrefetchPathScheduleCollection).SubPath.Add(ScheduleEntityBase.PrefetchPathScheduleitemCollection);
                path.Add(CompanyEntityBase.PrefetchPathCompanyVenueCategoryCollection).SubPath.Add(CompanyVenueCategoryEntityBase.PrefetchPathVenueCategoryEntity).SubPath.Add(VenueCategoryEntityBase.PrefetchPathCustomTextCollection);

                IPrefetchPathElement prefetchTerminal = path.Add(CompanyEntityBase.PrefetchPathTerminalCollection);
                prefetchTerminal.SubPath.Add(TerminalEntityBase.PrefetchPathDeviceEntity);

                path.Add(CompanyEntityBase.PrefetchPathBusinesshoursCollection);
                path.Add(CompanyEntityBase.PrefetchPathMessageTemplateCollection);
                path.Add(CompanyEntityBase.PrefetchPathCompanyAmenityCollection);
                path.Add(CompanyEntityBase.PrefetchPathCloudStorageAccountCollection);
                path.Add(CompanyEntityBase.PrefetchPathCustomTextCollection);
                path.Add(CompanyEntityBase.PrefetchPathCompanyCurrencyCollection);
                path.Add(CompanyEntityBase.PrefetchPathCompanyCultureCollection);
                
                IPrefetchPathElement prefetchAvailabilities = path.Add(CompanyEntityBase.PrefetchPathAvailabilityCollection);
                prefetchAvailabilities.SubPath.Add(AvailabilityEntityBase.PrefetchPathCustomTextCollection);
                prefetchAvailabilities.SubPath.Add(AvailabilityEntityBase.PrefetchPathProductCategoryEntity);
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyFields.CompanyId == companyId);

            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(filter, path);

            if (companyCollection.Count == 0)
                throw new ObymobiException(GetCompanyResult.CompanyIdUnknown, "CompanyId '{0}'", companyId);
            if (companyCollection.Count > 1)
                throw new ObymobiException(GetCompanyResult.MultipleCompaniesFoundForCompanyId, "CompanyId '{0}'", companyId);

            return companyCollection[0];
        }
        
        /// <summary>
        /// Check if a company is in it's business hours, depending on de server's time
        /// </summary>
        /// <param name="companyId">Company to check of</param>
        /// <returns></returns>
        public static bool IsCompanyInBusinessHours(int companyId)
        {
            CompanyEntity company;
            return IsCompanyInBusinessHours(companyId, out company);
        }

        /// <summary>
        /// Check if a company is in it's business hours, depending on de server's time
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="company">The company with prefetched BusinessHours.</param>
        /// <returns>
        ///   <c>true</c> if [is company in business hours] [the specified company id]; otherwise, <c>false</c>.
        /// </returns>/// 
        public static bool IsCompanyInBusinessHours(int companyId, out CompanyEntity company)
        {
            bool toReturn = false;

            // Get the DayOfWeekAndTime string ([DAYOFWEEK][TIME])
            // Examples:
            // Sunday 11:00 = 01100
            // Monday 13:00 = 11300
            // Friday 02:00 (Which is the night after thursday) = 50200
            string DayOfWeekAndTime = ((int)DateTime.Now.DayOfWeek).ToString() + DateTime.Now.ToString("HHmm");

            // Prefetch next moment after current (werkt als een prikklok: dus je hebt prikkers met OPEN en SLUITEN)
            // Het is vrij eenvoudig, als de volgende prikker SLUITEN is, dan is hij geopend, is de volgende prikker OPEN dan is hij gesloten

            // Create and initialize the business hours
            PrefetchPath pathBusinessHours = new PrefetchPath(EntityType.CompanyEntity);

            // Create and initialize the sort expression
            SortExpression sortBusinessHours = new SortExpression(BusinesshoursFields.DayOfWeekAndTime | SortOperator.Ascending);

            // Create and initialize a filter for the business hours
            // Idea, get the next business hours record based on the current day of week and time
            PredicateExpression filterBusinessHours = new PredicateExpression(BusinesshoursFields.DayOfWeekAndTime > DayOfWeekAndTime);

            // Add it to the prefetch path
            pathBusinessHours.Add(CompanyEntity.PrefetchPathBusinesshoursCollection, 1, filterBusinessHours, null, sortBusinessHours);

            // BusinessHour Exceptions
            PredicateExpression filterBusinesshourExceptions = new PredicateExpression();
            filterBusinesshourExceptions.Add(BusinesshoursexceptionFields.FromDateTime <= DateTime.Now);
            filterBusinesshourExceptions.Add(BusinesshoursexceptionFields.TillDateTime >= DateTime.Now);
            pathBusinessHours.Add(CompanyEntity.PrefetchPathBusinesshoursexceptionCollection, 0, filterBusinesshourExceptions);

            company = new CompanyEntity(companyId, pathBusinessHours);

            // First check for exceptions, they overrule
            if (company.BusinesshoursexceptionCollection.Count > 1)
            {
                // ERROR Overlap.
                throw new ObymobiException(BusinesshourCheckResult.OverlappingBusinesshourexceptions, "Error at: {0} (UTC) for company {1}", DateTime.UtcNow, companyId);
            }
            else if (company.BusinesshoursexceptionCollection.Count == 1)
            {
                // Exception
                toReturn = company.BusinesshoursexceptionCollection[0].Opened;
            }
            else
            {
                // Check the regular schedule
                if (company.BusinesshoursCollection.Count == 0)
                {
                    // This happens when the next closing or opening moment is on sunday and it's still saturday
                    // We now need to retrieve the next moment from 00000 (Day 0, 00:00).
                    BusinesshoursCollection businessHours = new BusinesshoursCollection();
                    filterBusinessHours = new PredicateExpression();
                    filterBusinessHours.Add(BusinesshoursFields.DayOfWeekAndTime >= "00000");
                    filterBusinessHours.Add(BusinesshoursFields.CompanyId == companyId);
                    businessHours.GetMulti(filterBusinessHours, 1, sortBusinessHours);
                    if (businessHours.Count != 1)
                        throw new ObymobiException(BusinesshourCheckResult.NoBusinesshoursAvailable, "Error at: {0} (UTC) for company {1}", DateTime.UtcNow, companyId);
                    else
                        toReturn = !businessHours[0].Opening;
                }
                else
                {
                    // The current state of being in business is the oppostie of the next marker of the Business Hours
                    toReturn = !company.BusinesshoursCollection[0].Opening;
                }
            }

            return toReturn;
        }

        /// <summary>
        /// Get a Obymobi.Data.CollectionClasses.CompanyCollection instance using the specified filter
        /// </summary>
        /// <param name="filter">The filter to use when retrieving the collection</param>
        /// <param name="relations">The relations to use when retrieving the collection</param>
        /// <returns>A CompanyCollection instance</returns>
        public static CompanyCollection GetCompanyCollection(PredicateExpression filter, RelationCollection relations, PrefetchPath prefetch)
        {
            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(filter, 0, null, relations, prefetch);

            return companyCollection;
        }

        /// <summary>
        /// Get the POS Connector used by the Company
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public static POSConnectorType GetPOSConnectorType(int companyId, ITransaction transaction)
        {
            if (companyId <= 0)
            {
                return POSConnectorType.Unknown;
            }

            POSConnectorType posConnectorType = POSConnectorType.Unknown;

            CompanyCollection companies = new CompanyCollection();
            PredicateExpression filter = new PredicateExpression(CompanyFields.CompanyId == companyId);

            companies.AddToTransaction(transaction);
            companies.GetMulti(filter);

            if (companies.Count > 0)
            {
                posConnectorType = companies[0].POSConnectorType;
            }

            return posConnectorType;
        }

        public static TimeZoneInfo GetCompanyTimeZone(int companyId)
        {
            TimeZoneInfo ret = TimeZoneInfo.Utc;

            CompanyEntity companyEntity = new CompanyEntity();
            companyEntity.FetchUsingPK(companyId, null, null, new IncludeFieldsList(CompanyFields.TimeZoneOlsonId));

            if (!companyEntity.IsNew)
            {
                ret = companyEntity.TimeZoneInfo;
            }

            return ret;
        }

        public static bool ShouldIncludeDeliveryTimeInOrderNotes(int companyId)
        {
            bool includeDeliveryTimeInOrderNotes = false;

            CompanyEntity companyEntity = new CompanyEntity();
            companyEntity.FetchUsingPK(companyId, null, null, new IncludeFieldsList(CompanyFields.IncludeDeliveryTimeInOrderNotes));

            if (!companyEntity.IsNew)
            {
                includeDeliveryTimeInOrderNotes = companyEntity.IncludeDeliveryTimeInOrderNotes;
            }

            return includeDeliveryTimeInOrderNotes;
        }
    }
}
