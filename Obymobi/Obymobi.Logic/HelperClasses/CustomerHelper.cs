﻿using System;
using System.Collections.Specialized;
using System.IO;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Net.Mail;
using Dionysos.SMS;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Obymobi.Security;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// CustomerHelper class
    /// </summary>
    public static class CustomerHelper
    {
        public enum CustomerHelperResult
        {             
            MissingParameters = 200,
            UsernameIsRequired = 300,
            CustomerIsNotNew = 301,            
            EmailAddressIsRequired = 302,
            NoPasswordTransferHashWasSupplied = 306,
            CustomerSocialmediaAlreadyExistsForTypeAndEmail = 307,
            NoCustomerFound = 308,
            CustomerNotVerified = 309,
            NewEmailEqualsOldEmail = 310

        }

        #region Account create / modification 

        /// <summary>
        /// Creates a new customer.
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="emailAddress"></param>
        /// <param name="passwordTransferHash"></param>
        /// <param name="anonymousAccount"></param>
        /// <param name="deviceType"></param>
        /// <param name="devicePlatform"></param>
        /// <param name="deviceModel"></param>
        /// <returns></returns>
        public static CustomerEntity CreateCustomer(CustomerEntity customer, string emailAddress, string passwordTransferHash, bool anonymousAccount, DeviceType deviceType = DeviceType.Unknown, DevicePlatform devicePlatform = DevicePlatform.Unknown, string deviceModel = "")
        {            
            if ((!customer.IsNew || customer.CustomerId > 0) && !customer.CanSingleSignOn)
                throw new ObymobiEntityException(CustomerHelperResult.CustomerIsNotNew, customer);

            // Without Phonenumber and Email is an Anonymous account created automatically on the Mobile
            if (emailAddress.IsNullOrWhiteSpace())
                throw new ObymobiException(CustomerHelperResult.EmailAddressIsRequired);

            if (passwordTransferHash.IsNullOrWhiteSpace())
                throw new ObymobiException(CustomerHelperResult.NoPasswordTransferHashWasSupplied);

            Transaction t = new Transaction(System.Data.IsolationLevel.ReadCommitted, "CreateCustomer-" + emailAddress);
            try
            {
                // Create customer
                customer.AddToTransaction(t);
                customer.Email = emailAddress;
                customer.CommunicationSalt = Dionysos.RandomUtil.GetRandomLowerCaseString(256);
                customer.PasswordSalt = Dionysos.RandomUtil.GetRandomLowerCaseString(256);
                customer.Password = CustomerPasswordHelper.GetPasswordStorageHash(passwordTransferHash, customer.PasswordSalt);
                customer.AnonymousAccount = anonymousAccount;
                customer.CanSingleSignOn = false;
                customer.InitialLinkIdentifier = Guid.NewGuid().ToString();                
                customer.Save();

                // Create a Device for the Customer
                DeviceEntity device = new DeviceEntity();
                device.AddToTransaction(t);
                device.Identifier = customer.Email;
                device.CustomerId = customer.CustomerId;
                device.Type = deviceType;
                device.DevicePlatformAsEnum = devicePlatform;
                device.Save();

                if (!anonymousAccount)
                {
                    // A real customer just registered himself, email
                    CustomerHelper.SendWelcomeEmail(emailAddress);
                }
                
                // Commit transaction
                t.Commit();
            }
            catch
            {
                t.Rollback();
                throw;
            }
            finally
            {
                t.Dispose();
            }

            return customer;
        }

        public static void SendWelcomeEmail(string email)
        {
            const int timeout = 30000;

            var mailUtil = new MailUtilV2(new System.Net.Mail.MailAddress("no-reply@crave-emenu.com", "Crave World"), new System.Net.Mail.MailAddress(email), "Welcome to Crave World!");
            mailUtil.SmtpClient.Timeout = timeout;
            mailUtil.ImagesSrcRootPath = "~/Templates";

            string templateBody = System.Web.Hosting.HostingEnvironment.MapPath("~/Templates/WelcomeToCraveWorld.html");
            string emailBody = File.ReadAllText(templateBody);
            mailUtil.BodyHtml = emailBody;

            // Prepare variables to merge with
            StringDictionary templateVariables = new StringDictionary();
            templateVariables.Add("EMAIL", email);
            mailUtil.SendMail(templateVariables).Wait(timeout);
        }

        /// <summary>
        /// Creates a real customer from an anonymous customer.
        /// </summary>
        /// <param name="anonCustomer"></param>
        /// <param name="emailAddress"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="passwordTransferHash"></param>
        /// <returns></returns>
        public static CustomerEntity CreateCustomerFromAnonymous(CustomerEntity anonCustomer, string emailAddress, string phoneNumber, string passwordTransferHash)
        {
            if (!StringUtil.IsNullOrWhiteSpace(emailAddress))
                anonCustomer.Email = emailAddress;
            if (!StringUtil.IsNullOrWhiteSpace(phoneNumber))
                anonCustomer.Phonenumber = phoneNumber;

            anonCustomer.Password = CustomerPasswordHelper.GetPasswordStorageHash(passwordTransferHash, anonCustomer.PasswordSalt);
            anonCustomer.AnonymousAccount = false;
            anonCustomer.CanSingleSignOn = false;
            anonCustomer.Save();

            // A real customer just registered himself, email
            CustomerHelper.SendWelcomeEmail(emailAddress);

            // Return the customer entity
            return anonCustomer;
        }

        /// <summary>
        /// Sets the device for the customer.
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="deviceType"></param>
        /// <param name="devicePlatform"></param>
        /// <param name="deviceModel"></param>
        public static void SetDevices(CustomerEntity customer, DeviceType deviceType, DevicePlatform devicePlatform, string deviceModel)
        {
            foreach (var device in customer.DeviceCollection)
            {
                device.Type = deviceType;
                device.DevicePlatformAsEnum = devicePlatform;
                device.Save();
            }
        }

        /// <summary>
        /// Verifies a customer account
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public static bool VerifyCustomer(string identifier, out CustomerEntity customerToReturn)
        {
            bool success = false;

            // Try to get the customer based on its guid
            CustomerEntity customer = CustomerHelper.GetCustomerByInitialLinkIdentifier(identifier);

            if (customer == null)
            { 
                // Try to get the customer based on its NewEmailAddressConfirmationGuid
                customer = CustomerHelper.GetCustomerByNewEmailAddressConfirmationGuid(identifier);

                // Check if a new customer isn't created meanwhile with the same email as the one that needs to be verified
                if (customer != null && !customer.NewEmailAddress.IsNullOrWhiteSpace())
                {
                    if (CustomerHelper.GetCustomerByEmail(customer.NewEmailAddress) != null)                    
                        throw new ObymobiException(VerifyCustomerResult.NewEmailAddressAlreadyExists, "Email address: {0}", customer.NewEmailAddress);                    
                }                
            }
            
            if (customer == null)
                throw new ObymobiException(VerifyCustomerResult.CustomerIdUnknown, "Customer guid: {0}", identifier);
            else if (customer.Blacklisted)
                throw new ObymobiException(VerifyCustomerResult.Blacklisted, "Account is disabled: {0}", customer.BlacklistedNotes);
            else if (customer.AnonymousAccount)
                throw new ObymobiException(VerifyCustomerResult.NoConfirmationForAnonymousAccounts);            
            else if (customer.Verified && customer.NewEmailAddress.IsNullOrWhiteSpace())
                throw new ObymobiException(VerifyCustomerResult.EmailAddressAlreadyVerified, "Customer already verified: {0}", customer.Email);

            Transaction dbTransaction = new Transaction(System.Data.IsolationLevel.ReadCommitted, customer.CustomerId.ToString() + "VerifyCustomer");

            try
            {
                string emailTo = string.Empty;
                
                dbTransaction.Add(customer);

                if (identifier.Equals(customer.NewEmailAddressConfirmationGuid))
                {
                    customer.NewEmailAddressVerified = true;
                    emailTo = customer.NewEmailAddress;
                }
                else
                {
                    customer.Verified = true;
                    customer.InitialLinkIdentifier = null;
                    emailTo = customer.Email;
                }
                customer.Save();

                // Send a welcome email
                CustomerHelper.SendWelcomeEmail(emailTo);
                
                dbTransaction.Commit();

                success = true;
            }
            catch (Exception ex)
            {
                dbTransaction.Rollback();
                throw ex;
            }
            finally
            {
                dbTransaction.Dispose();
            }

            customerToReturn = customer;

            return success;
        }

        /// <summary>
        /// Resets the password, or can be used to validate the reset password identifier
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <param name="passwordTransferHash">The password transfer hash, string.Empty if you want to only validate the request</param>
        /// <param name="resetPasswordLinkIdentifier">The reset password link identifier.</param>
        /// <returns></returns>
        /// <exception cref="ObymobiException">
        /// </exception>
        public static CustomerEntity ResetPassword(int customerId, string passwordTransferHash, string resetPasswordLinkIdentifier)
        {
            CustomerEntity customer = new CustomerEntity(customerId);

            if (customer.IsNew)
            {
                throw new ObymobiException(ResetPasswordResult.CustomerIdUnknown);
            }
            else if (customer.PasswordResetLinkIdentifier.IsNullOrWhiteSpace())
            {
                throw new ObymobiException(ResetPasswordResult.ResetPasswordIdentifierInvalid);
            }
            else if (customer.PasswordResetLinkFailedAtttempts > 5)
            {
                throw new ObymobiException(ResetPasswordResult.TooManyFailedAttempts);
            }
            else if (customer.PasswordResetLinkGeneratedUTC.HasValue && (DateTime.UtcNow - customer.PasswordResetLinkGeneratedUTC.Value).TotalHours > 48)
            {
                throw new ObymobiException(ResetPasswordResult.ResetPasswordIdentifierExpired);
            }
            else if (!customer.PasswordResetLinkIdentifier.Equals(resetPasswordLinkIdentifier))
            {
                customer.PasswordResetLinkFailedAtttempts++;
                customer.Save();
                throw new ObymobiException(ResetPasswordResult.ResetPasswordIdentifierInvalid);
            }
            else if (passwordTransferHash.IsNullOrWhiteSpace())
            {
                // Retrieve customer / validate request
            }
            else
            {                
                // Valid!
                // Reset Salt as well, otherwise someone who would have hacked the account would be able to 
                // continue to use it, since the salt is still valid to call the webservice calls
                customer.PasswordSalt = RandomUtil.GetRandomLowerCaseString(256);                
                customer.Password = CustomerPasswordHelper.GetPasswordStorageHash(passwordTransferHash, customer.PasswordSalt);
                customer.PasswordResetLinkFailedAtttempts = 0;
                customer.PasswordResetLinkGeneratedUTC = null;
                customer.PasswordResetLinkIdentifier = string.Empty;
                customer.Save();
            }

            return customer;
        }

        /// <summary>
        /// Unlocks the account for the specified phonenumber sends an sms containing the password
        /// </summary>
        /// <param name="phonenumber">The phonenumber of the account to unlock</param>
        public static void UnlockAccount(string phonenumber)
        {
            CustomerEntity customer = GetCustomerByPhonenumber(phonenumber);

            if (customer == null)
                throw new ObymobiException(UnlockAccountResult.UnknownPhonenumber);

            if (customer.FailedPasswordAttemptCount < 3)
                throw new ObymobiException(UnlockAccountResult.AccountNotLocked, "Phonenumber: {0}", phonenumber);
            else
            {
                Transaction dbTransaction = new Transaction(System.Data.IsolationLevel.ReadCommitted, customer.CustomerId.ToString() + "UnlockAccount");

                // Save using the transaction, if failed rollback
                try
                {
                    dbTransaction.Add(customer);

                    // Reset the failed password attempt count
                    customer.FailedPasswordAttemptCount = 0;

                    if (customer.Save(true))
                    {
                        MollieSMSRequest smsRequest = new MollieSMSRequest();
                        smsRequest.Originator = "Obymobi";
                        smsRequest.Recipients = customer.Phonenumber;
                        smsRequest.Message = "De blokkering op je Obymobi account is opgeheven. Je kunt vanaf nu weer inloggen en bestellen.";
                        try
                        {
                            smsRequest.Send();
                        }
                        catch (Exception ex)
                        {
                            throw new ObymobiException(UnlockAccountResult.SmsSentFailed, "Result: {0} - {1}\r\nStack: {2}\r\nMessage: {3}\r\n", smsRequest.ResultMessage, smsRequest.ResultCode,
                                ex.StackTrace, ex.Message);
                        }

                        if (smsRequest.ResultCode != 10)
                            throw new ObymobiException(UnlockAccountResult.SmsSentFailed, "Result: {0}, Code: {1}", smsRequest.ResultMessage, smsRequest.ResultCode);
                    }
                    else
                        throw new ObymobiException(GenericWebserviceCallResult.UnspecifiedEntitySaveFalseFailure);

                    dbTransaction.Commit();
                }
                catch (Exception ex)
                {
                    dbTransaction.Rollback();
                    throw ex;
                }
                finally
                {
                    dbTransaction.Dispose();
                }
            }
        }

        #endregion

        #region Retrieval methods

        /// <summary>
        /// Get Customer by Phonenumber
        /// </summary>
        /// <param name="phonenumber">Phonenumber</param>
        /// <returns>Null when not found</returns>
        public static CustomerEntity GetCustomer(string phonenumber)
        {
            PredicateExpression filter = new PredicateExpression(CustomerFields.Phonenumber == phonenumber);
            return LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<CustomerEntity>(filter);
        }        

        /// <summary>
        /// Gets an array of Obymobi.Logic.Model.Customer instances using the specified phonenumber
        /// </summary>
        /// <param name="phonenumber">The phonenumber of the customer</param>
        /// <returns>An array of Customer instances</returns>
        public static Customer[] GetCustomers(string phonenumber)
        {
            Customer[] customerArray = null;

            // Get the customer collection using the specified telephone number
            CustomerCollection customerCollection = GetCustomerCollection(phonenumber, string.Empty);

            // Initialize the array
            customerArray = new Customer[customerCollection.Count];
            for (int i = 0; i < customerCollection.Count; i++)
            {
                CustomerEntity customerEntity = customerCollection[i];
                customerArray[i] = CreateCustomerModelFromEntity(customerEntity);
            }

            return customerArray;
        }

        /// <summary>
        /// Gets an Obymobi.Logic.Model.Customer instances using the specified customer id
        /// </summary>
        /// <param name="customerId">The id of the customer</param>
        /// <returns>A Customer instances</returns>
        public static Customer GetCustomerById(int customerId)
        {
            Customer customer = null;

            PredicateExpression filter = new PredicateExpression(CustomerFields.CustomerId == customerId);
            var entity = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<CustomerEntity>(filter);

            if (entity != null)
                customer = CreateCustomerModelFromEntity(entity);

            return customer;
        }

        /// <summary>
        /// Gets a Obymobi.Data.CollectionClasses.CustomerCollection instance for the specified telephone number
        /// </summary>
        /// <param name="phonenumber">The telephone number to get the CustomerEntity instances for</param>
        /// <param name="emailAddress">The email address.</param>
        /// <returns>
        /// A CustomerCollection instance
        /// </returns>
        private static CustomerCollection GetCustomerCollection(string phonenumber, string emailAddress)
        {
            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();

            if (!phonenumber.IsNullOrWhiteSpace())
                filter.Add(CustomerFields.Phonenumber == phonenumber);

            if (!emailAddress.IsNullOrWhiteSpace())
                filter.Add(CustomerFields.Email % emailAddress);

            if (filter.Count == 0)
                throw new ObymobiException(CustomerHelperResult.MissingParameters);

            return GetCustomerCollection(filter);
        }

        /// <summary>
        /// Gets a Obymobi.Data.CollectionClasses.CustomerCollection instance for the specified telephone number
        /// </summary>
        /// <param name="filter">The filter to use to retrieve the entity collection</param>
        /// <returns>A CustomerCollection instance</returns>
        private static CustomerCollection GetCustomerCollection(PredicateExpression filter)
        {
            // Create and initialize a CustomerCollection instance
            // and retrieve the items using the filter
            CustomerCollection customerCollection = new CustomerCollection();
            customerCollection.GetMulti(filter);

            return customerCollection;
        }

        /// <summary>
        /// Gets a Obymobi.Data.EntityClasses.CustomerEntity instance for the specified customer id
        /// </summary>
        /// <param name="customerId">The id of the customer to get the entity for</param>
        /// <returns>A CustomerEntity instance</returns>
        [Obsolete("This method should not longer be used, all PK fields are now int's, which makes this redundant to the CustomerEntity contructor")]
        private static CustomerEntity GetCustomerEntity(int customerId)
        {
            CustomerEntity customerEntity = null;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(CustomerFields.CustomerId == customerId);

            CustomerCollection customerCollection = GetCustomerCollection(filter);
            if (customerCollection.Count == 1)
                customerEntity = customerCollection[0];
            else if (customerCollection.Count > 1)
                throw new ApplicationException(string.Format("Multiple customers found for customer id '{0}'", customerId));

            return customerEntity;
        }


        /// <summary>
        /// Get a Customer by the phonenumber
        /// </summary>
        /// <param name="phonenumber"></param>
        /// <returns></returns>
        private static CustomerEntity GetCustomerByPhonenumber(string phonenumber)
        {
            PredicateExpression filter = new PredicateExpression(CustomerFields.Phonenumber == phonenumber);
            CustomerEntity c = new CustomerEntity();
            c = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<CustomerEntity>(filter, c);
            return c;
        }

        /// <summary>
        /// Get a Customer by the email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static CustomerEntity GetCustomerByEmail(string email)
        {
            PredicateExpression filter = new PredicateExpression(CustomerFields.Email % email);
            CustomerEntity c = new CustomerEntity();
            c = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<CustomerEntity>(filter, c);
            return c;
        }

        /// <summary>
        /// Get a Customer by the email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static CustomerCollection GetCustomersByEmail(string email)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.AddWithOr(CustomerFields.Email % email);

            PredicateExpression subFilter = new PredicateExpression();
            subFilter.Add(CustomerFields.NewEmailAddress % email);
            subFilter.Add(CustomerFields.NewEmailAddressVerified == true);

            filter.AddWithOr(subFilter);
            
            CustomerCollection customers = new CustomerCollection();
            customers.GetMulti(filter);            
            return customers;
        }

        /// <summary>
        /// Get a Customer by the Guid
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private static CustomerEntity GetCustomerByInitialLinkIdentifier(string identifier)
        {
            PredicateExpression filter = new PredicateExpression(CustomerFields.InitialLinkIdentifier % identifier);
            CustomerEntity c = new CustomerEntity();
            c = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<CustomerEntity>(filter, c);
            return c;
        }

        /// <summary>
        /// Get a Customer by the Guid for it's new email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private static CustomerEntity GetCustomerByNewEmailAddressConfirmationGuid(string guid)
        {
            PredicateExpression filter = new PredicateExpression(CustomerFields.NewEmailAddressConfirmationGuid % guid);
            CustomerEntity c = new CustomerEntity();
            c = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<CustomerEntity>(filter, c);
            return c;
        }

        /// <summary>
        /// Retrieves and verifies an anonymous customer from the database.
        /// </summary>
        /// <param name="email">Email of the anonymous customer</param>
        /// <param name="passwordTransferHash">Hashed password to verify with the password in the DB.</param>
        /// <returns></returns>
        public static CustomerEntity GetAnonymousCustomerByEmail(string email, string passwordTransferHash)
        {
            CustomerEntity anonymousCustomer = CustomerHelper.GetCustomerByEmail(email);

            if (anonymousCustomer != null)
            {
                if (!anonymousCustomer.CanSingleSignOn && !CustomerPasswordHelper.GetPasswordStorageHash(passwordTransferHash, anonymousCustomer.PasswordSalt).Equals(anonymousCustomer.Password))
                    throw new ObymobiException(CreateCustomerResult.AnonymousPasswordInvalid, "Identifier: {0}", email);
            }                            
            return anonymousCustomer;
        }

        #endregion

        #region Model / Entity conversion

        /// <summary>
        /// Create a Obymobi.Logic.Model.Customer instance using the specified Obymobi.Data.EntityClasses.CustomerEntity instance
        /// </summary>
        /// <param name="customerEntity">The CustomerEntity instance to create the Customer instance for</param>
        /// <returns>A Customer instance</returns>
        public static Customer CreateCustomerModelFromEntity(CustomerEntity customerEntity)
        {
            Customer customer = new Customer();
            customer.CustomerId = customerEntity.CustomerId;
            customer.Phonenumber = customerEntity.Phonenumber;
            customer.Password = customerEntity.Password;
            customer.Verified = customerEntity.Verified.ToString();
            customer.Firstname = customerEntity.Firstname;
            customer.Lastname = customerEntity.Lastname;
            customer.LastnamePrefix = customerEntity.LastnamePrefix;
            customer.Email = customerEntity.Email;
            customer.Addressline1 = customerEntity.Addressline1;
            customer.Addressline2 = customerEntity.Addressline2;
            customer.Zipcode = customerEntity.Zipcode;
            customer.City = customerEntity.City;
            customer.Birthdate = customerEntity.Birthdate;
            if (customerEntity.Gender.HasValue)
                customer.GenderSingleLetter = customerEntity.Gender.Value ? "M" : "F";
            else
                customer.GenderSingleLetter = string.Empty;

            return customer;
        }

        #endregion

        #region Customer related data retrievel (favorites, last opened, etc.)

        /// <summary>
        /// Retrieve Recently Visited Companies for a user (Companes where an order was placed in the last 3 months)
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public static CompanyCollection GetRecentlyVisitedCompanies(int customerId)
        {
            CompanyCollection companies = new CompanyCollection();

            RelationCollection relationships = new RelationCollection();
            relationships.Add(OrderEntity.Relations.CompanyEntityUsingCompanyId, JoinHint.Left);

            PredicateExpression orderFilter = new PredicateExpression();
            orderFilter.Add(OrderFields.CustomerId == customerId);
            orderFilter.Add(OrderFields.CreatedUTC >= DateTime.UtcNow.AddMonths(-3));

            SortExpression sort = new SortExpression();
            sort.Add(OrderFields.CreatedUTC | SortOperator.Descending);

            companies.GetMulti(orderFilter, 0, sort, relationships);

            return companies;
        }

        /// <summary>
        /// Retrieve all required objects to query for Recently Ordered Products
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="companyId">companyId</param>
        /// <param name="sort">Sort object to fill</param>
        /// <param name="filter">Filter object to fill</param>
        /// <param name="joins">Join object to fill</param>
        private static void GetRecentlyOrderedProductQueryObjects(int customerId, int companyId, out SortExpression sort, out PredicateExpression filter, out RelationCollection joins)
        {
            // filter orders for current company and customer
            filter = new PredicateExpression();
            filter.Add(OrderFields.CompanyId == companyId);
            filter.Add(OrderFields.CustomerId == customerId);

            // Join required tables
            joins = new RelationCollection();
            joins.Add(ProductEntity.Relations.OrderitemEntityUsingProductId);
            joins.Add(OrderitemEntity.Relations.OrderEntityUsingOrderId);

            // Sort to get most recent 10
            sort = new SortExpression();
            sort.Add(OrderFields.CreatedUTC | SortOperator.Descending);
            sort.Add(ProductFields.Name | SortOperator.Ascending);
        }

        /// <summary>
        /// Get a DbCount of Recently Ordered Products
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="companyId">companyId</param>
        /// <returns></returns>
        public static int GetRecentlyOrderedProductsCount(int customerId, int companyId)
        {
            ProductCollection products = new ProductCollection();
            PredicateExpression filter;
            SortExpression sort;
            RelationCollection joins;
            CustomerHelper.GetRecentlyOrderedProductQueryObjects(customerId, companyId, out sort, out filter, out joins);

            return products.GetDbCount(filter, joins);
        }

        /// <summary>
        /// Get a Collection of Recently Ordered Products
        /// </summary>
        /// <param name="customerId">customerId</param>
        /// <param name="companyId">companyId</param>
        /// <param name="maxProductsToReturn">Max products to return</param>
        /// <returns></returns>
        public static ProductCollection GetRecentlyOrderedProducts(int customerId, int companyId, int maxProductsToReturn)
        {
            // Get recently ordered
            ProductCollection products = new ProductCollection();
            PredicateExpression filter;
            SortExpression sort;
            RelationCollection joins;
            CustomerHelper.GetRecentlyOrderedProductQueryObjects(customerId, companyId, out sort, out filter, out joins);

            products.GetMulti(filter, maxProductsToReturn, sort, joins);

            return products;
        }

        #endregion
    }
}
