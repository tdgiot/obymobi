﻿using Dionysos;
using Dionysos.Drawing;
using Dionysos.Security.Cryptography;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Obymobi.Enums;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// MediaRatioTypeMediaHelper class
    /// </summary>
    public static class MediaRatioTypeMediaHelper
    {
        private const int maxWidth = 1260;

        public enum MediaRatioTypeMediaHelperResult
        {
            FileOfMediaIsNullOrEmtpy = 1,
            SaveEntityCanOnlyBeUsedForSavedMediaRatioTypeMediaEntities = 2
        }

        /// <summary>
        /// Creates a Obymobi.Logic.Model.MediaRatioTypeMedia instance using the specified Obymobi.Data.EntityClasses.MediaRatioTypeMediaEntity instance
        /// </summary>
        /// <param name="mediaRatioTypeMediaEntity">The media ratio type media entity.</param>
        /// <returns>
        /// A MediaRatioTypeMedia instance
        /// </returns>
        public static MediaRatioTypeMedia CreateMediaRatioTypeMediaModelFromEntity(MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity)
        {
            MediaRatioTypeMedia mediaRatioTypeMedia = new MediaRatioTypeMedia();
            mediaRatioTypeMedia.MediaRatioTypeMediaId = mediaRatioTypeMediaEntity.MediaRatioTypeMediaId;
            mediaRatioTypeMedia.MediaId = mediaRatioTypeMediaEntity.MediaId;
            mediaRatioTypeMedia.MediaRatioTypeSystemName = mediaRatioTypeMediaEntity.MediaRatioType.MediaType.ToString();
            mediaRatioTypeMedia.Width = mediaRatioTypeMediaEntity.Width;
            mediaRatioTypeMedia.Height = mediaRatioTypeMediaEntity.Height;

            return mediaRatioTypeMedia;
        }   

        /// <summary>
        /// Resizes / crops the file of the Media and sets it to the MediaRatioTypeMediaEntity.MediaRatioTypeMediaFileEntity
        /// </summary>
        public static void ResizeAndPublishFile(this MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity, MediaEntity mediaEntity, bool saveEntity)
        {
            byte[] fileBytes = mediaEntity.FileBytes;

            if (fileBytes == null || fileBytes.Length == 0)
            {
                throw new ObymobiException(MediaRatioTypeMediaHelper.MediaRatioTypeMediaHelperResult.FileOfMediaIsNullOrEmtpy, "MediaRatioTypeMediaId: {0}, MediaId: {1}", mediaRatioTypeMediaEntity.MediaId, mediaEntity.MediaId);
            }

            if (mediaEntity.MediaFileMd5 == null)
            {
                // For backwards compatability
                mediaEntity.MediaFileMd5 = Hasher.GetHash(fileBytes, HashType.MD5);
                mediaEntity.Save();
            }

            mediaRatioTypeMediaEntity.ResizeAndPublishFile(mediaEntity.MediaFileMd5, saveEntity);
        }

        public static void ResizeAndPublishFile(this MediaRatioTypeMediaEntity mediaRatioTypeMedia, string mediaFileMd5, bool saveEntity, bool forceSave = false)
        {
            if ((saveEntity || forceSave) && mediaRatioTypeMedia.IsNew)
                throw new ObymobiEntityException(MediaRatioTypeMediaHelperResult.SaveEntityCanOnlyBeUsedForSavedMediaRatioTypeMediaEntities, mediaRatioTypeMedia);

            string newMd5;
            if(mediaRatioTypeMedia.MediaEntity.IsPdf())
            {
                // No work needed on the mediaFileMd5 as it's what we need, no crop information has to be added.
                newMd5 = mediaFileMd5;
            }
            else
            {
                // Image
                string dimensions = "t{0}-l{1}-w{2}-h{3}".FormatSafe(mediaRatioTypeMedia.Top, mediaRatioTypeMedia.Left, mediaRatioTypeMedia.Width, mediaRatioTypeMedia.Height);

                // Create a hash of the combined hash of the file and the crop dimensions
                newMd5 = Hasher.GetHash(mediaFileMd5 + dimensions, HashType.MD5);
            }

            MediaRatioTypeMediaFileEntity mediaRatioFile = mediaRatioTypeMedia.MediaRatioTypeMediaFileEntity;
            if (!newMd5.Equals(mediaRatioFile.FileMd5) || forceSave)
            {
                mediaRatioFile.FileMd5 = newMd5; // Field FileMd5 needs to be refactored to MediaRatioTypeMediaEntity

                if (forceSave || mediaRatioFile.IsNew)
                {
                    mediaRatioFile.UpdatedUTC = DateTime.UtcNow;
                    mediaRatioFile.ForceCdnPublication = true;
                }

                if (saveEntity)
                    mediaRatioFile.Save();
            }
        }

        /// <summary>
        /// Resizes / crops the file of the Media and sets it to the MediaRatioTypeMediaEntity.MediaRatioTypeMediaFileEntity
        /// </summary>
        /// <param name="mediaRatioTypeMedia">The media ratio type media.</param>
        /// <param name="saveEntity">if set to <c>true</c> [save entity].</param>
        public static MemoryStream ResizeAndGetFile(this MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity, MediaEntity mediaEntity)
        {
            byte[] fileBytes = mediaEntity.FileBytes;

            if (fileBytes == null || fileBytes.Length == 0)
            {
                throw new ObymobiException(MediaRatioTypeMediaHelper.MediaRatioTypeMediaHelperResult.FileOfMediaIsNullOrEmtpy, "MediaRatioTypeMediaId: {0}, MediaId: {1}", mediaRatioTypeMediaEntity.MediaId, mediaEntity.MediaId);
            }

            MemoryStream toReturn;

            if (mediaEntity.IsPdf() || mediaEntity.IsGif())
            {
                // PDF
                return new MemoryStream(fileBytes);
            }
            else if (mediaRatioTypeMediaEntity.MediaTypeAsEnum == Enums.MediaType.AttachmentImage ||
                mediaRatioTypeMediaEntity.MediaTypeAsEnum == Enums.MediaType.WidgetPdf1280x800)
            { 
                // Image, but no resizing!
                return new MemoryStream(fileBytes);
            }
            else
            {
                // Image, incl. resizing
                using (MemoryStream memoryStream = new MemoryStream(fileBytes))
                using (System.Drawing.Image image = System.Drawing.Image.FromStream(memoryStream))
                {
                    toReturn = MediaRatioTypeMediaHelper.ResizeAndGetFile(mediaRatioTypeMediaEntity, image);
                }
            }

            return toReturn;
        }                

        public static MemoryStream ResizeAndGetFile(this MediaRatioTypeMediaEntity mediaRatioTypeMedia, Image image)
        {
            Rectangle mediaRatioRectangle = mediaRatioTypeMedia.GetCropRectangle();
            var size = new Size(mediaRatioTypeMedia.MediaRatioType.Width, mediaRatioTypeMedia.MediaRatioType.Height);

            int jpgQuality = ObymobiConstants.JpgQualityMedia; // Default: 50
            if (mediaRatioTypeMedia.MediaEntity.JpgQuality.HasValue)
                jpgQuality = mediaRatioTypeMedia.MediaEntity.JpgQuality.Value;            
            else if (mediaRatioTypeMedia.MediaRatioType != null)
                jpgQuality = mediaRatioTypeMedia.MediaRatioType.JpgQuality;

            byte[] bytes = ImageUtil.ResizeImageFile(image, mediaRatioRectangle, size, image.RawFormat, jpgQuality);

            return new MemoryStream(bytes);            
        }
    }
}