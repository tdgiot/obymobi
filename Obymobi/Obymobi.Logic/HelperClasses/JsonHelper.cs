﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections;


namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// JsonHelper class
    /// </summary>
    public class JsonHelper
    {
        const string DOT = ".";

	    readonly object objectToSerialize;
        List<string> propertiesIncluded;
        List<string> propertiesExcluded;
        List<string> cumulativeProperties; //variable to iterate through properties
        PropertiesToSerialize propertiesToSerialize;

        /// <summary>
        /// PropertiesToSerialize enums
        /// </summary>
        public enum PropertiesToSerialize
        {
            /// <summary>
            /// Add all Properties
            /// </summary>
            All,
            /// <summary>
            /// Add all Properties except for the ones explictly excluded
            /// </summary>
            AllExceptExcluded,
            /// <summary>
            /// Add only explictly included properties
            /// </summary>
            OnlyIncluded,
            /// <summary>
            /// Add only properties marked with the IncludeInCodeGeneratorForJavascript-attribute
            /// </summary>
            IncludedForJavascriptOnly
        }

        public static string SerializeObject(object instance, PropertiesToSerialize propertiesToSerialize = PropertiesToSerialize.All)
        {
	        return new JsonHelper(instance).Serialize(propertiesToSerialize);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonHelper"/> class.
        /// </summary>
        /// <param name="instance">The instance.</param>
        public JsonHelper(object instance)
        {
            objectToSerialize = instance;
        }

        /// <summary>
        /// Includes the specified includes.
        /// </summary>
        /// <param name="includes">The includes.</param>
        public void Include(params string[] includes)
        {
            foreach (string str in includes)
                Include(str);
        }

        /// <summary>
        /// Excludes the specified excludes.
        /// </summary>
        /// <param name="excludes">The excludes.</param>
        public void Exclude(params string[] excludes)
        {
            foreach (string str in excludes)
                Exclude(str);
        }

        /// <summary>
        /// Serializes the specified properties to serialize.
        /// </summary>
        /// <param name="propertiesToSerialize">The properties to serialize.</param>
        /// <returns></returns>
        public string Serialize(PropertiesToSerialize properties = PropertiesToSerialize.All)
        {
            this.propertiesToSerialize = properties;

            var sb = new StringBuilder();
            cumulativeProperties = new List<string>();

            var enumerableInstance = this.objectToSerialize as IEnumerable;
            if (enumerableInstance != null)
            {
                sb.Append("[");
                foreach (var item in enumerableInstance)
                {
                    Iterate(item, sb, -1);
                }
                sb.Append("]");
            }
            else
                Iterate(objectToSerialize, sb, -1);

            return sb.ToString();
        }

        /// <summary>
        /// Includes the specified include.
        /// </summary>
        /// <param name="include">The include.</param>
        void Include(string include)
        {
            if (include.Equals(string.Empty))
                return;

            if (propertiesExcluded != null && propertiesExcluded.Contains(include))
                throw new Exception("JSON.Includes: The string [" + include + "] that you are trying to include already exists in the Exclude List");

            if (propertiesIncluded == null)
                propertiesIncluded = new List<string>();

            //if a composed property needs to be included, the hole scale need to be included too
            string[] propertyParts = include.Split('.');
            string propertyCompose = "";

            foreach (string s in propertyParts)
            {
                propertyCompose += s;
                if (!propertiesIncluded.Contains(propertyCompose))
                    propertiesIncluded.Add(propertyCompose);

                propertyCompose += ".";
            }
        }

        /// <summary>
        /// Excludes the specified exclude.
        /// </summary>
        /// <param name="exclude">The exclude.</param>
        void Exclude(string exclude)
        {
            if (exclude.Equals(string.Empty))
                return;

            if (propertiesIncluded != null && propertiesIncluded.Contains(exclude))
                throw new Exception("JSON.Excludes: The string [" + exclude + "] that you are trying to exclude already exists in the Include List");

            if (propertiesExcluded == null)
                propertiesExcluded = new List<string>();

            if (!propertiesExcluded.Contains(exclude))
                propertiesExcluded.Add(exclude);
        }

        /// <summary>
        /// Iterates the specified instance.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <param name="sb">The sb.</param>
        /// <param name="propertyLevel">The property level.</param>
        void Iterate(object instance, StringBuilder sb, int propertyLevel)
        {
            propertyLevel++;
            cumulativeProperties.Add("");

            sb.Append("{");
            foreach (PropertyInfo property in instance.GetType().GetProperties())
            {
                cumulativeProperties[propertyLevel] = property.Name;
                string cumulativePropertiesConcatenated = ConcatListToString(cumulativeProperties);

                switch (propertiesToSerialize)
                {
                    case PropertiesToSerialize.All:
                        ParseProperty(instance, property, sb, propertyLevel);
                        break;
                    case PropertiesToSerialize.AllExceptExcluded:
                        if (propertiesExcluded != null && !propertiesExcluded.Contains(cumulativePropertiesConcatenated))
                        {
                            ParseProperty(instance, property, sb, propertyLevel);
                        }
                        break;
                    case PropertiesToSerialize.OnlyIncluded:
                        if (propertiesIncluded != null && propertiesIncluded.Contains(cumulativePropertiesConcatenated))
                        {
                            ParseProperty(instance, property, sb, propertyLevel);
                        }
                        break;
                    case PropertiesToSerialize.IncludedForJavascriptOnly:
                        if (property.GetCustomAttributes(typeof(Obymobi.Attributes.IncludeInCodeGeneratorForJavascript), false).Length > 0)
                        {
                            ParseProperty(instance, property, sb, propertyLevel);
                        }
                        break;
                }
            }

            if (sb.ToString().EndsWith(","))
                sb.Remove(sb.Length - 1, 1);

            cumulativeProperties.RemoveAt(cumulativeProperties.Count - 1);
            sb.Append("}");
        }

        /// <summary>
        /// Parses the property.
        /// </summary>
        /// <param name="instance">The instance.</param>
        /// <param name="property">The property.</param>
        /// <param name="sb">The sb.</param>
        /// <param name="propertyLevel">The property level.</param>
        void ParseProperty(object instance, PropertyInfo property, StringBuilder sb, int propertyLevel)
        {
            sb.Append('\"' + ReplaceJSONCharacters(property.Name) + "\":");
            object propertyValue = property.GetValue(instance, null);

            //if(property.PropertyType.GetInterface(typeof(ICollection<>).FullName) != null)
            if (IsEnumerable(property.PropertyType))
            {
                sb.Append("[");

                var lista = (IEnumerable)property.GetValue(instance, null);
                if (lista != null)
                {
                    foreach (object obj in lista)
                    {
                        Iterate(obj, sb, propertyLevel);
                        sb.Append(",");
                    }

                    if (sb.ToString().EndsWith(","))
                        sb.Remove(sb.Length - 1, 1);
                }

                sb.Append("]");
            }
            else if (propertyValue != null)
            {
                if (property.PropertyType == typeof(string))
                {
                    sb.Append('\"' + ReplaceJSONCharacters(propertyValue.ToString()) + '\"');
                }
                else if (property.PropertyType == typeof(bool))
                {
                    sb.Append(ReplaceJSONCharacters(propertyValue.ToString().ToLower()));
                }
                else
                {
                    sb.Append(ReplaceJSONCharacters(propertyValue.ToString()));
                }
            }

            sb.Append(",");
        }

        /// <summary>
        /// Determines whether the specified type is enumerable.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        ///   <c>true</c> if the specified type is enumerable; otherwise, <c>false</c>.
        /// </returns>
        bool IsEnumerable(Type type)
        {
            return type.IsGenericType && (typeof(IEnumerable<>).MakeGenericType(type.GetGenericArguments())).IsAssignableFrom(type);
        }

        /// <summary>
        /// Replaces the JSON characters.
        /// </summary>
        /// <param name="cadena">The cadena.</param>
        /// <returns></returns>
        string ReplaceJSONCharacters(string cadena)
        {
            return cadena
            .Replace("\\", "\\\\")
            .Replace("\"", "\\\"")
            .Replace("/", "\\/")
            .Replace("\b", "\\\b")
            .Replace("\f", "\\f")
            .Replace("\n", "\\n")
            .Replace("\r", "\\r")
            .Replace("\t", "\\t");
        }

        /// <summary>
        /// Concats the list to string.
        /// </summary>
        /// <param name="list">The list.</param>
        /// <returns></returns>
        string ConcatListToString(List<string> list)
        {
            return string.Join(DOT, list.ToArray());
        }
    }
}
