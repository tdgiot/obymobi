﻿using DevExpress.XtraScheduler;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Logic.HelperClasses
{
    public static class PriceScheduleHelper
    {
        public static PriceLevelItemEntity GetActivePriceLevelItem(DateTime now, PredicateExpression subFilter, int priceScheduleId)
        {
            PriceLevelItemEntity priceLevelItem = null;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(PriceScheduleItemFields.PriceScheduleId == priceScheduleId);
            filter.Add(subFilter);

            PredicateExpression occurrenceFilter = new PredicateExpression();
            occurrenceFilter.Add(PriceScheduleItemOccurrenceFields.Recurring == true);
            filter.Add(occurrenceFilter);

            PredicateExpression timeFilter = new PredicateExpression();
            timeFilter.Add(PriceScheduleItemOccurrenceFields.StartTime <= now);
            timeFilter.AddWithAnd(PriceScheduleItemOccurrenceFields.EndTime > now);
            occurrenceFilter.AddWithOr(timeFilter);

            RelationCollection relations = new RelationCollection();
            relations.Add(PriceLevelItemEntityBase.Relations.PriceLevelEntityUsingPriceLevelId);
            relations.Add(PriceLevelEntityBase.Relations.PriceScheduleItemEntityUsingPriceLevelId);
            relations.Add(PriceScheduleItemEntityBase.Relations.PriceScheduleItemOccurrenceEntityUsingPriceScheduleItemId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.PriceLevelItemEntity);
            IPrefetchPathElement prefetchPriceLevel = prefetch.Add(PriceLevelItemEntityBase.PrefetchPathPriceLevelEntity);
            IPrefetchPathElement prefetchPriceScheduleItem = prefetchPriceLevel.SubPath.Add(PriceLevelEntityBase.PrefetchPathPriceScheduleItemCollection);
            IPrefetchPathElement prefetchPriceScheduleItemOccurrence = prefetchPriceScheduleItem.SubPath.Add(PriceScheduleItemEntityBase.PrefetchPathPriceScheduleItemOccurrenceCollection);

            PriceLevelItemCollection priceLevelItemCollection = new PriceLevelItemCollection();
            priceLevelItemCollection.GetMulti(filter, 0, null, relations, prefetch);

            foreach (PriceLevelItemEntity priceLevelItemEntity in priceLevelItemCollection)
            {
                if (PriceScheduleHelper.GetPriceLevelItemExistsInOccurrence(priceLevelItemEntity, occurrenceFilter, now, priceScheduleId))
                {
                    priceLevelItem = priceLevelItemEntity;
                    break;
                }
            }

            return priceLevelItem;
        }

        private static bool GetPriceLevelItemExistsInOccurrence(PriceLevelItemEntity priceLevelItemEntity, PredicateExpression filter, DateTime now, int priceSheduleId)
        {
            bool found = false;
            foreach (PriceScheduleItemEntity priceScheduleItemEntity in priceLevelItemEntity.PriceLevelEntity.PriceScheduleItemCollection)
            {
                if (priceScheduleItemEntity.PriceScheduleId != priceSheduleId)
                    continue;

                EntityView<PriceScheduleItemOccurrenceEntity> priceScheduleItemOccurrenceView = priceScheduleItemEntity.PriceScheduleItemOccurrenceCollection.DefaultView;
                priceScheduleItemOccurrenceView.Filter = filter;

                foreach (PriceScheduleItemOccurrenceEntity priceScheduleItemOccurrenceEntity in priceScheduleItemOccurrenceView)
                {
                    if (!priceScheduleItemOccurrenceEntity.Recurring)
                    {
                        found = true;
                    }
                    else
                    {
                        Appointment appointment = new DevExpressAppointment(AppointmentType.Pattern);
                        appointment.Start = priceScheduleItemOccurrenceEntity.StartTime.Value;
                        appointment.End = priceScheduleItemOccurrenceEntity.EndTime.Value;
                        appointment.RecurrenceInfo.Type = (RecurrenceType)priceScheduleItemOccurrenceEntity.RecurrenceType;
                        appointment.RecurrenceInfo.Range = (RecurrenceRange)priceScheduleItemOccurrenceEntity.RecurrenceRange;
                        appointment.RecurrenceInfo.Start = priceScheduleItemOccurrenceEntity.RecurrenceStart.Value;
                        appointment.RecurrenceInfo.End = priceScheduleItemOccurrenceEntity.RecurrenceEnd.Value;
                        appointment.RecurrenceInfo.OccurrenceCount = priceScheduleItemOccurrenceEntity.RecurrenceOccurrenceCount;
                        appointment.RecurrenceInfo.Periodicity = priceScheduleItemOccurrenceEntity.RecurrencePeriodicity;
                        appointment.RecurrenceInfo.DayNumber = priceScheduleItemOccurrenceEntity.RecurrenceDayNumber;
                        appointment.RecurrenceInfo.WeekDays = (WeekDays)priceScheduleItemOccurrenceEntity.RecurrenceWeekDays;
                        appointment.RecurrenceInfo.WeekOfMonth = (WeekOfMonth)priceScheduleItemOccurrenceEntity.RecurrenceWeekOfMonth;
                        appointment.RecurrenceInfo.Month = priceScheduleItemOccurrenceEntity.RecurrenceMonth;

                        TimeInterval nowInterval = new TimeInterval(now, TimeSpan.FromMilliseconds(1));

                        OccurrenceCalculator occurrenceCalculator = OccurrenceCalculator.CreateInstance(appointment.RecurrenceInfo);
                        AppointmentBaseCollection occurrences = occurrenceCalculator.CalcOccurrences(nowInterval, appointment);
                        if (occurrences.Count > 0)
                        {
                            found = true;
                        }
                    }

                    if (found)
                        break;
                }

                if (found)
                    break;
            }
            return found;
        }
    }
}
