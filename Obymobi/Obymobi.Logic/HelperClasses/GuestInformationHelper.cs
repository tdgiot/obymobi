﻿using System;
using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Linq;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Logic.HelperClasses
{
    public static class GuestInformationHelper
    {
        public static GuestInformationCollection GetGuestInformationsForMessagegroup(int companyId, MessagegroupType type, DateTime date, string groupName, IncludeFieldsList includes)
        {
            GuestInformationCollection guestInformations = new GuestInformationCollection();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(GuestInformationFields.CompanyId == companyId);

            if (type == MessagegroupType.Checkin || type == MessagegroupType.Checkout)
            {
                DateTime localBeginOfDay = date.MakeBeginOfDay();
                DateTime localEndOfDay = date.MakeEndOfDay();

                DateTime utcBeginOfDay = TimeZoneInfo.ConvertTimeToUtc(localBeginOfDay);
                DateTime utcEndOfDay = TimeZoneInfo.ConvertTimeToUtc(localEndOfDay);

                if (type == MessagegroupType.Checkin)
                {
                    filter = GuestInformationHelper.ApplyCheckinFilter(filter, utcBeginOfDay, utcEndOfDay);
                }
                else
                {
                    filter = GuestInformationHelper.ApplyCheckoutFilter(filter, utcBeginOfDay, utcEndOfDay);
                }
            }
            else if (type == MessagegroupType.Group)
            {
                filter = GuestInformationHelper.ApplyGroupNameFilter(filter, groupName);
            }
            else
            {
                filter = null;
            }

            if (filter != null)
            {
                guestInformations.GetMulti(filter, includes, 0);
            }

            return guestInformations;
        }

        private static PredicateExpression ApplyCheckinFilter(PredicateExpression filter, DateTime beginOfDay, DateTime endOfDay)
        {            
            filter.Add(GuestInformationFields.CheckInUTC >= beginOfDay);
            filter.Add(GuestInformationFields.CheckInUTC <= endOfDay);
            return filter;
        }

        private static PredicateExpression ApplyCheckoutFilter(PredicateExpression filter, DateTime beginOfDay, DateTime endOfDay)
        {
            filter.Add(GuestInformationFields.CheckOutUTC >= beginOfDay);
            filter.Add(GuestInformationFields.CheckOutUTC <= endOfDay);
            return filter;
        }

        private static PredicateExpression ApplyGroupNameFilter(PredicateExpression filter, string groupName)
        {
            filter.Add(new FieldLikePredicate(GuestInformationFields.GroupName, string.Format("%{0}%", groupName)));
            return filter;
        }

        public static IEnumerable<string> GetGroupNamesForCompany(int companyId)
        {
            PredicateExpression filter = new PredicateExpression(GuestInformationFields.CompanyId == companyId);
            filter.Add(GuestInformationFields.GroupName != DBNull.Value);
            filter.Add(GuestInformationFields.GroupName != string.Empty);

            IncludeFieldsList includes = new IncludeFieldsList(GuestInformationFields.GroupName);

            SortExpression sort = new SortExpression(new SortClause(GuestInformationFields.GroupName, SortOperator.Ascending));

            GuestInformationCollection guests = new GuestInformationCollection();
            guests.GetMulti(filter, 0, sort, null, null, includes, 0, 0);

            return guests.Select(x => x.GroupName).Distinct();
        }
    }
}
