﻿using Dionysos;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Logic.HelperClasses
{
    public class CompanyCultureHelper
    {
        public static CompanyCultureEntity CreateEntity(int companyId, string cultureCode)
        {
            if (cultureCode.IsNullOrWhiteSpace())
            {
                return null;
            }

            CompanyCultureEntity entity = new CompanyCultureEntity();
            entity.CultureCode = cultureCode;
            entity.CompanyId = companyId;

            return entity;
        }

        public static CompanyCultureCollection GetCompanyCulturesForCompany(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyCultureFields.CompanyId == companyId);

            CompanyCultureCollection cultures = new CompanyCultureCollection();
            cultures.GetMulti(filter, 0, new SortExpression(CompanyCultureFields.CultureCode | SortOperator.Ascending));

            return cultures;
        }

        public static Obymobi.Culture GetCultureByCompanyLanguageCode(int companyId, string languageCode)
        {
            Obymobi.Culture culture = CompanyCultureHelper.GetCultureByLanguageCode(languageCode);
            return CompanyCultureHelper.GetCompanySpecificCulture(companyId, culture);
        }

        private static Obymobi.Culture GetCompanySpecificCulture(int companyId, Obymobi.Culture culture)
        {
            if (culture == Obymobi.Culture.English_United_Kingdom)
            {
                if (companyId > 0)
                {
                    if (CompanyCultureHelper.UnitedStatesCompanies.Contains(companyId))
                    {
                        culture = Obymobi.Culture.English_United_States;
                    }
                    else if (CompanyCultureHelper.AustralianCompanies.Contains(companyId))
                    {
                        culture = Obymobi.Culture.English_Australia;
                    }
                    else if (CompanyCultureHelper.CaribbeanCompanies.Contains(companyId))
                    {
                        culture = Obymobi.Culture.English_Caribbean;
                    }
                }
            }
            else if (culture == Obymobi.Culture.Spanish_Spain)
            {
                if (companyId > 0)
                {
                    if (CompanyCultureHelper.PanamaCompanies.Contains(companyId))
                    {
                        culture = Obymobi.Culture.Spanish_Panama;
                    }
                }
            }

            return culture;
        }

        public static Obymobi.Culture GetCultureByLanguageCode(string languageCode)
        {
            languageCode = languageCode.ToUpperInvariant();
            switch (languageCode)
            {
                case "NL":
                    return Obymobi.Culture.Dutch_Netherlands;
                case "EN":
                    return Obymobi.Culture.English_United_Kingdom;
                case "DA":
                    return Obymobi.Culture.Danish_Denmark;
                case "DE":
                    return Obymobi.Culture.German_Germany;
                case "FR":
                    return Obymobi.Culture.French_France;
                case "ES":
                    return Obymobi.Culture.Spanish_Spain;
                case "ZH":
                    return Obymobi.Culture.Chinese_Simplified_China;
                case "AR":
                    return Obymobi.Culture.Arabic_United_Arab_Emirates;
                case "DV":
                    return Obymobi.Culture.Divehi_Maldives;
                case "TH":
                    return Obymobi.Culture.Thai_Thailand;
                case "PT":
                    return Obymobi.Culture.Portuguese_Portugal;
                case "JA":
                    return Obymobi.Culture.Japanese_Japan;
                case "RU":
                    return Obymobi.Culture.Russian_Russia;
                case "US":
                    return Obymobi.Culture.English_United_States;
                default:
                    throw new Exception(string.Format("LanguageCode {0} is not mapped. Add it to the list", languageCode));
                    break;
            }
        }

        private static readonly List<int> UnitedStatesCompanies = new List<int>()
        {
            245, // Print HotelX
            263, // Aria Cabana
            285, // The Hyde Los Angeles
            286, // Montage Beverly Hills
            309, // The Mirage Hotel
            332, // The Hyde New York
            336, // Montage Laguna Beach
            337, // Delete this company
            343, // Aria
            346, //	Bellagio Las Vegas
            347, //	MGM Grand
            348, //	Beau Rivage
            349, //	Excalibur
            350, //	Vdara
            351, //	Mandalay Bay
            352, //	Delano Las Vegas
            353, //	Monte Carlo
            354, //	New York New York
            355, //	Luxor
            356, //	The Mirage
            357, //	Secrets Maroma Beach
            370, //	Marina Del Rey Hotel
            371, //	Secrets Papagayo
            372, //	Caesars Palace
            373, //	Breathless Cabo San Lucas
            374, //	Mayton Inn
            376, //	Secrets Akumal
            399, //	Zoetry Paraiso de la Bonita
            403, //	Secrets Puerto Los Cabos
            404, //	xAriaCopy
            407, //	Marriott
            408, //	Breathless Riviera Cancun            
        };

        private static readonly List<int> AustralianCompanies = new List<int>()
        {
            340, //	The Hyde Australia
        };

        private static readonly List<int> CaribbeanCompanies = new List<int>()
        {
            292, //	Sandals Montego Bay
            344, //	The Bodyholiday
            377, //	xXDelete
            397, //	AM Resorts Demo
            398, //	XXDeletex
        };

        private static readonly List<int> PanamaCompanies = new List<int>()
        {
            400, //	Secrets Playa Bonita Panama
        };        
    }
}