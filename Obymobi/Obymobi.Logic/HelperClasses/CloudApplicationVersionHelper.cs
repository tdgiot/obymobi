﻿using Dionysos.Data.LLBLGen;
using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    public class CloudApplicationVersionHelper
    {
        /// <summary>
        /// Sets the version of the specified cloud application in the database.
        /// </summary>
        /// <param name="application">The cloud application to set the version for.</param>
        /// <param name="versionNumber">The version number to set.</param>
        public static void SetCloudApplicationVersion(CloudApplication application, VersionNumber versionNumber)
        {
            CloudApplicationVersionEntity cloudApplicationVersionEntity = null;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(CloudApplicationVersionFields.CloudApplication == application);
            filter.Add(CloudApplicationVersionFields.MachineName == Environment.MachineName);

            CloudApplicationVersionCollection cloudApplicationVersions = EntityCollection.GetMulti<CloudApplicationVersionCollection>(filter);
            if (cloudApplicationVersions.Count == 0)
            {
                cloudApplicationVersionEntity = new CloudApplicationVersionEntity();
                cloudApplicationVersionEntity.CloudApplication = application;
                cloudApplicationVersionEntity.MachineName = Environment.MachineName;
            }
            else if (cloudApplicationVersions.Count == 1)
            {
                cloudApplicationVersionEntity = cloudApplicationVersions[0];
            }

            if (cloudApplicationVersionEntity != null && versionNumber != null && versionNumber.HasVersionNumber && versionNumber.Number != cloudApplicationVersionEntity.Version)
            {
                cloudApplicationVersionEntity.Version = versionNumber.Number;
                cloudApplicationVersionEntity.Save();
            }
        }

        /// <summary>
        /// Sets the version of the specified cloud application in the database.
        /// </summary>
        /// <param name="application">The cloud application to set the version for.</param>
        /// <param name="versionNumber">The version number to set.</param>
        public static void SetCloudApplicationVersion(CloudApplication application, string versionNumber)
        {
            CloudApplicationVersionHelper.SetCloudApplicationVersion(application, new VersionNumber(versionNumber));
        }

        /// <summary>
        /// Sets the version of the specified cloud application in the database.
        /// </summary>
        /// <param name="application">The cloud application to set the version for.</param>
        /// <param name="versionNumber">The version number to set.</param>
        public static void SetCloudApplicationVersion(CloudApplication application, int versionNumber)
        {
            CloudApplicationVersionHelper.SetCloudApplicationVersion(application, new VersionNumber(versionNumber));
        }
    }
}
