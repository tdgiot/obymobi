﻿using System;
using System.Linq;
using System.Text;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// PosalterationoptionHelper class
    /// </summary>
    public class PosalterationoptionHelper
    {
        /// <summary>
        /// Cleans up.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="cleanupReport">The cleanup report.</param>
        /// <param name="batchId">The batch id.</param>
        public static void CleanUp(int companyId, ref StringBuilder cleanupReport, out long batchId)
        {
            cleanupReport.AppendLine("*** START: Delete Posalterationoption entities ***");
            batchId = -1;
            // Prepate filter for company
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationoptionFields.CompanyId == companyId);

            // Create collection and use for Scalar MAX
            PosalterationoptionCollection paos = new PosalterationoptionCollection();
            object maxBatchId = paos.GetScalar(Data.PosalterationoptionFieldIndex.SynchronisationBatchId, null, AggregateFunction.Max, filter);

            // Only proceed when we have an actual maxBatchId
            if (maxBatchId != DBNull.Value && maxBatchId is long)
            {
                // Retrieve all order posalterationoptions & delete them
                cleanupReport.AppendFormatLine("BatchId: {0}", maxBatchId);
                batchId = (long)maxBatchId;
                // Prepare filter for older posalterationoptions
                filter = new PredicateExpression();
                filter.Add(PosalterationoptionFields.CompanyId == companyId);
                filter.Add(PosalterationoptionFields.SynchronisationBatchId < maxBatchId);

                paos.GetMulti(filter);

                // Delete per piece
                foreach (var pao in paos)
                {
                    cleanupReport.AppendFormatLine("ExternalId: {0}, Name: {1}",
                        pao.ExternalId, pao.Name);
                    pao.Delete();
                }
                cleanupReport.AppendFormatLine("{0} entities deleted", paos.Count);
            }
            else
                cleanupReport.AppendLine("No valid batch found");

            cleanupReport.AppendLine("*** FINISHED: Delete Posalterationoption entities ***");
        }

        /// <summary>
        /// Synchronizes the alterationoptions from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="xml">The Xml string containing the serialized array of Posalterationoption instances</param>
        /// <param name="companyId">The id of the company to import the pos alterationoptions for</param>
        /// <param name="batchId"> </param>
        public static void SynchronizePosalterationoptions(string xml, int companyId, long batchId)
        {
            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPosalterationoptionsResult.XmlIsEmpty, "Xml string containing serialized Posalterationoption array is empty.");
            if (companyId <= 1)
                throw new ObymobiException(ImportPosalterationoptionsResult.CompanyIdIsZeroOrLess, "The specified company id '{0}' is smaller than 1.", companyId);

            // Deserialize the xml in order to get the array of Posalterationoption instances
            Posalterationoption[] posalterationoptions = XmlHelper.Deserialize<Posalterationoption[]>(xml);

            SynchronizePosalterationoptions(posalterationoptions, companyId, batchId);
        }

        /// <summary>
        /// Synchronizes the alterationoptions from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="posalterationoptions">The posalterationoptions.</param>
        /// <param name="companyId">The id of the company to import the pos alterationoptions for</param>
        /// <param name="batchId"> </param>
        public static void SynchronizePosalterationoptions(Posalterationoption[] posalterationoptions, int companyId, long batchId, string revenueCenter = null)
        {
            // Get a PosalterationoptionCollection for the specified company
            PosalterationoptionCollection posalterationoptionCollection = GetPosalterationoptionCollection(companyId);

            // Create an EntityView for the PosalterationoptionCollection
            EntityView<PosalterationoptionEntity> posalterationoptionView = posalterationoptionCollection.DefaultView;

            // Populate the list of external id's
            // This list is used for removing removed POS options from our database
            //var query = from c in posalterationoptionView select c.ExternalId;
            //List<string> posalterationoptionExternalIdList = query.ToList();

            // Walk through the Posalterationoption instances of the array
            for (int i = 0; i < posalterationoptions.Length; i++)
            {
                Posalterationoption posalterationoption = posalterationoptions[i];

                // Create and initialize the filter
                var filter = new PredicateExpression();
                filter.Add(PosalterationoptionFields.ExternalId == posalterationoption.ExternalId);
                filter.Add(PosalterationoptionFields.CompanyId == companyId);

                if (!string.IsNullOrWhiteSpace(revenueCenter))
                {
                    filter.Add(PosalterationoptionFields.RevenueCenter == revenueCenter);
                }

                // Set the filter to the EntityView
                posalterationoptionView.Filter = filter;

                // Check whether items have been found
                PosalterationoptionEntity posalterationoptionEntity = null;
                posalterationoption.CompanyId = companyId;
                if (posalterationoptionView.Count == 0)
                {
                    // No pos alterationoption found in database, add
                }
                else if (posalterationoptionView.Count == 1)
                {
                    // Pos alterationoption found in database, update
                    posalterationoptionEntity = posalterationoptionView[0];

                    // Option still exists on POS, remove it form the ExternalIdList
                    //posalterationoptionExternalIdList.Remove(posalterationoption.ExternalId);
                }
                else if (posalterationoptionView.Count > 1)
                {
                    // Multiple pos alterationoptions found in database for the specified external id, error!
                    throw new ObymobiException(ImportPosalterationoptionsResult.MultiplePosalterationoptionsFound, "Multiple pos alterationoptions found for the specified external id '{0}'.", posalterationoption.ExternalId);
                }

                // Update Fields Values
                posalterationoptionEntity = CreateUpdatePosalterationoptionEntityFromModel(posalterationoption, posalterationoptionEntity);

                try
                {
                    bool wasNew = posalterationoptionEntity.IsNew;

                    // Set Created or Updated batch to this batch.
                    if (wasNew)
                        posalterationoptionEntity.CreatedInBatchId = batchId;
                    else if (posalterationoptionEntity.IsDirty)
                        posalterationoptionEntity.UpdatedInBatchId = batchId;

                    // Make this backwards compatible by filling it anyway.
                    if (!posalterationoptionEntity.CreatedInBatchId.HasValue)
                        posalterationoptionEntity.CreatedInBatchId = batchId;

                    posalterationoptionEntity.SynchronisationBatchId = batchId;

                    // If the POS has a brackish config and is missing alteration options
                    // Check if the name is set, otherwise put set to UNKNOWN with the external option id
                    if (string.IsNullOrEmpty(posalterationoptionEntity.Name))
                        posalterationoptionEntity.Name = "UNKNOWN - " + posalterationoption.ExternalId;

                    if (posalterationoptionEntity.Save())
                    {
                        if (wasNew)
                        {
                            posalterationoptionEntity.Refetch();
                            posalterationoptionView.RelatedCollection.Add(posalterationoptionEntity);
                        }
                    }
                    else
                        throw new ObymobiException(GenericResult.EntitySaveException, "Save() returned false");
                }
                catch (Exception ex)
                {
                    throw new ObymobiException(GenericResult.EntitySaveException, ex);
                }
            }
        }

        /// <summary>
        /// Gets the external id of the pos alteration option for the specified alteration option id
        /// </summary>
        /// <param name="alterationoptionId">The id of the alteration option to get the pos alteration option id for</param>
        /// <returns>A System.String instance containing the external id</returns>
        public static string GetPosalterationoptionExternalId(int alterationoptionId)
        {
            string externalId = string.Empty;

            var filter = new PredicateExpression();
            filter.Add(AlterationoptionFields.AlterationoptionId == alterationoptionId);

            var relations = new RelationCollection();
            relations.Add(AlterationoptionEntity.Relations.PosalterationoptionEntityUsingPosalterationoptionId);

            PosalterationoptionCollection posalterationoptionCollection = new PosalterationoptionCollection();
            posalterationoptionCollection.GetMulti(filter, relations);

            if (posalterationoptionCollection.Count == 0)
            {
                // No pos alteration found, error
                throw new ObymobiException(GetPosalterationoptionExternalIdResult.NoPosalterationoptionFound, "No pos alteration options found for alteration option id '{0}'.", alterationoptionId);
            }
            
            if (posalterationoptionCollection.Count == 1)
            {
                // Pos alteration found
                externalId = posalterationoptionCollection[0].ExternalId;
            }
            else if (posalterationoptionCollection.Count > 1)
            {
                // Multiple pos alterations found, error
                throw new ObymobiException(GetPosalterationoptionExternalIdResult.MultiplePosalterationoptionsFound, "Multiple pos alteration options found for alteration option id '{0}'.", alterationoptionId);
            }

            return externalId;
        }

        /// <summary>
        /// Gets the alterationoption by posalterationoption id or a new one if none was found.
        /// </summary>
        /// <param name="posalterationoptionId">The posalterationoption id.</param>
        /// <param name="alterationoptionView"> </param>
        /// <returns>A (new) <see cref="AlterationoptionEntity"/> instance</returns>
        public static AlterationoptionEntity GetAlterationoptionByPosalterationoptionId(int posalterationoptionId, EntityView<AlterationoptionEntity> alterationoptionView)
        {
            // Create the AlterationoptionEntity instance
            AlterationoptionEntity alterationoption = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationoptionFields.PosalterationoptionId == posalterationoptionId);

            // Set the filter to the alterationoption view
            alterationoptionView.Filter = filter;

            // Check whether a alterationoption was found
            if (alterationoptionView.Count == 0)
                alterationoption = new AlterationoptionEntity();
            else if (alterationoptionView.Count == 1)
                alterationoption = alterationoptionView[0];
            else if (alterationoptionView.Count > 1)
                throw new ObymobiException(SynchronizeAlterationoptionsResult.MultipleAlterationoptionsFound, "Multiple alterationoptions found for the pos alterationoption id '{0}'.", posalterationoptionId);

            return alterationoption;
        }

        /// <summary>
        /// Gets the posalterationoption by external id or a new one if none was found.
        /// </summary>
        /// <param name="externalId">The external id.</param>
        /// <param name="posalterationoptionView">The posalterationoption view.</param>
        /// <returns>
        /// A (new) <see cref="PosalterationoptionEntity"/> instance
        /// </returns>
        public static PosalterationoptionEntity GetPosalterationoptionByExternalId(string externalId, EntityView<PosalterationoptionEntity> posalterationoptionView)
        {
            // Create the PosalterationoptionEntity instance
            PosalterationoptionEntity posalterationoption = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationoptionFields.ExternalId == externalId);

            // Set the filter to the POS alterationoption view
            posalterationoptionView.Filter = filter;

            // Check whether a POS alterationoption was found
            if (posalterationoptionView.Count == 0)
                posalterationoption = new PosalterationoptionEntity();
            else if (posalterationoptionView.Count == 1)
                posalterationoption = posalterationoptionView[0];
            else if (posalterationoptionView.Count > 1)
                throw new ObymobiException(ImportPosalterationoptionsResult.MultiplePosalterationoptionsFound, "Multiple pos alteration options found for external id '{0}'.", externalId);

            return posalterationoption;
        }

        /// <summary>
        /// Gets a PosalterationoptionCollection for the specified company id
        /// </summary>
        /// <param name="companyId">The id of the company to get the PosalterationoptionCollection for</param>
        /// <returns>An Obymobi.Data.PosalterationoptionCollection instance</returns>
        public static PosalterationoptionCollection GetPosalterationoptionCollection(int companyId, string revenueCenter = null)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationoptionFields.CompanyId == companyId);

            if (!string.IsNullOrWhiteSpace(revenueCenter))
            {
                filter.Add(PosalterationoptionFields.RevenueCenter == revenueCenter);
            }

            PosalterationoptionCollection posalterationoptionCollection = new PosalterationoptionCollection();
            posalterationoptionCollection.GetMulti(filter);

            return posalterationoptionCollection;
        }

        /// <summary>
        /// Creates the posalterationoption model from entity.
        /// </summary>
        /// <param name="posalterationoptionEntity">The posalterationoption entity.</param>
        /// <returns></returns>
        public static Posalterationoption CreatePosalterationoptionModelFromEntity(PosalterationoptionEntity posalterationoptionEntity)
        {
            Posalterationoption posalterationoptionModel = new Posalterationoption();

            posalterationoptionModel.PosalterationoptionId = posalterationoptionEntity.PosalterationoptionId;
            posalterationoptionModel.CompanyId = posalterationoptionEntity.CompanyId;
            posalterationoptionModel.ExternalId = posalterationoptionEntity.ExternalId;
            posalterationoptionModel.PosproductExternalId = posalterationoptionEntity.PosproductExternalId;
            posalterationoptionModel.Name = posalterationoptionEntity.Name;
            posalterationoptionModel.RevenueCenter = posalterationoptionEntity.RevenueCenter;

            posalterationoptionModel.PriceIn = posalterationoptionEntity.PriceIn;

            posalterationoptionModel.Description = posalterationoptionEntity.Description;
            posalterationoptionModel.FieldValue1 = posalterationoptionEntity.FieldValue1;
            posalterationoptionModel.FieldValue2 = posalterationoptionEntity.FieldValue2;
            posalterationoptionModel.FieldValue3 = posalterationoptionEntity.FieldValue3;
            posalterationoptionModel.FieldValue4 = posalterationoptionEntity.FieldValue4;
            posalterationoptionModel.FieldValue5 = posalterationoptionEntity.FieldValue5;
            posalterationoptionModel.FieldValue6 = posalterationoptionEntity.FieldValue6;
            posalterationoptionModel.FieldValue7 = posalterationoptionEntity.FieldValue7;
            posalterationoptionModel.FieldValue8 = posalterationoptionEntity.FieldValue8;
            posalterationoptionModel.FieldValue9 = posalterationoptionEntity.FieldValue9;
            posalterationoptionModel.FieldValue10 = posalterationoptionEntity.FieldValue10;

            posalterationoptionModel.RevenueCenter = posalterationoptionEntity.RevenueCenter;

            return posalterationoptionModel;
        }

        /// <summary>
        /// Gets the posalterationoptions.
        /// </summary>
        /// <param name="posalterationEntity">The posalteration entity.</param>
        /// <returns></returns>
        public static PosalterationoptionCollection GetPosalterationoptions(PosalterationEntity posalterationEntity)
        {
            PosalterationitemCollection posalterationItems = PosalterationitemHelper.GetPosalterationitems(posalterationEntity);
            return GetPosalterationoptions(posalterationItems);
        }

        /// <summary>
        /// Gets the posalterationoptions.
        /// </summary>
        /// <param name="posalterationitemCollection">The posalterationitem collection.</param>
        /// <returns></returns>
        public static PosalterationoptionCollection GetPosalterationoptions(PosalterationitemCollection posalterationitemCollection)
        {
            // GK To stage retrieval, could be done with custom joins.
            var posalterationoptionExternalIds = from pai in posalterationitemCollection select pai.ExternalPosalterationoptionId;

            PosalterationoptionCollection posalterationoptions = new PosalterationoptionCollection();
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationoptionFields.ExternalId == posalterationoptionExternalIds.ToList());
            posalterationoptions.GetMulti(filter);

            return posalterationoptions;
        }

        /// <summary>
        /// Creates a PosalterationoptionEntity instance for the specified Posalterationoption instance
        /// </summary>
        /// <param name="posalterationoption">The Posalterationoption instance to create the PosalterationoptionEntity instance for</param>
        /// <param name="posalterationoptionEntity"> </param>
        /// <returns>An Obymobi.Data.EntityClasses.PosalterationoptionEntity instance</returns>
        public static PosalterationoptionEntity CreateUpdatePosalterationoptionEntityFromModel(Posalterationoption posalterationoption, PosalterationoptionEntity posalterationoptionEntity)
        {
            if (posalterationoptionEntity == null)
                posalterationoptionEntity = new PosalterationoptionEntity();

            posalterationoptionEntity.CompanyId = posalterationoption.CompanyId;
            posalterationoptionEntity.ExternalId = posalterationoption.ExternalId;
            posalterationoptionEntity.Name = posalterationoption.Name;
            posalterationoptionEntity.PriceIn = posalterationoption.PriceIn;
            posalterationoptionEntity.PosproductExternalId = posalterationoption.PosproductExternalId;

            posalterationoptionEntity.FieldValue1 = posalterationoption.FieldValue1;
            posalterationoptionEntity.FieldValue2 = posalterationoption.FieldValue2;
            posalterationoptionEntity.FieldValue3 = posalterationoption.FieldValue3;
            posalterationoptionEntity.FieldValue4 = posalterationoption.FieldValue4;
            posalterationoptionEntity.FieldValue5 = posalterationoption.FieldValue5;
            posalterationoptionEntity.FieldValue6 = posalterationoption.FieldValue6;
            posalterationoptionEntity.FieldValue7 = posalterationoption.FieldValue7;
            posalterationoptionEntity.FieldValue8 = posalterationoption.FieldValue8;
            posalterationoptionEntity.FieldValue9 = posalterationoption.FieldValue8;
            posalterationoptionEntity.FieldValue10 = posalterationoption.FieldValue10;

            posalterationoptionEntity.RevenueCenter = posalterationoption.RevenueCenter;

            return posalterationoptionEntity;
        }
    }
}
