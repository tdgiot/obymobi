﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.HelperClasses
{
    public class TerminalStateHelper
    {
        // GK If I broke some stuff, just get to me :)
        // Reasoon for the rewrite, database access not efficient:
        // UpdateTerminalState gets called by:
        // TerminalHelper.UpdatLastRequest
        // DatabasePoller.UpdateTerminalStatesForTerminals 
        // Both have a loaded Terminal from the DB, which includes LastStateOnline and LastStateOperationMode
        // UpdateTerminalState gets the latest TerminalState entity using GetLatestTerminalStateEntity, ot be used to check if the state changed, but we already know that because
        // the last state is on the TerminalEntity itself (yes, it could be out of sync, but that can only happen is someone screws with the db).
        // Since this is called very often and the tablet gets very large I think it's better to NOT do that call and rely on the TerminalEntity
        // Worst of all, when the TerminalState gets saved it AGAIN loads the Terminal entity (while we already had it) in it's validator. 
        // So that's why it got changed. If you disagree, please tell me what I did wrong of forgot about.

        /// <summary>
        /// Gets the latest terminal state entity for the specified terminal id
        /// </summary>
        /// <param name="terminalId">The id of the terminal</param>
        /// <returns>The latest <see cref="TerminalStateEntity"/> instance if it exists, otherwise null</returns>
        private static TerminalStateEntity GetLatestTerminalStateEntity(TerminalEntity terminal)
        {
            TerminalStateEntity terminalStateEntity = null;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalStateFields.TerminalId == terminal.TerminalId);

            SortExpression sort = new SortExpression(new SortClause(TerminalStateFields.TerminalStateId, SortOperator.Descending));

            TerminalStateCollection terminalStateCollection = new TerminalStateCollection();
            terminalStateCollection.AddToTransaction(terminal);
            terminalStateCollection.GetMulti(filter, 1, sort);

            if (terminalStateCollection.Count == 1)
                terminalStateEntity = terminalStateCollection[0];

            return terminalStateEntity;
        }

        /// <summary>
        /// Updates the terminal state if the state of the terminal has changed
        /// </summary>
        /// <param name="terminal">The <see cref="TerminalEntity"/> instance to update the terminal state for</param>
        /// <param name="online">The flag which indicates whether the terminal is online</param>
        public static void UpdateTerminalState(TerminalEntity terminal, bool online)
        {            
            string message = string.Empty;
            TerminalStateEntity lastTerminalStateEntity = null;

            if (HasTerminalStateChanged(terminal, online, out message, out lastTerminalStateEntity))
            {
                DateTime utcNow = DateTime.UtcNow;

                TerminalStateEntity terminalStateEntity = new TerminalStateEntity();
                terminalStateEntity.TerminalEntity = terminal; // Updated this instead of setting TerminalId to prevent a unnecessary refetch of the terminal
                terminalStateEntity.Online = online;
                terminalStateEntity.OperationMode = terminal.OperationMode;
                terminalStateEntity.LastBatteryLevel = terminal.DeviceEntity.BatteryLevel;
                terminalStateEntity.LastIsCharging = terminal.DeviceEntity.IsCharging;
                terminalStateEntity.Message = message;
                terminalStateEntity.CreatedUTC = utcNow;
                if (lastTerminalStateEntity != null && lastTerminalStateEntity.CreatedUTC.HasValue)
                    terminalStateEntity.TimeSpan = (utcNow - lastTerminalStateEntity.CreatedUTC.Value);

                if (terminal.GetCurrentTransaction() != null)
                    terminal.GetCurrentTransaction().Add(terminalStateEntity);

                terminalStateEntity.Save();                
            }
        }

        /// <summary>
        /// Checks whether the terminal state has changed
        /// </summary>
        /// <param name="terminal">The <see cref="TerminalEntity"/> instance to update the terminal state for</param>
        /// <param name="online">The flag which indicates whether the terminal is online</param>
        /// <param name="message">The message to add to the terminal state record</param>
        /// <returns>A flag which indicates whether the terminal state has changed</returns>
        public static bool HasTerminalStateChanged(TerminalEntity terminal, bool online, out string message, out TerminalStateEntity lastTerminalStateEntity)
        {            
            message = string.Empty;

            bool? onlineStateChangedTo = null;
            lastTerminalStateEntity = null;
            TerminalOperationMode operationModeFrom = TerminalOperationMode.Unknown;
            TerminalOperationMode operationModeTo = TerminalOperationMode.Unknown;

            if (terminal.LastStateOnline.HasValue && terminal.LastStateOperationMode.HasValue)
            {
                // Check changes on the TerminalEntity itself
                if (terminal.LastStateOnline.Value != online)
                {
                    onlineStateChangedTo = online;
                }

                if (terminal.LastStateOperationMode.Value != terminal.OperationMode)
                {                    
                    operationModeFrom = terminal.OperationMode.ToEnum<TerminalOperationMode>();
                    operationModeTo = terminal.LastStateOperationMode.Value.ToEnum<TerminalOperationMode>();
                }
            }
            else
            { 
                // Check changes on the last TerminalStateEntity
                lastTerminalStateEntity = GetLatestTerminalStateEntity(terminal);
                if (lastTerminalStateEntity == null) // No terminal state record existed yet
                {
                    lastTerminalStateEntity = new TerminalStateEntity(); // Add it here, so later in the function we know we did the query, but nothing was found.
                    onlineStateChangedTo = online;
                    operationModeTo = terminal.OperationMode.ToEnum<TerminalOperationMode>();
                    message = "No terminal state was known yet.";                    
                }
                else if (lastTerminalStateEntity.Online != online) // Online status changed
                {
                    onlineStateChangedTo = online;
                }
                else if (lastTerminalStateEntity.OperationMode != terminal.OperationMode) // Operation mode changed
                {                    
                    operationModeFrom = terminal.OperationMode.ToEnum<TerminalOperationMode>();
                    operationModeTo = lastTerminalStateEntity.OperationModeAsEnum;                    
                }
            }

            // Prepare Text
            bool changed = false;
            if(message.Length > 0)
            {
                // No status before.
                changed = true;
            }
            else
            {
                if(onlineStateChangedTo.HasValue)
                {
                    changed = true;
                    if (onlineStateChangedTo.Value)
                        message += "Terminal came online";
                    else
                        message += "Terminal went offline";
                }

                if (operationModeFrom != operationModeTo)
                {
                    changed = true;
                    string operationModeChangeMessage = string.Format("Terminal operation mode changed from '{0}' to '{1}'.", operationModeFrom, operationModeTo);
                    message += StringUtil.CombineWithCommaSpace(message, operationModeChangeMessage);
                }
            }

            // If changed, get the last TerminalState if we didn't retrieve it yet.
            if (changed)
            {
                if (lastTerminalStateEntity != null && lastTerminalStateEntity.IsNew)
                {
                    // We retrieved already above, don't want to do the query twice, and we found out it failed.
                    lastTerminalStateEntity = null;
                }
                else if (lastTerminalStateEntity == null)
                    lastTerminalStateEntity = TerminalStateHelper.GetLatestTerminalStateEntity(terminal);
            }            

            return changed;
        }
    }
}
