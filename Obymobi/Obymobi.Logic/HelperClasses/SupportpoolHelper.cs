﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Text.RegularExpressions;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Enums;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    ///     Helper for the supportpool
    /// </summary>
    public static class SupportpoolHelper
    {
        #region Enums

        public enum SupportpoolHelperResult
        {
            ParameterIsMissing
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Gets an array of all <see cref="Supportpool" /> support pools
        /// </summary>
        /// <returns>An array of <see cref="Supportpool" /> instances</returns>
        public static Supportpool[] GetSupportpools()
        {
            SupportpoolCollection supportpools = new SupportpoolCollection();
            supportpools.GetMulti(null);

            List<Supportpool> supportpoolModels = new List<Supportpool>();

            foreach (SupportpoolEntity supportpool in supportpools)
            {
                supportpoolModels.Add(SupportpoolHelper.CreateSupportpoolModelFromEntity(supportpool));
            }

            return supportpoolModels.ToArray();
        }

        /// <summary>
        ///     Creates the supportpool model from the specified supportpool entity.
        /// </summary>
        /// <param name="supportpoolEntity">The supportpool entity.</param>
        /// <returns>A <see cref="Supportpool" /> instance</returns>
        public static Supportpool CreateSupportpoolModelFromEntity(SupportpoolEntity supportpoolEntity)
        {
            Supportpool supportpool = new Supportpool();

            supportpool.SupportpoolId = supportpoolEntity.SupportpoolId;
            supportpool.Name = supportpoolEntity.Name;
            supportpool.Phonenumber = supportpoolEntity.Phonenumber;

            return supportpool;
        }

        public static List<string> GetListOfEmailaddresses(SupportpoolEntity supportpoolEntity, bool fallback, SupportNotification? supportNotification)
        {
            List<string> addresses = new List<string>();

            string emailAddresses = string.Empty;
            if (supportpoolEntity != null)
            {
                emailAddresses = supportpoolEntity.Email;
            }

            if (fallback)
            {
                emailAddresses = ConfigurationManager.GetString(ObymobiConfigConstants.FallbackSupportpoolEmailAddresses, false);
            }

            if (emailAddresses.Contains(","))
            {
                addresses = emailAddresses.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            else if (emailAddresses.Contains(";"))
            {
                addresses = emailAddresses.Split(';', StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            else if (emailAddresses.Length > 0)
            {
                addresses.Add(emailAddresses);
            }

            addresses = addresses.Select(x => x.Trim()).ToList();

            if (!fallback)
            {
                // Add the email addresses from the support agents that are on support
                foreach (SupportpoolSupportagentEntity supportpoolSupportAgentEntity in supportpoolEntity.SupportpoolSupportagentCollection)
                {
                    string address = supportpoolSupportAgentEntity.SupportagentEntity.Email.Trim();
                    if (!string.IsNullOrWhiteSpace(address))
                    {
                        if (supportpoolSupportAgentEntity.OnSupport && AgentRequiresNotification(supportpoolSupportAgentEntity, supportNotification))
                        {
                            // Add the email address if the agent is on support
                            if (!addresses.Contains(address))
                                addresses.Add(address);
                        }
                        else
                        {
                            // Remove the email address if the agent is not on support
                            if (addresses.Contains(address))
                                addresses.Remove(address);
                        }
                    }
                }

                // Add the email addresses from the notification recipients that are on support
                foreach (SupportpoolNotificationRecipientEntity supportpoolNotificationRecipientEntity in supportpoolEntity.SupportpoolNotificationRecipientCollection)
                {
                    string address = supportpoolNotificationRecipientEntity.Email.Trim();
                    if (!string.IsNullOrWhiteSpace(address))
                    {
                        if (RecipientRequiresNotification(supportpoolNotificationRecipientEntity, supportNotification))
                        {
                            // Add the email address if the recipient has to be notified
                            if (!addresses.Contains(address))
                                addresses.Add(address);
                        }
                        else
                        {
                            // Remove the email address if the recipient does not has to be notified
                            if (addresses.Contains(address))
                                addresses.Remove(address);
                        }
                    }
                }
            }

            // Make sure it's valid emails
            IEnumerable<string> toReturn = addresses.Where(x => RegEx.IsEmail(x));

            return toReturn.ToList();
        }

        private static List<string> GetListOfPhonenumbers(SupportpoolEntity supportpoolEntity, bool fallback, SupportNotification? supportNotification)
        {
            List<string> numbers = new List<string>();

            string phoneNumbers = string.Empty;
            if (supportpoolEntity != null)
            {
                phoneNumbers = supportpoolEntity.Phonenumber;
            }

            if (fallback)
            {
                phoneNumbers = ConfigurationManager.GetString(ObymobiConfigConstants.FallbackSupportpoolPhonenumbers, false);
            }

            if (phoneNumbers.Contains(","))
            {
                numbers = phoneNumbers.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            else if (phoneNumbers.Contains(";"))
            {
                numbers = phoneNumbers.Split(';', StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            else if (phoneNumbers.Length > 0)
            {
                numbers.Add(phoneNumbers);
            }

            if (!fallback)
            {
                // Add the phone numbers from the support agents that are on support
                foreach (SupportpoolSupportagentEntity supportpoolSupportAgentEntity in supportpoolEntity.SupportpoolSupportagentCollection)
                {
                    string phonenumber = supportpoolSupportAgentEntity.SupportagentEntity.Phonenumber;
                    if (!string.IsNullOrWhiteSpace(phonenumber))
                    {
                        if (supportpoolSupportAgentEntity.OnSupport && AgentRequiresNotification(supportpoolSupportAgentEntity, supportNotification))
                        {
                            // Add the phone number if the agent is on support
                            if (!numbers.Contains(phonenumber))
                                numbers.Add(phonenumber);
                        }
                        else
                        {
                            // Remove the phone number if the agent is not on support
                            if (numbers.Contains(phonenumber))
                                numbers.Remove(phonenumber);
                        }
                    }
                }

                // Add the phone numbers from the notification recipients that are on support
                foreach (SupportpoolNotificationRecipientEntity supportpoolNotificationRecipientEntity in supportpoolEntity.SupportpoolNotificationRecipientCollection)
                {
                    string phonenumber = supportpoolNotificationRecipientEntity.Phonenumber;
                    if (!string.IsNullOrWhiteSpace(phonenumber))
                    {
                        if (RecipientRequiresNotification(supportpoolNotificationRecipientEntity, supportNotification))
                        {
                            // Add the phone number if the recipient has to be notified
                            if (!numbers.Contains(phonenumber))
                                numbers.Add(phonenumber);
                        }
                        else
                        {
                            // Remove the phone number if the recipient does not has to be notified
                            if (numbers.Contains(phonenumber))
                                numbers.Remove(phonenumber);
                        }
                    }
                }
            }

            return numbers;
        }

        public static List<String> GetSupportpoolPhonenumbers(this SupportpoolEntity supportpool, bool fallbackToSystemSupportpool, SupportNotification? supportNotification)
        {
            bool fallback = false;

            // Try to get the SystemSupportPool if empty and it's fall back.
            if ((supportpool == null || supportpool.IsNew) && fallbackToSystemSupportpool)
            {
                supportpool = SupportpoolHelper.GetSystemSupportpool();
            }

            if (supportpool == null || supportpool.IsNew)
            {
                fallback = fallbackToSystemSupportpool;
            }

            return SupportpoolHelper.GetListOfPhonenumbers(supportpool, fallback, supportNotification);
        }

        public static List<String> GetSupportpoolEmailaddresses(this SupportpoolEntity supportpool, bool fallbackToSystemSupportpool, SupportNotification? supportNotification)
        {
            bool fallback = false;

            // Try to get the SystemSupportPool if empty and it's fall back.
            if ((supportpool == null || supportpool.IsNew) && fallbackToSystemSupportpool)
            {
                supportpool = SupportpoolHelper.GetSystemSupportpool();
            }

            if (supportpool == null || supportpool.IsNew)
            {
                fallback = fallbackToSystemSupportpool;
            }

            return SupportpoolHelper.GetListOfEmailaddresses(supportpool, fallback, supportNotification);
        }

        public static SupportpoolEntity GetSystemSupportpool()
        {
            PredicateExpression filter = new PredicateExpression(SupportpoolFields.SystemSupportPool == true);
            PrefetchPath prefetch = new PrefetchPath(EntityType.SupportpoolEntity);
            prefetch.Add(SupportpoolEntity.PrefetchPathSupportpoolSupportagentCollection).SubPath.Add(SupportpoolSupportagentEntity.PrefetchPathSupportagentEntity);
            SupportpoolEntity supportpool = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<SupportpoolEntity>(filter, null, prefetch);

            if (supportpool == null)
            {
                SupportpoolEntity sp = new SupportpoolEntity();
                sp.Name = "System SupportPool";
                sp.Email = ConfigurationManager.GetString(ObymobiConfigConstants.FallbackSupportpoolEmailAddresses);
                sp.Phonenumber = ConfigurationManager.GetString(ObymobiConfigConstants.FallbackSupportpoolPhonenumbers);
                sp.SystemSupportPool = true;
                sp.Save();

                supportpool = sp;
            }

            return supportpool;
        }

        public static List<String> GetSystemSupportpoolPhonenumbers()
        {
            SupportpoolEntity supportpool = SupportpoolHelper.GetSystemSupportpool();
            return supportpool.GetSupportpoolPhonenumbers(false, null);
        }

        public static List<String> GetSystemSupportpoolEmailaddresses()
        {
            SupportpoolEntity supportpool = SupportpoolHelper.GetSystemSupportpool();
            return supportpool.GetSupportpoolEmailaddresses(false, null);
        }

        public static List<string> GetSlackUsernames(this SupportpoolEntity supportpoolEntity, SupportNotification? supportNotification)
        {
            List<string> slackUsernameList = new List<string>();

            foreach (SupportpoolSupportagentEntity supportpoolSupportAgentEntity in supportpoolEntity.SupportpoolSupportagentCollection)
            {
                if (supportpoolSupportAgentEntity.SupportagentEntity.SlackUsername.IsNullOrWhiteSpace())
                {
                    continue;
                }

                if (supportpoolSupportAgentEntity.OnSupport && AgentRequiresNotification(supportpoolSupportAgentEntity, supportNotification))
                {
                    slackUsernameList.Add(supportpoolSupportAgentEntity.SupportagentEntity.SlackUsername);
                }
            }

            return slackUsernameList;
        }

        private static bool AgentRequiresNotification(SupportpoolSupportagentEntity supportpoolSupportagentEntity, SupportNotification? supportNotification)
        {
            bool notify = true;

            if (supportNotification.HasValue)
            {
                switch (supportNotification.Value)
                {
                    case SupportNotification.OfflineTerminals:
                        notify = supportpoolSupportagentEntity.NotifyOfflineTerminals;
                        break;
                    case SupportNotification.OfflineClients:
                        notify = supportpoolSupportagentEntity.NotifyOfflineClients;
                        break;
                    case SupportNotification.UnprocessableOrders:
                        notify = supportpoolSupportagentEntity.NotifyUnprocessableOrders;
                        break;
                    case SupportNotification.NotRetrievedInTime:
                        notify = supportpoolSupportagentEntity.NotifyExpiredSteps;
                        break;
                    case SupportNotification.NotHandledInTime:
                        notify = false; // Crave support agents are not being notified of every order that is unhandled
                        break;
                    case SupportNotification.TooMuchOfflineClientsJump:
                        notify = supportpoolSupportagentEntity.NotifyTooMuchOfflineClientsJump;
                        break;
                    case SupportNotification.BouncedEmails:
                        notify = supportpoolSupportagentEntity.NotifyBouncedEmails;
                        break;
                    default:
                        break;
                }
            }

            return notify;
        }

        private static bool RecipientRequiresNotification(SupportpoolNotificationRecipientEntity supportpoolNotificationRecipientEntity, SupportNotification? supportNotification)
        {
            bool notify = true;

            if (supportNotification.HasValue)
            {
                switch (supportNotification.Value)
                {
                    case SupportNotification.OfflineTerminals:
                        notify = supportpoolNotificationRecipientEntity.NotifyOfflineTerminals;
                        break;
                    case SupportNotification.OfflineClients:
                        notify = supportpoolNotificationRecipientEntity.NotifyOfflineClients;
                        break;
                    case SupportNotification.UnprocessableOrders:
                        notify = supportpoolNotificationRecipientEntity.NotifyUnprocessableOrders;
                        break;
                    case SupportNotification.NotRetrievedInTime:
                    case SupportNotification.NotHandledInTime:
                        notify = supportpoolNotificationRecipientEntity.NotifyExpiredSteps;
                        break;
                    case SupportNotification.TooMuchOfflineClientsJump:
                        notify = supportpoolNotificationRecipientEntity.NotifyTooMuchOfflineClientsJump;
                        break;
                    case SupportNotification.BouncedEmails:
                        notify = supportpoolNotificationRecipientEntity.NotifyBouncedEmails;
                        break;
                    default:
                        break;
                }
            }

            return notify;
        }

        #endregion
    }
}