﻿using System;
using System.Collections.Generic;
using System.Data;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    public class NetmessageHelper
    {
        public const int NETMESSAGE_TIMEOUT = 5;

        public static Netmessage CreateNetmessageModelFromEntity(NetmessageEntity entity)
        {
            var netmessage = new Netmessage();
            netmessage.NetmessageId = entity.NetmessageId;
            netmessage.MessageTypeInt = entity.MessageType;
            netmessage.Guid = entity.Guid;
            netmessage.ReceiverIdentifier = entity.ReceiverIdentifier;
            netmessage.ReceiverClientId = entity.ReceiverClientId.GetValueOrDefault(0);
            netmessage.ReceiverCompanyId = entity.ReceiverCompanyId.GetValueOrDefault(0);
            netmessage.ReceiverDeliverypointId = entity.ReceiverDeliverypointId.GetValueOrDefault(0);
            netmessage.ReceiverTerminalId = entity.ReceiverTerminalId.GetValueOrDefault(0);
            netmessage.SenderIdentifier = entity.SenderIdentifier;
            netmessage.SenderClientId = entity.SenderClientId.GetValueOrDefault(0);
            netmessage.SenderCompanyId = entity.SenderCompanyId.GetValueOrDefault(0);
            netmessage.SenderDeliverypointId = entity.SenderDeliverypointId.GetValueOrDefault(0);
            netmessage.SenderTerminalId = entity.SenderTerminalId.GetValueOrDefault(0);
            netmessage.FieldValue1 = entity.FieldValue1;
            netmessage.FieldValue2 = entity.FieldValue2;
            netmessage.FieldValue3 = entity.FieldValue3;
            netmessage.FieldValue4 = entity.FieldValue4;
            netmessage.FieldValue5 = entity.FieldValue5;
            netmessage.FieldValue6 = entity.FieldValue6;
            netmessage.FieldValue7 = entity.FieldValue7;
            netmessage.FieldValue8 = entity.FieldValue8;
            netmessage.FieldValue9 = entity.FieldValue9;
            netmessage.Submitted = (entity.StatusAsEnum == NetmessageStatus.Completed);
            netmessage.Status = entity.StatusAsEnum.ToString();
            netmessage.Created = entity.CreatedUTC.HasValue ? entity.CreatedUTC.Value.UtcToLocalTime() : DateTime.MinValue;
            netmessage.MessageLog = entity.MessageLog;

            return netmessage;
        }

        public static Netmessage CreateTypedNetmessageModelFromEntity(NetmessageEntity entity, bool validateMessage = false)
        {
            // First create normal netmessage
            var model = CreateNetmessageModelFromEntity(entity);

            // Conver to right sub-class
            return CreateTypedNetmessageModelFromModel(model, validateMessage);
        }

        public static Netmessage CreateTypedNetmessageModelFromModel(Netmessage model, bool validateMessage = false)
        {
            // Convert Netmessage to actual sub-type
            Netmessage netmessage;
            switch (model.GetMessageType())
            {
                case NetmessageType.AnnounceNewMessage:
                    netmessage = model.ConvertTo<NetmessageAnnounceNewMessage>(validateMessage);
                    break;
                case NetmessageType.OrderStatusUpdated:
                    netmessage = model.ConvertTo<NetmessageOrderStatusUpdated>(validateMessage);
                    break;
                case NetmessageType.BroadcastMessage:
                    netmessage = model.ConvertTo<NetmessageBroadcastMessage>(validateMessage);
                    break;
                case NetmessageType.NewSocialMediaMessage:
                    netmessage = model.ConvertTo<NetmessageNewSocialMediaMessage>(validateMessage);
                    break;
                case NetmessageType.RoutestephandlerStatusUpdated:
                    netmessage = model.ConvertTo<NetmessageRoutestephandlerStatusUpdated>(validateMessage);
                    break;
                case NetmessageType.SetPmsCheckedOut:
                    netmessage = model.ConvertTo<NetmessageSetPmsCheckedOut>(validateMessage);
                    break;
                case NetmessageType.GetPmsFolio:
                    netmessage = model.ConvertTo<NetmessageGetPmsFolio>(validateMessage);
                    break;
                case NetmessageType.SetPmsFolio:
                    netmessage = model.ConvertTo<NetmessageSetPmsFolio>(validateMessage);
                    break;
                case NetmessageType.SetPmsCheckedIn:
                    netmessage = model.ConvertTo<NetmessageSetPmsCheckedIn>(validateMessage);
                    break;
                case NetmessageType.SetPmsGuestInformation:
                    netmessage = model.ConvertTo<NetmessageSetPmsGuestInformation>(validateMessage);
                    break;
                case NetmessageType.GetPmsGuestInformation:
                    netmessage = model.ConvertTo<NetmessageGetPmsGuestInformation>(validateMessage);
                    break;
                case NetmessageType.SetForwardTerminal:
                    netmessage = model.ConvertTo<NetmessageSetForwardedTerminal>(validateMessage);
                    break;
                case NetmessageType.GetPmsExpressCheckout:
                    netmessage = model.ConvertTo<NetmessageGetPmsExpressCheckout>(validateMessage);
                    break;
                case NetmessageType.SetPmsExpressCheckedOut:
                    netmessage = model.ConvertTo<NetmessageSetPmsExpressCheckedOut>(validateMessage);
                    break;
                case NetmessageType.AuthenticateResult:
                    netmessage = model.ConvertTo<NetmessageAuthenticateResult>(validateMessage);
                    break;
                case NetmessageType.SetNetworkInformation:
                    netmessage = model.ConvertTo<NetmessageSetNetworkInformation>(validateMessage);
                    break;
                case NetmessageType.SetClientType:
                    netmessage = model.ConvertTo<NetmessageSetClientType>(validateMessage);
                    break;
                case NetmessageType.ConnectToAgent:
                    netmessage = model.ConvertTo<NetmessageConnectToAgent>(validateMessage);
                    break;
                case NetmessageType.AgentCommandResponse:
                    netmessage = model.ConvertTo<NetmessageAgentCommandResponse>(validateMessage);
                    break;
                case NetmessageType.AgentCommandRequest:
                    netmessage = model.ConvertTo<NetmessageAgentCommandRequest>(validateMessage);
                    break;
                case NetmessageType.NewEventExternalListener:
                    netmessage = model.ConvertTo<NetmessageNewEventExternalListener>(validateMessage);
                    break;
                case NetmessageType.SetClientOperationMode:
                    netmessage = model.ConvertTo<NetmessageSetClientOperationMode>(validateMessage);
                    break;
                case NetmessageType.SetDeliverypointId:
                    netmessage = model.ConvertTo<NetmessageSetDeliverypointId>(validateMessage);
                    break;
                case NetmessageType.Ping:
                    netmessage = model.ConvertTo<NetmessagePing>(validateMessage);
                    break;
                case NetmessageType.Pong:
                    netmessage = model.ConvertTo<NetmessagePong>(validateMessage);
                    break;
                case NetmessageType.ClientCommand:
                    netmessage = model.ConvertTo<NetmessageClientCommand>(validateMessage);
                    break;
                case NetmessageType.TerminalCommand:
                    netmessage = model.ConvertTo<NetmessageTerminalCommand>(validateMessage);
                    break;
                case NetmessageType.PushEventToExternalListeners:
                case NetmessageType.Disconnect:
                    netmessage = model;
                    break;
                case NetmessageType.NewSurveyResult:
                    netmessage = model.ConvertTo<NetmessageNewSurveyResult>(validateMessage);
                    break;
                case NetmessageType.DeviceCommandExecute:
                    netmessage = model.ConvertTo<NetmessageDeviceCommandExecute>(validateMessage);
                    break;
                case NetmessageType.GetConnectedClients:
                    netmessage = model.ConvertTo<NetmessageGetConnectedClients>(validateMessage);
                    break;
                case NetmessageType.SystemState:
                    netmessage = model.ConvertTo<NetmessageSystemState>();
                    break;
                case NetmessageType.DeviceUnlink:
                    netmessage = model.ConvertTo<NetmessageDeviceUnlink>();
                    break;
                case NetmessageType.SandboxConfig:
                    netmessage = model.ConvertTo<NetmessageSandboxConfig>();
                    break;
                case NetmessageType.SetMasterTab:
                    netmessage = model.ConvertTo<NetmessageSetMasterTab>();
                    break;
                case NetmessageType.DownloadUpdate:
                    netmessage = model.ConvertTo<NetmessageDownloadUpdate>();
                    break;
                case NetmessageType.SwitchCometHandler:
                    netmessage = model.ConvertTo<NetmessageSwitchCometHandler>();
                    break;
                case NetmessageType.SetPmsWakeUp:
                    netmessage = model.ConvertTo<NetmessageSetPmsWakeUp>();
                    break;
                case NetmessageType.MenuUpdated:
                    netmessage = model.ConvertTo<NetmessageMenuUpdated>();
                    break;
                case NetmessageType.SwitchTab:
                    netmessage = model.ConvertTo<NetmessageSwitchTab>();
                    break;
                case NetmessageType.SendJsonObject:
                    netmessage = model.ConvertTo<NetmessageSendJsonObject>();
                    break;
                case NetmessageType.RefreshContent:
                    netmessage = model.ConvertTo<NetmessageRefreshContent>();
                    break;
                case NetmessageType.PmsTerminalStatusUpdated:
                    netmessage = model.ConvertTo<NetmessagePmsTerminalStatusUpdated>();
                    break;
                case NetmessageType.UpdateDeviceToken:
                    netmessage = model.ConvertTo<NetmessageUpdateDeviceToken>();
                    break;
                case NetmessageType.ClearCompanyCredentials:
                    netmessage = model.ConvertTo<NetmessageClearCompanyCredentials>();
                    break;
                case NetmessageType.MigrateToRestApi:
                    netmessage = model.ConvertTo<NetmessageMigrateToRestApi>();
                    break;
                default:
                    throw new ObymobiException(NetmessageValidateError.NoSuchMessageType, "Netmessage with type '{0}' does not exists!", model.MessageType);
            }

            return netmessage;
        }

        public static NetmessageEntity CreateNetmessageEntityFromModel(Netmessage message)
        {
            var entity = new NetmessageEntity();
            entity.NetmessageId = message.NetmessageId;
            entity.MessageType = message.MessageTypeInt;
            entity.Guid = message.Guid;
            entity.ReceiverIdentifier = message.ReceiverIdentifier.IsNullOrWhiteSpace() ? null : message.ReceiverIdentifier;
            entity.SenderIdentifier = message.SenderIdentifier.IsNullOrWhiteSpace() ? null : message.SenderIdentifier;
            entity.ReceiverClientId = ((message.ReceiverClientId == 0) ? (int?)null : message.ReceiverClientId);
            entity.ReceiverCompanyId = ((message.ReceiverCompanyId == 0) ? (int?)null : message.ReceiverCompanyId);
            entity.ReceiverDeliverypointId = ((message.ReceiverDeliverypointId == 0) ? (int?)null : message.ReceiverDeliverypointId);
            entity.ReceiverTerminalId = ((message.ReceiverTerminalId == 0) ? (int?)null : message.ReceiverTerminalId);
            entity.SenderClientId = ((message.SenderClientId == 0) ? (int?)null : message.SenderClientId);
            entity.SenderCompanyId = ((message.SenderCompanyId == 0) ? (int?)null : message.SenderCompanyId);
            entity.SenderDeliverypointId = ((message.SenderDeliverypointId == 0) ? (int?)null : message.SenderDeliverypointId);
            entity.SenderTerminalId = ((message.SenderTerminalId == 0) ? (int?)null : message.SenderTerminalId);
            entity.FieldValue1 = message.FieldValue1;
            entity.FieldValue2 = message.FieldValue2;
            entity.FieldValue3 = message.FieldValue3;
            entity.FieldValue4 = message.FieldValue4;
            entity.FieldValue5 = message.FieldValue5;
            entity.FieldValue6 = message.FieldValue6;
            entity.FieldValue7 = message.FieldValue7;
            entity.FieldValue8 = message.FieldValue8;
            entity.FieldValue9 = message.FieldValue9;
            entity.CreatedUTC = message.Created.LocalTimeToUtc();
            entity.MessageLog = message.MessageLog;

            return entity;
        }

        public static List<Netmessage> GetNetmessageForIdentifier(string macaddress)
        {
            var subFilter = new PredicateExpression(NetmessageFields.Status == NetmessageStatus.WaitingForVerification);
            subFilter.AddWithOr(NetmessageFields.Status == NetmessageStatus.Delivered);

            var filter = new PredicateExpression(NetmessageFields.ReceiverIdentifier == macaddress);
            filter.AddWithAnd(subFilter);

            var netmessageCollection = new NetmessageCollection();
            netmessageCollection.GetMulti(filter);

            return ConvertCollectionToList(netmessageCollection);
        }

        public static List<Netmessage> GetNetmessagesForClient(int clientId)
        {
            var subFilter = new PredicateExpression(NetmessageFields.Status == NetmessageStatus.WaitingForVerification);
            subFilter.AddWithOr(NetmessageFields.Status == NetmessageStatus.Delivered);

            var filter = new PredicateExpression(NetmessageFields.ReceiverClientId == clientId);
            filter.AddWithAnd(subFilter);

            var netmessageCollection = new NetmessageCollection();
            netmessageCollection.GetMulti(filter);

            return ConvertCollectionToList(netmessageCollection);
        }

        public static List<Netmessage> GetNetmessagesForTerminal(int terminalId)
        {
            var subFilter = new PredicateExpression(NetmessageFields.Status == NetmessageStatus.WaitingForVerification);
            subFilter.AddWithOr(NetmessageFields.Status == NetmessageStatus.Delivered);

            var filter = new PredicateExpression(NetmessageFields.ReceiverTerminalId == terminalId);
            filter.AddWithAnd(subFilter);

            var netmessageCollection = new NetmessageCollection();
            netmessageCollection.GetMulti(filter);

            return ConvertCollectionToList(netmessageCollection);
        }

        /// <summary>
        /// Converts a NetmessageCollection to List. Method also checks if Netmessages are TimedOut.
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        private static List<Netmessage> ConvertCollectionToList(IEnumerable<NetmessageEntity> collection)
        {
            var result = new List<Netmessage>();

            foreach (var netmessageEntity in collection)
            {
                result.Add(CreateNetmessageModelFromEntity(netmessageEntity));
            }

            return result;
        }
    }
}
