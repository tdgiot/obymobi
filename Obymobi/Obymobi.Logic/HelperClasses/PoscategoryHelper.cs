﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// PoscategoryHelper class
    /// </summary>
    public class PoscategoryHelper
    {
        /// <summary>
        /// PoscategoryHelperResult enums
        /// </summary>
        public enum PoscategoryHelperResult
        {
            /// <summary>
            /// POS category is missing for the category
            /// </summary>
            PoscategoryMissingForCategory = 201,
        }


        /// <summary>
        /// Cleans up.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="cleanupReport">The cleanup report.</param>
        /// <param name="batchId">The batch id.</param>
        public static void CleanUp(int companyId, ref StringBuilder cleanupReport, out long batchId)
        {
            cleanupReport.AppendLine("*** START: Delete Poscategory entities ***");
            batchId = -1;
            // Prepate filter for company
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PoscategoryFields.CompanyId == companyId);

            // Create collection and use for Scalar MAX
            PoscategoryCollection poscategories = new PoscategoryCollection();
            object maxBatchId = poscategories.GetScalar(Data.PoscategoryFieldIndex.SynchronisationBatchId, null, AggregateFunction.Max, filter);

            // Only proceed when we have an actual maxBatchId
            if (maxBatchId != DBNull.Value && maxBatchId is long)
            {
                // Retrieve all order poscategorys & delete them
                cleanupReport.AppendFormatLine("BatchId: {0}", maxBatchId);
                batchId = (long)maxBatchId;
                // Prepare filter for older poscategorys
                filter = new PredicateExpression();
                filter.Add(PoscategoryFields.CompanyId == companyId);
                filter.Add(PoscategoryFields.SynchronisationBatchId < maxBatchId);

                poscategories.GetMulti(filter);

                // Delete per piece
                foreach (var poscategory in poscategories)
                {
                    cleanupReport.AppendFormatLine("ExternalId: {0}, Name: {1}",
                        poscategory.ExternalId, poscategory.Name);
                    poscategory.Delete();
                }
                cleanupReport.AppendFormatLine("{0} entities deleted", poscategories.Count);
            }
            else
                cleanupReport.AppendLine("No valid batch found");

            cleanupReport.AppendLine("*** FINISHED: Delete Poscategory entities ***");
        }

        /// <summary>
        /// Synchronizes the categories from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="xml">The Xml string containing the serialized array of Poscategory instances</param>
        /// <param name="companyId">The id of the company to import the pos categories for</param>
        /// <param name="batchId">The batch id.</param>
        public static void SynchronizePoscategories(string xml, int companyId, long batchId)
        {
            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPoscategoriesResult.XmlIsEmpty, "Xml string containing serialized Poscategory array is empty.");
            else if (companyId <= 1)
                throw new ObymobiException(ImportPoscategoriesResult.CompanyIdIsZeroOrLess, "The specified company id '{0}' is smaller than 1.", companyId);
            else
            {
                // Deserialize the xml in order to get the array of Poscategory instances
                Poscategory[] poscategories = XmlHelper.Deserialize<Poscategory[]>(xml);

                SynchronizePoscategories(poscategories, companyId, batchId);
            }
        }

        /// <summary>
        /// Synchronizes the categories from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="poscategories">The poscategories.</param>
        /// <param name="companyId">The id of the company to import the pos categories for</param>
        /// <param name="batchId">The batch id.</param>
        public static void SynchronizePoscategories(Poscategory[] poscategories, int companyId, long batchId, string revenueCenter = null)
        {
            // Get a PoscategoryCollection for the specified company
            PoscategoryCollection poscategoryCollection = GetPoscategoryCollection(companyId);

            // Create an EntityView for the PoscategoryCollection
            EntityView<PoscategoryEntity> poscategoryView = poscategoryCollection.DefaultView;

            // Populate the list of external id's
            var query = from c in poscategoryView select c.ExternalId;
            List<string> poscategoryExternalIdList = query.ToList<string>();

            // Create a filter
            PredicateExpression filter = null;

            // Walk through the Poscategory instances of the array
            for (int i = 0; i < poscategories.Length; i++)
            {
                Poscategory poscategory = poscategories[i];

                // Initialize the filter
                filter = new PredicateExpression();
                filter.Add(PoscategoryFields.ExternalId == poscategory.ExternalId);
                filter.Add(PoscategoryFields.CompanyId == companyId);

                if (!string.IsNullOrWhiteSpace(revenueCenter))
                {
                    filter.Add(PoscategoryFields.RevenueCenter == revenueCenter);
                }

                // Set the filter to the EntityView
                poscategoryView.Filter = filter;

                // Check whether items have been found
                PoscategoryEntity poscategoryEntity = null;
                poscategory.CompanyId = companyId;
                if (poscategoryView.Count == 0)
                {
                    // Not found                    
                }
                else if (poscategoryView.Count == 1)
                {
                    // Pos category found in database
                    poscategoryEntity = poscategoryView[0];
                }
                else if (poscategoryView.Count > 1)
                {
                    // Multiple pos categories found in database for the specified external id, error!
                    throw new ObymobiException(ImportPoscategoriesResult.MultiplePoscategoriesFound, "Multiple pos categories found for the specified external id '{0}'.", poscategory.ExternalId);
                }

                // Update field values
                poscategoryEntity = CreateUpdatePoscategoryEntityFromModel(poscategory, poscategoryEntity);

                try
                {
                    bool wasNew = poscategoryEntity.IsNew;

                    // Set Created or Updated batch to this batch.
                    if (wasNew)
                        poscategoryEntity.CreatedInBatchId = batchId;
                    else if (poscategoryEntity.IsDirty)
                        poscategoryEntity.UpdatedInBatchId = batchId;

                    // Make this backwards compatible by filling it anyway.
                    if (!poscategoryEntity.CreatedInBatchId.HasValue)
                        poscategoryEntity.CreatedInBatchId = batchId;

                    poscategoryEntity.SynchronisationBatchId = batchId;

                    if (poscategoryEntity.Save())
                    {
                        if (wasNew)
                        {
                            poscategoryEntity.Refetch();
                            poscategoryView.RelatedCollection.Add(poscategoryEntity);
                        }
                    }
                    else
                        throw new ObymobiException(GenericResult.EntitySaveException, "Save() returned false");
                }
                catch (Exception ex)
                {
                    throw new ObymobiException(GenericResult.EntitySaveException, ex);
                }
            }

        }

        /// <summary>
        /// Gets the category by poscategory id or a new one if none was found.
        /// </summary>
        /// <param name="poscategoryId">The poscategory id.</param>
        /// <param name="categoryView">The category view.</param>
        /// <returns>
        /// A (new) <see cref="CategoryEntity"/> instance
        /// </returns>
        public static CategoryEntity GetCategoryByPoscategoryId(int poscategoryId, EntityView<CategoryEntity> categoryView)
        {
            // Create the CategoryEntity instance
            CategoryEntity category = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.PoscategoryId == poscategoryId);

            // Set the filter to the category view
            categoryView.Filter = filter;

            // Check whether a category was found
            if (categoryView.Count == 0)
                category = new CategoryEntity();
            else if (categoryView.Count == 1)
                category = categoryView[0];
            else if (categoryView.Count > 1)
                throw new ObymobiException(SynchronizeCategoriesResult.MultipleCategoriesFound, "Multiple categories found for the pos category id '{0}'.", poscategoryId);

            return category;
        }

        /// <summary>
        /// Gets the poscategory by external id or a new one if none was found.
        /// </summary>
        /// <param name="externalId">The external id.</param>
        /// <param name="poscategoryView">The poscategory view.</param>
        /// <returns>
        /// A (new) <see cref="PoscategoryEntity"/> instance
        /// </returns>
        public static PoscategoryEntity GetPoscategoryByExternalId(string externalId, EntityView<PoscategoryEntity> poscategoryView)
        {
            // Create the PoscategoryEntity instance
            PoscategoryEntity poscategory = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PoscategoryFields.ExternalId == externalId);

            // Set the filter to the POS category view
            poscategoryView.Filter = filter;

            // Check whether a POS category was found
            if (poscategoryView.Count == 0)
                poscategory = new PoscategoryEntity();
            else if (poscategoryView.Count == 1)
                poscategory = poscategoryView[0];
            else if (poscategoryView.Count > 1)
                throw new ObymobiException(ImportPoscategoriesResult.MultiplePoscategoriesFound, "Multiple pos categories found for external id '{0}'.", externalId);

            return poscategory;
        }

        /// <summary>
        /// Gets a PoscategoryCollection for the specified company id
        /// </summary>
        /// <param name="companyId">The id of the company to get the PoscategoryCollection for</param>
        /// <returns>An Obymobi.Data.PoscategoryCollection instance</returns>
        public static PoscategoryCollection GetPoscategoryCollection(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PoscategoryFields.CompanyId == companyId);

            PoscategoryCollection poscategoryCollection = new PoscategoryCollection();
            poscategoryCollection.GetMulti(filter);

            return poscategoryCollection;
        }

        /// <summary>
        /// Creates the poscategory model from entity.
        /// </summary>
        /// <param name="categoryEntity">The category entity.</param>
        /// <returns></returns>
        public static Poscategory CreatePoscategoryModelFromEntity(CategoryEntity categoryEntity)
        {
            if (!categoryEntity.PoscategoryId.HasValue)
                throw new ObymobiException(PoscategoryHelperResult.PoscategoryMissingForCategory, "CategoryId: {0}", categoryEntity.CategoryId);

            PoscategoryEntity poscategoryEntity = categoryEntity.PoscategoryEntity;

            Poscategory poscategoryModel = new Poscategory();

            poscategoryModel.PoscategoryId = poscategoryEntity.PoscategoryId;
            poscategoryModel.CompanyId = poscategoryEntity.CompanyId;
            poscategoryModel.ExternalId = poscategoryEntity.ExternalId;
            poscategoryModel.Name = poscategoryEntity.Name;
            poscategoryModel.FieldValue1 = poscategoryEntity.FieldValue1;
            poscategoryModel.FieldValue2 = poscategoryEntity.FieldValue2;
            poscategoryModel.FieldValue3 = poscategoryEntity.FieldValue3;
            poscategoryModel.FieldValue4 = poscategoryEntity.FieldValue4;
            poscategoryModel.FieldValue5 = poscategoryEntity.FieldValue5;
            poscategoryModel.FieldValue6 = poscategoryEntity.FieldValue6;
            poscategoryModel.FieldValue7 = poscategoryEntity.FieldValue7;
            poscategoryModel.FieldValue8 = poscategoryEntity.FieldValue8;
            poscategoryModel.FieldValue9 = poscategoryEntity.FieldValue9;
            poscategoryModel.FieldValue10 = poscategoryEntity.FieldValue10;

            poscategoryModel.RevenueCenter = poscategoryEntity.RevenueCenter;

            if (categoryEntity.ProductId.HasValue && categoryEntity.ProductEntity.PosproductId.HasValue)
                poscategoryModel.PosproductExternalId = categoryEntity.ProductEntity.PosproductEntity.ExternalId;
            else
                poscategoryModel.PosproductExternalId = string.Empty;

            return poscategoryModel;
        }

        /// <summary>
        /// Creates / Updates a PoscategoryEntity instance for the specified poscategory instance
        /// </summary>
        /// <param name="poscategory">The poscategory instance to create the PosdeliverypointEntity instance for</param>
        /// <param name="poscategoryEntity">The poscategoryEntity entity to be updated / created.</param>
        /// <returns>
        /// A new PoscategoryEntity, or the updated supplied one.
        /// </returns>
        public static PoscategoryEntity CreateUpdatePoscategoryEntityFromModel(Poscategory poscategory, PoscategoryEntity poscategoryEntity)
        {
            if (poscategoryEntity == null)
                poscategoryEntity = new PoscategoryEntity();

            poscategoryEntity.CompanyId = poscategory.CompanyId;
            poscategoryEntity.ExternalId = poscategory.ExternalId;
            poscategoryEntity.Name = poscategory.Name;

            poscategoryEntity.FieldValue1 = poscategory.FieldValue1;
            poscategoryEntity.FieldValue2 = poscategory.FieldValue2;
            poscategoryEntity.FieldValue3 = poscategory.FieldValue3;
            poscategoryEntity.FieldValue4 = poscategory.FieldValue4;
            poscategoryEntity.FieldValue5 = poscategory.FieldValue5;
            poscategoryEntity.FieldValue6 = poscategory.FieldValue6;
            poscategoryEntity.FieldValue7 = poscategory.FieldValue7;
            poscategoryEntity.FieldValue8 = poscategory.FieldValue8;
            poscategoryEntity.FieldValue9 = poscategory.FieldValue8;
            poscategoryEntity.FieldValue10 = poscategory.FieldValue10;

            poscategoryEntity.RevenueCenter = poscategory.RevenueCenter;


            return poscategoryEntity;
        }
    }
}
