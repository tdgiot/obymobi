﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    public class ApiAuthenticationHelper
    {
        public static ApiAuthenticationEntity GetApiAuthenticationEntityByType(ApiAuthenticationType type, int companyId)
        {
            ApiAuthenticationEntity apiAuthenticationEntity = null;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ApiAuthenticationFields.Type == type);
            filter.Add(ApiAuthenticationFields.CompanyId == companyId);

            ApiAuthenticationCollection apiAuthenticationCollection = new ApiAuthenticationCollection();
            apiAuthenticationCollection.GetMulti(filter);

            if (apiAuthenticationCollection.Count > 0)
                apiAuthenticationEntity = apiAuthenticationCollection[0];

            return apiAuthenticationEntity;
        }
    }
}
