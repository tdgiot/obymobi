﻿using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
	/// <summary>
	/// Category Helper
	/// </summary>
	public class CategoryHelper
	{
		#region Original implementation

        public static bool? HasGeofencedParent(CategoryEntity category)
        {
            bool? geofenced = null;

            if (category.Geofencing.HasValue)
                geofenced = category.Geofencing.Value;
            else if (category.ParentCategoryId.HasValue)
                geofenced = HasGeofencedParent(category.ParentCategoryEntity);

            return geofenced;
        }

        /// <summary>
        /// Gets the color from parent.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public static int GetColorFromParent(CategoryEntity category)
        {
            int color = 0;

            if (category.Color > 0)
                color = category.Color;
            else if (category.ParentCategoryId.HasValue)
                color = GetColorFromParent(category.ParentCategoryEntity);

            return color;
        }

        /// <summary>
        /// Gets the button text from parent.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public static string GetButtonTextFromParent(CategoryEntity category)
        {
            string text = string.Empty;

            if (!category.ButtonText.IsNullOrWhiteSpace())
                text = category.ButtonText;
            else if (category.ParentCategoryId.HasValue)
                text = CategoryHelper.GetButtonTextFromParent(category.ParentCategoryEntity);

            return text;
        }

        /// <summary>
        /// Gets the customize button text from parent.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <returns></returns>
        public static string GetCustomizeButtonTextFromParent(CategoryEntity category)
        {
            string text = string.Empty;

            if (!category.CustomizeButtonText.IsNullOrWhiteSpace())
                text = category.CustomizeButtonText;
            else if (category.ParentCategoryId.HasValue)
                text = CategoryHelper.GetCustomizeButtonTextFromParent(category.ParentCategoryEntity);

            return text;
        }

	    #endregion
    }    
}
