﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;
using System.IO;
using Obymobi.Logic.Routing;
using Dionysos.Net.Mail;

namespace Obymobi.Logic.HelperClasses
{
    public class GameSessionReportHelper
    {
        public static void CreateAndSendGameSessionReport(GameSessionReportConfigurationEntity gameSessionReportConfigurationEntity, DateTime start, DateTime end)
        {
            GameSessionReportEntity gameSessionReportEntity = CreateGameSessionReport(gameSessionReportConfigurationEntity, start, end);
            SendGameSessionReport(gameSessionReportEntity, false);
        }

        public static GameSessionReportEntity CreateGameSessionReport(GameSessionReportConfigurationEntity gameSessionReportConfigurationEntity, DateTime start, DateTime end)
        {
            Dictionary<string, List<GameSessionEntity>> gameSessionsPerDeliverypoint = new Dictionary<string, List<GameSessionEntity>>();

            List<GameSessionEntity> gameSessions = GameSessionHelper.GetGameSessions(gameSessionReportConfigurationEntity.CompanyId, start, end);
            foreach (GameSessionEntity gameSessionEntity in gameSessions)
            {
                string deliverypointName = gameSessionEntity.DeliverypointName;

                if (gameSessionReportConfigurationEntity.CompanyId == 343) // ARIA
                {
                    List<string> postfixes = new List<string> { " B", " BL", " BR", " L", " D" };
                    foreach (string postfix in postfixes)
                    {
                        if (deliverypointName.EndsWith(postfix))
                            deliverypointName = deliverypointName.Substring(0, deliverypointName.Length - postfix.Length);
                    }
                }

                if (!gameSessionsPerDeliverypoint.ContainsKey(deliverypointName))
                    gameSessionsPerDeliverypoint.Add(deliverypointName, new List<GameSessionEntity>());

                gameSessionsPerDeliverypoint[deliverypointName].Add(gameSessionEntity);
            }

            GameSessionReportEntity gameSessionReportEntity = new GameSessionReportEntity();
            gameSessionReportEntity.CompanyId = gameSessionReportConfigurationEntity.CompanyId;
            gameSessionReportEntity.Recipients = gameSessionReportConfigurationEntity.Recipients;

            foreach (string deliverypointName in gameSessionsPerDeliverypoint.Keys.OrderBy(x => x))
            {
                foreach (GameSessionEntity gameSessionEntity in gameSessionsPerDeliverypoint[deliverypointName])
                {
                    GameSessionReportItemEntity gameSessionReportItemEntity = new GameSessionReportItemEntity();
                    gameSessionReportItemEntity.ParentCompanyId = gameSessionReportEntity.CompanyId;
                    gameSessionReportItemEntity.DeliverypointName = deliverypointName;
                    gameSessionReportItemEntity.GameName = gameSessionEntity.GameName;
                    gameSessionReportItemEntity.StartUTC = gameSessionEntity.StartUTC;
                    gameSessionReportItemEntity.EndUTC = gameSessionEntity.EndUTC;
                    gameSessionReportItemEntity.Length = gameSessionEntity.Length;
                    gameSessionReportEntity.GameSessionReportItemCollection.Add(gameSessionReportItemEntity);
                }
            }

            if (!gameSessionReportEntity.Save(true))
                return null;
            return gameSessionReportEntity;
        }

        public static void SendGameSessionReport(GameSessionReportEntity gameSessionReportEntity, bool test)
        {
            CompanyEntity companyEntity = gameSessionReportEntity.CompanyEntity;

            // Create the report email
            StringBuilder emailBody = new StringBuilder();

            if (gameSessionReportEntity.GameSessionReportItemCollection.Count > 0)
            {
                emailBody.AppendFormatLine("<h1 style=\"color: #555;text-shadow: none;font-family: Arial, sans-serif;margin-top: 5px;\">Rooms to be charged for playing games:</h1>");
                emailBody.AppendFormatLine("<table width=\"570\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin: 0;\">");
                emailBody.AppendFormatLine("<tr>");
                emailBody.AppendFormatLine("<td align=\"left\" width=\"175\" style=\"padding: 0 15px 0 0; font-size: 16px;\"><br>");
                emailBody.AppendFormatLine("<strong>Room number</strong>");
                emailBody.AppendFormatLine("</td>");
                emailBody.AppendFormatLine("<td align=\"left\" width=\"200\" style=\"padding: 0 15px 0 0; font-size: 16px;\"><br>");
                emailBody.AppendFormatLine("<strong>Time of first play</strong>");
                emailBody.AppendFormatLine("</td>");
                emailBody.AppendFormatLine("<td align=\"left\" width=\"150\" style=\"padding: 0 15px 0 0; font-size: 16px;\"><br>");
                emailBody.AppendFormatLine("<strong>Total time played</strong>");
                emailBody.AppendFormatLine("</td>");
                emailBody.AppendFormatLine("</tr>");

                IEnumerable<IGrouping<string, GameSessionReportItemEntity>> query = gameSessionReportEntity.GameSessionReportItemCollection.GroupBy(x => x.DeliverypointName);
                foreach (IGrouping<string, GameSessionReportItemEntity> group in query.OrderByDescending(x => x.Min(y => y.StartUTC)))
                {
                    string deliverypointName = group.Key;
                    int length = group.Sum(x => x.Length);

                    emailBody.AppendFormatLine("<tr>");
                    emailBody.AppendFormatLine("<td align=\"left\" width=\"175\" style=\"padding: 0 15px 0 0; font-size: 16px;\"><br>");
                    emailBody.AppendFormatLine("{0}", deliverypointName);
                    emailBody.AppendFormatLine("</td>");
                    emailBody.AppendFormatLine("<td align=\"left\" width=\"200\" style=\"padding: 0 15px 0 0; font-size: 16px;\"><br>");
                    emailBody.AppendFormatLine("{0}", group.Min(x => x.StartUTC).UtcToLocalTime(companyEntity.TimeZoneInfo).ToShortTimeString());
                    emailBody.AppendFormatLine("</td>");
                    emailBody.AppendFormatLine("<td align=\"left\" width=\"150\" style=\"padding: 0 15px 0 0; font-size: 16px;\"><br>");
                    emailBody.AppendFormatLine("{0} mins", length);
                    emailBody.AppendFormatLine("</td>");
                    emailBody.AppendFormatLine("</tr>");
                }

                emailBody.AppendFormatLine("</table>");

                emailBody.AppendFormatLine("<h1 style=\"color: #555;text-shadow: none;font-family: Arial, sans-serif;margin-top: 40px;\">Details:</h1>");
                emailBody.AppendFormatLine("<table width=\"570\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin: 0;\">");
                emailBody.AppendFormatLine("<tr>");
                emailBody.AppendFormatLine("<td align=\"left\" width=\"175\" style=\"padding: 0 15px 0 0; font-size: 16px;\"><br>");
                emailBody.AppendFormatLine("<strong>Room number</strong>");
                emailBody.AppendFormatLine("</td>");
                emailBody.AppendFormatLine("<td align=\"left\" width=\"200\" style=\"padding: 0 15px 0 0; font-size: 16px;\"><br>");
                emailBody.AppendFormatLine("<strong>Time of first play</strong>");
                emailBody.AppendFormatLine("</td>");
                emailBody.AppendFormatLine("<td align=\"left\" width=\"150\" style=\"padding: 0 15px 0 0; font-size: 16px;\"><br>");
                emailBody.AppendFormatLine("<strong>Time played</strong>");
                emailBody.AppendFormatLine("</td>");
                emailBody.AppendFormatLine("</tr>");

                foreach (IGrouping<string, GameSessionReportItemEntity> group in query.OrderByDescending(x => x.Min(y => y.StartUTC)))
                {
                    string deliverypointName = group.Key;
                    int length = group.Sum(x => x.Length);

                    emailBody.AppendFormatLine("<tr>");
                    emailBody.AppendFormatLine("<td align=\"left\" width=\"175\" style=\"padding: 0 15px 0 0; font-size: 16px;\"><br>");
                    emailBody.AppendFormatLine("{0}", deliverypointName);
                    emailBody.AppendFormatLine("</td>");
                    emailBody.AppendFormatLine("<td align=\"left\" width=\"200\" style=\"padding: 0 15px 0 0; font-size: 16px;\"><br>");
                    emailBody.AppendFormatLine("{0}", group.Min(x => x.StartUTC).UtcToLocalTime(companyEntity.TimeZoneInfo).ToShortTimeString());
                    emailBody.AppendFormatLine("</td>");
                    emailBody.AppendFormatLine("<td align=\"left\" width=\"150\" style=\"padding: 0 15px 0 0; font-size: 16px;\"><br>");
                    emailBody.AppendFormatLine("{0} mins", length);
                    emailBody.AppendFormatLine("</td>");
                    emailBody.AppendFormatLine("</tr>");

                    foreach (GameSessionReportItemEntity gameSessionReportItemEntity in group)
                    {
                        emailBody.AppendFormatLine("<tr>");
                        emailBody.AppendFormatLine("<td align=\"left\" width=\"160\" style=\"padding: 0 15px 0 15px; font-size: 12px; color: gray;\"><br>");
                        emailBody.AppendFormatLine("{0}", gameSessionReportItemEntity.GameName);
                        emailBody.AppendFormatLine("</td>");
                        emailBody.AppendFormatLine("<td align=\"left\" width=\"200\" style=\"padding: 0 15px 0 0; font-size: 12px; color: gray;\"><br>");
                        emailBody.AppendFormatLine("{0}", gameSessionReportItemEntity.StartUTC.UtcToLocalTime(companyEntity.TimeZoneInfo).ToShortTimeString());
                        emailBody.AppendFormatLine("</td>");
                        emailBody.AppendFormatLine("<td align=\"left\" width=\"150\" style=\"padding: 0 15px 0 0; font-size: 12px; color: gray;\"><br>");
                        emailBody.AppendFormatLine("{0} mins", gameSessionReportItemEntity.Length);
                        emailBody.AppendFormatLine("</td>");
                        emailBody.AppendFormatLine("</tr>");
                    }
                }

                emailBody.AppendFormatLine("</table>");
            }

            // Send the report
            if (emailBody.Length > 0)
            {
                string[] recipients = gameSessionReportEntity.Recipients.Split(',', StringSplitOptions.RemoveEmptyEntries);

                string emailHtml = Properties.Resources.GameCharges;
                emailHtml = emailHtml.Replace("[[BODY]]", emailBody.ToString());

                foreach (string recipient in recipients)
                {
                    const int timeout = 30000;

                    var mailUtil = new MailUtilV2("info@crave-emenu.com", recipient.Trim(), "Crave Game Charges - " + companyEntity.Name);
                    mailUtil.SmtpClient.Timeout = timeout;
                    mailUtil.BodyHtml = emailHtml;
                    mailUtil.SendMail().Wait(timeout);
                }

                if (!test)
                {
                    // Set the sent date
                    gameSessionReportEntity.Sent = DateTime.UtcNow;
                    gameSessionReportEntity.Save();
                }
            }
        }
    }
}
