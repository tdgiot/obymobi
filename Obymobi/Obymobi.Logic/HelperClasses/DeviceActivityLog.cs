using System;
using Dionysos;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    public class DeviceActivityLog
    {
        #region Constructors

        public DeviceActivityLog(NetmessageEntity netmessage)
        {
            this.Entity = netmessage;

            if (netmessage.CreatedUTC.HasValue)
                this.DateTime = netmessage.CreatedUTC.Value.UtcToLocalTime();

            this.TypeText = $"Netmessage: {netmessage.MessageType.ToEnum<NetmessageType>()}";
            this.DescriptionTextShort = NetmessageHelper.CreateTypedNetmessageModelFromEntity(netmessage).ToString().Excerpt(250);
        }

        public DeviceActivityLog(ClientLogEntity clientlog)
        {
            this.Entity = clientlog;

            if (clientlog.CreatedUTC.HasValue)
                this.DateTime = clientlog.CreatedUTC.Value.UtcToLocalTime();

            this.TypeText = $"Log: {clientlog.TypeName}";
            this.DescriptionTextShort = clientlog.Message.Excerpt(250);
        }

        public DeviceActivityLog(ClientStateEntity clientState)
        {
            this.Entity = clientState;

            if (clientState.CreatedUTC.HasValue)
                this.DateTime = clientState.CreatedUTC.Value.UtcToLocalTime();

            this.TypeText = clientState.Message;
            this.DescriptionTextShort = clientState.ToString().Excerpt(250);
        }

        public DeviceActivityLog(TerminalLogEntity terminallog)
        {
            this.Entity = terminallog;

            if (terminallog.CreatedUTC.HasValue)
                this.DateTime = terminallog.CreatedUTC.Value.UtcToLocalTime();

            this.TypeText = $"Log: {terminallog.TypeName}";
            this.DescriptionTextShort = terminallog.Message;
            if (!terminallog.Log.IsNullOrWhiteSpace())
            {
                this.DescriptionTextShort += $" {terminallog.Log}";
            }
            this.DescriptionTextShort = this.DescriptionTextShort.Excerpt(250);
        }

        public DeviceActivityLog(TerminalStateEntity terminalState)
        {
            this.Entity = terminalState;
            
            if (terminalState.CreatedUTC.HasValue)
                this.DateTime = terminalState.CreatedUTC.Value.UtcToLocalTime();

            this.TypeText = terminalState.Message;
            this.DescriptionTextShort = terminalState.ToString().Excerpt(250);
        }

        #endregion

        #region Properties

        public IEntity Entity { get; set; }
        public DateTime? DateTime { get; set; }
        public string TypeText { get; set; }
        public string DescriptionTextShort { get; set; }
        public string LinkToEntityPageNoc
        {
            get
            {
                return string.Format("~/Company/DeviceActivityLog.aspx?id={0}&entity={1}", this.Entity.PrimaryKeyFields[0].CurrentValue, this.Entity.LLBLGenProEntityName);
            }
        }
        public string LinkToEntityPageCms
        {
            get
            {
                if (this.Entity.LLBLGenProEntityTypeValue == (int)EntityType.NetmessageEntity)
                    return string.Format("~/Company/Netmessage.aspx?id={0}", this.Entity.PrimaryKeyFields[0].CurrentValue);
                else if (this.Entity.LLBLGenProEntityTypeValue == (int)EntityType.ClientLogEntity)
                    return string.Format("~/Company/ClientLog.aspx?id={0}", this.Entity.PrimaryKeyFields[0].CurrentValue);
                else if (this.Entity.LLBLGenProEntityTypeValue == (int)EntityType.ClientStateEntity)
                    return string.Format("~/Company/ClientState.aspx?id={0}", this.Entity.PrimaryKeyFields[0].CurrentValue);
                else if (this.Entity.LLBLGenProEntityTypeValue == (int)EntityType.TerminalLogEntity)
                    return string.Format("~/Company/TerminalLog.aspx?id={0}", this.Entity.PrimaryKeyFields[0].CurrentValue);
                else if (this.Entity.LLBLGenProEntityTypeValue == (int)EntityType.TerminalStateEntity)
                    return string.Format("~/Company/TerminalState.aspx?id={0}", this.Entity.PrimaryKeyFields[0].CurrentValue);
                else
                    return string.Empty;
            }
        }

        #endregion
    }
}
