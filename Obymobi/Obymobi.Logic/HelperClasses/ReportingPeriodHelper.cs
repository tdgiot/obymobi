﻿using System;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Logic.HelperClasses
{
    public class ReportingPeriodHelper
    {
        public static Tuple<DateTime, DateTime> GetDateTimeForPeriod(ReportingPeriod period, TimeZoneInfo timeZoneInfo)
        {
            DateTime utcNow = DateTime.UtcNow;

            DateTime fromLocal = utcNow.UtcToLocalTime(timeZoneInfo);
            DateTime untilLocal = utcNow.UtcToLocalTime(timeZoneInfo);
            
            bool makeWholeDay = false;
            switch (period)
            {
                case ReportingPeriod.Today:
                    break;
                case ReportingPeriod.TradingDay:
                    fromLocal = fromLocal.AddDays(-1);
                    break;
                case ReportingPeriod.Yesterday:
                    fromLocal = fromLocal.AddDays(-1);
                    untilLocal = untilLocal.AddDays(-1).MakeEndOfDay();
                    break;
                case ReportingPeriod.Last2days:
                    fromLocal = fromLocal.AddDays(-2);
                    break;
                case ReportingPeriod.Last5days:
                    fromLocal = fromLocal.AddDays(-5);
                    break;
                case ReportingPeriod.Last7days:
                    fromLocal = fromLocal.AddDays(-7);
                    break;
                case ReportingPeriod.Last14days:
                    fromLocal = fromLocal.AddDays(-14);
                    break;
                case ReportingPeriod.Last28days:
                    fromLocal = fromLocal.AddDays(-28);
                    break;
                case ReportingPeriod.Last30days:
                    fromLocal = fromLocal.AddDays(-30);
                    break;
                case ReportingPeriod.Last60days:
                    fromLocal = fromLocal.AddDays(-60);
                    break;
                case ReportingPeriod.Last90days:
                    fromLocal = fromLocal.AddDays(-90);
                    break;
                case ReportingPeriod.Last180days:
                    fromLocal = fromLocal.AddDays(-180);
                    break;
                case ReportingPeriod.Last365days:
                    fromLocal = fromLocal.AddDays(-365);
                    break;
                case ReportingPeriod.CurrentMonth:
                    fromLocal = fromLocal.GetFirstDayOfMonth();
                    untilLocal = fromLocal.GetLastDayOfMonth();
                    makeWholeDay = true;
                    break;
                case ReportingPeriod.CurrentQuarter:
                    fromLocal = fromLocal.GetFirstDayOfQuarter();
                    untilLocal = untilLocal.GetLastDayOfQuarter();
                    makeWholeDay = true;
                    break;
                case ReportingPeriod.CurrentYear:
                    fromLocal = fromLocal.GetFirstDayOfYear();
                    untilLocal = fromLocal.GetLastDayOfYear();
                    makeWholeDay = true;
                    break;
                case ReportingPeriod.LastMonth:
                    fromLocal = fromLocal.AddMonths(-1).GetFirstDayOfMonth();
                    untilLocal = fromLocal.GetLastDayOfMonth();
                    makeWholeDay = true;
                    break;
                case ReportingPeriod.LastQuarter:
                    fromLocal = fromLocal.AddMonths(-3).GetFirstDayOfQuarter();
                    untilLocal = fromLocal.GetLastDayOfQuarter();
                    makeWholeDay = true;
                    break;
                case ReportingPeriod.LastYear:
                    fromLocal = fromLocal.AddYears(-1).GetFirstDayOfYear();
                    untilLocal = fromLocal.GetLastDayOfYear();
                    makeWholeDay = true;
                    break;
            }

            if (makeWholeDay)
            {
                fromLocal = fromLocal.MakeBeginOfDay();
                untilLocal = untilLocal.MakeEndOfDay();
            }

            return new Tuple<DateTime, DateTime>(fromLocal, untilLocal);
        }
    }
}