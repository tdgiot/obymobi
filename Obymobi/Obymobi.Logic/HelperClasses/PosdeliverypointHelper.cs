﻿using System;
using System.Text;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos.Data.LLBLGen;
using Dionysos;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// PosdeliverypointHelper class
    /// </summary>
    public class PosdeliverypointHelper
    {
        /// <summary>
        /// Cleans up.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="cleanupReport">The cleanup report.</param>
        /// <param name="batchId">The batch id.</param>
        public static void CleanUp(int companyId, ref StringBuilder cleanupReport, out long batchId)
        {
            cleanupReport.AppendLine("*** START: Delete Posdeliverypoint entities ***");
            batchId = -1;
            // Prepate filter for company
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosdeliverypointFields.CompanyId == companyId);

            // Create collection and use for Scalar MAX
            PosdeliverypointCollection posdeliverypoints = new PosdeliverypointCollection();
            object maxBatchId = posdeliverypoints.GetScalar(Data.PosdeliverypointFieldIndex.SynchronisationBatchId, null, AggregateFunction.Max, filter);

            // Only proceed when we have an actual maxBatchId
            if (maxBatchId != DBNull.Value && maxBatchId is long)
            {
                // Retrieve all order posdeliverypoints & delete them
                cleanupReport.AppendFormatLine("BatchId: {0}", maxBatchId);
                batchId = (long)maxBatchId;
                // Prepare filter for older posdeliverypoints
                filter = new PredicateExpression();
                filter.Add(PosdeliverypointFields.CompanyId == companyId);
                filter.Add(PosdeliverypointFields.SynchronisationBatchId < maxBatchId);

                posdeliverypoints.GetMulti(filter);

                // Delete per piece
                foreach (var posdeliverypoint in posdeliverypoints)
                {
                    cleanupReport.AppendFormatLine("ExternalId: {0}, Name: {1}",
                        posdeliverypoint.ExternalId, posdeliverypoint.Name);
                    posdeliverypoint.Delete();
                }
                cleanupReport.AppendFormatLine("{0} entities deleted", posdeliverypoints.Count);
            }

            cleanupReport.AppendLine("*** FINISHED: Delete Posdeliverypoint entities ***");
        }

        /// <summary>
        /// Gets the posdeliverypoint by external id.
        /// </summary>
        /// <param name="externalPosdeliverypointId">The external posdeliverypoint id.</param>
        /// <returns>Returns null when not found</returns>
        public PosdeliverypointEntity GetExternalId(string externalPosdeliverypointId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosdeliverypointFields.ExternalId == externalPosdeliverypointId);

            return LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<PosdeliverypointEntity>(filter);
        }

        /// <summary>
        /// Gets the id of the deliverypointgroup using the specified external pos deliverypoint group id
        /// </summary>
        /// <param name="externalPosdeliverypointgroupId">The external id of the pos deliverypointgroup</param>
        /// <param name="companyId">The id of the company</param>
        /// <returns>A System.Int32 instance containing the deliverypointgroup id</returns>
        public static int GetDeliverypointgroupId(string externalPosdeliverypointgroupId, int companyId)
        {
            int deliverypointGroupId = -1;

            PredicateExpression posdeliverypointgroupFilter = new PredicateExpression();
            posdeliverypointgroupFilter.Add(PosdeliverypointgroupFields.ExternalId == externalPosdeliverypointgroupId);
            posdeliverypointgroupFilter.Add(PosdeliverypointgroupFields.CompanyId == companyId);

            PosdeliverypointgroupCollection posdeliverypointgroupCollection = new PosdeliverypointgroupCollection();
            posdeliverypointgroupCollection.GetMulti(posdeliverypointgroupFilter);

            if (posdeliverypointgroupCollection.Count == 0)
            {
                // No pos deliverypointgroup found, look for the default deliverypoint group
                PredicateExpression deliverypointgroupFilter = new PredicateExpression();
                deliverypointgroupFilter.Add(DeliverypointgroupFields.CompanyId == companyId);

                DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();
                deliverypointgroupCollection.GetMulti(deliverypointgroupFilter);

                if (deliverypointgroupCollection.Count >= 1)
                    deliverypointGroupId = deliverypointgroupCollection[0].DeliverypointgroupId;

            }
            else if (posdeliverypointgroupCollection.Count == 1)
            {
                PosdeliverypointgroupEntity posdeliverypointgroupEntity = posdeliverypointgroupCollection[0];

                PredicateExpression deliverypointgroupFilter = new PredicateExpression();
                deliverypointgroupFilter.Add(DeliverypointgroupFields.PosdeliverypointgroupId == posdeliverypointgroupEntity.PosdeliverypointgroupId);
                deliverypointgroupFilter.Add(DeliverypointgroupFields.CompanyId == companyId);

                DeliverypointgroupCollection deliverypointgroupCollection = new DeliverypointgroupCollection();
                deliverypointgroupCollection.GetMulti(deliverypointgroupFilter);

                if (deliverypointgroupCollection.Count == 0)
                {
                    // No deliverypoint group found, error
                }
                else if (deliverypointgroupCollection.Count == 1)
                {
                    deliverypointGroupId = deliverypointgroupCollection[0].DeliverypointgroupId;
                }
                else if (deliverypointgroupCollection.Count > 1)
                {
                    // Multiple deliverypointgroup found, error
                }
            }
            else if (posdeliverypointgroupCollection.Count > 1)
            {
                // Multiple pos deliverypointgroup found, error
            }

            return deliverypointGroupId;
        }

        /// <summary>
        /// Gets the external id of the pos deliverypoint for the specified deliverypoint number
        /// </summary>
        /// <param name="deliverypointNumber">The number of the deliverypoint to get the pos deliverypoint id for</param>
        /// <param name="companyId">The id of the company</param>
        /// <returns>A System.String instance containing the external id</returns>
        public static string GetPosdeliverypointExternalId(int deliverypointNumber, int companyId)
        {
            string externalId = string.Empty;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointFields.Number == deliverypointNumber);
            filter.Add(DeliverypointFields.CompanyId == companyId);

            RelationCollection relations = new RelationCollection();
            relations.Add(DeliverypointEntity.Relations.PosdeliverypointEntityUsingPosdeliverypointId);

            PosdeliverypointCollection posdeliverypointCollection = new PosdeliverypointCollection();
            posdeliverypointCollection.GetMulti(filter, relations);

            if (posdeliverypointCollection.Count == 0)
            {
                // No pos deliverypoint found, error
                throw new ObymobiException(GetPosDeliverypointExternalIdResult.NoPosdeliverypointFound, "No pos deliverypoints found for deliverypoint number '{0}'.", deliverypointNumber);
            }
            else if (posdeliverypointCollection.Count == 1)
            {
                // Pos deliverypoint found
                externalId = posdeliverypointCollection[0].ExternalId;
            }
            else if (posdeliverypointCollection.Count > 1)
            {
                // Multiple pos deliverypoints found, error
                throw new ObymobiException(GetPosDeliverypointExternalIdResult.MultiplePosdeliverypointsFound, "Multiple pos deliverypoints found for deliverypoint number '{0}'.", deliverypointNumber);
            }

            return externalId;
        }

        /// <summary>
        /// Import the deliverypoints from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="xml">The Xml string containing the serialized array of Posdeliverypoint instances</param>
        /// <param name="companyId">The id of the company to import the pos deliverypoints for</param>
        /// <param name="batchId">The batch id.</param>
        public static void ImportPosdeliverypoints(string xml, int companyId, long batchId)
        {
            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPosdeliverypointsResult.XmlIsEmpty, "Xml string containing serialized Posdeliverypoint array is empty.");
            else if (companyId <= 1)
                throw new ObymobiException(ImportPosdeliverypointsResult.CompanyIdIsZeroOrLess, "The specified company id '{0}' is smaller than 1.", companyId);
            else
            {
                // Deserialize the xml in order to get the array of Posdeliverypoint instances
                Posdeliverypoint[] posdeliverypoints = XmlHelper.Deserialize<Posdeliverypoint[]>(xml);
                ImportPosdeliverypoints(posdeliverypoints, companyId, batchId);
            }
        }

        /// <summary>
        /// Import the deliverypoints from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="xml">The Xml string containing the serialized array of Posdeliverypoint instances</param>
        /// <param name="companyId">The id of the company to import the pos deliverypoints for</param>
        /// <param name="batchId">The batch id.</param>
        public static void ImportPosdeliverypoints(Posdeliverypoint[] posdeliverypoints, int companyId, long batchId, string revenueCenter = null)
        {
            // Get a PosdeliverypointCollection for the specified company
            PosdeliverypointCollection posdeliverypointCollection = GetPosdeliverypointCollection(companyId);

            // Create an EntityView for the PosdeliverypointCollection
            EntityView<PosdeliverypointEntity> posdeliverypointView = posdeliverypointCollection.DefaultView;

            // Create a filter
            PredicateExpression filter;

            // Walk through the Posdeliverypoint instances of the array
            for (int i = 0; i < posdeliverypoints.Length; i++)
            {
                Posdeliverypoint posdeliverypoint = posdeliverypoints[i];

                // Initialize the filter
                filter = new PredicateExpression(PosdeliverypointFields.ExternalId == posdeliverypoint.ExternalId);

                if (!string.IsNullOrWhiteSpace(revenueCenter))
                {
                    filter.Add(PosdeliverypointFields.RevenueCenter == revenueCenter);
                }

                // Set the filter to the EntityView
                posdeliverypointView.Filter = filter;

                // Check whether items have been found
                PosdeliverypointEntity posdeliverypointEntity = null;
                posdeliverypoint.CompanyId = companyId;
                if (posdeliverypointView.Count == 0)
                {
                    // No pos deliverypoint found in database, add                        
                }
                else if (posdeliverypointView.Count == 1)
                {
                    // Pos deliverypoint found in database, update
                    posdeliverypointEntity = posdeliverypointView[0];
                }
                else if (posdeliverypointView.Count > 1)
                {
                    // Multiple pos deliverypoints found in database for the specified external id, error!
                    throw new ObymobiException(ImportPosdeliverypointsResult.MultiplePosdeliverypointsFound, "Multiple pos deliverypoints found for the specified external id '{0}'.", posdeliverypoint.ExternalId);
                }

                // Update Fields
                posdeliverypointEntity = CreateUpdatePosdeliverypointEntityFromModel(posdeliverypoint, posdeliverypointEntity);

                // Set Name if empty
                if (posdeliverypointEntity.Name.IsNullOrWhiteSpace())
                    posdeliverypointEntity.Name = posdeliverypointEntity.ExternalId;

                try
                {
                    bool wasNew = posdeliverypointEntity.IsNew;

                    // Set Created or Updated batch to this batch.
                    if (wasNew)
                        posdeliverypointEntity.CreatedInBatchId = batchId;
                    else if (posdeliverypointEntity.IsDirty)
                        posdeliverypointEntity.UpdatedInBatchId = batchId;

                    // Make this backwards compatible by filling it anyway.
                    if (!posdeliverypointEntity.CreatedInBatchId.HasValue)
                        posdeliverypointEntity.CreatedInBatchId = batchId;

                    posdeliverypointEntity.SynchronisationBatchId = batchId;

                    if (posdeliverypointEntity.Save())
                    {
                        if (wasNew)
                        {
                            posdeliverypointEntity.Refetch();
                            posdeliverypointView.RelatedCollection.Add(posdeliverypointEntity);
                        }
                    }
                    else
                        throw new ObymobiException(GenericResult.EntitySaveException, "Save() returned false");
                }
                catch (Exception ex)
                {
                    throw new ObymobiException(GenericResult.EntitySaveException, ex);
                }
            }
        }

        /// <summary>
        /// Gets a PosdeliverypointCollection for the specified company id
        /// </summary>
        /// <param name="companyId">The id of the company to get the PosdeliverypointCollection for</param>
        /// <returns>
        /// An Obymobi.Data.PosdeliverypointCollection instance
        /// </returns>
        public static PosdeliverypointCollection GetPosdeliverypointCollection(int companyId)
        {
            PosdeliverypointCollection posdeliverypointCollection = new PosdeliverypointCollection();
            posdeliverypointCollection.GetMulti(new PredicateExpression(PosdeliverypointFields.CompanyId == companyId));

            return posdeliverypointCollection;
        }

        /// <summary>
        /// Creates the posalterationitem model from entity.
        /// </summary>
        /// <param name="posdeliverypointEntity">The posdeliverypoint entity.</param>
        /// <returns></returns>
        public static Posdeliverypoint CreatePosalterationitemModelFromEntity(PosdeliverypointEntity posdeliverypointEntity)
        {
            Posdeliverypoint posdeliverypointModel = new Posdeliverypoint();

            posdeliverypointModel.PosdeliverypointId = posdeliverypointEntity.PosdeliverypointId;
            posdeliverypointModel.CompanyId = posdeliverypointEntity.CompanyId;
            posdeliverypointModel.ExternalId = posdeliverypointEntity.ExternalId;
            posdeliverypointModel.ExternalPosdeliverypointgroupId = posdeliverypointEntity.ExternalPosdeliverypointgroupId;
            posdeliverypointModel.Name = posdeliverypointEntity.Name;
            posdeliverypointModel.Number = posdeliverypointEntity.ExternalId;

            posdeliverypointModel.FieldValue1 = posdeliverypointEntity.FieldValue1;
            posdeliverypointModel.FieldValue2 = posdeliverypointEntity.FieldValue2;
            posdeliverypointModel.FieldValue3 = posdeliverypointEntity.FieldValue3;
            posdeliverypointModel.FieldValue4 = posdeliverypointEntity.FieldValue4;
            posdeliverypointModel.FieldValue5 = posdeliverypointEntity.FieldValue5;
            posdeliverypointModel.FieldValue6 = posdeliverypointEntity.FieldValue6;
            posdeliverypointModel.FieldValue7 = posdeliverypointEntity.FieldValue7;
            posdeliverypointModel.FieldValue8 = posdeliverypointEntity.FieldValue8;
            posdeliverypointModel.FieldValue9 = posdeliverypointEntity.FieldValue9;
            posdeliverypointModel.FieldValue10 = posdeliverypointEntity.FieldValue10;

            posdeliverypointModel.RevenueCenter = posdeliverypointEntity.RevenueCenter;

            return posdeliverypointModel;
        }

        /// <summary>
        /// Creates / Updates a PosdeliverypointEntity instance for the specified Posdeliverypoint instance
        /// </summary>
        /// <param name="posdeliverypoint">The Posdeliverypoint instance to create the PosdeliverypointEntity instance for</param>
        /// <param name="posdeliverypointEntity">The posdeliverypoint entity to be updated / created.</param>
        /// <returns>
        /// A new PosdeliverypointEntity, or the updated supplied one.
        /// </returns>
        public static PosdeliverypointEntity CreateUpdatePosdeliverypointEntityFromModel(Posdeliverypoint posdeliverypoint, PosdeliverypointEntity posdeliverypointEntity)
        {
            if (posdeliverypointEntity == null)
                posdeliverypointEntity = new PosdeliverypointEntity();

            posdeliverypointEntity.CompanyId = posdeliverypoint.CompanyId;
            posdeliverypointEntity.ExternalId = posdeliverypoint.ExternalId;
            posdeliverypointEntity.ExternalPosdeliverypointgroupId = posdeliverypoint.ExternalPosdeliverypointgroupId;
            posdeliverypointEntity.Name = posdeliverypoint.Name;

            posdeliverypointEntity.FieldValue1 = posdeliverypoint.FieldValue1;
            posdeliverypointEntity.FieldValue2 = posdeliverypoint.FieldValue2;
            posdeliverypointEntity.FieldValue3 = posdeliverypoint.FieldValue3;
            posdeliverypointEntity.FieldValue4 = posdeliverypoint.FieldValue4;
            posdeliverypointEntity.FieldValue5 = posdeliverypoint.FieldValue5;
            posdeliverypointEntity.FieldValue6 = posdeliverypoint.FieldValue6;
            posdeliverypointEntity.FieldValue7 = posdeliverypoint.FieldValue7;
            posdeliverypointEntity.FieldValue8 = posdeliverypoint.FieldValue8;
            posdeliverypointEntity.FieldValue9 = posdeliverypoint.FieldValue8;
            posdeliverypointEntity.FieldValue10 = posdeliverypoint.FieldValue10;

            posdeliverypointEntity.RevenueCenter = posdeliverypoint.RevenueCenter;

            return posdeliverypointEntity;
        }
    }
}
