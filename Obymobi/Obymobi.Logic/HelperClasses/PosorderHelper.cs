﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.Model;
using Obymobi.Enums;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// PosorderHelper class
    /// </summary>
    public class PosorderHelper
    {
        /// <summary>
        /// Creates a Posorder instance from an Order instance
        /// </summary>
        /// <param name="order">The Order instance to create the Posorder for</param>
        /// <returns>An Obymobi.Logic.Model.Posorder instance</returns>
        public static Posorder CreatePosorderFromOrder(Order order)
        {
            Posorder posorder = new Posorder();
            posorder.PosorderId = order.OrderId;
            posorder.PosdeliverypointExternalId = PosdeliverypointHelper.GetPosdeliverypointExternalId(order.DeliverypointNumber, order.CompanyId);
            //posorder.PospaymentmethodExternalId = PospaymentmethodHelper.GetPospaymentmethodExternalId(order.CompanyId, order.PaymentmethodId);
            posorder.Type = order.Type;

            posorder.FieldValue1 = order.FieldValue1;
            posorder.FieldValue2 = order.FieldValue2;
            posorder.FieldValue3 = order.FieldValue3;
            posorder.FieldValue4 = order.FieldValue4;
            posorder.FieldValue5 = order.FieldValue5;
            posorder.FieldValue6 = order.FieldValue6;
            posorder.FieldValue7 = order.FieldValue7;
            posorder.FieldValue8 = order.FieldValue8;
            posorder.FieldValue9 = order.FieldValue8;
            posorder.FieldValue10 = order.FieldValue10;

            posorder.GuestCount = order.GuestCount;

            List<Posorderitem> orderitems = new List<Posorderitem>();

            int categoryId = -1;
            for (int i = 0; i < order.Orderitems.Length; i++)
            {
                Orderitem orderitem = order.Orderitems[i];

                #region Add a category header if needed

                // Get the product
                ProductEntity product = new ProductEntity(orderitem.ProductId);
                if (!product.IsNew)
                {
                    if (product.ProductCategoryCollection.Count > 0 &&                                  // Check whether the product belongs to a category
                        categoryId != product.ProductCategoryCollection[0].CategoryId)                  // Check whether the current category is different than the category of the product
                    {
                        categoryId = product.ProductCategoryCollection[0].CategoryId;

                        // Get the category
                        CategoryEntity category = product.ProductCategoryCollection[0].CategoryEntity;

                        // Recursively get the category products of parent categories
                        // and add them to the orderitem list
                        List<Posorderitem> categoryOrderitems = new List<Posorderitem>();
                        orderitems.AddRange(GetCategoryProducts(category, categoryOrderitems, orderitem.OrderitemId));

                        // Check whether the category is connected to a pos product
                        if (category.ProductId.HasValue && category.ProductEntity.PosproductId.HasValue)
                        {
                            PosproductEntity categoryPosproduct = product.ProductCategoryCollection[0].CategoryEntity.ProductEntity.PosproductEntity;

                            Posorderitem categoryItem = new Posorderitem();
                            categoryItem.PosorderitemId = orderitem.OrderitemId + category.ProductId.Value;
                            categoryItem.PosproductExternalId = categoryPosproduct.ExternalId;
                            categoryItem.Quantity = 1;
                            categoryItem.ProductPriceIn = 0;
                            categoryItem.Description = categoryPosproduct.Name;

                            orderitems.Add(categoryItem);
                        }
                    }
                }

                #endregion

                // Add the orderitem
                Posorderitem item = PosorderitemHelper.CreatePosorderitemFromOrderitem(orderitem);
                orderitems.Add(item);
            }

            posorder.Posorderitems = orderitems.ToArray();

            return posorder;
        }

        private static List<Posorderitem> GetCategoryProducts(CategoryEntity category, List<Posorderitem> categoryOrderitems, int orderItemId)
        {
            List<Posorderitem> orderitems = categoryOrderitems;

            if (category.ParentCategoryId.HasValue)
            {
                if (category.ParentCategoryEntity.ProductId.HasValue && category.ParentCategoryEntity.ProductEntity.PosproductId.HasValue)
                {
                    Posorderitem categoryItem = new Posorderitem();
                    categoryItem.PosorderitemId = orderItemId + category.ParentCategoryEntity.ProductId.Value;
                    categoryItem.PosproductExternalId = category.ParentCategoryEntity.ProductEntity.PosproductEntity.ExternalId;
                    categoryItem.Quantity = 1;
                    categoryItem.ProductPriceIn = 0;
                    categoryItem.Description = category.ParentCategoryEntity.ProductEntity.PosproductEntity.Name;

                    orderitems.Add(categoryItem);
                }

                GetCategoryProducts(category.ParentCategoryEntity, orderitems, orderItemId);
            }

            return orderitems;
        }
    }
}
