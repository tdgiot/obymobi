﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// ProductHelper class
    /// </summary>
    public static class ProductHelper
    {
        public static ProductCollection GetProductCollectionForCompany(int companyId, PrefetchPath path, ProductType? productType, SortExpression sort = null, IncludeFieldsList includes = null)
        {
            ProductCollection products = new ProductCollection();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == companyId);

            if (productType.HasValue)
                filter.Add(ProductFields.Type == (int)productType.Value);

            products.GetMulti(filter, 0, sort, null, path, includes, 0, 0);

            return products;
        }

        /// <summary>
        /// Gets the product collection containing products that cna be ordered for the company with the specified id.
        /// </summary>
        /// <param name="companyId">The id of the company to get the orderable products for.</param>
        /// <returns></returns>
        public static ProductCollection GetOrderableProductCollectionForCompany(int companyId, bool NameFieldOnly)
        {
            PredicateExpression filterProducts = new PredicateExpression();
            filterProducts.Add(ProductFields.CompanyId == companyId);
            filterProducts.Add(ProductFields.VisibilityType != VisibilityType.Never);
            filterProducts.Add(ProductCategoryFields.ProductCategoryId != DBNull.Value);
            filterProducts.Add(CategoryFields.ParentCategoryId != DBNull.Value);
            filterProducts.Add(ProductFields.Type == (int)ProductType.Product);
            
            PredicateExpression excludeServiceRequestsFilter = new PredicateExpression();
            excludeServiceRequestsFilter.Add(CategoryFields.Type != CategoryType.ServiceItems);
            excludeServiceRequestsFilter.Add(ProductFields.SubType == ProductSubType.Inherit);

            PredicateExpression subFilter = new PredicateExpression();
            subFilter.Add(ProductFields.SubType == (int)ProductSubType.Normal);
            subFilter.AddWithOr(excludeServiceRequestsFilter);

            filterProducts.Add(subFilter);

            ExcludeIncludeFieldsList fieldsList;
            if (NameFieldOnly)
            {
                fieldsList = new IncludeFieldsList();
                fieldsList.Add(ProductFields.Name);
            }
            else
                 fieldsList = new ExcludeIncludeFieldsList();

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductEntity.Relations.ProductCategoryEntityUsingProductId, JoinHint.Left);
            relations.Add(ProductCategoryEntity.Relations.CategoryEntityUsingCategoryId);

            SortExpression sortProduct = new SortExpression(ProductFields.Name | SortOperator.Ascending);

            ProductCollection products = new ProductCollection();
            products.GetMulti(filterProducts, 0, sortProduct, relations, null, fieldsList, 0, 0);

            return products;
        }

        /// <summary>
        /// Gets the alteration id by the posalteration external id.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="externalId">The external id.</param>
        /// <param name="transaction">The transaction.</param>
        /// <returns></returns>
        public static int GetProductIdByPosproductExternalId(int companyId, string externalId, Transaction transaction)
        {
            int productId = -1;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosproductFields.ExternalId == externalId);
            filter.Add(PosproductFields.CompanyId == companyId);

            RelationCollection relations = new RelationCollection();
            relations.Add(PosproductEntity.Relations.ProductEntityUsingPosproductId);

            ProductCollection products = new ProductCollection();
            products.AddToTransaction(transaction);
            products.GetMulti(filter, relations);

            if (products.Count == 0)
                throw new ObymobiException(GetProductIdResult.NoProductFound, "No product found for the specified external id '{0}'.", externalId);
            else if (products.Count >= 1)
                productId = products[0].ProductId;
            //else
            //    throw new ObymobiException(GetProductIdResult.MultipleProductsFound, "Multiple products found for the specified external id '{0}'.", externalId);

            return productId;
        }

        public static ProductCollection GetProductsForCompanyAndAlterationType(int companyId, AlterationType type)
        {
            PredicateExpression filter = new PredicateExpression(ProductFields.CompanyId == companyId);
            filter.Add(AlterationFields.Type == type);

            RelationCollection relations = new RelationCollection(ProductEntityBase.Relations.ProductAlterationEntityUsingProductId);
            relations.Add(ProductAlterationEntityBase.Relations.AlterationEntityUsingAlterationId);

            ProductCollection products = new ProductCollection();
            products.GetMulti(filter, relations);

            return products;
        }

        public static ProductCategoryCollection GetProductCategories(int companyId, bool onlyVisible, int categoryId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == companyId);

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductCategoryEntityBase.Relations.ProductEntityUsingProductId);
            relations.Add(ProductCategoryEntityBase.Relations.CategoryEntityUsingCategoryId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductCategoryEntity);
            prefetch.Add(ProductCategoryEntityBase.PrefetchPathProductEntity);
            prefetch.Add(ProductCategoryEntityBase.PrefetchPathCategoryEntity).SubPath.Add(CategoryEntityBase.PrefetchPathMenuEntity);

            SortExpression sort = new SortExpression(new SortClause(ProductFields.Name, SortOperator.Ascending));

            if (onlyVisible)
            {
                filter.Add(ProductFields.VisibilityType != VisibilityType.Never);
            }

            if (categoryId > 0)
            {
                filter.Add(ProductCategoryFields.CategoryId == categoryId);
            }

            return ProductHelper.GetProductCategories(filter, sort, relations, prefetch);
        }

        public static ProductCategoryCollection GetProductCategories(PredicateExpression filter, SortExpression sort, RelationCollection relations, PrefetchPath prefetch)
        {
            ProductCategoryCollection productCategories = new ProductCategoryCollection();
            productCategories.GetMulti(filter, 0, sort, relations, prefetch);

            return productCategories;
        }
    }
}

