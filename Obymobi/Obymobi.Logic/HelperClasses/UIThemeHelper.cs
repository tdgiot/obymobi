﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using Obymobi.Enums;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// UICThemeHelper class
    /// </summary>
    public static class UIThemeHelper
    {
        public static int GetCustomColorOrDefault(UITheme theme, UIColorType colorType)
        {
            int color = 0;
            UIThemeColor uiThemeColor = theme.UIThemeColors.FirstOrDefault(x => x.Type == (int)colorType);
            if(uiThemeColor != null)
            {
                color = uiThemeColor.Color;
            }
            else
            {
                UIColor uiColor = UIColors.GetByType(colorType);
                color = uiColor != null ? uiColor.Color : 0;
            }
            return color;
        }
    }
}