﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Constants;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// DeliverypointHelper class
    /// </summary>
    public class DeliverypointHelper
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="deliverypointId"></param>
        /// <param name="prefetchGroupAndTerminal"></param>
        /// <returns>A DeliverypointEntity instance if the deliverypoint was found, null if not</returns>
        public static DeliverypointEntity GetDeliverypoint(int deliverypointId, bool prefetchGroupAndTerminal)
        {
            DeliverypointEntity deliverypoint = null;

            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointFields.DeliverypointId == deliverypointId);

            RelationCollection joins = new RelationCollection();
            joins.Add(DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

            PrefetchPath path = new PrefetchPath(EntityType.DeliverypointEntity);
            path.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity);

            // Create and initialize a DeliverypointCollection instance
            // and retrieve the items using the filter
            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            if (prefetchGroupAndTerminal)
                deliverypointCollection.GetMulti(filter, 0, null, joins, path);
            else
                deliverypointCollection.GetMulti(filter, 0, null, joins, null);

            if (deliverypointCollection.Count == 1)
                deliverypoint = deliverypointCollection[0];

            return deliverypoint;
        }

        /// <summary>
        /// Gets a DeliverypointEntity instance for the specified company id and number
        /// </summary>
        /// <param name="companyId">The id of the company</param>
        /// <param name="number">The number of the deliverypoint</param>
        /// <param name="prefetchGroupAndTerminal">if set to <c>true</c> [prefetch group and terminal].</param>
        /// <returns>
        /// A DeliverypointEntity instance if the deliverypoint was found, null if not
        /// </returns>
        public static DeliverypointEntity GetDeliverypoint(int companyId, int number, bool prefetchGroupAndTerminal)
        {
            return GetDeliverypoint(companyId, number.ToString(), prefetchGroupAndTerminal);
        }

        /// <summary>
        /// Gets a DeliverypointEntity instance for the specified company id and number
        /// </summary>
        /// <param name="companyId">The id of the company</param>
        /// <param name="number">The number of the deliverypoint</param>
        /// <param name="prefetchGroupAndTerminal">if set to <c>true</c> [prefetch group and terminal].</param>
        /// <param name="deliverypointgroupId">The id of the deliverypointgroup.</param>
        /// <returns>
        /// A DeliverypointEntity instance if the deliverypoint was found, null if not. 
        /// Exception is thrown when multiple deliverypoints are found with the same number.
        /// </returns>
        public static DeliverypointEntity GetDeliverypoint(int companyId, string number, bool prefetchGroupAndTerminal, bool pmsDeliverypointgroup = false, int? deliverypointgroupId = null)
        {
            DeliverypointEntity deliverypoint = null;

            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointgroupFields.CompanyId == companyId);
            filter.Add(DeliverypointFields.Number == number);

            if (pmsDeliverypointgroup)
            {
                filter.Add(DeliverypointgroupFields.PmsIntegration == true);
            }

            RelationCollection joins = new RelationCollection();
            joins.Add(DeliverypointEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

            PrefetchPath path = null;
            if (prefetchGroupAndTerminal)
            {
                path = new PrefetchPath(EntityType.DeliverypointEntity);
                path.Add(DeliverypointEntity.PrefetchPathDeliverypointgroupEntity);
            }

            // Create and initialize a DeliverypointCollection instance
            // and retrieve the items using the filter
            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.GetMulti(filter, 0, null, joins, path);

            if (deliverypointCollection.Count > 1 && deliverypointgroupId.HasValue)
            {
                deliverypointCollection.Clear();

                filter.Add(DeliverypointgroupFields.DeliverypointgroupId == deliverypointgroupId);
                
                deliverypointCollection.GetMulti(filter, 0, null, joins, path);
            }

            if (deliverypointCollection.Count == 1)
            {
                deliverypoint = deliverypointCollection[0];
            }
            else if (deliverypointCollection.Count > 1)
            {
                throw new ApplicationException(string.Format("Multiple deliverypoints found for deliverypoint number '{0}'", number));
            }

            return deliverypoint;
        }

        public static DeliverypointCollection GetDeliverypointsLinkedToOnlineClientsForCompany(int companyId)
        {
            IncludeFieldsList deliverypointFields = new IncludeFieldsList();
            deliverypointFields.Add(DeliverypointFields.DeliverypointId);
            deliverypointFields.Add(DeliverypointFields.Number);
            deliverypointFields.Add(DeliverypointFields.DeliverypointgroupId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointFields.CompanyId == companyId);
            filter.Add(DeviceFields.LastRequestUTC >= DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1));

            RelationCollection relations = new RelationCollection();
            relations.Add(DeliverypointEntityBase.Relations.ClientEntityUsingDeliverypointId);
            relations.Add(ClientEntity.Relations.DeviceEntityUsingDeviceId);

            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.GetMulti(filter, 0, null, relations, null, deliverypointFields, 0, 0);

            return deliverypointCollection;
        }
    }
}
