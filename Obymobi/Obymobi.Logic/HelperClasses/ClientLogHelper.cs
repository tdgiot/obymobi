﻿using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// ClientLogHelper class
    /// </summary>
    public class ClientLogHelper
    {
        /// <summary>
        /// Creates a client log entity using the specified client id and log
        /// </summary>
        /// <param name="clientId">The id of the client to save the client log for</param>
        /// <param name="log">The actual log</param>
        /// <returns>A <see cref="ClientLogEntity"/> instance containig the</returns>
        public static ClientLogEntity CreateClientLogEntity(int clientId, ClientLogType type, string log, long fileDate, string filename = "")
        {
            var clientLog = new ClientLogEntity();
            clientLog.ClientId = clientId;
            clientLog.Type = (int)type;

            if (type == ClientLogType.ShippedLog)
            {
                DateTime dt;

                if (fileDate > 0)
                {
                    var parsedDate = fileDate.FromUnixTimeMillis();
                    dt = new DateTime(parsedDate.Year, parsedDate.Month, parsedDate.Day, 0, 0, 0, 0);
                }
                else
                {
                    dt = DateTime.Today;
                }

                var clientLogFileEntity = new ClientLogFileEntity();
                clientLogFileEntity.Message = log;
                clientLogFileEntity.LogDate = dt;
                clientLogFileEntity.Application = filename.ToLower();
                clientLogFileEntity.Save();

                clientLog = new ClientLogEntity();
                clientLog.ClientId = clientId;
                clientLog.Type = (int)type;
                clientLog.Message = filename + " " + dt.ToString("dd-MM-yyyy");
                clientLog.ClientLogFileId = clientLogFileEntity.ClientLogFileId;
            }
            else
            {
                clientLog.Message = log;
            }

            return clientLog;
        }
    }
}
