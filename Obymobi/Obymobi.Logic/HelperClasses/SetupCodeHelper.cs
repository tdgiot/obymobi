﻿using System;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
    public class SetupCodeHelper
    {
        public static EmenuConfiguration GetConfiguration(string setupCode)
        {
            EmenuConfiguration config = new EmenuConfiguration();
            config.ErrorCode = 0;
            config.Error = "OK";

            // Fetch setup code information
            PredicateExpression setupFilter = new PredicateExpression();
            setupFilter.Add(SetupCodeFields.Code == setupCode);
            setupFilter.Add(SetupCodeFields.ExpireDateUTC > DateTime.UtcNow);

            SetupCodeCollection setupCollection = new SetupCodeCollection();
            setupCollection.GetMulti(setupFilter);
            if (setupCollection.Count == 0)
            {
                config.ErrorCode = 100;
                config.Error = "Invalid setup code or code has expired";
                return config;
            }

            SetupCodeEntity setupEntity = setupCollection[0];

            // Fetch company, dpg and deliverypoint information
            PredicateExpression companyFilter = new PredicateExpression();
            companyFilter.Add(CompanyFields.CompanyId == setupEntity.CompanyId);
            
            PredicateExpression dpgFilter = new PredicateExpression();
            dpgFilter.Add(DeliverypointgroupFields.DeliverypointgroupId == setupEntity.DeliverypointgroupId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyOwnerEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathWifiConfigurationCollection);

            IPrefetchPathElement dpgPrefetch = prefetch.Add(CompanyEntityBase.PrefetchPathDeliverypointgroupCollection, 0, dpgFilter);
            dpgPrefetch.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathDeliverypointCollection);
            

            RelationCollection relation = new RelationCollection();
            relation.Add(CompanyEntityBase.Relations.DeliverypointgroupEntityUsingCompanyId);
            relation.Add(CompanyEntityBase.Relations.DeliverypointEntityUsingCompanyId);
            relation.Add(DeliverypointgroupEntityBase.Relations.DeliverypointEntityUsingDeliverypointgroupId);

            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(companyFilter, 0, null, relation, prefetch);

            if (companyCollection.Count == 0)
            {
                config.Error = "No company found for this setup code";
                config.ErrorCode = 101;
                return config;
            }

            CompanyEntity companyEntity = companyCollection[0];

            // Convert entity to config
            config.CompanyId = companyEntity.CompanyId;
            config.CompanyName = companyEntity.Name;

            config.CompanyOwnerUsername = companyEntity.CompanyOwnerEntity.Username;
            config.CompanyOwnerPassword = companyEntity.CompanyOwnerEntity.DecryptedPassword;

            DeliverypointgroupEntity deliverypointgroupEntity = companyEntity.DeliverypointgroupCollection[0];
            config.DeliverypointgroupId = deliverypointgroupEntity.DeliverypointgroupId;
            config.DeliverypointgroupName = deliverypointgroupEntity.Name;

            foreach (DeliverypointEntity deliverypointEntity in deliverypointgroupEntity.DeliverypointCollection)
            {
                EmenuConfiguration.Deliverypoint deliverypoint = new EmenuConfiguration.Deliverypoint();
                deliverypoint.DeliverypointId = deliverypointEntity.DeliverypointId;
                deliverypoint.DeliverypointName = deliverypointEntity.Name;
                deliverypoint.DeliverypointNumber = deliverypointEntity.Number;

                config.Deliverypoints.Add(deliverypoint);
            }

            foreach (WifiConfigurationEntity wifiEntity in companyEntity.WifiConfigurationCollection)
            {
                EmenuConfiguration.WifiConfiguration wifiConfig = new EmenuConfiguration.WifiConfiguration
                {
                    Ssid = wifiEntity.Ssid,
                    HiddenSsid = wifiEntity.HiddenSsid,
                    Security = wifiEntity.Security,
                    SecurityKey = wifiEntity.SecurityKey
                };
                config.WifiConfigurations.Add(wifiConfig);
            }

            return config;
        } 
    }
}