﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// PosdeliverypointgroupHelper class
    /// </summary>
    public class PosdeliverypointgroupHelper
    {
        /// <summary>
        /// Cleans up.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="cleanupReport">The cleanup report.</param>
        /// <param name="batchId">The batch id.</param>
        public static void CleanUp(int companyId, ref StringBuilder cleanupReport, out long batchId)
        {
            cleanupReport.AppendLine("*** START: Delete Posdeliverypointgroup entities ***");
            batchId = -1;
            // Prepate filter for company
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosdeliverypointgroupFields.CompanyId == companyId);

            // Create collection and use for Scalar MAX
            PosdeliverypointgroupCollection posdeliverypointgroups = new PosdeliverypointgroupCollection();
            object maxBatchId = posdeliverypointgroups.GetScalar(Data.PosdeliverypointgroupFieldIndex.SynchronisationBatchId, null, AggregateFunction.Max, filter);

            // Only proceed when we have an actual maxBatchId
            if (maxBatchId != DBNull.Value && maxBatchId is long)
            {
                // Retrieve all order posdeliverypointgroups & delete them
                cleanupReport.AppendFormatLine("BatchId: {0}", maxBatchId);
                batchId = (long)maxBatchId;
                // Prepare filter for older posdeliverypointgroups
                filter = new PredicateExpression();
                filter.Add(PosdeliverypointgroupFields.CompanyId == companyId);
                filter.Add(PosdeliverypointgroupFields.SynchronisationBatchId < maxBatchId);

                posdeliverypointgroups.GetMulti(filter);

                // Delete per piece
                foreach (var posdeliverypointgroup in posdeliverypointgroups)
                {
                    cleanupReport.AppendFormatLine("ExternalId: {0}, Name: {1}",
                        posdeliverypointgroup.ExternalId, posdeliverypointgroup.Name);
                    posdeliverypointgroup.Delete();
                }
                cleanupReport.AppendFormatLine("{0} entities deleted", posdeliverypointgroups.Count);
            }

            cleanupReport.AppendLine("*** FINISHED: Delete Posdeliverypointgroup entities ***");
        }

        /// <summary>
        /// Import the deliverypointgroups from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="xml">The Xml string containing the serialized array of Posdeliverypointgroup instances</param>
        /// <param name="companyId">The id of the company to import the pos deliverypointgroups for</param>
        /// <param name="batchId">The batch id.</param>
        public static void ImportPosdeliverypointgroups(string xml, int companyId, long batchId)
        {
            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPosdeliverypointgroupsResult.XmlIsEmpty, "Xml string containing serialized Posdeliverypointgroup array is empty.");
            else if (companyId <= 1)
                throw new ObymobiException(ImportPosdeliverypointgroupsResult.CompanyIdIsZeroOrLess, "The specified company id '{0}' is smaller than 1.", companyId);
            else
            {
                // Deserialize the xml in order to get the array of Posdeliverypointgroup instances
                Posdeliverypointgroup[] posdeliverypointgroups = XmlHelper.Deserialize<Posdeliverypointgroup[]>(xml);
                ImportPosdeliverypointgroups(posdeliverypointgroups, companyId, batchId);
            }
        }

        /// <summary>
        /// Import the deliverypointgroups from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="xml">The Xml string containing the serialized array of Posdeliverypointgroup instances</param>
        /// <param name="companyId">The id of the company to import the pos deliverypointgroups for</param>
        /// <param name="batchId">The batch id.</param>
        public static void ImportPosdeliverypointgroups(Posdeliverypointgroup[] posdeliverypointgroups, int companyId, long batchId, string revenueCenter = null)
        {
            // Get a PosdeliverypointCollection for the specified company
            PosdeliverypointgroupCollection posdeliverypointgroupCollection = GetPosdeliverypointgroupCollection(companyId);

            // Create an EntityView for the PosdeliverypointgroupCollection
            EntityView<PosdeliverypointgroupEntity> posdeliverypointgroupView = posdeliverypointgroupCollection.DefaultView;

            // Create a filter
            PredicateExpression filter = null;

            // Walk through the Posdeliverypointgroup instances of the array
            for (int i = 0; i < posdeliverypointgroups.Length; i++)
            {
                Posdeliverypointgroup posdeliverypointgroup = posdeliverypointgroups[i];

                // Initialize the filter
                filter = new PredicateExpression(PosdeliverypointgroupFields.ExternalId == posdeliverypointgroup.ExternalId);

                if (!string.IsNullOrWhiteSpace(revenueCenter))
                {
                    filter.Add(PosdeliverypointgroupFields.RevenueCenter == revenueCenter);
                }

                // Set the filter to the EntityView
                posdeliverypointgroupView.Filter = filter;

                // Check whether items have been found
                PosdeliverypointgroupEntity posdeliverypointgroupEntity = null;
                posdeliverypointgroup.CompanyId = companyId;
                if (posdeliverypointgroupView.Count == 0)
                {
                    // No pos deliverypoint found in database, add
                }
                else if (posdeliverypointgroupView.Count == 1)
                {
                    // Pos deliverypoint found in database, update
                    posdeliverypointgroupEntity = posdeliverypointgroupView[0];
                }
                else if (posdeliverypointgroupView.Count > 1)
                {
                    // Multiple pos deliverypointgroups found in database for the specified external id, error!
                    throw new ObymobiException(ImportPosdeliverypointgroupsResult.MultiplePosdeliverypointgroupsFound, "Multiple pos deliverypointgroups found for the specified external id '{0}'.", posdeliverypointgroup.ExternalId);
                }

                // Update Fields
                posdeliverypointgroupEntity = CreateUpdatePosdeliverypointgroupEntityFromModel(posdeliverypointgroup, posdeliverypointgroupEntity);

                // Then, create a PosdeliverypointEntity instance
                // and save it to the persistent storage                    


                try
                {
                    bool wasNew = posdeliverypointgroupEntity.IsNew;

                    // Set Created or Updated batch to this batch.
                    if (wasNew)
                        posdeliverypointgroupEntity.CreatedInBatchId = batchId;
                    else if (posdeliverypointgroupEntity.IsDirty)
                        posdeliverypointgroupEntity.UpdatedInBatchId = batchId;

                    // Make this backwards compatible by filling it anyway.
                    if (!posdeliverypointgroupEntity.CreatedInBatchId.HasValue)
                        posdeliverypointgroupEntity.CreatedInBatchId = batchId;

                    posdeliverypointgroupEntity.SynchronisationBatchId = batchId;

                    if (posdeliverypointgroupEntity.Save())
                    {
                        if (wasNew)
                        {
                            posdeliverypointgroupEntity.Refetch();
                            posdeliverypointgroupView.RelatedCollection.Add(posdeliverypointgroupEntity);
                        }
                    }
                    else
                        throw new ObymobiException(GenericResult.EntitySaveException, "Save() returned false");
                }
                catch (Exception ex)
                {
                    throw new ObymobiException(GenericResult.EntitySaveException, ex);
                }
            }
        }

        /// <summary>
        /// Gets a PosdeliverypointgroupCollection for the specified company id
        /// </summary>
        /// <param name="companyId">The id of the company to get the PosdeliverypointgroupCollection for</param>
        /// <returns>An Obymobi.Data.PosdeliverypointgroupCollection instance</returns>
        public static PosdeliverypointgroupCollection GetPosdeliverypointgroupCollection(int companyId)
        {
            PosdeliverypointgroupCollection posdeliverypointgroupCollection = new PosdeliverypointgroupCollection();
            posdeliverypointgroupCollection.GetMulti(new PredicateExpression(PosdeliverypointgroupFields.CompanyId == companyId));

            return posdeliverypointgroupCollection;
        }

        /// <summary>
        /// Creates / Updates a PosdeliverypointgroupEntity instance for the specified Posdeliverypointgroup instance
        /// </summary>
        /// <param name="posdeliverypointgroup">The Posdeliverypointgroup instance to create the PosdeliverypointEntity instance for</param>
        /// <param name="posdeliverypointgroupEntity">The PosdeliverypointgroupEntity entity to be updated / created.</param>
        /// <returns>
        /// A new PosdeliverypointgroupEntity, or the updated supplied one.
        /// </returns>
        public static PosdeliverypointgroupEntity CreateUpdatePosdeliverypointgroupEntityFromModel(Posdeliverypointgroup posdeliverypointgroup, PosdeliverypointgroupEntity posdeliverypointgroupEntity)
        {
            if (posdeliverypointgroupEntity == null)
                posdeliverypointgroupEntity = new PosdeliverypointgroupEntity();

            posdeliverypointgroupEntity.CompanyId = posdeliverypointgroup.CompanyId;
            posdeliverypointgroupEntity.ExternalId = posdeliverypointgroup.ExternalId;
            posdeliverypointgroupEntity.Name = posdeliverypointgroup.Name;

            posdeliverypointgroupEntity.FieldValue1 = posdeliverypointgroup.FieldValue1;
            posdeliverypointgroupEntity.FieldValue2 = posdeliverypointgroup.FieldValue2;
            posdeliverypointgroupEntity.FieldValue3 = posdeliverypointgroup.FieldValue3;
            posdeliverypointgroupEntity.FieldValue4 = posdeliverypointgroup.FieldValue4;
            posdeliverypointgroupEntity.FieldValue5 = posdeliverypointgroup.FieldValue5;
            posdeliverypointgroupEntity.FieldValue6 = posdeliverypointgroup.FieldValue6;
            posdeliverypointgroupEntity.FieldValue7 = posdeliverypointgroup.FieldValue7;
            posdeliverypointgroupEntity.FieldValue8 = posdeliverypointgroup.FieldValue8;
            posdeliverypointgroupEntity.FieldValue9 = posdeliverypointgroup.FieldValue8;
            posdeliverypointgroupEntity.FieldValue10 = posdeliverypointgroup.FieldValue10;

            posdeliverypointgroupEntity.RevenueCenter = posdeliverypointgroup.RevenueCenter;

            return posdeliverypointgroupEntity;
        }
    }
}
