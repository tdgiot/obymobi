﻿using System.Collections.Generic;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// PosorderitemHelper class
    /// </summary>
    public class PosorderitemHelper
    {
        /// <summary>
        /// Creates a Posorderitem instance from an Orderitem instance
        /// </summary>
        /// <param name="orderitem">The Orderitem instance to create the Posorderitem for</param>
        /// <returns>An Obymobi.Logic.Model.Posorderitem instance</returns>
        public static Posorderitem CreatePosorderitemFromOrderitem(Orderitem orderitem)
        {
            Posorderitem item = new Posorderitem();
            item.PosorderitemId = orderitem.OrderitemId;
            item.PosproductExternalId = PosproductHelper.GetPosproductExternalId(orderitem.ProductId);
            item.Quantity = orderitem.Quantity;
            item.ProductPriceIn = orderitem.PriceInPerPiece;
            item.Description = orderitem.ProductName;

            item.FieldValue1 = orderitem.FieldValue1;
            item.FieldValue2 = orderitem.FieldValue2;
            item.FieldValue3 = orderitem.FieldValue3;
            item.FieldValue4 = orderitem.FieldValue4;
            item.FieldValue5 = orderitem.FieldValue5;
            item.FieldValue6 = orderitem.FieldValue6;
            item.FieldValue7 = orderitem.FieldValue7;
            item.FieldValue8 = orderitem.FieldValue8;
            item.FieldValue9 = orderitem.FieldValue8;
            item.FieldValue10 = orderitem.FieldValue10;

            item.Notes = orderitem.Notes;
            item.Type = orderitem.Type;

            if (orderitem.Alterationitems != null && orderitem.Alterationitems.Length > 0)
            {
                List<Posalterationitem> posalterationitems = new List<Posalterationitem>();

                foreach (Alterationitem alterationitem in orderitem.Alterationitems)
                {
                    Posalterationitem posalterationitem = new Posalterationitem();
                    posalterationitem.PosalterationitemId = orderitem.OrderitemId + alterationitem.AlterationoptionId;
                    posalterationitem.ExternalPosalterationId = PosalterationHelper.GetPosalterationExternalId(alterationitem.AlterationId);
                    posalterationitem.ExternalPosalterationoptionId = PosalterationoptionHelper.GetPosalterationoptionExternalId(alterationitem.AlterationoptionId);

                    posalterationitems.Add(posalterationitem);
                }

                item.Posalterationitems = posalterationitems.ToArray();
            }

            return item;
        }
    }
}
