﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using Dionysos.Web;
using System.IO;
using System.Web.Caching;
using Dionysos;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Web.Caching;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// CacheHelper class
    /// </summary>
    public class CacheHelper
    {
        public static void SetCachingStore(ICachingStore store)
        {
            Dionysos.Web.CacheHelper.SetCachingStore(store);
        }

        /// <summary>
        /// Caches the entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        public static void CacheEntity(IEntity entity)
        {
            if (HttpContext.Current == null)
            {
                // Context is null, do nothing
            }
            else if (entity == null)
            {
                // Entity to cache is null
            }
            else if (entity.PrimaryKeyFields.Count == 0)
            {
                // Entity does not have a primary key field
            }
            else if (entity.PrimaryKeyFields.Count > 1)
            {
                // Entity has multiple primary key fields
            }
            else
            {
                object pkValue = entity.Fields[entity.PrimaryKeyFields[0].Name].CurrentValue;
                if (pkValue != null)
                {
                    string cacheKey = GetCacheKeyForEntity(entity.GetType(), (int)pkValue);
                    if (!cacheKey.IsNullOrWhiteSpace())
                    {
                        Dionysos.Web.CacheHelper.Add(false, cacheKey, entity, null, Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                    }
                }
            }
        }

        /// <summary>
        /// Tries the get entity.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pkValue">The pk value.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static bool TryGetEntity<T>(int pkValue, out T value)
        {
            bool success = false;

            value = default(T);

            // Get the cache key
            string cacheKey = GetCacheKeyForEntity(typeof(T), pkValue);

            if (HttpContext.Current == null)
            {
                // Http context not available
            }
            else if (HttpContext.Current.Cache == null)
            {
                // Cache not available
            }
            else
            {
                success = Dionysos.Web.CacheHelper.TryGetValue(cacheKey, false, out value);
            }

            return success;
        }

        /// <summary>
        /// Gets the cache key for entity.
        /// </summary>
        /// <param name="entityType">Type of the entity.</param>
        /// <param name="pkValue">The pk value.</param>
        /// <returns></returns>
        private static string GetCacheKeyForEntity(Type entityType, int pkValue)
        {
             return string.Format("{0}-{1}", entityType, pkValue);
        }

        private static void RemoveCachedFiles(CacheParameters cacheParams)
        {
            string cacheKeyWildcard = cacheParams.GetCacheKeyWildcard();

            Dionysos.Web.CacheHelper.RemoveWildcard(cacheKeyWildcard);

            // Remove the old cache file
            string[] files = Directory.GetFiles(CacheHelper.TempFilePath, string.Format("{0}.cache", cacheKeyWildcard), SearchOption.TopDirectoryOnly);
            foreach (string file in files)
            {
                try { File.Delete(Path.Combine(CacheHelper.TempFilePath, file)); }
                catch { }
            }
        }

        /// <summary>
        /// Caches the object.
        /// </summary>
        /// <param name="cacheKeyBase">The cache key base.</param>
        /// <param name="cacheKeyVersion">The cache key version.</param>
        /// <param name="obj">The obj.</param>
        /// <param name="cacheInMemory">if set to <c>true</c> [cache in memory].</param>
        public static void CacheObject<T>(CacheParameters cacheParams, T obj, bool cacheInMemory = true)
        {
            // Create the cache key
            if (obj != null && HttpContext.Current != null)
            {
                CacheHelper.RemoveCachedFiles(cacheParams);

                string cacheKey = cacheParams.GetCacheKey();
                string filePath = Path.Combine(CacheHelper.TempFilePath, string.Format("{0}.cache", cacheKey));

                try
                {
                    File.WriteAllText(filePath, XmlHelper.Serialize(obj));
                }
                catch
                {
                    // ignored
                }

                // Add the product menu items to the cache
                if (cacheInMemory)
                {
                    Dionysos.Web.CacheHelper.Add(false, cacheKey, obj, new CacheDependency(filePath));
                }
            }
        }

        /// <summary>
        /// Remove a cached object from cache
        /// </summary>
        /// <param name="cacheKey">Key of the cached object to be removed</param>
        public static void RemoveCacheObject(string cacheKey)
        {
            if (HttpContext.Current != null)
            {
                try
                {
                    string filePath = Path.Combine(CacheHelper.TempFilePath, string.Format("{0}.cache", cacheKey));
                    File.Delete(filePath);
                }
                catch
                {
                    // ignored
                }

                Dionysos.Web.CacheHelper.Remove(false, cacheKey);
            }
        }

        /// <summary>
        /// Remove cached objects for company 
        /// </summary>
        /// <param name="companyId"></param>
        public static void RemoveCacheObjectsForCompany(int companyId)
        {
            List<string> cacheKeys = Dionysos.Web.CacheHelper.GetAllCacheKeys();
            foreach (string cacheKey in cacheKeys)
            {
                Match m = Regex.Match(cacheKey, @"(\d[^-]*)");
                if (m.Success && m.Value.Equals(companyId.ToString(CultureInfo.InvariantCulture)))
                {
                    RemoveCacheObject(cacheKey);
                }
            }

            // Remove old cache files
            string[] files = Directory.GetFiles(CacheHelper.TempFilePath);
            foreach (string file in files)
            {
                try
                {
                    Match m = Regex.Match(Path.GetFileName(file), @"(\d[^-]*)");
                    if (m.Success && m.Value.Equals(companyId.ToString(CultureInfo.InvariantCulture)))
                    {
                        File.Delete(Path.Combine(CacheHelper.TempFilePath, file));
                    }
                }
                catch
                {
                    // ignored
                }
            }
        }

        /// <summary>
        /// Tries the get value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKeyBase">The cache key base.</param>
        /// <param name="cacheKeyVersion">The cache key version.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static bool TryGetValue<T>(CacheParameters cacheParams, out T value, bool isOtoucho = false)
        {
            bool success = false;

            value = default(T);

            if (HttpContext.Current != null)
            {
                string cacheKey = cacheParams.GetCacheKey();

                // Check whether the cache is alive
                if (HttpContext.Current.Cache != null && Dionysos.Web.CacheHelper.TryGetValue(cacheKey, false, out value))
                {
                    success = true;
                }

                string filePath = Path.Combine(TempFilePath, string.Format("{0}.cache", cacheKey));
                if (!success)
                {
                    if (File.Exists(filePath))
                    {
                        try
                        {
                            value = XmlHelper.Deserialize<T>(File.ReadAllText(filePath), isOtoucho);
                            success = true;

                            Dionysos.Web.CacheHelper.Add(false, cacheKey, value, new CacheDependency(filePath));
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex.Message);
                        }
                    }
                }
                else
                {
                    if (!File.Exists(filePath))
                    {
                        try
                        {
                            File.WriteAllText(filePath, XmlHelper.Serialize(value));
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex.Message);
                        }
                    }
                }
            }

            return success;
        }

        /// <summary>
        /// Gets the temp file path.
        /// </summary>
        public static string TempFilePath
        {
            get
            {
	            String mappath = WebShortcuts.MapPath(ConfigurationManager.GetString(DionysosConfigurationConstants.TempFileFolder));
				if (!Directory.Exists(mappath))
				{
					Directory.CreateDirectory(WebShortcuts.MapPath(ConfigurationManager.GetString(DionysosConfigurationConstants.TempFileFolder)));
				}

                return mappath;
            }
        }

        public class CacheParameters
        {
            public CacheParameters(string prefix, string timestamp, string version)
            {
                this.Prefix = prefix;
                this.CacheParams = new List<object>();
                this.Timestamp = timestamp;
                this.Version = version;
            }

            public string Prefix { get; set; }

            public List<object> CacheParams { get; set; }

            public string Timestamp { get; set; }

            public string Version { get; set; }

            public void Add(object cacheParam)
            {
                this.CacheParams.Add(cacheParam);
            }

            public string GetCacheKey()
            {
                // Get the cache key based on the cache parameters
                string cacheKey = this.Prefix;

                foreach (object cacheParam in this.CacheParams)
                {
                    cacheKey += string.Format("-{0}", cacheParam);
                }

                cacheKey += string.Format("-{0}", this.Timestamp);
                cacheKey += string.Format("-{0}", this.Version);

                return cacheKey;
            }

            public string GetCacheKeyWildcard()
            {
                // Get the cache key based on the cache parameters
                string cacheKey = this.Prefix;

                foreach (object cacheParam in this.CacheParams)
                {
                    cacheKey += string.Format("-{0}", cacheParam);
                }

                cacheKey += string.Format("-*-{0}", this.Version);

                return cacheKey;
            }

            public override string ToString()
            {
                return this.GetCacheKey();
            }
        }
    }
}
