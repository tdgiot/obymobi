﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Obymobi.Data;

namespace Obymobi.Logic.HelperClasses
{
    public class DeviceHelper
    {
        public static DeviceEntity GetDeviceEntityByIdentifier(string identifier, bool createWhenNotFound)
        {
            return DeviceHelper.GetDeviceEntityByIdentifier(identifier, createWhenNotFound, null, DeviceType.Unknown);
        }

        public static DeviceEntity GetDeviceEntityByIdentifier(string identifier, bool createWhenNotFound, PrefetchPath prefetch, DeviceType deviceType, ITransaction transaction = null)
        {
            DeviceEntity deviceEntity = null;

            var filter = new PredicateExpression();
            filter.Add(DeviceFields.Identifier == identifier);

            var deviceCollection = new DeviceCollection();
            deviceCollection.AddToTransaction(transaction);
            deviceCollection.GetMulti(filter, prefetch);

            if (deviceCollection.Count == 1)
            {
                deviceEntity = deviceCollection[0];
                deviceEntity.AddToTransaction(transaction);
            }
            else if (deviceCollection.Count == 0 && createWhenNotFound)
            {
                deviceEntity = new DeviceEntity();
                deviceEntity.AddToTransaction(transaction);
                deviceEntity.Identifier = identifier;
                deviceEntity.Type = deviceType;
                deviceEntity.Save();
            }
            else if (deviceCollection.Count > 1)
            {
                throw new ObymobiException(GetDeviceResult.MultipleDevicesFoundForIdentifier, "Multiple devices found for identifier '{0}'", identifier);
            }

            return deviceEntity;
        }

        public static DeviceEntity GetDeviceEntityByTerminalId(int terminalId)
        {
            IncludeFieldsList includes = new IncludeFieldsList();
            includes.Add(DeviceFields.Identifier);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.TerminalId == terminalId);

            RelationCollection relations = new RelationCollection();
            relations.Add(DeviceEntity.Relations.TerminalEntityUsingDeviceId);

            DeviceCollection devices = new DeviceCollection();
            devices.GetMulti(filter, 0, null, relations, null, includes, 0, 0);

            return devices.Count > 0 ? devices[0] : null;
        }

        public static DeviceEntity GetDeviceEntityByClientId(int clientId)
        {
            IncludeFieldsList includes = new IncludeFieldsList();
            includes.Add(DeviceFields.Identifier);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.ClientId == clientId);

            RelationCollection relations = new RelationCollection();
            relations.Add(DeviceEntity.Relations.ClientEntityUsingDeviceId);

            DeviceCollection devices = new DeviceCollection();
            devices.GetMulti(filter, 0, null, relations, null, includes, 0, 0);

            return devices.Count > 0 ? devices[0] : null;
        }
    }
}
