﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.HelperClasses
{
	public class MenuHelper
	{
		public enum MenuHelperErrors : int
		{
			/// <summary>
			/// Company does not exists
			/// </summary>
			CompanyDoesNotExist = 200,
			/// <summary>
			/// Deliverypointgroup does not exists
			/// </summary>
			DeliverypointgroupDoesNotExist = 300,
			/// <summary>
			/// Deliverypointgroup doesn't have a menu
			/// </summary>
			DeliverypointgroupHasNoMenu = 301
		}

	    public static Dictionary<int, int[]> GetMenuIdWithAttachedDeliverypointgroupIds(int companyId)
	    {
            PredicateExpression filter = new PredicateExpression(MenuFields.CompanyId == companyId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.MenuEntity);
            prefetch.Add(MenuEntityBase.PrefetchPathDeliverypointgroupCollection);

            MenuCollection menus = new MenuCollection();
            menus.GetMulti(filter, prefetch);

            return menus.ToDictionary(x => x.MenuId, x => x.DeliverypointgroupCollection.Select(y => y.DeliverypointgroupId).ToArray());
	    }
	}
}
