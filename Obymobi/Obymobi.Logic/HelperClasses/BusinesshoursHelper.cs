﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Logic.HelperClasses
{
    public class BusinesshoursHelper
    {
        public static bool IsTimeInCompanyBusinesshours(int companyId, out CompanyEntity company, DateTime time)
        {
            bool toReturn = false;

            string DayOfWeekAndTime = ((int)time.DayOfWeek).ToString() + time.ToString("HHmm");

            PrefetchPath pathBusinessHours = new PrefetchPath(EntityType.CompanyEntity);

            SortExpression sortBusinessHours = new SortExpression(BusinesshoursFields.DayOfWeekAndTime | SortOperator.Ascending);

            PredicateExpression filterBusinessHours = new PredicateExpression(BusinesshoursFields.DayOfWeekAndTime > DayOfWeekAndTime);

            pathBusinessHours.Add(CompanyEntity.PrefetchPathBusinesshoursCollection, 1, filterBusinessHours, null, sortBusinessHours);

            PredicateExpression filterBusinesshourExceptions = new PredicateExpression();
            filterBusinesshourExceptions.Add(BusinesshoursexceptionFields.FromDateTime <= time);
            filterBusinesshourExceptions.Add(BusinesshoursexceptionFields.TillDateTime >= time);
            pathBusinessHours.Add(CompanyEntity.PrefetchPathBusinesshoursexceptionCollection, 0, filterBusinesshourExceptions);

            company = new CompanyEntity(companyId, pathBusinessHours);

            if (company.BusinesshoursexceptionCollection.Count > 1)
            {
                throw new ObymobiException(BusinesshourCheckResult.OverlappingBusinesshourexceptions, "Error at: {0} for company {1}", time, companyId);
            }
            else if (company.BusinesshoursexceptionCollection.Count == 1)
            {
                // Exception
                toReturn = company.BusinesshoursexceptionCollection[0].Opened;
            }
            else
            {
                // Check the regular schedule
                if (company.BusinesshoursCollection.Count == 0)
                {
                    // This happens when the next closing or opening moment is on sunday and it's still saturday
                    // We now need to retrieve the next moment from 00000 (Day 0, 00:00).
                    BusinesshoursCollection businessHours = new BusinesshoursCollection();
                    filterBusinessHours = new PredicateExpression();
                    filterBusinessHours.Add(BusinesshoursFields.DayOfWeekAndTime >= "00000");
                    filterBusinessHours.Add(BusinesshoursFields.CompanyId == companyId);
                    businessHours.GetMulti(filterBusinessHours, 1, sortBusinessHours);
                    if (businessHours.Count != 1)
                        throw new ObymobiException(BusinesshourCheckResult.NoBusinesshoursAvailable, "Error at: {0} for company {1}", time, companyId);
                    else
                        toReturn = !businessHours[0].Opening;
                }
                else
                {
                    // The current state of being in business is the opposite of the next marker of the Businesshours
                    toReturn = !company.BusinesshoursCollection[0].Opening;
                }
            }
            return toReturn;
        }
    }
}
