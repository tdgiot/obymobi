﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.Model;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// PosproductPosalterationHelper class
    /// </summary>
    public class PosproductPosalterationHelper
    {
        /// <summary>
        /// Cleans up.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="cleanupReport">The cleanup report.</param>
        /// <param name="batchId">The batch id.</param>
        public static void CleanUp(int companyId, ref StringBuilder cleanupReport, out long batchId)
        {
            batchId = -1;
            cleanupReport.AppendLine("*** START: Delete PosproductPosalteration entities ***");
            // Prepate filter for company

            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosproductPosalterationFields.CompanyId == companyId);

            // Create collection and use for Scalar MAX
            PosproductPosalterationCollection posproductposalterations = new PosproductPosalterationCollection();
            object maxBatchId = posproductposalterations.GetScalar(Data.PosproductPosalterationFieldIndex.SynchronisationBatchId, null, AggregateFunction.Max, filter);

            // Only proceed when we have an actual maxBatchId
            if (maxBatchId != DBNull.Value && maxBatchId is long)
            {
                // Retrieve all order posproductposalterations & delete them
                cleanupReport.AppendFormatLine("BatchId: {0}", maxBatchId);
                batchId = (long)maxBatchId;
                // Prepare filter for older posproductposalterations
                filter = new PredicateExpression();
                filter.Add(PosproductPosalterationFields.CompanyId == companyId);
                filter.Add(PosproductPosalterationFields.SynchronisationBatchId < maxBatchId);

                posproductposalterations.GetMulti(filter);

                foreach (var posproductposalteration in posproductposalterations)
                {
                    cleanupReport.AppendFormatLine("ProductExternalId: {0}, AlterationExternalId: {1}",
                        posproductposalteration.PosproductExternalId, posproductposalteration.PosalterationExternalId);
                    posproductposalteration.Delete();
                }

                cleanupReport.AppendFormatLine("{0} entities deleted", posproductposalterations.Count);
            }
            else
                cleanupReport.AppendLine("No valid batch found");

            cleanupReport.AppendLine("*** FINISHED: Delete PosproductPosalteration entities ***");
        }

        /// <summary>
        /// Synchronizes the product alterations from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="posproduct">The posproduct.</param>
        /// <param name="companyId">The id of the company to import the pos products for</param>
        /// <param name="batchId">The batch id.</param>
        public static void SynchronizePosproductPosalterations(Posproduct posproduct, int companyId, long batchId)
        {
            // Get a PosproductPosalterationCollection for the specified company
            PosproductPosalterationCollection posproductPosalterationCollection = GetPosproductPosalterationCollection(companyId);

            // Create an EntityView for the PosproductPosalterationCollection
            EntityView<PosproductPosalterationEntity> posproductPosalterationView = posproductPosalterationCollection.DefaultView;

            // Populate the list of external id's
            var query = from c in posproductPosalterationView select c.ExternalId;
            List<string> posproductPosalterationExternalIdList = query.ToList<string>();

            // Create a filter
            PredicateExpression filter = null;

            // Walk through the PosproductPosalteration instances of the array
            for (int i = 0; i < posproduct.Posalterations.Length; i++)
            {
                Posalteration posalteration = posproduct.Posalterations[i];

                // Initialize the filter
                filter = new PredicateExpression();
                filter.Add(PosproductPosalterationFields.PosproductExternalId == posproduct.ExternalId);
                filter.Add(PosproductPosalterationFields.PosalterationExternalId == posalteration.ExternalId);
                filter.Add(PosproductPosalterationFields.CompanyId == companyId);

                // Set the filter to the EntityView
                posproductPosalterationView.Filter = filter;

                // Check whether items have been found
                if (posproductPosalterationView.Count == 0)
                {
                    // No pos product found in database, add

                    // Then, create a PosproductPosalterationEntity instance
                    // and save it to the persistent storage
                    PosproductPosalterationEntity posproductPosalterationEntity = new PosproductPosalterationEntity();
                    posproductPosalterationEntity.CompanyId = companyId;
                    posproductPosalterationEntity.PosproductExternalId = posproduct.ExternalId;
                    posproductPosalterationEntity.PosalterationExternalId = posalteration.ExternalId;
                    posproductPosalterationEntity.ExternalId = string.Format("{0}-{1}", posproduct.ExternalId, posalteration.ExternalId);

                    try
                    {
                        posproductPosalterationEntity.CreatedInBatchId = batchId;
                        posproductPosalterationEntity.SynchronisationBatchId = batchId;

                        if (posproductPosalterationEntity.Save())
                        {
                            posproductPosalterationEntity.Refetch();
                            posproductPosalterationView.RelatedCollection.Add(posproductPosalterationEntity);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ObymobiException(GenericResult.EntitySaveException, ex);
                    }
                }
                else if (posproductPosalterationView.Count == 1)
                {
                    // Pos product found in database, update
                    PosproductPosalterationEntity posproductPosalterationEntity = posproductPosalterationView[0];
                    posproductPosalterationEntity.CompanyId = companyId;
                    posproductPosalterationEntity.PosproductExternalId = posproduct.ExternalId;
                    posproductPosalterationEntity.PosalterationExternalId = posalteration.ExternalId;
                    posproductPosalterationEntity.ExternalId = string.Format("{0}-{1}", posproduct.ExternalId, posalteration.ExternalId);

                    try
                    {
                        if (posproductPosalterationEntity.IsDirty)
                            posproductPosalterationEntity.UpdatedInBatchId = batchId;

                        // Make this backwards compatible by filling it anyway.
                        if (!posproductPosalterationEntity.CreatedInBatchId.HasValue)
                            posproductPosalterationEntity.CreatedInBatchId = batchId;

                        posproductPosalterationEntity.SynchronisationBatchId = batchId;

                        posproductPosalterationEntity.Save();
                    }
                    catch (Exception ex)
                    {
                        throw new ObymobiException(GenericResult.EntitySaveException, ex);
                    }
                }
                else if (posproductPosalterationView.Count > 1)
                {
                    // Multiple pos products found in database for the specified external id, error!
                    throw new ObymobiException(ImportPosproductPosalterationsResult.MultiplePosproductPosalterationsFound, "Multiple pos product pos alterations found for the specified pos product id '{0}' and pos alteration id '{1}'.", posproduct.ExternalId, posalteration.ExternalId);
                }
            }

            //// Remove pos products which were not synchronized
            //if (posproductPosalterationExternalIdList.Count > 0)
            //{
            //    // There are some external id's which not were saved
            //    // Remove them from the database
            //    foreach (string externalId in posproductPosalterationExternalIdList)
            //    {
            //        PosproductPosalterationEntity posproductPosalterationEntity = GetPosproductPosalterationByExternalId(externalId, posproductPosalterationView);
            //        if (!posproductPosalterationEntity.IsNew)
            //        {
            //            posproductPosalterationEntity.Delete();
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Gets the productalteration by posproductalteration id or a new one if none was found.
        /// </summary>
        /// <param name="posproductalterationId">The posproductalteration id.</param>
        /// <param name="productalterationView">The productalteration view.</param>
        /// <returns>
        /// A (new) <see cref="ProductAlterationEntity"/> instance
        /// </returns>
        public static ProductAlterationEntity GetProductAlterationByPosproductPosalterationId(int posproductalterationId, EntityView<ProductAlterationEntity> productalterationView)
        {
            // Create the ProductAlterationEntity instance
            ProductAlterationEntity productalteration = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductAlterationFields.PosproductPosalterationId == posproductalterationId);

            // Set the filter to the productalteration view
            productalterationView.Filter = filter;

            // Check whether a productalteration was found
            if (productalterationView.Count == 0)
                productalteration = new ProductAlterationEntity();
            else if (productalterationView.Count == 1)
                productalteration = productalterationView[0];
            else if (productalterationView.Count > 1)
                throw new ObymobiException(SynchronizeProductAlterationsResult.MultipleProductAlterationsFound, "Multiple productalterations found for the pos product alteration id '{0}'.", posproductalterationId);

            return productalteration;
        }


        /// <summary>
        /// Gets the posproduct posalterations for posproduct.
        /// </summary>
        /// <param name="posproduct">The posproduct.</param>
        /// <returns></returns>
        public static PosproductPosalterationCollection GetPosproductPosalterationsForPosproduct(PosproductEntity posproduct)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosproductPosalterationFields.PosproductExternalId == posproduct.ExternalId);

            PosproductPosalterationCollection posproductPosalterationCollection = new PosproductPosalterationCollection();
            posproductPosalterationCollection.GetMulti(filter);

            return posproductPosalterationCollection;
        }

        /// <summary>
        /// Gets a PosproductPosalterationCollection for the specified company id
        /// </summary>
        /// <param name="companyId">The id of the company to get the PosproductPosalterationCollection for</param>
        /// <returns>An Obymobi.Data.PosproductCollection instance</returns>
        public static PosproductPosalterationCollection GetPosproductPosalterationCollection(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosproductPosalterationFields.CompanyId == companyId);

            PosproductPosalterationCollection posproductPosalterationCollection = new PosproductPosalterationCollection();
            posproductPosalterationCollection.GetMulti(filter);

            return posproductPosalterationCollection;
        }
    }
}
