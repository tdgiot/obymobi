﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Dionysos.Diagnostics;
using Obymobi.Data;

namespace Obymobi.Logic.HelperClasses
{
    /// <summary>
    /// PosproductHelper class
    /// </summary>
    public class PosproductHelper
    {
        /// <summary>
        /// Cleans up.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="cleanupReport">The cleanup report.</param>
        /// <param name="batchId">The batch id.</param>
        public static void CleanUp(int companyId, ref StringBuilder cleanupReport, out long batchId)
        {
            cleanupReport.AppendLine("*** START: Delete Posproduct entities ***");
            batchId = -1;
            // Prepate filter for company
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosproductFields.CompanyId == companyId);

            // Create collection and use for Scalar MAX
            PosproductCollection posproducts = new PosproductCollection();
            object maxBatchId = posproducts.GetScalar(Data.PosproductFieldIndex.SynchronisationBatchId, null, AggregateFunction.Max, filter);

            // Only proceed when we have an actual maxBatchId
            if (maxBatchId != DBNull.Value && maxBatchId is long)
            {
                // Retrieve all order posproducts & delete them
                cleanupReport.AppendFormatLine("BatchId: {0}", maxBatchId);
                batchId = (long)maxBatchId;
                // Prepare filter for older posproducts
                filter = new PredicateExpression();
                filter.Add(PosproductFields.CompanyId == companyId);
                filter.Add(PosproductFields.SynchronisationBatchId < maxBatchId);

                posproducts.GetMulti(filter);

                // Delete per piece
                foreach (var posproduct in posproducts)
                {
                    cleanupReport.AppendFormatLine("ExternalId: {0}, Name: {1}",
                        posproduct.ExternalId, posproduct.Name);
                    posproduct.Delete();
                }
                cleanupReport.AppendFormatLine("{0} entities deleted", posproducts.Count);
            }

            cleanupReport.AppendLine("*** FINISHED: Delete Posproduct entities ***");
        }

        /// <summary>
        /// Gets the product by posproduct id or a new one if none was found.
        /// </summary>
        /// <param name="posproductId">The posproduct id.</param>
        /// <param name="productView">The product view.</param>
        /// <returns>
        /// A (new) <see cref="ProductEntity"/> instance
        /// </returns>
        public static ProductEntity GetProductByPosproductId(int posproductId, EntityView<ProductEntity> productView)
        {
            // Create the ProductEntity instance
            ProductEntity product = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.PosproductId == posproductId);

            // Set the filter to the product view
            productView.Filter = filter;

            // Check whether a product was found
            if (productView.Count == 0)
                product = new ProductEntity();
            else if (productView.Count == 1)
                product = productView[0];
            else if (productView.Count > 1)
                product = productView[0]; // MB 20120619 Fix for story 30854091
            //throw new ObymobiException(SynchronizeProductsResult.MultipleProductsFound, "Multiple products found for the external id '{0}'.", posproductId);

            return product;
        }

        /// <summary>
        /// Gets the posproduct by external id or a new one if none was found.
        /// </summary>
        /// <param name="externalId">The external id.</param>
        /// <param name="posproductView">The posproduct view.</param>
        /// <returns>
        /// A (new) <see cref="PosproductEntity"/> instance
        /// </returns>
        public static PosproductEntity GetPosproductByExternalId(string externalId, EntityView<PosproductEntity> posproductView)
        {
            // Create the PosproductEntity instance
            PosproductEntity posproduct = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosproductFields.ExternalId == externalId);

            // Set the filter to the POS product view
            posproductView.Filter = filter;

            // Check whether a POS product was found
            if (posproductView.Count == 0)
                posproduct = new PosproductEntity();
            else if (posproductView.Count == 1)
                posproduct = posproductView[0];
            else if (posproductView.Count > 1)
                throw new ObymobiException(SynchronizeProductsResult.MultiplePosproductsFound, "Multiple pos products found for external id '{0}'.", externalId);

            return posproduct;
        }

        /// <summary>
        /// Gets the poscategory by external id or a new one if none was found.
        /// </summary>
        /// <param name="externalId">The external id.</param>
        /// <param name="poscategoryView">The poscategory view.</param>
        /// <returns>
        /// A (new) <see cref="PoscategoryEntity"/> instance
        /// </returns>
        public static PoscategoryEntity GetPoscategoryByExternalId(string externalId, EntityView<PoscategoryEntity> poscategoryView)
        {
            // Create the PoscategoryEntity instance
            PoscategoryEntity poscategory = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PoscategoryFields.ExternalId == externalId);

            // Set the filter to the POS category view
            poscategoryView.Filter = filter;

            // Check whether a POS category was found
            if (poscategoryView.Count == 0)
                poscategory = new PoscategoryEntity();
            else if (poscategoryView.Count == 1)
                poscategory = poscategoryView[0];
            else if (poscategoryView.Count > 1)
                throw new ObymobiException(SynchronizeProductsResult.MultiplePoscategoriesFound, "Multiple pos categories found for external id '{0}'.", externalId);

            return poscategory;
        }

        /// <summary>
        /// Gets the category by poscategory id or a new one if none was found.
        /// </summary>
        /// <param name="poscategoryId">The poscategory id.</param>
        /// <param name="categoryView">The category view.</param>
        /// <returns>
        /// A (new) <see cref="CategoryEntity"/> instance
        /// </returns>
        public static CategoryEntity GetCategoryByPoscategoryId(int poscategoryId, EntityView<CategoryEntity> categoryView)
        {
            // Create the CategoryEntity instance
            CategoryEntity category = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.PoscategoryId == poscategoryId);

            // Set the filter to the category view
            categoryView.Filter = filter;

            // Check whether a category was found
            if (categoryView.Count == 0)
                category = new CategoryEntity();
            else if (categoryView.Count == 1)
                category = categoryView[0];
            else if (categoryView.Count > 1)
                throw new ObymobiException(SynchronizeProductsResult.MultipleCategoriesFound, "Multiple pos categories found for pos category id '{0}'.", poscategoryId);

            return category;
        }

        /// <summary>
        /// Gets the product category by product id and category id
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <param name="categoryId">The category id.</param>
        /// <param name="productCategoryView">The product category view.</param>
        /// <returns>
        /// A (new) <see cref="ProductEntity"/> instance
        /// </returns>
        public static ProductCategoryEntity GetProductCategory(int productId, int categoryId, EntityView<ProductCategoryEntity> productCategoryView)
        {
            // Create the ProductCategoryEntity instance
            ProductCategoryEntity productCategory = null;

            // Create and initialize the filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductCategoryFields.ProductId == productId);
            filter.Add(ProductCategoryFields.CategoryId == categoryId);

            // Set the filter to the product category view
            productCategoryView.Filter = filter;

            // Check whether a product category was found
            if (productCategoryView.Count == 0)
                productCategory = new ProductCategoryEntity();
            else if (productCategoryView.Count == 1)
                productCategory = productCategoryView[0];
            else if (productCategoryView.Count > 1)
                throw new ObymobiException(SynchronizeProductsResult.MultipleProductCategoriesFound, "Multiple product categories found for product id '{0}' and category id '{1}'", productId, categoryId);

            return productCategory;
        }

        /// <summary>
        /// Gets the id of the category using the specified external pos deliverypoint group id
        /// </summary>
        /// <param name="externalPoscategoryId">The external id of the pos category</param>
        /// <returns>A System.Int32 instance containing the category id</returns>
        public static int GetCategoryId(string externalPoscategoryId)
        {
            int categoryId = -1;

            PoscategoryCollection poscategoryCollection = new PoscategoryCollection();
            poscategoryCollection.GetMulti(new PredicateExpression(PoscategoryFields.ExternalId == externalPoscategoryId));

            if (poscategoryCollection.Count == 0)
            {
                // No pos category found, error

            }
            else if (poscategoryCollection.Count == 1)
            {
                PoscategoryEntity poscategoryEntity = poscategoryCollection[0];

                CategoryCollection categoryCollection = new CategoryCollection();
                categoryCollection.GetMulti(new PredicateExpression(CategoryFields.PoscategoryId == poscategoryEntity.PoscategoryId));

                if (categoryCollection.Count == 0)
                {
                    // No category found, error
                }
                else if (categoryCollection.Count == 1)
                {
                    categoryId = categoryCollection[0].CategoryId;
                }
                else if (categoryCollection.Count > 1)
                {
                    // Multiple categories found, error
                }
            }
            else if (poscategoryCollection.Count > 1)
            {
                // Multiple pos categories found, error
            }

            return categoryId;
        }

        /// <summary>
        /// Gets the external id of the pos product for the specified product number
        /// </summary>
        /// <param name="productId">The product id.</param>
        /// <returns>
        /// A System.String instance containing the external id
        /// </returns>
        public static string GetPosproductExternalId(int productId)
        {
            string externalId = string.Empty;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.ProductId == productId);

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductEntity.Relations.PosproductEntityUsingPosproductId);

            PosproductCollection posproductCollection = new PosproductCollection();
            posproductCollection.GetMulti(filter, relations);

            if (posproductCollection.Count == 0)
            {
                // No pos product found, error
                throw new ObymobiException(GetPosProductExternalIdResult.NoPosproductFound, "No pos products found for product id '{0}'.", productId);
            }
            else if (posproductCollection.Count == 1)
            {
                // Pos product found
                externalId = posproductCollection[0].ExternalId;
            }
            else if (posproductCollection.Count > 1)
            {
                // Multiple pos products found, error
                throw new ObymobiException(GetPosProductExternalIdResult.MultiplePosproductsFound, "Multiple pos products found for product id '{0}'.", productId);
            }

            return externalId;
        }

        /// <summary>
        /// Synchronizes the products from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="xml">The Xml string containing the serialized array of Posproduct instances</param>
        /// <param name="companyId">The id of the company to import the pos products for</param>
        /// <param name="batchId">The batch id.</param>
        public static void SynchronizePosproducts(string xml, int companyId, long batchId)
        {
            if (xml.Trim().Length == 0)
                throw new ObymobiException(ImportPosproductsResult.XmlIsEmpty, "Xml string containing serialized Posproduct array is empty.");
            else if (companyId <= 1)
                throw new ObymobiException(ImportPosproductsResult.CompanyIdIsZeroOrLess, "The specified company id '{0}' is smaller than 1.", companyId);
            else
            {
                // Deserialize the xml in order to get the array of Posproduct instances
                Posproduct[] posproducts = XmlHelper.Deserialize<Posproduct[]>(xml);

                SynchronizePosproducts(posproducts, companyId, batchId);
            }
        }

        /// <summary>
        /// Synchronizes the products from the point-of-sale into the Obymobi backend
        /// </summary>
        /// <param name="posproducts">The posproducts.</param>
        /// <param name="companyId">The id of the company to import the pos products for</param>
        /// <param name="batchId">The batch id.</param>
        public static void SynchronizePosproducts(Posproduct[] posproducts, int companyId, long batchId, string revenueCenter = null)
        {
            // Get a PosproductCollection for the specified company
            PosproductCollection posproductCollection = GetPosproductCollection(companyId, true, false);

            // Create an EntityView for the PosproductCollection
            EntityView<PosproductEntity> posproductView = posproductCollection.DefaultView;

            // Populate the list of external id's
            var query = from c in posproductView select c.ExternalId;
            List<string> posproductExternalIdList = query.ToList<string>();

            // Create a filter
            PredicateExpression filter = null;

            // Walk through the Posproduct instances of the array
            for (int i = 0; i < posproducts.Length; i++)
            {
                PosproductEntity posproductEntity = null;
                Posproduct posproduct = posproducts[i];

                // Initialize the filter
                filter = new PredicateExpression();
                filter.Add(PosproductFields.ExternalId == posproduct.ExternalId);
                filter.Add(PosproductFields.CompanyId == companyId);

                if (!string.IsNullOrWhiteSpace(revenueCenter))
                {
                    filter.Add(PosproductFields.RevenueCenter == revenueCenter);
                }

                // Set the filter to the EntityView
                posproductView.Filter = filter;

                // Check whether items have been found
                posproduct.CompanyId = companyId;
                if (posproductView.Count == 0)
                {
                    // No pos product found in database, add
                }
                else if (posproductView.Count == 1)
                {
                    // Pos product found in database, update
                    posproductEntity = posproductView[0];
                }
                else if (posproductView.Count > 1)
                {
                    // Multiple pos products found in database for the specified external id, error!
                    throw new ObymobiException(ImportPosproductsResult.MultiplePosproductsFound, "Multiple pos products found for the specified external id '{0}'.", posproduct.ExternalId);
                }

                // Update field values
                posproductEntity = CreateUpdatePosproductEntityFromModel(posproduct, posproductEntity);

                // Set name to ExternalId if not set.
                if (posproductEntity.Name.IsNullOrWhiteSpace())
                    posproductEntity.Name = posproductEntity.ExternalId;

                try
                {
                    bool wasNew = posproductEntity.IsNew;

                    // Set Created or Updated batch to this batch.
                    if (wasNew)
                        posproductEntity.CreatedInBatchId = batchId;
                    else if (posproductEntity.IsDirty)
                        posproductEntity.UpdatedInBatchId = batchId;

                    // Make this backwards compatible by filling it anyway.
                    if (!posproductEntity.CreatedInBatchId.HasValue)
                        posproductEntity.CreatedInBatchId = batchId;

                    posproductEntity.SynchronisationBatchId = batchId;

                    // Update the price on the related products
                    if (!wasNew)
                    {
                        foreach (var product in posproductEntity.ProductCollection)
                        {
                            product.PriceIn = posproductEntity.PriceIn;

                            if (Dionysos.PathUtil.ContainsInvalidPathChars(product.Name))
                                product.Name = Dionysos.PathUtil.RemoveInvalidPathChars(product.Name);

                            product.Save();
                        }
                    }

                    if (posproductEntity.Save())
                    {
                        if (wasNew)
                        {
                            posproductEntity.Refetch();
                            posproductView.RelatedCollection.Add(posproductEntity);
                        }
                    }
                }
                catch
                {
                    // Do not throw an exception here because it will stop the whole sync batch
                }

                // Check whether the posproduct has alterations
                if (posproduct.Posalterations != null && posproduct.Posalterations.Length > 0)
                {
                    // Synchronize alterations and alteration options
                    PosalterationHelper.SynchronizePosalterations(posproduct.Posalterations, companyId, batchId);

                    // Synchronize the links between the pos products and pos alterations
                    PosproductPosalterationHelper.SynchronizePosproductPosalterations(posproduct, companyId, batchId);
                }
            }

            //// Remove pos products which were not synchronized
            //if (posproductExternalIdList.Count > 0)
            //{
            //    // There are some external id's which not were saved
            //    // Remove them from the database
            //    foreach (string externalId in posproductExternalIdList)
            //    {
            //        PosproductEntity posproductEntity = GetPosproductByExternalId(externalId, posproductView);
            //        if (!posproductEntity.IsNew)
            //        {
            //            posproductEntity.Delete();
            //        }
            //    }
            //}
        }


        /// <summary>
        /// Gets a PosproductCollection for the specified company id
        /// </summary>
        /// <param name="companyId">The id of the company to get the PosproductCollection for</param>
        /// <param name="prefetchProducts">if set to <c>true</c> [prefetch products].</param>
        /// <param name="hideImportedProducts">if set to <c>true</c> [hide imported products].</param>
        /// <returns>
        /// An Obymobi.Data.PosproductCollection instance
        /// </returns>
        public static PosproductCollection GetPosproductCollection(int companyId, bool prefetchProducts, bool hideImportedProducts)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosproductFields.CompanyId == companyId);

            PrefetchPath path = new PrefetchPath(EntityType.PosproductEntity);

            if (prefetchProducts)
            {
                path.Add(PosproductEntity.PrefetchPathProductCollection);
            }

            RelationCollection joins = new RelationCollection();
            if (hideImportedProducts)
            {
                joins.Add(PosproductEntity.Relations.ProductEntityUsingPosproductId, JoinHint.Left);
                filter.Add(ProductFields.ProductId == DBNull.Value);
            }

            PosproductCollection posproductCollection = new PosproductCollection();
            posproductCollection.GetMulti(filter, 0, null, joins, path);

            return posproductCollection;
        }

        /// <summary>
        /// Creates the posproduct model from entity.
        /// </summary>
        /// <param name="posproductEntity">The posproduct entity.</param>
        /// <param name="addNestedModels">if set to <c>true</c> [add nested models].</param>
        /// <returns></returns>
        public static Posproduct CreatePosproductModelFromEntity(PosproductEntity posproductEntity, bool addNestedModels)
        {
            Posproduct posproductModel = new Posproduct();

            posproductModel.PosproductId = posproductEntity.PosproductId;
            posproductModel.CompanyId = posproductEntity.CompanyId;
            posproductModel.ExternalId = posproductEntity.ExternalId;
            posproductModel.ExternalPoscategoryId = posproductEntity.ExternalPoscategoryId;
            posproductModel.Name = posproductEntity.Name;
            posproductModel.PriceIn = posproductEntity.PriceIn;
            posproductModel.VatTariff = posproductEntity.VatTariff;
            posproductModel.SortOrder = 0;
            posproductModel.Description = string.Empty;
            posproductModel.FieldValue1 = posproductEntity.FieldValue1;
            posproductModel.FieldValue2 = posproductEntity.FieldValue2;
            posproductModel.FieldValue3 = posproductEntity.FieldValue3;
            posproductModel.FieldValue4 = posproductEntity.FieldValue4;
            posproductModel.FieldValue5 = posproductEntity.FieldValue5;
            posproductModel.FieldValue6 = posproductEntity.FieldValue6;
            posproductModel.FieldValue7 = posproductEntity.FieldValue7;
            posproductModel.FieldValue8 = posproductEntity.FieldValue8;
            posproductModel.FieldValue9 = posproductEntity.FieldValue9;
            posproductModel.FieldValue10 = posproductEntity.FieldValue10;

            posproductModel.RevenueCenter = posproductEntity.RevenueCenter;

            // Retrieve Posalterations
            if (addNestedModels)
            {
                PosalterationCollection posalterations = PosalterationHelper.GetPosalterationsForPosproduct(posproductEntity);

                List<Posalteration> posalterationModels = new List<Posalteration>();
                foreach (var posalteration in posalterations)
                {
                    posalterationModels.Add(PosalterationHelper.CreatePosalterationModelFromEntity(posalteration, addNestedModels));
                }
            }

            return posproductModel;
        }

        /// <summary>
        /// Creates / Updates a PosproductEntity instance for the specified Posproduct instance
        /// </summary>
        /// <param name="posproduct">The Posproduct instance to create the PosproductEntity instance for</param>
        /// <param name="posproductEntity">The PosproductEntity entity to be updated / created.</param>
        /// <returns>
        /// A new PosproductEntity, or the updated supplied one.
        /// </returns>
        public static PosproductEntity CreateUpdatePosproductEntityFromModel(Posproduct posproduct, PosproductEntity posproductEntity)
        {
            if (posproductEntity == null)
                posproductEntity = new PosproductEntity();

            posproductEntity.CompanyId = posproduct.CompanyId;
            posproductEntity.ExternalId = posproduct.ExternalId;
            posproductEntity.ExternalPoscategoryId = posproduct.ExternalPoscategoryId;
            posproductEntity.Name = posproduct.Name;
            posproductEntity.PriceIn = posproduct.PriceIn;
            posproductEntity.VatTariff = posproduct.VatTariff;

            posproductEntity.FieldValue1 = posproduct.FieldValue1;
            posproductEntity.FieldValue2 = posproduct.FieldValue2;
            posproductEntity.FieldValue3 = posproduct.FieldValue3;
            posproductEntity.FieldValue4 = posproduct.FieldValue4;
            posproductEntity.FieldValue5 = posproduct.FieldValue5;
            posproductEntity.FieldValue6 = posproduct.FieldValue6;
            posproductEntity.FieldValue7 = posproduct.FieldValue7;
            posproductEntity.FieldValue8 = posproduct.FieldValue8;
            posproductEntity.FieldValue9 = posproduct.FieldValue8;
            posproductEntity.FieldValue10 = posproduct.FieldValue10;

            posproductEntity.RevenueCenter = posproduct.RevenueCenter;


            return posproductEntity;
        }


    }
}
