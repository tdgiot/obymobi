﻿using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.HelperClasses
{
    public class MessageTemplateHelper
    {
        public static MessageTemplate CreateMessageTemplateModelFromEntity(MessageTemplateEntity messageTemplateEntity)
        {
            MessageTemplate messageTemplate = new MessageTemplate();
            messageTemplate.MessageTemplateId = messageTemplateEntity.MessageTemplateId;
            messageTemplate.Name = messageTemplateEntity.Name;
            messageTemplate.Title = messageTemplateEntity.Title;
            messageTemplate.Message = messageTemplateEntity.Message;
            messageTemplate.Duration = messageTemplateEntity.Duration;
            messageTemplate.MediaId = messageTemplateEntity.MediaId.HasValue ? messageTemplateEntity.MediaId.Value : 0;
            messageTemplate.CategoryId = messageTemplateEntity.CategoryId.HasValue ? messageTemplateEntity.CategoryId.Value : 0;
            messageTemplate.EntertainmentId = messageTemplateEntity.EntertainmentId.HasValue ? messageTemplateEntity.EntertainmentId.Value : 0;
            messageTemplate.ProductId = messageTemplateEntity.ProductId.HasValue ? messageTemplateEntity.ProductId.Value : 0;
            messageTemplate.MessageLayoutType = (int)messageTemplateEntity.MessageLayoutType;

            return messageTemplate;
        }
    }
}
