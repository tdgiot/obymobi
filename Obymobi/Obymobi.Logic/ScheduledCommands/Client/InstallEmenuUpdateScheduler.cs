﻿using System;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Extensions;
using Obymobi.Logic.Comet;

namespace Obymobi.Logic.ScheduledCommands.Client
{
    public class InstallEmenuUpdateScheduler : ClientCommandSchedulerBase
    {
        private string version = string.Empty;
        private string companyClientApplicationVersion = null;

        /// <summary>
        /// Gets the value indicating the timespan for a scheduled command execution to expire
        /// </summary>
        public override TimeSpan ExpirationTimeSpan => new TimeSpan(0, 5, 0);

        /// <summary>
        /// Gets a value indicating whether the command can be added
        /// </summary>
        public override bool CanCommandBeAdded(ClientEntity clientEntity)
        {
            bool canBeAdded = true;

            if (this.companyClientApplicationVersion == null)
            {
                this.companyClientApplicationVersion = clientEntity.CompanyEntity.ClientApplicationVersion;
            }

            string clientVersion = clientEntity.DeviceEntity.ApplicationVersion;
            if (clientVersion.IsNullOrWhiteSpace())
            {
                // We cannot compare if we have no version
            }
            else if (this.companyClientApplicationVersion.IsNullOrWhiteSpace())
            {
                // We cannot compare if we have no version
            }
            else
            {
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(clientVersion, this.companyClientApplicationVersion);
                if (versionState == VersionNumber.VersionState.Equal || versionState == VersionNumber.VersionState.Newer)
                {
                    canBeAdded = false;
                }
            }

            return canBeAdded;
        }

        /// <summary>
        /// Executes the actual Comet command
        /// </summary>
        protected override void ExecuteCommandFromCometHelper()
        {
            string applicationVersion = GetDeviceFieldValue<string>(DeviceFields.ApplicationVersion);
            DeviceModel? deviceModel = GetDeviceFieldValue<DeviceModel?>(DeviceFields.DeviceModel);

            if (deviceModel.IsT3() && VersionNumber.CompareVersions(applicationVersion, "2022010602") == VersionNumber.VersionState.Older)
            {
                CometHelper.DeviceCommandExecuteClient(this.ClientId, "pm install -r sdcard/Download/CraveEmenu.apk");
            }
            else
            {
                CometHelper.ClientCommand(this.ClientId, ClientCommand.InstallEmenuUpdate, false);
            }
        }

        /// <summary>
        /// Executes the logic which has to be executed after the command is fired
        /// </summary>
        protected override void PostExecuteCommand() => this.version = GetDeviceFieldValue<string>(DeviceFields.ApplicationVersion);

        /// <summary>
        /// Gets a value indicating whether the command has been executed successfully
        /// </summary>
        public override bool HasCommandExecutedSuccessfully()
        {
            bool versionChanged = false;

            // Get the last version for the client
            string lastVersion = GetDeviceFieldValue<string>(DeviceFields.ApplicationVersion);

            if (string.IsNullOrWhiteSpace(lastVersion))
            {
                // This is weird
            }
            else if (string.IsNullOrWhiteSpace(this.version))
            {
                versionChanged = true;
            }
            else if (VersionNumber.CompareVersions(lastVersion, this.version) == VersionNumber.VersionState.Newer)
            {
                versionChanged = true;
            }

            return versionChanged;
        }

        /// <summary>
        /// Gets a value indicating whether the command can be executed
        /// </summary>
        public override bool CanExecuteCommand() => GetDeviceFieldValue<bool>(DeviceFields.UpdateEmenuDownloaded);
    }
}
