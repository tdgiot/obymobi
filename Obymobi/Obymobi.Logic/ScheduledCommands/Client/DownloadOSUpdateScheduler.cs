﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.HelperClasses;
using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Logic.ScheduledCommands.Client
{
    public class DownloadOSUpdateScheduler : ClientCommandSchedulerBase
    {
        #region Fields

        private Dictionary<DeviceModel, ReleaseEntity> companyReleases = new Dictionary<DeviceModel, ReleaseEntity>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets the value indicating the timespan for a scheduled command execution to expire
        /// </summary>
        public override TimeSpan ExpirationTimeSpan
        {
            get { return new TimeSpan(0, 90, 0); } // 90 minutes
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a value indicating whether the command can be added
        /// </summary>
        public override bool CanCommandBeAdded(ClientEntity clientEntity)
        {
            bool canBeAdded = true;
            
            if (clientEntity.DeviceEntity == null || clientEntity.DeviceEntity.IsNew)
            {
                canBeAdded = false;
            }
            else if (clientEntity.DeviceEntity != null)
            {
                string releaseVersion = string.Empty;

                DeviceModel deviceModel = clientEntity.DeviceEntity.DeviceModel.GetValueOrDefault(DeviceModel.Unknown);
                if (deviceModel != DeviceModel.Unknown)
                {
                    ReleaseEntity releaseEntity = null;
                    if (!this.companyReleases.TryGetValue(deviceModel, out releaseEntity))
                    {
                        releaseEntity = ReleaseHelper.GetCompanyRelease(deviceModel, clientEntity.CompanyId);
                        if (releaseEntity != null && releaseEntity.IntermediateReleaseId.HasValue)
                        {
                            // Get linked intermediate release entity                                                        
                            releaseEntity = releaseEntity.IntermedateReleaseEntity;                            
                        }                        
                        this.companyReleases.Add(deviceModel, releaseEntity);
                    }

                    if (releaseEntity != null)
                    {
                        releaseVersion = releaseEntity.Version;
                    }
                    else
                    {
                        canBeAdded = false;
                    }
                }

                string clientVersion = clientEntity.DeviceEntity.OsVersion;

                if (!clientVersion.IsNullOrWhiteSpace() && !releaseVersion.IsNullOrWhiteSpace())
                {
                    VersionNumber.VersionState versionState = VersionNumber.CompareVersions(clientVersion, releaseVersion);
                    if (versionState == VersionNumber.VersionState.Equal || versionState == VersionNumber.VersionState.Newer)
                    {
                        canBeAdded = false;
                    }
                }
            }            

            return canBeAdded;
        }

        /// <summary>
        /// Executes the actual Comet command
        /// </summary>
        protected override void ExecuteCommandFromCometHelper()
        {
            CometHelper.ClientCommand(this.ClientId, ClientCommand.DownloadOSUpdate, false);
        }

        /// <summary>
        /// Executes the logic which has to be executed after the command is fired
        /// </summary>
        protected override void PostExecuteCommand()
        {
            // Set the DownloadEmenuUpdate flag to false
            this.ScheduledCommand.ClientEntity.DeviceEntity.UpdateOSDownloaded = false;
            this.ScheduledCommand.ClientEntity.DeviceEntity.Save();
        }

        /// <summary>
        /// Gets a value indicating whether the command has been executed successfully
        /// </summary>
        public override bool HasCommandExecutedSuccessfully()
        {
            return this.GetDeviceFieldValue<bool>(DeviceFields.UpdateOSDownloaded);
        }

        /// <summary>
        /// Gets a value indicating whether the command can be executed
        /// </summary>
        public override bool CanExecuteCommand()
        {
            return true;
        }

        #endregion
    }
}
