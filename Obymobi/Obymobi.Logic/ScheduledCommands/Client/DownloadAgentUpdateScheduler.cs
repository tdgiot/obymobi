﻿using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Logic.ScheduledCommands.Client
{
    public class DownloadAgentUpdateScheduler : ClientCommandSchedulerBase
    {
        #region Fields

        private string releaseVersion = null;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the value indicating the timespan for a scheduled command execution to expire
        /// </summary>
        public override TimeSpan ExpirationTimeSpan
        {
            get { return new TimeSpan(0, 15, 0); } // 15 minutes
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes the actual Comet command
        /// </summary>
        protected override void ExecuteCommandFromCometHelper()
        {
            CometHelper.ClientCommand(this.ClientId, ClientCommand.DownloadAgentUpdate, false);
        }

        /// <summary>
        /// Executes the logic which has to be executed after the command is fired
        /// </summary>
        protected override void PostExecuteCommand()
        {
            // Set the DownloadEmenuUpdate flag to false
            this.ScheduledCommand.ClientEntity.DeviceEntity.UpdateAgentDownloaded = false;
            this.ScheduledCommand.ClientEntity.DeviceEntity.Save();
        }

        /// <summary>
        /// Gets a value indicating whether the command can be added
        /// </summary>
        public override bool CanCommandBeAdded(ClientEntity clientEntity)
        {
            bool canBeAdded = true;

            if (this.releaseVersion == null)
            {
                // Get the release version from the releases
                ReleaseEntity agentCompanyRelease = ReleaseHelper.GetCompanyRelease(ApplicationCode.Agent, clientEntity.CompanyId);
                ReleaseEntity agentStableReleaseEntity = ReleaseHelper.GetStableReleaseByCode(ApplicationCode.Agent);

                if (agentStableReleaseEntity != null)
                    this.releaseVersion = (agentCompanyRelease != null ? agentCompanyRelease.Version : agentStableReleaseEntity.Version);
                else
                    this.releaseVersion = string.Empty;
            }

            string clientVersion = clientEntity.DeviceEntity.AgentVersion;

            if (clientVersion.IsNullOrWhiteSpace())
            {
                // We cannot compare if we have no version
            }
            else if (this.releaseVersion.IsNullOrWhiteSpace())
            {
                // We cannot compare if we have no version
            }
            else
            {
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(clientVersion, this.releaseVersion);
                if (versionState == VersionNumber.VersionState.Equal || versionState == VersionNumber.VersionState.Newer)
                {
                    canBeAdded = false;
                }
            }

            return canBeAdded;
        }

        /// <summary>
        /// Gets a value indicating whether the command has been executed successfully
        /// </summary>
        public override bool HasCommandExecutedSuccessfully()
        {
            return this.GetDeviceFieldValue<bool>(DeviceFields.UpdateAgentDownloaded);
        }

        /// <summary>
        /// Gets a value indicating whether the command can be executed
        /// </summary>
        public override bool CanExecuteCommand()
        {
            return true;
        }

        #endregion
    }
}
