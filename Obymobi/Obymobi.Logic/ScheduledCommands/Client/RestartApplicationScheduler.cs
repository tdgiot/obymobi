﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.ScheduledCommands.Client
{
    public class RestartApplicationScheduler : ClientCommandSchedulerBase
    {
        #region Fields

        private DateTime? dateTimeOnStart = null;

        #endregion 

        #region Properties

        /// <summary>
        /// Gets the value indicating the timespan for a scheduled command execution to expire
        /// </summary>
        public override TimeSpan ExpirationTimeSpan
        {
            get { return new TimeSpan(0, 5, 0); } // 5 minutes
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a value indicating whether the command can be added
        /// </summary>
        public override bool CanCommandBeAdded(ClientEntity clientEntity)
        {
            return true;
        }

        /// <summary>
        /// Executes the actual Comet command
        /// </summary>
        protected override void ExecuteCommandFromCometHelper()
        {
            CometHelper.ClientCommand(this.ClientId, ClientCommand.RestartApplication, false);
        }

        /// <summary>
        /// Executes the logic which has to be executed after the command is fired
        /// </summary>
        protected override void PostExecuteCommand()
        {
            // Get the last request 
            this.dateTimeOnStart = this.GetDeviceFieldValue<DateTime?>(DeviceFields.LastRequestUTC);
        }

        /// <summary>
        /// Gets a value indicating whether the command has been executed successfully
        /// </summary>
        public override bool HasCommandExecutedSuccessfully()
        {
            bool lastRequestChanged = false;

            // Get the last request value for the client
            ClientEntity clientEntity = this.GetClientEntity(ClientFields.LoadedSuccessfully);
            if (clientEntity != null && clientEntity.DeviceId.HasValue)
            {
                DateTime? lastRequest = clientEntity.DeviceEntity.LastRequestUTC;
                bool loadedSuccessfully = clientEntity.LoadedSuccessfully;

                if (!lastRequest.HasValue)
                {
                    // This is weird
                }
                else if (!this.dateTimeOnStart.HasValue)
                {
                    lastRequestChanged = true;
                }
                else if (lastRequest.Value > this.dateTimeOnStart.Value.AddMinutes(1) && loadedSuccessfully)
                {
                    lastRequestChanged = true;
                }
            }

            return lastRequestChanged;
        }

        /// <summary>
        /// Gets a value indicating whether the command can be executed
        /// </summary>
        public override bool CanExecuteCommand()
        {
            return true;
        }

        #endregion
    }
}
