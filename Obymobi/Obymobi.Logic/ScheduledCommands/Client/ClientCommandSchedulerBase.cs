﻿using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data;

namespace Obymobi.Logic.ScheduledCommands.Client
{
    public abstract class ClientCommandSchedulerBase : CommandSchedulerBase
    {
        #region Properties

        /// <summary>
        /// Gets the client id belonging to the scheduled command
        /// </summary>
        protected int ClientId
        {
            get { return this.ScheduledCommand.ClientId.Value; }
        }

        /// <summary>
        /// Gets the value indicating whether the client is currently online
        /// </summary>
        public override bool IsOnline
        {
            get 
            {
                DateTime? lastRequest = this.GetDeviceFieldValue<DateTime?>(DeviceFields.LastRequestUTC);
                return (lastRequest.HasValue && (lastRequest.Value > DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1)));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a value indicating whether the command can be added
        /// </summary>
        public abstract bool CanCommandBeAdded(ClientEntity clientEntity);

        /// <summary>
        /// Gets a field value from the database for the specified terminal id and field.
        /// </summary>
        /// <param name="field">The entity field to get the value for.</param>
        /// <returns>The value from the database.</returns>
        protected T GetDeviceFieldValue<T>(EntityField field)
        {
            T value = default(T);

            IncludeFieldsList fields = new IncludeFieldsList(field);
            PredicateExpression filter = new PredicateExpression(ClientFields.ClientId == this.ClientId);

            RelationCollection relation = new RelationCollection(DeviceEntityBase.Relations.ClientEntityUsingDeviceId);

            DeviceCollection deviceCollection = new DeviceCollection();
            deviceCollection.GetMulti(filter, 1, null, relation, null, fields, 0, 0);

            if (deviceCollection.Count == 1)
                value = (T)deviceCollection[0].Fields[field.Name].CurrentValue;

            return value;
        }

        /// <summary>
        /// Gets a field value from the database for the specified client id and field.
        /// </summary>
        /// <param name="field">The entity field to get the value for.</param>
        /// <returns>The value from the database.</returns>
        protected T GetClientFieldValue<T>(EntityField field)
        {
            T value = default(T);

            IncludeFieldsList fields = new IncludeFieldsList(field);
            PredicateExpression filter = new PredicateExpression(ClientFields.ClientId == this.ClientId);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, fields, null);

            if (clientCollection.Count == 1)
                value = (T)clientCollection[0].Fields[field.Name].CurrentValue;

            return value;
        }

        protected ClientEntity GetClientEntity(params EntityField[] includedFields)
        {
            IncludeFieldsList fields = new IncludeFieldsList();
            fields.AddRange(includedFields);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntityBase.PrefetchPathDeviceEntity, new IncludeFieldsList {DeviceFields.LastRequestUTC, DeviceFields.LastSupportToolsRequestUTC});

            PredicateExpression filter = new PredicateExpression(ClientFields.ClientId == this.ClientId);

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, fields, prefetch);

            if (clientCollection.Count == 1)
            {
                return clientCollection[0];
            }

            return null;
        }

        #endregion
    }
}
