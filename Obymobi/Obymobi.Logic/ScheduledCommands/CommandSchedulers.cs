﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.ScheduledCommands.Client;
using Obymobi.Logic.ScheduledCommands.Terminal;

namespace Obymobi.Logic.ScheduledCommands
{
    public class CommandSchedulers
    {
        /// <summary>
        /// Gets the client command scheduler which belongs to the specified scheduled command.
        /// </summary>
        /// <param name="scheduledCommandEntity">The scheduled command entity</param>
        /// <returns>The command scheduler instance</returns>
        public static ClientCommandSchedulerBase GetClientCommandScheduler(ScheduledCommandEntity scheduledCommandEntity)
        {
            ClientCommandSchedulerBase commandScheduler = GetClientCommandScheduler(scheduledCommandEntity.ClientCommand.Value);

            // Set the scheduled command
            if (commandScheduler != null) commandScheduler.ScheduledCommand = scheduledCommandEntity;

            return commandScheduler;
        }

        /// <summary>
        /// Gets new client command scheduler by ClientCommand type
        /// </summary>
        /// <param name="command"></param>
        /// <returns>New ClientCommandSchedulerBase instance</returns>
        public static ClientCommandSchedulerBase GetClientCommandScheduler(ClientCommand command)
        {
            ClientCommandSchedulerBase commandScheduler = null;

            switch (command)
            {
                case ClientCommand.RestartApplication:
                    commandScheduler = new Client.RestartApplicationScheduler();
                    break;
                case ClientCommand.RestartDevice:
                    commandScheduler = new Client.RestartDeviceScheduler();
                    break;
                case ClientCommand.SendLogToWebservice:
                    commandScheduler = new Client.SendLogToWebserviceScheduler();
                    break;
                case ClientCommand.DailyOrderReset:
                    commandScheduler = new Client.DailyOrderResetScheduler();
                    break;
                case ClientCommand.DownloadEmenuUpdate:
                    commandScheduler = new Client.DownloadEmenuUpdateScheduler();
                    break;
                case ClientCommand.DownloadAgentUpdate:
                    commandScheduler = new Client.DownloadAgentUpdateScheduler();
                    break;
                case ClientCommand.DownloadSupportToolsUpdate:
                    commandScheduler = new Client.DownloadSupportToolsUpdateScheduler();
                    break;
                case ClientCommand.InstallEmenuUpdate:
                    commandScheduler = new Client.InstallEmenuUpdateScheduler();
                    break;
                case ClientCommand.InstallAgentUpdate:
                    commandScheduler = new Client.InstallAgentUpdateScheduler();
                    break;
                case ClientCommand.InstallSupportToolsUpdate:
                    commandScheduler = new Client.InstallSupportToolsUpdateScheduler();
                    break;
                case ClientCommand.ClearBrowserCache:
                    commandScheduler = new Client.ClearBrowserCacheScheduler();
                    break;
                case ClientCommand.DownloadOSUpdate:
                    commandScheduler = new Client.DownloadOSUpdateScheduler();
                    break;
                case ClientCommand.InstallOSUpdate:
                    commandScheduler = new Client.InstallOSUpdateScheduler();
                    break;
                case ClientCommand.DownloadInstallMessagingServiceUpdate:
                    commandScheduler = new Client.DownloadInstallMessagingServiceUpdate();
                    break;
            }

            return commandScheduler;
        }

        /// <summary>
        /// Gets the terminal command scheduler which belongs to the specified scheduled command.
        /// </summary>
        /// <param name="scheduledCommandEntity">The scheduled command entity</param>
        /// <returns>The command scheduler instance</returns>
        public static TerminalCommandSchedulerBase GetTerminalCommandScheduler(ScheduledCommandEntity scheduledCommandEntity)
        {
            TerminalCommandSchedulerBase commandScheduler = GetTerminalCommandScheduler(scheduledCommandEntity.TerminalCommand.Value);

            // Set the scheduled command
            if (commandScheduler != null) commandScheduler.ScheduledCommand = scheduledCommandEntity;

            return commandScheduler;
        }

        /// <summary>
        /// Gets the terminal command scheduler which belongs to the specified scheduled command.
        /// </summary>
        /// <param name="command">The command</param>
        /// <returns>The command scheduler instance</returns>
        public static TerminalCommandSchedulerBase GetTerminalCommandScheduler(TerminalCommand command)
        {
            TerminalCommandSchedulerBase commandScheduler = null;

            switch (command)
            {
                case TerminalCommand.RestartApplication:
                    commandScheduler = new Terminal.RestartApplicationScheduler();
                    break;
                case TerminalCommand.RestartDevice:
                    commandScheduler = new Terminal.RestartDeviceScheduler();
                    break;
                case TerminalCommand.SendLogToWebservice:
                    commandScheduler = new Terminal.SendLogToWebserviceScheduler();
                    break;
                case TerminalCommand.DownloadConsoleUpdate:
                    commandScheduler = new Terminal.DownloadConsoleUpdateScheduler();
                    break;
                case TerminalCommand.DownloadAgentUpdate:
                    commandScheduler = new Terminal.DownloadAgentUpdateScheduler();
                    break;
                case TerminalCommand.DownloadSupportToolsUpdate:
                    commandScheduler = new Terminal.DownloadSupportToolsUpdateScheduler();
                    break;
                case TerminalCommand.InstallConsoleUpdate:
                    commandScheduler = new Terminal.InstallConsoleUpdateScheduler();
                    break;
                case TerminalCommand.InstallAgentUpdate:
                    commandScheduler = new Terminal.InstallAgentUpdateScheduler();
                    break;
                case TerminalCommand.InstallSupportToolsUpdate:
                    commandScheduler = new Terminal.InstallSupportToolsUpdateScheduler();
                    break;
                case TerminalCommand.DownloadOSUpdate:
                    commandScheduler = new Terminal.DownloadOSUpdateScheduler();
                    break;
                case TerminalCommand.InstallOSUpdate:
                    commandScheduler = new Terminal.InstallOSUpdateScheduler();
                    break;
                case TerminalCommand.DownloadInstallMessagingServiceUpdate:
                    commandScheduler = new Terminal.DownloadInstallMessagingServiceUpdateScheduler();
                    break;
            }

            return commandScheduler;
        }
    }
}
