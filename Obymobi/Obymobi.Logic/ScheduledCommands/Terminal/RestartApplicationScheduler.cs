﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.ScheduledCommands.Terminal
{
    public class RestartApplicationScheduler : TerminalCommandSchedulerBase
    {
        #region Fields

        private DateTime? dateTimeOnStart = null;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the value indicating the timespan for a scheduled command execution to expire
        /// </summary>
        public override TimeSpan ExpirationTimeSpan
        {
            get { return new TimeSpan(0, 5, 0); } // 5 minutes
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a value indicating whether the command can be added
        /// </summary>
        public override bool CanCommandBeAdded(TerminalEntity terminalEntity)
        {
            return true;
        }

        /// <summary>
        /// Executes the actual Comet command
        /// </summary>
        protected override void ExecuteCommandFromCometHelper()
        {
            CometHelper.TerminalCommand(this.TerminalId, TerminalCommand.RestartApplication, false);
        }

        /// <summary>
        /// Executes the logic which has to be executed after the command is fired
        /// </summary>
        protected override void PostExecuteCommand()
        {
            // Get the last request 
            this.dateTimeOnStart = this.GetDeviceFieldValue<DateTime?>(DeviceFields.LastRequestUTC);
        }

        /// <summary>
        /// Gets a value indicating whether the command has been executed successfully
        /// </summary>
        public override bool HasCommandExecutedSuccessfully()
        {
            bool lastRequestChanged = false;

            // Get the last request value for the Terminal
            TerminalEntity terminalEntity = this.GetTerminalEntity(TerminalFields.LoadedSuccessfully);
            DateTime? lastRequest = terminalEntity.DeviceEntity.LastRequestUTC;
            bool loadedSuccessfully = terminalEntity.LoadedSuccessfully;

            if (!lastRequest.HasValue)
            {
                // This is weird
            }
            else if (!this.dateTimeOnStart.HasValue)
            {
                lastRequestChanged = true;
            }
            else if (lastRequest.Value > this.dateTimeOnStart.Value.AddMinutes(1) && loadedSuccessfully)
            {
                lastRequestChanged = true;
            }

            return lastRequestChanged;
        }

        /// <summary>
        /// Gets a value indicating whether the command can be executed
        /// </summary>
        public override bool CanExecuteCommand()
        {
            return true;
        }

        #endregion
    }
}
