﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Obymobi.Constants;

namespace Obymobi.Logic.ScheduledCommands.Terminal
{
    public class InstallSupportToolsUpdateScheduler : TerminalCommandSchedulerBase
    {
        #region Fields

        private string version = string.Empty;
        private string releaseVersion = null;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the value indicating the timespan for a scheduled command execution to expire
        /// </summary>
        public override TimeSpan ExpirationTimeSpan
        {
            get { return new TimeSpan(0, 5, 0); } // 5 minutes
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a value indicating whether the command can be added
        /// </summary>
        public override bool CanCommandBeAdded(TerminalEntity terminalEntity)
        {
            bool canBeAdded = true;

            if (this.releaseVersion == null)
            {
                // Get the release version from the releases
                ReleaseEntity supportToolsCompanyRelease = ReleaseHelper.GetCompanyRelease(ApplicationCode.SupportTools, terminalEntity.CompanyId);
                ReleaseEntity supportToolsStableReleaseEntity = ReleaseHelper.GetStableReleaseByCode(ApplicationCode.SupportTools);

                if (supportToolsStableReleaseEntity != null)
                    this.releaseVersion = (supportToolsCompanyRelease != null ? supportToolsCompanyRelease.Version : supportToolsStableReleaseEntity.Version);
                else
                    this.releaseVersion = string.Empty;
            }

            string terminalVersion = terminalEntity.DeviceEntity.SupportToolsVersion;

            if (terminalVersion.IsNullOrWhiteSpace())
            {
                // We cannot compare if we have no version
            }
            else if (this.releaseVersion.IsNullOrWhiteSpace())
            {
                // We cannot compare if we have no version
            }
            else
            {
                VersionNumber.VersionState versionState = VersionNumber.CompareVersions(terminalVersion, this.releaseVersion);
                if (versionState == VersionNumber.VersionState.Equal || versionState == VersionNumber.VersionState.Newer)
                {
                    canBeAdded = false;
                }
            }

            return canBeAdded;
        }

        /// <summary>
        /// Executes the actual Comet command
        /// </summary>
        protected override void ExecuteCommandFromCometHelper()
        {
            CometHelper.TerminalCommand(this.TerminalId, TerminalCommand.InstallSupportToolsUpdate, false);
        }

        /// <summary>
        /// Executes the logic which has to be executed after the command is fired
        /// </summary>
        protected override void PostExecuteCommand()
        {
            this.version = this.GetDeviceFieldValue<string>(DeviceFields.SupportToolsVersion);
        }

        /// <summary>
        /// Gets a value indicating whether the command has been executed successfully
        /// </summary>
        public override bool HasCommandExecutedSuccessfully()
        {
            bool versionChanged = false;

            // Get the last version for the Terminal
            string lastVersion = this.GetDeviceFieldValue<string>(DeviceFields.SupportToolsVersion);

            if (String.IsNullOrWhiteSpace(lastVersion))
            {
                // This is weird
            }
            else if (String.IsNullOrWhiteSpace(this.version))
            {
                versionChanged = true;
            }
            else if (VersionNumber.CompareVersions(lastVersion, this.version) == VersionNumber.VersionState.Newer)
            {
                versionChanged = true;
            }

            return versionChanged;
        }

        /// <summary>
        /// Gets a value indicating whether the command can be executed
        /// </summary>
        public override bool CanExecuteCommand()
        {
            return this.GetDeviceFieldValue<bool>(DeviceFields.UpdateSupportToolsDownloaded);
        }

        #endregion
    }
}
