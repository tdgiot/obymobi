﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Logic.ScheduledCommands.Terminal
{
    public class InstallOSUpdateScheduler : TerminalCommandSchedulerBase
    {
        #region Fields

        private string version = string.Empty;
        private Dictionary<DeviceModel, ReleaseEntity> companyReleases = new Dictionary<DeviceModel, ReleaseEntity>();

        #endregion

        #region Properties

        /// <summary>
        /// Gets the value indicating the timespan for a scheduled command execution to expire
        /// </summary>
        public override TimeSpan ExpirationTimeSpan
        {
            get { return new TimeSpan(0, 15, 0); } // 15 minutes
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a value indicating whether the command can be added
        /// </summary>
        public override bool CanCommandBeAdded(TerminalEntity terminalEntity)
        {
            bool canBeAdded = true;

            if (terminalEntity.DeviceEntity == null || terminalEntity.DeviceEntity.IsNew)
            {
                canBeAdded = false;
            }
            else if (terminalEntity.DeviceEntity != null)
            {
                string releaseVersion = string.Empty;

                DeviceModel deviceModel = terminalEntity.DeviceEntity.DeviceModel.GetValueOrDefault(DeviceModel.Unknown);
                if (deviceModel != DeviceModel.Unknown)
                {
                    ReleaseEntity releaseEntity = null;
                    if (!this.companyReleases.TryGetValue(deviceModel, out releaseEntity))
                    {
                        releaseEntity = ReleaseHelper.GetCompanyRelease(deviceModel, terminalEntity.CompanyId);
                        if (releaseEntity != null && releaseEntity.IntermediateReleaseId.HasValue)
                        {
                            // Get linked intermediate release entity                                                        
                            releaseEntity = releaseEntity.IntermedateReleaseEntity;
                        }
                        this.companyReleases.Add(deviceModel, releaseEntity);
                    }

                    if (releaseEntity != null)
                    {
                        releaseVersion = releaseEntity.Version;
                    }
                    else
                    {
                        canBeAdded = false;
                    }
                }

                string terminalVersion = terminalEntity.DeviceEntity.OsVersion;

                if (!terminalVersion.IsNullOrWhiteSpace() && !releaseVersion.IsNullOrWhiteSpace())
                {
                    VersionNumber.VersionState versionState = VersionNumber.CompareVersions(terminalVersion, releaseVersion);
                    if (versionState == VersionNumber.VersionState.Equal || versionState == VersionNumber.VersionState.Newer)
                    {
                        canBeAdded = false;
                    }
                }
            }

            return canBeAdded;
        }

        /// <summary>
        /// Executes the actual Comet command
        /// </summary>
        protected override void ExecuteCommandFromCometHelper()
        {
            CometHelper.TerminalCommand(this.TerminalId, TerminalCommand.InstallOSUpdate, false);
        }

        /// <summary>
        /// Executes the logic which has to be executed after the command is fired
        /// </summary>
        protected override void PostExecuteCommand()
        {
            this.version = this.GetDeviceFieldValue<string>(DeviceFields.OsVersion);
        }

        /// <summary>
        /// Gets a value indicating whether the command has been executed successfully
        /// </summary>
        public override bool HasCommandExecutedSuccessfully()
        {
            bool versionChanged = false;

            // Get the last version for the Terminal
            string lastVersion = this.GetDeviceFieldValue<string>(DeviceFields.OsVersion);

            if (String.IsNullOrWhiteSpace(lastVersion))
            {
                // This is weird
            }
            else if (String.IsNullOrWhiteSpace(this.version))
            {
                versionChanged = true;
            }
            else if (VersionNumber.CompareVersions(lastVersion, this.version) == VersionNumber.VersionState.Newer)
            {
                versionChanged = true;
            }

            return versionChanged;
        }

        /// <summary>
        /// Gets a value indicating whether the command can be executed
        /// </summary>
        public override bool CanExecuteCommand()
        {
            return this.GetDeviceFieldValue<bool>(DeviceFields.UpdateOSDownloaded);
        }

        #endregion
    }
}
