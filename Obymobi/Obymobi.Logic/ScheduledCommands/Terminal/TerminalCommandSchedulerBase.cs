﻿using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data;

namespace Obymobi.Logic.ScheduledCommands.Terminal
{
    public abstract class TerminalCommandSchedulerBase : CommandSchedulerBase
    {
        #region Properties

        /// <summary>
        /// Gets the terminal id belonging to the scheduled command
        /// </summary>
        protected int TerminalId
        {
            get { return this.ScheduledCommand.TerminalId.Value; }
        }

        /// <summary>
        /// Gets the value indicating whether the terminal is currently online
        /// </summary>
        public override bool IsOnline
        {
            get 
            { 
                DateTime? lastRequest = this.GetDeviceFieldValue<DateTime?>(DeviceFields.LastRequestUTC);
                return (lastRequest.HasValue && (lastRequest.Value > DateTime.UtcNow.AddMilliseconds(ObymobiIntervals.DEVICE_OFFLINE_THRESHOLD * -1)));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a value indicating whether the command can be added
        /// </summary>
        public abstract bool CanCommandBeAdded(TerminalEntity terminalEntity);

        /// <summary>
        /// Gets a field value from the database for the specified terminal id and field.
        /// </summary>
        /// <param name="field">The entity field to get the value for.</param>
        /// <returns>The value from the database.</returns>
        protected T GetDeviceFieldValue<T>(EntityField field)
        {
            T value = default(T);

            IncludeFieldsList fields = new IncludeFieldsList(field);
            PredicateExpression filter = new PredicateExpression(TerminalFields.TerminalId == this.TerminalId);

            RelationCollection relation = new RelationCollection(DeviceEntityBase.Relations.TerminalEntityUsingDeviceId);

            DeviceCollection deviceCollection = new DeviceCollection();
            deviceCollection.GetMulti(filter, 1, null, relation, null, fields, 0, 0);

            if (deviceCollection.Count == 1)
                value = (T)deviceCollection[0].Fields[field.Name].CurrentValue;

            return value;
        }

        /// <summary>
        /// Gets a field value from the database for the specified terminal id and field.
        /// </summary>
        /// <param name="field">The entity field to get the value for.</param>
        /// <returns>The value from the database.</returns>
        protected T GetTerminalFieldValue<T>(EntityField field)
        {
            T value = default(T);

            IncludeFieldsList fields = new IncludeFieldsList(field);
            PredicateExpression filter = new PredicateExpression(TerminalFields.TerminalId == this.TerminalId);

            RelationCollection relation = new RelationCollection(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);

            TerminalCollection terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter, 0, null, relation, null, fields, 0, 0);

            if (terminalCollection.Count == 1)
                value = (T)terminalCollection[0].Fields[field.Name].CurrentValue;

            return value;
        }

        protected TerminalEntity GetTerminalEntity(params EntityField[] includedFields)
        {
            IncludeFieldsList fields = new IncludeFieldsList();
            fields.AddRange(includedFields);

            PredicateExpression filter = new PredicateExpression(TerminalFields.TerminalId == this.TerminalId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity, new IncludeFieldsList {DeviceFields.Identifier, DeviceFields.LastRequestUTC});

            TerminalCollection terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter, fields, prefetch);

            if (terminalCollection.Count == 1)
            {
                return terminalCollection[0];
            }

            return null;
        }

        #endregion
    }
}
