﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Logic.ScheduledCommands
{
    public abstract class CommandSchedulerBase
    {
        #region Constants

        public const int CONCURRENT_COUNT = 5;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the value indicating the timespan for a scheduled command execution to expire
        /// </summary>
        public abstract TimeSpan ExpirationTimeSpan
        { get; }

        /// <summary>
        /// Gets the value indicating whether a client or terminal is currently online
        /// </summary>
        public abstract bool IsOnline
        { get; }

        /// <summary>
        /// Gets or sets the scheduled command for this scheduler
        /// </summary>
        public ScheduledCommandEntity ScheduledCommand
        { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Executes the command for this scheduler
        /// </summary>
        public void ExecuteCommand()
        {
            // And set the status of the scheduled command to 'Started'
            this.ScheduledCommand.Status = ScheduledCommandStatus.Started;
            this.ScheduledCommand.StartedUTC = DateTime.UtcNow;
            this.ScheduledCommand.ExpiresUTC = this.ScheduledCommand.StartedUTC.Value.Add(this.ExpirationTimeSpan);
            this.ScheduledCommand.Save();

            // Fire the command from the Comet helper
            ExecuteCommandFromCometHelper();

            // Execute additional logic after firing the command to the client of terminal
            PostExecuteCommand();
        }

        /// <summary>
        /// Executes the actual Comet command
        /// </summary>
        protected abstract void ExecuteCommandFromCometHelper();

        /// <summary>
        /// Executes the logic which has to be executed after the command is fired
        /// </summary>
        protected abstract void PostExecuteCommand();

        /// <summary>
        /// Gets a value indicating whether the command has been executed successfully
        /// </summary>
        public abstract bool HasCommandExecutedSuccessfully();

        /// <summary>
        /// Gets a value indicating whether the command can be executed
        /// </summary>
        public abstract bool CanExecuteCommand();

        #endregion
    }
}
