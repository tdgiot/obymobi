﻿using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Enums;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.Globalization
{
    public class TranslationManager
    {
        #region Fields
        
        private Dictionary<EntityType, IEntityCollection> translationsPerEntityType;
        // TODO: This screws up the name of the generic export file, why is this hardcoded?
        private int companyId = 227;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the TranslationManager class
        /// </summary>
        public TranslationManager(int companyId)
        {
            this.companyId = companyId;
        }

        #endregion

        #region Methods

        public void LoadGenericTranslations()
        {
            // Retrieve generic data
            GenericTranslationManager translationManager = new GenericTranslationManager();
            translationManager.FetchData();

            CustomTextCollection genericCustomTexts = translationManager.GetCustomTexts();

            //// Initialize the dictionary containing the generic translatable entities
            this.translationsPerEntityType = new Dictionary<EntityType, IEntityCollection>();
            this.translationsPerEntityType.Add(EntityType.ActionButtonEntity, genericCustomTexts.Where(x => x.ActionButtonId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.AdvertisementEntity, genericCustomTexts.Where(x => x.AdvertisementId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.AmenityEntity, genericCustomTexts.Where(x => x.AmenityId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.AnnouncementEntity, genericCustomTexts.Where(x => x.AnnouncementId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.AttachmentEntity, genericCustomTexts.Where(x => x.AttachmentId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.EntertainmentcategoryEntity, genericCustomTexts.Where(x => x.EntertainmentcategoryId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.GenericcategoryEntity, genericCustomTexts.Where(x => x.GenericcategoryId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.GenericproductEntity, genericCustomTexts.Where(x => x.GenericproductId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.PageEntity, genericCustomTexts.Where(x => x.PageId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.PageTemplateEntity, genericCustomTexts.Where(x => x.PageTemplateId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.PageElementEntity, this.GetPageElementCollection());
            this.translationsPerEntityType.Add(EntityType.PointOfInterestEntity, genericCustomTexts.Where(x => x.PointOfInterestId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.SiteEntity, genericCustomTexts.Where(x => x.SiteId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.SiteTemplateEntity, genericCustomTexts.Where(x => x.SiteTemplateId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.UITabEntity, genericCustomTexts.Where(x => x.UITabId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.UIWidgetEntity, genericCustomTexts.Where(x => x.UIWidgetId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.VenueCategoryEntity, genericCustomTexts.Where(x => x.VenueCategoryId.HasValue).ToEntityCollection<CustomTextCollection>());
        }

        public void LoadCompanyTranslations()
        {
            // Retrieve company related data
            CompanyTranslationManager translationManager = new CompanyTranslationManager(this.companyId);
            translationManager.FetchData();

            CustomTextCollection companyCustomTexts = translationManager.GetCustomTexts();

            // Initialize the dictionary containing the company specific translatable entities
            this.translationsPerEntityType = new Dictionary<EntityType, IEntityCollection>();
            this.translationsPerEntityType.Add(EntityType.AdvertisementEntity, companyCustomTexts.Where(x => x.AdvertisementId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.AlterationEntity, companyCustomTexts.Where(x => x.AlterationId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.AlterationoptionEntity, companyCustomTexts.Where(x => x.AlterationoptionId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.AnnouncementEntity, companyCustomTexts.Where(x => x.AnnouncementId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.AttachmentEntity, companyCustomTexts.Where(x => x.AttachmentId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.AvailabilityEntity, companyCustomTexts.Where(x => x.AvailabilityId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.CategoryEntity, companyCustomTexts.Where(x => x.CategoryId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.ClientConfigurationEntity, companyCustomTexts.Where(x => x.ClientConfigurationId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.CompanyEntity, companyCustomTexts.Where(x => x.CompanyId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.DeliverypointgroupEntity, companyCustomTexts.Where(x => x.DeliverypointgroupId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.PageEntity, companyCustomTexts.Where(x => x.PageId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.PageElementEntity, this.GetPageElementCollection(this.companyId));
            this.translationsPerEntityType.Add(EntityType.ProductEntity, companyCustomTexts.Where(x => x.ProductId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.RoomControlAreaEntity, companyCustomTexts.Where(x => x.RoomControlAreaId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.RoomControlSectionEntity, companyCustomTexts.Where(x => x.RoomControlSectionId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.RoomControlSectionItemEntity, companyCustomTexts.Where(x => x.RoomControlSectionItemId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.RoomControlComponentEntity, companyCustomTexts.Where(x => x.RoomControlComponentId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.RoomControlWidgetEntity, companyCustomTexts.Where(x => x.RoomControlWidgetId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.SiteEntity, companyCustomTexts.Where(x => x.SiteId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.StationEntity, companyCustomTexts.Where(x => x.StationId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.UIFooterItemEntity, companyCustomTexts.Where(x => x.UIFooterItemId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.UITabEntity, companyCustomTexts.Where(x => x.UITabId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.UIWidgetEntity, companyCustomTexts.Where(x => x.UIWidgetId.HasValue).ToEntityCollection<CustomTextCollection>());
        }

        public void LoadMenuTranslations(int menuId)
        {
            // Retrieve menu related data
            MenuTranslationManager translationManager = new MenuTranslationManager(menuId);
            translationManager.FetchData();

            CustomTextCollection menuCustomTexts = translationManager.GetCustomTexts();

            // Initialize the dictionary containing the menu specific translatable entities
            this.translationsPerEntityType = new Dictionary<EntityType, IEntityCollection>();
            this.translationsPerEntityType.Add(EntityType.AlterationEntity, menuCustomTexts.Where(x => x.AlterationId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.AlterationoptionEntity, menuCustomTexts.Where(x => x.AlterationoptionId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.CategoryEntity, menuCustomTexts.Where(x => x.CategoryId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.ProductEntity, menuCustomTexts.Where(x => x.ProductId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.ProductgroupEntity, menuCustomTexts.Where(x => x.ProductgroupId.HasValue).ToEntityCollection<CustomTextCollection>());
        }

        public void LoadAppLessTranslations()
        {
            // Retrieve appless related data
            AppLessTranslationManager translationManager = new AppLessTranslationManager(this.companyId);
            translationManager.FetchData();

            CustomTextCollection appLessCustomTexts = translationManager.GetCustomTexts();

            // Only appless related
            this.translationsPerEntityType = new Dictionary<EntityType, IEntityCollection>();
            this.translationsPerEntityType.Add(EntityType.ApplicationConfigurationEntity, appLessCustomTexts.Where(x => x.ApplicationConfigurationId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.NavigationMenuItemEntity, appLessCustomTexts.Where(x => x.NavigationMenuItemId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.LandingPageEntity, appLessCustomTexts.Where(x => x.LandingPageId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.OutletEntity, appLessCustomTexts.Where(x => x.OutletId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.ServiceMethodEntity, appLessCustomTexts.Where(x => x.ServiceMethodId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.CheckoutMethodEntity, appLessCustomTexts.Where(x => x.CheckoutMethodId.HasValue).ToEntityCollection<CustomTextCollection>());
            this.translationsPerEntityType.Add(EntityType.WidgetEntity, appLessCustomTexts.Where(x => x.WidgetId.HasValue).ToEntityCollection<CustomTextCollection>());
        }

        public string Export(string path, string sourceCultureCode, List<string> destinationCultureCodes)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            // Get all of the translations
            Dictionary<EntityType, Dictionary<int, EntityTranslation>> entityTranslations = this.GetEntityTranslations(sourceCultureCode, destinationCultureCodes);

            // Export the translations to Excel
            return this.ExportToExcel(path, entityTranslations, sourceCultureCode, destinationCultureCodes);
        }

        public Dictionary<EntityType, Dictionary<int, EntityTranslation>> GetEntityTranslations(string sourceCultureCode, List<string> destinationCultureCodes)
        {
            Dictionary<EntityType, Dictionary<int, EntityTranslation>> entityTranslations = new Dictionary<EntityType, Dictionary<int, EntityTranslation>>();

            Dictionary<EntityType, IEntityCollection> entityTypes = this.translationsPerEntityType;
            foreach (EntityType entityType in entityTypes.Keys)
            {
                Dictionary<int, EntityTranslation> translationsPerEntity = new Dictionary<int, EntityTranslation>();

                // Get the function from the dictionary
                IEntityCollection entityCollection = entityTypes[entityType];
                foreach (IEntity entity in entityCollection)
                {
                    string entityName = entityType.ToString(); // i.e. AlterationEntity
                    
                    int parentId = -1;
                    if (entityName.Equals(nameof(PageElementEntity)))
                    {
                        parentId = (int)entity.Fields["PageId"].CurrentValue;
                    }
                    else
                    {
                        string parentEntityName = entityName.Replace("Entity", string.Empty); // i.e. Alteration
                        string parentIdFieldName = parentEntityName + "Id"; // i.e. AlterationId
                        parentId = (int)entity.Fields[parentIdFieldName].CurrentValue;
                    }

                    // Skip language agnostic entities  (GK Mixed after Mr T said OK).
                    if (entity.Fields["CultureCode"].CurrentValue == null)
                    {
                        continue;
                    }

                    string entityCultureCode = entity.Fields["CultureCode"].CurrentValue.ToString();
                    if (!entityCultureCode.Equals(sourceCultureCode, StringComparison.InvariantCultureIgnoreCase) && !destinationCultureCodes.Contains(entityCultureCode, StringComparison.InvariantCultureIgnoreCase))
                        continue;

                    if (!translationsPerEntity.TryGetValue(parentId, out EntityTranslation entityTranslation))
                    {
                        entityTranslation = new EntityTranslation();
                        translationsPerEntity.Add(parentId, entityTranslation);
                    }

                    entityTranslation.EntityType = entityType;
                    entityTranslation.ID = parentId;

                    if (!this.IsCustomTextEntity(entityType))
                    {
                        foreach (IEntityField entityField in entity.Fields)
                        {
                            // Skip if the field is not of type String
                            if (entityField.ActualDotNetType != typeof(string))
                                continue;

                            // All good
                            // Get the name of the field and it's value
                            string fieldName = entityField.Name;
                            string fieldValue = entityField.CurrentValue != null ? entityField.CurrentValue as string : string.Empty;

                            if (entityName.Equals(nameof(PageElementEntity)))
                            {
                                if (!fieldName.Equals("StringValue1"))
                                    continue;

                                if (entity is PageElementEntity pageElementEntity)
                                {
                                    if (pageElementEntity.PageElementType == (int)PageElementType.SingleLineText)
                                        fieldName = "Title";
                                    else if (pageElementEntity.PageElementType == (int)PageElementType.MultiLineText)
                                        fieldName = "Text";
                                }
                            }
                            
                            if (!entityTranslation.FieldTranslations.TryGetValue(fieldName, out EntityTranslation.EntityFieldTranslation entityFieldTranslation))
                            {
                                entityFieldTranslation = new EntityTranslation.EntityFieldTranslation();
                                entityFieldTranslation.FieldName = fieldName;

                                entityTranslation.FieldTranslations.Add(fieldName, entityFieldTranslation);
                            }

                            if (!entityFieldTranslation.FieldValues.ContainsKey(entityCultureCode))
                                entityFieldTranslation.FieldValues.Add(entityCultureCode, fieldValue);
                        }
                    }
                    else if (entity.Fields["Type"] != null)
                    {
                        CustomTextType customTextType = (CustomTextType)entity.Fields["Type"].CurrentValue;
                        string fieldName = customTextType.ToString();
                        int type = (int)customTextType;
                        string fieldValue = entity.Fields["Text"].CurrentValue != null ? entity.Fields["Text"].CurrentValue as string : string.Empty;

                        EntityTranslation.EntityFieldTranslation entityFieldTranslation;

                        if (!entityTranslation.FieldTranslations.TryGetValue(fieldName, out entityFieldTranslation))
                        {
                            entityFieldTranslation = new EntityTranslation.EntityFieldTranslation();
                            entityFieldTranslation.FieldName = fieldName;
                            entityFieldTranslation.Type = type;

                            entityTranslation.FieldTranslations.Add(fieldName, entityFieldTranslation);
                        }

                        if (!entityFieldTranslation.FieldValues.ContainsKey(entityCultureCode))
                            entityFieldTranslation.FieldValues.Add(entityCultureCode, fieldValue);
                    }
                }

                entityTranslations.Add(entityType, translationsPerEntity);
            }

            return entityTranslations;
        }

        public bool IsCustomTextEntity(EntityType type)
        {
            return type != EntityType.PageElementEntity;            
        }

        private string ExportToExcel(string path, Dictionary<EntityType, Dictionary<int, EntityTranslation>> entityTranslations, string sourceCultureCode, List<string> destinationCultureCodes)
        {
            SLDocument excel = new SLDocument();

            int row = 1;
            int column = 1;

            foreach (EntityType entityType in entityTranslations.Keys)
            {
                Dictionary<int, EntityTranslation> translationsPerEntity = entityTranslations[entityType];

                string entityName = entityType.ToString(); // i.e. AlterationEntity

                // Corner case for PageElementEntity
                if (entityName.Equals(nameof(PageElementEntity)))
                {
                    entityName = "PageElement";
                }
                else
                {                    
                    entityName = entityName.Replace("Entity", string.Empty); // Alteration
                }

                // Add a new worksheet and select it
                excel.AddWorksheet(entityName);
                excel.SelectWorksheet(entityName);

                // Reset the row and column counters
                row = 1;
                column = 1;

                excel.SetCellValue(row, column, "ID");
                column++;

                if (this.IsCustomTextEntity(entityType))
                {
                    excel.SetCellValue(row, column, "Type");
                    column++;
                }

                excel.SetCellValue(row, column, "Field");
                column++;

                excel.SetCellValue(row, column, sourceCultureCode);
                column++;

                foreach (string destinationCultureCode in destinationCultureCodes)
                {
                    excel.SetCellValue(row, column, destinationCultureCode);
                    column++;
                }

                foreach (EntityTranslation entityTranslation in translationsPerEntity.Values)
                {
                    foreach (EntityTranslation.EntityFieldTranslation entityFieldTranslation in entityTranslation.FieldTranslations.Values)
                    {
                        if (!entityFieldTranslation.FieldValues.ContainsKey(sourceCultureCode))
                            continue;

                        row++;
                        column = 1;

                        excel.SetCellValue(row, column, entityTranslation.ID);
                        column++;

                        if (this.IsCustomTextEntity(entityType))
                        {
                            excel.SetCellValue(row, column, entityFieldTranslation.Type);
                            column++;
                        }

                        excel.SetCellValue(row, column, entityFieldTranslation.FieldName);
                        column++;

                        excel.SetCellValue(row, column, entityFieldTranslation.FieldValues[sourceCultureCode]);
                        column++;

                        foreach (string destinationCultureCode in destinationCultureCodes)
                        {
                            if (entityFieldTranslation.FieldValues.Keys.Contains(destinationCultureCode))
                                excel.SetCellValue(row, column, entityFieldTranslation.FieldValues[destinationCultureCode]);

                            column++;
                        }
                    }

                    // Set the width for the columns
                    if (!this.IsCustomTextEntity(entityType))
                    {
                        excel.SetColumnWidth(1, 10); // ID
                        excel.SetColumnWidth(2, 20); // Field name
                        excel.SetColumnWidth(3, 100); // Source culture
                    }
                    else
                    {
                        excel.SetColumnWidth(1, 10); // ID
                        excel.SetColumnWidth(2, 10); // Type
                        excel.SetColumnWidth(3, 20); // Field name
                        excel.SetColumnWidth(4, 100); // Source culture
                    }

                    column = 4;
                    foreach (string destinationCultureCode in destinationCultureCodes)
                    {
                        excel.SetColumnWidth(column, 100);
                        column++;
                    }
                }
            }

            // Remove the first worksheet
            excel.DeleteWorksheet(SLDocument.DefaultFirstSheetName);

            // Select the first worksheet
            excel.SelectWorksheet(excel.GetWorksheetNames()[0]);

            // and save the Excel file
            string fullPath = string.Empty;
            if (this.companyId > 0)
                fullPath = Path.Combine(path, string.Format("Translations-Company-{0}-{1}.xlsx", this.companyId, DateTime.UtcNow.ToString("yyyyMMddhhmmss")));
            else
                fullPath = Path.Combine(path, string.Format("Translations-Generic-{0}.xlsx", DateTime.UtcNow.ToString("yyyyMMddhhmmss")));

            excel.SaveAs(fullPath);

            return fullPath;
            
        }

        public string ImportFromExcel(string path)
        {
            SLDocument excel = new SLDocument(path);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Importing spreadsheet");

            foreach (string worksheetName in excel.GetWorksheetNames())
            {
                // Select the worksheet
                excel.SelectWorksheet(worksheetName); // i.e. Alteration

                // Create the entity name for the language entity
                string entityName = worksheetName.Equals("PageElement", StringComparison.InvariantCultureIgnoreCase) ? nameof(PageElementEntity) : nameof(CustomTextEntity);

                // Get the entity collection 
                IEntityCollection entityCollection = DataFactory.EntityCollectionFactory.GetEntityCollection(entityName) as IEntityCollection;
                if (entityCollection == null)
                    continue;

                int column = 4;
                if (entityCollection is CustomTextCollection)
                {
                    column = 5;
                }

                // Walk through the translated columns
                while (excel.HasCellValue(1, column))
                {
                    // Get the culture code
                    string cultureCode = excel.GetCellValueAsString(1, column);

                    string parentEntityIdFieldName;
                    if (worksheetName.Equals("PageElement"))
                    {
                        parentEntityIdFieldName = "PageId";
                    }
                    else
                    {
                        parentEntityIdFieldName = $"{worksheetName}Id"; // i.e. AlterationId
                    }

                    int row = 2;

                    while (excel.HasCellValue(row, 1))
                    {
                        int id = excel.GetCellValueAsInt32(row, 1);
                        int type = -1;
                        string fieldName;
                        string fieldValue;

                        if (entityCollection is CustomTextCollection)
                        {
                            type = excel.GetCellValueAsInt32(row, 2);
                            fieldName = "Text";
                            fieldValue = excel.GetCellValueAsString(row, column);
                        }
                        else
                        {
                            fieldName = excel.GetCellValueAsString(row, 2);
                            fieldValue = excel.GetCellValueAsString(row, column);
                        }

                        if (id > 0 && !fieldName.IsNullOrWhiteSpace() && !fieldValue.IsNullOrWhiteSpace())
                        {
                            PredicateExpression filter;
                            if (worksheetName.Equals("PageElement"))
                            {
                                filter = new PredicateExpression();
                                filter.Add(PageElementFields.PageId == id);
                                filter.Add(PageElementFields.CultureCode == cultureCode);
                                if (fieldName.Equals("Title"))
                                    filter.Add(PageElementFields.PageElementType == PageElementType.SingleLineText);
                                else if (fieldName.Equals("Text"))
                                    filter.Add(PageElementFields.PageElementType == PageElementType.MultiLineText);
                            }
                            else
                            {
                                filter = LLBLGenFilterUtil.GetFieldCompareValueEqualPredicateExpression(entityName, new string[] { "CultureCode", "Type", parentEntityIdFieldName }, new object[] { cultureCode, type, id });
                            }
                            
                            entityCollection.GetMulti(filter);

                            if (worksheetName.Equals("PageElement"))
                            {
                                if (entityCollection.Count == 0)
                                {
                                    // Create page element
                                    PageElementEntity pageElementEntity = new PageElementEntity();
                                    pageElementEntity.PageId = id;
                                    pageElementEntity.CultureCode = cultureCode; 
                                    if (fieldName.Equals("Title"))
                                        pageElementEntity.PageElementType = (int)PageElementType.SingleLineText;
                                    else if (fieldName.Equals("Text"))
                                        pageElementEntity.PageElementType = (int)PageElementType.MultiLineText;
                                    pageElementEntity.SystemName = fieldName.ToLower();
                                    pageElementEntity.StringValue1 = fieldValue;
                                    pageElementEntity.Save();

                                    // Create site culture
                                    PageEntity pageEntity = new PageEntity(id);

                                    PredicateExpression siteCultureFilter = new PredicateExpression();
                                    siteCultureFilter.Add(SiteCultureFields.SiteId == pageEntity.SiteId);
                                    siteCultureFilter.Add(SiteCultureFields.CultureCode == cultureCode);

                                    if (EntityCollection.GetDbCount<SiteCultureCollection>(siteCultureFilter) == 0)
                                    {
                                        SiteCultureEntity siteCultureEntity = new SiteCultureEntity();
                                        siteCultureEntity.ParentCompanyId = pageEntity.ParentCompanyId;
                                        siteCultureEntity.SiteId = pageEntity.SiteId;
                                        siteCultureEntity.CultureCode = cultureCode;
                                        siteCultureEntity.Save();                                        
                                    }
                                }
                                else if (entityCollection.Count == 1 && (entityCollection[0].Fields["StringValue1"].CurrentValue == null || !entityCollection[0].Fields["StringValue1"].CurrentValue.Equals(fieldValue)))
                                {
                                    entityCollection[0].SetNewFieldValue("StringValue1", fieldValue);
                                    entityCollection[0].Save();
                                }
                            }
                            else
                            {

                                if (entityCollection.Count == 0)
                                {
                                    if (DataFactory.EntityFactory.GetEntity(entityName) is IEntity entity)
                                    {
                                        entity.SetNewFieldValue("CultureCode", cultureCode);
                                        entity.SetNewFieldValue(parentEntityIdFieldName, id);
                                        entity.SetNewFieldValue(fieldName, fieldValue);

                                        if (entityCollection is CustomTextCollection && type > 0)
                                        {
                                            entity.SetNewFieldValue("Type", type.ToEnum<CustomTextType>());
                                        }

                                        try
                                        {
                                            entity.Save();
                                        }
                                        catch (Exception ex)
                                        {
                                            sb.AppendLine(string.Format("* Failed to import translation: Sheet: {3}, Row: {4}, Culture: {2}, Field: {0}, ID: {1}", parentEntityIdFieldName, id, cultureCode, worksheetName, row));
                                            sb.AppendLine(string.Format("* - Exception: {0}", ex.Message.Split('.')[0]));
                                        }
                                    }
                                }
                                else if (entityCollection.Count == 1 && (entityCollection[0].Fields[fieldName].CurrentValue == null || !entityCollection[0].Fields[fieldName].CurrentValue.Equals(fieldValue)))
                                {
                                    entityCollection[0].SetNewFieldValue(fieldName, fieldValue);
                                    entityCollection[0].Save();
                                }
                            }
                        }

                        row++;
                    }

                    column++;
                }
            }

            // Close the sheet
            excel.CloseWithoutSaving();

            sb.AppendLine("Finished");
            return sb.ToString();
        }

        public string ValidateExcel(string path)
        {
            string errorMessage = string.Empty;

            SLDocument excel = new SLDocument(path);

            foreach (string worksheetName in excel.GetWorksheetNames())
            {
                // Select the worksheet
                excel.SelectWorksheet(worksheetName); // i.e. Alteration

                int column = 4;

                // Walk through the translated columns
                while (excel.HasCellValue(1, column))
                {
                    // Get the culture code
                    string cultureCode = excel.GetCellValueAsString(1, column);

                    bool validCultureCode = Obymobi.Culture.Mappings.ContainsKey(cultureCode);
                    if (!validCultureCode)                    
                    {
                        if (errorMessage.Length > 0)
                            errorMessage += ", ";

                        errorMessage += string.Format("'{0}'", cultureCode);
                    }

                    column++;
                }
            }

            // Close the sheet
            excel.CloseWithoutSaving();

            return errorMessage;
        }

        private PageElementCollection GetPageElementCollection(int companyId = 0)
        {
            PredicateExpression filter = new PredicateExpression();
            if (companyId > 0)
            {
                filter.Add(SiteFields.CompanyId == companyId);
            }
            else
            {
                filter.Add(SiteFields.CompanyId == DBNull.Value);
            }

            PredicateExpression typeFilter = new PredicateExpression();
            typeFilter.Add(PageElementFields.PageElementType == PageElementType.SingleLineText);
            typeFilter.AddWithOr(PageElementFields.PageElementType == PageElementType.MultiLineText);
            filter.Add(typeFilter);

            RelationCollection relations = new RelationCollection();
            relations.Add(PageElementEntityBase.Relations.PageEntityUsingPageId);
            relations.Add(PageEntityBase.Relations.SiteEntityUsingSiteId);

            SortExpression sort = new SortExpression(PageElementFields.PageId | SortOperator.Ascending);

            return EntityCollection.GetMulti<PageElementCollection>(filter, sort, relations, null, null);
        }

        #endregion

        #region Inner classes

        public class EntityTranslation
        {
            public EntityTranslation()
            {
                this.FieldTranslations = new Dictionary<string, EntityFieldTranslation>();
            }

            public EntityType EntityType { get; set; }
            public int ID { get; set; }            
            
            public Dictionary<string, EntityFieldTranslation> FieldTranslations { get; set; }

            public class EntityFieldTranslation
            {
                public EntityFieldTranslation()
                {
                    this.FieldValues = new Dictionary<string, string>();
                }

                public string FieldName { get; set; }
                public int Type { get; set; }
                public Dictionary<string, string> FieldValues { get; set; }
            }
        }

        #endregion
    }

}
