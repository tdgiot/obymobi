﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Globalization
{
    public class CompanyTranslationManager
    {
        #region Fields

        private readonly int companyId;
        private readonly string cultureCode;

        private CustomTextCollection customTextCollection;

        #endregion

        #region Constructors

        public CompanyTranslationManager(int companyId, string cultureCode)
        {
            this.companyId = companyId;
            this.cultureCode = cultureCode;
        }

        public CompanyTranslationManager(int companyId)
        {
            this.companyId = companyId;
        }

        #endregion

        #region Public methods

        public void FetchData()
        {
            this.GetFilterAndSortForCustomTextsForCompanyRelatedEntities(out PredicateExpression filter, out SortExpression sort);

            this.customTextCollection = new CustomTextCollection();
            this.customTextCollection.GetMulti(filter, 0, sort);
        }

        public CustomTextCollection GetCustomTexts()
        {
            if (this.customTextCollection == null)
            {
                this.customTextCollection = new CustomTextCollection();
            }
            return this.customTextCollection;
        }

        #endregion

        #region Private methods

        private void GetFilterAndSortForCustomTextsForCompanyRelatedEntities(out PredicateExpression filter, out SortExpression sort)
        {
            filter = new PredicateExpression();
            filter.Add(CustomTextFields.ParentCompanyId == this.companyId);
            if (!this.cultureCode.IsNullOrWhiteSpace())
            {
                filter.Add(CustomTextFields.CultureCode == this.cultureCode);
            }

            sort = new SortExpression();
            sort.Add(new SortClause(CustomTextFields.AdvertisementId, SortOperator.Ascending));             // Advertisement
            sort.Add(new SortClause(CustomTextFields.AlterationId, SortOperator.Ascending));                // Alteration
            sort.Add(new SortClause(CustomTextFields.AlterationoptionId, SortOperator.Ascending));          // Alterationoption
            sort.Add(new SortClause(CustomTextFields.AnnouncementId, SortOperator.Ascending));              // Announcement
            sort.Add(new SortClause(CustomTextFields.AttachmentId, SortOperator.Ascending));                // Attachment
            sort.Add(new SortClause(CustomTextFields.AvailabilityId, SortOperator.Ascending));              // Availability
            sort.Add(new SortClause(CustomTextFields.CategoryId, SortOperator.Ascending));                  // Category
            sort.Add(new SortClause(CustomTextFields.ClientConfigurationId, SortOperator.Ascending));       // ClientConfiguration
            sort.Add(new SortClause(CustomTextFields.CompanyId, SortOperator.Ascending));                   // Company
            sort.Add(new SortClause(CustomTextFields.DeliverypointgroupId, SortOperator.Ascending));        // Deliverypointgroup
            sort.Add(new SortClause(CustomTextFields.PageId, SortOperator.Ascending));                      // Page
            sort.Add(new SortClause(CustomTextFields.ProductId, SortOperator.Ascending));                   // Product
            sort.Add(new SortClause(CustomTextFields.RoomControlAreaId, SortOperator.Ascending));           // RoomControlArea
            sort.Add(new SortClause(CustomTextFields.RoomControlSectionId, SortOperator.Ascending));        // RoomControlSection
            sort.Add(new SortClause(CustomTextFields.RoomControlSectionItemId, SortOperator.Ascending));    // RoomControlSectionItem
            sort.Add(new SortClause(CustomTextFields.RoomControlComponentId, SortOperator.Ascending));      // RoomControlComponent
            sort.Add(new SortClause(CustomTextFields.RoomControlWidgetId, SortOperator.Ascending));         // RoomControlWidget
            sort.Add(new SortClause(CustomTextFields.SiteId, SortOperator.Ascending));                      // Site
            sort.Add(new SortClause(CustomTextFields.StationId, SortOperator.Ascending));                   // Station
            sort.Add(new SortClause(CustomTextFields.UIFooterItemId, SortOperator.Ascending));              // UIFooterItem
            sort.Add(new SortClause(CustomTextFields.UITabId, SortOperator.Ascending));                     // UITab
            sort.Add(new SortClause(CustomTextFields.UIWidgetId, SortOperator.Ascending));                  // UIWidget
        }

        #endregion
    }
}
