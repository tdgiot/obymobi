﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Globalization
{
    public class GenericTranslationManager
    {
        #region Fields

        private CustomTextCollection customTextCollection;

        #endregion

        #region Constructors

        public GenericTranslationManager()
        {
                        
        }

        #endregion

        #region Public methods

        public void FetchData()
        {
            this.GetRelationsAndSortForGenericEntities(out PredicateExpression filter, out SortExpression sort);

            this.customTextCollection = new CustomTextCollection();
            this.customTextCollection.GetMulti(filter, 0, sort);
        }

        public CustomTextCollection GetCustomTexts()
        {
            if (this.customTextCollection == null)
            {
                this.customTextCollection = new CustomTextCollection();
            }
            return this.customTextCollection;
        }

        #endregion

        #region Private methods

        private void GetRelationsAndSortForGenericEntities(out PredicateExpression filter, out SortExpression sort)
        {
            filter = new PredicateExpression();
            filter.Add(CustomTextFields.ParentCompanyId == DBNull.Value);

            sort = new SortExpression();
            sort.Add(new SortClause(CustomTextFields.ActionButtonId, SortOperator.Ascending));              // ActionButton
            sort.Add(new SortClause(CustomTextFields.AdvertisementId, SortOperator.Ascending));             // Advertisement
            sort.Add(new SortClause(CustomTextFields.AmenityId, SortOperator.Ascending));                   // Amenity
            sort.Add(new SortClause(CustomTextFields.AnnouncementId, SortOperator.Ascending));              // Announcement
            sort.Add(new SortClause(CustomTextFields.AttachmentId, SortOperator.Ascending));                // Attachment
            sort.Add(new SortClause(CustomTextFields.EntertainmentcategoryId, SortOperator.Ascending));     // Entertainmentcategory
            sort.Add(new SortClause(CustomTextFields.GenericcategoryId, SortOperator.Ascending));           // Genericcategory
            sort.Add(new SortClause(CustomTextFields.GenericproductId, SortOperator.Ascending));            // Genericproduct
            sort.Add(new SortClause(CustomTextFields.PageId, SortOperator.Ascending));                      // Page
            sort.Add(new SortClause(CustomTextFields.PageTemplateId, SortOperator.Ascending));              // PageTemplate
            sort.Add(new SortClause(CustomTextFields.PointOfInterestId, SortOperator.Ascending));           // PointOfInterest
            sort.Add(new SortClause(CustomTextFields.SiteId, SortOperator.Ascending));                      // Site
            sort.Add(new SortClause(CustomTextFields.SiteTemplateId, SortOperator.Ascending));              // SiteTemplate
            sort.Add(new SortClause(CustomTextFields.UITabId, SortOperator.Ascending));                     // UITab
            sort.Add(new SortClause(CustomTextFields.UIWidgetId, SortOperator.Ascending));                  // UIWidget
            sort.Add(new SortClause(CustomTextFields.VenueCategoryId, SortOperator.Ascending));             // VenueCategory
        }

        #endregion
    }
}
