﻿using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;

namespace Obymobi.Logic.Globalization
{
    public class MenuTranslationManager
    {
        #region Fields

        private readonly MenuEntity menu;

        private AlterationoptionCollection alterationoptions;
        private AlterationCollection alterations;
        private ProductCollection products;
        private CategoryCollection categories;
        private ProductgroupCollection productgroups;

        private CustomTextCollection customTextCollection;

        #endregion

        #region Constructors

        public MenuTranslationManager(int menuId)
        {
            this.menu = new MenuEntity(menuId);        
        }

        #endregion

        #region Public methods

        public void FetchData()
        {
            this.alterationoptions = this.GetAlterationoptions();
            this.alterations = this.GetAlterations();
            this.products = this.GetProducts();
            this.categories = this.GetCategories();
            this.productgroups = this.GetProductgroups();
        }

        public CustomTextCollection GetCustomTexts()
        {
            this.customTextCollection = new CustomTextCollection();
            this.customTextCollection.AddRange(this.GetCustomTextsForAlterationoptions());
            this.customTextCollection.AddRange(this.GetCustomTextsForAlterations());
            this.customTextCollection.AddRange(this.GetCustomTextsForProducts());
            this.customTextCollection.AddRange(this.GetCustomTextsForCategories());
            this.customTextCollection.AddRange(this.GetCustomTextsForProductgroups());

            return this.customTextCollection;
        }

        #endregion

        #region Private methods

        private CustomTextCollection GetCustomTextsForProductgroups()
        {
            CustomTextCollection customTexts = new CustomTextCollection();
            foreach (CustomTextCollection collection in this.productgroups.Where(this.DoesProductgroupBelongToMenu).Select(x => x.CustomTextCollection))
            {
                customTexts.AddRange(collection);
            }
            return customTexts;
        }

        private CustomTextCollection GetCustomTextsForAlterationoptions()
        {
            CustomTextCollection customTexts = new CustomTextCollection();
            foreach (CustomTextCollection collection in this.alterationoptions.Where(this.DoesAlterationoptionBelongToMenu).Select(x => x.CustomTextCollection))
            {
                customTexts.AddRange(collection);
            }
            return customTexts;        
        }

        private CustomTextCollection GetCustomTextsForAlterations()
        {
            CustomTextCollection customTexts = new CustomTextCollection();        
            foreach (CustomTextCollection collection in this.alterations.Where(this.DoesAlterationBelongToMenu).Select(x => x.CustomTextCollection))
            {
                customTexts.AddRange(collection);
            }
            return customTexts;
        }

        private CustomTextCollection GetCustomTextsForProducts()
        {
            CustomTextCollection customTexts = new CustomTextCollection();        
            foreach (CustomTextCollection collection in this.products.Where(this.DoesProductBelongToMenu).Select(x => x.CustomTextCollection))
            {
                customTexts.AddRange(collection);
            }
            return customTexts;
        }

        private CustomTextCollection GetCustomTextsForCategories()
        {
            CustomTextCollection customTexts = new CustomTextCollection();        
            foreach (CustomTextCollection collection in this.categories.Where(this.DoesCategoryBelongToMenu).Select(x => x.CustomTextCollection))
            {
                customTexts.AddRange(collection);
            }
            return customTexts;
        }

        private bool DoesProductgroupBelongToMenu(ProductgroupEntity productgroup)
        {
            foreach (ProductEntity productEntity in productgroup.ProductCollection)
            {
                if (DoesProductBelongToMenu(productEntity))
                {
                    return true;
                }
            }

            foreach (ProductgroupItemEntity productgroupItemEntity in productgroup.ProductgroupItemCollection)
            {
                if (productgroupItemEntity.NestedProductgroupId.HasValue && DoesProductgroupBelongToMenu(productgroupItemEntity.ProductgroupEntity1))
                {
                    return true;
                }

                if (productgroupItemEntity.ProductId.HasValue && DoesProductBelongToMenu(productgroupItemEntity.ProductEntity))
                {
                    return true;
                }
            }

            return false;
        }

        private bool DoesAlterationoptionBelongToMenu(AlterationoptionEntity alterationoption)
        {
            foreach (AlterationitemEntity alterationitem in alterationoption.AlterationitemCollection)
            {
                AlterationEntity alteration = alterations.SingleOrDefault(x => x.AlterationId == alterationitem.AlterationId);
                if (alteration != null)
                {
                    if (this.DoesAlterationBelongToMenu(alteration))
                    {
                        return true;
                    }
                }            
            }
            return false;
        }

        private bool DoesAlterationBelongToMenu(AlterationEntity alteration)
        {
            foreach (ProductAlterationEntity productAlteration in alteration.ProductAlterationCollection)
            {
                ProductEntity product = this.products.SingleOrDefault(x => x.ProductId == productAlteration.ProductId);
                if (product != null)
                {
                    if (this.DoesProductBelongToMenu(product))
                    {
                        return true;
                    }
                }                
            }

            if (alteration.ParentAlterationId.GetValueOrDefault(0) > 0 && alteration.AlterationId != alteration.ParentAlterationId.GetValueOrDefault(0))
            {
                AlterationEntity parentAlteration = this.alterations.SingleOrDefault(x => x.AlterationId == alteration.ParentAlterationId);
                if (parentAlteration != null)
                {
                    return this.DoesAlterationBelongToMenu(parentAlteration);
                }                
            }
            return false;
        }

        private bool DoesProductBelongToMenu(ProductEntity product)
        {
            foreach (ProductCategoryEntity productCategory in product.ProductCategoryCollection)
            {
                CategoryEntity category = this.categories.SingleOrDefault(x => x.CategoryId == productCategory.CategoryId);
                if (category != null)
                {
                    if (this.DoesCategoryBelongToMenu(category))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private bool DoesCategoryBelongToMenu(CategoryEntity category)
        {
            return category.MenuId == this.menu.MenuId;
        }

        private AlterationoptionCollection GetAlterationoptions()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.AlterationoptionEntity);
            prefetch.Add(AlterationoptionEntityBase.PrefetchPathCustomTextCollection);
            prefetch.Add(AlterationoptionEntityBase.PrefetchPathAlterationitemCollection);

            return EntityCollection.GetMulti<AlterationoptionCollection>(AlterationoptionFields.CompanyId == this.menu.CompanyId, AlterationoptionFields.AlterationoptionId | SortOperator.Ascending, null, prefetch, null);
        }

        private AlterationCollection GetAlterations()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.AlterationEntity);
            prefetch.Add(AlterationEntityBase.PrefetchPathCustomTextCollection);
            prefetch.Add(AlterationEntityBase.PrefetchPathProductAlterationCollection);

            return EntityCollection.GetMulti<AlterationCollection>(AlterationFields.CompanyId == this.menu.CompanyId, AlterationFields.AlterationId | SortOperator.Ascending, null, prefetch, null);
        }

        private ProductCollection GetProducts()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductEntity);
            prefetch.Add(ProductEntityBase.PrefetchPathCustomTextCollection);
            prefetch.Add(ProductEntityBase.PrefetchPathProductCategoryCollection);

            return EntityCollection.GetMulti<ProductCollection>(ProductFields.CompanyId == this.menu.CompanyId, ProductFields.CompanyId | SortOperator.Ascending, null, prefetch, null);
        }

        private CategoryCollection GetCategories()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.CategoryEntity);
            prefetch.Add(CategoryEntityBase.PrefetchPathCustomTextCollection);

            return EntityCollection.GetMulti<CategoryCollection>(CategoryFields.CompanyId == this.menu.CompanyId, CategoryFields.CategoryId | SortOperator.Ascending, null, prefetch, null);
        }

        private ProductgroupCollection GetProductgroups()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductgroupEntity);
            prefetch.Add(ProductgroupEntityBase.PrefetchPathCustomTextCollection);
            prefetch.Add(ProductgroupEntityBase.PrefetchPathProductCollection);
            prefetch.Add(ProductgroupEntityBase.PrefetchPathProductgroupItemCollection);

            return EntityCollection.GetMulti<ProductgroupCollection>(ProductgroupFields.CompanyId == this.menu.CompanyId, ProductgroupFields.ProductgroupId | SortOperator.Ascending, null, prefetch, null);
        }

        #endregion
    }
}