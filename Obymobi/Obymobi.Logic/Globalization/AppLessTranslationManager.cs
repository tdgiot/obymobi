﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Logic.Globalization
{
    public class AppLessTranslationManager
    {
        #region Fields

        private readonly int companyId;

        private CustomTextCollection customTextCollection;

        #endregion

        #region Constructors

        public AppLessTranslationManager(int companyId)
        {
            this.companyId = companyId;
        }

        #endregion

        #region Public methods

        public void FetchData()
        {
            this.GetFilterAndSortForCustomTextsForAppLessCompanyRelatedEntities(out PredicateExpression filter, out SortExpression sort);

            this.customTextCollection = new CustomTextCollection();
            this.customTextCollection.GetMulti(filter, 0, sort);
        }

        public CustomTextCollection GetCustomTexts()
        {
            if (this.customTextCollection == null)
            {
                this.customTextCollection = new CustomTextCollection();
            }
            return this.customTextCollection;
        }

        #endregion

        #region Private methods

        private void GetFilterAndSortForCustomTextsForAppLessCompanyRelatedEntities(out PredicateExpression filter, out SortExpression sort)
        {
            filter = new PredicateExpression(CustomTextFields.ParentCompanyId == this.companyId);

            sort = new SortExpression();
            sort.Add(new SortClause(CustomTextFields.ApplicationConfigurationId, SortOperator.Ascending));  // ApplicationConfiguration
            sort.Add(new SortClause(CustomTextFields.NavigationMenuItemId, SortOperator.Ascending));        // NavigationMenuItem
            sort.Add(new SortClause(CustomTextFields.WidgetId, SortOperator.Ascending));                    // Widget
            sort.Add(new SortClause(CustomTextFields.LandingPageId, SortOperator.Ascending));               // LandingPage
            sort.Add(new SortClause(CustomTextFields.OutletId, SortOperator.Ascending));                    // Outlet
            sort.Add(new SortClause(CustomTextFields.ServiceMethodId, SortOperator.Ascending));             // ServiceMethod
            sort.Add(new SortClause(CustomTextFields.CheckoutMethodId, SortOperator.Ascending));            // CheckoutMethod
        }

        #endregion
    }
}
