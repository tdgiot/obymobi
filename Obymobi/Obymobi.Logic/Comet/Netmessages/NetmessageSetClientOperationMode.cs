﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using System;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageSetClientOperationMode : Netmessage
	{
        public NetmessageSetClientOperationMode()
		{
			this.SetMessageType(NetmessageType.SetClientOperationMode);
		}

        /// <summary>
        /// This method is called to verify if the Netmessage found in the Netmessage table is a duplicate (i.e. 2 requests for restart) of this instance as is called upon saving the current instance of the Netmessage.
        /// With this method you can decide if the current message should be ignored since it's a duplicate and/or the existing message should be overwritten by this new message.        
        /// </summary>
        /// <param name="existingNetmessage">The message found in the Netmessage table</param>
        /// <param name="overwriteExistingNetmessageWithThisMessage">Decide wheter the existing message should be overwritten by the current instance.</param>
        /// <returns>False means it's not a duplicate and should be added to the Netmessage table, True means it's a duplicate and this instance should be disregarded, 
        /// but if overwriteExistingMessageWithThisMessage is true the existing message must be updated with the values of this instance and be saved.</returns>
        public override bool IsDuplicateMessage(Netmessage existingNetmessage, out bool overwriteExistingNetmessageWithThisMessage)
		{
			bool toReturn = false;
            overwriteExistingNetmessageWithThisMessage = false;

			int operationMode;
            if (int.TryParse(existingNetmessage.FieldValue1, out operationMode))
			{
				if (operationMode != ClientOperationMode)
				{
                    existingNetmessage.FieldValue1 = ClientOperationMode.ToString();
                    overwriteExistingNetmessageWithThisMessage = true;
					toReturn = true;
				}
			}

			return toReturn;
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', ClientOperationMode: '{2}'".FormatSafe(this.NetmessageId, this.Status, this.ClientOperationMode);
            }
            catch
            {
                return base.ToString();
            }
        }

		public int ClientOperationMode
		{
			get
			{
                int operationMode;
                int.TryParse(this.FieldValue1, out operationMode);

                ClientOperationMode operationModeEnum;
                if (!EnumUtil.TryParse(operationMode, out operationModeEnum))
                    operationMode = (int)Obymobi.Enums.ClientOperationMode.Unknown;
				
				return operationMode;
			}
			set 
            {
                ClientOperationMode operationModeEnum;
                if (!EnumUtil.TryParse(value, out operationModeEnum))
                    value = (int)Obymobi.Enums.ClientOperationMode.Unknown;
                
                this.FieldValue1 = value.ToString(); 
            }
		}		
	}
}
