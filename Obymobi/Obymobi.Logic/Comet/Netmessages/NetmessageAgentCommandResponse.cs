﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageAgentCommandResponse : Netmessage
	{
		public NetmessageAgentCommandResponse()
		{
			this.SetMessageType(NetmessageType.AgentCommandResponse);
			this.SaveToDatabase = false;
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', ResponseMessage: '{2}'".FormatSafe(
                    this.NetmessageId, this.Status, this.ReponseMessage);
            }
            catch
            {
                return base.ToString();
            }
        }

		public string ReponseMessage
		{
			get { return this.FieldValue1; }
			set { this.FieldValue1 = value; }
		}

	    public string SenderMacAddress
	    {
	        get { return this.FieldValue2; }
            set { this.FieldValue2 = value; }
	    }
	}
}
