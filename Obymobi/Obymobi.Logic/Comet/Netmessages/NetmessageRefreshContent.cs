﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using System;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageRefreshContent : Netmessage
	{
        public NetmessageRefreshContent()
		{
			this.SetMessageType(NetmessageType.RefreshContent);
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', ModelType: '{2}'".FormatSafe(this.NetmessageId, this.Status);
            }
            catch
            {
                return base.ToString();
            }
        }			    
	}
}
