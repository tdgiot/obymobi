﻿using System;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
    public class NetmessageSetCloudEnvironment : Netmessage
    {
        public NetmessageSetCloudEnvironment()
        {
            MessageType = NetmessageType.SetCloudEnvironment;
        }

        public CloudEnvironment CloudEnvironment
        {
            get 
            { 
                int outValue;
                int.TryParse(this.FieldValue1, out outValue);
                return outValue.ToEnum<CloudEnvironment>();
            }
            set { this.FieldValue1 = value.ToIntString(); }
        }

        public CloudEnvironment ManualUrl
        {
            get
            {
                int outValue;
                int.TryParse(this.FieldValue2, out outValue);
                return outValue.ToEnum<CloudEnvironment>();
            }
            set { this.FieldValue2 = value.ToIntString(); }
        }
    }
}
