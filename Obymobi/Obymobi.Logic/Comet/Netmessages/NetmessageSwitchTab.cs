﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageSwitchTab : Netmessage
	{
        public NetmessageSwitchTab()
		{
			this.SetMessageType(NetmessageType.SwitchTab);
		}

		public override void Validate()
		{
            if (this.ReceiverClientId <= 0 && this.ReceiverTerminalId <= 0 && this.ReceiverIdentifier.IsNullOrWhiteSpace())
            {
                throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "Message does not contains a receiver (ReceiverClientId, ReceiverTerminalId or ReceiverIdentifier)");
            }

            if (this.UITabId.Length == 0)
			{
				throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "No UI Tab Id specified");
			}
		}

        /// <summary>
        /// This method is called to verify if the Netmessage found in the Netmessage table is a duplicate (i.e. 2 requests for restart) of this instance as is called upon saving the current instance of the Netmessage.
        /// With this method you can decide if the current message should be ignored since it's a duplicate and/or the existing message should be overwritten by this new message.        
        /// </summary>
        /// <param name="existingNetmessage">The message found in the Netmessage table</param>
        /// <param name="overwriteExistingNetmessageWithThisMessage">Decide wheter the existing message should be overwritten by the current instance.</param>
        /// <returns>False means it's not a duplicate and should be added to the Netmessage table, True means it's a duplicate and this instance should be disregarded, 
        /// but if overwriteExistingMessageWithThisMessage is true the existing message must be updated with the values of this instance and be saved.</returns>
        public override bool IsDuplicateMessage(Netmessage existingNetmessage, out bool overwriteExistingNetmessageWithThisMessage)
		{
            overwriteExistingNetmessageWithThisMessage = false;
            return (this.FieldValue1.Equals(existingNetmessage.FieldValue1));
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', ReceiverClientId: '{2}', ReceiverTerminalId: '{3}', UITabId: '{4}'".FormatSafe(
                    this.NetmessageId, this.Status, this.ReceiverClientId, this.ReceiverTerminalId, this.UITabId);
            }
            catch
            {
                return base.ToString();
            }
        }       

		public string UITabId
		{
			get { return this.FieldValue1; }
			set { this.FieldValue1 = value; }
		}
	}
}
