﻿using System;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using Dionysos;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageTerminalCommand : Netmessage
	{
        public NetmessageTerminalCommand()
		{
			this.SetMessageType(NetmessageType.TerminalCommand);
		}

		public override void Validate()
		{
            // Don't change this validation, you should create a new NetmessageType if you need parameters,
            // NO, also not for a tiny little thingy!
            if (!this.FieldValue3.IsNullOrWhiteSpace() ||
                !this.FieldValue4.IsNullOrWhiteSpace() ||
                !this.FieldValue5.IsNullOrWhiteSpace() ||
                !this.FieldValue6.IsNullOrWhiteSpace() ||
                !this.FieldValue7.IsNullOrWhiteSpace() ||
                !this.FieldValue8.IsNullOrWhiteSpace() ||
                !this.FieldValue9.IsNullOrWhiteSpace() /*||
                !this.FieldValue10.IsNullOrWhiteSpace()*/)
                throw new ObymobiException(NetmessageValidateError.ValidationFailed, "TerminalCommand must be parameterless, create a new Netmessage type of you need parameters");
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', TerminalId: '{2}', Command: '{3}' ({4}), SupportTools: {4}".FormatSafe(
                    this.NetmessageId, this.Status, this.ReceiverTerminalId, this.TerminalCommand.ToEnum<TerminalCommand>(), this.TerminalCommand, this.ToSupportTools);
            }
            catch
            {
                return base.ToString();
            }
        }

        /// <summary>
        /// This method is called to verify if the Netmessage found in the Netmessage table is a duplicate (i.e. 2 requests for restart) of this instance as is called upon saving the current instance of the Netmessage.
        /// With this method you can decide if the current message should be ignored since it's a duplicate and/or the existing message should be overwritten by this new message.        
        /// </summary>
        /// <param name="existingNetmessage">The message found in the Netmessage table</param>
        /// <param name="overwriteExistingNetmessageWithThisMessage">Decide wheter the existing message should be overwritten by the current instance.</param>
        /// <returns>False means it's not a duplicate and should be added to the Netmessage table, True means it's a duplicate and this instance should be disregarded, 
        /// but if overwriteExistingMessageWithThisMessage is true the existing message must be updated with the values of this instance and be saved.</returns>
        public override bool IsDuplicateMessage(Netmessage existingNetmessage, out bool overwriteExistingNetmessageWithThisMessage)
		{           
            overwriteExistingNetmessageWithThisMessage = false;
            return this.FieldValue1.Equals(existingNetmessage.FieldValue1);            
		}

        /// <summary>
        /// A value from the TerminalCommand enum
        /// </summary>
		public int TerminalCommand
		{
			get
			{
				int outValue;
				int.TryParse(this.FieldValue1, out outValue);
				return outValue;
			}
			set { this.FieldValue1 = value.ToString(); }
		}

        public bool ToSupportTools
        {
            get
            {
                bool outValue;
                bool.TryParse(this.FieldValue2, out outValue);
                return outValue;
            }
            set { this.FieldValue2 = value.ToString(); }
        }
	}
}
