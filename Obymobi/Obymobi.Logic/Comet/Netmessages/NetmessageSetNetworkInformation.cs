﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageSetNetworkInformation : Netmessage
	{
		public NetmessageSetNetworkInformation()
		{
			this.SetMessageType(NetmessageType.SetNetworkInformation);
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', InternalIP: '{2}', ExternalIP: '{3}'".FormatSafe(
                    this.NetmessageId, this.Status, this.InternalIP, this.ExternalIP);
            }
            catch
            {
                return base.ToString();
            }
        }

		public string InternalIP
		{
			get { return this.FieldValue1; }
			set { this.FieldValue1 = value; }
		}

		public string ExternalIP
		{
			get { return this.FieldValue2; }
			set { this.FieldValue2 = value; }
		}
	}
}
