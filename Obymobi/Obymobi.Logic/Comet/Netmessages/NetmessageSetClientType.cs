﻿using Dionysos;
using System;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageSetClientType : Netmessage
	{
		public NetmessageSetClientType()
		{
			this.SetMessageType(NetmessageType.SetClientType);
		}

		public override void Validate()
		{
			if (ClientType == NetmessageClientType.Unknown)
			{
				throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "Unable to parse ClientType");
			}

			if (ClientType == NetmessageClientType.Emenu)
			{
				if (ClientId < 0)
				{
					throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "Unable to parse ClientId");
				}
				/*if (DeliverypointId < 0)
				{
					throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "Unable to parse DeliverypointId");
				}*/
			}
			else if (ClientType == NetmessageClientType.Console || ClientType == NetmessageClientType.OnsiteServer)
			{
				if (TerminalId <= 0)
				{
					throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "Unable to parse TerminalId");
				}
			}
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', ClientType: '{2}', ClientId: '{3}', DeliverypointId: '{4}', CompanyId: '{5}', DeliverypointgroupId: '{6}', TerminalId: '{7}', IsMasterConsole: '{8}'".FormatSafe(
                    this.NetmessageId, this.Status, this.ClientType, this.ClientId, this.DeliverypointId, this.CompanyId, this.DeliverypointgroupId, this.TerminalId, this.IsMasterConsole);
            }
            catch
            {
                return base.ToString();
            }
        }

		public NetmessageClientType ClientType
		{
			get
			{
				NetmessageClientType outValue;
				return Enum.TryParse(this.FieldValue1, true, out outValue) ? outValue : NetmessageClientType.Unknown;
			}
			set { this.FieldValue1 = value.ToIntString(); }
		}

		public int ClientId
		{
			get
			{
			    int outValue = 0;
			    if (ClientType == NetmessageClientType.Emenu)
			    {
			        int.TryParse(this.FieldValue2, out outValue);
			    }

			    return outValue;
			}
			set { this.FieldValue2 = value.ToString(); }
		}

		public int DeliverypointId
		{
			get
			{
				int outValue = 0;
			    if (ClientType == NetmessageClientType.Emenu)
			    {
			        int.TryParse(this.FieldValue3, out outValue);
			    }

			    return outValue;
			}
			set { this.FieldValue3 = value.ToString(); }
		}

		public int CompanyId
		{
			get
			{
				int outValue;
				int.TryParse(this.FieldValue4, out outValue);
				return outValue;
			}
			set { this.FieldValue4 = value.ToString(); }
		}

		public int DeliverypointgroupId
		{
			get
			{
				int outValue;
				int.TryParse(this.FieldValue5, out outValue);
				return outValue;
			}
			set { this.FieldValue5 = value.ToString(); }
		}

		public int TerminalId
		{
			get
			{
				int outValue = 0;
			    if (ClientType == NetmessageClientType.Console || ClientType == NetmessageClientType.OnsiteServer)
			    {
			        int.TryParse(this.FieldValue2, out outValue);
			    }

			    return outValue;
			}
			set { this.FieldValue2 = value.ToString(); }
		}

		public bool IsMasterConsole
		{
			get
			{
			    bool outValue = false;
			    if (ClientType == NetmessageClientType.Console || ClientType == NetmessageClientType.OnsiteServer)
			    {
			        bool.TryParse(this.FieldValue3, out outValue);
			    }

			    return outValue;
			}
			set { this.FieldValue3 = value.ToString(); }
		}
	}
}
