﻿using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
    public class NetmessageSetMasterTab : Netmessage
    {
        public NetmessageSetMasterTab()
        {
            MessageType = NetmessageType.SetMasterTab;
        }

        public bool MasterTabEnabled
        {
            get
            {
                bool ret;
                bool.TryParse(this.FieldValue1, out ret);
                return ret;
            }
            set { this.FieldValue1 = value.ToString(); }
        }

        public bool IsSetFromClient
        {
            get
            {
                bool ret;
                bool.TryParse(this.FieldValue2, out ret);
                return ret;
            }
            set { this.FieldValue2 = value.ToString(); }
        }
    }
}
