﻿using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
    public class NetmessageDeviceUnlink : Netmessage
    {
        public NetmessageDeviceUnlink() : base(NetmessageType.DeviceUnlink)
        {
            SaveToDatabase = false;
        }

        public NetmessageDeviceUnlink(string identifier) : this()
        {
            DeviceMacAddress = identifier;
        }

        public string DeviceMacAddress
        {
            get { return this.FieldValue1; }
            set { this.FieldValue1 = value; }
        }
    }
}
