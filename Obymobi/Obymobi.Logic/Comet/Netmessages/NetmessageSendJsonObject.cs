﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using System;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageSendJsonObject : Netmessage
	{
        public NetmessageSendJsonObject()
		{
			this.SetMessageType(NetmessageType.SendJsonObject);
		}

        /// <summary>
        /// This method is called to verify if the Netmessage found in the Netmessage table is a duplicate (i.e. 2 requests for restart) of this instance as is called upon saving the current instance of the Netmessage.
        /// With this method you can decide if the current message should be ignored since it's a duplicate and/or the existing message should be overwritten by this new message.        
        /// </summary>
        /// <param name="existingNetmessage">The message found in the Netmessage table</param>
        /// <param name="overwriteExistingNetmessageWithThisMessage">Decide wheter the existing message should be overwritten by the current instance.</param>
        /// <returns>False means it's not a duplicate and should be added to the Netmessage table, True means it's a duplicate and this instance should be disregarded, 
        /// but if overwriteExistingMessageWithThisMessage is true the existing message must be updated with the values of this instance and be saved.</returns>
        public override bool IsDuplicateMessage(Netmessage existingNetmessage, out bool overwriteExistingNetmessageWithThisMessage)
		{
			bool toReturn = false;
            overwriteExistingNetmessageWithThisMessage = false;

			int modelType;
            if (int.TryParse(existingNetmessage.FieldValue1, out modelType))
			{
                if (modelType != this.ModelType)
				{
                    existingNetmessage.FieldValue1 = this.ModelType.ToString();
                    overwriteExistingNetmessageWithThisMessage = true;
					toReturn = true;
				}
			}

			return toReturn;
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', ModelType: '{2}'".FormatSafe(this.NetmessageId, this.Status, this.ModelType);
            }
            catch
            {
                return base.ToString();
            }
        }

		public int ModelType
		{
			get
			{
                int modelType;
                int.TryParse(this.FieldValue1, out modelType);

                ModelType modelTypeEnum;
                if (!EnumUtil.TryParse(modelType, out modelTypeEnum))
                    modelType = (int)Obymobi.Enums.ModelType.Unknown;

                return modelType;
			}
			set 
            {
                ModelType modelTypeEnum;
                if (!EnumUtil.TryParse(value, out modelTypeEnum))
                    value = (int)Obymobi.Enums.ModelType.Unknown;
                
                this.FieldValue1 = value.ToString(); 
            }
		}

	    public string JsonObject
	    {
	        get { return this.FieldValue2; }
	        set { this.FieldValue2 = value; }
	    }
	}
}
