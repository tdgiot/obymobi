﻿using System.Globalization;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
    public class NetmessageSandboxConfig : Netmessage
    {
        public NetmessageSandboxConfig()
        {
            this.MessageType = NetmessageType.SandboxConfig;
            this.SaveToDatabase = false;
        }

        public string CompanyOwnerUsername
        {
            get { return this.FieldValue1;  }
            set { this.FieldValue1 = value; }
        }

        public string CompanyOwnerPassword
        {
            get { return this.FieldValue2; }
            set { this.FieldValue2 = value; }
        }

        public int CompanyId
        {
            get
            {
                int ret;
                int.TryParse(this.FieldValue3, out ret);
                return ret;
            }
            set
            {
                this.FieldValue3 = value.ToString(CultureInfo.InvariantCulture);
            }
        }

        public int TerminalId
        {
            get
            {
                int ret;
                int.TryParse(this.FieldValue4, out ret);
                return ret;
            }
            set
            {
                this.FieldValue4 = value.ToString(CultureInfo.InvariantCulture);
            }
        }

        public int ClientId
        {
            get
            {
                int ret;
                int.TryParse(this.FieldValue5, out ret);
                return ret;
            }
            set
            {
                this.FieldValue5 = value.ToString(CultureInfo.InvariantCulture);
            }
        }

        public string ReceiverIdentfier
        {
            get { return this.FieldValue6; }
            set { this.FieldValue6 = value; }
        }

        public int DeliverypointgroupId
        {
            get
            {
                int ret;
                int.TryParse(this.FieldValue7, out ret);
                return ret;
            }
            set
            {
                this.FieldValue7 = value.ToString(CultureInfo.InvariantCulture);
            }
        }

        public string Salt
        {
            get { return this.FieldValue8; }
            set { this.FieldValue8 = value; }
        }
    }
}
