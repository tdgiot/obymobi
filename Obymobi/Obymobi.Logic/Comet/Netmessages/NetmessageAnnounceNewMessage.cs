﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageAnnounceNewMessage : Netmessage
	{
		public NetmessageAnnounceNewMessage()
		{
			this.SetMessageType(NetmessageType.AnnounceNewMessage);
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}'".FormatSafe(
                    this.NetmessageId, this.Status);
            }
            catch
            {
                return base.ToString();
            }
        }

		public override void Validate()
		{
			if (this.ReceiverClientId <= 0 && this.ReceiverIdentifier.IsNullOrWhiteSpace() && this.ReceiverDeliverypointId <= 0)
			{
				throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "No receiver client/identifier or deliverypoint id found!");
			}
		}
	}
}
