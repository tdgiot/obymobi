﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageNewSurveyResult : Netmessage
	{
        public NetmessageNewSurveyResult()
		{
            this.SetMessageType(NetmessageType.NewSurveyResult);
		}

		public override void Validate()
		{
			if (this.ReceiverCompanyId <= 0)
			{
				throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "No receving company id specified");
			}		
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}'".FormatSafe(
                    this.NetmessageId, this.Status);
            }
            catch
            {
                return base.ToString();
            }
        }
	}
}
