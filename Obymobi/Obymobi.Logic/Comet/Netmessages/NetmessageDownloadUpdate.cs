﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageDownloadUpdate : Netmessage
	{
        public NetmessageDownloadUpdate()
		{
			this.SetMessageType(NetmessageType.DownloadUpdate);
			this.SaveToDatabase = false;
		}

		public override void Validate()
		{
			if (ApplicationCode.Length == 0)
			{
				throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "Application code not specified.");
			}
		}

		public string ApplicationCode 
		{
			get { return this.FieldValue1; }
			set { this.FieldValue1 = value; }
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', ApplicationCode: '{2}'".FormatSafe(
                    this.NetmessageId, this.Status, this.ApplicationCode);
            }
            catch
            {
                return base.ToString();
            }
        }
	}
}
