﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageSetPmsCheckedIn : Netmessage
	{
		public NetmessageSetPmsCheckedIn()
		{
			this.SetMessageType(NetmessageType.SetPmsCheckedIn);
		}

		public override void Validate()
		{
			if (this.ReceiverDeliverypointId <= 0)
			{
				throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "No receiver deliverypoint id specified!");
			}

			if (GuestInformationData.Length == 0)
			{
				throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "No guest information data specified");
			}
		}

        /// <summary>
        /// This method is called to verify if the Netmessage found in the Netmessage table is a duplicate (i.e. 2 requests for restart) of this instance as is called upon saving the current instance of the Netmessage.
        /// With this method you can decide if the current message should be ignored since it's a duplicate and/or the existing message should be overwritten by this new message.        
        /// </summary>
        /// <param name="existingNetmessage">The message found in the Netmessage table</param>
        /// <param name="overwriteExistingNetmessageWithThisMessage">Decide wheter the existing message should be overwritten by the current instance.</param>
        /// <returns>False means it's not a duplicate and should be added to the Netmessage table, True means it's a duplicate and this instance should be disregarded, 
        /// but if overwriteExistingMessageWithThisMessage is true the existing message must be updated with the values of this instance and be saved.</returns>
        public override bool IsDuplicateMessage(Netmessage existingNetmessage, out bool overwriteExistingNetmessageWithThisMessage)
		{
            overwriteExistingNetmessageWithThisMessage = false;
            return (this.FieldValue1.Equals(existingNetmessage.FieldValue1));
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', GuestInformationData: '{2}'".FormatSafe(
                    this.NetmessageId, this.Status, this.GuestInformationData);
            }
            catch
            {
                return base.ToString();
            }
        }

		public string GuestInformationData
		{
			get { return this.FieldValue1; }
			set { this.FieldValue1 = value; }
		}
	}
}
