﻿using Dionysos;
using System;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageNewEventExternalListener : Netmessage
	{
		public NetmessageNewEventExternalListener()
		{
			this.SetMessageType(NetmessageType.NewEventExternalListener);
			this.SaveToDatabase = false;
		}

		public override void Validate()
		{
			if (EventType == CometExternalEventType.Unknown)
			{
				throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "Unable to parse EventType");
			}

			if (CompanyId <= 0)
			{
				throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "Unable to parse CompanyId");
			}
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', EventType: '{2}', CompanyId: '{3}'".FormatSafe(
                    this.NetmessageId, this.Status, this.EventType.ToString(), this.CompanyId);
            }
            catch
            {
                return base.ToString();
            }
        }

		public CometExternalEventType EventType
		{
			get
			{
				CometExternalEventType outValue;
				return Enum.TryParse(this.FieldValue1, out outValue) ? outValue : CometExternalEventType.Unknown;
			}
			set { this.FieldValue1 = value.ToIntString(); }
		}

		public int CompanyId
		{
			get
			{
				int outValue;
				int.TryParse(this.FieldValue2, out outValue);
				return outValue;
			}
			set { this.FieldValue2 = value.ToString(); }
		}
	}
}
