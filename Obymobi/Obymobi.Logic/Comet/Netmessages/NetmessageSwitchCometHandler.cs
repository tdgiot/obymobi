﻿using System;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
    public class NetmessageSwitchCometHandler : Netmessage
    {
        public NetmessageSwitchCometHandler()
            : base(NetmessageType.SwitchCometHandler)
        {
            
        }

        public override void Validate()
        {
            if (Handler == ClientCommunicationMethod.All || Handler == ClientCommunicationMethod.None)
            {
                throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "'All' and 'None' are not valid Handler types");
            }
        }

        public override bool IsDuplicateMessage(Netmessage existingNetmessage, out bool overwriteExistingNetmessageWithThisMessage)
        {
            base.IsDuplicateMessage(existingNetmessage, out overwriteExistingNetmessageWithThisMessage);

            if (existingNetmessage.FieldValue1 != this.FieldValue1)
            {
                overwriteExistingNetmessageWithThisMessage = true;
                return true;
            }

            overwriteExistingNetmessageWithThisMessage = false;
            return true;
        }

        public ClientCommunicationMethod Handler
        {
            get
            {
                return this.FieldValue1.ToEnum<ClientCommunicationMethod>();
            }
            set
            {
                this.FieldValue1 = value.ToIntString();
            }
        }
    }
}
