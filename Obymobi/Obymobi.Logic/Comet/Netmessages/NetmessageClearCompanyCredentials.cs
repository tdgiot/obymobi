﻿using System;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
    public class NetmessageClearCompanyCredentials : Netmessage
    {
        #region Constructors

        public NetmessageClearCompanyCredentials()
            : base(NetmessageType.ClearCompanyCredentials)
        {
            SaveToDatabase = true;
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            try { return "NetmessageId: '{0}', Status: '{1}'".FormatSafe(this.NetmessageId, this.Status); }
            catch { return base.ToString(); }
        }

        public override void Validate()
        {
            base.Validate();

            if (this.ReceiverIdentifier.IsNullOrWhiteSpace())
                throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "No receiver identifier found!");
        }

        #endregion
    }
}
