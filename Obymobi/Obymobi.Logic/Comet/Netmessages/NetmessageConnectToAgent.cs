﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageConnectToAgent : Netmessage
	{
		public NetmessageConnectToAgent()
		{
			this.SetMessageType(NetmessageType.ConnectToAgent);
			this.SaveToDatabase = false;
		}

		public override void Validate()
		{
			if (AgentMacAddress.Length == 0)
			{
				throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "Agent MAC address not specified.");
			}
		}

		public string AgentMacAddress 
		{
			get { return this.FieldValue1; }
			set { this.FieldValue1 = value; }
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', AgentMacAddress: '{2}'".FormatSafe(
                    this.NetmessageId, this.Status, this.AgentMacAddress);
            }
            catch
            {
                return base.ToString();
            }
        }
	}
}
