﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
    public class NetmessageMigrateToRestApi : Netmessage
    {
        public NetmessageMigrateToRestApi() : base(NetmessageType.MigrateToRestApi)
        {
            SaveToDatabase = true;
        }

        public override string ToString()
        {
            try
            {
                return $"NetmessageId: '{NetmessageId}', " +
                  $"Status: '{Status}', " +
                  $"CompanyOwnerUsername: '{CompanyOwnerUsername}', " +
                  $"CompanyOwnerPassword: '{CompanyOwnerPasswordEncrypted}', " +
                  $"DeliverypointgroupId: '{DeliverypointgroupId}'";
            }
            catch { return base.ToString(); }
        }

        public override void Validate()
        {
            base.Validate();

            if (ReceiverClientId <= 0)
            {
                throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "No receiver client id set");
            }

            if (CompanyOwnerUsername.IsNullOrWhiteSpace())
            {
                throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "No company owner username set");
            }

            if (CompanyOwnerPasswordEncrypted.IsNullOrWhiteSpace())
            {
                throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "No company owner password set");
            }

            if (DeliverypointgroupId <= 0)
            {
                throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "No deliverypointgroup id set");
            }
        }

        public string CompanyOwnerUsername
        {
            get { return FieldValue1; }
            set { FieldValue1 = value; }
        }

        public string CompanyOwnerPasswordEncrypted
        {
            get { return FieldValue2; }
            set { FieldValue2 = value; }
        }

        public int DeliverypointgroupId
        {
            get { return int.TryParse(FieldValue3, out int deliverypointgroupId) ? deliverypointgroupId : 0; }
            set { FieldValue3 = value.ToString(); }
        }
    }
}
