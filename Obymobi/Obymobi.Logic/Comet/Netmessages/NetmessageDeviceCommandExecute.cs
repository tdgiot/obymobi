﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
    public class NetmessageDeviceCommandExecute : Netmessage
    {
        public enum ValidationError
        {
            NoCommandSupplied
        }

        public NetmessageDeviceCommandExecute()
            : base(NetmessageType.DeviceCommandExecute)
        {
            
        }

        public override bool IsDuplicateMessage(Netmessage existingNetmessage, out bool overwriteExistingNetmessageWithThisMessage)
        {
            base.IsDuplicateMessage(existingNetmessage, out overwriteExistingNetmessageWithThisMessage);

            if (existingNetmessage.FieldValue1 == this.FieldValue1)
            {
                overwriteExistingNetmessageWithThisMessage = true;
                return true;
            }

            return false;
        }

        public override void Validate()
        {
            if (string.IsNullOrEmpty(this.FieldValue1))
            {
                throw new ObymobiNetmessageException(ValidationError.NoCommandSupplied, this);
            }
        }

        public string Command
        {
            get { return this.FieldValue1; }
            set { this.FieldValue1 = value; }
        }

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', Command: '{2}'".FormatSafe(
                    this.NetmessageId, this.Status, this.Command);
            }
            catch
            {
                return base.ToString();
            }
        }
    }
}
