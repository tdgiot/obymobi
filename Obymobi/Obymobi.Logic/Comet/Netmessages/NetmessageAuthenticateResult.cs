﻿using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageAuthenticateResult : Netmessage
	{
		public NetmessageAuthenticateResult()
		{
			this.SetMessageType(NetmessageType.AuthenticateResult);
			this.SaveToDatabase = false;
		}

		public bool IsAuthenticated
		{
			get
			{
				bool outValue;
				bool.TryParse(this.FieldValue1, out outValue);
				return outValue;
			}
			set { this.FieldValue1 = value.ToString().ToLower(); }
		}

		public string AuthenticateErrorMessage
		{
			get { return this.FieldValue2; }
			set { this.FieldValue2 = value; }
		}
	}
}
