﻿using Obymobi.Enums;
using Obymobi.Logic.Model;
using Dionysos;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageSetDeliverypointId : Netmessage
	{
        public NetmessageSetDeliverypointId()
		{
			this.SetMessageType(NetmessageType.SetDeliverypointId);
		}

		public override void Validate()
		{
            // Is allowed, because we need to be able to UNset a deliverypoint
            //if (this.DeliverypointId <= 0)
            //{
            //    throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "DeliverypointId is not specified.");
            //}

            //if (this.DeliverypointNumber.IsNullOrWhiteSpace())
            //{
            //    throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "DeliverypointNumber id is not specified.");
            //}
		}

        /// <summary>
        /// This method is called to verify if the Netmessage found in the Netmessage table is a duplicate (i.e. 2 requests for restart) of this instance as is called upon saving the current instance of the Netmessage.
        /// With this method you can decide if the current message should be ignored since it's a duplicate and/or the existing message should be overwritten by this new message.        
        /// </summary>
        /// <param name="existingNetmessage">The message found in the Netmessage table</param>
        /// <param name="overwriteExistingNetmessageWithThisMessage">Decide wheter the existing message should be overwritten by the current instance.</param>
        /// <returns>False means it's not a duplicate and should be added to the Netmessage table, True means it's a duplicate and this instance should be disregarded, 
        /// but if overwriteExistingMessageWithThisMessage is true the existing message must be updated with the values of this instance and be saved.</returns>
        public override bool IsDuplicateMessage(Netmessage existingNetmessage, out bool overwriteExistingNetmessageWithThisMessage)
		{
            // In case another message for a DP change is pending, we ALWAYS have to 'overwrite' it.

            overwriteExistingNetmessageWithThisMessage = false;
            var existingNetMessage = existingNetmessage.ConvertTo<NetmessageSetDeliverypointId>();

            if (existingNetMessage.DeliverypointId == this.DeliverypointId && existingNetMessage.DeliverypointgroupId == this.DeliverypointgroupId)
            {
                // Change to the same Deliverypoint, just ignore this new message.
            }
            else
            { 
                // This is a new Deliverypoint, update the existing message
                overwriteExistingNetmessageWithThisMessage = true;
            }            

            return true;
		}

		public int DeliverypointId
		{
			get
			{
				int outValue;
				int.TryParse(this.FieldValue1, out outValue);
				return outValue;
			}
			set { this.FieldValue1 = value.ToString(); }
		}

        public string DeliverypointNumber
        {
            get
            {
                return this.FieldValue2;
            }
            set { this.FieldValue2 = value; }
        }

        public int DeliverypointgroupId
        {
            get
            {
                int outValue;
                int.TryParse(this.FieldValue3, out outValue);
                return outValue;
            }
            set { this.FieldValue3 = value.ToString(); }
        }

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', DeliverypointId: '{2}', DeliverypointNumber: '{3}'".FormatSafe(
                    this.NetmessageId, this.Status, this.DeliverypointId, this.DeliverypointNumber);
            }
            catch
            {
                return base.ToString();
            }
        }
	}
}
