﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessagePing : Netmessage
	{
		public NetmessagePing()
		{
			this.SetMessageType(NetmessageType.Ping);
			this.SaveToDatabase = false;
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', Unixtime: '{2}'".FormatSafe(
                    this.NetmessageId, this.Status, this.UnixTime);
            }
            catch
            {
                return base.ToString();
            }
        }

		public long UnixTime
		{
			get
			{
				long outValue;
				long.TryParse(this.FieldValue1, out outValue);
				return outValue;
			}
			set { this.FieldValue1 = value.ToString(); }
		}
	}
}
