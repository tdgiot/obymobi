﻿using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageGetConnectedClients : Netmessage
	{
		public NetmessageGetConnectedClients() : base(NetmessageType.GetConnectedClients)
		{
			SaveToDatabase = false;
		}

		public int CompanyId
		{
			get
			{
				int companyId;
				if (!int.TryParse(this.FieldValue1, out companyId))
					companyId = 0;

				return companyId;
			}
			set { this.FieldValue1 = value.ToString(); }
		}

		public string[] ClientIdentifiers
		{
			get
			{
				if (this.FieldValue2 == null)
					this.FieldValue2 = "";

				return this.FieldValue2.Split(',');
			}
			set
			{
				this.FieldValue2 = "";
				foreach (string i in value)
				{
					this.FieldValue2 += i + ",";
				}

				if (this.FieldValue2.Length > 0)
					this.FieldValue2 = this.FieldValue2.Remove(this.FieldValue2.Length - 1);
			}
		}

		public string[] TerminalIdentifiers
		{
			get
			{
				if (this.FieldValue3 == null)
					this.FieldValue3 = "";

				return this.FieldValue3.Split(',');
			}
			set
			{
				this.FieldValue3 = "";
				foreach (string i in value)
				{
					this.FieldValue3 += i + ",";
				}

				if (this.FieldValue3.Length > 0)
					this.FieldValue3 = this.FieldValue3.Remove(this.FieldValue3.Length - 1);
			}
		}

		public string[] SupportToolsIdentifiers
		{
			get
			{
				if (this.FieldValue4 == null)
					this.FieldValue4 = "";

				return this.FieldValue4.Split(',');
			}
			set
			{
				this.FieldValue4 = "";
				foreach (string i in value)
				{
					this.FieldValue4 += i + ",";
				}

				if (this.FieldValue4.Length > 0)
					this.FieldValue4 = this.FieldValue4.Remove(this.FieldValue4.Length - 1);
			}
		}

	    public string SandboxIdentifiers
	    {
	        get { return this.FieldValue5; }
            set { this.FieldValue5 = value; }
	    }

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', CompanyId: '{2}', ClientIdentifiers: '{3}', TerminalIdentifiers: '{4}', SupportToolsIdentifiers: '{5}'".FormatSafe(
                    this.NetmessageId, this.Status, this.FieldValue1, this.FieldValue2, this.FieldValue3, this.FieldValue4);
            }
            catch
            {
                return base.ToString();
            }
        }

	}
}
