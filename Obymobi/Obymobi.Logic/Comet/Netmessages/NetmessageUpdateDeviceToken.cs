﻿using System;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
    public class NetmessageUpdateDeviceToken : Netmessage
    {
        #region Constructors

        public NetmessageUpdateDeviceToken()
            : base(NetmessageType.UpdateDeviceToken)
        {
            SaveToDatabase = true;
        }

        #endregion

        #region Properties

        public string DeviceToken
        {
            get => this.FieldValue1;
            set => this.FieldValue1 = value;
        }

        public int DeviceTokenTaskId
        {
            get
            {
                int.TryParse(this.FieldValue2, out int outValue);
                return outValue;
            }
            set => this.FieldValue2 = value.ToString();
        }

        public int DeviceTokenAction
        {
            get
            {
                int.TryParse(this.FieldValue3, out int outValue);
                return outValue;
            }
            set => this.FieldValue3 = value.ToString();
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            try { return "NetmessageId: '{0}', Status: '{1}'".FormatSafe(this.NetmessageId, this.Status); }
            catch { return base.ToString(); }
        }

        public override void Validate()
        {
            base.Validate();

            if (this.ReceiverIdentifier.IsNullOrWhiteSpace())
                throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "No receiver identifier found!");
        }

        #endregion
    }
}
