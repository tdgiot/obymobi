﻿using System;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessagePong : Netmessage
	{
        public NetmessagePong()
		{
			this.SetMessageType(NetmessageType.Pong);
			this.SaveToDatabase = false;
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', BatteryLevel: '{2}', IsCharging: '{3}', PrivateIpAddresses: '{4}', PublicIpAddress: '{5}', DeliverypointId: '{6}', CurrentCloudEnvironment: '{7}' ClientOperationMode: '{8}'".FormatSafe(
                    this.NetmessageId, this.Status, this.BatteryLevel, this.IsCharging, this.PrivateIpAddresses, this.PublicIpAddress, this.DeliverypointId, this.CurrentCloudEnvironment, this.ClientOperationMode);
            }
            catch
            {
                return base.ToString();
            }
        }

		public int BatteryLevel
		{
			get
			{
				int outValue;
				int.TryParse(this.FieldValue1, out outValue);
				return outValue;
			}
			set { this.FieldValue1 = value.ToString(); }
		}

        public bool IsCharging
        {
            get
            {
                bool outValue;
                bool.TryParse(this.FieldValue2, out outValue);
                return outValue;
            }
            set { this.FieldValue2 = value.ToString(); }
        }

        public string PrivateIpAddresses
        {
            get
            {
                return this.FieldValue3;
            }
            set { this.FieldValue3 = value; }
        }

        public string PublicIpAddress
        {
            get
            {
                return this.FieldValue4;
            }
            set { this.FieldValue4 = value; }
        }

        public int DeliverypointId
        {
            get
            {
                int outValue;
                int.TryParse(this.FieldValue5, out outValue);
                return outValue;
            }
            set { this.FieldValue5 = value.ToString(); }
        }

	    public CloudEnvironment CurrentCloudEnvironment
	    {
	        get 
            { 
                int outValue;
                Int32.TryParse(this.FieldValue6, out outValue);
	            //Enum.TryParse(this.FieldValue6, out outValue);
	            return outValue.ToEnum<CloudEnvironment>();
	        }
            set { this.FieldValue6 = value.ToIntString(); }
	    }


        public int ClientOperationMode
        {
            get
            {
                int outValue;
                int.TryParse(this.FieldValue7, out outValue);
                return outValue;
            }
            set { this.FieldValue7 = value.ToString(); }
        }

        public int LastWifiStrength
        {
            get
            {
                int outValue;
                int.TryParse(this.FieldValue8, out outValue);
                return outValue;
            }
            set { this.FieldValue8 = value.ToString(); }
        }

        public bool AgentIsRunning
        {
            get
            {
                bool outValue;
                bool.TryParse(this.FieldValue9, out outValue);
                return outValue;
            }
            set { this.FieldValue9 = value.ToString(); }
        }

        public bool SupportToolsIsRunning
        {
            get
            {
                bool outValue;
                bool.TryParse(this.FieldValue10, out outValue);
                return outValue;
            }
            set { this.FieldValue10 = value.ToString(); }
        }

        public bool BluetoothKeyboardConnected
        {
            get
            {
                bool outValue;
                bool.TryParse(this.FieldValue11, out outValue);
                return outValue;
            }
            set { this.FieldValue11 = value.ToString(); }
        }

        public bool BluetoothPrinterConnected
        {
            get
            {
                bool outValue;
                bool.TryParse(this.FieldValue12, out outValue);
                return outValue;
            }
            set { this.FieldValue12 = value.ToString(); }
        }

        public bool DeviceDevelopmentMode
        {
            get
            {
                bool outValue;
                bool.TryParse(this.FieldValue13, out outValue);
                return outValue;
            }
            set { this.FieldValue13 = value.ToString(); }
        }

        public bool RoomControlConnected
        {
            get
            {
                bool outValue;
                bool.TryParse(this.FieldValue14, out outValue);
                return outValue;
            }
            set { this.FieldValue14 = value.ToString(); }
        }

        public bool DoNotDisturbActive
        {
            get
            {
                bool outValue;
                bool.TryParse(this.FieldValue15, out outValue);
                return outValue;
            }
            set { this.FieldValue15 = value.ToString(); }
        }

        public bool ServiceRoomActive
        {
            get
            {
                bool outValue;
                bool.TryParse(this.FieldValue16, out outValue);
                return outValue;
            }
            set { this.FieldValue16 = value.ToString(); }
        }

	    public string LastWifiSsid
	    {
	        get { return this.FieldValue17; }
            set { this.FieldValue17 = value; }
	    }
        
        public bool HeartbeatServiceActive
        {
            get
            {
                bool outValue;
                bool.TryParse(this.FieldValue18, out outValue);
                return outValue;
            }
            set { this.FieldValue18 = value.ToString(); }
        }
	}
}
