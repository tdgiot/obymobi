﻿using Obymobi.Enums;
using Obymobi.Logic.Model;
using Dionysos;

namespace Obymobi.Logic.Comet.Netmessages
{
	public class NetmessageAgentCommandRequest : Netmessage
	{
		public NetmessageAgentCommandRequest()
		{
			this.SetMessageType(NetmessageType.AgentCommandRequest);
		}

		public override void Validate()
		{
			if (AgentMacAddress.Length == 0)
			{
				throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "Invalid Agent MAC address.");
			}
			if (Command.Length == 0)
			{
				throw new ObymobiNetmessageException(NetmessageValidateError.ValidationFailed, this, "Command not specified");
			}
		}

        public override string ToString()
        {
            try
            {
                return "NetmessageId: '{0}', Status: '{1}', AgentMac: '{2}', Command: '{3}'".FormatSafe(
                    this.NetmessageId, this.Status, this.AgentMacAddress, this.Command);
            }
            catch
            {
                return base.ToString();
            }
        }

		public string AgentMacAddress
		{
			get { return this.FieldValue1; }
			set { this.FieldValue1 = value; }
		}

		public string Command
		{
			get { return this.FieldValue2; }
			set { this.FieldValue2 = value; }
		}
	}
}
