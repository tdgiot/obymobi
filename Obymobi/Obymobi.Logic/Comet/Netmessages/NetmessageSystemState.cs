﻿using System;
using System.Globalization;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Netmessages
{
    public class NetmessageSystemState : Netmessage
    {
        public NetmessageSystemState()
            : base(NetmessageType.SystemState)
        {
            SaveToDatabase = false;

            TotalConnectedClients = new[] { 0, 0, 0 };
            ConnectedTerminals = new[] { 0, 0, 0 };
            ConnectedEmenu = new[] { 0, 0, 0 };
            ConnectedSupportTools = new[] { 0, 0, 0 };
            ConnectedOnSiteServers = new[] { 0, 0, 0 };
        }

        /// <summary>
        /// Index: 0 = Total, 1 = PokeIn, 2 = SignalR
        /// </summary>
        public int[] TotalConnectedClients
        {
            get { return Array.ConvertAll(this.FieldValue1.Split(','), int.Parse); }
            set { this.FieldValue1 = String.Join(",", value); }
        }

        public bool IsCmsConnected
        {
            get
            {
                bool ret;
                bool.TryParse(this.FieldValue2, out ret);
                return ret;
            }
            set { this.FieldValue2 = value.ToString(); }
        }

        public bool IsNocConnected
        {
            get
            {
                bool ret;
                bool.TryParse(this.FieldValue3, out ret);
                return ret;
            }
            set { this.FieldValue3 = value.ToString(); }
        }

        public bool IsWebserviceConnected
        {
            get
            {
                bool ret;
                bool.TryParse(this.FieldValue4, out ret);
                return ret;
            }
            set { this.FieldValue4 = value.ToString(); }
        }

        /// <summary>
        /// Index: 0 = Total, 1 = PokeIn, 2 = SignalR
        /// </summary>
        public int[] ConnectedTerminals
        {
            get { return Array.ConvertAll(this.FieldValue5.Split(','), int.Parse); }
            set { this.FieldValue5 = String.Join(",", value); }
        }

        /// <summary>
        /// Index: 0 = Total, 1 = PokeIn, 2 = SignalR
        /// </summary>
        public int[] ConnectedEmenu
        {
            get { return Array.ConvertAll(this.FieldValue6.Split(','), int.Parse); }
            set { this.FieldValue6 = String.Join(",", value); }
        }

        /// <summary>
        /// Index: 0 = Total, 1 = PokeIn, 2 = SignalR
        /// </summary>
        public int[] ConnectedSupportTools
        {
            get { return Array.ConvertAll(this.FieldValue7.Split(','), int.Parse); }
            set { this.FieldValue7 = String.Join(",", value); }
        }

        /// <summary>
        /// Index: 0 = Total, 1 = PokeIn, 2 = SignalR
        /// </summary>
        public int[] ConnectedOnSiteServers
        {
            get { return Array.ConvertAll(this.FieldValue8.Split(','), int.Parse); }
            set { this.FieldValue8 = String.Join(",", value); }
        }

        public int ConnectedSandboxClients
        {
            get
            {
                int ret;
                int.TryParse(this.FieldValue9, out ret);
                return ret;
            }
            set { this.FieldValue9 = value.ToString(CultureInfo.InvariantCulture); }
        }
    }
}
