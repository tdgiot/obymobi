﻿namespace Obymobi.Logic.Comet
{
	public class CometConstants
	{
		public const string CometInternalSalt = "3e722b834e99091b3bb986be5e54d8bec366689cdc3937bb71f96adf9a361c95";
		public const string CometIdentifierWebservice = "WS_916ba459e02049d97e4582b98cef0fcd631329d0";
		public const string CometIdentifierCms = "CMS_103b24abd1d2cd2df282b5fd8d4de4e323a6127c";
		public const string CometIdentifierNoc = "NOC_33b2a382454f6327e9ada0dda9712c99cb68715d";
        public const string CometIdentifierServices = "SVC_544955976eab58e623ed086b62427837b62b4d87";

        public const string CometIdentifierControlCenter = "CometIdentifierControlCenter";
	}
}
