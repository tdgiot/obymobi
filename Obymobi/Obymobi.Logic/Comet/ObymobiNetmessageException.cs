﻿using System;
using Dionysos;
using Dionysos.Diagnostics;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet
{
	[Serializable]
	public class ObymobiNetmessageException : ObymobiException
	{
		private readonly Netmessage netmessage;

		public ObymobiNetmessageException(Enum errorEnumValue, Netmessage netmessage) : base(errorEnumValue)
		{
			this.netmessage = netmessage;
		}

		public ObymobiNetmessageException(Enum errorEnumValue, Netmessage netmessage, string message, params object[] args)
			: base(errorEnumValue, message, args)
		{
			this.netmessage = netmessage;
		}

		public Netmessage Netmessage
		{
			get { return this.netmessage; }
		}

		public override string Message
		{
			get
			{
				string text = "Message: {0}\r\n\r\nError Type: {1}\r\n\r\nErrorValue: {2} ({3})\r\n\r\nAdditional Information: {4}\r\n\r\nNetmessage: {5}\r\n\r\n"
					.FormatSafe(base.Message, this.ErrorEnumType, this.ErrorEnumValueText, this.additionalInformation, JsonHelper.SerializeObject(this.netmessage));

				if (!this.writtenToDebug && TestUtil.IsPcDeveloper)
				{
					Debug.WriteLine(text);
					this.writtenToDebug = true;
				}

				return text;
			}
		}
	}
}
