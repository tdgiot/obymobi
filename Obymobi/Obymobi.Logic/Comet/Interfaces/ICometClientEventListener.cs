﻿using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Interfaces
{
	public interface ICometClientEventListener
	{
		void OnConnectFailed();
		void OnConnectionChanged(bool isConnected);
		void OnNetmessageReceived(Netmessage netmessage);
		void OnEventLog(string message, params object[] args);
	}
}
