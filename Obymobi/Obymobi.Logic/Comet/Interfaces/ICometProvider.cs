﻿using System.Web;
using System.Web.SessionState;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet.Interfaces
{
    /// <summary>
    /// CometProvider class
    /// </summary>
    public interface ICometProvider
    {
        void Initialize();

		void SendMessage(Netmessage netmessage);

        void AddHandler(int key, IMessageHandler handler, HttpContext context);
        void RemoveHandler(int key);
    }

	public interface IMessageHandler
	{
        void ReceivedMessage(Netmessage message, HttpContext context = null);
	}

    public class MessageHandlerContainer
    {
        public IMessageHandler Handler;
        public HttpContext Context;
    }
}
