﻿using System;
using System.Collections.Concurrent;
using System.Data;
using System.Net;
using System.Threading;
using Dionysos;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.SignalR.Client.Transports;
using Newtonsoft.Json;
using Obymobi.Enums;
using Obymobi.Logic.Comet.Interfaces;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Security;
using ConnectionState = Microsoft.AspNet.SignalR.Client.ConnectionState;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Logic.Comet
{
    public class SignalRClient : CometClient
    {
        private readonly object clientLock;
        private readonly BlockingCollection<Netmessage> messageQueue;
        private readonly ConcurrentDictionary<string, Netmessage> nonVerifiedMessages;
        private CancellationTokenSource messageQueueCancelToken;

        private readonly string cometUrl;
        private readonly string appIdentifier;
        private readonly string appSalt;

        private HubConnection hubConnection;
        private IHubProxy hubProxy;      
        
        public bool ForceWebSocket { get; set; }  

        public SignalRClient(ICometClientEventListener eventListener, string url, string identifier, string salt)
            : base(eventListener)
        {
            this.clientLock = new object();
            this.messageQueue = new BlockingCollection<Netmessage>();
            this.nonVerifiedMessages = new ConcurrentDictionary<string, Netmessage>();

            this.cometUrl = url;
            this.appIdentifier = identifier;
            this.appSalt = salt;

            this.ForceWebSocket = true;
        }

        public override void Connect()
        {
            SetIsConnecting(true);

            lock (this.clientLock)
            {
                if (this.hubConnection != null)
                {
                    this.hubConnection.Dispose();
                    this.hubConnection = null;
                }

                this.hubConnection = new HubConnection(this.cometUrl);
                this.hubConnection.Headers.Add("deviceIdentifier", this.appIdentifier);
                this.hubConnection.StateChanged += hubConnection_StateChanged;
                this.hubConnection.Error += hubConnection_Error;
                //hubConnection.TraceLevel = TraceLevels.All;
                //hubConnection.TraceWriter = Console.Out;

                this.hubProxy = hubConnection.CreateHubProxy("SignalRInterface");
                this.hubProxy.On<string>("ReceiveMessage", data =>
                    {
                        try
                        {
                            var netmessage = JsonConvert.DeserializeObject<Netmessage>(data);
                            HandleNetmessage(netmessage);
                        }
                        catch (Exception ex)
                        {
                            WriteToLog("SignalRClient.ReceiveMessage - Failed to deserialize message. Message: {0}", data);
                            WriteToLog("SignalRClient.ReceiveMessage - " + ex.Message);
                        }
                    });
                this.hubProxy.On<string>("MessageVerified", this.MessageVerified);

                ServicePointManager.DefaultConnectionLimit = 10;

                if (this.ForceWebSocket)
                {
                    this.hubConnection.Start(new WebSocketTransport());
                }
                else
                {
                    this.hubConnection.Start();
                }
            }
        }

        #region Event

        void hubConnection_Error(Exception exception)
        {
            if (exception == null)
                return;

            if (exception.StackTrace == null)
                WriteToLog("Exception: {0}", exception.Message);
            else
                WriteToLog("Exception: {0}", exception.ProcessStackTrace(true));
        }

        private void hubConnection_StateChanged(StateChange state)
        {
            if (state.OldState.Equals(state.NewState))
                return;

            if (state.NewState == ConnectionState.Connected)
            {
                string hubConnectionId = GetHubConnectionId();
                if (!IsConnected() || IdentifierComet != hubConnectionId)
                {
                    WriteToLog("OnStateChanged - Connected: {0} - {1}", hubConnectionId, this.hubConnection.Transport.Name);
                    IdentifierComet = this.hubConnection.ConnectionId;
                    LastPong = DateTime.UtcNow;

                    SetIsConnecting(false);
                    SetConnected(true);

                    // Authenticate
                    Authenticate(this.appIdentifier, this.appSalt);
                }
            }
            else if (state.NewState == ConnectionState.Connecting)
            {
                WriteToLog("OnStateChanged - Connecting");

                SetIsConnecting(true);
                SetConnected(false);
            }
            else if (state.NewState == ConnectionState.Reconnecting)
            {
                if (this.hubConnection != null)
                {
                    //WriteToLog("OnStateChanged - Reconnecting");

                    //SetIsConnecting(true);
                    //SetConnected(false);
                }
            }
            else if (state.NewState == ConnectionState.Disconnected)
            {
                WriteToLog("OnStateChanged - Disconnected");

                bool connectionFailed = (!IsConnected() && IsConnecting());

                SetIsConnecting(false);
                SetConnected(false);

                Dispose();

                if (connectionFailed)
                    ConnectionFailed();
            }
            else
            {
                WriteToLog("StateChanged - Unhandled state change: {0} => {1}", state.OldState, state.NewState);
            }
        }

        #endregion

        public override bool CheckLastPingPong()
        {
            if (!IsAuthenticated)
            {
                return (LastPong > DateTime.UtcNow.AddMinutes(-1));
            }

            return (LastPong >= DateTime.UtcNow.AddMinutes(-2));
        }

        public override void SendMessage(Netmessage message)
        {
            message.AddTraceMessage("SignalRClient ({0}), adding message to queue", appIdentifier);
            messageQueue.Add(message);
        }

        public override void Disconnect()
        {
            IsAuthenticated = false;

            if (this.hubConnection != null)
            {
                lock(this.clientLock)
                {
                    this.hubConnection.Dispose();
                    this.hubConnection = null;

                    this.hubProxy = null;
                }
            }

            SetIsConnecting(false);
            SetConnected(false);
        }

        public override void Authenticate(string identifier, string salt)
        {
            if (IsConnected())
            {
                long timestamp = DateTime.UtcNow.ToUnixTime();
                string hash = Hasher.GetHashFromParameters(salt, timestamp, identifier, IdentifierComet);

                WriteToLog("SignalRClient.Authenticate - ClientId: {0} - Identifier: {1}", IdentifierComet, identifier);

                lock(this.clientLock)
                {
                    InvokeAuthenticate(timestamp, identifier, hash);
                }
            }
            else
            {
                WriteToLog("SignalRClient.Authenticate - Not connected to server...");
            }
        }

        public override void Pong()
        {
            LastPong = DateTime.UtcNow;
            TransmitMessage(new NetmessagePong());
        }

        public override void SetAuthenticated(bool isAuthenticated)
        {
            IsAuthenticated = isAuthenticated;

            if (isAuthenticated)
            {
                LastPong = DateTime.UtcNow;

                messageQueueCancelToken = new CancellationTokenSource();
                new Thread((ProcessNetmessageQueue)).Start(messageQueueCancelToken.Token);
            }
        }

        public override void Dispose()
        {
            WriteToLog("SignalRClient.Dispose");

            if (messageQueueCancelToken != null && !messageQueueCancelToken.IsCancellationRequested)
            {
                // Cancel message queue
                messageQueueCancelToken.Cancel(false);
                messageQueue.Add(null);
            }

            lock (this.clientLock)
            {
                if (this.hubConnection != null)
                {
                    WriteToLog("SignalRClient.Dispose - Closing connection");

                    this.hubConnection.StateChanged -= hubConnection_StateChanged;
                    this.hubConnection.Error -= hubConnection_Error;

                    this.hubConnection.Dispose();
                    this.hubConnection = null;

                    this.hubProxy = null;
                }
                else
                {
                    WriteToLog("SignalRClient.Dispose - Client is already disposed");
                }
            }

            base.Dispose();
        }

        #region Private methods

        private void MessageVerified(string guid)
        {
            Netmessage removedMessage;
            if (!nonVerifiedMessages.TryRemove(guid, out removedMessage))
            {
                WriteToLog("Unable to remove message with guid '{0}' from non verified message list", guid);
            }
        }

        private void ProcessNetmessageQueue(object cancellationToken)
        {
            WriteToLog("ProcessNetmessageQueue - Started");

            var utcTime = DateTime.UtcNow.Subtract(new TimeSpan(0, 3, 0));

            // When processing start, first resend all non verified messages
            foreach (var nonVerifiedMessage in nonVerifiedMessages.Values)
            {
                // Check for timeout (3 minutes)
                if (utcTime > nonVerifiedMessage.Created)
                {
                    messageQueue.Add(nonVerifiedMessage);
                }
            }
            nonVerifiedMessages.Clear();

            // Start processing netmessages. This will continue untill the CancellationToken has been triggered.
            // Method is being handled by ThreadPool, so no worries about it blocking something.
            var token = (CancellationToken)cancellationToken;
            while (!token.IsCancellationRequested)
            {
                try
                {
                    var netmessage = messageQueue.Take(token);
                    if (netmessage == null) 
                        continue;

                    WriteToLog("ProcessNetmessageQueue - Processing message: {0}", netmessage.MessageType);

                    if (!IsConnected() || !IsAuthenticated)
                    {
                        WriteToLog("ProcessNetmessageQueue - Not connected or authenticated. Requeueing message: {0}", netmessage.MessageType);
                        messageQueue.Add(netmessage, token);
                    }
                    else
                    {
                        lock(this.clientLock)
                        {
                            TransmitMessage(netmessage);
                        }
                    }
                }
                catch (OperationCanceledException)
                {
                    break;
                }
            }

            WriteToLog("ProcessNetmessageQueue - Stopped");
        }

        public override void TransmitMessage(Netmessage message)
        {
            if (message.IsVerifyNeeded())
            {
                if (message.Guid.IsNullOrWhiteSpace())
                    message.Guid = GuidUtil.CreateGuid();

                if (message.Created == DateTime.MinValue)
                    message.Created = DateTime.UtcNow;

                nonVerifiedMessages.TryAdd(message.Guid, message);
            }

            if (IsConnected())
            {
                message.AddTraceMessage("Sending message from SignalRClient ({0}) to CometServer", appIdentifier);

                // Create JSON
                var settings = new JsonSerializerSettings();
                settings.NullValueHandling = NullValueHandling.Ignore;
                string netmessage = JsonConvert.SerializeObject(message, new Formatting(), settings);

                WriteToLog("Sending message to server: {0}", message.MessageType);

                InvokeMessage(netmessage);
            }
            else
            {
                WriteToLog("SignalRClient.TransmitMessage - Trying to send message while not connected");

                if (message.MessageType != NetmessageType.Pong)
                {
                    message.AddTraceMessage("Sending message from SignalRClient ({0}) - Trying to send message while not connected", appIdentifier);

                    using (var transaction = new Transaction(IsolationLevel.ReadUncommitted, "SignalRClient-TransmitMessage-" + message.MessageType))
                    {
                        try
                        {
                            // Save netmessage in database so it can be picked up by SupportTools even if the connection is down
                            var netmessageEntity = NetmessageHelper.CreateNetmessageEntityFromModel(message);
                            netmessageEntity.AddToTransaction(transaction);
                            netmessageEntity.Save();

                            transaction.Commit();
                        }
                        catch (Exception ex)
                        {
                            WriteToLog("TransmitMessage - {0}", ex.Message);
                            transaction.Rollback();
                        }
                    }
                }
            }
        }

        #endregion

        private string GetHubConnectionId()
        {
            lock(this.clientLock)
            {
                return (this.hubConnection != null) ? this.hubConnection.ConnectionId : string.Empty;
            }
        }

        private async void InvokeAuthenticate(long timestamp, string identifier, string hash)
        {
            try
            {
                if (this.hubProxy != null)
                {
                    await this.hubProxy.Invoke("Authenticate", timestamp, identifier, hash);
                }
            }
            catch (Exception ex)
            {
                WriteToLog("SignalRClient.Authenticate - Failed to invoke authenticate message: {0}", ex.Message);
            }
        }

        private async void InvokeMessage(string data)
        {
            try
            {
                if (this.hubProxy != null)
                {
                    await this.hubProxy.Invoke<string>("ReceiveMessage", data);
                }
            }
            catch (Exception ex)
            {
                WriteToLog("SignalRClient.TransmitMessage - Exception: " + ex.ProcessStackTrace(true));
            }
        }
    }
}
