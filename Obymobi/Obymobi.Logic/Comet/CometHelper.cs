﻿using System.Collections.Generic;
using System;
using System.Data;
using System.Threading.Tasks;
using Dionysos;
using Newtonsoft.Json;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Enums;
using Obymobi.Logic.Comet.Interfaces;
using Obymobi.Logic.Comet.Netmessages;
using System.Diagnostics;
using Newtonsoft.Json.Serialization;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Routing;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Logic.Comet
{
	/// <summary>
	/// CometHelper class
	/// </summary>
	public class CometHelper
	{
        /// <summary>
		/// CometHelperResult enums
		/// </summary>
		public enum CometHelperResult
		{
			/// <summary>
			/// Comet provider is not set
			/// </summary>
			CometProviderNotSet = 200,
			VerifyNetMessageNoTerminalOrClientSetForDevice = 201,

            BothClientAndTerminalIdAreSet = 300,
		}

		#region Singleton

		public static readonly CometHelper Instance;

		/// <summary>
		/// Constructs an instance of the Global class if the instance has not been initialized yet
		/// </summary>
		static CometHelper()
		{
			if (CometHelper.Instance == null)
			{
				// first create the instance
				CometHelper.Instance = new CometHelper();
			}
		}

		#endregion

		#region Static methods

		/// <summary>
		/// Sets and initializes the comet provider.
		/// </summary>
		/// <param name="cometProvider">The comet provider.</param>
		public static void SetCometProvider(ICometProvider cometProvider)
		{
			CometHelper.Instance.CometProvider = cometProvider;
            CometHelper.Instance.CometProvider.Initialize();
		}

		public static ICometProvider GetCometProvider()
		{
			return CometHelper.Instance.CometProvider;
		}
		
		public static bool IsCometProviderSet()
		{
			return (CometHelper.Instance.cometProvider != null);
		}

        #endregion

        #region Private Methods

        /// <summary>
		/// Gets a value indicating whether [validate comet provider set].
		/// </summary>
		/// <value>
		/// 	<c>true</c> if [validate comet provider set]; otherwise, <c>false</c>.
		/// </value>
		private bool ValidateCometProviderSet
		{
			get
			{
				if (this.cometProvider == null)
				{
					Debug.WriteLine("Comet Call Requested - No CometProvider set");
					throw new ObymobiException(OrderSaveResult.CometProviderNotSet);
				}

				return true;
			}
		}

		public void SendMessage(Netmessage message, ITransaction transaction = null)
		{
            // Message list
            List<Netmessage> messageToBeSend = new List<Netmessage>();

		    message.AddTraceMessage("Sending message from CometHelper");

			// Save message
			if (message.SaveToDatabase)
			{
				// Set to false, so we dont save it again when calling SendNetmessageToClient
				message.SaveToDatabase = false;

			    bool usingOwnTransaction = (transaction == null);
                if (transaction == null)
                {
                    transaction = new Transaction(IsolationLevel.ReadUncommitted, "NetmessageTransaction-" + message.MessageType);
                }

                try
                {
                    if (message.ReceiverTerminalId > 0 || message.ReceiverClientId > 0)
                    {
                        message.Guid = GuidUtil.CreateGuid();

                        DeviceEntity deviceEntity = null;

                        if (message.ReceiverTerminalId > 0)
                        {
                            deviceEntity = DeviceHelper.GetDeviceEntityByTerminalId(message.ReceiverTerminalId);
                        }
                        else if (message.ReceiverClientId > 0)
                        {
                            deviceEntity = DeviceHelper.GetDeviceEntityByClientId(message.ReceiverClientId);
                        }

                        message.ReceiverIdentifier = deviceEntity != null ? deviceEntity.Identifier : null;

                        NetmessageEntity entity = NetmessageHelper.CreateNetmessageEntityFromModel(message);
                        entity.AddToTransaction(transaction);

                        try
                        {
                            if (entity.Save())
                                message.NetmessageId = entity.NetmessageId;
                        }
                        catch (ObymobiException ex)
                        {
                            // Exception is thrown in NetmessageValidator when another Netmessage already exists in the database with the same data
                            // Overwrite the message GUID with the saved GUID from database
                            message.Guid = entity.Guid;
                            message.NetmessageId = entity.NetmessageId;
                        }

                        messageToBeSend.Add(message);
                    }
                    else if (message.ReceiverCompanyId > 0 || message.ReceiverDeliverypointId > 0)
                    {
                        // Build filter
                        PredicateExpression filter = new PredicateExpression();
                        if (message.ReceiverCompanyId > 0)
                        {
                            PredicateExpression subFilter = new PredicateExpression();
                            subFilter.Add(TerminalFields.CompanyId == message.ReceiverCompanyId);
                            subFilter.AddWithOr(ClientFields.CompanyId == message.ReceiverCompanyId);
                            filter.Add(subFilter);
                        }
                        else if (message.ReceiverDeliverypointId > 0)
                        {
                            filter.Add(ClientFields.DeliverypointId == message.ReceiverDeliverypointId);
                        }

                        RelationCollection relation = new RelationCollection();
                        relation.Add(DeviceEntityBase.Relations.ClientEntityUsingDeviceId, JoinHint.Left);
                        relation.Add(DeviceEntityBase.Relations.TerminalEntityUsingDeviceId, JoinHint.Left);

                        PrefetchPath prefetch = new PrefetchPath(EntityType.DeviceEntity);
                        prefetch.Add(DeviceEntityBase.PrefetchPathClientCollection);
                        prefetch.Add(DeviceEntityBase.PrefetchPathTerminalCollection);

                        DeviceCollection deviceCollection = new DeviceCollection();
                        deviceCollection.AddToTransaction(transaction);
                        deviceCollection.GetMulti(filter, 0, null, relation, prefetch);

                        foreach (DeviceEntity deviceEntity in deviceCollection)
                        {
                            // Clone message
                            Netmessage clone = message.ConvertTo<Netmessage>(false);
                            clone.Guid = GuidUtil.CreateGuid();
                            clone.ReceiverIdentifier = deviceEntity.Identifier;

                            if (deviceEntity.ClientEntitySingle != null)
                                clone.ReceiverClientId = deviceEntity.ClientEntitySingle.ClientId;
                            else if (deviceEntity.TerminalEntitySingle != null)
                                clone.ReceiverTerminalId = deviceEntity.TerminalEntitySingle.TerminalId;
                            else
                                continue;

                            NetmessageEntity entity = NetmessageHelper.CreateNetmessageEntityFromModel(clone);
                            entity.AddToTransaction(transaction);

                            try
                            {
                                if (entity.Save())
                                    clone.NetmessageId = entity.NetmessageId;
                            }
                            catch (ObymobiException)
                            {
                                // Exception is thrown in NetmessageValidator when another Netmessage already exists in the database with the same data
                                // Overwrite the message GUID with the saved GUID from database
                                clone.Guid = entity.Guid;
                                clone.NetmessageId = entity.NetmessageId;
                            }

                            // Add to list of messages to be send
                            messageToBeSend.Add(clone);
                        }
                    }
                    else if (!message.ReceiverIdentifier.IsNullOrWhiteSpace())
                    {
                        message.Guid = GuidUtil.CreateGuid();

                        NetmessageEntity entity = NetmessageHelper.CreateNetmessageEntityFromModel(message);
                        entity.AddToTransaction(transaction);

                        try
                        {
                            if (entity.Save())
                                message.NetmessageId = entity.NetmessageId;
                        }
                        catch (ObymobiException ex)
                        {
                            // Exception is thrown in NetmessageValidator when another Netmessage already exists in the database with the same data
                            // Overwrite the message GUID with the saved GUID from database
                            message.Guid = entity.Guid;
                            message.NetmessageId = entity.NetmessageId;
                        }

                        messageToBeSend.Add(message);
                    }

                    // If we are using our own transaction, commit it here
                    if (usingOwnTransaction)
                        transaction.Commit();
                }
                catch (Exception)
                {
                    // If we are using our own transaction, roll it back here
                    transaction.Rollback();
                }
                finally
                {
                    // If we are using our own transaction, dispose it here
                    if (usingOwnTransaction)
                        transaction.Dispose();
                }
			}
			else
			{
			    messageToBeSend.Add(message);
			}

            // Send message(s)
            if (CometHelper.Instance.ValidateCometProviderSet)
            {
                Task.Factory.StartNew(() =>
                    {
                        foreach (Netmessage netmessage in messageToBeSend)
                        {
                            Debug.WriteLine("INFORMATION: Sending message {1} with Guid '{0}'", netmessage.Guid, message.MessageType);
                            try
                            {
                                this.CometProvider.SendMessage(netmessage);
                            }
                            catch (ObymobiException ex)
                            {
                                RequestlogEntity requestlog = new RequestlogEntity();
                                requestlog.MethodName = "CometHelper.SendMessage";
                                requestlog.Parameter1 = netmessage.ToString();
                                requestlog.ErrorMessage = ex.Message;
                                requestlog.ErrorStackTrace = ex.StackTrace;
                                requestlog.Save();
                            }
                        }
                    });
            }
		}

		#endregion

        #region Public Netmessage Methods

		/// <summary>
		/// Announces the new message.
		/// </summary>
		/// <param name="companyId">The company id.</param>
		/// <param name="customerId">The customer id.</param>
		/// <param name="clientId">The client id.</param>
		/// <param name="deliverypointId">The deliverypoint id.</param>
		public static void AnnounceNewMessage(Message message)
		{
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            NetmessageAnnounceNewMessage netmessage = new NetmessageAnnounceNewMessage();
            netmessage.SenderCompanyId = message.CompanyId;
            netmessage.FieldValue1 = JsonConvert.SerializeObject(message, new Formatting(), settings);

            if (message.ClientId > 0)
				netmessage.ReceiverClientId = message.ClientId;
			if (message.DeliverypointId > 0)
                netmessage.ReceiverDeliverypointId = message.DeliverypointId;

			CometHelper.Instance.SendMessage(netmessage);
		}

		/// <summary>
		/// Sends the order status to client
		/// </summary>
		/// <param name="companyId">The company id.</param>
		/// <param name="orderId">The order id.</param>
		/// <param name="orderitemIds"></param>
		/// <param name="status">The status.</param>
		/// <param name="clientId">The receiving client id.</param>
		/// <param name="terminalId">The sending terminal id.</param>
		public static void SendOrderStatus(int companyId, int orderId, string orderitemIds, OrderStatus status, int clientId, int terminalId)
		{
			NetmessageOrderStatusUpdated message = new NetmessageOrderStatusUpdated();
			message.SenderCompanyId = companyId;
			message.OrderId = orderId;
			message.OrderItemIds = orderitemIds;
			message.OrderStatus = status;

			if (terminalId > 0)
				message.SenderTerminalId = terminalId;

			if (clientId > 0)
				message.ReceiverClientId = clientId;

			CometHelper.Instance.SendMessage(message);
		}

		/// <summary>
		/// Sends the routestephandler status to terminals
		/// </summary>
		/// <param name="companyId">The company id.</param>
		/// <param name="orderId">The order id.</param>
		/// <param name="routestephandlerId">The routestephandler id.</param>
		/// <param name="status">The status.</param>
		/// <param name="clientId">The client id.</param>
		/// <param name="terminalId">The terminal id.</param>
		public static void SendRoutestephandlerStatus(int companyId, int orderId, int routestephandlerId, OrderRoutestephandlerStatus status, int clientId, int terminalId)
		{
			NetmessageRoutestephandlerStatusUpdated message = new NetmessageRoutestephandlerStatusUpdated();
			message.OrderId = orderId;
			message.RoutestephandlerId = routestephandlerId;
			message.OrderRoutestephandlerStatus = status;

			if (companyId > 0)
				message.SenderCompanyId = companyId;

			// Send to client
//			if (clientId > 0)
//			{
//				message.ReceiverClientId = clientId;
//				message.Guid = "";
//				Instance.SendMessage(message);
//			}

			// Send to terminal
			if (terminalId > 0)
			{
				message.ReceiverTerminalId = terminalId;
				message.Guid = "";
				CometHelper.Instance.SendMessage(message);
			}
		}

		/// <summary>
		/// Sends the social message.
		/// </summary>
		/// <param name="companyId">The company id.</param>
		public static void SendSocialMessage(int companyId)
		{
			NetmessageNewSocialMediaMessage message = new NetmessageNewSocialMediaMessage();
			message.ReceiverCompanyId = companyId;

			CometHelper.Instance.SendMessage(message);
		}

		/// <summary>
		/// Verifies a netmessage
		/// </summary>
		/// <param name="clientId">Id of the receiving client</param>
		/// <param name="messageGuid">Message GUID</param>
		public static void VerifyNetmessage(DeviceEntity device, string messageGuid, bool isCompleted, string errorMessage, string trace = "")
		{
			NetmessageCollection netmessages = new NetmessageCollection();
            PredicateExpression filter = new PredicateExpression(NetmessageFields.Guid == messageGuid);
			netmessages.GetMulti(filter);

		    if (isCompleted)
		        trace += string.Format("[{0}] Netmessage completed\n", DateTimeUtil.GetTimeString());
            else
                trace += string.Format("[{0}] Netmessage verified\n", DateTimeUtil.GetTimeString());

			foreach (NetmessageEntity netmessage in netmessages)
			{
                using (Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "CometHelper-VerifyNetmessage-" + messageGuid))
			    {
			        try
			        {
			            netmessage.AddToTransaction(transaction);
			            netmessage.StatusAsEnum = (isCompleted ? NetmessageStatus.Completed : NetmessageStatus.Delivered);
			            netmessage.ErrorMessage = errorMessage;
			            netmessage.Save();

                        transaction.Commit();
			        }
			        catch (Exception ex)
			        {
                        Debug.WriteLine("CometHelper.VerifyNetmessage - Failed to verify netmessage - {0}", ex.Message);
			            transaction.Rollback();
			        }
			    }
			}
		}

		/// <summary>
		/// Broadcasts the deliverypoint message.
		/// </summary>
		/// <param name="deliverypointgroupId">The deliverypointgroup id.</param>
		/// <param name="ttl">The TTL.</param>
		/// <param name="message">The message.</param>
		/// <param name="deliverypointId">The deliverypoint id.</param>
		public static void BroadcastDeliverypointMessage(int ttl, string message, int deliverypointId, int companyId = 0)
		{
			NetmessageBroadcastMessage netmsg = new NetmessageBroadcastMessage();
			netmsg.TimeToLive = ttl;
			netmsg.BroadcastMessage = message;

			if (deliverypointId > 0)
				netmsg.ReceiverDeliverypointId = deliverypointId;

			if (companyId > 0)
				netmsg.ReceiverCompanyId = companyId;

			CometHelper.Instance.SendMessage(netmsg);
		}

		/// <summary>
		/// Send a checkout notification
		/// </summary>
		/// <param name="companyId">The deliverypoint id.</param>
		public static void SetPmsCheckedOut(int deliverypointId)
		{
			NetmessageSetPmsCheckedOut message = new NetmessageSetPmsCheckedOut();
			message.ReceiverDeliverypointId = deliverypointId;

			CometHelper.Instance.SendMessage(message);
		}

		/// <summary>
		/// Send a checkout notification
		/// </summary>
		/// <param name="companyId">The deliverypoint id.</param>
		public static void SetPmsCheckedIn(int deliverypointId, string guestInformation)
		{
			NetmessageSetPmsCheckedIn message = new NetmessageSetPmsCheckedIn();
			message.ReceiverDeliverypointId = deliverypointId;
			message.GuestInformationData = guestInformation;

			CometHelper.Instance.SendMessage(message);
		}

		/// <summary>
		/// Request for Folio information of PMS via OSS
		/// </summary>
		/// <param name="companyId">The deliverypoint id.</param>
		public static void SetPmsFolio(int deliverypointId, string folio)
		{
			// This is sort of a special case, the request is done for a Deliverypoint but must be handled by 
			// a terminal. 
			NetmessageSetPmsFolio message = new NetmessageSetPmsFolio();
			message.ReceiverDeliverypointId = deliverypointId;
			message.FolioData = folio;

			CometHelper.Instance.SendMessage(message);
		}

		/// <summary>
		/// Request for Folio information of PMS via OSS
		/// </summary>
		/// <param name="deliverypointId">The deliverypoint id.</param>
		/// <param name="guestInformation">The guest information.</param>
		public static void SetPmsGuestInformation(int deliverypointId, string guestInformation)
		{
			// This is sort of a special case, the request is done for a Deliverypoint but must be handled by 
			// a terminal. 
			NetmessageSetPmsGuestInformation message = new NetmessageSetPmsGuestInformation();
			message.ReceiverDeliverypointId = deliverypointId;
			message.GuestInformationData = guestInformation;

			CometHelper.Instance.SendMessage(message);
		}

		/// <summary>
		/// Request for Folio information of PMS via OSS
		/// </summary>
		/// <param name="companyId">The deliverypoint id.</param>
		public static void GetPmsFolio(int terminalId, string deliverypointNumber)
		{
			// This is sort of a special case, the request is done for a Deliverypoint but must be handled by 
			// a terminal. 
			NetmessageGetPmsFolio message = new NetmessageGetPmsFolio();
			message.ReceiverTerminalId = terminalId;
			message.DeliverypointNumber = deliverypointNumber;

			CometHelper.Instance.SendMessage(message);
		}

		/// <summary>
		/// Request for Folio information of PMS via OSS
		/// </summary>
		/// <param name="companyId">The deliverypoint id.</param>
		public static void GetPmsGuestInformation(int terminalId, string deliverypointNumber)
		{
			// This is sort of a special case, the request is done for a Deliverypoint but must be handled by 
			// a terminal. 
			NetmessageGetPmsGuestInformation message = new NetmessageGetPmsGuestInformation();
			message.ReceiverTerminalId = terminalId;
			message.DeliverypointNumber = deliverypointNumber;

			CometHelper.Instance.SendMessage(message);
		}

		/// <summary>
		/// Request for Folio information of PMS via OSS
		/// </summary>
		/// <param name="companyId">The deliverypoint id.</param>
		public static void GetPmsExpressCheckout(int terminalId, string deliverypointNumber, decimal balance)
		{
			// This is sort of a special case, the request is done for a Deliverypoint but must be handled by 
			// a terminal. 
			NetmessageGetPmsExpressCheckout message = new NetmessageGetPmsExpressCheckout();
			message.ReceiverTerminalId = terminalId;
			message.DeliverypointNumber = deliverypointNumber;
			message.Balance = balance;

			CometHelper.Instance.SendMessage(message);
		}

		/// <summary>
		/// Send a checkout notification
		/// </summary>
		/// <param name="companyId">The deliverypoint id.</param>
		public static void SetPmsExpressCheckedOut(int deliverypointId, string checkoutInfo)
		{
		    NetmessageSetPmsExpressCheckedOut message = new NetmessageSetPmsExpressCheckedOut();
			message.ReceiverDeliverypointId = deliverypointId;
			message.CheckoutInformation = checkoutInfo;

			CometHelper.Instance.SendMessage(message);
		}

        /// <summary>
        /// Sets the wakeUpStatus from the client via OSS through the Webservice
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="deliverypointNumber"></param>
        /// <param name="Xml"></param>
	    public static void SetPmsWakeUp(int deliverypointId, string wakeUpStatus)
        {
            NetmessageSetPmsWakeUp message = new NetmessageSetPmsWakeUp();
            message.ReceiverDeliverypointId = deliverypointId;
            message.WakeUpStatusData = wakeUpStatus;

            CometHelper.Instance.SendMessage(message);
        }

        /// <summary>
        /// Clears the wakeUpStatus from the client via OSS through the Webservice
        /// </summary>
        /// <param name="terminalId"></param>
        /// <param name="deliverypointNumber"></param>
        /// <param name="Xml"></param>
        public static void ClearPmsWakeUp(int deliverypointId, string wakeUpStatus)
        {
            NetmessageClearPmsWakeUp message = new NetmessageClearPmsWakeUp();
            message.ReceiverDeliverypointId = deliverypointId;
            message.WakeUpStatusData = wakeUpStatus;

            CometHelper.Instance.SendMessage(message);
        }

	    /// <summary>
		/// Set forward terminal ID for a console
		/// </summary>
		/// <param name="companyId">The deliverypoint id.</param>
		public static void SetForwardTerminal(int terminalId, int forwardToTerminalId)
		{
			// This is sort of a special case, the request is done for a Deliverypoint but must be handled by 
			// a terminal. 
			NetmessageSetForwardedTerminal message = new NetmessageSetForwardedTerminal();
			message.ReceiverTerminalId = terminalId;
			message.ForwardTerminalId = forwardToTerminalId;

			CometHelper.Instance.SendMessage(message);
		}

		/// <summary>
		/// Set the Operation Mode for a Client
		/// </summary>
		/// <param name="clientId">The client id.</param>
		/// <param name="operationMode">The operation mode.</param>
		public static void SetClientOperationMode(int clientId, ClientOperationMode operationMode, ITransaction transaction = null)
		{
			NetmessageSetClientOperationMode message = new NetmessageSetClientOperationMode();
			message.ReceiverClientId = clientId;
			message.ClientOperationMode = (int)operationMode;

			CometHelper.Instance.SendMessage(message, transaction);
		}

        public static void SetDeliverypointId(int clientId, DeliverypointEntity deliverypoint)
        {
            CometHelper.SetDeliverypointId(clientId, deliverypoint.DeliverypointId, deliverypoint.DeliverypointgroupId, deliverypoint.Number);
        }

        /// <summary>
        /// Sets the deliverypoint id.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="deliverypointId">The deliverypoint id.</param>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        public static void SetDeliverypointId(int clientId, int deliverypointId, int deliverypointgroupId, string deliverypointNumber = "", ITransaction transaction = null)
        {
            NetmessageSetDeliverypointId message = new NetmessageSetDeliverypointId();
            message.ReceiverClientId = clientId;
            message.DeliverypointId = deliverypointId;
            message.DeliverypointgroupId = deliverypointgroupId;

            if (deliverypointNumber.IsNullOrWhiteSpace())
                deliverypointNumber = new DeliverypointEntity(deliverypointId).Number;
            message.DeliverypointNumber = deliverypointNumber;            

            CometHelper.Instance.SendMessage(message, transaction);
        }

        /// <summary>
        /// Issuess a ClientCommand
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="command">The command.</param>
        public static void ClientCommand(int clientId, ClientCommand command, bool toSupportTools)
        {
            NetmessageClientCommand message = new NetmessageClientCommand();
            message.ReceiverClientId = clientId;
            message.ClientCommand = (int)command;
            message.ToSupportTools = toSupportTools;            

            CometHelper.Instance.SendMessage(message);
        }        

	    /// <summary>
        /// Issuess a TerminalCommand
        /// </summary>
        /// <param name="terminalId">The terminal id.</param>
        /// <param name="command">The command.</param>
        public static void TerminalCommand(int terminalId, TerminalCommand command, bool toSupportTools)
        {
            NetmessageTerminalCommand message = new NetmessageTerminalCommand();
            message.ReceiverTerminalId = terminalId;
            message.TerminalCommand = (int)command;
            message.ToSupportTools = toSupportTools;

            CometHelper.Instance.SendMessage(message);
        }

		/// <summary>
		/// Pushes the external order placed event.
		/// </summary>
		/// <param name="companyId">The company id.</param>
		/// <param name="order">The order.</param>
		public static void PushExternalOrderPlacedEvent(int companyId, Order order)
		{
			DeliverypointgroupEntity d = DeliverypointgroupHelper.GetDeliverypointgroupByDeliverypointId(order.DeliverypointId);
			CompanyEntity c = CompanyHelper.GetCompanyEntityByCompanyId(order.CompanyId, false);
			order.DeliverypointName = (d == null || d.DeliverypointCaption.IsNullOrWhiteSpace()) ? "Deliverypoint" : d.DeliverypointCaption;
			order.Created = DateTime.UtcNow.UtcToLocalTime().ToString("dd-MM-yyyy HH:mm:ss");
			order.CompanyName = (c == null) ? "Unknown" : c.Name;

			NetmessageNewEventExternalListener message = new NetmessageNewEventExternalListener();
			message.EventType = CometExternalEventType.NewOrder;
			message.CompanyId = companyId;
			message.FieldValue3 = JsonConvert.SerializeObject(order);

			CometHelper.Instance.SendMessage(message);
		}

        /// <summary>
        /// Sends the survey result
        /// </summary>
        /// <param name="companyId">The company id.</param>
        public static void SendSurveyResultMessage(int terminalId)
        {
            NetmessageNewSurveyResult message = new NetmessageNewSurveyResult();
            message.ReceiverTerminalId = terminalId;

            CometHelper.Instance.SendMessage(message);
        }

        public static void DeviceCommandExecuteClient(int clientId, string command)
        {
            NetmessageDeviceCommandExecute message = new NetmessageDeviceCommandExecute();
            message.ReceiverClientId = clientId;
            message.Command = command;

            CometHelper.Instance.SendMessage(message);
        }

        public static void DeviceCommandExecuteTerminal(int terminalId, string command)
        {
            NetmessageDeviceCommandExecute message = new NetmessageDeviceCommandExecute();
            message.ReceiverTerminalId = terminalId;
            message.Command = command;

            CometHelper.Instance.SendMessage(message);
        }

        public static void SetCloudEnvironment(int clientId, int terminalId, CloudEnvironment environment, CloudEnvironment manual = CloudEnvironment.ProductionPrimary)
        {
            NetmessageSetCloudEnvironment message = new NetmessageSetCloudEnvironment();
            if (clientId > 0 && terminalId > 0)
            {
                throw new ObymobiException(CometHelperResult.BothClientAndTerminalIdAreSet, "Both the clientId and terminalId have a value, one of them needs to be '0'.");   
            }
            
            if (clientId > 0)
                message.ReceiverClientId = clientId;
            else if (terminalId > 0)
                message.ReceiverTerminalId = terminalId;

            message.CloudEnvironment = environment;
            message.ManualUrl = manual;

            CometHelper.Instance.SendMessage(message);
        }

        public static void SendAgentCommandResponse(string macAddress, string commandResponse)
        {
            NetmessageAgentCommandResponse message = new NetmessageAgentCommandResponse();
            message.SenderMacAddress = macAddress;
            message.ReponseMessage = commandResponse;

            CometHelper.Instance.SendMessage(message);
        }

	    public static void SendDailyOrderReset(int clientId)
	    {
	        NetmessageClientCommand message = new NetmessageClientCommand();
            message.ClientCommand = (int)Enums.ClientCommand.DailyOrderReset;
	        message.ReceiverClientId = clientId;

            CometHelper.Instance.SendMessage(message);
	    }

        public static void DeviceUnlinked(int terminalId, string macAddress)
        {
            NetmessageDeviceUnlink message = new NetmessageDeviceUnlink(macAddress);
            message.ReceiverIdentifier = macAddress;
            message.ReceiverTerminalId = terminalId;

            CometHelper.Instance.SendMessage(message);
        }

        public static void SetConsoleMasterTab(int terminalId, bool hasMasterTab)
        {
            NetmessageSetMasterTab message = new NetmessageSetMasterTab();
            message.ReceiverTerminalId = terminalId;
            message.MasterTabEnabled = hasMasterTab;

            CometHelper.Instance.SendMessage(message);
        }

        public static void SwitchCometHandler(int companyId, ClientCommunicationMethod handlerType)
        {
            NetmessageSwitchCometHandler message = new NetmessageSwitchCometHandler();
            message.ReceiverCompanyId = companyId;
            message.Handler = handlerType;

            CometHelper.Instance.SendMessage(message);
        }

	    public static void MenuUpdated(int companyId, MenuUpdated model)
	    {
            NetmessageMenuUpdated message = new NetmessageMenuUpdated();
	        message.ReceiverCompanyId = companyId;
	        message.MenuUpdatedData = model.ToJson();

	        CometHelper.Instance.SendMessage(message);
	    }

	    public static void SendClearMessages(int clientId)
	    {
            NetmessageClientCommand message = new NetmessageClientCommand();
            message.ClientCommand = (int)Enums.ClientCommand.ClearSendMessages;
            message.ReceiverClientId = clientId;

            CometHelper.Instance.SendMessage(message);
	    }

        public static void SwitchTabCommand(int identifier, string uiTabId)
        {
            NetmessageSwitchTab message = new NetmessageSwitchTab();
            message.Guid = GuidUtil.CreateGuid();
            message.UITabId = uiTabId;

            //message.ReceiverClientId = clientId;
            //message.UITabId = uiTabId.ToString();

            CometHelper.Instance.SendMessage(message);
        }

	    public static void PushModelToTerminal(int terminalId, ModelType type, ModelBase model)
	    {
            CometHelper.PushModelToTerminal(terminalId, type, model.ToJson());
	    }

        public static void PushModelToClient(int clientId, ModelType type, ModelBase model)
        {
            CometHelper.PushModelToClient(clientId, type, model.ToJson());
        }

        public static void PushModelToTerminal(int terminalId, ModelType type, string json)
        {
            NetmessageSendJsonObject message = new NetmessageSendJsonObject();
            message.ReceiverTerminalId = terminalId;

            CometHelper.PushModel(message, type, json);
        }

        public static void PushModelToClient(int clientId, ModelType type, string json)
        {
            NetmessageSendJsonObject message = new NetmessageSendJsonObject();
            message.ReceiverClientId = clientId;

            CometHelper.PushModel(message, type, json);
        }

	    private static void PushModel(NetmessageSendJsonObject message, ModelType type, string json)
	    {
            message.ModelType = (int)type;
            message.JsonObject = json;
            message.SaveToDatabase = true;

            CometHelper.Instance.SendMessage(message); 
	    }

        public static void RefreshContent(int clientId)
        {
            NetmessageRefreshContent netmessage = new NetmessageRefreshContent(); 
            netmessage.ReceiverClientId = clientId;            

            CometHelper.Instance.SendMessage(netmessage);
        }

		public static void PmsTerminalStatusUpdated(int terminalId, bool isOnline, string lastSyncAsString)
        {
            NetmessagePmsTerminalStatusUpdated message = new NetmessagePmsTerminalStatusUpdated();
            message.ReceiverTerminalId = terminalId;
            message.IsOnline = isOnline;
            message.LastSyncAsString = lastSyncAsString;

            CometHelper.Instance.SendMessage(message);
        }

        public static void UpdateDeviceToken(DeviceTokenTaskEntity deviceTokenTaskEntity, ITransaction transaction)
        {
            NetmessageUpdateDeviceToken message = new NetmessageUpdateDeviceToken();
            message.ReceiverIdentifier = deviceTokenTaskEntity.DeviceEntity.Identifier;
            message.DeviceToken = deviceTokenTaskEntity.DecryptedToken;
            message.DeviceTokenTaskId = deviceTokenTaskEntity.DeviceTokenTaskId;
            message.DeviceTokenAction = (int)deviceTokenTaskEntity.Action;

            CometHelper.Instance.SendMessage(message, transaction);
        }

        public static void UpdateDeviceToken(DeviceEntity deviceEntity, DeviceTokenAction deviceTokenAction)
        {
            NetmessageUpdateDeviceToken message = new NetmessageUpdateDeviceToken();
            message.ReceiverIdentifier = deviceEntity.Identifier;
            message.DeviceToken = deviceEntity.DecryptedToken;
            message.DeviceTokenAction = (int)deviceTokenAction;

            CometHelper.Instance.SendMessage(message);
        }

        public static void ClearCompanyCredentials(string macAddress)
        {
            NetmessageClearCompanyCredentials message = new NetmessageClearCompanyCredentials();
            message.ReceiverIdentifier = macAddress;

            CometHelper.Instance.SendMessage(message);
        }

        #endregion

        #region Obsolete methods

        /// <summary>
        /// Verifies the netmessage.
        /// </summary>
        /// <param name="messageId">The message id.</param>
        [Obsolete("VerifyNetmessage(int messageId) is desprecated, use VerifyNetmessage(int clientId, string messageGuid) instead.")]
		public static void VerifyNetmessage(int messageId)
		{
			NetmessageCollection netmessages = new NetmessageCollection();
			netmessages.GetMulti(new PredicateExpression(NetmessageFields.NetmessageId == messageId));
			foreach (NetmessageEntity netmessage in netmessages)
			{
                using (Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "CometHelper-VerifyNetmessage-" + messageId))
                {
                    try
                    {
                        netmessage.AddToTransaction(transaction);
                        netmessage.StatusAsEnum = NetmessageStatus.Delivered;
                        netmessage.Save();

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine("CometHelper.VerifyNetmessage - Failed to verify netmessage - {0}", ex.Message);
                        transaction.Rollback();
                    }
                }
			}
		}

		/// <summary>
		/// Get a list of not submitted netmessages for a specific customer
		/// </summary>
		/// <param name="customerId"></param>
		/// <param name="clientId"></param>
		/// <param name="companyId"></param>
		/// <returns></returns>
		[Obsolete("Use NetmessageHelper.GetNetmessagesForClient instead")]
		public static List<Netmessage> GetNetmessagesForCustomer(int customerId, int clientId, int companyId)
		{
			List<Netmessage> output = new List<Netmessage>();

			PredicateExpression masterFilter = new PredicateExpression();
			PredicateExpression customerFilter = new PredicateExpression();
			if (customerId > 0)
				customerFilter.Add(NetmessageFields.ReceiverCustomerId == customerId);
			if (clientId > 0)
				customerFilter.Add(NetmessageFields.ReceiverClientId == clientId);
			customerFilter.Add(NetmessageFields.Status == (int)NetmessageStatus.WaitingForVerification);
			masterFilter.Add(customerFilter);

			if (companyId > 0)
			{
                PredicateExpression companyFilter = new PredicateExpression();
				companyFilter.Add(NetmessageFields.MessageType == (int)NetmessageType.BroadcastMessage);
				companyFilter.Add(NetmessageFields.SenderCompanyId == companyId);
				companyFilter.Add(NetmessageFields.Status == (int)NetmessageStatus.WaitingForVerification);
				masterFilter.AddWithOr(companyFilter);
			}

			NetmessageCollection netmessages = new NetmessageCollection();
			netmessages.GetMulti(masterFilter);
			if (netmessages.Count > 0)
			{
				foreach (NetmessageEntity netmessage in netmessages)
				{
					output.Add(NetmessageHelper.CreateNetmessageModelFromEntity(netmessage));
				}
			}

			return output;
		}

		/// <summary>
		/// Get a list of not submitted netmessages for a deliverypoint
		/// </summary>
		/// <param name="deliverypointId"></param>
		/// <returns></returns>
		[Obsolete("Use NetmessageHelper.GetNetmessagesForClient instead", false)]
		public static List<Netmessage> GetNetmessagesForDeliverypoint(int deliverypointId)
		{
			List<Netmessage> output = new List<Netmessage>();

			PredicateExpression customerFilter = new PredicateExpression();
			customerFilter.Add(NetmessageFields.ReceiverDeliverypointId == deliverypointId);
			customerFilter.Add(NetmessageFields.Status == NetmessageStatus.WaitingForVerification);

			NetmessageCollection netmessages = new NetmessageCollection();
			netmessages.GetMulti(customerFilter);
			if (netmessages.Count > 0)
			{
				foreach (NetmessageEntity entity in netmessages)
				{
					output.Add(NetmessageHelper.CreateNetmessageModelFromEntity(entity));
				}
			}

			return output;
		}

		/// <summary>
		/// Get a list of not submitted netmessages for a terminal
		/// </summary>
		/// <param name="terminalId"></param>
		/// <returns></returns>
		[Obsolete("Use NetmessageHelper.GetNetmessagesForTerminal instead")]
		public static List<Netmessage> GetNetmessagesForTerminal(int terminalId)
		{
			List<Netmessage> output = new List<Netmessage>();

			PredicateExpression filter = new PredicateExpression();
			filter.Add(NetmessageFields.ReceiverTerminalId == terminalId);
			filter.Add(NetmessageFields.Status == NetmessageStatus.WaitingForVerification);

			NetmessageCollection netmessages = new NetmessageCollection();
			netmessages.GetMulti(filter);
			if (netmessages.Count > 0)
			{
				foreach (NetmessageEntity netmessage in netmessages)
				{
					output.Add(NetmessageHelper.CreateNetmessageModelFromEntity(netmessage));
				}
			}

			return output;
		}

		/// <summary>
		/// Processes the netmessages for deliverypoint.
		/// </summary>
		/// <param name="deliverypointId">The deliverypoint id.</param>
		[Obsolete("Not used anymore in V9.0+")]
		public static void ProcessNetmessagesForDeliverypoint(int deliverypointId)
		{
			if (CometHelper.Instance.ValidateCometProviderSet)
			{
				List<Netmessage> netmessages = CometHelper.GetNetmessagesForDeliverypoint(deliverypointId);
				foreach (Netmessage netmessage in netmessages)
				{
					CometHelper.Instance.CometProvider.SendMessage(netmessage);
				}
			}
		}

		/// <summary>
		/// Processes the netmessages for terminal.
		/// </summary>
		/// <param name="terminalId">The terminal id.</param>
		[Obsolete("Not used anymore in V9.0+")]
		public static void ProcessNetmessagesForTerminal(int terminalId)
		{
		    if (!CometHelper.Instance.ValidateCometProviderSet)
		    {
		        return;
		    }
		    List<Netmessage> netmessages = CometHelper.GetNetmessagesForTerminal(terminalId);
		    foreach (Netmessage netmessage in netmessages)
		    {
		        CometHelper.Instance.CometProvider.SendMessage(netmessage);
		    }
		}

		/// <summary>
		/// Processes the netmessages for customer.
		/// </summary>
		/// <param name="customerId">The customer id.</param>
		/// <param name="clientId">The client id.</param>
		/// <param name="companyId">The company id.</param>
		[Obsolete("Not used anymore in V9.0+")]
		public static void ProcessNetmessagesForCustomer(int customerId, int clientId, int companyId = 0)
		{
			if (CometHelper.Instance.ValidateCometProviderSet)
			{
				List<Netmessage> netmessages = CometHelper.GetNetmessagesForCustomer(customerId, clientId, companyId);
				foreach (Netmessage message in netmessages)
				{
					CometHelper.Instance.CometProvider.SendMessage(message);
				}
			}
		}

		#endregion

		#region Properties

		private ICometProvider cometProvider;

		/// <summary>
		/// Gets or sets the cometProvider
		/// </summary>
		private ICometProvider CometProvider
		{
			get
			{
				if (this.cometProvider == null)
				{
					throw new ObymobiException(CometHelperResult.CometProviderNotSet);
				}
				return this.cometProvider;
			}
			set { this.cometProvider = value; }
		}

		#endregion
	}
}