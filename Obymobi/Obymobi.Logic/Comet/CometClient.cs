﻿using System;
using System.Diagnostics;
using System.Threading;
using Obymobi.Logic.Comet.Interfaces;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.Comet
{
	public abstract class CometClient
	{
        protected readonly ReaderWriterLockSlim ConnectionLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);

		protected ICometClientEventListener ClientEventListener;

		private bool isConnected;
		private bool isConnecting;
		
        public DateTime LastPong { get; set; }

		public string IdentifierComet { get; protected set; }

		public bool IsAuthenticated { get; set; }

		protected CometClient(ICometClientEventListener eventListener)
		{
			this.ClientEventListener = eventListener;
			this.LastPong = DateTime.UtcNow;

			if (this.ClientEventListener == null)
			{
				throw new Exception("CometClient ICometClientEventListener can not be null.");
			}
		}

	    public abstract void Connect();
		public abstract void Disconnect();
		public abstract void Authenticate(String identifier, String salt);
		public abstract void Pong();
	    public abstract void SetAuthenticated(bool isAuthenticated);
	    public abstract void TransmitMessage(Netmessage message);

        public virtual void Dispose()
        {
            WriteToLog("CometClient.Dispose");
        }

        /// <summary>
        /// When authenticated returns true if LastPing has happened in the last 2 minutes.
        /// When not authenticated returns true if the LastPing has happened in the last 30 seconds
        /// </summary>
        /// <returns></returns>
        public virtual bool CheckLastPingPong()
        {
            return true;
        }

		public virtual void SendMessage(Netmessage message)
		{
			if (message.IsVerifyNeeded())
			{
				message.Created = DateTime.UtcNow;
			}
		}

		protected void HandleNetmessage(Netmessage message)
		{
            try
            {
                WriteToLog("CometClient.HandleNetmessage - Received message: {0}", message.GetMessageType());
                this.ClientEventListener.OnNetmessageReceived(message);
            }
            catch (Exception ex)
            {
                WriteToLog("CometClient.HandleNetmessage - Exception while handling message. Exception: {0} | Message: {1}", ex.Message, message);
            }
		}

		protected void WriteToLog(string log, params object[] args)
		{
		    if (this.ClientEventListener != null)
		        this.ClientEventListener.OnEventLog(log, args);
		    else
		        Debug.WriteLine(log, args);
		}

		protected void ConnectionFailed()
		{
			SetIsConnecting(false);

            if (this.ClientEventListener != null)
			    this.ClientEventListener.OnConnectFailed();
		}

		public bool IsConnected()
		{
            this.ConnectionLock.EnterReadLock();

		    bool ret;
            try
            {
                ret =  this.isConnected;
            }
            finally
            {
                this.ConnectionLock.ExitReadLock();
            }
		    return ret;
		}

		protected void SetConnected(bool connected)
		{
			this.ConnectionLock.EnterWriteLock();

		    bool connectionChanged = false;
		    try
		    {
		        if (this.isConnected != connected)
		        {
		            if (!connected)
		            {
		                IsAuthenticated = false;
		            }
   
		            this.isConnected = connected;
		            connectionChanged = true;
		        }
		    }
		    finally
		    {
                this.ConnectionLock.ExitWriteLock();    
		    }

		    if (connectionChanged && this.ClientEventListener != null)
		        this.ClientEventListener.OnConnectionChanged(connected);
		}

		public bool IsConnecting()
		{
            this.ConnectionLock.EnterReadLock();

		    bool ret;
		    try
		    {
				ret = this.isConnecting;
		    }
		    finally
		    {
                this.ConnectionLock.ExitReadLock();    
		    }
		    return ret;
		}

		protected void SetIsConnecting(bool connecting)
		{
            this.ConnectionLock.EnterWriteLock();

            try
            {
                this.isConnecting = connecting;
            }
            finally
            {
                this.ConnectionLock.ExitWriteLock();
            }
		}
	}
}
