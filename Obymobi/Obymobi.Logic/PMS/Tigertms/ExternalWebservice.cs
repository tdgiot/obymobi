﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Collections.ObjectModel;
using Obymobi.Logic.PMS.Tigertms.Messages;
using System.Text.RegularExpressions;
using System.Diagnostics;
using Obymobi.Logic.PMS.FlatWsdl;

namespace Obymobi.Logic.PMS.Tigertms
{
    /// <summary>
    /// This Webservice is waiting for Tigertms to call it, it's the listening Webservice at the OSS side.
    /// </summary>
    [ServiceBehavior(Namespace = "http://tigergenericinterface.org/", InstanceContextMode = InstanceContextMode.Single)]
    [Soap11Conformant]
    public class ExternalWebservice : IExternalWebservice
    {
        Regex wsuserkey = new Regex("<wsuserkey>(.*?)</wsuserkey>");
        private ObservableCollection<IPmsMessage> receivedMessages;

        public ExternalWebservice(ObservableCollection<IPmsMessage> receivedMessages)
        {
            this.receivedMessages = receivedMessages;
        }

        /// <summary>
        /// Receives a message from the TigerTMS system. 
        /// It's a strange name for here, but it's how it's required to be named.
        /// </summary>
        /// <param name="msg">The contents, an XML message.</param>
        /// <returns>SUCCESS or FAILED</returns>
        public string SendMessageToExternalInterface(string Msg)
        {
            string toReturn = "SUCCESS";
            try
            {
                if (TestUtil.IsPcGabriel)
                {
                    Debug.WriteLine(Msg);
                }

                // Check if there is a wsuserkey, if so, SUCCESS! 
                // We were requested by TigerTMS to respond succes even to message we don't support
                // as long as they are including a wsuserkey
                Match wsuserkeyMatch = this.wsuserkey.Match(Msg);
                if (wsuserkeyMatch != null)
                {
                    // We're fine.
                    try
                    {
                        MessageBase message = MessageBase.GetMessageFromXml(Msg);
                        this.receivedMessages.Add(message);
                    }
                    catch
                    {
                        // Absorb message, it contained a wsuserkey.
                    }
                }
                else
                {
                    toReturn = "FAILED";
                }

            }
            catch
            {
                if (TestUtil.IsPcDeveloper)
                {
                    throw;
                }
                else
                    toReturn = "FAILED";
            }

            return toReturn;
        }


    }
}
