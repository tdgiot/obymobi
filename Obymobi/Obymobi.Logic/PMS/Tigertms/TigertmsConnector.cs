﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Description;
using Obymobi.Logic.PMS.Tigertms.Messages;
using Obymobi.Logic.HelperClasses;
using System.Threading;
using System.Collections.Concurrent;
using Dionysos;
using Obymobi.Logic.Model;
using System.Xml;
using Obymobi.Logic.PMS.FlatWsdl;

namespace Obymobi.Logic.PMS.Tigertms
{
    public class TigertmsConnector : PmsConnector
    {
        public enum TigertmsConnectorResult
        {
            CallingWebserviceFailed = 200,
            CallingWebserviceReturnedFailed = 201,
            MessageSerializationFailed = 202,
            NoResponseMessageFound = 203,
            MultipleSimilairRequests = 204,
        }

        #region Fields

        // Read from settings

        private TigertmsWebservice.InterfaceMessageIn webserviceToCallTo = null;
        private string webserviceToCallToUrl = string.Empty;
        private string webserviceUserKey = string.Empty;

        private ExternalWebservice webserviceToBeCalledOn = null;
        private string webserviceToBeCalledOnUrl = string.Empty;

        private FlatWsdlServiceHost host;
        private string webserviceSuccessResponse = "SUCCESS";
        private bool developmentMode = false;
        private InterfaceMessageIn webserviceToCallToDummy = null;

        #endregion

        #region Constructor & Initialization

        public TigertmsConnector(TigertmsConfigurationAdapter config = null, LogHandler logHandler = null) : base(logHandler)
        {
            if (config == null)
            {
                config = new TigertmsConfigurationAdapter();
                config.ReadFromConfigurationManager();
            }

            // Initialize settings
            this.webserviceToBeCalledOnUrl = config.WebserviceToBeCalledOnUrl;

            this.webserviceToCallToUrl = config.WebserviceToCallToUrl;
            this.webserviceUserKey = config.WebserviceUserkey;
            this.developmentMode = config.TestMode;

            // Write settings to log
            this.LogDebug("WebserviceToBeCalledOnUrl: '{0}'", this.webserviceToBeCalledOnUrl);
            this.LogDebug("WwebserviceToCallToUrl: '{0}'", this.webserviceToCallToUrl);
            this.LogDebug("WebserviceUserKey: '{0}'", this.webserviceUserKey);
            this.LogDebug("DevelopmentMode: '{0}'", this.developmentMode);

            // Initialize receiver
            this.InitializePmsReceiver();

            // Hookup events
            this.PmsMessageReceived += this.TigertmsConnector_PmsMessageReceived;

            this.webserviceResponseMessageTimeout = 60000;
        }

        public override string LogPrefix()
        {
            return "TigertmsConnector";
        }

        protected override void InitializePmsReceiver()
        {
            Uri baseAddress = new Uri(this.webserviceToBeCalledOnUrl);

            // Create the ServiceHost.
            if (this.host == null)
            {
                // Init the service
                this.webserviceToBeCalledOn = new ExternalWebservice(this.receivedMessages);

                // Host it
                this.host = new FlatWsdlServiceHost(this.webserviceToBeCalledOn, baseAddress);

                // No check for Endpoints > 0, should be there, otherwise a crash is correct.
                this.host.Description.Endpoints[0].Binding.Namespace = "http://tigergenericinterface.org/";

                // Enable metadata publishing.
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
                this.host.Description.Behaviors.Add(smb);

                // Open the ServiceHost to start listening for messages. Since
                // no endpoints are explicitly configured, the runtime will create
                // one endpoint per base address for each service contract implemented
                // by the service.
                this.host.Open();
            }

            // For development / testing spawn our own TigertmsDummyService
            if (this.developmentMode)
            {
                this.webserviceToCallToDummy = new InterfaceMessageIn(this.webserviceToBeCalledOn, this);
            }
        }

        public override void StopReceiving()
        {
            if (this.host != null)
            {
                this.host.Close();
                this.host = null;
            }
        }

        #endregion

        #region Methods to call the PMS

        private ConcurrentDictionary<string, EventWaitHandle> waitHandlers = new ConcurrentDictionary<string, EventWaitHandle>();
        private ConcurrentDictionary<string, MessageBase> responseMessages = new ConcurrentDictionary<string, MessageBase>();

        /// <summary>
        /// Indicates a request to have the PMS check a room out. The balance should match the PMS' record for
        /// the account / room number specified. The PMS must initiate an actual Check Out Room (after this
        /// message's required Express Checkout response) if this Express Checkout Request is accepted.
        /// </summary>
        /// <param name="checkoutInfo">The checkout info.</param>
        /// <returns>
        /// Boolean if the checkout succeeded
        /// </returns>
        public override bool Checkout(Model.Checkout checkoutInfo)
        {
            // Prepare message for sendingout
            bool toReturn;
            Requestcheckout requestCheckout = new Requestcheckout();
            requestCheckout.Room = checkoutInfo.DeliverypointNumber;
            requestCheckout.Balance = checkoutInfo.ChargePriceIn;//.ToStringInvariantCulture("N2");
            string responseMessageSignature = requestCheckout.GetResponseMessageSignature();

            // Call the Webservice            
            Checkoutresults result = (Checkoutresults)this.SendMessageToPmsSynchronously(requestCheckout, true);

            if (result.Reply == "Complete")
            {
                toReturn = true;
                this.LogVerbose("checkout completed: " + requestCheckout.Room);

                GuestInformation guestInformation = result.ToGuestInformationModel();
                if (guestInformation == null || guestInformation.DeliverypointNumber.IsNullOrWhiteSpace())
                {
                    this.LogVerbose("guestinformation == null || deliverypointNumber is empty");
                }
                else
                {
                    this.LogVerbose("checkout data: " + guestInformation.ToString());
                    this.OnCheckedOut(guestInformation);
                    this.LogVerbose("on checkedout done");
                }                
            }
            else
            {
                toReturn = false;
                this.LogWarning("Express checkout failed: For Room '{0}', Reason: '{1}'", requestCheckout.Room, result.Reply);
            }

            return toReturn;
        }

        /// <summary>
        /// Indicates a wakeUpStatus responds from the user on the client. 
        /// </summary>
        /// <param name="wakeUpStatus"></param>
        /// <returns></returns>
        public override bool SetWakeUpStatus(WakeUpStatus wakeUpStatus)
        {
            // Prepare message for sending out
            bool toReturn = true;
            
            // Call teh webservice

            //this.SendMessageToPmsSynchronously()

            return toReturn;
        }

        /// <summary>
        /// Get the Room Information for a Room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>
        /// The Room Information for the Room
        /// </returns>
        public override List<Model.GuestInformation> GetGuestInformation(string deliverypointNumber)
        {
            List<Model.GuestInformation> toReturn = new List<Model.GuestInformation>();

            // Prepare message for sending out
            Requestrefresh requestRefresh = new Requestrefresh();
            requestRefresh.Room = deliverypointNumber;

            try
            {
                // Call the webservice
                string result = this.SendMessageToPms(requestRefresh);
                if (result != this.webserviceSuccessResponse)
                {
                    this.LogWarning(string.Format("GetGuestInformation failed: For Room '{0}', Error: '{1}'", deliverypointNumber, result));
                }
                else
                {
                    GuestInformation guestInfo = new GuestInformation();
                    guestInfo.DeliverypointNumber = deliverypointNumber;
                    guestInfo.Occupied = false;
                    guestInfo.AllowViewFolio = false;
                    toReturn.Add(guestInfo);
                }
            }
            catch (Exception ex)
            {
                this.LogWarning(string.Format("TigertmsConnector - GetGuestInformation - Exception: {0}", ex.Message));
                throw;
            }

            return toReturn;
        }

        /// <summary>
        /// Get the Folio for the Room detailing all charges and credits booked for the Room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>
        /// The Folio for the room
        /// </returns>
        public override List<Model.Folio> GetFolio(string deliverypointNumber)
        {
            List<Model.Folio> toReturn = new List<Model.Folio>();

            // Preepare message for sending out
            Requestbill requestBill = new Requestbill();
            requestBill.Room = deliverypointNumber;
            string responseMessageSignature = requestBill.GetResponseMessageSignature();

            // Call the webservice
            Roombillresults bill = (Roombillresults)this.SendMessageToPmsSynchronously(requestBill, true);
            string errorText = string.Empty;
            if (bill != null)
            {
                XmlElement errorElement = null;
                if (bill.AnyElements != null)
                    errorElement = bill.AnyElements.FirstOrDefault(x => x.Name.Equals("error", StringComparison.OrdinalIgnoreCase));
                if (errorElement != null && !errorElement.Value.IsNullOrWhiteSpace())
                {
                    // Error.       
                    errorText = string.Format("GetFolio failed: For Room '{0}', Error: '{1}'", deliverypointNumber, errorElement.Value);
                    this.LogWarning(errorText);
                }
                else
                {
                    try
                    {
                        string billXml = XmlHelper.Serialize(bill);
                        this.LogInfo("Roombillresult XML: {0}", billXml);
                    }
                    catch (Exception)
                    {
                        // ignore
                    }

                    // Got a bill, convert to model and add to toreturn list
                    toReturn.Add(bill.ToFolioModel());
                }
            }
            else
            {
                errorText = string.Format("GetFolio failed: For Room: '{0}' - Unknown error", deliverypointNumber);
                this.LogWarning(errorText);
            }

            if (!errorText.IsNullOrWhiteSpace())
            {
                // Return an Empty folio with the error text
                Folio folioWithError = new Folio();
                folioWithError.DeliverypointNumber = deliverypointNumber;
                folioWithError.Error = errorText;
                toReturn.Add(folioWithError);
            }

            return toReturn;
        }

        /// <summary>
        /// Get the balance of the room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>
        /// The balance of the room
        /// </returns>
        public override decimal GetBalance(string deliverypointNumber)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the state of the room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>
        /// The room state
        /// </returns>
        public override RoomState GetRoomStatus(string deliverypointNumber)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Add one or more FolioItems to the Folio
        /// </summary>
        /// <param name="deliverypointNumber"></param>
        /// <param name="folioItems">Items to be added</param>
        /// <returns>
        /// True of false indicating if the item could be added
        /// </returns>
        public override bool AddToFolio(string deliverypointNumber, List<Model.FolioItem> folioItems)
        {
            // Prepare message for sendingout
            bool toReturn = false;

            // This integration supports only 1 item per charge, so that's what we do.
            string dateTimeString = MessageBase.DateTimeToString(DateTime.Now);
            foreach (FolioItem folioItem in folioItems)
            {
                this.LogVerbose("AddToFolio: Room: '{0}', Description: '{1}'", deliverypointNumber, folioItem.Description);

                Chargerecord charge = new Chargerecord();
                charge.Room = deliverypointNumber;
                charge.Type = "minibar"; // Hardcoded... you dirty boy..
                charge.ItemId = "";
                charge.ItemDescription = folioItem.Description;
                charge.ItemQuantity = "1";
                charge.Charge = folioItem.PriceIn.ToStringInvariantCulture("N2");

                if (folioItem.Created != null && folioItem.Created != DateTime.MinValue)
                    charge.DateTime = MessageBase.DateTimeToString(folioItem.Created);
                else
                    charge.DateTime = dateTimeString;

                string result = this.SendMessageToPms(charge);
                toReturn = (result == this.webserviceSuccessResponse);
            }
            return toReturn;
        }

        /// <summary>
        /// Set the Room State
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <param name="state">State of the room</param>
        /// <returns>
        /// True of false indicating if the state could be updated
        /// </returns>
        public override bool UpdateRoomState(string deliverypointNumber, RoomState state)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Methods to respond to calls from PMS

        void TigertmsConnector_PmsMessageReceived(PmsConnector sender, IPmsMessage pmsMessage)
        {
            MessageBase tigerMessage = pmsMessage as MessageBase;
            
            // Messages received from tiger
            if (tigerMessage.GetType() == typeof(Checkinresults))
            {
                // Check In (unsollicited, so don't work with the signalling and queue)                            
                Checkinresults checkinResults = (Checkinresults)tigerMessage;
                this.LogVerbose("Check In message received: Room: '{0}', Reservation: '{1}'", tigerMessage.Room, tigerMessage.ReservationNumber);

                GuestInformation guestInformation = checkinResults.ToGuestInformationModel();

                // For testing / development:
                this.CheckedInForDevelopmentMode = true;                
                this.GuestInformationForDevelopmentMode = guestInformation;

                this.OnCheckedIn(guestInformation);
            }
            else if (tigerMessage.GetType() == typeof(Checkoutresults))
            {
                // Check out (unsollicited, so don't work with the signalling and queue)
                Checkoutresults checkoutInfo = (Checkoutresults)tigerMessage;
                this.LogVerbose("Checkout message received: Room: '{0}', Reservation: '{1}'", tigerMessage.Room, tigerMessage.ReservationNumber);

                // For testing / development:
                this.CheckedInForDevelopmentMode = false;
                this.GuestInformationForDevelopmentMode = null;

                GuestInformation guestInformation = checkoutInfo.ToGuestInformationModel();
                this.OnCheckedOut(guestInformation);
            }
            else if (tigerMessage.GetType() == typeof(Roombillresults))
            {
                // Room bill results (unsollicited, so don't work with the signalling and queue)
                Roombillresults roombillResults = (Roombillresults)tigerMessage;
                this.LogVerbose("Room bill results received: Room: '{0}'", tigerMessage.Room);

                Folio folio = roombillResults.ToFolioModel();
                this.OnFolioUpdated(folio);
            }
            else if (tigerMessage.GetType() == typeof(Refreshresults))
            {
                // Refresh results (unsollicited, so don't work with the signalling and queue)
                Refreshresults refreshResults = (Refreshresults)tigerMessage;
                this.LogVerbose("Refresh results message received: Room: '{0}'", refreshResults.Room);

                GuestInformation guestInformation = refreshResults.ToGuestInformationModel();

                this.GuestInformationForDevelopmentMode = guestInformation;
                this.OnGuestInformationUpdated(this.GuestInformationForDevelopmentMode);
            }
            else if (tigerMessage.GetType() == typeof(Roommoveresults))
            {
                // Room move (unsollicited, so don't work with the signalling and queue)
                Roommoveresults moveInfo = (Roommoveresults)tigerMessage;
                this.LogVerbose("Roommove message received: From Room: '{0}', To Room: '{1}'", moveInfo.RoomOld, tigerMessage.Room);

                this.OnMoved(moveInfo.RoomOld, string.Empty, moveInfo.Room, string.Empty);
            }
            else if (tigerMessage.GetType() == typeof(Wakeupsetresults))
            {
                // Wakeupset results (unsollicited, so don't work with the signalling and queue)
                Wakeupsetresults wakeupsetResults = (Wakeupsetresults)tigerMessage;
                this.LogVerbose("Wakeup-set results message received: For Room: '{0}'", wakeupsetResults.Room);

                WakeUpStatus wakeupStatus = wakeupsetResults.ToWakeUpStatusModel();
                this.OnWakeUpSet(wakeupStatus);
            }
            else if (tigerMessage.GetType() == typeof(Wakeupclearresults))
            {
                // Wakeupclear results (unsollicited, so don't work with the signalling and queue)
                Wakeupclearresults wakeupclearResults = (Wakeupclearresults)tigerMessage;
                this.LogVerbose("Wakeup-clear results message received: For Room: '{0}'", wakeupclearResults.Room);

                WakeUpStatus wakeupStatus = wakeupclearResults.ToWakeUpStatusModel();
                this.OnWakeUpCleared(wakeupStatus);
            }
            else if (tigerMessage.GetType() == typeof(Roomdataresults))
            {
                Roomdataresults roomdataResults = (Roomdataresults)tigerMessage;
                this.LogVerbose("Roomdata results message received: For Room: '{0}'", roomdataResults.Room);

                RoomData roomData = roomdataResults.ToRoomDataModel();

                this.RefreshGuestInformationForDevelopment(roomData);
                this.OnRoomDataUpdated(roomData);
            }
            else if (tigerMessage.GetType() == typeof(Editguestresults))
            {
                Editguestresults editguestResults = (Editguestresults)tigerMessage;
                this.LogVerbose("Editguest results message received: For Room: '{0}'", editguestResults.Room);

                GuestInformation guestInformation = editguestResults.ToGuestInformationModel();

                this.RefreshGuestInformationForDevelopment(guestInformation);
                this.OnGuestInformationEdited(guestInformation);
            }
            else
            {
                // Unsollicited message.
                this.LogWarning("Unsollicited message, message is ignored: " + tigerMessage.GetMessageSignature());
            }
        }

        private void RefreshGuestInformationForDevelopment(GuestInformation guestInformation)
        {
            if (this.GuestInformationForDevelopmentMode != null && guestInformation != null)
            {
                this.GuestInformationForDevelopmentMode.Title = guestInformation.Title;
                this.GuestInformationForDevelopmentMode.CustomerFirstname = guestInformation.CustomerFirstname;
                this.GuestInformationForDevelopmentMode.CustomerLastname = guestInformation.CustomerLastname;
                this.GuestInformationForDevelopmentMode.AllowViewFolio = guestInformation.AllowViewFolio;
                this.GuestInformationForDevelopmentMode.AllowExpressCheckout = guestInformation.AllowExpressCheckout;
                this.GuestInformationForDevelopmentMode.LanguageCode = guestInformation.LanguageCode;
            }
        }

        private void RefreshGuestInformationForDevelopment(RoomData roomData)
        {
            if (this.GuestInformationForDevelopmentMode != null && roomData != null)
            {
                this.GuestInformationForDevelopmentMode.VoicemailMessage = roomData.VoicemailMessage;
            }
        }

        #endregion

        #region Webservice communication

        protected override string SendMessageToPms(IPmsMessage message)
        {
            // Convert message to XML
            string messageXml;
            try
            {
                // Send message via single entry method, easiest for generic use, and the names methods of the
                // webservice to the exact same thing.
                messageXml = XmlHelper.Serialize(message);
            }
            catch (Exception ex)
            {
                throw new ObymobiException(TigertmsConnectorResult.MessageSerializationFailed, ex);
            }

            // Submit message to Webservice
            string result;
            try
            {
                this.LogDebug("Message being pushed to webservice: {0}", message.MessageDescription);

                if (this.developmentMode)
                {
                    if (TestUtil.IsPcGabriel)
                    {
                        if (message is Requestbill)
                        {
                            result = this.webserviceToCallToDummy.externalInterfaceMessageIn(messageXml, 1000);
                        }
                        else
                            result = this.webserviceToCallToDummy.externalInterfaceMessageIn(messageXml, 250);
                    }
                    else
                        result = this.webserviceToCallToDummy.externalInterfaceMessageIn(messageXml, 2500);
                }
                else
                    result = this.WebserviceToCallTo.externalInterfaceMessageIn(messageXml);

                // Return if we had success
                if (result == this.webserviceSuccessResponse)
                {
                    // OK
                }
                else
                {
                    // FAILED!
                    throw new ObymobiException(TigertmsConnectorResult.CallingWebserviceReturnedFailed, "Url: {0}\r\nResult: '{1}'\r\nMessage: {2}", this.WebserviceToCallTo.Url, result, messageXml);
                }
            }
            catch (ObymobiException ex)
            {
                throw new ObymobiException(TigertmsConnectorResult.CallingWebserviceFailed, ex, "Url: {0}\r\n Message: {1}", this.WebserviceToCallTo.Url, messageXml);
            }
            catch
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// Gets the webservice to which calls will be done
        /// </summary>
        public TigertmsWebservice.InterfaceMessageIn WebserviceToCallTo
        {
            get
            {
                if (this.webserviceToCallTo == null)
                {
                    this.webserviceToCallTo = new TigertmsWebservice.InterfaceMessageIn();
                    this.webserviceToCallTo.Url = this.webserviceToCallToUrl;
                    
                    if (TestUtil.IsPcGabriel)
                    {
                        this.webserviceToCallTo.Timeout = 100000;
                    }
                }
                return this.webserviceToCallTo;
            }
        }

        /// <summary>
        /// Gets the url of the Webservice Tigertms can call this PMS Connector
        /// </summary>
        public string WebserviceToBeCalledOnUrl
        {
            get
            {
                return this.webserviceToBeCalledOnUrl;
            }
        }

        #endregion

        #region Properties for Development / Test mode

        private bool checkedInForDevelopmentMode = false;

        /// <summary>
        /// Gets or sets the checkedInForDevelopmentMode
        /// </summary>
        public bool CheckedInForDevelopmentMode
        {
            get
            {
                return this.checkedInForDevelopmentMode;
            }
            set
            {
                this.checkedInForDevelopmentMode = value;
            }
        }


        private GuestInformation guestInformationForDevelopmentMode = null;


        /// <summary>
        /// Gets or sets the guestInformation
        /// </summary>
        public GuestInformation GuestInformationForDevelopmentMode
        {
            get
            {
                return this.guestInformationForDevelopmentMode;
            }
            set
            {
                this.guestInformationForDevelopmentMode = value;
            }
        }

        

        #endregion
    }
}
