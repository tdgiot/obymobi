﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.HelperClasses;
using System.Xml.Serialization;
using System.Xml;
using System.Globalization;
using Dionysos;

namespace Obymobi.Logic.PMS.Tigertms.Messages
{
    public abstract class MessageBase : IPmsMessage
    {
        public MessageBase()
        {
            this.Wsuserkey = Dionysos.ConfigurationManager.GetString(PmsConfigurationConstants.TigertmsWebserviceUserkey);
        }

        [XmlAttribute("resno")]
        public string ReservationNumber
        { get; set; }

        [XmlElement("room")]
        public string Room
        { get; set; }

        [XmlElement("wsuserkey")]
        public string Wsuserkey
        { get; set; }

        [XmlAnyAttribute]
        public XmlAttribute[] AnyAttributes;

        [XmlAnyElement]
        public XmlElement[] AnyElements;

        public string MessageType
        {
            get
            {
                return this.GetType().Name;
            }
        }

        /// <summary>
        /// Gets the MessageSignature for this message
        /// </summary>
        /// <returns></returns>
        public string GetMessageSignature()
        {
            return string.Format("{0}-{1}", this.GetType().Name.ToLower(), this.Room);
        }

        /// <summary>
        /// Gets the MessageSignature of the expected return message
        /// </summary>
        /// <returns></returns>
        public abstract string GetResponseMessageSignature();

        public string ToXml()
        {
            return XmlHelper.Serialize(this);
        }

        /// <summary>
        /// Determines the type of message and deserializes it to that type
        /// </summary>
        /// <returns></returns>
        public static MessageBase GetMessageFromXml(string xml)
        {
            if (xml.Contains("<addguestresults"))
            {
                return XmlHelper.Deserialize<Addguestresults>(xml);
            }
            else if (xml.Contains("<billitem"))
            {
                return XmlHelper.Deserialize<BillItem>(xml);
            }
            else if (xml.Contains("<chargerecord"))
            {
                return XmlHelper.Deserialize<Chargerecord>(xml);
            }
            else if (xml.Contains("<checkinresults"))
            {
                return XmlHelper.Deserialize<Checkinresults>(xml);
            }
            else if (xml.Contains("<checkoutresults"))
            {
                return XmlHelper.Deserialize<Checkoutresults>(xml);
            }
            else if (xml.Contains("<editguestresults"))
            {
                return XmlHelper.Deserialize<Editguestresults>(xml);
            }
            else if (xml.Contains("<refreshresults"))
            {
                return XmlHelper.Deserialize<Refreshresults>(xml);
            }
            else if (xml.Contains("<removeguestresults"))
            {
                return XmlHelper.Deserialize<Removeguestresults>(xml);
            }
            else if (xml.Contains("<requestbill"))
            {
                return XmlHelper.Deserialize<Requestbill>(xml);
            }
            else if (xml.Contains("<requestcheckout"))
            {
                return XmlHelper.Deserialize<Requestcheckout>(xml);
            }
            else if (xml.Contains("<requestrefresh"))
            {
                return XmlHelper.Deserialize<Requestrefresh>(xml);
            }
            else if (xml.Contains("<roombillresults"))
            {
                return XmlHelper.Deserialize<Roombillresults>(xml);
            }
            else if (xml.Contains("<roomdataresults"))
            {
                return XmlHelper.Deserialize<Roomdataresults>(xml);
            }
            else if (xml.Contains("<roommoveresults"))
            {
                return XmlHelper.Deserialize<Roommoveresults>(xml);
            }
            else if (xml.Contains("<roomstatusresults"))
            {
                return XmlHelper.Deserialize<Roomstatusresults>(xml);
            }
            else if (xml.Contains("<wakeupclearresults"))
            {
                return XmlHelper.Deserialize<Wakeupclearresults>(xml);
            }
            else if (xml.Contains("<wakeupsetresults"))
            {
                return XmlHelper.Deserialize<Wakeupsetresults>(xml);
            }
            else if (xml.Contains("<wakeupstatusresults"))
            {
                return XmlHelper.Deserialize<Wakeupstatusresults>(xml);
            }
            else
                throw new NotImplementedException("Message type is not implemented: " + xml);
        }

        public static string TimeStringFormat = "HH:mm:ss";
        public static string FirstDateStringFormat = "dd/MM/yyyy";
        public static string SecondDateStringFormat = "MM/dd/yyyy";
        public static string FirstDateTimeStringFormat = "dd/MM/yyyy HH:mm:ss";
        public static string SecondDateTimeStringFormat = "MM/dd/yyyy HH:mm:ss";
        public static string ThirdDateTimeStringFormat = "yyyy-MM-dd HH:mm:ss";

        /// <summary>
        /// Converts a TigerTMS DateTime string to a DateTime object (DateTime.MinValue when parse fails)
        /// </summary>
        /// <param name="datetimeString">The datetime string.</param>
        /// <returns>DateTime.MinValue when parse fails</returns>
        public static DateTime StringToDateTime(string datetimeString, string dateTimeFormat = "")
        {
            string formatToUse = dateTimeFormat.IsNullOrWhiteSpace() ? MessageBase.FirstDateTimeStringFormat : dateTimeFormat;
            DateTime? toReturn = MessageBase.ParseToDateTime(datetimeString, formatToUse);

            if (!toReturn.HasValue)
            {
                if (datetimeString.Contains(" ", StringComparison.InvariantCultureIgnoreCase))
                {
                    toReturn = MessageBase.ParseToDateTime(datetimeString, MessageBase.SecondDateTimeStringFormat);
                    if (!toReturn.HasValue)
                    {
                        toReturn = MessageBase.ParseToDateTime(datetimeString, MessageBase.ThirdDateTimeStringFormat);
                    }
                }
                else
                {
                    toReturn = MessageBase.ParseToDateTime(datetimeString, MessageBase.FirstDateStringFormat);
                    if (!toReturn.HasValue)
                    {
                        toReturn = MessageBase.ParseToDateTime(datetimeString, MessageBase.SecondDateStringFormat);
                    }
                }

            }

            if (!toReturn.HasValue)
            {
                toReturn = DateTime.MinValue;
            }

            return toReturn.Value;
        }

        /// <summary>
        /// Converts a DateTime object to a TigerTMS datetime string
        /// </summary>  
        /// <param name="datetime">The datetime.</param>
        /// <returns></returns>
        public static string DateTimeToString(DateTime datetime)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            return datetime.ToString(MessageBase.FirstDateTimeStringFormat, provider);
        }

        /// <summary>
        /// Converts a string to a datetime
        /// </summary>
        /// <param name="datetimeString"></param>
        /// <param name="formatToUse"></param>
        /// <returns></returns>
        private static DateTime? ParseToDateTime(string datetimeString, string formatToUse)
        {
            DateTime? toReturn;
            try
            {
                toReturn = DateTime.ParseExact(datetimeString, formatToUse, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                toReturn = null;
            }
            return toReturn;
        }

        /// <summary>
        /// Gets a description about the message for tracing/logging, should be RoomNo + Message Type
        /// </summary>
        public string MessageDescription
        {
            get
            {
                return string.Format("Room: '{0}', Message: '{1}'", this.Room, this.GetType().Name);
            }
        }
    }
}
