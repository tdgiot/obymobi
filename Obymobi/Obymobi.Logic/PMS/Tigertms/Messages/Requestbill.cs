﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "requestbill")]
    public class Requestbill : MessageBase
    {
        [XmlElement("extension")]
        public string Extension
        { get; set; }

        /// <summary>
        /// Gets the MessageSignature of the expected return message
        /// </summary>
        /// <returns></returns>
        public override string GetResponseMessageSignature()
        {
            return string.Format("roombillresults-{0}", this.Room);
        }
    }
}
