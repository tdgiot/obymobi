﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

namespace Obymobi.Logic.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "removeguestresults")]
    public class Removeguestresults : MessageBase
    {

        [XmlArray("extensions")]
        [XmlArrayItem("extension")]
        public string[] Extensions
        { get; set; }

        [XmlElement("title")]
        public string Title
        { get; set; }

        [XmlElement("last")]
        public string Last
        { get; set; }

        [XmlElement("first")]
        public string First
        { get; set; }

        public Model.GuestInformation ToGuestInformationModel()
        {
            Model.GuestInformation gi = new Model.GuestInformation();
            gi.DeliverypointNumber = this.Room;            
            gi.Title = this.Title;
            gi.CustomerLastname = this.Last;
            gi.CustomerFirstname = this.First;

            return gi;
        }

        public override string GetResponseMessageSignature()
        {
            throw new NotImplementedException("Message is a response itself");
        }
    }
}
