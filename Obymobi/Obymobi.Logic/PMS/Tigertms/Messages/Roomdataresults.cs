﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Logic.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "roomdataresults")]
    public class Roomdataresults : MessageBase
    {
        [XmlArray("extensions")]
        [XmlArrayItem("extension")]
        public string[] Extensions
        { get; set; }

        /// <summary>
        /// 0	- Barred
        /// 1	– Local calls only
        /// 2	– National 
        /// 3	– Unrestricted
        /// </summary>
        [XmlElement("cos")]
        public string ClassOfService
        { get; set; }

        /// <summary>
        /// Do Not Disturb feature is either Y or N   ( Yes or No).        
        /// </summary>
        [XmlElement("dnd")]
        public string DoNotDisturb
        { get; set; }

        /// <summary>
        /// Message Waiting Lamp on is either Y or N  ( Yes or No).
        /// </summary>
        [XmlElement("mw")]
        public string MessageWaitingLamp
        { get; set; }

        public Model.RoomData ToRoomDataModel()
        {
            Model.RoomData rd = new Model.RoomData();
            rd.DeliverypointNumber = this.Room;            

            int classOfService;
            if (int.TryParse(this.ClassOfService, out classOfService))
            {
                rd.ClassOfServiceSet = true;
                rd.ClassOfService = classOfService;
            }

            if (!this.DoNotDisturb.IsNullOrWhiteSpace())
            {
                rd.DoNotDisturbSet = true;
                rd.DoNotDisturb = this.DoNotDisturb.Equals("y", StringComparison.InvariantCultureIgnoreCase);
            }

            if (!this.MessageWaitingLamp.IsNullOrWhiteSpace())
            {
                rd.VoicemailMessageSet = true;
                rd.VoicemailMessage = this.MessageWaitingLamp.Equals("y", StringComparison.InvariantCultureIgnoreCase);
            }

            return rd;
        }

        public override string GetResponseMessageSignature()
        {
            throw new NotImplementedException("Message is a response itself");
        }
    }
}
