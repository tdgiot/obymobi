﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Obymobi.Logic.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "requestcheckout")]
    public class Requestcheckout : MessageBase
    {
        [XmlElement("extension")]
        public string Extension
        { get; set; }

        [XmlElement("balance")]
        public decimal Balance
        { get; set; }

        /// <summary>
        /// Gets the MessageSignature of the expected return message
        /// </summary>
        /// <returns></returns>
        public override string GetResponseMessageSignature()
        {
            return string.Format("checkoutresults-{0}", this.Room);
        }
    }
}
