﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using Obymobi.Logic.Model;
using System.Globalization;

namespace Obymobi.Logic.PMS.Tigertms.Messages
{
    [Serializable, XmlRootAttribute(ElementName = "roombillresults")]
    public class Roombillresults : MessageBase
    {
        [XmlArray("extensions")]
        [XmlArrayItem("extension")]
        public string[] Extensions
        { get; set; }

        [XmlElement("balance")]
        public string Balance
        { get; set; }

        [XmlElement("item")]
        public List<BillItem> Items
        { get; set; }

        public Folio ToFolioModel()
        {
            Folio folio = new Folio();
            folio.DeliverypointNumber = this.Room;            

            decimal balance;
            if (decimal.TryParse(this.Balance, NumberStyles.Integer | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture, out balance))
                folio.Balance = balance;

            List<FolioItem> folioItems = new List<FolioItem>();

            foreach (var item in this.Items)
            {
                FolioItem folioItem = new FolioItem();
                folioItem.Code = item.Code;
                folioItem.Description = item.Description;

                decimal price;
                if (decimal.TryParse(item.Charge, NumberStyles.Integer | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.InvariantCulture, out price))
                    folioItem.PriceIn = price;
                else
                    folioItem.Description += " (Price unknown)";

                folioItem.Created = MessageBase.StringToDateTime(item.DateTime);
                folioItems.Add(folioItem);
            }

            folio.FolioItems = folioItems.ToArray();

            return folio;
        }

        public override string GetResponseMessageSignature()
        {
            throw new NotImplementedException("Message is a response itself");
        }
    }
}
