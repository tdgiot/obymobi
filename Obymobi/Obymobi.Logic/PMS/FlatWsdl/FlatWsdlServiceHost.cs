﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Obymobi.Logic.PMS.FlatWsdl
{
    public class FlatWsdlServiceHost : ServiceHost
    {
        #region Constructors

        public FlatWsdlServiceHost()
        {
        }

        public FlatWsdlServiceHost(Type serviceType, params Uri[] baseAddresses) : base(serviceType, baseAddresses)
        {
        }

        public FlatWsdlServiceHost(object singeltonInstance, params Uri[] baseAddresses) : base(singeltonInstance, baseAddresses)
        {
        }

        #endregion

        protected override void ApplyConfiguration()
        {
            base.ApplyConfiguration();

            ServiceDebugBehavior debug = this.Description.Behaviors.Find<ServiceDebugBehavior>();

            // if not found - add behavior with setting turned on 
            if (debug == null)
            {
                this.Description.Behaviors.Add(new ServiceDebugBehavior { IncludeExceptionDetailInFaults = true });
            }
            else
            {
                // make sure setting is turned ON
                if (!debug.IncludeExceptionDetailInFaults)
                {
                    debug.IncludeExceptionDetailInFaults = true;
                }
            }

            if (this.Description.Endpoints == null || this.Description.Endpoints.Count == 0)
            {
                AddDefaultEndpoints();
            }

            InjectFlatWsdlExtension();
        }

        private void InjectFlatWsdlExtension()
        {
            foreach (ServiceEndpoint endpoint in this.Description.Endpoints)
            {
                endpoint.Behaviors.Add(new FlatWsdl());
            }
        }
    }
}