﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.PMS
{
    public class WebserviceCallContainer
    {
        public Action WebserviceCall { get; set; }

        public IPmsMessage Message;
    }
}
