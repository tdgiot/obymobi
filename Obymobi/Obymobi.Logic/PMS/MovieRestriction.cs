﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.PMS
{
    /// <summary>
    /// MovieRestriction enums
    /// </summary>
    public enum MovieRestriction : int
    {
        /// <summary>
        /// Everything is allowed
        /// </summary>
        AllAllowed = 0,
        /// <summary>
        /// Everything is restricted
        /// </summary>
        AllRestricted = 1,
        /// <summary>
        /// Should be payed for
        /// </summary>
        PayRestricted = 2,
        /// <summary>
        /// Adults only
        /// </summary>
        AdultRestricted = 3
    }
}
