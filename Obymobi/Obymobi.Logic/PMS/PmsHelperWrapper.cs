﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Enums;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Logic.PMS
{
    public static class PmsHelperWrapper
    {
        public enum PmsHelperWrapperResult : int
        {
            MultipleTerminalsWithPmsConnectorIsNotSupported = 200
        }

        /// <summary>
        /// Gets the type of the related pos connector.
        /// </summary>
        /// <param name="company">The company.</param>
        /// <returns></returns>
        public static PMSConnectorType GetRelatedPosConnectorType(this CompanyEntity company)
        {
            PMSConnectorType toReturn = PMSConnectorType.Unknown;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.CompanyId == company.CompanyId);
            filter.Add(TerminalFields.PMSConnectorType > 0);

            IncludeFieldsList includeFields = new IncludeFieldsList();
            includeFields.Add(TerminalFields.PMSConnectorType);

            TerminalCollection terminals = new TerminalCollection();
            terminals.GetMulti(filter, includeFields, null);

            if (terminals.Count > 1)
                throw new ObymobiException(PmsHelperWrapperResult.MultipleTerminalsWithPmsConnectorIsNotSupported, "Company: '{0}', CompanyId: '{1}'",
                    company.Name, company.CompanyId);
            else if (terminals.Count == 1)
                toReturn = terminals[0].PMSConnectorTypeEnum;

            return toReturn;
        }

        /// <summary>
        /// Gets the PMS connected terminal for the company.         
        /// </summary>        
        /// <param name="companyId">The company id.</param>        
        /// <returns>Terminal or null</returns>
        public static TerminalEntity GetPmsConnectedTerminalByCompanyId(int companyId)
        {
            return PmsHelperWrapper.GetPmsConnectedTerminal(companyId, null);
        }

        /// <summary>
        /// Gets the PMS connected terminal for the client.         
        /// </summary>                
        /// <param name="clientId">The client id.</param>
        /// <returns>Terminal or null</returns>
        public static TerminalEntity GetPmsConnectedTerminalByClientId(int clientId)
        {
            return PmsHelperWrapper.GetPmsConnectedTerminal(null, clientId);
        }



        /// <summary>
        /// Gets the PMS connected terminal for the company. 
        /// Fill CompanyId or ClientId
        /// </summary>        
        /// <param name="companyId">The company id, or null if you supply a clientId.</param>
        /// <param name="clientId">The client id, or null if you supply a companyId.</param>
        /// <returns>Terminal or null</returns>
        private static TerminalEntity GetPmsConnectedTerminal(int? companyId, int? clientId)
        {
            TerminalEntity toReturn = null;

            PredicateExpression filter = new PredicateExpression();
            RelationCollection joins = new RelationCollection();

            // Search by CompanyId
            if (companyId.HasValue)
            {
                filter.Add(TerminalFields.CompanyId == companyId.Value);
            }

            // Search by Client Id
            if (clientId.HasValue)
            {
                filter.Add(ClientFields.ClientId == clientId.Value);
                joins.Add(TerminalEntity.Relations.CompanyEntityUsingCompanyId);
                joins.Add(CompanyEntity.Relations.ClientEntityUsingCompanyId);
            }

            // Require the Terminal to be PMS connected
            filter.Add(TerminalFields.PMSConnectorType > 0);

            TerminalCollection terminals = new TerminalCollection();
            terminals.GetMulti(filter, joins);

            if (terminals.Count == 1)
                toReturn = terminals[0];

            return toReturn;
        }
    }
}
