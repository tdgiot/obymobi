﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.PMS
{
    /// <summary>
    /// DoNotDisturbState enums
    /// </summary>
    public enum DoNotDisturbState : int
    {
        /// <summary>
        /// State is none
        /// </summary>
        None = 0,
        /// <summary>
        /// State is do not disturb
        /// </summary>
        DoNotDisturb = 1
    }
}
