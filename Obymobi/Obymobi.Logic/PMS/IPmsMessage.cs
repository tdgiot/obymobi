﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.PMS
{
    public interface IPmsMessage
    {
        /// <summary>
        /// Gets a 'Signature' of the message that identifies the message.
        /// This could be a transaction id or a combination of the paramters used in the message
        /// </summary>
        /// <returns></returns>
        string GetMessageSignature();
        /// <summary>
        /// Gets the 'Signature' of the message that will be the response for the message.
        /// </summary>
        /// <returns></returns>
        string GetResponseMessageSignature();
        /// <summary>
        /// Gets a description about the message for tracing/logging, should be RoomNo + Message Type
        /// </summary>
        string MessageDescription { get; }
    }
}
