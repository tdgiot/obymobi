﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.PMS
{
    /// <summary>
    /// GameRestriction enums
    /// </summary>
    public enum GameRestriction
    {
        /// <summary>
        /// Everything is allowed
        /// </summary>
        AllAllowed = 0,
        /// <summary>
        /// Everything is restricted
        /// </summary>
        AllRestricted = 1,
        /// <summary>
        /// Adults only
        /// </summary>
        AdultRestricted = 2
    }
}
