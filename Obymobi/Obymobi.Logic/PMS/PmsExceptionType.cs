﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.PMS
{
    public enum PmsExceptionType
    {
        Unspecified = 100,
        UnprocessableCommand = 200,
    }
}
