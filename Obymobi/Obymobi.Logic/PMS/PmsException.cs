﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.PMS.Comtrol
{
    public class PmsException : ObymobiException
    {
        public PmsException(PmsExceptionType type, String message, params object[] args)
            : base(type, message, args)
        {

        }

        public PmsException(PmsExceptionType type, Exception ex, String message, params object[] args)
            : base(type, ex, message, args)
        {

        }
    }
}
