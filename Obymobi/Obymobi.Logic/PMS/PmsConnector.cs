﻿using System;
using System.Collections.Generic;
using Obymobi.Logic.Model;
using Dionysos;
using System.Collections.ObjectModel;
using System.Threading;
using System.Collections.Concurrent;
using System.Collections.Specialized;
using Obymobi.Logic.HelperClasses;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Obymobi.Logic.PMS
{
    /*
     *
     * To decide: Is Roomnumber enough?
     * Do we implement guests per room?
     * CheckedInRoom or RoomCheckedIn or CheckedIn
     *
     *
    */


    /// <summary>
    /// Base class for a PMS Connector
    /// The PMS Connector does two-way traffic, it can call the PMS to request stuff
    /// and can be called by the PMS to be notified of stuff.
    /// </summary>
    public abstract class PmsConnector : LoggingClassBase
    {
        public enum PmsConnectorResult : int
        {
            NullResponseFromWebservice = 200,
            MultipleMessagesSameSignature = 201
        }

        #region Generic methods

        bool eventsHooked = false;

        public PmsConnector(LogHandler logHandler = null)
        {
            this.receivedMessages.CollectionChanged += this.receivedMessages_CollectionChanged;
            this.eventsHooked = true;

            this.Logged += logHandler;
        }

        public override string LogPrefix()
        {
            return "PmsConnector";
        }

        public void Initialize()
        {
            InitializePmsReceiver();
        }

        #endregion

        #region Receive from Pms

        /// <summary>
        /// Checked-In logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="guestInformation">The guest information.</param>
        public delegate void CheckInHandler(PmsConnector sender, GuestInformation guestInformation);
        /// <summary>
        /// Occurs when [checked in].
        /// </summary>
        public event CheckInHandler CheckedIn;

        /// <summary>
        /// This message should be received when the guest checks into a previously checked-out room.
        /// </summary>
        /// <param name="roomInformation"></param>
        protected virtual void OnCheckedIn(GuestInformation roomInformation)
        {
            // Put in local variable to be safe in multithreaded environment
            CheckInHandler listener = this.CheckedIn;
            if (listener != null)
            {
                listener(this, roomInformation);
            }            
        }

        /// <summary>
        /// Checked out logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        public delegate void CheckOutHandler(PmsConnector sender, GuestInformation guestInformation);
        /// <summary>
        /// Occurs when [checked out].
        /// </summary>
        public event CheckOutHandler CheckedOut;

        /// <summary>
        /// This message should be received when the guest checks out of a room (not moving rooms).
        /// </summary>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        protected virtual void OnCheckedOut(GuestInformation guestInformation)
        {
            CheckOutHandler listener = this.CheckedOut;
            if (listener != null)
            {
                listener(this, guestInformation);
            }
        }

        /// <summary>
        /// Moved Room logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="fromDeliverypointNumber">From deliverypoint number.</param>
        /// <param name="fromStationNumber">From station number.</param>
        /// <param name="toDeliverypointNumber">To deliverypoint number.</param>
        /// <param name="toStationNumber">To station number.</param>
        public delegate void MoveHandler(PmsConnector sender, string fromDeliverypointNumber, string fromStationNumber, string toDeliverypointNumber, string toStationNumber);
        /// <summary>
        /// Occurs when [moved].
        /// </summary>
        public event MoveHandler Moved;

        /// <summary>
        /// This message should be received when a credit limit is set for a room
        /// </summary>
        /// <param name="fromDeliverypointNumber">From deliverypoint number.</param>
        /// <param name="fromStationNumber">From station number.</param>
        /// <param name="toDeliverypointNumber">To deliverypoint number.</param>
        /// <param name="toStationNumber">To station number.</param>
        protected virtual void OnMoved(string fromDeliverypointNumber, string fromStationNumber, string toDeliverypointNumber, string toStationNumber)
        {
            MoveHandler listener = this.Moved;
            if (listener != null)
            {
                listener(this, fromDeliverypointNumber, fromStationNumber, toDeliverypointNumber, toStationNumber);
            }            
        }

        /// <summary>
        /// Guest information logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="guestInformation">The guest information.</param>
        public delegate void GuestInformationUpdateHandler(PmsConnector sender, GuestInformation guestInformation);
        /// <summary>
        /// Occurs when [guest information updated].
        /// </summary>
        public event GuestInformationUpdateHandler GuestInformationUpdated;

        /// <summary>
        /// This message should be received when information about the room is updated (moving rooms have seperate methods)
        /// </summary>
        /// <param name="guestInformation"></param>
        protected virtual void OnGuestInformationUpdated(GuestInformation guestInformation)
        {
            GuestInformationUpdateHandler listener = this.GuestInformationUpdated;
            if (listener != null)
            {
                listener(this, guestInformation);
            }            
        }

        /// <summary>
        /// Credit limit logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="limit">The limit.</param>
        public delegate void CreditLimitUpdateHandler(PmsConnector sender, string deliverypointNumber, decimal limit);
        /// <summary>
        /// Occurs when [credit limit updated].
        /// </summary>
        public event CreditLimitUpdateHandler CreditLimitUpdated;

        /// <summary>
        /// This message should be received when a credit limit is set for a room
        /// </summary>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="limit">The limit.</param>
        protected virtual void OnCreditLimitUpdated(string deliverypointNumber, decimal limit)
        {
            CreditLimitUpdateHandler listener = this.CreditLimitUpdated;
            if (listener != null)
            {
                listener(this, deliverypointNumber, limit);
            }            
        }

        /// <summary>
        /// Do not disturb handler
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="state">The state.</param>
        public delegate void DoNotDisturbUpdateHandler(PmsConnector sender, string deliverypointNumber, DoNotDisturbState state);
        /// <summary>
        /// Occurs when [do not disturb state updated].
        /// </summary>
        public event DoNotDisturbUpdateHandler DoNotDisturbStateUpdated;

        /// <summary>
        /// This message should be received when a credit limit is set for a room
        /// </summary>
        /// <param name="deliverypointNumber"></param>
        /// <param name="stationNumber"></param>
        /// <param name="state"></param>
        protected virtual void OnDoNotDisturbUpdated(string deliverypointNumber, string stationNumber, DoNotDisturbState state)
        {
            DoNotDisturbUpdateHandler listener = this.DoNotDisturbStateUpdated;
            if (listener != null)
            {
                listener(this, deliverypointNumber, state);
            }            
        }

        /// <summary>
        /// Room state logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="state">The state.</param>
        public delegate void RoomStateUpdateHandler(PmsConnector sender, string deliverypointNumber, RoomState state);
        /// <summary>
        /// Occurs when [room state updated].
        /// </summary>
        public event RoomStateUpdateHandler RoomStateUpdated;

        /// <summary>
        /// This message should be received when a credit limit is set for a room
        /// </summary>
        /// <param name="deliverypointNumber"></param>
        /// <param name="state"></param>
        protected virtual void OnRoomStateUpdate(string deliverypointNumber, RoomState state)
        {
            RoomStateUpdateHandler listener = this.RoomStateUpdated;
            if (listener != null)
            {
                listener(this, deliverypointNumber, state);
            }            
        }

        /// <summary>
        /// Set wake up logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="guestInformation">The wake up status.</param>
        public delegate void WakeUpSetHandler(PmsConnector sender, WakeUpStatus wakeUpStatus);
        /// <summary>
        /// Occurs when [wakeup status is set].
        /// </summary>
        public event WakeUpSetHandler WakeUpSet;

        /// <summary>
        /// This message should be received when information about the wake-up status is set
        /// </summary>
        /// <param name="guestInformation"></param>
        protected virtual void OnWakeUpSet(WakeUpStatus wakeUpStatus)
        {
            WakeUpSetHandler listener = this.WakeUpSet;
            if (listener != null)
            {
                listener(this, wakeUpStatus);
            }            
        }

        /// <summary>
        /// Clear wake up logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="guestInformation">The wake up status.</param>
        public delegate void WakeUpClearHandler(PmsConnector sender, WakeUpStatus wakeUpStatus);
        /// <summary>
        /// Occurs when [wakeup status is cleared].
        /// </summary>
        public event WakeUpClearHandler WakeUpCleared;

        /// <summary>
        /// This message should be received when information about the wake-up status is cleared
        /// </summary>
        /// <param name="guestInformation"></param>
        protected virtual void OnWakeUpCleared(WakeUpStatus wakeUpStatus)
        {
            WakeUpClearHandler listener = this.WakeUpCleared;
            if (listener != null)
            {
                listener(this, wakeUpStatus);
            }            
        }

        /// <summary>
        /// Folio logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="folio">Folio model.</param>
        public delegate void FolioUpdateHandler(PmsConnector sender, Folio folio);
        /// <summary>
        /// Occurs when [folio updated].
        /// </summary>
        public event FolioUpdateHandler FolioUpdated;

        /// <summary>
        /// This message should be received when folio for a room is updated
        /// </summary>
        /// <param name="folio"></param>
        protected virtual void OnFolioUpdated(Folio folio)
        {
            FolioUpdateHandler listener = this.FolioUpdated;
            if (listener != null)
            {
                listener(this, folio);
            }            
        }

        /// <summary>
        /// RoomData logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="roomData">RoomData model.</param>
        public delegate void RoomDataUpdateHandler(PmsConnector sender, RoomData roomData);
        /// <summary>
        /// Occurs when [roomData updated].
        /// </summary>
        public event RoomDataUpdateHandler RoomDataUpdated;

        /// <summary>
        /// This message should be received when roomdata for a room is updated
        /// </summary>
        /// <param name="roomData"></param>
        protected virtual void OnRoomDataUpdated(RoomData roomData)
        {
            RoomDataUpdateHandler listener = this.RoomDataUpdated;
            if (listener != null)
            {
                listener(this, roomData);
            }            
        }

        /// <summary>
        /// Guest information edited
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="roomData">RoomData model.</param>
        public delegate void GuestInformationEditedHandler(PmsConnector sender, GuestInformation guestInformation);
        /// <summary>
        /// Occurs when [GuestInformation is edited].
        /// </summary>
        public event GuestInformationEditedHandler GuestInformationEdited;

        /// <summary>
        /// This message should be received when guest information for a room is updated 
        /// </summary>
        /// <param name="roomData"></param>
        protected virtual void OnGuestInformationEdited(GuestInformation guestInformation)
        {
            GuestInformationEditedHandler listener = this.GuestInformationEdited;
            if (listener != null)
            {
                listener(this, guestInformation);
            }            
        }

        #endregion

        #region Push to PMS

        /// <summary>
        /// Indicates a request to have the PMS check a room out. The balance should match the PMS' record for
        /// the account / room number specified. The PMS must initiate an actual Check Out Room (after this
        /// message's required Express Checkout response) if this Express Checkout Request is accepted.
        /// </summary>
        /// <param name="checkoutInfo">The checkout info.</param>
        /// <returns>
        /// Boolean if the checkout succeeded
        /// </returns>
        public abstract bool Checkout(Checkout checkoutInfo);

        /// <summary>
        /// Get the Room Information for a Room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>The Room Information for the Room</returns>
        public abstract List<GuestInformation> GetGuestInformation(string deliverypointNumber);

        /// <summary>
        /// Get the Folio for the Room detailing all charges and credits booked for the Room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>The Folio for the room</returns>
        public abstract List<Folio> GetFolio(string deliverypointNumber);

        /// <summary>
        /// Get the balance of the room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>The balance of the room</returns>
        public abstract decimal GetBalance(string deliverypointNumber);

        /// <summary>
        /// Get the state of the room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>The room state</returns>
        public abstract RoomState GetRoomStatus(string deliverypointNumber);

        /// <summary>
        /// Add one or more FolioItems to the Folio
        /// </summary>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="folioItems">Items to be added</param>
        /// <returns>
        /// True of false indicating if the item could be added
        /// </returns>
        public abstract bool AddToFolio(string deliverypointNumber, List<FolioItem> folioItems);

        /// <summary>
        /// Set the Room State
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <param name="state">State of the room</param>
        /// <returns>True of false indicating if the state could be updated</returns>
        public abstract bool UpdateRoomState(string deliverypointNumber, RoomState state);

        /// <summary>
        /// Sets the wakeup status of a customer
        /// </summary>
        /// <param name="wakeUpStatus"></param>
        /// <returns></returns>
        public abstract bool SetWakeUpStatus(WakeUpStatus wakeUpStatus);

        #endregion

        #region Synchronisation of Webservice Calls

        private readonly object receivedMessagesLock = new object();
        protected ObservableCollection<IPmsMessage> receivedMessages = new ObservableCollection<IPmsMessage>();

        protected int webserviceResponseMessageTimeout = 35000;
        private readonly ConcurrentDictionary<string, ResponseSubject> responseSubjects = new ConcurrentDictionary<string, ResponseSubject>();
        
        protected IPmsMessage SendMessageToPmsSynchronously(IPmsMessage requestMessage, bool throwExceptionOnNull)
        {
            if (!this.eventsHooked)
                throw new TechnicalException("CollectionChanged event was not assigned!");

            string responseMessageSignature = requestMessage.GetResponseMessageSignature();
            
            IPmsMessage messageToReturn;

            // Create response object or get already existing one
            ResponseSubject responseSubject = responseSubjects.GetOrAdd(responseMessageSignature, s =>
                                                                                                  {
                                                                                                      var subject = new ResponseSubject(responseMessageSignature);
                                                                                                      lock(this.receivedMessagesLock)
                                                                                                      {
                                                                                                          this.receivedMessages.CollectionChanged += subject.ReceivedMessagesOnCollectionChanged;
                                                                                                      }

                                                                                                      return subject;
                                                                                                  });
            try
            {
                bool isNewTask;
                Task<ResponseResult> taskResult = responseSubject.GetWaitTask(TimeSpan.FromMilliseconds(this.webserviceResponseMessageTimeout), out isNewTask);

                if (isNewTask)
                {
                    this.LogVerbose("Created new ResponseSubject, call webservice");

                    try
                    {
                        // Submit message to webservice (in that call handle errors and stuff)
                        this.SendMessageToPms(requestMessage);
                    }
                    catch (Exception e)
                    {
                        this.LogError("Uncaught exception occured when interacting with webservice:");
                        this.LogError(e.GetAllMessages(true));

                        throw;
                    }
                }
                else
                {
                    this.LogVerbose("Re-using ResponseSubject");
                }

                // Wait for task to finish
                ResponseResult result = taskResult.Result;
                messageToReturn = result.Result;
                
                // Check if we timed out
                if (result.IsTimeout)
                {
                    this.LogWarning("Wait handle timed out: '{0}' - Waited: {1}ms", requestMessage.MessageDescription, result.Elapsed.TotalMilliseconds);
                }
                else
                {
                    this.LogVerbose("Response of webservice in {0}ms", result.Elapsed.TotalMilliseconds);
                }
            }
            catch (Exception ex)
            {
                this.LogError("Uncaught exception occured when processing result:");
                this.LogError("{0}", ex.GetAllMessages(true));

                throw;
            }
            finally
            {
                ResponseSubject tmp;
                if (responseSubjects.TryRemove(responseMessageSignature, out tmp))
                {
                    // Remove collection changed event registration
                    lock(this.receivedMessagesLock)
                    {
                        this.receivedMessages.CollectionChanged -= responseSubject.ReceivedMessagesOnCollectionChanged;
                    }
                }
            }

            if (throwExceptionOnNull && messageToReturn == null)
            {
                try
                {
                    string messageXml = XmlHelper.Serialize(requestMessage);
                    throw new ObymobiException(PmsConnectorResult.NullResponseFromWebservice, "Null response for message: '{0}'\r\n{1}",
                        requestMessage.MessageDescription, messageXml);
                }
                catch
                {
                    throw new ObymobiException(PmsConnectorResult.NullResponseFromWebservice, "Null response for message: '{0}'", requestMessage.MessageDescription);
                }
            }

            return messageToReturn;
        }

        void receivedMessages_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // We're only interested in ADD, since that means a new message has arrived.
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                if (e.NewItems.Count > 0)
                {
                    foreach (var item in e.NewItems)
                    {
                        IPmsMessage message = null;
                        try
                        {
                            message = (IPmsMessage)item;

                            // Log the Message Signature
                            try
                            {
                                this.LogDebug("Received a message: {0}", message.MessageDescription);
                            }
                            catch (Exception ex)
                            {
                                this.LogWarning(ex.Message);
                            }

                            if (!this.responseSubjects.ContainsKey(message.GetMessageSignature()))
                            {
                                // Some messages can be sent without us asking for it, of those we implemented: Checkout and Checkin,
                                // which are invoked when the PMS get's a guest checked in or out.
                                lock (this.receivedMessagesLock)
                                {
                                    this.receivedMessages.Remove(message);
                                }

                                // Notify connector of received message
                                this.OnPmsMessageReceived(message);
                            }
                        }
                        catch (Exception ex)
                        {
                            if (message != null)
                            {
                                try
                                {
                                    string messageXml = XmlHelper.Serialize(message);
                                    this.LogError("An exception occured while trying to handle '{0}'-message: {1}\r\n{2}\r\n{3}", message.GetType().Name, ex.Message, message.MessageDescription, messageXml);
                                }
                                catch
                                {
                                    this.LogError("An exception occured while trying to handle '{0}'-message: {1}\r\n{2}", message.GetType().Name, ex.Message, message.MessageDescription);
                                }
                            }
                            else
                                this.LogError("Message could not be cased to a valid typed MessageBase object: {0}", ex.Message);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Initialize the webservice / WCF / socket / etc that receives the
        /// push messages from the PMS
        /// </summary>
        protected abstract void InitializePmsReceiver();

        /// <summary>
        /// Calls the webservice.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        protected abstract string SendMessageToPms(IPmsMessage message);

        /// <summary>
        /// Pms Message Received
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="guestInformation">The guest information.</param>
        public delegate void PmsMessageReceivedHandler(PmsConnector sender, IPmsMessage guestInformation);

        /// <summary>
        /// Occurs when a PMS message was received.
        /// </summary>
        public event PmsMessageReceivedHandler PmsMessageReceived;

        /// <summary>
        /// This message should be received when the guest checks into a previously checked-out room.
        /// </summary>
        /// <param name="roomInformation"></param>
        protected virtual void OnPmsMessageReceived(IPmsMessage message)
        {
            // Put in local variable to be safe in multithreaded environment
            PmsMessageReceivedHandler listener = this.PmsMessageReceived;
            if (listener != null)
            {
                listener(this, message);
            }            
        }

        /// <summary>
        /// Closes the connection to the pms system
        /// </summary>
        public abstract void StopReceiving();

        private class ResponseSubject
        {
            private readonly AutoResetEvent waitHandle;
            private readonly string signature;

            private Task<ResponseResult> waitTask;
            private volatile IPmsMessage resultMessage;
            
            public ResponseSubject(string signature)
            {
                this.signature = signature;
                this.waitHandle = new AutoResetEvent(false);
            }

            public Task<ResponseResult> GetWaitTask(TimeSpan timeout, out bool isNewTask)
            {
                lock(this)
                {
                    isNewTask = waitTask == null;
                    
                    return waitTask ?? (waitTask = Task.Factory.StartNew(() => WaitForResult(timeout)));
                }
            }

            private ResponseResult WaitForResult(TimeSpan timeout)
            {
                ResponseResult result = new ResponseResult();

                // Wait for the signal, timeout after the timeout interval
                Stopwatch stopwatchCall = new Stopwatch();
                stopwatchCall.Start();
                if (!waitHandle.WaitOne(timeout))
                {
                    result.IsTimeout = true;
                }
                else
                {
                    result.IsTimeout = false;
                    result.Result = resultMessage;
                }
                stopwatchCall.Stop();
                result.Elapsed = stopwatchCall.Elapsed;

                return result;
            }

            public void ReceivedMessagesOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
            {
                if (e.Action != NotifyCollectionChangedAction.Add || e.NewItems.Count <= 0)
                {
                    return;
                }

                foreach (var item in e.NewItems)
                {
                    try
                    {
                        var message = (IPmsMessage)item;
                        if (message.GetMessageSignature().Equals(this.signature, StringComparison.CurrentCultureIgnoreCase))
                        {
                            resultMessage = message;
                            waitHandle.Set();
                            break;
                        }
                    }
                    catch (Exception)
                    {
                        // ignore
                    }
                }
            }
        }

        private class ResponseResult
        {
            public IPmsMessage Result { get; set; }
            public bool IsTimeout { get; set; }
            public TimeSpan Elapsed { get; set; }
        }

        #endregion
    }
}
