﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Logic.PMS
{
    /// <summary>
    /// Exception class for when the communication with a POS fails
    /// </summary>
    public class PmsException : ObymobiException
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.POS.POSException class
        /// </summary>	
        public PmsException(PmsError errorType, string message)
            : base(errorType, message)
        {
            this.errorType = errorType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliverypointLockedException"/> class.
        /// </summary>
        /// <param name="errorType">Type of the error.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public PmsException(PmsError errorType, string message, params object[] args)
            : base(errorType, message, args)
        {
            this.errorType = errorType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliverypointLockedException"/> class.
        /// </summary>
        /// <param name="errorType">Type of the error.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public PmsException(PmsError errorType, Exception innerException, string message, params object[] args)
            : base(errorType, innerException, message, args)
        {
            this.errorType = errorType;
        }

        private PmsError errorType = PmsError.UnspecifiedError;
        /// <summary>
        /// Gets or sets the type of the error.
        /// </summary>
        /// <value>
        /// The type of the error.
        /// </value>
        public PmsError ErrorType
        {
            get
            {
                return this.errorType;
            }
            set
            {
                this.errorType = value;
            }
        }

        /// <summary>
        /// Throws the typed error for a POS exception (i.e. DeliverypointLocked, Connectivety, etc.), otherwise the generic is thrown.
        /// </summary>
        /// <param name="errorType">Type of the error.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public static void ThrowTyped(PmsError errorType, Exception innerException, string message, params object[] args)
        {
            if (args == null)
                args = new string[1] { string.Empty };

            throw new PmsException(errorType, innerException, message, args);
        }

        #endregion
    }
}
