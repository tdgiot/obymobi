﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Dionysos.Diagnostics;

namespace Obymobi.Logic.PMS.Comtrol
{
    /// <summary>
    /// UhllMessage
    /// </summary>
    public class UhllMessage : IPmsMessage
    {

        #region Errors enum
        /// <summary>
        /// UhllResult enums
        /// </summary>
        public enum UhllResult
        {
            /// <summary>
            /// The message header is incorrect
            /// </summary>
            MessageHeaderIncorrect = 200,
            /// <summary>
            /// The message field definition is incorrect
            /// </summary>
            MessageFieldDefinitionIncorrect = 201,
            /// <summary>
            /// The message field is not supported
            /// </summary>
            MessageFieldNotSupported = 202,
            /// <summary>
            /// The message is wrongly formatted
            /// </summary>
            MessageWronglyFormatted = 203,
            /// <summary>
            /// Values are missing
            /// </summary>
            MissingValues = 204,
            /// <summary>
            /// Field data is too big
            /// </summary>
            FieldDataToBig = 205
        }
        #endregion

        #region Fields

        private Dictionary<DataField, object> fieldValues = new Dictionary<DataField, object>();

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="UhllMessage"/> class.
        /// </summary>
        public UhllMessage()
        {
            this.MessageType = Comtrol.MessageType.NotSupported;
            this.SequenceNumber = 9999;
            this.TransactionId = -1;
        }

        public UhllMessage(MessageType messageType)
        {
            this.MessageType = messageType;
            this.SequenceNumber = 9999;
            this.TransactionId = -1;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Parses the specified message string.
        /// </summary>
        /// <param name="messageString">The message string.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public static bool Parse(string messageString, out UhllMessage message)
        {
            bool success = true;
            message = new UhllMessage();

            // Parse the Header
            UhllMessage.ParseMessageHeader(messageString, message);

            if (message.MessageType != MessageType.NotSupported)
            {
                // Parse the Fields
                UhllMessage.ParseMessageFields(messageString, message);
            }

            return success;
        }

        private static void ParseMessageHeader(string messageString, UhllMessage message)
        {
            // Parse the header
            // Field            Positions   Used by us?
            // Message Type (2) 0-1         Yes
            // DMM (3)          2-4         No
            // Reserved (2)     5-6         No
            // Transaction (4)  7-10        Yes
            // Sequence (4)     11-14       Yes
            int messageTypeAsInt, transactionId, sequenceNumber;

            // Parse header fields and exception on problem

            try
            {
                // Message Type
                if (!int.TryParse(messageString.Substring(0, 2), out messageTypeAsInt))
                    throw new ObymobiException(UhllResult.MessageHeaderIncorrect, "Header: Message Type not an int: '{0}' - Message: {1}", messageString.Substring(0, 2), message);

                MessageType messageType;
                if (!EnumUtil.TryParse(messageTypeAsInt, out messageType))
                    messageType = MessageType.NotSupported;

                message.MessageType = messageType;

                // TransactionId
                if (!int.TryParse(messageString.Substring(7, 4), out transactionId))
                    throw new ObymobiException(UhllResult.MessageHeaderIncorrect, "Header: Transaction Id not an int: '{0}' - Message: {1}", messageString.Substring(7, 4), message);

                message.TransactionId = transactionId;

                // Sequence number
                if (!int.TryParse(messageString.Substring(11, 4), out sequenceNumber))
                    throw new ObymobiException(UhllResult.MessageHeaderIncorrect, "Header: Sequence Number not an int: '{0}' - Message: {1}", messageString.Substring(11, 4), message);

                message.SequenceNumber = sequenceNumber;
            }
            catch (Exception ex)
            {
                throw new ObymobiException(UhllResult.MessageWronglyFormatted, ex, "Message: {0}", messageString);
            }

        }

        private static void ParseMessageFields(string messageString, UhllMessage message)
        {
            string fields = messageString.Substring(15); // Strip the header away.

            // Field    Positions   
            // DFID     0-2         3-digit descriptor for the type of data this one message element represents.
            // Length   3-5         3-digit descriptor for the length of the data that this element represents.
            // Data     > 5         The data itself
            int position = 0;
            while (position < fields.Length)
            {
                if (fields[position] == (Char)3)
                {
                    // We have encountered the end of the message
                    break;
                }

                int dfid, length;
                DataField field;

                // Parse Dfid int and check if the field is supported
                if (!int.TryParse(fields.Substring(position, 3), out dfid))
                    throw new ObymobiException(UhllResult.MessageFieldDefinitionIncorrect, "Field incorrect: DFID not an int. '{0}' - Message: {1}", fields.Substring(position, 3), message);
                else if (!EnumUtil.TryParse(dfid, out field))
                {
                    // Be tolerant for 'newer' fields that were not yet in our specification
                    Debug.WriteLine(string.Format("Field incorrect: DFID not supported: '{0}' - Message: {1}", dfid, message));
                    throw new ObymobiException(UhllResult.MessageFieldDefinitionIncorrect, "Field incorrect: DFID not supported: '{0}' - Message: {1}", dfid, message);
                }
                position += 3;


                if (!int.TryParse(fields.Substring(position, 3), out length))
                    throw new ObymobiException(UhllResult.MessageFieldDefinitionIncorrect, "Field incorrect: Length not an int. '{0}' - Message: {1}", fields.Substring(position + 2, 3), message);
                position += 3;

                string data = fields.Substring(position, length);
                position += length;

                // Set the value
                message.SetValue(field, data);
            }
        }

        /// <summary>
        /// Toes the uhll command string.
        /// </summary>
        /// <returns></returns>
        public string ToUhllCommandString()
        {
            return Encoding.ASCII.GetString(this.ToUhllCommand(true));
        }

        /// <summary>
        /// Toes the uhll command.
        /// </summary>
        /// <returns></returns>
        public byte[] ToUhllCommand()
        {
            return this.ToUhllCommand(false);
        }

        /// <summary>
        /// Toes the uhll command.
        /// </summary>
        /// <param name="readableAsciCharacters">if set to <c>true</c> [readable asci characters].</param>
        /// <returns></returns>
        public byte[] ToUhllCommand(bool readableAsciCharacters)
        {
            StringBuilder sb = new StringBuilder();

            if (this.MessageType == Comtrol.MessageType.NotSupported)
                throw new ObymobiException(UhllResult.MissingValues, "MessageType");
            else if (this.TransactionId < 0)
                throw new ObymobiException(UhllResult.MissingValues, "TransactionId");
            else if (this.SequenceNumber < 0)
                throw new ObymobiException(UhllResult.MissingValues, "SequenceNumber");

            // Header
            if (readableAsciCharacters)
                sb.Append("<STX>"); // STX
            else
                sb.Append((char)2); // STX

            // Message Type
            sb.Append(((int)this.MessageType).ToString().PadLeft(2, '0'));

            // DMM
            sb.Append(TcpClient.DMM.ToString().PadLeft(3, '0'));

            // Reserved
            sb.Append("00");

            // TransactionId
            sb.Append(this.TransactionId.ToString().PadLeft(4, '0'));

            // Sequence Number
            sb.Append(this.SequenceNumber.ToString().PadLeft(4, '0'));

            // The fields
            var orderedFields = this.fieldValues.Keys.OrderBy(f => (int)f);
            foreach (var field in orderedFields)
            {
                // Dfid
                sb.Append(((int)field).ToString().PadLeft(3, '0'));

                // Retrieve field data
                string data;
                this.TryGetValue(field, out data);

                // Data length
                if (data.Length > 999)
                    throw new ObymobiException(UhllResult.FieldDataToBig, "The data for field '{0}' is larger than 999. Data: '{1}'", field.ToString(), data);
                else
                    sb.Append(data.Length.ToString().PadLeft(3, '0'));

                // Data
                sb.Append(data);
            }

            if (readableAsciCharacters)
                sb.Append("<ETX>"); // ETX            
            else
                sb.Append((char)3); // ETX            

            // LRC
            if (readableAsciCharacters)
            {
                sb.Append("<LRC>");
            }

            System.Text.ASCIIEncoding ascii = new ASCIIEncoding();
            string message = sb.ToString();
            var bytesToSend = ascii.GetBytes(message).ToList();

            if (!readableAsciCharacters)
            {
                var bytesToLrc = ascii.GetBytes(message.Substring(1)).ToList();
                var lrc = TcpClient.CalculateLRC(bytesToLrc);
                bytesToSend.Add(lrc);
                Debug.WriteLine(string.Format("Message: {0} - Lrc: {1}", sb.ToString(), Encoding.ASCII.GetString(new byte[] { lrc })));
            }

            return bytesToSend.ToArray();
        }

        public string GetMessageSignature()
        {
            return this.TransactionId.ToString();
        }

        public string GetResponseMessageSignature()
        {
            return this.TransactionId.ToString();
        }

        public string MessageDescription
        {
            get
            {
                string roomNumber;
                if (!this.TryGetValue(DataField.RoomNumber, out roomNumber))
                    roomNumber = string.Empty;
                return StringUtil.FormatSafe("Type: '{0}' - Room: '{1}' - Transaction: '{2}'",
                    this.MessageType.ToString(), roomNumber, this.TransactionId);
            }
        }

        #endregion

        #region Header

        /// <summary>
        /// 
        /// </summary>
        public MessageType MessageType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int TransactionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int SequenceNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="UhllMessage"/> is sent.
        /// </summary>
        /// <value>
        ///   <c>true</c> if sent; otherwise, <c>false</c>.
        /// </value>
        public bool Sent { get; set; }

        /// <summary>
        /// Gets or sets the resend count.
        /// </summary>
        /// <value>
        /// The resend count.
        /// </value>
        public int ResendCount { get; set; }

        #endregion

        #region Data Fields

        private List<UhllMessage> uhllMessages = new List<UhllMessage>();

        /// <summary>
        /// Gets or sets the uhll messages of the transaction if there's more than one.
        /// This instance is the first message and the messages in this collection are the following
        /// ones        
        /// </summary>
        /// <value>
        /// The uhll messages.
        /// </value>
        public List<UhllMessage> MessagesInTransactionExcludingSelf
        {
            get
            {
                return this.uhllMessages;
            }
        }

        public List<UhllMessage> MessagesInTransactionIncludingSelf
        {
            get
            {
                List<UhllMessage> msgs = new List<UhllMessage>();
                msgs.Add(this);
                msgs.AddRange(this.MessagesInTransactionExcludingSelf);
                return msgs;
            }
        }

        public string Roomnumber
        {
            get
            {
                string roomNumber;
                if (!this.TryGetValue(DataField.RoomNumber, out roomNumber))
                {
                    roomNumber = string.Empty;
                }
                return roomNumber;
            }
        }


        /// <summary>
        /// Toes the key value pairs text.
        /// </summary>
        /// <returns></returns>
        public string ToKeyValuePairsText()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var field in this.fieldValues)
            {
                sb.AppendFormatLine("{0}: {1}", field.Key.ToString(), field.Value.ToString());
            }
            return sb.ToString();
        }

        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="value">The value.</param>
        public void SetValue(DataField field, object value)
        {
            if (this.fieldValues.ContainsKey(field))
                this.fieldValues[field] = value;
            else
                this.fieldValues.Add(field, value);
        }

        /// <summary>
        /// Tries to get the value from the Fields
        /// </summary>
        /// <typeparam name="T">The type of the value to get.</typeparam>
        /// <param name="field">The field to retrieve.</param>
        /// <param name="value">Returns the value, if found and type matches; otherwise returns the default value of the type.</param>
        /// <param name="exceptionWhenNotFound">if set to <c>true</c> [exception when not found].</param>
        /// <returns>
        /// true if the key and corresponding value exists in the QueryString; otherwise false.
        /// </returns>
        public bool TryGetValue<T>(DataField field, out T value, bool exceptionWhenNotFound)
        {
            return TryGetValue<T>(field, out value, null, exceptionWhenNotFound);
        }

        /// <summary>
        /// Tries to get the value from the Fields
        /// </summary>
        /// <typeparam name="T">The type of the value to get.</typeparam>
        /// <param name="field">The field to retrieve.</param>
        /// <param name="value">Returns the value, if found and type matches; otherwise returns the default value of the type.</param>
        /// <returns>true if the key and corresponding value exists in the QueryString; otherwise false.</returns>
        public bool TryGetValue<T>(DataField field, out T value)
        {
            return TryGetValue<T>(field, out value, null, false);
        }

        /// <summary>
        /// Tries to get the value from the Fields
        /// </summary>
        /// <typeparam name="T">The type of the value to get.</typeparam>
        /// <param name="field">The field.</param>
        /// <param name="value">Returns the value, if found and type matches; otherwise returns the default value of the type.</param>
        /// <param name="provider">An IFormatProvider interface implementation that supplies culture-specific formatting information.</param>
        /// <param name="exceptionWhenNotFound">if set to <c>true</c> [exception when not found].</param>
        /// <returns>
        /// true if the key and corresponding value exists in the QueryString; otherwise false.
        /// </returns>
        public bool TryGetValue<T>(DataField field, out T value, IFormatProvider provider, bool exceptionWhenNotFound)
        {
            object storedValue = null;
            if (this.fieldValues.ContainsKey(field))
            {
                storedValue = this.fieldValues[field];
            }
            else if (exceptionWhenNotFound)
                throw new ObymobiException(UhllResult.MissingValues, "Field: '{0}' has no value", field.ToString());


            if (storedValue != null)
            {
                // Value is found, try to change the type
                try
                {
                    value = (T)Convert.ChangeType(storedValue, typeof(T), provider);
                    return true;
                }
                catch
                {
                    // Type could not be changed
                }
            }

            // Value is not found, return default
            value = default(T);
            return false;
        }

        #endregion

    }
}
