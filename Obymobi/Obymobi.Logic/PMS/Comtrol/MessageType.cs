﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.PMS.Comtrol
{
    /// <summary>
    /// MessageType enums
    /// </summary>
    public enum MessageType : int
    {
        // ONLY Add message types that we support, don't just complete this based
        // on the documentation

        /// <summary>
        /// This message should be sent to check the first guest into a previously checked-out room. Use this 
        /// message to check in only the primary (first) guest in a room - see Check In Guest for adding 
        /// subsequent guests. If there are multiple Station, Voice DID or Data DID numbers to associate with this 
        /// Room, send additional Check In Room messages for each additional piece of information with the 
        /// appropriate data all as part of the same initial Check In Room transaction.
        /// </summary>
        CheckInRoom = 14,

        /// <summary>
        /// This message should be sent to check all guests out of a checked-in room.
        /// This should NOT be used to check out individual guests from a room - use Check Out Guest for that
        /// purpose.
        /// If a multiple message Check In Room transaction was used to check this room in (due to multiple
        /// station numbers being associated with the room), then one additional Check Out Room message
        /// should be sent for each additional Station Number checked in,(Voice DID, Data DID etc…) as
        /// part of the same Check Out Room transaction.
        /// </summary>
        CheckoutRoom = 15,

        /// <summary>
        /// Sent to notify of a change in the clean/dirty status of a guest room. This message can be sent by the
        /// PMS or a device.
        /// </summary>
        MaidCode = 17,

        /// <summary>
        /// Used to move an already checked-in guest from one room into a different, unoccupied room. If the
        /// second room is already occupied, use the Swap message instead of this one.
        /// </summary>
        RoomMove = 18,

        /// <summary>
        /// This message is used to notify of a change in the Do Not Disturb status for a particular room / station
        /// number.
        /// When sent by the PMS, this message will change the Do Not Disturb status of the indicated room /
        /// station.
        /// When received by the PMS, this message indicates that the Do Not Disturb status for the indicated
        /// room / station has been updated by the guest and the PMS should note that change.
        /// </summary>
        DoNotDisturb = 19,

        /// <summary>
        /// Generic error message that is used to report any trouble or error condition encountered by either
        /// Lodging Link or a GSS device. This message is sent primarily for informational purposes and usually 
        /// can not be acted on by the PMS programmer any way other than by notifying the PMS operator that an
        /// error has occurred.
        /// If the DMM DFID matches the PMS, this is an indication that Lodging Link has discovered an error with
        /// the PMS' implementation of UHLL either at the message, language or protocol level and should be
        /// brought to the attention of the programmer.
        /// </summary>
        Error = 24,

        /// <summary>
        /// Response to request to describe each of the checked-in guests that match the requested room /
        /// account number (given in the Request Folio Information message). One of these messages should be
        /// sent (all as part of the same UHLL Transaction) for each guest that meets the given criteria.
        /// </summary>
        FolioInformation = 25,

        /// <summary>
        /// Used to describe the individual elements that make up the total charges for the account / room / station
        /// (given in the Request Folio Items message). One of these messages should be sent (all as part of the
        /// same UHLL Transaction) for each item in the folio.
        /// </summary>
        FolioItem = 26,

        /// <summary>
        /// Sent by PMS to indicate the balance for the indicated room and/or account. This message is only sent
        /// after the individual Folio Item messages as part of the response to Request Folio Items.
        /// </summary>
        Balance = 27,

        /// <summary>
        /// Sent in response to an Express Checkout Request to indicate acceptance or rejection of that request.
        /// An explicit Check Out Room (as a separate transaction) must be sent by the PMS pursuant to a this 
        /// message if the PMS wishes to complete the check out procedure.
        /// </summary>
        ExpressCheckout = 34,

        /// <summary>
        /// Sent to inquire on the status of a particular room / station. If this message is received by the PMS, it
        /// should respond with a Room Status message for all rooms that match the indicated criteria. This
        /// message may also be sent by the PMS to request room status information (not common), in which case
        /// it will receive a Room Status message for each room / station indicated in the original request.
        /// </summary>
        RequestRoomStatus = 35,

        /// <summary>
        /// Request for the PMS to provide basic occupancy information for the indicated station / room / account.
        /// All guests that match the indicated criteria should be returned using a Folio Information multiplemessage
        /// transaction.
        /// </summary>
        RequestFolioInformation = 37,

        /// <summary>
        /// One-way posting from a GSS device requesting the PMS to charge the indicated account / station /
        /// room for the goods or services described.
        /// </summary>
        Post = 38,

        /// <summary>
        /// Indicates a request to have the PMS check a room out. The balance should match the PMS' record for
        /// the account / room number specified. The PMS must initiate an actual Check Out Room (after this
        /// message's required Express Checkout response) if this Express Checkout Request is accepted.
        /// </summary>
        ExpressCheckoutRequest = 39,

        /// <summary>
        /// Detailed request of the billing line-items and total balance associated with the indicated station / room /
        /// account. All folio items that match the indicated criteria should be returned using zero or more Folio
        /// Item messages followed by a Balance message, all part of the same multiple-message transaction.
        /// </summary>
        RequestFolioItems = 40,

        /// <summary>
        /// Sent in response to a Request Room Status to indicate the clean / dirty status of the indicated station /
        /// room number.
        /// </summary>
        RoomStatus = 43,

        /// <summary>
        /// Message-level positive ACKnowledgement sent to indicate successful receipt of the associated UHLL
        /// message. The Transaction ID and Sequence Number of the ACK_MSG will reflect the message being
        /// ACKnowledged.The DFID 178 listed below is reserved for future use, and should not be filled in when
        /// sending this message
        /// </summary>
        Ack_Msg = 45,

        /// <summary>
        /// Message-level Negative ACKnowledgement sent to indicate unsuccessful receipt of the associated
        /// UHLL message. The Transaction ID and Sequence Number of the NAK_MSG will reflect the message
        /// being Negatively ACKnowledged.
        /// The DFID 178 listed below is reserved for future use, and should not be filled in when sending this
        /// message.
        /// </summary>
        Nak_Msg = 46,

        /// <summary>
        /// Sent by the PMS after two guests have swapped occupied rooms.
        /// Note: This message should be sent only after the PMS has already executed the swap for the guests.
        /// This message should then be sent to notify Lodging Link of the changes which are reflected in the PMS
        /// database for the new room assignments.
        /// </summary>
        Swap = 52,

        /// <summary>
        /// Sent to modify the available credit for a guest to charge against. Used with any one-way posting device
        /// that allows credit limits to be set. This message is used to modify the Restriction Limit that may have
        /// previously been established for this station / room / account through the Check In Room or Check In
        /// Guest message.
        /// </summary>
        CreditLimit = 54,

        /// <summary>
        /// Used to establish the default, preferred language for the indicated room / station.
        /// </summary>
        Language = 63,

        /// <summary>
        /// Used to change the VIP status of the indicated guest's room.
        /// </summary>
        Vip = 64,

        /// <summary>
        /// Not supported
        /// </summary>
        NotSupported = 99999
    }
}
