﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using Dionysos.Diagnostics;
using System.Threading;
using Dionysos;
using System.Collections.ObjectModel;

namespace Obymobi.Logic.PMS.Comtrol
{
    /// <summary>
    /// TcpClient
    /// </summary>
    public class TcpClient
    {
        /// <summary>
        /// TcpClientResult enums
        /// </summary>
        public enum TcpClientResult
        {
            /// <summary>
            /// Acknowledgement received for unknown message
            /// </summary>
            ReceivedAcknowledgementForUnknownMessage = 200,
            /// <summary>
            /// Connection failed
            /// </summary>
            ConnectingFailed = 201
        }

        #region Fields

        /// <summary>
        /// DMM
        /// </summary>
        public const int DMM = 999;
        private int transactionId = 0;
        private int reconnectAttempts = 0;
        private Socket socket;
        private byte[] asyncReceiveBuffer = new byte[1];
        private ComtrolConnector pmsConnector;
        private IPEndPoint ipEndPoint;
        private List<UhllMessage> pendingMessages = new List<UhllMessage>();
        private bool isConnecting = false;
        private readonly object connectLock = new Object();
        private bool isReceiving = false;
        private readonly object receiveLock = new Object();
        private bool isSending = false;
        private readonly object sendLock = new Object();
        //private List<UhllMessage> receivedMessages = new List<UhllMessage>();
        private ObservableCollection<IPmsMessage> receivedMessages;
        private Dictionary<int, List<UhllMessage>> messageTransactions = new Dictionary<int, List<UhllMessage>>();

        #endregion

        #region Constructor & initializing logic

        /// <summary>
        /// Initializes a new instance of the <see cref="TcpClient"/> class.
        /// </summary>
        /// <param name="pmsConnector">The PMS connector.</param>
        /// <param name="ipAddress">The ip address.</param>
        /// <param name="port">The port.</param>
        public TcpClient(ComtrolConnector pmsConnector, String ipAddress, int port, ObservableCollection<IPmsMessage> receivedMessages)
        {
            this.pmsConnector = pmsConnector;
            this.receivedMessages = receivedMessages;

            // Get the IP end point to connect to
            this.ipEndPoint = new IPEndPoint(IPAddress.Parse(ipAddress), port);

            // Start receiving (does also connect)
            this.BeginReceive();
        }

        #endregion

        #region Message receiving and processing

        private bool SocketIsConnected
        {
            get
            {
                return (this.socket != null && this.socket.Connected);
            }
        }

        private void Connect()
        {
            lock (this.connectLock)
            {
                // Don't allow multiple connects at the same time
                if (this.isConnecting)
                    return;

                if (this.reconnectAttempts > 10)
                    throw new ObymobiException(TcpClientResult.ConnectingFailed, "Connection to: {0}:{1} failed.", this.ipEndPoint.Address.ToString(), this.ipEndPoint.Port);

                if (!this.SocketIsConnected)
                {
                    // Initialize the socket
                    this.isConnecting = true;
                    this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                }
                else
                {
                    this.Connected(null);
                }

                // Connect A-sync
                AsyncCallback onConnected = Connected;
                this.socket.BeginConnect(this.ipEndPoint, onConnected, null);
            }
        }

        private void Connected(IAsyncResult result)
        {
            try
            {
                lock (this.connectLock)
                {
                    if (result != null)
                    {
                        this.socket.EndConnect(result);
                    }

                    this.isConnecting = false;

                    if (this.socket.Connected)
                    {
                        this.pmsConnector.LogDebug("TCP Client for Comtrol is Connected!");
                        reconnectAttempts = 0;

                        // Open listener
                        this.BeginReceive();

                        // Process queue
                        this.ProcessSendQueue();
                    }
                    else
                    {
                        this.pmsConnector.LogWarning("Reconnect attempt: {0}", reconnectAttempts);
                        reconnectAttempts++;
                    }
                }
            }
            catch (Exception ex)
            {
                this.pmsConnector.LogError("Exception in Connected: {0}", ex.Message);
                this.pmsConnector.LogError("Reconnect attempt: {0}", reconnectAttempts);
                reconnectAttempts++;

                throw;
            }
        }

        private void BeginReceive()
        {
            // Check if we're connected
            if (!this.SocketIsConnected)
            {
                this.pmsConnector.LogDebug("Socket was not connected, reconnect");
                this.Connect();
            }
            else
            {
                lock (this.receiveLock)
                {
                    // Only start receiving when we're not yet receiving. 
                    // Extra protecting because this should not be happening.
                    if (!isReceiving)
                    {
                        // Start Async receiver
                        AsyncCallback onMessageReceived = ReceiveData;
                        // GK On purpose: Will this work? And if so, we need to protect against that!                
                        this.socket.BeginReceive(asyncReceiveBuffer, 0, asyncReceiveBuffer.Length, SocketFlags.None, onMessageReceived, null);
                    }
                    else
                    {

                        this.pmsConnector.LogInfo("Attempted to receive, but already receiving.");
                    }
                }
            }
        }

        private void ReceiveData(IAsyncResult result)
        {
            // First end this a-sync call
            this.pmsConnector.LogVerbose("Start receiving data!");
            int read = this.socket.EndReceive(result);
            UhllMessage message = null;
            bool messageAcknowledged = false;
            try
            {
                if (read == 0)
                    return;

                // Got data, process.
                char openingCharacter = Encoding.ASCII.GetString(this.asyncReceiveBuffer)[0];
                if (openingCharacter == (char)2) // Char 2 = STX = Start Text 
                {
                    string messageString;

                    // Receive remainder of the message from the Socket connection
                    bool canProceed = false;
                    if (!this.ReceiveMessage(out messageString))
                    {
                        try
                        {
                            // Try to parse anyway, maybe only LRC is wrong.
                            UhllMessage.Parse(messageString, out message);
                            canProceed = true;

                            // Report the Error
                            UhllMessage errorReport = new UhllMessage();
                            errorReport.MessageType = MessageType.Error;
                            errorReport.SetValue(DataField.SequenceNumber, message.SequenceNumber);
                            errorReport.SetValue(DataField.TransactionId, message.TransactionId);
                            errorReport.SetValue(DataField.ErrorCode, "0000");
                            errorReport.SetValue(DataField.TextString, "Couldn't parse message (wrong lrc?)");
                            errorReport.SetValue(DataField.DMM, TcpClient.DMM.ToString());

                            this.QueueMessage(errorReport);
                        }
                        catch
                        {
                            throw new Exception(string.Format("Message receive failed: '{0}'", messageString));
                        }
                    }
                    else
                        canProceed = true;

                    if (canProceed)
                    {
                        // Parse message
                        this.pmsConnector.LogVerbose("Parse Message");
                        UhllMessage.Parse(messageString, out message);

                        // Acknowledge we got the message
                        this.pmsConnector.LogVerbose("Acknowledge Message: {0}.{1} {2}", message.TransactionId, message.SequenceNumber, message.MessageType);
                        this.QueueAcknowledgementMessage(message, true);
                        messageAcknowledged = true;

                        // Process message
                        this.pmsConnector.LogVerbose("Process Message: {0}.{1} {2}", message.TransactionId, message.SequenceNumber, message.MessageType);
                        this.ProcessMessage(message);
                    }

                    else
                    {
                        // No idea, disregard
                        this.pmsConnector.LogWarning("Received a message with an incorrect opening karakter: " + openingCharacter);
                    }
                }
            }
            catch (ObymobiException oex)
            {
                // Queue non acknowledge
                if (!messageAcknowledged &&
                    message != null &&
                    message.TransactionId > 0 &&
                    message.SequenceNumber > 0)
                {
                    this.QueueAcknowledgementMessage(message, false);
                }

                // Queue error
                this.QueueErrorMessage(message, oex.ErrorEnumValue);
                this.pmsConnector.LogError("ObymobiException in ReceiveData: " + oex.Message);

                // This was a manageable error, just ignore and wait for other messages.
                // throw;
            }
            catch (Exception ex)
            {
                this.pmsConnector.LogError("Exception in ReceiveData: " + ex.Message);
                throw;
            }
            finally
            {
                lock (this.receiveLock)
                {
                    // Always restart the listener                
                    this.isReceiving = false;
                    this.BeginReceive();
                }
            }

        }

        private void ProcessMessage(UhllMessage message)
        {
            // Locally in the TcpClient we do the 'network level' processing
            // further functional processing is delegated to the ComtrolConnectio
            if (message.MessageType == MessageType.Ack_Msg || message.MessageType == MessageType.Nak_Msg)
            {
                UhllMessage pendingMessage = null;

                // All operations on the pending message must synchronous.                
                lock (this.pendingMessages)
                {
                    pendingMessage = this.pendingMessages.FirstOrDefault(m => m.TransactionId == message.TransactionId);
                }
                if (pendingMessage != null)
                {
                    if (message.MessageType == MessageType.Ack_Msg)
                    {
                        // All operations on the pending message must synchronous.                
                        lock (this.pendingMessages)
                        {
                            this.pendingMessages.Remove(pendingMessage);
                        }
                    }
                    else
                    {
                        // Resend 
                        if (message.ResendCount >= 2)
                        {
                            // Message Failed too many times
                            this.pmsConnector.LogError(string.Format("Message failed: {0}", message.MessageType));

                            // All operations on the pending message must synchronous.
                            lock (this.pendingMessages)
                            {
                                this.pendingMessages.Remove(pendingMessage);
                            }
                        }
                        else
                        {
                            // Try to resend
                            message.ResendCount++;
                            message.Sent = false;
                            this.ProcessSendQueue();
                        }
                    }
                }
                else
                    this.pmsConnector.LogWarning(string.Format("Retrieved acknowlegdement for unknown message with TransactionId '{0}'", message.TransactionId));
            }
            else
            {
                // Only process when it's the last message of a batch.
                if (message.SequenceNumber == 9999)
                {
                    if (this.messageTransactions.ContainsKey(message.TransactionId))
                    {
                        this.pmsConnector.LogVerbose("Received last message of Transaction '{1}' - Now process.", message.SequenceNumber, message.TransactionId);
                        // Message is part of transaction, process all transaction messages at once.
                        this.messageTransactions[message.TransactionId].Add(message);

                        // Combine to one message 
                        var messagesInTransaction = this.messageTransactions[message.TransactionId];
                        var firstMessage = messagesInTransaction[0];
                        for (int i = 1; i < messagesInTransaction.Count; i++)
                        {
                            firstMessage.MessagesInTransactionExcludingSelf.Add(messagesInTransaction[i]);
                        }

                        this.messageTransactions.Remove(message.TransactionId);
                        this.receivedMessages.Add(firstMessage);
                    }
                    else
                    {
                        // Functional handling is done in the ComtrolConnector
                        this.pmsConnector.LogVerbose("Processing Single Message '{0}' with TransactionId '{1}'", message.SequenceNumber, message.TransactionId);
                        this.receivedMessages.Add(message);
                    }
                }
                else
                {
                    // Add to the transaction, because it's not the last message.
                    this.pmsConnector.LogVerbose("Received message '{0}' - Which is part of Transaction: '{1}' - Now process.", message.SequenceNumber, message.TransactionId);
                    if (!this.messageTransactions.ContainsKey(message.TransactionId))
                        this.messageTransactions.Add(message.TransactionId, new List<UhllMessage>());

                    this.messageTransactions[message.TransactionId].Add(message);
                }
            }
        }

        private bool ReceiveMessage(out string message)
        {
            message = string.Empty;
            bool? success = null;
            int bufferSize = 1024;
            bool keepReceiving = true;
            int nothingReceivedIterations = 0;
            StringBuilder sb = new StringBuilder();
            List<byte> receivedBytes = new List<byte>();

            // Start receiving
            while (keepReceiving && !success.HasValue)
            {
                this.pmsConnector.LogDebug("Start receiving message");
                // Receive data from socket                
                byte[] receivedData = new byte[1024];
                this.socket.ReceiveTimeout = 2000;
                int receivedSize = this.socket.Receive(receivedData, 0, bufferSize, 0);

                // Convert recevied to usable string and append to result
                if (receivedSize > 0)
                {
                    this.pmsConnector.LogVerbose("Received more than 0 bytes");
                    Array.Resize(ref receivedData, receivedSize);
                    string encodedString = Encoding.ASCII.GetString(receivedData);

                    // Keep array of bytes for LRC (Longitudinal Redunancy Check)
                    receivedBytes.AddRange(receivedData);

                    if (TestUtil.IsPcDeveloper)
                    {
                        string byteValues = "";
                        foreach (var bijt in receivedBytes)
                        {
                            byteValues += string.Format("[{0}='{1}']", bijt.ToString(), Encoding.ASCII.GetString(new byte[] { bijt }));
                        }
                        this.pmsConnector.LogVerbose("Bytes received: " + receivedBytes.Count);
                        this.pmsConnector.LogVerbose("Content: " + byteValues);
                        byte lrc = TcpClient.CalculateLRC(receivedBytes.Take(receivedBytes.Count - 1).ToList());
                        string lrcResultText = String.Format("Byte value: '{0}', ASCII '{1}'", lrc.ToString(), Encoding.ASCII.GetString(new byte[] { lrc }));
                        this.pmsConnector.LogVerbose("Lrc over (all Bytes - 1): " + lrcResultText);
                        this.pmsConnector.LogVerbose("Encoded ASCII string of all bytes: " + encodedString);
                    }

                    // Add to result of the message                         
                    sb.Append(encodedString);
                    this.pmsConnector.LogDebug("Received: " + sb.ToString());

                    string receivedTillNow = sb.ToString();

                    if (receivedTillNow.Last() == (char)3)
                    {
                        // The message ended with terminator character whichs means
                        // the LRC value is still on it's way.
                        this.pmsConnector.LogDebug("Terminator received as last character, which means still wiat for LRC");
                    }
                    else if (receivedTillNow.Contains((char)3))
                    {
                        this.pmsConnector.LogVerbose("Now we should be able to validate the message");
                        // It included the terminator and that's not the last character, which means we have the complete message                        
                        // We have received the terminator
                        // Calculate LRC

                        // Remove the LRC from the message, becuase we calculate it up to the (char)3
                        var bytesToCalculateLrcOver = receivedBytes.Take(receivedBytes.Count - 1);

                        var lrc = TcpClient.CalculateLRC(bytesToCalculateLrcOver.ToList());

                        message = receivedTillNow.Substring(0, receivedTillNow.IndexOf((char)3));
                        if (lrc != receivedData[receivedData.Length - 1])
                        {
                            this.pmsConnector.LogWarning(string.Format("LRC Failure: Received lrc: '{0}', Calculated lrc: '{1}'", receivedData[0], lrc));
                            sb = new StringBuilder();
                            success = false;
                        }
                        else
                        {
                            this.pmsConnector.LogInfo("Recevied message: " + message);
                            success = true;
                        }
                    }
                }
                else if (receivedSize == 0)
                {
                    // Received nothing
                    if (nothingReceivedIterations >= 4)
                    {
                        keepReceiving = false;
                        this.pmsConnector.LogWarning("Data receiving took too long, received so far: '{0}'", sb.ToString());
                        success = false;
                        break;
                    }
                    else
                    {
                        this.pmsConnector.LogDebug("Waiting for data...");
                        Thread.Sleep(1000);
                    }

                    nothingReceivedIterations++;
                }
            }

            return success.Value;
        }

        #endregion

        #region Sending commands

        /// <summary>
        /// Queues the message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void QueueMessage(UhllMessage message)
        {
            // All operations on the pending message must be synchronous.                
            lock (this.pendingMessages)
            {
                this.pendingMessages.Add(message);
            }
            this.ProcessSendQueue();
        }

        /// <summary>
        /// Queues the acknowledgement to be send for the message supplied as the parameter.
        /// </summary>
        /// <param name="message">The message to send the acknowledement for.</param>
        /// <param name="acknowledge">If set to <c>true</c> an ACK_MSG (Received OK) is sent, otherwise a NAK_MSG (Received NOT Ok).</param>
        private void QueueAcknowledgementMessage(UhllMessage message, bool acknowledge)
        {
            UhllMessage acknowledgement = new UhllMessage();

            acknowledgement.MessageType = acknowledge ? MessageType.Ack_Msg : MessageType.Nak_Msg;
            acknowledgement.TransactionId = message.TransactionId;
            acknowledgement.SequenceNumber = 9999;

            this.QueueMessage(acknowledgement);
        }

        private void QueueErrorMessage(UhllMessage message, Enum errorType)
        {
            UhllMessage error = new UhllMessage();

            error.MessageType = MessageType.Error;
            error.SetValue(DataField.ErrorCode, errorType.GetType() + " - " + errorType.ToString());

            if (message != null)
            {
                try
                {
                    error.TransactionId = message.TransactionId > 0 ? message.TransactionId : 0;
                    error.SequenceNumber = message.SequenceNumber > 0 ? message.SequenceNumber : 0;
                    error.SetValue(DataField.TextString, "Message content: " + message.ToUhllCommand());
                }
                catch
                {
                    // Might throw errors itself, so prevent that from happening 
                }
            }

            this.QueueMessage(error);
        }

        /// <summary>
        /// Queue an Error message in response to an invalid message received.
        /// </summary>
        /// <param name="errornousMessage">The message that was unprocessable</param>
        /// <param name="errorMessage">The error message text to explain the error</param>
        /// <param name="args">Arguments to format errorMessage</param>
        public void QueueErrorMessage(UhllMessage errornousMessage, string errorMessage, params object[] args)
        {
            // Report the Error
            UhllMessage errorReport = new UhllMessage();
            errorReport.MessageType = MessageType.Error;
            errorReport.SetValue(DataField.SequenceNumber, errornousMessage.SequenceNumber);
            errorReport.SetValue(DataField.TransactionId, errornousMessage.TransactionId);
            errorReport.SetValue(DataField.ErrorCode, "0000");
            errorReport.SetValue(DataField.TextString, errorMessage.FormatSafe(args));
            errorReport.SetValue(DataField.DMM, TcpClient.DMM.ToString());

            this.QueueMessage(errorReport);
        }

        private void ProcessSendQueue()
        {
            lock (this.sendLock)
            {
                if (this.isSending)
                {
                    this.pmsConnector.LogDebug("Attempted to send, but already sending.");
                    return;
                }

                // Check if we're connected
                if (!this.SocketIsConnected)
                {
                    this.Connect();
                    return;
                }

                // Retrieve first from dictionary
                lock (this.pendingMessages)
                {
                    UhllMessage messageToSend = null;
                    var messagesToBeSent = this.pendingMessages.Where(m => !m.Sent);

                    if (messagesToBeSent.Count() > 0)
                    {
                        // Retrieve from the message to be sent the one with the lowest transactionId and sequence number 
                        messageToSend = messagesToBeSent.OrderBy(m => m.TransactionId).OrderBy(m => m.SequenceNumber).FirstOrDefault();

                        // Mark as Sent, because other threads could be processing while this on is 'in flight'
                        messageToSend.Sent = true;

                        if (messageToSend.TransactionId <= 0)
                        {
                            messageToSend.TransactionId = this.transactionId;
                            this.transactionId++;
                        }

                        AsyncCallback onSent = MessageSent;
                        if (messageToSend != null)
                        {
                            var bytesToSent = messageToSend.ToUhllCommand();
                            this.socket.BeginSend(bytesToSent, 0, bytesToSent.Length, SocketFlags.None, onSent, messageToSend);
                        }

                    }
                }
            }
        }

        private void MessageSent(IAsyncResult result)
        {
            lock (this.sendLock)
            {
                this.isSending = false;
                this.socket.EndSend(result);

                if (!result.IsCompleted)
                {
                    this.pmsConnector.LogError("Result IS Completed == FALSE");
                    throw new Exception("Message not sent!");
                }
            }

            // GK Think we don't need this, the Sent flag is set just before it's submitted
            // through the socket. This is to prevent that during it's sending it's send again by another thread.
            //if (result.IsCompleted)
            //{
            //    // Mark the message as sent
            //    if (result.AsyncState != null)
            //    {
            //        var sentMessage = (UhllMessage)result.AsyncState;
            //        sentMessage.Sent = true;
            //    }
            //}

            // Always send again
            this.ProcessSendQueue();
        }

        #endregion

        #region Utils

        /// <summary>
        /// Longitudinal Redundancy Check (LRC) calculator for a byte array. 
        /// This was proved from the LRC Logic of Edwards TurboPump Controller SCU-1600.
        /// ex) DATA (hex 6 bytes): 02 30 30 31 23 03
        ///     LRC  (hex 1 byte ): EC        
        /// </summary>
        public static byte CalculateLRC(List<byte> bytes)
        {
            //Debug.WriteLine("Calculate LRC - Amount of bytes: " + bytes.Count);
            //Debug.WriteLine("Calculate LRC - Bytes as string using ASCII: " + Encoding.ASCII.GetString(bytes.ToArray()));

            // Start with our value.
            byte LRC = bytes[0];
            for (int i = 1; i < bytes.Count; i++)
            {
                //string byteOne = string.Format("Byte {0}: {1} - (Value: {2}, ASCII: {3})", (i - 1).ToString().PadLeft(2, '0'), Convert.ToString(LRC, 2).PadLeft(8, '0'), LRC.ToString(), Encoding.ASCII.GetString(new byte[] { LRC }));
                //string byteTwo = string.Format("Byte {0}: {1} - (Value: {2}, ASCII: {3})", (1).ToString().PadLeft(2, '0'), Convert.ToString(bytes[i], 2).PadLeft(8, '0'), bytes[i].ToString(), Encoding.ASCII.GetString(new byte[] { bytes[i] }));
                //Debug.WriteLine(byteOne);
                //Debug.WriteLine(byteTwo);
                LRC ^= bytes[i];
                //string byteResult = string.Format("Result : {0} - (Value: {1}, ASCII: {2})", Convert.ToString(LRC, 2).PadLeft(8, '0'), LRC.ToString(), Encoding.ASCII.GetString(new byte[] { LRC }));
                //Debug.WriteLine(byteResult);
            }
            return LRC;
        }

        #endregion

    }
}
