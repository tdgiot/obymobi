﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.Model;
using System.Globalization;
using Dionysos;

namespace Obymobi.Logic.PMS.Comtrol
{
    /// <summary>
    /// ComtrolConnector
    /// </summary>
    public class ComtrolConnector : PmsConnector
    {
        private TcpClient tcpClient;
        private List<UhllMessage> responseMessages = new List<UhllMessage>();
        int port;
        string host;

        /// <summary>
        /// ComtrolConnectorResult enums
        /// </summary>
        public enum ComtrolConnectorResult
        {
            /// <summary>
            /// Non processable message
            /// </summary>
            NonProcessableMessage = 200,
            RequiredParametersMissingOrInvalid = 201
        }

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="ComtrolConnector"/> class.
        /// </summary>
        public ComtrolConnector(ComtrolConfigurationAdapter config = null)
        {
            if (config == null)
            {
                config = new ComtrolConfigurationAdapter();
                config.ReadFromConfigurationManager();
            }

            this.host = config.HostAddress;
            this.port = config.Port;

            this.InitializePmsReceiver();

            this.PmsMessageReceived += new PmsMessageReceivedHandler(ComtrolConnector_PmsMessageReceived);
        }

        public override string LogPrefix()
        {
            return "ComtrolConnector";
        }

        #endregion

        #region Pms interaction

        protected override string SendMessageToPms(IPmsMessage message)
        {
            this.tcpClient.QueueMessage((UhllMessage)message);
            return "queued";
        }

        public override void StopReceiving()
        {
            
        }

        #endregion

        #region Implementation of Abstract class

        /// <summary>
        /// Initialize the webservice / WCF / socket / etc that receives the
        /// push messages from the PMS
        /// </summary>
        protected override void InitializePmsReceiver()
        {
            this.tcpClient = new TcpClient(this, this.host, this.port, this.receivedMessages);
        }

        /// <summary>
        /// Indicates a request to have the PMS check a room out. The balance should match the PMS' record for
        /// the account / room number specified. The PMS must initiate an actual Check Out Room (after this
        /// message's required Express Checkout response) if this Express Checkout Request is accepted.
        /// </summary>
        /// <param name="checkoutInfo">The checkout info.</param>
        /// <returns>
        /// Boolean if the checkout succeeded
        /// </returns>
        public override bool Checkout(Model.Checkout checkoutInfo)
        {
            if (checkoutInfo.DeliverypointNumber.IsNullOrWhiteSpace())
                throw new ObymobiException(ComtrolConnectorResult.RequiredParametersMissingOrInvalid, "Deliverypointnumber is missing");

            UhllMessage checkout = new UhllMessage();
            checkout.MessageType = MessageType.ExpressCheckoutRequest;
            checkout.SetValue(DataField.AccountNumber, checkoutInfo.AccountNumber);
            checkout.SetValue(DataField.RoomNumber, checkoutInfo.DeliverypointNumber);
            checkout.SetValue(DataField.BalanceAmount, checkoutInfo.ChargePriceIn);

            UhllMessage response = this.SendMessageToPmsSynchronously(checkout, true) as UhllMessage;

            string status;
            bool toReturn = false;
            if (!response.TryGetValue(DataField.GenericStatus, out status))
            {
                if (status.Equals("Y", StringComparison.OrdinalIgnoreCase))
                {
                    toReturn = true;
                    this.LogVerbose("Checkout successful for Room: '{0}'", response.Roomnumber);
                }
                else
                {
                    toReturn = false;
                    this.LogWarning("Checkout failed for Room: '{0}'", response.Roomnumber);
                }
            }

            return toReturn;
        }

        /// <summary>
        /// Get the Room Information for a Room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>
        /// The Room Information for the Room
        /// </returns>
        public override List<Model.GuestInformation> GetGuestInformation(string deliverypointNumber)
        {
            // GK Ask Comtrol if it necessary to do a full re-sync, or that we can call a single room
            if (deliverypointNumber.IsNullOrWhiteSpace())
                throw new ObymobiException(ComtrolConnectorResult.RequiredParametersMissingOrInvalid, "Deliverypointnumber is missing");

            UhllMessage requestFolioInfo = new UhllMessage(MessageType.FolioInformation);
            requestFolioInfo.SetValue(DataField.RoomNumber, deliverypointNumber);

            UhllMessage response = this.SendMessageToPmsSynchronously(requestFolioInfo, true) as UhllMessage;

            List<GuestInformation> guestInformations = new List<GuestInformation>();
            foreach (var message in response.MessagesInTransactionIncludingSelf)
            {
                string fullname, accountnumber, genericstatus, messagewaiting, stationnumber, roomnumber, inhibit, billingAccess;

                // Required
                message.TryGetValue(DataField.FullName, out fullname, true);
                message.TryGetValue(DataField.AccountNumber, out accountnumber, true);
                message.TryGetValue(DataField.GenericStatus, out genericstatus, true);
                message.TryGetValue(DataField.MessageWaiting, out messagewaiting, true);
                message.TryGetValue(DataField.StationNumber, out stationnumber, true);
                message.TryGetValue(DataField.RoomNumber, out roomnumber, true);
                message.TryGetValue(DataField.Inhibit, out inhibit, true);
                message.TryGetValue(DataField.BillingAccess, out billingAccess, true);

                // Folio Query Status: 0 = success, 1 = Vacant, 2 = Invald account
                if (genericstatus == "1")
                {
                    this.LogWarning("Queried Vacant Room: '{0}'", roomnumber);
                }
                else if (genericstatus == "2")
                {
                    this.LogWarning("Queried Wrong Account for Room: '{0}' (Account: '{1}')", roomnumber, accountnumber);
                }

                GuestInformation guestInfo = new GuestInformation();
                guestInfo.CustomerFirstname = fullname;
                guestInfo.AccountNumber = accountnumber;
                guestInfo.DeliverypointNumber = roomnumber;
                if (billingAccess == "0" || billingAccess == "3")
                {
                    guestInfo.AllowExpressCheckout = true;
                    guestInfo.AllowViewFolio = true;
                }
                else if (billingAccess == "1")
                {
                    guestInfo.AllowExpressCheckout = false;
                    guestInfo.AllowViewFolio = false;
                }
                else if (billingAccess == "2")
                {
                    guestInfo.AllowExpressCheckout = false;
                    guestInfo.AllowViewFolio = true;
                }

                if (inhibit == "1")
                    guestInfo.AllowFolioPosting = false;
                else if (inhibit == "0")
                    guestInfo.AllowFolioPosting = true;

                this.LogVerbose("Folio Information for Room: {0}", guestInfo.DeliverypointNumber);
                this.LogVerbose("Full Name: {0}", guestInfo.CustomerFirstname);
                this.LogVerbose("AccountNumber: {0}", guestInfo.AccountNumber);
                this.LogVerbose("Inhibit: {0}", guestInfo.AllowFolioPosting);

                guestInformations.Add(guestInfo);
            }

            return guestInformations;
        }

        /// <summary>
        /// Get the Folio for the Room detailing all charges and credits booked for the Room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>
        /// The Folio for the room
        /// </returns>
        public override List<Model.Folio> GetFolio(string deliverypointNumber)
        {
            if (deliverypointNumber.IsNullOrWhiteSpace())
                throw new ObymobiException(ComtrolConnectorResult.RequiredParametersMissingOrInvalid, "Deliverypointnumber is missing");

            UhllMessage checkout = new UhllMessage(MessageType.RequestFolioItems);
            checkout.SetValue(DataField.RoomNumber, deliverypointNumber);

            UhllMessage response = this.SendMessageToPmsSynchronously(checkout, true) as UhllMessage;

            Folio folio = new Folio();
            folio.DeliverypointNumber = deliverypointNumber;
            List<FolioItem> folioItems = new List<FolioItem>();

            foreach (var message in response.MessagesInTransactionIncludingSelf)
            {
                if (message.MessageType == MessageType.FolioItem)
                {
                    string description, amount, postdate, roomnumber;
                    // Required
                    message.TryGetValue(DataField.ChargeDescription, out description, true);
                    message.TryGetValue(DataField.ChargeAmount, out amount, true);
                    message.TryGetValue(DataField.PostDate, out postdate, true);
                    message.TryGetValue(DataField.RoomNumber, out roomnumber, true);

                    FolioItem fi = new FolioItem();

                    fi.Description = description;

                    // Date
                    DateTime date;
                    if (this.parseDate(postdate, out date))
                    {
                        fi.Created = date;
                    }
                    else
                    {
                        this.LogWarning("Unparseable date: '{0}'", postdate);
                        this.InvalidMessage(message, "Unparseable date");
                    }

                    // Amount
                    decimal amountDecimal;
                    if (decimal.TryParse(amount, NumberStyles.None, CultureInfo.InvariantCulture, out amountDecimal))
                    {
                        fi.PriceIn = amountDecimal;
                    }
                    else
                    {
                        this.LogWarning("Unparseable decimal: '{0}'", amount);
                        this.InvalidMessage(message, "Can't parse decimal");
                    }

                    folioItems.Add(fi);

                    this.LogVerbose("Folio item: {0} - {1} - {2}", fi.Description, fi.PriceIn, fi.Created);
                }
                else if (message.MessageType == MessageType.Balance)
                {
                    // We don't use this message.
                    string balance;
                    message.TryGetValue(DataField.BalanceAmount, out balance);
                    this.LogDebug("Balance message: '{0}'", balance);
                }
                else
                {
                    this.LogWarning("Unexpected message in transaction: '{0}'", message.MessageDescription);
                }
            }

            folio.FolioItems = folioItems.ToArray();
            List<Folio> folios = new List<Folio>();
            folios.Add(folio);

            return folios;
        }

        /// <summary>
        /// Add one or more FolioItems to the Folio
        /// </summary>
        /// <param name="deliverypointNumber"></param>
        /// <param name="folioItems">Items to be added</param>
        /// <returns>
        /// True of false indicating if the item could be added
        /// </returns>
        public override bool AddToFolio(string deliverypointNumber, List<Model.FolioItem> folioItems)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Set the Room State
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <param name="state">State of the room</param>
        /// <returns>
        /// True of false indicating if the state could be updated
        /// </returns>
        public override bool UpdateRoomState(string deliverypointNumber, RoomState state)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the balance of the room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>
        /// The balance of the room
        /// </returns>
        public override decimal GetBalance(string deliverypointNumber)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get the state of the room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>
        /// The room state
        /// </returns>
        public override RoomState GetRoomStatus(string deliverypointNumber)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Set the WakeUpStatus
        /// </summary>
        /// <param name="wakeUpStatus">The WakeUpStatus to set</param>
        /// <returns>
        /// The room state
        /// </returns>
        public override bool SetWakeUpStatus(WakeUpStatus wakeUpStatus)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Received Message Processing

        private void InvalidMessage(UhllMessage message, string error, params object[] args)
        {
            string errorText = error.FormatSafe(args);
            this.LogError("Message of type: '{0}' cannot be parsed: {1}.\r\n{2}", message.MessageType.ToString(), errorText, message.ToKeyValuePairsText());
            this.tcpClient.QueueErrorMessage(message, errorText);
        }

        /// <summary>
        /// Processes the message.
        /// </summary>
        /// <param name="messages">The messages.</param>
        public void ProcessMessage(UhllMessage message)
        {
            if (message == null)
            {
                this.LogError("Process Message called with 'null' parameter6", message.MessageType);
                return;
            }
            else if (message.MessagesInTransactionExcludingSelf.Count == 0)
            {
                this.LogDebug("Start processing message: '{0}'", message.MessageType);
            }
            else
            {
                this.LogDebug("Start processing '{0}' messages", message.MessagesInTransactionIncludingSelf.Count);
                foreach (var submessage in message.MessagesInTransactionIncludingSelf)
                {
                    this.LogDebug("Message: '{0}'", message.MessageType);
                }
            }

            MessageType messageType = message.MessageType;

            if (messageType == MessageType.CheckInRoom)
                this.ProcessCheckInRoom(message);
            else if (messageType == MessageType.CheckoutRoom)
                this.ProcessCheckoutRoom(message);
            else if (messageType == MessageType.RoomMove)
                this.ProcessRoomMove(message);
            else if (messageType == MessageType.DoNotDisturb)
                this.ProcessDoNotDisturb(message);
            else if (messageType == MessageType.Error)
                this.ProcessError(message);
            else if (messageType == MessageType.RoomStatus)
                this.ProcessRoomStatus(message);
            else if (messageType == MessageType.Swap)
                this.ProcessSwap(message);
            else if (messageType == MessageType.MaidCode)
                this.ProcessMaidCode(message);
            else if (messageType == MessageType.CreditLimit)
                this.ProcessCreditLimit(message);
            else if (messageType == MessageType.Language)
                this.ProcessLanguage(message);
            else if (messageType == MessageType.Vip)
                this.ProcessVip(message);
            else
                this.LogInfo("Message not supported: '{0}'", message.ToString());
        }

        private void ProcessCheckInRoom(UhllMessage message)
        {
            GuestInformation checkInGuestInformation = new GuestInformation();

            string fullname, accountNumber, stationNumber, roomNumber;

            // Required fields
            message.TryGetValue(DataField.FullName, out fullname, true);
            message.TryGetValue(DataField.AccountNumber, out accountNumber, true);
            message.TryGetValue(DataField.StationNumber, out stationNumber, true);
            message.TryGetValue(DataField.RoomNumber, out roomNumber, true);

            // Optional fields
            string businessName, zipcode, restrictionlimit,
                guestId, language, inhibit, password;
            message.TryGetValue(DataField.BusinessName, out businessName, false);
            message.TryGetValue(DataField.Zipcode, out zipcode, false);
            message.TryGetValue(DataField.RestrictionLimit, out restrictionlimit, false);
            message.TryGetValue(DataField.GuestId, out guestId, false);
            message.TryGetValue(DataField.Language, out language, false);
            message.TryGetValue(DataField.Inhibit, out inhibit, false);
            message.TryGetValue(DataField.Password, out password, false);

            checkInGuestInformation.CustomerFirstname = fullname;
            checkInGuestInformation.AccountNumber = accountNumber;
            checkInGuestInformation.DeliverypointNumber = roomNumber;

            checkInGuestInformation.Company = businessName;
            checkInGuestInformation.Zipcode = zipcode;
            checkInGuestInformation.CreditLimit = Convert.ToDecimal(restrictionlimit, CultureInfo.InvariantCulture);
            checkInGuestInformation.LanguageCode = language;
            checkInGuestInformation.AllowFolioPosting = inhibit == "0" ? true : false;
            checkInGuestInformation.Password = password;

            checkInGuestInformation.AccountNumber = accountNumber;
            checkInGuestInformation.Occupied = true;

            this.OnCheckedIn(checkInGuestInformation);
        }

        private void ProcessCheckoutRoom(UhllMessage message)
        {
            string roomNumber;

            // Required fields
            message.TryGetValue(DataField.RoomNumber, out roomNumber, true);

            // Optional fields
            GuestInformation guestInformation = new GuestInformation();
            guestInformation.Occupied = false;
            guestInformation.DeliverypointNumber = roomNumber;

            this.OnCheckedOut(guestInformation);
        }

        private void ProcessRoomMove(UhllMessage message)
        {
            string fullname, accountnumber, newstationnumber, newroomnumber,
                oldstationnumber, oldroomnumber;
            message.TryGetValue(DataField.FullName, out fullname, true);
            message.TryGetValue(DataField.AccountNumber, out accountnumber, true);
            message.TryGetValue(DataField.NewStationNumber, out newstationnumber, true);
            message.TryGetValue(DataField.NewRoomNumber, out newroomnumber, true);
            message.TryGetValue(DataField.StationNumber, out oldstationnumber, true);
            message.TryGetValue(DataField.RoomNumber, out oldroomnumber, true);

            this.OnMoved(oldroomnumber, oldstationnumber, newroomnumber, newstationnumber);
        }

        private void ProcessMaidCode(UhllMessage message)
        {
            string stationNumber, status;

            // Required fields
            // GK >> StationNumer vs RoomNumber :S
            message.TryGetValue(DataField.StationNumber, out stationNumber, true);
            message.TryGetValue(DataField.GenericStatus, out status, true);

            // Try parse value
            int roomStateInt;
            RoomState roomState = RoomState.Unknown;
            if (!int.TryParse(status, out roomStateInt))
            {
                this.InvalidMessage(message, "Status not an int: '{0}'", roomStateInt);
            }
            else if (!EnumUtil.TryParse(Convert.ToInt32(status), out roomState))
            {
                this.InvalidMessage(message, "Status not available: '{0}'", roomStateInt);
            }

            // GK TODO - Convert station number to Roomnumber

            this.OnRoomStateUpdate(stationNumber, roomState);
        }

        private void ProcessDoNotDisturb(UhllMessage message)
        {
            string status, stationnumber, roomnumber;

            message.TryGetValue(DataField.GenericStatus, out status, true);

            message.TryGetValue(DataField.StationNumber, out stationnumber, false);
            message.TryGetValue(DataField.RoomNumber, out roomnumber, false);

            if (stationnumber.IsNullOrWhiteSpace() && roomnumber.IsNullOrWhiteSpace())
            {
                this.InvalidMessage(message, "No roomnumber and no stationnumber specified.");
                return;
            }

            int dndStateInt;
            DoNotDisturbState dndState = DoNotDisturbState.None;
            if (!int.TryParse(status, out dndStateInt))
            {
                this.InvalidMessage(message, "Status not an int: '{0}'", dndStateInt);
                return;
            }
            else if (!EnumUtil.TryParse(Convert.ToInt32(dndStateInt), out dndState))
            {
                this.InvalidMessage(message, "Status not available: '{0}'", dndStateInt);
                return;
            }

            this.OnDoNotDisturbUpdated(roomnumber, stationnumber, dndState);
        }

        private void ProcessError(UhllMessage message)
        {
            string errorText, sequenceNumber, transactionId, errorCode, dmm;

            // Required
            message.TryGetValue(DataField.TextString, out errorText, true);
            message.TryGetValue(DataField.ErrorCode, out errorCode, true);
            message.TryGetValue(DataField.DMM, out dmm, true);

            // Optional
            message.TryGetValue(DataField.SequenceNumber, out sequenceNumber, false);
            message.TryGetValue(DataField.TransactionId, out transactionId, false);

            this.LogWarning("Error reported: '{0}' ({1}). Sequence: '{2}', TransactionId: '{3}', DMM: '{4}'", errorText, errorCode, sequenceNumber, transactionId, dmm);
        }

        private void ProcessRoomStatus(UhllMessage message)
        {
            string status, stationnumber, roomnumber;

            message.TryGetValue(DataField.StationNumber, out stationnumber, false);
            message.TryGetValue(DataField.RoomNumber, out roomnumber, false);

            if (stationnumber.IsNullOrWhiteSpace() && roomnumber.IsNullOrWhiteSpace())
            {
                this.InvalidMessage(message, "No roomnumber and no stationnumber specified.");
                return;
            }

            message.TryGetValue(DataField.RoomNumber, out status, false);

            int roomStateInt;
            RoomState roomState = RoomState.Unknown;
            if (!int.TryParse(status, out roomStateInt))
            {
                this.InvalidMessage(message, "Status not an int: '{0}'", roomStateInt);
                return;
            }
            else if (!EnumUtil.TryParse(Convert.ToInt32(roomStateInt), out roomState))
            {
                this.InvalidMessage(message, "Status not available: '{0}'", roomStateInt);
                return;
            }

            this.LogVerbose("ProcessRoomStatus: Room '{0}' - State: '{1}'", roomnumber, status);
        }

        private void ProcessSwap(UhllMessage message)
        {
            string newstationnumber, newroomnumber, oldstationnumber, oldroomnumber;
            message.TryGetValue(DataField.NewStationNumber, out newstationnumber, true);
            message.TryGetValue(DataField.NewRoomNumber, out newroomnumber, true);
            message.TryGetValue(DataField.StationNumber, out oldstationnumber, true);
            message.TryGetValue(DataField.RoomNumber, out oldroomnumber, true);

            this.OnMoved(oldroomnumber, oldstationnumber, newroomnumber, newstationnumber);
        }

        private void ProcessCreditLimit(UhllMessage message)
        {
            string restrictionlimit, accountnumber, stationnumber, roomnumber, inhibit;
            message.TryGetValue(DataField.RestrictionLimit, out restrictionlimit, true);
            message.TryGetValue(DataField.AccountNumber, out accountnumber, true);
            message.TryGetValue(DataField.StationNumber, out stationnumber, true);
            message.TryGetValue(DataField.RoomNumber, out roomnumber, true);
            message.TryGetValue(DataField.Inhibit, out inhibit, true);

            decimal limitDecimal;
            if (!decimal.TryParse(restrictionlimit, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out limitDecimal))
            {
                this.InvalidMessage(message, "Can't parse decimal");
                return;
            }

            this.OnCreditLimitUpdated(roomnumber, limitDecimal);
        }

        private void ProcessLanguage(UhllMessage message)
        {
            string language, stationnumber, roomnumber;
            message.TryGetValue(DataField.StationNumber, out stationnumber, true);
            message.TryGetValue(DataField.RoomNumber, out roomnumber, true);
            message.TryGetValue(DataField.Language, out language, true);

            GuestInformation gi = new GuestInformation();
            gi.LanguageCode = language;
            gi.DeliverypointNumber = roomnumber;

            this.OnGuestInformationUpdated(gi);
        }

        private void ProcessVip(UhllMessage message)
        {
            string fullname, stationnumber, roomnumber, status;
            message.TryGetValue(DataField.StationNumber, out stationnumber, false);
            message.TryGetValue(DataField.RoomNumber, out roomnumber, false);
            message.TryGetValue(DataField.FullName, out fullname, true);
            message.TryGetValue(DataField.GenericStatus, out status, true);

            if (stationnumber.IsNullOrWhiteSpace() && roomnumber.IsNullOrWhiteSpace())
            {
                this.InvalidMessage(message, "No roomnumber and no stationnumber specified.");
                return;
            }

            GuestInformation gi = new GuestInformation();
            gi.Vip = (status == "1");
            gi.DeliverypointNumber = roomnumber;

            this.OnGuestInformationUpdated(gi);
        }

        private bool parseDate(string dateString, out DateTime date)
        {
            return DateTime.TryParseExact(dateString, "MM/dd/yyyy", null, DateTimeStyles.None, out date);
        }

        void ComtrolConnector_PmsMessageReceived(PmsConnector sender, IPmsMessage message)
        {
            this.ProcessMessage(message as UhllMessage);
        }
    }

        #endregion
}


