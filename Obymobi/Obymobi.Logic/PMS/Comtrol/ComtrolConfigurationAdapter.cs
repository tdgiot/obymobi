﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Logic.POS.Interfaces;

namespace Obymobi.Logic.PMS.Comtrol
{
    public class ComtrolConfigurationAdapter : IConfigurationAdapter
    {
        #region Methods
        /// <summary>
        /// Read configuration from a Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalModel(Model.Terminal terminal)
        {
            this.HostAddress = terminal.PosValue1;
            this.Port = Convert.ToInt32(terminal.PosValue2);
        }

        /// <summary>
        /// Read configuration from a Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void ReadFromTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            this.HostAddress = terminal.PosValue1;
            this.Port = Convert.ToInt32(terminal.PosValue2);
        }

        /// <summary>
        /// Read configuration from Configuration Manager
        /// </summary>
        public void ReadFromConfigurationManager()
        {
            this.HostAddress = Dionysos.ConfigurationManager.GetString(PmsConfigurationConstants.ComtrolHostAddress);
            this.Port = Dionysos.ConfigurationManager.GetInt(PmsConfigurationConstants.ComtrolPort);

        }

        /// <summary>
        /// Write configuration to Configuration Manager
        /// </summary>
        public void WriteToConfigurationManager()
        {
            Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.TigertmsWebserviceToCallToUrl, this.HostAddress);
            Dionysos.ConfigurationManager.SetValue(PmsConfigurationConstants.TigertmsWebserviceUserkey, this.Port);
        }

        /// <summary>
        /// Write configuration to Terminal Entity
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalEntity(Data.EntityClasses.TerminalEntity terminal)
        {
            terminal.PosValue1 = this.HostAddress;
            terminal.PosValue2 = this.Port.ToString();
        }

        /// <summary>
        /// Write configuration to Terminal Model
        /// </summary>
        /// <param name="terminal"></param>
        public void WriteToTerminalModel(Model.Terminal terminal)
        {
            terminal.PosValue1 = this.HostAddress;
            terminal.PosValue2 = this.Port.ToString();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the webservice to call to URL.
        /// </summary>
        /// <value>
        /// The webservice to call to URL.
        /// </value>
        public string HostAddress { get; set; }

        /// <summary>
        /// Gets or sets the user key.
        /// </summary>
        /// <value>
        /// The user key.
        /// </value>
        public int Port { get; set; }

        #endregion
    }
}
