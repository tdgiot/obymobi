﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.PMS.Comtrol
{
    /// <summary>
    /// GroupDirectoryCode enums
    /// </summary>
    public enum GroupDirectoryCode
    {
        /// <summary>
        /// Do not include in directory
        /// </summary>
        DoNotIncludeInDirectory = 0,
        /// <summary>
        /// Do include in directory
        /// </summary>
        IncludingInDirectory = 1
    }
}
