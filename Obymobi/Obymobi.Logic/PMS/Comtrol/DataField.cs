﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.PMS.Comtrol
{
    /// <summary>
    /// DataField enums
    /// </summary>
    public enum DataField : int
    {
        /// <summary>
        /// String - Reserved
        /// </summary>   
        TextString = 1,

        /// <summary>
        /// String - Guest name to display on any GSS equipment that permits it (e.g. PBX telephone with name display)
        /// </summary>   
        FullName = 8,

        /// <summary>
        /// String - Company affiliation for guest
        /// </summary>
        BusinessName = 18,

        /// <summary>
        /// String - Home ZIP code for the guest
        /// </summary>
        Zipcode = 23,

        /// <summary>
        /// Enum(VipStatus)
        /// </summary>
        VipStatus = 59,

        /// <summary>
        /// Currency - Credit Limit allowed on any oneway posting devices that support setting amount limits for incurred charges
        /// </summary>
        RestrictionLimit = 89,

        /// <summary>
        /// Sequence Number of the UHLL Message that caused this error
        /// </summary>
        SequenceNumber = 90,

        /// <summary>
        /// Transaction ID of the UHLL Message that caused this error
        /// </summary>
        TransactionId = 91,

        /// <summary>
        /// String - Unique ID for the hotel employee responsible for requesting this  charge to the indicated account / room / station
        /// </summary>
        AgentId = 104,

        /// <summary>
        /// String - Unique identifier used for charging this guest for goods and services purchased
        /// </summary>
        AccountNumber = 106,

        /// <summary>
        /// String - Unique identifier given to a set of guests who wish to be identified together as a group
        /// </summary>
        GroupNumber = 109,

        /// <summary>
        /// Enum(GroupDirectoryStatus) - Group directory inclusion flag
        /// </summary>
        GroupDirectoryStatus = 121,

        /// <summary>
        /// Value depends on message type
        /// Maid code: Enum(MaidCodes) Possible "states" of the room associated with the indicated station number
        /// Room move: Reserved
        /// Do not disturb: Reserved
        /// Folio information: Enum(FolioQueryStatus) Response to request to describe each of the checked-in guests that match the requested room /
        /// account number (given in the Request Folio Information message). One of these messages should be
        /// sent (all as part of the same UHLL Transaction) for each guest that meets the given criteria.
        /// Folio item: Reserved
        /// Room Status: Enum(MaidCodes)
        /// Swap: RESERVED
        /// Vip: Enum(VipStatus)
        /// </summary>
        GenericStatus = 144,

        /// <summary>
        /// Enum(FolioPostingTransactionCode) - GSS Device category that this charge posting falls into.
        /// </summary>
        FolioPostingTransactionCode = 145,

        /// <summary>
        /// String - Vendor specified code that details the error condition. If the DMM field is the DMM of the PMS, then the UHLL Error Codes chart lists those codes and their associated values.
        /// </summary>
        ErrorCode = 146,

        /// <summary>
        /// String - GSS-system specific code further categorizing this charge
        /// </summary>
        RevenueCode = 147,

        /// <summary>
        /// String - Human-readable description of this particular charge item
        /// </summary>
        ChargeDescription = 148,

        /// <summary>
        /// Currency - Credit or debit amount of this particular folio item. Negative amounts will be treated as credits to the account.
        /// </summary>
        ChargeAmount = 149,

        /// <summary>
        /// Currency
        /// Express Checkout Request: Total charge amount the guest has agreed to pay pursuant to the room
        /// being checked out. This should be compared to the PMS's current total for the account / room
        /// requesting the check out.
        /// </summary>
        BalanceAmount = 150,

        /// <summary>
        /// String - Number of text messages waiting for the guest at the front desk
        /// </summary>
        MessageWaiting = 151,

        /// <summary>
        /// Date - Date on which this credit / charge was posted to the folio
        /// </summary>
        PostDate = 153,

        /// <summary>
        /// String - Unique ID used to identify this guest as part of a multiple-guest folio/account
        /// </summary>
        GuestId = 163,

        /// <summary>
        /// Enum(Languages) - Guest's primary spoken language
        /// </summary>
        Language = 164,

        /// <summary>
        /// String - New "Extension" number associated with this guest/room. Generally used by 
        /// telecommunications (PBX) equipment.
        /// </summary>
        NewStationNumber = 165,

        /// <summary>
        /// String - Direct In-Dial number for voice calls
        /// </summary>
        VoiceDid = 166,

        /// <summary>
        /// String - Direct In-Dial number for data calls
        /// </summary>
        DataDid = 167,

        /// <summary>
        /// RESERVED
        /// </summary>
        ResyncFlag = 168,

        /// <summary>
        /// New name / number that uniquely identifies the room in which the guest has been registered
        /// </summary>
        NewRoomNumber = 169,

        /// <summary>
        /// String - "Extension" number associated with this guest/room. Generally used by telecommunications (PBX) equipment.
        /// </summary>
        StationNumber = 174,

        /// <summary>
        /// String - Name / number that uniquely identifies the room in which the guest has been registered
        /// </summary>
        RoomNumber = 175,

        /// <summary>
        /// Enum(FolioPosting) Allow or Disallow simple posting for this guest
        /// </summary>
        Inhibit = 177,

        /// <summary>
        /// DMM designation of the device that reported this error condition
        /// </summary>
        DMM = 178,

        /// <summary>
        /// String - Password to associate with guest when secure access to a resource is required
        /// </summary>
        Password = 182,

        /// <summary>
        /// Enum(CallRestrictions) - Classification of telephone call types this guest may make from the associated Station Number
        /// </summary>
        AccessLevel = 183,

        /// <summary>
        /// Time - Time of day this charge was incurred
        /// </summary>
        Time = 197,

        /// <summary>
        /// Time - Length of time the chargeable event lasted for
        /// </summary>
        Duration = 198,

        /// <summary>
        /// String - Primary rate plan or amount associated with this guest's stay
        /// </summary>
        Rate = 200,

        /// <summary>
        /// RESERVED
        /// </summary>
        VendorSpecificField = 201,

        /// <summary>
        /// String - Private Identification Number
        /// </summary>
        GuestPin = 305,

        /// <summary>
        /// Enum(MovieAccess) Classification of movie types this guest may view
        /// </summary>
        MovieAccess = 306,

        /// <summary>
        /// Enum(BillingAccess) Classification of billing and checkout types this guest may view
        /// 0 All Allowed
        /// 1 All Restricted
        /// 2 Allow Viewing
        /// 3 Allow Remote/Express Checkout
        /// </summary>
        BillingAccess = 307,

        /// <summary>
        /// Enum(GameAccess) Classification of video game types this guest may view
        /// </summary>
        GameAccess = 308,

        /// <summary>
        /// Enum(DndStatus) - Current Do Not Disturb status of the guest in the room, station number, etc.
        /// </summary>
        DoNotDisturbStatus = 310,

        /// <summary>
        /// String - The display name given to a set of guests who wish to be identified together as a group
        /// </summary>
        GroupName = 319

    }
}
