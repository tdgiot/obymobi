﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.ServiceModel;
using Dionysos;
using Obymobi.Logic.HTNGService;
using Obymobi.Logic.PMS.FlatWsdl;
using Obymobi.Logic.PMS.Innsist.Messages;

namespace Obymobi.Logic.PMS.Innsist
{
    [ServiceBehavior(Namespace = "http://htng.org/2011B", InstanceContextMode = InstanceContextMode.Single, IncludeExceptionDetailInFaults = true)]
    [Soap11Conformant]
    public class ExternalWebservice : IExternalWebservice
    {
        private readonly ObservableCollection<IPmsMessage> receivedMessages;

        public ExternalWebservice(ObservableCollection<IPmsMessage> receivedMessages)
        {
            this.receivedMessages = receivedMessages;
        }

        public CheckedInResponse CheckedIn(CheckedInRequest request)
        {
            HTNG_HotelCheckInNotifRQ notiRq = request.HTNG_HotelCheckInNotifRQ;

            CheckedIn checkedInMessage = new CheckedIn();
            checkedInMessage.ExpressCheckout = true;
            checkedInMessage.RoomId = notiRq.Room.RoomType.RoomID;

            if (notiRq.AffectedGuests != null && notiRq.AffectedGuests.UniqueID != null)
            {
                checkedInMessage.GuestId = notiRq.AffectedGuests.UniqueID.ID;
            }

            ResGuestType guestType = notiRq.HotelReservations.HotelReservation[0].ResGuests[0];
            checkedInMessage.Group = guestType.GroupEventCode;

            RoomStayType roomStay = notiRq.HotelReservations.HotelReservation[0].RoomStays[0];
            DateTime t;
            if (DateTime.TryParseExact(roomStay.TimeSpan.Start, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out t))
            {
                checkedInMessage.Arrival = t;
            }
            else if (guestType.ArrivalTimeSpecified)
            {
                checkedInMessage.Arrival = guestType.ArrivalTime;
            }

            if (DateTime.TryParseExact(roomStay.TimeSpan.End, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out t))
            {
                checkedInMessage.Departure = t;
            }
            else if (guestType.DepartureTimeSpecified)
            {
                checkedInMessage.Departure = guestType.DepartureTime;
            }

            if (guestType.Profiles != null && guestType.Profiles.Length > 0)
            {
                CustomerType customer = guestType.Profiles[0].Profile.Customer;
                checkedInMessage.Vip = customer.VIP_Indicator;
                checkedInMessage.Language = customer.Language;

                PersonNameType personName = customer.PersonName[0];
                checkedInMessage.Title = (personName.NamePrefix != null && personName.NamePrefix.Length > 0) ? personName.NamePrefix[0] : "";
                checkedInMessage.First = (personName.GivenName != null && personName.GivenName.Length > 0) ? personName.GivenName[0] : "";
                if (!personName.SurnamePrefix.IsNullOrWhiteSpace())
                {
                    checkedInMessage.Last = string.Format("{0} {1}", personName.SurnamePrefix, personName.Surname);
                }
                else
                {
                    checkedInMessage.Last = personName.Surname;
                }
            }            

            // Add message to collection
            this.receivedMessages.Add(checkedInMessage);

            return new CheckedInResponse
            {
                HTNG_HotelCheckInNotifRS = new HTNG_ResponseBaseType
                {
                    Items = new object[] { new SuccessType() },
                    EchoToken = notiRq.EchoToken,
                    TimeStamp = notiRq.TimeStamp,
                    TimeStampSpecified = true
                }
            };
        }

        public CheckedOutResponse CheckedOut(CheckedOutRequest request)
        {
            HTNG_HotelCheckOutNotifRQ notiRq = request.HTNG_HotelCheckOutNotifRQ;

            CheckedOut checkedOut = new CheckedOut();
            checkedOut.RoomId = notiRq.Room.RoomType.RoomID;

            if (notiRq.AffectedGuests != null && notiRq.AffectedGuests.UniqueID != null)
            {
                checkedOut.GuestId = notiRq.AffectedGuests.UniqueID.ID;
            }

            ResGuestType guestType = notiRq.HotelReservations.HotelReservation[0].ResGuests[0];
            if (guestType.Profiles != null && guestType.Profiles.Length > 0)
            {
                CustomerType customer = guestType.Profiles[0].Profile.Customer;

                PersonNameType personName = customer.PersonName[0];
                checkedOut.Title = (personName.NamePrefix != null && personName.NamePrefix.Length > 0) ? personName.NamePrefix[0] : "";
                checkedOut.First = (personName.GivenName != null && personName.GivenName.Length > 0) ? personName.GivenName[0] : "";
                if (!personName.SurnamePrefix.IsNullOrWhiteSpace())
                {
                    checkedOut.Last = string.Format("{0} {1}", personName.SurnamePrefix, personName.Surname);
                }
                else
                {
                    checkedOut.Last = personName.Surname;
                }
            } 

            // Add message to collection
            this.receivedMessages.Add(checkedOut);

            return new CheckedOutResponse
            {
                HTNG_HotelCheckOutNotifRS = new HTNG_ResponseBaseType
                {
                    Items = new object[] { new SuccessType() },
                    EchoToken = request.HTNG_HotelCheckOutNotifRQ.EchoToken,
                    TimeStamp = request.HTNG_HotelCheckOutNotifRQ.TimeStamp,
                    TimeStampSpecified = true
                }
            };
        }

        public StayUpdatedResponse StayUpdated(StayUpdatedRequest request)
        {
            HTNG_HotelStayUpdateNotifRQ notiRq = request.HTNG_HotelStayUpdateNotifRQ;
            
            if (!notiRq.HotelReservations.HotelReservation[0].ResStatus.Equals("In-house", StringComparison.InvariantCultureIgnoreCase))
            {
                // Just return, we only update info if the people are actually checked in
                return new StayUpdatedResponse
                {
                    HTNG_HotelStayUpdateNotifRS = new HTNG_ResponseBaseType
                    {
                        Items = new object[] { new SuccessType() },
                        EchoToken = notiRq.EchoToken,
                        TimeStamp = notiRq.TimeStamp,
                        TimeStampSpecified = true
                    }
                };
            }

            StayUpdated stayUpdated = new StayUpdated();
            stayUpdated.RoomId = notiRq.Room.RoomType.RoomID;

            ResGuestType guestType = notiRq.HotelReservations.HotelReservation[0].ResGuests[0];
            stayUpdated.Group = guestType.GroupEventCode;

            if (notiRq.AffectedGuests != null && notiRq.AffectedGuests.UniqueID != null)
            {
                stayUpdated.GuestId = notiRq.AffectedGuests.UniqueID.ID;
            }

            RoomStayType roomStay = notiRq.HotelReservations.HotelReservation[0].RoomStays[0];
            DateTime t;
            if (DateTime.TryParseExact(roomStay.TimeSpan.Start, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out t))
            {
                stayUpdated.Arrival = t;
            }
            else if (guestType.ArrivalTimeSpecified)
            {
                stayUpdated.Arrival = guestType.ArrivalTime;
            }
            
            if (DateTime.TryParseExact(roomStay.TimeSpan.End, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out t))
            {
                stayUpdated.Departure = t;
            }
            else if (guestType.DepartureTimeSpecified)
            {
                stayUpdated.Departure = guestType.DepartureTime;
            }

            if (guestType.Profiles != null && guestType.Profiles.Length > 0)
            {
                CustomerType customer = guestType.Profiles[0].Profile.Customer;
                stayUpdated.Vip = customer.VIP_Indicator;
                stayUpdated.Language = customer.Language;

                PersonNameType personName = customer.PersonName[0];
                stayUpdated.Title = (personName.NamePrefix != null && personName.NamePrefix.Length > 0) ? personName.NamePrefix[0] : "";
                stayUpdated.First = (personName.GivenName != null && personName.GivenName.Length > 0) ? personName.GivenName[0] : "";
                if (!personName.SurnamePrefix.IsNullOrWhiteSpace())
                {
                    stayUpdated.Last = string.Format("{0} {1}", personName.SurnamePrefix, personName.Surname);
                }
                else
                {
                    stayUpdated.Last = personName.Surname;
                }
            }   

            this.receivedMessages.Add(stayUpdated);

            return new StayUpdatedResponse
            {
                HTNG_HotelStayUpdateNotifRS = new HTNG_ResponseBaseType
                {
                    Items = new object[] { new SuccessType() },
                    EchoToken = notiRq.EchoToken,
                    TimeStamp = notiRq.TimeStamp,
                    TimeStampSpecified = true
                }
            };
        }

        public RoomMovedResponse RoomMoved(RoomMovedRequest request)
        {
            HTNG_HotelRoomMoveNotifRQ notiRq = request.HTNG_HotelRoomMoveNotifRQ;

            HTNG_HotelRoomMoveNotifRQSourceRoomInformation sourceRooms = notiRq.SourceRoomInformation;
            HTNG_HotelRoomMoveNotifRQDestinationRoomInformation destinationRooms = notiRq.DestinationRoomInformation;

            if (!sourceRooms.Room.RoomType.RoomID.Equals(destinationRooms.Room.RoomType.RoomID))
            {
                RoomMoved roomMoved = new RoomMoved();
                roomMoved.RoomIdOld = sourceRooms.Room.RoomType.RoomID;
                roomMoved.RoomId = destinationRooms.Room.RoomType.RoomID;

                this.receivedMessages.Add(roomMoved);
            }

            return new RoomMovedResponse
            {
                HTNG_HotelRoomMoveNotifRS = new HTNG_ResponseBaseType
                {
                    Items = new object[] { new SuccessType() },
                    EchoToken = notiRq.EchoToken,
                    TimeStamp = notiRq.TimeStamp,
                    TimeStampSpecified = true
                }
            };
        }
    }
}