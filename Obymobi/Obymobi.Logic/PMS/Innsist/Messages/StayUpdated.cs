﻿using System;
using Obymobi.Enums;

namespace Obymobi.Logic.PMS.Innsist.Messages
{
    public class StayUpdated : MessageBase
    {
        public string Title
        { get; set; }

        public string Last
        { get; set; }

        public string First
        { get; set; }

        public string Language
        { get; set; }

        public string Group
        { get; set; }

        public bool Vip
        { get; set; }

        public DateTime Arrival
        { get; set; }

        public DateTime Departure
        { get; set; }

        public string GuestId 
        { get; set; }

        public Model.GuestInformation ToGuestInformationModel()
        {
            Model.GuestInformation gi = new Model.GuestInformation();
            gi.DeliverypointNumber = this.RoomId;
            gi.Occupied = true;
            gi.Title = this.Title;
            gi.CustomerLastname = this.Last;
            gi.CustomerFirstname = this.First;
            gi.GuestId = this.GuestId;
            gi.LanguageCode = this.Language;
            gi.GroupName = this.Group;
            gi.Arrival = DateTime.SpecifyKind(this.Arrival, DateTimeKind.Unspecified);
            gi.Departure = DateTime.SpecifyKind(this.Departure, DateTimeKind.Unspecified);
            gi.Vip = this.Vip;

            gi.TvSetting = (int)PMSTvSetting.Unknown;
            gi.MinibarSetting = (int)PMSMinibarSetting.Unknown;

            gi.AllowViewFolio = true;
            gi.AllowExpressCheckout = true;

            return gi;
        }  
    }
}