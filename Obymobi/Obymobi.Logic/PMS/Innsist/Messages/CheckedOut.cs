﻿namespace Obymobi.Logic.PMS.Innsist.Messages
{
    public class CheckedOut : MessageBase
    {
        public string Title
        { get; set; }

        public string Last
        { get; set; }

        public string First
        { get; set; }

        public string GuestId { get; set; }

        public Model.GuestInformation ToGuestInformationModel()
        {
            Model.GuestInformation gi = new Model.GuestInformation();
            gi.DeliverypointNumber = this.RoomId;
            gi.Title = this.Title;
            gi.CustomerLastname = this.Last;
            gi.CustomerFirstname = this.First;
            gi.Occupied = false;
            gi.GuestId = this.GuestId;

            return gi;
        }
    }
}