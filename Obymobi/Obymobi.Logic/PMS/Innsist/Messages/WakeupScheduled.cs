﻿using System;
using Obymobi.Logic.InnsistClientSvc;
using Obymobi.Logic.Model;

namespace Obymobi.Logic.PMS.Innsist.Messages
{
    public class WakeupScheduled : MessageBase
    {
        public DateTime WakeUpDate { get; set; }
        public ActionType Status { get; set; }

        public WakeUpStatus ToWakeUpStatusModel()
        {
            WakeUpStatus wakeUpStatus = new WakeUpStatus();
            wakeUpStatus.DeliverypointNumber = this.RoomId;
            wakeUpStatus.WakeUpDate = DateTime.SpecifyKind(this.WakeUpDate, DateTimeKind.Unspecified);
            wakeUpStatus.WakeUpTime = DateTime.SpecifyKind(this.WakeUpDate, DateTimeKind.Unspecified);

            return wakeUpStatus;
        }
    }
}