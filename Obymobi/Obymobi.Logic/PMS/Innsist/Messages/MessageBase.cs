﻿namespace Obymobi.Logic.PMS.Innsist.Messages
{
    public class MessageBase : IPmsMessage
    {
        public string RoomId { get; set; }

        /// <summary>
        /// Gets a 'Signature' of the message that identifies the message.
        /// This could be a transaction id or a combination of the paramters used in the message
        /// </summary>
        /// <returns></returns>
        public string GetMessageSignature()
        {
            return string.Format("{0}-{1}", this.GetType().Name.ToLower(), this.RoomId);
        }

        /// <summary>
        /// Gets the 'Signature' of the message that will be the response for the message.
        /// </summary>
        /// <returns></returns>
        public string GetResponseMessageSignature()
        {
            return string.Empty;
        }

        /// <summary>
        /// Gets a description about the message for tracing/logging, should be RoomNo + Message Type
        /// </summary>
        public string MessageDescription { get; private set; }
    }
}