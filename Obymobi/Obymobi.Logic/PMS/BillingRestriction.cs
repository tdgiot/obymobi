﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Logic.PMS
{
    /// <summary>
    /// BillingRestriction enums
    /// </summary>
    public enum BillingRestriction
    {
        /// <summary>
        /// All is allowed
        /// </summary>
        AllAllowed = 0,
        /// <summary>
        /// All is restricted
        /// </summary>
        AllRestricted = 1,
        /// <summary>
        /// Viewing is allowed
        /// </summary>
        AllowViewing = 2,
        /// <summary>
        /// Remote express checkout is allowed
        /// </summary>
        AllowRemoteExpressCheckout = 3
    }
}
