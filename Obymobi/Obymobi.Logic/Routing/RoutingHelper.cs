﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Hosting;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Dionysos.Web;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.Routing.UseCases;
using Obymobi.Logic.Routing.Requests;
using Obymobi.Interfaces;
using SD.LLBLGen.Pro.QuerySpec;

namespace Obymobi.Logic.Routing
{
    /// <summary>
    ///     RoutingHelper class
    /// </summary>
    public static class RoutingHelper
    {
        public static Dictionary<RoutestephandlerType, IHandleRoutestepUseCase> RoutestepHandlerUseCases = new Dictionary<RoutestephandlerType, IHandleRoutestepUseCase>
        {
            { RoutestephandlerType.SMS, new HandleSendSmsRoutestepUseCase() },
            { RoutestephandlerType.EmailOrder, new HandleEmailOrderRoutestepUseCase() },
            { RoutestephandlerType.EmailDocument, new HandleEmailDocumentRoutestepUseCase() }
        };

        public static IEnumerable<OrderRoutestephandlerStatus> FinalOrderRoutestephandlerStatuses => new List<OrderRoutestephandlerStatus>
        {
            OrderRoutestephandlerStatus.Completed,
            OrderRoutestephandlerStatus.CompletedByOtherStep,
            OrderRoutestephandlerStatus.Failed,
            OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure,
        };

        /// <summary>
        ///     RoutingHelperResult enums
        /// </summary>
        public enum RoutingHelperResult
        {
            /// <summary>
            ///     Route is ambigious cause there are multiple categories with different routes
            /// </summary>
            AmbigiousRouteDueToMultipleCategoriesWithDifferentRoutes = 200,

            /// <summary>
            ///     Multiple active transactions are used
            /// </summary>
            MultipleActiveTransactions = 201,

            /// <summary>
            ///     Required parameters are missing
            /// </summary>
            RequiredParametersMissing = 202,

            /// <summary>
            ///     The order doesn't have a route
            /// </summary>
            NoRouteForOrder = 203,

            /// <summary>
            ///     No routesteps for this route
            /// </summary>
            RouteHasZeroSteps = 204,

            /// <summary>
            ///     The route doesn't start with number one
            /// </summary>
            RouteDoesNotStartWithNumberOne = 205,

            /// <summary>
            ///     The orderRoutestephandlerId is defined twice
            /// </summary>
            OrderroutestephandlerIdDefinedTwiceForUpdate = 206,

            /// <summary>
            ///     The orderRoutestephandlerId and the statusList aren't equal
            /// </summary>
            OrderroutestephandlerIdAndStatusListsAreUnequal = 207,

            /// <summary>
            ///     The orderRoutestep update is missing
            /// </summary>
            OrderroutestepUpdateMissing = 208,

            /// <summary>
            ///     The data is inconsistent
            /// </summary>
            InconsistentData = 209,

            /// <summary>
            ///     Paremeters are missing
            /// </summary>
            ParametersMissing = 210,

            /// <summary>
            ///     There are multiple orderRoutesteps active for the same terminal
            /// </summary>
            MultipleOrderRouteStepsForSameTerminalActive = 211,

            /// <summary>
            ///     There aren't any handlers for the routeStep
            /// </summary>
            RoutestepHasZeroHandlers = 212,

            /// <summary>
            ///     The orderRoutestep doesn't have any terminal
            /// </summary>
            NoTerminalSpecifiedForOrderRoutestep = 213,

            /// <summary>
            ///     The terminal used in the route is offline
            /// </summary>
            TerminalOfflineInRoute = 214,

            /// <summary>
            ///     Potential Stack overflow has been prevented
            /// </summary>
            PotentialStackOverflowPrevented = 215,

            /// <summary>
            ///     An attempt was made to change the Timeout on the orderRoutestephandler
            /// </summary>
            AttemptedToChangeTimedoutOrderRoutestephandler = 216,

            /// <summary>
            ///     An attempt was made to change a failed orderRoutestephandler
            /// </summary>
            AttemptedToChangeFailedOrderRoutestephandler = 217,

            /// <summary>
            ///     Attempt was made to update an orderRoutestephandler while terminal was not the owner of this step or has been set
            ///     as master
            /// </summary>
            AttemptToChangeOrderRoutestephandlerWhileNotOwnerOrMaster = 218
        }

        #region  Fields

        /// <summary>
        ///     FirstStepNumber
        /// </summary>
        public const int FIRST_STEP_NUMBER = 1;

        #endregion

        #region Properties

        private static ExcludeFieldsList ExcludeFieldValueFields
        {
            get
            {
                ExcludeFieldsList list = new ExcludeFieldsList();
                list.Add(OrderRoutestephandlerFields.FieldValue1);
                list.Add(OrderRoutestephandlerFields.FieldValue2);
                list.Add(OrderRoutestephandlerFields.FieldValue3);
                list.Add(OrderRoutestephandlerFields.FieldValue4);
                list.Add(OrderRoutestephandlerFields.FieldValue5);
                list.Add(OrderRoutestephandlerFields.FieldValue6);
                list.Add(OrderRoutestephandlerFields.FieldValue7);
                list.Add(OrderRoutestephandlerFields.FieldValue8);
                list.Add(OrderRoutestephandlerFields.FieldValue9);
                list.Add(OrderRoutestephandlerFields.FieldValue10);
                return list;
            }
        }

        public static string AppPath
        {
            get
            {
                string virtualPath = HostingEnvironment.ApplicationVirtualPath;
                if (virtualPath != null)
                {
                    if (virtualPath.Contains(ObymobiConstants.WebAppPathManagement))
                    {
                        virtualPath = virtualPath.Replace(ObymobiConstants.WebAppPathManagement, ObymobiConstants.WebAppPathApi);
                    }
                    else if (virtualPath.Contains(ObymobiConstants.WebAppPathServices))
                    {
                        virtualPath = virtualPath.Replace(ObymobiConstants.WebAppPathServices, ObymobiConstants.WebAppPathApi);
                    }
                }

                return HostingEnvironment.MapPath(virtualPath + "/App/");
            }
        }

        #endregion

        #region Route Writing

        /// <summary>
        ///     Gets the OrderRoutestephandler matching this order thats waiting to be processed
        /// </summary>
        /// <param name="orderId">The id of the order which we need the OrderRoutestephandler from</param>
        /// <param name="requireTerminal">if set to <c>true</c> [require terminal].</param>
        /// <param name="excludeIncludeFieldList">The exclude include field list.</param>
        /// <returns></returns>
        public static OrderRoutestephandlerCollection GetActiveOrderRoutestephandlers(int orderId, bool requireTerminal, ExcludeIncludeFieldsList excludeIncludeFieldList)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderRoutestephandlerFields.OrderId == orderId);
            filter.Add(OrderRoutestephandlerFields.Status == OrderRoutestephandlerStatus.WaitingToBeRetrieved);

            if (requireTerminal)
            {
                filter.Add(OrderRoutestephandlerFields.TerminalId != DBNull.Value);
            }

            OrderRoutestephandlerCollection orderRoutestephandlerCollection = new OrderRoutestephandlerCollection();
            orderRoutestephandlerCollection.GetMulti(filter);

            return orderRoutestephandlerCollection;
        }

        /// <summary>
        ///     Gets the active terminal ids.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>List of terminalIds</returns>
        public static List<int> GetActiveTerminalIds(int orderId)
        {
            // Provide an include list to prevent all fields from being retrieved, we need only FK fields
            // which are included ALWAYS.
            IncludeFieldsList includeFieldsList = new IncludeFieldsList();

            // Get the active terminals for this order
            OrderRoutestephandlerCollection steps = RoutingHelper.GetActiveOrderRoutestephandlers(orderId, true, includeFieldsList);

            // The query in GetActiveOrderRoutestephandlers ensures TerminalIds, therefore we can use .GetValueOrDefault
            return steps.Select(x => x.TerminalId.GetValueOrDefault()).ToList();
        }

        /// <summary>
        ///     Gets the Terminals linked to the OrderRoutestephandlers that are now WaitingToBeHandeled
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>TerminalCollection of active terminals</returns>
        public static TerminalCollection GetActiveTerminals(int orderId)
        {
            // Create an empty terminal collection
            TerminalCollection terminals = new TerminalCollection();

            // Provide an include list to prevent all fields from being retrieved, we need only FK fields
            // which are included ALWAYS.
            IncludeFieldsList includeFieldsList = new IncludeFieldsList();

            // Get the active terminals for this order
            OrderRoutestephandlerCollection steps = RoutingHelper.GetActiveOrderRoutestephandlers(orderId, true, includeFieldsList);

            // Get all the terminal entities
            foreach (OrderRoutestephandlerEntity orderRouteStephandler in steps)
            {
                terminals.Add(orderRouteStephandler.TerminalEntity);
            }

            return terminals;
        }

        /// <summary>
        ///     Fetches the correct route for an Order and then writes the OrderRoutesteps for that order.
        ///     If no correct route can be found the Order.Status will be set to Unroutable and an exception will be thrown.
        /// </summary>
        /// <param name="order">Order to write the route for</param>
        /// <param name="startRoute">
        ///     If set to <c>true</c> the route will be started directly by setting the first step(s) to
        ///     WaitingToBeHandled.
        /// </param>
        public static void WriteRouteForOrder(OrderEntity masterOrder, bool startRoute)
        {
            // If the order has a route already, no need to write one, unless it's an escalation route!
            if (masterOrder.OrderRoutestephandlerCollection.Count > 0)
            {
                return;
            }

            try
            {
                // Split the order into separate orders if needed
                Dictionary<int, OrderEntity> orderPerRoute = RoutingHelper.GetOrdersForRouting(masterOrder);
                foreach (int routeId in orderPerRoute.Keys)
                {
                    // Get the order
                    OrderEntity order = orderPerRoute[routeId];

                    // Get the route for this order, throws an exception when it can't be
                    // determined.
                    RouteEntity route = RoutingHelper.GetRoutePrefetched(routeId, masterOrder.GetCurrentTransaction());

                    // Write the route if one is available.
                    if (route == null || route.IsNew)
                    {
                        // Throw exception, can't proceed.
                        throw new ObymobiException(RoutingHelperResult.NoRouteForOrder, "OrderId: {0}\r\nRoute Info: {1}", order.OrderId, order.RoutingLog);
                    }

                    // Create the route
                    RoutingHelper.WriteRoute(order, route, startRoute);

                    // MDB ROUTING
                    // OrderStatus is already set to Routed in RoutingHelper.WriteRoute
                    // Mark order ready (it's now routed) - But only when not waiting for Payment
                    //if (order.StatusAsEnum != OrderStatus.WaitingForPaymentCompleted)
                    //    order.StatusAsEnum = OrderStatus.Routed;

                    // Write some diagnostics info
                    if (!order.RoutingLog.IsNullOrWhiteSpace())
                    {
                        order.RoutingLog += "\r\n";
                    }
                    order.RoutingLog += string.Format("Routed according to Route: '{0}' (RouteId: {1})", route.Name, route.RouteId);
                    order.Save();
                }
            }
            catch (ObymobiException oby)
            {
                // No route available, order fails.
                if (!masterOrder.RoutingLog.IsNullOrWhiteSpace())
                {
                    masterOrder.RoutingLog += "\r\n";
                }
                masterOrder.RoutingLog += string.Format("RoutingHelper.WriteRouteForOrder Failed: {0} - {1}", oby.Message, oby.ErrorEnumValueText);
                masterOrder.StatusAsEnum = OrderStatus.Unprocessable;
                masterOrder.ErrorCodeAsEnum = OrderProcessingError.Unroutable;
                masterOrder.Save();
                throw;
            }
            catch (Exception ex)
            {
                // No route available, order fails.
                if (!masterOrder.RoutingLog.IsNullOrWhiteSpace())
                {
                    masterOrder.RoutingLog += "\r\n";
                }
                masterOrder.RoutingLog += string.Format("RoutingHelper.WriteRouteForOrder Failed: {0}", ex.Message);
                masterOrder.StatusAsEnum = OrderStatus.Unprocessable;
                masterOrder.ErrorCodeAsEnum = OrderProcessingError.Unroutable;
                masterOrder.Save();
                throw;
            }
        }

        private static Dictionary<int, OrderEntity> GetOrdersForRouting(OrderEntity masterOrder)
        {
            Dictionary<int, OrderEntity> orders = new Dictionary<int, OrderEntity>();

            // Create a dictionary containing the orderitem ids per route
            Dictionary<int, List<int>> orderitemsPerRoute = new Dictionary<int, List<int>>();

            // Check for orderitems
            if (masterOrder.OrderitemCollection.Count == 0)
            {
                // This order is a default service item without any orderitems

                // Get the routeId for this order
                int? routeId = RoutingHelper.GetRouteIdForOrder(masterOrder);
                if (routeId.HasValue)
                {
                    List<int> orderitemIds = new List<int>();
                    orderitemIds.Add(-1);
                    orderitemsPerRoute.Add(routeId.Value, orderitemIds);
                }
            }
            else
            {
                // This loop gets the different routes for the order
                // and fills the dictionary with the route ids and corresponding order item ids
                foreach (OrderitemEntity orderitem in masterOrder.OrderitemCollection)
                {
                    // Get the route id for the current order item
                    int? routeId = RoutingHelper.GetRouteIdForOrderitem(orderitem);
                    if (routeId.HasValue)
                    {
                        // Check whether the route was already in the dictionary
                        if (orderitemsPerRoute.ContainsKey(routeId.Value))
                        {
                            List<int> orderitemIds = orderitemsPerRoute[routeId.Value];
                            orderitemIds.Add(orderitem.OrderitemId);
                            orderitemsPerRoute[routeId.Value] = orderitemIds;
                        }
                        else
                        {
                            List<int> orderitemIds = new List<int>();
                            orderitemIds.Add(orderitem.OrderitemId);
                            orderitemsPerRoute.Add(routeId.Value, orderitemIds);
                        }
                    }
                }
            }

            bool isMasterOrder = true;
            int masterRouteId = -1;
            foreach (int routeId in orderitemsPerRoute.Keys)
            {
                if (isMasterOrder) // Skip the first one, so some orderitems wil stay on the original order
                {
                    masterRouteId = routeId;
                }
                else
                {
                    // Get the orderitem ids 
                    List<int> orderitemIds = orderitemsPerRoute[routeId];

                    // Get the orderitems
                    OrderitemCollection orderitemCollection = new OrderitemCollection();
                    orderitemCollection.AddToTransaction(masterOrder.GetCurrentTransaction());
                    orderitemCollection.GetMulti(OrderitemFields.OrderitemId == orderitemIds);

                    EntityView<OrderitemEntity> orderitemView = orderitemCollection.DefaultView;

                    // Create a copy of the master order
                    OrderEntity order = new OrderEntity();
                    order.AddToTransaction(masterOrder.GetCurrentTransaction());

                    // Copy the field values of the master order
                    LLBLGenEntityUtil.CopyFields(masterOrder, order);

                    // Set the master order id
                    order.MasterOrderId = masterOrder.OrderId;
                    order.Guid = Guid.NewGuid().ToString();
                    order.ValidatedByOrderProcessingHelper = true;

                    // Save the order
                    if (order.Save())
                    {
                        foreach (int orderitemId in orderitemIds)
                        {
                            orderitemView.Filter = new PredicateExpression(OrderitemFields.OrderitemId == orderitemId);
                            if (orderitemView.Count == 1)
                            {
                                OrderitemEntity orderitem = orderitemView[0];
                                orderitem.AddToTransaction(masterOrder.GetCurrentTransaction());
                                if (!orderitem.IsNew)
                                {
                                    orderitem.OverrideChangeProtection = true; // Set the override flag otherwise an exception will be thrown because an existing orderitem cannot be changed anymore
                                    orderitem.OrderId = order.OrderId;
                                    orderitem.Save();
                                }
                            }
                        }

                        // Refetch the order
                        // and put it in the order list
                        order.Refetch();
                        orders.Add(routeId, order);
                    }
                }

                isMasterOrder = false;
            }

            // Refetch the order
            // and put it in the order list
            masterOrder.AlreadyFetchedOrderitemCollection = false;
            orders.Add(masterRouteId, masterOrder);

            return orders;
        }

        /// <summary>
        ///     Writes the escalation route for order.
        /// </summary>
        /// <param name="order">The order.</param>
        /// <param name="route">The route.</param>
        public static void WriteEscalationRouteForOrder(OrderEntity order, RouteEntity route)
        {
            RoutingHelper.WriteRoute(order, route, true, true);
        }

        /// <summary>
        ///     Write the OrderRoutestep Entities for an Order and set Order.Status to .Routed
        /// </summary>
        /// <param name="order">Order to write the route for, and of which the status will be set to Routed</param>
        /// <param name="route">Route which to use a template to write the route.</param>
        /// <param name="startRoute">Specifies if the route should start after creation</param>
        /// <param name="escalationRoute">if set to <c>true</c> it's written as an escalation route.</param>
        private static void WriteRoute(OrderEntity order, RouteEntity route, bool startRoute, bool escalationRoute = false)
        {
            // Precheck the route (before flight ;))
            if (route.RoutestepCollection.Count == 0)
            {
                // No steps in the route, at least one would be required.
                throw new ObymobiEntityException(RoutingHelperResult.RouteHasZeroSteps, route);
            }
            if (route.RoutestepCollection.Min(x => x.Number) != RoutingHelper.FIRST_STEP_NUMBER)
            {
                // The first step MUST start with One
                throw new ObymobiEntityException(RoutingHelperResult.RouteDoesNotStartWithNumberOne, route);
            }

            // Stepnumber increase when glueing a escaltionRoute to an existing route
            int stepNumberIncrease = 0;
            if (escalationRoute)
            {
                stepNumberIncrease = order.OrderRoutestephandlerCollection.Max(x => x.Number);
            }

            // Iterate over each step
            List<OrderRoutestephandlerEntity> firstSteps = new List<OrderRoutestephandlerEntity>();
            int lastStepnumber = route.RoutestepCollection.Max(rs => rs.Number);
            foreach (RoutestepEntity templateStep in route.RoutestepCollection)
            {
                // Iterate over the handlers of the step
                foreach (RoutestephandlerEntity templateStepHandler in templateStep.RoutestephandlerCollection)
                {
                    // Create the new step that we will save for the order
                    OrderRoutestephandlerEntity actualStep = new OrderRoutestephandlerEntity();

                    // Copy all fields from step and stephandle
                    LLBLGenEntityUtil.CopyFields(templateStep, actualStep);
                    LLBLGenEntityUtil.CopyFields(templateStepHandler, actualStep);

                    // Set non-copyable fields
                    actualStep.Number = templateStep.Number + stepNumberIncrease;
                    actualStep.OrderId = order.OrderId;
                    actualStep.LogAlways = route.LogAlways;
                    actualStep.Timeout = templateStep.TimeoutMinutes.HasValue && templateStep.TimeoutMinutes.Value > 0 ? templateStep.TimeoutMinutes.Value : route.StepTimeoutMinutes;
                    actualStep.EscalationStep = escalationRoute;
                    actualStep.EscalationRouteId = templateStep.RouteEntity.EscalationRouteId;
                    actualStep.BeingHandledSupportNotificationTimeoutMinutes = route.BeingHandledSupportNotificationTimeoutMinutes ?? 15; // Don't change this without updating the Google Doc 'Intervals & Treshholds'
                    actualStep.RetrievalSupportNotificationTimeoutMinutes = route.RetrievalSupportNotificationTimeoutMinutes ?? 3; // Don't change this without updating the Google Doc 'Intervals & Treshholds'
                    actualStep.OriginatedFromRoutestepHandlerId = templateStepHandler.RoutestephandlerId;
                    actualStep.File = order.File;

                    // Disable continue on failure for last step
                    if (templateStep.Number == lastStepnumber)
                    {
                        actualStep.ContinueOnFailure = false;
                    }

                    // All steps, including the first will always be set to be waiting untill the
                    // order is triggered to be started bij this RoutingHelper
                    actualStep.Status = (int)OrderRoutestephandlerStatus.WaitingForPreviousStep;

                    // Set Transaction
                    actualStep.AddToTransaction(order);

                    // Save
                    actualStep.Save();

                    // Record the first step number we have created to later activate that as the first step
                    if (templateStep.Number == RoutingHelper.FIRST_STEP_NUMBER)
                    {
                        firstSteps.Add(actualStep);
                    }
                }
            }

            // Mark order as Routed (not when escaltion route, it was already in-route)
            if (!escalationRoute)
            {
                order.StatusAsEnum = OrderStatus.Routed;
            }

            order.Save();

            // Start route if requested to do so (here, because we want the route to be completely written before it starts)
            if (startRoute)
            {
                foreach (OrderRoutestephandlerEntity step in firstSteps)
                {
                    step.StatusAsEnum = OrderRoutestephandlerStatus.WaitingToBeRetrieved;

                    RoutingHelper.SetForwardedTerminal(step);

                    step.Save();
                }
            }
        }

        public static void SetForwardedTerminal(OrderRoutestephandlerEntity step)
        {
            // Set the correct terminal, since it might be forwarded
            TerminalCollection terminals = new TerminalCollection();
            terminals.AddToTransaction(step);
            if (step.TerminalId.HasValue)
            {
                PredicateExpression terminalFilter = new PredicateExpression();
                terminalFilter.Add(TerminalFields.TerminalId == step.TerminalId);
                TerminalEntity handlingTerminal = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<TerminalEntity>(terminalFilter, terminals);
                TerminalEntity forwardToTerminal = TerminalHelper.GetTerminalToForwardTo(handlingTerminal);
                if (forwardToTerminal != null)
                {
                    step.ForwardedFromTerminalId = step.TerminalId;
                    step.TerminalId = forwardToTerminal.TerminalId;
                }
            }
        }

        #endregion

        #region Route Processing

        /// <summary>
        ///     Starts the route by setting the steps with number 1 to 'WaitingToBeHandled'
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <param name="transaction">The transaction.</param>
        public static void StartRoute(int orderId, ITransaction transaction)
        {
            // Create filter to get all first steps for this order
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderRoutestephandlerFields.OrderId == orderId);
            filter.Add(OrderRoutestephandlerFields.Number == RoutingHelper.FIRST_STEP_NUMBER);

            // Retrieve the steps
            OrderRoutestephandlerCollection steps = new OrderRoutestephandlerCollection();

            IncludeFieldsList includefields = new IncludeFieldsList();
            includefields.Add(OrderRoutestephandlerFields.Status);

            steps.AddToTransaction(transaction);
            steps.GetMulti(filter, includefields, null);

            // Update the statuses
            foreach (OrderRoutestephandlerEntity step in steps)
            {
                step.StatusAsEnum = OrderRoutestephandlerStatus.WaitingToBeRetrieved;
                step.AddToTransaction(transaction);
                step.Save();
            }
        }

        /// <summary>
        ///     Process Routingstephandler without a terminal or human interaction.
        ///     WARNING: These routingsteps are handled in a seperated thread, which means your current main
        ///     thread is useless and you should go cry in a corner if something goes wrond.
        /// </summary>
        /// <param name="orderRoutestephandlerEntity">The OrderRoutestephandlerEntity to process</param>
        public static void HandleNoTerminalRoutingstephandling(OrderRoutestephandlerEntity orderRoutestephandlerEntity, Func<MediaEntity, byte[]> downloadMediaDelegate)
        {
            Task.Factory.StartNew(() =>
                                  {
                                      // Set Thread Variable to prevent Authorization
                                      if (Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable) == null)
                                          Thread.AllocateNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable);

                                      Thread.SetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable), true);

                                      if (!RoutestepHandlerUseCases.ContainsKey(orderRoutestephandlerEntity.HandlerTypeAsEnum))
                                          return;

                                      IHandleRoutestepUseCase handleRoutestepUseCase = RoutestepHandlerUseCases[orderRoutestephandlerEntity.HandlerTypeAsEnum];
                                      if (handleRoutestepUseCase == null)
                                          return;

                                      try
                                      {
                                          switch (orderRoutestephandlerEntity.HandlerTypeAsEnum)
                                          {
                                              case RoutestephandlerType.SMS:
                                                  HandleSendSmsRoutestepRequest handleSendSmsRoutestepRequest = new HandleSendSmsRoutestepRequest
                                                  {
                                                      OrderRoutestephandlerEntityId = orderRoutestephandlerEntity.OrderRoutestephandlerId,
                                                  };
                                                  handleRoutestepUseCase.HandleRoutestep(handleSendSmsRoutestepRequest);
                                                  break;

                                              case RoutestephandlerType.EmailOrder:
                                                  HandleEmailOrderRoutestepRequest handleEmailOrderRoutestepRequest = new HandleEmailOrderRoutestepRequest
                                                  {
                                                      OrderRoutestephandlerEntityId = orderRoutestephandlerEntity.OrderRoutestephandlerId,
                                                      EmailHtml = Properties.Resources.NewOrder,
                                                      DownloadMediaDelegate = downloadMediaDelegate
                                                  };
                                                  handleRoutestepUseCase.HandleRoutestep(handleEmailOrderRoutestepRequest);
                                                  break;

                                              case RoutestephandlerType.HotSOS:
                                              case RoutestephandlerType.Quore:
                                              case RoutestephandlerType.Hyatt_HotSOS:
                                              case RoutestephandlerType.Alice:

                                                  HandleRoutestepRequest handleServiceRoutestepRequest = new HandleRoutestepRequest
                                                  {
                                                      OrderRoutestephandlerEntityId = orderRoutestephandlerEntity.OrderRoutestephandlerId
                                                  };
                                                  handleRoutestepUseCase.HandleRoutestep(handleServiceRoutestepRequest);
                                                  break;

                                              case RoutestephandlerType.EmailDocument:

                                                  HandleEmailDocumentRoutestepRequest handleEmailDocumentRoutestepRequest = new HandleEmailDocumentRoutestepRequest
                                                  {
                                                      OrderRoutestephandlerEntityId = orderRoutestephandlerEntity.OrderRoutestephandlerId,
                                                      File = orderRoutestephandlerEntity.File,
                                                      EmailHtml = Properties.Resources.NewPrintRequest
                                                  };
                                                  handleRoutestepUseCase.HandleRoutestep(handleEmailDocumentRoutestepRequest);
                                                  break;

                                              default:
                                                  throw new NotImplementedException("RoutingHelper.HandleNoTerminalRoutingstephandling not yet implemented for HandlerType" + orderRoutestephandlerEntity.HandlerTypeAsEnum);
                                          }
                                      }
                                      finally
                                      {
                                          // Unset GodMode
                                          if (Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable) == null)
                                              Thread.AllocateNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable);

                                          Thread.SetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable), false);
                                      }
                                  });
        }

        /// <summary>
        ///     Updates the order status based on the orderRoutestephandlers in an order
        /// </summary>
        /// <param name="order">The order</param>
        /// <param name="status">The status</param>
        public static void UpdateOrderStatus(OrderEntity order, OrderStatus status, bool onlyWhenFailed, TerminalEntity terminal = null)
        {
            if (order != null)
            {
                // Check if the order is processed manually
                if (status == OrderStatus.Processed)
                {
                    if (order.OrderRoutestephandlerCollection.Count > 0)
                    {
                        // Check if a orderRoutestephandler failed
                        foreach (OrderRoutestephandlerEntity orderRoutestephandler in order.OrderRoutestephandlerCollection)
                        {
                            if (onlyWhenFailed && orderRoutestephandler.Status != (int)OrderRoutestephandlerStatus.Failed)
                            {
                                continue;
                            }

                            // If terminal is not orignal owner of the orderroutestephandler and the terminal is not activated as master, throw exception
                            if (terminal != null && orderRoutestephandler.TerminalId.HasValue && orderRoutestephandler.TerminalId.Value != terminal.TerminalId && !terminal.MasterTab)
                            {
                                throw new ObymobiException(RoutingHelperResult.AttemptToChangeOrderRoutestephandlerWhileNotOwnerOrMaster, "Attemted to change OrderRoutestephandler while not being owner of the step or master. OrderroutestephandlerId: {0}", orderRoutestephandler.OrderRoutestephandlerId);
                            }

                            // Add the entity to the transaction
                            Transaction transaction = new Transaction(IsolationLevel.ReadCommitted, "manuallyProcessOrder");

                            try
                            {
                                // The current step failed
                                orderRoutestephandler.Status = (int)OrderRoutestephandlerStatus.Completed;
                                orderRoutestephandler.ErrorCode = (int)OrderProcessingError.ManuallyProcessed;
                                orderRoutestephandler.IsDirty = true; // Just to make sure the entity is dirty
                                orderRoutestephandler.Fields[(int)OrderRoutestephandlerFieldIndex.Status].IsChanged = true; // Otherwise it will not do it's logic when the status hasn't changed
                                orderRoutestephandler.AddToTransaction(transaction);
                                orderRoutestephandler.Save();

                                transaction.Commit();
                            }
                            catch
                            {
                                transaction.Rollback();
                                throw;
                            }
                            finally
                            {
                                transaction.Dispose();
                            }

                            // Get out of this loop quick
                            break;
                        }
                    }
                    else
                    {
                        // Add the entity to the transaction
                        Transaction transaction = new Transaction(IsolationLevel.ReadCommitted, "manuallyProcessOrder");

                        try
                        {
                            // The current step failed
                            order.Status = (int)OrderStatus.Processed;
                            // GK Never remove the reason of the error!
                            // order.ErrorCode = (int)OrderProcessingError.ManuallyProcessed; 
                            order.AddToTransaction(transaction);
                            order.Save();

                            transaction.Commit();
                        }
                        catch
                        {
                            transaction.Rollback();
                            throw;
                        }
                        finally
                        {
                            transaction.Dispose();
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Updates the orderroutestephandler with the statuses and errors that have been supplied
        /// </summary>
        /// <param name="orderroutestephandlerId">The orderroutestephandler id</param>
        /// <param name="stepStatus">The step status.</param>
        /// <param name="stepError">The step error.</param>
        public static void UpdateOrderroutestephandler(int orderroutestephandlerId, OrderRoutestephandlerStatus stepStatus, OrderProcessingError stepError)
        {
            OrderRoutestephandlerSaveStatus status = new OrderRoutestephandlerSaveStatus();
            status.OrderRoutestephandlerId = orderroutestephandlerId;
            status.Error = (int)stepError;
            status.Status = (int)stepStatus;

            List<OrderRoutestephandlerSaveStatus> statuses = new List<OrderRoutestephandlerSaveStatus>();
            statuses.Add(status);

            RoutingHelper.UpdateOrderroutestephandlers(statuses);
        }

        /// <summary>
        ///     Updates the orderroutestephandlers with the statuses and errors that have been supplied
        /// </summary>
        /// <param name="routestephandlerSaveStatuses">The list of orderroutestephandlerSaveStatuses</param>
        public static void UpdateOrderroutestephandlers(List<OrderRoutestephandlerSaveStatus> routestephandlerSaveStatuses, TerminalEntity terminal = null)
        {
            // Retrieve the relevant Orderroutesteps (based on Guid or Id)
            List<int> orderroutestephandlerIds = routestephandlerSaveStatuses.Select(step => step.OrderRoutestephandlerId).ToList();
            List<string> orderroutestephandlerGuids = routestephandlerSaveStatuses.Select(step => step.OrderRoutestephandlerGuid).ToList();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderRoutestephandlerFields.OrderRoutestephandlerId == orderroutestephandlerIds);
            filter.AddWithOr(OrderRoutestephandlerFields.Guid == orderroutestephandlerGuids);

            // Don't load unnecessary data
            ExcludeFieldsList excludeFields = new ExcludeFieldsList();
            excludeFields.Add(OrderRoutestephandlerFields.FieldValue1);
            excludeFields.Add(OrderRoutestephandlerFields.FieldValue2);
            excludeFields.Add(OrderRoutestephandlerFields.FieldValue3);
            excludeFields.Add(OrderRoutestephandlerFields.FieldValue4);
            excludeFields.Add(OrderRoutestephandlerFields.FieldValue5);
            excludeFields.Add(OrderRoutestephandlerFields.FieldValue6);
            excludeFields.Add(OrderRoutestephandlerFields.FieldValue7);
            excludeFields.Add(OrderRoutestephandlerFields.FieldValue8);
            excludeFields.Add(OrderRoutestephandlerFields.FieldValue9);
            excludeFields.Add(OrderRoutestephandlerFields.FieldValue9);

            // Retrieve from db.
            OrderRoutestephandlerCollection steps = new OrderRoutestephandlerCollection();
            steps.GetMulti(filter, excludeFields, null);

            // Now update all the steps that were found
            // If a step is not found, because the route was deleted, it won't be fetched and won't be processed
            // but it's no problem the Order was Finished (either in Complete or Failed already)
            Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "RoutingeHelper.UpdateOrderroutestephandlers" + DateTime.UtcNow.Ticks);
            try
            {
                foreach (OrderRoutestephandlerEntity step in steps)
                {
                    // Get status by either Id or Guid, first ID, then Guid
                    OrderRoutestephandlerSaveStatus status = routestephandlerSaveStatuses.FirstOrDefault(saveStatus => saveStatus.OrderRoutestephandlerId == step.OrderRoutestephandlerId);

                    if (status == null)
                    {
                        status = routestephandlerSaveStatuses.FirstOrDefault(saveStatus => saveStatus.OrderRoutestephandlerGuid == step.Guid);
                    }

                    if (status == null)
                    {
                        throw new ObymobiException(RoutingHelperResult.OrderroutestepUpdateMissing, "Missing status - OrderroutestephandlerId: {0}", step.OrderRoutestephandlerId);
                    }

                    if (terminal != null)
                    {
                        // Throw exception when terminal is trying to update an orderroutestep linked to another terminal while it's not active as master
                        if (step.TerminalEntity.TerminalId != terminal.TerminalId && !terminal.MasterTab)
                        {
                            throw new ObymobiException(RoutingHelperResult.AttemptToChangeOrderRoutestephandlerWhileNotOwnerOrMaster, "Attemted to change OrderRoutestephandler while not being owner of the step or master. OrderroutestephandlerId: {0}", step.OrderRoutestephandlerId);
                        }
                    }

                    if (status.Status != (int)OrderRoutestephandlerStatus.Failed && step.StatusAsEnum == OrderRoutestephandlerStatus.Failed)
                    {
                        if (step.ErrorCodeAsEnum == OrderProcessingError.TimedOut)
                        {
                            throw new ObymobiException(RoutingHelperResult.AttemptedToChangeTimedoutOrderRoutestephandler, "Attempted to change timed out OrderRoutestephandler. OrderroutestephandlerId: {0}", step.OrderRoutestephandlerId);
                        }

                        throw new ObymobiException(RoutingHelperResult.AttemptedToChangeFailedOrderRoutestephandler, "Attempted to change failed OrderRoutestephandler. OrderroutestephandlerId: {0}", step.OrderRoutestephandlerId);
                    }

                    step.Status = status.Status;
                    step.ErrorCode = status.Error;
                    step.ErrorText = status.ErrorMessage;

                    // Add to transaction and save.
                    step.AddToTransaction(transaction);
                    step.Save();
                }

                transaction.Commit();
            }
            catch
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                transaction.Dispose();
            }
        }

        /// <summary>
        ///     Cancel routes (and orders) based on filters supplied by parameters. Parameters are used as AND (not OR)
        /// </summary>
        /// <param name="customerId">The customer id to cancel all orders for (0 for no filter).</param>
        /// <param name="clientId">The client id to cancel all order for (0 for no filter).</param>
        /// <param name="companyId">The company id to cancel all orders for (0 for no filter).</param>
        /// <param name="orderGuid">The id of the client to process the orders for (string.Empty for now filter)</param>
        /// <param name="errorCode">The error code to set the order to.</param>
        public static void CancelRoutes(int customerId, int clientId, int companyId, string orderGuid, OrderProcessingError? errorCode)
        {
            if (customerId <= 0 && clientId <= 0 && orderGuid.IsNullOrWhiteSpace())
            {
                throw new ObymobiException(RoutingHelperResult.ParametersMissing, "RoutingHelper.CancelOrder requires at least 1 parameter to be filled");
            }

            // We only allow to cancel orders that are not being handled (route was not started)
            PredicateExpression filter = new PredicateExpression();

            if (companyId > 0)
            {
                filter.Add(OrderFields.CompanyId == companyId);
            }

            if (customerId > 0)
            {
                filter.Add(OrderFields.CustomerId == customerId);
            }

            if (clientId > 0)
            {
                filter.Add(OrderFields.ClientId == clientId);
            }

            if (!orderGuid.IsNullOrWhiteSpace())
            {
                filter.Add(OrderFields.Guid == orderGuid);
            }

            filter.Add(OrderFields.Status != OrderStatus.InRoute & OrderFields.Status != OrderStatus.Processed);

            OrderCollection orders = new OrderCollection();
            PrefetchPath path = new PrefetchPath(EntityType.OrderEntity);
            path.Add(OrderEntityBase.PrefetchPathOrderRoutestephandlerCollection);
            orders.GetMulti(filter, path);

            Transaction transaction = new Transaction(IsolationLevel.ReadCommitted, $"RoutingHelper.CancelRoute-{DateTime.UtcNow.Ticks}");
            try
            {
                foreach (OrderEntity order in orders)
                {
                    RoutingHelper.UpdateOrderStatus(order, OrderStatus.Processed, false);
                    //// Clean up route
                    //foreach (var step in order.OrderRoutestephandlerCollection)
                    //{
                    //    step.AddToTransaction(transaction);
                    //    step.Delete();
                    //}

                    //// Set order to Processed with Error: Cancelled
                    //order.AddToTransaction(transaction);
                    //order.StatusAsEnum = OrderStatus.Processed;
                    //if (errorCode.HasValue)
                    //    order.ErrorCodeAsEnum = errorCode.Value;
                    //order.RoutingLog += "\r\n Order was cancelled (RoutingHelper.CancelRoute)\r\n";
                    //order.RoutingLog += RoutingHelper.OrderroutestephandlersToCsv(order.OrderRoutestephandlerCollection);
                    //order.Save();

                    transaction.Commit();
                }
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                transaction.Dispose();
            }
        }

        /// <summary>
        ///     Checks the timeout orderroutestephandler, and saves it with FAILED if it expired.
        /// </summary>
        public static string FailExpiredRoutestephandlers()
        {
            return RoutingHelper.FailExpiredRoutestephandlers(null, null, null);
        }

        /// <summary>
        ///     Checks the timeout orderroutestephandler, and saves it with FAILED if it expired.
        ///     GK: This is a sort of temp method, we need to get a centralized, maybe even scheduled method, to expire
        ///     Orderroutestephandlers
        /// </summary>
        /// <param name="terminalId">The terminal id.</param>
        /// <param name="transaction">The transaction in which to perform the actions..</param>
        public static void FailExpiredRoutestephandlers(int? terminalId, ITransaction transaction)
        {
            RoutingHelper.FailExpiredRoutestephandlers(terminalId, transaction, null);
        }

        /// <summary>
        ///     Checks the timeout orderroutestephandler, and saves it with FAILED if it expired.
        ///     GK: This is a sort of temp method, we need to get a centralized, maybe even scheduled method, to expire
        ///     Orderroutestephandlers
        /// </summary>
        /// <param name="orderIds">The order ids.</param>
        /// <param name="transaction">The transaction in which to perform the actions.</param>
        public static void FailExpiredRoutestephandlers(List<int> orderIds, ITransaction transaction)
        {
            RoutingHelper.FailExpiredRoutestephandlers(null, transaction, orderIds);
        }

        /// <summary>
        ///     Checks the timeout orderroutestephandler, and saves it with FAILED if it expired.
        ///     GK: This is a sort of temp method, we need to get a centralized, maybe even scheduled method, to expire
        ///     Orderroutestephandlers
        /// </summary>
        /// <param name="terminalId">The terminal id.</param>
        /// <param name="transaction">The transaction in which to perform the actions.</param>
        /// <param name="orderIds">The order ids to update the steps for, this is .</param>
        /// <param name="attemptNo">The attempt no.</param>
        private static string FailExpiredRoutestephandlers(int? terminalId, ITransaction transaction, List<int> orderIds)
        {
            StringBuilder sb = new StringBuilder();

            // Perform actual logic
            OrderRoutestephandlerCollection steps = new OrderRoutestephandlerCollection();

            // Filter for all expired orders that haven't been completed/failed yet.
            PredicateExpression filter = new PredicateExpression();

            if (terminalId.HasValue)
            {
                filter.Add(OrderRoutestephandlerFields.TerminalId == terminalId);
            }

            if (orderIds != null)
            {
                filter.Add(OrderRoutestephandlerFields.OrderId == orderIds);
            }

            filter.Add(OrderRoutestephandlerFields.TimeoutExpiresUTC != DBNull.Value);
            filter.Add(OrderRoutestephandlerFields.TimeoutExpiresUTC < DateTime.UtcNow);
            filter.Add(OrderRoutestephandlerFields.Status.NotIn(FinalOrderRoutestephandlerStatuses));

            // Exception (yes, I hate them too...) 
            // On-the-case console's won't time-out
            PredicateExpression filterExcludeOnTheCaseConsoles = new PredicateExpression();
            filterExcludeOnTheCaseConsoles.Add(OrderRoutestephandlerFields.Status == OrderRoutestephandlerStatus.BeingHandled);
            filterExcludeOnTheCaseConsoles.Add(OrderRoutestephandlerFields.HandlerType == RoutestephandlerType.Console);
            filterExcludeOnTheCaseConsoles.Negate = true;
            filter.Add(filterExcludeOnTheCaseConsoles);

            if (transaction != null)
            {
                transaction.Add(steps);
            }

            steps.GetMulti(filter);

            IList<int> processedOrderIds = new List<int>();

            foreach (OrderRoutestephandlerEntity step in steps)
            {
                if (processedOrderIds.Contains(step.OrderId) && step.Refetch() && FinalOrderRoutestephandlerStatuses.Contains(step.StatusAsEnum))
                {
                    // Keep track of the OrderIds that we have handled. We need to refetch future steps per Order 
                    // because the validator can update parallel steps and we would have 'out-of-sync' data
                    // as the step would be updated in the DB but here in memory still have the old data.
                    continue;
                }

                ITransaction localTransaction = null;

                try
                {
                    processedOrderIds.Add(step.OrderId);
                    step.StatusAsEnum = OrderRoutestephandlerStatus.Failed;
                    step.ErrorCodeAsEnum = OrderProcessingError.TimedOut;

                    if (transaction == null)
                    {
                        localTransaction = new Transaction(IsolationLevel.ReadUncommitted, "FailExpiredRoutestephandlers-Order-" + step.OrderId);
                        localTransaction.Add(step);
                    }

                    step.Save();

                    localTransaction?.Commit();
                }
                catch (Exception e)
                {
                    sb.AppendLine($"Failed to updated status. (OrderRoutestephandlerId={step.OrderRoutestephandlerId}, OrderId={step.OrderId}). Exception: {e.Message}.");
                    localTransaction?.Rollback();
                }
                finally
                {
                    localTransaction?.Dispose();
                }
            }

            return sb.ToString();
        }

        /// <summary>
        ///     Sends the orderroutestep handler status via comet (for performance prefetch the Order on the step).
        /// </summary>
        /// <param name="step">The step.</param>
        public static void SendOrderroutestepHandlerStatusViaComet(OrderRoutestephandlerEntity step)
        {
            // For now only to terminals & don't sent useless updates (which is, waiting for previous step)
            if (step.TerminalId.HasValue && step.StatusAsEnum != OrderRoutestephandlerStatus.WaitingForPreviousStep)
            {
                // In case you come here due to performance problems, maybe think about writing the companyId and orderId to the OrderroutestephandlerEntity
                //				int customerId = (step.OrderEntity.CustomerId.HasValue ? step.OrderEntity.CustomerId.Value : 0);
                int clientId = (step.OrderEntity.ClientId.HasValue ? step.OrderEntity.ClientId.Value : 0);

                // Try to send the order placed message to PokeIn
                try
                {
                    // Send message to PokeIn service.. an order has been placed for company
                    CometHelper.SendRoutestephandlerStatus(step.OrderEntity.CompanyId, step.OrderEntity.OrderId, step.OrderRoutestephandlerId, step.StatusAsEnum, clientId, step.TerminalId.Value);
                }
                catch
                {
                    if (TestUtil.IsPcDeveloper)
                    {
                        throw;
                    }
                }
            }
        }

        #endregion

        /// <summary>
        ///     Get the route id for an order
        /// </summary>
        /// <param name="order">The order to get the route for</param>
        /// <returns>
        ///     Route, or null when none is found / could not be determined
        /// </returns>
        private static int? GetRouteIdForOrder(OrderEntity order)
        {
            int? routeId = null;
            StringBuilder routeDiagnosis = new StringBuilder();

            // Try to use order notes route if order contains a special message
            if (order.Notes.Length > 0)
            {
                if (order.DeliverypointEntity.DeliverypointgroupEntity.OrderNotesRouteId.HasValue)
                {
                    routeId = order.DeliverypointEntity.DeliverypointgroupEntity.OrderNotesRouteId.Value;
                    routeDiagnosis.AppendFormatLine("Route using OrderNotesRoute on Deliverypointgroup: Route found from Deliverypointgroup: {0} (DeliverypointgroupId: '{1}'), RouteId: '{2}'", order.DeliverypointEntity.DeliverypointgroupEntity.Name, order.DeliverypointEntity.DeliverypointgroupId, routeId);
                }
                else
                {
                    routeDiagnosis.AppendFormatLine("Route using OrderNotesRoute Deliverypointgroup: No route found via Deliverypointgroup (Deliverypointgroup: '{0}', Id: '{1}')", order.DeliverypointEntity.DeliverypointgroupEntity.Name, order.DeliverypointEntity.DeliverypointgroupId);
                }
            }

            // Try to use the system message route
            if (order.TypeAsEnum == OrderType.RequestForWakeUp)
            {
                if (order.DeliverypointEntity.DeliverypointgroupEntity.SystemMessageRouteId.HasValue)
                {
                    routeId = order.DeliverypointEntity.DeliverypointgroupEntity.SystemMessageRouteId.Value;
                    routeDiagnosis.AppendFormatLine("Route using SystemMessageRoute on Deliverypointgroup: One route found from Deliverypointgroup: {0} (DeliverypointgroupId: '{1}'), RouteId: '{2}'", order.DeliverypointEntity.DeliverypointgroupEntity.Name, order.DeliverypointEntity.DeliverypointgroupId, routeId);
                }
                else
                {
                    routeDiagnosis.AppendFormatLine("Route using SystemMessageRoute Deliverypointgroup: No route found via Deliverypointgroup (Deliverypointgroup: '{0}', Id: '{1}')", order.DeliverypointEntity.DeliverypointgroupEntity.Name, order.DeliverypointEntity.DeliverypointgroupId);
                }
            }

            // Try to use the dpg route
            if (!routeId.HasValue)
            {
                if (order.DeliverypointEntity.DeliverypointgroupEntity.RouteId.HasValue)
                {
                    routeId = order.DeliverypointEntity.DeliverypointgroupEntity.RouteId.Value;
                    routeDiagnosis.AppendFormatLine("Route using Deliverypointgroup: One route found from Deliverypointgroup: {0} (DeliverypointgroupId: '{1}'), RouteId: '{2}'", order.DeliverypointEntity.DeliverypointgroupEntity.Name, order.DeliverypointEntity.DeliverypointgroupId, routeId);
                }
                else
                {
                    routeDiagnosis.AppendFormatLine("Route using Deliverypointgroup: No route found via Deliverypointgroup (Deliverypointgroup: '{0}', Id: '{1}')", order.DeliverypointEntity.DeliverypointgroupEntity.Name, order.DeliverypointEntity.DeliverypointgroupId);
                }
            }

            // Try to get the RouteId from the Company (could be null, which would keep routeId null)
            if (!routeId.HasValue)
            {
                if (order.CompanyEntity.RouteId.HasValue)
                {
                    routeId = order.CompanyEntity.RouteId.Value;
                    routeDiagnosis.AppendFormatLine("Route using Company: One route found from Company (Company: '{0}', Id: '{1}'), RouteId: '{2}'", order.CompanyEntity.Name, order.CompanyEntity.CompanyId, routeId);
                }
                else
                {
                    routeDiagnosis.AppendFormatLine("Route using Company: No route found via Company (Company: '{0}', Id: '{1}')", order.CompanyEntity.Name, order.CompanyEntity.CompanyId);
                }
            }

            // Save a report of how we could not determine the route
            if (!routeId.HasValue)
            {
                order.RoutingLog += routeDiagnosis.ToString();
            }

            return routeId;
        }

        /// <summary>
        ///     Get the route id for an order item
        /// </summary>
        /// <param name="orderitem">The order item to get the route for</param>
        /// <returns>
        ///     Route, or null when none is found / could not be determined
        /// </returns>
        private static int? GetRouteIdForOrderitem(OrderitemEntity orderitem)
        {
            int? routeId = null;
            StringBuilder routeDiagnosis = new StringBuilder();

            if (orderitem.OrderEntity.TypeAsEnum == OrderType.Document)
            {
                // A print request for a document from the IRT, always use the EmailDocumentRoute that's set on the deliverypointgroup
                if (orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.EmailDocumentRouteId.HasValue)
                {
                    routeId = orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.EmailDocumentRouteId.Value;
                    routeDiagnosis.AppendFormatLine("Route using EmailDocumentRoute on Deliverypointgroup: One route found from Deliverypointgroup: {0} (DeliverypointgroupId: '{1}'), RouteId: '{2}'", orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.Name, orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupId, routeId);
                }
                else
                {
                    routeDiagnosis.AppendFormatLine("Route using EmailDocumentRoute Deliverypointgroup: No route found via Deliverypointgroup (Deliverypointgroup: '{0}', Id: '{1}')", orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.Name, orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupId);
                }
            }
            else
            {
                // Check whether master order or the orderitem has notes attached to it
                if (orderitem.OrderEntity.Notes.Length > 0 || orderitem.Notes.Length > 0)
                {
                    if (orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.OrderNotesRouteId.HasValue)
                    {
                        routeId = orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.OrderNotesRouteId.Value;
                        routeDiagnosis.AppendFormatLine("Route using OrderNotesRoute on Deliverypointgroup: Route found from Deliverypointgroup: {0} (DeliverypointgroupId: '{1}'), RouteId: '{2}'", orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.Name, orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupId, routeId);
                    }
                    else
                    {
                        routeDiagnosis.AppendFormatLine("Route using OrderNotesRoute Deliverypointgroup: No route found via Deliverypointgroup (Deliverypointgroup: '{0}', Id: '{1}')", orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.Name, orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupId);
                    }
                }

                // Check whether the orderitem has a product id
                if (!routeId.HasValue)
                {
                    if (!orderitem.ProductId.HasValue)
                    {
                        routeDiagnosis.AppendFormatLine("Route using Orderitem: No is no product connect to Orderitem (Id: '{0}')", orderitem.OrderitemId);
                    }
                    else
                    {
                        if (orderitem.ProductEntity.RouteId.HasValue)
                        {
                            routeId = orderitem.ProductEntity.RouteId;
                            routeDiagnosis.AppendFormatLine("Route using Product: One route found from Product (Product: '{0}', Id: '{1}'), RouteId: '{2}'", orderitem.ProductEntity.Name, orderitem.ProductId.Value, routeId);
                        }
                        else
                        {
                            routeDiagnosis.AppendFormatLine("Route using Product: No route found via Product (Product: '{0}', Id: '{1}')", orderitem.ProductEntity.Name, orderitem.ProductId.Value);
                        }
                    }
                }

                // Check whether a route id was found on the product
                if (!routeId.HasValue)
                {
                    if (orderitem.CategoryId.HasValue)
                    {
                        int categoryRouteId = orderitem.CategoryEntity.GetRouteId();
                        if (categoryRouteId > 0)
                        {
                            routeId = categoryRouteId;
                            routeDiagnosis.AppendFormatLine("Route using Category: One route found from Category (Category: '{0}', Id: '{1}'), RouteId: '{2}'", orderitem.CategoryEntity.Name, orderitem.CategoryId.Value, routeId);
                        }
                        else
                        {
                            routeDiagnosis.AppendFormatLine("Route using Category: No route found via Category (Category: '{0}', Id: '{1}')", orderitem.CategoryEntity.Name, orderitem.CategoryId.Value);
                        }
                    }
                    else
                    {
                        routeDiagnosis.AppendFormatLine("Route using Category: No category connected to order item (Product: '{0}', Id: '{1}')", orderitem.ProductName, orderitem.OrderitemId);
                    }
                }

                // Check whether a route id was found on the category
                if (!routeId.HasValue)
                {
                    if (orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.RouteId.HasValue)
                    {
                        routeId = orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.RouteId.Value;
                        routeDiagnosis.AppendFormatLine("Route using Deliverypointgroup: One route found from Deliverypointgroup: {0} (DeliverypointgroupId: '{1}'), RouteId: '{2}'", orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.Name, orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupId, routeId);
                    }
                    else
                    {
                        routeDiagnosis.AppendFormatLine("Route using Deliverypointgroup: No route found via Deliverypointgroup (Deliverypointgroup: '{0}', Id: '{1}')", orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupEntity.Name, orderitem.OrderEntity.DeliverypointEntity.DeliverypointgroupId);
                    }
                }

                // Try to get the RouteId from the Company (could be null, which would keep routeId null)
                if (!routeId.HasValue)
                {
                    if (orderitem.OrderEntity.CompanyEntity.RouteId.HasValue)
                    {
                        routeId = orderitem.OrderEntity.CompanyEntity.RouteId.Value;
                        routeDiagnosis.AppendFormatLine("Route using Company: One route found from Company (Company: '{0}', Id: '{1}'), RouteId: '{2}'", orderitem.OrderEntity.CompanyEntity.Name, orderitem.OrderEntity.CompanyEntity.CompanyId, routeId);
                    }
                    else
                    {
                        routeDiagnosis.AppendFormatLine("Route using Company: No route found via Company (Company: '{0}', Id: '{1}')", orderitem.OrderEntity.CompanyEntity.Name, orderitem.OrderEntity.CompanyEntity.CompanyId);
                    }
                }
            }

            // Save a report of how we could not determine the route (GK Always - we probably have enough bits to store this information)
            // if (!routeId.HasValue)
            orderitem.OrderEntity.RoutingLog += routeDiagnosis.ToString();

            return routeId;
        }

        /// <summary>
        ///     Get a Route with all relevant data prefetched:
        ///     Routesteps, RoutestepsHandlers and the Terminal.
        /// </summary>
        /// <param name="routeId">The route id.</param>
        /// <param name="transaction">The transaction.</param>
        /// <returns></returns>
        public static RouteEntity GetRoutePrefetched(int routeId, ITransaction transaction)
        {
            PrefetchPath path = new PrefetchPath(EntityType.RouteEntity);
            path.Add(RouteEntityBase.PrefetchPathRoutestepCollection).SubPath.Add(RoutestepEntityBase.PrefetchPathRoutestephandlerCollection).SubPath.Add(RoutestephandlerEntityBase.PrefetchPathTerminalEntity);

            RouteEntity route = new RouteEntity();
            route.AddToTransaction(transaction);
            route.FetchUsingPK(routeId, path);

            return route;
        }

        /// <summary>
        ///     ! This method is only intended to be used by the ProductCategoryValidator !
        ///     Get all routes for the Product related to the ProductCategoryEntity and does additional
        ///     logic that's suitable to the ProductCategoryValidator. Like that the ProductCategoryEntity
        ///     doesn't have to be saved yet and that it will not retrieve this ProductCategoryEntity from
        ///     the database but will use the in memory instance (which might have changed vs. the database version)
        /// </summary>
        /// <param name="productCategoryEntity">The product category entity.</param>
        /// <returns>
        ///     A list of Categories with the Route the direct the Product to
        /// </returns>
        public static Dictionary<CategoryEntity, int> GetRouteIds(ProductCategoryEntity productCategoryEntity)
        {
            return RoutingHelper.GetRouteIds(null, productCategoryEntity, null);
        }

        /// <summary>
        ///     Get's all relevant RouteId's for a Product or ProductCategory.
        ///     Also includes the Category of which the Route is derived.
        /// </summary>
        /// <param name="productIds">ProductIds to find the Routes for</param>
        /// <param name="productCategoryEntity">ProductCategory of which Product to find the Routes for</param>
        /// <param name="transaction">Seperate transaction to make all queries part of</param>
        /// <returns>
        ///     An Dictionary with 0 or more routes. More than 1 route is wrong as it makes the route choosing ambigious.
        /// </returns>
        private static Dictionary<CategoryEntity, int> GetRouteIds(List<int> productIds, ProductCategoryEntity productCategoryEntity, ITransaction transaction)
        {
            Dictionary<CategoryEntity, int> routes = new Dictionary<CategoryEntity, int>();

            // Check the we're not part of more than 1 transaction.
            int transactionCount = 0;

            if (transaction != null)
            {
                transactionCount++;
            }

            if (productCategoryEntity != null && productCategoryEntity.GetCurrentTransaction() != null)
            {
                transaction = productCategoryEntity.GetCurrentTransaction();
                transactionCount++;
            }

            if (transactionCount > 1)
            {
                throw new ObymobiException(RoutingHelperResult.MultipleActiveTransactions);
            }

            // Get the all ProductCategory entities for this product
            PredicateExpression pasFilter = new PredicateExpression();
            if (productIds != null)
            {
                pasFilter.Add(ProductCategoryFields.ProductId == productIds);
            }
            else if (productCategoryEntity != null)
            {
                pasFilter.Add(ProductCategoryFields.ProductId == productCategoryEntity.ProductId);
            }

            // Don't included this ProductCategoryEntity itself, it's added from memory
            if (productCategoryEntity != null && !productCategoryEntity.IsNew)
            {
                pasFilter.Add(ProductCategoryFields.ProductCategoryId != productCategoryEntity.ProductCategoryId);
            }

            // Retrieve the ProductCategories - But only if we have a valid filter (otherwise we return everything)
            List<int> categoryIds = new List<int>();
            if (pasFilter.Count > 0)
            {
                ProductCategoryCollection pas = new ProductCategoryCollection();
                pas.AddToTransaction(transaction);
                pas.GetMulti(pasFilter);

                // Get the Categories of which this product is part
                // Get CategoryIds from the retrieved ProductCategory entities:
                categoryIds = pas.Select(x => x.CategoryId).ToList();
            }
            // Add CategoryId from memory from the useableentity
            if (productCategoryEntity != null)
            {
                categoryIds.Add(productCategoryEntity.CategoryId);
            }

            // Only fetch routes based on category when we actually have any categories.
            if (categoryIds.Count > 0)
            {
                // Fetch categories with Path for parent categories.
                PredicateExpression categoryFilter = new PredicateExpression();
                categoryFilter.Add(CategoryFields.CategoryId == categoryIds);
                CategoryCollection categories = new CategoryCollection();
                categories.AddToTransaction(transaction);

                PrefetchPath pathCategories = new PrefetchPath(EntityType.CategoryEntity);

                // Go 5 levels deep for prefetching
                pathCategories.Add(CategoryEntityBase.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntityBase.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntityBase.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntityBase.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntityBase.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntityBase.PrefetchPathParentCategoryEntity);

                categories.GetMulti(categoryFilter, pathCategories);

                // Get the route for each Category of which the Product is a child                
                foreach (CategoryEntity category in categories)
                {
                    CategoryEntity routeFromCategory;
                    int? routeId = RoutingHelper.GetRouteId(category, out routeFromCategory);
                    if (routeId.HasValue && !routes.ContainsKey(routeFromCategory))
                    {
                        routes.Add(routeFromCategory, routeId.Value);
                    }
                }
            }

            return routes;
        }

        /// <summary>
        ///     Get the RouteId for a Category, recursevily.
        ///     If the Category itself has no RouteId, it's tried to be retrieved from it's parent, and so on.
        /// </summary>
        /// <param name="category">Category to get the RouteId for</param>
        /// <param name="routeIdFromCategory">
        ///     Outputs the actual category that contained the RouteId (could be the supplied
        ///     category, but also one of it's parents)
        /// </param>
        /// <returns>
        ///     The RouteId or null when no route was configured.
        /// </returns>
        private static int? GetRouteId(CategoryEntity category, out CategoryEntity routeIdFromCategory)
        {
            if (category.RouteId.HasValue)
            {
                routeIdFromCategory = category;
                return category.RouteId.Value;
            }
            if (category.ParentCategoryId.HasValue)
            {
                return RoutingHelper.GetRouteId(category.ParentCategoryEntity, out routeIdFromCategory);
            }
            routeIdFromCategory = null;
            return null;
        }

        /// <summary>
        ///     Gets the routestephandler statuses for a terminal for the requested Orderguids.
        ///     Also times-out
        /// </summary>
        /// <param name="orderRoutestephandlerGuids">The order routestephandler guids.</param>
        /// <returns></returns>
        public static List<OrderRoutestephandlerSaveStatus> GetRoutestephandlerStatuses(List<string> orderRoutestephandlerGuids)
        {
            // Create filter for order Guids
            PredicateExpression orderFilter = new PredicateExpression();
            orderFilter.Add(OrderRoutestephandlerFields.Guid == orderRoutestephandlerGuids);

            // Filter to get only Routestephandlers that belong to this terminal
            PredicateExpression handlerFilter = new PredicateExpression();
            handlerFilter.Add(OrderRoutestephandlerFields.Guid == orderRoutestephandlerGuids);

            // Create prefetch for Handlers
            PrefetchPath path = new PrefetchPath(EntityType.OrderEntity);
            IPrefetchPathElement pathOrderRoutestephandler = OrderEntityBase.PrefetchPathOrderRoutestephandlerCollection;
            pathOrderRoutestephandler.ExcludedIncludedFields = RoutingHelper.ExcludeFieldValueFields;
            pathOrderRoutestephandler.Filter = handlerFilter;
            path.Add(pathOrderRoutestephandler);

            RelationCollection joins = new RelationCollection();
            joins.Add(OrderEntityBase.Relations.OrderRoutestephandlerEntityUsingOrderId);

            // Include list of fields for the order
            IncludeFieldsList includeOrderFields = new IncludeFieldsList();
            includeOrderFields.Add(OrderFields.OrderId);
            includeOrderFields.Add(OrderFields.Status);
            includeOrderFields.Add(OrderFields.Guid);

            OrderCollection orders = new OrderCollection();
            orders.GetMulti(orderFilter, 0, null, joins, path, includeOrderFields, 0, 0);

            List<OrderRoutestephandlerSaveStatus> list = new List<OrderRoutestephandlerSaveStatus>();
            foreach (OrderEntity order in orders)
            {
                foreach (OrderRoutestephandlerEntity handler in order.OrderRoutestephandlerCollection)
                {
                    OrderRoutestephandlerSaveStatus status = OrderRoutestephandlerHelper.CreateRoutestephandlerSaveStatusModelFromEntity(handler, order);
                    list.Add(status);
                }
            }

            return list;
        }

        /// <summary>
        ///     Gets the non retrieved orders for OSS and Console (RoutestephandlerStatus.WaitingToBeRetrieved)
        ///     And retrieved and beingHandled orders to for console (Still need to be displayed till completed)
        /// </summary>
        /// <param name="terminalId">The terminal id.</param>
        /// <param name="isConsole">Orders retrieved for console usage?</param>
        /// <returns></returns>
        public static Order[] GetNonRetrievedOrdersForTerminal(int terminalId, bool isConsole, bool updateOrdersAsRetrievedByConsole)
        {
            List<OrderRoutestephandlerStatus> status = new List<OrderRoutestephandlerStatus>();
            status.Add(OrderRoutestephandlerStatus.WaitingToBeRetrieved);
            if (isConsole)
            {
                status.Add(OrderRoutestephandlerStatus.RetrievedByHandler);
                status.Add(OrderRoutestephandlerStatus.BeingHandled);
            }
            return RoutingHelper.GetActiveOrdersForTerminal(terminalId, status, null, null, null, false, null, updateOrdersAsRetrievedByConsole);
        }

        /// <summary>
        ///     Gets unprocessed orders for OSS and Console (OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure,
        ///     OrderRoutestephandlerStatus.Failed)
        /// </summary>
        /// <param name="terminalId">The terminal id.</param>
        /// <returns></returns>
        public static Order[] GetUnprocessedOrdersForTerminal(int terminalId, bool updateOrdersAsRetrievedByConsole)
        {
            List<OrderRoutestephandlerStatus> status = new List<OrderRoutestephandlerStatus> { OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure, OrderRoutestephandlerStatus.Failed };

            return RoutingHelper.GetActiveOrdersForTerminal(terminalId, status, null, null, null, false, null, updateOrdersAsRetrievedByConsole);
        }

        /// <summary>
        ///     Method used to retrieve orders that are still 'In-Route'.
        ///     This method will NOT return Orders that have completed their route (routes are deleted once completed).
        /// </summary>
        /// <param name="terminalId">The terminal id of the terminal to retrieve the orders for.</param>
        /// <param name="status">The status(es) of the OrderRoutesteps which to retrieve orders.</param>
        /// <param name="orderId">The order id specified to retrieve a specific order.</param>
        /// <param name="minutes">The minutes.</param>
        /// <param name="preOrderMinutes">The pre order minutes.</param>
        /// <param name="applyPosSpecificTranslation">if set to <c>true</c> [apply pos specific translation].</param>
        /// <param name="localTime">The local time.</param>
        /// <returns></returns>
        public static Order[] GetActiveOrdersForTerminal(int terminalId, List<OrderRoutestephandlerStatus> status, int? orderId, int? minutes, int? preOrderMinutes, bool applyPosSpecificTranslation, DateTime? localTime, bool updateOrdersAsRetrievedByConsole)
        {
            // Get OrderIds of relevant orders, if an OrderId is suppleid use that one
            // otherwise do a query to the Routestep table to get relevant orders
            List<int> orderIds = null;

            List<int> routestephandlerStatusIds = new List<int>();

            if (status == null)
            {
                status = new List<OrderRoutestephandlerStatus>();
            }
            else if (status.Count > 0)
            {
                // Convert statusses to int list for filter:
                routestephandlerStatusIds = status.Select(x => (int)x).ToList();
            }

            if (orderId.HasValue)
            {
                // Specific query for one order,
                orderIds = new List<int>();
                orderIds.Add(orderId.Value);
            }
            else
            {
                if (routestephandlerStatusIds.Count > 0)
                {
                    // Convert statusses to int list for filter:
                    List<int> statusesAsInts = status.Select(x => (int)x).ToList();

                    PredicateExpression routestepFilter = new PredicateExpression();
                    routestepFilter.Add(OrderRoutestephandlerFields.Status == statusesAsInts);
                    routestepFilter.Add(OrderRoutestephandlerFields.TerminalId == terminalId);

                    PrefetchPath pathOrderRoutestephandler = new PrefetchPath(EntityType.OrderRoutestephandlerEntity);
                    // Prefetch Order because it will be used when the Routestep is saved later 
                    pathOrderRoutestephandler.Add(OrderRoutestephandlerEntityBase.PrefetchPathOrderEntity);

                    OrderRoutestephandlerCollection routesteps = new OrderRoutestephandlerCollection();
                    routesteps.GetMulti(routestepFilter, pathOrderRoutestephandler);

                    if (routesteps.Count > 0)
                    {
                        orderIds = routesteps.Select(x => x.OrderId).ToList();
                    }

                    TerminalEntity terminal = new TerminalEntity(terminalId);

                    // If the orderroutestephandlers are being retrieved for the console and NOT for the masterTab
                    // change the status to RetrievedByHandler because the console has them now
                    if (!terminal.IsNew && terminal.TerminalType == (int)TerminalType.Console && updateOrdersAsRetrievedByConsole)
                    {
                        Transaction transaction = new Transaction(IsolationLevel.ReadUncommitted, "GetActiveOrdersForTerminal.updateStep");

                        try
                        {
                            foreach (OrderRoutestephandlerEntity routestep in routesteps)
                            {
                                if (routestep.StatusAsEnum == OrderRoutestephandlerStatus.WaitingToBeRetrieved)
                                {
                                    routestep.StatusAsEnum = OrderRoutestephandlerStatus.RetrievedByHandler;
                                    routestep.AddToTransaction(transaction);
                                    routestep.Save();
                                }
                            }

                            transaction.Commit();
                        }
                        catch
                        {
                            transaction.Rollback();
                        }
                        finally
                        {
                            transaction.Dispose();
                        }
                    }
                }
            }

            // If we have no OrderIds, there won't be results, so just end this method.
            if (orderIds == null)
            {
                return new Order[0];
            }

            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();

            // Filter specific OrderId
            filter.Add(OrderFields.OrderId == orderIds);

            // Get the current time vs. local time to be used in filters
            DateTime currentTimeUtc = DateTime.UtcNow;
            if (localTime.HasValue)
            {
                currentTimeUtc = localTime.Value;
            }

            // Create a filter to get orders that have been updated in the last X minutes.
            if (minutes.HasValue)
            {
                if (minutes > 0)
                {
                    minutes = minutes * -1;
                }

                filter.Add(OrderFields.UpdatedUTC >= currentTimeUtc.AddMinutes((double)minutes));
            }

            // Pre-ordering
            // GK-ROUTING > Who did this logic? - No comments, not self explanatory
            PredicateExpression filterPlaceTime = new PredicateExpression();
            if (preOrderMinutes.HasValue)
            {
                // Show future orders up untill maximum the amount further in the future than specified.
                // GK - This is probably used to 'peek' forward.
                filterPlaceTime.AddWithAnd(OrderFields.PlaceTimeUTC > currentTimeUtc);
                filterPlaceTime.AddWithAnd(OrderFields.PlaceTimeUTC <= currentTimeUtc.AddMinutes((double)preOrderMinutes));
            }
            else
            {
                // Orders without Placetime should always be retrieved
                PredicateExpression filterNoPlaceTime = new PredicateExpression();
                filterNoPlaceTime.AddWithOr(OrderFields.PlaceTimeUTC == DBNull.Value);
                filterNoPlaceTime.AddWithOr(OrderFields.PlaceTimeUTC <= currentTimeUtc);

                // Orders that are not yet due
                PredicateExpression filterPreStart = new PredicateExpression();
                // WAS but it's already filtered on being 'active' by the RouteHandlers at the beginning - filterPreStart.AddWithAnd(OrderFields.Status != OrderStatus.QueuedOnWebservice);
                filterPreStart.AddWithAnd(OrderFields.PlaceTimeUTC > currentTimeUtc);

                filterPlaceTime.AddWithOr(filterNoPlaceTime);
                filterPlaceTime.AddWithOr(filterPreStart);
            }
            filter.Add(filterPlaceTime);

            OrderCollection orders = new OrderCollection();
            RelationCollection joins = new RelationCollection();
            PrefetchPath path = new PrefetchPath(EntityType.OrderEntity);

            // Path for Alterationitems
            IPrefetchPathElement pathAlterationItems = OrderitemAlterationitemEntityBase.PrefetchPathAlterationitemEntity;
            pathAlterationItems.SubPath.Add(AlterationitemEntityBase.PrefetchPathAlterationEntity);
            pathAlterationItems.SubPath.Add(AlterationitemEntityBase.PrefetchPathAlterationoptionEntity);

            // Path for OrderitemAlterationitems
            IPrefetchPathElement pathOrderitemAlterationItems = OrderitemEntityBase.PrefetchPathOrderitemAlterationitemCollection;
            pathOrderitemAlterationItems.SubPath.Add(pathAlterationItems);

            // Path for Orderitems
            IPrefetchPathElement pathOrderitem = OrderEntityBase.PrefetchPathOrderitemCollection;
            pathOrderitem.SubPath.Add(OrderitemEntityBase.PrefetchPathProductEntity);
            pathOrderitem.SubPath.Add(pathOrderitemAlterationItems);

            // Path for active RouteStepHandlers for the requesting terminal
            IPrefetchPathElement pathOrderRoutestephandlers = OrderEntityBase.PrefetchPathOrderRoutestephandlerCollection;
            PredicateExpression filterOrderRoutestephandlers = new PredicateExpression();
            filterOrderRoutestephandlers.Add(OrderRoutestephandlerFields.TerminalId == terminalId);
            filterOrderRoutestephandlers.Add(OrderRoutestephandlerFields.Status == routestephandlerStatusIds);
            pathOrderRoutestephandlers.Filter = filterOrderRoutestephandlers;

            IPrefetchPathElement pathPaymentTransactions = OrderEntityBase.PrefetchPathPaymentTransactionCollection;

            // Path for Customer
            path.Add(OrderEntityBase.PrefetchPathCustomerEntity);

            // Path for Company
            path.Add(OrderEntityBase.PrefetchPathCompanyEntity);

            // Path for Handlers
            path.Add(pathOrderitem);
            path.Add(pathOrderRoutestephandlers);
            path.Add(pathPaymentTransactions);

            orders.GetMulti(filter, 0, null, joins, path);

            List<Order> ordersToReturn = new List<Order>();

            foreach (OrderEntity order in orders)
            {
                // GK In case an order is active more than 1 time for a console in 1 step, only send 1 order.
                // This could happen if a Terminal is forwarded to another Terminal with which is shares a parallel step
                Order orderModel;
                if (order.OrderRoutestephandlerCollection.Count > 0)
                {
                    // Get the order routestep handlers that are not forwarded to this terminal
                    List<OrderRoutestephandlerEntity> forThisTerminal = order.OrderRoutestephandlerCollection.Where(rs => !rs.ForwardedFromTerminalId.HasValue).ToList();

                    OrderRoutestephandlerEntity orderRoutestephandlerEntity;

                    if (forThisTerminal.Count > 0)
                    {
                        // Get the first order routestep handler that is not forward to this terminal
                        orderRoutestephandlerEntity = forThisTerminal.First();
                    }
                    else
                    {
                        // We get here if two or more terminals have forwarded their orders to this terminal while this
                        // terminal wasn't originally part of that step. Just show the first, it doesn't matter which one we handle.
                        orderRoutestephandlerEntity = order.OrderRoutestephandlerCollection[0];
                    }

                    orderModel = OrderHelper.CreateOrderModelFromEntity(order, orderRoutestephandlerEntity, applyPosSpecificTranslation, false);

                    // Check whether the order has a POS routestephandler
                    PredicateExpression posRoutestephandlerFilter = new PredicateExpression();
                    posRoutestephandlerFilter.Add(OrderRoutestephandlerFields.OrderId == order.OrderId);
                    posRoutestephandlerFilter.Add(OrderRoutestephandlerFields.HandlerType == RoutestephandlerType.POS);
                    orderModel.HasPosRoutestephandler = new OrderRoutestephandlerCollection().GetDbCount(posRoutestephandlerFilter) > 0;

                    if (orderRoutestephandlerEntity.HandlerTypeAsEnum == RoutestephandlerType.Console && orderRoutestephandlerEntity.OriginatedFromRoutestepHandlerId.HasValue)
                    {
                        CustomTextCollection customTextCollection = new CustomTextCollection();
                        customTextCollection.GetMulti(CustomTextFields.RoutestephandlerId == orderRoutestephandlerEntity.OriginatedFromRoutestepHandlerId.Value);

                        // On the case
                        CustomTextEntity onTheCaseCustomTextEntity = customTextCollection.FirstOrDefault(x => x.Type == Enums.CustomTextType.RoutestephandlerOnTheCaseCaption && x.CultureCode.Equals(order.CompanyEntity.CultureCode, StringComparison.InvariantCultureIgnoreCase));
                        if (onTheCaseCustomTextEntity != null)
                            orderModel.OnTheCaseCaption = onTheCaseCustomTextEntity.Text;

                        // Complete
                        CustomTextEntity completeCustomTextEntity = customTextCollection.FirstOrDefault(x => x.Type == Enums.CustomTextType.RoutestephandlerCompleteCaption && x.CultureCode.Equals(order.CompanyEntity.CultureCode, StringComparison.InvariantCultureIgnoreCase));
                        if (completeCustomTextEntity != null)
                            orderModel.CompleteCaption = completeCustomTextEntity.Text;

                        // Manually process order
                        CustomTextEntity manuallyProcessOrderCustomTextEntity = customTextCollection.FirstOrDefault(x => x.Type == Enums.CustomTextType.RoutestephandlerManuallyProcessOrderCaption && x.CultureCode.Equals(order.CompanyEntity.CultureCode, StringComparison.InvariantCultureIgnoreCase));
                        if (manuallyProcessOrderCustomTextEntity != null)
                            orderModel.ManuallyProcessOrderCaption = manuallyProcessOrderCustomTextEntity.Text;
                    }
                }
                else
                {
                    orderModel = OrderHelper.CreateOrderModelFromEntity(order, null, applyPosSpecificTranslation, false);
                }

                ordersToReturn.Add(orderModel);
            }

            return ordersToReturn.ToArray();
        }

        #region Verification methods

        /// <summary>
        ///     Checks if all the terminals used in the routes of a company are online
        /// </summary>
        /// <param name="companyId">The company id</param>
        /// <returns>True if all online, false if one or more are offline</returns>
        public static bool TerminalsOnlineForCompany(int companyId)
        {
            bool toReturn = false;

            // We only want the terminals for this company
            PredicateExpression filterTerminals = new PredicateExpression();
            filterTerminals.Add(RouteFields.CompanyId == companyId);
            filterTerminals.Add(RoutestephandlerFields.TerminalId != DBNull.Value);

            // We only want terminals that are offline
            PredicateExpression filterSubFailedTerminals = new PredicateExpression();
            filterSubFailedTerminals.Add(DeviceFields.LastRequestUTC < DateTime.UtcNow.AddSeconds(-90) | TerminalFields.LastStatus != TerminalState.Online);
            filterTerminals.Add(filterSubFailedTerminals);

            // Set the relations
            RelationCollection relations = new RelationCollection();
            relations.Add(TerminalEntityBase.Relations.RoutestephandlerEntityUsingTerminalId);
            relations.Add(RoutestephandlerEntityBase.Relations.RoutestepEntityUsingRoutestepId);
            relations.Add(RoutestepEntityBase.Relations.RouteEntityUsingRouteId);
            relations.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);

            // Get the offline terminals for this company in the routes
            TerminalCollection terminals = new TerminalCollection();
            int failedTerminals = (int)terminals.GetScalar(TerminalFieldIndex.TerminalId, null, AggregateFunction.Count, filterTerminals, relations, null);

            // Fail because of terminal:
            if (failedTerminals == 0)
            {
                toReturn = true;
            }

            return toReturn;
        }

        /// <summary>
        ///     Checks if all terminals used in all routes of a company are consoles
        /// </summary>
        /// <param name="companyId">Company, which to check the routes off</param>
        /// <returns></returns>
        public static bool OnlyConsolesInRoute(int companyId)
        {
            bool toReturn = false;

            // We only want the terminals for this company
            PredicateExpression filterTerminals = new PredicateExpression();
            filterTerminals.Add(RouteFields.CompanyId == companyId);
            filterTerminals.Add(RoutestephandlerFields.TerminalId != DBNull.Value);

            // We only want terminals that aren't console
            PredicateExpression filterNoConsole = new PredicateExpression();
            filterNoConsole.Add(TerminalFields.HandlingMethod != TerminalType.Console);
            filterTerminals.Add(filterNoConsole);

            // Set the relations
            RelationCollection relations = new RelationCollection();
            relations.Add(TerminalEntityBase.Relations.RoutestephandlerEntityUsingTerminalId);
            relations.Add(RoutestephandlerEntityBase.Relations.RoutestepEntityUsingRoutestepId);
            relations.Add(RoutestepEntityBase.Relations.RouteEntityUsingRouteId);

            // Get the amount of terminals that aren't console for this company
            TerminalCollection terminals = new TerminalCollection();
            int otherTerminals = (int)terminals.GetScalar(TerminalFieldIndex.TerminalId, null, AggregateFunction.Count, filterTerminals, relations, null);

            if (otherTerminals == 0)
            {
                toReturn = true;
            }

            return toReturn;
        }

        /// <summary>
        ///     Verifies the first step is available for a route. (Routestep.Number == Routing.FirstStepNumber)
        /// </summary>
        /// <param name="routeId">The route id.</param>
        /// <param name="transaction">The transaction to do DB operations with.</param>
        /// <returns>
        ///     <c>true</c> if a first step is available (Routestep.Number == Routing.FirstStepNumber) otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFirstStepAvailable(int routeId, ITransaction transaction)
        {
            // Check if the current step is not the first step itself
            RoutestepCollection routesteps = new RoutestepCollection();
            routesteps.AddToTransaction(transaction);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(RoutestepFields.RouteId == routeId);
            filter.Add(RoutestepFields.Number == RoutingHelper.FIRST_STEP_NUMBER);

            // If there's no step one, throw exception
            return (routesteps.GetDbCount(filter) > 0);
        }

        /// <summary>
        ///     Determines whether is RoutestephandlerType is compatible with Terminal
        /// </summary>
        /// <param name="handlerType">Type of the handler.</param>
        /// <param name="terminalType">The terminal type (allowed to be null, if none is configured).</param>
        /// <returns>obvious</returns>
        public static bool IsRoutestepHandlerTypeCompatibleWithTerminalType(RoutestephandlerType handlerType, TerminalType? terminalType)
        {
            List<RoutestephandlerType> nonTerminalRoutestephandlerTypes = new List<RoutestephandlerType>();
            nonTerminalRoutestephandlerTypes.Add(RoutestephandlerType.EmailOrder);
            nonTerminalRoutestephandlerTypes.Add(RoutestephandlerType.SMS);
            nonTerminalRoutestephandlerTypes.Add(RoutestephandlerType.HotSOS);
            nonTerminalRoutestephandlerTypes.Add(RoutestephandlerType.Quore);
            nonTerminalRoutestephandlerTypes.Add(RoutestephandlerType.Hyatt_HotSOS);
            nonTerminalRoutestephandlerTypes.Add(RoutestephandlerType.Alice);
            nonTerminalRoutestephandlerTypes.Add(RoutestephandlerType.ExternalSystem);

            if (nonTerminalRoutestephandlerTypes.Contains(handlerType))
            {
                return true; // Terminal is irrelevant.
            }

            bool compatible;
            switch (handlerType)
            {
                case RoutestephandlerType.Console:
                    compatible = terminalType == TerminalType.Console;
                    break;
                case RoutestephandlerType.Printer:
                    compatible = terminalType == TerminalType.Console || terminalType == TerminalType.OnSiteServer;
                    break;
                case RoutestephandlerType.POS:
                    compatible = terminalType == TerminalType.OnSiteServer;
                    break;
                case RoutestephandlerType.EmailDocument:
                    compatible = terminalType == TerminalType.Console;
                    break;
                default:
                    throw new NotImplementedException("RoutingHelper.IsRoutestepHandlerTypeCompatibleWithTerminal not yet implemented for HandlerType: " + handlerType);
            }

            return compatible;
        }

        /// <summary>
        ///     Determines whether is RoutestephandlerType is compatible with Terminal
        /// </summary>
        /// <param name="handlerType">Type of the handler.</param>
        /// <param name="terminal">The terminal.</param>
        /// <returns>obvious</returns>
        public static bool IsRoutestepHandlerTypeCompatibleWithTerminalType(RoutestephandlerType handlerType, TerminalEntity terminal)
        {
            TerminalType? type = null;
            if (terminal != null)
            {
                type = terminal.TerminalTypeEnum;
            }

            return RoutingHelper.IsRoutestepHandlerTypeCompatibleWithTerminalType(handlerType, type);
        }

        /// <summary>
        ///     Indicates of the HandlerType requires user interaction (i.e. Console) or goes automatically (i.e. POS, Printer,
        ///     etc)
        /// </summary>
        /// <param name="orderRoutestephandler">The Order Handler Type</param>
        /// <returns>True if we have to wait for a user interaction, otherwise false</returns>
        public static bool RequiresUserAction(this OrderRoutestephandlerEntity orderRoutestephandler)
        {
            return RoutingHelper.RequiresUserAction(orderRoutestephandler.HandlerTypeAsEnum);
        }

        /// <summary>
        ///     Indicates of the HandlerType requires user interaction (i.e. Console) or goes automatically (i.e. POS, Printer,
        ///     etc)
        /// </summary>
        /// <param name="handlerType">The Handler Type</param>
        /// <returns>True if we have to wait for a user interaction, otherwise false</returns>
        public static bool RequiresUserAction(RoutestephandlerType handlerType)
        {
            if (handlerType == RoutestephandlerType.Console)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Determines whether the supplied customer has any expired orders
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>
        ///     <c>true</c> if the customer has expired orders; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasCustomerExpiredOrders(int customerId)
        {
            int orderCount;
            return RoutingHelper.HasExpiredOrders(null, customerId, out orderCount);
        }

        /// <summary>
        ///     Determines whether the supplied customer has any expired orders
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <param name="orderCount">The order count.</param>
        /// <returns>
        ///     <c>true</c> if the customer has expired orders; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasCustomerExpiredOrders(int customerId, out int orderCount)
        {
            return RoutingHelper.HasExpiredOrders(null, customerId, out orderCount);
        }

        /// <summary>
        ///     Determines whether the supplied client has any expired orders
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <returns>
        ///     <c>true</c> if the client has expired orders; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasClientExpiredOrders(int clientId)
        {
            int orderCount;
            return RoutingHelper.HasExpiredOrders(clientId, null, out orderCount);
        }

        /// <summary>
        ///     Determines whether the supplied client has any expired orders
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="orderCount">The order count.</param>
        /// <returns>
        ///     <c>true</c> if the client has expired orders; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasClientExpiredOrders(int clientId, out int orderCount)
        {
            return RoutingHelper.HasExpiredOrders(clientId, null, out orderCount);
        }

        /// <summary>
        ///     PRIVATE method for all Has[someting]ExpiredOrders methods.
        /// </summary>
        /// <param name="clientId">The client id.</param>
        /// <param name="customerId">The customer id.</param>
        /// <param name="orderCount">The order count.</param>
        /// <returns>
        ///     <c>true</c> if there are expired orders; otherwise, <c>false</c>.
        /// </returns>
        private static bool HasExpiredOrders(int? clientId, int? customerId, out int orderCount)
        {
            PredicateExpression orderFilter;
            RelationCollection joins;

            // Get the basics for joining and filtering to get expired orders
            RoutingHelper.GetExpiredOrdersRetrievalSet(out orderFilter, out joins);

            // Additionally filter on ClientId
            if (clientId.HasValue)
            {
                orderFilter.Add(OrderFields.ClientId == clientId);
            }
            else if (customerId.HasValue)
            {
                orderFilter.Add(OrderFields.CustomerId == customerId);
            }

            // Retrieve
            OrderCollection orders = new OrderCollection();
            orderCount = orders.GetDbCount(orderFilter, joins);
            return (orderCount > 0);
        }

        /// <summary>
        ///     Determines whether the Order processing has expired
        /// </summary>
        /// <param name="order">The order, prefeteched with OrderRoutstephandler-Terminal.</param>
        /// <param name="inMemory">If set to <c>true</c> it's check in memory/with lazy loading (good is it's already prefetched).</param>
        /// <returns>
        ///     <c>true</c> if the order has not been processed for 90s otherwise, unless it's in a manual step (i.e. on console),
        ///     <c>false</c>.
        /// </returns>
        public static bool IsOrderExpired(OrderEntity order, bool inMemory)
        {
            return RoutingHelper.IsOrderExpired(order, null, inMemory);
        }

        /// <summary>
        ///     Determines whether the Order processing has expired
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <returns>
        ///     <c>true</c> if the order has not been processed for 90s otherwise, unless it's in a manual step (i.e. on console),
        ///     <c>false</c>.
        /// </returns>
        public static bool IsOrderExpired(int orderId)
        {
            return RoutingHelper.IsOrderExpired(null, orderId, false);
        }

        /// <summary>
        ///     Determines whether the Order processing has expired
        /// </summary>
        /// <param name="order">The order, prefeteched with OrderRoutstephandler-Terminal.</param>
        /// <param name="orderId">The order id.</param>
        /// <param name="inMemory">If set to <c>true</c> it's check in memory/with lazy loading (good is it's already prefetched).</param>
        /// <returns>
        ///     <c>true</c> if the order has not been processed for 90s otherwise, unless it's in a manual step (i.e. on console),
        ///     <c>false</c>.
        /// </returns>
        private static bool IsOrderExpired(OrderEntity order, int? orderId, bool inMemory)
        {
            if (order == null && !orderId.HasValue)
            {
                throw new ObymobiException(RoutingHelperResult.ParametersMissing, "RoutingHelper.IsOrderExpired");
            }
            if (order == null && inMemory)
            {
                throw new ObymobiException(RoutingHelperResult.ParametersMissing, "RoutingHelper.IsOrderExpired");
            }

            bool isExpired;
            if (!inMemory)
            {
                PredicateExpression orderFilter;
                RelationCollection joins;

                // Get the basics for joining and filtering to get expired orders
                RoutingHelper.GetExpiredOrdersRetrievalSet(out orderFilter, out joins);

                if (order != null)
                {
                    orderFilter.Add(OrderFields.OrderId == order.OrderId);
                }
                else
                {
                    orderFilter.Add(OrderFields.OrderId == orderId.Value);
                }

                OrderCollection orders = new OrderCollection();
                isExpired = orders.GetDbCount(orderFilter, joins) > 0;
            }
            else
            {
                isExpired = RoutingHelper.IsOrderExpiredInMemory(order);
            }

            return isExpired;
        }

        /// <summary>
        ///     Determines whether an order is expired, it does this using the OrderEntity in memory / lazy loading
        /// </summary>
        /// <param name="order">The order.</param>
        /// <returns>
        ///     <c>true</c> if the order is expired; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsOrderExpiredInMemory(OrderEntity order)
        {
            TimeSpan processingTime = TimeSpan.MinValue;
            bool? processingExpired = null;
            if (order.StatusAsEnum == OrderStatus.Routed)
            {
                // It's routed but not yet being handled
                if (order.CreatedUTC.HasValue)
                {
                    // Check Created date for processing time
                    processingTime = DateTime.UtcNow - order.CreatedUTC.Value;
                }
            }
            else if (order.StatusAsEnum == OrderStatus.InRoute)
            {
                // It's being handled, check time since last step has been started
                // Check if the current step(s) are handled automatically, those should be handled in 90s
                List<RoutestephandlerType> automaticHandlingTypes = RoutingHelper.GetAutomaticlyHandledRoutestephandlers();
                List<OrderRoutestephandlerStatus> pendingStatus = RoutingHelper.GetPendingStatuses();

                List<OrderRoutestephandlerEntity> currentSteps = order.OrderRoutestephandlerCollection.Where(step => pendingStatus.Contains(step.StatusAsEnum)).ToList();
                List<OrderRoutestephandlerEntity> currentAutomaticSteps = currentSteps.Where(step => automaticHandlingTypes.Contains(step.HandlerTypeAsEnum)).ToList();

                if (currentSteps.Count == 0)
                {
                    // Strange, in-route but no acti
                    throw new ObymobiException(RoutingHelperResult.InconsistentData, "OrderId: {0}, is 'In-Route' but has no pending steps", order.OrderId);
                }
                if (!currentSteps.Any(step => step.UpdatedUTC.HasValue))
                {
                    throw new ObymobiException(RoutingHelperResult.InconsistentData, "OrderId: {0}, is 'In-Route' but has no values for Updated in it's steps", order.OrderId);
                }
                if (currentAutomaticSteps.Count > 0)
                {
                    // Check if these steps are not older than 90s
                    DateTime lastUpdated = currentSteps.Where(step => step.UpdatedUTC.HasValue).Max(step => step.UpdatedUTC.Value);
                    processingTime = DateTime.UtcNow - lastUpdated;
                }
                else
                {
                    // Manual steps, so don't expire
                    processingExpired = false;
                }
            }
            else if (order.StatusAsEnum == OrderStatus.WaitingForPaymentCompleted)
            {
                // Don't expire until paid.
                processingExpired = false;
            }
            else if (order.StatusAsEnum == OrderStatus.Unprocessable)
            {
                // It's failed, processing expired (it's inherited from the pre-routing logic to report this as expired)
                processingExpired = true;
            }

            // Check if we have a processingexpired outcome already, otherwise base on processing time
            if (!processingExpired.HasValue && processingTime != TimeSpan.MinValue)
            {
                processingExpired = processingTime.TotalSeconds > 90;
            }
            else
            {
                // Be postive, we can't check... in live, continue, in dev ERROR and fix.
                if (TestUtil.IsPcDeveloper)
                {
                    throw new Exception("This should never happen, check code and fix");
                }
                processingExpired = false;
            }

            return processingExpired.Value;
        }

        /// <summary>
        ///     Determines whether the step is 'Finished', which means it needs no furhter handling
        ///     (Could be completed,
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>
        ///     <c>true</c> if the specified status is finished; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFailed(int status)
        {
            return RoutingHelper.IsFailed(status.ToEnum<OrderRoutestephandlerStatus>());
        }

        /// <summary>
        ///     Determines whether the step is 'Finished', which means it needs no furhter handling
        ///     (Could be completed,
        /// </summary>
        /// <param name="routestephandlerSaveStatus">The routestephandler save status.</param>
        /// <returns>
        ///     <c>true</c> if the step needs no further handing; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFailed(this OrderRoutestephandlerSaveStatus routestephandlerSaveStatus)
        {
            OrderRoutestephandlerStatus status = routestephandlerSaveStatus.Status.ToEnum<OrderRoutestephandlerStatus>();
            return RoutingHelper.IsFailed(status);
        }

        /// <summary>
        ///     Determines whether the step is 'Finished', which means it needs no furhter handling
        ///     (Could be completed,
        /// </summary>
        /// <param name="routestephandler">The routestephandler</param>
        /// <returns>
        ///     <c>true</c> if the step needs no further handing; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFailed(this OrderRoutestephandler routestephandler)
        {
            OrderRoutestephandlerStatus status = routestephandler.Status.ToEnum<OrderRoutestephandlerStatus>();
            return RoutingHelper.IsFailed(status);
        }

        /// <summary>
        ///     Determines whether the specified status is failed.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>
        ///     <c>true</c> if the specified status is failed; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFailed(OrderRoutestephandlerStatus status)
        {
            if (status == OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure || status == OrderRoutestephandlerStatus.Failed)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Determines whether the step is 'Finished', which means it needs no furhter handling
        ///     (Could be completed,
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>
        ///     <c>true</c> if the specified status is finished; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFinished(int status)
        {
            return RoutingHelper.IsFinished(status.ToEnum<OrderRoutestephandlerStatus>());
        }

        /// <summary>
        ///     Determines whether the step is 'Finished', which means it needs no furhter handling
        ///     (Could be completed,
        /// </summary>
        /// <param name="routestephandlerSaveStatus">The routestephandler save status.</param>
        /// <returns>
        ///     <c>true</c> if the step needs no further handing; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFinished(this OrderRoutestephandlerSaveStatus routestephandlerSaveStatus)
        {
            OrderRoutestephandlerStatus status = routestephandlerSaveStatus.Status.ToEnum<OrderRoutestephandlerStatus>();
            return RoutingHelper.IsFinished(status);
        }

        /// <summary>
        ///     Determines whether the step is 'Finished', which means it needs no furhter handling
        ///     (Could be completed,
        /// </summary>
        /// <param name="routestephandler">The routestephandler</param>
        /// <returns>
        ///     <c>true</c> if the step needs no further handing; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsFinished(this OrderRoutestephandler routestephandler)
        {
            OrderRoutestephandlerStatus status = routestephandler.Status.ToEnum<OrderRoutestephandlerStatus>();
            return RoutingHelper.IsFinished(status);
        }

        /// <summary>
        ///     Determines whether the specified status is finished.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>
        ///     <c>true</c> if the specified status is finished; otherwise, <c>false</c>.
        /// </returns>
        private static bool IsFinished(OrderRoutestephandlerStatus status)
        {
            // Failed isn't finished anymore cause otherwise the table remains locked
            if (status == OrderRoutestephandlerStatus.Completed || status == OrderRoutestephandlerStatus.CompletedByOtherStep ||
                //status == OrderRoutestephandlerStatus.Failed ||
                status == OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure)
            {
                return true;
            }
            return false;
        }

        #endregion

        /// <summary>
        ///     Gets the automaticly handled routestephandlers, which don't require human handling / action.
        /// </summary>
        /// <returns></returns>
        public static List<RoutestephandlerType> GetAutomaticlyHandledRoutestephandlers()
        {
            List<RoutestephandlerType> types = new List<RoutestephandlerType>();
            types.Add(RoutestephandlerType.POS);
            types.Add(RoutestephandlerType.Printer);
            types.Add(RoutestephandlerType.EmailOrder);
            types.Add(RoutestephandlerType.SMS);
            types.Add(RoutestephandlerType.HotSOS);
            types.Add(RoutestephandlerType.Quore);
            types.Add(RoutestephandlerType.Hyatt_HotSOS);
            types.Add(RoutestephandlerType.Alice);
            types.Add(RoutestephandlerType.EmailDocument);
            return types;
        }

        /// <summary>
        ///     Get routestephandlers which are automatically handled by the webservice and don't require
        ///     human or terminal handling / action
        /// </summary>
        /// <returns></returns>
        public static List<RoutestephandlerType> GetNoTerminalRoutestephandlers()
        {
            List<RoutestephandlerType> types = new List<RoutestephandlerType>();
            types.Add(RoutestephandlerType.EmailOrder);
            types.Add(RoutestephandlerType.SMS);
            types.Add(RoutestephandlerType.HotSOS);
            types.Add(RoutestephandlerType.Quore);
            types.Add(RoutestephandlerType.Hyatt_HotSOS);
            types.Add(RoutestephandlerType.Alice);
            types.Add(RoutestephandlerType.EmailDocument);
            return types;
        }

        /// <summary>
        ///     Gets the manually handled routestephandlers, which do require human handling / action.
        /// </summary>
        /// <returns></returns>
        public static List<RoutestephandlerType> GetManuallyHandledRoutestephandlers()
        {
            List<RoutestephandlerType> types = new List<RoutestephandlerType>();
            types.Add(RoutestephandlerType.Console);
            return types;
        }

        /// <summary>
        ///     Gets the pending statuses, meaning it's about to be handled or being handled
        /// </summary>
        /// <returns></returns>
        public static List<OrderRoutestephandlerStatus> GetPendingStatuses()
        {
            List<OrderRoutestephandlerStatus> statuses = new List<OrderRoutestephandlerStatus>();
            statuses.Add(OrderRoutestephandlerStatus.BeingHandled);
            statuses.Add(OrderRoutestephandlerStatus.WaitingToBeRetrieved);
            return statuses;
        }

        /// <summary>
        ///     Gets the amount of expired orders or all expired orders for a company
        /// </summary>
        /// <param name="companyId">Id of the company. Can be null</param>
        /// <returns></returns>
        public static int GetExpiredOrdersCount(int? companyId)
        {
            PredicateExpression orderFilter;
            RelationCollection joins;

            // Get the basics for joining and filtering to get expired orders
            RoutingHelper.GetExpiredOrdersRetrievalSet(out orderFilter, out joins);

            if (companyId.HasValue)
            {
                orderFilter.Add(OrderFields.CompanyId == companyId.Value);
            }

            OrderCollection orders = new OrderCollection();
            return orders.GetDbCount(orderFilter, joins);
        }

        /// <summary>
        ///     Gets a filter and relationscollection to be able to retrieve all expired orders in the database
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="joins">The joins.</param>
        public static void GetExpiredOrdersRetrievalSet(out PredicateExpression filter, out RelationCollection joins)
        {
            // Only check orders that are pending (not processed, waiting for payment of failed)

            PredicateExpression filterPendingOrders = new PredicateExpression();
            filterPendingOrders.Add(OrderFields.Status == (int)OrderStatus.NotRouted);
            filterPendingOrders.AddWithOr(OrderFields.Status == (int)OrderStatus.InRoute);
            filterPendingOrders.AddWithOr(OrderFields.Status == (int)OrderStatus.Routed);

            // The cases of orders that expire:
            // Expire scenario 1: Routed but not started (Order.Created is older than 90s)
            PredicateExpression filterNotStarted = new PredicateExpression();
            filterNotStarted.Add(OrderFields.Status == (int)OrderStatus.Routed);
            filterNotStarted.Add(OrderFields.CreatedUTC < DateTime.UtcNow.AddSeconds(-90));

            // Expire scenario 2: Current routestep(s) are automatic (POS, Email, etc.) but taking longer than 90s
            PredicateExpression filterStepTimedOut = new PredicateExpression();

            // Only check steps that are automatic
            List<int> automaticHandlerTypes = RoutingHelper.GetAutomaticlyHandledRoutestephandlers().Select(type => (int)type).ToList();
            filterStepTimedOut.Add(OrderRoutestephandlerFields.HandlerType == automaticHandlerTypes);

            // Only check steps that are current
            List<int> currentStatuses = RoutingHelper.GetPendingStatuses().Select(type => (int)type).ToList();
            filterStepTimedOut.Add(OrderRoutestephandlerFields.Status == currentStatuses);

            // Only steps that are active for more than 90s
            filterStepTimedOut.Add(OrderRoutestephandlerFields.UpdatedUTC < DateTime.UtcNow.AddSeconds(-90));

            PredicateExpression filterExpiredOrders = new PredicateExpression();
            filterExpiredOrders.Add(filterNotStarted);
            filterExpiredOrders.AddWithOr(filterStepTimedOut);

            // Combine expiration scenario's
            filter = new PredicateExpression();
            filter.Add(filterPendingOrders);
            filter.Add(filterExpiredOrders);

            joins = new RelationCollection();
            joins.Add(OrderEntityBase.Relations.OrderRoutestephandlerEntityUsingOrderId);
        }

        /// <summary>
        ///     Convert the OrderRoutestephandlers to a CSV text
        /// </summary>
        /// <param name="steps">The steps.</param>
        /// <returns></returns>
        public static string OrderroutestephandlersToCsv(OrderRoutestephandlerCollection steps)
        {
            StringBuilder csv = new StringBuilder();
            csv.AppendFormatLine("OrderRoutestephandlerId,Guid,OrderId,TerminalId,Number,HandlerType,PrintReportType,Status,ErrorCode,ErrorText,Completed,FieldValue1,FieldValue2,FieldValue3,FieldValue4,FieldValue5,FieldValue6,FieldValue7,FieldValue8,FieldValue9,FieldValue10,Created,CreatedBy,Updated,UpdatedBy");
            foreach (OrderRoutestephandlerEntity step in steps)
            {
                #region Write CSV line

                // OrderRoutestephandlerId
                csv.AppendFormat("{0},", step.OrderRoutestephandlerId);
                // Guid
                csv.AppendFormat("{0},", step.Guid);
                // OrderId
                csv.AppendFormat("{0},", step.OrderId);
                // TerminalId
                csv.AppendFormat("{0},", step.TerminalId);
                // Number
                csv.AppendFormat("{0},", step.Number);
                // HandlerType
                csv.AppendFormat("{0},", step.HandlerTypeAsEnum);
                // PrintReportType
                csv.AppendFormat("{0},", step.PrintReportType);
                // Status
                csv.AppendFormat("{0},", step.StatusAsEnum);
                // ErrorCode 
                csv.AppendFormat("{0},", step.ErrorCode);
                // ErrorText
                csv.AppendFormat("{0},", step.ErrorText);
                // Completed
                csv.AppendFormat("{0},", step.CompletedUTC);
                // FieldValue1 
                csv.AppendFormat("{0},", step.FieldValue1);
                // FieldValue2
                csv.AppendFormat("{0},", step.FieldValue2);
                // FieldValue3
                csv.AppendFormat("{0},", step.FieldValue3);
                // FieldValue4
                csv.AppendFormat("{0},", step.FieldValue4);
                // FieldValue5
                csv.AppendFormat("{0},", step.FieldValue5);
                // FieldValue6
                csv.AppendFormat("{0},", step.FieldValue6);
                // FieldValue7
                csv.AppendFormat("{0},", step.FieldValue7);
                // FieldValue8
                csv.AppendFormat("{0},", step.FieldValue8);
                // FieldValue9
                csv.AppendFormat("{0},", step.FieldValue9);
                // FieldValue10
                csv.AppendFormat("{0},", step.FieldValue10);
                // Created
                csv.AppendFormat("{0},", step.CreatedUTC);
                // CreatedBy
                csv.AppendFormat("{0},", step.CreatedBy);
                // Updated
                csv.AppendFormat("{0},", step.UpdatedUTC);
                // UpdatedBy
                csv.AppendFormat("{0}", step.UpdatedBy);

                // End line
                csv.AppendFormat("\r\n");

                #endregion
            }

            return csv.ToString();
        }

        /// <summary>
        ///     Adds the requested amount of steps to route.
        /// </summary>
        /// <param name="routeId">The route id.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="transaction">The transaction.</param>
        /// <returns></returns>
        public static bool AddStepsToRoute(int routeId, int amount, ITransaction transaction)
        {
            // Get highest stepnumber
            RoutestepCollection steps = new RoutestepCollection();
            steps.AddToTransaction(transaction);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(RoutestepFields.RouteId == routeId);

            object scalarResult = steps.GetScalar(RoutestepFieldIndex.Number, null, AggregateFunction.Max, filter);
            int maxNumber = scalarResult != null && scalarResult is int ? (int)scalarResult : 0;
            maxNumber++;

            for (int i = 0; i < amount; i++)
            {
                RoutestepEntity step = new RoutestepEntity();
                step.AddToTransaction(transaction);
                step.RouteId = routeId;
                step.Number = maxNumber;
                step.Save();

                maxNumber++;
            }

            return true;
        }

        #region Reverse Compatability Sheit

        /// <summary>
        ///     Creates the missing routes for all companies without routes
        /// </summary>
        /// <returns>Report of all performed actions</returns>
        public static string CreateMissingRoutes(bool commitTransaction)
        {
            // Create a new transaction
            StringBuilder sbSuccess = new StringBuilder();
            StringBuilder sbFailures = new StringBuilder();
            Transaction transaction = new Transaction(IsolationLevel.ReadCommitted, "RoutingHelper.CreateMissingRoutes-" + DateTime.UtcNow.Ticks);
            StringBuilder sb = new StringBuilder();
            // Start working with the transaction
            try
            {
                // Retrieve all Deliverypointgroupes without a Route             
                PredicateExpression filter = new PredicateExpression();
                filter.Add(DeliverypointgroupFields.RouteId == DBNull.Value);

                PrefetchPath path = new PrefetchPath(EntityType.DeliverypointgroupEntity);
                path.Add(DeliverypointgroupEntityBase.PrefetchPathCompanyEntity).SubPath.Add(CompanyEntityBase.PrefetchPathTerminalCollection);

                DeliverypointgroupCollection deliverypointGroups = new DeliverypointgroupCollection();
                deliverypointGroups.AddToTransaction(transaction);
                deliverypointGroups.GetMulti(filter, path);

                foreach (DeliverypointgroupEntity group in deliverypointGroups)
                {
                    sb = new StringBuilder();
                    sb.AppendFormatLine("Starting with group: {0} (Company: {1})", group.Name, group.CompanyEntity.Name);
                    // Check if the company has any routes, if so, just assign that
                    RouteCollection routes = new RouteCollection();
                    routes.AddToTransaction(transaction);
                    routes.GetMulti(RouteFields.CompanyId == group.CompanyId);

                    RouteEntity route = new RouteEntity();
                    if (routes.Count == 1)
                    {
                        route = routes[0];
                    }
                    else if (routes.Count > 1)
                    {
                        sb.AppendFormatLine("Company has multiple routes, can't pick one.");
                    }
                    if (routes.Count == 0)
                    {
                        sb.AppendFormatLine("Company has no routes, created one.");
                        // Create a route                        
                        route.Name = "Default route (generated)";
                        route.CompanyId = group.CompanyId;
                        route.AddToTransaction(transaction);

                        // Create a single route step
                        RoutestepEntity step = new RoutestepEntity();
                        step.Number = RoutingHelper.FIRST_STEP_NUMBER;
                        step.AddToTransaction(transaction);
                        route.RoutestepCollection.Add(step);

                        // Create a single handlers      
                        if (group.CompanyEntity.TerminalCollection.Count == 0)
                        {
                            sb.AppendFormatLine("Can't create route: No terminals");
                        }
                        else if (group.CompanyEntity.TerminalCollection.Count > 1)
                        {
                            sb.AppendFormatLine("Can't create route: Multiple terminals");
                        }
                        else
                        {
                            TerminalEntity terminal = group.CompanyEntity.TerminalCollection[0];
                            if (terminal.TerminalType == (int)TerminalType.Demo)
                            {
                                sb.AppendFormatLine("Can't create route: Terminal Type is DEMO.");
                            }
                            else if (terminal.TerminalType == (int)TerminalType.OnSiteServer)
                            {
                                // POS!
                                if (terminal.POSConnectorTypeEnum == POSConnectorType.Unknown)
                                {
                                    sb.AppendFormatLine("Can't create route: Terminal Type is OSS, but no POS Connector is selected.");
                                }
                                else
                                {
                                    RoutestephandlerEntity routestephandler = new RoutestephandlerEntity();
                                    routestephandler.TerminalEntity = terminal;
                                    routestephandler.HandlerType = (int)RoutestephandlerType.POS;
                                    step.RoutestephandlerCollection.Add(routestephandler);
                                    route.Save(true);
                                }
                            }
                            else if (terminal.TerminalType == (int)TerminalType.Console)
                            {
                                // Console!
                                RoutestephandlerEntity routestephandler = new RoutestephandlerEntity();
                                routestephandler.TerminalEntity = terminal;
                                routestephandler.HandlerType = (int)RoutestephandlerType.Console;
                                step.RoutestephandlerCollection.Add(routestephandler);
                                route.Save(true);
                            }
                            else
                            {
                                // UNKNOWN
                                sb.AppendFormatLine("Can't create route: Terminal Type is UNKNOWN.");
                            }
                        }

                        if (route.IsNew)
                        {
                            sb.AppendFormatLine("FAILED - Route could not be determined or created, can't continue");
                            sbFailures.AppendFormatLine(sb.ToString());
                        }
                        else
                        {
                            group.RouteId = route.RouteId;
                            group.AddToTransaction(transaction);
                            group.Save();
                            sb.AppendFormatLine("SUCCESS - Route created HandlingMethod: {0}", route.RoutestepCollection[0].RoutestephandlerCollection[0].HandlerType.ToEnum<RoutestephandlerType>().ToString());
                            sbSuccess.AppendFormatLine(sb.ToString());
                        }
                    }
                }

                // Commit the transaction
                if (commitTransaction)
                {
                    transaction.Commit();
                }
                else
                {
                    transaction.Rollback();
                }
            }
            catch (Exception ex)
            {
                // Something went wrong, rollback the full transaction
                transaction.Rollback();
                sbFailures.AppendFormatLine(sb.ToString());
                sb.AppendFormatLine("FAILED ALL - Exception: {0}", ex.Message);

                throw;
            }
            finally
            {
                // Dispose the Transaction to free the DB connection
                transaction.Dispose();
                InformationLoggerWeb.LogInformation("MissingRoutesGenerated", sb.ToString());
            }

            return string.Format("<h1>Failures</h1>: {0}\r\n\r\n<h1>Successes</h1>: {1}", sbFailures, sbSuccess);
        }

        #endregion

        //    RoutingHelper.SendOrderPlacedToConsoles(new OrderEntity(orderId));
        //}

        ///// <summary>
        ///// Sends the OrderPlaced Poke to consoles.
        ///// </summary>
        ///// <param name="orderId">The order id.</param>
        //public static void SendOrderPlacedToConsoles(OrderEntity order)
        //{
        //    if (order == null || order.IsNew)
        //        return;

        //    OrderRoutestephandlerCollection steps = new OrderRoutestephandlerCollection();
        //    PredicateExpression filter = new PredicateExpression();
        //    filter.Add(OrderRoutestephandlerFields.OrderId == order.OrderId);
        //    filter.Add(OrderRoutestephandlerFields.Status == OrderRoutestephandlerStatus.WaitingToBeRetrieved);
        //    filter.Add(OrderRoutestephandlerFields.TerminalId > 0);
        //    filter.Add(OrderRoutestephandlerFields.HandlerType == RoutestephandlerType.Console);
        //    steps.GetMulti(filter);

        //    foreach (var step in steps)
        //    {
        //        if (step.TerminalId.HasValue)
        //            CometHelper.OrderPlaced(step.TerminalId.Value, order.CustomerId ?? 0, order.ClientId ?? 0);
        //    }
        //}
    }
}