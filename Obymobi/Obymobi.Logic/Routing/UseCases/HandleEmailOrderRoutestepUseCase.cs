﻿using Dionysos;
using Dionysos.Net.Mail;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logging;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Logic.Routing.Requests;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text;

namespace Obymobi.Logic.Routing.UseCases
{
    public class HandleEmailOrderRoutestepUseCase : HandleRoutestepUseCaseBase<HandleEmailOrderRoutestepRequest>
    {
        public override void HandleRoutestep(HandleEmailOrderRoutestepRequest request)
        {
            int orderRoutestephandlerEntityId = request.OrderRoutestephandlerEntityId;
            string emailHtml = request.EmailHtml;
            Func<MediaEntity, byte[]> downloadMediaDelegate = request.DownloadMediaDelegate;

            OrderRoutestephandlerEntity orderRoutestephandlerEntity = new OrderRoutestephandlerEntity(orderRoutestephandlerEntityId);
            if (!orderRoutestephandlerEntity.IsNew)
            {
                try
                {
                    OrderEntity fullOrder = new OrderEntity(orderRoutestephandlerEntity.OrderId);
                    if (!fullOrder.IsNew)
                    {
                        // FieldValue 1 = To Address & FieldValue6 = To Customer (send the email (also) to the customer)
                        List<string> toAddresses = GetToAddresses(orderRoutestephandlerEntity, fullOrder);

                        // FieldValue2 = From E-mail                        
                        string fromAddress = orderRoutestephandlerEntity.FieldValue2;

                        // FieldValue3 = Email Title
                        string emailTitle = orderRoutestephandlerEntity.FieldValue3.IsNullOrWhiteSpace() ? "No subject" : orderRoutestephandlerEntity.FieldValue3;

                        // FieldValue4 = Custom (Body) Text
                        string customBodyText = orderRoutestephandlerEntity.FieldValue4;

                        // FieldValue5 = Don't use Template but use customBodyText Only
                        bool dontUseDefaultTemplate = (!orderRoutestephandlerEntity.FieldValue5.IsNullOrWhiteSpace() && orderRoutestephandlerEntity.FieldValue5.Equals("1"));

                        // Html to fill the [[BODY]] of the template with.
                        string templateBody = Properties.Resources.NewOrderBody;
                        bool customBody = false;

                        // Overwrite the text
                        if (!customBodyText.IsNullOrWhiteSpace())
                        {
                            if (dontUseDefaultTemplate)
                            {
                                if (customBodyText.Contains("<html>"))
                                    emailHtml = customBodyText;
                                else
                                    emailHtml = StringUtil.ReplaceLineBreakWithBR(orderRoutestephandlerEntity.FieldValue4); // Plain text body
                                customBody = true;
                            }
                            else
                            {
                                emailHtml = emailHtml.Replace("[[BODY]]", StringUtil.ReplaceLineBreakWithBR(customBodyText)); // Body with footer/header template                                            
                            }
                        }
                        else // Use the default body.
                        {
                            emailHtml = emailHtml.Replace("[[BODY]]", File.ReadAllText(templateBody));
                        }

                        // Get images from Media if it's a customBody                        
                        Dictionary<string, MemoryStream> imageStreams = null;
                        if (customBody && emailHtml.Contains("src=") && orderRoutestephandlerEntity.OriginatedFromRoutestepHandlerId.HasValue)
                        {
                            imageStreams = new Dictionary<string, MemoryStream>();

                            // We need to check for images related to this RouteStepHandler
                            PrefetchPath path = new PrefetchPath(EntityType.RoutestephandlerEntity);
                            path.Add(RoutestephandlerEntity.PrefetchPathMediaCollection);
                            RoutestephandlerEntity handler = new RoutestephandlerEntity(orderRoutestephandlerEntity.OriginatedFromRoutestepHandlerId.Value, path);

                            foreach (MediaEntity mediaEntity in handler.MediaCollection)
                            {
                                byte[] fileBytes = downloadMediaDelegate.Invoke(mediaEntity);
                                if (fileBytes != null && fileBytes.Length > 0)
                                {
                                    MemoryStream ms = new MemoryStream(fileBytes);
                                    imageStreams.Add(mediaEntity.Name, ms);
                                }
                            }
                        }

                        // Prepare variables to merge with
                        StringDictionary templateVariables = GenerateEmailBodyTemplate(orderRoutestephandlerEntity);

                        const int timeout = 30000;

                        foreach (string address in toAddresses)
                        {
                            MailUtilV2 mailUtil = new MailUtilV2(fromAddress, address, emailTitle, imageStreams);
                            mailUtil.SmtpClient.Timeout = timeout;
                            mailUtil.BodyHtml = emailHtml;
                            mailUtil.SendMail(templateVariables).Wait(timeout);
                        }

                        if (imageStreams != null)
                        {
                            foreach (KeyValuePair<string, MemoryStream> stream in imageStreams)
                            {
                                stream.Value.Dispose();
                            }
                        }
                    }

                    // OrderRoutestephandler is complete when the email is sent
                    orderRoutestephandlerEntity.StatusAsEnum = OrderRoutestephandlerStatus.Completed;
                }
                catch (Exception ex)
                {
                    orderRoutestephandlerEntity.StatusAsEnum = OrderRoutestephandlerStatus.Failed;
                    string error = ExceptionUtil.GetAllMessages(ex, true);

                    if (error.Contains("Unable to connect to the remote server"))
                        orderRoutestephandlerEntity.ErrorCodeAsEnum = Enums.OrderProcessingError.SmtpServerError;
                    else if (error.Contains("Failure sending mail"))
                        orderRoutestephandlerEntity.ErrorCodeAsEnum = Enums.OrderProcessingError.UnspecifiedSendingMailError;
                    else
                        orderRoutestephandlerEntity.ErrorCodeAsEnum = Enums.OrderProcessingError.UnspecifiedError;

                    orderRoutestephandlerEntity.OrderEntity.RoutingLog += "\r\nFailure EmailOrderRouteStepHandler for step: {0} \r\nMessage: {1}".FormatSafe(orderRoutestephandlerEntity.Number, error);

                    Exceptionlogger.CreateExceptionlog(ex);
                }
                finally
                {
                    // Create a new transaction
                    Transaction transaction = new Transaction(IsolationLevel.ReadCommitted, "EmailOrderRoutestephandlerHelper");

                    // Start working with the transaction
                    try
                    {
                        orderRoutestephandlerEntity.AddToTransaction(transaction);
                        orderRoutestephandlerEntity.Save();

                        if (orderRoutestephandlerEntity.AlreadyFetchedOrderEntity)
                        {
                            orderRoutestephandlerEntity.OrderEntity.AddToTransaction(transaction);
                            orderRoutestephandlerEntity.OrderEntity.Save();
                        }

                        // Commit the transaction
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        // Something went wrong, rollback the full transaction
                        transaction.Rollback();

                        Exceptionlogger.CreateExceptionlog(ex);
                        throw ex;
                    }
                    finally
                    {
                        // Dispose the Transaction to free the DB connection
                        transaction.Dispose();
                    }
                }
            }
        }

        private List<string> GetToAddresses(OrderRoutestephandlerEntity orderRoutestephandlerEntity, OrderEntity fullOrder)
        {
            List<string> toAddresses = new List<string>();
            // FieldValue1 = To E-mail (being a support pool, manually entered email address and/or to the customer
            string toLine = string.Empty;
            if (orderRoutestephandlerEntity.SupportpoolId.HasValue && orderRoutestephandlerEntity.SupportpoolId.Value > 0)
            {
                toLine = orderRoutestephandlerEntity.SupportpoolEntity.Email;
            }
            if (!orderRoutestephandlerEntity.FieldValue1.IsNullOrWhiteSpace())
            {
                toLine = StringUtil.CombineWithSemicolon(toLine, orderRoutestephandlerEntity.FieldValue1);
            }

            // FieldValue6 = To Customer
            if (!orderRoutestephandlerEntity.FieldValue6.IsNullOrWhiteSpace() && orderRoutestephandlerEntity.FieldValue6.Equals("1"))
            {
                if (!fullOrder.Email.IsNullOrWhiteSpace())
                    toLine = StringUtil.CombineWithSemicolon(toLine, fullOrder.Email);
                else if (fullOrder.CustomerEntity.Email.IsNullOrWhiteSpace())
                    toLine = StringUtil.CombineWithSemicolon(toLine, fullOrder.CustomerEntity.Email);
            }

            // Make 1 splitter:
            toLine = toLine.Replace(',', ';');
            toLine = toLine.Replace(' ', ';');

            if (toLine.Contains(";"))
            {
                string[] addresses = toLine.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string address in addresses)
                    toAddresses.Add(address);
            }
            else
                toAddresses.Add(toLine);

            return toAddresses;
        }

        private StringDictionary GenerateEmailBodyTemplate(OrderRoutestephandlerEntity orderRoutestephandlerEntity)
        {
            OrderEntity orderEntity = orderRoutestephandlerEntity.OrderEntity;
            Order order = OrderHelper.CreateOrderModelFromEntity(orderEntity, false, true);

            TimeZoneInfo toTimeZoneInfo = CompanyHelper.GetCompanyTimeZone(orderEntity.CompanyId);
            TimeZoneInfo fromTimeZoneInfo = TimeZoneInfo.Utc;
            DateTime orderCratedDateTime = orderEntity.CreatedUTC.GetValueOrDefault();

            StringDictionary templateVars = new StringDictionary();
            templateVars.Add("DELIVERYPOINTCAPTION", orderEntity.DeliverypointEntity.DeliverypointgroupEntity.DeliverypointCaption);
            templateVars.Add("DELIVERYPOINT", order.DeliverypointNumber.ToString());
            templateVars.Add("DELIVERYPOINTNUMBER", order.DeliverypointNumber.ToString());
            templateVars.Add("DELIVERYPOINTNAME", order.DeliverypointName);
            templateVars.Add("CREATED", orderCratedDateTime.ToTimeZone(fromTimeZoneInfo, toTimeZoneInfo).ToString(CultureInfo.InvariantCulture));

            StringBuilder orderlistVar = new StringBuilder();
            orderlistVar.AppendLine("<table width='480' border='0'>");
            foreach (Orderitem orderitem in order.Orderitems)
            {
                orderlistVar.Append("<tr><td width='30' class='orderitem'>");
                orderlistVar.Append(orderitem.Quantity);
                orderlistVar.Append("</td><td width='450' class='orderitem'>");
                orderlistVar.Append(orderitem.ProductName);
                orderlistVar.AppendLine("</td></tr>");

                if (orderitem.Alterationitems != null)
                {
                    foreach (Alterationitem alterationitem in orderitem.Alterationitems)
                    {
                        orderlistVar.Append("<tr><td width='30' class='alterationitem'></td><td width='450' class='alterationitem'>");
                        orderlistVar.Append(alterationitem.AlterationName + ": ");

                        if (!alterationitem.AlterationoptionName.IsNullOrWhiteSpace())
                        {
                            orderlistVar.Append(alterationitem.AlterationoptionName);
                        }
                        else if (!alterationitem.Time.IsNullOrWhiteSpace())
                        {
                            orderlistVar.Append(alterationitem.Time);
                        }
                        else if (!alterationitem.Value.IsNullOrWhiteSpace())
                        {
                            orderlistVar.Append(alterationitem.Value);
                        }
                        else
                        {
                            orderlistVar.Append("????");
                        }

                        orderlistVar.AppendLine("</td></tr>");
                    }
                }

                if (!string.IsNullOrWhiteSpace(orderitem.Notes))
                {
                    orderlistVar.Append("<tr><td width='30' class='alterationitem'></td><td width='450' class='alterationitem'>");
                    orderlistVar.Append("Special instructions: ");
                    orderlistVar.Append(orderitem.Notes);
                    orderlistVar.AppendLine("</td></tr>");
                }
            }

            orderlistVar.AppendLine("</table>");

            templateVars.Add("ORDER", orderlistVar.ToString());
            templateVars.Add("COMPANYNAME", orderEntity.CompanyName);

            templateVars.Add("NOTES", orderEntity.Notes.ReplaceLineBreakWithBR());
            templateVars.Add("STATUSTEXT", orderEntity.StatusText);
            templateVars.Add("PRICE", orderEntity.PriceIn.ToString("0.00"));
            templateVars.Add("ORDERNUMBER", orderEntity.OrderId.ToString());
            templateVars.Add("CUSTOMERLASTNAME", orderEntity.CustomerLastname);
            templateVars.Add("CUSTOMEREMAIL", orderEntity.Email);
            templateVars.Add("CUSTOMERPHONE", orderEntity.CustomerPhonenumber);

            return templateVars;
        }
    }
}
