﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using Dionysos;
using Dionysos.Net.Mail;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logging;
using Obymobi.Logic.Routing.Requests;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;

namespace Obymobi.Logic.Routing.UseCases
{
    public class HandleEmailDocumentRoutestepUseCase : HandleRoutestepUseCaseBase<HandleEmailDocumentRoutestepRequest>
    {
        public override void HandleRoutestep(HandleEmailDocumentRoutestepRequest request)
        {
            int orderRoutestephandlerEntityId = request.OrderRoutestephandlerEntityId;
            byte[] file = request.File;
            string emailHtml = request.EmailHtml;

            var orderRoutestephandlerEntity = new OrderRoutestephandlerEntity(orderRoutestephandlerEntityId);
            if (!orderRoutestephandlerEntity.IsNew)
            {
                try
                {
                    var prefetch = new PrefetchPath(EntityType.OrderEntity);
                    prefetch.Add(OrderEntityBase.PrefetchPathDeliverypointEntity).SubPath.Add(DeliverypointEntityBase.PrefetchPathDeliverypointgroupEntity);

                    var fullOrder = new OrderEntity(orderRoutestephandlerEntity.OrderId, prefetch);
                    if (!fullOrder.IsNew)
                    {
                        // FieldValue 1 = To Address 
                        List<string> toAddresses = this.GetToAddresses(orderRoutestephandlerEntity);

                        // FieldValue2 = From E-mail                        
                        string fromAddress = "no-reply@crave-emenu.com";

                        // FieldValue3 = Email Title
                        string emailTitle = "New print request!";

                        // Prepare variables to merge with
                        var templateVariables = this.GenerateEmailBodyTemplate(orderRoutestephandlerEntity.OrderEntity);

                        // Get images from Media if it's a customBody                        
                        var attachments = new List<System.Net.Mail.Attachment>();
                        using (MemoryStream stream = new MemoryStream(file))
                        {
                            var roomCaption = !fullOrder.DeliverypointEntity.DeliverypointgroupEntity.DeliverypointCaption.IsNullOrWhiteSpace() ? fullOrder.DeliverypointEntity.DeliverypointgroupEntity.DeliverypointCaption : "Room";

                            var filename = string.Format("document-{0}-{1}.pdf", roomCaption.ToLowerInvariant(), fullOrder.DeliverypointEntity.Number);

                            var attachment = new System.Net.Mail.Attachment(stream, filename);
                            attachments.Add(attachment);

                            const int timeout = 30000;

                            foreach (var address in toAddresses)
                            {
                                var mailUtil = new MailUtilV2(fromAddress, address, emailTitle);
                                mailUtil.SmtpClient.Timeout = timeout;
                                mailUtil.BodyHtml = emailHtml;
                                mailUtil.Attachments = attachments;
                                mailUtil.SendMail(templateVariables).Wait(timeout);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    orderRoutestephandlerEntity.StatusAsEnum = OrderRoutestephandlerStatus.Failed;
                    string error = ExceptionUtil.GetAllMessages(ex, true);

                    if (error.Contains("Unable to connect to the remote server"))
                        orderRoutestephandlerEntity.ErrorCodeAsEnum = Enums.OrderProcessingError.SmtpServerError;
                    else if (error.Contains("Failure sending mail"))
                        orderRoutestephandlerEntity.ErrorCodeAsEnum = Enums.OrderProcessingError.UnspecifiedSendingMailError;
                    else
                        orderRoutestephandlerEntity.ErrorCodeAsEnum = Enums.OrderProcessingError.UnspecifiedError;

                    orderRoutestephandlerEntity.OrderEntity.RoutingLog += "\r\nFailure EmailDocumentRouteStepHandler for step: {0} \r\nMessage: {1}".FormatSafe(orderRoutestephandlerEntity.Number, error);

                    Exceptionlogger.CreateExceptionlog(ex);
                }
                finally
                {
                    // Create a new transaction
                    var transaction = new Transaction(IsolationLevel.ReadCommitted, "EmailDocumentRoutestephandlerHelper");

                    // Start working with the transaction
                    try
                    {
                        orderRoutestephandlerEntity.AddToTransaction(transaction);
                        orderRoutestephandlerEntity.Save();

                        if (orderRoutestephandlerEntity.AlreadyFetchedOrderEntity)
                        {
                            orderRoutestephandlerEntity.OrderEntity.AddToTransaction(transaction);
                            orderRoutestephandlerEntity.OrderEntity.Save();
                        }

                        // Commit the transaction
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        // Something went wrong, rollback the full transaction
                        transaction.Rollback();

                        Exceptionlogger.CreateExceptionlog(ex);
                        throw ex;
                    }
                    finally
                    {
                        // Dispose the Transaction to free the DB connection
                        transaction.Dispose();
                    }
                }
            }
        }

        private List<string> GetToAddresses(OrderRoutestephandlerEntity orderRoutestephandlerEntity)
        {
            var toAddresses = new List<string>();

            // FieldValue1 = To E-mail (being a support pool, manually entered email address and/or to the customer
            var toLine = string.Empty;
            if (!orderRoutestephandlerEntity.FieldValue1.IsNullOrWhiteSpace())
            {
                toLine = StringUtil.CombineWithSemicolon(toLine, orderRoutestephandlerEntity.FieldValue1);
            }

            // Make 1 splitter:
            toLine = toLine.Replace(',', ';');
            toLine = toLine.Replace(' ', ';');

            if (toLine.Contains(";"))
            {
                var addresses = toLine.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                toAddresses.AddRange(addresses);
            }
            else
                toAddresses.Add(toLine);

            return toAddresses;
        }

        private StringDictionary GenerateEmailBodyTemplate(OrderEntity orderEntity)
        {
            var templateVars = new StringDictionary();
            templateVars.Add("DELIVERYPOINTCAPTION", orderEntity.DeliverypointEntity.DeliverypointgroupEntity.DeliverypointCaption.ToLowerInvariant());
            templateVars.Add("DELIVERYPOINT", orderEntity.DeliverypointEntity.Number);
            templateVars.Add("DELIVERYPOINTNUMBER", orderEntity.DeliverypointEntity.Number);
            templateVars.Add("DELIVERYPOINTNAME", orderEntity.DeliverypointEntity.Name);
            templateVars.Add("CREATED", orderEntity.CreatedUTC.ToString());

            return templateVars;
        }
    }
}
