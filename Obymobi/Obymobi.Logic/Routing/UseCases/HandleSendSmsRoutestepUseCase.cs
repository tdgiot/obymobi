﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using Dionysos;
using Dionysos.SMS;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logging;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Logic.Routing.Requests;

namespace Obymobi.Logic.Routing.UseCases
{
    public class HandleSendSmsRoutestepUseCase : HandleRoutestepUseCaseBase<HandleSendSmsRoutestepRequest>
    {
        public override void HandleRoutestep(HandleSendSmsRoutestepRequest request)
        {
            int orderRoutestephandlerEntityId = request.OrderRoutestephandlerEntityId;

            var orderRoutestephandlerEntity = new OrderRoutestephandlerEntity(orderRoutestephandlerEntityId);
            if (!orderRoutestephandlerEntity.IsNew)
            {
                var smsRequest = new MollieSMSRequest();
                try
                {
                    var fullOrder = new OrderEntity(orderRoutestephandlerEntity.OrderId);
                    if (!fullOrder.IsNew)
                    {
                        smsRequest.Originator = orderRoutestephandlerEntity.FieldValue2;

                        if (orderRoutestephandlerEntity.SupportpoolId.HasValue && orderRoutestephandlerEntity.SupportpoolId.Value > 0)
                            smsRequest.Recipients = orderRoutestephandlerEntity.SupportpoolEntity.Phonenumber.Replace(" ", string.Empty);
                        else
                            smsRequest.Recipients = orderRoutestephandlerEntity.FieldValue1.Replace(" ", string.Empty);

                        smsRequest.Message = this.GenerateSMSBody(orderRoutestephandlerEntity);

                        if (smsRequest.Message.Length > 160)
                            smsRequest.Message = smsRequest.Message.Substring(0, 156) + "...";

                        smsRequest.Send();

                        if (smsRequest.ResultEnum == MollieSMSRequest.MollieResult.Success)
                        {
                            if (orderRoutestephandlerEntity.HandlerTypeAsEnum == RoutestephandlerType.SMS)
                            {
                                // OrderRoutestephandler is complete when the email is sent
                                orderRoutestephandlerEntity.StatusAsEnum = OrderRoutestephandlerStatus.Completed;
                            }
                        }
                        else
                        {
                            orderRoutestephandlerEntity.StatusAsEnum = OrderRoutestephandlerStatus.Failed;
                            orderRoutestephandlerEntity.ErrorText = string.Format("Failed to send SMS to {0}. Result code: {1} - {2}", smsRequest.Recipients, smsRequest.ResultCode, smsRequest.ResultMessage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    orderRoutestephandlerEntity.StatusAsEnum = OrderRoutestephandlerStatus.Failed;
                    orderRoutestephandlerEntity.OrderEntity.ErrorText = string.Format("Failed to send SMS to {0}. Exception {1}", smsRequest.Recipients, ex.Message);
                    Exceptionlogger.CreateExceptionlog(ex);
                }
                finally
                {
                    // Create a new transaction
                    var transaction = new Transaction(IsolationLevel.ReadCommitted, "SmsRoutingstephandler");

                    // Start working with the transaction
                    try
                    {
                        orderRoutestephandlerEntity.AddToTransaction(transaction);
                        orderRoutestephandlerEntity.Save();

                        // Commit the transaction
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        // Something went wrong, rollback the full transaction
                        transaction.Rollback();

                        Exceptionlogger.CreateExceptionlog(ex);
                        throw ex;
                    }
                    finally
                    {
                        // Dispose the Transaction to free the DB connection
                        transaction.Dispose();
                    }
                }
            }
        }

        private string GenerateSMSBody(OrderRoutestephandlerEntity orderRoutestephandlerEntity)
        {
            StringDictionary templateVariables = this.GetOrderTemplateVariables(orderRoutestephandlerEntity);

            var smsBody = new StringBuilder();

            if (String.IsNullOrEmpty(orderRoutestephandlerEntity.FieldValue4))
            {
                // Default sms
                smsBody.AppendLine("New Order - [[ORDERNUMBER]]");
                smsBody.AppendLine("[[DELIVERYPOINTCAPTION]]: [[DELIVERYPOINT]]");
            }
            else
            {
                // Custom sms
                smsBody.Append(orderRoutestephandlerEntity.FieldValue4);
            }

            return this.ReplaceTemplateVariables(smsBody.ToString(), templateVariables);
        }

        private StringDictionary GetOrderTemplateVariables(OrderRoutestephandlerEntity orderRoutestephandlerEntity)
        {
            var orderEntity = orderRoutestephandlerEntity.OrderEntity;
            Order order = OrderHelper.CreateOrderModelFromEntity(orderEntity, false, true);

            var templateVars = new StringDictionary();
            templateVars.Add("DELIVERYPOINTCAPTION", order.DeliverypointName);
            templateVars.Add("DELIVERYPOINT", order.DeliverypointNumber.ToString());
            templateVars.Add("CREATED", order.Created);

            var orderlistVar = new StringBuilder();
            foreach (Orderitem orderitem in order.Orderitems)
            {
                orderlistVar.AppendLine(orderitem.Quantity + "x " + orderitem.ProductName);

                if (orderitem.Alterationitems != null)
                {
                    foreach (Alterationitem alterationitem in orderitem.Alterationitems)
                    {
                        orderlistVar.AppendLine("   " + alterationitem.AlterationName + ": " + alterationitem.AlterationoptionName);
                    }
                }
            }

            templateVars.Add("ORDER", orderlistVar.ToString());
            templateVars.Add("COMPANYNAME", orderEntity.CompanyName);
            templateVars.Add("NOTES", orderEntity.Notes);
            templateVars.Add("STATUSTEXT", orderEntity.StatusText);
            templateVars.Add("PRICE", orderEntity.PriceIn.ToString("0.00"));
            templateVars.Add("ORDERNUMBER", orderEntity.OrderId.ToString());
            
            return templateVars;
        }

        /// <summary>
        /// Replaces the template variables.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <param name="templateVariables">The template variables.</param>
        /// <returns>The template with all template variables replaced.</returns>
        private string ReplaceTemplateVariables(string template, StringDictionary templateVariables)
        {
            foreach (DictionaryEntry de in templateVariables)
            {
                template = StringUtil.Replace(template, String.Concat("[[", de.Key, "]]"), (string)de.Value, StringComparison.OrdinalIgnoreCase);
            }

            return template;
        }
    }
}
