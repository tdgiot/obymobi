﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Routing.Requests
{
    public class HandleEmailDocumentRoutestepRequest : HandleRoutestepRequest
    {
        public byte[] File { get; set; }

        public string EmailHtml { get; set; }
    }
}
