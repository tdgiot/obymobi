﻿using Obymobi.Data.EntityClasses;
using System;

namespace Obymobi.Logic.Routing.Requests
{
    public class HandleEmailOrderRoutestepRequest : HandleRoutestepRequest
    {
        public string EmailHtml { get; set; }

        public Func<MediaEntity, byte[]> DownloadMediaDelegate { get; set; }
    }
}
