﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TypedStatsTable
/// </summary>
public class TypedStatsTable<TRowType> : DataTable where TRowType : DataRow
{
	protected TypedStatsTable()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    protected override Type GetRowType()
    {
        return typeof(TRowType);
    }

    public TRowType this[int index]
    {
        get
        {
            return (TRowType)this.Rows[index];
        }
    }

    public void Add(TRowType row)
    {
        this.Rows.Add(row);
    }

    public void Remove(TRowType row)
    {
        this.Rows.Remove(row);
    }

    public new TRowType NewRow()
    {
        TRowType row = (TRowType)base.NewRow();
        return row;
    }
}