﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;

namespace Obymobi.Logic.Analytics
{
    public class EventHubConnectionString
    {
        public EventHubConnectionString(string connectionString)
        {
            string[] connectionStringElements = connectionString.Split(';');
            foreach (var element in connectionStringElements)
            {
                if (element.StartsWith("Endpoint=", StringComparison.InvariantCultureIgnoreCase))
                    this.EndPoint = element.Substring(9).ToLower();
                else if (element.StartsWith("SharedAccessKeyName=", StringComparison.InvariantCultureIgnoreCase))
                    this.SharedAccessKeyName = element.Substring(20);
                else if (element.StartsWith("SharedAccessKey=", StringComparison.InvariantCultureIgnoreCase))
                    this.SharedAccessKey = element.Substring(16);
                else if (element.StartsWith("EntityPath=", StringComparison.InvariantCultureIgnoreCase))
                    this.EntityPath = element.Substring(11);
            }

            List<string> itemsNotFound = new List<string>();
            if (string.IsNullOrWhiteSpace(this.EndPoint))
                itemsNotFound.Add("EndPoint");
            if (string.IsNullOrWhiteSpace(this.SharedAccessKeyName))
                itemsNotFound.Add("SharedAccessKeyName");
            if (string.IsNullOrWhiteSpace(this.SharedAccessKey))
                itemsNotFound.Add("SharedAccessKey");
            if (string.IsNullOrWhiteSpace(this.EntityPath))
                itemsNotFound.Add("EntityPath");

            if (itemsNotFound.Count > 0)
            {
                throw new FormatException(string.Format("ConnectionString '{0}' isnt'correctly formated (Missing or invalid: '{1}'). Example: Endpoint=sb://crvnltcs-dev.servicebus.windows.net/;SharedAccessKeyName=std-send-only;SharedAccessKey=fnfJeefPXn1Bd9qetSXooFAKEooPZglTNru1GXebLRO4X0U=;EntityPath=std",
                    connectionString, string.Join(",", itemsNotFound)));
            }
        }

        public string GetHttpsEndPoint(string publisherId = null)
        {
            string httpsEndPoint = this.EndPoint.Replace("sb://", "https://") + this.EntityPath;
            if (publisherId != null)
            {
                httpsEndPoint = string.Format("{0}/publishers/{1}", httpsEndPoint, publisherId);
            }

            return httpsEndPoint;
        }

        public string EndPoint { get; private set; }
        public string SharedAccessKeyName { get; private set; }
        public string SharedAccessKey { get; private set; }
        public string EntityPath { get; private set; }

    }
}
