﻿using SpreadsheetLight;

namespace Obymobi.Logic.Analytics
{
    public interface IReport
    {
        bool RunReport(out SLDocument sl);
    }
}
