﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

    [Flags]
    public enum Borders
    {
        None = 0,
        Left = 1,
        Top = 2,
        Right = 4,
        Bottom = 8,
        All = Left | Top | Right | Bottom,
    }
