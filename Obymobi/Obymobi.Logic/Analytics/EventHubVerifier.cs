﻿using Dionysos.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Analytics
{
    public class EventHubVerifier : IVerifier
    {
        public string ErrorMessage { get; set; }

        public bool Verify()
        {
            this.ErrorMessage = null;

            try
            {
                EventHubUtil.GenerateSharedAccessSignatureToken(connectionStringText:null);
            }
            catch (Exception ex)
            {
                this.ErrorMessage = "Event Hub configuration is invalid / incomplete: " + ex.Message;
            }

            return this.ErrorMessage == null;
        }
    }
}
