﻿using Dionysos;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Drawing;
using Color = System.Drawing.Color;

/// <summary>
/// Summary description for Spreadsheetstaticants
/// </summary>
public static class SpreadsheetHelper
{
    // Excel Colors
    public static System.Drawing.Color Green1 = System.Drawing.Color.FromArgb(245,248,239);
    public static System.Drawing.Color Green2 = System.Drawing.Color.FromArgb(235,241,222);
    public static System.Drawing.Color Green3 = System.Drawing.Color.FromArgb(225,235,206);
    public static System.Drawing.Color Green4 = System.Drawing.Color.FromArgb(215,228,189);
    public static System.Drawing.Color Green5 = System.Drawing.Color.FromArgb(205,221,172);
    public static System.Drawing.Color Green6 = System.Drawing.Color.FromArgb(195,214,155);
    public static System.Drawing.Color Green7 = System.Drawing.Color.FromArgb(185,208,139);
    public static System.Drawing.Color Green8 = System.Drawing.Color.FromArgb(175,201,122);
    public static System.Drawing.Color Green9 = System.Drawing.Color.FromArgb(165,194,106);
    public static System.Drawing.Color Green10 = System.Drawing.Color.FromArgb(155,187,89);
    public static System.Drawing.Color Green11 = System.Drawing.Color.FromArgb(139,168,80);
    public static System.Drawing.Color Green12 = System.Drawing.Color.FromArgb(124,150,71);
    public static System.Drawing.Color Green13 = System.Drawing.Color.FromArgb(108,131,62);
    public static System.Drawing.Color Green14 = System.Drawing.Color.FromArgb(93,112,53);
    public static System.Drawing.Color Green15 = System.Drawing.Color.FromArgb(77,93,44);
    public static System.Drawing.Color Green16 = System.Drawing.Color.FromArgb(62,75,36);
    public static System.Drawing.Color Green17 = System.Drawing.Color.FromArgb(46,56,27);
    public static System.Drawing.Color Green18 = System.Drawing.Color.FromArgb(31,37,18);
    public static System.Drawing.Color Green19 = System.Drawing.Color.FromArgb(15, 18, 9);
    public static System.Drawing.Color Heading1 = SpreadsheetHelper.Green12;
    public static System.Drawing.Color Heading2 = SpreadsheetHelper.Green10;
    public static System.Drawing.Color Heading3 = SpreadsheetHelper.Green8;
    public static System.Drawing.Color DividingBorderColor = SpreadsheetHelper.Green4;

    // Crave Colors (based on Crave Green)
    //public static System.Drawing.Color Green1 = System.Drawing.Color.FromArgb(240,249,234);
    //public static System.Drawing.Color Green2 = System.Drawing.Color.FromArgb(225,242,212);
    //public static System.Drawing.Color Green3 = System.Drawing.Color.FromArgb(210,236,191);
    //public static System.Drawing.Color Green4 = System.Drawing.Color.FromArgb(195,229,169);
    //public static System.Drawing.Color Green5 = System.Drawing.Color.FromArgb(180,223,148);
    //public static System.Drawing.Color Green6 = System.Drawing.Color.FromArgb(165,216,126);
    //public static System.Drawing.Color Green7 = System.Drawing.Color.FromArgb(150,210,105);
    //public static System.Drawing.Color Green8 = System.Drawing.Color.FromArgb(135,203,83);
    //public static System.Drawing.Color Green9 = System.Drawing.Color.FromArgb(120,197,62);
    //public static System.Drawing.Color Green10 = System.Drawing.Color.FromArgb(105,190,40); // Crave Green
    //public static System.Drawing.Color Green11 = System.Drawing.Color.FromArgb(94,171,36);
    //public static System.Drawing.Color Green12 = System.Drawing.Color.FromArgb(84,152,32);
    //public static System.Drawing.Color Green13 = System.Drawing.Color.FromArgb(73,133,28);
    //public static System.Drawing.Color Green14 = System.Drawing.Color.FromArgb(63,114,24);
    //public static System.Drawing.Color Green15 = System.Drawing.Color.FromArgb(52,95,20);
    //public static System.Drawing.Color Green16 = System.Drawing.Color.FromArgb(42,76,16);
    //public static System.Drawing.Color Green17 = System.Drawing.Color.FromArgb(31,57,12);
    //public static System.Drawing.Color Green18 = System.Drawing.Color.FromArgb(21,38,8);
    //public static System.Drawing.Color Green19 = System.Drawing.Color.FromArgb(10,19,4);
    public static string DecimalFormatCode = "#,##0.00";
    public static string IntegerFormatCode = "#,##0";

    public static void SetPageSizeOfWorksheets(this SLDocument spreadsheet, SLPaperSizeValues paperSize)
    {
        List<string> worksheetNames = spreadsheet.GetWorksheetNames();
        foreach (string worksheetName in worksheetNames)
        {
            SLPageSettings pageSettings = spreadsheet.GetPageSettings(worksheetName);
            pageSettings.PaperSize = paperSize;
            spreadsheet.SetPageSettings(pageSettings, worksheetName);
        }
    }

    public static bool TryParseCellReference(string cellReference, out Cell cell)
    {
        cell = new Cell();

        // Validate input
        if(cellReference.Length < 2) return false;
        if(!char.IsLetter(cellReference[0])) return false;

        // Parse
        bool toReturn = true;
        string columnName = string.Empty;
        string rowAsString = string.Empty;
        //bool completedLetters = false;
        foreach (char t in cellReference)
        {
            if(char.IsLetter(t)) 
            {
                columnName += t;
            }
            else if (char.IsNumber(t))
            {
                rowAsString += t;
            }
            else
            { 
                // Invalid charater
                toReturn = false;
                break;
            }
        }

        if (toReturn) cell = new Cell(SLConvert.ToColumnIndex(columnName), Convert.ToInt32(rowAsString));

        return toReturn;
    }

    public static void SetCellBackground(this SLDocument spreadsheet, System.Drawing.Color backgroundColor, bool bottomBorder, string cellReference)
    {
        spreadsheet.SetCellBackground(backgroundColor, bottomBorder, cellReference, cellReference);
    }

    public static void SetCellBackground(this SLDocument spreadsheet, System.Drawing.Color backgroundColor, bool bottomBorder, string startCellReference, string endCellreference)
    {
        var range = new CellRange(startCellReference, endCellreference);

        // Iterate over Columns first
        for (int iColumn = range.Start.Column; iColumn <= range.End.Column; iColumn++)
        {
            // Then rows
            for (int iRow = range.Start.Row; iRow < range.End.Row; iRow++)
            {
                // Fetch style, set properties, re-set Style
                SLStyle style = spreadsheet.GetCellStyle(iRow, iColumn);
                style.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, backgroundColor, backgroundColor);
                if (bottomBorder)
                {
                    style.Border.BottomBorder.BorderStyle = DocumentFormat.OpenXml.Spreadsheet.BorderStyleValues.Thin;
                    style.Border.BottomBorder.Color = SpreadsheetHelper.Green16;
                }
                spreadsheet.SetCellStyle(iRow, iColumn, style);
            }
        }
    }

    public static void SetCellStyleDecimal(this SLDocument spreadsheet, string cellReference)
    {
        SLStyle style = spreadsheet.GetCellStyle(cellReference);
        style.FormatCode = "#,##0.00";
        spreadsheet.SetCellStyle(cellReference, style);
    }

    public static void SetBorder(this SLDocument spreadsheet, System.Drawing.Color borderColor, Borders borders, int fromColumn, int toColumn, int fromRow, int toRow)
    {
        var cells = new CellRange(new Cell(fromColumn, fromRow), new Cell(toColumn, toRow));
        spreadsheet.SetStyle(cells, borders: borders, borderColor: borderColor);
    }

    public static void SetCellStyleVerticalAlign(this SLDocument spreadsheet, string cellReference, DocumentFormat.OpenXml.Spreadsheet.VerticalAlignmentValues alignment)
    {
        spreadsheet.SetStyle(new Cell(cellReference), verticalAlignment: alignment);
    }

    public static void SetStyle(this SLDocument spreadsheet, string cellReference, HorizontalAlignmentValues? horizontalAlignment = null, VerticalAlignmentValues? verticalAlignment = null, Color? textColor = null,
    Color? backgroundColor = null, Borders? borders = null, Color? borderColor = null, string formatCode = null, double? fontSize = null, BorderStyleValues borderStyle = BorderStyleValues.Thin, FontStyle fontStyle = FontStyle.Regular)
    {
        var c = new Cell(cellReference);                
        spreadsheet.SetStyle(c, horizontalAlignment, verticalAlignment, textColor, backgroundColor, borders, borderColor, formatCode, fontSize, borderStyle, fontStyle);
    }

    public static void SetStyle(this SLDocument spreadsheet, string startCellReference, string endCellreference, HorizontalAlignmentValues? horizontalAlignment = null, VerticalAlignmentValues? verticalAlignment = null, Color? textColor = null,
        Color? backgroundColor = null, Borders? borders = null, Color? borderColor = null, string formatCode = null, double? fontSize = null, BorderStyleValues borderStyle = BorderStyleValues.Thin, FontStyle fontStyle = FontStyle.Regular)
    {
        var cells = new CellRange(new Cell(startCellReference), new Cell(endCellreference));
        spreadsheet.SetStyle(cells, horizontalAlignment, verticalAlignment, textColor, backgroundColor, borders, borderColor, formatCode, fontSize, borderStyle, fontStyle);
    }

    public static void SetStyle(this SLDocument spreadsheet, CellRange cells, HorizontalAlignmentValues? horizontalAlignment = null, VerticalAlignmentValues? verticalAlignment = null, Color? textColor = null,
        Color? backgroundColor = null, Borders? borders = null, Color? borderColor = null, string formatCode = null, double? fontSize = null, BorderStyleValues borderStyle = BorderStyleValues.Thin, FontStyle fontStyle = FontStyle.Regular)
    {
        for (int iColumn = cells.Start.Column; iColumn <= cells.End.Column; iColumn++)
        {
            for (int iRow = cells.Start.Row; iRow <= cells.End.Row; iRow++)
            {
                spreadsheet.SetStyle(new Cell(iColumn, iRow), horizontalAlignment, verticalAlignment, textColor, backgroundColor, borders, borderColor, formatCode, fontSize, borderStyle, fontStyle);
                spreadsheet.GetCellStyle(iRow, iColumn);
            }
        }
    }

    public static void SetStyle(this SLDocument spreadsheet, Cell cell, HorizontalAlignmentValues? horizontalAlignment = null, VerticalAlignmentValues? verticalAlignment = null, Color? textColor = null,
        Color? backgroundColor = null, Borders? borders = null, Color? borderColor = null, string formatCode = null, double? fontSize = null, BorderStyleValues borderStyle = BorderStyleValues.Thin, FontStyle fontStyle = FontStyle.Regular)
    {
        // Fetch style, set properties, re-set Style
        SLStyle style = spreadsheet.GetCellStyle(cell.ToCellReference());

        if (horizontalAlignment.HasValue) style.SetHorizontalAlignment(horizontalAlignment.Value);
        if (verticalAlignment.HasValue) style.SetVerticalAlignment(verticalAlignment.Value);
        if (textColor.HasValue) style.SetFontColor(textColor.Value);
        if (!formatCode.IsNullOrWhiteSpace()) style.FormatCode = formatCode;
        if(backgroundColor.HasValue) style.Fill.SetPattern(DocumentFormat.OpenXml.Spreadsheet.PatternValues.Solid, backgroundColor.Value, backgroundColor.Value);
        style.Font.FontSize = fontSize;

        style.Font.Bold = fontStyle.HasFlag(FontStyle.Bold);
        style.Font.Italic = fontStyle.HasFlag(FontStyle.Italic);
        style.Font.Strike = fontStyle.HasFlag(FontStyle.Strikeout);
        if (fontStyle.HasFlag(FontStyle.Underline))
        {
            style.Font.Underline = UnderlineValues.Single;
        }

        if (borders.HasValue)
        {
            if (!borderColor.HasValue) borderColor = System.Drawing.Color.Black;
            if (borders.HasFlag(Borders.Top))
            {
                style.Border.TopBorder.BorderStyle = borderStyle;
                style.Border.TopBorder.Color = borderColor.Value;
            }

            if (borders.HasFlag(Borders.Right))
            {
                style.Border.RightBorder.BorderStyle = borderStyle;
                style.Border.RightBorder.Color = borderColor.Value;
            }

            if (borders.HasFlag(Borders.Bottom))
            {
                style.Border.BottomBorder.BorderStyle = borderStyle;
                style.Border.BottomBorder.Color = borderColor.Value;
            }

            if (borders.HasFlag(Borders.Left))
            {
                style.Border.LeftBorder.BorderStyle = borderStyle;
                style.Border.LeftBorder.Color = borderColor.Value;
            }
        }

        spreadsheet.SetCellStyle(cell.Row, cell.Column, style);
    }

    public static void SetCellBackground(this SLDocument spreadsheet, int indent, bool bottomBorder, string startCellReference, string endCellreference)
    {        
        System.Drawing.Color c = SpreadsheetHelper.GetColorForIndent(indent);
        spreadsheet.SetStyle(startCellReference, endCellreference, backgroundColor: c);
    }

    public static void SetColumnStyle(this SLDocument spreadsheet, int column, HorizontalAlignmentValues? horizontalAlignment = null)
    {
        spreadsheet.SetColumnStyle(column, column, horizontalAlignment);
    }

    public static void SetColumnStyle(this SLDocument spreadsheet, int columnStart, int columnEnd, HorizontalAlignmentValues? horizontalAlignment = null)
    {
        var style = new SLStyle();
        if (horizontalAlignment.HasValue) style.SetHorizontalAlignment(horizontalAlignment.Value);

        spreadsheet.SetColumnStyle(columnStart, columnEnd, style);
    }

    public static System.Drawing.Color GetColorForIndent(int indent)
    {
        switch (indent)
        {
            case 0:
                return SpreadsheetHelper.Green10;
            case 1:
                return SpreadsheetHelper.Green8;
            case 2:
                return SpreadsheetHelper.Green6;
            case 3:
                return SpreadsheetHelper.Green4;
            case 4:
                return SpreadsheetHelper.Green2;         
            default:
                return SpreadsheetHelper.Green1;                            
        }
    }

    public struct Cell
    {
        public Cell(string cellReference)
        {
            Cell c;
            if (!SpreadsheetHelper.TryParseCellReference(cellReference, out c))
            {
                throw new ArgumentException("cellReference is wrong: " + cellReference);
            }
            this.Column = c.Column;
            this.Row = c.Row;
        }

        public Cell(int column, int row)
        {
            this.Column = column;
            this.Row = row;
        }

        public int Column;
        public int Row;

        public string ToCellReference()
        {
            return SLConvert.ToColumnName(this.Column) + this.Row;
        }
    }

    public struct CellRange
    {
        public CellRange(string startCellReference, string endCellReference)
        {
            Cell start, end;
            if (!SpreadsheetHelper.TryParseCellReference(startCellReference, out start) ||
                !SpreadsheetHelper.TryParseCellReference(endCellReference, out end))
            {
                throw new Exception("Invalid cell reference(s): {0} or {1}".FormatSafe(startCellReference, endCellReference));
            }

            this.Start = start;
            this.End = end;
        }

        public CellRange(Cell start, Cell end)
        {
            this.Start = start;
            this.End = end;
        }

        public Cell Start;
        public Cell End;
    }
}