﻿using Dionysos.Diagnostics;
using Dionysos.Web;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Analytics
{
    public static class EventHubUtil
    {
        /// <summary>
        /// Get a Shared Access Signature token to be used to submit message to an Event Hub on Azure
        /// </summary>
        /// <param name="publisherId">The unique ID of the publisher, i.e. it's MAC Address (optional, for external publishers only)</param>
        /// <param name="connectionString">Connection String (optional, when not provided / null is retrieved from AppSettings)</param>
        /// <param name="expireInDays">Specific expiry of the token, defaults to 5 years. (optional)</param>
        /// <returns>Complete Shared Access Signature Token, i.e. 'SharedAccessSignature sr=[EncodedHttpsEndpoint]&sig=[SASSignature]&se=[Expiry Epoch]&skn=[Shared Access Key Name]' </returns>
        public static string GenerateSharedAccessSignatureToken(string publisherId = null, string connectionStringText = null, int? expireInDays = null)
        {
            EventHubConnectionString connectionStringObject;
            connectionStringObject = connectionStringText == null ? EventHubUtil.GetConnectionStringFromAppSettings() : new EventHubConnectionString(connectionStringText);

            return GenerateSharedAccessSignatureToken(publisherId, connectionStringObject, expireInDays);
        }

        /// <summary>
        /// Get a Shared Access Signature token to be used to submit message to an Event Hub on Azure
        /// </summary>
        /// <param name="publisherId">The unique ID of the publisher, i.e. it's MAC Address (optional, for external publishers only)</param>
        /// <param name="connectionString">Connection String of the Event Hub</param>
        /// <param name="expireInDays">Specific expiry of the token, defaults to 5 years. (optional)</param>
        /// <returns>Complete Shared Access Signature Token, i.e. 'SharedAccessSignature sr=[EncodedHttpsEndpoint]&sig=[SASSignature]&se=[Expiry Epoch]&skn=[Shared Access Key Name]' </returns>
        public static string GenerateSharedAccessSignatureToken(string publisherId, EventHubConnectionString connectionStringObject, int? expireInDays = null)
        {
            if (connectionStringObject == null)
                connectionStringObject = EventHubUtil.GetConnectionStringFromAppSettings();

            // Arrange expiration
            expireInDays = expireInDays.HasValue ? expireInDays.Value : (5 * 365); // Default to 5 years            
            TimeSpan fromEpochStart = DateTime.UtcNow - new DateTime(1970, 1, 1);
            string expiry = Convert.ToString((int)fromEpochStart.TotalSeconds + TimeSpan.FromDays(expireInDays.Value).TotalSeconds);

            string encodedHttpsEndpoint = WebUtility.UrlEncode(connectionStringObject.GetHttpsEndPoint(publisherId)).ToLower();

            string stringToSign = encodedHttpsEndpoint + "\n" + expiry;

            HMACSHA256 hmac = new HMACSHA256(Encoding.UTF8.GetBytes(connectionStringObject.SharedAccessKey));
            var signature = WebUtility.UrlEncode(Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(stringToSign))));

            return String.Format(CultureInfo.InvariantCulture, "SharedAccessSignature sr={0}&sig={1}&se={2}&skn={3}",
                            encodedHttpsEndpoint, signature, expiry, connectionStringObject.SharedAccessKeyName);
        }

        /// <summary>
        /// Publish a message to an Event Hub
        /// </summary>
        /// <param name="message">Message to be published (most probably JSON)</param>
        /// <param name="publisherId">The unique ID of the publisher, i.e. it's MAC Address (optional, for external publishers only)</param>
        /// <param name="connectionString">Connection String (optional, when not provided / null is retrieved from AppSettings)</param>
        /// <param name="expireInDays">Specific expiry of the token, defaults to 5 years. (optional)</param>
        public static void PublishMessage(string message, string publisherId = null, string connectionStringText = null, int ? expireInDays = null)
        {
            EventHubConnectionString connectionStringObject;
            connectionStringObject = connectionStringText == null ? EventHubUtil.GetConnectionStringFromAppSettings() : new EventHubConnectionString(connectionStringText);

            EventHubUtil.PublishMessage(message, publisherId, connectionStringObject, expireInDays);
        }

        /// <summary>
        /// Publish a message to an Event Hub
        /// </summary>
        /// <param name="message">Message to be published (most probably JSON)</param>
        /// <param name="connectionString">Connection String</param>
        /// <param name="publisherId">The unique ID of the publisher, i.e. it's MAC Address (optional, for external publishers only)</param>
        /// <param name="expireInDays">Specific expiry of the token, defaults to 5 years. (optional)</param>
        public static void PublishMessage(string message, string publisherId, EventHubConnectionString connectionStringObject, int? expireInDays = null)
        {
            // https://blog.davemdavis.net/2015/08/18/azure-event-hub-rest-api/

            if (connectionStringObject == null)
                connectionStringObject = EventHubUtil.GetConnectionStringFromAppSettings();

            string token = EventHubUtil.GenerateSharedAccessSignatureToken(publisherId, connectionStringObject);
            Uri endPointUri = new Uri(connectionStringObject.GetHttpsEndPoint(publisherId));
            try
            {
                using (var client = new HttpClient())
                {
                    // Prepare the client
                    client.BaseAddress = new Uri(string.Format("{0}://{1}", endPointUri.Scheme, endPointUri.Authority));
                    client.DefaultRequestHeaders.Accept.Clear();

                    // Remove the 'SharedAccessSignature ' part of the token, as the header is already named and this would
                    // cause it to appear twice.
                    token = token.Replace("SharedAccessSignature ", string.Empty);
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("SharedAccessSignature", token);

                    // A static example message converted to HttpContent                    
                    HttpContent content = new StringContent(message, Encoding.UTF8);
                    content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

                    // Post it
                    var response = client.PostAsync(endPointUri.AbsolutePath + "/messages?timeout=60&api-version=2014-01", content).Result;

                    // Check the response
                    if (response.IsSuccessStatusCode)
                    {
                        Debug.WriteLine("Happy Panda, posted to: " + endPointUri + "/messages?timeout=60&api-version=2014-01");
                    }
                    else
                    {
                        Debug.WriteLine("Sad Panda: " + response.StatusCode);
                        throw new Exception(string.Format("Failed to send message, status code: '{0}'", response.StatusCode));
                    }
                }
            }
            catch (Exception exception)
            {
                Debug.WriteLine("Exception: " + exception.Message);
                throw new Exception("Failed to publish message to Event Hub: " + exception.Message, exception);
            }
        }

        private const string ConnectionStringCacheKey = "EventHubUtil.ConnectionStringCacheKey";

        public static EventHubConnectionString GetConnectionStringFromAppSettings()
        {
            EventHubConnectionString connectionStringObject;
            if (!CacheHelper.TryGetValue(ConnectionStringCacheKey, false, out connectionStringObject))
            {
                string connectionStringText = ConfigurationManager.AppSettings["CraveAnalyticsEventHubConnectionString"] ?? "App Setting 'CraveAnalyticsEventHubConnectionString' Not Found";
                connectionStringObject = new EventHubConnectionString(connectionStringText);
                CacheHelper.Add(false, ConnectionStringCacheKey, connectionStringObject);
            }

            return connectionStringObject;
        }
    }
}
