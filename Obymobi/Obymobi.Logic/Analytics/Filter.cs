﻿using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Logic.Analytics
{
    /// <summary>
    /// Summary description for Filter
    /// </summary>
    [Serializable]
    public class Filter
    {
        public String Name { get; set; }
        public DateTime FromUtc { get; set; }
        public DateTime TillUtc { get; set; }        
        public int CompanyId { get; set; }
        public bool IncludeInRoomTablets { get; set; }
        public bool IncludeByodApps { get; set; }
        public bool IncludeFailedOrders { get; set; }
        public List<int> DeliverypointIds { get; set; }
        public List<int> DeliverypointgroupIds { get; set; }
        public bool DeliverypointsExclude { get; set; }
        public List<OrderStatus> OrderStatuses { get; set; }
        public int ProductId { get; set; }
        public List<int> ProductIdsFromAlterationProducts { get; set; }

        public Filter()
        {
            this.DeliverypointgroupIds = null;
            this.DeliverypointIds = null;
            this.IncludeInRoomTablets = true;
            this.IncludeByodApps = true;
            this.IncludeFailedOrders = false;
            this.DeliverypointsExclude = false;
            this.ProductId = -1;
        }

        public Filter(DateTime fromUtc, DateTime tillUtc, int companyId):this()
        {
            this.FromUtc = fromUtc;
            this.TillUtc = tillUtc;
            this.CompanyId = companyId;
        }

        public ReportProcessingTaskEntity CreateReportProcessingTask()
        {
            ReportProcessingTaskEntity toReturn = new ReportProcessingTaskEntity();
            toReturn.CompanyId = this.CompanyId;
            toReturn.FromUTC = this.FromUtc;
            toReturn.TillUTC = this.TillUtc;
            toReturn.Filter = Obymobi.Logic.HelperClasses.XmlHelper.Serialize(this, true);
            return toReturn;
        }

        private string timeZoneOlsonId = string.Empty;
        public string TimeZoneOlsonId
        {
            get
            {
                return this.timeZoneOlsonId;
            }
            set
            {
                this.timeZoneOlsonId = value;                
            }
        }

        public DateTime FromTimeZoned
        {
            get
            {
                return TimeZoneHelper.ConvertFromUtc(this.FromUtc, this.TimeZoneOlsonId);
            }
        }

        public DateTime TillTimeZoned
        {
            get
            {
                return TimeZoneHelper.ConvertFromUtc(this.TillUtc, this.TimeZoneOlsonId);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormatLine("Company: {0} ({1}),", new CompanyEntity(this.CompanyId).Name, this.CompanyId);

            sb.AppendFormatLine("From - Till (Utc): {0} - {1},", this.FromUtc.ToString(), this.TillUtc.ToString());

            if (!this.TimeZoneOlsonId.IsNullOrWhiteSpace())
            {
                TimeZoneInfo timeZoneInfo = TimeZoneHelper.GetTimeZoneInfo(this.TimeZoneOlsonId);
                DateTime fromTimeZoned = this.FromUtc.UtcToLocalTime(timeZoneInfo);
                DateTime tillTimeZoned = this.TillUtc.UtcToLocalTime(timeZoneInfo);
                sb.AppendFormatLine("From - Till ({0}): {1} - {2},", this.TimeZoneOlsonId, fromTimeZoned, tillTimeZoned);
            }            

            sb.AppendFormatLine("Include Byod Apps: {0},", this.IncludeByodApps);
            
            sb.AppendFormatLine("Include In-Room Tablets: {0},", this.IncludeInRoomTablets);           

            sb.AppendFormatLine("Include Failed Orders: {0},", this.IncludeFailedOrders);

            if (this.DeliverypointIds == null || this.DeliverypointIds.Count == 0)
            {
                sb.AppendFormatLine("Include Deliverypoints: All");
            }
            else
            {
                PredicateExpression filter;
                if (this.DeliverypointsExclude)
                {
                    sb.AppendFormat("Excluded Deliverypoints: ");
                    filter = new PredicateExpression(DeliverypointFields.DeliverypointId == this.DeliverypointIds);
                    filter.Add(DeliverypointFields.CompanyId == this.CompanyId); // This could become quite a list... As it would return every DP of every Company.
                }
                else
                {
                    sb.AppendFormat("Included Deliverypoints: ");
                    filter = new PredicateExpression(DeliverypointFields.DeliverypointId == this.DeliverypointIds);
                }
                
                DeliverypointCollection deliverypoints = new DeliverypointCollection();
                
                SortExpression sort = new SortExpression(DeliverypointFields.Number | SortOperator.Ascending);
                deliverypoints.GetMulti(filter, 0, sort);

                List<int> deliverypointIdsCopy = new List<int>(this.DeliverypointIds);

                foreach (DeliverypointEntity dp in deliverypoints)
                {
                    // Keep track which DP's we found
                    if (deliverypointIdsCopy.Contains(dp.DeliverypointId)) deliverypointIdsCopy.Remove(dp.DeliverypointId);
                    sb.AppendFormat("{0} ({1}), ", dp.Number, dp.DeliverypointId);
                }

                if (deliverypointIdsCopy.Count > 0)
                {
                    sb.AppendFormat("Unknown DeliveypointIds: ");
                    foreach (int dpid in deliverypointIdsCopy) sb.AppendFormat("{0}, ", dpid);
                }
                sb.AppendFormat("\r\n");
            }

            if (this.DeliverypointgroupIds == null || this.DeliverypointgroupIds.Count == 0)
            {
                sb.AppendFormatLine("Include Deliverypointgroups: All");
            }
            else
            {
                sb.AppendFormat("Include Deliverypointgroups: ");
                DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
                PredicateExpression filter = new PredicateExpression(DeliverypointgroupFields.DeliverypointgroupId == this.DeliverypointgroupIds);
                SortExpression sort = new SortExpression(DeliverypointgroupFields.Name | SortOperator.Ascending);
                deliverypointgroups.GetMulti(filter, 0, sort);

                List<int> deliverypointgroupIdsCopy = new List<int>(this.DeliverypointgroupIds);

                foreach (DeliverypointgroupEntity dpg in deliverypointgroups)
                { 
                    // Keep track which DPG's we found
                    if (deliverypointgroupIdsCopy.Contains(dpg.DeliverypointgroupId)) deliverypointgroupIdsCopy.Remove(dpg.DeliverypointgroupId);
                    sb.AppendFormat("{0} ({1}), ", dpg.Name, dpg.DeliverypointgroupId);
                }

                if (deliverypointgroupIdsCopy.Count > 0)
                {
                    sb.AppendFormat("Unknown DeliveypointIds: ");
                    foreach (int dpgid in deliverypointgroupIdsCopy) sb.AppendFormat("{0}, ", dpgid);
                }                

                sb.AppendFormat("\r\n");
            }

            return sb.ToString();
        }

        [System.Xml.Serialization.XmlAnyAttributeAttribute()]
        public System.Xml.XmlAttribute[] AnyAttribute;

        [System.Xml.Serialization.XmlAnyElement()]
        public System.Xml.XmlElement[] AnyElement;
    }
}