﻿using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Internal.Implementations;

namespace Obymobi.Logic
{
    public class DevExpressAppointment : AppointmentInstance
    {
        public DevExpressAppointment() : base()
        {
            // Leave this ctor alone!
            // Is needed by MarketingAppointment.cs to create an "exception" appointment
        }

        public DevExpressAppointment(AppointmentType type) : base(type)
        {

        }
    }
}