﻿using Obymobi.Attributes;
using Obymobi.Enums;
using Obymobi.Logic;
using Obymobi.Logic.Model;
using System.Collections.Generic;

public interface IWebserviceHandler
{
    #region GET methods

    bool IsWebserviceAlive();

    bool IsDatabaseAlive();

    string GetReleaseAndDownloadLocation(string username, string password, int companyId, string applicationCode);

    ObyTypedResult<Client> GetClients(long timestamp, string macAddress, string hash); // OSS

    [IncludeInCodeGeneratorForFlex]
    [ClientLocalStorageFlex]
    ObyTypedResult<Client> GetClient(long timestamp, string macAddress, int clientId, string clientmacAddress, int deliverypointgroupId, int deliverypointId, string applicationVersion, string osVersion, int deviceType, string hash);

    [IncludeInCodeGeneratorForAndroid]
    [ClientLocalStorageAndroid]
    ObyTypedResult<Client> GetClient(long timestamp, string macAddress, int clientId, string clientmacAddress, int deliverypointgroupId, int deliverypointId, string applicationVersion, string osVersion, int deviceType, int? deviceModel, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Company> GetCompanies(long timestamp, string username, string password, string hash);

    [IncludeInCodeGeneratorForAndroid]
    [ClientLocalStorageAndroid]
    [IncludeInCodeGeneratorForFlex]
    [ClientLocalStorageFlex]
    ObyTypedResult<Company> GetCompany(long timestamp, string macAddress, int companyId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    [ClientLocalStorageAndroid]
    [IncludeInCodeGeneratorForFlex]
    [ClientLocalStorageFlex]
    ObyTypedResult<Advertisement> GetAdvertisements(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    [ClientLocalStorageAndroid]
    [IncludeInCodeGeneratorForFlex]
    [ClientLocalStorageFlex]
    ObyTypedResult<Entertainment> GetEntertainment(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForFlex]
    [ClientLocalStorageFlex]
    ObyTypedResult<Paymentmethod> GetPaymentmethods(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    [ClientLocalStorageAndroid]
    [IncludeInCodeGeneratorForFlex]
    [ClientLocalStorageFlex]
    [IncludeInCodeGeneratorForMobile]
    ObyTypedResult<ProductmenuItem> GetMenu(long timestamp, string macAddress, int deliverypointgroupId, bool applyMarkup, string hash);

    [IncludeInCodeGeneratorForFlex]
    [ClientLocalStorageFlex]
    ObyTypedResult<Survey> GetSurveys(long timestamp, string macAddress, string hash);

    [ClientLocalStorageAndroid]
    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Announcement> GetAnnouncements(long timestamp, string macAddress, string hash);

    [ClientLocalStorageAndroid]
    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Deliverypoint> GetDeliverypoints(long timestamp, string macAddress, int deliverypointgroupId, string hash);

    [ClientLocalStorageAndroid]
    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Terminal> GetTerminal(long timestamp, string macAddress, int terminalId, string applicationVersion, string osVersion, int deviceType, string hash);

    ObyTypedResult<PosIntegrationInformation> GetPosIntegrationInformation(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<ClientStatus> GetSetClientStatus(long timestamp, string macAddress, ClientStatus clientStatus, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<TerminalStatus> GetSetTerminalStatus(long timestamp, string macAddress, TerminalStatus terminalStatus, string hash); 

    ObyTypedResult<Product> GetProducts(long timestamp, string macAddress, string hash);

    ObyTypedResult<Category> GetCategories(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Terminal> GetTerminals(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Order> GetOrders(long timestamp, string macAddress, int statusCode, string statusMessage, string version, bool isConsole, bool isMasterConsole, string hash);

    ObyTypedResult<TerminalOperationMode> GetTerminalOperationMode(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> GetPmsFolio(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> GetPmsGuestInformation(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> GetPmsExpressCheckout(long timestamp, string macAddress, decimal balance, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Order> GetOrderHistory(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Obymobi.Logic.Model.Analytics> GetAnalytics(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Netmessage> GetNetmessages(long timestamp, string macAddress, int customerId, int clientId, int companyId, int deliverypointId, int terminalId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Site> GetSite(long timestamp, string macAddress, int siteId, int deviceType, string languageCode, string cultureCode, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Company> GetPointOfInterest(long timestamp, string macAddress, int pointOfInterestId, int deviceTypeInt, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> GetSiteTicks(long timestamp, string macAddress, int siteId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> GetPointOfInterestTicks(long timestamp, string macAddress, int pointOfInterestId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<RoomControlConfiguration> GetRoomControlConfiguration(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> GetEstimatedDeliveryTime(long timestamp, string macAddress, int deliverypointgroupId, string hash); 

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<UIScheduleItem> GetUISchedule(long timestamp, string macAddress, int deliverypointgroupId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<DeliveryTime> GetEstimatedDeliveryTimes(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<PriceSchedule> GetPriceSchedule(long timestamp, string macAddress, int priceScheduleId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<CustomText> GetTranslations(long timestamp, string macAddress, string cultureCode, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Messagegroup> GetPmsMessagegroups(long timestamp, string macAddress, int messagegroupType, string date, string groupName, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<GuestGroup> GetGuestGroups(long timestamp, string macAddress, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Map> GetMap(long timestamp, string macAddress, int mapId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> GetMapTimestamp(long timestamp, string macAddress, int mapId, string hash);

    #endregion

    #region SET methods

    [IncludeInCodeGeneratorForAndroid]
    [IncludeInCodeGeneratorForFlex]
    ObyTypedResult<ClientStatus> GetClientStatuses(long timestamp, string macAddress, string hash);

    ObyTypedResult<SimpleWebserviceResult> SetClientStatuses(long timestamp, string macAddress, ClientStatus[] clientStatuses, string hash);

    [IncludeInCodeGeneratorForAndroid]
    [IncludeInCodeGeneratorForFlex]
    ObyTypedResult<Order> SaveOrder(long timestamp, string macAddress, Order order, string hash);

    ObyTypedResult<Posorder> CreatePosorder(long timestamp, string macAddress, Order order, string hash);

    [IncludeInCodeGeneratorForFlex]
    ObyTypedResult<SurveyResult> SaveSurveyResults(long timestamp, string macAddress, SurveyResult results, string hash);

    ObyTypedResult<Rating> SaveRatings(long timestamp, string macAddress, Rating[] ratings, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<ClientEntertainment> SaveClientEntertainment(long timestamp, string macAddress, ClientEntertainment[] clientEntertainments, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SaveClientLog(long timestamp, string macAddress, int logType, string log, long fileDate, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SaveClientLogZipped(long timestamp, string macAddress, int logType, string base64Zip, long fileDate, string hash);

    ObyTypedResult<Posproduct> SetPosProducts(long timestamp, string macAddress, long batchId, Posproduct[] posproducts, string hash);

    ObyTypedResult<Poscategory> SetPosCategories(long timestamp, string macAddress, long batchId, Poscategory[] poscategories, string hash);

    ObyTypedResult<Posdeliverypointgroup> SetPosDeliverypointgroups(long timestamp, string macAddress, long batchId, Posdeliverypointgroup[] posdeliverypointgroups, string hash);

    ObyTypedResult<Posdeliverypoint> SetPosDeliverypoints(long timestamp, string macAddress, long batchId, Posdeliverypoint[] posdeliverypoints, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<OrderRoutestephandlerSaveStatus> SetRoutestephandlerSaveStatuses(long timestamp, string macAddress, OrderRoutestephandlerSaveStatus[] orderSaveStatuses, string hash);

    ObyTypedResult<TerminalOperationMode> SetTerminalOperationMode(long timestamp, string macAddress, int terminalOperationMode, string hash);

    ObyTypedResult<TerminalOperationState> SetTerminalOperationState(long timestamp, string macAddress, int terminalOperationState, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SetClientDeliverypoint(long timestamp, string macAddress, int deliverypointId, int deliverypointgroupId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SendMessage(long timestamp, string macAddress, string deliverypointNumbers, string deliverypointgroupIds, int clientId, int orderId, string title, string message, int duration, int mediaId, int categoryId, int entertainmentId, int productId, bool urgent, bool saveMessage, bool mobileClients, string messagegroupIds, int messageLayoutType, Action[] actions, string hash);

    ObyTypedResult<SimpleWebserviceResult> SetPmsFolio(long timestamp, string macAddress, Folio folio, string hash);

    ObyTypedResult<SimpleWebserviceResult> SetPmsGuestInformation(long timestamp, string macAddress, GuestInformation guestInformation, string hash);

    ObyTypedResult<SimpleWebserviceResult> SetPmsCheckedOut(long timestamp, string macAddress, int deliverypointId, string deliverypointNumber, string hash);

    ObyTypedResult<SimpleWebserviceResult> SetPmsCheckedIn(long timestamp, string macAddress, GuestInformation guestInformation, string hash);

    ObyTypedResult<SimpleWebserviceResult> SetPmsExpressCheckedOut(long timestamp, string macAddress, Checkout checkoutInfo, string hash);

    ObyTypedResult<SimpleWebserviceResult> SetPmsWakeUp(long timestamp, string macAddress, WakeUpStatus wakeUpStatus, string hash);

    ObyTypedResult<SimpleWebserviceResult> ClearPmsWakeUp(long timestamp, string macAddress, WakeUpStatus wakeUpStatus, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SetClientWakeUpTime(long timestamp, string macAddress, string wakeUpTime, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SetForwardTerminal(long timestamp, string macAddress, int forwardToTerminalId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SetApplicationVersion(long timestamp, string macAddress, string applicationCode, string versionNumber, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SetAgentCommandReponse(long timestamp, string macAddress, string resultMessage, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SetEstimatedDeliveryTime(long timestamp, string macAddress, int deliverypointgroupId, int estimatedDeliveryTime, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> UpdateUIWidget(long timestamp, string macAddress, UIWidget uiWidget, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> UpdateAvailability(long timestamp, string macAddress, Availability availability, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SaveGuestInformations(long timestamp, string macAddress, GuestInformation[] guestInformations, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SaveFolios(long timestamp, string macAddress, Folio[] folios, string hash);

    #endregion

    #region Misc methods

    ObyTypedResult<SimpleWebserviceResult> VerifyTerminal(long timestamp, string macAddress, int terminalId, string hash);

    ObyTypedResult<SimpleWebserviceResult> CleanUpPosData(long timestamp, string macAddress, string hash);

    ObyTypedResult<SimpleWebserviceResult> WriteToLog(long timestamp, string macAddress, int terminalId, int orderId, string orderGuid, int type, string message, string status, string log, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> WriteToLogZipped(long timestamp, string macAddress, int terminalId, int type, string message, string base64Zip, long fileDate, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> PrintFile();

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> EmailFile();

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> RemoveMessages(long timestamp, string macAddress, string messageIds, int dialogCloseResult, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Message> GetMessages(long timestamp, string macAddress, int deliverypointId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    [IncludeInCodeGeneratorForFlex]
    ObyTypedResult<SimpleWebserviceResult> ProcessOrders(long timestamp, string macAddress, int clientId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> CancelOrder(long timestamp, string macAddress, int clientId, int customerId, string orderGuid, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SetOrderPrinted(long timestamp, string macAddress, int orderId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SetMobileOrdering(long timestamp, string macAddress, bool enabled, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> UpdateLastRequest(long timestamp, string macAddress, int clientId, int terminalId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<Release> GetRelease(long timestamp, string macAddress, string applicationCode, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SetUpdateStatus(long timestamp, string macAddress, string applicationCode, int updateStatus, string customMessage, int releaseId, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> VerifyNetmessageByGuid(long timestamp, string macAddress, string messageGuid, bool isCompleted, string errorMessage, string trace, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> DailyOrderReset(long timestamp, string macAddress, string deliverypointNumbers, string deliverypointgroupIds, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> SendClearMessages(long timestamp, string macAddress, string deliverypointNumbers, string deliverypointgroupIds, string messageGroupIds, string hash);

    [IncludeInCodeGeneratorForAndroid]
    ObyTypedResult<SimpleWebserviceResult> RegisterGameSession(long timestamp, string macAddress, string url, string hash);

    #endregion
}