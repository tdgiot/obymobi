﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Logic.Extensions
{
    public static class CompanyEntityExtensions
    {
        public static CultureInfo GetDefaultCompanyCulture(this CompanyEntity companyEntity)
        {
            string defaultCompanyCultureCode = companyEntity.CultureCode;
            return CultureInfo.GetCultureInfo(defaultCompanyCultureCode);
        }

        public static IEnumerable<CultureInfo> GetCompanyCultures(this CompanyEntity companyEntity)
        {
            IEnumerable<string> companyTranslationCultureCodes = companyEntity.CompanyCultureCollection.Select(c => c.CultureCode);
            return companyTranslationCultureCodes.Select(c => CultureInfo.GetCultureInfo(c));
        }
    }
}
