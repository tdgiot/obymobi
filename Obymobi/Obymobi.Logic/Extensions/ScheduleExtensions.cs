﻿using System;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Logic
{
    public static class ScheduleExtensions
    {
        public static DateTime TimeStartAsDateTime(this ScheduleitemEntity scheduleitem)
        {
            return GetDateTimeForTimeString(scheduleitem.TimeStart);
        }

        public static DateTime TimeEndAsDateTime(this ScheduleitemEntity scheduleitem)
        {
            return GetDateTimeForTimeString(scheduleitem.TimeEnd);
        }

        private static DateTime GetDateTimeForTimeString(string time)
        {
            int hours = Convert.ToInt32(time.Substring(0, 2));
            int minutes = Convert.ToInt32(time.Substring(2, 2));

            DateTime toReturn = new DateTime(2000, 1, 1, hours, minutes, 0);

            return toReturn;
        }
    }
}
