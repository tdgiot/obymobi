﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Dionysos.Printing;
using Obymobi.Constants;

namespace Obymobi.Logic.Status
{
    /// <summary>
    /// OnsiteServerStatus class
    /// </summary>
    public class OnsiteServerStatus : CloudStatus
    {
        #region Fields

        private bool printernameSpecified = false;
        private bool printernameValid = false;
        private string printerError = string.Empty;
        private bool printer = false;
        private string[] printerNames = null;

        private bool checkPrinter = true;
        private bool internet = false;
        private bool webservice = false;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets if the webservice is online
        /// </summary>
        public bool Webservice
        {
            get
            {
                return this.webservice;
            }
            set
            {
                this.webservice = value;
            }
        }

        /// <summary>
        /// Gets or sets if the Internet is online ;) (Or should we save if we have a connection?)
        /// </summary>
        public bool Internet
        {
            get
            {
                return this.internet;
            }
            set
            {
                this.internet = value;
            }
        }


        /// <summary>
        /// Gets or sets the printer name of the terminal
        /// </summary>
        public string[] Printernames
        {
            get
            {
                return this.printerNames;
            }
            set
            {
                bool changed = (this.printerNames != value);
                this.printerNames = value;
                if (changed)
                    this.OnPrinternameChanged();
            }
        }

        /// <summary>
        /// Gets or sets the flag which indicates whether the printername is specified in the configuration
        /// </summary>
        public bool PrinternameSpecified
        {
            get
            {
                return this.printernameSpecified;
            }
            set
            {
                bool changed = (this.printernameSpecified != value);
                this.printernameSpecified = value;
                if (changed)
                    this.OnPrinternameSpecifiedChanged();
            }
        }

        /// <summary>
        /// Gets or sets the flag which indicates whether the printername is valid
        /// </summary>
        public bool PrinternameValid
        {
            get
            {
                return this.printernameValid;
            }
            set
            {
                bool changed = (this.printernameValid != value);
                this.printernameValid = value;
                if (changed)
                    this.OnPrinternameValidChanged();
            }
        }

        /// <summary>
        /// Gets or sets the string containing the printer error
        /// </summary>
        public string PrinterError
        {
            get
            {
                return this.printerError;
            }
            set
            {
                bool changed = (this.printerError != value);
                this.printerError = value;
                if (changed)
                    this.OnPrinterErrorChanged();
            }
        }

        /// <summary>
        /// Gets a flag which indicates whether the printer is configured correctly
        /// </summary>
        public bool Printer
        {
            get
            {
                if (this.checkPrinter)
                {
                    this.CheckPrinter();

                    bool changed = (this.printer != (this.PrinternameSpecified && this.PrinternameValid && (this.PrinterError.Length == 0)));
                    this.printer = this.PrinternameSpecified && this.PrinternameValid && (this.PrinterError.Length == 0);
                    if (changed)
                        this.OnPrinterChanged();
                }
                else
                    this.printer = true;

                return this.printer;
            }
        }        

        #endregion

        #region Methods

        public override void Refresh()
        {
            this.Refresh(true);
        }

        /// <summary>
        /// Refreshes the specified check printer.
        /// </summary>
        /// <param name="checkPrinter">if set to <c>true</c> [check printer].</param>
        public void Refresh(bool checkPrinter)
        {
            base.Refresh();

            this.checkPrinter = checkPrinter;

            if (this.checkPrinter)
            {
                this.CheckPrinter();
            }
        }

        /// <summary>
        /// Checks whether the printer is online
        /// </summary>
        public void CheckPrinter()
        {
            if (this.Printernames == null)
                return;

            foreach (string printer in this.printerNames)
            {
                this.PrinternameSpecified = (printer.Trim().Length > 0);
                this.OnPrinternameChecked();

                try
                {
                    if (this.PrinternameSpecified)
                    {
                        this.PrinternameValid = PrintUtil.IsValidPrinterName(printer);
                        this.OnPrinternameValidChecked();

                        if (this.PrinternameValid)
                        {
                            this.PrinterError = PrintUtil.GetPrinterError(printer);
                            this.OnPrinterErrorChecked();
                        }
                    }

                    // One failure is enough to error
                    if (!this.PrinterError.IsNullOrWhiteSpace())
                        break;
                }
                catch
                {

                }
            }
        }

        /// <summary>
        /// Called when [printername specified changed].
        /// </summary>
        public void OnPrinternameSpecifiedChanged()
        {
            if (this.PrinternameSpecifiedChanged != null)
                this.PrinternameSpecifiedChanged(null, null);
        }

        /// <summary>
        /// Called when [printername valid changed].
        /// </summary>
        public void OnPrinternameValidChanged()
        {
            if (this.PrinternameValidChanged != null)
                this.PrinternameValidChanged(null, null);
        }

        /// <summary>
        /// Called when [printername valid checked].
        /// </summary>
        public void OnPrinternameValidChecked()
        {
            if (this.PrinternameValidChecked != null)
                this.PrinternameValidChecked(null, null);
        }

        /// <summary>
        /// Called when [printer error changed].
        /// </summary>
        public void OnPrinterErrorChanged()
        {
            if (this.PrinterErrorChanged != null)
                this.PrinterErrorChanged(null, null);
        }

        /// <summary>
        /// Called when [printer error checked].
        /// </summary>
        public void OnPrinterErrorChecked()
        {
            if (this.PrinterErrorChecked != null)
                this.PrinterErrorChecked(null, null);
        }

        /// <summary>
        /// Called when [printer changed].
        /// </summary>
        public void OnPrinterChanged()
        {
            if (this.PrinterChanged != null)
                this.PrinterChanged(null, null);
        }

        /// <summary>
        /// Called when [printername changed].
        /// </summary>
        public void OnPrinternameChanged()
        {
            if (this.PrinternameChanged != null)
                this.PrinternameChanged(null, null);
        }

        /// <summary>
        /// Called when [printername checked].
        /// </summary>
        public void OnPrinternameChecked()
        {
            if (this.PrinternameChecked != null)
                this.PrinternameChecked(null, null);
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when [printername specified changed].
        /// </summary>
        public event EventHandler PrinternameSpecifiedChanged;
        /// <summary>
        /// Occurs when [printername valid changed].
        /// </summary>
        public event EventHandler PrinternameValidChanged;
        /// <summary>
        /// Occurs when [printername valid checked].
        /// </summary>
        public event EventHandler PrinternameValidChecked;
        /// <summary>
        /// Occurs when [printer error changed].
        /// </summary>
        public event EventHandler PrinterErrorChanged;
        /// <summary>
        /// Occurs when [printer error checked].
        /// </summary>
        public event EventHandler PrinterErrorChecked;
        /// <summary>
        /// Occurs when [printer changed].
        /// </summary>
        public event EventHandler PrinterChanged;
        /// <summary>
        /// Occurs when [printername changed].
        /// </summary>
        public event EventHandler PrinternameChanged;
        /// <summary>
        /// Occurs when [printername checked].
        /// </summary>
        public event EventHandler PrinternameChecked;

        #endregion
    }
}
