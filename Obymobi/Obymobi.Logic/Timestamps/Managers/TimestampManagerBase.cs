﻿using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps.Managers
{
    public abstract class TimestampManagerBase
    {
        protected abstract EntityField IdentifierField { get; }

        protected abstract IEntityCollection GetEntities(int companyId);

        public IEnumerable<object> GetIdentifierValues(int companyId)
        {
            List<object> identifierValues = new List<object>();

            IEntityCollection entities = this.GetEntities(companyId);
            foreach (IEntity entity in entities)
            {
                if (entity.Fields[this.IdentifierField.FieldIndex].CurrentValue != null)
                    identifierValues.Add(entity.Fields[this.IdentifierField.FieldIndex].CurrentValue);
            }

            return identifierValues;
        }

        protected abstract TimestampBase GetTimestamp(object identifier);
    }
}
