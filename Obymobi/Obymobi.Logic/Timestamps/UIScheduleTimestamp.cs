﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class UIScheduleTimestamp : TimestampBase
    {
        public UIScheduleTimestamp(int uiScheduleId)
            : base(uiScheduleId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.UIScheduleId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedUIScheduleUTC, TimestampFieldIndex.PublishedUIScheduleUTC }
                };
            }
        }
    }
}
