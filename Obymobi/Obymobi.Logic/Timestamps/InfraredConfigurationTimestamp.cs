﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class InfraredConfigurationTimestamp : TimestampBase
    {
        public InfraredConfigurationTimestamp(int infraredConfigurationId)
            : base(infraredConfigurationId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.InfraredConfigurationId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedInfraredConfigurationUTC, TimestampFieldIndex.PublishedInfraredConfigurationUTC }
                };
            }
        }
    }
}
