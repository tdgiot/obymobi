﻿using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public abstract class TimestampBase
    {
        public TimestampBase(object foreignKeyValue)
        {
            this.ForeignKeyValue = foreignKeyValue;
            this.TimestampEntity = TimestampHelper.GetOrCreateTimestampEntity(this.ForeignKeyField, this.ForeignKeyValue, null);
        }

        protected TimestampEntity TimestampEntity { get; private set; }

        public object ForeignKeyValue { get; private set; }

        public abstract EntityField ForeignKeyField { get; }

        public Dictionary<string, object> ParameterNamesAndValues
        {
            get 
            { 
                return new Dictionary<string, object> { { this.ForeignKeyField.Name, this.ForeignKeyValue } };
            }
        }

        protected abstract Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields { get; }

        public DateTime? GetLastModifiedUtc()
        {
            DateTime? latestLastModifiedUtc = null;

            foreach (TimestampFieldIndex modifiedField in this.Fields.Keys)
            {
                DateTime? lastModifiedUtc = this.TimestampEntity.Fields[(int)modifiedField].CurrentValue as DateTime?;
                if (lastModifiedUtc.HasValue)
                {
                    if (!latestLastModifiedUtc.HasValue)
                        latestLastModifiedUtc = lastModifiedUtc;
                    else if (latestLastModifiedUtc.Value < lastModifiedUtc.Value)
                        latestLastModifiedUtc = lastModifiedUtc;
                }
            }

            return latestLastModifiedUtc;
        }

        public DateTime? GetLastPublishedUtc()
        {
            DateTime? latestLastPublishedUtc = null;

            foreach (TimestampFieldIndex modifiedField in this.Fields.Keys)
            {
                TimestampFieldIndex publishedField = this.Fields[modifiedField];

                DateTime? lastPublishedUtc = this.TimestampEntity.Fields[(int)publishedField].CurrentValue as DateTime?;
                if (lastPublishedUtc.HasValue)
                {
                    if (!latestLastPublishedUtc.HasValue)
                        latestLastPublishedUtc = lastPublishedUtc;
                    else if (latestLastPublishedUtc.Value < lastPublishedUtc.Value)
                        latestLastPublishedUtc = lastPublishedUtc;
                }
            }

            return latestLastPublishedUtc;
        }

        public void UpdatePublishedTimestamps()
        {
            DateTime utcNow = DateTime.UtcNow;

            foreach (TimestampFieldIndex modifiedField in this.Fields.Keys)
            {
                TimestampFieldIndex publishedField = this.Fields[modifiedField];
                this.TimestampEntity.SetNewFieldValue((int)publishedField, utcNow);
            }

            this.TimestampEntity.Save();
        }
        
        public bool HasUnpublishedChanges()
        {
            bool hasUnpublishedChanges = false;

            foreach (TimestampFieldIndex modifiedField in this.Fields.Keys)
            {
                TimestampFieldIndex publishedField = this.Fields[modifiedField];

                if (!this.IsFieldDirty(modifiedField, publishedField))
                    continue;

                hasUnpublishedChanges = true;
                break;
            }

            return hasUnpublishedChanges;
        }

        private bool IsFieldDirty(TimestampFieldIndex modifiedField, TimestampFieldIndex publishedField)
        {
            DateTime? modifiedUtc = this.TimestampEntity.Fields[(int)modifiedField].CurrentValue as DateTime?;
            DateTime? publishedUtc = this.TimestampEntity.Fields[(int)publishedField].CurrentValue as DateTime?;

            if (!modifiedUtc.HasValue) // We cannot determine without a modified value
                return false;

            if (publishedUtc.HasValue && publishedUtc.Value >= modifiedUtc.Value) // Not dirty
                return false;

            return true;
        }
    }
}
