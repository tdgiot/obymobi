﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class CompanyVenueCategoriesTimestamp : TimestampBase
    {
        public CompanyVenueCategoriesTimestamp(int companyId)
            : base(companyId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.CompanyId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedCompanyVenueCategoriesUTC, TimestampFieldIndex.PublishedCompanyVenueCategoriesUTC },       // TODO NEW API
                    { TimestampFieldIndex.ModifiedVenueCategoriesCustomTextUTC, TimestampFieldIndex.PublishedVenueCategoriesCustomTextUTC }, // Rename to *CompanyVenueCategories
                };
            }
        }
    }
}
