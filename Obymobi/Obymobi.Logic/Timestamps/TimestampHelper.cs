﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class TimestampHelper
    {
        public static void UpdateTimestampField(EntityField foreignKeyField, object foreignKeyValue, EntityField timestampField, ITransaction transaction)
        { 
            UpdateTimestampField(foreignKeyField, foreignKeyValue, timestampField, DateTime.UtcNow, transaction);
        }

        public static void UpdateTimestampField(EntityField foreignKeyField, object foreignKeyValue, EntityField timestampField, DateTime timestampValue, ITransaction transaction)
        {
            TimestampEntity timestampEntity = GetOrCreateTimestampEntity(foreignKeyField, foreignKeyValue, transaction);
            timestampEntity.SetNewFieldValue(timestampField.FieldIndex, timestampValue);
            timestampEntity.Validator = null;
            timestampEntity.AddToTransaction(transaction);
            timestampEntity.Save();
        }

        public static TimestampEntity GetOrCreateTimestampEntity(EntityField foreignKeyField, object foreignKeyValue, ITransaction transaction)
        {
            TimestampEntity timestampEntity = null;
            PredicateExpression filter = new PredicateExpression(new FieldCompareValuePredicate(foreignKeyField, ComparisonOperator.Equal, foreignKeyValue));

            TimestampCollection timestampCollection = new TimestampCollection();
            timestampCollection.AddToTransaction(transaction);
            timestampCollection.GetMulti(filter);

            if (timestampCollection.Count > 0)
                timestampEntity = timestampCollection[0];

            if (timestampEntity == null)
            {
                timestampEntity = new TimestampEntity();
                timestampEntity.SetNewFieldValue(foreignKeyField.FieldIndex, foreignKeyValue);
                timestampEntity.AddToTransaction(transaction);
                timestampEntity.Save();
            }

            return timestampEntity;
        }
    }
}
