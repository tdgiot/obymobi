﻿using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public abstract class TimestampManagerBase
    {
        public TimestampManagerBase(object foreignKeyValue)
        {
            this.TimestampEntity = TimestampHelper.GetOrCreateTimestampEntity(this.ForeignKeyField, foreignKeyValue, null);
        }

        protected TimestampEntity TimestampEntity { get; private set; }

        protected abstract EntityField ForeignKeyField { get; }

        protected abstract Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields { get; } 

        public void UpdatePublishedTimestamps()
        {
            foreach (TimestampFieldIndex modifiedField in this.Fields.Keys)
            {
                TimestampFieldIndex publishedField = this.Fields[modifiedField];
                this.TimestampEntity.SetNewFieldValue((int)publishedField, DateTime.UtcNow);
            }
            this.TimestampEntity.Save();
        }
        
        public bool IsDirty()
        {
            bool isDirty = false;

            foreach (TimestampFieldIndex modifiedField in this.Fields.Keys)
            {
                TimestampFieldIndex publishedField = this.Fields[modifiedField];

                if (!this.IsFieldDirty(modifiedField, publishedField))
                    continue;

                isDirty = true;
                break;
            }

            return isDirty;
        }

        private bool IsFieldDirty(TimestampFieldIndex modifiedField, TimestampFieldIndex publishedField)
        {
            DateTime? modifiedUtc = this.TimestampEntity.Fields[(int)modifiedField].CurrentValue as DateTime?;
            DateTime? publishedUtc = this.TimestampEntity.Fields[(int)publishedField].CurrentValue as DateTime?;

            if (!modifiedUtc.HasValue) // We cannot determine without a modified value
                return false;

            if (publishedUtc.HasValue && publishedUtc.Value >= modifiedUtc.Value) // Not dirty
                return false;

            return true;
        }
    }
}
