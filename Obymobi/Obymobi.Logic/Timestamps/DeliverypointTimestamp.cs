﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class DeliverypointTimestamp : TimestampBase
    {
        public DeliverypointTimestamp(int deliverypointId)
            : base(deliverypointId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.DeliverypointId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedDeliverypointUTC, TimestampFieldIndex.PublishedDeliverypointUTC }
                };
            }
        }
    }
}
