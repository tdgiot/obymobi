﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class EntertainmentConfigurationTimestamp : TimestampBase
    {
        public EntertainmentConfigurationTimestamp(int entertainmentConfigurationId)
            : base(entertainmentConfigurationId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.EntertainmentConfigurationId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedAdvertisementConfigurationUTC, TimestampFieldIndex.PublishedAdvertisementConfigurationUTC },
                    { TimestampFieldIndex.ModifiedAdvertisementConfigurationCustomTextUTC, TimestampFieldIndex.PublishedAdvertisementConfigurationCustomTextUTC },
                    { TimestampFieldIndex.ModifiedAdvertisementConfigurationMediaUTC, TimestampFieldIndex.PublishedAdvertisementConfigurationMediaUTC },
                    { TimestampFieldIndex.ModifiedEntertainmentCategoriesUTC, TimestampFieldIndex.PublishedEntertainmentCategoriesUTC },
                    { TimestampFieldIndex.ModifiedEntertainmentCategoriesCustomTextUTC, TimestampFieldIndex.PublishedEntertainmentCategoriesCustomTextUTC },
                    { TimestampFieldIndex.ModifiedEntertainmentCategoriesMediaUTC, TimestampFieldIndex.PublishedEntertainmentCategoriesMediaUTC }
                };
            }
        }
    }
}
