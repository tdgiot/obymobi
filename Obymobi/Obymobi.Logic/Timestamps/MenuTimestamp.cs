﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class MenuTimestamp : TimestampBase
    {
        public MenuTimestamp(int menuId) 
            : base(menuId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.MenuId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedAlterationsUTC, TimestampFieldIndex.PublishedAlterationsUTC },
                    { TimestampFieldIndex.ModifiedAlterationsCustomTextUTC, TimestampFieldIndex.PublishedAlterationsCustomTextUTC },
                    { TimestampFieldIndex.ModifiedAlterationsMediaUTC, TimestampFieldIndex.PublishedAlterationsMediaUTC },
                    { TimestampFieldIndex.ModifiedMenuUTC, TimestampFieldIndex.PublishedMenuUTC },
                    { TimestampFieldIndex.ModifiedMenuCustomTextUTC, TimestampFieldIndex.PublishedMenuCustomTextUTC },
                    { TimestampFieldIndex.ModifiedMenuMediaUTC, TimestampFieldIndex.PublishedMenuMediaUTC },
                    { TimestampFieldIndex.ModifiedProductgroupsUTC, TimestampFieldIndex.PublishedProductgroupsUTC },
                    { TimestampFieldIndex.ModifiedProductgroupsCustomTextUTC, TimestampFieldIndex.PublishedProductgroupsCustomTextUTC },
                    { TimestampFieldIndex.ModifiedProductgroupsMediaUTC, TimestampFieldIndex.PublishedProductgroupsMediaUTC },
                    { TimestampFieldIndex.ModifiedProductsUTC, TimestampFieldIndex.PublishedProductsUTC },
                    { TimestampFieldIndex.ModifiedProductsCustomTextUTC, TimestampFieldIndex.PublishedProductsCustomTextUTC },
                    { TimestampFieldIndex.ModifiedProductsMediaUTC, TimestampFieldIndex.PublishedProductsMediaUTC },
                    { TimestampFieldIndex.ModifiedSchedulesUTC, TimestampFieldIndex.PublishedSchedulesUTC },
                };
            }
        }
    }
}
