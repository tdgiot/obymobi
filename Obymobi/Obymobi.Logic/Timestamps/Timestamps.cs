﻿using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Timestamps.Managers;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public static class Timestamps
    {
        private static AdvertisementConfigurationTimestampManager AdvertisementConfigurationTimestampManager = new AdvertisementConfigurationTimestampManager();

        private static List<TimestampManagerBase> TimestampManagers = new List<TimestampManagerBase>
        {
            AdvertisementConfigurationTimestampManager,
        };

        public static void UpdatePublishedTimestamps(int companyId)
        {
            foreach (TimestampManagerBase timestampManagerBase in TimestampManagers)
            {
                IEnumerable<object> identifierValues = timestampManagerBase.GetIdentifierValues(companyId);
                foreach (object identifierValue in identifierValues)
                {
                    
                }
            } 
        }
    }
}
