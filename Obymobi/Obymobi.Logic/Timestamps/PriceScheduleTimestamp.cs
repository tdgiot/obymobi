﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class PriceScheduleTimestamp : TimestampBase
    {
        public PriceScheduleTimestamp(int priceScheduleId)
            : base(priceScheduleId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.PriceScheduleId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedPriceScheduleUTC, TimestampFieldIndex.PublishedPriceScheduleUTC }
                };
            }
        }
    }
}
