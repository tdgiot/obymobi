﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class RoomControlConfigurationTimestamp : TimestampBase
    {
        public RoomControlConfigurationTimestamp(int roomControlConfigurationId)
            : base(roomControlConfigurationId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.RoomControlConfigurationId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedRoomControlConfigurationUTC, TimestampFieldIndex.PublishedRoomControlConfigurationUTC },
                    { TimestampFieldIndex.ModifiedRoomControlConfigurationCustomTextUTC, TimestampFieldIndex.PublishedRoomControlConfigurationCustomTextUTC },
                    { TimestampFieldIndex.ModifiedRoomControlConfigurationMediaUTC, TimestampFieldIndex.PublishedRoomControlConfigurationMediaUTC	}
                };
            }
        }
    }
}
