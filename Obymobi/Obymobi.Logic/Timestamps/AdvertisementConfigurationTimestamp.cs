﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class AdvertisementConfigurationTimestamp : TimestampBase
    {
        public AdvertisementConfigurationTimestamp(int advertisementConfigurationId)
            : base(advertisementConfigurationId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.AdvertisementConfigurationId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedAdvertisementConfigurationUTC, TimestampFieldIndex.PublishedAdvertisementConfigurationUTC },
                    { TimestampFieldIndex.ModifiedAdvertisementConfigurationCustomTextUTC, TimestampFieldIndex.PublishedAdvertisementConfigurationCustomTextUTC },
                    { TimestampFieldIndex.ModifiedAdvertisementConfigurationMediaUTC, TimestampFieldIndex.PublishedAdvertisementConfigurationMediaUTC }
                };
            }
        }
    }
}
