﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class MapTimestamp : TimestampBase
    {
        public MapTimestamp(int mapId)
            : base(mapId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.MapId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedMapUTC, TimestampFieldIndex.PublishedMapUTC }
                };
            }
        }
    }
}
