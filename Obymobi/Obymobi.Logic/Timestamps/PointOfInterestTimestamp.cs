﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class PointOfInterestTimestamp : TimestampBase
    {
        public PointOfInterestTimestamp(int pointOfInterestId)
            : base(pointOfInterestId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.PointOfInterestId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedPointOfInterestUTC, TimestampFieldIndex.PublishedPointOfInterestUTC },
                    { TimestampFieldIndex.ModifiedPointOfInterestCustomTextUTC, TimestampFieldIndex.PublishedPointOfInterestCustomTextUTC },
                    { TimestampFieldIndex.ModifiedPointOfInterestMediaUTC, TimestampFieldIndex.PublishedPointOfInterestMediaUTC	},
                    { TimestampFieldIndex.ModifiedPointOfInterestVenueCategoriesUTC, TimestampFieldIndex.PublishedPointOfInterestVenueCategoriesUTC },
                    { TimestampFieldIndex.ModifiedPointOfInterestVenueCategoriesCustomTextUTC, TimestampFieldIndex.PublishedPointOfInterestVenueCategoriesCustomTextUTC }
                };
            }
        }
    }
}
