﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class ClientConfigurationTimestamp : TimestampBase
    {
        public ClientConfigurationTimestamp(int clientConfigurationId)
            : base(clientConfigurationId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.ClientConfigurationId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedClientConfigurationUTC, TimestampFieldIndex.PublishedClientConfigurationUTC },
                    { TimestampFieldIndex.ModifiedClientConfigurationCustomTextUTC, TimestampFieldIndex.PublishedClientConfigurationCustomTextUTC },
                    { TimestampFieldIndex.ModifiedClientConfigurationMediaUTC, TimestampFieldIndex.PublishedClientConfigurationMediaUTC }
                };
            }
        }
    }
}
