﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class UIModeTimestamp : TimestampBase
    {
        public UIModeTimestamp(int uiModeId)
            : base(uiModeId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.UIModeId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedUIModeUTC, TimestampFieldIndex.PublishedUIModeUTC },
                    { TimestampFieldIndex.ModifiedUIModeCustomTextUTC, TimestampFieldIndex.PublishedUIModeCustomTextUTC },
                    { TimestampFieldIndex.ModifiedUIModeMediaUTC, TimestampFieldIndex.PublishedUIModeMediaUTC	}
                };
            }
        }
    }
}
