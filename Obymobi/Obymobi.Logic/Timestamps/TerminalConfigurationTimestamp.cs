﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class TerminalConfigurationTimestamp : TimestampBase
    {
        public TerminalConfigurationTimestamp(int terminalConfigurationId)
            : base(terminalConfigurationId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.TerminalConfigurationId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedTerminalConfigurationUTC, TimestampFieldIndex.PublishedTerminalConfigurationUTC },
                };
            }
        }
    }
}
