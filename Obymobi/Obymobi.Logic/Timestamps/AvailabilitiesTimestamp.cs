﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class AvailabilitiesTimestamp : TimestampBase
    {
        public AvailabilitiesTimestamp(int companyId)
            : base(companyId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.CompanyId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedAvailabilitiesUTC, TimestampFieldIndex.PublishedAvailabilitiesUTC },
                    { TimestampFieldIndex.ModifiedAvailabilitiesCustomTextUTC, TimestampFieldIndex.PublishedAvailabilitiesCustomTextUTC },
                };
            }
        }
    }
}
