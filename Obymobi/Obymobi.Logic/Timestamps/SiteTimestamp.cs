﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class SiteTimestamp : TimestampBase
    {
        public SiteTimestamp(int siteId)
            : base(siteId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.SiteId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedSiteUTC, TimestampFieldIndex.PublishedSiteUTC },
                    { TimestampFieldIndex.ModifiedSiteCustomTextUTC, TimestampFieldIndex.PublishedSiteCustomTextUTC },
                    { TimestampFieldIndex.ModifiedSiteMediaUTC, TimestampFieldIndex.PublishedSiteMediaUTC	}
                };
            }
        }
    }
}
