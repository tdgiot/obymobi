﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public static class TimestampFactory
    {
        public static MenuTimestamp GetMenuTimestamp(int menuId)
        {
            return new MenuTimestamp(menuId);
        }

        public static CompanyTimestamp GetCompanyTimestamp(int companyId)
        {
            return new CompanyTimestamp(companyId);
        }

        public static ClientConfigurationTimestamp GetClientConfigurationTimestamp(int clientConfigurationId)
        {
            return new ClientConfigurationTimestamp(clientConfigurationId);
        }

        public static TerminalConfigurationTimestamp GetTerminalConfigurationTimestamp(int terminalConfigurationId)
        {
            return new TerminalConfigurationTimestamp(terminalConfigurationId);
        }

        public static DeliverypointsTimestamp GetDeliverypointsTimestamp(int deliverypointgroupId)
        {
            return new DeliverypointsTimestamp(deliverypointgroupId);
        }

        public static PriceScheduleTimestamp GetPriceScheduleTimestamp(int priceScheduleId)
        {
            return new PriceScheduleTimestamp(priceScheduleId);
        }

        public static AdvertisementConfigurationTimestamp GetAdvertisementsTimestamp(int advertisementConfigurationId)
        {
            return new AdvertisementConfigurationTimestamp(advertisementConfigurationId);
        }

        public static EntertainmentConfigurationTimestamp GetEntertainmentTimestamp(int entertainmentConfigurationId)
        {
            return new EntertainmentConfigurationTimestamp(entertainmentConfigurationId);
        }

        public static RoomControlConfigurationTimestamp GetRoomControlConfigurationTimestamp(int roomControlConfigurationId)
        {
            return new RoomControlConfigurationTimestamp(roomControlConfigurationId);
        }

        public static InfraredConfigurationTimestamp GetInfraredConfigurationTimestamp(int infraredConfigurationId)
        {
            return new InfraredConfigurationTimestamp(infraredConfigurationId);
        }

        public static SiteTimestamp GetSiteTimestamp(int siteId)
        {
            return new SiteTimestamp(siteId);
        }

        public static TerminalTimestamp GetTerminalTimestamp(int terminalId)
        {
            return new TerminalTimestamp(terminalId);
        }

        public static UIModeTimestamp GetUIModeTimestamp(int uiModeId)
        {
            return new UIModeTimestamp(uiModeId);
        }

        public static UIScheduleTimestamp GetUIScheduleTimestamp(int uiScheduleId)
        {
            return new UIScheduleTimestamp(uiScheduleId);
        }

        public static UIThemeTimestamp GetUIThemeTimestamp(int uiThemeId)
        {
            return new UIThemeTimestamp(uiThemeId);
        }

        public static PointOfInterestTimestamp GetPointOfInterestTimestamp(int pointOfInterestId)
        {
            return new PointOfInterestTimestamp(pointOfInterestId);
        }

        public static CompanyAmenitiesTimestamp GetCompanyAmenitiesTimestamp(int companyId)
        {
            return new CompanyAmenitiesTimestamp(companyId);
        }

        public static AvailabilitiesTimestamp GetAvailabilitiesTimestamp(int companyId)
        {
            return new AvailabilitiesTimestamp(companyId);
        }

        public static CloudStorageAccountsTimestamp GetCloudStorageAccountsTimestamp(int companyId)
        {
            return new CloudStorageAccountsTimestamp(companyId);
        }

        public static CompanyVenueCategoriesTimestamp GetCompanyVenueCategoriesTimestamp(int companyId)
        {
            return new CompanyVenueCategoriesTimestamp(companyId);
        }
    }
}
