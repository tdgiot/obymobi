﻿using Obymobi.Data;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Logic.Timestamps
{
    public class UIThemeTimestamp : TimestampBase
    {
        public UIThemeTimestamp(int uiThemeId)
            : base(uiThemeId)
        {
        }

        public override EntityField ForeignKeyField
        {
            get { return TimestampFields.UIThemeId; }
        }

        protected override Dictionary<TimestampFieldIndex, TimestampFieldIndex> Fields
        {
            get 
            {
                return new Dictionary<TimestampFieldIndex, TimestampFieldIndex>
                {
                    { TimestampFieldIndex.ModifiedUIThemeUTC, TimestampFieldIndex.PublishedUIThemeUTC },
                    { TimestampFieldIndex.ModifiedUIThemeMediaUTC, TimestampFieldIndex.PublishedUIThemeMediaUTC	}
                };
            }
        }
    }
}
