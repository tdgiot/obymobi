using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ClientConfigurationRouteEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ClientConfigurationRouteTimestampUpdater : GeneralTimestampUpdater<ClientConfigurationRouteEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    ClientConfigurationRouteFields.Type,
                    ClientConfigurationRouteFields.RouteId,
                    ClientConfigurationRouteFields.ParentCompanyId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForClientConfiguration(ClientConfigurationRouteEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.ClientConfigurationId, entity.ClientConfigurationId, TimestampFields.ModifiedClientConfigurationUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(ClientConfigurationRouteEntity entity, bool wasNew)
        {
            this.UpdateTimestampForClientConfiguration(entity);
        }

        #endregion
    }
}
