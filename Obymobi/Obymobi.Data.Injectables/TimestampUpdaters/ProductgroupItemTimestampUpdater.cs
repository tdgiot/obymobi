using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ProductgroupItemEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ProductgroupItemTimestampUpdater : GeneralTimestampUpdater<ProductgroupItemEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(ProductgroupItemEntity entity, bool wasNew)
        {
            new MenuTimestampUpdater().UpdateTimestampForProductgroups(entity.ParentCompanyId, entity.GetCurrentTransaction());
        }

        #endregion
    }
}
