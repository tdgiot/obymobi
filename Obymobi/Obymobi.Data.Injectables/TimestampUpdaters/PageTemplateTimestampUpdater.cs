using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(PageTemplateEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class PageTemplateTimestampUpdater : GeneralTimestampUpdater<PageTemplateEntity>
    {
        #region Methods

        internal void UpdateTimestampForPageTemplate(PageTemplateEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PageFields.PageTemplateId == entity.PageTemplateId };

            PageCollection pageCollection = new PageCollection();
            pageCollection.AddToTransaction(entity);
            pageCollection.GetMulti(filter, new IncludeFieldsList(PageFields.SiteId, PageFields.ParentCompanyId), null);

            foreach (PageEntity pageEntity in pageCollection)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(PageTemplateEntity entity, bool wasNew)
        {
            this.UpdateTimestampForPageTemplate(entity);
        }

        #endregion
    }
}
