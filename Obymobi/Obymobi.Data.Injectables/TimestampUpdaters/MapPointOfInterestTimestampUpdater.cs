using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MapPointOfInterestEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MapPointOfInterestTimestampUpdater : GeneralTimestampUpdater<MapPointOfInterestEntity>
    {
        #region Methods

        protected override void UpdateTimestamps(MapPointOfInterestEntity entity, bool wasNew)
        {
            this.UpdateTimestampField(TimestampFields.MapId, entity.MapId, TimestampFields.ModifiedMapUTC, entity.GetCurrentTransaction());
        }

        #endregion
    }
}
