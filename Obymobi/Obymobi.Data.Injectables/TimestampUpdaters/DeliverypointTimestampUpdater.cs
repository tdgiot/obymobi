using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(DeliverypointEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class DeliverypointTimestampUpdater : GeneralTimestampUpdater<DeliverypointEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    DeliverypointFields.DeliverypointId,
                    DeliverypointFields.Number,
                    DeliverypointFields.Name,
                    DeliverypointFields.GooglePrinterId,
                    DeliverypointFields.DeviceId,
                    DeliverypointFields.EnableAnalytics,
                    DeliverypointFields.RoomControlConfigurationId,
                    DeliverypointFields.RoomControllerType,
                    DeliverypointFields.RoomControllerIp,
                    DeliverypointFields.RoomControllerSlaveId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForDeliverypoint(DeliverypointEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.DeliverypointId, entity.DeliverypointId, TimestampFields.ModifiedDeliverypointUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForDeliverypoints(DeliverypointEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.DeliverypointgroupId, entity.DeliverypointgroupId, TimestampFields.ModifiedDeliverypointsUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForClients(DeliverypointEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { ClientFields.DeliverypointId == entity.DeliverypointId };

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.AddToTransaction(entity);
            clientCollection.GetMulti(filter, 0, null, null, null, new IncludeFieldsList(ClientFields.ClientId), 0, 0);

            foreach (ClientEntity clientEntity in clientCollection)
            {
                this.UpdateTimestampField(TimestampFields.ClientId, clientEntity.ClientId, TimestampFields.ModifiedClientUTC, entity.GetCurrentTransaction());    
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(DeliverypointEntity entity, bool wasNew)
        {
            this.UpdateTimestampForDeliverypoint(entity);
            this.UpdateTimestampForDeliverypoints(entity);

            if (entity.ClientConfigurationIdChanged)
                this.UpdateTimestampForClients(entity);
        }

        #endregion
    }
}
