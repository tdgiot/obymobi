using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(DeliverypointgroupEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class DeliverypointgroupTimestampUpdater : GeneralTimestampUpdater<DeliverypointgroupEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    DeliverypointgroupFields.DeliverypointgroupId,
                    DeliverypointgroupFields.MenuId,
                    DeliverypointgroupFields.UIModeId,
                    DeliverypointgroupFields.UIThemeId,
                    DeliverypointgroupFields.UIScheduleId,
                    DeliverypointgroupFields.Pincode,
                    DeliverypointgroupFields.PincodeSU,
                    DeliverypointgroupFields.PincodeGM,
                    DeliverypointgroupFields.PriceScheduleId,
                    DeliverypointgroupFields.HidePrices,
                    DeliverypointgroupFields.ClearSessionOnTimeout,
                    DeliverypointgroupFields.ResetTimeout,
                    DeliverypointgroupFields.ScreenTimeoutInterval,
                    DeliverypointgroupFields.DimLevelDull,
                    DeliverypointgroupFields.DimLevelMedium,
                    DeliverypointgroupFields.DimLevelBright,
                    DeliverypointgroupFields.DockedDimLevelDull,
                    DeliverypointgroupFields.DockedDimLevelMedium,
                    DeliverypointgroupFields.DockedDimLevelBright,
                    DeliverypointgroupFields.PowerSaveTimeout,
                    DeliverypointgroupFields.PowerSaveLevel,
                    DeliverypointgroupFields.OutOfChargeLevel,
                    DeliverypointgroupFields.DailyOrderReset,
                    DeliverypointgroupFields.SleepTime,
                    DeliverypointgroupFields.WakeUpTime,
                    DeliverypointgroupFields.HomepageSlideshowInterval,
                    DeliverypointgroupFields.IsChargerRemovedDialogEnabled,
                    DeliverypointgroupFields.IsChargerRemovedReminderDialogEnabled,
                    DeliverypointgroupFields.PowerButtonHardBehaviour,
                    DeliverypointgroupFields.PowerButtonSoftBehaviour,
                    DeliverypointgroupFields.ScreenOffMode,
                    DeliverypointgroupFields.ScreensaverMode,
                    DeliverypointgroupFields.RoomserviceCharge,
                    DeliverypointgroupFields.BrowserAgeVerificationEnabled,
                    DeliverypointgroupFields.BrowserAgeVerificationLayout,
                    DeliverypointgroupFields.RestartApplicationDialogEnabled,
                    DeliverypointgroupFields.RebootDeviceDialogEnabled,
                    DeliverypointgroupFields.RestartTimeoutSeconds,
                    DeliverypointgroupFields.GooglePrinterId,
                    DeliverypointgroupFields.PmsIntegration,
                    DeliverypointgroupFields.PmsAllowShowBill,
                    DeliverypointgroupFields.PmsAllowExpressCheckout,
                    DeliverypointgroupFields.PmsAllowShowGuestName,
                    DeliverypointgroupFields.PmsLockClientWhenNotCheckedIn,
                    DeliverypointgroupFields.IsOrderitemAddedDialogEnabled,
                    DeliverypointgroupFields.IsClearBasketDialogEnabled,
                    DeliverypointgroupFields.Name,
                    DeliverypointgroupFields.ClientConfigurationId,
                    DeliverypointgroupFields.EstimatedDeliveryTime,
                    DeliverypointgroupFields.ApiVersion,
                    DeliverypointgroupFields.OrderCompletedEnabled,
                    DeliverypointgroupFields.AffiliateCampaignId,
                    DeliverypointgroupFields.AddressId
                };
            }
        }

        private bool ShouldUpdateTimestampForCompany { get; set; }
        private bool ShouldUpdateTimestampForEstimatedDeliveryTime { get; set; }
        private bool ShouldUpdateTimestampForClientConfiguration { get; set; }

        #endregion

        #region Methods

        public override bool RelevantFieldsChanged(IEntity entity)
        {
            DeliverypointgroupEntity useableEntity = entity as DeliverypointgroupEntity;
            if (useableEntity == null)
                return base.RelevantFieldsChanged(entity);

            this.ShouldUpdateTimestampForCompany = entity.Fields[DeliverypointgroupFields.Name.FieldIndex].IsChanged ||
                                                   entity.Fields[DeliverypointgroupFields.ApiVersion.FieldIndex].IsChanged;
            this.ShouldUpdateTimestampForEstimatedDeliveryTime = entity.Fields[DeliverypointgroupFields.EstimatedDeliveryTime.FieldIndex].IsChanged;
            this.ShouldUpdateTimestampForClientConfiguration = useableEntity.ClientConfigurationId.HasValue;

            return base.RelevantFieldsChanged(entity);
        }

        public override void UpdateTimestamps(IEntity entity, bool wasNew)
        {
            DeliverypointgroupEntity useableEntity = entity as DeliverypointgroupEntity;
            if (useableEntity == null)
                return;

            if (this.ShouldUpdateTimestampForCompany)
            {
                this.UpdateTimestampForCompany(useableEntity);
            }

            if (this.ShouldUpdateTimestampForEstimatedDeliveryTime)
            {
                this.UpdateTimestampForEstimatedDeliveryTime(useableEntity);
            }

            if (this.ShouldUpdateTimestampForClientConfiguration)
            {
                this.UpdateTimestampForClientConfiguration(useableEntity);
            }
        }

        internal void UpdateTimestampForCompany(DeliverypointgroupEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId, TimestampFields.ModifiedCompanyUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForEstimatedDeliveryTime(DeliverypointgroupEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.DeliverypointgroupId, entity.DeliverypointgroupId, TimestampFields.ModifiedDeliveryTimeUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForClientConfiguration(DeliverypointgroupEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.ClientConfigurationId, entity.ClientConfigurationId, TimestampFields.ModifiedClientConfigurationUTC, entity.GetCurrentTransaction());
        }

        #endregion
    }
}
