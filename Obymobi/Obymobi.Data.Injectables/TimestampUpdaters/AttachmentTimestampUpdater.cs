using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(AttachmentEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class AttachmentTimestampUpdater : GeneralTimestampUpdater<AttachmentEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    AttachmentFields.AttachmentId,
                    AttachmentFields.Name,
                    AttachmentFields.Type,
                    AttachmentFields.Url,
                    AttachmentFields.AllowAllDomains,
                    AttachmentFields.AllowedDomains
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForPage(AttachmentEntity entity)
        {
            PageEntity pageEntity = new PageEntity();
            pageEntity.AddToTransaction(entity);
            pageEntity.FetchUsingPK(entity.PageId.Value, null, null, new IncludeFieldsList { PageFields.SiteId, PageFields.ParentCompanyId });
            if (!pageEntity.IsNew)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForPageTemplate(AttachmentEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PageFields.PageTemplateId == entity.PageTemplateId.Value };

            PageCollection pageCollection = new PageCollection();
            pageCollection.AddToTransaction(entity);
            pageCollection.GetMulti(filter, new IncludeFieldsList(PageFields.SiteId), null);

            foreach (PageEntity pageEntity in pageCollection)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(AttachmentEntity entity, bool wasNew)
        {
            if (entity.ProductId.HasValue)
            {
                if (entity.ProductEntity.BrandId.HasValue)
                {
                    new MenuTimestampUpdater().UpdateTimestampForBrandProducts(entity.ProductEntity.BrandId.Value, entity.GetCurrentTransaction());
                }
                else if (entity.ProductEntity.CompanyId.HasValue)
                {
                    new MenuTimestampUpdater().UpdateTimestampForProducts(entity.ProductEntity.CompanyId.Value, entity.GetCurrentTransaction());
                }
            }
            else if (entity.PageId.HasValue)
            {
                this.UpdateTimestampForPage(entity);
            }
            else if (entity.PageTemplateId.HasValue)
            {
                this.UpdateTimestampForPageTemplate(entity);
            }
        }

        #endregion
    }
}
