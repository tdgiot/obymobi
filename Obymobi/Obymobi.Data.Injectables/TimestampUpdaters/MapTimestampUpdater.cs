﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MapEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MapTimestampUpdater : GeneralTimestampUpdater<MapEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    MapFields.MapId,
                    MapFields.CompanyId,
                    MapFields.MapProvider,
                    MapFields.Name,
                    MapFields.MyLocationEnabled,
                    MapFields.ZoomControlsEnabled,
                    MapFields.IndoorEnabled,
                    MapFields.MapType,
                    MapFields.ZoomLevel
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampsForMap(MapEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.MapId, entity.MapId, TimestampFields.ModifiedMapUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(MapEntity entity, bool wasNew)
        {
            this.UpdateTimestampsForMap(entity);
        }

        #endregion
    }
}
