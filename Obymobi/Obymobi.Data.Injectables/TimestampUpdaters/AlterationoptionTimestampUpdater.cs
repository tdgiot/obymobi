using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(AlterationoptionEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class AlterationoptionTimestampUpdater : GeneralTimestampUpdater<AlterationoptionEntity>
    {
        #region Methods

        protected override void UpdateTimestamps(AlterationoptionEntity entity, bool wasNew)
        {
            if (entity.BrandId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForBrandAlterations(entity.BrandId.Value, entity.GetCurrentTransaction());
            }
            else if (entity.CompanyId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForAlterations(entity.CompanyId.Value, entity.GetCurrentTransaction());
            }
        }

        #endregion
    }
}
