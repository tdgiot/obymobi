﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Api.Logic.Timestamps;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    public class GeneralTimestampUpdater<TEntity> : TimestampUpdaterBase where TEntity : EntityBase
    {
        /// <summary>
        /// Gets the fields that are used in determining whether the timestamp needs to be updated.
        /// </summary>
        protected override IEnumerable<EntityField> Fields => new List<EntityField>();

        private IList<int> changedFieldsIndexes = new List<int>();

        /// <inheritdoc/>
        /// <remarks>This method should not be overridden, use UpdateTimestamps(TEntity entity, bool wasNew).</remarks>
        public override void UpdateTimestamps(IEntity entity, bool wasNew)
        {
            TEntity useableEntity = entity as TEntity;
            if (useableEntity != null)
            {
                this.UpdateTimestamps(useableEntity, wasNew);
            }
        }

        /// <summary>
        /// Updates the timestamp(s) for the specified entity.
        /// </summary>
        /// <param name="entity">The entity to update the timestamp for.</param>
        /// <param name="adapter">The data access adapter to use.</param>
        /// <param name="wasNew">Flag which indicates whether the entity was new when it was saved.</param>
        protected virtual void UpdateTimestamps(TEntity entity, bool wasNew)
        {
        }

        /// <summary>
        /// Checks whether the relevant fields were changed to determine whether a timestamp needs to be updated.
        /// </summary>
        /// <param name="entity">The entity to check whether it's fields have been changed.</param>
        /// <returns>True if the one or more of the relevant fields were changed, False if not.</returns>
        public override bool RelevantFieldsChanged(IEntity entity)
        {
            if (!this.Fields.Any())
            {
                return true;
            }

            bool isChanged = false;
            foreach (EntityField entityField in this.Fields)
            {
                if (entity.Fields[entityField.FieldIndex].IsChanged)
                {
                    changedFieldsIndexes.Add(entityField.FieldIndex);
                    isChanged = true;
                }
            }

            return isChanged;
        }

        protected bool IsFieldChanged(IEntityField field)
        {
            return this.changedFieldsIndexes.Contains(field.FieldIndex);
        }

        protected void UpdateTimestampField(EntityField foreignKeyField, object foreignKeyValue, EntityField timestampField)
        {
            TimestampHelper.UpdateTimestampField(foreignKeyField, foreignKeyValue, timestampField, DateTime.UtcNow, null);
        }

        protected void UpdateTimestampField(EntityField foreignKeyField, object foreignKeyValue, EntityField timestampField, ITransaction transaction)
        {
            TimestampHelper.UpdateTimestampField(foreignKeyField, foreignKeyValue, timestampField, DateTime.UtcNow, transaction);
        }

        protected void UpdateTimestampField(EntityField foreignKeyField, object foreignKeyValue, EntityField timestampField, DateTime timestampValue)
        {
            TimestampHelper.UpdateTimestampField(foreignKeyField, foreignKeyValue, timestampField, timestampValue, null);
        }

        protected void UpdateTimestampField(EntityField foreignKeyField, object foreignKeyValue, EntityField timestampField, DateTime timestampValue, ITransaction transaction)
        {
            TimestampHelper.UpdateTimestampField(foreignKeyField, foreignKeyValue, timestampField, timestampValue, transaction);
        }
    }
}
