﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(TerminalMessageTemplateCategoryEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class TerminalMessageTemplateCategoryTimestampUpdater : GeneralTimestampUpdater<TerminalMessageTemplateCategoryEntity>
    {
        #region Methods

        internal void UpdateTimestampForTerminalMessageTemplateCategory(TerminalMessageTemplateCategoryEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.TerminalId, entity.TerminalId, TimestampFields.ModifiedMessageTemplatesUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(TerminalMessageTemplateCategoryEntity entity, bool wasNew)
        {
            this.UpdateTimestampForTerminalMessageTemplateCategory(entity);
        }

        #endregion
    }
}
