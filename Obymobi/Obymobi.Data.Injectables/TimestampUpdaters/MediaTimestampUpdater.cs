using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MediaEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MediaTimestampUpdater : GeneralTimestampUpdater<MediaEntity>
    {
        #region Methods

        internal void UpdateTimestampForAdvertisement(MediaEntity entity)
        {
            PredicateExpression filter = new PredicateExpression(AdvertisementConfigurationAdvertisementFields.AdvertisementId == entity.AdvertisementId.Value);

            AdvertisementConfigurationAdvertisementCollection advertisementConfigurationAdvertisementCollection = new AdvertisementConfigurationAdvertisementCollection();
            advertisementConfigurationAdvertisementCollection.AddToTransaction(entity);
            advertisementConfigurationAdvertisementCollection.GetMulti(filter, new IncludeFieldsList(AdvertisementConfigurationAdvertisementFields.AdvertisementConfigurationId, AdvertisementConfigurationAdvertisementFields.ParentCompanyId), null);

            foreach (AdvertisementConfigurationAdvertisementEntity advertisementConfigurationAdvertisementEntity in advertisementConfigurationAdvertisementCollection)
            {
                this.UpdateTimestampField(TimestampFields.AdvertisementConfigurationId, advertisementConfigurationAdvertisementEntity.AdvertisementConfigurationId, TimestampFields.ModifiedAdvertisementConfigurationMediaUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForAlterations(AlterationEntity entity, ITransaction transaction)
        {
            if (entity.BrandId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForBrandAlterationsMedia(entity.BrandId.Value, transaction);
            }
            else if (entity.CompanyId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForAlterationsMedia(entity.CompanyId.Value, transaction);
            }
        }

        internal void UpdateTimestampForAlterationoption(AlterationoptionEntity entity, ITransaction transaction)
        {
            if (entity.BrandId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForBrandAlterationsMedia(entity.BrandId.Value, transaction);
            }
            else if (entity.CompanyId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForAlterationsMedia(entity.CompanyId.Value, transaction);
            }
        }

        internal void UpdateTimestampForAttachment(MediaEntity entity)
        {
            if (entity.AttachmentEntity.ProductId.HasValue)
            {
                this.UpdateTimestampForProduct(entity.AttachmentEntity.ProductEntity, entity.GetCurrentTransaction());
            }
            else if (entity.AttachmentEntity.PageId.HasValue)
            {
                this.UpdateTimestampForAttachmentPage(entity);
            }
            else if (entity.AttachmentEntity.PageTemplateId.HasValue)
            {
                this.UpdateTimestampForAttachmentPageTemplate(entity);
            }
        }

        internal void UpdateTimestampForAttachmentPage(MediaEntity entity)
        {
            PageEntity pageEntity = new PageEntity();
            pageEntity.AddToTransaction(entity);
            pageEntity.FetchUsingPK(entity.AttachmentEntity.PageId.Value, null, null, new IncludeFieldsList { PageFields.SiteId });
            if (!pageEntity.IsNew)
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteMediaUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForAttachmentPageTemplate(MediaEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PageFields.PageTemplateId == entity.AttachmentEntity.PageTemplateId.Value };

            PageCollection pageCollection = new PageCollection();
            pageCollection.AddToTransaction(entity);
            pageCollection.GetMulti(filter, new IncludeFieldsList(PageFields.SiteId), null);

            foreach (PageEntity pageEntity in pageCollection)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteMediaUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForCategory(CategoryEntity entity, ITransaction transaction)
        {
            if (entity.MenuId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForMenuMedia(entity.MenuId.Value, transaction);
            }
        }

        internal void UpdateTimestampForClientConfiguration(MediaEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.ClientConfigurationId, entity.ClientConfigurationId.Value, TimestampFields.ModifiedClientConfigurationMediaUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForCompany(MediaEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId.Value, TimestampFields.ModifiedCompanyMediaUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForEntertainment(MediaEntity entity)
        {
            PredicateExpression filter = new PredicateExpression(EntertainmentConfigurationEntertainmentFields.EntertainmentId == entity.EntertainmentId.Value);

            EntertainmentConfigurationEntertainmentCollection entertainmentConfigurationEntertainmentCollection = new EntertainmentConfigurationEntertainmentCollection();
            entertainmentConfigurationEntertainmentCollection.AddToTransaction(entity);
            entertainmentConfigurationEntertainmentCollection.GetMulti(filter, new IncludeFieldsList(EntertainmentConfigurationEntertainmentFields.EntertainmentConfigurationId), null);

            foreach (EntertainmentConfigurationEntertainmentEntity entertainmentConfigurationEntertainmentEntity in entertainmentConfigurationEntertainmentCollection)
            {
                this.UpdateTimestampField(TimestampFields.EntertainmentConfigurationId, entertainmentConfigurationEntertainmentEntity.EntertainmentConfigurationId, TimestampFields.ModifiedEntertainmentConfigurationMediaUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForPageElement(MediaEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PageElementFields.PageElementId == entity.PageElementId.Value };
            RelationCollection relations = new RelationCollection 
            { 
                PageElementEntityBase.Relations.PageEntityUsingPageId
            };

            PageCollection pageCollection = new PageCollection();
            pageCollection.AddToTransaction(entity);
            pageCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(PageFields.SiteId), 0, 0);

            foreach (PageEntity pageEntity in pageCollection)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteMediaUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForPage(MediaEntity entity)
        {
            PageEntity pageEntity = new PageEntity();
            pageEntity.AddToTransaction(entity);
            pageEntity.FetchUsingPK(entity.PageId.Value, null, null, new IncludeFieldsList { PageFields.SiteId });
            if (!pageEntity.IsNew)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteMediaUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForPageTemplateElement(MediaEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PageTemplateElementFields.PageTemplateElementId == entity.PageTemplateElementId.Value };
            RelationCollection relations = new RelationCollection 
            { 
                PageTemplateElementEntityBase.Relations.PageTemplateEntityUsingPageTemplateId,
                PageTemplateEntityBase.Relations.PageEntityUsingPageTemplateId
            };

            PageCollection pageCollection = new PageCollection();
            pageCollection.AddToTransaction(entity);
            pageCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(PageFields.SiteId), 0, 0);

            foreach (PageEntity pageEntity in pageCollection)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteMediaUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForPageTemplate(MediaEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PageFields.PageTemplateId == entity.PageTemplateId.Value };

            PageCollection pageCollection = new PageCollection();
            pageCollection.AddToTransaction(entity);
            pageCollection.GetMulti(filter, new IncludeFieldsList(PageFields.SiteId), null);

            foreach (PageEntity pageEntity in pageCollection)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteMediaUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForPointOfInterest(MediaEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.PointOfInterestId, entity.PointOfInterestId.Value, TimestampFields.ModifiedPointOfInterestMediaUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForProduct(ProductEntity entity, ITransaction transaction)
        {
            if (entity.BrandId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForBrandProductsMedia(entity.BrandId.Value, transaction);
            }
            else if (entity.CompanyId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForProductsMedia(entity.CompanyId.Value, transaction);
            }
        }

        internal void UpdateTimestampForProductgroup(ProductgroupEntity entity, ITransaction transaction)
        {
            new MenuTimestampUpdater().UpdateTimestampForProductgroupsMedia(entity.CompanyId, transaction);
        }

        internal void UpdateTimestampForRoomControlSection(MediaEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { RoomControlSectionFields.RoomControlSectionId == entity.RoomControlSectionId.Value };
            RelationCollection relations = new RelationCollection { RoomControlSectionEntityBase.Relations.RoomControlAreaEntityUsingRoomControlAreaId };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationMediaUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForRoomControlSectionItem(MediaEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { RoomControlSectionItemFields.RoomControlSectionItemId == entity.RoomControlSectionItemId.Value };
            RelationCollection relations = new RelationCollection 
            { 
                RoomControlSectionItemEntityBase.Relations.RoomControlSectionEntityUsingRoomControlSectionId,
                RoomControlSectionEntityBase.Relations.RoomControlAreaEntityUsingRoomControlAreaId 
            };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationMediaUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForSite(MediaEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.SiteId, entity.SiteId.Value, TimestampFields.ModifiedSiteMediaUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForStation(MediaEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { StationFields.StationId == entity.StationId.Value };
            RelationCollection relations = new RelationCollection 
            { 
                StationEntityBase.Relations.StationListEntityUsingStationListId,
                StationListEntityBase.Relations.RoomControlSectionItemEntityUsingStationListId,
                RoomControlSectionItemEntityBase.Relations.RoomControlSectionEntityUsingRoomControlSectionId,
                RoomControlSectionEntityBase.Relations.RoomControlAreaEntityUsingRoomControlAreaId 
            };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationMediaUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForUIFooterItem(MediaEntity entity)
        {
            UIFooterItemEntity uiFooterItemEntity = new UIFooterItemEntity();
            uiFooterItemEntity.AddToTransaction(entity);
            uiFooterItemEntity.FetchUsingPK(entity.UIFooterItemId.Value, null, null, new IncludeFieldsList { UIFooterItemFields.UIModeId });
            if (!uiFooterItemEntity.IsNew)
            {
                this.UpdateTimestampField(TimestampFields.UIModeId, uiFooterItemEntity.UIModeId, TimestampFields.ModifiedUIModeMediaUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForUITheme(MediaEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.UIThemeId, entity.UIThemeId.Value, TimestampFields.ModifiedUIThemeMediaUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForUIWidget(MediaEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { UIWidgetFields.UIWidgetId == entity.UIWidgetId.Value };
            RelationCollection relations = new RelationCollection { UIWidgetEntityBase.Relations.UITabEntityUsingUITabId };

            UITabCollection uITabCollection = new UITabCollection();
            uITabCollection.AddToTransaction(entity);
            uITabCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(UITabFields.UIModeId), 0, 0);

            foreach (UITabEntity uiTabEntity in uITabCollection)
            {
                this.UpdateTimestampField(TimestampFields.UIModeId, uiTabEntity.UIModeId, TimestampFields.ModifiedUIModeMediaUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForDeliverypointgroup(MediaEntity entity)
        {
            DeliverypointgroupEntity deliverypointgroupEntity = new DeliverypointgroupEntity();
            deliverypointgroupEntity.AddToTransaction(entity);
            deliverypointgroupEntity.FetchUsingPK(entity.DeliverypointgroupId.Value, null, null, new ExcludeIncludeFieldsList { DeliverypointgroupFields.ClientConfigurationId });

            if (!deliverypointgroupEntity.IsNew && deliverypointgroupEntity.ClientConfigurationId.HasValue)
            {
                UpdateTimestampField(TimestampFields.ClientConfigurationId, deliverypointgroupEntity.ClientConfigurationId.Value, TimestampFields.ModifiedClientConfigurationMediaUTC, entity.GetCurrentTransaction());
            }            
        }

        internal void UpdateTimestampForMediaRelationships(MediaEntity entity, bool wasNew)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(MediaRelationshipFields.MediaId == entity.MediaId);

            MediaRelationshipCollection mediaRelationshipCollection = new MediaRelationshipCollection();
            mediaRelationshipCollection.AddToTransaction(entity);
            mediaRelationshipCollection.GetMulti(filter);

            foreach (MediaRelationshipEntity mediaRelationship in mediaRelationshipCollection)
            {
                if (mediaRelationship?.TimestampUpdater != null)
                {
                    mediaRelationship.TimestampUpdater.UpdateTimestamps((IEntity)mediaRelationship, wasNew);
                }
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(MediaEntity entity, bool wasNew)
        {
            if (entity.AdvertisementId.HasValue)
            {
                this.UpdateTimestampForAdvertisement(entity);
            }
            else if (entity.AlterationId.HasValue)
            {
                this.UpdateTimestampForAlterations(entity.AlterationEntity, entity.GetCurrentTransaction());
            }
            else if (entity.AlterationoptionId.HasValue)
            {
                this.UpdateTimestampForAlterationoption(entity.AlterationoptionEntity, entity.GetCurrentTransaction());
            }
            else if (entity.AttachmentId.HasValue)
            {
                this.UpdateTimestampForAttachment(entity);
            }
            else if (entity.CategoryId.HasValue)
            {
                this.UpdateTimestampForCategory(entity.CategoryEntity, entity.GetCurrentTransaction());
            }
            else if (entity.ClientConfigurationId.HasValue)
            {
                this.UpdateTimestampForClientConfiguration(entity);
            }
            else if (entity.CompanyId.HasValue)
            {
                this.UpdateTimestampForCompany(entity);
            }
            else if (entity.EntertainmentId.HasValue)
            {
                this.UpdateTimestampForEntertainment(entity);
            }
            else if (entity.PageElementId.HasValue)
            {
                this.UpdateTimestampForPageElement(entity);
            }
            else if (entity.PageId.HasValue)
            {
                this.UpdateTimestampForPage(entity);
            }
            else if (entity.PageTemplateElementId.HasValue)
            {
                this.UpdateTimestampForPageTemplateElement(entity);
            }
            else if (entity.PageTemplateId.HasValue)
            {
                this.UpdateTimestampForPageTemplate(entity);
            }
            else if (entity.PointOfInterestId.HasValue)
            {
                this.UpdateTimestampForPointOfInterest(entity);
            }
            else if (entity.ProductId.HasValue)
            {
                this.UpdateTimestampForProduct(entity.ProductEntity, entity.GetCurrentTransaction());
            }
            else if (entity.ProductgroupId.HasValue)
            {
                this.UpdateTimestampForProductgroup(entity.ProductgroupEntity, entity.GetCurrentTransaction());
            }
            else if (entity.RoomControlSectionId.HasValue)
            {
                this.UpdateTimestampForRoomControlSection(entity);
            }
            else if (entity.RoomControlSectionItemId.HasValue)
            {
                this.UpdateTimestampForRoomControlSectionItem(entity);
            }
            else if (entity.SiteId.HasValue)
            {
                this.UpdateTimestampForSite(entity);
            }
            else if (entity.StationId.HasValue)
            {
                this.UpdateTimestampForStation(entity);
            }
            else if (entity.UIFooterItemId.HasValue)
            {
                this.UpdateTimestampForUIFooterItem(entity);
            }
            else if (entity.UIThemeId.HasValue)
            {
                this.UpdateTimestampForUITheme(entity);
            }
            else if (entity.UIWidgetId.HasValue)
            {
                this.UpdateTimestampForUIWidget(entity);
            }
            else if (entity.DeliverypointgroupId.HasValue)
            {
                UpdateTimestampForDeliverypointgroup(entity);
            }
        }

        #endregion
    }
}
