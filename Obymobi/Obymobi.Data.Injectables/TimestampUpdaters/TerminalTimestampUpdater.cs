using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(TerminalEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class TerminalTimestampUpdater : GeneralTimestampUpdater<TerminalEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    TerminalFields.TerminalId,
                    TerminalFields.Type,
                    TerminalFields.Name,
                    TerminalFields.ResetTimeout,
                    TerminalFields.ForwardToTerminalId,
                    TerminalFields.PmsTerminalOfflineNotificationEnabled,
                    TerminalFields.UIModeId,
                    TerminalFields.PrintingEnabled,
                    TerminalFields.UseHardKeyboard,
                    TerminalFields.MaxVolume,
                    TerminalFields.TerminalConfigurationId,
                    TerminalFields.MaxProcessTime
                };
            }
        }

        private bool ShouldUpdateTimestampForTerminal { get; set; }
        private bool ShouldUpdateTimestampForCompany { get; set; }
        private bool ShouldUpdateTimestampForTerminalConfiguration { get; set; }

        #endregion

        #region Methods

        public override bool RelevantFieldsChanged(IEntity entity)
        {
            this.ShouldUpdateTimestampForTerminal = entity.Fields[TerminalFields.Name.FieldIndex].IsChanged ||
                                                    entity.Fields[TerminalFields.Type.FieldIndex].IsChanged ||
                                                    entity.Fields[TerminalFields.ResetTimeout.FieldIndex].IsChanged ||
                                                    entity.Fields[TerminalFields.ForwardToTerminalId.FieldIndex].IsChanged ||
                                                    entity.Fields[TerminalFields.PmsTerminalOfflineNotificationEnabled.FieldIndex].IsChanged ||
                                                    entity.Fields[TerminalFields.MaxProcessTime.FieldIndex].IsChanged;
            this.ShouldUpdateTimestampForCompany = entity.Fields[TerminalFields.Name.FieldIndex].IsChanged;
            this.ShouldUpdateTimestampForTerminalConfiguration = entity.Fields[TerminalFields.UIModeId.FieldIndex].IsChanged ||
                                                                 entity.Fields[TerminalFields.PrintingEnabled.FieldIndex].IsChanged ||
                                                                 entity.Fields[TerminalFields.UseHardKeyboard.FieldIndex].IsChanged ||
                                                                 entity.Fields[TerminalFields.MaxVolume.FieldIndex].IsChanged;

            return base.RelevantFieldsChanged(entity);
        }

        public override void UpdateTimestamps(IEntity entity, bool wasNew)
        {
            TerminalEntity useableEntity = entity as TerminalEntity;
            if (useableEntity == null)
                return;

            if (this.ShouldUpdateTimestampForTerminal)
            {
                this.UpdateTimestampForTerminal(useableEntity);
            }

            if (this.ShouldUpdateTimestampForCompany)
            {
                this.UpdateTimestampForCompany(useableEntity);
            }

            if (useableEntity.TerminalConfigurationId.HasValue && this.ShouldUpdateTimestampForTerminalConfiguration)
            {
                this.UpdateTimestampForTerminalConfiguration(useableEntity);
            }
        }

        internal void UpdateTimestampForTerminal(TerminalEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.TerminalId, entity.TerminalId, TimestampFields.ModifiedTerminalUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForCompany(TerminalEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId, TimestampFields.ModifiedCompanyUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForTerminalConfiguration(TerminalEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.TerminalConfigurationId, entity.TerminalConfigurationId, TimestampFields.ModifiedTerminalConfigurationUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(TerminalEntity entity, bool wasNew)
        {
            this.UpdateTimestamps(entity, wasNew);
        }

        #endregion
    }
}
