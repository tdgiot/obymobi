using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(CategoryAlterationEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class CategoryAlterationTimestampUpdater : GeneralTimestampUpdater<CategoryAlterationEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(CategoryAlterationEntity entity, bool wasNew)
        {
            if (entity.CategoryEntity.MenuId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForMenu(entity.CategoryEntity.MenuId.Value, entity.GetCurrentTransaction());
            }
        }

        #endregion
    }
}
