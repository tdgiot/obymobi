using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(RouteEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class RouteTimestampUpdater : GeneralTimestampUpdater<RouteEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(RouteEntity entity, bool wasNew)
        {
            // TODO NEW API
        }

        #endregion
    }
}
