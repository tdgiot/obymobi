using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(StationListEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class StationListTimestampUpdater : GeneralTimestampUpdater<StationListEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    StationListFields.Name
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForStationList(StationListEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { StationListFields.StationListId == entity.StationListId };
            RelationCollection relations = new RelationCollection 
            { 
                StationListEntity.Relations.RoomControlSectionItemEntityUsingStationListId,
                RoomControlSectionItemEntity.Relations.RoomControlSectionEntityUsingRoomControlSectionId,
                RoomControlSectionEntity.Relations.RoomControlAreaEntityUsingRoomControlAreaId 
            };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(StationListEntity entity, bool wasNew)
        {
            this.UpdateTimestampForStationList(entity);
        }

        #endregion
    }
}
