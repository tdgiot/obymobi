using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ProductEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ProductTimestampUpdater : GeneralTimestampUpdater<ProductEntity>
    {
        protected override IEnumerable<EntityField> Fields =>
            new List<EntityField>
            {
                ProductFields.AllowFreeText,
                ProductFields.ButtonText,
                ProductFields.Color,
                ProductFields.CustomizeButtonText,
                ProductFields.DeliveryLocationType,
                ProductFields.Geofencing,
                ProductFields.Name,
                ProductFields.PriceIn,
                ProductFields.ProductgroupId,
                ProductFields.ScheduleId,
                ProductFields.SubType,
                ProductFields.Type,
                ProductFields.ViewLayoutType,
                ProductFields.VisibilityType,
                ProductFields.WebTypeSmartphoneUrl,
                ProductFields.WebTypeTabletUrl,
                ProductFields.Description,
                ProductFields.AnnouncementAction,
                ProductFields.PointOfInterestId,
                ProductFields.WebLinkType,
                ProductFields.OpenNewWindow
            };

        internal void UpdateTimestampForProductgroups(ProductEntity entity, bool wasNew)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductgroupItemFields.ProductId == entity.ProductId);

            ProductgroupItemCollection productgroupItemCollection = new ProductgroupItemCollection();
            productgroupItemCollection.AddToTransaction(entity);
            productgroupItemCollection.GetMulti(filter);

            foreach (ProductgroupItemEntity productgroupItem in productgroupItemCollection)
            {
                ITimestampEntity timestampEntity = productgroupItem.ProductgroupEntity;
                if (timestampEntity != null && timestampEntity.TimestampUpdater != null)
                {
                    timestampEntity.TimestampUpdater.UpdateTimestamps((IEntity)timestampEntity, wasNew);
                }
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(ProductEntity entity, bool wasNew)
        {
            if (entity.BrandId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForBrandProducts(entity.BrandId.Value, entity.GetCurrentTransaction());
            }
            else if (entity.CompanyId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForProducts(entity.CompanyId.Value, entity.GetCurrentTransaction());
            }

            if (IsFieldChanged(ProductFields.AnnouncementAction))
            {
                new AnnouncementActionTimestampUpdater().UpdateTimestampForAnnouncementAction(entity.CompanyId.Value, entity.GetCurrentTransaction());
            }

            if (entity.ScheduleId.HasValue && this.IsFieldChanged(ProductFields.ScheduleId))
            {
                // If the schedule field is changed, check if any other product is linked to this scheduleTimestampFields.
                // When a new schedule is added to the system and linked to a product, the schedule is not marked as modified
                // on the menu and therefor not published with the menu. If a change was made to the schedule while it's linked
                // to a product everything works fine.
                // This fix checks if there's only 1 product linked to the schedule and will then trigger the ScheduleTimestampUpdater.
                // The same is done in CategoryTimestampUpdater.
                // 
                // TODO: Change schedules to be published separate from the menu
                PredicateExpression filter = new PredicateExpression(ProductFields.ScheduleId == entity.ScheduleId);
                int productWithScheduleCount = new ProductCollection().GetDbCount(filter);
                if (productWithScheduleCount == 1)
                {
                    new ScheduleTimestampUpdater().UpdateTimestampForSchedule(entity.ScheduleEntity);
                }
            }

            this.UpdateTimestampForProductgroups(entity, wasNew);
        }
    }
}
