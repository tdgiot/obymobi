﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(CompanyAmenityEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class CompanyAmenityTimestampUpdater : GeneralTimestampUpdater<CompanyAmenityEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    CompanyAmenityFields.CompanyId,
                    CompanyAmenityFields.AmenityId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForAmenity(CompanyAmenityEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId, TimestampFields.ModifiedAmenitiesUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(CompanyAmenityEntity entity, bool wasNew)
        {
            this.UpdateTimestampForAmenity(entity);
        }

        #endregion
    }
}
