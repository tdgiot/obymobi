using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(CompanyEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class CompanyTimestampUpdater : GeneralTimestampUpdater<CompanyEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    CompanyFields.ApiVersion,
                    CompanyFields.City,
                    CompanyFields.ClockMode,
                    CompanyFields.CometHandlerType,
                    CompanyFields.CompanyId,
                    CompanyFields.CountryCode,
                    CompanyFields.CultureCode,
                    CompanyFields.CurrencyCode,
                    CompanyFields.Description,
                    CompanyFields.GoogleAnalyticsId,
                    CompanyFields.Latitude,
                    CompanyFields.Longitude,
                    CompanyFields.MobileOrderingDisabled,
                    CompanyFields.Name,
                    CompanyFields.Pincode,
                    CompanyFields.PincodeGM,
                    CompanyFields.PincodeSU,
                    CompanyFields.PriceFormatType,
                    CompanyFields.ShowCurrencySymbol,
                    CompanyFields.SystemPassword,
                    CompanyFields.TemperatureUnit,
                    CompanyFields.TimeZoneOlsonId
                };
            }
        }

        private bool ShouldUpdateTimestampForTerminalConfiguration { get; set; }

        private bool ShouldUpdateTimestampForClientConfiguration { get; set; }

        #endregion

        #region Methods

        public override bool RelevantFieldsChanged(IEntity entity)
        {
            this.ShouldUpdateTimestampForTerminalConfiguration = entity.Fields[CompanyFields.Pincode.FieldIndex].IsChanged || 
                                                                 entity.Fields[CompanyFields.PincodeSU.FieldIndex].IsChanged || 
                                                                 entity.Fields[CompanyFields.PincodeGM.FieldIndex].IsChanged;

            this.ShouldUpdateTimestampForClientConfiguration = entity.Fields[CompanyFields.ShowCurrencySymbol.FieldIndex].IsChanged ||
                                                               entity.Fields[CompanyFields.DeviceRebootMethod.FieldIndex].IsChanged ||
                                                               entity.Fields[CompanyFields.AllowFreeText.FieldIndex].IsChanged;

            return base.RelevantFieldsChanged(entity);
        }

        public override void UpdateTimestamps(IEntity entity, bool wasNew)
        {
            CompanyEntity useableEntity = entity as CompanyEntity;
            if (useableEntity == null)
                return;

            this.UpdateTimestampForCompany(useableEntity);

            if (this.ShouldUpdateTimestampForTerminalConfiguration)
            {
                foreach (TerminalConfigurationEntity terminalConfigurationEntity in useableEntity.TerminalConfigurationCollection)
                {
                    this.UpdateTimestampForTerminalConfiguration(terminalConfigurationEntity);
                }
            }

            if (this.ShouldUpdateTimestampForClientConfiguration)
            {
                foreach (ClientConfigurationEntity clientConfigurationEntity in useableEntity.ClientConfigurationCollection)
                {
                    this.UpdateTimestampForClientConfiguration(clientConfigurationEntity);
                }
            }
        }

        internal void UpdateTimestampForCompany(CompanyEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId, TimestampFields.ModifiedCompanyUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForTerminalConfiguration(TerminalConfigurationEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.TerminalConfigurationId, entity.TerminalConfigurationId, TimestampFields.ModifiedTerminalConfigurationUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForClientConfiguration(ClientConfigurationEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.ClientConfigurationId, entity.ClientConfigurationId, TimestampFields.ModifiedClientConfigurationUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(CompanyEntity entity, bool wasNew)
        {
            this.UpdateTimestampForCompany(entity);
        }

        #endregion
    }
}
