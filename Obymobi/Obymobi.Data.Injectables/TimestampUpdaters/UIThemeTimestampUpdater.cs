using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(UIThemeEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class UIThemeTimestampUpdater : GeneralTimestampUpdater<UIThemeEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    UIThemeFields.UIThemeId,
                    UIThemeFields.Name
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForUITheme(UIThemeEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.UIThemeId, entity.UIThemeId, TimestampFields.ModifiedUIThemeUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(UIThemeEntity entity, bool wasNew)
        {
            this.UpdateTimestampForUITheme(entity);
        }

        #endregion
    }
}
