using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(RoomControlSectionEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class RoomControlSectionTimestampUpdater : GeneralTimestampUpdater<RoomControlSectionEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    RoomControlSectionFields.RoomControlAreaId,
                    RoomControlSectionFields.Name,
                    RoomControlSectionFields.Type,
                    RoomControlSectionFields.SortOrder,
                    RoomControlSectionFields.Visible,
                    RoomControlSectionFields.FieldValue1,
                    RoomControlSectionFields.FieldValue2,
                    RoomControlSectionFields.FieldValue3,
                    RoomControlSectionFields.FieldValue4,
                    RoomControlSectionFields.FieldValue5,
                    RoomControlSectionFields.ParentCompanyId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForRoomControlSection(RoomControlSectionEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { RoomControlSectionFields.RoomControlSectionId == entity.RoomControlSectionId };
            RelationCollection relations = new RelationCollection { RoomControlSectionEntity.Relations.RoomControlAreaEntityUsingRoomControlAreaId };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(RoomControlSectionEntity entity, bool wasNew)
        {
            this.UpdateTimestampForRoomControlSection(entity);
        }

        #endregion
    }
}
