﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(AvailabilityEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class AvailabilityTimestampUpdater : GeneralTimestampUpdater<AvailabilityEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    AvailabilityFields.AvailabilityId,
                    AvailabilityFields.Name,
                    AvailabilityFields.Status,
                    AvailabilityFields.ActionEntertainmentId,
                    AvailabilityFields.ActionCategoryId,
                    AvailabilityFields.ActionProductCategoryId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForAvailability(AvailabilityEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId, TimestampFields.ModifiedAvailabilitiesUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(AvailabilityEntity entity, bool wasNew)
        {
            this.UpdateTimestampForAvailability(entity);
        }

        #endregion
    }
}
