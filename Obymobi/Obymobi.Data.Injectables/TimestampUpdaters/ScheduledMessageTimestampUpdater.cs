using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ScheduledMessageEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ScheduledMessageTimestampUpdater : GeneralTimestampUpdater<ScheduledMessageEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(ScheduledMessageEntity entity, bool wasNew)
        {
            // TODO NEW API
        }

        #endregion
    }
}
