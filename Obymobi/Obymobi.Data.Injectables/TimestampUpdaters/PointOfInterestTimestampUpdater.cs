using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(PointOfInterestEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class PointOfInterestTimestampUpdater : GeneralTimestampUpdater<PointOfInterestEntity>
    {
        internal void UpdateTimestampForPointOfInterest(PointOfInterestEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.PointOfInterestId, entity.PointOfInterestId, TimestampFields.ModifiedPointOfInterestUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampsForPointOfInterestMaps(PointOfInterestEntity entity)
        {
            PredicateExpression filter = new PredicateExpression(MapPointOfInterestFields.PointOfInterestId == entity.PointOfInterestId);

            MapPointOfInterestCollection mapPointOfInterestCollection = new MapPointOfInterestCollection();
            mapPointOfInterestCollection.AddToTransaction(entity);
            mapPointOfInterestCollection.GetMulti(filter, new IncludeFieldsList(MapPointOfInterestFields.MapId), null);

            foreach (MapPointOfInterestEntity mapPointOfInterestEntity in mapPointOfInterestCollection)
            {
                this.UpdateTimestampField(TimestampFields.MapId, mapPointOfInterestEntity.MapId, TimestampFields.ModifiedMapUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(PointOfInterestEntity entity, bool wasNew)
        {
            this.UpdateTimestampForPointOfInterest(entity);
            this.UpdateTimestampsForPointOfInterestMaps(entity);
        }
    }
}
