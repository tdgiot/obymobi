using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(AlterationitemAlterationEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class AlterationitemAlterationTimestampUpdater : GeneralTimestampUpdater<AlterationitemAlterationEntity>
    {
        #region Methods

        protected override void UpdateTimestamps(AlterationitemAlterationEntity entity, bool wasNew)
        {
            if (entity.AlterationitemEntity.ParentBrandId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForBrandAlterations(entity.AlterationitemEntity.ParentBrandId.Value, entity.GetCurrentTransaction());
            }
            else if (entity.AlterationitemEntity.ParentCompanyId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForAlterations(entity.AlterationitemEntity.ParentCompanyId.Value, entity.GetCurrentTransaction());
            }
        }

        #endregion
    }
}
