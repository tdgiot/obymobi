using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ProductAttachmentEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ProductAttachmentTimestampUpdater : GeneralTimestampUpdater<ProductAttachmentEntity>
    {
        #region Methods

        internal void UpdateTimestampForProduct(ProductAttachmentEntity entity)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductCategoryFields.ProductId == entity.ProductId);
            filter.Add(CategoryFields.MenuId != DBNull.Value);

            RelationCollection relations = new RelationCollection(ProductCategoryEntityBase.Relations.CategoryEntityUsingCategoryId);

            this.UpdateTimestampForProduct(entity, filter, relations);
        }

        internal void UpdateTimestampForProduct(IEntity entity, PredicateExpression filter, RelationCollection relations)
        {
            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.AddToTransaction(entity);
            categoryCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(CategoryFields.MenuId), 0, 0);

            foreach (CategoryEntity categoryEntity in categoryCollection)
            {
                if (categoryEntity.MenuId.HasValue)
                {
                    this.UpdateTimestampField(TimestampFields.MenuId, categoryEntity.MenuId.Value, TimestampFields.ModifiedProductsUTC, entity.GetCurrentTransaction());
                    this.UpdateTimestampField(TimestampFields.MenuId, categoryEntity.MenuId.Value, TimestampFields.ModifiedProductsMediaUTC, entity.GetCurrentTransaction());
                    this.UpdateTimestampField(TimestampFields.MenuId, categoryEntity.MenuId.Value, TimestampFields.ModifiedProductsCustomTextUTC, entity.GetCurrentTransaction());
                }
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(ProductAttachmentEntity entity, bool wasNew)
        {
            this.UpdateTimestampForProduct(entity);
        }

        #endregion
    }
}
