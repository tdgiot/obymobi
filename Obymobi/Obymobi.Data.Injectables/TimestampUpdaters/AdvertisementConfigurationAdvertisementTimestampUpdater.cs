using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(AdvertisementConfigurationAdvertisementEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class AdvertisementConfigurationAdvertisementTimestampUpdater : GeneralTimestampUpdater<AdvertisementConfigurationAdvertisementEntity>
    {
        #region Methods

        internal void UpdateTimestampsForAdvertisement(AdvertisementConfigurationAdvertisementEntity entity)
        {
            PredicateExpression filter = new PredicateExpression(AdvertisementConfigurationAdvertisementFields.AdvertisementId == entity.AdvertisementId);

            AdvertisementConfigurationAdvertisementCollection advertisementConfigurationAdvertisementCollection = new AdvertisementConfigurationAdvertisementCollection();
            advertisementConfigurationAdvertisementCollection.AddToTransaction(entity);
            advertisementConfigurationAdvertisementCollection.GetMulti(filter, new IncludeFieldsList(AdvertisementConfigurationAdvertisementFields.AdvertisementConfigurationId, AdvertisementConfigurationAdvertisementFields.ParentCompanyId), null);

            foreach (AdvertisementConfigurationAdvertisementEntity advertisementConfigurationAdvertisementEntity in advertisementConfigurationAdvertisementCollection)
            {
                this.UpdateTimestampField(TimestampFields.AdvertisementConfigurationId, advertisementConfigurationAdvertisementEntity.AdvertisementConfigurationId, TimestampFields.ModifiedAdvertisementConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(AdvertisementConfigurationAdvertisementEntity entity, bool wasNew)
        {
            this.UpdateTimestampsForAdvertisement(entity);
        }

        #endregion
    }
}
