using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(UIThemeColorEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class UIThemeColorTimestampUpdater : GeneralTimestampUpdater<UIThemeColorEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    UIThemeColorFields.UIThemeColorId,
                    UIThemeColorFields.Type,
                    UIThemeColorFields.Color,
                    UIThemeColorFields.ParentCompanyId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForUITheme(UIThemeColorEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.UIThemeId, entity.UIThemeId, TimestampFields.ModifiedUIThemeUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(UIThemeColorEntity entity, bool wasNew)
        {
            this.UpdateTimestampForUITheme(entity);
        }

        #endregion
    }
}
