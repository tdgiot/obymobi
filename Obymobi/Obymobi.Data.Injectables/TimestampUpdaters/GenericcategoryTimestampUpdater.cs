using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(GenericcategoryEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class GenericcategoryTimestampUpdater : GeneralTimestampUpdater<GenericcategoryEntity>
    {
        #region Methods

        internal void UpdateTimestampsForGenericcategory(GenericcategoryEntity entity)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.GenericcategoryId == entity.GenericcategoryId);
            filter.Add(CategoryFields.MenuId != DBNull.Value);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.AddToTransaction(entity);
            categoryCollection.GetMulti(filter, new IncludeFieldsList(CategoryFields.MenuId), null);

            foreach (CategoryEntity categoryEntity in categoryCollection)
            {
                if (categoryEntity.MenuId.HasValue)
                    this.UpdateTimestampField(TimestampFields.MenuId, categoryEntity.MenuId.Value, TimestampFields.ModifiedMenuUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(GenericcategoryEntity entity, bool wasNew)
        {
            this.UpdateTimestampsForGenericcategory(entity);
        }

        #endregion
    }
}
