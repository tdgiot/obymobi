using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MediaRatioTypeMediaEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MediaRatioTypeMediaTimestampUpdater : GeneralTimestampUpdater<MediaRatioTypeMediaEntity>
    {
        protected override void UpdateTimestamps(MediaRatioTypeMediaEntity entity, bool wasNew)
        {
            ITimestampEntity timestampEntity = entity.MediaEntity;
            if (timestampEntity?.TimestampUpdater != null)
            {
                timestampEntity.TimestampUpdater.UpdateTimestamps((IEntity)timestampEntity, wasNew);
            }
        }
    }
}
