using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(CompanyReleaseEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class CompanyReleaseTimestampUpdater : GeneralTimestampUpdater<CompanyReleaseEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    CompanyReleaseFields.CompanyId,
                    CompanyReleaseFields.ReleaseId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForCompanyReleases(CompanyReleaseEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId, TimestampFields.ModifiedCompanyReleasesUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(CompanyReleaseEntity entity, bool wasNew)
        {
            this.UpdateTimestampForCompanyReleases(entity);
        }

        #endregion
    }
}
