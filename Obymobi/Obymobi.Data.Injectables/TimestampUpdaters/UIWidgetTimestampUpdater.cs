using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(UIWidgetEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class UiWidgetTimestampUpdater : GeneralTimestampUpdater<UIWidgetEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    UIWidgetFields.UIWidgetId,
                    UIWidgetFields.UITabId,
                    UIWidgetFields.Name,
                    UIWidgetFields.Type,
                    UIWidgetFields.SortOrder,
                    UIWidgetFields.AdvertisementId,
                    UIWidgetFields.ProductCategoryId,
                    UIWidgetFields.CategoryId,
                    UIWidgetFields.EntertainmentId,
                    UIWidgetFields.EntertainmentcategoryId,
                    UIWidgetFields.SiteId,
                    UIWidgetFields.PageId,
                    UIWidgetFields.RoomControlType,
                    UIWidgetFields.RoomControlAreaId,
                    UIWidgetFields.RoomControlSectionId,
                    UIWidgetFields.UITabType,
                    UIWidgetFields.Url,
                    UIWidgetFields.ParentCompanyId,
                    UIWidgetFields.IsVisible,
                    UIWidgetFields.FieldValue1,
                    UIWidgetFields.FieldValue2,
                    UIWidgetFields.FieldValue3,
                    UIWidgetFields.FieldValue4,
                    UIWidgetFields.FieldValue5,
                    UIWidgetFields.MessageLayoutType
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForUIWidget(UIWidgetEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { UIWidgetFields.UIWidgetId == entity.UIWidgetId };
            RelationCollection relations = new RelationCollection { UIWidgetEntity.Relations.UITabEntityUsingUITabId };

            UITabCollection uITabCollection = new UITabCollection();
            uITabCollection.AddToTransaction(entity);
            uITabCollection.GetMulti(filter, 0, null, relations, null,new IncludeFieldsList(UITabFields.UIModeId), 0, 0);

            foreach (UITabEntity uiTabEntity in uITabCollection)
            {
                this.UpdateTimestampField(TimestampFields.UIModeId, uiTabEntity.UIModeId, TimestampFields.ModifiedUIModeUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(UIWidgetEntity entity, bool wasNew)
        {
            this.UpdateTimestampForUIWidget(entity);
        }

        #endregion
    }
}
