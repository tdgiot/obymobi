using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MessageEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MessageTimestampUpdater : GeneralTimestampUpdater<MessageEntity>
    {
        #region Methods

        internal void UpdateTimestampForMessages(MessageEntity entity)
        {
            foreach (MessageRecipientEntity messageRecipientEntity in entity.MessageRecipientCollection)
            {
                if (!entity.DeliverypointId.HasValue)
                    continue;

                this.UpdateTimestampField(TimestampFields.DeliverypointId, messageRecipientEntity.DeliverypointId.Value, TimestampFields.ModifiedMessagesUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(MessageEntity entity, bool wasNew)
        {
            this.UpdateTimestampForMessages(entity);
        }

        #endregion
    }
}
