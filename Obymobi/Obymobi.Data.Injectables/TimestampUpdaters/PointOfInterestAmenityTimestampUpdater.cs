﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(PointOfInterestAmenityEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class PointOfInterestAmenityTimestampUpdater : GeneralTimestampUpdater<PointOfInterestAmenityEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    PointOfInterestAmenityFields.PointOfInterestId,
                    PointOfInterestAmenityFields.AmenityId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForAmenity(PointOfInterestAmenityEntity entity)
        {
            //PointOfInterestEntity pointOfInterestEntity = new PointOfInterestEntity();
            //pointOfInterestEntity.PointOfInterestId = pointOfInterestId;
            //pointOfInterestEntity.ModifiedAmenitiesUTC = DateTime.UtcNow; // TODO NEW API Create field
            //pointOfInterestEntity.IsNew = false;
            //pointOfInterestEntity.Save();            
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(PointOfInterestAmenityEntity entity, bool wasNew)
        {
            this.UpdateTimestampForAmenity(entity);
        }

        #endregion
    }
}
