using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(RoutestepEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class RoutestepTimestampUpdater : GeneralTimestampUpdater<RoutestepEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(RoutestepEntity entity, bool wasNew)
        {
            // TODO NEW API
        }

        #endregion
    }
}
