using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MediaRelationshipEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MediaRelationshipTimestampUpdater : GeneralTimestampUpdater<MediaRelationshipEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(MediaRelationshipEntity entity, bool wasNew)
        {
            if (entity.ProductId.HasValue)
            {
                if (entity.ProductEntity.CompanyId.HasValue)
                {
                    new MenuTimestampUpdater().UpdateTimestampForProductsMedia(entity.ProductEntity.CompanyId.Value, entity.GetCurrentTransaction());
                }
            }
        }

        #endregion
    }
}
