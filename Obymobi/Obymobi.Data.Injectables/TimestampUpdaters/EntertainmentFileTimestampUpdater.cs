using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(EntertainmentFileEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class EntertainmentFileTimestampUpdater : GeneralTimestampUpdater<EntertainmentFileEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    EntertainmentFileFields.Blob,
                    EntertainmentFileFields.Filename
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForEntertainmentConfigurations(EntertainmentFileEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { EntertainmentFileFields.EntertainmentFileId == entity.EntertainmentFileId };
            RelationCollection relations = new RelationCollection
            {
                EntertainmentFileEntity.Relations.EntertainmentEntityUsingEntertainmentFileId,
                EntertainmentEntity.Relations.EntertainmentConfigurationEntertainmentEntityUsingEntertainmentId,
                EntertainmentConfigurationEntertainmentEntity.Relations.EntertainmentConfigurationEntityUsingEntertainmentConfigurationId
            };

            EntertainmentConfigurationCollection entertainmentConfigurationCollection = new EntertainmentConfigurationCollection();
            entertainmentConfigurationCollection.AddToTransaction(entity);
            entertainmentConfigurationCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(EntertainmentConfigurationFields.EntertainmentConfigurationId), 0, 0);

            foreach (EntertainmentConfigurationEntity entertainmentConfigurationEntity in entertainmentConfigurationCollection)
            {
                this.UpdateTimestampField(TimestampFields.EntertainmentConfigurationId, entertainmentConfigurationEntity.EntertainmentConfigurationId, TimestampFields.ModifiedEntertainmentConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(EntertainmentFileEntity entity, bool wasNew)
        {
            this.UpdateTimestampForEntertainmentConfigurations(entity);
        }

        #endregion
    }
}
