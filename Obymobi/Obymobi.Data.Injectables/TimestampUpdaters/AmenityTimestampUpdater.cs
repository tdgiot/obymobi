﻿using System;
using System.Collections.Generic;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(AmenityEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class AmenityTimestampUpdater : GeneralTimestampUpdater<AmenityEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    AmenityFields.AmenityId,
                    AmenityFields.Name
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForAmenity(AmenityEntity entity)
        {
            PredicateExpression filter = new PredicateExpression(CompanyAmenityFields.AmenityId == entity.AmenityId);

            CompanyAmenityCollection companyAmenityCollection = new CompanyAmenityCollection();
            companyAmenityCollection.AddToTransaction(entity);
            companyAmenityCollection.GetMulti(filter, new IncludeFieldsList { CompanyAmenityFields.CompanyId }, null);

            foreach (CompanyAmenityEntity companyAmenityEntity in companyAmenityCollection)
            {
                this.UpdateTimestampField(TimestampFields.CompanyId, companyAmenityEntity.CompanyId, TimestampFields.ModifiedAmenitiesUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(AmenityEntity entity, bool wasNew)
        {
            this.UpdateTimestampForAmenity(entity);
        }

        #endregion
    }
}
