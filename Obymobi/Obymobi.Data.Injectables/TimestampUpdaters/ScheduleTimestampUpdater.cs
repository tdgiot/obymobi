using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ScheduleEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ScheduleTimestampUpdater : GeneralTimestampUpdater<ScheduleEntity>
    {
        #region Methods

        internal void UpdateTimestampForSchedule(ScheduleEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { ScheduleFields.ScheduleId == entity.ScheduleId };
            RelationCollection relations = new RelationCollection
            {
                ScheduleEntity.Relations.ProductEntityUsingScheduleId,
                ProductEntity.Relations.ProductCategoryEntityUsingProductId,
                ProductCategoryEntity.Relations.CategoryEntityUsingCategoryId,
                CategoryEntity.Relations.MenuEntityUsingMenuId
            };

            MenuCollection menuCollection = new MenuCollection();
            menuCollection.AddToTransaction(entity);
            menuCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(MenuFields.MenuId), 0, 0);

            foreach (MenuEntity menuEntity in menuCollection)
            {
                this.UpdateTimestampField(TimestampFields.MenuId, menuEntity.MenuId, TimestampFields.ModifiedSchedulesUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(ScheduleEntity entity, bool wasNew)
        {
            this.UpdateTimestampForSchedule(entity);
        }

        #endregion
    }
}
