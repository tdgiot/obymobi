using System;
using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MenuEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MenuTimestampUpdater : GeneralTimestampUpdater<MenuEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    MenuFields.MenuId,
                    MenuFields.Name
                };
            }
        }

        #endregion

        #region Methods

        public void UpdateTimestampForProducts(int companyId, ITransaction transaction)
        {
            this.UpdateTimestampsForCompany(companyId, TimestampFields.ModifiedProductsUTC, transaction);
        }

        public void UpdateTimestampForProductsCustomText(int companyId, ITransaction transaction)
        {
            this.UpdateTimestampsForCompany(companyId, TimestampFields.ModifiedProductsCustomTextUTC, transaction);
        }

        public void UpdateTimestampForProductsMedia(int companyId, ITransaction transaction)
        {
            this.UpdateTimestampsForCompany(companyId, TimestampFields.ModifiedProductsMediaUTC, transaction);
        }

        public void UpdateTimestampForAlterations(int companyId, ITransaction transaction)
        {
            this.UpdateTimestampsForCompany(companyId, TimestampFields.ModifiedAlterationsUTC, transaction);
        }

        public void UpdateTimestampForAlterationsCustomText(int companyId, ITransaction transaction)
        {
            this.UpdateTimestampsForCompany(companyId, TimestampFields.ModifiedAlterationsCustomTextUTC, transaction);
        }

        public void UpdateTimestampForAlterationsMedia(int companyId, ITransaction transaction)
        {
            this.UpdateTimestampsForCompany(companyId, TimestampFields.ModifiedAlterationsMediaUTC, transaction);
        }

        public void UpdateTimestampForProductgroups(int companyId, ITransaction transaction)
        {
            this.UpdateTimestampsForCompany(companyId, TimestampFields.ModifiedProductgroupsUTC, transaction);
        }

        public void UpdateTimestampForProductgroupsCustomText(int companyId, ITransaction transaction)
        {
            this.UpdateTimestampsForCompany(companyId, TimestampFields.ModifiedProductgroupsCustomTextUTC, transaction);
        }

        public void UpdateTimestampForProductgroupsMedia(int companyId, ITransaction transaction)
        {
            this.UpdateTimestampsForCompany(companyId, TimestampFields.ModifiedProductgroupsMediaUTC, transaction);
        }

        public void UpdateTimestampForBrandAlterations(int brandId, ITransaction transaction)
        {
            this.UpdateTimestampsForBrand(brandId, TimestampFields.ModifiedAlterationsUTC, transaction);
        }

        public void UpdateTimestampForBrandAlterationsCustomText(int brandId, ITransaction transaction)
        {
            this.UpdateTimestampsForBrand(brandId, TimestampFields.ModifiedAlterationsCustomTextUTC, transaction);
        }

        public void UpdateTimestampForBrandAlterationsMedia(int brandId, ITransaction transaction)
        {
            this.UpdateTimestampsForBrand(brandId, TimestampFields.ModifiedAlterationsMediaUTC, transaction);
        }

        public void UpdateTimestampForBrandProducts(int brandId, ITransaction transaction)
        {
            this.UpdateTimestampsForBrand(brandId, TimestampFields.ModifiedProductsUTC, transaction);
        }

        public void UpdateTimestampForBrandProductsCustomText(int brandId, ITransaction transaction)
        {
            this.UpdateTimestampsForBrand(brandId, TimestampFields.ModifiedProductsCustomTextUTC, transaction);
        }

        public void UpdateTimestampForBrandProductsMedia(int brandId, ITransaction transaction)
        {
            this.UpdateTimestampsForBrand(brandId, TimestampFields.ModifiedProductsMediaUTC, transaction);
        }

        public void UpdateTimestampForMenu(int menuId, ITransaction transaction)
        {
            this.UpdateTimestampField(TimestampFields.MenuId, menuId, TimestampFields.ModifiedMenuUTC, transaction);
        }

        public void UpdateTimestampForMenuCustomText(int menuId, ITransaction transaction)
        {
            this.UpdateTimestampField(TimestampFields.MenuId, menuId, TimestampFields.ModifiedMenuCustomTextUTC, transaction);
        }

        public void UpdateTimestampForMenuMedia(int menuId, ITransaction transaction)
        {
            this.UpdateTimestampField(TimestampFields.MenuId, menuId, TimestampFields.ModifiedMenuMediaUTC, transaction);
        }

        internal void UpdateTimestampsForCompany(int companyId, EntityField timestampField, ITransaction transaction)
        {
            PredicateExpression filter = new PredicateExpression(MenuFields.CompanyId == companyId);

            this.UpdateTimestampsForMenuCollection(filter, null, timestampField, transaction);
        }

        internal void UpdateTimestampsForBrand(int brandId, EntityField timestampField, ITransaction transaction)
        {
            PredicateExpression filter = new PredicateExpression(CompanyBrandFields.BrandId == brandId);
            RelationCollection relationCollection = new RelationCollection { MenuEntityBase.Relations.CompanyEntityUsingCompanyId, CompanyEntityBase.Relations.CompanyBrandEntityUsingCompanyId };

            this.UpdateTimestampsForMenuCollection(filter, relationCollection, timestampField, transaction);
        }

        internal void UpdateTimestampsForMenuCollection(PredicateExpression filter, RelationCollection relationCollection, EntityField timestampField, ITransaction transaction)
        {
            MenuCollection menuCollection = this.GetMenuCollection(filter, relationCollection, transaction);

            foreach (MenuEntity menu in menuCollection)
            {
                this.UpdateTimestampField(TimestampFields.MenuId, menu.MenuId, timestampField, transaction);
            }
        }

        internal MenuCollection GetMenuCollection(PredicateExpression filter, RelationCollection relationCollection, ITransaction transaction)
        {
            MenuCollection menuCollection = new MenuCollection();
            menuCollection.AddToTransaction(transaction);
            menuCollection.GetMulti(filter, 0, null, relationCollection, null, new IncludeFieldsList(MenuFields.MenuId), 0, 0);

            return menuCollection;
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(MenuEntity entity, bool wasNew)
        {
            this.UpdateTimestampForMenu(entity.MenuId, entity.GetCurrentTransaction());
        }

        #endregion
    }
}
