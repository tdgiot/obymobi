﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(AdvertisementConfigurationEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class AdvertisementConfigurationTimestampUpdater : GeneralTimestampUpdater<AdvertisementConfigurationEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    AdvertisementConfigurationFields.Name
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampsForAdvertisementConfiguration(AdvertisementConfigurationEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.AdvertisementConfigurationId, entity.AdvertisementConfigurationId, TimestampFields.ModifiedAdvertisementConfigurationUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(AdvertisementConfigurationEntity entity, bool wasNew)
        {
            this.UpdateTimestampsForAdvertisementConfiguration(entity);
        }

        #endregion
    }
}
