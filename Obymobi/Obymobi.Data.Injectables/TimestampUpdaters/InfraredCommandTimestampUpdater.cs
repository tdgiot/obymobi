using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(InfraredCommandEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class InfraredCommandTimestampUpdater : GeneralTimestampUpdater<InfraredCommandEntity>
    {
        #region Methods

        internal void UpdateTimestampsForInfraredCommand(InfraredCommandEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.InfraredConfigurationId, entity.InfraredConfigurationId, TimestampFields.ModifiedInfraredConfigurationUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(InfraredCommandEntity entity, bool wasNew)
        {
            this.UpdateTimestampsForInfraredCommand(entity);
        }

        #endregion
    }
}
