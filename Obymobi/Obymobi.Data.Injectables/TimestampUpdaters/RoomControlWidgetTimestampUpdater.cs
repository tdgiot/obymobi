using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(RoomControlWidgetEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class RoomControlWidgetTimestampUpdater : GeneralTimestampUpdater<RoomControlWidgetEntity>
    {
        #region Methods

        internal void UpdateTimestampForRoomControlSection(RoomControlWidgetEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { RoomControlSectionFields.RoomControlSectionId == entity.RoomControlSectionId.Value };
            RelationCollection relations = new RelationCollection { RoomControlSectionEntity.Relations.RoomControlAreaEntityUsingRoomControlAreaId };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForRoomControlSectionItem(RoomControlWidgetEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { RoomControlSectionItemFields.RoomControlSectionItemId == entity.RoomControlSectionItemId.Value };
            RelationCollection relations = new RelationCollection 
            { 
                RoomControlSectionItemEntity.Relations.RoomControlSectionEntityUsingRoomControlSectionId,
                RoomControlSectionEntity.Relations.RoomControlAreaEntityUsingRoomControlAreaId 
            };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(RoomControlWidgetEntity entity, bool wasNew)
        {
            if (entity.RoomControlSectionId.HasValue)
                this.UpdateTimestampForRoomControlSection(entity);
            else if (entity.RoomControlSectionItemId.HasValue)
                this.UpdateTimestampForRoomControlSectionItem(entity);
        }

        #endregion
    }
}
