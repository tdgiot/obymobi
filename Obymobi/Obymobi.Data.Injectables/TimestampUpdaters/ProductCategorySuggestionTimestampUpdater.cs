using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ProductCategorySuggestionEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ProductCategorySuggestionTimestampUpdater : GeneralTimestampUpdater<ProductCategorySuggestionEntity>
    {
        #region Methods

        protected override void UpdateTimestamps(ProductCategorySuggestionEntity entity, bool wasNew)
        {
            if (entity.ProductCategoryId.HasValue && entity.ProductCategoryEntity.CategoryEntity.MenuId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForMenu(entity.ProductCategoryEntity.CategoryEntity.MenuId.Value, entity.GetCurrentTransaction());
            }
        }

        #endregion
    }
}
