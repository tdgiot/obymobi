using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(PageEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class PageTimestampUpdater : GeneralTimestampUpdater<PageEntity>
    {
        #region Methods

        internal void UpdateTimestampForPage(PageEntity entity)
        {
            PageEntity pageEntity = new PageEntity();
            pageEntity.AddToTransaction(entity);
            pageEntity.FetchUsingPK(entity.PageId, null, null, new IncludeFieldsList { PageFields.SiteId, PageFields.ParentCompanyId });
            if (!pageEntity.IsNew)
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(PageEntity entity, bool wasNew)
        {
            this.UpdateTimestampForPage(entity);
        }

        #endregion
    }
}
