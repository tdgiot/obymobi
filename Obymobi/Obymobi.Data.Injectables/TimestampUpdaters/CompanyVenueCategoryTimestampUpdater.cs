using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(CompanyVenueCategoryEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class CompanyVenueCategoryTimestampUpdater : GeneralTimestampUpdater<CompanyVenueCategoryEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    CompanyVenueCategoryFields.CompanyId,
                    CompanyVenueCategoryFields.VenueCategoryId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForVenueCategories(CompanyVenueCategoryEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId, TimestampFields.ModifiedCompanyVenueCategoriesUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(CompanyVenueCategoryEntity entity, bool wasNew)
        {
            this.UpdateTimestampForVenueCategories(entity);
        }

        #endregion
    }
}
