using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(PageElementEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class PageElementTimestampUpdater : GeneralTimestampUpdater<PageElementEntity>
    {
        #region Methods

        internal void UpdateTimestampForPage(PageElementEntity entity)
        {
            PageEntity pageEntity = new PageEntity();
            pageEntity.AddToTransaction(entity);
            pageEntity.FetchUsingPK(entity.PageId, null, null, new IncludeFieldsList { PageFields.SiteId, PageFields.ParentCompanyId });
            if (!pageEntity.IsNew)
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(PageElementEntity entity, bool wasNew)
        {
            this.UpdateTimestampForPage(entity);
        }

        #endregion
    }
}
