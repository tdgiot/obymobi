using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(EntertainmentcategoryEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class EntertainmentcategoryTimestampUpdater : GeneralTimestampUpdater<EntertainmentcategoryEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    EntertainmentcategoryFields.EntertainmentcategoryId,
                    EntertainmentcategoryFields.Name
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForEntertainmentcategories(EntertainmentcategoryEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { EntertainmentFields.EntertainmentcategoryId == entity.EntertainmentcategoryId };
            RelationCollection relations = new RelationCollection { EntertainmentEntity.Relations.EntertainmentConfigurationEntertainmentEntityUsingEntertainmentId };

            EntertainmentConfigurationEntertainmentCollection entertainmentConfigurationEntertainmentCollection = new EntertainmentConfigurationEntertainmentCollection();
            entertainmentConfigurationEntertainmentCollection.AddToTransaction(entity);
            entertainmentConfigurationEntertainmentCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(EntertainmentConfigurationEntertainmentFields.EntertainmentConfigurationId, EntertainmentConfigurationEntertainmentFields.ParentCompanyId), 0, 0);

            foreach (EntertainmentConfigurationEntertainmentEntity entertainmentConfigurationEntertainmentEntity in entertainmentConfigurationEntertainmentCollection)
            {
                this.UpdateTimestampField(TimestampFields.EntertainmentConfigurationId, entertainmentConfigurationEntertainmentEntity.EntertainmentConfigurationId, TimestampFields.ModifiedEntertainmentCategoriesUTC, entity.GetCurrentTransaction());
            }            
        }

        protected override void UpdateTimestamps(EntertainmentcategoryEntity entity, bool wasNew)
        {
            this.UpdateTimestampForEntertainmentcategories(entity);
        }

        #endregion
    }
}
