using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(AlterationEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class AlterationTimestampUpdater : GeneralTimestampUpdater<AlterationEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    AlterationFields.AvailableOnObymobi,
                    AlterationFields.AvailableOnOtoucho,
                    AlterationFields.Description,
                    AlterationFields.EndTime,
                    AlterationFields.FriendlyName,
                    AlterationFields.GenericalterationId,
                    AlterationFields.IntervalMinutes,
                    AlterationFields.LayoutType,
                    AlterationFields.MaxLeadHours,
                    AlterationFields.MaxOptions,
                    AlterationFields.MinLeadMinutes,
                    AlterationFields.MinOptions,
                    AlterationFields.Name,
                    AlterationFields.OrderLevelEnabled,
                    AlterationFields.PriceAddition,
                    AlterationFields.ShowDatePicker,
                    AlterationFields.SortOrder,
                    AlterationFields.StartTime,
                    AlterationFields.Value,
                    AlterationFields.Visible
                };
            }
        }

        #endregion

        #region Methods

        protected override void UpdateTimestamps(AlterationEntity entity, bool wasNew)
        {
            if (entity.BrandId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForBrandAlterations(entity.BrandId.Value, entity.GetCurrentTransaction());
            }
            else if (entity.CompanyId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForAlterations(entity.CompanyId.Value, entity.GetCurrentTransaction());
            }
        }

        #endregion
    }
}
