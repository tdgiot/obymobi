using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(AlterationitemEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class AlterationitemTimestampUpdater : GeneralTimestampUpdater<AlterationitemEntity>
    {
        #region Methods

        protected override void UpdateTimestamps(AlterationitemEntity entity, bool wasNew)
        {
            if (entity.ParentBrandId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForBrandAlterations(entity.ParentBrandId.Value, entity.GetCurrentTransaction());
            }
            else if (entity.ParentCompanyId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForAlterations(entity.ParentCompanyId.Value, entity.GetCurrentTransaction());
            }
        }

        #endregion
    }
}
