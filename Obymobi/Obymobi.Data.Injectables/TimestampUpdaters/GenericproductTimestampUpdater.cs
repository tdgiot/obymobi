using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(GenericproductEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class GenericproductTimestampUpdater : GeneralTimestampUpdater<GenericproductEntity>
    {
        #region Methods

        internal void UpdateTimestampsForGenericproduct(GenericproductEntity entity)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.GenericproductId == entity.GenericproductId);
            filter.Add(CategoryFields.MenuId != DBNull.Value);

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductEntity.Relations.ProductCategoryEntityUsingProductId);
            relations.Add(ProductCategoryEntity.Relations.CategoryEntityUsingCategoryId);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.AddToTransaction(entity);
            categoryCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(CategoryFields.MenuId), 0, 0);

            foreach (CategoryEntity categoryEntity in categoryCollection)
            {
                if (categoryEntity.MenuId.HasValue)
                {
                    this.UpdateTimestampField(TimestampFields.MenuId, categoryEntity.MenuId.Value, TimestampFields.ModifiedProductsUTC, entity.GetCurrentTransaction());
                }
            }
        }

        protected override void UpdateTimestamps(GenericproductEntity entity, bool wasNew)
        {
            this.UpdateTimestampsForGenericproduct(entity);
        }

        #endregion
    }
}
