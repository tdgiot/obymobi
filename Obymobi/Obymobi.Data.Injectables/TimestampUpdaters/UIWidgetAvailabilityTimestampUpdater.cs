using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(UIWidgetAvailabilityEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class UiWidgetAvailabilityTimestampUpdater : GeneralTimestampUpdater<UIWidgetAvailabilityEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    UIWidgetAvailabilityFields.UIWidgetAvailabilityId,
                    UIWidgetAvailabilityFields.UIWidgetId,
                    UIWidgetAvailabilityFields.AvailabilityId,
                    UIWidgetAvailabilityFields.ParentCompanyId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForUIMode(UIWidgetAvailabilityEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { UIWidgetFields.UIWidgetId == entity.UIWidgetId };
            RelationCollection relations = new RelationCollection 
            {
                UIWidgetEntity.Relations.UITabEntityUsingUITabId,
                UITabEntity.Relations.UIModeEntityUsingUIModeId
            };

            UIModeCollection uiModeCollection = new UIModeCollection();
            uiModeCollection.AddToTransaction(entity);
            uiModeCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(UIModeFields.UIModeId), 0, 0);

            foreach (UIModeEntity uiModeEntity in uiModeCollection)
            {
                this.UpdateTimestampField(TimestampFields.UIModeId, uiModeEntity.UIModeId, TimestampFields.ModifiedUIModeUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(UIWidgetAvailabilityEntity entity, bool wasNew)
        {
            this.UpdateTimestampForUIMode(entity);
        }

        #endregion
    }
}
