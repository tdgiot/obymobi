using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(RoomControlConfigurationEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class RoomControlConfigurationTimestampUpdater : GeneralTimestampUpdater<RoomControlConfigurationEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    RoomControlConfigurationFields.Name
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForRoomControlConfiguration(RoomControlConfigurationEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, entity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(RoomControlConfigurationEntity entity, bool wasNew)
        {
            this.UpdateTimestampForRoomControlConfiguration(entity);
        }

        #endregion
    }
}
