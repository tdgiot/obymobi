using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(EntertainmentDependencyEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class EntertainmentDependencyTimestampUpdater : GeneralTimestampUpdater<EntertainmentDependencyEntity>
    {
        #region Methods

        internal void UpdateTimestampsForEntertainment(EntertainmentDependencyEntity entity)
        {
            PredicateExpression filter = new PredicateExpression(EntertainmentConfigurationEntertainmentFields.EntertainmentId == entity.EntertainmentId);

            EntertainmentConfigurationEntertainmentCollection entertainmentConfigurationEntertainmentCollection = new EntertainmentConfigurationEntertainmentCollection();
            entertainmentConfigurationEntertainmentCollection.AddToTransaction(entity);
            entertainmentConfigurationEntertainmentCollection.GetMulti(filter, new IncludeFieldsList(EntertainmentConfigurationEntertainmentFields.EntertainmentConfigurationId, EntertainmentConfigurationEntertainmentFields.ParentCompanyId), null);

            foreach (EntertainmentConfigurationEntertainmentEntity entertainmentConfigurationEntertainmentEntity in entertainmentConfigurationEntertainmentCollection)
            {
                this.UpdateTimestampField(TimestampFields.EntertainmentConfigurationId, entertainmentConfigurationEntertainmentEntity.EntertainmentConfigurationId, TimestampFields.ModifiedEntertainmentConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(EntertainmentDependencyEntity entity, bool wasNew)
        {
            this.UpdateTimestampsForEntertainment(entity);
        }

        #endregion
    }
}
