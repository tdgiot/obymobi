using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(CustomTextEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class CustomTextTimestampUpdater : GeneralTimestampUpdater<CustomTextEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    CustomTextFields.Type,
                    CustomTextFields.Text,
                    CustomTextFields.CultureCode,
                    CustomTextFields.ActionButtonId,
                    CustomTextFields.AdvertisementId,
                    CustomTextFields.AlterationId,
                    CustomTextFields.AlterationoptionId,
                    CustomTextFields.AmenityId,
                    CustomTextFields.AttachmentId,
                    CustomTextFields.AvailabilityId,
                    CustomTextFields.CategoryId,
                    CustomTextFields.ClientConfigurationId,
                    CustomTextFields.CompanyId,
                    CustomTextFields.EntertainmentcategoryId,
                    CustomTextFields.GenericcategoryId,
                    CustomTextFields.GenericproductId,
                    CustomTextFields.PointOfInterestId,
                    CustomTextFields.RoomControlAreaId,
                    CustomTextFields.RoomControlComponentId,
                    CustomTextFields.RoomControlSectionId,
                    CustomTextFields.RoomControlSectionItemId,
                    CustomTextFields.RoomControlWidgetId,
                    CustomTextFields.ProductId,
                    CustomTextFields.ScheduledMessageId,
                    CustomTextFields.StationId,
                    CustomTextFields.UIFooterItemId,
                    CustomTextFields.UITabId,
                    CustomTextFields.UIWidgetId,
                    CustomTextFields.VenueCategoryId,
                    CustomTextFields.ParentCompanyId,
                    CustomTextFields.InfraredCommandId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForAdvertisement(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression(AdvertisementConfigurationAdvertisementFields.AdvertisementId == entity.AdvertisementId.Value);

            AdvertisementConfigurationAdvertisementCollection advertisementConfigurationAdvertisementCollection = new AdvertisementConfigurationAdvertisementCollection();
            advertisementConfigurationAdvertisementCollection.AddToTransaction(entity);
            advertisementConfigurationAdvertisementCollection.GetMulti(filter, new IncludeFieldsList(AdvertisementConfigurationAdvertisementFields.AdvertisementConfigurationId), null);

            foreach (AdvertisementConfigurationAdvertisementEntity advertisementConfigurationAdvertisementEntity in advertisementConfigurationAdvertisementCollection)
            {
                this.UpdateTimestampField(TimestampFields.AdvertisementConfigurationId, advertisementConfigurationAdvertisementEntity.AdvertisementConfigurationId, TimestampFields.ModifiedAdvertisementConfigurationCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForAlteration(AlterationEntity entity, ITransaction transaction)
        {
            if (entity.BrandId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForBrandAlterationsCustomText(entity.BrandId.Value, transaction);
            }
            else if (entity.CompanyId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForAlterationsCustomText(entity.CompanyId.Value, transaction);
            }
        }

        internal void UpdateTimestampForAlterationoption(AlterationoptionEntity entity, ITransaction transaction)
        {
            if (entity.BrandId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForBrandAlterationsCustomText(entity.BrandId.Value, transaction);
            }
            else if (entity.CompanyId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForAlterationsCustomText(entity.CompanyId.Value, transaction);
            }
        }

        internal void UpdateTimestampForAmenity(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { CompanyAmenityFields.AmenityId == entity.AmenityId.Value };

            CompanyAmenityCollection companyAmenityCollection = new CompanyAmenityCollection();
            companyAmenityCollection.AddToTransaction(entity);
            companyAmenityCollection.GetMulti(filter, new IncludeFieldsList(CompanyAmenityFields.CompanyId), null);

            foreach (CompanyAmenityEntity companyAmenityEntity in companyAmenityCollection)
            {
                this.UpdateTimestampField(TimestampFields.CompanyId, companyAmenityEntity.CompanyId, TimestampFields.ModifiedAmenitiesCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForAttachment(CustomTextEntity entity)
        {
            if (entity.AttachmentEntity.ProductId.HasValue)
            {
                this.UpdateTimestampForProduct(entity.AttachmentEntity.ProductEntity, entity.GetCurrentTransaction());
            }
            else if (entity.AttachmentEntity.PageId.HasValue)
            {
                this.UpdateTimestampForAttachmentPage(entity);
            }
            else if (entity.AttachmentEntity.PageTemplateId.HasValue)
            {
                this.UpdateTimestampForAttachmentPageTemplate(entity);
            }
        }

        internal void UpdateTimestampForAttachmentPage(CustomTextEntity entity)
        {
            PageEntity pageEntity = new PageEntity();
            pageEntity.AddToTransaction(entity);
            pageEntity.FetchUsingPK(entity.AttachmentEntity.PageId.Value, null, null, new IncludeFieldsList { PageFields.SiteId });
            if (!pageEntity.IsNew)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForAttachmentPageTemplate(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PageFields.PageTemplateId == entity.AttachmentEntity.PageTemplateId.Value };

            PageCollection pageCollection = new PageCollection();
            pageCollection.AddToTransaction(entity);
            pageCollection.GetMulti(filter, new IncludeFieldsList(PageFields.SiteId), null);

            foreach (PageEntity pageEntity in pageCollection)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForAvailability(CustomTextEntity entity)
        {
            AvailabilityEntity availabilityEntity = new AvailabilityEntity();
            availabilityEntity.AddToTransaction(entity);
            availabilityEntity.FetchUsingPK(entity.AvailabilityId.Value, null, null, new IncludeFieldsList { AvailabilityFields.CompanyId });
            if (!availabilityEntity.IsNew)
            {
                this.UpdateTimestampField(TimestampFields.CompanyId, availabilityEntity.CompanyId, TimestampFields.ModifiedAvailabilitiesCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForCategory(CategoryEntity entity, ITransaction transaction)
        {
            if (entity.MenuId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForMenuCustomText(entity.MenuId.Value, transaction);
            }
        }

        internal void UpdateTimestampForClientConfiguration(CustomTextEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.ClientConfigurationId, entity.ClientConfigurationId.Value, TimestampFields.ModifiedClientConfigurationCustomTextUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForCompany(CustomTextEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId.Value, TimestampFields.ModifiedCompanyCustomTextUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForDeliverypointgroup(CustomTextEntity entity)
        {
            DeliverypointgroupEntity deliverypointgroupEntity = new DeliverypointgroupEntity();
            deliverypointgroupEntity.AddToTransaction(entity);
            deliverypointgroupEntity.FetchUsingPK(entity.DeliverypointgroupId.Value, null, null, new ExcludeIncludeFieldsList { DeliverypointgroupFields.ClientConfigurationId });

            if (!deliverypointgroupEntity.IsNew && deliverypointgroupEntity.ClientConfigurationId.HasValue)
            {
                this.UpdateTimestampField(TimestampFields.ClientConfigurationId, deliverypointgroupEntity.ClientConfigurationId, TimestampFields.ModifiedClientConfigurationCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForEntertainmentcategory(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { EntertainmentFields.EntertainmentcategoryId == entity.EntertainmentcategoryId.Value };
            RelationCollection relations = new RelationCollection {  EntertainmentEntityBase.Relations.EntertainmentConfigurationEntertainmentEntityUsingEntertainmentId };

            EntertainmentConfigurationEntertainmentCollection entertainmentConfigurationEntertainmentCollection = new EntertainmentConfigurationEntertainmentCollection();
            entertainmentConfigurationEntertainmentCollection.AddToTransaction(entity);
            entertainmentConfigurationEntertainmentCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(EntertainmentConfigurationEntertainmentFields.EntertainmentConfigurationId), 0, 0);

            foreach (EntertainmentConfigurationEntertainmentEntity entertainmentConfigurationEntertainmentEntity in entertainmentConfigurationEntertainmentCollection)
            {
                this.UpdateTimestampField(TimestampFields.EntertainmentConfigurationId, entertainmentConfigurationEntertainmentEntity.EntertainmentConfigurationId, TimestampFields.ModifiedEntertainmentCategoriesCustomTextUTC, entity.GetCurrentTransaction());
            }            
        }

        internal void UpdateTimestampForGenericcategory(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(GenericcategoryFields.GenericcategoryId == entity.GenericcategoryId.Value);
            filter.Add(CategoryFields.MenuId != DBNull.Value);

            RelationCollection relations = new RelationCollection(GenericcategoryEntityBase.Relations.CategoryEntityUsingGenericcategoryId);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.AddToTransaction(entity);
            categoryCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(CategoryFields.MenuId), 0, 0);

            foreach (CategoryEntity categoryEntity in categoryCollection)
            {
                if (categoryEntity.MenuId.HasValue)
                {
                    this.UpdateTimestampField(TimestampFields.MenuId, categoryEntity.MenuId.Value, TimestampFields.ModifiedMenuCustomTextUTC, entity.GetCurrentTransaction());
                }
            }
        }

        internal void UpdateTimestampForPage(CustomTextEntity entity)
        {
            PageEntity pageEntity = new PageEntity();
            pageEntity.AddToTransaction(entity);
            pageEntity.FetchUsingPK(entity.PageId.Value, null, null, new IncludeFieldsList { PageFields.SiteId });
            if (!pageEntity.IsNew)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForPageTemplate(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PageFields.PageTemplateId == entity.PageTemplateId.Value };

            PageCollection pageCollection = new PageCollection();
            pageCollection.AddToTransaction(entity);
            pageCollection.GetMulti(filter, new IncludeFieldsList(PageFields.SiteId), null);

            foreach (PageEntity pageEntity in pageCollection)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForPointOfInterest(CustomTextEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.PointOfInterestId, entity.PointOfInterestId.Value, TimestampFields.ModifiedPointOfInterestCustomTextUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForProduct(ProductEntity entity, ITransaction transaction)
        {
            if (entity.BrandId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForBrandProductsCustomText(entity.BrandId.Value, transaction);
            }
            else if (entity.CompanyId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForProductsCustomText(entity.CompanyId.Value, transaction);
            }
        }

        internal void UpdateTimestampForProductgroup(ProductgroupEntity entity, ITransaction transaction)
        {
            new MenuTimestampUpdater().UpdateTimestampForProductgroupsCustomText(entity.CompanyId, transaction);
        }

        internal void UpdateTimestampForRoomControlArea(CustomTextEntity entity)
        {
            RoomControlAreaEntity roomControlAreaEntity = new RoomControlAreaEntity();
            roomControlAreaEntity.AddToTransaction(entity);
            roomControlAreaEntity.FetchUsingPK(entity.RoomControlAreaId.Value, null, null, new IncludeFieldsList { RoomControlAreaFields.RoomControlConfigurationId });
            if (!roomControlAreaEntity.IsNew)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForRoomControlComponent(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { RoomControlComponentFields.RoomControlComponentId == entity.RoomControlComponentId.Value };
            RelationCollection relations = new RelationCollection 
            { 
                RoomControlComponentEntityBase.Relations.RoomControlSectionEntityUsingRoomControlSectionId,
                RoomControlSectionEntityBase.Relations.RoomControlAreaEntityUsingRoomControlAreaId
            };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForRoomControlSection(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { RoomControlSectionFields.RoomControlSectionId == entity.RoomControlSectionId.Value };
            RelationCollection relations = new RelationCollection  {  RoomControlSectionEntityBase.Relations.RoomControlAreaEntityUsingRoomControlAreaId };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForRoomControlSectionItem(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { RoomControlSectionItemFields.RoomControlSectionItemId == entity.RoomControlSectionItemId.Value };
            RelationCollection relations = new RelationCollection 
            { 
                RoomControlSectionItemEntityBase.Relations.RoomControlSectionEntityUsingRoomControlSectionId,
                RoomControlSectionEntityBase.Relations.RoomControlAreaEntityUsingRoomControlAreaId 
            };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForRoomControlWidget(CustomTextEntity entity)
        {
            if (entity.RoomControlWidgetEntity.RoomControlSectionId.HasValue)
                this.UpdateTimestampForRoomControlWidgetRoomControlSection(entity);
            else if (entity.RoomControlWidgetEntity.RoomControlSectionItemId.HasValue)
                this.UpdateTimestampForRoomControlWidgetRoomControlSectionItem(entity);
        }

        internal void UpdateTimestampForRoomControlWidgetRoomControlSection(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { RoomControlSectionFields.RoomControlSectionId == entity.RoomControlWidgetEntity.RoomControlSectionId.Value };
            RelationCollection relations = new RelationCollection { RoomControlSectionEntityBase.Relations.RoomControlAreaEntityUsingRoomControlAreaId };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForRoomControlWidgetRoomControlSectionItem(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { RoomControlSectionItemFields.RoomControlSectionItemId == entity.RoomControlWidgetEntity.RoomControlSectionItemId.Value };
            RelationCollection relations = new RelationCollection 
            { 
                RoomControlSectionItemEntityBase.Relations.RoomControlSectionEntityUsingRoomControlSectionId,
                RoomControlSectionEntityBase.Relations.RoomControlAreaEntityUsingRoomControlAreaId 
            };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForSite(CustomTextEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.SiteId, entity.SiteId.Value, TimestampFields.ModifiedSiteCustomTextUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForStation(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { StationFields.StationId == entity.StationId.Value };
            RelationCollection relations = new RelationCollection 
            { 
                StationEntityBase.Relations.StationListEntityUsingStationListId,
                StationListEntityBase.Relations.RoomControlSectionItemEntityUsingStationListId,
                RoomControlSectionItemEntityBase.Relations.RoomControlSectionEntityUsingRoomControlSectionId,
                RoomControlSectionEntityBase.Relations.RoomControlAreaEntityUsingRoomControlAreaId 
            };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForUIFooterItem(CustomTextEntity entity)
        {
            UIFooterItemEntity uiFooterItemEntity = new UIFooterItemEntity();
            uiFooterItemEntity.AddToTransaction(entity);
            uiFooterItemEntity.FetchUsingPK(entity.UIFooterItemId.Value, null, null, new IncludeFieldsList { UIFooterItemFields.UIModeId });
            if (!uiFooterItemEntity.IsNew)
            {
                this.UpdateTimestampField(TimestampFields.UIModeId, uiFooterItemEntity.UIModeId, TimestampFields.ModifiedUIModeCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForUITab(CustomTextEntity entity)
        {
            UITabEntity uiTabEntity = new UITabEntity();
            uiTabEntity.AddToTransaction(entity);
            uiTabEntity.FetchUsingPK(entity.UITabId.Value, null, null, new IncludeFieldsList { UITabFields.UIModeId });
            if (!uiTabEntity.IsNew)
            {
                this.UpdateTimestampField(TimestampFields.UIModeId, uiTabEntity.UIModeId, TimestampFields.ModifiedUIModeCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForUIWidget(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { UIWidgetFields.UIWidgetId == entity.UIWidgetId.Value };
            RelationCollection relations = new RelationCollection { UIWidgetEntityBase.Relations.UITabEntityUsingUITabId };

            UITabCollection uITabCollection = new UITabCollection();
            uITabCollection.AddToTransaction(entity);
            uITabCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(UITabFields.UIModeId), 0, 0);

            foreach (UITabEntity uiTabEntity in uITabCollection)
            {
                this.UpdateTimestampField(TimestampFields.UIModeId, uiTabEntity.UIModeId, TimestampFields.ModifiedUIModeCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForVenueCategory(CustomTextEntity entity)
        {
            this.UpdateTimestampForVenueCategoryViaCompanyVenueCategory(entity);
            this.UpdateTimestampForVenueCategoryViaPointOfInterestVenueCategory(entity);
        }

        internal void UpdateTimestampForVenueCategoryViaCompanyVenueCategory(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { CompanyVenueCategoryFields.VenueCategoryId == entity.VenueCategoryId.Value };

            CompanyVenueCategoryCollection companyVenueCategoryCollection = new CompanyVenueCategoryCollection();
            companyVenueCategoryCollection.AddToTransaction(entity);
            companyVenueCategoryCollection.GetMulti(filter, new IncludeFieldsList { CompanyVenueCategoryFields.CompanyId }, null);

            foreach (CompanyVenueCategoryEntity companyVenueCategoryEntity in companyVenueCategoryCollection)
            {
                this.UpdateTimestampField(TimestampFields.CompanyId, companyVenueCategoryEntity.CompanyId, TimestampFields.ModifiedCompanyVenueCategoriesCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForVenueCategoryViaPointOfInterestVenueCategory(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PointOfInterestVenueCategoryFields.VenueCategoryId == entity.VenueCategoryId.Value };

            PointOfInterestVenueCategoryCollection pointOfInterestVenueCategoryCollection = new PointOfInterestVenueCategoryCollection();
            pointOfInterestVenueCategoryCollection.AddToTransaction(entity);
            pointOfInterestVenueCategoryCollection.GetMulti(filter, new IncludeFieldsList { PointOfInterestVenueCategoryFields.PointOfInterestId }, null);

            foreach (PointOfInterestVenueCategoryEntity pointOfInterestVenueCategoryEntity in pointOfInterestVenueCategoryCollection)
            {
                this.UpdateTimestampField(TimestampFields.PointOfInterestId, pointOfInterestVenueCategoryEntity.PointOfInterestId, TimestampFields.ModifiedCompanyVenueCategoriesCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForInfraredCommand(CustomTextEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { InfraredCommandFields.InfraredCommandId == entity.InfraredCommandId.Value };

            InfraredCommandCollection infraredCommandCollection = new InfraredCommandCollection();
            infraredCommandCollection.AddToTransaction(entity);
            infraredCommandCollection.GetMulti(filter, new IncludeFieldsList { InfraredCommandFields.InfraredConfigurationId }, null);

            foreach (InfraredCommandEntity infraredCommandEntity in infraredCommandCollection)
            {
                this.UpdateTimestampField(TimestampFields.InfraredConfigurationId, infraredCommandEntity.InfraredConfigurationId, TimestampFields.ModifiedInfraredConfigurationCustomTextUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(CustomTextEntity entity, bool wasNew)
        {
            if (entity.AdvertisementId.HasValue)
            {
                this.UpdateTimestampForAdvertisement(entity);
            }
            else if (entity.AlterationId.HasValue)
            {
                this.UpdateTimestampForAlteration(entity.AlterationEntity, entity.GetCurrentTransaction());
            }
            else if (entity.AlterationoptionId.HasValue)
            {
                this.UpdateTimestampForAlterationoption(entity.AlterationoptionEntity, entity.GetCurrentTransaction());
            }
            else if (entity.AmenityId.HasValue)
            {
                this.UpdateTimestampForAmenity(entity);
            }
            else if (entity.AttachmentId.HasValue)
            {
                this.UpdateTimestampForAttachment(entity);
            }
            else if (entity.AvailabilityId.HasValue)
            {
                this.UpdateTimestampForAvailability(entity);
            }
            else if (entity.CategoryId.HasValue)
            {
                this.UpdateTimestampForCategory(entity.CategoryEntity, entity.GetCurrentTransaction());
            }
            else if (entity.ClientConfigurationId.HasValue)
            {
                this.UpdateTimestampForClientConfiguration(entity);
            }
            else if (entity.CompanyId.HasValue)
            {
                this.UpdateTimestampForCompany(entity);
            }
            else if (entity.DeliverypointgroupId.HasValue)
            {
                this.UpdateTimestampForDeliverypointgroup(entity);
            }
            else if (entity.EntertainmentcategoryId.HasValue)
            {
                this.UpdateTimestampForEntertainmentcategory(entity);
            }
            else if (entity.GenericcategoryId.HasValue)
            {
                this.UpdateTimestampForGenericcategory(entity);
            }
            else if (entity.PageId.HasValue)
            {
                this.UpdateTimestampForPage(entity);
            }
            else if (entity.PageTemplateId.HasValue)
            {
                this.UpdateTimestampForPageTemplate(entity);
            }
            else if (entity.PointOfInterestId.HasValue)
            {
                this.UpdateTimestampForPointOfInterest(entity);
            }
            else if (entity.ProductId.HasValue)
            {
                this.UpdateTimestampForProduct(entity.ProductEntity, entity.GetCurrentTransaction());
            }
            else if (entity.ProductgroupId.HasValue)
            {
                this.UpdateTimestampForProductgroup(entity.ProductgroupEntity, entity.GetCurrentTransaction());
            }
            else if (entity.RoomControlAreaId.HasValue)
            {
                this.UpdateTimestampForRoomControlArea(entity);
            }
            else if (entity.RoomControlComponentId.HasValue)
            {
                this.UpdateTimestampForRoomControlComponent(entity);
            }
            else if (entity.RoomControlSectionId.HasValue)
            {
                this.UpdateTimestampForRoomControlSection(entity);
            }
            else if (entity.RoomControlSectionItemId.HasValue)
            {
                this.UpdateTimestampForRoomControlSectionItem(entity);
            }
            else if (entity.RoomControlWidgetId.HasValue)
            {
                this.UpdateTimestampForRoomControlWidget(entity);
            }
            else if (entity.SiteId.HasValue)
            {
                this.UpdateTimestampForSite(entity);
            }
            else if (entity.StationId.HasValue)
            {
                this.UpdateTimestampForStation(entity);
            }
            else if (entity.UIFooterItemId.HasValue)
            {
                this.UpdateTimestampForUIFooterItem(entity);
            }
            else if (entity.UITabId.HasValue)
            {
                this.UpdateTimestampForUITab(entity);
            }
            else if (entity.UIWidgetId.HasValue)
            {
                this.UpdateTimestampForUIWidget(entity);
            }
            else if (entity.VenueCategoryId.HasValue)
            {
                this.UpdateTimestampForVenueCategory(entity);
            }
            else if (entity.InfraredCommandId.HasValue)
            {
                this.UpdateTimestampForInfraredCommand(entity);
            }
        }

        #endregion
    }
}
