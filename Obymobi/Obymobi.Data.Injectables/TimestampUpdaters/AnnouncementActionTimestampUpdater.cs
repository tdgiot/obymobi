﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    public class AnnouncementActionTimestampUpdater : GeneralTimestampUpdater<AnnouncementEntity>
    {
        public void UpdateTimestampForAnnouncementAction(int companyId, ITransaction transaction)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, companyId, TimestampFields.ModifiedAnnouncementActionsUTC, transaction);
        }

        protected override void UpdateTimestamps(AnnouncementEntity entity, bool wasNew)
        {
            if (!entity.CompanyId.HasValue)
                return;

            UpdateTimestampForAnnouncementAction(entity.CompanyId.Value, entity.GetCurrentTransaction());
        }
    }
}
