using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(PriceScheduleEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class PriceScheduleTimestampUpdater : GeneralTimestampUpdater<PriceScheduleEntity>
    {
        #region Methods

        internal void UpdateTimestampForPriceSchedule(PriceScheduleEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.PriceScheduleId, entity.PriceScheduleId, TimestampFields.ModifiedPriceScheduleUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(PriceScheduleEntity entity, bool wasNew)
        {
            this.UpdateTimestampForPriceSchedule(entity);
        }

        #endregion
    }
}
