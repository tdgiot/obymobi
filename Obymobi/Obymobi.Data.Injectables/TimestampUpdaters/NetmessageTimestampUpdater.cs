using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(NetmessageEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class NetmessageTimestampUpdater : GeneralTimestampUpdater<NetmessageEntity>
    {
        #region Methods

        internal void UpdateTimestampForNetmessages(NetmessageEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.DeviceIdentifier, entity.ReceiverIdentifier, TimestampFields.ModifiedNetmessagesUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(NetmessageEntity entity, bool wasNew)
        {
            // TODO NEW API
            // Temporarily disabled, not sure whether timestamps are really needed for Netmessages
            //this.UpdateTimestampForNetmessages(entity);
        }

        #endregion
    }
}
