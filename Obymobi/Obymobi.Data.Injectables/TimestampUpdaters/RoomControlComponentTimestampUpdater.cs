using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(RoomControlComponentEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class RoomControlComponentTimestampUpdater : GeneralTimestampUpdater<RoomControlComponentEntity>
    {
        #region Methods

        internal void UpdateTimestampForRoomControlComponent(RoomControlComponentEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { RoomControlComponentFields.RoomControlComponentId == entity.RoomControlComponentId };
            RelationCollection relations = new RelationCollection 
            { 
                RoomControlComponentEntity.Relations.RoomControlSectionEntityUsingRoomControlSectionId,
                RoomControlSectionEntity.Relations.RoomControlAreaEntityUsingRoomControlAreaId
            };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(RoomControlComponentEntity entity, bool wasNew)
        {
            this.UpdateTimestampForRoomControlComponent(entity);
        }

        #endregion
    }
}
