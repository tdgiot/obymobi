using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(EntertainmenturlEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class EntertainmenturlTimestampUpdater : GeneralTimestampUpdater<EntertainmenturlEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    EntertainmenturlFields.Url,
                    EntertainmenturlFields.AccessFullDomain,
                    EntertainmenturlFields.ParentCompanyId,
                    EntertainmenturlFields.InitialZoom
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForEntertainmentConfigurations(EntertainmenturlEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { EntertainmentConfigurationEntertainmentFields.EntertainmentId == entity.EntertainmentId };

            EntertainmentConfigurationEntertainmentCollection entertainmentConfigurationEntertainmentCollection = new EntertainmentConfigurationEntertainmentCollection();
            entertainmentConfigurationEntertainmentCollection.AddToTransaction(entity);
            entertainmentConfigurationEntertainmentCollection.GetMulti(filter);

            foreach (EntertainmentConfigurationEntertainmentEntity entertainmentConfigurationEntertainmentEntity in entertainmentConfigurationEntertainmentCollection)
            {
                this.UpdateTimestampField(TimestampFields.EntertainmentConfigurationId, entertainmentConfigurationEntertainmentEntity.EntertainmentConfigurationId, TimestampFields.ModifiedEntertainmentConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(EntertainmenturlEntity entity, bool wasNew)
        {
            this.UpdateTimestampForEntertainmentConfigurations(entity);
        }

        #endregion
    }
}
