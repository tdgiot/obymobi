using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(UITabEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class UITabTimestampUpdater : GeneralTimestampUpdater<UITabEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    UITabFields.UITabId,
                    UITabFields.UIModeId,
                    UITabFields.Type,
                    UITabFields.CategoryId,
                    UITabFields.EntertainmentId,
                    UITabFields.URL,
                    UITabFields.Zoom,
                    UITabFields.SiteId,
                    UITabFields.MapId,
                    UITabFields.Width,
                    UITabFields.SortOrder,
                    UITabFields.Visible,
                    UITabFields.ParentCompanyId,
                    UITabFields.RestrictedAccess,
                    UITabFields.AllCategoryVisible
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForUITab(UITabEntity entity)
        {
            UITabEntity uiTabEntity = new UITabEntity();
            uiTabEntity.AddToTransaction(entity);
            uiTabEntity.FetchUsingPK(entity.UITabId, null, null, new IncludeFieldsList { UITabFields.UIModeId });
            if (!uiTabEntity.IsNew)
                this.UpdateTimestampField(TimestampFields.UIModeId, uiTabEntity.UIModeId, TimestampFields.ModifiedUIModeUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(UITabEntity entity, bool wasNew)
        {
            this.UpdateTimestampForUITab(entity);
        }

        #endregion
    }
}
