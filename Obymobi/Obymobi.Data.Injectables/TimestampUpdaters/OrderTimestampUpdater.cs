using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(OrderEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class OrderTimestampUpdater : GeneralTimestampUpdater<OrderEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(OrderEntity entity, bool wasNew)
        {
            // TODO NEW API
            // First see how we want to implement this
        }

        #endregion
    }
}
