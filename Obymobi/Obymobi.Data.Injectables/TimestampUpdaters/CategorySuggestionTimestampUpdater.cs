using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(CategorySuggestionEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class CategorySuggestionTimestampUpdater : GeneralTimestampUpdater<CategorySuggestionEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(CategorySuggestionEntity entity, bool wasNew)
        {
            if (entity.CategoryId.HasValue && entity.CategoryEntity.MenuId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForMenu(entity.CategoryEntity.MenuId.Value, entity.GetCurrentTransaction());
            }
        }

        #endregion
    }
}
