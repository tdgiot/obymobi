using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MediaCultureEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MediaCultureTimestampUpdater : GeneralTimestampUpdater<MediaCultureEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(MediaCultureEntity entity, bool wasNew)
        {
            ITimestampEntity timestampEntity = entity.MediaEntity;
            if (timestampEntity?.TimestampUpdater != null)
            {
                timestampEntity.TimestampUpdater.UpdateTimestamps((IEntity)timestampEntity, wasNew);
            }
        }

        #endregion
    }
}
