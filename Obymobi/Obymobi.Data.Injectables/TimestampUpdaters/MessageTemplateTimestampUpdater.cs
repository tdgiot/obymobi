using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MessageTemplateEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MessageTemplateTimestampUpdater : GeneralTimestampUpdater<MessageTemplateEntity>
    {
        protected override IEnumerable<EntityField> Fields =>
            new List<EntityField>
            {
                MessageTemplateFields.MessageTemplateId,
                MessageTemplateFields.Name,
                MessageTemplateFields.Title,
                MessageTemplateFields.Message,
                MessageTemplateFields.Duration,
                MessageTemplateFields.MediaId,
                MessageTemplateFields.CategoryId,
                MessageTemplateFields.EntertainmentId,
                MessageTemplateFields.ProductId,
                MessageTemplateFields.ProductCategoryId,
                MessageTemplateFields.Url,
                MessageTemplateFields.NotifyOnYes,
                MessageTemplateFields.MessageLayoutType
            };

        internal void UpdateTimestampForMessageTemplates(MessageTemplateEntity entity)
        {
            List<int> messageTemplateCategoryIds = entity.MessageTemplateCategoryMessageTemplateCollection.Select(x => x.MessageTemplateCategoryEntity.MessageTemplateCategoryId).ToList();

            foreach (TerminalEntity terminal in entity.CompanyEntity.TerminalCollection)
            {
                if (terminal.TerminalTypeEnum != TerminalType.Console)
                {
                    continue;
                }

                if (terminal.TerminalMessageTemplateCategoryCollection.Count == 0 || 
                     terminal.TerminalMessageTemplateCategoryCollection.Any(x => messageTemplateCategoryIds.Contains(x.MessageTemplateCategoryId)))
                {
                    this.UpdateTimestampField(TimestampFields.TerminalId, terminal.TerminalId, TimestampFields.ModifiedMessageTemplatesUTC, entity.GetCurrentTransaction());
                }
            }
        }

        protected override void UpdateTimestamps(MessageTemplateEntity entity, bool wasNew)
        {      
            this.UpdateTimestampForMessageTemplates(entity);
        }
    }
}
