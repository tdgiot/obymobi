﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Linq;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(AffiliateCampaignAffiliatePartnerEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class AffiliateCampaignAffiliatePartnerTimestampUpdater : GeneralTimestampUpdater<AffiliateCampaignAffiliatePartnerEntity>
    {
        protected override void UpdateTimestamps(AffiliateCampaignAffiliatePartnerEntity entity, bool wasNew)
        {
            PredicateExpression filter = new PredicateExpression(DeliverypointgroupFields.AffiliateCampaignId == entity.AffiliateCampaignId);

            DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
            deliverypointgroups.AddToTransaction(entity);
            deliverypointgroups.GetMulti(filter, new IncludeFieldsList(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupFields.CompanyId), null);

            foreach (DeliverypointgroupEntity deliverypointgroup in deliverypointgroups.Where(x => x.ClientConfigurationId.HasValue))
            {
                UpdateTimestampField(TimestampFields.ClientConfigurationId, deliverypointgroup.ClientConfigurationId, TimestampFields.ModifiedClientConfigurationUTC, entity.GetCurrentTransaction());
            }
        }
    }
}
