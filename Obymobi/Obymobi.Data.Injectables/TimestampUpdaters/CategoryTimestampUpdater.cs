using System;
using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(CategoryEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class CategoryTimestampUpdater : GeneralTimestampUpdater<CategoryEntity>
    {
        protected override IEnumerable<EntityField> Fields =>
            new List<EntityField>
            {
                CategoryFields.CategoryId,
                CategoryFields.ParentCategoryId,
                CategoryFields.Name,
                CategoryFields.Description,
                CategoryFields.SortOrder,
                CategoryFields.Type,
                CategoryFields.HidePrices,
                CategoryFields.AllowFreeText,
                CategoryFields.ScheduleId,
                CategoryFields.ViewLayoutType,
                CategoryFields.DeliveryLocationType,
                CategoryFields.VisibilityType,
                CategoryFields.Geofencing
            };

        /// <inheritdoc/>
        protected override void UpdateTimestamps(CategoryEntity entity, bool wasNew)
        {
            if (entity.MenuId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForMenu(entity.MenuId.Value, entity.GetCurrentTransaction());
            }

            if (entity.ScheduleId.HasValue && this.IsFieldChanged(ProductFields.ScheduleId))
            {
                // If the schedule field is changed, check if any other category is linked to this schedule
                // When a new schedule is added to the system and linked to a category, the schedule is not marked as modified
                // on the menu and therefor not published with the menu. If a change was made to the schedule while it's linked
                // to a category everything works fine.
                // This fix checks if there's only 1 category linked to the schedule and will then trigger the ScheduleTimestampUpdater.
                // The same is done in ProductTimestampUpdater.
                // 
                // TODO: Change schedules to be published separate from the menu
                PredicateExpression filter = new PredicateExpression(CategoryFields.ScheduleId == entity.ScheduleId);
                int categoryWithScheduleCount = new CategoryCollection().GetDbCount(filter);
                if (categoryWithScheduleCount == 1)
                {
                    new ScheduleTimestampUpdater().UpdateTimestampForSchedule(entity.ScheduleEntity);
                }
            }
        }
    }
}
