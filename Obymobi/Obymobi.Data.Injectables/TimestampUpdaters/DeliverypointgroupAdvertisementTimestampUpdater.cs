using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(DeliverypointgroupAdvertisementEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class DeliverypointgroupAdvertisementTimestampUpdater : GeneralTimestampUpdater<DeliverypointgroupAdvertisementEntity>
    {
        #region Methods

        public override void UpdateTimestamps(IEntity entity, bool wasNew)
        {
            DeliverypointgroupAdvertisementEntity useableEntity = entity as DeliverypointgroupAdvertisementEntity;
            if (useableEntity == null)
                return;

            if (useableEntity.DeliverypointgroupEntity.ClientConfigurationId.HasValue && useableEntity.DeliverypointgroupEntity.ClientConfigurationEntity.AdvertisementConfigurationId.HasValue)
            {
                this.UpdateTimestampForAdvertisementConfiguration(useableEntity.DeliverypointgroupEntity.ClientConfigurationEntity);
            }
        }

        internal void UpdateTimestampForAdvertisementConfiguration(ClientConfigurationEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.AdvertisementConfigurationId, entity.AdvertisementConfigurationId, TimestampFields.ModifiedAdvertisementConfigurationUTC, entity.GetCurrentTransaction());
        }

        #endregion
    }
}
