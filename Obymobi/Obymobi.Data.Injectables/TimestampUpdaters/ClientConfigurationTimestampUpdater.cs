using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ClientConfigurationEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ClientConfigurationTimestampUpdater : GeneralTimestampUpdater<ClientConfigurationEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    ClientConfigurationFields.ClientConfigurationId,
                    ClientConfigurationFields.Name,
                    ClientConfigurationFields.DeliverypointgroupId,
                    ClientConfigurationFields.MenuId,
                    ClientConfigurationFields.UIModeId,
                    ClientConfigurationFields.UIThemeId,
                    ClientConfigurationFields.UIScheduleId,
                    ClientConfigurationFields.RoomControlConfigurationId,
                    ClientConfigurationFields.WifiConfigurationId,
                    ClientConfigurationFields.AdvertisementConfigurationId,
                    ClientConfigurationFields.EntertainmentConfigurationId,

                    ClientConfigurationFields.Pincode,
                    ClientConfigurationFields.PincodeSU,
                    ClientConfigurationFields.PincodeGM,
                    ClientConfigurationFields.HidePrices,
                    ClientConfigurationFields.ShowCurrencySymbol,
                    ClientConfigurationFields.ClearSessionOnTimeout,
                    ClientConfigurationFields.ResetTimeout,
                    ClientConfigurationFields.ScreenTimeoutInterval,
                    ClientConfigurationFields.DimLevelDull,
                    ClientConfigurationFields.DimLevelMedium,
                    ClientConfigurationFields.DimLevelBright,
                    ClientConfigurationFields.DockedDimLevelDull,
                    ClientConfigurationFields.DockedDimLevelMedium,
                    ClientConfigurationFields.DockedDimLevelBright,
                    ClientConfigurationFields.PowerSaveTimeout,
                    ClientConfigurationFields.PowerSaveLevel,
                    ClientConfigurationFields.OutOfChargeLevel,
                    ClientConfigurationFields.DailyOrderReset,
                    ClientConfigurationFields.SleepTime,
                    ClientConfigurationFields.WakeUpTime,
                    ClientConfigurationFields.HomepageSlideshowInterval,
                    ClientConfigurationFields.IsChargerRemovedDialogEnabled,
                    ClientConfigurationFields.IsChargerRemovedReminderDialogEnabled,
                    ClientConfigurationFields.PowerButtonHardBehaviour,
                    ClientConfigurationFields.PowerButtonSoftBehaviour,
                    ClientConfigurationFields.ScreenOffMode,
                    ClientConfigurationFields.ScreensaverMode,
                    ClientConfigurationFields.DeviceRebootMethod,
                    ClientConfigurationFields.RoomserviceCharge,
                    ClientConfigurationFields.AllowFreeText,
                    ClientConfigurationFields.BrowserAgeVerificationEnabled,
                    ClientConfigurationFields.GooglePrinterId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForClientConfiguration(ClientConfigurationEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.ClientConfigurationId, entity.ClientConfigurationId, TimestampFields.ModifiedClientConfigurationUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(ClientConfigurationEntity entity, bool wasNew)
        {
            this.UpdateTimestampForClientConfiguration(entity);
        }

        #endregion
    }
}
