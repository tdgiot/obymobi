using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ProductgroupEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ProductgroupTimestampUpdater : GeneralTimestampUpdater<ProductgroupEntity>
    {
        #region Methods

        internal void UpdateTimestampForProduct(ProductgroupEntity entity)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.ProductgroupId == entity.ProductgroupId);
            filter.Add(CategoryFields.MenuId != DBNull.Value);

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductgroupEntityBase.Relations.ProductEntityUsingProductgroupId);
            relations.Add(ProductEntityBase.Relations.ProductCategoryEntityUsingProductId);
            relations.Add(ProductCategoryEntityBase.Relations.CategoryEntityUsingCategoryId);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.AddToTransaction(entity);
            categoryCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(CategoryFields.MenuId), 0, 0);

            foreach (CategoryEntity categoryEntity in categoryCollection)
            {
                if (categoryEntity.MenuId.HasValue)
                {
                    this.UpdateTimestampField(TimestampFields.MenuId, categoryEntity.MenuId.Value, TimestampFields.ModifiedProductgroupsUTC, entity.GetCurrentTransaction());
                }
            }
        }

        internal void UpdateTimestampForProductgroup(ProductgroupEntity entity, bool wasNew)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductgroupItemFields.NestedProductgroupId == entity.ProductgroupId);

            ProductgroupItemCollection productgroupItemCollection = new ProductgroupItemCollection();
            productgroupItemCollection.AddToTransaction(entity);
            productgroupItemCollection.GetMulti(filter);

            foreach (ProductgroupItemEntity productgroupItem in productgroupItemCollection)
            {
                this.UpdateTimestamps(productgroupItem.ProductgroupEntity, wasNew);
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(ProductgroupEntity entity, bool wasNew)
        {
            new MenuTimestampUpdater().UpdateTimestampForProductgroups(entity.CompanyId, entity.GetCurrentTransaction());
        }

        #endregion
    }
}
