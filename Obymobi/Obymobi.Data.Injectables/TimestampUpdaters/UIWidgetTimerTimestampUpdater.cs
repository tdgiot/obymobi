using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(UIWidgetTimerEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class UiWidgetTimerTimestampUpdater : GeneralTimestampUpdater<UIWidgetTimerEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    UIWidgetTimerFields.UIWidgetTimerId,
                    UIWidgetTimerFields.CountToTime,
                    UIWidgetTimerFields.CountToDate,
                    UIWidgetTimerFields.ParentCompanyId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForUIWidget(UIWidgetTimerEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { UIWidgetFields.UIWidgetId == entity.UIWidgetId };
            RelationCollection relations = new RelationCollection { UIWidgetEntity.Relations.UITabEntityUsingUITabId };

            UITabCollection uITabCollection = new UITabCollection();
            uITabCollection.AddToTransaction(entity);
            uITabCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(UITabFields.UIModeId), 0, 0);

            foreach (UITabEntity uiTabEntity in uITabCollection)
            {
                this.UpdateTimestampField(TimestampFields.UIModeId, uiTabEntity.UIModeId, TimestampFields.ModifiedUIModeUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(UIWidgetTimerEntity entity, bool wasNew)
        {
            this.UpdateTimestampForUIWidget(entity);
        }

        #endregion
    }
}
