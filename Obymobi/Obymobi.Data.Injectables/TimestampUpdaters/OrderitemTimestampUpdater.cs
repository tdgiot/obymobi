using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(OrderitemEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class OrderitemTimestampUpdater : GeneralTimestampUpdater<OrderitemEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(OrderitemEntity entity, bool wasNew)
        {
            // TODO NEW API
            // First see how we want to implement this
        }

        #endregion
    }
}
