using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(RoomControlAreaEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class RoomControlAreaTimestampUpdater : GeneralTimestampUpdater<RoomControlAreaEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    RoomControlAreaFields.Name,
                    RoomControlAreaFields.NameSystem,
                    RoomControlAreaFields.SortOrder,
                    RoomControlAreaFields.Type,
                    RoomControlAreaFields.Visible,
                    RoomControlAreaFields.ParentCompanyId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForRoomControlConfiguration(RoomControlAreaEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, entity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(RoomControlAreaEntity entity, bool wasNew)
        {
            this.UpdateTimestampForRoomControlConfiguration(entity);
        }

        #endregion
    }
}
