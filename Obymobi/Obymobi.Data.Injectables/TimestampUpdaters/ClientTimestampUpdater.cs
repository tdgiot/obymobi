using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ClientEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ClientTimestampUpdater : GeneralTimestampUpdater<ClientEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    ClientFields.DeliverypointId,
                    ClientFields.DeliverypointGroupId,
                    ClientFields.RoomControlAreaId
                    //ClientFields.ClientConfigurationId // TODO NEW API
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForClient(ClientEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.ClientId, entity.ClientId, TimestampFields.ModifiedClientUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(ClientEntity entity, bool wasNew)
        {
            this.UpdateTimestampForClient(entity);
        }

        #endregion
    }
}
