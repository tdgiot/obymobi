﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MessageTemplateCategoryMessageTemplateEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MessageTemplateCategoryMessageTemplateTimestampUpdater : GeneralTimestampUpdater<MessageTemplateCategoryMessageTemplateEntity>
    {
        #region Methods

        internal void UpdateTimestampForMessageTemplateCategoryMessageTemplates(MessageTemplateCategoryMessageTemplateEntity entity)
        {
            foreach (TerminalMessageTemplateCategoryEntity terminalMessageTemplateCategoryEntity in entity.MessageTemplateCategoryEntity.TerminalMessageTemplateCategoryCollection)
            {
                this.UpdateTimestampField(TimestampFields.TerminalId, terminalMessageTemplateCategoryEntity.TerminalId, TimestampFields.ModifiedMessageTemplatesUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(MessageTemplateCategoryMessageTemplateEntity entity, bool wasNew)
        {
            this.UpdateTimestampForMessageTemplateCategoryMessageTemplates(entity);
        }

        #endregion
    }
}
