using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;


namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(EntertainmentConfigurationEntertainmentEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class EntertainmentConfigurationEntertainmentTimestampUpdater : GeneralTimestampUpdater<EntertainmentConfigurationEntertainmentEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    EntertainmentConfigurationEntertainmentFields.EntertainmentConfigurationId,
                    EntertainmentConfigurationEntertainmentFields.EntertainmentId,
                    EntertainmentConfigurationEntertainmentFields.ParentCompanyId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForEntertainmentConfiguration(EntertainmentConfigurationEntertainmentEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.EntertainmentConfigurationId, entity.EntertainmentConfigurationId, TimestampFields.ModifiedEntertainmentConfigurationUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(EntertainmentConfigurationEntertainmentEntity entity, bool wasNew)
        {
            this.UpdateTimestampForEntertainmentConfiguration(entity);
        }

        #endregion
    }
}
