using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(PointOfInterestVenueCategoryEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class PointOfInterestVenueCategoryTimestampUpdater : GeneralTimestampUpdater<PointOfInterestVenueCategoryEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    PointOfInterestVenueCategoryFields.PointOfInterestId,
                    PointOfInterestVenueCategoryFields.VenueCategoryId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForVenueCategories(PointOfInterestVenueCategoryEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.PointOfInterestId, entity.PointOfInterestId, TimestampFields.ModifiedPointOfInterestVenueCategoriesUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(PointOfInterestVenueCategoryEntity entity, bool wasNew)
        {
            this.UpdateTimestampForVenueCategories(entity);
        }

        #endregion
    }
}
