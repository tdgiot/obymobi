using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(UIFooterItemEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class UiFooterItemTimestampUpdater : GeneralTimestampUpdater<UIFooterItemEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    UIFooterItemFields.UIFooterItemId,
                    UIFooterItemFields.Name,
                    UIFooterItemFields.Type,
                    UIFooterItemFields.Position,
                    UIFooterItemFields.SortOrder,
                    UIFooterItemFields.ActionIntent,
                    UIFooterItemFields.Visible,
                    UIFooterItemFields.ParentCompanyId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForUIFooterItem(UIFooterItemEntity entity)
        {
            UIFooterItemEntity uiFooterItemEntity = new UIFooterItemEntity();
            uiFooterItemEntity.AddToTransaction(entity);
            uiFooterItemEntity.FetchUsingPK(entity.UIFooterItemId, null, null, new IncludeFieldsList { UIFooterItemFields.UIModeId });
            if (!uiFooterItemEntity.IsNew)
            {
                this.UpdateTimestampField(TimestampFields.UIModeId, uiFooterItemEntity.UIModeId, TimestampFields.ModifiedUIModeUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(UIFooterItemEntity entity, bool wasNew)
        {
            this.UpdateTimestampForUIFooterItem(entity);
        }

        #endregion
    }
}
