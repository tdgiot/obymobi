using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(CloudStorageAccountEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class CloudStorageAccountTimestampUpdater : GeneralTimestampUpdater<CloudStorageAccountEntity>
    {
        #region Methods

        internal void UpdateTimestampForCloudStorageAccounts(CloudStorageAccountEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId, TimestampFields.ModifiedCloudStorageAccountsUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(CloudStorageAccountEntity entity, bool wasNew)
        {
            this.UpdateTimestampForCloudStorageAccounts(entity);
        }

        #endregion
    }
}
