using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(PriceScheduleItemEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class PriceScheduleItemTimestampUpdater : GeneralTimestampUpdater<PriceScheduleItemEntity>
    {
        #region Methods

        internal void UpdateTimestampForPriceSchedule(PriceScheduleItemEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.PriceScheduleId, entity.PriceScheduleId, TimestampFields.ModifiedPriceScheduleUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(PriceScheduleItemEntity entity, bool wasNew)
        {
            this.UpdateTimestampForPriceSchedule(entity);
        }

        #endregion
    }
}
