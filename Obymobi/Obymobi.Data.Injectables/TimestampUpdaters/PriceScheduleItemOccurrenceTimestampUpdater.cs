using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(PriceScheduleItemOccurrenceEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class PriceScheduleItemOccurrenceTimestampUpdater : GeneralTimestampUpdater<PriceScheduleItemOccurrenceEntity>
    {
        #region Methods

        internal void UpdateTimestampForPriceSchedule(PriceScheduleItemOccurrenceEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PriceScheduleItemFields.PriceScheduleItemId == entity.PriceScheduleItemId };

            PriceScheduleItemCollection priceScheduleItemCollection = new PriceScheduleItemCollection();
            priceScheduleItemCollection.AddToTransaction(entity);
            priceScheduleItemCollection.GetMulti(filter, 0, null, null, null, new IncludeFieldsList(PriceScheduleItemFields.PriceScheduleId, PriceScheduleItemFields.ParentCompanyId), 0, 0);

            foreach (PriceScheduleItemEntity priceScheduleItemEntity in priceScheduleItemCollection)
            {
                this.UpdateTimestampField(TimestampFields.PriceScheduleId, priceScheduleItemEntity.PriceScheduleId, TimestampFields.ModifiedPriceScheduleUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(PriceScheduleItemOccurrenceEntity entity, bool wasNew)
        {
            this.UpdateTimestampForPriceSchedule(entity);
        }

        #endregion
    }
}
