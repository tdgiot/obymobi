using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(DeliverypointgroupEntertainmentEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class DeliverypointgroupEntertainmentTimestampUpdater : GeneralTimestampUpdater<DeliverypointgroupEntertainmentEntity>
    {
        #region Methods

        public override void UpdateTimestamps(IEntity entity, bool wasNew)
        {
            DeliverypointgroupEntertainmentEntity useableEntity = entity as DeliverypointgroupEntertainmentEntity;
            if (useableEntity == null)
                return;

            if (useableEntity.DeliverypointgroupEntity.ClientConfigurationId.HasValue && useableEntity.DeliverypointgroupEntity.ClientConfigurationEntity.EntertainmentConfigurationId.HasValue)
            {
                this.UpdateTimestampForEntertainmentConfiguration(useableEntity.DeliverypointgroupEntity.ClientConfigurationEntity);
            }
        }

        internal void UpdateTimestampForEntertainmentConfiguration(ClientConfigurationEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.EntertainmentConfigurationId, entity.EntertainmentConfigurationId, TimestampFields.ModifiedEntertainmentConfigurationUTC, entity.GetCurrentTransaction());
        }

        #endregion
    }
}
