using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(VenueCategoryEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class VenueCategoryTimestampUpdater : GeneralTimestampUpdater<VenueCategoryEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    VenueCategoryFields.Name,
                    VenueCategoryFields.MarkerIcon
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForVenueCategoryViaCompanyVenueCategory(VenueCategoryEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { CompanyVenueCategoryFields.VenueCategoryId == entity.VenueCategoryId };

            CompanyVenueCategoryCollection companyVenueCategoryCollection = new CompanyVenueCategoryCollection();
            companyVenueCategoryCollection.AddToTransaction(entity);
            companyVenueCategoryCollection.GetMulti(filter, new IncludeFieldsList { CompanyVenueCategoryFields.CompanyId }, null);

            foreach (CompanyVenueCategoryEntity companyVenueCategoryEntity in companyVenueCategoryCollection)
            {
                this.UpdateTimestampField(TimestampFields.CompanyId, companyVenueCategoryEntity.CompanyId, TimestampFields.ModifiedCompanyVenueCategoriesUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForVenueCategoryViaPointOfInterestVenueCategory(VenueCategoryEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PointOfInterestVenueCategoryFields.VenueCategoryId == entity.VenueCategoryId };

            PointOfInterestVenueCategoryCollection pointOfInterestVenueCategoryCollection = new PointOfInterestVenueCategoryCollection();
            pointOfInterestVenueCategoryCollection.AddToTransaction(entity);
            pointOfInterestVenueCategoryCollection.GetMulti(filter, new IncludeFieldsList { PointOfInterestVenueCategoryFields.PointOfInterestId }, null);

            foreach (PointOfInterestVenueCategoryEntity pointOfInterestVenueCategoryEntity in pointOfInterestVenueCategoryCollection)
            {
                this.UpdateTimestampField(TimestampFields.PointOfInterestId, pointOfInterestVenueCategoryEntity.PointOfInterestId, TimestampFields.ModifiedPointOfInterestVenueCategoriesUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(VenueCategoryEntity entity, bool wasNew)
        {
            this.UpdateTimestampForVenueCategoryViaCompanyVenueCategory(entity);
            this.UpdateTimestampForVenueCategoryViaPointOfInterestVenueCategory(entity);
        }

        #endregion
    }
}
