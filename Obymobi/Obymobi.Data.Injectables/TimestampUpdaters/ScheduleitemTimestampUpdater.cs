using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ScheduleitemEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ScheduleitemTimestampUpdater : GeneralTimestampUpdater<ScheduleitemEntity>
    {
        #region Methods

        /// <summary>
        /// Create a relation from the product(s) to the schedule item entity
        /// </summary>
        /// <param name="entity">Schedule item entity which is used for relation invoking.</param>
        internal void UpdateTimestampForScheduleProduct(ScheduleitemEntity entity)
        {
            this.UpdateTimestampForMenu(entity, new RelationCollection
            {
                ScheduleEntity.Relations.ProductEntityUsingScheduleId,
                ProductEntity.Relations.ProductCategoryEntityUsingProductId,
                ProductCategoryEntity.Relations.CategoryEntityUsingCategoryId,
                CategoryEntity.Relations.MenuEntityUsingMenuId
            });
        }

        /// <summary>
        /// Create a relation directly to the schedule item entity
        /// </summary>
        /// <param name="entity">Schedule item entity which is used for relation invoking.</param>
        internal void UpdateTimestampForScheduleCategory(ScheduleitemEntity entity)
        {
            this.UpdateTimestampForMenu(entity, new RelationCollection()
            {
                ScheduleEntity.Relations.CategoryEntityUsingScheduleId,
                CategoryEntity.Relations.MenuEntityUsingMenuId
            });
        }

        /// <summary>
        /// Default method which is dynamically invoked it's base class
        /// </summary>
        /// <param name="entity">Schedule item entity which is used for relation invoking</param>
        /// <param name="wasNew">Determines if the entity item is new or not</param>
        protected override void UpdateTimestamps(ScheduleitemEntity entity, bool wasNew)
        {
            this.UpdateTimestampForScheduleProduct(entity);
            this.UpdateTimestampForScheduleCategory(entity);
        }

        /// <summary>
        /// Process all menu collection which require a timestamp update based on the applied changes.
        /// </summary>
        /// <param name="entity">Schedule item entity which is used for relation invoking</param>
        /// <param name="relations">All relations which require a timestamp update for the publishing page.</param>
        private void UpdateTimestampForMenu(ScheduleitemEntity entity, RelationCollection relations)
        {
            PredicateExpression filter = new PredicateExpression { ScheduleFields.ScheduleId == entity.ScheduleId };

            MenuCollection menuCollection = new MenuCollection();
            menuCollection.AddToTransaction(entity);
            menuCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(MenuFields.MenuId), 0, 0);

            foreach (MenuEntity menuEntity in menuCollection)
                this.UpdateTimestampField(TimestampFields.MenuId, menuEntity.MenuId, TimestampFields.ModifiedSchedulesUTC, entity.GetCurrentTransaction());
        }

        #endregion
    }
}
