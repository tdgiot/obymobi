using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(RoomControlSectionItemEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class RoomControlSectionItemTimestampUpdater : GeneralTimestampUpdater<RoomControlSectionItemEntity>
    {
        #region Methods

        internal void UpdateTimestampForRoomControlSectionItem(RoomControlSectionItemEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { RoomControlSectionItemFields.RoomControlSectionItemId == entity.RoomControlSectionItemId };
            RelationCollection relations = new RelationCollection 
            { 
                RoomControlSectionItemEntity.Relations.RoomControlSectionEntityUsingRoomControlSectionId,
                RoomControlSectionEntity.Relations.RoomControlAreaEntityUsingRoomControlAreaId 
            };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(RoomControlSectionItemEntity entity, bool wasNew)
        {
            this.UpdateTimestampForRoomControlSectionItem(entity);
        }

        #endregion
    }
}
