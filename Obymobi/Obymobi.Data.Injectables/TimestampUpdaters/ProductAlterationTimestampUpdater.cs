using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ProductAlterationEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ProductAlterationTimestampUpdater : GeneralTimestampUpdater<ProductAlterationEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(ProductAlterationEntity entity, bool wasNew)
        {
            if (entity.ProductEntity.BrandId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForBrandProducts(entity.ProductEntity.BrandId.Value, entity.GetCurrentTransaction());
            }
            else if (entity.ProductEntity.CompanyId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForProducts(entity.ProductEntity.CompanyId.Value, entity.GetCurrentTransaction());
            }
        }

        #endregion
    }
}
