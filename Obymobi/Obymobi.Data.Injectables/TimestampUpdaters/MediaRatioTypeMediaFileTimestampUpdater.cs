using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MediaRatioTypeMediaFileEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MediaRatioTypeMediaFileTimestampUpdater : GeneralTimestampUpdater<MediaRatioTypeMediaFileEntity>
    {
        // TODO NEW API
        // Updating the timestamps for this is done through MediaTimestampUpdater
    }
}
