using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(StationEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class StationTimestampUpdater : GeneralTimestampUpdater<StationEntity>
    {
        #region Methods

        internal void UpdateTimestampForStationList(StationEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { StationListFields.StationListId == entity.StationListId };
            RelationCollection relations = new RelationCollection 
            { 
                StationListEntity.Relations.RoomControlSectionItemEntityUsingStationListId,
                RoomControlSectionItemEntity.Relations.RoomControlSectionEntityUsingRoomControlSectionId,
                RoomControlSectionEntity.Relations.RoomControlAreaEntityUsingRoomControlAreaId 
            };

            RoomControlAreaCollection roomControlAreaCollection = new RoomControlAreaCollection();
            roomControlAreaCollection.AddToTransaction(entity);
            roomControlAreaCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(RoomControlAreaFields.RoomControlConfigurationId), 0, 0);

            foreach (RoomControlAreaEntity roomControlAreaEntity in roomControlAreaCollection)
            {
                this.UpdateTimestampField(TimestampFields.RoomControlConfigurationId, roomControlAreaEntity.RoomControlConfigurationId, TimestampFields.ModifiedRoomControlConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(StationEntity entity, bool wasNew)
        {
            this.UpdateTimestampForStationList(entity);
        }

        #endregion
    }
}
