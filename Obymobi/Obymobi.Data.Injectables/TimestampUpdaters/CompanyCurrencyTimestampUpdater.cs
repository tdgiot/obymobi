using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(CompanyCurrencyEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class CompanyCurrencyTimestampUpdater : GeneralTimestampUpdater<CompanyCurrencyEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    CompanyCurrencyFields.CompanyId,
                    CompanyCurrencyFields.CurrencyCode
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForCompany(CompanyCurrencyEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId, TimestampFields.ModifiedCompanyUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(CompanyCurrencyEntity entity, bool wasNew)
        {
            this.UpdateTimestampForCompany(entity);
        }

        #endregion
    }
}
