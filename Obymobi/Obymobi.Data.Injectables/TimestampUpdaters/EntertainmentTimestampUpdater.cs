using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(EntertainmentEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class EntertainmentTimestampUpdater : GeneralTimestampUpdater<EntertainmentEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    EntertainmentFields.AppCloseInterval,
                    EntertainmentFields.AppDataClearInterval,
                    EntertainmentFields.BackButton,
                    EntertainmentFields.ClassName,
                    EntertainmentFields.DefaultEntertainmenturlId,
                    EntertainmentFields.Description,
                    EntertainmentFields.EntertainmentcategoryId,
                    EntertainmentFields.EntertainmentFileId,
                    EntertainmentFields.EntertainmentFileVersion,
                    EntertainmentFields.EntertainmentId,
                    EntertainmentFields.EntertainmentType,
                    EntertainmentFields.HomeButton,
                    EntertainmentFields.MenuContainer,
                    EntertainmentFields.Name,
                    EntertainmentFields.NavigateButton,
                    EntertainmentFields.NavigationBar,
                    EntertainmentFields.PackageName,
                    EntertainmentFields.PostfixWithTimestamp,
                    EntertainmentFields.PreventCaching,
                    EntertainmentFields.RestrictedAccess,
                    EntertainmentFields.TitleAsHeader
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampsForEntertainmentConfiguration(EntertainmentEntity entity)
        {
            PredicateExpression filter = new PredicateExpression(EntertainmentConfigurationEntertainmentFields.EntertainmentId == entity.EntertainmentId);

            EntertainmentConfigurationEntertainmentCollection entertainmentConfigurationEntertainmentCollection = new EntertainmentConfigurationEntertainmentCollection();
            entertainmentConfigurationEntertainmentCollection.AddToTransaction(entity);
            entertainmentConfigurationEntertainmentCollection.GetMulti(filter, new IncludeFieldsList(EntertainmentConfigurationEntertainmentFields.EntertainmentConfigurationId, EntertainmentConfigurationEntertainmentFields.ParentCompanyId), null);

            foreach (EntertainmentConfigurationEntertainmentEntity entertainmentConfigurationEntertainmentEntity in entertainmentConfigurationEntertainmentCollection)
            {
                this.UpdateTimestampField(TimestampFields.EntertainmentConfigurationId, entertainmentConfigurationEntertainmentEntity.EntertainmentConfigurationId, TimestampFields.ModifiedEntertainmentConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        internal void UpdateTimestampForDeliverypointgroupEntertainment(EntertainmentEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { DeliverypointgroupEntertainmentFields.EntertainmentId == entity.EntertainmentId };

            RelationCollection relationCollection = new RelationCollection
            {
                ClientConfigurationEntityBase.Relations.DeliverypointgroupEntityUsingClientConfigurationId,
                DeliverypointgroupEntityBase.Relations.DeliverypointgroupEntertainmentEntityUsingDeliverypointgroupId
            };

            IncludeFieldsList includeFieldsList = new IncludeFieldsList { ClientConfigurationFields.EntertainmentConfigurationId };

            ClientConfigurationCollection clientConfigurationCollection = new ClientConfigurationCollection();
            clientConfigurationCollection.AddToTransaction(entity);
            clientConfigurationCollection.GetMulti(filter, 0, null, relationCollection, null, includeFieldsList, 0, 0);

            foreach (ClientConfigurationEntity clientConfigurationEntity in clientConfigurationCollection)
            {
                this.UpdateTimestampField(TimestampFields.EntertainmentConfigurationId, clientConfigurationEntity.EntertainmentConfigurationId, TimestampFields.ModifiedEntertainmentConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(EntertainmentEntity entity, bool wasNew)
        {
            this.UpdateTimestampsForEntertainmentConfiguration(entity);
            this.UpdateTimestampForDeliverypointgroupEntertainment(entity);
        }

        #endregion
    }
}
