using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(UIModeEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class UIModeTimestampUpdater : GeneralTimestampUpdater<UIModeEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    UIModeFields.UIModeId,
                    UIModeFields.Type,
                    UIModeFields.Name,
                    UIModeFields.ShowServiceOptions,
                    UIModeFields.ShowRequestBill,
                    UIModeFields.ShowDeliverypoint,
                    UIModeFields.ShowClock,
                    UIModeFields.ShowBattery,
                    UIModeFields.ShowFullscreenEyecatcher,
                    UIModeFields.ShowFooterLogo,
                    UIModeFields.DefaultUITabId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForUIMode(UIModeEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.UIModeId, entity.UIModeId, TimestampFields.ModifiedUIModeUTC, entity.GetCurrentTransaction());
        }

        internal void UpdateTimestampForUIModes(UIModeEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId.Value, TimestampFields.ModifiedUIModesUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(UIModeEntity entity, bool wasNew)
        {
            this.UpdateTimestampForUIMode(entity);

            if (entity.CompanyId.HasValue)
                this.UpdateTimestampForUIModes(entity);
        }

        #endregion
    }
}
