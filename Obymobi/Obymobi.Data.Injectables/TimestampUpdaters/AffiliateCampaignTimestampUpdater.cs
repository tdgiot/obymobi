using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(AffiliateCampaignEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class AffiliateCampaignTimestampUpdater : GeneralTimestampUpdater<AffiliateCampaignEntity>
    {
        protected override void UpdateTimestamps(AffiliateCampaignEntity entity, bool wasNew)
        {
            PredicateExpression filter = new PredicateExpression(DeliverypointgroupFields.AffiliateCampaignId == entity.AffiliateCampaignId);

            DeliverypointgroupCollection deliverypointgroups = new DeliverypointgroupCollection();
            deliverypointgroups.AddToTransaction(entity);
            deliverypointgroups.GetMulti(filter, new IncludeFieldsList(DeliverypointgroupFields.DeliverypointgroupId, DeliverypointgroupFields.CompanyId), null);

            foreach (DeliverypointgroupEntity deliverypointgroup in deliverypointgroups)
            {
                if (!deliverypointgroup.ClientConfigurationId.HasValue)
                {
                    continue;
                }
                
                UpdateTimestampField(TimestampFields.ClientConfigurationId, deliverypointgroup.ClientConfigurationId, TimestampFields.ModifiedClientConfigurationUTC, entity.GetCurrentTransaction());
            }
        }
    }
}
