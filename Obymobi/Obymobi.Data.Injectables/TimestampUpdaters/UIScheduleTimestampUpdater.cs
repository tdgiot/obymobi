using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(UIScheduleEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class UIScheduleTimestampUpdater : GeneralTimestampUpdater<UIScheduleEntity>
    {
        #region Methods

        internal void UpdateTimestampForUISchedule(UIScheduleEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.UIScheduleId, entity.UIScheduleId, TimestampFields.ModifiedUIScheduleUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(UIScheduleEntity entity, bool wasNew)
        {
            this.UpdateTimestampForUISchedule(entity);
        }

        #endregion
    }
}
