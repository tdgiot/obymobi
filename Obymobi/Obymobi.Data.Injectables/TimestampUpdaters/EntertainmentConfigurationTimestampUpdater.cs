using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(EntertainmentConfigurationEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class EntertainmentConfigurationTimestampUpdater : GeneralTimestampUpdater<EntertainmentConfigurationEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    EntertainmentConfigurationFields.Name
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForEntertainmentConfiguration(EntertainmentConfigurationEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.EntertainmentConfigurationId, entity.EntertainmentConfigurationId, TimestampFields.ModifiedEntertainmentConfigurationUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(EntertainmentConfigurationEntity entity, bool wasNew)
        {
            this.UpdateTimestampForEntertainmentConfiguration(entity);
        }

        #endregion
    }
}
