using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(CompanyCultureEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class CompanyCultureTimestampUpdater : GeneralTimestampUpdater<CompanyCultureEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    CompanyCultureFields.CompanyId,
                    CompanyCultureFields.CultureCode
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForCompany(CompanyCultureEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId, TimestampFields.ModifiedCompanyUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(CompanyCultureEntity entity, bool wasNew)
        {
            this.UpdateTimestampForCompany(entity);
        }

        #endregion
    }
}
