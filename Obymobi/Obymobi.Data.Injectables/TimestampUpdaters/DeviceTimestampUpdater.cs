using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(DeviceEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class DeviceTimestampUpdater : GeneralTimestampUpdater<DeviceEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(DeviceEntity entity, bool wasNew)
        {
            // TODO NEW API
        }

        #endregion
    }
}
