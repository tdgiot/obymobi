using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ReleaseEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ReleaseTimestampUpdater : GeneralTimestampUpdater<ReleaseEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    ReleaseFields.ReleaseId,
                    ReleaseFields.Crc32,
                    ReleaseFields.Intermediate,
                    ReleaseFields.Version
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForCompanyReleases(ReleaseEntity entity)
        {
            PredicateExpression filter = new PredicateExpression(CompanyReleaseFields.ReleaseId == entity.ReleaseId);

            CompanyReleaseCollection companyReleaseCollection = new CompanyReleaseCollection();
            companyReleaseCollection.AddToTransaction(entity);
            companyReleaseCollection.GetMulti(filter, new IncludeFieldsList(CompanyReleaseFields.CompanyId), null);

            foreach (CompanyReleaseEntity companyReleaseEntity in companyReleaseCollection)
            {
                this.UpdateTimestampField(TimestampFields.CompanyId, companyReleaseEntity.CompanyId, TimestampFields.ModifiedCompanyReleasesUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(ReleaseEntity entity, bool wasNew)
        {
            this.UpdateTimestampForCompanyReleases(entity);
        }

        #endregion
    }
}
