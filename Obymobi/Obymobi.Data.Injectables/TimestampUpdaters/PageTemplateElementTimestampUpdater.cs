using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(PageTemplateElementEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class PageTemplateElementTimestampUpdater : GeneralTimestampUpdater<PageTemplateElementEntity>
    {
        #region Methods

        internal void UpdateTimestampForPage(PageTemplateElementEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PageFields.PageTemplateId == entity.PageTemplateId };

            PageCollection pageCollection = new PageCollection();
            pageCollection.AddToTransaction(entity);
            pageCollection.GetMulti(filter, new IncludeFieldsList(PageFields.SiteId, PageFields.ParentCompanyId), null);

            foreach (PageEntity pageEntity in pageCollection)
            {
                this.UpdateTimestampField(TimestampFields.SiteId, pageEntity.SiteId, TimestampFields.ModifiedSiteUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(PageTemplateElementEntity entity, bool wasNew)
        {
            this.UpdateTimestampForPage(entity);
        }

        #endregion
    }
}
