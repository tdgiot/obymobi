using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.ExtensionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(UIScheduleItemOccurrenceEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class UIScheduleItemOccurrenceTimestampUpdater : GeneralTimestampUpdater<UIScheduleItemOccurrenceEntity>
    {
        internal void UpdateTimeStampForUISchedule(UIScheduleItemOccurrenceEntity entity)
        {
            if (entity.UIScheduleItemId.HasValue && entity.UIScheduleItemEntity.IsHandledClientSide())
            {
                UpdateTimestampField(TimestampFields.UIScheduleId, entity.UIScheduleItemEntity.UIScheduleId, TimestampFields.ModifiedUIScheduleUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(UIScheduleItemOccurrenceEntity entity, bool wasNew)
        {
            UpdateTimeStampForUISchedule(entity);
        }
    }
}
