using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(OrderitemAlterationitemEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class OrderitemAlterationitemTimestampUpdater : GeneralTimestampUpdater<OrderitemAlterationitemEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(OrderitemAlterationitemEntity entity, bool wasNew)
        {
            // TODO NEW API
            // First see how we want to implement this
        }

        #endregion
    }
}
