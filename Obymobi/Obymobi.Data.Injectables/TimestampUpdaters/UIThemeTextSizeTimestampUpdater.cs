using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(UIThemeTextSizeEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class UIThemeTextSizeTimestampUpdater : GeneralTimestampUpdater<UIThemeTextSizeEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    UIThemeTextSizeFields.UIThemeTextSizeId,
                    UIThemeTextSizeFields.Type,
                    UIThemeTextSizeFields.TextSize,
                    UIThemeTextSizeFields.ParentCompanyId
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampForUITheme(UIThemeTextSizeEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.UIThemeId, entity.UIThemeId, TimestampFields.ModifiedUIThemeUTC, entity.GetCurrentTransaction());
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(UIThemeTextSizeEntity entity, bool wasNew)
        {
            this.UpdateTimestampForUITheme(entity);
        }

        #endregion
    }
}
