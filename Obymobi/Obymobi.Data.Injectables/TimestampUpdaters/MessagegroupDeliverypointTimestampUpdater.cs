using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MessagegroupDeliverypointEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MessagegroupDeliverypointTimestampUpdater : GeneralTimestampUpdater<MessagegroupDeliverypointEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(MessagegroupDeliverypointEntity entity, bool wasNew)
        {
            MessagegroupEntity messagegroupEntity = new MessagegroupEntity();
            messagegroupEntity.AddToTransaction(entity);
            messagegroupEntity.FetchUsingPK(entity.MessagegroupId, null, null, new IncludeFieldsList { MessagegroupFields.CompanyId });
            if (!messagegroupEntity.IsNew)
                this.UpdateTimestampField(TimestampFields.CompanyId, messagegroupEntity.CompanyId, TimestampFields.ModifiedMessagegroupsUTC, entity.GetCurrentTransaction());
        }

        #endregion
    }
}
