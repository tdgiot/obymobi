using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(ProductCategoryEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class ProductCategoryTimestampUpdater : GeneralTimestampUpdater<ProductCategoryEntity>
    {
        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    ProductCategoryFields.ProductId,
                    ProductCategoryFields.CategoryId,
                    ProductCategoryFields.ProductCategoryId,
                    ProductCategoryFields.SortOrder,
                    ProductCategoryFields.CategoryId,
                    ProductCategoryFields.ParentCompanyId
                };
            }
        }

        protected override void UpdateTimestamps(ProductCategoryEntity entity, bool wasNew)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            if (entity.CategoryEntity == null)
            {
                throw new ArgumentNullException(nameof(entity.CategoryEntity), $"Product-category entity '{entity.ProductCategoryId}' does not have a Category entity. Company: {entity.ParentCompanyId}.");
            }

            if (entity.CategoryEntity.MenuId.HasValue)
            {
                new MenuTimestampUpdater().UpdateTimestampForMenu(entity.CategoryEntity.MenuId.Value, entity.GetCurrentTransaction());
            }
        }
    }
}
