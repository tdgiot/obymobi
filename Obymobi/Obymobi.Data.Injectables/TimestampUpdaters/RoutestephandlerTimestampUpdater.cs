using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(RoutestephandlerEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class RoutestephandlerTimestampUpdater : GeneralTimestampUpdater<RoutestephandlerEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(RoutestephandlerEntity entity, bool wasNew)
        {
            // TODO NEW API
        }

        #endregion
    }
}
