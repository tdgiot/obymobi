﻿using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(AdvertisementEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class AdvertisementTimestampUpdater : GeneralTimestampUpdater<AdvertisementEntity>
    {
        #region Properties

        protected override IEnumerable<EntityField> Fields
        {
            get
            {
                return new List<EntityField>
                {
                    AdvertisementFields.AdvertisementId,
                    AdvertisementFields.Name,
                    AdvertisementFields.ProductCategoryId,
                    AdvertisementFields.ActionCategoryId,
                    AdvertisementFields.ActionEntertainmentId,
                    AdvertisementFields.ActionEntertainmentCategoryId,
                    AdvertisementFields.ActionUrl
                };
            }
        }

        #endregion

        #region Methods

        internal void UpdateTimestampsForAdvertisement(AdvertisementEntity entity)
        {
            PredicateExpression filter = new PredicateExpression(AdvertisementConfigurationAdvertisementFields.AdvertisementId == entity.AdvertisementId);

            AdvertisementConfigurationAdvertisementCollection advertisementConfigurationAdvertisementCollection = new AdvertisementConfigurationAdvertisementCollection();
            advertisementConfigurationAdvertisementCollection.AddToTransaction(entity);
            advertisementConfigurationAdvertisementCollection.GetMulti(filter, new IncludeFieldsList(AdvertisementConfigurationAdvertisementFields.AdvertisementConfigurationId, AdvertisementConfigurationAdvertisementFields.ParentCompanyId), null);

            foreach (AdvertisementConfigurationAdvertisementEntity advertisementConfigurationAdvertisementEntity in advertisementConfigurationAdvertisementCollection)
            {
                this.UpdateTimestampField(TimestampFields.AdvertisementConfigurationId, advertisementConfigurationAdvertisementEntity.AdvertisementConfigurationId, TimestampFields.ModifiedAdvertisementConfigurationUTC, entity.GetCurrentTransaction());
            }
        }

        protected override void UpdateTimestamps(AdvertisementEntity entity, bool wasNew)
        {
            this.UpdateTimestampsForAdvertisement(entity);
        }

        #endregion
    }
}
