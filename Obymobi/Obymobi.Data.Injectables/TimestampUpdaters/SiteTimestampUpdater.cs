using System;
using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(SiteEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class SiteTimestampUpdater : GeneralTimestampUpdater<SiteEntity>
    {
        #region Methods

        internal void UpdateTimestampForSite(SiteEntity entity)
        {
            this.UpdateTimestampField(TimestampFields.SiteId, entity.SiteId, TimestampFields.ModifiedSiteUTC, entity.GetCurrentTransaction());
        }

        protected override void UpdateTimestamps(SiteEntity entity, bool wasNew)
        {
            this.UpdateTimestampForSite(entity);
        }

        #endregion
    }
}
