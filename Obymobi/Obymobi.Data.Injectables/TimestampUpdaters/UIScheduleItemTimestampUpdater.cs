using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.ExtensionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(UIScheduleItemEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class UIScheduleItemTimestampUpdater : GeneralTimestampUpdater<UIScheduleItemEntity>
    {
        #region Methods

        internal void UpdateTimestampForUISchedule(UIScheduleItemEntity entity)
        {
            if (entity.IsHandledClientSide())
            {
                UpdateTimestampField(TimestampFields.UIScheduleId, entity.UIScheduleId, TimestampFields.ModifiedUIScheduleUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(UIScheduleItemEntity entity, bool wasNew)
        {
            UpdateTimestampForUISchedule(entity);
        }

        #endregion
    }
}
