using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(PriceLevelEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class PriceLevelTimestampUpdater : GeneralTimestampUpdater<PriceLevelEntity>
    {
        #region Methods

        internal void UpdateTimestampForPriceLevelItem(PriceLevelEntity entity)
        {
            PredicateExpression filter = new PredicateExpression { PriceLevelFields.PriceLevelId == entity.PriceLevelId };
            RelationCollection relations = new RelationCollection { PriceLevelEntity.Relations.PriceScheduleItemEntityUsingPriceLevelId };

            PriceScheduleItemCollection priceScheduleItemCollection = new PriceScheduleItemCollection();
            priceScheduleItemCollection.AddToTransaction(entity);
            priceScheduleItemCollection.GetMulti(filter, 0, null, relations, null, new IncludeFieldsList(PriceScheduleItemFields.PriceScheduleId, PriceScheduleItemFields.ParentCompanyId), 0, 0);

            foreach (PriceScheduleItemEntity priceScheduleItemEntity in priceScheduleItemCollection)
            {
                this.UpdateTimestampField(TimestampFields.PriceScheduleId, priceScheduleItemEntity.PriceScheduleId, TimestampFields.ModifiedPriceScheduleUTC, entity.GetCurrentTransaction());
            }
        }

        /// <inheritdoc/>
        protected override void UpdateTimestamps(PriceLevelEntity entity, bool wasNew)
        {
            this.UpdateTimestampForPriceLevelItem(entity);
        }

        #endregion
    }
}
