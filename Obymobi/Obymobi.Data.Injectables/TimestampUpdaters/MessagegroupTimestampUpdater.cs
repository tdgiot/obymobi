using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.TimestampUpdaters
{
    [DependencyInjectionInfo(typeof(MessagegroupEntity), "TimestampUpdater", ContextType = DependencyInjectionContextType.Singleton)]
    [Serializable]
    public class MessagegroupTimestampUpdater : GeneralTimestampUpdater<MessagegroupEntity>
    {
        #region Methods

        /// <inheritdoc/>
        protected override void UpdateTimestamps(MessagegroupEntity entity, bool wasNew)
        {
            this.UpdateTimestampField(TimestampFields.CompanyId, entity.CompanyId, TimestampFields.ModifiedMessagegroupsUTC, entity.GetCurrentTransaction());
        }

        #endregion
    }
}
