﻿using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Data.Injectables
{
    /// <summary>
    /// THIS IS NOT INTENDED TO BE A GOOD STRUCTURAL SOLUTION!!!! 
    /// It's done because it was required to have this functionality on short term, a Pivot is
    /// created for a structured solution: https://www.pivotaltracker.com/story/show/65545466
    /// </summary>
    public static class LastModifiedHelper
    {
        public static void UpdateLastModified(IEntityCore entity, bool delete)
        {
            if(delete)
                entity.IsDirty = true;

            var entityTypeAsEnum = entity.LLBLGenProEntityTypeValue.ToEnum<EntityType>();
            switch (entityTypeAsEnum)
            { 
                case EntityType.PointOfInterestAmenityEntity:
                    ((PointOfInterestAmenityEntity)entity).UpdateLastModified();
                    break;
                case EntityType.PointOfInterestVenueCategoryEntity:
                    ((PointOfInterestVenueCategoryEntity)entity).UpdateLastModified();
                    break;
                case EntityType.UITabEntity:
                    ((UITabEntity)entity).UpdateLastModified();
                    break;                
                case EntityType.MediaEntity:
                    ((MediaEntity)entity).UpdateLastModified();
                    break;
                case EntityType.BusinesshoursEntity:
                    ((BusinesshoursEntity)entity).UpdateLastModified();
                    break;
                case EntityType.PageEntity:
                    ((PageEntity)entity).UpdateLastModified();
                    break;
                case EntityType.PageElementEntity:
                    ((PageElementEntity)entity).UpdateLastModified();
                    break;
                case EntityType.MediaRatioTypeMediaEntity:
                    ((MediaRatioTypeMediaEntity)entity).UpdateLastModified();
                    break;
                case EntityType.MediaRatioTypeMediaFileEntity:
                    ((MediaRatioTypeMediaFileEntity)entity).UpdateLastModified();
                    break;
                case EntityType.AttachmentEntity:
                    ((AttachmentEntity)entity).UpdateLastModified();
                    break;
                case EntityType.PageTemplateEntity:
                    ((PageTemplateEntity)entity).UpdateLastModified();
                    break;
                case EntityType.PageTemplateElementEntity:
                    ((PageTemplateElementEntity)entity).UpdateLastModified();
                    break;
                case EntityType.CustomTextEntity:
                    ((CustomTextEntity)entity).UpdateLastModified();
                    break;
                default:
                    break;
            }
        }        

        private static void UpdateLastModified(this PointOfInterestAmenityEntity poiAmenity)
        {
            if (poiAmenity.IsDirty)
                poiAmenity.PointOfInterestEntity.UpdatePointOfInterestLastModified(poiAmenity);
        }

        private static void UpdateLastModified(this PointOfInterestVenueCategoryEntity poiVenueCategory)
        {
            if (poiVenueCategory.IsDirty)
                poiVenueCategory.PointOfInterestEntity.UpdatePointOfInterestLastModified(poiVenueCategory);
        }

        private static void UpdateLastModified(this UITabEntity uiTab)
        {
            if (uiTab.IsDirty)
                if (uiTab.UIModeEntity.PointOfInterestId.HasValue)
                    uiTab.UIModeEntity.PointOfInterestEntity.UpdatePointOfInterestLastModified(uiTab);
        }

        private static void UpdateLastModified(this CustomTextEntity customText)
        {
            if (customText.IsDirty)
            {
                if (customText.UITabId.HasValue && customText.UITabEntity.UIModeEntity.PointOfInterestId.HasValue)
                    customText.UITabEntity.UIModeEntity.PointOfInterestEntity.UpdatePointOfInterestLastModified(customText);
                else if (customText.SiteId.HasValue)
                    customText.SiteEntity.UpdateSiteLastModified(customText);
                else if (customText.PageId.HasValue)
                    customText.PageEntity.SiteEntity.UpdateSiteLastModified(customText);
                else if (customText.SiteTemplateId.HasValue)
                    customText.SiteTemplateEntity.UpdateLastModified(customText);
                else if (customText.PageTemplateId.HasValue)
                    customText.PageTemplateEntity.SiteTemplateEntity.UpdateLastModified(customText);
                else if (customText.AttachmentId.HasValue)
                {
                    if (customText.AttachmentEntity.PageId.HasValue)
                        customText.AttachmentEntity.PageEntity.SiteEntity.UpdateSiteLastModified(customText.AttachmentEntity);
                    else if (customText.AttachmentEntity.PageTemplateId.HasValue)
                        customText.AttachmentEntity.PageTemplateEntity.SiteTemplateEntity.UpdateLastModified(customText.AttachmentEntity);
                }
            }
        }

        private static void UpdateLastModified(this MediaEntity media, bool force = false)
        {
            if (media.IsDirty || force)
            {
                if (media.PointOfInterestId.HasValue)
                    media.PointOfInterestEntity.UpdatePointOfInterestLastModified(media);
                if(media.PageId.HasValue)
                    media.PageEntity.SiteEntity.UpdateSiteLastModified(media);
                if (media.PageElementId.HasValue)
                    media.PageElementEntity.PageEntity.SiteEntity.UpdateSiteLastModified(media);
                if (media.AttachmentId.HasValue)
                {
                    if (media.AttachmentEntity.PageId.HasValue)
                    {
                        media.AttachmentEntity.AddToTransaction(media);
                        media.AttachmentEntity.PageEntity.SiteEntity.UpdateSiteLastModified(media.AttachmentEntity);
                    }
                    else if (media.AttachmentEntity.PageTemplateId.HasValue)
                    {
                        media.AttachmentEntity.AddToTransaction(media);
                        media.AttachmentEntity.PageTemplateEntity.SiteTemplateEntity.UpdateLastModified(media.AttachmentEntity);
                    }
                }
                if (media.PageTemplateId.HasValue)
                    media.PageTemplateEntity.SiteTemplateEntity.UpdateLastModified(media);
                if (media.PageTemplateElementId.HasValue)
                    media.PageTemplateElementEntity.PageTemplateEntity.SiteTemplateEntity.UpdateLastModified(media);
                if (media.SiteId.HasValue)
                    media.SiteEntity.UpdateSiteLastModified(media);
            }
        }

        private static void UpdateLastModified(this MediaRatioTypeMediaEntity mediaFile)
        {
            mediaFile.MediaEntity.AddToTransaction(mediaFile);
            mediaFile.MediaEntity.UpdateLastModified(true);
        }

        private static void UpdateLastModified(this MediaRatioTypeMediaFileEntity mediaFile)
        {
            mediaFile.MediaRatioTypeMediaEntity.MediaEntity.AddToTransaction(mediaFile);
            mediaFile.MediaRatioTypeMediaEntity.MediaEntity.UpdateLastModified(true);
        }

        private static void UpdateLastModified(this BusinesshoursEntity businesshours)
        {
            if (businesshours.IsDirty)
                if (businesshours.PointOfInterestId.HasValue)
                    businesshours.PointOfInterestEntity.UpdatePointOfInterestLastModified(businesshours);
        }
        
        private static void UpdateLastModified(this PageEntity page)
        {
            if (page.IsDirty)  
                page.SiteEntity.UpdateSiteLastModified(page);
        }                

        private static void UpdateLastModified(this PageElementEntity pageElement)
        {
            if (pageElement.IsDirty)
                pageElement.PageEntity.SiteEntity.UpdateSiteLastModified(pageElement);
        }

        private static void UpdateLastModified(this AttachmentEntity attachment)
        {
            if (attachment.IsDirty)
            {
                if (attachment.PageId.HasValue)
                    attachment.PageEntity.SiteEntity.UpdateSiteLastModified(attachment);            
                else if (attachment.PageTemplateId.HasValue)
                    attachment.PageTemplateEntity.SiteTemplateEntity.UpdateLastModified(attachment);
            }
        }

        private static void UpdatePointOfInterestLastModified(this PointOfInterestEntity poi, IEntity relatedEntity)
        {
            poi.AddToTransaction(relatedEntity);
            poi.LastModifiedUTC = DateTime.UtcNow;
            poi.Save();
        }        

        private static void UpdateSiteLastModified(this SiteEntity site, IEntity relatedEntity)
        {
            if (site != null)
            {
                site.AddToTransaction(relatedEntity);
                site.LastModifiedUTC = DateTime.UtcNow;
                site.Save();
            }
        }        

        public static void UpdateLastModified(this PageTemplateEntity pageTemplate)
        {
            if (pageTemplate.IsDirty)
                pageTemplate.SiteTemplateEntity.UpdateLastModified(pageTemplate);
        }        

        public static void UpdateLastModified(this PageTemplateElementEntity pageTemplateElement)
        {
            if (pageTemplateElement.IsDirty)
                pageTemplateElement.PageTemplateEntity.SiteTemplateEntity.UpdateLastModified(pageTemplateElement);
        }

        public static void UpdateLastModified(this SiteTemplateEntity siteTemplate, IEntity relatedEntity)
        {
            if (siteTemplate != null && siteTemplate.SiteCollection.Count > 0)
            {
                foreach (SiteEntity site in siteTemplate.SiteCollection)
                    site.UpdateSiteLastModified(relatedEntity);
            }
        }        
    }
}
