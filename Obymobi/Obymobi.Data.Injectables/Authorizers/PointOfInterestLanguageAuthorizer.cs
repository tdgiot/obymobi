﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(PointOfInterestLanguageEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class PointOfInterestLanguageAuthorizer : GeneralAuthorizer
    {
        protected override Role RoleAllowedToLoadEntity
        {
            get { return Role.Administrator; }
        }

        protected override Role RoleAllowedToSaveNewEntity
        {
            get { return Role.Administrator; }
        }

        protected override Role RoleAllowedToSaveExistingEntity
        {
            get { return Role.Administrator; }
        }

        protected override Role RoleAllowedToDeleteEntity
        {
            get { return Role.Administrator; }
        }
    }
}
