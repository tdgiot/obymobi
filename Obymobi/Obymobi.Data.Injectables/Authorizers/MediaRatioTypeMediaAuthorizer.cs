﻿using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(MediaRatioTypeMediaEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class MediaRatioTypeMediaAuthorizer : GeneralAuthorizer
    {
        protected override bool DoesEntityBelongToUser(GeneralAuthorizer.AccessType accessType, IEntity useableEntity, CompanyCollection companyCollectionForUser)
        {
            MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity = useableEntity as MediaRatioTypeMediaEntity;

            IEntity relatedEntity = mediaRatioTypeMediaEntity.MediaEntity.RelatedEntity;
            if (relatedEntity != null && !base.DoesEntityBelongToUser(accessType, relatedEntity, companyCollectionForUser))
                return false;

            return base.DoesEntityBelongToUser(accessType, useableEntity, companyCollectionForUser);
        }
    }
}
