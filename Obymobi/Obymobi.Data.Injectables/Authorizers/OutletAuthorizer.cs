﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(OutletEntity), nameof(EntityBase.AuthorizerToUse), ContextType = DependencyInjectionContextType.Singleton)]
    public class OutletAuthorizer : GeneralAuthorizer
    {
        protected override Role RoleAllowedToLoadEntity => Role.Supervisor;

        protected override Role RoleAllowedToSaveNewEntity => Role.Supervisor;

        protected override Role RoleAllowedToSaveExistingEntity => Role.Supervisor;

        protected override Role RoleAllowedToDeleteEntity => Role.Supervisor;
    }
}
