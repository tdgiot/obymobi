﻿using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(MediaRatioTypeMediaFileEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class MediaRatioTypeMediaFileAuthorizer : GeneralAuthorizer
    {
        protected override bool DoesEntityBelongToUser(GeneralAuthorizer.AccessType accessType, IEntity useableEntity, CompanyCollection companyCollectionForUser)
        {
            MediaRatioTypeMediaFileEntity mediaRatioTypeMediaFileEntity = useableEntity as MediaRatioTypeMediaFileEntity;

            IEntity relatedEntity = mediaRatioTypeMediaFileEntity?.MediaRatioTypeMediaEntity?.MediaEntity?.RelatedEntity;
            if (relatedEntity != null && !base.DoesEntityBelongToUser(accessType, relatedEntity, companyCollectionForUser))
                return false;

            return base.DoesEntityBelongToUser(accessType, useableEntity, companyCollectionForUser);
        }
    }
}
