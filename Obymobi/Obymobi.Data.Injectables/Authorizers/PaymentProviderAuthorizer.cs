﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(PaymentProviderEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class PaymentProviderAuthorizer : GeneralAuthorizer
    {
        protected override Role RoleAllowedToLoadEntity => Role.Crave;

        protected override Role RoleAllowedToSaveNewEntity => Role.GodMode;

        protected override Role RoleAllowedToSaveExistingEntity => Role.GodMode;

        protected override Role RoleAllowedToDeleteEntity => Role.GodMode;
    }
}
