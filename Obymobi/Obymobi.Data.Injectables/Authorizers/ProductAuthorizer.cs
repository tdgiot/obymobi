﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(ProductEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class ProductAuthorizer : GeneralAuthorizer
    {
        protected override Role RoleAllowedToLoadEntity => Role.Customer;

        protected override Role RoleAllowedToSaveNewEntity => Role.Customer;

        protected override Role RoleAllowedToSaveExistingEntity => Role.Customer;

        protected override Role RoleAllowedToDeleteEntity => Role.Customer;
    }
}
