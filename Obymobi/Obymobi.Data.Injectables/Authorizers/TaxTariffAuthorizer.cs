﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(TaxTariffEntity), nameof(EntityBase.AuthorizerToUse), ContextType = DependencyInjectionContextType.Singleton)]
    public class TaxTariffAuthorizer : GeneralAuthorizer
    {
        protected override Role RoleAllowedToLoadEntity => Role.Customer;

        protected override Role RoleAllowedToSaveNewEntity => Role.Customer;

        protected override Role RoleAllowedToSaveExistingEntity => Role.Customer;

        protected override Role RoleAllowedToDeleteEntity => Role.Supervisor;
    }}
