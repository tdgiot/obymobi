﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(EntertainmenturlEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class EntertainmenturlAuthorizer : GeneralAuthorizer
    {
        protected override Role RoleAllowedToLoadEntity
        {
            get { return Role.Customer; }
        }

        protected override Role RoleAllowedToSaveNewEntity
        {
            get { return Role.Reseller; }
        }

        protected override Role RoleAllowedToSaveExistingEntity
        {
            get { return Role.Reseller; }
        }

        protected override Role RoleAllowedToDeleteEntity
        {
            get { return Role.Reseller; }
        }
    }
}