﻿using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(UIThemeEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class UIThemeAuthorizer : GeneralAuthorizer
    {
        protected override Role RoleAllowedToLoadEntity
        {
            get { return Role.Customer; }
        }

        protected override Role RoleAllowedToSaveNewEntity
        {
            get { return Role.Supervisor; }
        }

        protected override Role RoleAllowedToSaveExistingEntity
        {
            get { return Role.Supervisor; }
        }

        protected override Role RoleAllowedToDeleteEntity
        {
            get { return Role.Supervisor; }
        }
    }
}
