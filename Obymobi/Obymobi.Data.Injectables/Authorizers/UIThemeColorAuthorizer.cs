﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(UIThemeColorEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class UIThemeColorAuthorizer : GeneralAuthorizer
    {
        protected override Role RoleAllowedToLoadEntity
        {
            get { return Role.Customer; }
        }

        protected override Role RoleAllowedToSaveNewEntity
        {
            get { return Role.Supervisor; }
        }

        protected override Role RoleAllowedToSaveExistingEntity
        {
            get { return Role.Supervisor; }
        }

        protected override Role RoleAllowedToDeleteEntity
        {
            get { return Role.Supervisor; }
        }
    }
}