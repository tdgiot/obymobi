﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Linq;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(CustomerEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class CustomerAuthorizer : GeneralAuthorizer
    {
        protected override bool DoesEntityBelongToUser(GeneralAuthorizer.AccessType accessType, IEntity useableEntity, CompanyCollection companyCollectionForUser)
        {
            CustomerEntity customerEntity = useableEntity as CustomerEntity;

            if (customerEntity.ClientId.HasValue && companyCollectionForUser.All(c => c.CompanyId != customerEntity.ClientEntity.CompanyId))
                return this.ThrowAuthorizationException(accessType, useableEntity);

            return base.DoesEntityBelongToUser(accessType, useableEntity, companyCollectionForUser);
        }
    }
}
