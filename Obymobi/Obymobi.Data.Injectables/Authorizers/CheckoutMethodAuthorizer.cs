﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(CheckoutMethodEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class CheckoutMethodAuthorizer : GeneralAuthorizer
    {
        protected override Role RoleAllowedToLoadEntity => Role.Supervisor;

        protected override Role RoleAllowedToSaveNewEntity => Role.Supervisor;

        protected override Role RoleAllowedToSaveExistingEntity => Role.Crave;

        protected override Role RoleAllowedToDeleteEntity => Role.Crave;
    }
}
