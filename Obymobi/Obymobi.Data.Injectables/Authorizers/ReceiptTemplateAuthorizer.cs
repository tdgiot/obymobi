﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(ReceiptTemplateEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class ReceiptTemplateAuthorizer : GeneralAuthorizer
    {
        protected override Role RoleAllowedToLoadEntity => Role.Crave;

        protected override Role RoleAllowedToSaveNewEntity => Role.Crave;

        protected override Role RoleAllowedToSaveExistingEntity => Role.Crave;

        protected override Role RoleAllowedToDeleteEntity => Role.Crave;
    }
}
