﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(MediaEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class MediaAuthorizer : GeneralAuthorizer
    {
        protected override bool DoesEntityBelongToUser(GeneralAuthorizer.AccessType accessType, IEntity useableEntity, CompanyCollection companyCollectionForUser)
        {
            MediaEntity mediaEntity = useableEntity as MediaEntity;

            IEntity relatedEntity = mediaEntity.RelatedEntity;
            if (relatedEntity != null && !base.DoesEntityBelongToUser(accessType, relatedEntity, companyCollectionForUser))
                return false;

            return base.DoesEntityBelongToUser(accessType, useableEntity, companyCollectionForUser);
        }
    }
}
