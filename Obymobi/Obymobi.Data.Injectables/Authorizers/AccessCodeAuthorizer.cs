﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(AccessCodeEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class AccessCodeAuthorizer : GeneralAuthorizer
    {
        protected override Role RoleAllowedToLoadEntity
        { 
            get { return Role.Customer; }
        }

        protected override Role RoleAllowedToSaveNewEntity
        {
            get { return Role.Administrator; }
        }

        protected override Role RoleAllowedToSaveExistingEntity
        {
            get { return Role.Administrator; }
        }

        protected override Role RoleAllowedToDeleteEntity
        {
            get { return Role.Administrator; }
        }
    }
}
