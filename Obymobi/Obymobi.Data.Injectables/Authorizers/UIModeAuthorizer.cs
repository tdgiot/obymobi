﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(UIModeEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class UIModeAuthorizer : GeneralAuthorizer
    {
        protected override Role RoleAllowedToLoadEntity
        {
            get { return Role.Customer; }
        }

        protected override Role RoleAllowedToSaveNewEntity
        {
            get { return Role.Reseller; }
        }

        protected override Role RoleAllowedToSaveExistingEntity
        {
            get { return Role.Reseller; }
        }

        protected override Role RoleAllowedToDeleteEntity
        {
            get { return Role.Reseller; }
        }
    }
}
