﻿using System;
using System.Linq;
using System.Threading;
using Dionysos;
using Obymobi.Constants;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Authorizers
{
    [Serializable]
    [DependencyInjectionInfo(typeof(IEntity), "AuthorizerToUse", ContextType = DependencyInjectionContextType.Singleton)]
    public class GeneralAuthorizer : AuthorizerBase
    {
        #region Fields

        public const string ERROR_TEXT_LOAD = "You are not allowed to load the requested data.";
        public const string ERROR_TEXT_SAVE_NEW = "You are not allowed to save new data.";
        public const string ERROR_TEXT_SAVE_EXISTING = "You are not allowed to modify existing data.";
        public const string ERROR_TEXT_DELETE = "You are not allowed to delete existing data.";

        public readonly Role LOWEST_ROLE = Enum.GetValues(typeof(Role)).Cast<Role>().Min();

        #endregion

        #region Enums

        public enum AccessType
        {
            Load,
            SaveNew,
            SaveExisting,
            Delete
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the role for the current user
        /// </summary>
        public static Role CurrentRole
        {
            get
            {
                Role role;

                Object godModeOverride = Thread.GetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable));
                Object threadRole = Thread.GetData(Thread.GetNamedDataSlot("Role"));
                if (godModeOverride != null && (bool)godModeOverride)
                {
                    role = Role.GodMode;
                }
                else if (threadRole != null)
                {
                    role = (Role)threadRole;
                }
                else
                {
                    role = CmsSessionHelper.CurrentRole;
                }

                return role;
            }
        }

        /// <summary>
        /// Gets the role which is allowed to load an entity
        /// </summary>
        protected virtual Role RoleAllowedToLoadEntity => LOWEST_ROLE;

        /// <summary>
        /// Gets the role which is allowed to save a new entity
        /// </summary>
        protected virtual Role RoleAllowedToSaveNewEntity => LOWEST_ROLE;

        /// <summary>
        /// Gets the role which is allowed to save an existing entity
        /// </summary>
        protected virtual Role RoleAllowedToSaveExistingEntity => LOWEST_ROLE;

        /// <summary>
        /// Gets the role which is allowed to delete an entity
        /// </summary>
        protected virtual Role RoleAllowedToDeleteEntity => LOWEST_ROLE;

        #endregion

        #region Methods

        /// <summary>
        /// Determines whether the caller is allowed to load the specified entity
        /// </summary>
        /// <param name="entity">The entity to load</param>
        /// <returns>True if the entity can be loaded, False if not</returns>
        public override bool CanLoadEntity(IEntityCore entity)
        {
            IEntity useableEntity = entity as IEntity;

            if (GeneralAuthorizer.CheckIfEntityIsPersisted(useableEntity))
            {
                return this.CheckAuthorizationForEntity(entity, AccessType.Load);
            }

            return base.CanLoadEntity(entity);
        }

        /// <summary>
        /// Determines whether the caller is allowed to save the new instance passed in
        /// and sets the value of the "Created" and/or "CreatedUTC" field
        /// </summary>
        /// <param name="entity">The entity to save</param>
        /// <returns>True if the entity can be saved, False if not</returns>
        public override bool CanSaveNewEntity(IEntityCore entity)
        {
            IEntity useableEntity = entity as IEntity;

            this.CheckAuthorizationForEntity(entity, AccessType.SaveNew);

            if (useableEntity != null)
            {
                if (useableEntity.Fields["Created"] != null)
                {
                    useableEntity.SetNewFieldValue("Created", DateTime.Now);
                }

                if (useableEntity.Fields["CreatedUTC"] != null)
                {
                    useableEntity.SetNewFieldValue("CreatedUTC", DateTime.UtcNow);
                }
            }
            return base.CanSaveNewEntity(entity);
        }

        /// <summary>
        /// Determines whether the caller is allowed to save the modified existing instance passed in
        /// and sets the value of the "Updated" and/or "UpdatedUTC" field
        /// </summary>
        /// <param name="entity">The entity to save</param>
        /// <returns>True if the entity can be saved, False if not</returns>
        public override bool CanSaveExistingEntity(IEntityCore entity)
        {
            IEntity useableEntity = entity as IEntity;

            if (GeneralAuthorizer.CheckIfEntityIsPersisted(useableEntity))
            {
                this.CheckAuthorizationForEntity(entity, AccessType.SaveExisting);
            }

            if (useableEntity != null)
            {
                if (useableEntity.Fields["Updated"] != null)
                {
                    useableEntity.SetNewFieldValue("Updated", DateTime.Now);
                }

                if (useableEntity.Fields["UpdatedUTC"] != null)
                {
                    useableEntity.SetNewFieldValue("UpdatedUTC", DateTime.UtcNow);
                }
            }

            return base.CanSaveExistingEntity(entity);
        }

        /// <summary>
        /// Determines whether the caller is allowed to delete of the entity type passed in
        /// </summary>
        /// <param name="entity">The entity to delete</param>
        /// <returns>True if the entity can be deleted, False if not</returns>
        public override bool CanDeleteEntity(IEntityCore entity)
        {
            IEntity useableEntity = entity as IEntity;

            if (GeneralAuthorizer.CheckIfEntityIsPersisted(useableEntity))
            {
                this.CheckAuthorizationForEntity(entity, AccessType.Delete);
            }

            return base.CanDeleteEntity(entity);
        }

        /// <summary>
        /// Checks whether the current user is allowed to access the specified entity
        /// and then checks whether the entity is connected to the companies for the current user
        /// </summary>
        /// <param name="entity">The entity to check</param>
        /// <param name="accessType">The type of access for the call (Load, SaveNew, SaveExisting or Delete)</param>
        public bool CheckAuthorizationForEntity(IEntityCore entity, AccessType accessType)
        {
            // Prevent stackoverflow by recursion:
            if (!GeneralAuthorizerState.Instance.IsCheckingAccess)
            {
                try
                {
                    GeneralAuthorizerState.Instance.IsCheckingAccess = true;

                    // Only check the access within application "ObymobiCms"
                    if (Global.ApplicationInfo.ApplicationName == "ObymobiCms" || Global.ApplicationInfo.ApplicationName == "Crave.Management")
                    {
                        IEntity useableEntity = entity as IEntity;

                        // Get the current role
                        Role currentRole = CurrentRole;

                        if (currentRole < Role.Administrator)
                        {
                            // 1. Check whether the entity can be accessed in general
                            switch (accessType)
                            {
                                case AccessType.Load:
                                    if (currentRole < RoleAllowedToLoadEntity)
                                    {
                                        ThrowAuthorizationException(accessType, useableEntity);
                                    }
                                    if (!Exists(useableEntity))
                                    {
                                        ThrowEntityNotFoundException(useableEntity);
                                    }
                                    break;
                                case AccessType.SaveNew:
                                    if (currentRole < RoleAllowedToSaveNewEntity)
                                    {
                                        ThrowAuthorizationException(accessType, useableEntity);
                                    }
                                    break;
                                case AccessType.SaveExisting:
                                    if (currentRole < RoleAllowedToSaveExistingEntity)
                                    {
                                        ThrowAuthorizationException(accessType, useableEntity);
                                    }
                                    break;
                                case AccessType.Delete:
                                    if (currentRole < RoleAllowedToDeleteEntity)
                                    {
                                        ThrowAuthorizationException(accessType, useableEntity);
                                    }
                                    break;
                            }

                            // 2. Check whether the entity can be accessed by the current user
                            return this.DoesEntityBelongToUser(accessType, useableEntity, CmsSessionHelper.CompanyCollectionForUser);
                        }
                    }
                }
                finally
                {
                    GeneralAuthorizerState.Instance.IsCheckingAccess = false;
                }
            }

            return true;
        }

        private static bool Exists(IEntity useableEntity) => useableEntity.Fields.State == EntityState.Fetched;

        /// <summary>
        /// Throws an authorization exception for the specified access type and entity
        /// </summary>
        /// <param name="accessType">The type of access for the call (Load, SaveNew, SaveExisting or Delete)</param>
        /// <param name="useableEntity">The entity to throw the exception for</param>
        protected bool ThrowAuthorizationException(AccessType accessType, IEntity useableEntity)
        {
            if (useableEntity is ISkipAuthorization)
            {
                return false;
            }

            throw new AuthorizationException(GeneralAuthorizer.GetErrorText(accessType) + " (Entity: {0}. ID: {1})", useableEntity.LLBLGenProEntityName, useableEntity.PrimaryKeyFields[0].CurrentValue);
        }

        /// <summary>
        /// Throws an entity not found exception for the specified entity
        /// </summary>
        /// <param name="useableEntity">The entity to throw the exception for</param>
        protected void ThrowEntityNotFoundException(IEntity useableEntity) => throw new EntityNotFoundException($"Entity not found. (Entity: {useableEntity.LLBLGenProEntityName}. ID: {useableEntity.PrimaryKeyFields[0].CurrentValue})");

        private static bool TryGetBrandIdFromInterface(IEntity useableEntity, out int? brandId, out bool throwAuthorizationException)
        {
            // It's possible entites have multiple entities 
            bool returnValue = false;

            brandId = null;
            throwAuthorizationException = true;

            if (useableEntity is IBrandRelatedEntity brandRelatedEntity)
            {
                brandId = brandRelatedEntity.BrandId;
                returnValue = true;
            }

            if (useableEntity is INullableBrandRelatedEntity nullableBrandRelatedEntity)
            {
                brandId = nullableBrandRelatedEntity.BrandId;
                returnValue = true;
            }

            if (useableEntity is INullableBrandRelatedChildEntity nullableBrandRelatedChildEntity)
            {
                brandId = !brandId.HasValue ? nullableBrandRelatedChildEntity.ParentBrandId : null;
                throwAuthorizationException = false;
                returnValue = true;
            }

            return returnValue;
        }

        /// <summary>
        /// Checks whether the specified entity belongs to one of the companies for the current user
        /// </summary>
        /// <param name="accessType">The type of access for the call (Load, SaveNew, SaveExisting or Delete)</param>
        /// <param name="useableEntity">The entity to check</param>
        /// <param name="companyCollectionForUser">The collection of companies for the current user</param>
        protected virtual bool DoesEntityBelongToUser(AccessType accessType, IEntity useableEntity, CompanyCollection companyCollectionForUser)
        {
            if (useableEntity is IBrandRelatedEntity brandRelatedEntity)
            {
                if (!CmsSessionHelper.GetUserRoleForBrand(brandRelatedEntity.BrandId).HasValue)
                {
                    return this.ThrowAuthorizationException(accessType, useableEntity);
                }

                return true;
            }

            if (useableEntity is INullableBrandRelatedEntity nullableBrandRelatedEntity && nullableBrandRelatedEntity.BrandId.HasValue)
            {
                if (!CmsSessionHelper.GetUserRoleForBrand(nullableBrandRelatedEntity.BrandId.Value).HasValue)
                {
                    return this.ThrowAuthorizationException(accessType, useableEntity);
                }

                return true;
            }

            if (useableEntity is INullableBrandRelatedChildEntity nullableBrandRelatedChildEntity && nullableBrandRelatedChildEntity.ParentBrandId.HasValue)
            {
                if (CmsSessionHelper.GetUserRoleForBrand(nullableBrandRelatedChildEntity.ParentBrandId.Value).HasValue)
                {
                    return true;
                }
            }

            // Company related i.e. ProductEntity
            if (useableEntity is ICompanyRelatedEntity companyRelatedEntity)
            {
                if (!this.IsCompanyIdValidForCompanyCollection(companyRelatedEntity.CompanyId, companyCollectionForUser))
                {
                    return this.ThrowAuthorizationException(accessType, useableEntity);
                }
            }
            // Company related child i.e. ProductCategory
            else if (useableEntity is ICompanyRelatedChildEntity companyRelatedChildEntity)
            {
                if (companyRelatedChildEntity.ParentCompanyId == -1)
                {
                    throw new AuthorizationException("ParentCompanyId hasn't been set yet for entity '{0}' with ID '{1}'!", useableEntity.LLBLGenProEntityName, useableEntity.PrimaryKeyFields[0].CurrentValue);
                }
                if (companyRelatedChildEntity.ParentCompanyId == 0)
                {
                    throw new AuthorizationException("ParentCompanyId is 0 (zero) for entity '{0}' with ID '{1}'! This is probably because you're using an IncludeFieldsList and forgot to add the ParentCompanyId field!", useableEntity.LLBLGenProEntityName, useableEntity.PrimaryKeyFields[0].CurrentValue);
                }
                if (!this.IsCompanyIdValidForCompanyCollection(companyRelatedChildEntity.ParentCompanyId, companyCollectionForUser))
                {
                    return this.ThrowAuthorizationException(accessType, useableEntity);
                }
            }
            // Nullable company related, so can be company related or generic i.e. Entertainment
            else if (useableEntity is INullableCompanyRelatedEntity nullableCompanyRelatedEntity)
            {
                int? companyId = nullableCompanyRelatedEntity.CompanyId;
                if (useableEntity is BusinesshoursEntity businesshoursEntity && businesshoursEntity.OutletId.HasValue)
                {
                    companyId = businesshoursEntity.OutletEntity.CompanyId;
                }

                if (!companyId.HasValue)
                {
                    // Generic entities can only be changed by the administrator role or above.
                    if (CurrentRole < Role.Administrator && accessType != AccessType.Load)
                    {
                        return this.ThrowAuthorizationException(accessType, useableEntity);
                    }
                }
                else if (!this.IsCompanyIdValidForCompanyCollection(companyId.Value, companyCollectionForUser))
                {
                    return this.ThrowAuthorizationException(accessType, useableEntity);
                }
            }
            // Nullable company related child, so can be company related or generic i.e. Entertainmenturl
            else if (useableEntity is INullableCompanyRelatedChildEntity nullableCompanyRelatedChildEntity)
            {
                if (!nullableCompanyRelatedChildEntity.ParentCompanyId.HasValue) // Generic entity like a generic entertainment for example
                {
                    // Dirty haxor to find the company ID of page elements and pages because they're messed up and soon obsolete anyways
                    int parentCompanyId = 0;
                    if (nullableCompanyRelatedChildEntity is PageElementEntity && (nullableCompanyRelatedChildEntity as PageElementEntity).Parent != null)
                    {
                        PageEntity pageEntity = ((nullableCompanyRelatedChildEntity as PageElementEntity).Parent as PageEntity);
                        if (pageEntity.ParentCompanyId.HasValue)
                        {
                            parentCompanyId = pageEntity.ParentCompanyId.Value;
                        }
                        else if (pageEntity.Parent != null)
                        {
                            SiteEntity siteEntity = (pageEntity.Parent as SiteEntity);
                            parentCompanyId = siteEntity.CompanyId ?? 0;
                        }
                    }
                    else if (nullableCompanyRelatedChildEntity is PageEntity && (nullableCompanyRelatedChildEntity as PageEntity).Parent != null)
                    {
                        SiteEntity siteEntity = ((nullableCompanyRelatedChildEntity as PageEntity).Parent as SiteEntity);
                        parentCompanyId = siteEntity.CompanyId ?? 0;
                    }

                    if (parentCompanyId <= 0 || !this.IsCompanyIdValidForCompanyCollection(parentCompanyId, companyCollectionForUser))
                    {
                        // Generic entities can only be loaded by non-administrators
                        if (CurrentRole < Role.Administrator && accessType != AccessType.Load)
                        {
                            return this.ThrowAuthorizationException(accessType, useableEntity);
                        }
                    }
                }
                else if (nullableCompanyRelatedChildEntity.ParentCompanyId.Value == 0)
                {
                    throw new AuthorizationException("ParentCompanyId is 0 (zero) for entity '{0}' with ID '{1}'! This is probably because you're using an IncludeFieldsList and forgot to add the ParentCompanyId field!", useableEntity.LLBLGenProEntityName, useableEntity.PrimaryKeyFields[0].CurrentValue);
                }
                else if (!this.IsCompanyIdValidForCompanyCollection(nullableCompanyRelatedChildEntity.ParentCompanyId.Value, companyCollectionForUser))
                {
                    return this.ThrowAuthorizationException(accessType, useableEntity);
                }
            }

            return true;
        }

        /// <summary>
        /// Checks whether the specified company id is valid for the specified collection of companies
        /// </summary>
        /// <param name="companyId">The company id to check</param>
        /// <param name="companyCollectionForUser">The collection of companies for the current user</param>
        /// <returns></returns>
        protected bool IsCompanyIdValidForCompanyCollection(int companyId, CompanyCollection companyCollectionForUser)
        {
            if (companyCollectionForUser.Any(c => c.CompanyId == companyId))
            {
                return true;
            }

            return CmsSessionHelper.IsCompanyIdValidForCompanyBrandCollection(companyId);
        }

        /// <summary>
        /// Checks whether the specified entity is persisted in the database
        /// </summary>
        /// <param name="useableEntity">The entity to check</param>
        /// <returns>True if the entity was persisted in the database, False if not</returns>
        protected static bool CheckIfEntityIsPersisted(IEntity useableEntity)
        {
            bool isPersistedEntity = false;

            // GK Make sure it's a saved entity: (IsNew doenst WORK!), Why? Don't know, don't care.
            if (useableEntity.PrimaryKeyFields[0].CurrentValue != null)
            {
                if (useableEntity.PrimaryKeyFields[0].CurrentValue is long && (long)useableEntity.PrimaryKeyFields[0].CurrentValue > 0)
                {
                    isPersistedEntity = true;
                }
                else if (useableEntity.PrimaryKeyFields[0].CurrentValue is int && (int)useableEntity.PrimaryKeyFields[0].CurrentValue > 0)
                {
                    isPersistedEntity = true;
                }
            }

            return isPersistedEntity;
        }

        /// <summary>
        /// Sets the GodMode override flag for the current thread
        /// </summary>
        /// <param name="set">The flag to enabled or disable the GodMode</param>
        public static void SetThreadGodMode(bool set)
        {
            if (set)
            {
                if (Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable) == null)
                {
                    Thread.AllocateNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable);
                }

                Thread.SetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable), true);
            }
            else
            {
                if (Thread.GetNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable) != null)
                {
                    Thread.FreeNamedDataSlot(ObymobiConstants.GeneralAuthorizerGodModeOverrideThreadVariable);
                }
            }
        }

        /// <summary>
        /// Returns the error text for the specified access type
        /// </summary>
        /// <param name="accessType">The type of access for the call (Load, SaveNew, SaveExisting or Delete)</param>
        /// <returns>A string containing the error text</returns>
        public static string GetErrorText(AccessType accessType)
        {
            string errorText = string.Empty;
            switch (accessType)
            {
                case AccessType.Load:
                    errorText = ERROR_TEXT_LOAD;
                    break;
                case AccessType.SaveNew:
                    errorText = ERROR_TEXT_SAVE_NEW;
                    break;
                case AccessType.SaveExisting:
                    errorText = ERROR_TEXT_SAVE_EXISTING;
                    break;
                case AccessType.Delete:
                    errorText = ERROR_TEXT_DELETE;
                    break;
            }

            return errorText;
        }

        #endregion

        #region Classes

        public class GeneralAuthorizerState
        {
            #region Fields

            private static readonly GeneralAuthorizerState instance = new GeneralAuthorizerState();

            #endregion

            #region Constructors

            /// <summary>
            /// Initializes a new instance of the GeneralValidatorState class.
            /// </summary>
            private GeneralAuthorizerState()
            { }

            #endregion

            #region Properties

            /// <summary>
            /// Gets or sets whether the instance is archiving.
            /// </summary>
            public bool IsCheckingAccess { get; set; }

            /// <summary>
            /// Gets the current instance.
            /// </summary>
            public static GeneralAuthorizerState Instance => instance;

            #endregion
        }

        #endregion
    }
}
