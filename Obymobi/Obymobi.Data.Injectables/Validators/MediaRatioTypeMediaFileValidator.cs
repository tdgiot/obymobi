﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Dionysos.Security.Cryptography;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(MediaRatioTypeMediaFileEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class MediaRatioTypeMediaFileValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            MediaRatioTypeMediaFileEntity useableEntity = involvedEntity as MediaRatioTypeMediaFileEntity;

            // GK Not sure if a new entity also gives IsChanged, but assume so. If not, you know the bug now :)
            if (!useableEntity.BypassValidation || useableEntity.ForceCdnPublication)
            {
                if (useableEntity.ForceCdnPublication || useableEntity.Fields[MediaRatioTypeMediaFileFields.FileMd5.Name].IsChanged)
                {
                    useableEntity.ValidatorFileWasChangedBeforeSave = true;
                }
            }            
            
            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            MediaRatioTypeMediaFileEntity useableEntity = involvedEntity as MediaRatioTypeMediaFileEntity;

            if (useableEntity.ValidatorFileWasChangedBeforeSave) // Skip upload for menu manager
            {
                if (useableEntity.SkipUpload)
                    useableEntity.SkipUpload = false;
                else
                {
                    // Write a task to the MediaProcessingQueue           
                    MediaHelper.QueueMediaRatioTypeMediaFileTask(useableEntity, MediaProcessingTaskEntity.ProcessingAction.Upload, useableEntity.ForceCdnPublication, useableEntity.GetCurrentTransaction());
                }
            }

            // Reset validator state
            useableEntity.ValidatorFileWasChangedBeforeSave = false;

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            MediaRatioTypeMediaFileEntity useableEntity = involvedEntity as MediaRatioTypeMediaFileEntity;

            useableEntity.AfterDelete += useableEntity_AfterDelete;          

            base.ValidateEntityBeforeDelete(involvedEntity);
        }

        void useableEntity_AfterDelete(MediaRatioTypeMediaFileEntity entity)
        {
            // Write a task to the MediaProcessingQueue
            try
            {
                // GK Media.zip is going to be gone as soon as possible.
                // Remove the file from the API media directory (if present)
                // MediaHelper.DeleteMediaRatioTypeMediaFileFromApiMediaDirectory(entity.MediaRatioTypeMediaEntity);

                if (!entity.BypassValidation)
                    MediaHelper.QueueMediaRatioTypeMediaFileTask(entity, MediaProcessingTaskEntity.ProcessingAction.Delete, true, entity.GetCurrentTransaction());
            }
            catch (Exception)
            {                
                if (entity.MediaRatioTypeMediaEntity.MediaType.HasValue && !Enum.IsDefined(typeof(MediaType), entity.MediaRatioTypeMediaEntity.MediaType.Value))
                {                    
                    // Ignore exception, it's just a media to used a MediaType that no longer exists.
                }
                else
                    throw;
            }
        }


        public bool HttpContext { get; set; }
    }
}


