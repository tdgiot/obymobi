﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Crave.Domain.UseCases;
using Crave.Domain.UseCases.Requests;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AttachmentEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AttachmentValidator : GeneralValidator
    {
        private readonly SaveAttachmentForBrandProductUseCase saveAttachmentForBrandProductUseCase;
        private readonly DeleteAttachmentForBrandProductUseCase deleteAttachmentForBrandProductUseCase;

        public AttachmentValidator()
        {
            this.saveAttachmentForBrandProductUseCase = new SaveAttachmentForBrandProductUseCase();
            this.deleteAttachmentForBrandProductUseCase = new DeleteAttachmentForBrandProductUseCase();
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            AttachmentEntity useableEntity = involvedEntity as AttachmentEntity;

            if (useableEntity.ProductId.HasValue && useableEntity.ProductEntity.BrandId.HasValue)
            {
                this.saveAttachmentForBrandProductUseCase.Execute(new AttachmentForBrandProductRequest
                {
                    BrandProductId = useableEntity.ProductId.Value,
                    Attachment = useableEntity
                });
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            AttachmentEntity useableEntity = involvedEntity as AttachmentEntity;

            if (useableEntity.ProductId.HasValue && useableEntity.ProductEntity.BrandId.HasValue)
            {
                this.deleteAttachmentForBrandProductUseCase.Execute(new AttachmentForBrandProductRequest
                {
                    BrandProductId = useableEntity.ProductId.Value,
                    Attachment = useableEntity
                });
            }

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

