﻿using System;
using System.Linq;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Routing;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(RoutestepEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class RoutestepValidator : GeneralValidator
    {
        public enum RoutestepValidatorResult : int
        {
            FirstStepMustBeStepOne = 200,
            StepNumberNotUnique = 201,
            NotAClosedSequenceOfStepNumbers = 202,
            TransactionRequiredForDeleteActions = 203
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            RoutestepEntity useableEntity = involvedEntity as RoutestepEntity;

            // Verify stepnumber consistency
            RoutestepCollection routesteps = new RoutestepCollection();
            routesteps.AddToTransaction(useableEntity);

            if (!useableEntity.ValidatorOverrideSequenceChecking)
            {
                ValidateStepNumberConsistency(useableEntity, routesteps);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private static void ValidateStepNumberConsistency(RoutestepEntity useableEntity, RoutestepCollection routesteps)
        {
            PredicateExpression filterUniqueStepNumber = new PredicateExpression();
            filterUniqueStepNumber.Add(RoutestepFields.RouteId == useableEntity.RouteId);
            SortExpression sort = new SortExpression(RoutestepFields.Number | SortOperator.Ascending);

            // Get all steps
            routesteps.GetMulti(filterUniqueStepNumber, 0, sort);

            // Make sure the stepnumber is unique
            if (routesteps.Any(rs => rs.Number == useableEntity.Number && rs.RoutestepId != useableEntity.RoutestepId))
            {
                throw new ObymobiEntityException(RoutestepValidatorResult.StepNumberNotUnique, useableEntity);
            }

            // Check that all steps are a closed sequence
            int stepNumber = RoutingHelper.FIRST_STEP_NUMBER;
            foreach (RoutestepEntity step in routesteps)
            {
                if (step.Number != stepNumber)
                {
                    // It must be a closed sequence, so if this fails it's wrong!
                    throw new ObymobiEntityException(RoutestepValidatorResult.NotAClosedSequenceOfStepNumbers, useableEntity);
                }

                stepNumber++;
            }

            // If this step is new, make sure is has the stepNumber which is following the sequence just checked
            if (useableEntity.IsNew && useableEntity.Number != stepNumber)
            {
                throw new ObymobiEntityException(RoutestepValidatorResult.NotAClosedSequenceOfStepNumbers, useableEntity);
            }
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            RoutestepEntity useableEntity = involvedEntity as RoutestepEntity;

            // Must be atomic - GK-ROUTING: Not yet forced, how to handle cascade deletions which don't belong to a transaction??
            // if (useableEntity.GetCurrentTransaction() == null)
            //    throw new ObymobiException(RoutestepValidatorResult.TransactionRequiredForDeleteActions, useableEntity);

            // Renumber other Steps
            PredicateExpression filterUniqueStepNumber = new PredicateExpression();
            filterUniqueStepNumber.Add(RoutestepFields.RouteId == useableEntity.RouteId);
            SortExpression sort = new SortExpression(RoutestepFields.Number | SortOperator.Ascending);

            // Get all steps
            RoutestepCollection routesteps = new RoutestepCollection();
            routesteps.AddToTransaction(useableEntity);
            routesteps.GetMulti(filterUniqueStepNumber, 0, sort);

            if (routesteps.Count == 1)
            {
                throw new EntityDeleteException("Can not delete the last routestep of the route");
            }

            RoutestephandlerCollection routestephandlers = new RoutestephandlerCollection();
            RelationCollection r = new RelationCollection(RoutestephandlerEntityBase.Relations.RoutestepEntityUsingRoutestepId);

            PredicateExpression f = new PredicateExpression();
            f.Add(RoutestepFields.RouteId == useableEntity.RouteId);
            f.AddWithAnd(RoutestepFields.RoutestepId != useableEntity.RoutestepId);

            int handlerCount = routestephandlers.GetDbCount(f, r);

            if (handlerCount == 0)
            {
                throw new EntityDeleteException("Can not delete routestep as there are no other routesteps with handlers");
            }

            // Check that all steps are a closed sequence
            int stepNumber = RoutingHelper.FIRST_STEP_NUMBER;
            foreach (RoutestepEntity step in routesteps)
            {
                if (step.RoutestepId == useableEntity.RoutestepId)
                {
                    // Skip, this one is going to be deleted                    
                }
                else
                {
                    step.Number = stepNumber;
                    step.AddToTransaction(useableEntity);
                    step.ValidatorOverrideSequenceChecking = true;
                    step.Save();
                    stepNumber++;
                }
            }

            // Remove all handlers linked to this routestep
            RoutestephandlerCollection handlerCollection = new RoutestephandlerCollection();
            handlerCollection.DeleteMulti(RoutestephandlerFields.RoutestepId == useableEntity.RoutestepId);

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

