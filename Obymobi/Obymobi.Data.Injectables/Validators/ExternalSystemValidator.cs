﻿using System;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ExternalSystemEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ExternalSystemValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            if (involvedEntity is ExternalSystemEntity useableEntity)
            {
                if (useableEntity.IsNew)
                {
                    SetDefaultValues(useableEntity);
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private static void SetDefaultValues(ExternalSystemEntity externalSystem)
        {
            switch (externalSystem.Type)
            {
                case ExternalSystemType.Deliverect:
                    externalSystem.StringValue4 = Guid.NewGuid().ToString();
                    break;
            }
        }
    }
}

