﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ClientLogEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ClientLogValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            ClientLogEntity useableEntity = involvedEntity as ClientLogEntity;

            useableEntity.TypeText = useableEntity.TypeName;
            useableEntity.FromStatusText = useableEntity.FromStatusName;
            useableEntity.ToStatusText = useableEntity.ToStatusName;
            useableEntity.FromOperationModeText = useableEntity.FromOperationModeName;
            useableEntity.ToOperationModeText = useableEntity.ToOperationModeName;

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

