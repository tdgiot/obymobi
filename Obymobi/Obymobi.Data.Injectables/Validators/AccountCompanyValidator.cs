﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AccountCompanyEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AccountCompanyValidator : GeneralValidator
    {
        public enum AccountCompanyValidatorResult
        {
            AccountCompanyAlreadyExists = 200
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AccountCompanyEntity useableEntity = involvedEntity as AccountCompanyEntity;

            // Check whether the account company record already exists
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AccountCompanyFields.AccountId == useableEntity.AccountId);
            filter.Add(AccountCompanyFields.CompanyId == useableEntity.CompanyId);
            if (!useableEntity.IsNew)
                filter.Add(AccountCompanyFields.AccountCompanyId != useableEntity.AccountCompanyId);

            AccountCompanyCollection accountCompanyCollection = new AccountCompanyCollection();
            accountCompanyCollection.AddToTransaction(useableEntity);
            if (accountCompanyCollection.GetDbCount(filter) > 0)
                throw new ObymobiException(AccountCompanyValidatorResult.AccountCompanyAlreadyExists);

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            AccountCompanyEntity useableEntity = involvedEntity as AccountCompanyEntity;

            CmsSessionHelper.ClearCompaniesFromCacheForAccount(useableEntity.AccountEntity);
           
            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            AccountCompanyEntity useableEntity = involvedEntity as AccountCompanyEntity;

            CmsSessionHelper.ClearCompaniesFromCacheForAccount(useableEntity.AccountEntity);

            base.ValidateEntityBeforeDelete(involvedEntity);
        }

        
    }
}

