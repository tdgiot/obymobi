﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(BusinesshoursEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class BusinesshoursValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            BusinesshoursEntity useableEntity = involvedEntity as BusinesshoursEntity;

            if (!useableEntity.CompanyId.HasValue && 
                !useableEntity.PointOfInterestId.HasValue &&
                !useableEntity.OutletId.HasValue)
            {
                throw new ObymobiException(BusinesshoursSaveResult.CompanyIdZero, $"No {nameof(BusinesshoursEntity.CompanyId)} or {nameof(BusinesshoursEntity.PointOfInterestId)} or {nameof(BusinesshoursEntity.OutletId)} set");
            }
                

            // Validate DayOfWeekAndTime
            if (useableEntity.DayOfWeekAndTime.Length != 5)
            {
                throw new ObymobiException(BusinesshoursSaveResult.DayOfWeekAndTimeInvalid, "CompanyId: {0}, input: {1}", useableEntity.CompanyId, useableEntity.DayOfWeekAndTime);
            }
            else
            {
                try
                {
                    int dayOfWeek = Convert.ToInt32(useableEntity.DayOfWeekAndTime.Substring(0, 1));
                    int hours = Convert.ToInt32(useableEntity.DayOfWeekAndTime.Substring(1, 2));
                    int minutes = Convert.ToInt32(useableEntity.DayOfWeekAndTime.Substring(3, 2));

                    if (dayOfWeek < 0 || dayOfWeek > 6 || hours < 0 || hours > 23 || minutes < 0 || minutes > 59)
                        throw new ObymobiException(BusinesshoursSaveResult.DayOfWeekAndTimeInvalid, "CompanyId: {0}, input: {1}", useableEntity.CompanyId, useableEntity.DayOfWeekAndTime);
                }
                catch
                {
                    throw new ObymobiException(BusinesshoursSaveResult.DayOfWeekAndTimeInvalid, "CompanyId: {0}, input: {1}", useableEntity.CompanyId, useableEntity.DayOfWeekAndTime);
                }
            }

            // Check with previous
            SortExpression sortAsc = new SortExpression(BusinesshoursFields.DayOfWeekAndTime | SortOperator.Ascending);
            SortExpression sortDesc = new SortExpression(BusinesshoursFields.DayOfWeekAndTime | SortOperator.Descending);

            // Previous
            BusinesshoursCollection businessHourBefore = new BusinesshoursCollection();
            PredicateExpression filter = new PredicateExpression();
            if (useableEntity.CompanyId.HasValue)
            {
                filter.Add(BusinesshoursFields.CompanyId == useableEntity.CompanyId);
            }
            else if (useableEntity.PointOfInterestId.HasValue)
            {
                filter.Add(BusinesshoursFields.PointOfInterestId == useableEntity.PointOfInterestId);
            }
            else if (useableEntity.OutletId.HasValue)
            {
                filter.Add(BusinesshoursFields.OutletId == useableEntity.OutletId);
            }
                

            filter.Add(BusinesshoursFields.DayOfWeekAndTime < useableEntity.DayOfWeekAndTime);            
            //filter.Add(BusinesshoursFields.BusinesshoursId != useableEntity.BusinesshoursId);
            businessHourBefore.AddToTransaction(useableEntity);
            businessHourBefore.GetMulti(filter, 1, sortDesc);

            if (businessHourBefore.Count == 1)
            {
                if (businessHourBefore[0].Opening == useableEntity.Opening)
                    throw new ObymobiException(BusinesshoursSaveResult.SameTypeTwiceInARow, "CompanyId: {0}, existing Id: {1}, existing DayOfWeek: {2}, new: {3}", useableEntity.CompanyId, businessHourBefore[0].BusinesshoursId, businessHourBefore[0].DayOfWeekAndTime, useableEntity.DayOfWeekAndTime);
            }
            else
            {
                // Not found, check from max value (saturday 23:59)
                filter = new PredicateExpression();
                if (useableEntity.CompanyId.HasValue)
                {
                    filter.Add(BusinesshoursFields.CompanyId == useableEntity.CompanyId);
                }
                else if (useableEntity.PointOfInterestId.HasValue)
                {
                    filter.Add(BusinesshoursFields.PointOfInterestId == useableEntity.PointOfInterestId);
                }
                else if (useableEntity.OutletId.HasValue)
                {
                    filter.Add(BusinesshoursFields.OutletId == useableEntity.OutletId);
                }

                filter.Add(BusinesshoursFields.DayOfWeekAndTime < 62359);
                businessHourBefore.AddToTransaction(useableEntity);
                businessHourBefore.GetMulti(filter, 1, sortDesc);
                if (businessHourBefore.Count == 0)
                {
                    // Means 0 existing, than this has to be OPENING
                    if (!useableEntity.Opening)
                        throw new ObymobiException(BusinesshoursSaveResult.FirstMustBeOpening, "CompanyId: {0}, input: {1}", useableEntity.CompanyId, useableEntity.DayOfWeekAndTime);
                }
                else
                {
                    if (businessHourBefore[0].Opening == useableEntity.Opening)
                        throw new ObymobiException(BusinesshoursSaveResult.SameTypeTwiceInARow, "CompanyId: {0}, existing Id: {1}, existing DayOfweek: {2}, new: {3}", useableEntity.CompanyId, businessHourBefore[0].BusinesshoursId, businessHourBefore[0].DayOfWeekAndTime, useableEntity.DayOfWeekAndTime);
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

