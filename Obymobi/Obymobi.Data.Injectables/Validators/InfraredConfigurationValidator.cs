﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(InfraredConfigurationEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class InfraredConfigurationValidator : GeneralValidator
    {
        private bool wasNew = false;

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            InfraredConfigurationEntity useableEntity = involvedEntity as InfraredConfigurationEntity;
            this.wasNew = useableEntity.IsNew;            

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            InfraredConfigurationEntity useableEntity = involvedEntity as InfraredConfigurationEntity;

            if (this.wasNew)
            {
                InfraredCommandType[] types = EnumUtil.ToArray<InfraredCommandType>();
                foreach (InfraredCommandType type in types)
                {
                    InfraredCommandEntity command = new InfraredCommandEntity();
                    command.InfraredConfigurationId = useableEntity.InfraredConfigurationId;
                    command.Type = type;
                    command.Save();                    
                }
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            InfraredConfigurationEntity useableEntity = involvedEntity as InfraredConfigurationEntity;

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

