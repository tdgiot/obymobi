﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AmenityLanguageEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AmenityLanguageValidator : GeneralValidator
    {
        public enum AmenityLanguageValidatorResult
        {
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AmenityLanguageEntity useableEntity = involvedEntity as AmenityLanguageEntity;

            if (useableEntity.IsDirty)
            {
                useableEntity.AmenityEntity.LastModifiedUTC = DateTime.UtcNow;
                useableEntity.AmenityEntity.Save();
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            AmenityLanguageEntity useableEntity = involvedEntity as AmenityLanguageEntity;

            useableEntity.AmenityEntity.LastModifiedUTC = DateTime.UtcNow;
            useableEntity.AmenityEntity.Save();

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

