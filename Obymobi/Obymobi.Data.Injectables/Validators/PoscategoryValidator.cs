﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PoscategoryEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PoscategoryValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            PoscategoryEntity useableEntity = involvedEntity as PoscategoryEntity;

            // Company Required
            if (useableEntity.CompanyId <= 0)
                throw new ObymobiException(PoscategorySaveResult.CompanyIdEmpty);

            // External id required
            if (useableEntity.ExternalId.Length == 0)
                throw new ObymobiException(PoscategorySaveResult.ExternalIdEmpty);
            else if (useableEntity.IsNew)
            {
                // Check whether the external id already exists
                PredicateExpression filter = new PredicateExpression();
                filter.Add(PoscategoryFields.CompanyId == useableEntity.CompanyId);
                filter.Add(PoscategoryFields.ExternalId == useableEntity.ExternalId);

                PoscategoryCollection poscategoryCollection = new PoscategoryCollection();
                poscategoryCollection.AddToTransaction(useableEntity);                
                if (poscategoryCollection.GetDbCount(filter) > 0)
                    throw new ObymobiException(PoscategorySaveResult.ExternalIdAlreadyExists);
            }

            // Field 'Name' must have a value
            if (useableEntity.Name.Length == 0)
                throw new ObymobiException(PoscategorySaveResult.NameEmpty);

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            PoscategoryEntity useableEntity = involvedEntity as PoscategoryEntity;

            foreach (var category in useableEntity.CategoryCollection)
                category.Delete();

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

