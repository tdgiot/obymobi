﻿using System;
using System.Linq;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AlterationProductEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AlterationProductValidator : GeneralValidator
    {
        public enum AlterationProductValidatorException
        {
            AlterationProductCircularDependencyDetected
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AlterationProductEntity useableEntity = involvedEntity as AlterationProductEntity;

            // Fetch alteration with parents prefetched
            PrefetchPath prefetchPath = new PrefetchPath(EntityType.AlterationEntity);
            prefetchPath.Add(AlterationEntityBase.PrefetchPathParentAlterationEntity).SubPath
                        .Add(AlterationEntityBase.PrefetchPathParentAlterationEntity).SubPath
                        .Add(AlterationEntityBase.PrefetchPathParentAlterationEntity);
            AlterationEntity alterationEntity = new AlterationEntity(useableEntity.AlterationId, prefetchPath);

            AlterationEntity rootAlterationEntity = alterationEntity.TraverseToRootAlterationEntity;

            // Check if product is not linked to the same alteration, thus creating a circular dependency
            if (!rootAlterationEntity.IsNew && useableEntity.ProductEntity.ProductAlterationCollection.Any(x => x.AlterationId == rootAlterationEntity.AlterationId))
            {
                throw new ObymobiException(AlterationProductValidatorException.AlterationProductCircularDependencyDetected, 
                                           "Product '{0} (ID: {1})' already has root alteration '{2}' configured, thus it can not be linked back to said product", 
                                           useableEntity.ProductEntity.Name, 
                                           useableEntity.ProductEntity.ProductId,
                                           rootAlterationEntity.Name);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}