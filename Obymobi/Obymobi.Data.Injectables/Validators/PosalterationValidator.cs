﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PosalterationEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PosalterationValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            PosalterationEntity useableEntity = involvedEntity as PosalterationEntity;

            foreach (var ai in useableEntity.AlterationCollection)
                ai.Delete();

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}
