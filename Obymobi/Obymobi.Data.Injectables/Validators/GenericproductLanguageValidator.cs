﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(GenericproductLanguageEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class GenericproductLanguageValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            GenericproductLanguageEntity useableEntity = involvedEntity as GenericproductLanguageEntity;

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

