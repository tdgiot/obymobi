﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AzureNotificationHubEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AzureNotificationHubVerifier : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            var hubEntity = (AzureNotificationHubEntity)involvedEntity;

            var filter = new PredicateExpression();
            filter.Add(AzureNotificationHubFields.ConnectionString == hubEntity.ConnectionString);
            filter.AddWithAnd(AzureNotificationHubFields.HubPath == hubEntity.HubPath);

            var hubCollection = new AzureNotificationHubCollection();
            if (hubCollection.GetDbCount(filter) > 0)
            {
                throw new ObymobiException(AzureNotificationHubSaveResult.HubAlreadyExists, "A hub with this configuration already exists.");
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}
