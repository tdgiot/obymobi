﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Dionysos.Security.Cryptography;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(MediaProcessingTaskEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class MediaProcessingTaskValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            MediaProcessingTaskEntity useableEntity = involvedEntity as MediaProcessingTaskEntity;

            if(useableEntity.MediaRatioTypeMediaId.HasValue)
                useableEntity.MediaRatioTypeMediaIdNonRelationCopy = useableEntity.MediaRatioTypeMediaId.Value;
            
            base.ValidateEntityBeforeSave(involvedEntity);
        }   
    }
}


