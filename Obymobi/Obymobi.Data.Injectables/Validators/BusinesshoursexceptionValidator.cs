﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(BusinesshoursexceptionEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class BusinesshoursexceptionValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            BusinesshoursexceptionEntity useableEntity = involvedEntity as BusinesshoursexceptionEntity;

            // Validate Company ID
            if (useableEntity.CompanyId <= 0)
                throw new ObymobiException(BusinesshoursexceptionSaveResult.NoCompanyId);

            // Validate From before till
            if (useableEntity.FromDateTime > useableEntity.TillDateTime)
                throw new ObymobiException(BusinesshoursexceptionSaveResult.FromIsAfterTill);

            // Overlapping with others
            BusinesshoursexceptionCollection exceptions = new BusinesshoursexceptionCollection();
            exceptions.AddToTransaction(useableEntity);
            PredicateExpression filter = new PredicateExpression(BusinesshoursexceptionFields.CompanyId == useableEntity.CompanyId);
            filter.Add(BusinesshoursexceptionFields.FromDateTime <= useableEntity.TillDateTime);
            filter.Add(BusinesshoursexceptionFields.TillDateTime >= useableEntity.FromDateTime);
            exceptions.GetMulti(filter);

            if (exceptions.Count > 0)
                throw new ObymobiException(BusinesshoursexceptionSaveResult.Overlapping);

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

