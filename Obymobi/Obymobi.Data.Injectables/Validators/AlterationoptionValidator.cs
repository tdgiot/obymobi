﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Linq;
using Obymobi.Enums;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Crave.Domain.UseCases;
using Crave.Domain.UseCases.Requests;
using Dionysos.Data.Extensions;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AlterationoptionEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AlterationoptionValidator : GeneralValidator
    {
        private readonly UpdateAlterationoptionWithProductUseCase updateAlterationoptionWithProductUseCase;

        public AlterationoptionValidator()
        {
            this.updateAlterationoptionWithProductUseCase = new UpdateAlterationoptionWithProductUseCase();
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AlterationoptionEntity useableEntity = involvedEntity as AlterationoptionEntity;
            useableEntity.NameChanged = useableEntity.Fields[AlterationoptionFields.Name.Name].IsChanged;

            // If linked to a Posproduct, require that Posproduct to have exact 1 ProductEntity
            if (useableEntity.PosproductId.HasValue && useableEntity.PosproductEntity.ProductCollection.Count != 1)
            {
                // Retry if needed using a fetch
                ProductCollection products = new ProductCollection();
                PredicateExpression filterProducts = new PredicateExpression();
                filterProducts.Add(ProductFields.PosproductId == useableEntity.PosproductId);
                products.AddToTransaction(useableEntity);                

                if (products.GetDbCount(filterProducts) != 1)
                {
                    throw new ObymobiException(AlterationoptionSaveResult.LinkedToPosproductNotLinkedToExactlyOneProduct, "AlterationoptionId: {0}, PosproductId: {1}",
                        useableEntity.AlterationoptionId, useableEntity.PosproductId);
                }
            }

            if (useableEntity.ProductId.HasValue || useableEntity.IsChanged(AlterationoptionFields.ProductId))
            {
                this.UpdateProductLink(useableEntity);
            }

            if (useableEntity.IsChanged(AlterationoptionFields.ProductId) ||
                useableEntity.IsChanged(AlterationoptionFields.ExternalProductId))
            {
                UpdateExternalProductLink(useableEntity);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            AlterationoptionEntity useableEntity = involvedEntity as AlterationoptionEntity;

            if (useableEntity.ValidatorCreateOrUpdateDefaultCustomText && useableEntity.NameChanged)
            {
                this.UpdateDefaultCustomText(useableEntity);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        private void UpdateDefaultCustomText(AlterationoptionEntity alterationoption)
        {
            string companyCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            if (!companyCultureCode.IsNullOrWhiteSpace())
            {
                CustomTextEntity customText = alterationoption.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCultureCode && x.Type == CustomTextType.AlterationoptionName);
                if (customText == null)
                {
                    customText = CustomTextHelper.CreateEntity(CustomTextType.AlterationoptionName, companyCultureCode, alterationoption.Name, AlterationoptionFields.AlterationoptionId, alterationoption.AlterationoptionId);
                }
                customText.Text = alterationoption.Name;
                customText.Save();
            }
        }

        private void UpdateExternalProductLink(AlterationoptionEntity alterationoption)
        {
            if (alterationoption.ExternalProductId.HasValue)
            {
                alterationoption.PriceIn = alterationoption.ExternalProductEntity.Price;
                alterationoption.IsAvailable = alterationoption.ExternalProductEntity.IsEnabled;
            }
        }

        private void UpdateProductLink(AlterationoptionEntity alterationoption)
        {
            if (alterationoption.ProductId.HasValue)
            {
                AlterationoptionEntity dbAlterationoption = new AlterationoptionEntity(alterationoption.AlterationoptionId);

                if (alterationoption.ProductId != dbAlterationoption.ProductId)
                {
                    this.ResetProductInheritProperties(alterationoption);
                }

                this.updateAlterationoptionWithProductUseCase.Execute(new UpdateAlterationoptionWithProductRequest
                {
                    Alterationoption = alterationoption,
                    Product = alterationoption.ProductEntity
                });
            }
            else if (alterationoption.IsChanged(AlterationoptionFields.ProductId))
            {
                alterationoption.InheritName = false;
                alterationoption.InheritPrice = false;
                alterationoption.InheritTaxTariff = false;
                alterationoption.InheritIsAlcoholic = false;
                alterationoption.InheritTags = false;
                alterationoption.InheritCustomText = false;
            }
        }

        private void ResetProductInheritProperties(AlterationoptionEntity alterationoption)
        {
            alterationoption.InheritName = true;
            alterationoption.InheritPrice = true;
            alterationoption.InheritTaxTariff = true;
            alterationoption.InheritIsAlcoholic = true;
            alterationoption.InheritTags = true;
            alterationoption.InheritCustomText = true;
        }
    }
}

