using System;
using System.Linq;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Dionysos;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Comet;
using Dionysos.Data.LLBLGen;
using System.Text;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(CompanyEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class CompanyValidator : GeneralValidator
    {
        public const string MINIMUM_REQUIRED_EMENU_APPLICATION_FOR_REST_API = "19112511";
        public const string MINIMUM_REQUIRED_EMENU_APPLICATION_FOR_MESSAGING_V3 = "20012301";

        public enum CompanyValidatorResult
        {
            CantDeleteBecauseItsUsedInSites = 200,
            ProductsWithPackageAlterationInUse = 201
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            CompanyEntity useableEntity = involvedEntity as CompanyEntity;

            // Field 'Name' must have a value
            if (useableEntity.Name.Length == 0)
                throw new ObymobiException(CompanySaveResult.NameEmpty);

            if (useableEntity.Fields[(int)CompanyFieldIndex.ApiVersion].IsChanged && useableEntity.ApiVersion == 2 
                && GetHasClientsRunningBelowMinimumApplicationVersion(useableEntity.CompanyId, MINIMUM_REQUIRED_EMENU_APPLICATION_FOR_REST_API))
            {
                throw new ObymobiException(CompanySaveResult.ClientsRunningBelowMinimumApplicationVersionForRestApi,
                                           "Unable to switch to REST API. There are clients running below the minimum required Emenu version: {0}. Update the clients first.",
                                           MINIMUM_REQUIRED_EMENU_APPLICATION_FOR_REST_API);
            }
            if (useableEntity.Fields[(int)CompanyFieldIndex.MessagingVersion].IsChanged && useableEntity.MessagingVersion == 3 
                && GetHasClientsRunningBelowMinimumApplicationVersion(useableEntity.CompanyId, MINIMUM_REQUIRED_EMENU_APPLICATION_FOR_MESSAGING_V3))
            {
                throw new ObymobiException(CompanySaveResult.ClientsRunningBelowMinimumApplicationVersionForRestApi, 
                    "Unable to switch to Messaging V3. There are clients running below the minimum required Emenu version: {0}. Update the clients first.",
                    MINIMUM_REQUIRED_EMENU_APPLICATION_FOR_MESSAGING_V3);
            }

            // Set a Salt
            if (useableEntity.Salt.IsNullOrWhiteSpace())
            {
                useableEntity.Salt = Dionysos.RandomUtil.GetRandomLowerCaseString(48);
            }

            // Set a salt for pms communication
            if (useableEntity.SaltPms.IsNullOrWhiteSpace())
            {
                string saltKey = Dionysos.RandomUtil.GetRandomLowerCaseString(16); // Must be 16 characters
                useableEntity.SaltPms = Dionysos.Security.Cryptography.Cryptographer.EncryptUsingCBC(saltKey);
            }

            // Replace diacritics
            useableEntity.NameWithoutDiacritics = useableEntity.Name.ReplaceDiacritics();

            // Can't set an empty email if that email is used for survey results
            if (useableEntity.Fields[(int)CompanyFieldIndex.CorrespondenceEmail].IsChanged && StringUtil.IsNullOrWhiteSpace(useableEntity.CorrespondenceEmail))
            {
                // Check the surveys for the company if anyone want to send their results to this email
                foreach (SurveyEntity survey in useableEntity.SurveyCollection)
                {
                    if (survey.EmailResults && StringUtil.IsNullOrWhiteSpace(survey.Email))
                        throw new ObymobiException(CompanySaveResult.EmailIsUsedBySurvey, "SurveyId: {0}", survey.SurveyId);
                }
            }

            // Send netmessage when switching comet handler type
            if (!useableEntity.IsNew && useableEntity.Fields[(int)CompanyFieldIndex.CometHandlerType].IsChanged)
            {
                CometHelper.SwitchCometHandler(useableEntity.CompanyId, useableEntity.CometHandlerType.ToEnum<ClientCommunicationMethod>());
            }

            // Check whether the AvailableOnObymobi flag changed to true
            // ot whether the none of the DPGs has its AvailableOnObymobi flag enabled
            if (useableEntity.IsNew)
            {
                // If the company is new, skip this check
            }
            else if (useableEntity.Fields[CompanyFields.AvailableOnObymobi.FieldIndex].IsChanged && useableEntity.AvailableOnObymobi)
            {
                // Remove the records from the AccessCodeCompany table
                AccessCodeCompanyCollection accessCodeCompanyCollection = new AccessCodeCompanyCollection();
                accessCodeCompanyCollection.AddToTransaction(useableEntity);
                accessCodeCompanyCollection.GetMulti(AccessCodeCompanyFields.CompanyId == useableEntity.CompanyId);
                foreach (AccessCodeCompanyEntity accessCodeCompany in accessCodeCompanyCollection)
                    accessCodeCompany.Delete();
            }

            // Update LastCompanyRemovedFromMobileCompanyList
            // If a Company is taken of the Mobile Company list the LastCompanyListModifiedTicks doesn't update as that is the 
            // Max of all Company.CompanyDataLastModified, and when that company is removed it won't be included. Therefore
            // this (dirty) solution.
            if (!useableEntity.IsNew &&
                !useableEntity.AvailableOnObymobi &&
                useableEntity.Fields[(int)CompanyFieldIndex.AvailableOnObymobi].IsChanged)
            {
                Dionysos.ConfigurationManager.SetValue(ObymobiDataConfigConstants.LastItemRemovedFromMobileCompanyList, DateTime.UtcNow);
            }

            // Clear company name list cache (mainly for CMS)
            if (useableEntity.IsNew || useableEntity.Fields[(int)CompanyFieldIndex.Name].IsChanged)
            {
                Dionysos.Web.CacheHelper.RemoveMultiple(false, "Obymobi.Cms.CompanyCollection");
            }

            // DailyReportSend time change
            if (!useableEntity.IsNew && useableEntity.Fields[(int)CompanyFieldIndex.DailyReportSendTime].IsChanged)
            {
                DateTime utcNow = DateTime.UtcNow;
                var localNow = TimeZoneInfo.ConvertTime(utcNow, useableEntity.TimeZoneInfo);

                DateTime dailyReportSendTime = useableEntity.DailyReportSendTime.GetValueOrDefault(DateTime.MinValue);
                DateTime lastDailyReportSend = new DateTime(localNow.Year, localNow.Month, localNow.Day, dailyReportSendTime.Hour, dailyReportSendTime.Minute, 0);

                if (lastDailyReportSend > localNow)
                    lastDailyReportSend = lastDailyReportSend.AddDays(-1);

                // Reset last report send date
                useableEntity.LastDailyReportSendUTC = TimeZoneInfo.ConvertTimeToUtc(lastDailyReportSend, useableEntity.TimeZoneInfo);
            }

            if (!useableEntity.IsNew && useableEntity.Fields[(int)CompanyFieldIndex.AlterationDialogMode].IsChanged && useableEntity.AlterationDialogMode == AlterationDialogMode.v1)
            {
                ProductCollection products = ProductHelper.GetProductsForCompanyAndAlterationType(useableEntity.CompanyId, AlterationType.Package);
                if (products.Count > 0)
                {
                    StringBuilder productnames = new StringBuilder();

                    for (int i = 0; i < products.Count; i++)
                    {
                        if (i == products.Count - 1)
                        {
                            productnames.Append(products[i].Name);
                        }
                        else
                        {
                            productnames.Append($"{products[i].Name}, ");
                        }
                    }

                    throw new ObymobiException(CompanyValidatorResult.ProductsWithPackageAlterationInUse, "Unable to change alteration dialog mode since there are products with 'Package' alteration type: " + productnames.ToString());
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private bool GetHasClientsRunningBelowMinimumApplicationVersion(int companyId, string version)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddHours(-1));
            filter.Add(DeviceFields.ApplicationVersion < version);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clientCollection = new ClientCollection();
            return clientCollection.GetDbCount(filter, relations) > 0;
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            CompanyEntity useableEntity = involvedEntity as CompanyEntity;

            if (!useableEntity.PreviousCultureCode.IsNullOrWhiteSpace() &&
                useableEntity.PreviousCultureCode != useableEntity.CultureCode)
            {
                UpdateCustomTextCultures(useableEntity);
            }

            // Put the company into the cache
            useableEntity.Fields.State = EntityState.Fetched;
            CacheHelper.CacheEntity(useableEntity);

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            CompanyEntity useableEntity = involvedEntity as CompanyEntity;

            if (CmsSessionHelper.CurrentRole < Role.GodMode)
            {
                throw new ObymobiEntityException(CompanySaveResult.NotEnoughRightsToDeleteCompany, useableEntity);
            }
            if (!useableEntity.OverruleDeletionPrevention)
            {
                throw new ObymobiEntityException(CompanySaveResult.DeleteCompanyFlagIsNotSet, useableEntity);
            }

            // Check if it's used in sites
            // GK I know that this ain't to pretty since we work with Int Value, if you have a good solution for me - please enlighten me!
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PageElementFields.IntValue1 == useableEntity.CompanyId);
            filter.Add(PageElementFields.PageElementType == (int)PageElementType.Venue);

            PageElementCollection pageElements = new PageElementCollection();
            pageElements.AddToTransaction(useableEntity);
            pageElements.GetMulti(filter);

            StringBuilder errorText = new StringBuilder();
            if (pageElements.Count > 0)
            {
                errorText.Append(pageElements.Select(x => "Page '{0}' of Site: '{1}' | ".FormatSafe(x.PageEntity.Name, x.PageEntity.SiteEntity.Name)));
                throw new ObymobiEntityException(CompanyValidatorResult.CantDeleteBecauseItsUsedInSites, "Can't delete Company '{0}' because it's used in Sites: '{1}'", useableEntity.Name, errorText.ToString());
            }

            // Clear company name list cache (CMS)
            Dionysos.Web.CacheHelper.RemoveMultiple(false, "Obymobi.Cms.CompanyCollection");

            base.ValidateEntityBeforeDelete(involvedEntity);
        }

        private void UpdateCustomTextCultures(CompanyEntity entity)
        {
            if (entity.CompanyCultureCollection.Any(x => x.CultureCode == entity.CultureCode))
            { return; }

            AddMissingCompanyCulture(entity.CultureCode, entity.CompanyId);
            UpdateCultures(entity.CultureCode, entity.PreviousCultureCode, entity.CompanyId);
        }

        private void AddMissingCompanyCulture(string newCulture, int companyId) => CompanyCultureHelper.CreateEntity(companyId, newCulture).Save();

        private void UpdateCultures(string newCulture, string previousCulture, int companyId)
        {
            CustomTextCollection customTextCollectionToUpdate = GetCustomTextEntities(previousCulture, companyId);

            foreach (CustomTextEntity entity in customTextCollectionToUpdate)
            {
                entity.CultureCode = newCulture;
            }

            customTextCollectionToUpdate.SaveMulti();
        }

        private CustomTextCollection GetCustomTextEntities(string culture, int companyId)
        {
            PredicateExpression filter = new PredicateExpression(CustomTextFields.CultureCode == culture);
            filter.Add(new PredicateExpression(CustomTextFields.CompanyId == companyId, PredicateExpressionOperator.Or, CustomTextFields.ParentCompanyId == companyId));

            return EntityCollection.GetMulti<CustomTextCollection>(filter);
        }

    }
}

