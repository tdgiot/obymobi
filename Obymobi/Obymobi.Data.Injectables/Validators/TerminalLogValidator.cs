﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(TerminalLogEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class TerminalLogValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            TerminalLogEntity useableEntity = involvedEntity as TerminalLogEntity;

            // Check if the OrderGuid is changed and actually filled for this TerminalLog
            if (useableEntity.Fields[(int)TerminalLogFieldIndex.OrderGuid].IsChanged && !useableEntity.OrderGuid.IsNullOrWhiteSpace())
            {
                PredicateExpression filterOrders = new PredicateExpression();
                filterOrders.Add(OrderFields.Guid == useableEntity.OrderGuid);

                OrderCollection orders = new OrderCollection();

                // Add to Transaction
                orders.AddToTransaction(useableEntity);

                // Get the orders with the same guid as this terminalLog
                orders.GetMulti(filterOrders);

                // Update the terminalLog with the orderId
                if (orders.Count > 0)
                {
                    OrderEntity order = orders[0];
                    useableEntity.OrderId = order.OrderId;
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

