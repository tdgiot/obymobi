﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(CompanyOwnerEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class CompanyOwnerValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            CompanyOwnerEntity useableEntity = involvedEntity as CompanyOwnerEntity;

            if (!useableEntity.MaintenanceBypassNewValidation)
            {
                // Field 'Username' must have a value
                if (useableEntity.Username.Length == 0)
                    throw new ObymobiException(CompanyOwnerSaveResult.UsernameEmpty);

                if (useableEntity.Username.Trim().Split(' ').Length > 1)
                    throw new ObymobiException(CompanyOwnerSaveResult.InvalidUsername);

                // The username must be unique
                if (useableEntity.IsNew)
                {
                    var companyOwners = new CompanyOwnerCollection();
                    if (companyOwners.GetDbCount(CompanyOwnerFields.Username == useableEntity.Username) > 0)
                        throw new ObymobiException(CompanyOwnerSaveResult.UsernameAlreadyExists);
                }

                // Field 'Password' must have a value
                if (useableEntity.Password.Length == 0)
                    throw new ObymobiException(CompanyOwnerSaveResult.PasswordEmpty);

                // Create / update user for CompanyOwner
                UserEntity user = CreateOrUpdateUser(useableEntity);

                if (!useableEntity.UserId.HasValue)
                    useableEntity.UserId = user.UserId;
            }
            
            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private static UserEntity CreateOrUpdateUser(CompanyOwnerEntity useableEntity)
        {
            UserEntity user;
            if (!useableEntity.UserId.HasValue)
                user = new UserEntity();
            else
                user = useableEntity.UserEntity;

            user.AddToTransaction(useableEntity);
            user.Username = useableEntity.Username;

            string password = useableEntity.Password;
            if (useableEntity.IsPasswordEncrypted)
                password = Dionysos.Security.Cryptography.Cryptographer.DecryptStringUsingRijndael(useableEntity.Password);

            Dionysos.Web.Security.UserManager.Instance.SetPassword(user, password);
            user.Save();

            return user;
        }
    }
}

