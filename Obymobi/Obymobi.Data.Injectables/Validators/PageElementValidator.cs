﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;
using Obymobi.Logic.Cms;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PageElementEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PageElementValidator : GeneralValidator
    {
        public enum PageElementValidatorResult
        { 
            DuplicatePageElement = 200
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            PageElementEntity useableEntity = involvedEntity as PageElementEntity;

            // Ensure unique page elements
            if (useableEntity.IsNew ||
                useableEntity.Fields[PageElementFields.PageId.Name].IsChanged ||
                useableEntity.Fields[PageElementFields.SystemName.Name].IsChanged)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(PageElementFields.PageId == useableEntity.PageId);
                filter.Add(PageElementFields.SystemName == useableEntity.SystemName);
                if(!useableEntity.CultureCode.IsNullOrWhiteSpace())
                    filter.Add(PageElementFields.CultureCode == useableEntity.CultureCode);
                else
                    filter.Add(PageElementFields.CultureCode == DBNull.Value);

                if (!useableEntity.IsNew)
                    filter.Add(PageElementFields.PageElementId != useableEntity.PageElementId);

                PageElementCollection pageElements = new PageElementCollection();
                pageElements.AddToTransaction(useableEntity);

                if (pageElements.GetDbCount(filter) > 0)
                    throw new ObymobiEntityException(PageElementValidatorResult.DuplicatePageElement, "PageElement already exists for: PageId: '{0}', SystemName '{1}', 'LanguageId: '{2}'", useableEntity.PageId, useableEntity.SystemName, useableEntity.LanguageId);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

