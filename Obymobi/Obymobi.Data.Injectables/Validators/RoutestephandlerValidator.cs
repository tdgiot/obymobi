﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Routing;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(RoutestephandlerEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class RoutestephandlerValidator : GeneralValidator
    {
        public enum RoutestephandlerValidatorResult
        {
            OnlyOneAutomaticHandlerAllowedPerRoutestep = 200,
            HandlerTypeAndTerminalIncompatible = 201,
            OnlyOneRoutestepAllowedForEmailDocumentRoutestephandler = 202
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            RoutestephandlerEntity useableEntity = involvedEntity as RoutestephandlerEntity;

            RoutestephandlerCollection routestephandlers = new RoutestephandlerCollection();
            routestephandlers.AddToTransaction(useableEntity);

            // Check for multiple automatic handlers in 1 step.
            List<int> automaticallyHandledHandlers = RoutingHelper.GetAutomaticlyHandledRoutestephandlers().Select(x => (int)x).ToList();

            PredicateExpression filterAutomaticHandlersForStep = new PredicateExpression();
            filterAutomaticHandlersForStep.Add(RoutestephandlerFields.RoutestepId == useableEntity.RoutestepId);

            if (!automaticallyHandledHandlers.Contains(useableEntity.HandlerType))
                filterAutomaticHandlersForStep.Add(RoutestephandlerFields.HandlerType == automaticallyHandledHandlers);

            if (!useableEntity.IsNew)
                filterAutomaticHandlersForStep.Add(RoutestephandlerFields.RoutestephandlerId != useableEntity.RoutestephandlerId);

            if (routestephandlers.GetDbCount(filterAutomaticHandlersForStep) > 0)
                throw new ObymobiEntityException(RoutestephandlerValidatorResult.OnlyOneAutomaticHandlerAllowedPerRoutestep, useableEntity);

            // Verify that Terminal and HandlerType are compatible
            if (!RoutingHelper.IsRoutestepHandlerTypeCompatibleWithTerminalType(useableEntity.HandlerType.ToEnum<RoutestephandlerType>(), useableEntity.TerminalEntity.TerminalTypeEnum))
                throw new ObymobiEntityException(RoutestephandlerValidatorResult.HandlerTypeAndTerminalIncompatible, useableEntity);

            // Verify that this is the only step in the route.
            if (useableEntity.RoutestephandlerTypeAsEnum == RoutestephandlerType.EmailDocument)
            {
                PredicateExpression routestepFilter = new PredicateExpression(RoutestepFields.RouteId == useableEntity.RoutestepEntity.RouteId);
                RoutestepCollection routesteps = new RoutestepCollection();

                if (routesteps.GetDbCount(routestepFilter) > 1)
                    throw new ObymobiEntityException(RoutestephandlerValidatorResult.OnlyOneRoutestepAllowedForEmailDocumentRoutestephandler, useableEntity);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            RoutestephandlerEntity useableEntity = involvedEntity as RoutestephandlerEntity;

            PredicateExpression routestephandlerFilter = new PredicateExpression(RoutestephandlerFields.RoutestepId == useableEntity.RoutestepId);
            RoutestephandlerCollection routestephandlers = new RoutestephandlerCollection();

            int numberOfStepsInRouteStep = routestephandlers.GetDbCount(routestephandlerFilter);

            if (numberOfStepsInRouteStep == 1)
            {
                throw new EntityDeleteException("Can not delete the last routestephandler of this routestep");
            }

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

