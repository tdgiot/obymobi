﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(VenueCategoryEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class VenueCategoryValidator : GeneralValidator
    {
        public enum VenueCategoryValidatorResult
        {
            NoNameSpecified = 200,
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            VenueCategoryEntity useableEntity = involvedEntity as VenueCategoryEntity;

            if (useableEntity.IsDirty)
                useableEntity.LastModifiedUTC = DateTime.UtcNow;

            if (useableEntity.Name.IsNullOrWhiteSpace())
                throw new ObymobiException(VenueCategoryValidatorResult.NoNameSpecified);

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

