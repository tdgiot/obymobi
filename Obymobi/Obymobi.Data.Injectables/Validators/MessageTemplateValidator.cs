﻿using System;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(MessageTemplateEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class MessageTemplateValidator : GeneralValidator
    {
        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            MessageTemplateEntity useableEntity = (MessageTemplateEntity)involvedEntity;

            // Set the defaults
            if (useableEntity.IsNew)
            {
                useableEntity.MessageLayoutType = MessageLayoutType.Medium;
            }

            foreach (ScheduledMessageEntity scheduledMessageEntity in useableEntity.ScheduledMessageCollection)
            {
                if (!scheduledMessageEntity.ManualTitleEnabled)
                    scheduledMessageEntity.Title = useableEntity.Title;

                if (!scheduledMessageEntity.ManualFriendlyNameEnabled)
                    scheduledMessageEntity.FriendlyName = useableEntity.Name;

                if (!scheduledMessageEntity.ManualMessageEnabled)
                    scheduledMessageEntity.Message = useableEntity.Message;

                if (!scheduledMessageEntity.ManualMediaEnabled)
                    scheduledMessageEntity.MediaId = useableEntity.MediaId;

                if (!scheduledMessageEntity.ManualMessageLayoutTypeEnabled)
                    scheduledMessageEntity.MessageLayoutType = useableEntity.MessageLayoutType;

                scheduledMessageEntity.Save();
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }
    }
}