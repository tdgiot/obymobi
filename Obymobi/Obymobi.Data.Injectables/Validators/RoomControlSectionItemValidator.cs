﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using System.Linq;
using Obymobi.Enums;
using Dionysos;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(RoomControlSectionItemEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class RoomControlSectionItemValidator : GeneralValidator
    {
        private bool WasNew { get; set; }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            RoomControlSectionItemEntity useableEntity = involvedEntity as RoomControlSectionItemEntity;
            useableEntity.NameChanged = useableEntity.Fields[RoomControlSectionItemFields.Name.Name].IsChanged;

            if (useableEntity.Name.IsNullOrWhiteSpace())
            {
                this.SetDefaultNameByType(useableEntity);
            }

            this.WasNew = useableEntity.IsNew;            

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            RoomControlSectionItemEntity useableEntity = involvedEntity as RoomControlSectionItemEntity;

            if (this.WasNew && useableEntity.Type == RoomControlSectionItemType.RemoteControl && useableEntity.RoomControlWidgetCollection.Count == 0)
            {
                this.CreateRemoteControlWidgets(useableEntity);
            }

            if (useableEntity.ValidatorCreateOrUpdateDefaultSectionItemLanguage && useableEntity.NameChanged)
            {
                this.UpdateDefaultCustomText(useableEntity);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        private void SetDefaultNameByType(RoomControlSectionItemEntity sectionItemEntity)
        {
            if (sectionItemEntity.Type == RoomControlSectionItemType.Scene)
                sectionItemEntity.Name = "Scene";
            else if (sectionItemEntity.Type == RoomControlSectionItemType.StationListing)
                sectionItemEntity.Name = "Stations";
            else if (sectionItemEntity.Type == RoomControlSectionItemType.WakeUpTimer)
                sectionItemEntity.Name = "Wake up";
            else if (sectionItemEntity.Type == RoomControlSectionItemType.SleepTimer)
                sectionItemEntity.Name = "Sleep";
            else if (sectionItemEntity.Type == RoomControlSectionItemType.RemoteControl)
                sectionItemEntity.Name = "Remote control";
        }

        private void CreateRemoteControlWidgets(RoomControlSectionItemEntity sectionItemEntity)
        {
            RoomControlWidgetEntity volumeWidgetEntity = new RoomControlWidgetEntity();
            volumeWidgetEntity.Type = RoomControlWidgetType.Volume1x4;
            volumeWidgetEntity.RoomControlSectionItemId = sectionItemEntity.RoomControlSectionItemId;
            volumeWidgetEntity.AddToTransaction(sectionItemEntity);
            volumeWidgetEntity.Save();

            RoomControlWidgetEntity channelWidgetEntity = new RoomControlWidgetEntity();
            channelWidgetEntity.Type = RoomControlWidgetType.Channel1x4;
            channelWidgetEntity.RoomControlSectionItemId = sectionItemEntity.RoomControlSectionItemId;
            channelWidgetEntity.AddToTransaction(sectionItemEntity);
            channelWidgetEntity.Save();

            RoomControlWidgetEntity navigationWidgetEntity = new RoomControlWidgetEntity();
            navigationWidgetEntity.Type = RoomControlWidgetType.Navigation2x3;
            navigationWidgetEntity.RoomControlSectionItemId = sectionItemEntity.RoomControlSectionItemId;
            navigationWidgetEntity.AddToTransaction(sectionItemEntity);
            navigationWidgetEntity.Save();

            RoomControlWidgetEntity numpadWidgetEntity = new RoomControlWidgetEntity();
            numpadWidgetEntity.Type = RoomControlWidgetType.Numpad2x3;
            numpadWidgetEntity.RoomControlSectionItemId = sectionItemEntity.RoomControlSectionItemId;
            numpadWidgetEntity.AddToTransaction(sectionItemEntity);
            numpadWidgetEntity.Save();

            RoomControlWidgetEntity powerButtonWidgetEntity = new RoomControlWidgetEntity();
            powerButtonWidgetEntity.Type = RoomControlWidgetType.PowerButton1x1;
            powerButtonWidgetEntity.RoomControlSectionItemId = sectionItemEntity.RoomControlSectionItemId;
            powerButtonWidgetEntity.AddToTransaction(sectionItemEntity);
            powerButtonWidgetEntity.Save();
        }

        private void UpdateDefaultCustomText(RoomControlSectionItemEntity roomControlSectionItemEntity)
        {
            string companyCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            if (!companyCultureCode.IsNullOrWhiteSpace())
            {
                CustomTextEntity customText = roomControlSectionItemEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCultureCode && x.Type == CustomTextType.RoomControlSectionItemName);
                if (customText == null)
                {
                    customText = CustomTextHelper.CreateEntity(CustomTextType.RoomControlSectionItemName, companyCultureCode, roomControlSectionItemEntity.Name, RoomControlSectionItemFields.RoomControlSectionItemId, roomControlSectionItemEntity.RoomControlSectionItemId);
                }
                customText.Text = roomControlSectionItemEntity.Name;
                customText.Save();
            }
        }
    }
}

