﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(CompanyEntertainmentEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class CompanyEntertainmentValidator : GeneralValidator
    {
        public enum CompanyEntertainmentValidatorResult
        {
            EntertainmentBelongsToOtherCompany = 200,            
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            CompanyEntertainmentEntity useableEntity = involvedEntity as CompanyEntertainmentEntity;

            // Check if the Entertainment belongs to the selected Company or is generic.
            if (useableEntity.Fields[CompanyEntertainmentFields.EntertainmentId.Name].IsChanged ||
                useableEntity.Fields[CompanyEntertainmentFields.CompanyId.Name].IsChanged)
            {
                if (!useableEntity.EntertainmentEntity.CompanyId.HasValue ||
                    useableEntity.EntertainmentEntity.CompanyId.Value == useableEntity.CompanyId)
                {
                    // Good, either generic Entertainment of belongs to the selected company
                }
                else
                { 
                    // Bad, linked to Entertainment of other Company
                    throw new ObymobiEntityException(CompanyEntertainmentValidatorResult.EntertainmentBelongsToOtherCompany, useableEntity);
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

