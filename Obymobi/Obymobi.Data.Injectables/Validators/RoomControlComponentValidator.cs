﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using System.Linq;
using Obymobi.Enums;
using Dionysos;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(RoomControlComponentEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class RoomControlComponentValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            RoomControlComponentEntity useableEntity = involvedEntity as RoomControlComponentEntity;
            useableEntity.NameChanged = useableEntity.Fields[RoomControlComponentFields.Name.Name].IsChanged;

            if (useableEntity.Name.IsNullOrWhiteSpace())
            {
                useableEntity.Name = useableEntity.NameSystem;
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            RoomControlComponentEntity useableEntity = involvedEntity as RoomControlComponentEntity;

            if (useableEntity.ValidatorCreateOrUpdateDefaultComponentLanguage && useableEntity.NameChanged)
            {
                this.UpdateDefaultCustomText(useableEntity);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        private void UpdateDefaultCustomText(RoomControlComponentEntity roomControlComponentEntity)
        {
            string companyCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            if (!companyCultureCode.IsNullOrWhiteSpace())
            {
                CustomTextEntity customText = roomControlComponentEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCultureCode && x.Type == CustomTextType.RoomControlComponentName);
                if (customText == null)
                {
                    customText = CustomTextHelper.CreateEntity(CustomTextType.RoomControlComponentName, companyCultureCode, roomControlComponentEntity.Name, RoomControlComponentFields.RoomControlComponentId, roomControlComponentEntity.RoomControlComponentId);
                }
                customText.Text = roomControlComponentEntity.Name;
                customText.Save();
            }
        }        
    }
}

