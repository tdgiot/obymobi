﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(OrderitemAlterationitemEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class OrderitemAlterationitemValidator : GeneralValidator
    {
        public enum OrderitemAlterationitemValidatorErrors : int
        {
            ChangeNotAllowedToExistingItem = 200,
            OrderitemAlterationitemIsNotValidatedByOrderProcessingHelper = 202,
            InvalidAlterationType = 203,
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            OrderitemAlterationitemEntity useableEntity = involvedEntity as OrderitemAlterationitemEntity;

            // All Orders must be created using the OrderProcessingHelper
            if (useableEntity.IsNew)
            {
                if (!useableEntity.ValidatedByOrderProcessingHelper)
                {
                    throw new ObymobiException(OrderitemAlterationitemValidatorErrors.OrderitemAlterationitemIsNotValidatedByOrderProcessingHelper);
                }
            }

            if (useableEntity.AlterationType == 0)
                useableEntity.AlterationType = 1; // Default to Options
            else if (!Enum.IsDefined(typeof(AlterationType), useableEntity.AlterationType))
                throw new ObymobiException(OrderitemAlterationitemValidatorErrors.InvalidAlterationType, "Alteration Type not suppored: {0}", useableEntity.AlterationType);

            // Don't allow OrderItemAlterationitems to be changed once saved.
            string changedField;
            if (!useableEntity.IsNew && !useableEntity.OverrideChangeProtection && this.IsNonAllowedFieldChanged(useableEntity, out changedField))
            {
                throw new ObymobiException(OrderitemAlterationitemValidatorErrors.ChangeNotAllowedToExistingItem, "OrderitemAlterationitemId: {0}", useableEntity.OrderitemAlterationitemId);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private bool IsNonAllowedFieldChanged(OrderitemAlterationitemEntity orderitemAlterationitem, out string changedFieldName)
        {
            bool toReturn = false;
            changedFieldName = string.Empty;
            OrderitemAlterationitemFieldIndex[] allowedFields = this.FieldsAllowedToChangeWhenNotNew();

            for (int i = 0; i < orderitemAlterationitem.Fields.Count; i++)
            {
                IEntityField field = orderitemAlterationitem.Fields[i];
                if (field.IsChanged)
                {
                    var changedFieldIndex = field.FieldIndex.ToEnum<OrderitemAlterationitemFieldIndex>();                    

                    if (!allowedFields.Any(f => f == changedFieldIndex))
                    {
                        toReturn = true;
                        changedFieldName = field.Name;
                        break;
                    }
                }
            }

            return toReturn;
        }

        private OrderitemAlterationitemFieldIndex[] FieldsAllowedToChangeWhenNotNew()
        {
            return new OrderitemAlterationitemFieldIndex[] { 
                OrderitemAlterationitemFieldIndex.AlterationitemId, // AlterationitemId can be set to null when an Alterationitem is deleted
                OrderitemAlterationitemFieldIndex.FormerAlterationitemId,
                OrderitemAlterationitemFieldIndex.AlterationType,
                OrderitemAlterationitemFieldIndex.Guid,
                OrderitemAlterationitemFieldIndex.UpdatedUTC,
                OrderitemAlterationitemFieldIndex.UpdatedBy                                                         
            };
        }
    }
}

