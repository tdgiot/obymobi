﻿using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(CompanyReleaseEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class CompanyReleaseValidator : GeneralValidator
    {
        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            CompanyReleaseEntity useableEntity = involvedEntity as CompanyReleaseEntity;

            ReleaseHelper.UpdateVersionOnCompanyForRelease(useableEntity.CompanyEntity, useableEntity.ReleaseEntity);

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            CompanyReleaseEntity useableEntity = involvedEntity as CompanyReleaseEntity;

            ReleaseHelper.UpdateVersionForCompaniesWithoutCompanyRelease(useableEntity.ReleaseEntity.ApplicationEntity, useableEntity.ReleaseId);
            ReleaseHelper.UpdateVersionForCompaniesWithCompanyRelease(useableEntity.ReleaseEntity.ApplicationEntity, useableEntity.ReleaseId);

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

