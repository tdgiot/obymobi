﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Dionysos;
using Obymobi.Analytics.Google.Recording;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(OrderEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class OrderValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            OrderEntity useableEntity = involvedEntity as OrderEntity;

            // Set the field values of the denormalized fields
            if (useableEntity.IsNew)
            {
                if (useableEntity.CustomerId.HasValue)
                {
                    useableEntity.CustomerFirstname = useableEntity.CustomerEntity.Firstname;
                    useableEntity.CustomerLastname = useableEntity.CustomerEntity.Lastname;
                    useableEntity.CustomerLastnamePrefix = useableEntity.CustomerEntity.LastnamePrefix;
                    if (useableEntity.CustomerPhonenumber.IsNullOrWhiteSpace() && !useableEntity.CustomerEntity.Phonenumber.IsNullOrWhiteSpace())
                        useableEntity.CustomerPhonenumber = useableEntity.CustomerEntity.Phonenumber;
                }

                if (useableEntity.DeliverypointId.HasValue)
                {
                    useableEntity.DeliverypointName = useableEntity.DeliverypointEntity.Name;
                    useableEntity.DeliverypointNumber = useableEntity.DeliverypointEntity.Number;
                    useableEntity.DeliverypointgroupName = useableEntity.DeliverypointEntity.DeliverypointgroupEntity.Name;
                }                
            }
            useableEntity.CompanyName = useableEntity.CompanyEntity.Name;

            // Require unique Guid if filled
            if (!useableEntity.Guid.IsNullOrWhiteSpace() && useableEntity.Fields[OrderFields.Guid.Name].IsChanged)
            {
                OrderCollection orders = new OrderCollection();
                orders.AddToTransaction(useableEntity);

                PredicateExpression filter = new PredicateExpression();
                filter.Add(OrderFields.Guid == useableEntity.Guid);
                if (!useableEntity.IsNew)
                    filter.Add(OrderFields.OrderId != useableEntity.OrderId);

                if (orders.GetDbCount(filter) > 0)
                    throw new ObymobiEntityException(OrderSaveResult.GuidNotUnique, useableEntity);
            }

            if (useableEntity.IsNew)
            {
                // Update the Text Values of the Enums
                useableEntity.StatusText = useableEntity.StatusAsEnum.ToString();
                useableEntity.ErrorText = useableEntity.ErrorCodeAsEnum.ToString();
                useableEntity.TypeText = useableEntity.TypeAsEnum.ToString();

                // All checks that were placed and required here are removed and are checked by the OrderProcessingHelper
                if (!useableEntity.ValidatedByOrderProcessingHelper)
                {
                    throw new ObymobiException(OrderSaveResult.OrderIsNotValidatedByOrderProcessingHelper);
                }
            }
            else
            {
                // Update the Text Values of the Enums
                useableEntity.StatusText = useableEntity.StatusAsEnum.ToString();

                // Check if it's an allowed change. Saved orders are only very limited allowed to be changed
                string changedField;
                if (!useableEntity.IsNew && !useableEntity.OverrideChangeProtection && this.IsNonAllowedFieldChanged(useableEntity, out changedField))
                {
                    throw new ObymobiException(OrderSaveResult.ChangeNotAllowedToExistingOrder, "OrderId: {0}, Field: {1}",
                                               useableEntity.OrderId, changedField);
                }
            }

            // Check if the status changed, and if it did, if it changed from Unproccesable to Processed
            if (useableEntity.IsNew || useableEntity.Fields[OrderFields.Status.Name].IsChanged)
            {
                useableEntity.SendPokeInUpdateAfterSave = true;

                // Check if we have a manually processed order:
                if (useableEntity.Fields[OrderFields.Status.Name].DbValue != null &&
                    useableEntity.Fields[OrderFields.Status.Name].DbValue != DBNull.Value)
                {
                    if ((int)useableEntity.Fields[OrderFields.Status.Name].DbValue == (int)OrderStatus.Unprocessable &&
                        useableEntity.StatusAsEnum == OrderStatus.Processed)
                    {
                        useableEntity.validatorVariableManuallyProcessedOrder = true;
                    }
                }
            }
            else
                useableEntity.SendPokeInUpdateAfterSave = false;

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            OrderEntity useableEntity = involvedEntity as OrderEntity;

            if (useableEntity.SendPokeInUpdateAfterSave)
            {
                this.CreateAnalyticsProcessingTask(useableEntity);

                // GK A bit dirty, but hey, who cares?
                // Because an order gets set to processed by the noc if could give a false postive
                // therefore, if there's an error we always update pokein listeners as unprocessable.
                OrderStatus status = useableEntity.ErrorCodeAsEnum != OrderProcessingError.None ? OrderStatus.Unprocessable : useableEntity.StatusAsEnum;

                // FO With routing per product
                // We determine the status of an order by the status of its orderitems on the Emenu
                string orderitemIds = "";
                foreach (OrderitemEntity orderitem in useableEntity.OrderitemCollection)
                {
                    if (orderitem.OrderitemId > 0)
                        orderitemIds += orderitem.OrderitemId + "-";
                }

                // Check if we can find a TerminalId (is used as sender only.
                int? terminalId = null;
                foreach (var orderRouteStepHandler in useableEntity.OrderRoutestephandlerCollection)
                {
                    if (orderRouteStepHandler.TerminalId.HasValue)
                    {
                        terminalId = orderRouteStepHandler.TerminalId;
                        break;
                    }
                }

                // Send update to Client (if there is one, not for Mobile Orders)
                if (useableEntity.ClientId.HasValue && useableEntity.ClientId.Value > 0)
                {
                    CometHelper.SendOrderStatus(useableEntity.CompanyId,
                                                useableEntity.OrderId,
                                                orderitemIds,
                                                status,
                                                useableEntity.ClientId ?? 0,
                                                terminalId ?? 0);
                }
                else
                {
                    Debug.WriteLine("INFORMATION: SendOrderStatus message skipped, because there is no Client.");
                }
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        private void CreateAnalyticsProcessingTask(OrderEntity orderEntity)
        {
            // Log Change to Analytics
            try
            {
                string label = orderEntity.StatusAsEnum.ToString();
                if (orderEntity.validatorVariableManuallyProcessedOrder)
                {
                    label += " (Manually Processed)";
                }
                AnalyticsProcessingTaskHelper.CreateTask(orderEntity, "Order Status Updated", label);
            }
            catch
            {
                // Not worth crashing
                if (TestUtil.IsPcDeveloper)
                    throw;
            }
        }

        public override bool ValidateFieldValue(IEntityCore involvedEntity, int fieldIndex, object value)
        {
            // Prevent changes of the OrderStatus / ErrorCode without careful consideration.
            // TODO.
            return base.ValidateFieldValue(involvedEntity, fieldIndex, value);
        }

        private bool IsNonAllowedFieldChanged(OrderEntity order, out string changedFieldName)
        {
            bool toReturn = false;
            changedFieldName = string.Empty;
            OrderFieldIndex[] allowedFields = this.FieldsAllowedToChangeWhenNotNew();

            for (int i = 0; i < order.Fields.Count; i++)
            {
                IEntityField field = order.Fields[i];
                if (field.IsChanged)
                {
                    var changedFieldIndex = field.FieldIndex.ToEnum<OrderFieldIndex>();
                    
                    if (!allowedFields.Any(f => f == changedFieldIndex)) // GK Warning told me this could never be true, let's see if that's the case.
                    {
                        toReturn = true;
                        changedFieldName = field.Name;
                        break;
                    }
                }
            }

            return toReturn;
        }

        private OrderFieldIndex[] FieldsAllowedToChangeWhenNotNew()
        {
            return new [] { OrderFieldIndex.BenchmarkProcessedUTC,
                                            OrderFieldIndex.ConfirmationCode, 
                                            OrderFieldIndex.ErrorSentByEmail,
                                            OrderFieldIndex.ErrorSentBySMS, 
                                            OrderFieldIndex.Guid, 
                                            OrderFieldIndex.PlaceTimeUTC, 
                                            OrderFieldIndex.RoutingLog, 
                                            OrderFieldIndex.Printed, 
                                            OrderFieldIndex.Status, 
                                            OrderFieldIndex.StatusText,
                                            OrderFieldIndex.ProcessedUTC, 
                                            OrderFieldIndex.UpdatedUTC,
                                            OrderFieldIndex.UpdatedBy,
                                            OrderFieldIndex.ErrorCode,
                                            OrderFieldIndex.ErrorText,
                                            OrderFieldIndex.DeliverypointName,
                                            OrderFieldIndex.ClientId,
                                            OrderFieldIndex.CompanyName,
                                            OrderFieldIndex.HotSOSServiceOrderId,
                                            OrderFieldIndex.DeliverypointId,
                                            OrderFieldIndex.ServiceMethodId,
                                            OrderFieldIndex.CheckoutMethodId,
                                            OrderFieldIndex.ChargeToDeliverypointId
                                          };
        }
    }
}


