﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PosalterationitemEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PosalterationitemValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            PosalterationitemEntity useableEntity = involvedEntity as PosalterationitemEntity;

            foreach (var ai in useableEntity.AlterationitemCollection)
                ai.Delete();

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}
