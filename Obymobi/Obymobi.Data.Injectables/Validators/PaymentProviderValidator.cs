﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PaymentProviderEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PaymentProviderValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            PaymentProviderEntity useableEntity = involvedEntity as PaymentProviderEntity;

            

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

