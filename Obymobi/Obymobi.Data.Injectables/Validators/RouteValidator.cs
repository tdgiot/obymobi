﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Routing;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(RouteEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class RouteValidator : GeneralValidator
    {
        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            RouteEntity useableEntity = involvedEntity as RouteEntity;

            if (useableEntity.ValidatorOverrideCreateDefaultStep && useableEntity.RoutestepCollection.Count == 0)
            {
                // Always create a first step
                // Always automatically create the first step
                RoutestepEntity routestep = new RoutestepEntity();
                routestep.Number = RoutingHelper.FIRST_STEP_NUMBER;
                routestep.RouteId = useableEntity.RouteId;
                routestep.AddToTransaction(useableEntity);
                routestep.Save();
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            RouteEntity useableEntity = involvedEntity as RouteEntity;

            // Need to delete these manually due to checks in their validators
            PredicateExpression routestepFilter = new PredicateExpression(RoutestepFields.RouteId == useableEntity.RouteId);

            RoutestepCollection routestepCollection = new RoutestepCollection();
            routestepCollection.GetMulti(routestepFilter);

            foreach (RoutestepEntity routestepEntity in routestepCollection)
            {
                RoutestephandlerCollection routestephandlerCollection = new RoutestephandlerCollection();
                routestephandlerCollection.DeleteMulti(RoutestephandlerFields.RoutestepId == routestepEntity.RoutestepId);
            }

            routestepCollection.DeleteMulti(routestepFilter);

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}
