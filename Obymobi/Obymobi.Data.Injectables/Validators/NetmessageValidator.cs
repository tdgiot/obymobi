﻿using System;
using System.Collections.Generic;
using System.Text;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(NetmessageEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class NetmessageValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            NetmessageEntity useableEntity = involvedEntity as NetmessageEntity;

            if (useableEntity.IsNew)
            {
                useableEntity.CreatedUTC = DateTime.UtcNow;

                // Select all non-submitted netmessage rows from database where messagetype is the same as useableEntity
                var filter = new PredicateExpression();
                filter.Add(NetmessageFields.MessageType == useableEntity.MessageType);
                filter.Add(NetmessageFields.Status == Obymobi.Enums.NetmessageStatus.WaitingForVerification);

                filter.Add(NetmessageFields.SenderClientId == useableEntity.SenderClientId);
                filter.Add(NetmessageFields.SenderCompanyId == useableEntity.SenderCompanyId);
                filter.Add(NetmessageFields.SenderDeliverypointId == useableEntity.SenderDeliverypointId);
                filter.Add(NetmessageFields.SenderTerminalId == useableEntity.SenderTerminalId);
                filter.Add(NetmessageFields.ReceiverClientId == useableEntity.ReceiverClientId);
                filter.Add(NetmessageFields.ReceiverCompanyId == useableEntity.ReceiverCompanyId);
                filter.Add(NetmessageFields.ReceiverDeliverypointId == useableEntity.ReceiverDeliverypointId);
                filter.Add(NetmessageFields.ReceiverDeliverypointgroupId == useableEntity.ReceiverDeliverypointgroupId);
                filter.Add(NetmessageFields.ReceiverTerminalId == useableEntity.ReceiverTerminalId);

                var useableModel = NetmessageHelper.CreateTypedNetmessageModelFromEntity(useableEntity);

                var collection = new NetmessageCollection();
                collection.AddToTransaction(useableEntity);
                collection.GetMulti(filter);
                if (collection.Count > 0)
                {
                    foreach (var entity in collection)
                    {
                        var savedModel = NetmessageHelper.CreateTypedNetmessageModelFromEntity(entity);

                        bool overwriteExistingNetmessageWithThisMessage = false;
                        if (useableModel.IsDuplicateMessage(savedModel, out overwriteExistingNetmessageWithThisMessage))
                        {
                            if (overwriteExistingNetmessageWithThisMessage)
                            {
                                // GK I could be wrong here, but we already have the entity, so let's just use that (?)
                                // var savedEntity = NetmessageHelper.CreateNetmessageEntityFromModel(savedModel);
                                entity.AddToTransaction(useableEntity);
                                entity.FieldValue1 = useableEntity.FieldValue1;
                                entity.FieldValue2 = useableEntity.FieldValue2;
                                entity.FieldValue3 = useableEntity.FieldValue3;
                                entity.FieldValue4 = useableEntity.FieldValue4;
                                entity.FieldValue5 = useableEntity.FieldValue5;
                                entity.FieldValue6 = useableEntity.FieldValue6;
                                entity.FieldValue7 = useableEntity.FieldValue7;
                                entity.FieldValue8 = useableEntity.FieldValue8;
                                entity.FieldValue9 = useableEntity.FieldValue9;
                                entity.MessageLog = useableModel.MessageLog;
                                entity.IsNew = false;
                                entity.Validator = null;
                                entity.Save();
                            }

                            useableEntity.NetmessageId = savedModel.NetmessageId;
                            useableEntity.Guid = savedModel.Guid;

                            throw new ObymobiEntityException(NetmessageValidateError.AlreadyExists, useableEntity);
                        }
                    }
                }
            }

            useableEntity.MessageTypeText = useableEntity.MessageTypeAsEnum.ToString();
            useableEntity.StatusText = useableEntity.StatusAsEnum.ToString();

	        base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

