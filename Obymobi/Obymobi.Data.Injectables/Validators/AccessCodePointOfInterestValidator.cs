﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AccessCodePointOfInterestEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AccessCodePointOfInterestValidator : GeneralValidator
    {
        public enum AccessCodePointOfInterestValidatorResult
        {
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AccessCodePointOfInterestEntity useableEntity = involvedEntity as AccessCodePointOfInterestEntity;

            if (useableEntity.IsDirty)
            {
                // Ronnie Robusto style but unfortunately no other option
                useableEntity.PointOfInterestEntity.LastModifiedUTC = DateTime.UtcNow;
                useableEntity.PointOfInterestEntity.Save();

                useableEntity.AccessCodeEntity.LastModifiedUTC = DateTime.UtcNow;
                useableEntity.AccessCodeEntity.Save();
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            AccessCodePointOfInterestEntity useableEntity = involvedEntity as AccessCodePointOfInterestEntity;

            // Ronnie Robusto style but unfortunately no other option
            useableEntity.PointOfInterestEntity.LastModifiedUTC = DateTime.UtcNow;
            useableEntity.PointOfInterestEntity.Save();

            useableEntity.AccessCodeEntity.LastModifiedUTC = DateTime.UtcNow;
            useableEntity.AccessCodeEntity.Save();

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

