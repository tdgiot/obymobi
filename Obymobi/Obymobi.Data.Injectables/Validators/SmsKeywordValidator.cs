﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(SmsKeywordEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class SmsKeywordValidator : GeneralValidator
    {
        public enum SmsKeywordError
        {
            KeywordAlreadyExists,
            NotLinkedToCompany
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            var useableEntity = involvedEntity as SmsKeywordEntity;

            if (!useableEntity.CompanyId.HasValue || useableEntity.CompanyId.GetValueOrDefault() <= 0)
            {
                throw new ObymobiException(SmsKeywordError.NotLinkedToCompany, "Keyword '{0}' is not linked to a company.", useableEntity.Keyword);
            }

            var filter = new PredicateExpression(SmsKeywordFields.Keyword == useableEntity.Keyword);
            if (!useableEntity.IsNew)
                filter.Add(SmsKeywordFields.SmsKeywordId != useableEntity.SmsKeywordId);

            var keyCollection = new SmsKeywordCollection();
            keyCollection.GetMulti(filter);

            if (keyCollection.Count > 0)
            {
                throw new ObymobiException(SmsKeywordError.KeywordAlreadyExists, "Keyword '{0}' already exists in '{1}'.", useableEntity.Keyword, keyCollection[0].SmsInformationEntity.Name);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }
    }
}
