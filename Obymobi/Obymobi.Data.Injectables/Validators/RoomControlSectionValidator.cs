﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using System.Linq;
using Obymobi.Enums;
using Dionysos;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(RoomControlSectionEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class RoomControlSectionValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            RoomControlSectionEntity useableEntity = involvedEntity as RoomControlSectionEntity;
            useableEntity.NameChanged = useableEntity.Fields[RoomControlSectionFields.Name.Name].IsChanged;

            if (useableEntity.Name.IsNullOrWhiteSpace())
            {
                this.SetDefaultNameByType(useableEntity);
            }

            base.ValidateEntityBeforeSave(involvedEntity);            
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            RoomControlSectionEntity useableEntity = involvedEntity as RoomControlSectionEntity;

            if (useableEntity.ValidatorCreateOrUpdateDefaultSectionLanguage && useableEntity.NameChanged)
            {
                this.UpdateDefaultCustomText(useableEntity);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        private void SetDefaultNameByType(RoomControlSectionEntity sectionEntity)
        {
            if (sectionEntity.Type == RoomControlSectionType.Lighting)
                sectionEntity.Name = "Lighting";
            else if (sectionEntity.Type == RoomControlSectionType.Blinds)
                sectionEntity.Name = "Drapes";
            else if (sectionEntity.Type == RoomControlSectionType.Comfort)
                sectionEntity.Name = "Comfort";
            else if (sectionEntity.Type == RoomControlSectionType.Custom)
                sectionEntity.Name = "Custom";
            else if (sectionEntity.Type == RoomControlSectionType.ScreenSleep)
                sectionEntity.Name = "Screen sleep";
            else if (sectionEntity.Type == RoomControlSectionType.Service)
                sectionEntity.Name = "Service";
            else if (sectionEntity.Type == RoomControlSectionType.Dashboard)
                sectionEntity.Name = "Dashboard";
            else if (sectionEntity.Type == RoomControlSectionType.Timers)
                sectionEntity.Name = "Timers";            
        }

        private void UpdateDefaultCustomText(RoomControlSectionEntity roomControlSectionEntity)
        {
            string companyCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            if (!companyCultureCode.IsNullOrWhiteSpace())
            {
                CustomTextEntity customText = roomControlSectionEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCultureCode && x.Type == CustomTextType.RoomControlSectionName);
                if (customText == null)
                {
                    customText = CustomTextHelper.CreateEntity(CustomTextType.RoomControlSectionName, companyCultureCode, roomControlSectionEntity.Name, RoomControlSectionFields.RoomControlSectionId, roomControlSectionEntity.RoomControlSectionId);
                }
                customText.Text = roomControlSectionEntity.Name;
                customText.Save();                
            }
        }
    }
}

