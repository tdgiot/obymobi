﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using System.Text.RegularExpressions;
using Dionysos;
using Dionysos.Globalization;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(DeviceEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class DeviceValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            DeviceEntity useableEntity = involvedEntity as DeviceEntity;

            if (useableEntity.Fields[(int)DeviceFieldIndex.Identifier].IsChanged)
            {
                DeviceCollection deviceCollection = new DeviceCollection();
                deviceCollection.AddToTransaction(useableEntity);

                PredicateExpression filter = new PredicateExpression();
                filter.Add(DeviceFields.Identifier == useableEntity.Identifier);
                if (useableEntity.DeviceId > 0)
                {
                    // Exclude the entity itself from the search for the identifier
                    filter.Add(DeviceFields.DeviceId != useableEntity.DeviceId);
                }

                if (deviceCollection.GetDbCount(filter) > 0)
                    throw new ObymobiException(DeviceSaveResult.IdentifierAlreadyUsedForDevice, "There is already a device with identifier: {0}", useableEntity.Identifier);
            }

            if (!useableEntity.IsNew && useableEntity.Fields[DeviceFields.CommunicationMethod.Name].IsChanged)
                this.WriteLog(useableEntity);

            // Prevent the LastRequestUTC to be set to an older value
            DateTime? lastRequestUtcInDb = useableEntity.Fields[(int)DeviceFieldIndex.LastRequestUTC].DbValue as DateTime?;
            DateTime? lastRequestUtc = useableEntity.Fields[(int)DeviceFieldIndex.LastRequestUTC].CurrentValue as DateTime?;
            if (lastRequestUtcInDb.HasValue && lastRequestUtc.HasValue && lastRequestUtc < lastRequestUtcInDb)
                useableEntity.Fields[(int)DeviceFieldIndex.LastRequestUTC].IsChanged = false;

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private void WriteLog(DeviceEntity useableEntity)
        {
            if (useableEntity.ClientEntitySingle != null)
            {
                var oldEntity = new DeviceEntity(useableEntity.DeviceId);
                if (useableEntity.CommunicationMethod != oldEntity.CommunicationMethod)
                {
                    var log = string.Format("Communication method changed to {0}", useableEntity.CommunicationMethod.ToEnum<ClientCommunicationMethod>().ToString());
                    var clientLog = ClientLogHelper.CreateClientLogEntity(useableEntity.ClientEntitySingle.ClientId, ClientLogType.CommunicationMethodChanged, log, 0);
                    clientLog.AddToTransaction(useableEntity);
                    clientLog.Save();
                }
            }
            else if (useableEntity.TerminalEntitySingle != null)
            {
                var oldEntity = new DeviceEntity(useableEntity.DeviceId);
                if (useableEntity.CommunicationMethod != oldEntity.CommunicationMethod)
                {
                    TerminalLogEntity log = new TerminalLogEntity();
                    log.AddToTransaction(useableEntity);
                    log.TerminalId = useableEntity.TerminalEntitySingle.TerminalId;
                    log.TypeAsEnum = TerminalLogType.CommunicationMethodChanged;
                    log.Message = string.Format("Communication method changed to {0}", useableEntity.CommunicationMethod.ToEnum<ClientCommunicationMethod>().ToString());
                    log.Log = "";
                    log.Save();
                }
            }
        }
    }
}

