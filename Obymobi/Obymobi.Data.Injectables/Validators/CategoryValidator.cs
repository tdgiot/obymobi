﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using System.Linq;
using Dionysos;
using Dionysos.Data.Extensions;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(CategoryEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class CategoryValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            CategoryEntity useableEntity = involvedEntity as CategoryEntity;
            useableEntity.NameChanged = useableEntity.Fields[CategoryFields.Name.Name].IsChanged;

            // Prevent Childs in categories that have products
            if (useableEntity.ParentCategoryId.HasValue && useableEntity.ParentCategoryEntity.ProductCategoryCollection.Count > 0)
                throw new ObymobiException(CategorySaveResult.ParentCategoryContainsProducts);

            // Company Required
            if (useableEntity.CompanyId <= 0)
                throw new ObymobiException(CategorySaveResult.CompanyIdEmpty);

            // Field 'Name' must have a value
            if (useableEntity.Name.Length == 0)
                throw new ObymobiException(CategorySaveResult.NameEmpty);

            // Require either a MenuId (otherwise it's an orphan)
            if (!useableEntity.MenuId.HasValue)
            {
                if (useableEntity.ParentCategoryId.HasValue && useableEntity.ParentCategoryEntity.MenuId.HasValue)
                {
                    useableEntity.MenuId = useableEntity.ParentCategoryEntity.MenuId;
                }
                else
                    throw new ObymobiException(CategorySaveResult.MenuIdIsRequired);
            }

            // To prevent infinite recursion
            if (useableEntity.ParentCategoryId.HasValue)
            {
                if (useableEntity.CategoryId == useableEntity.ParentCategoryId)
                    throw new ObymobiException(CategorySaveResult.SelfReferencing);

                ArrayList underlyingCategories = new ArrayList();
                underlyingCategories = useableEntity.GetAllUnderlyingCategoryIds(useableEntity, underlyingCategories);
                if (underlyingCategories.Contains(useableEntity.ParentCategoryId.Value))
                    throw new ObymobiException(CategorySaveResult.CircularReference);
            }

            useableEntity.ParentCategoryChanged = useableEntity.Fields[CategoryFields.ParentCategoryId.Name].IsChanged;
            if (useableEntity.ParentCategoryChanged)
            {
                useableEntity.OldParentCategoryId = (int?)useableEntity.Fields[CategoryFields.ParentCategoryId.Name].DbValue;
                useableEntity.MenuItemsMustBeLinkedToExternalProduct = useableEntity.ParentCategoryEntity.MenuItemsMustBeLinkedToExternalProduct;
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            CategoryEntity useableEntity = involvedEntity as CategoryEntity;

            if (useableEntity.ValidatorCreateOrUpdateDefaultCategoryLanguage && useableEntity.NameChanged)
            {
                this.UpdateDefaultCustomText(useableEntity);
            }


            base.ValidateEntityAfterSave(useableEntity);
        }

        private void UpdateDefaultCustomText(CategoryEntity categoryEntity)
        {
            string companyCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            if (!companyCultureCode.IsNullOrWhiteSpace())
            {
                CustomTextEntity customText = categoryEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCultureCode && x.Type == CustomTextType.CategoryName);
                if (customText == null)
                {
                    customText = CustomTextHelper.CreateEntity(CustomTextType.CategoryName, companyCultureCode, categoryEntity.Name, CategoryFields.CategoryId, categoryEntity.CategoryId);
                }
                else
                {
                    customText.Text = categoryEntity.Name;
                }
                customText.Save();
            }
        }        
    }
}

