﻿using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ApplicationEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ApplicationValidator : GeneralValidator
    {
        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            ApplicationEntity useableEntity = involvedEntity as ApplicationEntity;

            ReleaseHelper.UpdateVersionForCompaniesWithoutCompanyRelease(useableEntity, null);
            ReleaseHelper.UpdateVersionForCompaniesWithCompanyRelease(useableEntity, null);

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            ApplicationEntity useableEntity = involvedEntity as ApplicationEntity;

            ReleaseHelper.UpdateVersionForCompaniesWithoutCompanyRelease(useableEntity, useableEntity.ReleaseId);
            ReleaseHelper.UpdateVersionForCompaniesWithCompanyRelease(useableEntity, useableEntity.ReleaseId);

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

