using Crave.Domain.UseCases;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Dionysos;
using Dionysos.Data.Extensions;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Integrations.POS;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Linq;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ProductEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ProductValidator : GeneralValidator
    {
        private readonly UpdateProductsForBrandProductUseCase updateProductsForBrandProductUseCase;
        private readonly UpdateProductWithBrandProductUseCase updateProductWithBrandProductUseCase;
        private readonly UpdateAlterationoptionFieldsWithProductUseCase updateAlterationoptionFieldsWithProductUseCase;
        private readonly RemoveBrandProductFromProductUseCase removeBrandProductFromProductUseCase;

        public ProductValidator()
        {
            this.updateProductsForBrandProductUseCase = new UpdateProductsForBrandProductUseCase();
            this.updateProductWithBrandProductUseCase = new UpdateProductWithBrandProductUseCase();
            this.removeBrandProductFromProductUseCase = new RemoveBrandProductFromProductUseCase();
            this.updateAlterationoptionFieldsWithProductUseCase = new UpdateAlterationoptionFieldsWithProductUseCase();
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            ProductEntity useableEntity = involvedEntity as ProductEntity;
            useableEntity.NameChanged = useableEntity.IsChanged(ProductFields.Name);

            ValidateProductName(useableEntity);

            if (useableEntity.IsChanged(ProductFields.PosproductId))
            {
                ValidatePosProductIdChange(useableEntity);
            }

            if (useableEntity.IsChanged(ProductFields.GenericproductId))
            {
                GenericProductIdChanged(useableEntity);
            }

            if (useableEntity.IsChanged(ProductFields.BrandProductId))
            {
                BrandProductIdChanged(useableEntity);
            }
            else if (useableEntity.BrandProductId.HasValue)
            {
                UpdateProductWithBrandProduct(useableEntity, useableEntity.BrandProductEntity);
            }

            if (useableEntity.IsChanged(ProductFields.ExternalProductId))
            {
                ExternalProductIdChanged(useableEntity);
            }

            if (useableEntity.IsChanged(ProductFields.TaxTariffId))
            {
                TaxTariffIdChanged(useableEntity);
            }

            if (useableEntity.IsChanged(ProductFields.SubType))
            {
                SubTypeChanged(useableEntity);
            }

            if (useableEntity.HasAnyChanged(ProductFields.Name, ProductFields.PriceIn, ProductFields.TaxTariffId, ProductFields.IsAlcoholic))
            {
                UpdateAlterationoptionWithProduct(useableEntity);
            }

            if (useableEntity.ExternalProductId.HasValue && useableEntity.IsChanged(ProductFields.ExternalProductId))
            {
                useableEntity.PriceIn = useableEntity.ExternalProductEntity.Price;
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            ProductEntity useableEntity = involvedEntity as ProductEntity;

            if (useableEntity.ValidatorCreateOrUpdateDefaultCustomText && useableEntity.NameChanged)
            {
                this.UpdateDefaultCustomText(useableEntity);
            }

            if (useableEntity.BrandId.HasValue)
            {
                this.updateProductsForBrandProductUseCase.Execute(new UpdateProductsForBrandProductRequest
                {
                    BrandProduct = useableEntity
                });
            }

            base.ValidateEntityAfterSave(useableEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            ProductEntity useableEntity = involvedEntity as ProductEntity;

            // Prevent products to be deleted that serve as a AlterationOption in the POS
            if (useableEntity.PosproductId.HasValue)
            {
                PredicateExpression filterAlterationOptions = new PredicateExpression();
                filterAlterationOptions.Add(AlterationoptionFields.CompanyId == useableEntity.CompanyId);
                filterAlterationOptions.Add(AlterationoptionFields.PosproductId == useableEntity.PosproductId);
                filterAlterationOptions.Add(AlterationoptionFields.Version == 1);

                AlterationoptionCollection alterationOptions = new AlterationoptionCollection();
                alterationOptions.AddToTransaction(useableEntity);
                if (alterationOptions.GetDbCount(filterAlterationOptions) > 0)
                {
                    throw new ObymobiException(ProductSaveResult.CantDeleteProductsThatAreUsedByAlterationOptions, "ProductId: {0}", useableEntity.ProductId);
                }
            }
            base.ValidateEntityBeforeDelete(involvedEntity);
        }

        private void UpdateDefaultCustomText(ProductEntity productEntity)
        {
            string companyCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            if (!companyCultureCode.IsNullOrWhiteSpace())
            {
                CustomTextEntity customText = productEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCultureCode && x.Type == CustomTextType.ProductName);
                if (customText == null)
                {
                    customText = CustomTextHelper.CreateEntity(CustomTextType.ProductName, companyCultureCode, productEntity.Name, ProductFields.ProductId, productEntity.ProductId);
                }

                customText.Text = productEntity.Name;
                customText.AddToTransaction(productEntity);
                customText.Save();
            }
        }

        private void UpdateProductWithBrandProduct(ProductEntity product, ProductEntity brandProduct)
        {
            SimpleResponse response = this.updateProductWithBrandProductUseCase.Execute(new UpdateProductWithBrandProductRequest
            {
                Product = product,
                BrandProduct = brandProduct
            });

            if (!response.Success)
            {
                throw new ObymobiException(ProductSaveResult.UpdateProductWithBrandProductFailed, response.Message);
            }
        }

        private void UpdateAlterationoptionWithProduct(ProductEntity product)
        {
            foreach (AlterationoptionEntity alterationoption in product.AlterationoptionCollection)
            {
                this.updateAlterationoptionFieldsWithProductUseCase.Execute(new UpdateAlterationoptionFieldsWithProductRequest
                {
                    Alterationoption = alterationoption,
                    Product = product
                });

                alterationoption.AddToTransaction(product);
                alterationoption.Save();
            }
        }

        private void RemoveBrandProductFromProduct(ProductEntity product)
        {
            SimpleResponse response = this.removeBrandProductFromProductUseCase.Execute(new RemoveBrandProductFromProductRequest
            {
                Product = product
            });

            if (!response.Success)
            {
                throw new ObymobiException(ProductSaveResult.RemoveBrandProductFromProductFailed, response.Message);
            }
        }

        private void ResetBrandInheritPropertiesOnProduct(ProductEntity product)
        {
            product.InheritName = true;
            product.InheritPrice = true;
            product.InheritButtonText = true;
            product.InheritCustomizeButtonText = true;
            product.InheritWebTypeTabletUrl = true;
            product.InheritWebTypeSmartphoneUrl = true;
            product.InheritDescription = true;
            product.InheritColor = true;
            product.InheritAlterationsFromBrand = true;
            product.InheritAttachmentsFromBrand = true;
            product.InheritMedia = true;
        }

        private void ValidateProductName(ProductEntity product)
        {
            if (product.Name.Length == 0 && !product.GenericproductId.HasValue && !product.BrandProductId.HasValue)
            {
                throw new ObymobiException(ProductSaveResult.NameEmpty);
            }

            if (PathUtil.ContainsInvalidPathChars(product.Name))
            {
                throw new ObymobiException(ProductSaveResult.NameInvalidCharacters, "Product: {0}", product.Name);
            }
        }

        private void ValidatePosProductIdChange(ProductEntity product)
        {
            if (!product.PosproductId.HasValue)
            {
                return;
            }

            ValidatePosProductBelongsToProductCompany(product);
            ValidatePosProductIsNotAlreadyLinked(product);

            PosHelperWrapper.ProductValidateEntityBeforeSave(product);
        }

        private void ValidatePosProductBelongsToProductCompany(ProductEntity product)
        {
            if (product.CompanyId != product.PosproductEntity.CompanyId)
            {
                throw new ObymobiException(ProductSaveResult.LinkedToPosproductOfOtherCompany);
            }
        }

        private void ValidatePosProductIsNotAlreadyLinked(ProductEntity product)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.PosproductId == product.PosproductId.Value);
            if (!product.IsNew)
                filter.Add(ProductFields.ProductId != product.ProductId);

            ProductCollection products = new ProductCollection();
            if (products.GetDbCount(filter) > 0)
            {
                throw new ObymobiEntityException(ProductSaveResult.TriedToSaveMultipleProductsForOnePosProductId, product);
            }
        }

        private void BrandProductIdChanged(ProductEntity product)
        {
            if (product.BrandProductId.HasValue)
            {
                this.ResetBrandInheritPropertiesOnProduct(product);

                if (!product.ValidatorPreventBrandProductUpdate)
                {
                    this.UpdateProductWithBrandProduct(product, product.BrandProductEntity);
                }
            }
            else
            {
                this.RemoveBrandProductFromProduct(product);
            }
        }

        private void GenericProductIdChanged(ProductEntity product)
        {
            if (product.GenericproductId.HasValue)
            {
                product.GenericProductChangedUTC = DateTime.UtcNow;
                product.ManualDescriptionEnabled = !StringUtil.IsNullOrWhiteSpace(product.Description);
            }
            else
            {
                product.GenericProductChangedUTC = null;
                product.ManualDescriptionEnabled = false;
            }
        }

        private void ExternalProductIdChanged(ProductEntity product)
        {
            if (product.ExternalProductId.HasValue)
            {
                product.PriceIn = product.ExternalProductEntity.Price;
                product.IsAvailable = product.ExternalProductEntity.IsEnabled;
                product.ExternalIdentifier = product.ExternalProductEntity.Id;
            }
        }

        private void TaxTariffIdChanged(ProductEntity product)
        {
            if (product.TaxTariffId.HasValue)
            {
                product.VattariffPercentage = product.VattariffEntity.Percentage;
            }
        }

        private void SubTypeChanged(ProductEntity product)
        {
            if (product.WebLinkType.HasValue)
            { return; }

            switch (product.SubTypeAsEnum)
            {
                case ProductSubType.WebLink:
                    product.WebLinkType = WebLinkType.Button;
                    break;
            }
        }
    }
}

