﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(DeliverypointgroupEntertainmentEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class DeliverypointgroupEntertainmentEntityValidator : GeneralValidator
    {
        public enum DeliverypointgroupEntertainmentValidatorResult
        {
            EntertainmentBelongsToOtherCompany = 200,
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            DeliverypointgroupEntertainmentEntity useableEntity = involvedEntity as DeliverypointgroupEntertainmentEntity;

            // Check if the Entertainment belongs to the selected Company or is generic.
            if (useableEntity.Fields[DeliverypointgroupEntertainmentFields.DeliverypointgroupId.Name].IsChanged ||
                useableEntity.Fields[DeliverypointgroupEntertainmentFields.EntertainmentId.Name].IsChanged)
            {
                if (useableEntity.EntertainmentEntity.CompanyId.HasValue &&
                    useableEntity.DeliverypointgroupEntity.CompanyId != useableEntity.EntertainmentEntity.CompanyId)
                {
                    throw new ObymobiEntityException(DeliverypointgroupEntertainmentValidatorResult.EntertainmentBelongsToOtherCompany, useableEntity);
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

