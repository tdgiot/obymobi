﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ProductSuggestionEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ProductSuggestionValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            ProductSuggestionEntity useableEntity = involvedEntity as ProductSuggestionEntity;

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            ProductSuggestionEntity useableEntity = involvedEntity as ProductSuggestionEntity;

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

