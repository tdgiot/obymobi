﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(TerminalStateEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class TerminalStateValidator : GeneralValidator
    {
        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            TerminalStateEntity useableEntity = involvedEntity as TerminalStateEntity;

            // Set the new values of the terminal state to the terminal entity            
            TerminalEntity terminalEntity = useableEntity.TerminalEntity;
            terminalEntity.LastStateOnline = useableEntity.Online;
            terminalEntity.LastStateOperationMode = useableEntity.OperationMode;
            terminalEntity.Save();
            
            base.ValidateEntityAfterSave(involvedEntity);
        }
    }
}

