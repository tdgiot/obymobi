﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Crave.Api.Logic;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AmenityEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AmenityValidator : GeneralValidator
    {
        public enum AmenityValidatorResult
        {
            NoNameSpecified = 200,
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AmenityEntity useableEntity = involvedEntity as AmenityEntity;

            if (useableEntity.IsDirty)
                useableEntity.LastModifiedUTC = DateTime.UtcNow;

            if (useableEntity.Name.IsNullOrWhiteSpace())
                throw new ObymobiException(AmenityValidatorResult.NoNameSpecified);

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            AmenityEntity useableEntity = involvedEntity as AmenityEntity;

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

