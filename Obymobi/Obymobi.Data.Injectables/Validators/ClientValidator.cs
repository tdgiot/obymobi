﻿using System;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.Comet;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ClientEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ClientValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            ClientEntity useableEntity = involvedEntity as ClientEntity;

            // Client must be connected to a company
            if (useableEntity.CompanyId <= 0)
                throw new ObymobiEntityException(ClientSaveResult.CompanyIdEmpty, useableEntity);

            ClientCollection clients = new ClientCollection();
            clients.AddToTransaction(useableEntity);

            // Only one device
            if (useableEntity.Fields[ClientFields.DeviceId.Name].IsChanged &&
                useableEntity.DeviceId.HasValue)
            {
                PredicateExpression filterDevices = new PredicateExpression();
                filterDevices.Add(ClientFields.DeviceId == useableEntity.DeviceId);
                if (!useableEntity.IsNew)
                    filterDevices.Add(ClientFields.ClientId != useableEntity.ClientId);

                if (clients.GetDbCount(filterDevices) > 0)
                    throw new ObymobiException(ClientSaveResult.MultipleClientsForDevice);

                TerminalCollection terminals = new TerminalCollection();
                terminals.AddToTransaction(useableEntity);
                PredicateExpression filterTerminals = new PredicateExpression();
                filterTerminals.Add(TerminalFields.DeviceId == useableEntity.DeviceId);

                if (terminals.GetDbCount(filterTerminals) > 0)
                    throw new ObymobiException(ClientSaveResult.DeviceAlreadyConnectToTerminal);
            }

            // On change of Device - Log the change & Remove the deliverypointID and number, since we're switchting physical device - don't want to take those settings
            if (!useableEntity.IsNew && useableEntity.Fields[ClientFields.DeviceId.Name].IsChanged)
            {
                if (!useableEntity.ValidatorOverwriteDontLogDeviceChange)
                {
                    var log = ClientHelper.CreateLogForDeviceChange(useableEntity);
                    log.AddToTransaction(useableEntity);
                    log.Save();
                }
            }

            // Log changes of the DeliverypointId
            if (!useableEntity.IsNew && useableEntity.Fields[ClientFields.DeliverypointId.Name].IsChanged)
            {
                ClientLogEntity log = new ClientLogEntity();
                log.AddToTransaction(useableEntity);
                log.TypeEnum = ClientLogType.DeliverypointChanged;
                log.ClientId = useableEntity.ClientId;
                log.ParentCompanyId = useableEntity.CompanyId;

                DeliverypointCollection deliverypoints = new DeliverypointCollection();
                deliverypoints.AddToTransaction(deliverypoints);
                DeliverypointEntity previousDeliverypoint = null;
                DeliverypointEntity newDeliverypoint = null;

                if (useableEntity.Fields[ClientFields.DeliverypointId.Name].DbValue != null && useableEntity.Fields[ClientFields.DeliverypointId.Name].DbValue != DBNull.Value)
                {
                    int previousDeliverypointId = (int)useableEntity.Fields[ClientFields.DeliverypointId.Name].DbValue;
                    PredicateExpression filter = new PredicateExpression(DeliverypointFields.DeliverypointId == previousDeliverypointId);
                    previousDeliverypoint = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<DeliverypointEntity>(filter, deliverypoints);
                }

                if (useableEntity.DeliverypointId.HasValue && !useableEntity.DeliverypointEntity.IsNew)
                {
                    newDeliverypoint = useableEntity.DeliverypointEntity;
                }

                if (previousDeliverypoint != null && newDeliverypoint != null)
                {
                    // Moved from one to another
                    log.Message = StringUtil.FormatSafe("From Deliverypoint: '{0}' (Id: '{1}') to '{2}' (Id: '{3}').\r\n\r\n{4}",
                        previousDeliverypoint.Number, previousDeliverypoint.DeliverypointId, newDeliverypoint.Number, newDeliverypoint.DeliverypointId, Environment.StackTrace);
                }
                else if (previousDeliverypoint == null && newDeliverypoint != null)
                {
                    // Assigned from nothing to somnething
                    log.Message = StringUtil.FormatSafe("From no Deliverypoint to '{0}' (Id: '{1}').\r\n\r\n{2}",
                        newDeliverypoint.Number, newDeliverypoint.DeliverypointId, Environment.StackTrace);
                }
                else if (previousDeliverypoint != null && newDeliverypoint == null)
                {
                    // Assigned from something to nothing.
                    log.Message = StringUtil.FormatSafe("From Deliverypoint: '{0}' (Id: '{1}') to nothing.\r\n\r\n{2}",
                        previousDeliverypoint.Number, previousDeliverypoint.DeliverypointId, Environment.StackTrace);
                }

                log.Save();

                if (useableEntity.DeliverypointId.HasValue && useableEntity.CompanyEntity.LinkDefaultRoomControlArea)
                {
                    DeliverypointEntity deliverypointEntity = new DeliverypointEntity();
                    deliverypointEntity.AddToTransaction(useableEntity);
                    deliverypointEntity.FetchUsingPK(useableEntity.DeliverypointId.Value);

                    if (!deliverypointEntity.IsNew && deliverypointEntity.RoomControlConfigurationId.HasValue)
                    {
                        string deliverypointName = deliverypointEntity.Name;
                        if (!deliverypointName.IsNullOrWhiteSpace())
                        {
                            string areaName;

                            if (deliverypointName.Contains(" BD")) // Board Room
                                areaName = "Board Room";
                            else if (deliverypointName.Contains(" BR")) // Bedroom Right
                                areaName = "Bedroom Right";
                            else if (deliverypointName.Contains(" BL")) // Bedroom Left
                                areaName = "Bedroom Left";
                            else if (deliverypointName.Contains(" L")) // Living, Living Room
                                areaName = "Living";
                            else if (deliverypointName.Contains(" B")) // Bedroom
                                areaName = "Bedroom";
                            else if (deliverypointName.Contains(" D")) // Dining
                                areaName = "Dining";
                            else // Bedroom
                                areaName = "Bedroom";

                            foreach (RoomControlAreaEntity roomControlAreaEntity in deliverypointEntity.RoomControlConfigurationEntity.RoomControlAreaCollection)
                            {
                                if (roomControlAreaEntity.Name.StartsWith(areaName))
                                {
                                    if (!useableEntity.RoomControlAreaId.HasValue || useableEntity.RoomControlAreaId.Value != roomControlAreaEntity.RoomControlAreaId)
                                    {
                                        useableEntity.RoomControlAreaId = roomControlAreaEntity.RoomControlAreaId;

                                        ClientLogEntity clientLogEntity = new ClientLogEntity();
                                        clientLogEntity.AddToTransaction(useableEntity);
                                        clientLogEntity.TypeEnum = ClientLogType.DeliverypointChanged;
                                        clientLogEntity.ClientId = useableEntity.ClientId;
                                        clientLogEntity.ParentCompanyId = useableEntity.CompanyId;
                                        clientLogEntity.Message = string.Format("Assigned room control area '{0} (ID: {1})' to client.", roomControlAreaEntity.Name, roomControlAreaEntity.RoomControlAreaId);
                                        clientLogEntity.Save();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (useableEntity.Fields[ClientFields.DeliverypointGroupId.Name].IsChanged &&
                useableEntity.DeliverypointGroupId.HasValue)
            {
                if (useableEntity.DeliverypointgroupEntity.CompanyId != useableEntity.CompanyId)
                    throw new ObymobiEntityException(ClientSaveResult.DeliverypointgroupDoesNotBelongToCompanyOfClient, useableEntity);
            }

            // If both Deliverypoint & Deliverypointgroup are set, make sure those match
            if ((useableEntity.Fields[ClientFields.DeliverypointId.Name].IsChanged || useableEntity.Fields[ClientFields.DeliverypointGroupId.Name].IsChanged) &&
                useableEntity.DeliverypointId.HasValue && !useableEntity.DeliverypointgroupEntity.IsNew)                
            {
                if (useableEntity.DeliverypointEntity.DeliverypointgroupId != useableEntity.DeliverypointGroupId)
                    throw new ObymobiEntityException(ClientSaveResult.DeliverypointAndDeliverypointgroupDontMatch, useableEntity);

                if (useableEntity.DeliverypointEntity.CompanyId != useableEntity.CompanyId)
                    throw new ObymobiEntityException(ClientSaveResult.DeliverypointDoesNotBelongToCompanyOfClient, useableEntity);
            }

            // Indicate if we need to send stuff via Comet that was changed
            useableEntity.UpdateOperationModeViaComet = (!useableEntity.IsNew && useableEntity.Fields[ClientFields.OperationMode.Name].IsChanged);
            useableEntity.UpdateDeliverypointIdViaComet = (!useableEntity.IsNew && useableEntity.Fields[ClientFields.DeliverypointId.Name].IsChanged);

            // Reset status of all clients netmessages when deliverypoint or deliverypointgroup has changed
            if (!useableEntity.IsNew)
            {
                bool resetNetmessages = false;
                PredicateExpression netmessageFilter = new PredicateExpression(NetmessageFields.ReceiverClientId == useableEntity.ClientId);
                netmessageFilter.AddWithAnd(NetmessageFields.Status == (int)NetmessageStatus.WaitingForVerification);

                // Make sure the Client is connected to a Deliverypointgroup of the same company            
                if (useableEntity.DeliverypointGroupId.HasValue &&
                    useableEntity.Fields[ClientFields.DeliverypointGroupId.Name].IsChanged)
                {
                    // Deliverypointgorup is changed. Reset all messages send to deliverypointgroup or deliverypoint.
                    resetNetmessages = true;

                    var subFilter = new PredicateExpression(NetmessageFields.ReceiverDeliverypointgroupId == (int)useableEntity.Fields[ClientFields.DeliverypointGroupId.Name].DbValue);
                    subFilter.AddWithOr(NetmessageFields.ReceiverDeliverypointId > 0);
                    netmessageFilter.AddWithAnd(subFilter);
                }
                else if (useableEntity.DeliverypointId.HasValue &&
                    useableEntity.Fields[ClientFields.DeliverypointId.Name].IsChanged)
                {
                    // Deliverypoint is changed. Reset all messages send to deliverypoint
                    resetNetmessages = true;

                    netmessageFilter.AddWithAnd(NetmessageFields.ReceiverDeliverypointId == (int)useableEntity.Fields[ClientFields.DeliverypointGroupId.Name].DbValue);
                }

                if (resetNetmessages)
                {
                    // Set status of all messages matching the filter
                    var netmessageUpdate = new NetmessageEntity();
                    netmessageUpdate.StatusAsEnum = NetmessageStatus.CancelledNewConfiguration;

                    var netmessageCollection = new NetmessageCollection();
                    netmessageCollection.AddToTransaction(useableEntity);
                    netmessageCollection.UpdateMulti(netmessageUpdate, netmessageFilter);
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            ClientEntity useableEntity = involvedEntity as ClientEntity;

            // Send NetMessage about ClientOperationMode
            if (useableEntity.UpdateOperationModeViaComet)
                CometHelper.SetClientOperationMode(useableEntity.ClientId, useableEntity.OperationModeEnum, useableEntity.GetCurrentTransaction());

            if (useableEntity.UpdateDeliverypointIdViaComet)
                CometHelper.SetDeliverypointId(useableEntity.ClientId, useableEntity.DeliverypointId.GetValueOrDefault(0), useableEntity.DeliverypointGroupId.GetValueOrDefault(0), string.Empty, useableEntity.GetCurrentTransaction());

            base.ValidateEntityAfterSave(involvedEntity);
        }
    }
}

