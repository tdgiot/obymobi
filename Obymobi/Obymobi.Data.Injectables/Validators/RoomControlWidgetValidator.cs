﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using System.Linq;
using Obymobi.Enums;
using Dionysos;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(RoomControlWidgetEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class RoomControlWidgetValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            RoomControlWidgetEntity useableEntity = involvedEntity as RoomControlWidgetEntity;
            useableEntity.CaptionChanged = useableEntity.Fields[RoomControlWidgetFields.Caption.Name].IsChanged;

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            RoomControlWidgetEntity useableEntity = involvedEntity as RoomControlWidgetEntity;

            if (useableEntity.ValidatorCreateOrUpdateDefaultWidgetLanguage && useableEntity.CaptionChanged)
            {
                this.UpdateDefaultCustomText(useableEntity);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        private void UpdateDefaultCustomText(RoomControlWidgetEntity roomControlWidgetEntity)
        {
            string companyCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            if (!companyCultureCode.IsNullOrWhiteSpace())
            {
                CustomTextEntity customText = roomControlWidgetEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCultureCode && x.Type == CustomTextType.RoomControlWidgetCaption);
                if (customText == null)
                {
                    customText = CustomTextHelper.CreateEntity(CustomTextType.RoomControlWidgetCaption, companyCultureCode, roomControlWidgetEntity.Caption, RoomControlWidgetFields.RoomControlWidgetId, roomControlWidgetEntity.RoomControlWidgetId);
                }
                customText.Text = roomControlWidgetEntity.Caption;
                customText.Save();
            }
        }
    }
}

