﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PosdeliverypointgroupEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PosdeliverypointgroupValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            PosdeliverypointgroupEntity useableEntity = involvedEntity as PosdeliverypointgroupEntity;

            foreach (var deliverypointgroup in useableEntity.DeliverypointgroupCollection)
                deliverypointgroup.Delete();

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}
