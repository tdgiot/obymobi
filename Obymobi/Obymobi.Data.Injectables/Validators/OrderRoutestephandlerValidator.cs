﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Routing;
using Obymobi.Web.CloudStorage;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(OrderRoutestephandlerEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class OrderRoutestephandlerValidator : GeneralValidator
    {
        public enum OrderRoutestephandlerValidatorResult : int
        {
            TransactionRequiredForSaveActions = 200,
            OnlyOneAutomaticHandlerAllowedPerOrderRoutestep = 201,
            HandlerTypeAndTerminalIncompatible = 202,
            NoTerminalSpecifiedForOrderRoutestep = 203
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            OrderRoutestephandlerEntity useableEntity = involvedEntity as OrderRoutestephandlerEntity;

            // Require the action to be part of a transaction > to be sure a next step is always activated after
            // the previous one has been completed & the order is updated in the correct way.
            if (useableEntity.GetCurrentTransaction() == null)
                throw new ObymobiEntityException(OrderRoutestephandlerValidatorResult.TransactionRequiredForSaveActions, useableEntity);

            // Fill Guid when Empty
            if (useableEntity.Guid.IsNullOrWhiteSpace())
                useableEntity.Guid = Guid.NewGuid().ToString();

            // Prevent multiple AutomaticlyProcessed OrderRoutestephandlers in one step
            OrderRoutestephandlerValidator.PreventMultipleAutomaticallyProcessedHandlersAtStep(useableEntity);

            // Make sure HandlerType and Terminal are compatible
            // Verify that Terminal and HandlerType are compatible
            if (!RoutingHelper.IsRoutestepHandlerTypeCompatibleWithTerminalType(useableEntity.HandlerType.ToEnum<RoutestephandlerType>(), useableEntity.TerminalEntity))
                throw new ObymobiEntityException(OrderRoutestephandlerValidatorResult.HandlerTypeAndTerminalIncompatible, useableEntity);

            // Update any time stamps
            OrderRoutestephandlerValidator.UpdateTimestamps(useableEntity);

            // Update the Order when the first step is started
            OrderRoutestephandlerValidator.UpdateOrderWhenRouteStarted(useableEntity);

            // Update the parallel steps when a step is 'Being Handled'
            if (useableEntity.HandlerTypeAsEnum == RoutestephandlerType.Console &&
                       useableEntity.Status == (int)OrderRoutestephandlerStatus.BeingHandled &&
                       useableEntity.Fields[(int)OrderRoutestephandlerFieldIndex.Status].IsChanged)
            {
                // Check for parallel consoles to update

                if (useableEntity.HasParallelStep)
                    OrderRoutestephandlerValidator.UpdateParallelSteps(useableEntity);
            }

            // Update the current and next steps when a step is completed
            OrderRoutestephandlerValidator.UpdateCurrentAndNextStepsOnComplete(useableEntity);

            // Set Timeout Expiration if not set yet (fall back for first steps that don't get activated in this Validator)
            if (useableEntity.Status > (int)OrderRoutestephandlerStatus.WaitingForPreviousStep)
            {
                // Set Timeout for Completion
                if(!useableEntity.TimeoutExpiresUTC.HasValue)
                    useableEntity.TimeoutExpiresUTC = useableEntity.Timeout > 0 ? DateTime.UtcNow.AddMinutes(useableEntity.Timeout) : DateTime.MaxValue;

                // Set Notification Timeout for Retrieval            
                if(useableEntity.RetrievalSupportNotificationTimeoutMinutes > 0 && !useableEntity.RetrievalSupportNotificationTimeoutUTC.HasValue)                
                    useableEntity.RetrievalSupportNotificationTimeoutUTC = DateTime.UtcNow.AddMinutes(useableEntity.RetrievalSupportNotificationTimeoutMinutes);                

                // Set Notification Timeout for Handling
                if(useableEntity.BeingHandledSupportNotificationTimeoutMinutes > 0 && !useableEntity.BeingHandledSupportNotificationTimeoutUTC.HasValue)                
                    useableEntity.BeingHandledSupportNotificationTimeoutUTC = DateTime.UtcNow.AddMinutes(useableEntity.BeingHandledSupportNotificationTimeoutMinutes);                
            }                                  

            // Set Error text
            try
            {
                if (useableEntity.ErrorText.IsNullOrWhiteSpace())
                {
                    useableEntity.ErrorText = useableEntity.ErrorCodeAsEnum.ToString();
                }
            }
            catch
            {
                // Catch wrong errors that have no enum value.
                useableEntity.ErrorText = "Unknown Error Code";
            }

            useableEntity.StatusText = useableEntity.StatusAsEnum.ToString();
            useableEntity.HandlerTypeText = useableEntity.HandlerTypeAsEnum.ToString();

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            OrderRoutestephandlerEntity useableEntity = involvedEntity as OrderRoutestephandlerEntity;

            // Notify via Comet
            if (useableEntity.TerminalId.HasValue && useableEntity.StatusAsEnum != OrderRoutestephandlerStatus.WaitingForPreviousStep)
                RoutingHelper.SendOrderroutestepHandlerStatusViaComet(useableEntity);

            ITransaction trans = useableEntity.GetCurrentTransaction();

            // Clean up the Route if it was finished (either completed or failed).            
            OrderRoutestephandlerCollection steps = null;
            if (useableEntity.ValidatorFlagRouteHasFinished)
            {
                // Check if we need to log
                if ((useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Failed ||
                    useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Completed && useableEntity.ErrorCodeAsEnum == OrderProcessingError.ManuallyProcessed) ||
                    useableEntity.LogAlways)
                {
                    OrderEntity order = new OrderEntity();
                    order.AddToTransaction(trans);
                    order.FetchUsingPK(useableEntity.OrderId);

                    // Create Log
                    steps = GetAllStepsOfRoute(useableEntity, trans, steps);
                    order.RoutingLog = RoutingHelper.OrderroutestephandlersToCsv(steps);
                }

                // Clean up the mess
                if (steps == null)
                    steps = OrderRoutestephandlerValidator.GetAllStepsOfRoute(useableEntity, trans, steps);

                foreach (var step in steps)
                {
                    step.AddToTransaction(trans);
                    step.Delete();
                }
            }

            // Check if the next step is a step which is not handled by a terminal, but directly by the webservice
            if (useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.WaitingToBeRetrieved && RoutingHelper.GetNoTerminalRoutestephandlers().Contains(useableEntity.HandlerTypeAsEnum))
            {
                RoutingHelper.HandleNoTerminalRoutingstephandling(useableEntity, (MediaEntity mediaEntity) => Obymobi.Web.CloudStorage.MediaExtensions.Download(mediaEntity));
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        /// <summary>
        /// Updates parallel steps if the status is update by "on the case" button
        /// </summary>
        /// <param name="useableEntity">The useable entity.</param>
        private static void UpdateParallelSteps(OrderRoutestephandlerEntity useableEntity)
        {
            foreach (OrderRoutestephandlerEntity or in useableEntity.OrderEntity.OrderRoutestephandlerCollection)
            {
                if (or.OrderRoutestephandlerId != useableEntity.OrderRoutestephandlerId && or.Number == useableEntity.Number)
                {
                    // The order has been picked up by another console
                    // Remove order from other consoles

                    or.Status = (int)OrderRoutestephandlerStatus.BeingHandledByOtherStep;

                    or.AddToTransaction(useableEntity);
                    or.Save();
                }
            }
        }

        /// <summary>
        /// Prevents the multiple automatically processed handlers at step.
        /// </summary>
        /// <param name="useableEntity">The useable entity.</param>
        private static void PreventMultipleAutomaticallyProcessedHandlersAtStep(OrderRoutestephandlerEntity useableEntity)
        {
            OrderRoutestephandlerCollection orderroutestephandlers = new OrderRoutestephandlerCollection();
            orderroutestephandlers.AddToTransaction(useableEntity);

            // Check for multiple automatic handlers in 1 step.
            List<int> automaticallyHandledHandlers = RoutingHelper.GetAutomaticlyHandledRoutestephandlers().Select(x => (int)x).ToList();
            PredicateExpression filterAutomaticHandlersForStep = new PredicateExpression();
            filterAutomaticHandlersForStep.Add(OrderRoutestephandlerFields.OrderId == useableEntity.OrderId);
            filterAutomaticHandlersForStep.Add(OrderRoutestephandlerFields.Number == useableEntity.Number);
            filterAutomaticHandlersForStep.Add(OrderRoutestephandlerFields.HandlerType == automaticallyHandledHandlers);
            if (!useableEntity.IsNew)
                filterAutomaticHandlersForStep.Add(OrderRoutestephandlerFields.OrderRoutestephandlerId != useableEntity.OrderRoutestephandlerId);

            if (orderroutestephandlers.GetDbCount(filterAutomaticHandlersForStep) > 0)
                throw new ObymobiEntityException(OrderRoutestephandlerValidatorResult.OnlyOneAutomaticHandlerAllowedPerOrderRoutestep, useableEntity);
        }

        /// <summary>
        /// Updates the order when route started.
        /// </summary>
        /// <param name="useableEntity">The useable entity.</param>
        private static void UpdateOrderWhenRouteStarted(OrderRoutestephandlerEntity useableEntity)
        {
            // Check if it's a first step, the status is changed, has a value and is actually started (not waiting for previous step)
            if (useableEntity.Number == RoutingHelper.FIRST_STEP_NUMBER &&
                useableEntity.Fields[(int)OrderRoutestephandlerFieldIndex.Status].IsChanged &&
                useableEntity.StatusAsEnum != OrderRoutestephandlerStatus.WaitingForPreviousStep)
            {
                // Entity might have been loaded without OrderEntity
                if (useableEntity.OrderEntity == null)
                    useableEntity.GetSingleOrderEntity(true);
                OrderHelper.UpdateOrderStatus(useableEntity.OrderEntity, Enums.OrderStatus.InRoute);
            }
        }

        /// <summary>
        /// Updates the current and next steps.
        /// The current (parallel) steps are all being completed
        /// The next step(s) are being promoted to 'WaitingToBeHandled'
        /// </summary>
        /// <param name="useableEntity">The useable entity.</param>
        private static void UpdateCurrentAndNextStepsOnComplete(OrderRoutestephandlerEntity useableEntity)
        {
            if (useableEntity.ValidatorFlagBeingPartofUpdateBecauseOfCompletionOfAStep)
            {
                // Skip.
            }
            // Check if the status changed to Completed or Failed, which means actions have to be taken.
            else if (useableEntity.Fields[(int)OrderRoutestephandlerFieldIndex.Status].IsChanged &&
                (useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Completed || useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Failed))
            {
                OrderRoutestephandlerCollection orderroutestephandlers = new OrderRoutestephandlerCollection();
                orderroutestephandlers.AddToTransaction(useableEntity);

                PredicateExpression filter = new PredicateExpression();

                // For this order 
                filter.Add(OrderRoutestephandlerFields.OrderId == useableEntity.OrderId);

                // Determine filter of relevant steps to update                
                if ((useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Failed && !useableEntity.ContinueOnFailure) ||
                    (useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Completed && useableEntity.ErrorCodeAsEnum == OrderProcessingError.ManuallyProcessed))
                {
                    // Failed, so cancel all paralele and future steps
                    // Or manually processed because the current step failed, so parallel and future steps needs to be cancelled to
                    filter.Add(OrderRoutestephandlerFields.Number == useableEntity.Number | OrderRoutestephandlerFields.Number > useableEntity.Number);
                }
                else if (useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Completed && useableEntity.CompleteRouteOnComplete)
                {
                    // Completed AND CompleteRouteOnComplete, the this route step being completed means we can regard the whole route as 
                    // being completed, so complete all paralele and future steps
                    filter.Add(OrderRoutestephandlerFields.Number == useableEntity.Number | OrderRoutestephandlerFields.Number > useableEntity.Number);
                }
                else
                {
                    // Completed OR Failed but step is set to 'ContinueOnFailure' (ContinueOnFailure means, ignore failure, just behave like it is completed)
                    // Update only at current step Number (parallel) or the next step
                    filter.Add(OrderRoutestephandlerFields.Number == useableEntity.Number | OrderRoutestephandlerFields.Number == (useableEntity.Number + 1));
                }

                // Don't load self
                filter.Add(OrderRoutestephandlerFields.OrderRoutestephandlerId != useableEntity.OrderRoutestephandlerId);

                SortExpression sort = new SortExpression();
                sort.Add(OrderRoutestephandlerFields.Number | SortOperator.Ascending);
                orderroutestephandlers.GetMulti(filter, 0, sort);

                // In case of multiple handlers for one step (parallel) don't act on a failed step until
                // all the steps have failed.
                var parallelSteps = orderroutestephandlers.Where(sh => sh.Number == useableEntity.Number);
                if (useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Failed && // The step failed
                    parallelSteps.Count() > 0 && // We have parallel steps
                    // If any of the parallel steps are still up for processing, don't act on this failure
                    // Check if at least one is still being handled
                    (
                        parallelSteps.Any(sh => sh.StatusAsEnum == OrderRoutestephandlerStatus.WaitingToBeRetrieved) ||
                        parallelSteps.Any(sh => sh.StatusAsEnum == OrderRoutestephandlerStatus.RetrievedByHandler) ||
                        parallelSteps.Any(sh => sh.StatusAsEnum == OrderRoutestephandlerStatus.BeingHandled)

                    ))
                {
                    // Don't act on this failed step since paralel steps might still succeed.
                }
                else
                {
                    // This handler succeeded OR
                    // it is a single handler in a step and if failed OR
                    // all other parallel handlers of this step already failed
                    OrderRoutestephandlerValidator.ProcessStepCompletionOrFailure(useableEntity, orderroutestephandlers);
                }
            }
        }

        private static void ProcessStepCompletionOrFailure(OrderRoutestephandlerEntity useableEntity, OrderRoutestephandlerCollection orderroutestephandlers)
        {
            bool hasNextStep = false;
            // Update all parallel and future steps
            TerminalCollection terminals = new TerminalCollection();
            terminals.AddToTransaction(useableEntity);
            foreach (var step in orderroutestephandlers)
            {
                // Prevent the logic of this validator to be applied to saves as a result of this logic:
                step.ValidatorFlagBeingPartofUpdateBecauseOfCompletionOfAStep = true;

                if (useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Completed && useableEntity.CompleteRouteOnComplete)
                {
                    // This step will complete the whole order, so all steps will be set to completeed
                    step.Status = (int)OrderRoutestephandlerStatus.CompletedByOtherStep;
                    step.CompletedUTC = useableEntity.CompletedUTC;
                }
                else if (useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Completed ||
                     (useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Failed && useableEntity.ContinueOnFailure))
                {
                    if (useableEntity.ErrorCodeAsEnum == OrderProcessingError.ManuallyProcessed)
                    {
                        // The current step is beeing manually processed
                        // So that means the order failed because of this step
                        step.Status = (int)OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure;
                    }
                    else
                    {
                        // Completed OR Failed but step is set to 'ContinueOfFailure', both have same handling.

                        // Update current & next steps.
                        if (step.Number == useableEntity.Number)
                        {
                            // Same level, complete now
                            step.Status = (int)OrderRoutestephandlerStatus.CompletedByOtherStep;
                            step.CompletedUTC = useableEntity.CompletedUTC;
                        }
                        else
                        {
                            // Next step, is now current, so waiting to be retrieved
                            step.Status = (int)OrderRoutestephandlerStatus.WaitingToBeRetrieved;
                            hasNextStep = true;

                            // Set the correct terminal, since it might be forwarded
                            RoutingHelper.SetForwardedTerminal(step);
                        }

                        // Set the timeout expires time if there's a timeout
                        step.TimeoutExpiresUTC = step.Timeout > 0 ? DateTime.UtcNow.AddMinutes(step.Timeout) : DateTime.MaxValue;
                    }
                }
                else
                {
                    // Cancel all steps
                    step.StatusAsEnum = OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure;
                }

                step.AddToTransaction(useableEntity);
                step.Save();
            }

            // Check if we should mark the orders as 'Processed'
            // If this step completed and there's no next step, complete the order.
            // Or this step is completed AND it's set to 'CompleteRouteOnComplete'                
            // Or this step is completed through manually processing with an error code so the other steps failed because of this                
            if ((useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Completed && !hasNextStep) ||
                (useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Completed && useableEntity.CompleteRouteOnComplete) ||
                useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Completed && useableEntity.ErrorCodeAsEnum == OrderProcessingError.ManuallyProcessed)
            {
                // Set the flag that the route has finished
                useableEntity.ValidatorFlagRouteHasFinished = true;

                // The order is now completed                    
                // GK Fix below is kind of dirty, why is the orderentiy empty.
                if (useableEntity.OrderEntity == null)
                    useableEntity.OrderEntity = new OrderEntity(useableEntity.OrderId);
                OrderHelper.UpdateOrderStatus(useableEntity.OrderEntity, Enums.OrderStatus.Processed);
            }

            // Check if we should mark the order as 'Failed' or that we can still escalate
            // If this step failed & it's not set to 'ContinueOnFailure', fail the whole order
            if (useableEntity.StatusAsEnum == OrderRoutestephandlerStatus.Failed && !useableEntity.ContinueOnFailure)
            {
                // Set the flag that the route has finished
                //useableEntity.ValidatorFlagRouteHasFinished = true;
                bool escalated = false;
                if (!useableEntity.EscalationStep && useableEntity.EscalationRouteId.HasValue)
                {
                    // We have a fall-back route!
                    RouteCollection routes = new RouteCollection();
                    routes.AddToTransaction(useableEntity);
                    PredicateExpression filterEscalationRoute = new PredicateExpression();
                    filterEscalationRoute.Add(RouteFields.RouteId == useableEntity.EscalationRouteId.Value);
                    var escalationRoute = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<RouteEntity>(filterEscalationRoute, routes);

                    // Transaction will be retrieved from order in sub method of method below to write route, so make sure it's part of the transaction
                    useableEntity.OrderEntity.AddToTransaction(useableEntity);

                    RoutingHelper.WriteEscalationRouteForOrder(useableEntity.OrderEntity, escalationRoute);

                    escalated = true;
                }

                // Mark order as unproccesed when we weren't able to escalate the order
                if (!escalated)
                {
                    useableEntity.OrderEntity.ErrorCode = useableEntity.ErrorCode;
                    OrderHelper.UpdateOrderStatus(useableEntity.OrderEntity, Enums.OrderStatus.Unprocessable);
                }
            }
        }


        private static OrderRoutestephandlerCollection GetAllStepsOfRoute(OrderRoutestephandlerEntity useableEntity, ITransaction trans, OrderRoutestephandlerCollection steps)
        {
            steps = new OrderRoutestephandlerCollection();
            steps.AddToTransaction(trans);
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderRoutestephandlerFields.OrderId == useableEntity.OrderId);
            SortExpression sort = new SortExpression();
            sort.Add(OrderRoutestephandlerFields.Number | SortOperator.Ascending);

            steps.GetMulti(filter, 0, sort);
            return steps;
        }

        /// <summary>
        /// Updates the timestamps.
        /// </summary>
        /// <param name="useableEntity">The useable entity.</param>
        private static void UpdateTimestamps(OrderRoutestephandlerEntity useableEntity)
        {
            if (useableEntity.Fields[(int)OrderRoutestephandlerFieldIndex.Status].IsChanged)
            {
                switch (useableEntity.StatusAsEnum)
                {
                    case OrderRoutestephandlerStatus.WaitingForPreviousStep:
                        // Nothing to timestamp
                        break;
                    case OrderRoutestephandlerStatus.WaitingToBeRetrieved:
                        useableEntity.WaitingToBeRetrievedUTC = DateTime.UtcNow;
                        break;
                    case OrderRoutestephandlerStatus.RetrievedByHandler:
                        useableEntity.RetrievedByHandlerUTC = DateTime.UtcNow;
                        break;
                    case OrderRoutestephandlerStatus.BeingHandled:
                        useableEntity.BeingHandledUTC = DateTime.UtcNow;
                        break;
                    case OrderRoutestephandlerStatus.Completed:
                    case OrderRoutestephandlerStatus.CompletedByOtherStep:
                    case OrderRoutestephandlerStatus.CancelledDueToOtherStepFailure:
                    case OrderRoutestephandlerStatus.Failed:
                        useableEntity.CompletedUTC = DateTime.UtcNow;
                        break;
                    default:
                        break;
                }
            }
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            // First save the step in another table before deleting it
            OrderRoutestephandlerEntity useableEntity = involvedEntity as OrderRoutestephandlerEntity;

            ITransaction trans = useableEntity.GetCurrentTransaction();

            OrderRoutestephandlerHistoryEntity toStep = new OrderRoutestephandlerHistoryEntity();
            LLBLGenEntityUtil.CopyFields(useableEntity, toStep, null);
            toStep.AddToTransaction(trans);
            toStep.Save();

            base.ValidateEntityBeforeDelete(involvedEntity);
        }

    }
}


