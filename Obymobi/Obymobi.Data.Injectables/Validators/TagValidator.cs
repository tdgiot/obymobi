﻿using System;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(TagEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class TagValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            TagEntity useableEntity = involvedEntity as TagEntity;

            useableEntity.Name = TagHelper.FormatTagName(useableEntity.Name);

            if (useableEntity.IsNew
                || useableEntity.Fields[TagFields.CompanyId.Name].IsChanged
                || useableEntity.Fields[TagFields.Name.Name].IsChanged)
            {
                ValidateTagIsUnique(useableEntity);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private static void ValidateTagIsUnique(TagEntity useableEntity)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(TagFields.Name == useableEntity.Name);

            PredicateExpression companyFilter = new PredicateExpression();
            companyFilter.Add(TagFields.CompanyId == DBNull.Value);
            if (useableEntity.CompanyId.HasValue)
            {
                companyFilter.AddWithOr(TagFields.CompanyId == useableEntity.CompanyId);
            }

            filter.AddWithAnd(companyFilter);

            TagCollection tagCollection = new TagCollection();
            tagCollection.GetMulti(filter, null, 1);
            if (tagCollection.Count > 0)
            {
                string tagType = tagCollection[0].CompanyId.HasValue ? "Company" : "System";
                
                throw new EntitySaveException($"{tagType} tag with name '{useableEntity.Name}' already exists.");
            }
        }
    }
}

