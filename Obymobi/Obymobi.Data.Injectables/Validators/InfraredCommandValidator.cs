﻿using System;
using Dionysos;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(InfraredCommandEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class InfraredCommandValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            InfraredCommandEntity useableEntity = involvedEntity as InfraredCommandEntity;

            if (useableEntity.Name.IsNullOrWhiteSpace() && useableEntity.Type != InfraredCommandType.Custom)
            {
                useableEntity.Name = useableEntity.Type.GetStringValue();
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

