﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Crave.Domain.UseCases;
using Crave.Domain.UseCases.Requests;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ProductAlterationEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ProductAlterationValidator : GeneralValidator
    {
        private readonly SaveProductAlterationForBrandProductUseCase saveProductAlterationForBrandProductUseCase;
        private readonly DeleteProductAlterationForBrandProductUseCase deleteProductAlterationForBrandProductUseCase;

        public ProductAlterationValidator()
        {
            this.saveProductAlterationForBrandProductUseCase = new SaveProductAlterationForBrandProductUseCase();
            this.deleteProductAlterationForBrandProductUseCase = new DeleteProductAlterationForBrandProductUseCase();
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            ProductAlterationEntity useableEntity = involvedEntity as ProductAlterationEntity;

            // Verify that this Alteration is ready for the POS
            foreach (var alterationItem in useableEntity.AlterationEntity.AlterationitemCollection)
            {
                if (alterationItem.AlterationoptionEntity.PosproductId.HasValue &&
                    alterationItem.AlterationoptionEntity.PosproductEntity.ProductCollection.Count != 1)
                {
                    // An alterationoption that can't be processed by the POS, therefore prevent it from adding
                    throw new ObymobiException(ProductAlterationSaveResult.AlterationNotReadyForPos, "AlterationId: {0}", useableEntity.AlterationId);
                }
            }

            // Reactivate the related POS item
            if (useableEntity.PosproductPosalterationId.HasValue)
            {
                // Get the current productAlteration from the DB
                // And check if there is another alteration chosen to overwrite the current one

                // Check if the productAlteration already exists
                if (useableEntity.Fields[(int)ProductAlterationFieldIndex.AlterationId].CurrentValue != null &&
                    (int)useableEntity.Fields[(int)ProductAlterationFieldIndex.AlterationId].CurrentValue != useableEntity.AlterationId)
                {
                    // Check if the alteration that is currently set is required.
                    // If so, we are NOT gonna change the productAlteration to avoid POS problems
                    if (useableEntity.AlterationEntity.MinOptions > 0)
                    {
                        // This is a required choice
                        throw new ObymobiEntityException(ProductAlterationSaveResult.CantChangeAlterationFromProductWhenRequiredInPos, useableEntity);
                    }
                }
                else
                {
                    // New alteration that will be attached to this product.
                    // Check if this alteration isn't already attached
                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(ProductAlterationFields.ProductId == useableEntity.ProductId);
                    filter.Add(ProductAlterationFields.AlterationId == useableEntity.AlterationId);
                    if (!useableEntity.IsNew)
                        filter.Add(ProductAlterationFields.ProductAlterationId != useableEntity.ProductAlterationId);

                    ProductAlterationCollection productAlterationCollection = new ProductAlterationCollection();
                    productAlterationCollection.AddToTransaction(useableEntity);

                    if (productAlterationCollection.GetDbCount(filter) > 0)
                    {
                        throw new ObymobiEntityException(ProductAlterationSaveResult.CantUseAlterationTwice, useableEntity);
                    }
                }

                useableEntity.PosproductPosalterationEntity.AddToTransaction(useableEntity);
                useableEntity.PosproductPosalterationEntity.DeletedFromCms = false;
                useableEntity.PosproductPosalterationEntity.Save();
            }

            if (useableEntity.AlterationEntity.Type == AlterationType.Package && useableEntity.ProductEntity != null && useableEntity.ProductEntity.CompanyId.HasValue)
            {
                CompanyEntity company = useableEntity.ProductEntity.CompanyEntity;
                if (company != null && !company.IsNew && company.AlterationDialogMode == AlterationDialogMode.v1)
                {
                    throw new ObymobiException(ProductAlterationSaveResult.PackageTypeAlterationNotAllowedWithV1AlterationDialogMode);
                }

                // Check if alteration does not have items which are linked back to this product (creating circular dependency)
                PrefetchPath prefetchPath = new PrefetchPath(EntityType.AlterationProductEntity);
                prefetchPath.Add(AlterationProductEntityBase.PrefetchPathAlterationEntity).SubPath
                            .Add(AlterationEntityBase.PrefetchPathParentAlterationEntity).SubPath
                            .Add(AlterationEntityBase.PrefetchPathParentAlterationEntity);
                
                PredicateExpression filter = new PredicateExpression();
                filter.Add(AlterationProductFields.ProductId == useableEntity.ProductId);
                
                AlterationProductCollection alterationProductCollection = new AlterationProductCollection();
                alterationProductCollection.GetMulti(filter, prefetchPath);

                foreach (AlterationProductEntity alterationProductEntity in alterationProductCollection)
                {
                    AlterationEntity rootAlterationEntity = alterationProductEntity.AlterationEntity.TraverseToRootAlterationEntity;
                    if (rootAlterationEntity.AlterationId == useableEntity.AlterationId)
                    {
                        throw new ObymobiException(ProductAlterationSaveResult.ProductAlterationCircularDependencyDetected,
                                                   "Alteration '{0} (ID: {1})' already links to product '{2}', therefor it can not be configured for this product",
                                                   rootAlterationEntity.Name,
                                                   rootAlterationEntity.AlterationId,
                                                   useableEntity.ProductEntity.Name);
                    }
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            ProductAlterationEntity useableEntity = involvedEntity as ProductAlterationEntity;

            if (useableEntity.ProductEntity.BrandId.HasValue)
            {
                this.saveProductAlterationForBrandProductUseCase.Execute(new ProductAlterationForBrandProductRequest
                {
                    BrandProductId = useableEntity.ProductId,
                    ProductAlteration = useableEntity
                });
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            ProductAlterationEntity useableEntity = involvedEntity as ProductAlterationEntity;

            if (!useableEntity.BeingDeletedInPosProductPosAlterationDeletionTransaction)
            {
                // Set related POS Entity to Deleted
                if (useableEntity.PosproductPosalterationId.HasValue)
                {
                    // Check if this is a required option for the POS, in that case, don't allow it to be deleted
                    var posalteration = PosalterationHelper.GetPosalterationByExternalId(useableEntity.ProductEntity.CompanyId.GetValueOrDefault(), useableEntity.PosproductPosalterationEntity.PosalterationExternalId, useableEntity.GetCurrentTransaction());

                    if (posalteration != null)
                    {
                        if (posalteration.MinOptions.HasValue && posalteration.MinOptions > 0)
                        {
                            // This is a required choice
                            throw new ObymobiEntityException(ProductAlterationSaveResult.CantRemoveAlterationFromProductWhenRequiredInPos, useableEntity);
                        }
                    }

                    // Mark this items a deleted to prevent it from being added again on the next sync
                    useableEntity.PosproductPosalterationEntity.AddToTransaction(useableEntity);
                    useableEntity.PosproductPosalterationEntity.DeletedFromCms = true;
                    useableEntity.PosproductPosalterationEntity.Save();
                }
            }

            if (useableEntity.ProductEntity.BrandId.HasValue)
            {
                this.deleteProductAlterationForBrandProductUseCase.Execute(new ProductAlterationForBrandProductRequest
                {
                    BrandProductId = useableEntity.ProductId,
                    ProductAlteration = useableEntity
                });
            }

            base.ValidateEntityBeforeDelete(involvedEntity);
        }        
    }
}

