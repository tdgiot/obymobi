﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AccessCodeCompanyEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AccessCodeCompanyValidator : GeneralValidator
    {
        public enum AccessCodeCompanyValidatorResult
        {
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AccessCodeCompanyEntity useableEntity = involvedEntity as AccessCodeCompanyEntity;

            if (useableEntity.IsDirty)
            {
                useableEntity.AccessCodeEntity.LastModifiedUTC = DateTime.UtcNow;
                useableEntity.AccessCodeEntity.Save();
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            AccessCodeCompanyEntity useableEntity = involvedEntity as AccessCodeCompanyEntity;

            useableEntity.AccessCodeEntity.LastModifiedUTC = DateTime.UtcNow;
            useableEntity.AccessCodeEntity.Save();

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

