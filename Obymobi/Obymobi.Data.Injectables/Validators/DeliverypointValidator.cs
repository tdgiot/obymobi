﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(DeliverypointEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class DeliverypointValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            DeliverypointEntity useableEntity = involvedEntity as DeliverypointEntity;
            useableEntity.ClientConfigurationIdChanged = useableEntity.Fields[DeliverypointFields.ClientConfigurationId.FieldIndex].IsChanged;

            // Field 'Name' must have a value
            if (useableEntity.Name.Length == 0)
                throw new ObymobiEntityException(DeliverypointSaveResult.NameEmpty, useableEntity);

            // Field DeliverypointgroupId is empty
            if (useableEntity.DeliverypointgroupId <= 0)
                throw new ObymobiEntityException(DeliverypointSaveResult.DeliverypointgroupIdEmpty, useableEntity);

            // Check if the related Posdeliverypoint belongs to the same company.
            if (useableEntity.PosdeliverypointId.HasValue &&
                useableEntity.PosdeliverypointEntity.CompanyId != useableEntity.CompanyId)
            {
                throw new ObymobiEntityException(DeliverypointSaveResult.PosdeliverypointDoesntBelongToCompany, useableEntity);
            }

            if (useableEntity.PosdeliverypointId.HasValue)
            {
                PredicateExpression filterUniquePosdeliverypoint = new PredicateExpression();
                filterUniquePosdeliverypoint.Add(DeliverypointFields.PosdeliverypointId == useableEntity.PosdeliverypointId);
                if (!useableEntity.IsNew)
                    filterUniquePosdeliverypoint.Add(DeliverypointFields.DeliverypointId != useableEntity.DeliverypointId);

                DeliverypointCollection deliverypoints = new DeliverypointCollection();
                deliverypoints.AddToTransaction(useableEntity);

                if (deliverypoints.GetDbCount(filterUniquePosdeliverypoint) > 0)
                    throw new ObymobiEntityException(DeliverypointSaveResult.MultipleDeliverypointsForPosdeliverypoint, useableEntity);
            }

            // Check if the deliverypointgroup already has this number
            PredicateExpression filterUniqueDeliverypointNumber = new PredicateExpression();
            filterUniqueDeliverypointNumber.Add(DeliverypointFields.DeliverypointgroupId == useableEntity.DeliverypointgroupId);
            filterUniqueDeliverypointNumber.Add(DeliverypointFields.Number == useableEntity.Number);
            if (!useableEntity.IsNew)
                filterUniqueDeliverypointNumber.Add(DeliverypointFields.DeliverypointId != useableEntity.DeliverypointId);

            DeliverypointCollection deliverypointsWithSameNumber = new DeliverypointCollection();
            deliverypointsWithSameNumber.AddToTransaction(useableEntity);

            if (deliverypointsWithSameNumber.GetDbCount(filterUniqueDeliverypointNumber) > 0)
                throw new ObymobiException(DeliverypointSaveResult.MultipleDeliverypointsWithSameNumber, "Deliverypointnumber: '{0}', DeliverypointgroupId: '{1}'", useableEntity.Number, useableEntity.DeliverypointgroupId);

            // Don't allow changing of DeliverypointGroup if it's used by a Client
            if (useableEntity.Fields[(int)DeliverypointFieldIndex.DeliverypointgroupId].IsChanged &&
                useableEntity.HasClient)
            { 
                string clients = "";
                foreach(var client in useableEntity.ClientCollection)
                {
                    clients = StringUtil.CombineWithCommaSpace(clients, "{0} ({1}, Pk: {2})".FormatSafe(client.MacAddress, client.DeviceEntity.PrivateIpAddresses, client.ClientId));
                }
                throw new ObymobiException(DeliverypointSaveResult.DeliverypointGroupCantBeChangedWhenDeliverypointIsLinkedToClients, "Deliverypoint: {0} (Id: {1}), Used by Clients: {2}", useableEntity.Number, useableEntity.DeliverypointId, clients);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

