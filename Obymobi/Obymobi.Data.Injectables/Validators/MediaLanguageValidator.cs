﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(MediaLanguageEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class MediaLanguageValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            MediaLanguageEntity useableEntity = involvedEntity as MediaLanguageEntity;

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}


