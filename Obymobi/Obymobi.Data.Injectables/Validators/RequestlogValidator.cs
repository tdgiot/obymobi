﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(RequestlogEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class RequestlogValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            var useableEntity = involvedEntity as RequestlogEntity;

            // Clear fields if they should not be saved
            if (useableEntity.ResultCode.Equals("100", StringComparison.OrdinalIgnoreCase))
            {
                if (!Dionysos.ConfigurationManager.GetBool(ObymobiConfigConstants.RequestlogLogRawRequestsOnSuccess))
                    useableEntity.RawRequest = string.Empty;

                if (!Dionysos.ConfigurationManager.GetBool(ObymobiConfigConstants.RequestlogLogResultBodyOnSuccess))
                    useableEntity.ResultBody = string.Empty;
            }

            if (string.IsNullOrEmpty(useableEntity.ServerName))
            {
                useableEntity.ServerName = Environment.MachineName;
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

