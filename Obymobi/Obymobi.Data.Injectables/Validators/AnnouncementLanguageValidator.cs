﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AnnouncementLanguageEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AnnouncementLanguageValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AnnouncementLanguageEntity useableEntity = involvedEntity as AnnouncementLanguageEntity;

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

