﻿using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ReleaseEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ReleaseValidator : GeneralValidator
    {
        public enum ReleaseValidatorResult
        {
            VersionAlreadyExistsForApplication = 200,
            FullReleaseDoesNotExists = 201,
            IntermediateReleaseAlreadyLinked = 202,
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            var useableEntity = involvedEntity as ReleaseEntity;

            if (!useableEntity.ApplicationEntity.Code.StartsWith("CraveOS", StringComparison.InvariantCultureIgnoreCase))
            {
                useableEntity.Intermediate = false;
            }

            if (useableEntity.IsNew || useableEntity.Fields[(int)ReleaseFieldIndex.Version].IsChanged)
            {
                var filter = new PredicateExpression();
                filter.Add(ReleaseFields.ApplicationId == useableEntity.ApplicationId);
                filter.Add(ReleaseFields.Intermediate == useableEntity.Intermediate);
                filter.Add(ReleaseFields.Version == useableEntity.Version);

                var releaseCollection = new ReleaseCollection();
                if (releaseCollection.GetDbCount(filter) > 0)
                {
                    throw new ObymobiException(ReleaseValidatorResult.VersionAlreadyExistsForApplication);
                }
            }

            if (useableEntity.ApplicationEntity.Code.StartsWith("CraveOS", StringComparison.InvariantCultureIgnoreCase))
            {
                if (useableEntity.Intermediate && 
                    (useableEntity.IsNew || 
                    useableEntity.Fields[(int)ReleaseFieldIndex.Version].IsChanged || 
                    useableEntity.Fields[(int)ReleaseFieldIndex.Intermediate].IsChanged))
                {
                    var filter = new PredicateExpression();
                    filter.Add(ReleaseFields.ApplicationId == useableEntity.ApplicationId);
                    filter.Add(ReleaseFields.Intermediate == false);
                    filter.Add(ReleaseFields.Version == useableEntity.Version);

                    var include = new IncludeFieldsList();
                    include.Add(ReleaseFields.ReleaseId);
                    include.Add(ReleaseFields.IntermediateReleaseId);

                    var releaseCollection = new ReleaseCollection();
                    releaseCollection.GetMulti(filter, include, 0);
                    if (releaseCollection.Count == 0)
                    {
                        throw new ObymobiException(ReleaseValidatorResult.FullReleaseDoesNotExists);
                    }

                    var fullReleaseEntity = releaseCollection[0];
                    if (fullReleaseEntity.ReleaseId == useableEntity.ReleaseId)
                    {
                        throw new ObymobiException(ReleaseValidatorResult.FullReleaseDoesNotExists);
                    }
                    if (fullReleaseEntity.IntermediateReleaseId.HasValue && 
                        (useableEntity.IsNew || fullReleaseEntity.IntermediateReleaseId != useableEntity.ReleaseId))
                    {
                        throw new ObymobiException(ReleaseValidatorResult.IntermediateReleaseAlreadyLinked);
                    }

                    // Save for later
                    useableEntity.AfterSaveFullReleaseEntity = fullReleaseEntity;
                }
            }
            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            var useableEntity = involvedEntity as ReleaseEntity;

            if (useableEntity.AfterSaveFullReleaseEntity != null)
            {
                useableEntity.AfterSaveFullReleaseEntity.IntermediateReleaseId = useableEntity.ReleaseId;
                useableEntity.AfterSaveFullReleaseEntity.Save();
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }
    }
}
