﻿using System;
using Crave.Domain.UseCases;
using Crave.Domain.UseCases.Requests;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(CustomTextEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class CustomTextValidator : GeneralValidator
    {
        private readonly SaveCustomTextForBrandProductUseCase saveCustomTextForBrandProductUseCase;
        private readonly DeleteCustomTextForBrandProductUseCase deleteCustomTextForBrandProductUseCase;

        public CustomTextValidator()
        {
            this.saveCustomTextForBrandProductUseCase = new SaveCustomTextForBrandProductUseCase();
            this.deleteCustomTextForBrandProductUseCase = new DeleteCustomTextForBrandProductUseCase();
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            CustomTextEntity useableEntity = involvedEntity as CustomTextEntity;

            if (useableEntity.Type == CustomTextType.CompanyVenuePageDescription && useableEntity.Fields[(int)CustomTextFieldIndex.Text].IsChanged)
            {
                // FO: Don't ask me, i'm just the messenger, copied from CompanyLanguageValidator.cs
                PredicateExpression filter = new PredicateExpression();
                filter.Add(PageElementFields.PageElementType == (int)PageElementType.Venue);
                filter.Add(PageElementFields.LanguageId == useableEntity.LanguageId);
                filter.Add(PageElementFields.IntValue1 == useableEntity.CompanyId);

                RelationCollection relations = new RelationCollection();
                relations.Add(SiteEntityBase.Relations.PageEntityUsingSiteId);
                relations.Add(PageEntityBase.Relations.PageElementEntityUsingPageId);

                SiteCollection siteCollection = new SiteCollection();
                siteCollection.GetMulti(filter, relations);

                foreach (SiteEntity siteEntity in siteCollection)
                {
                    // Update site last modified date so site is refreshed when loaded
                    siteEntity.AddToTransaction(useableEntity);
                    siteEntity.LastModifiedUTC = DateTime.UtcNow;
                    siteEntity.Save();
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            CustomTextEntity useableEntity = involvedEntity as CustomTextEntity;

            if (useableEntity.ProductId.HasValue && useableEntity.ProductEntity.BrandId.HasValue)
            {
                this.saveCustomTextForBrandProductUseCase.Execute(new CustomTextForBrandProductRequest
                {
                    BrandProductId = useableEntity.ProductId.Value,
                    CustomText = useableEntity
                });
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            CustomTextEntity useableEntity = involvedEntity as CustomTextEntity;

            if (useableEntity.ProductId.HasValue && useableEntity.ProductEntity.BrandId.HasValue)
            {
                this.deleteCustomTextForBrandProductUseCase.Execute(new CustomTextForBrandProductRequest
                {
                    BrandProductId = useableEntity.ProductId.Value,
                    CustomText = useableEntity
                });
            }

            base.ValidateEntityBeforeDelete(involvedEntity);
        }

    }
}

