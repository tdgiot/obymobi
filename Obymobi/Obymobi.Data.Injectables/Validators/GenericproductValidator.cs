﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Crave.Api.Logic;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(GenericproductEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class GenericproductValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            GenericproductEntity useableEntity = involvedEntity as GenericproductEntity;

            // Field 'Name' must have a value
            if (useableEntity.Name.Length == 0)
                throw new ObymobiException(GenericproductSaveResult.NameEmpty);

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

