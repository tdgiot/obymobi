﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ReportProcessingTaskEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ReportProcessingTaskValidator : GeneralValidator
    {
        public enum ReportProcessingTaskResult : int
        {
            FromUTCNotSet = 200,
            TillUTCNotSet = 201,
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            ReportProcessingTaskEntity useableEntity = involvedEntity as ReportProcessingTaskEntity;

            if (!useableEntity.FromUTC.HasValue)
                throw new ObymobiEntityException(ReportProcessingTaskResult.FromUTCNotSet, useableEntity);

            if (!useableEntity.TillUTC.HasValue)
                throw new ObymobiEntityException(ReportProcessingTaskResult.TillUTCNotSet, useableEntity);

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

