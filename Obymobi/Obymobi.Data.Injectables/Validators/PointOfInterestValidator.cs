﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PointOfInterestEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PointOfInterestValidator : GeneralValidator
    {
        private bool WasNew = false;

        public enum PointOfInterestValidatorResult
        { 
            CantDeleteBecauseItsUsedInSites = 200
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            PointOfInterestEntity useableEntity = involvedEntity as PointOfInterestEntity;
            
            this.WasNew = useableEntity.IsNew;

            // Update the LastModified field
            if (useableEntity.IsDirty)
                useableEntity.LastModifiedUTC = DateTime.UtcNow;

            // Check whether the Visible flag changed to true
            if (!useableEntity.IsNew && ((useableEntity.Fields[(int)PointOfInterestFieldIndex.Visible].IsChanged && useableEntity.Visible)))
            {
                // Remove the records from the AccessCodePointOfInterest table
                AccessCodePointOfInterestCollection accessCodePointOfInterestCollection = new AccessCodePointOfInterestCollection();
                accessCodePointOfInterestCollection.AddToTransaction(useableEntity);
                accessCodePointOfInterestCollection.GetMulti(AccessCodePointOfInterestFields.PointOfInterestId == useableEntity.PointOfInterestId);
                foreach (AccessCodePointOfInterestEntity accessCodePointOfInterest in accessCodePointOfInterestCollection)
                    accessCodePointOfInterest.Delete();
            }

            // Update LastCompanyRemovedFromMobileCompanyList
            // If a Company is taken of the Mobile Company list the LastCompanyListModifiedTicks doesn't update as that is the 
            // Max of all Company.CompanyDataLastModified, and when that company is removed it won't be included. Therefore
            // this (dirty) solution.
            if (!useableEntity.IsNew &&
                !useableEntity.Visible &&
                useableEntity.Fields[(int)PointOfInterestFieldIndex.Visible].IsChanged)
            {
                Dionysos.ConfigurationManager.SetValue(ObymobiDataConfigConstants.LastItemRemovedFromMobileCompanyList, DateTime.UtcNow);
            }

            base.ValidateEntityBeforeSave(involvedEntity);            
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            PointOfInterestEntity useableEntity = involvedEntity as PointOfInterestEntity;
            
            if (this.WasNew)
            {
                // Create the ui modes for holding the tabs of an point of interest
                var uiModeTablet = new UIModeEntity();
                uiModeTablet.Type = UIModeType.GuestOwnedTabletDevices;
                uiModeTablet.Name = useableEntity.Name + " - Tablet";
                uiModeTablet.PointOfInterestId = useableEntity.PointOfInterestId;
                uiModeTablet.AddToTransaction(useableEntity);
                uiModeTablet.Save();

                var uiModeMobile = new UIModeEntity();
                uiModeMobile.Type = UIModeType.GuestOwnedMobileDevices;
                uiModeMobile.Name = useableEntity.Name + " - Mobile";
                uiModeMobile.PointOfInterestId = useableEntity.PointOfInterestId;
                uiModeMobile.AddToTransaction(useableEntity);
                uiModeMobile.Save();
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            PointOfInterestEntity useableEntity = involvedEntity as PointOfInterestEntity;

            // Check if it's used in sites
            // GK I know that this ain't to pretty since we work with Int Value, if you have a good solution for me - please enlighten me!
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PageElementFields.IntValue2 == useableEntity.PointOfInterestId);
            filter.Add(PageElementFields.PageElementType == (int)PageElementType.Venue);

            PageElementCollection pageElements = new PageElementCollection();
            pageElements.AddToTransaction(useableEntity);
            pageElements.GetMulti(filter);

            string errorText = string.Empty;
            if (pageElements.Count > 0)
            { 
                foreach(var pageElement in pageElements)
                {
                    errorText += "Page '{0}' of Site: '{1}' | ".FormatSafe(pageElement.PageEntity.Name, pageElement.PageEntity.SiteEntity.Name);
                }

                throw new ObymobiEntityException(PointOfInterestValidatorResult.CantDeleteBecauseItsUsedInSites, "Can't delete Point of Interest '{0}' because it's used in Sites: '{1}'", useableEntity.Name, errorText);
            }

            // Update the LastItemRemovedFromMobileCompanyList value in the config
            Dionysos.ConfigurationManager.SetValue(ObymobiDataConfigConstants.LastItemRemovedFromMobileCompanyList, DateTime.UtcNow);

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

