﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Cms;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(UserRoleEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class UserRoleValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            UserRoleEntity userRole = (UserRoleEntity)involvedEntity;

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            UserRoleEntity userRole = (UserRoleEntity)involvedEntity;

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}
