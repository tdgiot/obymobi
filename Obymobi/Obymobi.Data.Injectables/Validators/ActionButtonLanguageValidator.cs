﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ActionButtonLanguageEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ActionButtonLanguageValidator : GeneralValidator
    {
        public enum ActionButtonLanguageValidatorResult
        {
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            ActionButtonLanguageEntity useableEntity = involvedEntity as ActionButtonLanguageEntity;

            if (useableEntity.IsDirty)
            {
                useableEntity.ActionButtonEntity.LastModifiedUTC = DateTime.UtcNow;
                useableEntity.ActionButtonEntity.Save();
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            ActionButtonLanguageEntity useableEntity = involvedEntity as ActionButtonLanguageEntity;

            useableEntity.ActionButtonEntity.LastModifiedUTC = DateTime.UtcNow;
            useableEntity.ActionButtonEntity.Save();

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

