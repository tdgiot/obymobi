﻿using System;
using System.Linq;
using Dionysos;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ProductgroupEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ProductgroupValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            ProductgroupEntity useableEntity = involvedEntity as ProductgroupEntity;
            useableEntity.NameChanged = useableEntity.Fields[ProductgroupFields.Name.Name].IsChanged;

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            ProductgroupEntity useableEntity = involvedEntity as ProductgroupEntity;

            if (useableEntity.ValidatorCreateOrUpdateDefaultCustomText && useableEntity.NameChanged)
            {
                this.UpdateDefaultCustomText(useableEntity);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        private void UpdateDefaultCustomText(ProductgroupEntity productgroupEntity)
        {
            string companyCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            if (!companyCultureCode.IsNullOrWhiteSpace())
            {
                CustomTextEntity customText = productgroupEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCultureCode && x.Type == CustomTextType.ProductgroupName);
                if (customText == null)
                {
                    customText = CustomTextHelper.CreateEntity(CustomTextType.ProductgroupName, companyCultureCode, productgroupEntity.Name, ProductgroupFields.ProductgroupId, productgroupEntity.ProductgroupId);
                }
                customText.Text = productgroupEntity.Name;
                customText.Save();
            }
        }
    }
}

