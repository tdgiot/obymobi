﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Linq;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Crave.Api.Logic;
using Dionysos.Data.Extensions;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AlterationEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AlterationValidator : GeneralValidator
    {
        public enum AlterationValidatorResult
        {
            MinAndMaxOptionsIsZero = 200,
            MaxOptionsIsZero = 201
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AlterationEntity useableEntity = involvedEntity as AlterationEntity;
            useableEntity.NameChanged = useableEntity.Fields[AlterationFields.Name.Name].IsChanged;

             if (useableEntity.MaxOptions < useableEntity.MinOptions)
                useableEntity.MaxOptions = useableEntity.MinOptions;

            // Max options should always be smaller than the amount of options currently selected by default
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AlterationFields.AlterationId == useableEntity.AlterationId);
            filter.Add(AlterationitemFields.SelectedOnDefault == true);
            RelationCollection relations = new RelationCollection();
            relations.Add(AlterationitemEntity.Relations.AlterationEntityUsingAlterationId);

            int amountOfOptionsSelectedByDefault = EntityCollection.GetDbCount<AlterationitemCollection>(filter, relations);
            if (amountOfOptionsSelectedByDefault > useableEntity.MaxOptions)
                throw new EntitySaveException("Alteration could not be saved because the Maximum # selected is currently set to '{0}', while currently '{1}' options are selected by default.", amountOfOptionsSelectedByDefault, useableEntity.MaxOptions);
            
            if (useableEntity.IsChanged(AlterationFields.ExternalProductId))
            {
                UpdateAlterationWithExternalProductFields(useableEntity);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            AlterationEntity useableEntity = involvedEntity as AlterationEntity;

            if (useableEntity.ValidatorCreateOrUpdateDefaultCustomText && useableEntity.NameChanged)
            {
                this.UpdateDefaultCustomText(useableEntity);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        private void UpdateDefaultCustomText(AlterationEntity alterationEntity)
        {
            string companyCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            if (!companyCultureCode.IsNullOrWhiteSpace())
            {
                CustomTextEntity customText = alterationEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCultureCode && x.Type == CustomTextType.AlterationName);
                if (customText == null)
                {
                    customText = CustomTextHelper.CreateEntity(CustomTextType.AlterationName, companyCultureCode, alterationEntity.Name, AlterationFields.AlterationId, alterationEntity.AlterationId);
                }
                customText.Text = alterationEntity.Name;    
                customText.Save();
            }
        }

        private void UpdateAlterationWithExternalProductFields(AlterationEntity alterationEntity)
        {
            if (alterationEntity.ExternalProductId.HasValue)
            {
                ExternalProductEntity externalProduct = alterationEntity.ExternalProductEntity;

                alterationEntity.MinOptions = externalProduct.MinOptions;
                alterationEntity.MaxOptions = externalProduct.MaxOptions;
                alterationEntity.AllowDuplicates = externalProduct.AllowDuplicates;
                alterationEntity.IsAvailable = externalProduct.IsEnabled;
            }
        }
    }
}

