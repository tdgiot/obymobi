﻿using System;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Dionysos.Data.LLBLGen;
using System.Linq;
using System.Web.Routing;
using Dionysos;
using Obymobi.Enums;
using Obymobi.Logic.CraveService;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(DeliverypointgroupEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class DeliverypointgroupValidator : GeneralValidator
    {
        public enum DeliverypointgroupValidatorResult : int
        {
            UIModesNotSetForDeliverypointgroup = 400,
            PincodeTooShort = 500,
            ClientsRunningBelowMinimumApplicationVersionForRestApi = 600,
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            DeliverypointgroupEntity useableEntity = involvedEntity as DeliverypointgroupEntity;
            useableEntity.WasNew = useableEntity.IsNew;
            useableEntity.ClientConfigurationIdChanged = useableEntity.Fields[DeliverypointgroupFields.ClientConfigurationId.FieldIndex].IsChanged;
            useableEntity.NameChanged = useableEntity.Fields[DeliverypointgroupFields.Name.FieldIndex].IsChanged;
            useableEntity.EstimatedDeliveryTimeChanged = useableEntity.Fields[DeliverypointgroupFields.EstimatedDeliveryTime.FieldIndex].IsChanged;

            if (useableEntity.Fields[(int)DeliverypointgroupFieldIndex.ApiVersion].IsChanged && useableEntity.ApiVersion == 2)
            {
                if (this.GetHasClientsRunningBelowMinimumApplicationVersion(useableEntity.DeliverypointgroupId, CompanyValidator.MINIMUM_REQUIRED_EMENU_APPLICATION_FOR_REST_API))
                {
                    throw new ObymobiException(DeliverypointgroupValidatorResult.ClientsRunningBelowMinimumApplicationVersionForRestApi, "Unable to switch to REST API. There are clients running below the minimum required Emenu version: {0}. Update the clients first.", CompanyValidator.MINIMUM_REQUIRED_EMENU_APPLICATION_FOR_REST_API);
                }
            }

            if (useableEntity.Fields[(int)DeliverypointgroupFieldIndex.MessagingVersion].IsChanged && useableEntity.MessagingVersion == 3)
            {
                if (this.GetHasClientsRunningBelowMinimumApplicationVersion(useableEntity.DeliverypointgroupId, CompanyValidator.MINIMUM_REQUIRED_EMENU_APPLICATION_FOR_MESSAGING_V3))
                {
                    throw new ObymobiException(CompanySaveResult.ClientsRunningBelowMinimumApplicationVersionForRestApi, "Unable to switch to Messaging V3. There are clients running below the minimum required Emenu version: {0}. Update the clients first.", CompanyValidator.MINIMUM_REQUIRED_EMENU_APPLICATION_FOR_MESSAGING_V3);
                }
            }

            if (useableEntity.AvailableOnObymobi)
            {
                // Crave mobile checkbox requires multiple ui modes to be set
                StringBuilder builder = new StringBuilder();
                if (useableEntity.MobileUIModeId.IsNullOrZero() ||
                    useableEntity.TabletUIModeId.IsNullOrZero())
                    builder.AppendLine("");

                if (useableEntity.MobileUIModeId.IsNullOrZero())
                    builder.AppendLine("- UI mode guest owned mobile devices");
                if (useableEntity.TabletUIModeId.IsNullOrZero())
                    builder.AppendLine("- UI mode guest owned tablets");

                if (builder.Length > 0)
                    throw new ObymobiEntityException(DeliverypointgroupValidatorResult.UIModesNotSetForDeliverypointgroup, "Crave mobile requires different UI modes, which haven't been set yet for the deliverypoint group. Please set the following UI modes first: {0} ", builder.ToString());
            }

            if (useableEntity.Pincode.Length < 4 || useableEntity.PincodeSU.Length < 4 || useableEntity.PincodeGM.Length < 4)
            {
                throw new ObymobiEntityException(DeliverypointgroupValidatorResult.PincodeTooShort, "Pincodes have to be at least 4 characters long (Pincode: {0}, SU: {1}, GM: {2})", useableEntity.Pincode.Length, useableEntity.PincodeSU.Length, useableEntity.PincodeGM.Length);
            }

            if (!useableEntity.Fields[CompanyFields.AvailableOnObymobi.FieldIndex].IsChanged)
            {
                // AvailableOnObymobi hasnt changed, so dont bother
            }
            else if (useableEntity.AvailableOnObymobi)
            { 
                // AvailableOnObymobi is set to true, so dont delete the access code
            }
            else if (EntityCollection.GetMulti<DeliverypointgroupCollection>(DeliverypointgroupFields.CompanyId == useableEntity.CompanyId, null, DeliverypointgroupFields.AvailableOnObymobi).All(x => !x.AvailableOnObymobi))
            {
                // Remove the records from the AccessCodeCompany table
                AccessCodeCompanyCollection accessCodeCompanyCollection = new AccessCodeCompanyCollection();
                accessCodeCompanyCollection.AddToTransaction(useableEntity);
                accessCodeCompanyCollection.GetMulti(AccessCodeCompanyFields.CompanyId == useableEntity.CompanyId);
                foreach (AccessCodeCompanyEntity accessCodeCompany in accessCodeCompanyCollection)
                    accessCodeCompany.Delete();
            }

            if (useableEntity.CompanyEntity.AvailableOnOtoucho)
            {
                if (useableEntity.UIModeId.IsNullOrZero())
                {
                    throw new ObymobiEntityException(DeliverypointgroupValidatorResult.UIModesNotSetForDeliverypointgroup, "Crave emenu requires a UI mode, which hasn't been set yet for the deliverypoint group. Please set the following UI mode first: Venue owned devices");
                }
            }

            if (useableEntity.IsNew && !useableEntity.BeingCopied)
            {
                // Default values
                useableEntity.OrderCompletedEnabled = true;
                useableEntity.OrderProcessingNotificationEnabled = true;
                useableEntity.OrderProcessedNotificationEnabled = true;
                useableEntity.DeliverypointCaption = useableEntity.DeliverypointCaption.IsNullOrWhiteSpace() ? "Room" : useableEntity.DeliverypointCaption;
            }

            this.UpdateClientConfigurationRoutes(useableEntity);

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private bool GetHasClientsRunningBelowMinimumApplicationVersion(int deliverypointgroupId, string version)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.DeliverypointGroupId == deliverypointgroupId);
            filter.Add(DeviceFields.LastRequestUTC > DateTime.UtcNow.AddHours(-1));
            filter.Add(DeviceFields.ApplicationVersion < version);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            ClientCollection clientCollection = new ClientCollection();
            return clientCollection.GetDbCount(filter, relations) > 0;
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            DeliverypointgroupEntity useableEntity = involvedEntity as DeliverypointgroupEntity;

            if (useableEntity.WasNew)
            {
                if (!useableEntity.BeingCopied)
                {
                    this.CreateDefaultMobileTexts(useableEntity);
                    this.CreateDefaultNotificationTexts(useableEntity);
                }

                if (!useableEntity.ClientConfigurationId.HasValue)
                {
                    ClientConfigurationHelper.CreateFromDeliverypointgroup(useableEntity);
                }
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        private void CreateDefaultNotificationTexts(DeliverypointgroupEntity useableEntity)
        {
            string cultureCode = useableEntity.CompanyEntity.CultureCode;

            CustomTextCollection customTexts = new CustomTextCollection();
            customTexts.AddToTransaction(useableEntity.GetCurrentTransaction());

            if (useableEntity.OutOfChargeTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOutOfChargeTitle, cultureCode, DeliverypointgroupValidator.OUT_OF_CHARGE_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.OutOfChargeMessage.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOutOfChargeText, cultureCode, DeliverypointgroupValidator.OUT_OF_CHARGE_MESSAGE, useableEntity.DeliverypointgroupId));
            if (useableEntity.OrderProcessedTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOrderProcessedTitle, cultureCode, DeliverypointgroupValidator.ORDER_PROCESSED_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.OrderProcessedMessage.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOrderProcessedText, cultureCode, DeliverypointgroupValidator.ORDER_PROCESSED_MESSAGE, useableEntity.DeliverypointgroupId));
            if (useableEntity.OrderFailedTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOrderFailedTitle, cultureCode, DeliverypointgroupValidator.ORDER_FAILED_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.OrderFailedMessage.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOrderFailedText, cultureCode, DeliverypointgroupValidator.ORDER_FAILED_MESSAGE, useableEntity.DeliverypointgroupId));
            if (useableEntity.OrderSavingTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOrderSavingTitle, cultureCode, DeliverypointgroupValidator.ORDER_SAVING_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.OrderSavingMessage.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOrderSavingText, cultureCode, DeliverypointgroupValidator.ORDER_SAVING_MESSAGE, useableEntity.DeliverypointgroupId));
            if (useableEntity.OrderCompletedTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOrderCompletedTitle, cultureCode, DeliverypointgroupValidator.ORDER_COMPLETE_VERIFICATION_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.OrderCompletedMessage.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOrderCompletedText, cultureCode, DeliverypointgroupValidator.ORDER_COMPLETE_VERIFICATION_MESSAGE, useableEntity.DeliverypointgroupId));
            if (useableEntity.ServiceSavingTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupServiceSavingTitle, cultureCode, DeliverypointgroupValidator.SERVICE_SAVING_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.ServiceSavingMessage.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupServiceSavingText, cultureCode, DeliverypointgroupValidator.SERVICE_SAVING_MESSAGE, useableEntity.DeliverypointgroupId));
            if (useableEntity.ServiceProcessedTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupServiceProcessedTitle, cultureCode, DeliverypointgroupValidator.SERVICE_PROCESSED_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.ServiceProcessedMessage.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupServiceProcessedText, cultureCode, DeliverypointgroupValidator.SERVICE_PROCESSED_MESSAGE, useableEntity.DeliverypointgroupId));
            if (useableEntity.RatingSavingTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupRatingSavingTitle, cultureCode, DeliverypointgroupValidator.RATING_SAVING_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.RatingSavingMessage.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupRatingSavingText, cultureCode, DeliverypointgroupValidator.RATING_SAVING_MESSAGE, useableEntity.DeliverypointgroupId));
            if (useableEntity.RatingProcessedTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupRatingProcessedTitle, cultureCode, DeliverypointgroupValidator.RATING_PROCESSED_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.RatingProcessedMessage.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupRatingProcessedText, cultureCode, DeliverypointgroupValidator.RATING_PROCESSED_MESSAGE, useableEntity.DeliverypointgroupId));

            if (useableEntity.GetCurrentTransaction() != null)
            {
                customTexts.AddToTransaction(useableEntity.GetCurrentTransaction());
            }            
            customTexts.SaveMulti();
        }

        private void CreateDefaultMobileTexts(DeliverypointgroupEntity useableEntity)
        {
            string cultureCode = useableEntity.CompanyEntity.CultureCode;

            CustomTextCollection customTexts = new CustomTextCollection();
            customTexts.AddToTransaction(useableEntity.GetCurrentTransaction());

            if (useableEntity.OrderProcessingNotificationTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOrderProcessingNotificationTitle, cultureCode, DeliverypointgroupValidator.ON_THE_CASE_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.OrderProcessingNotification.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOrderProcessingNotificationText, cultureCode, DeliverypointgroupValidator.ON_THE_CASE_MESSAGE, useableEntity.DeliverypointgroupId));
            if (useableEntity.ServiceProcessingNotificationTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupServiceProcessingNotificationTitle, cultureCode, DeliverypointgroupValidator.ON_THE_CASE_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.ServiceProcessingNotification.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupServiceProcessingNotificationText, cultureCode, DeliverypointgroupValidator.ON_THE_CASE_MESSAGE, useableEntity.DeliverypointgroupId));

            if (useableEntity.OrderProcessedNotificationTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOrderProcessedNotificationTitle, cultureCode, DeliverypointgroupValidator.COMPLETE_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.OrderProcessedNotification.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupOrderProcessedNotificationText, cultureCode, DeliverypointgroupValidator.COMPLETE_MESSAGE, useableEntity.DeliverypointgroupId));
            if (useableEntity.ServiceProcessedNotificationTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupServiceProcessedNotificationTitle, cultureCode, DeliverypointgroupValidator.COMPLETE_TITLE, useableEntity.DeliverypointgroupId));
            if (useableEntity.ServiceProcessedNotification.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupServiceProcessedNotificationText, cultureCode, DeliverypointgroupValidator.COMPLETE_MESSAGE, useableEntity.DeliverypointgroupId));
            if (useableEntity.FreeformMessageTitle.IsNullOrWhiteSpace())
                customTexts.Add(CustomTextHelper.CreateEntityForDeliverypointgroup(CustomTextType.DeliverypointgroupFreeformMessageTitle, cultureCode, DeliverypointgroupValidator.FREEFORM_TEXT_TITLE, useableEntity.DeliverypointgroupId));

            if (useableEntity.GetCurrentTransaction() != null)
            {
                customTexts.AddToTransaction(useableEntity.GetCurrentTransaction());
            }            
            customTexts.SaveMulti();
        }

        private void UpdateClientConfigurationRoutes(DeliverypointgroupEntity useableEntity)
        {
            if (useableEntity.Fields[DeliverypointgroupFields.RouteId.FieldIndex].IsChanged)
            {
                this.UpdateClientConfigurationRoute(useableEntity, useableEntity.RouteId, RouteType.Normal);
            }

            if (useableEntity.Fields[DeliverypointgroupFields.SystemMessageRouteId.FieldIndex].IsChanged)
            {
                this.UpdateClientConfigurationRoute(useableEntity, useableEntity.SystemMessageRouteId, RouteType.SystemMessage);
            }

            if (useableEntity.Fields[DeliverypointgroupFields.OrderNotesRouteId.FieldIndex].IsChanged)
            {
                this.UpdateClientConfigurationRoute(useableEntity, useableEntity.OrderNotesRouteId, RouteType.OrderNotes);
            }

            if (useableEntity.Fields[DeliverypointgroupFields.EmailDocumentRouteId.FieldIndex].IsChanged)
            {
                this.UpdateClientConfigurationRoute(useableEntity, useableEntity.EmailDocumentRouteId, RouteType.EmailDocument);
            }
        }

        private void UpdateClientConfigurationRoute(DeliverypointgroupEntity useableEntity, int? routeId, RouteType routeType)
        {
            if (!useableEntity.ClientConfigurationId.HasValue)
                return;

            if (!routeId.HasValue)
            {
                this.DeleteClientConfigurationRoute(useableEntity, useableEntity.ClientConfigurationId.Value, routeType);
            }
            else
            {
                ClientConfigurationRouteEntity clientConfigurationRouteEntity = this.GetClientConfigurationRoute(useableEntity.ClientConfigurationId.Value, routeType);
                if (clientConfigurationRouteEntity == null)
                {
                    clientConfigurationRouteEntity = this.CreateClientConfigurationRoute(useableEntity.ClientConfigurationId.Value, useableEntity.CompanyId, routeId.Value, routeType);
                }

                clientConfigurationRouteEntity.RouteId = routeId.Value;
                clientConfigurationRouteEntity.AddToTransaction(useableEntity);
                clientConfigurationRouteEntity.Save();
            }
        }

        private void DeleteClientConfigurationRoute(DeliverypointgroupEntity useableEntity, int clientConfigurationId, RouteType routeType)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientConfigurationRouteFields.ClientConfigurationId == clientConfigurationId);
            filter.Add(ClientConfigurationRouteFields.Type == routeType);

            ClientConfigurationRouteCollection clientConfigurationRouteCollection = new ClientConfigurationRouteCollection();
            clientConfigurationRouteCollection.AddToTransaction(useableEntity);
            clientConfigurationRouteCollection.DeleteMulti(filter);
        }

        private ClientConfigurationRouteEntity GetClientConfigurationRoute(int clientConfigurationId, RouteType routeType)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientConfigurationRouteFields.ClientConfigurationId == clientConfigurationId);
            filter.Add(ClientConfigurationRouteFields.Type == routeType);

            ClientConfigurationRouteCollection clientConfigurationRouteCollection = new ClientConfigurationRouteCollection();
            clientConfigurationRouteCollection.GetMulti(filter);

            return clientConfigurationRouteCollection.FirstOrDefault();
        }

        private ClientConfigurationRouteEntity CreateClientConfigurationRoute(int clientConfigurationId, int parentCompanyId, int routeId, RouteType routeType)
        {
            ClientConfigurationRouteEntity clientConfigurationRouteEntity = new ClientConfigurationRouteEntity();
            clientConfigurationRouteEntity.ClientConfigurationId = clientConfigurationId;
            clientConfigurationRouteEntity.ParentCompanyId = parentCompanyId;
            clientConfigurationRouteEntity.RouteId = routeId;
            clientConfigurationRouteEntity.Type = (int)routeType;

            return clientConfigurationRouteEntity;
        }

        #region Constants

        private const string OUT_OF_CHARGE_TITLE = "My battery is getting low";
        private const string OUT_OF_CHARGE_MESSAGE = "Please put me on charge so that I can assist you later.";
        private const string ORDER_PROCESSED_TITLE = "Room Service Order";
        private const string ORDER_PROCESSED_MESSAGE = "Thank you for submitting your room service order.";
        private const string ORDER_FAILED_TITLE = "There has been a delay in submitting your order";
        private const string ORDER_FAILED_MESSAGE = "Unfortunately there appears to be a problem with processing your order, please contact reception on ext: ?.";
        private const string ORDER_SAVING_TITLE = "Thank you for your room service order";
        private const string ORDER_SAVING_MESSAGE = "Your room service order is on its way to the kitchen.";
        private const string ORDER_COMPLETE_VERIFICATION_TITLE = "Please confirm that you are ready to submit your room service request";
        private const string ORDER_COMPLETE_VERIFICATION_MESSAGE = "Is that the complete order that you wish to place for your room?";
        private const string SERVICE_SAVING_TITLE = "Service Request";
        private const string SERVICE_SAVING_MESSAGE = "Your service request is in the process of being submitted to the team.";
        private const string SERVICE_PROCESSED_TITLE = "Thank you for your service request";
        private const string SERVICE_PROCESSED_MESSAGE = "We are on the case and we will be in contact shortly.";
        private const string RATING_SAVING_TITLE = "Ratings Submission";
        private const string RATING_SAVING_MESSAGE = "Your ratings are being submitted to the Management Team.";
        private const string RATING_PROCESSED_TITLE = "Thank you for your rating submission";
        private const string RATING_PROCESSED_MESSAGE = "The Management Team appreciates your feedback.";

        private const string ON_THE_CASE_TITLE = "Thank you for your request";
        private const string ON_THE_CASE_MESSAGE = "Thank you for your request. We will send you a notification when your request has been actioned.";
        private const string COMPLETE_TITLE = "Your request has been actioned";
        private const string COMPLETE_MESSAGE = "Your request has been actioned by a member of the team.";
        private const string FREEFORM_TEXT_TITLE = "A message from a member of the team.";

        #endregion
    }
}

