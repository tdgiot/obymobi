﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AccessCodeEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AccessCodeValidator : GeneralValidator
    {
        public enum AccessCodeValidatorResult
        {
            NoCodeSpecified = 200,
            AccessCodeAlreadyExists = 210
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AccessCodeEntity useableEntity = involvedEntity as AccessCodeEntity;

            if (useableEntity.IsDirty)
                useableEntity.LastModifiedUTC = DateTime.UtcNow;

            // Validate whether a code is supplied
            if (useableEntity.Code.IsNullOrWhiteSpace())
                throw new ObymobiException(AccessCodeValidatorResult.NoCodeSpecified);

            // Check whether the code already exists
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AccessCodeFields.Code == useableEntity.Code);
            if (!useableEntity.IsNew)
                filter.Add(AccessCodeFields.AccessCodeId != useableEntity.AccessCodeId);

            AccessCodeCollection accessCodeCollection = new AccessCodeCollection();
            accessCodeCollection.AddToTransaction(useableEntity);
            if (accessCodeCollection.GetDbCount(filter) > 0)
                throw new ObymobiException(AccessCodeValidatorResult.AccessCodeAlreadyExists);

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            AccessCodeEntity useableEntity = involvedEntity as AccessCodeEntity;

            useableEntity.AfterDelete += useableEntity_AfterDelete;

            base.ValidateEntityBeforeDelete(involvedEntity);
        }

        private void useableEntity_AfterDelete(AccessCodeEntity entity)
        {
            Dionysos.ConfigurationManager.SetValue(ObymobiDataConfigConstants.LastItemRemovedFromAccessCodes, DateTime.UtcNow);
        }
    }
}

