﻿using System;
using System.Linq;
using Dionysos;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Cms;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PageEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PageValidator : GeneralValidator
    {
        public enum PageValidatorResult
        {            
            PageTypeNotCompatibleWithPage = 201,
            PageTypesDoesntMatchWithPageTemplate = 202
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            PageEntity useableEntity = involvedEntity as PageEntity;
            useableEntity.WasNew = useableEntity.IsNew;
            useableEntity.NameChanged = useableEntity.Fields[PageFields.Name.Name].IsChanged;

            // You can't change Page Types of a Page when they are incomptaible and there's content
            if (!useableEntity.IsNew && useableEntity.Fields[PageFields.PageType.Name].IsChanged && useableEntity.PageElementCollection.Count > 0)
            {
                if (!PageHelper.IsPageTypeChangeCompatible(useableEntity))
                {
                    PageType originalPageType;
                    EnumUtil.TryParse<PageType>((int)useableEntity.Fields[PageFields.PageType.Name].DbValue, out originalPageType);
                    throw new ObymobiEntityException(PageValidatorResult.PageTypeNotCompatibleWithPage, "The newly set PageType: '{0}' for Page: '{1}' of Site '{2}' is not compatible with the current page type: '{3}'",
                        useableEntity.PageTypeAsEnum, useableEntity.Name, useableEntity.SiteEntity.Name, originalPageType);
                }
            }

            // Recursively set the visible flag to the child pages
            if (!useableEntity.IsNew && useableEntity.Fields[PageFields.Visible.Name].IsChanged && useableEntity.PageCollection.Count > 0)
            {
                foreach (PageEntity childPage in useableEntity.PageCollection)
                {
                    childPage.Visible = useableEntity.Visible;
                    childPage.Save();
                }
            }

            // Check if this page and the attached page template have the same PageType
            if (useableEntity.Fields[PageFields.PageType.Name].IsChanged && useableEntity.PageTemplateId.HasValue)
            {
                PageTemplateEntity pageTemplate = new PageTemplateEntity(useableEntity.PageTemplateId.Value);
                if (pageTemplate.PageType != useableEntity.PageType)
                    throw new ObymobiEntityException(PageValidatorResult.PageTypesDoesntMatchWithPageTemplate, "The page type of this page must match the page type of the attached PageTemplate: '{0}'", pageTemplate.PageTypeAsEnum);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            PageEntity useableEntity = involvedEntity as PageEntity;
            
            if(useableEntity.WasNew && useableEntity.ValidatorCreateDefaultLanguageAgnosticElements)
            {
                // Always create the Language Agnostic PageElements for the on default Agnostic elements
                PageTypeBase pageType = PageTypeHelper.GetPageTypeInstance(useableEntity.PageTypeAsEnum);
                foreach (PageTypeElement pageElement in pageType.PageTypeElements)
                {
                    if (pageElement.LanguageAgnosticOnDefault)
                    {
                        PageElementEntity pageElementEntity = new PageElementEntity();
                        pageElementEntity.PageId = useableEntity.PageId;
                        pageElementEntity.SystemName = pageElement.SystemName;
                        pageElementEntity.CultureCode = null;
                        pageElementEntity.PageElementType = (int)pageElement.PageTypeElementType;
                        pageElementEntity.AddToTransaction(useableEntity);
                        pageElementEntity.Save();
                    }
                }
            }

            if (useableEntity.ValidatorCreateOrUpdateDefaultPageLanguage && useableEntity.NameChanged)
            {
                this.UpdateDefaultCustomText(useableEntity);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        private void UpdateDefaultCustomText(PageEntity pageEntity)
        {
            string cultureCode = CustomTextHelper.DefaultCulture.Code;
            if (!cultureCode.IsNullOrWhiteSpace())
            {
                CustomTextEntity customText = pageEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == cultureCode && x.Type == CustomTextType.PageName);
                if (customText == null)
                {
                    customText = CustomTextHelper.CreateEntity(CustomTextType.PageName, cultureCode, pageEntity.Name, PageFields.PageId, pageEntity.PageId);
                }
                customText.Text = pageEntity.Name;
                customText.Save();
            }            
        }        
    }
}

