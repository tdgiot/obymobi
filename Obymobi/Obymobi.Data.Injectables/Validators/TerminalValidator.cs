﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Comet;
using Obymobi.Logic.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(TerminalEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class TerminalValidator : GeneralValidator
    {
        public enum TerminalValidatorResult : int
        {
            MultipleTerminalsWithPmsConnectorNotSupported = 200,
            TerminalForwardWouldCreateInfiniteLoop = 201,
            ForwardToTerminalDoesNotExists = 202,
            ForwardToTerminalBelongsToOtherCompany = 203,
            ForwardToTerminalHasADifferentHandlingType = 203,
            MultipleTerminalsForDevice = 204,
            DeviceAlreadyConnectedToClient = 205,
            DeliverypointgroupDoesBelongToOtherCompany = 206,
            RoutestephandlersWithTerminalSpecificHandlerTypesAreUsingThisTerminal = 207
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            TerminalEntity useableEntity = involvedEntity as TerminalEntity;            

            TerminalCollection terminals = new TerminalCollection();
            terminals.AddToTransaction(useableEntity);

            // Check if we have multiple terminals with PMs connectors
            if (useableEntity.Fields[TerminalFields.PMSConnectorType.Name].IsChanged && useableEntity.PMSConnectorType > 0)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(TerminalFields.CompanyId == useableEntity.CompanyId);
                filter.Add(TerminalFields.PMSConnectorType > 0);
                if (!useableEntity.IsNew)
                    filter.Add(TerminalFields.TerminalId != useableEntity.TerminalId);

                if (terminals.GetDbCount(filter) > 0)
                    throw new ObymobiEntityException(TerminalValidatorResult.MultipleTerminalsWithPmsConnectorNotSupported, useableEntity);
            }

            if (useableEntity.Fields[TerminalFields.HandlingMethod.Name].IsChanged)
            {
                useableEntity.Type = useableEntity.HandlingMethod;
            }

            // Only one device
            if (useableEntity.DeviceId.HasValue &&
                useableEntity.Fields[ClientFields.DeviceId.Name].IsChanged)
            {
                ClientCollection clients = new ClientCollection();
                clients.AddToTransaction(useableEntity);
                PredicateExpression filterClients = new PredicateExpression();
                filterClients.Add(ClientFields.DeviceId == useableEntity.DeviceId);
                if (clients.GetDbCount(filterClients) > 0)
                    throw new ObymobiEntityException(TerminalValidatorResult.DeviceAlreadyConnectedToClient, useableEntity);
                
                terminals.AddToTransaction(useableEntity);
                PredicateExpression filterTerminals = new PredicateExpression();
                filterTerminals.Add(TerminalFields.DeviceId == useableEntity.DeviceId);
                if (!useableEntity.IsNew)
                    filterTerminals.Add(TerminalFields.TerminalId != useableEntity.TerminalId);

                if (terminals.GetDbCount(filterTerminals) > 0)
                    throw new ObymobiEntityException(TerminalValidatorResult.MultipleTerminalsForDevice, useableEntity);
            }

            // Send changes to terminal 
            if (!useableEntity.IsNew && useableEntity.Fields[TerminalFields.MasterTab.Name].IsChanged && !useableEntity.ValidatorOverwriteSendMasterTabChangedNetmessage)
            {
                CometHelper.SetConsoleMasterTab(useableEntity.TerminalId, useableEntity.MasterTab);
            }

            // Log change of DeviceId
            if (!useableEntity.IsNew && useableEntity.Fields[TerminalFields.DeviceId.Name].IsChanged && !useableEntity.ValidatorOverwriteDontLogDeviceChange)
            {
                var log = TerminalHelper.CreateLogForDeviceChange(useableEntity);
                log.AddToTransaction(useableEntity);
                log.Save();

                // Unlinked, fetch old device and send netmessage to that device
                if (!useableEntity.DeviceId.HasValue && useableEntity.Fields[TerminalFields.DeviceId.Name].DbValue != null)
                {    
                    DeviceCollection devices = new DeviceCollection();
                    devices.AddToTransaction(useableEntity);

                    int previousDeviceId = (int)useableEntity.Fields[TerminalFields.DeviceId.Name].DbValue;

                    PredicateExpression filter = new PredicateExpression(DeviceFields.DeviceId == previousDeviceId);
                    DeviceEntity previousDevice = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<DeviceEntity>(filter, devices);
                        
                    if (previousDevice != null)
                    {
                        CometHelper.DeviceUnlinked(useableEntity.TerminalId, previousDevice.Identifier);
                    }
                }
            }

            // Prevent linkage to a Deliverypointgroup of another company
            if (useableEntity.Fields[TerminalFields.DeliverypointgroupId.Name].IsChanged &&
                useableEntity.DeliverypointgroupId.HasValue)
            {
                if (useableEntity.DeliverypointgroupEntity.CompanyId != useableEntity.CompanyId)
                {
                    throw new ObymobiEntityException(TerminalValidatorResult.DeliverypointgroupDoesBelongToOtherCompany, useableEntity);
                }
            }

            // Prevent infinite loops when a terminal gets forwarded
            if (useableEntity.Fields[TerminalFields.ForwardToTerminalId.Name].IsChanged &&
                useableEntity.ForwardToTerminalId.HasValue)
            {
                // Check if Terminal Exists and belongs to this company
                PredicateExpression filterForwardTerminal = new PredicateExpression();
                filterForwardTerminal.Add(TerminalFields.TerminalId == useableEntity.ForwardToTerminalId);
                TerminalEntity forwardToTerminal = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<TerminalEntity>(filterForwardTerminal, terminals);

                if (forwardToTerminal == null)
                {
                    // Forward to terminal does not exist
                    throw new ObymobiEntityException(TerminalValidatorResult.ForwardToTerminalDoesNotExists, useableEntity);
                }
                else if (forwardToTerminal.CompanyId != useableEntity.CompanyId)
                {
                    // Forward to terminal belongs to other company
                    throw new ObymobiEntityException(TerminalValidatorResult.ForwardToTerminalBelongsToOtherCompany, useableEntity);
                }
                else if (forwardToTerminal.HandlingMethod != useableEntity.HandlingMethod)
                {
                    throw new ObymobiEntityException(TerminalValidatorResult.ForwardToTerminalHasADifferentHandlingType, useableEntity);
                }

                // Get count of all Terminals of company so we can detect (and prevent) an infinite loop when checking the linking chain up- / downstream
                PredicateExpression filterTerminalsForCompany = new PredicateExpression();
                filterTerminalsForCompany.Add(TerminalFields.CompanyId == useableEntity.CompanyId);
                int terminalcount = terminals.GetDbCount(filterTerminalsForCompany);

                // Retrieve the Ids of all terminals to which we are forwarding from this terminal on. If it includes itself we can't forward because that would create a loop.
                List<TerminalEntity> terminalIdsForwardingFromThisTerminal = new List<TerminalEntity>();
                TerminalHelper.GetTerminalsForwardingFromThisTerminal(useableEntity, useableEntity.GetCurrentTransaction(), terminalIdsForwardingFromThisTerminal);
                if (terminalIdsForwardingFromThisTerminal.Any(t => t.TerminalId == useableEntity.TerminalId))
                {
                    throw new ObymobiEntityException(TerminalValidatorResult.TerminalForwardWouldCreateInfiniteLoop, useableEntity);
                }
            }

            useableEntity.WasNewBeforeSave = useableEntity.IsNew;

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            TerminalEntity useableEntity = involvedEntity as TerminalEntity;

            // Write a TerminalState entity if this is just created.
            if (useableEntity.WasNewBeforeSave)
            {
                TerminalStateHelper.UpdateTerminalState(useableEntity, false);

                if (!useableEntity.TerminalConfigurationId.HasValue)
                {
                    TerminalConfigurationHelper.CreateFromTerminal(useableEntity);
                }
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }
    }
}
