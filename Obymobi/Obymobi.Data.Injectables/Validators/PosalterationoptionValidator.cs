﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PosalterationoptionEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PosalterationoptionValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            PosalterationoptionEntity useableEntity = involvedEntity as PosalterationoptionEntity;

            foreach (var ao in useableEntity.AlterationoptionCollection)
                ao.Delete();

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}
