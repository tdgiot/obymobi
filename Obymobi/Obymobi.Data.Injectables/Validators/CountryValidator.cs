﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(CountryEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class CountryValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            CountryEntity useableEntity = involvedEntity as CountryEntity;

            // Field 'Name' must have a value
            if (useableEntity.Name.Length == 0)
                throw new ObymobiException(CountrySaveResult.NameEmpty);

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

