﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(SurveyEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class SurveyValidator : GeneralValidator
    {
        public enum SurveyValidatorResult : int
        {
            EmailIsRequiredToSendEmailResults = 200,
            AutomaticSignOnUserRequiredToSendEmailResults = 201,
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            SurveyEntity useableEntity = involvedEntity as SurveyEntity;

            if (useableEntity.EmailResults)
            {
                if (StringUtil.IsNullOrWhiteSpace(useableEntity.Email) && StringUtil.IsNullOrWhiteSpace(useableEntity.CompanyEntity.CorrespondenceEmail))
                    throw new ObymobiEntityException(SurveyValidatorResult.EmailIsRequiredToSendEmailResults, useableEntity);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

