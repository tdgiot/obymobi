﻿using System;
using Dionysos;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ScheduledCommandTaskEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ScheduledCommandTaskValidator : GeneralValidator
    {

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            var useableEntity = involvedEntity as ScheduledCommandTaskEntity;

            // Status           
            if (useableEntity.Active && useableEntity.Status == ScheduledCommandTaskStatus.Inactive)
            {
                // Set the status to 'Pending' if the task has been activated
                useableEntity.Status = ScheduledCommandTaskStatus.Pending;

                if (!useableEntity.IsNew)
                {
                    ScheduledCommandEntity scheduledCommand = new ScheduledCommandEntity();
                    scheduledCommand.Status = ScheduledCommandStatus.Pending;

                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == useableEntity.ScheduledCommandTaskId);
                    filter.Add(ScheduledCommandFields.Status == ScheduledCommandStatus.Inactive);

                    useableEntity.ScheduledCommandCollection.UpdateMulti(scheduledCommand, filter);
                }                
            }
            else if (!useableEntity.Active)
            {
                // Set the status to 'Inactive' again
                useableEntity.Status = ScheduledCommandTaskStatus.Inactive;

                if (!useableEntity.IsNew)
                {
                    ScheduledCommandEntity scheduledCommand = new ScheduledCommandEntity();
                    scheduledCommand.Status = ScheduledCommandStatus.Inactive;

                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == useableEntity.ScheduledCommandTaskId);
                    filter.Add(ScheduledCommandFields.Status == ScheduledCommandStatus.Pending);

                    useableEntity.ScheduledCommandCollection.UpdateMulti(scheduledCommand, filter);
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

