﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(DeliverypointgroupOccupancyEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class DeliverypointgroupOccupancyValidator : GeneralValidator
    {
        public enum DeliverypointgroupOccupancyError
        {
            AmountCanNotBeNegative,
            DateCanNotBeFuture,
            OccupancyWithDateAlreadyExists,
            StopSaving
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            var useableEntity = involvedEntity as DeliverypointgroupOccupancyEntity;

            if (useableEntity.Amount < 0 && !useableEntity.UpdateIfExists)
            {
                throw new ObymobiException(DeliverypointgroupOccupancyError.AmountCanNotBeNegative, "Supplied amount ({0}) is invalid, can not be negative.", useableEntity.Amount);
            }

            if (useableEntity.Amount > 0  && useableEntity.Date > DateTime.Today)
            {
                throw new ObymobiException(DeliverypointgroupOccupancyError.DateCanNotBeFuture, "Occupancy date can not be in the future.");
            }

            if (useableEntity.IsNew || useableEntity.UpdateIfExists)
            {
                var filter = new PredicateExpression(DeliverypointgroupOccupancyFields.DeliverypointgroupId == useableEntity.DeliverypointgroupId);
                filter.Add(DeliverypointgroupOccupancyFields.Date == useableEntity.Date);

                var collection = new DeliverypointgroupOccupancyCollection();
                collection.GetMulti(filter);
                if (collection.Count > 0)
                {
                    if (useableEntity.UpdateIfExists)
                    {
                        var oldEntity = collection[0];
                        if (useableEntity.Amount > 0)
                        {
                            oldEntity.Amount = useableEntity.Amount;
                            oldEntity.Save();
                        }
                        else
                        {
                            oldEntity.Delete();
                        }

                        throw new ObymobiException(DeliverypointgroupOccupancyError.StopSaving);
                    }
                    
                    throw new ObymobiException(DeliverypointgroupOccupancyError.OccupancyWithDateAlreadyExists, "Occupancy with date '{0}' already exists", useableEntity.Date.ToShortDateString());
                }
            }


            if (useableEntity.Amount == 0 && useableEntity.UpdateIfExists)
            {
                throw new ObymobiException(DeliverypointgroupOccupancyError.StopSaving);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}
