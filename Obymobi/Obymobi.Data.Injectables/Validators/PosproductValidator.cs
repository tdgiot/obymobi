﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Integrations.POS;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PosproductEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PosproductValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            PosproductEntity useableEntity = involvedEntity as PosproductEntity;

            // Company Required
            if (useableEntity.CompanyId <= 0)
                throw new ObymobiException(PosproductSaveResult.CompanyIdEmpty);

            // External id required
            if (useableEntity.ExternalId.Length == 0)
                throw new ObymobiException(PosproductSaveResult.ExternalIdEmpty);

            if (useableEntity.IsNew)
            {
                // Check whether the external id already exists
                PredicateExpression filter = new PredicateExpression();
                filter.Add(PosproductFields.CompanyId == useableEntity.CompanyId);
                filter.Add(PosproductFields.ExternalId == useableEntity.ExternalId);

                PosproductCollection posproductCollection = new PosproductCollection();
                posproductCollection.AddToTransaction(useableEntity);
                if (posproductCollection.GetDbCount(filter) > 0)
                    throw new ObymobiException(PosproductSaveResult.ExternalIdAlreadyExists);
            }

            // Field 'Name' must have a value
            if (useableEntity.Name.Length == 0)
                throw new ObymobiException(PosproductSaveResult.NameEmpty);

            // Apply POS specific validation
            PosHelperWrapper.PosproductValidateEntityBeforeSave(useableEntity);

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            PosproductEntity useableEntity = involvedEntity as PosproductEntity;

            // Deleting might be a bit to harsh.
            for(int i = useableEntity.ProductCollection.Count - 1; i >= 0; i--)            
            {
                ProductEntity product = useableEntity.ProductCollection[i];
                product.AddToTransaction(useableEntity);
                product.PosproductId = null;
                product.Visible = false;
                product.Save();
            }

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

