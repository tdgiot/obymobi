﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(VattariffEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class VattariffValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            VattariffEntity useableEntity = involvedEntity as VattariffEntity;

            // Field 'Name' must have a value
            if (useableEntity.Name.Length == 0)
                throw new ObymobiException(VatTariffSaveResult.NameEmpty);

            // Field 'Percentage' must have a value
            if (useableEntity.Percentage == 0)
                throw new ObymobiException(VatTariffSaveResult.PercentageEmptyOrLessThanZero);

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

