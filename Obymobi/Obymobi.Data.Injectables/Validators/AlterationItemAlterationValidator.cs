﻿using System;
using Obymobi.Data.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AlterationitemAlterationEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AlterationitemAlterationValidator : GeneralValidator
    {
        private static readonly AlterationV3CircularDependencyDetector CircularDependencyDetector = new AlterationV3CircularDependencyDetector();

        public enum AlterationitemAlterationValidatorException
        {
            AlterationitemAlterationCircularDependencyDetected
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AlterationitemAlterationEntity useableEntity = involvedEntity as AlterationitemAlterationEntity;

            if (useableEntity.IsNew && CircularDependencyDetector.HasCircularDependency(useableEntity))
            {
                throw new ObymobiException(AlterationitemAlterationValidatorException.AlterationitemAlterationCircularDependencyDetected,
                    $"Alteration '{useableEntity.AlterationEntity.Name} (ID: {useableEntity.AlterationEntity.AlterationId})' is already configured as a parent alteration, " +
                    "thus it can not be linked back to said item");
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}
