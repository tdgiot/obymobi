﻿using System;
using Dionysos;
using Obymobi.Cms.Logic.Sites;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(SiteTemplateEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class SiteTemplateValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            SiteTemplateEntity useableEntity = involvedEntity as SiteTemplateEntity;

            if (useableEntity.Fields[SiteTemplateFields.SiteType.Name].IsChanged)
            { 
                // Check if the new SiteType is compatible
                if (!SiteTemplateHelper.SiteTypesCompatibleWithTemplate(useableEntity).Contains(useableEntity.SiteTypeAsEnum))
                    throw new EntitySaveException("Site Type: '{0}' is not compatible with Site Template: '{1}'", useableEntity.SiteTypeAsEnum, useableEntity.Name);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    
    }
}

