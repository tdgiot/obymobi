﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PosdeliverypointEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PosdeliverypointValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            PosdeliverypointEntity useableEntity = involvedEntity as PosdeliverypointEntity;

            // Company Required
            if (useableEntity.CompanyId <= 0)
                throw new ObymobiException(PosdeliverypointSaveResult.CompanyIdEmpty);

            // External id required
            if (useableEntity.ExternalId.Length == 0)
                throw new ObymobiException(PosdeliverypointSaveResult.ExternalIdEmpty);
            else if (useableEntity.IsNew)
            {
                // Check whether the external id already exists
                PredicateExpression filter = new PredicateExpression();
                filter.Add(PosdeliverypointFields.CompanyId == useableEntity.CompanyId);
                filter.Add(PosdeliverypointFields.ExternalId == useableEntity.ExternalId);

                PosdeliverypointCollection posdeliverypointCollection = new PosdeliverypointCollection();
                posdeliverypointCollection.AddToTransaction(useableEntity);                
                if (posdeliverypointCollection.GetDbCount(filter) > 0)
                    throw new ObymobiException(PosdeliverypointSaveResult.ExternalIdAlreadyExists);
            }

            // Field 'Name' must have a value
            if (useableEntity.Name.Length == 0)
                throw new ObymobiException(PosdeliverypointSaveResult.NameEmpty);

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            PosdeliverypointEntity useableEntity = involvedEntity as PosdeliverypointEntity;

            foreach (var deliverypoint in useableEntity.DeliverypointCollection)
                deliverypoint.Delete();

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

