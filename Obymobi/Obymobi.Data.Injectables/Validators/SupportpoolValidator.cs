﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Cms;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(SupportpoolEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class SupportpoolValidator : GeneralValidator
    {
        public enum SupportpoolValidatorResult
        {
            AnotherSupportpoolAlreadyIsSystemSupportPool = 200
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            SupportpoolEntity useableentity = (SupportpoolEntity)involvedEntity;

            if (useableentity.Fields[SupportpoolFields.SystemSupportPool.FieldIndex].IsChanged && useableentity.SystemSupportPool)
            {
                SupportpoolEntity supportpool = new SupportpoolEntity();
                supportpool.SystemSupportPool = false;

                SupportpoolCollection supportpools = new SupportpoolCollection();
                supportpools.UpdateMulti(supportpool, SupportpoolFields.SupportpoolId != useableentity.SupportpoolId);                
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}
