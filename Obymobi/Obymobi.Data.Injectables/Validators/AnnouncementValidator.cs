﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AnnouncementEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AnnouncementValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AnnouncementEntity useableEntity = involvedEntity as AnnouncementEntity;

            AnnouncementCollection announcements = new AnnouncementCollection();
            announcements.AddToTransaction(useableEntity);

            if (useableEntity.DialogType.HasValue && useableEntity.DialogType.Value == (int)AnnouncementDialogType.ReorderDialog)
            {
                // Only once announcement can exists with dialog type 'ReorderDialog'
                PredicateExpression filterUnique = new PredicateExpression();
                filterUnique.Add(AnnouncementFields.DialogType == AnnouncementDialogType.ReorderDialog);
                filterUnique.Add(AnnouncementFields.CompanyId == useableEntity.CompanyId);
                if (!useableEntity.IsNew)
                    filterUnique.Add(AnnouncementFields.AnnouncementId != useableEntity.AnnouncementId);

                if (announcements.GetDbCount(filterUnique) > 0)
                    throw new ObymobiException(AnnouncementSaveResult.ReorderDialogAnnouncementAlreadyExists);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

