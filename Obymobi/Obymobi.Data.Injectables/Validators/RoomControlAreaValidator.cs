﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using System.Linq;
using Obymobi.Enums;
using Dionysos;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(RoomControlAreaEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class RoomControlAreaValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            RoomControlAreaEntity useableEntity = involvedEntity as RoomControlAreaEntity;
            useableEntity.NameChanged = useableEntity.Fields[RoomControlAreaFields.Name.Name].IsChanged;

            if (useableEntity.Name.IsNullOrWhiteSpace())
            {
                useableEntity.Name = useableEntity.NameSystem;                
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            RoomControlAreaEntity useableEntity = involvedEntity as RoomControlAreaEntity;

            if (useableEntity.ValidatorCreateOrUpdateDefaultAreaLanguage && useableEntity.NameChanged)
            {
                this.UpdateDefaultCustomText(useableEntity);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            RoomControlAreaEntity useableEntity = involvedEntity as RoomControlAreaEntity;

            base.ValidateEntityBeforeDelete(involvedEntity);
        }

        private void UpdateDefaultCustomText(RoomControlAreaEntity roomControlAreaEntity)
        {
            string companyCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            if (!companyCultureCode.IsNullOrWhiteSpace())
            {
                CustomTextEntity customText = roomControlAreaEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCultureCode && x.Type == CustomTextType.RoomControlAreaName);
                if (customText == null)
                {
                    customText = CustomTextHelper.CreateEntity(CustomTextType.RoomControlAreaName, companyCultureCode, roomControlAreaEntity.Name, RoomControlAreaFields.RoomControlAreaId, roomControlAreaEntity.RoomControlAreaId);
                }
                customText.Text = roomControlAreaEntity.Name;
                customText.Save();
            }
        }        
    }
}

