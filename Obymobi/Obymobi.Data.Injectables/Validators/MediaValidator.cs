﻿using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Dionysos;
using Crave.Domain.UseCases;
using Crave.Domain.UseCases.Requests;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(MediaEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class MediaValidator : GeneralValidator
    {
        private readonly SaveMediaForBrandProductUseCase saveMediaForBrandProductUseCase;
        private readonly DeleteMediaForBrandProductUseCase deleteMediaForBrandProductUseCase;

        public MediaValidator()
        {
            this.saveMediaForBrandProductUseCase = new SaveMediaForBrandProductUseCase();
            this.deleteMediaForBrandProductUseCase = new DeleteMediaForBrandProductUseCase();
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            MediaEntity useableEntity = involvedEntity as MediaEntity;

            if (!useableEntity.RelatedEntityId.HasValue)
                throw new ObymobiException(MediaSaveResult.NoForeignKeySet);

            if (useableEntity.RelatedEntityType == EntityType.AttachmentEntity)
            {
                useableEntity.JpgQuality = 70;
            }

            if (useableEntity.MimeType.IsNullOrWhiteSpace())
            { 
                // Required!
                throw new ObymobiEntityException(MediaSaveResult.MimeTypeNotSet, useableEntity);
            }
            else if (useableEntity.Extension.IsNullOrWhiteSpace())
            {
                throw new ObymobiEntityException(MediaSaveResult.ExtensionNotSet, useableEntity);
            }
            else if(!useableEntity.Extension.StartsWith("."))
            {
                throw new ObymobiEntityException(MediaSaveResult.ExtensionInvalidFormat, useableEntity, "Invalid extension: {0}", useableEntity.Extension);
            }

            // To Lower - just to keep it nice
            useableEntity.MimeType = useableEntity.MimeType.ToLower();
            useableEntity.Extension = useableEntity.Extension.ToLower();

            // Overwrite the mrtms from the agnostic media (parent) to this media entity
            if (!useableEntity.IsNew && useableEntity.Fields[MediaFields.AgnosticMediaId.Name].IsChanged && useableEntity.IsCultureSpecific)
            {
                MediaHelper.OverwriteRatiosFromAgnosticMedia(useableEntity);
            }

            if (!useableEntity.IsNew && useableEntity.Fields[MediaFields.ActionSiteId.Name].IsChanged && (useableEntity.ActionSiteId <= 0 || !useableEntity.ActionSiteId.HasValue))
            {
                // Linked to a different site, but the pageId is still set from the old linked site. Reset the actionPageId
                useableEntity.ActionPageId = null;
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            MediaEntity useableEntity = involvedEntity as MediaEntity;

            if (useableEntity.ProductId.HasValue && useableEntity.ProductEntity.BrandId.HasValue)
            {
                this.saveMediaForBrandProductUseCase.Execute(new MediaForBrandProductRequest
                {
                    BrandProductId = useableEntity.ProductId.Value,
                    Media = useableEntity
                });
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            MediaEntity useableEntity = involvedEntity as MediaEntity;

            if (useableEntity.ProductId.HasValue && useableEntity.ProductEntity.BrandId.HasValue)
            {
                this.deleteMediaForBrandProductUseCase.Execute(new MediaForBrandProductRequest
                {
                    BrandProductId = useableEntity.ProductId.Value,
                    Media = useableEntity
                });
            }

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}


