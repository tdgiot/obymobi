﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AlterationLanguageEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AlterationLanguageValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AlterationLanguageEntity useableEntity = involvedEntity as AlterationLanguageEntity;

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

