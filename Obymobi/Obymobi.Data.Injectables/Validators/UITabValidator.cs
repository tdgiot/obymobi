﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;
using Dionysos.Data.LLBLGen;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(UITabEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class UITabValidator : GeneralValidator
    {
        public enum UITabValidatorResult : int
        {
            AutomaticSignOnUserRequiredForCmsLogin = 200,
            TabCaptionAlreadyExistsForThisUIMode = 300,
            SiteIsNotAvailableToCompany = 301,
            CategoryAlreadyUsedForUITabInThisUIMode = 302
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            UITabEntity useableEntity = involvedEntity as UITabEntity;

            // Verify if there are no double UI tab names if set for a Point Of Interest
            if (useableEntity.UIModeId > 0 && !useableEntity.Caption.IsNullOrWhiteSpace())
            {
                if (this.DoesTabCaptionExistInUIMode(useableEntity.UITabId, useableEntity.UIModeId, useableEntity.Caption, useableEntity))
                    throw new ObymobiEntityException(UITabValidatorResult.TabCaptionAlreadyExistsForThisUIMode, "It is not allowed for two UI tabs to have the same caption within a UI mode. Please enter a different caption than: {0}", useableEntity.Caption);
            }

            if (useableEntity.Fields[UITabFields.SiteId.Name].IsChanged && useableEntity.SiteId.HasValue)
            {
                PredicateExpression sitefilter = new PredicateExpression(SiteFields.SiteId == useableEntity.SiteId.Value);
                SiteEntity site = LLBLGenEntityUtil.GetSingleEntityUsingPredicateExpression<SiteEntity>(sitefilter);
                if(site.CompanyId.HasValue && 
                    useableEntity.UIModeEntity.CompanyId > 0 &&
                    site.CompanyId != useableEntity.UIModeEntity.CompanyId)
                {                    
                    throw new ObymobiEntityException(UITabValidatorResult.SiteIsNotAvailableToCompany, "The chosen site '{0}' is linked to Company '{1}' and therefore can't be used by this UItab, '{2}', as it's linked to another company: '{3}'",
                        site.Name, site.CompanyEntity.Name, useableEntity.UITabId, useableEntity.UIModeEntity.CompanyEntity.Name);       
                }
            }

            if (useableEntity.Fields[UITabFields.CategoryId.Name].IsChanged && useableEntity.CategoryId.HasValue)
            {
                // A category can only be used once in a UI mode, the UI on the IRT gets fucked when we have the same category twice
                var filter = new PredicateExpression(UIModeFields.UIModeId == useableEntity.UIModeId);
                filter.Add(UITabFields.CategoryId == useableEntity.CategoryId.Value);
                if (!useableEntity.IsNew)
                    filter.Add(UITabFields.UITabId != useableEntity.UITabId);
                var relations = new RelationCollection(UITabEntityBase.Relations.UIModeEntityUsingUIModeId);
                var uiTabs = new UITabCollection();
                if (uiTabs.GetDbCount(filter, relations) > 0)
                    throw new ObymobiEntityException(UITabValidatorResult.CategoryAlreadyUsedForUITabInThisUIMode, "The chosen category is already linked through another UI tab in this UI mode. Categories can only be linked to an UI tab once in a UI mode.");
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private bool DoesTabCaptionExistInUIMode(int uiTabId, int uiModeId, string caption, UITabEntity uiTabEntity)
        {
            var filter = new PredicateExpression();
            filter.Add(UITabFields.UIModeId == uiModeId);
            filter.AddWithAnd(UITabFields.Caption == caption);
            filter.AddWithAnd(UITabFields.UITabId != uiTabId);

            var tabsCount = new UITabCollection();
            tabsCount.AddToTransaction(uiTabEntity);
            return (tabsCount.GetDbCount(filter) > 0); // Each tab is created two times (1 for each UI mode: Tablet/Mobile)
        }
    }
}

