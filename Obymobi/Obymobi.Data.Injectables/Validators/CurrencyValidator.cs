﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(CurrencyEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class CurrencyValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            CurrencyEntity useableEntity = involvedEntity as CurrencyEntity;

            // Field 'Name' must have a value
            if (useableEntity.Name.Length == 0)
                throw new ObymobiException(CurrencySaveResult.NameEmpty);

            // Field 'Symbol' must have a value
            if (useableEntity.Symbol.Length == 0)
                throw new ObymobiException(CurrencySaveResult.SymbolEmpty);

            // Field 'Code' must have a value
            if (useableEntity.CodeIso4217.IsNullOrWhiteSpace())
                throw new ObymobiException(CurrencySaveResult.CodeEmpty);
            else if (useableEntity.CodeIso4217.Length != 3)
                throw new ObymobiException(CurrencySaveResult.CodeNotExactThreeCharacters);

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

