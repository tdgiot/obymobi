﻿using System;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(UserBrandEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class UserBrandValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            UserBrandEntity useableEntity = (UserBrandEntity)involvedEntity;

            if (useableEntity.Role == BrandRole.Owner)
            {
                // Check if this is the last brand owner 
                if (!useableEntity.BrandEntity.UserBrandCollection.Any(a => a.Role == BrandRole.Owner && a.UserBrandId != useableEntity.UserBrandId))
                {
                    throw new ObymobiException(UserBrandSaveResult.CanNotDeleteLastOwner);
                }
            }

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}