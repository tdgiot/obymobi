﻿using System;
using System.Linq;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PageTemplateEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PageTemplateValidator : GeneralValidator
    {
        public enum PageTemplateValidatorResult
        {                        
            PageTypesDoesntMatchWithAttachedPages = 202,
            PageTemplateIsUsedByPages = 203
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            PageTemplateEntity useableEntity = involvedEntity as PageTemplateEntity;
            useableEntity.NameChanged = useableEntity.Fields[PageTemplateFields.Name.Name].IsChanged;

            // Check if this PageTemplate and the attached pages PageType
            if (useableEntity.Fields[PageTemplateFields.PageType.Name].IsChanged && useableEntity.PageCollection.Count > 0)
            {
                var filter = new PredicateExpression(PageFields.PageTemplateId == useableEntity.PageTemplateId);
                filter.Add(PageFields.PageType != useableEntity.PageType);
                var pages = new PageCollection();                
                if (pages.GetDbCount(filter) > 0)
                {
                    throw new ObymobiEntityException(PageTemplateValidatorResult.PageTypesDoesntMatchWithAttachedPages, "The page type of this page template can't be modified since there are pages attached to it. Check the page template page for more details.");
                }                
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            PageTemplateEntity useableEntity = involvedEntity as PageTemplateEntity;

            if (useableEntity.ValidatorCreateOrUpdatePageTemplateLanguage && useableEntity.NameChanged)
            {
                this.UpdateDefaultCustomText(useableEntity);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            PageTemplateEntity useableEntity = involvedEntity as PageTemplateEntity;

            // Check if there are any pages linked to this template page
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PageFields.PageTemplateId == useableEntity.PageTemplateId);
            
            PageCollection pages = new PageCollection();
            if (pages.GetDbCount(filter) > 0)
            {
                throw new ObymobiEntityException(PageTemplateValidatorResult.PageTemplateIsUsedByPages, "There are pages which are linked to this page template. " +
                                                                                                        "Removing this page template can result in a loss of content for these pages. " +
                                                                                                        "Check the Pages tab to see what pages are linked.");
            }                

            base.ValidateEntityBeforeDelete(involvedEntity);
        }

        private void UpdateDefaultCustomText(PageTemplateEntity pageTemplateEntity)
        {
            string companyCultureCode = CmsSessionHelper.DefaultCultureCodeForCompany;
            if (!companyCultureCode.IsNullOrWhiteSpace())
            {
                CustomTextEntity customText = pageTemplateEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCultureCode && x.Type == CustomTextType.PageTemplateName);
                if (customText == null)
                {
                    customText = CustomTextHelper.CreateEntity(CustomTextType.PageTemplateName, companyCultureCode, pageTemplateEntity.Name, PageTemplateFields.PageTemplateId, pageTemplateEntity.PageTemplateId);
                }
                customText.Text = pageTemplateEntity.Name;
                customText.Save();
            }
        }        
    }
}

