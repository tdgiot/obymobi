﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PosproductPosalterationEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PosproductPosalterationValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            PosproductPosalterationEntity useableEntity = involvedEntity as PosproductPosalterationEntity;

            // Find the related ProductAlteration items to delete
            ProductAlterationCollection productAlterations = new ProductAlterationCollection();

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductAlterationEntity.Relations.ProductEntityUsingProductId);
            relations.Add(ProductEntity.Relations.PosproductEntityUsingPosproductId);
            relations.Add(ProductAlterationEntity.Relations.AlterationEntityUsingAlterationId);
            relations.Add(AlterationEntity.Relations.PosalterationEntityUsingPosalterationId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosproductFields.ExternalId == useableEntity.PosproductExternalId);
            filter.Add(PosalterationFields.ExternalId == useableEntity.PosalterationExternalId);
            
            productAlterations.AddToTransaction(useableEntity);
            productAlterations.GetMulti(filter, relations);
            foreach (var productAlteration in productAlterations)
            {
                productAlteration.BeingDeletedInPosProductPosAlterationDeletionTransaction = true;
                productAlteration.Delete();
            }

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}
