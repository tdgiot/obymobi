﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ProductCategoryEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ProductCategoryValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            ProductCategoryEntity useableEntity = involvedEntity as ProductCategoryEntity;

            // Always add an item to the bottom of the category
            ProductCategoryValidator.MakeLastItemOfCategory(useableEntity);

            // Prevent adding to a Category having childeren
            ProductCategoryValidator.CheckForChildCategories(useableEntity);

            // Keep unique ProductId-CategoryId combination
            ProductCategoryValidator.CheckUniqueProductCategoryCombination(useableEntity);

            // Prevent a product from a different company to be added to a category of another
            if (useableEntity.ProductEntity.CompanyId != useableEntity.CategoryEntity.CompanyId)
                throw new ObymobiException(ProductCategorySaveResult.ProductCategoryLinkOfDifferentCompanies, "Product: {0} (Company: {1}), Category: {2} (Company: {3})",
                    useableEntity.ProductId, useableEntity.ProductEntity.CompanyId, useableEntity.CategoryId, useableEntity.CategoryEntity.CompanyId);

            if (!useableEntity.IsNew)
            {
                useableEntity.CategoryChanged = useableEntity.Fields[(int) ProductCategoryFieldIndex.CategoryId].IsChanged;
                if (useableEntity.CategoryChanged)
                {
                    // Remove all old tags
                    useableEntity.ProductCategoryTagCollection.DeleteMulti();
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            ProductCategoryEntity useableEntity = involvedEntity as ProductCategoryEntity;
            
            if (useableEntity.WasNew || useableEntity.CategoryChanged)
            {
                // Save tags entries for all parent categories
                ProductCategoryTagCollection productCategoryTagsToAdd = CreateProductCategoryTagForParent(useableEntity, useableEntity.CategoryEntity);
                productCategoryTagsToAdd.SaveMulti();
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }
        
        private static ProductCategoryTagCollection CreateProductCategoryTagForParent(ProductCategoryEntity useableEntity, CategoryEntity categoryEntity)
        {
            ProductCategoryTagCollection productCategoryTagsToAdd = new ProductCategoryTagCollection();
            foreach (CategoryTagEntity categoryTagEntity in categoryEntity.CategoryTagCollection)
            {
                ProductCategoryTagEntity productCategoryTagEntity = new ProductCategoryTagEntity();
                productCategoryTagEntity.ProductCategoryEntity = useableEntity;
                productCategoryTagEntity.CategoryId = categoryEntity.CategoryId;
                productCategoryTagEntity.TagId = categoryTagEntity.TagId;

                productCategoryTagsToAdd.Add(productCategoryTagEntity);
            }

            if (categoryEntity.ParentCategoryId.HasValue)
            {
                productCategoryTagsToAdd.AddRange(CreateProductCategoryTagForParent(useableEntity, categoryEntity.ParentCategoryEntity));
            }

            return productCategoryTagsToAdd;
        }

        private static void CheckForChildCategories(ProductCategoryEntity useableEntity)
        {
            CategoryCollection categories = new CategoryCollection();
            categories.AddToTransaction(useableEntity);
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.ParentCategoryId == useableEntity.CategoryId);

            if (categories.GetDbCount(filter) > 0)
                throw new ObymobiException(ProductCategorySaveResult.CategoryContainsSubCategories);
        }

        private static void MakeLastItemOfCategory(ProductCategoryEntity useableEntity)
        {
            if (useableEntity.SortOrder == 0)
            {
                ProductCategoryCollection pcs = new ProductCategoryCollection();
                pcs.AddToTransaction(useableEntity);
                PredicateExpression filterForMax = new PredicateExpression();
                filterForMax.Add(ProductCategoryFields.CategoryId == useableEntity.CategoryId);                
                object max = pcs.GetScalar(ProductCategoryFieldIndex.SortOrder, null, AggregateFunction.Max, filterForMax);
                if (max != DBNull.Value)
                    useableEntity.SortOrder = (int)max + 1;
            }
        }

        private static PredicateExpression CheckUniqueProductCategoryCombination(ProductCategoryEntity useableEntity)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductCategoryFields.ProductId == useableEntity.ProductId);
            filter.Add(ProductCategoryFields.CategoryId == useableEntity.CategoryId);

            ProductCategoryCollection productCategoryCollection = new ProductCategoryCollection();
            productCategoryCollection.AddToTransaction(useableEntity);

            if (!useableEntity.IsNew)
                filter.Add(ProductCategoryFields.ProductCategoryId != useableEntity.ProductCategoryId);

            if (productCategoryCollection.GetDbCount(filter) > 0)
                throw new ObymobiException(ProductCategorySaveResult.ProductIdCategoryIdCombinationNotUnique);
            return filter;
        }
    }
}

