﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Cms;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PointOfInterestLanguageEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PointOfInterestLanguageValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            var useableEntity = (PointOfInterestLanguageEntity)involvedEntity;

            if (useableEntity.Fields[(int)CompanyLanguageFieldIndex.VenuePageDescription].IsChanged)
            {
                // Venue description is changed, check if POI is linked from a site

                var filter = new PredicateExpression();
                filter.Add(PageElementFields.PageElementType == (int)PageElementType.Venue);
                filter.Add(PageElementFields.LanguageId == useableEntity.LanguageId);
                filter.Add(PageElementFields.IntValue2 == useableEntity.PointOfInterestId);

                var relations = new RelationCollection();
                relations.Add(SiteEntityBase.Relations.PageEntityUsingSiteId);
                relations.Add(PageEntityBase.Relations.PageElementEntityUsingPageId);

                var siteCollection = new SiteCollection();
                siteCollection.GetMulti(filter, relations);

                foreach (var siteEntity in siteCollection)
                {
                    // Update site last modified date so site is refreshed when loaded
                    siteEntity.AddToTransaction(useableEntity);
                    siteEntity.LastModifiedUTC = DateTime.UtcNow;
                    siteEntity.Save();
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}
