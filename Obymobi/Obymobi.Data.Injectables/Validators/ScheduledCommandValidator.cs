﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ScheduledCommandEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ScheduledCommandValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            ScheduledCommandEntity useableEntity = involvedEntity as ScheduledCommandEntity;

            // Set the scheduled command to status Pending directly if the parent task is already active
            if (useableEntity.IsNew && useableEntity.ScheduledCommandTaskEntity.Active)
            {
                useableEntity.Status = ScheduledCommandStatus.Pending;
            }

            // Set the sort value
            if (useableEntity.ClientCommand.HasValue)
            {
                int sort = (int)useableEntity.ClientCommand;

                // The emenu should be download and installed at the very end
                // due to the fact that the SupportTools needs a running Emenu to communicate its updateStatus
                if (useableEntity.ClientCommand == ClientCommand.DownloadEmenuUpdate ||
                    useableEntity.ClientCommand == ClientCommand.InstallEmenuUpdate)
                    sort += 50;

                useableEntity.Sort = sort;
            }
            else if (useableEntity.TerminalCommand.HasValue)
            {
                int sort = ((int)useableEntity.TerminalCommand + 100);

                // The console should be download and installed at the very end
                // due to the fact that the SupportTools needs a running Emenu to communicate its updateStatus
                if (useableEntity.TerminalCommand == TerminalCommand.DownloadConsoleUpdate ||
                    useableEntity.TerminalCommand == TerminalCommand.InstallConsoleUpdate)
                    sort += 50;

                useableEntity.Sort = sort;
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            ScheduledCommandEntity useableEntity = involvedEntity as ScheduledCommandEntity;

            // Set the progress of the scheduled command task
            IncludeFieldsList fields = new IncludeFieldsList(ScheduledCommandFields.Status, ScheduledCommandFields.ParentCompanyId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(ScheduledCommandFields.ScheduledCommandTaskId == useableEntity.ScheduledCommandTaskId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ScheduledCommandEntity);
            prefetch.Add(ScheduledCommandEntityBase.PrefetchPathScheduledCommandTaskEntity);

            ScheduledCommandCollection scheduledCommandCollection = new ScheduledCommandCollection();
            scheduledCommandCollection.GetMulti(filter, fields, prefetch);

            useableEntity.ScheduledCommandTaskEntity.CommandCount = scheduledCommandCollection.Count;
            useableEntity.ScheduledCommandTaskEntity.FinishedCount = scheduledCommandCollection.Count(x => x.Status == ScheduledCommandStatus.Expired || x.Status == ScheduledCommandStatus.Failed || x.Status == ScheduledCommandStatus.Succeeded);
            useableEntity.ScheduledCommandTaskEntity.Save();

            base.ValidateEntityAfterSave(involvedEntity);
        }
    }
}

