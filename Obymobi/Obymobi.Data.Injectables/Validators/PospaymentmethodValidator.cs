﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(PospaymentmethodEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class PospaymentmethodValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            PospaymentmethodEntity useableEntity = involvedEntity as PospaymentmethodEntity;
        }
    }
}
