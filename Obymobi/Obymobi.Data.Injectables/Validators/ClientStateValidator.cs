﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ClientStateEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ClientStateValidator : GeneralValidator
    {
        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            ClientStateEntity useableEntity = involvedEntity as ClientStateEntity;

            // Set the new values of the client state to the client entity
            ClientEntity clientEntity = useableEntity.ClientEntity;
            clientEntity.LastStateOnline = useableEntity.Online;
            clientEntity.LastStateOperationMode = useableEntity.OperationMode;
            clientEntity.Save();
            
            base.ValidateEntityAfterSave(involvedEntity);
        }
    }
}

