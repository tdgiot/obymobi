﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(SmsInformationEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class SmsInformationValidator : GeneralValidator
    {
        public enum SmsInformationError
        {
            CannotDeleteDefault
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            var useableEntity = involvedEntity as SmsInformationEntity;

            useableEntity.IsDefaultChanged = useableEntity.Fields[(int)SmsInformationFieldIndex.IsDefault].IsChanged;

            if (useableEntity.Originator.Length > 9)
                useableEntity.Originator = useableEntity.Originator.Substring(0, 9);

            if (useableEntity.BodyText.Length > 130)
                useableEntity.BodyText = useableEntity.BodyText.Substring(0, 130);

            if (!useableEntity.IsDefault && (useableEntity.IsDefaultChanged || useableEntity.IsNew))
            {
                // Check if there is another entity which is set as default
                var filter = new PredicateExpression(SmsInformationFields.IsDefault == true);
                var smsInformationCollection = new SmsInformationCollection();
                var dbCount = smsInformationCollection.GetDbCount(filter);
                if (dbCount == 0)
                {
                    // No entities found, put this one as default
                    useableEntity.IsDefault = true;
                }
            }

            if (!useableEntity.IsNew && useableEntity.IsDefault && useableEntity.IsDefaultChanged)
            {
                // Entity is being updated to be default, remove any linked keywords
                foreach (var keyword in useableEntity.SmsKeywordCollection)
                {
                    keyword.Delete();
                }
            }
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            var useableEntity = involvedEntity as SmsInformationEntity;

            if (useableEntity.IsDefaultChanged && useableEntity.IsDefault)
            {
                // Entity has been set to default, update all other entities to not-default
                var updateEntity = new SmsInformationEntity();
                updateEntity.IsDefault = false;

                var filter = new PredicateExpression();
                filter.Add(SmsInformationFields.SmsInformationId != useableEntity.SmsInformationId);

                var collection = new SmsInformationCollection();
                collection.UpdateMulti(updateEntity, filter);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            var useableEntity = involvedEntity as SmsInformationEntity;

            if (useableEntity.IsDefault)
            {
                throw new ObymobiException(SmsInformationError.CannotDeleteDefault, "Unable to delete SMS information because it is set as default.");
            }

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}
