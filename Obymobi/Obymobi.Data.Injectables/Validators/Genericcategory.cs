﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Crave.Api.Logic;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(GenericcategoryEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class GenericcategoryValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            GenericcategoryEntity useableEntity = involvedEntity as GenericcategoryEntity;

            // Field 'Name' must have a value
            if (useableEntity.Name.Length == 0)
                throw new ObymobiException(GenericcategorySaveResult.NameEmpty);

            // To prevent infinite recursion
            if (useableEntity.ParentGenericcategoryId.HasValue)
            {
                if (useableEntity.GenericcategoryId == useableEntity.ParentGenericcategoryId)
                    throw new ObymobiException(GenericcategorySaveResult.SelfReference);
                else
                {
                    ArrayList underlyingCategories = new ArrayList();
                    underlyingCategories = useableEntity.GetAllUnderlyingGenericccategoryIds(useableEntity, underlyingCategories);
                    if (underlyingCategories.Contains(useableEntity.ParentGenericcategoryId.Value))
                        throw new ObymobiException(GenericcategorySaveResult.CircularReference);
                }
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

