﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Crave.Api.Logic;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AdvertisementEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AdvertisementValidator : GeneralValidator
    {
        public enum AdvertisementValidatorResult
        {
            ProductBelongsToOtherCompany = 200,
            EntertainmentBelongsToOtherCompany = 201,
            ActionCategoryBelongsToOtherCompany = 202,
            ActionEntertainmentBelongsToOtherCompany = 203,
            DeliverypointGroupBelongsToOtherCompany = 204
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AdvertisementEntity useableEntity = involvedEntity as AdvertisementEntity;

            // Validate Company consitency to related Entities
            if (useableEntity.CompanyId.HasValue)
            {
                CheckCompanyConsistency(useableEntity);
            }

            if (!useableEntity.IsNew && useableEntity.Fields[AdvertisementFields.ActionSiteId.Name].IsChanged && (useableEntity.ActionSiteId <= 0 || !useableEntity.ActionSiteId.HasValue))
            {
                // Linked to a different site, but the pageId is still set from the old linked site. Reset the actionPageId
                useableEntity.ActionPageId = null;
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private static void CheckCompanyConsistency(AdvertisementEntity useableEntity)
        {
            int companyId = useableEntity.CompanyId.Value;
            if (useableEntity.ProductId.HasValue &&
                (useableEntity.Fields[AdvertisementFields.ProductId.Name].IsChanged ||
                useableEntity.Fields[CompanyEntertainmentFields.CompanyId.Name].IsChanged))
            {
                if (useableEntity.ProductEntity.CompanyId != companyId)
                    throw new ObymobiEntityException(AdvertisementValidatorResult.ProductBelongsToOtherCompany, useableEntity);
            }

            if (useableEntity.EntertainmentId.HasValue &&
                (useableEntity.Fields[AdvertisementFields.EntertainmentId.Name].IsChanged ||
                useableEntity.Fields[CompanyEntertainmentFields.CompanyId.Name].IsChanged))
            {
                if (useableEntity.EntertainmentEntity.CompanyId.HasValue && useableEntity.EntertainmentEntity.CompanyId != companyId)
                    throw new ObymobiEntityException(AdvertisementValidatorResult.EntertainmentBelongsToOtherCompany, useableEntity);
            }

            if (useableEntity.ActionCategoryId.HasValue &&
                (useableEntity.Fields[AdvertisementFields.ActionCategoryId.Name].IsChanged ||
                useableEntity.Fields[CompanyEntertainmentFields.CompanyId.Name].IsChanged))
            {
                if (useableEntity.ActionCategoryEntity.CompanyId != companyId)
                    throw new ObymobiEntityException(AdvertisementValidatorResult.ActionCategoryBelongsToOtherCompany, useableEntity);
            }

            if (useableEntity.ActionEntertainmentId.HasValue &&
                (useableEntity.Fields[AdvertisementFields.ActionEntertainmentId.Name].IsChanged ||
                useableEntity.Fields[CompanyEntertainmentFields.CompanyId.Name].IsChanged))
            {
                if (useableEntity.ActionEntertainmentEntity.CompanyId.HasValue && useableEntity.ActionEntertainmentEntity.CompanyId != companyId)
                    throw new ObymobiEntityException(AdvertisementValidatorResult.ActionEntertainmentBelongsToOtherCompany, useableEntity);
            }

            if (useableEntity.DeliverypointgroupId.HasValue &&
                (useableEntity.Fields[AdvertisementFields.DeliverypointgroupId.Name].IsChanged ||
                useableEntity.Fields[CompanyEntertainmentFields.CompanyId.Name].IsChanged))
            {
                if (useableEntity.DeliverypointgroupEntity.CompanyId != companyId)
                    throw new ObymobiEntityException(AdvertisementValidatorResult.DeliverypointGroupBelongsToOtherCompany, useableEntity);
            }
        }
    }
}

