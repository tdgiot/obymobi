﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(ActionButtonEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class ActionButtonValidator : GeneralValidator
    {
        public enum ActionButtonValidatorResult
        {
            NoNameSpecified = 200,
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            ActionButtonEntity useableEntity = involvedEntity as ActionButtonEntity;

            if (useableEntity.IsDirty)
                useableEntity.LastModifiedUTC = DateTime.UtcNow;

            if (useableEntity.Name.IsNullOrWhiteSpace())
                throw new ObymobiException(ActionButtonValidatorResult.NoNameSpecified);

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            ActionButtonEntity useableEntity = involvedEntity as ActionButtonEntity;

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

