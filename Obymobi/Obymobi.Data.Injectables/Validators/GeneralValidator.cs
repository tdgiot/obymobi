﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Linq;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Dionysos.Data;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Enums;
using Dionysos.Reflection;
using Obymobi.Logic.Model;
using System.Threading;
using Obymobi.Constants;
using System.IO;
using System.Web;
using Dionysos.Interfaces.Data;

namespace Obymobi.Data.Injectables.Validators
{
    [Serializable]
    [DependencyInjectionInfo(typeof(CommonEntityBase), "Validator", ContextType = DependencyInjectionContextType.Singleton)]
    public class GeneralValidator : ValidatorBase
    {
        #region Methods

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            // Check the company id for company related entities
            this.CheckCompanyIdForCompanyRelatedEntities(involvedEntity);

            // Set the RelevantFieldsChanged and WasNew flag
            ITimestampEntity timestampEntity = involvedEntity as ITimestampEntity;
            if (timestampEntity != null && timestampEntity.TimestampUpdater != null)
            {
                timestampEntity.RelevantFieldsChanged = timestampEntity.TimestampUpdater.RelevantFieldsChanged((IEntity)involvedEntity);
                timestampEntity.WasNew = involvedEntity.IsNew;
            }

            base.ValidateEntityBeforeSave(involvedEntity);

            Object updateVersionsDisabled = Thread.GetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralValidatorDisableUpdateVersions));
            if (updateVersionsDisabled != null && (bool)updateVersionsDisabled)
            {
                // Do nothing
            }
            else
            {
                // GK Temporary solution for Sites & Points of Interest
                LastModifiedHelper.UpdateLastModified(involvedEntity, false);
            }
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            // Update the timestamps after the save
            ITimestampEntity timestampEntity = involvedEntity as ITimestampEntity;
            if (timestampEntity != null && timestampEntity.TimestampUpdater != null && timestampEntity.RelevantFieldsChanged)
                timestampEntity.TimestampUpdater.UpdateTimestamps((IEntity)involvedEntity, timestampEntity.WasNew);

            base.ValidateEntityAfterSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            IEntity useableEntity = involvedEntity as IEntity;
            
            // Update the timestamps before deleting (i.e. deleting an entertainment from an entertainment configuration)
            ITimestampEntity timestampEntity = involvedEntity as ITimestampEntity;
            if (timestampEntity != null && timestampEntity.TimestampUpdater != null)
                timestampEntity.TimestampUpdater.UpdateTimestamps((IEntity)involvedEntity, timestampEntity.WasNew);

            if (useableEntity != null)
            {
                string noActionMessage = DataFactory.EntityUtil.CheckForRestrictedRelatedEntities(useableEntity);
                if (!Instance.Empty(noActionMessage))
                {
                    throw new EntityDeleteException(string.Format("The item cannot be deleted as there are still items which refer to the item to delete:\r\n\r\n{0}", noActionMessage));
                }
                else
                {
                    // Delete the related entities
                    DataFactory.EntityUtil.DeleteRelatedEntities(useableEntity);
                }
            }

            base.ValidateEntityBeforeDelete(involvedEntity);

            Object updateVersionsDisabled = Thread.GetData(Thread.GetNamedDataSlot(ObymobiConstants.GeneralValidatorDisableUpdateVersions));
            if (updateVersionsDisabled != null && (bool)updateVersionsDisabled)
            {
                // Do nothing
            }
            else
            {
                // GK Temporary solution for Sites & Points of Interest
                LastModifiedHelper.UpdateLastModified(involvedEntity, true);
            }
        }

        /// <summary>
        /// We store timestamps of latest changes to certain defined sets of data, these timestamps are updated using this:
        /// </summary>
        private static void UpdateDataVersions(IEntityCore involvedEntity, bool isDeleteAction)
        {
            IEntity useableEntity;
            TimestampsToUpdate updateInfo = new TimestampsToUpdate();

            // Determine what the useableEntity entity is (in case of Media its it's related entity)
            if (involvedEntity.LLBLGenProEntityTypeValue == (int)EntityType.MediaEntity ||
                            involvedEntity.LLBLGenProEntityTypeValue == (int)EntityType.MediaRatioTypeMediaEntity ||
                            involvedEntity.LLBLGenProEntityTypeValue == (int)EntityType.MediaRatioTypeMediaFileEntity ||
                            involvedEntity.LLBLGenProEntityTypeValue == (int)EntityType.MediaCultureEntity)
            {
                // In case it's Media, or a related type of Media, we need to get the related entity and check with that.
                //System.Diagnostics.Debug.WriteLine("MEDIA - " + involvedEntity.LLBLGenProEntityName + " - " + ((IEntity)involvedEntity).PrimaryKeyFields[0].CurrentValue);
                MediaEntity media = GetMediaEntity((IEntity)involvedEntity);

                if (((ITransactionalElement)involvedEntity).Transaction != null)
                {
                    (((ITransactionalElement)involvedEntity).Transaction).Add(media);
                }

                useableEntity = media.RelatedEntity;
                updateInfo.UpdateMedia = true;
            }
            else
            {
                useableEntity = (IEntity)involvedEntity;
            }

            // Init some variables that might be used by multiple cases
            CompanyCollection companies = new CompanyCollection();
            RelationCollection joins = new RelationCollection();
            PredicateExpression filter = new PredicateExpression();
            IncludeFieldsList includeFields = new IncludeFieldsList();
            includeFields.Add(CompanyFields.CompanyId);
            includeFields.Add(CompanyFields.Name);
            includeFields.Add(CompanyFields.CompanyDataLastModifiedUTC);
            includeFields.Add(CompanyFields.CompanyMediaLastModifiedUTC);
            includeFields.Add(CompanyFields.MenuDataLastModifiedUTC);
            includeFields.Add(CompanyFields.MenuMediaLastModifiedUTC);
            includeFields.Add(CompanyFields.DeliverypointDataLastModifiedUTC);
            includeFields.Add(CompanyFields.SurveyDataLastModifiedUTC);
            includeFields.Add(CompanyFields.SurveyMediaLastModifiedUTC);
            includeFields.Add(CompanyFields.AnnouncementDataLastModifiedUTC);
            includeFields.Add(CompanyFields.AnnouncementMediaLastModifiedUTC);
            includeFields.Add(CompanyFields.EntertainmentDataLastModifiedUTC);
            includeFields.Add(CompanyFields.EntertainmentMediaLastModifiedUTC);
            includeFields.Add(CompanyFields.AdvertisementDataLastModifiedUTC);
            includeFields.Add(CompanyFields.AdvertisementMediaLastModifiedUTC);
            includeFields.Add(CompanyFields.Salt);

            if (useableEntity != null)
            {
                ITransaction transaction = ((ITransactionalElement)useableEntity).Transaction;
                if (transaction != null)
                {
                    companies.AddToTransaction(transaction);
                }
            }

            if (useableEntity is CompanyEntity)
            {
                CompanyEntity company = useableEntity as CompanyEntity;

                List<CompanyTimestampsUpdate> updates = new List<CompanyTimestampsUpdate>();
                if (updateInfo.UpdateMedia)
                {
                    // We're not saving a company, but Media', 
                    // so we don't get a loop:
                    company.CompanyDataLastModifiedUTC = DateTime.UtcNow;
                    company.CompanyMediaLastModifiedUTC = company.CompanyDataLastModifiedUTC;
                    AddCompanyTimestampsUpdateToList(updates, company);

                    company.Save();
                }
                else if (!company.PreventCompanyDataLastModifiedUpdate)
                {
                    // It's updating it self, just update the value - 
                    // DON'T save it, would create a loop!
                    company.CompanyDataLastModifiedUTC = DateTime.UtcNow;
                    AddCompanyTimestampsUpdateToList(updates, company);
                }

                // Broadcast                    
                /*if (updates.Count > 0)
                    CometHelper.BroadcastCompanyTimestampsUpdated(updates);*/

                // GK Prevent very hard errors when not defined in an application.
                if (TestUtil.IsPcDeveloper && !ConfigurationManager.IsDefined(ObymobiDataConfigConstants.CompanyDataLastChanged))
                {
                    throw new Exception("Add ObymobiDataConfigInfo to your startup, thanks, GK!");
                }
                else if (ConfigurationManager.IsDefined(ObymobiDataConfigConstants.CompanyDataLastChanged))
                {
                    ConfigurationManager.SetValue(ObymobiDataConfigConstants.CompanyDataLastChanged, company.CompanyDataLastModifiedUTC);
                }
            }
            else if (useableEntity is VattariffEntity)
            {
                VattariffEntity vattariffentity = useableEntity as VattariffEntity;

                // Prepare Joins & Filter
                joins.Add(CompanyEntity.Relations.ProductEntityUsingCompanyId);
                filter.Add(ProductFields.VattariffId == vattariffentity.VattariffId);

                // Fetch relevant Companies & Update those companies' timestamps
                companies.GetMulti(filter, 0, null, joins, null, includeFields, 1, int.MaxValue);

                updateInfo.UpdateMenuData = true;
                GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
            }
            else if (useableEntity is GenericproductEntity)
            {
                GenericproductEntity genericproduct = useableEntity as GenericproductEntity;

                // Prepare Joins & Filter
                joins.Add(CompanyEntity.Relations.ProductEntityUsingCompanyId);
                filter.Add(ProductFields.GenericproductId == genericproduct.GenericproductId);

                // Fetch relevant Companies & Update those companies' timestamps
                companies.GetMulti(filter, 0, null, joins, null, includeFields, 1, int.MaxValue);

                updateInfo.UpdateMenuData = true;
                GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
            }            
            else if (useableEntity is GenericcategoryEntity)
            {
                GenericcategoryEntity genericcategory = useableEntity as GenericcategoryEntity;

                // Prepare Joins & Filter
                joins.Add(CompanyEntity.Relations.CategoryEntityUsingCompanyId);
                filter.Add(CategoryFields.GenericcategoryId == genericcategory.GenericcategoryId);

                // Fetch relevant Companies & Update those companies' timestamps
                companies.GetMulti(filter, 0, null, joins, null, includeFields, 1, int.MaxValue);

                updateInfo.UpdateMenuData = true;
                GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
            }            
            else if (useableEntity is EntertainmentEntity)
            {
                EntertainmentEntity entertainmentEntity = useableEntity as EntertainmentEntity;
                
                joins.Add(CompanyEntityBase.Relations.EntertainmentEntityUsingCompanyId);
                filter.Add(EntertainmentFields.EntertainmentId == entertainmentEntity.EntertainmentId);

                joins.Add(CompanyEntityBase.Relations.DeliverypointgroupEntityUsingCompanyId);
                joins.Add(DeliverypointgroupEntityBase.Relations.DeliverypointgroupEntertainmentEntityUsingDeliverypointgroupId);
                filter.AddWithOr(DeliverypointgroupEntertainmentFields.EntertainmentId == entertainmentEntity.EntertainmentId);

                companies.GetMulti(filter, 0, null, joins, null, includeFields, 1, int.MaxValue);

                joins = new RelationCollection();
                filter = new PredicateExpression();
                filter.Add(CompanyFields.CompanyId != companies.Select(c => c.CompanyId).ToList());
                joins.Add(CompanyEntityBase.Relations.DeliverypointgroupEntityUsingCompanyId);
                joins.Add(DeliverypointgroupEntityBase.Relations.DeliverypointgroupEntertainmentEntityUsingDeliverypointgroupId);
                joins.Add(DeliverypointgroupEntertainmentEntityBase.Relations.EntertainmentEntityUsingEntertainmentId);                
                joins.Add(EntertainmentEntityBase.Relations.EntertainmentDependencyEntityUsingEntertainmentId);
                filter.Add(EntertainmentDependencyFields.DependentEntertainmentId == entertainmentEntity.EntertainmentId);

                CompanyCollection extraCompanies = new CompanyCollection();
                extraCompanies.GetMulti(filter, 0, null, joins, null, includeFields, 1, int.MaxValue);

                companies.AddRange(extraCompanies);

                updateInfo.UpdateMenuData = true;
                updateInfo.UpdateEntertainmentData = true;
                GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
            }
            else if (useableEntity is EntertainmentDependencyEntity)
            {
                EntertainmentDependencyEntity entertainmentDependencyEntity = useableEntity as EntertainmentDependencyEntity;

                joins.Add(CompanyEntityBase.Relations.EntertainmentEntityUsingCompanyId);
                filter.Add(EntertainmentFields.EntertainmentId == entertainmentDependencyEntity.EntertainmentId);

                joins.Add(CompanyEntityBase.Relations.DeliverypointgroupEntityUsingCompanyId);
                joins.Add(DeliverypointgroupEntityBase.Relations.DeliverypointgroupEntertainmentEntityUsingDeliverypointgroupId);
                filter.AddWithOr(DeliverypointgroupEntertainmentFields.EntertainmentId == entertainmentDependencyEntity.EntertainmentId);                

                companies.GetMulti(filter, 0, null, joins, null, includeFields, 1, int.MaxValue);

                updateInfo.UpdateEntertainmentData = true;
                GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
            }
            else if (useableEntity is UIModeEntity)
            {
                UIModeEntity uiModeEntity = useableEntity as UIModeEntity;

                // Prepare Joins & Filter
                joins.Add(CompanyEntity.Relations.UIModeEntityUsingCompanyId);
                filter.Add(UIModeFields.UIModeId == uiModeEntity.UIModeId);

                // Fetch relevant Companies & Update those companies' timestamps
                companies.GetMulti(filter, 0, null, joins, null, includeFields, 1, int.MaxValue);

                updateInfo.UpdateMenuData = true;
                updateInfo.UpdateEntertainmentData = true;
                GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
            }
            else if (useableEntity is AdvertisementEntity)
            {
                AdvertisementEntity advertisementEntity = useableEntity as AdvertisementEntity;

                // Prepare Joins & Filter
                joins.Add(CompanyEntity.Relations.AdvertisementEntityUsingCompanyId);
                filter.Add(AdvertisementFields.AdvertisementId == advertisementEntity.AdvertisementId);

                // Fetch relevant Companies & Update those companies' timestamps
                companies.GetMulti(filter, 0, null, joins, null, includeFields, 1, int.MaxValue);

                // Possible that the advertisement is being attached to a company
                // If that's the case, we need to update the media timestamp cause they are set before the attachment
                if (companies.Count == 0 && advertisementEntity.CompanyId > 0)
                {
                    companies.GetMulti(new PredicateExpression(CompanyFields.CompanyId == advertisementEntity.CompanyId));
                    updateInfo.UpdateMedia = true;
                    updateInfo.UpdateCompanyData = true;
                    updateInfo.UpdateAdvertisementData = true;
                }

                updateInfo.UpdateMenuData = true;
                updateInfo.UpdateAdvertisementData = true;

                GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
            }
            else if (useableEntity is AdvertisementTagAdvertisementEntity)
            {
                AdvertisementTagAdvertisementEntity advertisementTagAdvertisementEntity = useableEntity as AdvertisementTagAdvertisementEntity;

                // Prepare Joins & Filter
                joins.Add(CompanyEntity.Relations.AdvertisementEntityUsingCompanyId);
                filter.Add(AdvertisementFields.AdvertisementId == advertisementTagAdvertisementEntity.AdvertisementId);

                // Fetch relevant Companies & Update those companies' timestamps
                companies.GetMulti(filter, 0, null, joins, null, includeFields, 1, int.MaxValue);

                updateInfo.UpdateMenuData = true;
                GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
            }
            else if (useableEntity is AdvertisementTagCategoryEntity)
            {
                AdvertisementTagCategoryEntity advertisementTagCategoryEntity = useableEntity as AdvertisementTagCategoryEntity;

                // Prepare Joins & Filter
                joins.Add(CompanyEntity.Relations.CategoryEntityUsingCompanyId);
                filter.Add(CategoryFields.CategoryId == advertisementTagCategoryEntity.CategoryId);

                // Fetch relevant Companies & Update those companies' timestamps
                companies.GetMulti(filter, 0, null, joins, null, includeFields, 1, int.MaxValue);

                updateInfo.UpdateMenuData = true;
                GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
            }
            else if (useableEntity is AdvertisementTagProductEntity)
            {
                AdvertisementTagProductEntity advertisementTagProductEntity = useableEntity as AdvertisementTagProductEntity;

                // Prepare Joins & Filter
                joins.Add(CompanyEntity.Relations.ProductEntityUsingCompanyId);
                joins.Add(ProductEntity.Relations.AdvertisementTagProductEntityUsingProductId);

                filter.Add(ProductFields.ProductId == advertisementTagProductEntity.ProductId);

                // Fetch relevant Companies & Update those companies' timestamps
                companies.GetMulti(filter, 0, null, joins, null, includeFields, 1, int.MaxValue);

                updateInfo.UpdateMenuData = true;
                GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
            }
            else if (useableEntity is AdvertisementTagEntertainmentEntity)
            {
                AdvertisementTagEntertainmentEntity advertisementTagEntertainmentEntity = useableEntity as AdvertisementTagEntertainmentEntity;

                // Prepare Joins & Filter
                joins.Add(CompanyEntity.Relations.EntertainmentEntityUsingCompanyId);
                joins.Add(EntertainmentEntity.Relations.AdvertisementTagEntertainmentEntityUsingEntertainmentId);

                filter.Add(EntertainmentFields.EntertainmentId == advertisementTagEntertainmentEntity.EntertainmentId);

                // Fetch relevant Companies & Update those companies' timestamps
                companies.GetMulti(filter, 0, null, joins, null, includeFields, 1, int.MaxValue);

                updateInfo.UpdateEntertainmentData = true;
                GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
            }            
            else if (useableEntity is MessageTemplateEntity)
            {
                if (useableEntity.Fields["CompanyId"].CurrentValue != null)
                {
                    updateInfo.UpdateCompanyData = true;
                    GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
                }
            }
            else if (useableEntity is ProductEntity)
            {
                ProductEntity productEntity = useableEntity as ProductEntity;

                if (productEntity.GenericproductId > 0 && productEntity.GenericproductEntity != null)
                {
                    // Check if product is being attached to a generic product
                    // Or if the product was already attached to it

                    ProductEntity dbProduct = new ProductEntity(productEntity.ProductId);
                    if (dbProduct != null && !dbProduct.GenericproductId.HasValue)
                    {
                        // New generic product that's being linked

                        updateInfo.UpdateMedia = true;
                    }
                    else if (productEntity.GenericproductEntity.MediaCollection.Count > 0)
                    {
                        // If its not a new linking between product and generic product
                        // We're checking of the media of the attached generic product has been modified after the current media menu timestamp of the company    

                        DateTime currentCompanyMenuMediaTimestamp = productEntity.CompanyEntity.MenuMediaLastModifiedUTC;
                        DateTime latestGenericProductMediaTimestamp = currentCompanyMenuMediaTimestamp;

                        foreach (MediaEntity media in productEntity.GenericproductEntity.MediaCollection)
                        {
                            if (media.UpdatedUTC.HasValue && media.UpdatedUTC > latestGenericProductMediaTimestamp)
                                latestGenericProductMediaTimestamp = media.UpdatedUTC.Value;
                            else if (media.CreatedUTC.HasValue && media.CreatedUTC > latestGenericProductMediaTimestamp)
                                latestGenericProductMediaTimestamp = media.CreatedUTC.Value;
                        }

                        if (latestGenericProductMediaTimestamp > currentCompanyMenuMediaTimestamp)
                            updateInfo.UpdateMedia = true;
                    }
                }

                companies.Add(productEntity.CompanyEntity);

                updateInfo.UpdateMenuData = true;
                updateInfo.UpdatePosIntegrationInformation = true;

                GeneralValidator.updateTimeStamps(useableEntity, companies, updateInfo);
            }
            else if (useableEntity is AttachmentEntity && ((AttachmentEntity)useableEntity).PageId.HasValue)
            {
                // Is taken care of in LastModifiedHelper.UpdateLastModified(involvedEntity, false);
            }            
            else if (useableEntity != null)
            {
                // Generic Handling
                IEntityInformation entityInformation = EntityInformationUtil.GetEntityInformation(useableEntity.LLBLGenProEntityName);
                if (entityInformation == null)
                    throw new ObymobiException(UnspecifiedError.Unknown, "EntityInformation not found for: '{0}'. Refresh your EntityInformation and/or add it in de Master database.", useableEntity.LLBLGenProEntityName);

                // Determine if, and what TimeStamps should be updated
                EntityInformationEntity eie = (EntityInformationEntity)entityInformation;
                int? companyId = null;
                updateInfo.UpdateCompanyData = eie.UpdateCompanyDataVersionOnChange;
                updateInfo.UpdateMenuData = eie.UpdateMenuDataVersionOnChange;
                updateInfo.UpdatePosIntegrationInformation = eie.UpdatePosIntegrationInformationOnChange;
                updateInfo.UpdateDeliverypointData = eie.UpdateDeliverypointDataVersionOnChange;
                updateInfo.UpdateSurveyData = eie.UpdateSurveyDataVersionOnChange;
                updateInfo.UpdateAnnouncementData = eie.UpdateAnnouncementDataVersionOnChange;
                updateInfo.UpdateEntertainmentData = eie.UpdateEntertainmentDataVersionOnChange;
                updateInfo.UpdateAdvertisementData = eie.UpdateAdvertisementDataVersionOnChange;

                // Check if we have to check if relevant fields were changed for Company, Menu data or PosIntegrationInformation.
                // Media is ALWAYS relevant, Delete doesn't have to include field changes.
                if (!updateInfo.UpdateMedia && !isDeleteAction && (updateInfo.UpdateCompanyData || updateInfo.UpdateMenuData || updateInfo.UpdatePosIntegrationInformation || updateInfo.UpdateDeliverypointData || updateInfo.UpdateSurveyData || updateInfo.UpdateAnnouncementData || updateInfo.UpdateEntertainmentData || updateInfo.UpdateAdvertisementData))
                    GeneralValidator.RelevantFieldsChanged(useableEntity, eie, ref updateInfo);

                if (updateInfo.UpdateCompanyData || updateInfo.UpdateMenuData || updateInfo.UpdatePosIntegrationInformation || updateInfo.UpdateDeliverypointData || updateInfo.UpdateSurveyData || updateInfo.UpdateAnnouncementData || updateInfo.UpdateEntertainmentData || updateInfo.UpdateAdvertisementData)
                {
                    #region Get the company id

                    bool companyIsNull = false;
                    // We need to update, try to find the related company
                    if (useableEntity is ICompanyRelatedEntity)
                    {
                        // Entity that can look up it's related company
                        ICompanyRelatedEntity companyRelatedEntity = useableEntity as ICompanyRelatedEntity;
                        companyId = companyRelatedEntity.CompanyId;
                    }
                    else if (useableEntity is ICompanyRelatedChildEntity)
                    {
                        // Entity that can look up it's related company
                        ICompanyRelatedChildEntity companyRelatedChildEntity = useableEntity as ICompanyRelatedChildEntity;
                        companyId = companyRelatedChildEntity.ParentCompanyId;
                    }
                    else if (useableEntity is INullableCompanyRelatedEntity)
                    {
                        // Entity that can look up it's related company
                        INullableCompanyRelatedEntity companyRelatedEntity = useableEntity as INullableCompanyRelatedEntity;
                        companyIsNull = !companyRelatedEntity.CompanyId.HasValue;
                        companyId = companyRelatedEntity.CompanyId;
                    }
                    else if (useableEntity is INullableCompanyRelatedChildEntity)
                    {
                        // Entity that can look up it's related company
                        INullableCompanyRelatedChildEntity companyRelatedChildEntity = useableEntity as INullableCompanyRelatedChildEntity;
                        companyIsNull = !companyRelatedChildEntity.ParentCompanyId.HasValue;
                        companyId = companyRelatedChildEntity.ParentCompanyId;
                    }
                    else if (useableEntity is ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity)
                    {
                        // Entity that can look up it's related company
                        ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity companyRelatedChildEntityOrNullableCompanyRelatedChildEntity = useableEntity as ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity;
                        companyIsNull = !companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.ParentCompanyId.HasValue;
                        companyId = companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.ParentCompanyId;
                    }
                    else if (useableEntity.Fields["CompanyId"] != null)
                    {
                        // The entity has a direct relation with Company
                        if (useableEntity.Fields["CompanyId"].CurrentValue != null)
                        {
                            companyId = (int)useableEntity.Fields["CompanyId"].CurrentValue;
                        }
                        else
                        {
                            if (TestUtil.IsPcDeveloper)
                            {
                                if (!useableEntity.Fields["CompanyId"].IsNullable)
                                    throw new NotImplementedException(string.Format("We can't update the Data TimeStamp for an Entity of type '{0}', because the CompanyId is empty.",
                                                                                    useableEntity.LLBLGenProEntityName));
                            }
                        }
                    }
                    // GK I very much disagree with this reflection approach below, you should just implement ICompanyRelatedEntity 
                    // for these cases.
                    else if (Member.HasProperty(useableEntity, "CompanyId"))
                    {
                        if (TestUtil.IsPcDeveloper)
                            throw new NotImplementedException(string.Format("We can't update the Data TimeStamp for an Entity of type '{0}', because the related Company can not be found!", useableEntity.LLBLGenProEntityName));
                        else
                            companyId = (int)Member.InvokeProperty(useableEntity, "CompanyId");
                    }

                    #endregion

                    // Update the timestamps.
                    if (!companyIsNull)
                        updateTimeStamps(useableEntity, companyId, updateInfo);
                }
            }
            else
            {
                // Problem...
            }
        }

        /// <summary>
        /// Check if the changed field are relevant to update the company/menuData TimeStamps
        /// </summary>
        /// <param name="entity">Entity to Check fields of</param>
        /// <param name="entityInformation">Entity information for that entity</param>
        /// <param name="timestampsToUpdate">Timestamps to update</param>
        private static void RelevantFieldsChanged(IEntity entity, EntityInformationEntity entityInformation, ref TimestampsToUpdate timestampsToUpdate)
        {
            // Switches to track if relevant fields (containing data that require updating the timestamp) are chagned
            // For example: The last changed field on a Terminal or Client are non relevant.
            bool relevantFieldsChangedForCompanyData = false;
            bool relevantFieldsChangedForMenuData = false;
            bool relevantFieldsChangedPosintegrationInformation = false;
            bool relevantFieldsChangedDeliverypointData = false;
            bool relevantFieldsChangedSurveyData = false;
            bool relevantFieldsChangedAnnouncementData = false;
            bool relevantFieldsChangedEntertainmentData = false;
            bool relevantFieldsChangedAdvertisementData = false;
            foreach (IEntityField field in entity.Fields)
            {
                // Skip Created, CreatedBy, Updated & UpdatedBy
                if (field.Name.Equals("Created") || field.Name.Equals("CreatedUTC") || field.Name.Equals("CreatedBy") || field.Name.Equals("Updated") || field.Name.Equals("UpdatedUTC") || field.Name.Equals("UpdatedBy"))
                {
                    continue;
                }

                // Check if the field is changed and if we have update information.
                // Not known fields are regarded as NON-Relevant.
                if (field.IsChanged && entityInformation.DatabaseFields.ContainsKey(field.Name))
                {
                    // We found entityfieldinformation meta data, perform check.
                    EntityFieldInformationEntity fieldInfo = (EntityFieldInformationEntity)entityInformation.DatabaseFields[field.Name];

                    if (!fieldInfo.ExcludeForUpdateCompanyDataVersionOnChange)
                        relevantFieldsChangedForCompanyData = true;

                    if (!fieldInfo.ExcludeForUpdateMenuDataVersionOnChange)
                        relevantFieldsChangedForMenuData = true;

                    if (!fieldInfo.ExcludeForUpdatePosIntegrationInformationVersionOnChange)
                        relevantFieldsChangedPosintegrationInformation = true;

                    if (!fieldInfo.ExcludeForUpdateDeliverypointDataVersionOnChange)
                        relevantFieldsChangedDeliverypointData = true;

                    if (!fieldInfo.ExcludeForUpdateSurveyDataVersionOnChange)
                        relevantFieldsChangedSurveyData = true;

                    if (!fieldInfo.ExcludeForUpdateAnnouncementDataVersionOnChange)
                        relevantFieldsChangedAnnouncementData = true;

                    if (!fieldInfo.ExcludeForUpdateEntertainmentDataVersionOnChange)
                        relevantFieldsChangedEntertainmentData = true;

                    if (!fieldInfo.ExcludeForUpdateAdvertisementDataVersionOnChange)
                        relevantFieldsChangedAdvertisementData = true;

                    // When the field generic product id has been set for a Product
                    // update the timestamp for the media, so the media will be retrieved
                    if (field.Name.Equals("GenericproductId"))
                        timestampsToUpdate.UpdateMedia = true;
                }
                else if (field.IsChanged && !entityInformation.DatabaseFields.ContainsKey(field.Name))
                {
                    // Only throw on Dev-PC's,
                    if (TestUtil.IsPcDeveloper)
                        throw new ObymobiException(UnspecifiedError.Unknown, "EntityFieldInformation Missing for: {0}.{1}", entity.LLBLGenProEntityName, field.Name);
                }

                // If we found just a single field that's relevant to what we're looking for
                // we can break, no need to search for more changed fields.
                // Condition is: OR we don't have to search for that data type OR we found a change for that
                if ((!timestampsToUpdate.UpdateCompanyData || relevantFieldsChangedForCompanyData) &&
                    (!timestampsToUpdate.UpdateMenuData || relevantFieldsChangedForMenuData) &&
                    (!timestampsToUpdate.UpdatePosIntegrationInformation || relevantFieldsChangedPosintegrationInformation) &&
                    (!timestampsToUpdate.UpdateDeliverypointData || relevantFieldsChangedDeliverypointData) &&
                    (!timestampsToUpdate.UpdateSurveyData || relevantFieldsChangedSurveyData) &&
                    (!timestampsToUpdate.UpdateAnnouncementData || relevantFieldsChangedAnnouncementData) &&
                    (!timestampsToUpdate.UpdateEntertainmentData || relevantFieldsChangedEntertainmentData) &&
                    (!timestampsToUpdate.UpdateAdvertisementData || relevantFieldsChangedAdvertisementData))
                {
                    break;
                }
            }

            if (timestampsToUpdate.UpdateCompanyData)
                timestampsToUpdate.UpdateCompanyData = relevantFieldsChangedForCompanyData;

            if (timestampsToUpdate.UpdateMenuData)
                timestampsToUpdate.UpdateMenuData = relevantFieldsChangedForMenuData;

            if (timestampsToUpdate.UpdatePosIntegrationInformation)
                timestampsToUpdate.UpdatePosIntegrationInformation = relevantFieldsChangedPosintegrationInformation;

            if (timestampsToUpdate.UpdateDeliverypointData)
                timestampsToUpdate.UpdateDeliverypointData = relevantFieldsChangedDeliverypointData;

            if (timestampsToUpdate.UpdateSurveyData)
                timestampsToUpdate.UpdateSurveyData = relevantFieldsChangedSurveyData;

            if (timestampsToUpdate.UpdateAnnouncementData)
                timestampsToUpdate.UpdateAnnouncementData = relevantFieldsChangedAnnouncementData;

            if (timestampsToUpdate.UpdateEntertainmentData)
                timestampsToUpdate.UpdateEntertainmentData = relevantFieldsChangedEntertainmentData;

            if (timestampsToUpdate.UpdateAdvertisementData)
                timestampsToUpdate.UpdateAdvertisementData = relevantFieldsChangedAdvertisementData;
        }

        private static MediaEntity GetMediaEntity(IEntity useableEntity)
        {
            // Specific logic for media
            MediaEntity media = null;
            if (useableEntity.LLBLGenProEntityTypeValue == (int)EntityType.MediaEntity)
            {
                media = useableEntity as MediaEntity;
            }
            else if (useableEntity.LLBLGenProEntityTypeValue == (int)EntityType.MediaRatioTypeMediaEntity)
            {
                if (((MediaRatioTypeMediaEntity)useableEntity).MediaEntity.IsNew)
                    ((MediaRatioTypeMediaEntity)useableEntity).Refetch();

                media = ((MediaRatioTypeMediaEntity)useableEntity).MediaEntity;
            }
            else if (useableEntity.LLBLGenProEntityTypeValue == (int)EntityType.MediaRatioTypeMediaFileEntity)
            {
                if (((MediaRatioTypeMediaFileEntity)useableEntity).MediaRatioTypeMediaEntity == null || ((MediaRatioTypeMediaFileEntity)useableEntity).MediaRatioTypeMediaEntity.IsNew)
                {
                    ((MediaRatioTypeMediaFileEntity)useableEntity).AlreadyFetchedMediaRatioTypeMediaEntity = false;
                    ((MediaRatioTypeMediaFileEntity)useableEntity).Refetch();
                }

                if (((MediaRatioTypeMediaFileEntity)useableEntity).MediaRatioTypeMediaEntity.MediaEntity.IsNew)
                    ((MediaRatioTypeMediaFileEntity)useableEntity).MediaRatioTypeMediaEntity.Refetch();

                media = ((MediaRatioTypeMediaFileEntity)useableEntity).MediaRatioTypeMediaEntity.MediaEntity;
            }
            else if (useableEntity.LLBLGenProEntityTypeValue == (int)EntityType.MediaCultureEntity)
            {
                if (((MediaCultureEntity)useableEntity).MediaEntity.IsNew)
                    ((MediaCultureEntity)useableEntity).Refetch();

                media = ((MediaCultureEntity)useableEntity).MediaEntity;
            }
            return media;
        }

        private static void updateTimeStamps(IEntity useableEntity, int? companyId, TimestampsToUpdate timestampsToUpdate)
        {
            if (!companyId.HasValue || companyId <= 0)
            {
                // GK I had to choose between two Evils here
                // 1. Throw a very hard error when you can't find the related company in all occasions, which could make our software crash just because we can't update this timestamp
                // 2. Don't throw an error when running in production and taking the risk that updated data might not appear as the timestamp is not updated
                // I choose the latter, someone will notify that an update won't come true and call us, at that point we can solve problem.
                if (useableEntity is EntertainmenturlEntity ||
                    useableEntity is EntertainmentcategoryEntity ||                    
                    useableEntity is PointOfInterestVenueCategoryEntity ||
                    useableEntity is AccessCodePointOfInterestEntity ||
                    useableEntity is AdvertisementTagEntity ||                    
                    useableEntity is UITabEntity ||                    
                    useableEntity is UIFooterItemEntity ||
                    useableEntity is CustomTextEntity)
                {
                    // No worries.
                }
                else if (TestUtil.IsPcDeveloper)
                {
                    if (companyId.HasValue && companyId > 0)
                        throw new NotImplementedException(string.Format("We can't update the Data TimeStamp for an Entity of type '{0}', because there's no link to company found.",
                            useableEntity.LLBLGenProEntityName));
                    else
                        throw new NotImplementedException(string.Format("We can't update the Data TimeStamp for an Entity of type '{0}', because the set companyId <= 0.",
                                                    useableEntity.LLBLGenProEntityName));
                }
            }
            else
            {
                CompanyCollection companies = new CompanyCollection();
                PredicateExpression filter = new PredicateExpression(CompanyFields.CompanyId == companyId.Value);
                ITransaction transaction = ((ITransactionalElement)useableEntity).Transaction;
                companies.AddToTransaction(transaction);
                companies.GetMulti(filter);

                if (companies.Count == 0)
                    throw new ObymobiException(UnspecifiedError.Unknown, "Company with Id '{0}' not found when updating Data TimeStamps", companyId.Value);

                updateTimeStamps(useableEntity, companies, timestampsToUpdate);
            }
        }

        private static void updateTimeStamps(IEntity useableEntity, CompanyCollection companies, TimestampsToUpdate timestampsToUpdate)
        {
            ITransaction transaction = ((ITransactionalElement)useableEntity).Transaction;

            DateTime utcNow = DateTime.UtcNow;
            List<CompanyTimestampsUpdate> updates = new List<CompanyTimestampsUpdate>();
            foreach (CompanyEntity company in companies)
            {
                // Update the required timestamps                
                if (timestampsToUpdate.UpdateCompanyData)
                {
                    // Update CompanyData Timestamp
                    company.CompanyDataLastModifiedUTC = utcNow;
                    if (timestampsToUpdate.UpdateMedia)
                        company.CompanyMediaLastModifiedUTC = utcNow;
                }

                if (timestampsToUpdate.UpdatePosIntegrationInformation)
                {
                    // Update PosIntegrationInformation TimeStamp
                    company.PosIntegrationInfoLastModifiedUTC = utcNow;
                }

                if (timestampsToUpdate.UpdateDeliverypointData)
                {
                    // Update DeliverypointData TimeStamp
                    company.DeliverypointDataLastModifiedUTC = utcNow;
                }

                if (timestampsToUpdate.UpdateSurveyData)
                {
                    // Update SurveyData TimeStamp
                    company.SurveyDataLastModifiedUTC = utcNow;
                    if (timestampsToUpdate.UpdateMedia)
                        company.SurveyMediaLastModifiedUTC = utcNow;
                }

                if (timestampsToUpdate.UpdateAnnouncementData)
                {
                    // Update AnnouncementData TimeStamp
                    company.AnnouncementDataLastModifiedUTC = utcNow;
                    if (timestampsToUpdate.UpdateMedia)
                        company.AnnouncementMediaLastModifiedUTC = utcNow;
                }

                if (timestampsToUpdate.UpdateEntertainmentData)
                {
                    // Update EntertainmentData TimeStamp
                    company.EntertainmentDataLastModifiedUTC = utcNow;
                    if (timestampsToUpdate.UpdateMedia)
                        company.EntertainmentMediaLastModifiedUTC = utcNow;
                }

                if (timestampsToUpdate.UpdateAdvertisementData)
                {
                    // Update AdvertisementData TimeStamp
                    company.AdvertisementDataLastModifiedUTC = utcNow;
                    if (timestampsToUpdate.UpdateMedia)
                        company.AdvertisementMediaLastModifiedUTC = utcNow;
                }

                // Add to the transaction if we have one
                if (transaction != null)
                    transaction.Add(company);

                // Save
                company.PreventCompanyDataLastModifiedUpdate = true;
                AddCompanyTimestampsUpdateToList(updates, company);
                company.Save();
                company.PreventCompanyDataLastModifiedUpdate = false;
            }

            // Update the database wide latest version of CompanyData                    
            if (TestUtil.IsPcDeveloper && !ConfigurationManager.IsDefined(ObymobiDataConfigConstants.CompanyDataLastChanged))
            {
                // GK Prevent very hard errors when not defined in an application by firiing for developers only
                throw new Exception("Add ObymobiDataConfigInfo to your startup, thanks, GK!");
            }
            
            if (ConfigurationManager.IsDefined(ObymobiDataConfigConstants.CompanyDataLastChanged))
            {
                ConfigurationManager.SetValue(ObymobiDataConfigConstants.CompanyDataLastChanged, utcNow);
            }
        }

        public static void AddCompanyTimestampsUpdateToList(List<CompanyTimestampsUpdate> updates, CompanyEntity company)
        {
            // Get the latest update timestamp from the DB Values
            // Retrieve the DBValue for the last modified fields. (So we now what was in the DB)
            List<Object> lastUpdatedTimeStamps = new List<Object>{ company.Fields[CompanyFields.CompanyDataLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.CompanyMediaLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.MenuDataLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.MenuMediaLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.PosIntegrationInfoLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.DeliverypointDataLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.SurveyDataLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.SurveyMediaLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.AnnouncementDataLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.AnnouncementMediaLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.EntertainmentDataLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.EntertainmentMediaLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.AdvertisementDataLastModifiedUTC.Name.ToString()].DbValue,
                company.Fields[CompanyFields.AdvertisementMediaLastModifiedUTC.Name.ToString()].DbValue };

            // Get the max of all values, if a value is null treat it as if it's DateTime.MinValue
            DateTime lastUpdatedMax = DateTime.MinValue;
            foreach (Object lastUpdatedTimeStamp in lastUpdatedTimeStamps)
            {
                if (lastUpdatedTimeStamp != null && (DateTime)lastUpdatedTimeStamp > lastUpdatedMax)
                    lastUpdatedMax = (DateTime)lastUpdatedTimeStamp;
            }

            // Get the latest update timestamp from the current update
            List<DateTime> currentUpdates = new List<DateTime>{ company.CompanyDataLastModifiedUTC, company.CompanyMediaLastModifiedUTC, company.MenuDataLastModifiedUTC, 
                                                                company.MenuMediaLastModifiedUTC, company.PosIntegrationInfoLastModifiedUTC, company.DeliverypointDataLastModifiedUTC,
                                                                company.SurveyDataLastModifiedUTC, company.SurveyMediaLastModifiedUTC, company.AnnouncementDataLastModifiedUTC,
                                                                company.AnnouncementMediaLastModifiedUTC, company.EntertainmentDataLastModifiedUTC, company.EntertainmentMediaLastModifiedUTC,
                                                                company.AdvertisementDataLastModifiedUTC, company.AdvertisementMediaLastModifiedUTC };
            DateTime currentUpdateMax = currentUpdates.Max();

            // To prevent that calling this method multiple times for different entities 
            // from flooding poke-in limit to 1 per 10 seoncds
            if ((currentUpdateMax - lastUpdatedMax).TotalSeconds > 10)
            {
                CompanyTimestampsUpdate update = new CompanyTimestampsUpdate();

                update.CompanyId = company.CompanyId;
                update.CompanyDataLastModifiedTicks = company.CompanyDataLastModifiedUTC.Ticks.ToString();
                update.CompanyMediaLastModifiedTicks = company.CompanyMediaLastModifiedUTC.Ticks.ToString();
                //update.MenuDataLastModifiedTicks = company.MenuDataLastModifiedUTC.Ticks.ToString();
                //update.MenuMediaLastModifiedTicks = company.MenuMediaLastModifiedUTC.Ticks.ToString();
                update.PosIntegrationInformationLastModifiedTicks = company.PosIntegrationInfoLastModifiedUTC.Ticks.ToString();
                update.DeliverypointDataLastModifiedTicks = company.DeliverypointDataLastModifiedUTC.Ticks.ToString();
                update.SurveyDataLastModifiedTicks = company.SurveyDataLastModifiedUTC.Ticks.ToString();
                update.SurveyMediaLastModifiedTicks = company.SurveyMediaLastModifiedUTC.Ticks.ToString();
                update.AnnouncementDataLastModifiedTicks = company.AnnouncementDataLastModifiedUTC.Ticks.ToString();
                update.AnnouncementMediaLastModifiedTicks = company.AnnouncementMediaLastModifiedUTC.Ticks.ToString();
                update.EntertainmentDataLastModifiedTicks = company.EntertainmentDataLastModifiedUTC.Ticks.ToString();
                update.EntertainmentMediaLastModifiedTicks = company.EntertainmentMediaLastModifiedUTC.Ticks.ToString();
                update.AdvertisementDataLastModifiedTicks = company.AdvertisementDataLastModifiedUTC.Ticks.ToString();
                update.AdvertisementMediaLastModifiedTicks = company.AdvertisementMediaLastModifiedUTC.Ticks.ToString();

                updates.Add(update);
            }
        }

        private void CheckCompanyIdForCompanyRelatedEntities(IEntityCore involvedEntity)
        {
            if (involvedEntity is ICompanyRelatedChildEntity)
            {
                ICompanyRelatedChildEntity companyRelatedChildEntity = involvedEntity as ICompanyRelatedChildEntity;
                if (companyRelatedChildEntity != null)
                {
                    if (involvedEntity.IsNew)
                    {
                        if (companyRelatedChildEntity.Parent != null)
                        {
                            ICompanyRelatedEntity parent = this.GetCompanyRelatedParent(companyRelatedChildEntity.Parent);
                            if (parent != null)
                            {
                                companyRelatedChildEntity.ParentCompanyId = parent.CompanyId;
                            }
                        }
                    }
                    else if (!companyRelatedChildEntity.ParentCompanyId.Equals(involvedEntity.Fields["ParentCompanyId"].DbValue))
                    {
                        // ParentCompanyId changed!
                        throw new EntitySaveException("It's not allowed to change the ParentCompanyId for a company related child entity!");
                    }
                }
            }
            else if (involvedEntity is INullableCompanyRelatedChildEntity)
            {
                INullableCompanyRelatedChildEntity nullableCompanyRelatedChildEntity = involvedEntity as INullableCompanyRelatedChildEntity;
                if (nullableCompanyRelatedChildEntity != null)
                {
                    if (involvedEntity.IsNew)
                    {
                        if (nullableCompanyRelatedChildEntity.Parent != null)
                        {
                            INullableCompanyRelatedEntity parent = this.GetNullableCompanyRelatedParent(nullableCompanyRelatedChildEntity.Parent);
                            if (parent != null)
                            {
                                nullableCompanyRelatedChildEntity.ParentCompanyId = parent.CompanyId;
                            }
                        }
                    }
                    else if (nullableCompanyRelatedChildEntity.ParentCompanyId.HasValue && !nullableCompanyRelatedChildEntity.ParentCompanyId.Equals(involvedEntity.Fields["ParentCompanyId"].DbValue))
                    {
                        // ParentCompanyId changed!
                        throw new EntitySaveException("It's not allowed to change the nullable ParentCompanyId for a company related child entity!");
                    }
                }
            }
            else if (involvedEntity is ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity)
            {
                ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity companyRelatedChildEntityOrNullableCompanyRelatedChildEntity = involvedEntity as ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity;
                if (companyRelatedChildEntityOrNullableCompanyRelatedChildEntity != null)
                {
                    if (involvedEntity.IsNew)
                    {
                        if (companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent != null)
                        {
                            if (companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent is ICompanyRelatedEntity ||
                                companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent is ICompanyRelatedChildEntity)
                            {
                                ICompanyRelatedEntity parent = this.GetCompanyRelatedParent(companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent);
                                if (parent != null)
                                {
                                    companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.ParentCompanyId = parent.CompanyId;
                                }
                            }
                            else if (companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent is INullableCompanyRelatedEntity ||
                                     companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent is INullableCompanyRelatedChildEntity)
                            {
                                INullableCompanyRelatedEntity parent = this.GetNullableCompanyRelatedParent(companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent);
                                if (parent != null)
                                {
                                    companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.ParentCompanyId = parent.CompanyId;
                                }
                            }
                            else if (companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent is ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity)
                            {
                                IEntity parent = this.GetCompanyRelatedParentOrNullableCompanyRelatedParent(companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.Parent);
                                if (parent != null)
                                {
                                    object companyId = parent.Fields["CompanyId"].CurrentValue;
                                    companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.ParentCompanyId = (int?)companyId;
                                }
                            }
                        }
                    }
                    else if (companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.ParentCompanyId.HasValue)
                    {
                        string parentCompanyIdFieldName = "ParentCompanyId";
                        if(involvedEntity is MediaEntity)
                        {
                            parentCompanyIdFieldName = "RelatedCompanyId";
                        }
                        if(!companyRelatedChildEntityOrNullableCompanyRelatedChildEntity.ParentCompanyId.Equals(involvedEntity.Fields[parentCompanyIdFieldName].DbValue))
                        {
                            // ParentCompanyId changed!
                            throw new EntitySaveException("It's not allowed to change the nullable ParentCompanyId for a company related child entity!");
                        }
                    }
                }
            }
        }

        private ICompanyRelatedEntity GetCompanyRelatedParent(IEntityCore involvedEntity)
        {
            ICompanyRelatedEntity parent = null;

            if (involvedEntity is ICompanyRelatedEntity)
                parent = involvedEntity as ICompanyRelatedEntity;
            else if (involvedEntity is ICompanyRelatedChildEntity)
            {
                ICompanyRelatedChildEntity child = involvedEntity as ICompanyRelatedChildEntity;
                if (child != null && child.Parent != null)
                    parent = GetCompanyRelatedParent(child.Parent);
            }

            return parent;
        }

        private INullableCompanyRelatedEntity GetNullableCompanyRelatedParent(IEntityCore involvedEntity)
        {
            INullableCompanyRelatedEntity parent = null;

            if (involvedEntity is INullableCompanyRelatedEntity)
                parent = involvedEntity as INullableCompanyRelatedEntity;
            else if (involvedEntity is INullableCompanyRelatedChildEntity)
            {
                INullableCompanyRelatedChildEntity child = involvedEntity as INullableCompanyRelatedChildEntity;
                if (child != null && child.Parent != null)
                    parent = GetNullableCompanyRelatedParent(child.Parent);
            }

            return parent;
        }

        private IEntity GetCompanyRelatedParentOrNullableCompanyRelatedParent(IEntityCore involvedEntity)
        {
            IEntity parent = null;

            INullableBrandRelatedEntity entity = involvedEntity as INullableBrandRelatedEntity;
            if (entity != null && entity.BrandId.HasValue)
            {
                // If the entity is linked to a brand we do not want to also link it to a company
                return null;
            }

            if (involvedEntity is INullableCompanyRelatedEntity)
            {
                parent = involvedEntity as IEntity;
            }
            else if (involvedEntity is ICompanyRelatedEntity)
            {
                parent = involvedEntity as IEntity;
            }
            else if (involvedEntity is INullableCompanyRelatedChildEntity)
            {
                INullableCompanyRelatedChildEntity child = involvedEntity as INullableCompanyRelatedChildEntity;
                if (child != null && child.Parent != null)
                    parent = GetNullableCompanyRelatedParent(child.Parent) as IEntity;
            }
            else if (involvedEntity is ICompanyRelatedChildEntity)
            {
                ICompanyRelatedChildEntity child = involvedEntity as ICompanyRelatedChildEntity;
                if (child != null && child.Parent != null)
                    parent = GetCompanyRelatedParent(child.Parent) as IEntity;
            }
            else if (involvedEntity is ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity)
            {
                ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity child = involvedEntity as ICompanyRelatedChildEntityOrNullableCompanyRelatedChildEntity;
                if (child != null && child.Parent != null)
                    parent = GetCompanyRelatedParentOrNullableCompanyRelatedParent(child.Parent) as IEntity;
            }

            return parent;
        }

        #endregion

        #region Classes

        private class TimestampsToUpdate
        {
            public bool UpdateCompanyData = false;
            public bool UpdateMenuData = false;
            public bool UpdateMedia = false;
            public bool updatePosIntegrationInformation = false;
            public bool UpdatePosIntegrationInformation
            {
                get
                {
                    return this.updatePosIntegrationInformation;
                }
                set
                {
                    this.updatePosIntegrationInformation = value;
                }
            }
            public bool UpdateDeliverypointData = false;
            public bool UpdateSurveyData = false;
            public bool UpdateAnnouncementData = false;
            public bool UpdateEntertainmentData = false;
            public bool UpdateAdvertisementData = false;
        }

        #endregion
    }
}
