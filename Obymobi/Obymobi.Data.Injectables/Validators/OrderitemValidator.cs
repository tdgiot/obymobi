﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(OrderitemEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class OrderitemValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            OrderitemEntity useableEntity = involvedEntity as OrderitemEntity;

            // All Orders must be created using the OrderProcessingHelper
            if (useableEntity.IsNew)
            {
                if (!useableEntity.ValidatedByOrderProcessingHelper)
                {
                    throw new ObymobiException(OrderitemSaveResult.OrderitemIsNotValidatedByOrderProcessingHelper);
                }
            }

            // Don't allow OrderItems to be changed once saved - of course there's always one exception :)
            string changedField;
            if (useableEntity.Fields[(int)OrderitemFields.ProductId.FieldIndex].IsChanged &&
                useableEntity.Fields[(int)OrderitemFields.ProductId.FieldIndex].CurrentValue == null)
            { 
                // Most probably a product is deleted which is ok.
            }
            else if (!useableEntity.IsNew && !useableEntity.OverrideChangeProtection && this.IsNonAllowedFieldChanged(useableEntity, out changedField))
            {
                throw new ObymobiException(OrderitemSaveResult.ChangeNotAllowedToExistingOrderitem, "OrderitemId: {0}", useableEntity.OrderitemId);
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private bool IsNonAllowedFieldChanged(OrderitemEntity orderitem, out string changedFieldName)
        {
            bool toReturn = false;
            changedFieldName = string.Empty;
            OrderitemFieldIndex[] allowedFields = this.FieldsAllowedToChangeWhenNotNew();

            for (int i = 0; i < orderitem.Fields.Count; i++)
            {
                IEntityField field = orderitem.Fields[i];
                if (field.IsChanged)
                {
                    var changedFieldIndex = field.FieldIndex.ToEnum<OrderitemFieldIndex>();                    

                    if (!allowedFields.Any(f => f == changedFieldIndex)) // GK Warning told me this could never be true, let's see if  that's the case
                    {
                        toReturn = true;
                        break;
                    }

                    if (!toReturn)
                    {
                        changedFieldName = field.Name;
                    }
                }
            }

            return toReturn;
        }

        private OrderitemFieldIndex[] FieldsAllowedToChangeWhenNotNew()
        {
            return new OrderitemFieldIndex[] { 
                OrderitemFieldIndex.Guid,
                OrderitemFieldIndex.UpdatedUTC,
                OrderitemFieldIndex.UpdatedBy,
                OrderitemFieldIndex.CategoryId,
                OrderitemFieldIndex.CategoryPath
            };
        }
    }
}

