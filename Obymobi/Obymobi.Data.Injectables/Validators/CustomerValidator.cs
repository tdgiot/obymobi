﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using System.Text.RegularExpressions;
using Dionysos;
using Dionysos.Globalization;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(CustomerEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class CustomerValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            CustomerEntity useableEntity = involvedEntity as CustomerEntity;
        
            // Check whether the customer has a GUID otherwise create one and check if it doesn't already exists
            ValidateGuid(useableEntity);

            // Check whether the email already exists in the database, but not when it's a Otoucho Client
            ValidateEmail(useableEntity);
            
            // Check whether the phonenumber already exists in the database, but not when it's a Otoucho Client
            ValidatePhonenumber(useableEntity);     

            // Validate Country Code
            ValidateCountryCode(useableEntity);
            
            if (useableEntity.CommunicationSalt.IsNullOrWhiteSpace())
                useableEntity.CommunicationSalt = Dionysos.RandomUtil.GetRandomLowerCaseString(256);

            // Field 'Password' must have a value 
            // GK No longer for IsNew entities, because the Salt is based on the CustomerId, so we need to save first.
            if (!useableEntity.IsNew && useableEntity.Password.IsNullOrWhiteSpace())
                throw new ObymobiException(CustomerSaveResult.PasswordEmpty);

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private static void ValidateCountryCode(CustomerEntity useableEntity)
        {
            if (!useableEntity.CountryCode.IsNullOrWhiteSpace() &&
                (useableEntity.IsNew || useableEntity.Fields[(int)CustomerFieldIndex.CountryCode].IsChanged))
            {                
                var country = CountryInformationUtil.GetCountryByLetterCode(useableEntity.CountryCode);
                if (country == null)
                {
                    throw new ObymobiException(CustomerSaveResult.InvalidCountryCode, "Country code: {0}", useableEntity.CountryCode);
                }
            }
        }

        private static void ValidateEmail(CustomerEntity useableEntity)
        {
            if (!useableEntity.Email.IsNullOrWhiteSpace() &&
                (useableEntity.IsNew || useableEntity.Fields[(int)CustomerFieldIndex.Email].IsChanged) &&
                !useableEntity.ClientId.HasValue)
            {
                // Validate the Emailadress
                if(!useableEntity.AnonymousAccount)
                {
                    if (!Dionysos.Text.RegularExpressions.RegEx.IsEmail(useableEntity.Email))
                        throw new ObymobiException(CustomerSaveResult.EmailIncorrectFormat, "Email: {0}", useableEntity.Email);
                }

                CustomerCollection customerCollection = new CustomerCollection();
                customerCollection.AddToTransaction(useableEntity);

                PredicateExpression filter = new PredicateExpression();
                filter.Add(CustomerFields.Email % useableEntity.Email);

                if (customerCollection.GetDbCount(filter) > 0)
                    throw new ObymobiException(CustomerSaveResult.EmailAlreadyInUse);

                // Update Device identifiers
                DeviceCollection devices = new DeviceCollection();
                devices.AddToTransaction(useableEntity);
                if (useableEntity.CustomerId > 0)
                {
                    filter = new PredicateExpression();
                    filter.Add(DeviceFields.CustomerId == useableEntity.CustomerId);

                    devices.GetMulti(filter);

                    foreach (var device in devices)
                    {
                        if (!device.Identifier.Equals(useableEntity.Email))
                        {
                            device.AddToTransaction(useableEntity);
                            device.Identifier = useableEntity.Email;
                            device.Save();
                        }
                    }
                }
            }
            else if (!useableEntity.ClientId.HasValue && useableEntity.Email.IsNullOrWhiteSpace())
            { 
                throw new ObymobiException(CustomerSaveResult.EmailaddressMissing); 
            }
        }

        private static void ValidatePhonenumber(CustomerEntity useableEntity)
        {
            if (!useableEntity.Phonenumber.IsNullOrWhiteSpace() &&
                (useableEntity.IsNew || useableEntity.Fields[(int)CustomerFieldIndex.Phonenumber].IsChanged) &&
                !useableEntity.ClientId.HasValue)
            {
                // Validate the Phonenumber
                // FO: We can't verify the phonenumber this way, what if the customer is located in the netherlands but is using a telephone from a different country?
                //string cleanPhonenumber;
                //if (!CountryInformationUtil.TryStandardizePhonenumber(useableEntity.Phonenumber, out cleanPhonenumber))
                //    throw new ObymobiException(CustomerSaveResult.PhonenumberIncorrectFormat, "Number: {0}", useableEntity.Phonenumber);
                //else
                //    useableEntity.Phonenumber = cleanPhonenumber;

                CustomerCollection customerCollection = new CustomerCollection();
                customerCollection.AddToTransaction(useableEntity);

                PredicateExpression filter = new PredicateExpression();
                filter.Add(CustomerFields.Phonenumber == useableEntity.Phonenumber);
                if (useableEntity.CustomerId > 0)
                {
                    // Exclude the entity itself from the search for the phonenumber
                    filter.Add(CustomerFields.CustomerId != useableEntity.CustomerId);
                }

                if (customerCollection.GetDbCount(filter) > 0)
                    throw new ObymobiException(CustomerSaveResult.PhonenumberAlreadyInUse);
            }
        }

        private void ValidateGuid(CustomerEntity useableEntity)
        {
            var checkForGuidDuplicate = useableEntity.IsNew || useableEntity.GUID.IsNullOrWhiteSpace();

            if (StringUtil.IsNullOrWhiteSpace(useableEntity.GUID))
                useableEntity.GUID = Guid.NewGuid().ToString();

            if (checkForGuidDuplicate)
            {
                var customerCollection = new CustomerCollection();
                while (true)
                {
                    var filter = new PredicateExpression(CustomerFields.GUID == useableEntity.GUID);
                    if (customerCollection.GetDbCount(filter) == 0)
                        break;

                    useableEntity.GUID = Guid.NewGuid().ToString();
                }
            }
        }

        #region Legacy

        //private static void ValidateUsername(CustomerEntity useableEntity)
        //{
        //    // Check if the Username is allowed
        //    bool usernameIsEmail = Dionysos.Text.RegularExpressions.RegEx.IsEmail(useableEntity.Username);
        //    if (usernameIsEmail)
        //    {
        //        // Don't allow an e-mailaddress username if the e-mail address is not set to the same.
        //        if (!useableEntity.Email.Equals(useableEntity.Username))
        //            throw new ObymobiException(CustomerSaveResult.EmailAddressUsernameMustBeEqualToSetEmailAddress);
        //    }

        //    // Check if the username contains more than 4 sequential digits it's the same as the phonenumber, unless it's an email.
        //    int longestNumberSequence = 0;
        //    int currentNumberSequence = 0;
        //    for (int i = 0; i < useableEntity.Username.Length; i++)
        //    {
        //        if (Char.IsDigit(useableEntity.Username[i]))
        //            currentNumberSequence++;
        //        else
        //        {
        //            if (currentNumberSequence > longestNumberSequence)
        //                longestNumberSequence = currentNumberSequence;
        //            currentNumberSequence = 0;
        //        }
        //    }

        //    if (longestNumberSequence > 4 && !usernameIsEmail)
        //    {
        //        if (!useableEntity.Phonenumber.Equals(useableEntity.Username))
        //            throw new ObymobiException(CustomerSaveResult.NotMoreThan4SequentialsDigitsCanBeUserInUsernameUnlessItsEqualToThePhonenumber);
        //    }

        //    // Check for unique username (not for Otoucho) & update related device
        //    if (!useableEntity.Username.IsNullOrWhiteSpace() &&
        //        (useableEntity.IsNew || useableEntity.Fields[(int)CustomerFieldIndex.Username].IsChanged) &&
        //        !useableEntity.ClientId.HasValue)
        //    {
        //        CustomerCollection customerCollection = new CustomerCollection();
        //        customerCollection.AddToTransaction(useableEntity);

        //        PredicateExpression filter = new PredicateExpression();
        //        filter.Add(CustomerFields.Username % useableEntity.Username);

        //        if (customerCollection.GetDbCount(filter) > 0)
        //            throw new ObymobiException(CustomerSaveResult.UsernameAlreadyInUse);

        //        DeviceCollection devices = new DeviceCollection();
        //        devices.AddToTransaction(useableEntity);

        //        // Update Device identifiers
        //        if (useableEntity.CustomerId > 0)
        //        {
        //            filter = new PredicateExpression();
        //            filter.Add(DeviceFields.CustomerId == useableEntity.CustomerId);

        //            devices.GetMulti(filter);

        //            foreach (var device in devices)
        //            {
        //                if (!device.Identifier.Equals(useableEntity.Username))
        //                {
        //                    device.AddToTransaction(useableEntity);
        //                    device.Identifier = useableEntity.Username;
        //                    device.Save();
        //                }
        //            }
        //        }
        //    }
        //}

        #endregion
    }
}

