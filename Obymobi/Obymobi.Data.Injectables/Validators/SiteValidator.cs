﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Cms.Logic.Sites;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(SiteEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class SiteValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            SiteEntity useableEntity = involvedEntity as SiteEntity;

            if (useableEntity.IsDirty)
                useableEntity.LastModifiedUTC = DateTime.UtcNow;

            // If the CompanyId is changed this means all Media must be uploaded again because their folder names changes.
            if (useableEntity.Fields[(int)SiteFieldIndex.CompanyId].IsChanged)
            {
                useableEntity.ValidatorFlagRequeueMedia = true;
            }

            // Ensure Site Type is compatible
            if (useableEntity.Fields[SiteFields.SiteType.Name].IsChanged)
            {
                // Check if the new SiteType is compatible
                if (!SiteHelper.SiteTypesCompatibleWithSite(useableEntity).Contains(useableEntity.SiteTypeAsEnum))
                    throw new EntitySaveException("Site Type: '{0}' is not compatible with Site: '{1}'", useableEntity.SiteTypeAsEnum, useableEntity.Name);
            }

            // CompanyId changed can only if it’s not linked to UITabs of that Company
            if (useableEntity.Fields[SiteFields.CompanyId.Name].IsChanged && useableEntity.CompanyId.HasValue)
            {                 
                // It's only relevant if it's being changed to a value (in case of not a value it would be compatible with any company)
                List<int> companyIds = CmsSessionHelper.CompanyCollectionForUser.Select(c => c.CompanyId).ToList();
                CompanyCollection companiesUsingSiteOnUiTab = SiteHelper.CompaniesUsingSiteAsUITab(useableEntity, useableEntity.GetCurrentTransaction(), companyIds);

                foreach (var company in companiesUsingSiteOnUiTab)
                {
                    if (company.CompanyId != useableEntity.CompanyId.Value)
                    { 
                        string companyNames = String.Join(", ", companiesUsingSiteOnUiTab.Select(x => x.Name));
                        CompanyEntity newCompany = new CompanyEntity(useableEntity.CompanyId.Value);
                        throw new EntitySaveException("Can't set Company of Site '{0}' to '{1}', because it's (also) used by '{2}'", useableEntity.Name, newCompany.Name, companyNames);
                    }
                }                
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityAfterSave(IEntityCore involvedEntity)
        {
            SiteEntity useableEntity = involvedEntity as SiteEntity;

            if (useableEntity.ValidatorFlagRequeueMedia)
            {
                useableEntity.ValidatorFlagRequeueMedia = false;
                SiteHelper.QueueMedia(useableEntity);
            }

            base.ValidateEntityAfterSave(involvedEntity);
        }
    }
}

