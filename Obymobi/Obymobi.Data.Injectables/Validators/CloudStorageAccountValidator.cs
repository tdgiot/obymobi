﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;
using Dionysos.Core;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(CloudStorageAccountEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class CloudStorageAccountValidator : GeneralValidator
    {
        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            CloudStorageAccountEntity useableEntity = involvedEntity as CloudStorageAccountEntity;

            if (useableEntity.Fields[(int)CloudStorageAccountFieldIndex.AccountPassword].IsChanged)
                useableEntity.AccountPassword = Dionysos.Security.Cryptography.Cryptographer.EncryptUsingCBC(useableEntity.AccountPassword);

            base.ValidateEntityBeforeSave(involvedEntity);
        }
    }
}

