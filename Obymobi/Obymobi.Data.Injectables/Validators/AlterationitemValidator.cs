using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(AlterationitemEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class AlterationitemValidator : GeneralValidator
    {
        private static readonly AlterationV3CircularDependencyDetector CircularDependencyDetector = new AlterationV3CircularDependencyDetector();

        public enum AlterationitemValidatorException
        {
            AlterationitemCircularDependencyDetected,
            AlterationitemDifferentCompany
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            AlterationitemEntity useableEntity = involvedEntity as AlterationitemEntity;

            ValidateAlterationHasNoCircularDependency(useableEntity);
            ValidateAlterationAndAlterationitemBelongsToSameCompany(useableEntity);
            ValidatePosalterationitemIsNotLinked(useableEntity);

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        private void ValidateAlterationHasNoCircularDependency(AlterationitemEntity useableEntity)
        {
            if (!useableEntity.IsNew)
            {
                return;
            }

            if (CircularDependencyDetector.HasCircularDependency(useableEntity))
            {
                throw new ObymobiException(AlterationitemValidatorException.AlterationitemCircularDependencyDetected,
                    $"Alteration '{useableEntity.AlterationoptionEntity.Name} (ID: {useableEntity.AlterationoptionEntity.AlterationoptionId})' is already configured on a parent alteration, " +
                    "thus it can not be linked back to said item");
            }
        }

        private void ValidateAlterationAndAlterationitemBelongsToSameCompany(AlterationitemEntity useableEntity)
        {
            if (useableEntity.ParentCompanyId.GetValueOrDefault() == 0)
            {
                return;
            }

            if (useableEntity.AlterationEntity.CompanyId.GetValueOrDefault() == 0)
            {
                return;
            }

            // Prevent an Alteration Item being linked to an Alteration for a different Company
            if (useableEntity.ParentCompanyId != useableEntity.AlterationEntity.CompanyId)
            {
                throw new ObymobiException(AlterationitemValidatorException.AlterationitemDifferentCompany, "Alteration Item: {0} (Company: {1}), Alteration: {2} (Company: {3})",
                    useableEntity.AlterationitemId, useableEntity.ParentCompanyId, useableEntity.AlterationId, useableEntity.AlterationEntity.CompanyId);
            }
        }

        private void ValidatePosalterationitemIsNotLinked(AlterationitemEntity useableEntity)
        {
            if (!useableEntity.PosalterationitemId.HasValue)
            {
                return;
            }

            // Prevent double items (meaning 2 or more items for 1 Posalterationitem
            PredicateExpression filterUniquePosalterationitem = new PredicateExpression();
            filterUniquePosalterationitem.Add(AlterationitemFields.PosalterationitemId == useableEntity.PosalterationitemId);
            filterUniquePosalterationitem.Add(AlterationitemFields.Version == 1);

            if (!useableEntity.IsNew)
                filterUniquePosalterationitem.Add(AlterationitemFields.AlterationitemId != useableEntity.AlterationitemId);

            AlterationitemCollection alterationitems = new AlterationitemCollection();
            alterationitems.AddToTransaction(useableEntity);

            if (alterationitems.GetDbCount(filterUniquePosalterationitem) > 0)
                throw new ObymobiEntityException(AlterationitemSaveResult.MultipleAlterationitemsForPosalterationitem, useableEntity);
        }
    }
}

