﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Dionysos;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(VenueCategoryLanguageEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class VenueCategoryLanguageValidator : GeneralValidator
    {
        public enum VenueCategoryLanguageValidatorResult
        {
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            VenueCategoryLanguageEntity useableEntity = involvedEntity as VenueCategoryLanguageEntity;

            if (useableEntity.IsDirty)
            {
                useableEntity.VenueCategoryEntity.LastModifiedUTC = DateTime.UtcNow;
                useableEntity.VenueCategoryEntity.Save();
            }

            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            VenueCategoryLanguageEntity useableEntity = involvedEntity as VenueCategoryLanguageEntity;

            useableEntity.VenueCategoryEntity.LastModifiedUTC = DateTime.UtcNow;
            useableEntity.VenueCategoryEntity.Save();

            base.ValidateEntityBeforeDelete(involvedEntity);
        }
    }
}

