﻿using System;
using System.Collections.Generic;
using System.Text;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using System.Collections;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Crave.Api.Logic;

namespace Obymobi.Data.Injectables.Validators
{
    [DependencyInjectionInfo(typeof(EntertainmentEntity), "Validator", ContextType = DependencyInjectionContextType.Singleton), Serializable]
    public class EntertainmentValidator : GeneralValidator
    {
        public enum EntertainmentValidatorResult
        {
            EntertainmentIsLinkedToAdvertisementOfOtherCompany = 200,
        }

        public override void ValidateEntityBeforeSave(IEntityCore involvedEntity)
        {
            EntertainmentEntity useableEntity = involvedEntity as EntertainmentEntity;

            // Validate Company consitency to related Entities
            if(!useableEntity.IsNew && useableEntity.EntertainmentId > 0 && useableEntity.CompanyId.HasValue && useableEntity.Fields[EntertainmentFields.CompanyId.Name].IsChanged)
            {
                // Check it it's used in Advertisments of other Companies
                AdvertisementCollection advertisements = new AdvertisementCollection();
                advertisements.AddToTransaction(useableEntity);

                PredicateExpression filter = new PredicateExpression();
                filter.Add(AdvertisementFields.EntertainmentId == useableEntity.EntertainmentId | AdvertisementFields.ActionEntertainmentId == useableEntity.EntertainmentId);
                
                PredicateExpression unrelatedEntityFilter = new PredicateExpression();
                unrelatedEntityFilter.Add(AdvertisementFields.CompanyId != DBNull.Value & AdvertisementFields.CompanyId != useableEntity.CompanyId.Value);
                unrelatedEntityFilter.AddWithOr(AdvertisementFields.DeliverypointgroupId != DBNull.Value & DeliverypointgroupFields.CompanyId != useableEntity.CompanyId.Value);

                filter.AddWithOr(unrelatedEntityFilter);

                RelationCollection joins = new RelationCollection();
                joins.Add(AdvertisementEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

                if (advertisements.GetDbCount(filter, joins) > 0)
                    throw new ObymobiException(EntertainmentValidatorResult.EntertainmentIsLinkedToAdvertisementOfOtherCompany);
            }

            if (useableEntity.Fields[EntertainmentFields.EntertainmentType.Name].IsChanged)
            {
                if (!useableEntity.IsNew && 
                    useableEntity.EntertainmentType != EntertainmentType.CustomBrowser)
                    this.ResetBrowserSettings(useableEntity);

                this.ConfigureBrowserSettings(useableEntity);
            }

            if (useableEntity.ConfigureBrowserSettings)
            {
                this.ResetBrowserSettings(useableEntity);
                this.ConfigureBrowserSettings(useableEntity);
            }
                
            base.ValidateEntityBeforeSave(involvedEntity);
        }

        public override void ValidateEntityBeforeDelete(IEntityCore involvedEntity)
        {
            EntertainmentEntity useableEntity = involvedEntity as EntertainmentEntity;

            useableEntity.AfterDelete += useableEntity_AfterDelete;
            
            base.ValidateEntityBeforeDelete(involvedEntity);
        }

        void useableEntity_AfterDelete(EntertainmentEntity entity)
        {
            if (entity.EntertainmentFileId.HasValue)
            {
                CloudTaskHelper.DeleteEntertainmentFile(null, entity.EntertainmentFileEntity);
            }
        }
        
        private void ResetBrowserSettings(EntertainmentEntity entertainment)
        {
            entertainment.MenuContainer = false;
            entertainment.HomeButton = false;
            entertainment.BackButton = false;
            entertainment.NavigateButton = false;
            entertainment.NavigationBar = false;
            entertainment.TitleAsHeader = false;
            entertainment.RestrictedAccess = false;
        }

        /// <summary>
        /// Configures a browser entertainment with the right settings based on its browser type
        /// </summary>
        /// <param name="entertainment"></param>
        public void ConfigureBrowserSettings(EntertainmentEntity entertainment)
        {
            if (entertainment.EntertainmentType == EntertainmentType.Browser)
            {
                entertainment.MenuContainer = true;
                entertainment.HomeButton = true;
                entertainment.BackButton = true;
                entertainment.NavigateButton = true;
                entertainment.NavigationBar = true;
            }
            else if (entertainment.EntertainmentType == EntertainmentType.RestrictedBrowser)
            {
                entertainment.MenuContainer = true;
                entertainment.HomeButton = true;
                entertainment.BackButton = true;
                entertainment.TitleAsHeader = true;
                entertainment.RestrictedAccess = true;
            }
            else if (entertainment.EntertainmentType == EntertainmentType.Web)
            {
                entertainment.RestrictedAccess = true;
            }
            else if (entertainment.EntertainmentType == EntertainmentType.Cms)
            {
                entertainment.MenuContainer = true;
                entertainment.HomeButton = true;
                entertainment.BackButton = true;
                entertainment.TitleAsHeader = true;
                entertainment.RestrictedAccess = true;
            }
            else if (entertainment.EntertainmentType == EntertainmentType.IpadBrowser)
            {
                entertainment.MenuContainer = true;
                entertainment.BackButton = true;
                entertainment.TitleAsHeader = true;
            }
        }
    }
}

