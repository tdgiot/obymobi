﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Data.Injectables
{
    public class AlterationV3CircularDependencyDetector
    {
        public bool HasCircularDependency(AlterationitemAlterationEntity alterationitemAlteration)
        {
            ICollection<AlterationitemEntity> parentAlterationitems = new List<AlterationitemEntity> { alterationitemAlteration.AlterationitemEntity };
                                                      
            AddParentAlterationitemsLinkedViaNestedAlterations(alterationitemAlteration.AlterationitemEntity.AlterationId, parentAlterationitems);

            return parentAlterationitems.Select(item => item.AlterationId).Contains(alterationitemAlteration.AlterationId);
        }

        public bool HasCircularDependency(AlterationitemEntity alterationitem)
        {
            ICollection<AlterationitemEntity> parentAlterationitems = new List<AlterationitemEntity>();

            AddParentAlterationitemsLinkedViaNestedAlterations(alterationitem.AlterationId, parentAlterationitems);

            return parentAlterationitems.Select(item => item.AlterationoptionId).Contains(alterationitem.AlterationoptionId);
        }

        private static void AddParentAlterationitemsLinkedViaNestedAlterations(int alterationId, ICollection<AlterationitemEntity> parentAlterationitems)
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.AlterationitemAlterationEntity);
            prefetch.Add(AlterationitemAlterationEntityBase.PrefetchPathAlterationitemEntity, new IncludeFieldsList(AlterationitemFields.AlterationId, AlterationitemFields.AlterationoptionId));

            IPredicateExpression filter = new PredicateExpression(AlterationitemAlterationFields.AlterationId == alterationId);

            AlterationitemAlterationCollection alterationitemAlterationCollection = new AlterationitemAlterationCollection();
            alterationitemAlterationCollection.GetMulti(filter, prefetch);

            foreach (AlterationitemAlterationEntity alterationitemAlteration in alterationitemAlterationCollection)
            {
                parentAlterationitems.Add(alterationitemAlteration.AlterationitemEntity);

                AddParentAlterationitemsLinkedViaNestedAlterations(alterationitemAlteration.AlterationitemEntity.AlterationId, parentAlterationitems);
            }
        }
    }
}
