﻿<%@ WebHandler Language="C#" Class="Crave.NocService.Ip" %>

using System;
using System.Web;

namespace Crave.NocService
{
    /// <summary>
    /// Summary description for ip
    /// </summary>
    public class Ip : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write(GetIp(context));
        }

        public static String GetIp(HttpContext context)
        {
            string visitorIPAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = context.Request.ServerVariables["REMOTE_ADDR"];

            if (string.IsNullOrEmpty(visitorIPAddress))
                visitorIPAddress = context.Request.UserHostAddress;

            return visitorIPAddress;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}