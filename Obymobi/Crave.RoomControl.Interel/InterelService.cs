﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Crave.RoomControl.Enums;
using Crave.RoomControl.Interel.Models;
using Crave.RoomControl.Models;
using Crave.RoomControl.Web;
using Dionysos;
using Newtonsoft.Json;
using Serilog;
using FileNotFoundException = System.IO.FileNotFoundException;

namespace Crave.RoomControl.Interel
{
    public class InterelService : IntegrationBase, WebHookServer.IWebHookRequestHandler
    {
        private readonly string interelBaseUrl; // Sandbox: http://94.200.105.118:1224/interelrestapi/v2/api/
        //roomcontrol/

        // Not used to good to have as reference
        private const string APP_KEY = "60000000T2000001";
        private const string PUBLIC_KEY = "<RSAKeyValue><Modulus>y8P2d4rKbRdfoTy9huj5c0VQvuDRVJRAbQN8rnOHQA4yJV4deuSQxs1Jh6gueDNnjanzYmDjhbjhlYC+Ow47VdQX/O5f0U60Hk+3cHgF1Gq9zY1cuQEQ9xtaF/xWuFNjkG//hHY8TgI8GNdG2DddCM2EBZAxOnwcRs+4oVGjJN0=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

        private readonly Dictionary<int, string> roomTokensDictionary = new Dictionary<int, string>();

        private readonly Builder builder;
        private readonly Encrypter encrypter;
        private readonly ILogger logger;

        private WebHookServer webHookServer;

        private InterelService(Builder builder)
        {
            this.logger = builder.Logger;
            this.interelBaseUrl = builder.InterelBaseUrl;

            this.encrypter = new Encrypter(builder.PublicRSAKey);
            this.builder = builder;
        }

        public void Initialize()
        {
            string tokensFilename = this.builder.TokensFilename;
            string accessToken = string.Empty;

            if (!File.Exists(tokensFilename))
            {
                this.logger.Error("[Interel] Access token file '{0}' does not exists", tokensFilename);
                throw new FileNotFoundException(string.Format("Interel access token file '{0}' does not exists", tokensFilename));
            }

            this.logger.Information("[Interel] Loading access tokens from '{file}'.", tokensFilename);

            // Read tokens file
            IEnumerable<string> tokensList = File.ReadLines(tokensFilename);
            foreach (string tokenEntry in tokensList)
            {
                string[] tokenEntrySplit = tokenEntry.Split(',');

                if (int.TryParse(tokenEntrySplit[0], out int roomNumber) && tokenEntrySplit[1].Length > 0)
                {
                    if (string.IsNullOrEmpty(accessToken))
                    {
                        // Save the first token to later use to setup webhooks
                        accessToken = tokenEntrySplit[1];
                    }

                    this.roomTokensDictionary.Add(roomNumber, tokenEntrySplit[1]);
                }
            }

            if (string.IsNullOrEmpty(accessToken))
            {
                this.logger.Error("[Interel] Unable to read one valid access token from file: {0}", tokensFilename);
                throw new ApplicationException(string.Format("Unable to read one valid access token from file: {0}", tokensFilename));
            }

            this.logger.Information("[Interel] Base url: {url}", this.interelBaseUrl);
            TestInterelUrl();

            // Start webhook server
            this.webHookServer = new WebHookServer.Builder(this.encrypter, this.logger)
                                 .SetIncomingUrl(this.builder.IncomingWebHookUrl)
                                 .SetIncomingPort(this.builder.IncomingWebHookPort)
                                 .SetInterelServer(this.interelBaseUrl)
                                 .SetAccessToken(accessToken)
                                 .SetWebHookRequestHandler(this)
                                 .SetOverrideWebHookUrl(this.builder.IncomingWebHookUrlOverride)
                                 .Build();
        }

        public override bool Start()
        {
            this.webHookServer.Start();
            return true;
        }

        public override void Stop() => this.webHookServer.Stop();

        public override void Synchronize()
        {
            // For now we don't sync.
            // Implement if it's an issue if rooms are not pre-cached
        }

        public override Task<Room> RequestRoomAsync(string roomId)
        {
            return Task.Factory.StartNew(() =>
                                         {
                                             DevRoomDetail roomDetail = GetRoomState(roomId);
                                             if (roomDetail != null)
                                             {
                                                 // Create empty room - if not exists - otherwise this method will be called again
                                                 CreateEmptyRoom(roomId);

                                                 return UpdateRoom(roomId, roomDetail);
                                             }

                                             return null;
                                         });
        }

        public void NewWebHookRequest(WebHookRequest request)
        {
            foreach (WebHookNotification notification in request.Notifications)
            {
                Room room = GetRoom(notification.Action);
                if (room == null)
                {
                    continue;
                }

                string notifyCode = notification.NotifyCode;
                if (notifyCode.Equals("RoomStatus", StringComparison.OrdinalIgnoreCase))
                {
                    UpdateRoomStatus(room, notification.State);
                }
                else if (notifyCode.Equals("SetPoint", StringComparison.OrdinalIgnoreCase))
                {
                    UpdateHvacSetPoint(room, notification.Identifier, notification.State);
                }
                else if (notifyCode.Equals("FanMode", StringComparison.OrdinalIgnoreCase))
                {
                    UpdateHvacFanMode(room, notification.Identifier, notification.State);
                }
            }
        }

        public override void HandleDebugRequest(WebRequest request)
        {
            // Does nothing
        }

        protected override void RoomCreated(string roomId)
        {
            DevRoomDetail roomDetail = GetRoomState(roomId);
            if (roomDetail != null)
            {
                UpdateRoom(roomId, roomDetail);
            }
        }

        public override void HandleCommandRequest(Room room, CommandType command, string parameter1, string parameter2, int deviceAddress)
        {
            if (!int.TryParse(parameter1, out int parameter1AsInt))
            {
                try
                {
                    parameter1AsInt = Convert.ToInt32(parameter1.Substring(0, parameter1.IndexOf('.') > 0 ? parameter1.IndexOf('.') : parameter1.Length));
                }
                catch (Exception)
                {
                    parameter1AsInt = 0;
                }
            }

            if (!int.TryParse(parameter2, out int parameter2AsInt))
            {
                parameter2AsInt = 0;
            }

            switch (command)
            {
                case CommandType.HvacSetTargetTemperature:
                    SetHvacTargetTemperature(room, parameter1AsInt, parameter2);
                    break;
                case CommandType.HvacSetMode:
                    SetHvacMode(room, (HvacMode)parameter1AsInt, parameter2);
                    break;
                case CommandType.HvacSetFanMode:
                    SetHvacFanMode(room, (HvacFanMode)parameter1AsInt, parameter2);
                    break;

                case CommandType.BlindOpen:
                case CommandType.BlindClose:
                case CommandType.BlindToggle:
                case CommandType.BlindStop:
                    DrapeAction(room, command, parameter1AsInt, parameter2AsInt);
                    break;
                case CommandType.ToggleLight:
                    ToggleLight(room, parameter1);
                    break;
                case CommandType.LightSetLevel:
                    SetLightLevel(room, parameter1, parameter2AsInt);
                    break;
                case CommandType.SceneActivate:
                    CallScene(room, parameter1);
                    break;
                case CommandType.DoNotDisturbSetState:
                    SetDoNotDisturbState(room, parameter1AsInt);
                    break;
                case CommandType.MakeUpRoomSetState:
                    SetMakeUpRoomState(room, parameter1AsInt);
                    break;
                case CommandType.SceneDeactivate:
                case CommandType.HvacSetScale:
                    this.logger.Information("[Interel] Command not supported: {command}", command);
                    break;
            }
        }

        #region Command implementations

        private void SetHvacTargetTemperature(Room room, int temperature, string hvacId)
        {
            RoomHvac roomHvac = room.GetHvac(hvacId);
            if (roomHvac == null)
            {
                return;
            }

            roomHvac.HvacTemperature = temperature;

            if (roomHvac.HvacMode != HvacMode.Auto)
            {
                this.logger.Information("[Interel] Temp is set while HVAC is off - turning HVAC on");
                roomHvac.HvacMode = HvacMode.Auto;
                SetHvacFanMode(room, HvacFanMode.Auto, hvacId);
            }

            BroadcastAttributeUpdate(room.RoomId, hvacId, AttributeType.HvacTargetTemperature, temperature);

            ChangeSetPoint model = new ChangeSetPoint
            {
                SetPoint = temperature - (decimal)273.15, // Convert Kelvin to Celsius
                ThermostatId = hvacId
            };
            CallRoomControlPut("changeSetpoint", room.RoomIdAsInt, model);
        }

        private void SetHvacMode(Room room, HvacMode mode, string hvacId)
        {
            RoomHvac roomHvac = room.GetHvac(hvacId);
            if (roomHvac == null)
            {
                return;
            }

            if (mode == HvacMode.Off && roomHvac.HvacMode != HvacMode.Off)
            {
                roomHvac.HvacMode = HvacMode.Off;
                SetHvacFanMode(room, HvacFanMode.Off, hvacId);
            }
            else if (mode == HvacMode.Auto && roomHvac.HvacMode != HvacMode.Auto)
            {
                roomHvac.HvacMode = HvacMode.Auto;
                SetHvacFanMode(room, HvacFanMode.Auto, hvacId);
            }
        }

        private void SetHvacFanMode(Room room, HvacFanMode fanMode, string hvacId)
        {
            RoomHvac roomHvac = room.GetHvac(hvacId);
            if (roomHvac == null || roomHvac.HvacFanMode == fanMode)
            {
                return;
            }

            roomHvac.HvacFanMode = fanMode;
            roomHvac.HvacMode = fanMode == HvacFanMode.Off ? HvacMode.Off : HvacMode.Auto;

            BroadcastAttributeUpdate(room.RoomId, hvacId, AttributeType.HvacMode, (int)roomHvac.HvacMode);
            BroadcastAttributeUpdate(room.RoomId, hvacId, AttributeType.HvacFanMode, (int)fanMode);

            SetFanMode model = new SetFanMode
            {
                ThermostatId = hvacId,
                FanMode = fanMode.ToString()
            };
            CallRoomControlPut("setFanMode", room.RoomIdAsInt, model);
        }

        private void DrapeAction(Room room, CommandType command, int drapeId, int isCurtain)
        {
            string blindState;
            if (command == CommandType.BlindOpen)
            {
                blindState = "Open";
            }
            else if (command == CommandType.BlindClose)
            {
                blindState = "Close";
            }
            else if (command == CommandType.BlindStop)
            {
                blindState = "Stop";
            }
            else
            {
                return;
            }

            if (isCurtain == 1)
            {
                SetCurtainState model = new SetCurtainState
                {
                    CurtainId = drapeId.ToString(),
                    State = blindState
                };
                CallRoomControlPut("setCurtainState", room.RoomIdAsInt, model);
            }
            else
            {
                SetBlindState model = new SetBlindState
                {
                    BlindId = drapeId.ToString(),
                    State = blindState
                };
                CallRoomControlPut("setBlindState", room.RoomIdAsInt, model);
            }
        }

        private void ToggleLight(Room room, string lightId)
        {
            RoomLight light = room.GetLight(lightId);
            if (light == null)
            {
                light = new RoomLight(lightId, 0);
                room.Lights.Add(light);
            }
            light.Level = light.Level == 0 ? 100 : 0;

            SetLightLevel(room, lightId, light.Level);
        }

        private void SetLightLevel(Room room, string lightId, int level)
        {
            string state = "OFF";
            if (level > 0)
            {
                state = "ON";
                level = 100;
            }

            RoomLight light = room.GetLight(lightId);
            if (light == null)
            {
                light = new RoomLight(lightId, level);
                room.Lights.Add(light);
            }
            light.Level = level;

            SetLight model = new SetLight
            {
                Light = lightId,
                State = state
            };
            CallRoomControlPut("setLight", room.RoomIdAsInt, model);
        }

        private void CallScene(Room room, string sceneId)
        {
            CallScene model = new CallScene
            {
                SceneName = sceneId
            };
            CallRoomControlPost("callScene", room.RoomIdAsInt, model);
        }

        private void SetDoNotDisturbState(Room room, int state)
        {
            BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.DoNotDisturbState, state);

            SetRoomState model = new SetRoomState
            {
                State = state == 0 ? "Deactivate" : "Activate"
            };
            CallRoomControlPut("setDnd", room.RoomIdAsInt, model);
        }

        private void SetMakeUpRoomState(Room room, int state)
        {
            BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.MakeUpRoomState, state);

            SetRoomState model = new SetRoomState
            {
                State = state == 0 ? "Deactivate" : "Activate"
            };
            CallRoomControlPut("setMur", room.RoomIdAsInt, model);
        }

        #endregion 

        #region WebHook Updates

        private void UpdateRoomStatus(Room room, string value)
        {
            room.DoNotDisturb = value.Equals("SetDnd", StringComparison.OrdinalIgnoreCase);
            room.MakeUpRoom = value.Equals("SetMur", StringComparison.OrdinalIgnoreCase);

            BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.DoNotDisturbState, room.DoNotDisturb ? 1 : 0);
            BroadcastAttributeUpdate(room.RoomId, room.RoomId, AttributeType.MakeUpRoomState, room.MakeUpRoom ? 1 : 0);
        }

        private void UpdateHvacSetPoint(Room room, string hvacId, string value)
        {
            if (!double.TryParse(value, out double setPointValue))
            {
                this.logger.Warning("[Interel] UpdateHvacSetPoint - Failed to parse set-point value: {value}", value);
                return;
            }

            RoomHvac roomHvac = room.Hvacs.FirstOrDefault(h => h.Id == hvacId);
            if (roomHvac != null)
            {
                roomHvac.HvacTemperature = setPointValue + 273.15;

                BroadcastAttributeUpdate(room.RoomId, roomHvac.Id, AttributeType.HvacTargetTemperature, roomHvac.HvacTemperature);
            }
        }

        private void UpdateHvacFanMode(Room room, string hvacId, string value)
        {
            RoomHvac roomHvac = room.Hvacs.FirstOrDefault(h => h.Id == hvacId);
            if (roomHvac != null)
            {
                HvacFanMode fanMode = InterelHelper.ConvertHvacFanMode(value);
                HvacMode hvacMode = fanMode == HvacFanMode.Off ? HvacMode.Off : HvacMode.Auto;

                roomHvac.HvacMode = hvacMode;
                roomHvac.HvacFanMode = fanMode;

                BroadcastAttributeUpdate(room.RoomId, roomHvac.Id, AttributeType.HvacMode, (int)hvacMode);
                BroadcastAttributeUpdate(room.RoomId, roomHvac.Id, AttributeType.HvacFanMode, (int)fanMode);
            }
        }

        #endregion

        private DevRoomDetail GetRoomState(string roomId)
        {
            if (int.TryParse(roomId, out int roomNumber))
            {
                DevRoomDetail roomDetail = CallRoomControlGet<DevRoomDetail>("GetRoomState", roomNumber);

                return roomDetail;
            }

            return null;
        }

        private bool TestInterelUrl()
        {
            string url = string.Format("{0}/RoomControl/getapistatus", this.interelBaseUrl);
            this.logger.Information("[Interel] Testing base URL: {url}", this.interelBaseUrl);

            try
            {
                HttpClient httpClient = new HttpClient();
                string response = httpClient.GetStringAsync(url).Result;
                if (!response.Contains("true", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.logger.Error("[Interel] URL test failed. Check if the server is running. Response: {result}.", response);
                    throw new ApplicationException("Interel url test failed");
                }
            }
            catch (Exception)
            {
                this.logger.Error("[Interel] Base URL test failed. Check if the configured Interel URL ({baseurl}) is correct and if the following URL works in a browser: {url}.", this.interelBaseUrl, url);
                throw;
            }

            return true;
        }

        private T CallRoomControlGet<T>(string method, int roomNumber)
        {
            if (!this.roomTokensDictionary.TryGetValue(roomNumber, out string accessToken))
            {
                this.logger.Warning("[Interel] CallRoomControlGet - Failed to get access token for room: {number}", roomNumber);
                return default(T);
            }

            try
            {
                this.logger.Verbose("[Interel] CallRoomControlGet - {room} - {method}", roomNumber, method);

                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                string response = httpClient.GetStringAsync(string.Format("{0}/RoomControl/{1}", this.interelBaseUrl, method)).Result;

                this.logger.Verbose("[Interel] CallRoomControlGet - {room} - {method} - {json}", roomNumber, method, response);

                try
                {
                    T model = JsonConvert.DeserializeObject<T>(response);
                    return model;
                }
                catch (JsonException ex)
                {
                    this.logger.Warning(ex, "Unable to deserialize: {response}", response);
                }
            }
            catch (Exception e)
            {
                AggregateException exception = e as AggregateException;
                if (exception != null)
                {
                    AggregateException ex = exception.Flatten();
                    this.logger.Warning("[Interel] CallRoomControlGet - Exception: {message}\n{stacktrace}", ex.Message, ex.StackTrace);
                }
                else
                {
                    this.logger.Warning("[Interel] CallRoomControlGet - Exception: {message}", e.GetAllMessages(true));
                }
            }


            return default(T);
        }

        private void CallRoomControlPut<T>(string method, int roomNumber, T model)
        {
            if (!this.roomTokensDictionary.TryGetValue(roomNumber, out string accessToken))
            {
                this.logger.Warning("[Interel] CallRoomControlPut - Failed to get access token for room: {number}", roomNumber);
                return;
            }

            string jsonContent = JsonConvert.SerializeObject(model, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            this.logger.Verbose("[Interel] CallRoomControlPut - {method} - {json}", method, jsonContent);
            string payload = this.encrypter.EncryptPayload(jsonContent);

            try
            {
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

                string url = string.Format("{0}/RoomControl/{1}?payload={2}", this.interelBaseUrl, method, payload);
                this.logger.Verbose("[Interel] CallRoomControlPut - AccessToken: {token}", accessToken);
                this.logger.Verbose("[Interel] CallRoomControlPut - Url: {url}", url);

                HttpResponseMessage response = httpClient.PutAsync(url, new StringContent(payload)).Result;

                string result = response.Content.ReadAsStringAsync().Result;

                this.logger.Debug("[Interel] CallRoomControlPut - {method} - {status} ({code}) - {result}", method, response.StatusCode, (int)response.StatusCode, result);

                if (!response.IsSuccessStatusCode)
                {
                    this.logger.Warning("[Interel] CallRoomControlPut - Failed request: {reason} - {result}", response.ReasonPhrase, result);
                }
            }
            catch (Exception e)
            {
                this.logger.Warning("[Interel] CallRoomControlPut - Exception: {message}", e.ProcessStackTrace(true));
            }
        }

        private void CallRoomControlPost<T>(string method, int roomNumber, T model)
        {
            if (!this.roomTokensDictionary.TryGetValue(roomNumber, out string accessToken))
            {
                this.logger.Warning("[Interel] CallRoomControlPost - Failed to get access token for room: {number}", roomNumber);
                return;
            }

            string jsonContent = JsonConvert.SerializeObject(model, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            this.logger.Verbose("[Interel] CallRoomControlPost - {method} - {json}", method, jsonContent);

            string payload = this.encrypter.EncryptPayload(jsonContent);

            try
            {
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                HttpResponseMessage response = httpClient.PostAsync(string.Format("{0}/RoomControl/{1}?payload={2}", this.interelBaseUrl, method, payload), new StringContent(payload)).Result;
                string result = response.Content.ReadAsStringAsync().Result;

                this.logger.Debug("[Interel] CallRoomControlPost - {method} - {status} ({code}) - {result}", method, response.StatusCode, (int)response.StatusCode, result);

                if (!response.IsSuccessStatusCode)
                {
                    this.logger.Warning("[Interel] CallRoomControlPost - Failed request: {reason} - {result}", response.ReasonPhrase, result);
                }
            }
            catch (Exception e)
            {
                this.logger.Warning("[Interel] CallRoomControlPost - Exception: {message}", e.ProcessStackTrace(true));
            }
        }

        private Room UpdateRoom(string roomId, DevRoomDetail roomDetail)
        {
            Room room = GetRoom(roomId);
            room.DoNotDisturb = roomDetail.Dnd;
            room.MakeUpRoom = roomDetail.Mur;

            if (roomDetail.DevThermos != null)
            {
                foreach (DevRoomDetail.Thermostat thermostat in roomDetail.DevThermos)
                {
                    RoomHvac roomHvac = room.GetHvac(thermostat.ThermostatId);
                    if (roomHvac == null)
                    {
                        roomHvac = new RoomHvac(thermostat.ThermostatId);
                        room.Hvacs.Add(roomHvac);
                    }

                    // Interel HVAC temps are always celsius, easy to convert to Kelvin
                    roomHvac.RoomTemperature = thermostat.ActualTemp + 273.15;
                    roomHvac.HvacTemperature = thermostat.SetPoint + 273.15;
                    roomHvac.HvacFanMode = InterelHelper.ConvertHvacFanMode(thermostat.FanSpeed);
                    roomHvac.HvacMode = roomHvac.HvacFanMode == HvacFanMode.Off ? HvacMode.Off : HvacMode.Auto;
                    roomHvac.HvacScale = HvacScale.Celsius;
                }
            }

            return room;
        }

        public class Builder
        {
            internal ILogger Logger { get; private set; }
            internal string TokensFilename { get; private set; }
            internal string InterelBaseUrl { get; private set; }
            internal string IncomingWebHookUrl { get; private set; }
            internal int IncomingWebHookPort { get; private set; }
            internal string PublicRSAKey { get; private set; }

            internal string IncomingWebHookUrlOverride { get; private set; }

            public Builder(ILogger logger)
            {
                this.Logger = logger;
                this.TokensFilename = "interel_tokens.csv";
                this.IncomingWebHookPort = 8082;
            }

            public Builder SetTokensFilename(string filename)
            {
                if (string.IsNullOrEmpty(filename))
                {
                    filename = "interel_tokens.csv";
                }

                this.TokensFilename = filename.Trim();
                return this;
            }

            public Builder SetInterelServerUrl(string url)
            {
                this.InterelBaseUrl = url.Trim();
                return this;
            }

            public Builder SetIncomingWebHookUrl(string url)
            {
                this.IncomingWebHookUrl = url.Trim();
                return this;
            }

            public Builder SetIncomingWebHookPort(int port)
            {
                this.IncomingWebHookPort = port;
                return this;
            }

            public Builder SetRSAKey(string key)
            {
                this.PublicRSAKey = key.Trim();
                return this;
            }

            public Builder SetOverrideIncomingWebHookUrl(string url)
            {
                this.IncomingWebHookUrlOverride = url;
                return this;
            }

            public InterelService Build()
            {
                if (string.IsNullOrEmpty(this.InterelBaseUrl))
                {
                    throw new Exception("Interel server base URL is not set");
                }

                if (string.IsNullOrEmpty(this.IncomingWebHookUrl))
                {
                    throw new Exception("Incoming WebHook URL is not set");
                }

                if (this.IncomingWebHookPort <= 0 || this.IncomingWebHookPort > 65535)
                {
                    throw new Exception("Incoming WebHook port has to be between 0 and 65535, default is 8082");
                }

                if (string.IsNullOrEmpty(this.PublicRSAKey))
                {
                    throw new Exception("RSA authentication key is not set");
                }

                return new InterelService(this);
            }
        }
    }
}
