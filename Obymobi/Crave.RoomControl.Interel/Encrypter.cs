﻿using System.Security.Cryptography;
using System.Text;

namespace Crave.RoomControl.Interel
{
    public class Encrypter
    {
        private static readonly char[] hexLookup = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

        private readonly string publicKey;

        public Encrypter(string publicKey)
        {
            this.publicKey = publicKey;
        }

        public string EncryptPayload(string dataToEncrypt)
        {
            byte[] encryptedData = Encrypt(dataToEncrypt, this.publicKey);

            return ToHex(encryptedData);
        }

        /// <summary>
        /// Encrypts the specified data using the public key.
        /// </summary>
        /// <param name="dataToEncrypt">The data to encrypt.</param>
        /// <param name="publicKey">The public key.</param>
        /// <returns>The encrypted bytes</returns>
        private static byte[] Encrypt(string dataToEncrypt, string publicKey)
        {
            UTF8Encoding byteConverter = new UTF8Encoding();
            byte[] bytesToEncrypt = byteConverter.GetBytes(dataToEncrypt);

            // create a new instance of RSACryptoServiceProvider.
            RSACryptoServiceProvider rsaCryptoServiceProvider = new RSACryptoServiceProvider();
            rsaCryptoServiceProvider.FromXmlString(publicKey);
            byte[] encryptedBytes = rsaCryptoServiceProvider.Encrypt(bytesToEncrypt, false);
            rsaCryptoServiceProvider.Clear();
            
            return encryptedBytes;
        }

        /// <summary>
        /// Converts a <see cref="T:byte[]"/> to a hex-encoded string.
        /// </summary>
        /// <param name="data">The byte array to be converted.</param>
        /// <returns>The generated encoded string.</returns>
        private static string ToHex(byte[] data)
        {
            if (data == null)
            {
                return string.Empty;
            }

            char[] content = new char[data.Length * 2];
            int output = 0;
            foreach (byte t in data)
            {
                var d = t;
                content[output++] = hexLookup[d / 0x10];
                content[output++] = hexLookup[d % 0x10];
            }
            return new string(content);
        }
    }
}