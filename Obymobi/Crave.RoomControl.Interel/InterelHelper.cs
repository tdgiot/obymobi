﻿using System;
using Crave.RoomControl.Enums;

namespace Crave.RoomControl.Interel
{
    public class InterelHelper
    {
        public static HvacFanMode ConvertHvacFanMode(string interelMode)
        {
            if (interelMode.Equals("Low", StringComparison.InvariantCultureIgnoreCase))
            {
                return HvacFanMode.Low;
            }
            if (interelMode.Equals("Medium", StringComparison.InvariantCultureIgnoreCase))
            {
                return HvacFanMode.Medium;
            }
            if (interelMode.Equals("High", StringComparison.InvariantCultureIgnoreCase))
            {
                return HvacFanMode.High;
            }
            if (interelMode.Equals("Off", StringComparison.InvariantCultureIgnoreCase))
            {
                return HvacFanMode.Off;
            }
            
            return HvacFanMode.Auto;
        }
    }
}