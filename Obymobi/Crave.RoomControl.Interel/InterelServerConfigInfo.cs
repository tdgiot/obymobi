﻿using Dionysos.Configuration;

namespace Crave.RoomControl.Interel
{
    public class InterelServerConfigInfo : ConfigurationItemCollection
    {
        private const string SECTION_NAME = "InterelServerConfig";

        public InterelServerConfigInfo()
        {
            Add(new ConfigurationItem(SECTION_NAME, Constants.INTEREL_SERVER, "Url of the Interel server", "", typeof(string)));
            Add(new ConfigurationItem(SECTION_NAME, Constants.PUBLIC_RSA_KEY, "Public RSA key to for secure communication with Interel server (<![CDATA[RSA_KEY_VALUE_HERE]]>)", "", typeof(string)));
            Add(new ConfigurationItem(SECTION_NAME, Constants.TOKENS_FILE, "CSV file containing room number Interel access tokens (default = interel_tokens.csv)", "interel_tokens.csv", typeof(string)));
            Add(new ConfigurationItem(SECTION_NAME, Constants.INCOMING_WEBHOOK_URL, "URL on which Interel sends notifications to the RoomControlServer", "", typeof(string)));
            Add(new ConfigurationItem(SECTION_NAME, Constants.INCOMING_WEBHOOK_PORT, "Port on which Interel sends notifications to the RoomControlServer (default = 8082)", 8082, typeof(int)));
            Add(new ConfigurationItem(SECTION_NAME, Constants.INCOMING_WEBHOOK_URL_OVERRIDE, "DEBUG ONLY - Override external webhook url (and port)", "", typeof(string)));
        }

        public class Constants
        {
            public const string INTEREL_SERVER = "InterelServerUrl";
            public const string PUBLIC_RSA_KEY = "PublicRSAKey";
            public const string TOKENS_FILE = "TokensFile";
            public const string INCOMING_WEBHOOK_URL = "IncomingWebHookUrl";
            public const string INCOMING_WEBHOOK_PORT = "IncomingWebHookPort";
            public const string INCOMING_WEBHOOK_URL_OVERRIDE = "OverrideWebHookUrl";
        }
    }
}
