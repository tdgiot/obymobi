﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using Crave.RoomControl.Interel.Models;
using Dionysos;
using Newtonsoft.Json;
using Serilog;

namespace Crave.RoomControl.Interel
{
    internal class WebHookServer
    {
        private readonly Builder builder;
        private readonly HttpListener httpListener = new HttpListener();
        private readonly ILogger logger;
        private readonly IWebHookRequestHandler webHookRequestHandler;

        private WebHookServer(Builder builder)
        {
            this.builder = builder;
            this.logger = builder.Logger;
            this.webHookRequestHandler = builder.WebHookRequestHandler;

            int port = this.builder.Port;

            if (this.webHookRequestHandler == null)
            {
                throw new ArgumentNullException("webHookRequestHandler");
            }

            if (!HttpListener.IsSupported)
            {
                throw new NotSupportedException("Needs Windows XP SP2, Server 2003 or later. Upgrade yo server!");
            }

            string prefix = string.Format("http://*:{0}/interel/webhook/", port);
            this.httpListener.Prefixes.Add(prefix);
        }

        public void Start()
        {
            this.httpListener.Start();
            Run();

            this.logger.Information("[Interel] WebHook URL: {0}:{1}/interel/webhook/", this.builder.Url, this.builder.Port);
            this.logger.Information("[Interel] Use '{0}:{1}/interel/webhook/test' to test access", this.builder.Url, this.builder.Port);

            DeleteAllSubscriptions();
            Subscribe();
        }

        public void Stop()
        {
            DeleteAllSubscriptions();

            this.httpListener.Stop();
            this.httpListener.Close();
        }

        /// <summary>
        /// -endpoint POST "https://API_SERVER/api/WebHookSubscription/Subscribe
        /// -header "Authorization: Bearer JWT_TOKEN"
        /// -body JSON(application/json) "WEBHOOK_OBJECT"
        /// </summary>
        private void Subscribe()
        {
            this.logger.Information("[Interel] Subscribing to Interel webhook events");

            WebHookSubscribe model = new WebHookSubscribe
            {
                WebHookUri = string.Format("{0}:{1}/interel/webhook/", this.builder.Url, this.builder.Port),
                Secret = "123456789abcdefghijklmnopqrstuvwxyz",
                Filter = new[] { "*" }
            };

            if (!this.builder.ExternalWebHookUrl.IsNullOrWhiteSpace())
            {
                model.WebHookUri = string.Format("{0}/interel/webhook/", this.builder.ExternalWebHookUrl);
                this.logger.Information("[Interel] OVERRIDE WEBHOOK URL: {0}", model.WebHookUri);
            }

            string jsonContent = JsonConvert.SerializeObject(model, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });

            try
            {
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.builder.AccessToken);
                HttpResponseMessage response = httpClient.PostAsync(string.Format("{0}/Subscribe", this.builder.InterelServer), new StringContent(jsonContent, Encoding.UTF8, "application/json")).Result;

                string responseResult = response.Content.ReadAsStringAsync().Result;

                this.logger.Information("[Interel] [WebHook] Subscribe result: " + responseResult);
            }
            catch (Exception ex)
            {
                this.logger.Warning("[Interel] [WebHook] Failed to subscribe. Message: {message} - {innerEx}", ex.Message, ex.InnerException != null ? ex.InnerException.Message : "");
            }

        }

        /// <summary>
        ///-endpoint DELETE "https://API_SERVER/api/DeleteAll"
        /// -header "Authorization: Bearer JWT_TOKEN"
        /// </summary>
        private void DeleteAllSubscriptions()
        {
            this.logger.Information("[Interel] Deleting previous subscriptions from Interel server");

            try
            {
                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.builder.AccessToken);
                HttpResponseMessage response = httpClient.DeleteAsync(string.Format("{0}/DeleteAll", this.builder.InterelServer)).Result;
            }
            catch (Exception ex)
            {
                this.logger.Warning("[Interel] [WebHook] Failed to delete old subscriptions. Message: {message} - {innerEx}", ex.Message, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }

        private void Run()
        {
            ThreadPool.QueueUserWorkItem((o) =>
                                         {
                                             this.logger.Information("[Interel] WebHook webserver running...");
                                             try
                                             {
                                                 while (this.httpListener.IsListening)
                                                 {
                                                     ThreadPool.QueueUserWorkItem((c) =>
                                                                                  {
                                                                                      HttpListenerContext ctx = c as HttpListenerContext;
                                                                                      if (ctx == null)
                                                                                      {
                                                                                          return;
                                                                                      }

                                                                                      try
                                                                                      {
                                                                                          string rstr = HandleRequest(ctx.Request);
                                                                                          byte[] buf = Encoding.UTF8.GetBytes(rstr);
                                                                                          ctx.Response.ContentLength64 = buf.Length;
                                                                                          ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                                                                                      }
                                                                                      catch
                                                                                      {
                                                                                          // ignored
                                                                                      }
                                                                                      finally
                                                                                      {
                                                                                          // always close the stream
                                                                                          ctx.Response.OutputStream.Close();
                                                                                      }
                                                                                  }, this.httpListener.GetContext());
                                                 }
                                             }
                                             catch
                                             {
                                                 // ignored
                                             }
                                         });
        }

        private string HandleRequest(HttpListenerRequest request)
        {
            this.logger.Debug("[Interel] New Request: {0}", request.RawUrl);

            string method = request.Url.AbsolutePath.Replace("/interel/webhook", string.Empty).ToLower();

            if (method.Equals("/"))
            {
                string echoValue = request.QueryString.Get("echo");
                if (!string.IsNullOrEmpty(echoValue))
                {
                    return echoValue;
                }

                if (!request.HasEntityBody)
                {
                    this.logger.Warning("[Interel] No client data was sent with the request.");
                    return "";
                }

                System.IO.Stream body = request.InputStream;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, request.ContentEncoding);

                string s = reader.ReadToEnd();

                this.logger.Debug("[Interel] WebHook Data: {json}", s);

                WebHookRequest webHookRequest = null;
                try
                {
                    webHookRequest = JsonConvert.DeserializeObject<WebHookRequest>(s);
                    if (webHookRequest == null)
                    {
                        this.logger.Error("[Interel] Failed to deserialize data\n\r{data}", s);
                    }
                }
                catch (Exception ex)
                {
                    this.logger.Error(ex, "[Interel] Failed to deserialize data\n\r{data}", s);
                }


                body.Close();
                reader.Close();

                if (webHookRequest == null)
                {
                    return string.Empty;
                }

                this.webHookRequestHandler.NewWebHookRequest(webHookRequest);
            }
            else if (method.Equals("/test"))
            {
                return "CraveRoomControlServer - Interel incomming webhook service is running - " + DateTime.Now;
            }
            else if (method.Equals("/eventsimulator"))
            {
                EventSimulatorTrigger trigger = new EventSimulatorTrigger();

                string number = request.QueryString.Get("number");
                if (!int.TryParse(number, out int i))
                {
                    i = 0;
                }
                trigger.Number = i;

                trigger.NotifyCode = request.QueryString.Get("notifyCode");
                trigger.Value = request.QueryString.Get("value");
                trigger.Identifier = request.QueryString.Get("identifier");

                string jsonContent = JsonConvert.SerializeObject(trigger, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                HttpClient httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", this.builder.AccessToken);
                HttpResponseMessage response = httpClient.PostAsync("http://94.200.105.118:1224/eventsimulator/Api/Trigger", new StringContent(jsonContent, Encoding.UTF8, "application/json")).Result;

                string responseResult = response.Content.ReadAsStringAsync().Result;

                return "Event triggered - " + jsonContent + "\n\r" + responseResult;
            }

            return string.Empty;
        }

        public class Builder
        {
            internal ILogger Logger { get; private set; }
            internal Encrypter Encrypter { get; private set; }
            internal string Url { get; private set; }
            internal int Port { get; private set; }
            internal string InterelServer { get; private set; }
            internal string AccessToken { get; private set; }
            internal IWebHookRequestHandler WebHookRequestHandler { get; private set; }

            internal string ExternalWebHookUrl { get; private set; }

            public Builder(Encrypter encrypter, ILogger logger)
            {
                this.Port = 8082;
                this.Encrypter = encrypter;
                this.Logger = logger;
            }

            public Builder SetIncomingUrl(string url)
            {
                this.Url = url;
                return this;
            }

            public Builder SetIncomingPort(int port)
            {
                this.Port = port;
                return this;
            }

            public Builder SetInterelServer(string url)
            {
                if (!url.EndsWith("/"))
                {
                    url += "/WebHookSubscription";
                }
                else
                {
                    url += "WebHookSubscription";
                }

                this.InterelServer = url;
                return this;
            }

            public Builder SetAccessToken(string accessToken)
            {
                this.AccessToken = accessToken;
                return this;
            }

            public Builder SetWebHookRequestHandler(IWebHookRequestHandler handler)
            {
                this.WebHookRequestHandler = handler;
                return this;
            }

            public Builder SetOverrideWebHookUrl(string webhookUrl)
            {
                this.ExternalWebHookUrl = webhookUrl;
                return this;
            }

            public WebHookServer Build() => new WebHookServer(this);
        }

        public interface IWebHookRequestHandler
        {
            void NewWebHookRequest(WebHookRequest request);
        }
    }
}
