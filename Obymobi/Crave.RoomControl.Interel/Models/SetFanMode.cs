﻿using System;
using Dionysos;

namespace Crave.RoomControl.Interel.Models
{
    public class SetFanMode
    {
        public long TimeStamp { get; set; }
        public string ThermostatId { get; set; }
        public string FanMode { get; set; }

        public SetFanMode()
        {
            this.TimeStamp = DateTime.UtcNow.ToUnixTime();
        }
    }
}