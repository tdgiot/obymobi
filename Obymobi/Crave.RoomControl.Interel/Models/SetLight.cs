﻿using System;
using Dionysos;

namespace Crave.RoomControl.Interel.Models
{
    public class SetLight
    {
        public long TimeStamp { get; set; }
        public string Light { get; set; }
        public string State { get; set; }

        public SetLight()
        {
            this.TimeStamp = DateTime.UtcNow.ToUnixTime();
        }
    }
}