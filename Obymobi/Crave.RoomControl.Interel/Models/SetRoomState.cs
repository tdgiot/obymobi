﻿using System;
using Dionysos;
using Newtonsoft.Json;

namespace Crave.RoomControl.Interel.Models
{
    public class SetRoomState
    {
        [JsonProperty("timeStamp")]
        public long TimeStamp { get; set; }
        
        [JsonProperty("state")]
        public string State { get; set; }

        public SetRoomState()
        {
            this.TimeStamp = DateTime.UtcNow.ToUnixTime();
        }
    }
}