﻿namespace Crave.RoomControl.Interel.Models
{
    public class WebHookRequest
    {
        public string Id { get; set; }
        public int Attempt { get; set; }
        public WebHookNotification[] Notifications { get; set; }
    }

    public class WebHookNotification
    {
        public string Action { get; set; }
        public string EventDate { get; set; }
        public string NotifyCode { get; set; }
        public string State { get; set; }
        public string Identifier { get; set; }
    }
}