﻿using System;
using Dionysos;
using Newtonsoft.Json;

namespace Crave.RoomControl.Interel.Models
{
    public class ChangeSetPoint
    {
        [JsonProperty("timeStamp")]
        public long TimeStamp { get; set; }

        [JsonProperty("thermostatId")]
        public string ThermostatId { get; set; }

        [JsonProperty("setPoint")]
        public decimal SetPoint { get; set; }

        public ChangeSetPoint()
        {
            this.TimeStamp = DateTime.UtcNow.ToUnixTime();
        }
    }
}