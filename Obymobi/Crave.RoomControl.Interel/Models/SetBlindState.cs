﻿using System;
using Dionysos;

namespace Crave.RoomControl.Interel.Models
{
    public class SetBlindState
    {
        public long TimeStamp { get; set; }
        public string BlindId { get; set; }
        public string State { get; set; }

        public SetBlindState()
        {
            this.TimeStamp = DateTime.UtcNow.ToUnixTime();
        }
    }
}