﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.RoomControl.Interel.Models
{
    public class WebHookSubscribe
    {
        public String Id { get; set; }
        public String WebHookUri { get; set; }
        public String Secret { get; set; }
        public String[] Filter { get; set; }
    }
}
