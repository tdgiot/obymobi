﻿namespace Crave.RoomControl.Interel.Models
{
    public class EventSimulatorTrigger
    {
        public int Number { get; set; }
        public string NotifyCode { get; set; }
        public string Value { get; set; }
        public string Identifier { get; set; }
    }
}