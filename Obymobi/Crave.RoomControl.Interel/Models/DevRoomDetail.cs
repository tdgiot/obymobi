﻿namespace Crave.RoomControl.Interel.Models
{
    public class DevRoomDetail
    {
        public bool Dnd { get; set; }
        public bool Mur { get; set; }

        public Thermostat[] DevThermos { get; set; }

        public class Thermostat
        {
            public string ThermostatId { get; set; }
            public double ActualTemp { get; set; }
            public string FanSpeed { get; set; }
            public double SetPoint { get; set; }
        }
    }
}