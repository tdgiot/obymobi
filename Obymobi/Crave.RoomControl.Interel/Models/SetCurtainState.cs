﻿using System;
using Dionysos;

namespace Crave.RoomControl.Interel.Models
{
    public class SetCurtainState
    {
        public long TimeStamp { get; set; }
        public string CurtainId { get; set; }
        public string State { get; set; }

        public SetCurtainState()
        {
            this.TimeStamp = DateTime.UtcNow.ToUnixTime();
        }
    }
}