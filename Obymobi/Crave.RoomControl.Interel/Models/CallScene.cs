﻿using System;
using Dionysos;

namespace Crave.RoomControl.Interel.Models
{
    public class CallScene
    {
        public long TimeStamp { get; set; }
        public string SceneName { get; set; }

        public CallScene()
        {
            this.TimeStamp = DateTime.UtcNow.ToUnixTime();
        }
    }
}