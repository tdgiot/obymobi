﻿using System;

namespace ObymobiXperimental.Comet
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 6)
            {
                Console.WriteLine(@"Missing parameters.. Check source on what's needed");
                return;
            }

            string pokeInUrl = args[0];
            string macAddress = args[1];
            string salt = args[2];
            int clientId = int.Parse(args[3]);
            int deliverypointgroupId = int.Parse(args[4]);
            int companyId = int.Parse(args[5]);

            var client = new CometLoadClient(pokeInUrl, macAddress, salt, clientId, deliverypointgroupId, companyId);
            client.Connect();
        }
    }
}
