﻿using System;
using System.Threading.Tasks;
using Dionysos;
using Dionysos.Net;
using Newtonsoft.Json;
using Obymobi.Enums;
using Obymobi.Logic.Comet.Netmessages;
using Obymobi.Logic.Model;
using Obymobi.Security;
using PokeIn.Comet;

namespace ObymobiXperimental.Comet
{
    public class CometLoadClient
    {
        public DesktopClient Client;

        private readonly object handlerLock = new object();
        private ConnectionState connectionState = ConnectionState.Disconnected;
        private readonly InfiniteLooper pingPongChecker;

        private string PokeInUrl { get; set; }
        private string MacAddress { get; set; }
        private string Salt { get; set; }
        private int CompanyId { get; set; }
        private int DeliverypointgroupId { get; set; }
        public int ClientId { get; set; }

        private DateTime LastPong { get; set; }

        public delegate void OnLogEvent(string log);

        public event OnLogEvent OnLog;

        public CometLoadClient(string pokeInUrl, string macAddress, string salt, int clientId, int deliverypointgroupId, int companyId)
        {
            State = ConnectionState.Disconnected;
            PokeInUrl = pokeInUrl;
            MacAddress = macAddress;
            Salt = salt;
            CompanyId = companyId;
            ClientId = clientId;
            DeliverypointgroupId = deliverypointgroupId;
            LastPong = DateTime.UtcNow;

            pingPongChecker = new InfiniteLooper(() =>
            {
                if (State == ConnectionState.Connected && this.Client != null && !CheckLastPingPong())
                {
                    OnEventLog("Ping/Pong - No ping message received for 2 minutes, disconnecting clients.");

                    this.Client.Dispose();
                    this.Client = null;

                    State = ConnectionState.Disconnected;

                    Connect();
                }
            }, 30000, -1, OnExceptionHandler);
        }

        private void OnExceptionHandler(Exception exception)
        {
            OnEventLog("Ping/Pong Exception - " + exception.Message);

            StopPingPongChecker();
            StartPingPongChecker();
        }

        private void StartPingPongChecker()
        {
            lock (handlerLock)
            {
                if (this.pingPongChecker != null && !this.pingPongChecker.IsRunning)
                {
                    OnEventLog("Ping/Pong - Starting PingPongChecker");

                    this.pingPongChecker.Start();
                }
            }
        }

        private void StopPingPongChecker()
        {
            lock (handlerLock)
            {
                if (this.pingPongChecker != null && this.pingPongChecker.IsRunning)
                {
                    OnEventLog("Ping/Pong - Stopping PingPongChecker");
                    this.pingPongChecker.Stop();
                }
            }
        }

        private bool CheckLastPingPong()
        {
            return (DateTime.UtcNow.Subtract(LastPong) < new TimeSpan(0, 2, 0));
        }

        public ConnectionState State
        {
            get
            {
                lock (handlerLock)
                {
                    return connectionState;
                }
            }
            private set
            {
                lock (handlerLock)
                {
                    if (connectionState != value)
                    {
                        var oldState = connectionState;
                        connectionState = value;

                        OnEventLog("ConnectionState - Changed connection state from '{0}' to '{1}'.", oldState, connectionState);

                        if (value == ConnectionState.Connected && (oldState == ConnectionState.Connecting || oldState == ConnectionState.Reconnecting))
                        {
                            StartPingPongChecker();
                        }
                        else if (value == ConnectionState.Disconnected && oldState != ConnectionState.Disconnecting)
                        {
                            ReloadConnection(30);
                            StopPingPongChecker();
                        }
                    }
                }
            }
        }

        internal void Connect()
        {
            if (State == ConnectionState.Disconnected || State == ConnectionState.Reconnecting)
            {
                State = ConnectionState.Connecting;

                if (this.Client != null)
                {
                    this.Client.Dispose();
                    this.Client = null;
                }

                if (PokeInUrl.EndsWith("/"))
                    PokeInUrl = PokeInUrl.Substring(0, PokeInUrl.Length - 1);

                if (!PokeInUrl.EndsWith("/host.PokeIn"))
                    PokeInUrl += "/host.PokeIn";

                OnEventLog("Connect - Connecting to CometServer: '{0}'", PokeInUrl);
                this.Client = new DesktopClient(new PokeInInterface(this), PokeInUrl, "");
                Client.OnClientConnected += (PokeInClient_OnClientConnected);
                Client.OnClientDisconnected += (PokeInClient_OnClientDisconnected);
                Client.OnErrorReceived += (PokeInClient_OnErrorReceived);
                this.Client.Connect();
            }
            else
            {
                OnEventLog("Connect - Can not connect becuase of current State {0}", State);
            }
        }

        private void PokeInClient_OnErrorReceived(DesktopClient c, string errorMessage)
        {
            OnEventLog("PokeInClient.PokeInClient_OnErrorReceived - '{0}'", errorMessage);
        }

        private void PokeInClient_OnClientDisconnected(DesktopClient c)
        {
            OnEventLog("PokeInClient.PokeInClient_OnClientDisconnected - Disconnected");

            Disconnect(true);
        }

        private void PokeInClient_OnClientConnected(DesktopClient c)
        {
            OnEventLog("PokeInClient.PokeInClient_OnClientConnected - Connected");

            State = ConnectionState.Connected;

            long timestamp = DateTime.UtcNow.ToUnixTime();
            string hash = Hasher.GetHashFromParameters(Salt, timestamp, MacAddress, c.ClientID);

            OnEventLog("PokeInClient.Authenticate - ClientId: {0} - Identifier: {1}", c.ClientID, MacAddress);

            this.Client.SendAsync("PokeInInterface.Authenticate", timestamp, MacAddress, hash);
        }

        internal void Disconnect(bool forced = false)
        {
            OnEventLog("Disconnect");

            if (!forced)
                State = ConnectionState.Disconnecting;

            if (this.Client != null)
            {
                this.Client.Dispose();

                if (forced)
                {
                    ReloadConnection(30);
                }
            }
        }

        private void ReloadConnection(int delay)
        {
            if (State != ConnectionState.Reconnecting)
            {
                State = ConnectionState.Reconnecting;

                if (this.Client != null)
                {
                    this.Client.Dispose();
                    this.Client = null;
                }

                if (delay <= 0)
                {
                    OnEventLog("ReloadConnection - Reconnecting to CometServer", delay);
                    Connect();
                }
                else
                {
                    OnEventLog("ReloadConnection - Reconnecting to CometServer in {0} seconds", delay);
                    Task.Factory.StartDelayed(delay * 1000, Connect);
                }
            }
        }

        public void SendMessage(Netmessage message)
        {
            // Create JSON
            var settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            string netmessage = JsonConvert.SerializeObject(message, new Formatting(), settings);

            OnEventLog("Sending message to server: {0}", message.MessageType);
            Client.SendAsync("PokeInInterface.ReceiveMessage", netmessage);
        }

        #region ICometClientEventListener Implementation


        public void OnNetmessageReceived(Netmessage netmessage)
        {
            if (netmessage.MessageType == NetmessageType.AuthenticateResult)
            {
                var message = netmessage.ConvertTo<NetmessageAuthenticateResult>();

                if (this.Client != null)
                {
                    if (message.IsAuthenticated)
                    {
                        OnEventLog("Authenticated to CometServer");

                        var clientType = new NetmessageSetClientType();
                        clientType.ClientType = NetmessageClientType.Emenu;
                        clientType.ClientId = ClientId;
                        clientType.CompanyId = CompanyId;
                        clientType.DeliverypointgroupId = DeliverypointgroupId;
                        clientType.DeliverypointId = 0;

                        LastPong = DateTime.UtcNow;

                        SendMessage(clientType);
                    }
                    else
                    {
                        OnEventLog("Failed to autenticate. Message: {0}", message.FieldValue2);
                    }
                }
            }
            else if (netmessage.MessageType == NetmessageType.Ping)
            {
                if (this.Client != null)
                {
                    // Respond with a Pong
                    var pong = new NetmessagePong();
                    pong.PrivateIpAddresses = NetUtil.GetLocalIpsJoined();
                    pong.PublicIpAddress = NetUtil.GetRemoteIp();
                    pong.IsCharging = true;
                    pong.BatteryLevel = 100;
                    pong.CurrentCloudEnvironment = CloudEnvironment.Manual;

                    this.LastPong = DateTime.UtcNow;
                    this.SendMessage(pong);
                }
            }
            else if (netmessage.MessageType == NetmessageType.Disconnect)
            {
                Disconnect(true);
            }
        }

        public void OnEventLog(string message, params object[] args)
        {
            var intermediateString = string.Format(message, args);
            var finalString = string.Format("[{0}] {1}", MacAddress, intermediateString);

            Console.WriteLine(finalString);

            if (OnLog != null)
            {
                OnLog.Invoke(finalString);
            }
        }

        #endregion

        public class PokeInInterface
        {
            private readonly CometLoadClient myClient;

            public PokeInInterface(CometLoadClient client)
            {
                myClient = client;
            }

            public void ReceiveMessage(string message)
            {
                try
                {
                    var netmessage = JsonConvert.DeserializeObject<Netmessage>(message);
                    myClient.OnNetmessageReceived(netmessage);
                }
                catch (Exception ex)
                {
                    myClient.OnEventLog("PokeInInterface.ReceiveMessage - Failed to deserialize message. Exception: {0} | Message: {1}", ex.Message, message);
                }
            }
        }
    }
}
