﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace Obymobi.Azure.Data
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Log.</summary>
	public enum LogFieldIndex
	{
		///<summary>Created. </summary>
		Created,
		///<summary>FaultDomain. </summary>
		FaultDomain,
		///<summary>Instance. </summary>
		Instance,
		///<summary>LogId. </summary>
		LogId,
		///<summary>LogType. </summary>
		LogType,
		///<summary>Message. </summary>
		Message,
		///<summary>Region. </summary>
		Region,
		///<summary>UpdateDomain. </summary>
		UpdateDomain,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Notification.</summary>
	public enum NotificationFieldIndex
	{
		///<summary>Created. </summary>
		Created,
		///<summary>EmailAddresses. </summary>
		EmailAddresses,
		///<summary>FaultDomain. </summary>
		FaultDomain,
		///<summary>Instance. </summary>
		Instance,
		///<summary>Message. </summary>
		Message,
		///<summary>NotificationId. </summary>
		NotificationId,
		///<summary>NotificationType. </summary>
		NotificationType,
		///<summary>PhoneNumbers. </summary>
		PhoneNumbers,
		///<summary>Region. </summary>
		Region,
		///<summary>UpdateDomain. </summary>
		UpdateDomain,
		/// <summary></summary>
		AmountOfFields
	}



	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>Log</summary>
		LogEntity,
		///<summary>Notification</summary>
		NotificationEntity
	}


	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	#endregion

	#region Included code

	#endregion
}

