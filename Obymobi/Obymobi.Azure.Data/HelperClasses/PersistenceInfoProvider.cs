﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Azure.Data.HelperClasses
{
	/// <summary>Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal static class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			this.InitClass(2);
			InitLogEntityMappings();
			InitNotificationEntityMappings();
		}

		/// <summary>Inits LogEntity's mappings</summary>
		private void InitLogEntityMappings()
		{
			this.AddElementMapping("LogEntity", @"ObymobiAzure", @"dbo", "Log", 8, 0);
			this.AddElementFieldMapping("LogEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 0);
			this.AddElementFieldMapping("LogEntity", "FaultDomain", "FaultDomain", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("LogEntity", "Instance", "Instance", true, "NVarChar", 200, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("LogEntity", "LogId", "LogId", false, "BigInt", 0, 19, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int64), 3);
			this.AddElementFieldMapping("LogEntity", "LogType", "LogType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("LogEntity", "Message", "Message", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("LogEntity", "Region", "Region", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("LogEntity", "UpdateDomain", "UpdateDomain", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
		}

		/// <summary>Inits NotificationEntity's mappings</summary>
		private void InitNotificationEntityMappings()
		{
			this.AddElementMapping("NotificationEntity", @"ObymobiAzure", @"dbo", "Notification", 10, 0);
			this.AddElementFieldMapping("NotificationEntity", "Created", "Created", true, "DateTime", 0, 0, 0, false, "", null, typeof(System.DateTime), 0);
			this.AddElementFieldMapping("NotificationEntity", "EmailAddresses", "EmailAddresses", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 1);
			this.AddElementFieldMapping("NotificationEntity", "FaultDomain", "FaultDomain", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("NotificationEntity", "Instance", "Instance", true, "VarChar", 200, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("NotificationEntity", "Message", "Message", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("NotificationEntity", "NotificationId", "NotificationId", false, "BigInt", 0, 19, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int64), 5);
			this.AddElementFieldMapping("NotificationEntity", "NotificationType", "NotificationType", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("NotificationEntity", "PhoneNumbers", "PhoneNumbers", true, "NVarChar", 2147483647, 0, 0, false, "", null, typeof(System.String), 7);
			this.AddElementFieldMapping("NotificationEntity", "Region", "Region", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("NotificationEntity", "UpdateDomain", "UpdateDomain", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
		}

	}
}
