﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Azure.Data.HelperClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>Singleton implementation of the FieldInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the FieldInfoProviderBase class is threadsafe.</remarks>
	internal static class FieldInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IFieldInfoProvider _providerInstance = new FieldInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static FieldInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the FieldInfoProviderCore</summary>
		/// <returns>Instance of the FieldInfoProvider.</returns>
		public static IFieldInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the FieldInfoProvider. Used by singleton wrapper.</summary>
	internal class FieldInfoProviderCore : FieldInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="FieldInfoProviderCore"/> class.</summary>
		internal FieldInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores.</summary>
		private void Init()
		{
			this.InitClass( (2 + 0));
			InitLogEntityInfos();
			InitNotificationEntityInfos();

			this.ConstructElementFieldStructures(InheritanceInfoProviderSingleton.GetInstance());
		}

		/// <summary>Inits LogEntity's FieldInfo objects</summary>
		private void InitLogEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(LogFieldIndex), "LogEntity");
			this.AddElementFieldInfo("LogEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)LogFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("LogEntity", "FaultDomain", typeof(Nullable<System.Int32>), false, false, false, true,  (int)LogFieldIndex.FaultDomain, 0, 0, 10);
			this.AddElementFieldInfo("LogEntity", "Instance", typeof(System.String), false, false, false, true,  (int)LogFieldIndex.Instance, 200, 0, 0);
			this.AddElementFieldInfo("LogEntity", "LogId", typeof(System.Int64), true, false, true, false,  (int)LogFieldIndex.LogId, 0, 0, 19);
			this.AddElementFieldInfo("LogEntity", "LogType", typeof(System.Int32), false, false, false, false,  (int)LogFieldIndex.LogType, 0, 0, 10);
			this.AddElementFieldInfo("LogEntity", "Message", typeof(System.String), false, false, false, true,  (int)LogFieldIndex.Message, 2147483647, 0, 0);
			this.AddElementFieldInfo("LogEntity", "Region", typeof(System.String), false, false, false, true,  (int)LogFieldIndex.Region, 50, 0, 0);
			this.AddElementFieldInfo("LogEntity", "UpdateDomain", typeof(Nullable<System.Int32>), false, false, false, true,  (int)LogFieldIndex.UpdateDomain, 0, 0, 10);
		}
		/// <summary>Inits NotificationEntity's FieldInfo objects</summary>
		private void InitNotificationEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(NotificationFieldIndex), "NotificationEntity");
			this.AddElementFieldInfo("NotificationEntity", "Created", typeof(Nullable<System.DateTime>), false, false, false, true,  (int)NotificationFieldIndex.Created, 0, 0, 0);
			this.AddElementFieldInfo("NotificationEntity", "EmailAddresses", typeof(System.String), false, false, false, true,  (int)NotificationFieldIndex.EmailAddresses, 2147483647, 0, 0);
			this.AddElementFieldInfo("NotificationEntity", "FaultDomain", typeof(Nullable<System.Int32>), false, false, false, true,  (int)NotificationFieldIndex.FaultDomain, 0, 0, 10);
			this.AddElementFieldInfo("NotificationEntity", "Instance", typeof(System.String), false, false, false, true,  (int)NotificationFieldIndex.Instance, 200, 0, 0);
			this.AddElementFieldInfo("NotificationEntity", "Message", typeof(System.String), false, false, false, true,  (int)NotificationFieldIndex.Message, 2147483647, 0, 0);
			this.AddElementFieldInfo("NotificationEntity", "NotificationId", typeof(System.Int64), true, false, true, false,  (int)NotificationFieldIndex.NotificationId, 0, 0, 19);
			this.AddElementFieldInfo("NotificationEntity", "NotificationType", typeof(System.Int32), false, false, false, false,  (int)NotificationFieldIndex.NotificationType, 0, 0, 10);
			this.AddElementFieldInfo("NotificationEntity", "PhoneNumbers", typeof(System.String), false, false, false, true,  (int)NotificationFieldIndex.PhoneNumbers, 2147483647, 0, 0);
			this.AddElementFieldInfo("NotificationEntity", "Region", typeof(System.String), false, false, false, true,  (int)NotificationFieldIndex.Region, 50, 0, 0);
			this.AddElementFieldInfo("NotificationEntity", "UpdateDomain", typeof(Nullable<System.Int32>), false, false, false, true,  (int)NotificationFieldIndex.UpdateDomain, 0, 0, 10);
		}
		
	}
}




