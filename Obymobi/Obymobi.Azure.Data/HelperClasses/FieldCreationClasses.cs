﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Azure.Data.FactoryClasses;
using Obymobi.Azure.Data;

namespace Obymobi.Azure.Data.HelperClasses
{
	/// <summary>Field Creation Class for entity LogEntity</summary>
	public partial class LogFields
	{
		/// <summary>Creates a new LogEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(LogFieldIndex.Created);}
		}
		/// <summary>Creates a new LogEntity.FaultDomain field instance</summary>
		public static EntityField FaultDomain
		{
			get { return (EntityField)EntityFieldFactory.Create(LogFieldIndex.FaultDomain);}
		}
		/// <summary>Creates a new LogEntity.Instance field instance</summary>
		public static EntityField Instance
		{
			get { return (EntityField)EntityFieldFactory.Create(LogFieldIndex.Instance);}
		}
		/// <summary>Creates a new LogEntity.LogId field instance</summary>
		public static EntityField LogId
		{
			get { return (EntityField)EntityFieldFactory.Create(LogFieldIndex.LogId);}
		}
		/// <summary>Creates a new LogEntity.LogType field instance</summary>
		public static EntityField LogType
		{
			get { return (EntityField)EntityFieldFactory.Create(LogFieldIndex.LogType);}
		}
		/// <summary>Creates a new LogEntity.Message field instance</summary>
		public static EntityField Message
		{
			get { return (EntityField)EntityFieldFactory.Create(LogFieldIndex.Message);}
		}
		/// <summary>Creates a new LogEntity.Region field instance</summary>
		public static EntityField Region
		{
			get { return (EntityField)EntityFieldFactory.Create(LogFieldIndex.Region);}
		}
		/// <summary>Creates a new LogEntity.UpdateDomain field instance</summary>
		public static EntityField UpdateDomain
		{
			get { return (EntityField)EntityFieldFactory.Create(LogFieldIndex.UpdateDomain);}
		}
	}

	/// <summary>Field Creation Class for entity NotificationEntity</summary>
	public partial class NotificationFields
	{
		/// <summary>Creates a new NotificationEntity.Created field instance</summary>
		public static EntityField Created
		{
			get { return (EntityField)EntityFieldFactory.Create(NotificationFieldIndex.Created);}
		}
		/// <summary>Creates a new NotificationEntity.EmailAddresses field instance</summary>
		public static EntityField EmailAddresses
		{
			get { return (EntityField)EntityFieldFactory.Create(NotificationFieldIndex.EmailAddresses);}
		}
		/// <summary>Creates a new NotificationEntity.FaultDomain field instance</summary>
		public static EntityField FaultDomain
		{
			get { return (EntityField)EntityFieldFactory.Create(NotificationFieldIndex.FaultDomain);}
		}
		/// <summary>Creates a new NotificationEntity.Instance field instance</summary>
		public static EntityField Instance
		{
			get { return (EntityField)EntityFieldFactory.Create(NotificationFieldIndex.Instance);}
		}
		/// <summary>Creates a new NotificationEntity.Message field instance</summary>
		public static EntityField Message
		{
			get { return (EntityField)EntityFieldFactory.Create(NotificationFieldIndex.Message);}
		}
		/// <summary>Creates a new NotificationEntity.NotificationId field instance</summary>
		public static EntityField NotificationId
		{
			get { return (EntityField)EntityFieldFactory.Create(NotificationFieldIndex.NotificationId);}
		}
		/// <summary>Creates a new NotificationEntity.NotificationType field instance</summary>
		public static EntityField NotificationType
		{
			get { return (EntityField)EntityFieldFactory.Create(NotificationFieldIndex.NotificationType);}
		}
		/// <summary>Creates a new NotificationEntity.PhoneNumbers field instance</summary>
		public static EntityField PhoneNumbers
		{
			get { return (EntityField)EntityFieldFactory.Create(NotificationFieldIndex.PhoneNumbers);}
		}
		/// <summary>Creates a new NotificationEntity.Region field instance</summary>
		public static EntityField Region
		{
			get { return (EntityField)EntityFieldFactory.Create(NotificationFieldIndex.Region);}
		}
		/// <summary>Creates a new NotificationEntity.UpdateDomain field instance</summary>
		public static EntityField UpdateDomain
		{
			get { return (EntityField)EntityFieldFactory.Create(NotificationFieldIndex.UpdateDomain);}
		}
	}
	

}