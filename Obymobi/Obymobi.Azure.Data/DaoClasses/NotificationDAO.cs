﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using Obymobi.Azure.Data.EntityClasses;
using Obymobi.Azure.Data.FactoryClasses;
using Obymobi.Azure.Data.CollectionClasses;
using Obymobi.Azure.Data.HelperClasses;
using Obymobi.Azure.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.SqlServer;


namespace Obymobi.Azure.Data.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the Notification Entity. It will perform database oriented actions for a entity of type 'NotificationEntity'.</summary>
	public partial class NotificationDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public NotificationDAO() : base(InheritanceHierarchyType.None, "NotificationEntity", new NotificationEntityFactory())
		{
		}








		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
