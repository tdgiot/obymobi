﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Azure.Data.FactoryClasses;
using Obymobi.Azure.Data.CollectionClasses;
using Obymobi.Azure.Data.DaoClasses;
using Obymobi.Azure.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Azure.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Notification'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class NotificationEntity : NotificationEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public NotificationEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="notificationId">PK value for Notification which data should be fetched into this Notification object</param>
		public NotificationEntity(System.Int64 notificationId):
			base(notificationId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="notificationId">PK value for Notification which data should be fetched into this Notification object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public NotificationEntity(System.Int64 notificationId, IPrefetchPath prefetchPathToUse):
			base(notificationId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="notificationId">PK value for Notification which data should be fetched into this Notification object</param>
		/// <param name="validator">The custom validator object for this NotificationEntity</param>
		public NotificationEntity(System.Int64 notificationId, IValidator validator):
			base(notificationId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected NotificationEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
