﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Azure.Data.FactoryClasses;
using Obymobi.Azure.Data.CollectionClasses;
using Obymobi.Azure.Data.DaoClasses;
using Obymobi.Azure.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Azure.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Log'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class LogEntity : LogEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public LogEntity():base()
		{
		}

		/// <summary>CTor</summary>
		/// <param name="logId">PK value for Log which data should be fetched into this Log object</param>
		public LogEntity(System.Int64 logId):
			base(logId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="logId">PK value for Log which data should be fetched into this Log object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public LogEntity(System.Int64 logId, IPrefetchPath prefetchPathToUse):
			base(logId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="logId">PK value for Log which data should be fetched into this Log object</param>
		/// <param name="validator">The custom validator object for this LogEntity</param>
		public LogEntity(System.Int64 logId, IValidator validator):
			base(logId, validator)
		{
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected LogEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
