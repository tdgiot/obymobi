﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

using Obymobi.Azure.Data.DaoClasses;
using Obymobi.Azure.Data.HelperClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Azure.Data.FactoryClasses
{
	/// <summary>
	/// Generic factory for DAO objects. 
	/// </summary>
	public partial class DAOFactory
	{
		/// <summary>
		/// Private CTor, no instantiation possible.
		/// </summary>
		private DAOFactory()
		{
		}

		/// <summary>Creates a new LogDAO object</summary>
		/// <returns>the new DAO object ready to use for Log Entities</returns>
		public static LogDAO CreateLogDAO()
		{
			return new LogDAO();
		}

		/// <summary>Creates a new NotificationDAO object</summary>
		/// <returns>the new DAO object ready to use for Notification Entities</returns>
		public static NotificationDAO CreateNotificationDAO()
		{
			return new NotificationDAO();
		}

		/// <summary>Creates a new TypedListDAO object</summary>
		/// <returns>The new DAO object ready to use for Typed Lists</returns>
		public static TypedListDAO CreateTypedListDAO()
		{
			return new TypedListDAO();
		}

		#region Included Code

		#endregion
	}
}
