﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace Obymobi.Analytics.Cache.Data
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Hit.</summary>
	public enum HitFieldIndex
	{
		///<summary>Action. </summary>
		Action,
		///<summary>AdvertisementId. </summary>
		AdvertisementId,
		///<summary>AdvertisementLocation. </summary>
		AdvertisementLocation,
		///<summary>AdvertisementName. </summary>
		AdvertisementName,
		///<summary>AlterationName. </summary>
		AlterationName,
		///<summary>ApplicationType. </summary>
		ApplicationType,
		///<summary>AppName. </summary>
		AppName,
		///<summary>AttachmentId. </summary>
		AttachmentId,
		///<summary>AttachmentName. </summary>
		AttachmentName,
		///<summary>CategoryId. </summary>
		CategoryId,
		///<summary>CategoryName. </summary>
		CategoryName,
		///<summary>ClientId. </summary>
		ClientId,
		///<summary>ClientOperationMode. </summary>
		ClientOperationMode,
		///<summary>CompanyId. </summary>
		CompanyId,
		///<summary>CompanyName. </summary>
		CompanyName,
		///<summary>Date. </summary>
		Date,
		///<summary>DeliverypointGroupId. </summary>
		DeliverypointGroupId,
		///<summary>DeliverypointGroupName. </summary>
		DeliverypointGroupName,
		///<summary>DeliverypointId. </summary>
		DeliverypointId,
		///<summary>DeliverypointName. </summary>
		DeliverypointName,
		///<summary>DeliverypointNumber. </summary>
		DeliverypointNumber,
		///<summary>DeliverypointNumberInt. </summary>
		DeliverypointNumberInt,
		///<summary>DeviceType. </summary>
		DeviceType,
		///<summary>EntertainmentId. </summary>
		EntertainmentId,
		///<summary>EntertainmentName. </summary>
		EntertainmentName,
		///<summary>EventAction. </summary>
		EventAction,
		///<summary>EventCategory. </summary>
		EventCategory,
		///<summary>EventLabel. </summary>
		EventLabel,
		///<summary>EventValue. </summary>
		EventValue,
		///<summary>HitHour. </summary>
		HitHour,
		///<summary>HitId. </summary>
		HitId,
		///<summary>HitMinute. </summary>
		HitMinute,
		///<summary>HitNumber. </summary>
		HitNumber,
		///<summary>HitTime. </summary>
		HitTime,
		///<summary>HitType. </summary>
		HitType,
		///<summary>HitVersion. </summary>
		HitVersion,
		///<summary>IsTablet. </summary>
		IsTablet,
		///<summary>LegacyApplicationName. </summary>
		LegacyApplicationName,
		///<summary>LegacyApplicationVersion. </summary>
		LegacyApplicationVersion,
		///<summary>LegacyCustomDimension1. </summary>
		LegacyCustomDimension1,
		///<summary>LegacyCustomDimension2. </summary>
		LegacyCustomDimension2,
		///<summary>MacAddress. </summary>
		MacAddress,
		///<summary>MediaId. </summary>
		MediaId,
		///<summary>MessageId. </summary>
		MessageId,
		///<summary>MessageName. </summary>
		MessageName,
		///<summary>MessageResponse. </summary>
		MessageResponse,
		///<summary>NavigationSource. </summary>
		NavigationSource,
		///<summary>OperatingSystem. </summary>
		OperatingSystem,
		///<summary>OrderId. </summary>
		OrderId,
		///<summary>OrderitemId. </summary>
		OrderitemId,
		///<summary>OrderSource. </summary>
		OrderSource,
		///<summary>OrderType. </summary>
		OrderType,
		///<summary>PageId. </summary>
		PageId,
		///<summary>PageName. </summary>
		PageName,
		///<summary>PrimaryKeys. </summary>
		PrimaryKeys,
		///<summary>ProductId. </summary>
		ProductId,
		///<summary>ProductName. </summary>
		ProductName,
		///<summary>RoomControlAreaId. </summary>
		RoomControlAreaId,
		///<summary>RoomControlAreaName. </summary>
		RoomControlAreaName,
		///<summary>RoomControlAreaSectionId. </summary>
		RoomControlAreaSectionId,
		///<summary>RoomControlAreaSectionItemId. </summary>
		RoomControlAreaSectionItemId,
		///<summary>RoomControlAreaSectionItemName. </summary>
		RoomControlAreaSectionItemName,
		///<summary>RoomControlAreaSectionName. </summary>
		RoomControlAreaSectionName,
		///<summary>RoomControlComponentId. </summary>
		RoomControlComponentId,
		///<summary>RoomControlComponentName. </summary>
		RoomControlComponentName,
		///<summary>RoomControlSceneName. </summary>
		RoomControlSceneName,
		///<summary>RoomControlStationId. </summary>
		RoomControlStationId,
		///<summary>RoomControlStationListingId. </summary>
		RoomControlStationListingId,
		///<summary>RoomControlStationListingName. </summary>
		RoomControlStationListingName,
		///<summary>RoomControlStationName. </summary>
		RoomControlStationName,
		///<summary>RoomControlThermostatFanMode. </summary>
		RoomControlThermostatFanMode,
		///<summary>RoomControlThermostatMode. </summary>
		RoomControlThermostatMode,
		///<summary>RoomControlThermostatScale. </summary>
		RoomControlThermostatScale,
		///<summary>RoomControlThermostatTemperature. </summary>
		RoomControlThermostatTemperature,
		///<summary>RoomControlTvRemoteAction. </summary>
		RoomControlTvRemoteAction,
		///<summary>ScreenName. </summary>
		ScreenName,
		///<summary>ServiceRequestName. </summary>
		ServiceRequestName,
		///<summary>SiteId. </summary>
		SiteId,
		///<summary>SiteName. </summary>
		SiteName,
		///<summary>TabId. </summary>
		TabId,
		///<summary>TabName. </summary>
		TabName,
		///<summary>TestPrefix. </summary>
		TestPrefix,
		///<summary>TimeOnSite. </summary>
		TimeOnSite,
		///<summary>TimeStamp. </summary>
		TimeStamp,
		///<summary>Version. </summary>
		Version,
		///<summary>VisitId. </summary>
		VisitId,
		///<summary>VisitNumber. </summary>
		VisitNumber,
		///<summary>VisitStartTime. </summary>
		VisitStartTime,
		///<summary>WidgetCaption. </summary>
		WidgetCaption,
		///<summary>WidgetId. </summary>
		WidgetId,
		///<summary>WidgetName. </summary>
		WidgetName,
		/// <summary></summary>
		AmountOfFields
	}



	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>Hit</summary>
		HitEntity
	}


	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	#endregion

	#region Included code

	#endregion
}

