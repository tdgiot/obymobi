﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Analytics.Cache.Data.HelperClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>Singleton implementation of the FieldInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the FieldInfoProviderBase class is threadsafe.</remarks>
	internal static class FieldInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IFieldInfoProvider _providerInstance = new FieldInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static FieldInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the FieldInfoProviderCore</summary>
		/// <returns>Instance of the FieldInfoProvider.</returns>
		public static IFieldInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the FieldInfoProvider. Used by singleton wrapper.</summary>
	internal class FieldInfoProviderCore : FieldInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="FieldInfoProviderCore"/> class.</summary>
		internal FieldInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores.</summary>
		private void Init()
		{
			this.InitClass( (1 + 0));
			InitHitEntityInfos();

			this.ConstructElementFieldStructures(InheritanceInfoProviderSingleton.GetInstance());
		}

		/// <summary>Inits HitEntity's FieldInfo objects</summary>
		private void InitHitEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(HitFieldIndex), "HitEntity");
			this.AddElementFieldInfo("HitEntity", "Action", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.Action, 1500, 0, 0);
			this.AddElementFieldInfo("HitEntity", "AdvertisementId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.AdvertisementId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "AdvertisementLocation", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.AdvertisementLocation, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "AdvertisementName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.AdvertisementName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "AlterationName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.AlterationName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "ApplicationType", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.ApplicationType, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "AppName", typeof(System.String), false, false, false, false,  (int)HitFieldIndex.AppName, 25, 0, 0);
			this.AddElementFieldInfo("HitEntity", "AttachmentId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.AttachmentId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "AttachmentName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.AttachmentName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "CategoryId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.CategoryId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "CategoryName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.CategoryName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "ClientId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.ClientId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "ClientOperationMode", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.ClientOperationMode, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "CompanyId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.CompanyId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "CompanyName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.CompanyName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "Date", typeof(System.DateTime), false, false, false, false,  (int)HitFieldIndex.Date, 0, 0, 0);
			this.AddElementFieldInfo("HitEntity", "DeliverypointGroupId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.DeliverypointGroupId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "DeliverypointGroupName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.DeliverypointGroupName, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "DeliverypointId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.DeliverypointId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "DeliverypointName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.DeliverypointName, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "DeliverypointNumber", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.DeliverypointNumber, 20, 0, 0);
			this.AddElementFieldInfo("HitEntity", "DeliverypointNumberInt", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.DeliverypointNumberInt, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "DeviceType", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.DeviceType, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "EntertainmentId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.EntertainmentId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "EntertainmentName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.EntertainmentName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "EventAction", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.EventAction, 100, 0, 0);
			this.AddElementFieldInfo("HitEntity", "EventCategory", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.EventCategory, 100, 0, 0);
			this.AddElementFieldInfo("HitEntity", "EventLabel", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.EventLabel, 1500, 0, 0);
			this.AddElementFieldInfo("HitEntity", "EventValue", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.EventValue, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "HitHour", typeof(System.Int32), false, false, false, false,  (int)HitFieldIndex.HitHour, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "HitId", typeof(System.Int64), true, false, true, false,  (int)HitFieldIndex.HitId, 0, 0, 19);
			this.AddElementFieldInfo("HitEntity", "HitMinute", typeof(System.Int32), false, false, false, false,  (int)HitFieldIndex.HitMinute, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "HitNumber", typeof(System.Int32), false, false, false, false,  (int)HitFieldIndex.HitNumber, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "HitTime", typeof(System.Int32), false, false, false, false,  (int)HitFieldIndex.HitTime, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "HitType", typeof(System.String), false, false, false, false,  (int)HitFieldIndex.HitType, 10, 0, 0);
			this.AddElementFieldInfo("HitEntity", "HitVersion", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.HitVersion, 4, 0, 0);
			this.AddElementFieldInfo("HitEntity", "IsTablet", typeof(Nullable<System.Boolean>), false, false, false, true,  (int)HitFieldIndex.IsTablet, 0, 0, 0);
			this.AddElementFieldInfo("HitEntity", "LegacyApplicationName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.LegacyApplicationName, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "LegacyApplicationVersion", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.LegacyApplicationVersion, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "LegacyCustomDimension1", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.LegacyCustomDimension1, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "LegacyCustomDimension2", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.LegacyCustomDimension2, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "MacAddress", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.MacAddress, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "MediaId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.MediaId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "MessageId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.MessageId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "MessageName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.MessageName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "MessageResponse", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.MessageResponse, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "NavigationSource", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.NavigationSource, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "OperatingSystem", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.OperatingSystem, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "OrderId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.OrderId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "OrderitemId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.OrderitemId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "OrderSource", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.OrderSource, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "OrderType", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.OrderType, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "PageId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.PageId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "PageName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.PageName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "PrimaryKeys", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.PrimaryKeys, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "ProductId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.ProductId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "ProductName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.ProductName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "RoomControlAreaId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.RoomControlAreaId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "RoomControlAreaName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.RoomControlAreaName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "RoomControlAreaSectionId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.RoomControlAreaSectionId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "RoomControlAreaSectionItemId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.RoomControlAreaSectionItemId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "RoomControlAreaSectionItemName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.RoomControlAreaSectionItemName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "RoomControlAreaSectionName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.RoomControlAreaSectionName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "RoomControlComponentId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.RoomControlComponentId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "RoomControlComponentName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.RoomControlComponentName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "RoomControlSceneName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.RoomControlSceneName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "RoomControlStationId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.RoomControlStationId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "RoomControlStationListingId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.RoomControlStationListingId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "RoomControlStationListingName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.RoomControlStationListingName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "RoomControlStationName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.RoomControlStationName, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "RoomControlThermostatFanMode", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.RoomControlThermostatFanMode, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "RoomControlThermostatMode", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.RoomControlThermostatMode, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "RoomControlThermostatScale", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.RoomControlThermostatScale, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "RoomControlThermostatTemperature", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.RoomControlThermostatTemperature, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "RoomControlTvRemoteAction", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.RoomControlTvRemoteAction, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "ScreenName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.ScreenName, 1500, 0, 0);
			this.AddElementFieldInfo("HitEntity", "ServiceRequestName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.ServiceRequestName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "SiteId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.SiteId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "SiteName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.SiteName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "TabId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.TabId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "TabName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.TabName, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "TestPrefix", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.TestPrefix, 50, 0, 0);
			this.AddElementFieldInfo("HitEntity", "TimeOnSite", typeof(System.Int32), false, false, false, false,  (int)HitFieldIndex.TimeOnSite, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "TimeStamp", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.TimeStamp, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "Version", typeof(System.Int32), false, false, false, false,  (int)HitFieldIndex.Version, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "VisitId", typeof(System.Int32), false, false, false, false,  (int)HitFieldIndex.VisitId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "VisitNumber", typeof(System.Int32), false, false, false, false,  (int)HitFieldIndex.VisitNumber, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "VisitStartTime", typeof(System.Int32), false, false, false, false,  (int)HitFieldIndex.VisitStartTime, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "WidgetCaption", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.WidgetCaption, 150, 0, 0);
			this.AddElementFieldInfo("HitEntity", "WidgetId", typeof(Nullable<System.Int32>), false, false, false, true,  (int)HitFieldIndex.WidgetId, 0, 0, 10);
			this.AddElementFieldInfo("HitEntity", "WidgetName", typeof(System.String), false, false, false, true,  (int)HitFieldIndex.WidgetName, 150, 0, 0);
		}
		
	}
}




