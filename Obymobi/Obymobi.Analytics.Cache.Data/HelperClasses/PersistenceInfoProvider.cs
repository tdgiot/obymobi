﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Analytics.Cache.Data.HelperClasses
{
	/// <summary>Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal static class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			this.InitClass(1);
			InitHitEntityMappings();
		}

		/// <summary>Inits HitEntity's mappings</summary>
		private void InitHitEntityMappings()
		{
			this.AddElementMapping("HitEntity", @"AnalyticsCache", @"dbo", "Hit", 91, 0);
			this.AddElementFieldMapping("HitEntity", "Action", "Action", true, "NVarChar", 1500, 0, 0, false, "", null, typeof(System.String), 0);
			this.AddElementFieldMapping("HitEntity", "AdvertisementId", "AdvertisementId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("HitEntity", "AdvertisementLocation", "AdvertisementLocation", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("HitEntity", "AdvertisementName", "AdvertisementName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 3);
			this.AddElementFieldMapping("HitEntity", "AlterationName", "AlterationName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("HitEntity", "ApplicationType", "ApplicationType", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("HitEntity", "AppName", "AppName", false, "NVarChar", 25, 0, 0, false, "", null, typeof(System.String), 6);
			this.AddElementFieldMapping("HitEntity", "AttachmentId", "AttachmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 7);
			this.AddElementFieldMapping("HitEntity", "AttachmentName", "AttachmentName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 8);
			this.AddElementFieldMapping("HitEntity", "CategoryId", "CategoryId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 9);
			this.AddElementFieldMapping("HitEntity", "CategoryName", "CategoryName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 10);
			this.AddElementFieldMapping("HitEntity", "ClientId", "ClientId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
			this.AddElementFieldMapping("HitEntity", "ClientOperationMode", "ClientOperationMode", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 12);
			this.AddElementFieldMapping("HitEntity", "CompanyId", "CompanyId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 13);
			this.AddElementFieldMapping("HitEntity", "CompanyName", "CompanyName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 14);
			this.AddElementFieldMapping("HitEntity", "Date", "Date", false, "Date", 0, 0, 0, false, "", null, typeof(System.DateTime), 15);
			this.AddElementFieldMapping("HitEntity", "DeliverypointGroupId", "DeliverypointGroupId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 16);
			this.AddElementFieldMapping("HitEntity", "DeliverypointGroupName", "DeliverypointGroupName", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 17);
			this.AddElementFieldMapping("HitEntity", "DeliverypointId", "DeliverypointId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 18);
			this.AddElementFieldMapping("HitEntity", "DeliverypointName", "DeliverypointName", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 19);
			this.AddElementFieldMapping("HitEntity", "DeliverypointNumber", "DeliverypointNumber", true, "NVarChar", 20, 0, 0, false, "", null, typeof(System.String), 20);
			this.AddElementFieldMapping("HitEntity", "DeliverypointNumberInt", "DeliverypointNumberInt", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 21);
			this.AddElementFieldMapping("HitEntity", "DeviceType", "DeviceType", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 22);
			this.AddElementFieldMapping("HitEntity", "EntertainmentId", "EntertainmentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 23);
			this.AddElementFieldMapping("HitEntity", "EntertainmentName", "EntertainmentName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 24);
			this.AddElementFieldMapping("HitEntity", "EventAction", "EventAction", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 25);
			this.AddElementFieldMapping("HitEntity", "EventCategory", "EventCategory", true, "NVarChar", 100, 0, 0, false, "", null, typeof(System.String), 26);
			this.AddElementFieldMapping("HitEntity", "EventLabel", "EventLabel", true, "NVarChar", 1500, 0, 0, false, "", null, typeof(System.String), 27);
			this.AddElementFieldMapping("HitEntity", "EventValue", "EventValue", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 28);
			this.AddElementFieldMapping("HitEntity", "HitHour", "HitHour", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 29);
			this.AddElementFieldMapping("HitEntity", "HitId", "HitId", false, "BigInt", 0, 19, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int64), 30);
			this.AddElementFieldMapping("HitEntity", "HitMinute", "HitMinute", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 31);
			this.AddElementFieldMapping("HitEntity", "HitNumber", "HitNumber", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 32);
			this.AddElementFieldMapping("HitEntity", "HitTime", "HitTime", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 33);
			this.AddElementFieldMapping("HitEntity", "HitType", "HitType", false, "NVarChar", 10, 0, 0, false, "", null, typeof(System.String), 34);
			this.AddElementFieldMapping("HitEntity", "HitVersion", "HitVersion", true, "NVarChar", 4, 0, 0, false, "", null, typeof(System.String), 35);
			this.AddElementFieldMapping("HitEntity", "IsTablet", "IsTablet", true, "Bit", 0, 0, 0, false, "", null, typeof(System.Boolean), 36);
			this.AddElementFieldMapping("HitEntity", "LegacyApplicationName", "LegacyApplicationName", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 37);
			this.AddElementFieldMapping("HitEntity", "LegacyApplicationVersion", "LegacyApplicationVersion", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 38);
			this.AddElementFieldMapping("HitEntity", "LegacyCustomDimension1", "LegacyCustomDimension1", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 39);
			this.AddElementFieldMapping("HitEntity", "LegacyCustomDimension2", "LegacyCustomDimension2", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 40);
			this.AddElementFieldMapping("HitEntity", "MacAddress", "MacAddress", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 41);
			this.AddElementFieldMapping("HitEntity", "MediaId", "MediaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 42);
			this.AddElementFieldMapping("HitEntity", "MessageId", "MessageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 43);
			this.AddElementFieldMapping("HitEntity", "MessageName", "MessageName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 44);
			this.AddElementFieldMapping("HitEntity", "MessageResponse", "MessageResponse", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 45);
			this.AddElementFieldMapping("HitEntity", "NavigationSource", "NavigationSource", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 46);
			this.AddElementFieldMapping("HitEntity", "OperatingSystem", "OperatingSystem", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 47);
			this.AddElementFieldMapping("HitEntity", "OrderId", "OrderId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 48);
			this.AddElementFieldMapping("HitEntity", "OrderitemId", "OrderitemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 49);
			this.AddElementFieldMapping("HitEntity", "OrderSource", "OrderSource", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 50);
			this.AddElementFieldMapping("HitEntity", "OrderType", "OrderType", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 51);
			this.AddElementFieldMapping("HitEntity", "PageId", "PageId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 52);
			this.AddElementFieldMapping("HitEntity", "PageName", "PageName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 53);
			this.AddElementFieldMapping("HitEntity", "PrimaryKeys", "PrimaryKeys", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 54);
			this.AddElementFieldMapping("HitEntity", "ProductId", "ProductId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 55);
			this.AddElementFieldMapping("HitEntity", "ProductName", "ProductName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 56);
			this.AddElementFieldMapping("HitEntity", "RoomControlAreaId", "RoomControlAreaId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 57);
			this.AddElementFieldMapping("HitEntity", "RoomControlAreaName", "RoomControlAreaName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 58);
			this.AddElementFieldMapping("HitEntity", "RoomControlAreaSectionId", "RoomControlAreaSectionId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 59);
			this.AddElementFieldMapping("HitEntity", "RoomControlAreaSectionItemId", "RoomControlAreaSectionItemId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 60);
			this.AddElementFieldMapping("HitEntity", "RoomControlAreaSectionItemName", "RoomControlAreaSectionItemName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 61);
			this.AddElementFieldMapping("HitEntity", "RoomControlAreaSectionName", "RoomControlAreaSectionName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 62);
			this.AddElementFieldMapping("HitEntity", "RoomControlComponentId", "RoomControlComponentId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 63);
			this.AddElementFieldMapping("HitEntity", "RoomControlComponentName", "RoomControlComponentName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 64);
			this.AddElementFieldMapping("HitEntity", "RoomControlSceneName", "RoomControlSceneName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 65);
			this.AddElementFieldMapping("HitEntity", "RoomControlStationId", "RoomControlStationId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 66);
			this.AddElementFieldMapping("HitEntity", "RoomControlStationListingId", "RoomControlStationListingId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 67);
			this.AddElementFieldMapping("HitEntity", "RoomControlStationListingName", "RoomControlStationListingName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 68);
			this.AddElementFieldMapping("HitEntity", "RoomControlStationName", "RoomControlStationName", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 69);
			this.AddElementFieldMapping("HitEntity", "RoomControlThermostatFanMode", "RoomControlThermostatFanMode", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 70);
			this.AddElementFieldMapping("HitEntity", "RoomControlThermostatMode", "RoomControlThermostatMode", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 71);
			this.AddElementFieldMapping("HitEntity", "RoomControlThermostatScale", "RoomControlThermostatScale", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 72);
			this.AddElementFieldMapping("HitEntity", "RoomControlThermostatTemperature", "RoomControlThermostatTemperature", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 73);
			this.AddElementFieldMapping("HitEntity", "RoomControlTvRemoteAction", "RoomControlTvRemoteAction", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 74);
			this.AddElementFieldMapping("HitEntity", "ScreenName", "ScreenName", true, "NVarChar", 1500, 0, 0, false, "", null, typeof(System.String), 75);
			this.AddElementFieldMapping("HitEntity", "ServiceRequestName", "ServiceRequestName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 76);
			this.AddElementFieldMapping("HitEntity", "SiteId", "SiteId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 77);
			this.AddElementFieldMapping("HitEntity", "SiteName", "SiteName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 78);
			this.AddElementFieldMapping("HitEntity", "TabId", "TabId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 79);
			this.AddElementFieldMapping("HitEntity", "TabName", "TabName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 80);
			this.AddElementFieldMapping("HitEntity", "TestPrefix", "TestPrefix", true, "NVarChar", 50, 0, 0, false, "", null, typeof(System.String), 81);
			this.AddElementFieldMapping("HitEntity", "TimeOnSite", "TimeOnSite", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 82);
			this.AddElementFieldMapping("HitEntity", "TimeStamp", "TimeStamp", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 83);
			this.AddElementFieldMapping("HitEntity", "Version", "Version", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 84);
			this.AddElementFieldMapping("HitEntity", "VisitId", "VisitId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 85);
			this.AddElementFieldMapping("HitEntity", "VisitNumber", "VisitNumber", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 86);
			this.AddElementFieldMapping("HitEntity", "VisitStartTime", "VisitStartTime", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 87);
			this.AddElementFieldMapping("HitEntity", "WidgetCaption", "WidgetCaption", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 88);
			this.AddElementFieldMapping("HitEntity", "WidgetId", "WidgetId", true, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 89);
			this.AddElementFieldMapping("HitEntity", "WidgetName", "WidgetName", true, "NVarChar", 150, 0, 0, false, "", null, typeof(System.String), 90);
		}

	}
}
