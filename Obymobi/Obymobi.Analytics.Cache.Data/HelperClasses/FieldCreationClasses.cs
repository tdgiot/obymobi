﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Analytics.Cache.Data.FactoryClasses;
using Obymobi.Analytics.Cache.Data;

namespace Obymobi.Analytics.Cache.Data.HelperClasses
{
	/// <summary>Field Creation Class for entity HitEntity</summary>
	public partial class HitFields
	{
		/// <summary>Creates a new HitEntity.Action field instance</summary>
		public static EntityField Action
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.Action);}
		}
		/// <summary>Creates a new HitEntity.AdvertisementId field instance</summary>
		public static EntityField AdvertisementId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.AdvertisementId);}
		}
		/// <summary>Creates a new HitEntity.AdvertisementLocation field instance</summary>
		public static EntityField AdvertisementLocation
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.AdvertisementLocation);}
		}
		/// <summary>Creates a new HitEntity.AdvertisementName field instance</summary>
		public static EntityField AdvertisementName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.AdvertisementName);}
		}
		/// <summary>Creates a new HitEntity.AlterationName field instance</summary>
		public static EntityField AlterationName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.AlterationName);}
		}
		/// <summary>Creates a new HitEntity.ApplicationType field instance</summary>
		public static EntityField ApplicationType
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.ApplicationType);}
		}
		/// <summary>Creates a new HitEntity.AppName field instance</summary>
		public static EntityField AppName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.AppName);}
		}
		/// <summary>Creates a new HitEntity.AttachmentId field instance</summary>
		public static EntityField AttachmentId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.AttachmentId);}
		}
		/// <summary>Creates a new HitEntity.AttachmentName field instance</summary>
		public static EntityField AttachmentName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.AttachmentName);}
		}
		/// <summary>Creates a new HitEntity.CategoryId field instance</summary>
		public static EntityField CategoryId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.CategoryId);}
		}
		/// <summary>Creates a new HitEntity.CategoryName field instance</summary>
		public static EntityField CategoryName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.CategoryName);}
		}
		/// <summary>Creates a new HitEntity.ClientId field instance</summary>
		public static EntityField ClientId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.ClientId);}
		}
		/// <summary>Creates a new HitEntity.ClientOperationMode field instance</summary>
		public static EntityField ClientOperationMode
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.ClientOperationMode);}
		}
		/// <summary>Creates a new HitEntity.CompanyId field instance</summary>
		public static EntityField CompanyId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.CompanyId);}
		}
		/// <summary>Creates a new HitEntity.CompanyName field instance</summary>
		public static EntityField CompanyName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.CompanyName);}
		}
		/// <summary>Creates a new HitEntity.Date field instance</summary>
		public static EntityField Date
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.Date);}
		}
		/// <summary>Creates a new HitEntity.DeliverypointGroupId field instance</summary>
		public static EntityField DeliverypointGroupId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.DeliverypointGroupId);}
		}
		/// <summary>Creates a new HitEntity.DeliverypointGroupName field instance</summary>
		public static EntityField DeliverypointGroupName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.DeliverypointGroupName);}
		}
		/// <summary>Creates a new HitEntity.DeliverypointId field instance</summary>
		public static EntityField DeliverypointId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.DeliverypointId);}
		}
		/// <summary>Creates a new HitEntity.DeliverypointName field instance</summary>
		public static EntityField DeliverypointName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.DeliverypointName);}
		}
		/// <summary>Creates a new HitEntity.DeliverypointNumber field instance</summary>
		public static EntityField DeliverypointNumber
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.DeliverypointNumber);}
		}
		/// <summary>Creates a new HitEntity.DeliverypointNumberInt field instance</summary>
		public static EntityField DeliverypointNumberInt
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.DeliverypointNumberInt);}
		}
		/// <summary>Creates a new HitEntity.DeviceType field instance</summary>
		public static EntityField DeviceType
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.DeviceType);}
		}
		/// <summary>Creates a new HitEntity.EntertainmentId field instance</summary>
		public static EntityField EntertainmentId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.EntertainmentId);}
		}
		/// <summary>Creates a new HitEntity.EntertainmentName field instance</summary>
		public static EntityField EntertainmentName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.EntertainmentName);}
		}
		/// <summary>Creates a new HitEntity.EventAction field instance</summary>
		public static EntityField EventAction
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.EventAction);}
		}
		/// <summary>Creates a new HitEntity.EventCategory field instance</summary>
		public static EntityField EventCategory
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.EventCategory);}
		}
		/// <summary>Creates a new HitEntity.EventLabel field instance</summary>
		public static EntityField EventLabel
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.EventLabel);}
		}
		/// <summary>Creates a new HitEntity.EventValue field instance</summary>
		public static EntityField EventValue
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.EventValue);}
		}
		/// <summary>Creates a new HitEntity.HitHour field instance</summary>
		public static EntityField HitHour
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.HitHour);}
		}
		/// <summary>Creates a new HitEntity.HitId field instance</summary>
		public static EntityField HitId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.HitId);}
		}
		/// <summary>Creates a new HitEntity.HitMinute field instance</summary>
		public static EntityField HitMinute
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.HitMinute);}
		}
		/// <summary>Creates a new HitEntity.HitNumber field instance</summary>
		public static EntityField HitNumber
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.HitNumber);}
		}
		/// <summary>Creates a new HitEntity.HitTime field instance</summary>
		public static EntityField HitTime
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.HitTime);}
		}
		/// <summary>Creates a new HitEntity.HitType field instance</summary>
		public static EntityField HitType
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.HitType);}
		}
		/// <summary>Creates a new HitEntity.HitVersion field instance</summary>
		public static EntityField HitVersion
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.HitVersion);}
		}
		/// <summary>Creates a new HitEntity.IsTablet field instance</summary>
		public static EntityField IsTablet
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.IsTablet);}
		}
		/// <summary>Creates a new HitEntity.LegacyApplicationName field instance</summary>
		public static EntityField LegacyApplicationName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.LegacyApplicationName);}
		}
		/// <summary>Creates a new HitEntity.LegacyApplicationVersion field instance</summary>
		public static EntityField LegacyApplicationVersion
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.LegacyApplicationVersion);}
		}
		/// <summary>Creates a new HitEntity.LegacyCustomDimension1 field instance</summary>
		public static EntityField LegacyCustomDimension1
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.LegacyCustomDimension1);}
		}
		/// <summary>Creates a new HitEntity.LegacyCustomDimension2 field instance</summary>
		public static EntityField LegacyCustomDimension2
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.LegacyCustomDimension2);}
		}
		/// <summary>Creates a new HitEntity.MacAddress field instance</summary>
		public static EntityField MacAddress
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.MacAddress);}
		}
		/// <summary>Creates a new HitEntity.MediaId field instance</summary>
		public static EntityField MediaId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.MediaId);}
		}
		/// <summary>Creates a new HitEntity.MessageId field instance</summary>
		public static EntityField MessageId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.MessageId);}
		}
		/// <summary>Creates a new HitEntity.MessageName field instance</summary>
		public static EntityField MessageName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.MessageName);}
		}
		/// <summary>Creates a new HitEntity.MessageResponse field instance</summary>
		public static EntityField MessageResponse
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.MessageResponse);}
		}
		/// <summary>Creates a new HitEntity.NavigationSource field instance</summary>
		public static EntityField NavigationSource
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.NavigationSource);}
		}
		/// <summary>Creates a new HitEntity.OperatingSystem field instance</summary>
		public static EntityField OperatingSystem
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.OperatingSystem);}
		}
		/// <summary>Creates a new HitEntity.OrderId field instance</summary>
		public static EntityField OrderId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.OrderId);}
		}
		/// <summary>Creates a new HitEntity.OrderitemId field instance</summary>
		public static EntityField OrderitemId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.OrderitemId);}
		}
		/// <summary>Creates a new HitEntity.OrderSource field instance</summary>
		public static EntityField OrderSource
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.OrderSource);}
		}
		/// <summary>Creates a new HitEntity.OrderType field instance</summary>
		public static EntityField OrderType
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.OrderType);}
		}
		/// <summary>Creates a new HitEntity.PageId field instance</summary>
		public static EntityField PageId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.PageId);}
		}
		/// <summary>Creates a new HitEntity.PageName field instance</summary>
		public static EntityField PageName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.PageName);}
		}
		/// <summary>Creates a new HitEntity.PrimaryKeys field instance</summary>
		public static EntityField PrimaryKeys
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.PrimaryKeys);}
		}
		/// <summary>Creates a new HitEntity.ProductId field instance</summary>
		public static EntityField ProductId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.ProductId);}
		}
		/// <summary>Creates a new HitEntity.ProductName field instance</summary>
		public static EntityField ProductName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.ProductName);}
		}
		/// <summary>Creates a new HitEntity.RoomControlAreaId field instance</summary>
		public static EntityField RoomControlAreaId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlAreaId);}
		}
		/// <summary>Creates a new HitEntity.RoomControlAreaName field instance</summary>
		public static EntityField RoomControlAreaName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlAreaName);}
		}
		/// <summary>Creates a new HitEntity.RoomControlAreaSectionId field instance</summary>
		public static EntityField RoomControlAreaSectionId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlAreaSectionId);}
		}
		/// <summary>Creates a new HitEntity.RoomControlAreaSectionItemId field instance</summary>
		public static EntityField RoomControlAreaSectionItemId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlAreaSectionItemId);}
		}
		/// <summary>Creates a new HitEntity.RoomControlAreaSectionItemName field instance</summary>
		public static EntityField RoomControlAreaSectionItemName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlAreaSectionItemName);}
		}
		/// <summary>Creates a new HitEntity.RoomControlAreaSectionName field instance</summary>
		public static EntityField RoomControlAreaSectionName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlAreaSectionName);}
		}
		/// <summary>Creates a new HitEntity.RoomControlComponentId field instance</summary>
		public static EntityField RoomControlComponentId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlComponentId);}
		}
		/// <summary>Creates a new HitEntity.RoomControlComponentName field instance</summary>
		public static EntityField RoomControlComponentName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlComponentName);}
		}
		/// <summary>Creates a new HitEntity.RoomControlSceneName field instance</summary>
		public static EntityField RoomControlSceneName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlSceneName);}
		}
		/// <summary>Creates a new HitEntity.RoomControlStationId field instance</summary>
		public static EntityField RoomControlStationId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlStationId);}
		}
		/// <summary>Creates a new HitEntity.RoomControlStationListingId field instance</summary>
		public static EntityField RoomControlStationListingId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlStationListingId);}
		}
		/// <summary>Creates a new HitEntity.RoomControlStationListingName field instance</summary>
		public static EntityField RoomControlStationListingName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlStationListingName);}
		}
		/// <summary>Creates a new HitEntity.RoomControlStationName field instance</summary>
		public static EntityField RoomControlStationName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlStationName);}
		}
		/// <summary>Creates a new HitEntity.RoomControlThermostatFanMode field instance</summary>
		public static EntityField RoomControlThermostatFanMode
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlThermostatFanMode);}
		}
		/// <summary>Creates a new HitEntity.RoomControlThermostatMode field instance</summary>
		public static EntityField RoomControlThermostatMode
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlThermostatMode);}
		}
		/// <summary>Creates a new HitEntity.RoomControlThermostatScale field instance</summary>
		public static EntityField RoomControlThermostatScale
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlThermostatScale);}
		}
		/// <summary>Creates a new HitEntity.RoomControlThermostatTemperature field instance</summary>
		public static EntityField RoomControlThermostatTemperature
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlThermostatTemperature);}
		}
		/// <summary>Creates a new HitEntity.RoomControlTvRemoteAction field instance</summary>
		public static EntityField RoomControlTvRemoteAction
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.RoomControlTvRemoteAction);}
		}
		/// <summary>Creates a new HitEntity.ScreenName field instance</summary>
		public static EntityField ScreenName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.ScreenName);}
		}
		/// <summary>Creates a new HitEntity.ServiceRequestName field instance</summary>
		public static EntityField ServiceRequestName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.ServiceRequestName);}
		}
		/// <summary>Creates a new HitEntity.SiteId field instance</summary>
		public static EntityField SiteId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.SiteId);}
		}
		/// <summary>Creates a new HitEntity.SiteName field instance</summary>
		public static EntityField SiteName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.SiteName);}
		}
		/// <summary>Creates a new HitEntity.TabId field instance</summary>
		public static EntityField TabId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.TabId);}
		}
		/// <summary>Creates a new HitEntity.TabName field instance</summary>
		public static EntityField TabName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.TabName);}
		}
		/// <summary>Creates a new HitEntity.TestPrefix field instance</summary>
		public static EntityField TestPrefix
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.TestPrefix);}
		}
		/// <summary>Creates a new HitEntity.TimeOnSite field instance</summary>
		public static EntityField TimeOnSite
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.TimeOnSite);}
		}
		/// <summary>Creates a new HitEntity.TimeStamp field instance</summary>
		public static EntityField TimeStamp
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.TimeStamp);}
		}
		/// <summary>Creates a new HitEntity.Version field instance</summary>
		public static EntityField Version
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.Version);}
		}
		/// <summary>Creates a new HitEntity.VisitId field instance</summary>
		public static EntityField VisitId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.VisitId);}
		}
		/// <summary>Creates a new HitEntity.VisitNumber field instance</summary>
		public static EntityField VisitNumber
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.VisitNumber);}
		}
		/// <summary>Creates a new HitEntity.VisitStartTime field instance</summary>
		public static EntityField VisitStartTime
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.VisitStartTime);}
		}
		/// <summary>Creates a new HitEntity.WidgetCaption field instance</summary>
		public static EntityField WidgetCaption
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.WidgetCaption);}
		}
		/// <summary>Creates a new HitEntity.WidgetId field instance</summary>
		public static EntityField WidgetId
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.WidgetId);}
		}
		/// <summary>Creates a new HitEntity.WidgetName field instance</summary>
		public static EntityField WidgetName
		{
			get { return (EntityField)EntityFieldFactory.Create(HitFieldIndex.WidgetName);}
		}
	}
	

}