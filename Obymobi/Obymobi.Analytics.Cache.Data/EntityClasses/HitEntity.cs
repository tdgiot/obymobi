﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections;
#if !CF
using System.Runtime.Serialization;
#endif
using Obymobi.Analytics.Cache.Data.FactoryClasses;
using Obymobi.Analytics.Cache.Data.CollectionClasses;
using Obymobi.Analytics.Cache.Data.DaoClasses;
using Obymobi.Analytics.Cache.Data.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Analytics.Cache.Data.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>Entity class which represents the entity 'Hit'. This class is used for Business Logic or for framework extension code.</summary>
	[Serializable]
	public partial class HitEntity : HitEntityBase,
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
        IFlatHitRecord
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		/// <summary>CTor</summary>
		public HitEntity():base()
		{                        
		}

		/// <summary>CTor</summary>
		/// <param name="hitId">PK value for Hit which data should be fetched into this Hit object</param>
		public HitEntity(System.Int64 hitId):
			base(hitId)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="hitId">PK value for Hit which data should be fetched into this Hit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		public HitEntity(System.Int64 hitId, IPrefetchPath prefetchPathToUse):
			base(hitId, prefetchPathToUse)
		{
		}

		/// <summary>CTor</summary>
		/// <param name="hitId">PK value for Hit which data should be fetched into this Hit object</param>
		/// <param name="validator">The custom validator object for this HitEntity</param>
		public HitEntity(System.Int64 hitId, IValidator validator):
			base(hitId, validator)
		{            
		}
		
		/// <summary>CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected HitEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included Code

		#endregion
	}
}
