﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization;
using System.Data;
using System.Xml.Serialization;
using Obymobi.Analytics.Cache.Data;
using Obymobi.Analytics.Cache.Data.FactoryClasses;
using Obymobi.Analytics.Cache.Data.DaoClasses;
using Obymobi.Analytics.Cache.Data.RelationClasses;
using Obymobi.Analytics.Cache.Data.HelperClasses;
using Obymobi.Analytics.Cache.Data.CollectionClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Analytics.Cache.Data.EntityClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity base class which represents the base class for the entity 'Hit'.<br/><br/>
	/// 
	/// </summary>
	[Serializable]
	public abstract partial class HitEntityBase : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		public string LLBLGenProEntityName {
			get { return "HitEntity"; }
		}
	
		#region Class Member Declarations

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
		}
		#endregion
		
		/// <summary>Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static HitEntityBase()
		{
			SetupCustomPropertyHashtables();
		}

		/// <summary>CTor</summary>
		protected HitEntityBase() :base("HitEntity")
		{
			InitClassEmpty(null);
		}

		/// <summary>CTor</summary>
		/// <param name="hitId">PK value for Hit which data should be fetched into this Hit object</param>
		protected HitEntityBase(System.Int64 hitId):base("HitEntity")
		{
			InitClassFetch(hitId, null, null);
		}

		/// <summary>CTor</summary>
		/// <param name="hitId">PK value for Hit which data should be fetched into this Hit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		protected HitEntityBase(System.Int64 hitId, IPrefetchPath prefetchPathToUse): base("HitEntity")
		{
			InitClassFetch(hitId, null, prefetchPathToUse);
		}

		/// <summary>CTor</summary>
		/// <param name="hitId">PK value for Hit which data should be fetched into this Hit object</param>
		/// <param name="validator">The custom validator object for this HitEntity</param>
		protected HitEntityBase(System.Int64 hitId, IValidator validator):base("HitEntity")
		{
			InitClassFetch(hitId, validator, null);
		}

		/// <summary>Private CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected HitEntityBase(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance(), PersistenceInfoProviderSingleton.GetInstance());
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}	

		/// <summary> Will perform post-ReadXml actions</summary>
		protected override void PerformPostReadXmlFixups()
		{
		}
				
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				default:
					break;				
			}
			return toReturn;
		}



		/// <summary> ISerializable member. Does custom serialization so event handlers do not get serialized.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{

			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}
		
		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}

		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}
		
		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependingRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity objects, referenced by this entity</returns>
		protected override List<IEntity> GetDependentRelatedEntities()
		{
			List<IEntity> toReturn = new List<IEntity>();
			return toReturn;
		}
		
		/// <summary> Gets a List of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection objects, referenced by this entity</returns>
		protected override List<IEntityCollection> GetMemberEntityCollections()
		{
			List<IEntityCollection> toReturn = new List<IEntityCollection>();


			return toReturn;
		}


		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="hitId">PK value for Hit which data should be fetched into this Hit object</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 hitId)
		{
			return FetchUsingPK(hitId, null, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="hitId">PK value for Hit which data should be fetched into this Hit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 hitId, IPrefetchPath prefetchPathToUse)
		{
			return FetchUsingPK(hitId, prefetchPathToUse, null, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="hitId">PK value for Hit which data should be fetched into this Hit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 hitId, IPrefetchPath prefetchPathToUse, Context contextToUse)
		{
			return FetchUsingPK(hitId, prefetchPathToUse, contextToUse, null);
		}

		/// <summary> Fetches the contents of this entity from the persistent storage using the primary key.</summary>
		/// <param name="hitId">PK value for Hit which data should be fetched into this Hit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		public bool FetchUsingPK(System.Int64 hitId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			return Fetch(hitId, prefetchPathToUse, contextToUse, excludedIncludedFields);
		}

		/// <summary> Refetches the Entity from the persistent storage. Refetch is used to re-load an Entity which is marked "Out-of-sync", due to a save action. Refetching an empty Entity has no effect. </summary>
		/// <returns>true if Refetch succeeded, false otherwise</returns>
		public override bool Refetch()
		{
			return Fetch(this.HitId, null, null, null);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new HitRelations().GetAllRelations();
		}


		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			return toReturn;
		}
	
		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validatorToUse">Validator to use.</param>
		private void InitClassEmpty(IValidator validatorToUse)
		{
			OnInitializing();
			this.Fields = CreateFields();
			this.Validator = validatorToUse;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}		

		/// <summary> Initializes the the entity and fetches the data related to the entity in this entity.</summary>
		/// <param name="hitId">PK value for Hit which data should be fetched into this Hit object</param>
		/// <param name="validator">The validator object for this HitEntity</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		private void InitClassFetch(System.Int64 hitId, IValidator validator, IPrefetchPath prefetchPathToUse)
		{
			OnInitializing();
			this.Validator = validator;
			this.Fields = CreateFields();
			InitClassMembers();	
			Fetch(hitId, prefetchPathToUse, null, null);

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassFetch
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}

		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Action", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementLocation", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AdvertisementName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AlterationName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ApplicationType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AppName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttachmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("AttachmentName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CategoryName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ClientOperationMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CompanyName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Date", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointGroupId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointGroupName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeliverypointNumberInt", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("DeviceType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EntertainmentName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EventAction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EventCategory", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EventLabel", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("EventValue", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HitHour", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HitId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HitMinute", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HitNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HitTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HitType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("HitVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("IsTablet", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LegacyApplicationName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LegacyApplicationVersion", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LegacyCustomDimension1", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LegacyCustomDimension2", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MacAddress", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MediaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("MessageResponse", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("NavigationSource", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OperatingSystem", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderitemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderSource", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("OrderType", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PageName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PrimaryKeys", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ProductName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlAreaId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlAreaName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlAreaSectionId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlAreaSectionItemId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlAreaSectionItemName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlAreaSectionName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlComponentName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlSceneName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlStationId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlStationListingId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlStationListingName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlStationName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlThermostatFanMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlThermostatMode", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlThermostatScale", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlThermostatTemperature", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("RoomControlTvRemoteAction", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ScreenName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("ServiceRequestName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("SiteName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TabId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TabName", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TestPrefix", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeOnSite", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeStamp", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Version", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VisitId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VisitNumber", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("VisitStartTime", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetCaption", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WidgetName", fieldHashtable);
		}
		#endregion

		/// <summary> Fetches the entity from the persistent storage. Fetch simply reads the entity into an EntityFields object. </summary>
		/// <param name="hitId">PK value for Hit which data should be fetched into this Hit object</param>
		/// <param name="prefetchPathToUse">the PrefetchPath which defines the graph of objects to fetch as well</param>
		/// <param name="contextToUse">The context to add the entity to if the fetch was succesful. </param>
		/// <param name="excludedIncludedFields">The list of IEntityField objects which have to be excluded or included for the fetch. 
		/// If null or empty, all fields are fetched (default). If an instance of ExcludeIncludeFieldsList is passed in and its ExcludeContainedFields property
		/// is set to false, the fields contained in excludedIncludedFields are kept in the query, the rest of the fields in the query are excluded.</param>
		/// <returns>True if succeeded, false otherwise.</returns>
		private bool Fetch(System.Int64 hitId, IPrefetchPath prefetchPathToUse, Context contextToUse, ExcludeIncludeFieldsList excludedIncludedFields)
		{
			try
			{
				OnFetch();
				this.Fields[(int)HitFieldIndex.HitId].ForcedCurrentValueWrite(hitId);
				CreateDAOInstance().FetchExisting(this, this.Transaction, prefetchPathToUse, contextToUse, excludedIncludedFields);
				return (this.Fields.State == EntityState.Fetched);
			}
			finally
			{
				OnFetchComplete();
			}
		}

		/// <summary> Creates the DAO instance for this type</summary>
		/// <returns></returns>
		protected override IDao CreateDAOInstance()
		{
			return DAOFactory.CreateHitDAO();
		}
		
		/// <summary> Creates the entity factory for this type.</summary>
		/// <returns></returns>
		protected override IEntityFactory CreateEntityFactory()
		{
			return new HitEntityFactory();
		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static HitRelations Relations
		{
			get	{ return new HitRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Action property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."Action"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 1500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String Action
		{
			get { return (System.String)GetValue((int)HitFieldIndex.Action, true); }
			set	{ SetValue((int)HitFieldIndex.Action, value, true); }
		}

		/// <summary> The AdvertisementId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."AdvertisementId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AdvertisementId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.AdvertisementId, false); }
			set	{ SetValue((int)HitFieldIndex.AdvertisementId, value, true); }
		}

		/// <summary> The AdvertisementLocation property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."AdvertisementLocation"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdvertisementLocation
		{
			get { return (System.String)GetValue((int)HitFieldIndex.AdvertisementLocation, true); }
			set	{ SetValue((int)HitFieldIndex.AdvertisementLocation, value, true); }
		}

		/// <summary> The AdvertisementName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."AdvertisementName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AdvertisementName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.AdvertisementName, true); }
			set	{ SetValue((int)HitFieldIndex.AdvertisementName, value, true); }
		}

		/// <summary> The AlterationName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."AlterationName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AlterationName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.AlterationName, true); }
			set	{ SetValue((int)HitFieldIndex.AlterationName, value, true); }
		}

		/// <summary> The ApplicationType property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."ApplicationType"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ApplicationType
		{
			get { return (System.String)GetValue((int)HitFieldIndex.ApplicationType, true); }
			set	{ SetValue((int)HitFieldIndex.ApplicationType, value, true); }
		}

		/// <summary> The AppName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."AppName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 25<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String AppName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.AppName, true); }
			set	{ SetValue((int)HitFieldIndex.AppName, value, true); }
		}

		/// <summary> The AttachmentId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."AttachmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> AttachmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.AttachmentId, false); }
			set	{ SetValue((int)HitFieldIndex.AttachmentId, value, true); }
		}

		/// <summary> The AttachmentName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."AttachmentName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String AttachmentName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.AttachmentName, true); }
			set	{ SetValue((int)HitFieldIndex.AttachmentName, value, true); }
		}

		/// <summary> The CategoryId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."CategoryId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CategoryId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.CategoryId, false); }
			set	{ SetValue((int)HitFieldIndex.CategoryId, value, true); }
		}

		/// <summary> The CategoryName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."CategoryName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CategoryName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.CategoryName, true); }
			set	{ SetValue((int)HitFieldIndex.CategoryName, value, true); }
		}

		/// <summary> The ClientId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."ClientId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ClientId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.ClientId, false); }
			set	{ SetValue((int)HitFieldIndex.ClientId, value, true); }
		}

		/// <summary> The ClientOperationMode property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."ClientOperationMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ClientOperationMode
		{
			get { return (System.String)GetValue((int)HitFieldIndex.ClientOperationMode, true); }
			set	{ SetValue((int)HitFieldIndex.ClientOperationMode, value, true); }
		}

		/// <summary> The CompanyId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."CompanyId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> CompanyId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.CompanyId, false); }
			set	{ SetValue((int)HitFieldIndex.CompanyId, value, true); }
		}

		/// <summary> The CompanyName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."CompanyName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String CompanyName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.CompanyName, true); }
			set	{ SetValue((int)HitFieldIndex.CompanyName, value, true); }
		}

		/// <summary> The Date property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."Date"<br/>
		/// Table field type characteristics (type, precision, scale, length): Date, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime Date
		{
			get { return (System.DateTime)GetValue((int)HitFieldIndex.Date, true); }
			set	{ SetValue((int)HitFieldIndex.Date, value, true); }
		}

		/// <summary> The DeliverypointGroupId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."DeliverypointGroupId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointGroupId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.DeliverypointGroupId, false); }
			set	{ SetValue((int)HitFieldIndex.DeliverypointGroupId, value, true); }
		}

		/// <summary> The DeliverypointGroupName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."DeliverypointGroupName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointGroupName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.DeliverypointGroupName, true); }
			set	{ SetValue((int)HitFieldIndex.DeliverypointGroupName, value, true); }
		}

		/// <summary> The DeliverypointId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."DeliverypointId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.DeliverypointId, false); }
			set	{ SetValue((int)HitFieldIndex.DeliverypointId, value, true); }
		}

		/// <summary> The DeliverypointName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."DeliverypointName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.DeliverypointName, true); }
			set	{ SetValue((int)HitFieldIndex.DeliverypointName, value, true); }
		}

		/// <summary> The DeliverypointNumber property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."DeliverypointNumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 20<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeliverypointNumber
		{
			get { return (System.String)GetValue((int)HitFieldIndex.DeliverypointNumber, true); }
			set	{ SetValue((int)HitFieldIndex.DeliverypointNumber, value, true); }
		}

		/// <summary> The DeliverypointNumberInt property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."DeliverypointNumberInt"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> DeliverypointNumberInt
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.DeliverypointNumberInt, false); }
			set	{ SetValue((int)HitFieldIndex.DeliverypointNumberInt, value, true); }
		}

		/// <summary> The DeviceType property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."DeviceType"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String DeviceType
		{
			get { return (System.String)GetValue((int)HitFieldIndex.DeviceType, true); }
			set	{ SetValue((int)HitFieldIndex.DeviceType, value, true); }
		}

		/// <summary> The EntertainmentId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."EntertainmentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EntertainmentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.EntertainmentId, false); }
			set	{ SetValue((int)HitFieldIndex.EntertainmentId, value, true); }
		}

		/// <summary> The EntertainmentName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."EntertainmentName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EntertainmentName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.EntertainmentName, true); }
			set	{ SetValue((int)HitFieldIndex.EntertainmentName, value, true); }
		}

		/// <summary> The EventAction property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."EventAction"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EventAction
		{
			get { return (System.String)GetValue((int)HitFieldIndex.EventAction, true); }
			set	{ SetValue((int)HitFieldIndex.EventAction, value, true); }
		}

		/// <summary> The EventCategory property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."EventCategory"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 100<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EventCategory
		{
			get { return (System.String)GetValue((int)HitFieldIndex.EventCategory, true); }
			set	{ SetValue((int)HitFieldIndex.EventCategory, value, true); }
		}

		/// <summary> The EventLabel property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."EventLabel"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 1500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String EventLabel
		{
			get { return (System.String)GetValue((int)HitFieldIndex.EventLabel, true); }
			set	{ SetValue((int)HitFieldIndex.EventLabel, value, true); }
		}

		/// <summary> The EventValue property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."EventValue"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> EventValue
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.EventValue, false); }
			set	{ SetValue((int)HitFieldIndex.EventValue, value, true); }
		}

		/// <summary> The HitHour property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."HitHour"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HitHour
		{
			get { return (System.Int32)GetValue((int)HitFieldIndex.HitHour, true); }
			set	{ SetValue((int)HitFieldIndex.HitHour, value, true); }
		}

		/// <summary> The HitId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."HitId"<br/>
		/// Table field type characteristics (type, precision, scale, length): BigInt, 19, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int64 HitId
		{
			get { return (System.Int64)GetValue((int)HitFieldIndex.HitId, true); }
			set	{ SetValue((int)HitFieldIndex.HitId, value, true); }
		}

		/// <summary> The HitMinute property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."HitMinute"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HitMinute
		{
			get { return (System.Int32)GetValue((int)HitFieldIndex.HitMinute, true); }
			set	{ SetValue((int)HitFieldIndex.HitMinute, value, true); }
		}

		/// <summary> The HitNumber property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."HitNumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HitNumber
		{
			get { return (System.Int32)GetValue((int)HitFieldIndex.HitNumber, true); }
			set	{ SetValue((int)HitFieldIndex.HitNumber, value, true); }
		}

		/// <summary> The HitTime property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."HitTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 HitTime
		{
			get { return (System.Int32)GetValue((int)HitFieldIndex.HitTime, true); }
			set	{ SetValue((int)HitFieldIndex.HitTime, value, true); }
		}

		/// <summary> The HitType property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."HitType"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 10<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.String HitType
		{
			get { return (System.String)GetValue((int)HitFieldIndex.HitType, true); }
			set	{ SetValue((int)HitFieldIndex.HitType, value, true); }
		}

		/// <summary> The HitVersion property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."HitVersion"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 4<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String HitVersion
		{
			get { return (System.String)GetValue((int)HitFieldIndex.HitVersion, true); }
			set	{ SetValue((int)HitFieldIndex.HitVersion, value, true); }
		}

		/// <summary> The IsTablet property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."IsTablet"<br/>
		/// Table field type characteristics (type, precision, scale, length): Bit, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Boolean> IsTablet
		{
			get { return (Nullable<System.Boolean>)GetValue((int)HitFieldIndex.IsTablet, false); }
			set	{ SetValue((int)HitFieldIndex.IsTablet, value, true); }
		}

		/// <summary> The LegacyApplicationName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."LegacyApplicationName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LegacyApplicationName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.LegacyApplicationName, true); }
			set	{ SetValue((int)HitFieldIndex.LegacyApplicationName, value, true); }
		}

		/// <summary> The LegacyApplicationVersion property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."LegacyApplicationVersion"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LegacyApplicationVersion
		{
			get { return (System.String)GetValue((int)HitFieldIndex.LegacyApplicationVersion, true); }
			set	{ SetValue((int)HitFieldIndex.LegacyApplicationVersion, value, true); }
		}

		/// <summary> The LegacyCustomDimension1 property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."LegacyCustomDimension1"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LegacyCustomDimension1
		{
			get { return (System.String)GetValue((int)HitFieldIndex.LegacyCustomDimension1, true); }
			set	{ SetValue((int)HitFieldIndex.LegacyCustomDimension1, value, true); }
		}

		/// <summary> The LegacyCustomDimension2 property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."LegacyCustomDimension2"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String LegacyCustomDimension2
		{
			get { return (System.String)GetValue((int)HitFieldIndex.LegacyCustomDimension2, true); }
			set	{ SetValue((int)HitFieldIndex.LegacyCustomDimension2, value, true); }
		}

		/// <summary> The MacAddress property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."MacAddress"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MacAddress
		{
			get { return (System.String)GetValue((int)HitFieldIndex.MacAddress, true); }
			set	{ SetValue((int)HitFieldIndex.MacAddress, value, true); }
		}

		/// <summary> The MediaId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."MediaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MediaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.MediaId, false); }
			set	{ SetValue((int)HitFieldIndex.MediaId, value, true); }
		}

		/// <summary> The MessageId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."MessageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> MessageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.MessageId, false); }
			set	{ SetValue((int)HitFieldIndex.MessageId, value, true); }
		}

		/// <summary> The MessageName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."MessageName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MessageName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.MessageName, true); }
			set	{ SetValue((int)HitFieldIndex.MessageName, value, true); }
		}

		/// <summary> The MessageResponse property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."MessageResponse"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String MessageResponse
		{
			get { return (System.String)GetValue((int)HitFieldIndex.MessageResponse, true); }
			set	{ SetValue((int)HitFieldIndex.MessageResponse, value, true); }
		}

		/// <summary> The NavigationSource property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."NavigationSource"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String NavigationSource
		{
			get { return (System.String)GetValue((int)HitFieldIndex.NavigationSource, true); }
			set	{ SetValue((int)HitFieldIndex.NavigationSource, value, true); }
		}

		/// <summary> The OperatingSystem property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."OperatingSystem"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OperatingSystem
		{
			get { return (System.String)GetValue((int)HitFieldIndex.OperatingSystem, true); }
			set	{ SetValue((int)HitFieldIndex.OperatingSystem, value, true); }
		}

		/// <summary> The OrderId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."OrderId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OrderId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.OrderId, false); }
			set	{ SetValue((int)HitFieldIndex.OrderId, value, true); }
		}

		/// <summary> The OrderitemId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."OrderitemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> OrderitemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.OrderitemId, false); }
			set	{ SetValue((int)HitFieldIndex.OrderitemId, value, true); }
		}

		/// <summary> The OrderSource property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."OrderSource"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderSource
		{
			get { return (System.String)GetValue((int)HitFieldIndex.OrderSource, true); }
			set	{ SetValue((int)HitFieldIndex.OrderSource, value, true); }
		}

		/// <summary> The OrderType property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."OrderType"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String OrderType
		{
			get { return (System.String)GetValue((int)HitFieldIndex.OrderType, true); }
			set	{ SetValue((int)HitFieldIndex.OrderType, value, true); }
		}

		/// <summary> The PageId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."PageId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> PageId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.PageId, false); }
			set	{ SetValue((int)HitFieldIndex.PageId, value, true); }
		}

		/// <summary> The PageName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."PageName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PageName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.PageName, true); }
			set	{ SetValue((int)HitFieldIndex.PageName, value, true); }
		}

		/// <summary> The PrimaryKeys property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."PrimaryKeys"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String PrimaryKeys
		{
			get { return (System.String)GetValue((int)HitFieldIndex.PrimaryKeys, true); }
			set	{ SetValue((int)HitFieldIndex.PrimaryKeys, value, true); }
		}

		/// <summary> The ProductId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."ProductId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> ProductId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.ProductId, false); }
			set	{ SetValue((int)HitFieldIndex.ProductId, value, true); }
		}

		/// <summary> The ProductName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."ProductName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ProductName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.ProductName, true); }
			set	{ SetValue((int)HitFieldIndex.ProductName, value, true); }
		}

		/// <summary> The RoomControlAreaId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlAreaId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlAreaId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.RoomControlAreaId, false); }
			set	{ SetValue((int)HitFieldIndex.RoomControlAreaId, value, true); }
		}

		/// <summary> The RoomControlAreaName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlAreaName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControlAreaName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.RoomControlAreaName, true); }
			set	{ SetValue((int)HitFieldIndex.RoomControlAreaName, value, true); }
		}

		/// <summary> The RoomControlAreaSectionId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlAreaSectionId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlAreaSectionId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.RoomControlAreaSectionId, false); }
			set	{ SetValue((int)HitFieldIndex.RoomControlAreaSectionId, value, true); }
		}

		/// <summary> The RoomControlAreaSectionItemId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlAreaSectionItemId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlAreaSectionItemId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.RoomControlAreaSectionItemId, false); }
			set	{ SetValue((int)HitFieldIndex.RoomControlAreaSectionItemId, value, true); }
		}

		/// <summary> The RoomControlAreaSectionItemName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlAreaSectionItemName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControlAreaSectionItemName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.RoomControlAreaSectionItemName, true); }
			set	{ SetValue((int)HitFieldIndex.RoomControlAreaSectionItemName, value, true); }
		}

		/// <summary> The RoomControlAreaSectionName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlAreaSectionName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControlAreaSectionName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.RoomControlAreaSectionName, true); }
			set	{ SetValue((int)HitFieldIndex.RoomControlAreaSectionName, value, true); }
		}

		/// <summary> The RoomControlComponentId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlComponentId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlComponentId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.RoomControlComponentId, false); }
			set	{ SetValue((int)HitFieldIndex.RoomControlComponentId, value, true); }
		}

		/// <summary> The RoomControlComponentName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlComponentName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControlComponentName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.RoomControlComponentName, true); }
			set	{ SetValue((int)HitFieldIndex.RoomControlComponentName, value, true); }
		}

		/// <summary> The RoomControlSceneName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlSceneName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControlSceneName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.RoomControlSceneName, true); }
			set	{ SetValue((int)HitFieldIndex.RoomControlSceneName, value, true); }
		}

		/// <summary> The RoomControlStationId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlStationId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlStationId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.RoomControlStationId, false); }
			set	{ SetValue((int)HitFieldIndex.RoomControlStationId, value, true); }
		}

		/// <summary> The RoomControlStationListingId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlStationListingId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlStationListingId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.RoomControlStationListingId, false); }
			set	{ SetValue((int)HitFieldIndex.RoomControlStationListingId, value, true); }
		}

		/// <summary> The RoomControlStationListingName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlStationListingName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControlStationListingName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.RoomControlStationListingName, true); }
			set	{ SetValue((int)HitFieldIndex.RoomControlStationListingName, value, true); }
		}

		/// <summary> The RoomControlStationName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlStationName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControlStationName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.RoomControlStationName, true); }
			set	{ SetValue((int)HitFieldIndex.RoomControlStationName, value, true); }
		}

		/// <summary> The RoomControlThermostatFanMode property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlThermostatFanMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControlThermostatFanMode
		{
			get { return (System.String)GetValue((int)HitFieldIndex.RoomControlThermostatFanMode, true); }
			set	{ SetValue((int)HitFieldIndex.RoomControlThermostatFanMode, value, true); }
		}

		/// <summary> The RoomControlThermostatMode property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlThermostatMode"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControlThermostatMode
		{
			get { return (System.String)GetValue((int)HitFieldIndex.RoomControlThermostatMode, true); }
			set	{ SetValue((int)HitFieldIndex.RoomControlThermostatMode, value, true); }
		}

		/// <summary> The RoomControlThermostatScale property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlThermostatScale"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControlThermostatScale
		{
			get { return (System.String)GetValue((int)HitFieldIndex.RoomControlThermostatScale, true); }
			set	{ SetValue((int)HitFieldIndex.RoomControlThermostatScale, value, true); }
		}

		/// <summary> The RoomControlThermostatTemperature property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlThermostatTemperature"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> RoomControlThermostatTemperature
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.RoomControlThermostatTemperature, false); }
			set	{ SetValue((int)HitFieldIndex.RoomControlThermostatTemperature, value, true); }
		}

		/// <summary> The RoomControlTvRemoteAction property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."RoomControlTvRemoteAction"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String RoomControlTvRemoteAction
		{
			get { return (System.String)GetValue((int)HitFieldIndex.RoomControlTvRemoteAction, true); }
			set	{ SetValue((int)HitFieldIndex.RoomControlTvRemoteAction, value, true); }
		}

		/// <summary> The ScreenName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."ScreenName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 1500<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ScreenName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.ScreenName, true); }
			set	{ SetValue((int)HitFieldIndex.ScreenName, value, true); }
		}

		/// <summary> The ServiceRequestName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."ServiceRequestName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String ServiceRequestName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.ServiceRequestName, true); }
			set	{ SetValue((int)HitFieldIndex.ServiceRequestName, value, true); }
		}

		/// <summary> The SiteId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."SiteId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> SiteId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.SiteId, false); }
			set	{ SetValue((int)HitFieldIndex.SiteId, value, true); }
		}

		/// <summary> The SiteName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."SiteName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String SiteName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.SiteName, true); }
			set	{ SetValue((int)HitFieldIndex.SiteName, value, true); }
		}

		/// <summary> The TabId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."TabId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TabId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.TabId, false); }
			set	{ SetValue((int)HitFieldIndex.TabId, value, true); }
		}

		/// <summary> The TabName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."TabName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TabName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.TabName, true); }
			set	{ SetValue((int)HitFieldIndex.TabName, value, true); }
		}

		/// <summary> The TestPrefix property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."TestPrefix"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 50<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String TestPrefix
		{
			get { return (System.String)GetValue((int)HitFieldIndex.TestPrefix, true); }
			set	{ SetValue((int)HitFieldIndex.TestPrefix, value, true); }
		}

		/// <summary> The TimeOnSite property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."TimeOnSite"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 TimeOnSite
		{
			get { return (System.Int32)GetValue((int)HitFieldIndex.TimeOnSite, true); }
			set	{ SetValue((int)HitFieldIndex.TimeOnSite, value, true); }
		}

		/// <summary> The TimeStamp property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."TimeStamp"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> TimeStamp
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.TimeStamp, false); }
			set	{ SetValue((int)HitFieldIndex.TimeStamp, value, true); }
		}

		/// <summary> The Version property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."Version"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 Version
		{
			get { return (System.Int32)GetValue((int)HitFieldIndex.Version, true); }
			set	{ SetValue((int)HitFieldIndex.Version, value, true); }
		}

		/// <summary> The VisitId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."VisitId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 VisitId
		{
			get { return (System.Int32)GetValue((int)HitFieldIndex.VisitId, true); }
			set	{ SetValue((int)HitFieldIndex.VisitId, value, true); }
		}

		/// <summary> The VisitNumber property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."VisitNumber"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 VisitNumber
		{
			get { return (System.Int32)GetValue((int)HitFieldIndex.VisitNumber, true); }
			set	{ SetValue((int)HitFieldIndex.VisitNumber, value, true); }
		}

		/// <summary> The VisitStartTime property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."VisitStartTime"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 VisitStartTime
		{
			get { return (System.Int32)GetValue((int)HitFieldIndex.VisitStartTime, true); }
			set	{ SetValue((int)HitFieldIndex.VisitStartTime, value, true); }
		}

		/// <summary> The WidgetCaption property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."WidgetCaption"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WidgetCaption
		{
			get { return (System.String)GetValue((int)HitFieldIndex.WidgetCaption, true); }
			set	{ SetValue((int)HitFieldIndex.WidgetCaption, value, true); }
		}

		/// <summary> The WidgetId property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."WidgetId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual Nullable<System.Int32> WidgetId
		{
			get { return (Nullable<System.Int32>)GetValue((int)HitFieldIndex.WidgetId, false); }
			set	{ SetValue((int)HitFieldIndex.WidgetId, value, true); }
		}

		/// <summary> The WidgetName property of the Entity Hit<br/><br/></summary>
		/// <remarks>Mapped on  table field: "Hit"."WidgetName"<br/>
		/// Table field type characteristics (type, precision, scale, length): NVarChar, 0, 0, 150<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): true, false, false</remarks>
		public virtual System.String WidgetName
		{
			get { return (System.String)GetValue((int)HitFieldIndex.WidgetName, true); }
			set	{ SetValue((int)HitFieldIndex.WidgetName, value, true); }
		}



		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}

		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		[System.ComponentModel.Browsable(false), XmlIgnore]
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary>Returns the Obymobi.Analytics.Cache.Data.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)Obymobi.Analytics.Cache.Data.EntityType.HitEntity; }
		}

		#endregion

		
		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
