﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using Obymobi.Analytics.Cache.Data.EntityClasses;
using Obymobi.Analytics.Cache.Data.FactoryClasses;
using Obymobi.Analytics.Cache.Data.CollectionClasses;
using Obymobi.Analytics.Cache.Data.HelperClasses;
using Obymobi.Analytics.Cache.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.DQE.SqlServer;


namespace Obymobi.Analytics.Cache.Data.DaoClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END

	/// <summary>General DAO class for the Hit Entity. It will perform database oriented actions for a entity of type 'HitEntity'.</summary>
	public partial class HitDAO : CommonDaoBase
	{
		/// <summary>CTor</summary>
		public HitDAO() : base(InheritanceHierarchyType.None, "HitEntity", new HitEntityFactory())
		{
		}








		
		#region Custom DAO code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomDAOCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion
		
		#region Included Code

		#endregion
	}
}
