﻿using System;

namespace Obymobi.Analytics.Cache.Data
{
    public interface IFlatHitRecord
    {
        int VisitId { get; set; }
        int VisitNumber { get; set; }
        int VisitStartTime { get; set; }
        DateTime Date { get; set; }
        int HitHour { get; set; }
        int HitMinute { get; set; }
        int HitNumber { get; set; }
        int HitTime { get; set; }
        string AppName { get; set; }
        int Version { get; set; }
        string ScreenName { get; set; }
        string EventAction { get; set; }
        string EventCategory { get; set; }
        string EventLabel { get; set; }
        int? EventValue { get; set; }
        int TimeOnSite { get; set; }
        string ApplicationType { get; set; }

        string Action { get; set; }
        int? AdvertisementId { get; set; }
        string AdvertisementLocation { get; set; }
        string AdvertisementName { get; set; }
        string AlterationName { get; set; }
        //string ApplicationName { get; set; }
        int? AttachmentId { get; set; }
        string AttachmentName { get; set; }
        int? CategoryId { get; set; }
        string CategoryName { get; set; }
        int? ClientId { get; set; }
        string ClientOperationMode { get; set; }
        int? CompanyId { get; set; }
        string CompanyName { get; set; }
        int? DeliverypointGroupId { get; set; }
        string DeliverypointGroupName { get; set; }
        int? DeliverypointId { get; set; }
        string DeliverypointName { get; set; }
        string DeliverypointNumber { get; set; }
        int? DeliverypointNumberInt { get; set; }
        string DeviceType { get; set; }
        int? EntertainmentId { get; set; }
        string EntertainmentName { get; set; }

        string HitType { get; set; }
        string HitVersion { get; set; }
        bool? IsTablet { get; set; }
        string LegacyApplicationName { get; set; }
        string LegacyApplicationVersion { get; set; }
        string LegacyCustomDimension1 { get; set; }
        string LegacyCustomDimension2 { get; set; }
        string MacAddress { get; set; }
        int? MediaId { get; set; }
        int? MessageId { get; set; }
        string MessageName { get; set; }
        string MessageResponse { get; set; }
        string NavigationSource { get; set; }
        string OperatingSystem { get; set; }
        int? OrderId { get; set; }
        int? OrderitemId { get; set; }
        string OrderSource { get; set; }
        string OrderType { get; set; }
        int? PageId { get; set; }
        string PageName { get; set; }
        string PrimaryKeys { get; set; }
        int? ProductId { get; set; }
        string ProductName { get; set; }
        int? RoomControlAreaId { get; set; }
        string RoomControlAreaName { get; set; }
        int? RoomControlAreaSectionId { get; set; }
        int? RoomControlAreaSectionItemId { get; set; }
        string RoomControlAreaSectionItemName { get; set; }
        string RoomControlAreaSectionName { get; set; }
        int? RoomControlComponentId { get; set; }
        string RoomControlComponentName { get; set; }
        string RoomControlSceneName { get; set; }
        int? RoomControlStationId { get; set; }
        int? RoomControlStationListingId { get; set; }
        string RoomControlStationListingName { get; set; }
        string RoomControlStationName { get; set; }
        string RoomControlThermostatFanMode { get; set; }
        string RoomControlThermostatMode { get; set; }
        string RoomControlThermostatScale { get; set; }
        int? RoomControlThermostatTemperature { get; set; }
        string RoomControlTvRemoteAction { get; set; }
        
        string ServiceRequestName { get; set; }
        int? SiteId { get; set; }
        string SiteName { get; set; }
        int? TabId { get; set; }
        string TabName { get; set; }
        string TestPrefix { get; set; }
        int? TimeStamp { get; set; }

        string WidgetCaption { get; set; }
        int? WidgetId { get; set; }
        string WidgetName { get; set; }
    }
}