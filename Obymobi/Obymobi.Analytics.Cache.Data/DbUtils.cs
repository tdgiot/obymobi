﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Analytics.Cache.Data.HelperClasses;
using SD.LLBLGen.Pro.DQE.SqlServer;
using System.Reflection;
using System.Data;


namespace Obymobi.Analytics.Cache.Data
{
    public partial class DbUtils
    {
        public static string GetCurrentCatalogName()
        {
            string catalogName = OriginalCatalogName;
            Dictionary<string, string> catalogNames = getOverwritesDictionary();
            if (catalogNames.ContainsKey(OriginalCatalogName))
            {
                catalogName = catalogNames[OriginalCatalogName];
                if (string.IsNullOrEmpty(catalogName)) return OriginalCatalogName;
            }
            return catalogName;
        }

        public static string OriginalCatalogName
        {
            get
            {
                if (_originalCatalogName == null)
                {
                    _originalCatalogName = PersistenceInfoProviderSingleton.GetInstance().GetFieldPersistenceInfo("CustomerEntity", "CustomerId").SourceCatalogName;
                }
                return _originalCatalogName;
            }
        }

        private static string _originalCatalogName;


        private static Dictionary<string, string> getOverwritesDictionary()
        {
            var obj = typeof(DynamicQueryEngine).InvokeMember("_catalogOverwrites", BindingFlags.GetField | BindingFlags.NonPublic | BindingFlags.Static, null, null, null);
            return obj as Dictionary<string, string>;
        }

        public static void CheckDatabaseAccessable(IDbConnection conn, ref StringBuilder errorMessage)
        {
            errorMessage.AppendLine("Checking if database is still accessable...");
            try
            {
                // Fetch current locks on database
                IDbCommand command = conn.CreateCommand();
                command.CommandText = "SELECT Bar FROM Foo";
                command.CommandType = CommandType.Text;
                command.CommandTimeout = 5;
                using (IDataReader reader = command.ExecuteReader(CommandBehavior.Default))
                {
                    errorMessage.AppendLine("Read table Foo from database: True");
                }
            }
            catch (Exception exception)
            {
                errorMessage.AppendLine("Read table Foo from database: False");
                errorMessage.AppendLine("Exception: " + exception.Message);
            }
        }

        public static void WriteDatabaseLocks(IDbConnection conn, ref StringBuilder errorMessage)
        {
            try
            {
                // Fetch current locks on database
                IDbCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT dm_tran_locks.request_session_id AS SPID,
                                               DB_NAME(dm_tran_locks.resource_database_id) AS dbname,
                                               CASE
                                                   WHEN resource_type = 'OBJECT'
                                                       THEN OBJECT_NAME(dm_tran_locks.resource_associated_entity_id)
                                                   ELSE OBJECT_NAME(partitions.OBJECT_ID)
                                               END AS ObjectName,
	                                           partitions.object_id AS LockedObjectId, 
                                               partitions.index_id,
                                               indexes.name AS index_name,
	                                           dm_tran_locks.request_mode,
                                               dm_tran_locks.request_status,
                                               dm_tran_locks.resource_type,
                                               AT.name as TransactionName,
	                                           ST.text AS SqlStatementText,
	                                           dm_tran_locks.resource_description
                                        FROM sys.dm_tran_locks
	                                         LEFT JOIN sys.partitions ON partitions.hobt_id = dm_tran_locks.resource_associated_entity_id
	                                         JOIN sys.indexes ON indexes.OBJECT_ID = partitions.OBJECT_ID AND indexes.index_id = partitions.index_id
	                                         JOIN sys.dm_exec_sessions ES ON ES.session_id = dm_tran_locks.request_session_id
	                                         JOIN sys.dm_tran_session_transactions TST ON ES.session_id = TST.session_id
                                             JOIN sys.dm_tran_active_transactions AT ON TST.transaction_id = AT.transaction_id
	                                         JOIN sys.dm_exec_connections CN ON CN.session_id = ES.session_id
	                                         CROSS APPLY sys.dm_exec_sql_text(CN.most_recent_sql_handle) AS ST
                                        WHERE resource_associated_entity_id > 0 AND resource_database_id = DB_ID()
                                        ORDER BY request_session_id, resource_associated_entity_id";
                command.CommandTimeout = 10;
                command.CommandType = CommandType.Text;
                using (IDataReader reader = command.ExecuteReader(CommandBehavior.Default))
                {
                    const string lockRowFormat = "{0,-5} | {1,-20} | {2,-20} | {3,-14} | {4,-8} | {5,-20} | {6,-8} | {7,-14} | {8,-13} | {9,-20} | {10,-20}";
                    var header = string.Format(lockRowFormat, "SPID", "DatabaseName", "ObjectName", "LockedObjectId", "index_id", "index_name", "LockType", "request_status", "resource_type", "TransactionName", "resource_description");

                    int rows = 0;
                    while (reader.Read())
                    {
                        rows++;

                        errorMessage.AppendLine(header);
                        //errorMessage.AppendFormatLine(lockRowFormat, reader[0], reader[1], reader[2], reader[3], reader[4], reader[5], reader[6], reader[7], reader[8], reader[9], reader[11]);
                        errorMessage.AppendLine(reader.GetString(10));
                        errorMessage.AppendLine();
                    }

                    if (rows == 0)
                    {
                        errorMessage.AppendLine("No locks on database this time Mr. Sherlock :)");
                    }
                }
            }
            catch (Exception exception)
            {
                errorMessage.AppendLine();
                //errorMessage.AppendFormatLine("Failed to fetch locking info from database: {0}", exception.Message);
                errorMessage.AppendLine(exception.ProcessStackTrace());
            }
        }
    }
}
