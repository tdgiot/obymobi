﻿using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Api.Logic.Repositories
{
    public class CompanyRepository
    {
        public CompanyEntity GetCompanyEntityByCompanyId(int companyId, bool prefetch = true)
        {
            PrefetchPath path = null;
            if (prefetch)
            {
                path = new PrefetchPath(EntityType.CompanyEntity);

                IPrefetchPathElement prefetchMedia = path.Add(CompanyEntityBase.PrefetchPathMediaCollection);
                prefetchMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
                prefetchMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);

                path.Add(CompanyEntityBase.PrefetchPathDeliverypointgroupCollection);
                path.Add(CompanyEntityBase.PrefetchPathCountryEntity);
                path.Add(CompanyEntityBase.PrefetchPathTimeZoneEntity);
                path.Add(CompanyEntityBase.PrefetchPathScheduleCollection).SubPath.Add(ScheduleEntityBase.PrefetchPathScheduleitemCollection);
                path.Add(CompanyEntityBase.PrefetchPathCompanyVenueCategoryCollection).SubPath.Add(CompanyVenueCategoryEntityBase.PrefetchPathVenueCategoryEntity).SubPath.Add(VenueCategoryEntityBase.PrefetchPathCustomTextCollection);

                IPrefetchPathElement prefetchTerminal = path.Add(CompanyEntityBase.PrefetchPathTerminalCollection);
                prefetchTerminal.SubPath.Add(TerminalEntityBase.PrefetchPathDeviceEntity);

                path.Add(CompanyEntityBase.PrefetchPathBusinesshoursCollection);
                path.Add(CompanyEntityBase.PrefetchPathMessageTemplateCollection);
                path.Add(CompanyEntityBase.PrefetchPathCompanyAmenityCollection);
                path.Add(CompanyEntityBase.PrefetchPathCloudStorageAccountCollection);
                path.Add(CompanyEntityBase.PrefetchPathCustomTextCollection);
                path.Add(CompanyEntityBase.PrefetchPathCompanyCurrencyCollection);
                path.Add(CompanyEntityBase.PrefetchPathCompanyCultureCollection);

                IPrefetchPathElement prefetchAvailabilities = path.Add(CompanyEntityBase.PrefetchPathAvailabilityCollection);
                prefetchAvailabilities.SubPath.Add(AvailabilityEntityBase.PrefetchPathCustomTextCollection);
                prefetchAvailabilities.SubPath.Add(AvailabilityEntityBase.PrefetchPathProductCategoryEntity);
            }

            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyFields.CompanyId == companyId);

            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(filter, path);

            if (companyCollection.Count == 0)
                throw new ObymobiException(GetCompanyResult.CompanyIdUnknown, "CompanyId '{0}'", companyId);
            if (companyCollection.Count > 1)
                throw new ObymobiException(GetCompanyResult.MultipleCompaniesFoundForCompanyId, "CompanyId '{0}'", companyId);

            return companyCollection[0];
        }
    }
}
