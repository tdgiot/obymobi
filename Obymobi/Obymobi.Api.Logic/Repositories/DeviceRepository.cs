﻿using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.Repositories
{
    public class DeviceRepository
    {
        public DeviceEntity GetDevice(string identifier)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeviceFields.Identifier == identifier);

            DeviceCollection deviceCollection = new DeviceCollection();
            deviceCollection.GetMulti(filter);

            return deviceCollection.FirstOrDefault();
        }
    }
}
