﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Api.Logic.Repositories
{
    public class MessageTemplateRepository
    {
        public MessageTemplateCollection GetMessageTemplatesForTerminal(int terminalId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.AddWithOr(TerminalMessageTemplateCategoryFields.TerminalId == terminalId);

            RelationCollection relations = new RelationCollection();
            relations.Add(MessageTemplateEntityBase.Relations.MessageTemplateCategoryMessageTemplateEntityUsingMessageTemplateId);
            relations.Add(MessageTemplateCategoryMessageTemplateEntityBase.Relations.MessageTemplateCategoryEntityUsingMessageTemplateCategoryId);
            relations.Add(MessageTemplateCategoryEntityBase.Relations.TerminalMessageTemplateCategoryEntityUsingMessageTemplateCategoryId);

            MessageTemplateCollection messageTemplateCollection = new MessageTemplateCollection();
            messageTemplateCollection.GetMulti(filter, relations);

            return messageTemplateCollection;
        }
    }
}
