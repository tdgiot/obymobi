﻿using System;
using System.Linq;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.Repositories
{
    public class ClientRepository
    {
        public ClientEntity GetFirstClientWithDeviceTokenForCompany(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);
            filter.Add(DeviceFields.Token != DBNull.Value);
            filter.Add(DeviceFields.Token != string.Empty);

            RelationCollection relations = new RelationCollection();
            relations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            prefetch.Add(ClientEntityBase.PrefetchPathDeviceEntity, new IncludeFieldsList(DeviceFields.Identifier, DeviceFields.Token));

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, 1, null, relations, prefetch, new IncludeFieldsList(ClientFields.DeviceId), 0, 0);

            return clientCollection.FirstOrDefault();
        }

        public bool Save(ClientEntity client, bool recursive)
        {
            return client.Save(recursive);
        }
    }
}
