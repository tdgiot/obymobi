﻿using System;
using System.Linq;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.Repositories
{
    public class TerminalRepository
    {
        public TerminalEntity GetFirstTerminalWithDeviceTokenForCompany(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.CompanyId == companyId);
            filter.Add(DeviceFields.Token != DBNull.Value);
            filter.Add(DeviceFields.Token != string.Empty);

            RelationCollection relations = new RelationCollection();
            relations.Add(TerminalEntityBase.Relations.DeviceEntityUsingDeviceId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathDeviceEntity, new IncludeFieldsList(DeviceFields.Identifier, DeviceFields.Token));

            TerminalCollection terminalCollection = new TerminalCollection();
            terminalCollection.GetMulti(filter, 1, null, relations, prefetch, new IncludeFieldsList(ClientFields.DeviceId), 0, 0);

            return terminalCollection.FirstOrDefault();
        }

        public bool Save(TerminalEntity terminal, bool recursive)
        {
            return terminal.Save(recursive);
        }
    }
}
