﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Api.Logic.Repositories
{
    public class CompanyCultureRepository
    {
        public CompanyCultureCollection GetCompanyCulturesForCompany(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyCultureFields.CompanyId == companyId);

            CompanyCultureCollection cultures = new CompanyCultureCollection();
            cultures.GetMulti(filter, 0, new SortExpression(CompanyCultureFields.CultureCode | SortOperator.Ascending));

            return cultures;
        }
    }
}
