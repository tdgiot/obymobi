﻿using System;
using System.Web;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class TerminalLogHelper
    {
        public enum WriteToTerminalLogResult : int
        {
            /// <summary>
            /// Update last terminal result is unknown
            /// </summary>
            Unknown = 0,

            /// <summary>
            /// Last request was successfully updated
            /// </summary>
            Success = 100,

            /// <summary>
            /// Terminal was not found in the database 
            /// </summary>
            TerminalUnknown = 200,

            /// <summary>
            /// Multiple terminals found for the specified terminal id
            /// </summary>
            MultipleTerminalsFound = 300,

            /// <summary>
            /// Writing to terminal log failed
            /// </summary>
            Failed = 400
        }

        public static void WriteToTerminalLog(int terminalId, int? orderId, string orderGuid, int type, string message, string status, string log, long fileDate = 0, string filename = "")
        {
            TerminalEntity terminalEntity = null;

            string cacheKey = string.Format("Terminal{0}", terminalId);

            if (HttpContext.Current != null && HttpContext.Current.Cache[cacheKey] != null)
            {
                terminalEntity = HttpContext.Current.Cache[cacheKey] as TerminalEntity;
            }
            else
            {
                // Create and initialize a filter
                PredicateExpression filter = new PredicateExpression();
                filter.Add(TerminalFields.TerminalId == terminalId);

                // Get a TerminalCollection instance for the specified filter
                TerminalCollection terminalCollection = new TerminalCollection();
                terminalCollection.GetMulti(filter);
                if (terminalCollection.Count == 0)
                {
                    throw new ObymobiException(WriteToTerminalLogResult.TerminalUnknown, "TerminalId: {0}", terminalId);
                }
                if (terminalCollection.Count > 1)
                {
                    throw new ObymobiException(WriteToTerminalLogResult.MultipleTerminalsFound, "TerminalId: {0}", terminalId);
                }

                terminalEntity = terminalCollection[0];
            }

            if (terminalEntity != null)
            {
                try
                {
                    if (type == (int)TerminalLogType.ShippedLog)
                    {
                        DateTime dt;

                        if (fileDate > 0)
                        {
                            DateTime parsedDate = fileDate.FromUnixTimeMillis();
                            dt = new DateTime(parsedDate.Year, parsedDate.Month, parsedDate.Day);
                        }
                        else
                        {
                            DateTime utcNow = DateTime.UtcNow;
                            dt = new DateTime(utcNow.Year, utcNow.Month, utcNow.Day);
                        }

                        TerminalLogEntity terminalLog = new TerminalLogEntity();
                        terminalLog.TerminalId = terminalId;
                        terminalLog.Type = type;
                        terminalLog.Message = message;

                        TerminalLogFileEntity terminalLogFileEntity = new TerminalLogFileEntity();
                        terminalLogFileEntity.Message = log;
                        terminalLogFileEntity.LogDate = dt;
                        terminalLogFileEntity.Application = filename.ToLower();
                        terminalLogFileEntity.Save();

                        terminalLog.TerminalLogFileId = terminalLogFileEntity.TerminalLogFileId;
                        terminalLog.Save();
                    }
                    else
                    {
                        // Require a valid OrderId
                        if (orderId.HasValue && orderId.Value <= 0)
                        {
                            orderId = null;
                        }

                        TerminalLogEntity terminalLog = new TerminalLogEntity();
                        terminalLog.TerminalId = terminalId;
                        terminalLog.Type = type;
                        terminalLog.OrderId = orderId;
                        terminalLog.OrderGuid = orderGuid;
                        terminalLog.Message = message ?? string.Empty;
                        terminalLog.Status = status ?? string.Empty;
                        terminalLog.Log = log ?? string.Empty;
                        terminalLog.Save();
                    }
                }
                catch (Exception ex)
                {
                    throw new ObymobiException(WriteToTerminalLogResult.Failed, ex, "Terminal: {0}", terminalId);
                }
            }
        }
    }
}
