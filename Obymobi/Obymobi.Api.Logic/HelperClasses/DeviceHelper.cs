﻿using Obymobi.Constants;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class DeviceHelper
    {
        public static void SetApplicationVersion(DeviceEntity deviceEntity, string appCode, string appVersion)
        {
            switch (appCode)
            {
                case ApplicationCode.Agent:
                    deviceEntity.AgentVersion = appVersion;
                    break;
                case ApplicationCode.SupportTools:
                    deviceEntity.SupportToolsVersion = appVersion;
                    break;
                case ApplicationCode.Emenu:
                case ApplicationCode.Console:
                    deviceEntity.ApplicationVersion = appVersion;
                    break;
                case ApplicationCode.MessagingService:
                    deviceEntity.MessagingServiceVersion = appVersion;
                    break;
                default:
                    throw new InvalidOperationException(string.Format("No implementation for application code '{0}'", appCode));
            }

            deviceEntity.Save();
        }
    }
}
