﻿using System;
using System.Collections.Generic;
using System.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.DaoClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    /// <summary>
    /// AnalyticsHelper class
    /// </summary>
    public class AnalyticsHelper
    {
        /// <summary>
        /// Creates the analytics for company.
        /// </summary>
        /// <param name="companyEntity">The company entity.</param>
        /// <returns></returns>
        public static Obymobi.Logic.Model.Analytics CreateAnalyticsForCompany(CompanyEntity companyEntity)
        {
            Obymobi.Logic.Model.Analytics analytics = new Obymobi.Logic.Model.Analytics();

            //// Order count
            //addOrderCount(analytics, companyEntity.CompanyId);

            //// Order information
            //PrefetchPath orderPrefetch = new PrefetchPath(EntityType.OrderEntity);
            //orderPrefetch.Add(OrderEntityBase.PrefetchPathOrderitemCollection);

            //OrderCollection ordersToday = new OrderCollection();
            //PredicateExpression filter = new PredicateExpression();
            //filter.Add(OrderFields.CompanyId == companyEntity.CompanyId);
            //filter.Add(OrderFields.CreatedUTC >= Dionysos.DateTimeUtil.MakeBeginOfDay(DateTime.UtcNow));
            //filter.Add(OrderFields.Status == OrderStatus.Processed);
            //ordersToday.GetMulti(filter, orderPrefetch);

            //analytics.Total = 0;
            //analytics.Revenue = 0;
            //foreach (OrderEntity order in ordersToday)
            //{
            //    foreach (OrderitemEntity orderItem in order.OrderitemCollection)
            //    {
            //        analytics.Revenue += orderItem.PriceIn;
            //        analytics.Total += orderItem.Quantity;
            //    }
            //}

            //// Demographics
            //CustomerCollection customers = new CustomerCollection();

            //RelationCollection relations = new RelationCollection();
            //relations.Add(CustomerEntity.Relations.OrderEntityUsingCustomerId);

            //filter = new PredicateExpression();
            //filter.Add(OrderFields.CompanyId == companyEntity.CompanyId);
            //filter.Add(OrderFields.CreatedUTC >= Dionysos.DateTimeUtil.MakeBeginOfDay(DateTime.UtcNow));
            //filter.Add(OrderFields.Status == OrderStatus.Processed);

            //customers.GetMulti(filter, relations);

            //analytics.MaleCount = 0;
            //analytics.FemaleCount = 0;
            //int totalAge = 0;
            //int ageCount = 0;
            //foreach (CustomerEntity customer in customers)
            //{
            //    if (customer.Gender.HasValue)
            //    {
            //        if (customer.Gender.Value)
            //            analytics.MaleCount++;
            //        else if (!customer.Gender.Value)
            //            analytics.FemaleCount++;
            //    }

            //    if (customer.Birthdate.HasValue)
            //    {
            //        DateTime now = DateTime.Today;
            //        int age = now.Year - customer.Birthdate.Value.Year;
            //        if (now < customer.Birthdate.Value.AddYears(age))
            //            age--;

            //        totalAge += age;
            //        ageCount++;
            //    }
            //}

            //if (ageCount > 0)
            //{
            //    analytics.AverageAge = totalAge / ageCount;
            //}
            //else
            //{
            //    analytics.AverageAge = 0;
            //}

            //// Create the bestseller list
            //analytics.Bestsellers = new string[10];

            return analytics;
        }

        /// <summary>
        /// Gets the bestsellers.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="count">The count.</param>
        /// <param name="dateFrom">The date from.</param>
        /// <param name="dateTo">The date to.</param>
        /// <returns></returns>
        public static DataTable getBestsellers(int companyId, List<int> deliverypointNumbers, int count, DateTime dateFrom, DateTime dateTo)
        {
            ResultsetFields fields = new ResultsetFields(3);
            fields.DefineField(ProductFields.ProductId, 0);
            fields.DefineField(ProductFields.Name, 1);
            fields.DefineField(OrderitemFields.Quantity, 2, "ProductAmount", "Orderitem", AggregateFunction.Sum);

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductEntity.Relations.OrderitemEntityUsingProductId);
            relations.Add(OrderitemEntity.Relations.OrderEntityUsingOrderId);

            GroupByCollection groupBy = new GroupByCollection();
            groupBy.Add(ProductFields.ProductId);
            groupBy.Add(ProductFields.Name);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderFields.CompanyId == companyId);
            filter.Add(OrderFields.Status == OrderStatus.Processed);
            filter.Add(OrderFields.CreatedUTC >= dateFrom.ToUniversalTime());
            filter.Add(OrderFields.CreatedUTC <= dateTo.AddDays(1).ToUniversalTime());
            filter.Add(ProductFields.Type == ProductType.Product);
            if (deliverypointNumbers != null)
                filter.Add(OrderFields.DeliverypointNumber == deliverypointNumbers);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(fields[2], SortOperator.Descending));

            DataTable dt = new DataTable();
            TypedListDAO dao = new TypedListDAO();
            dao.GetMultiAsDataTable(fields, dt, count, sort, filter, relations, false, groupBy, null, 0, 0);
            // Column 0: product id
            // Column 1: product name
            // Column 2: product amount
            return dt;
        }

        /// <summary>
        /// Gets the top categories.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="count">The count.</param>
        /// <param name="dateFrom">The date from.</param>
        /// <param name="dateTo">The date to.</param>
        /// <returns></returns>
        public static DataTable getTopCategories(int companyId, List<int> deliverypointNumbers, int count, DateTime dateFrom, DateTime dateTo)
        {
            ResultsetFields fields = new ResultsetFields(2);
            fields.DefineField(CategoryFields.Name, 0);
            fields.DefineField(CategoryFields.Name, 1, "CategoryAmount", "Category", AggregateFunction.Count);

            RelationCollection relations = new RelationCollection();
            relations.Add(OrderitemEntity.Relations.CategoryEntityUsingCategoryId, JoinHint.Inner);
            relations.Add(OrderitemEntity.Relations.OrderEntityUsingOrderId, JoinHint.Inner);

            GroupByCollection groupBy = new GroupByCollection();
            groupBy.Add(CategoryFields.Name);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderFields.CompanyId == companyId);
            filter.Add(OrderFields.Status == OrderStatus.Processed);
            filter.Add(OrderFields.CreatedUTC >= dateFrom.ToUniversalTime());
            filter.Add(OrderFields.CreatedUTC <= dateTo.AddDays(1).ToUniversalTime());
            if (deliverypointNumbers != null)
                filter.Add(OrderFields.DeliverypointNumber == deliverypointNumbers);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(fields[1], SortOperator.Descending));

            DataTable dt = new DataTable();
            TypedListDAO dao = new TypedListDAO();
            dao.GetMultiAsDataTable(fields, dt, count, sort, filter, relations, false, groupBy, null, 0, 0);
            // Column 0: category name
            // Column 1: category amount
            return dt;
        }

        /// <summary>
        /// Gets the deliverypoint order count.
        /// </summary>
        /// <param name="companyId">The company id.</param>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="dateFrom">The date from.</param>
        /// <param name="dateTo">The date to.</param>
        /// <returns></returns>
        public static DataTable getDeliverypointOrderCount(int companyId, List<int> deliverypointNumbers, DateTime dateFrom, DateTime dateTo)
        {
            ResultsetFields fields = new ResultsetFields(3);
            fields.DefineField(OrderFields.DeliverypointNumber, 0);
            fields.DefineField(OrderFields.Type, 1);
            fields.DefineField(OrderFields.Type, 2, "Amount", "Order", AggregateFunction.Count);

            GroupByCollection groupBy = new GroupByCollection();
            groupBy.Add(OrderFields.DeliverypointNumber);
            groupBy.Add(OrderFields.Type);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderFields.CompanyId == companyId);
            filter.Add(OrderFields.Status == OrderStatus.Processed);
            filter.Add(OrderFields.CreatedUTC >= dateFrom.ToUniversalTime());
            filter.Add(OrderFields.CreatedUTC <= dateTo.AddDays(1).ToUniversalTime());
            if (deliverypointNumbers != null)
                filter.Add(OrderFields.DeliverypointNumber == deliverypointNumbers);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(fields[0], SortOperator.Ascending));

            DataTable dt = new DataTable();
            TypedListDAO dao = new TypedListDAO();
            dao.GetMultiAsDataTable(fields, dt, 0, sort, filter, null, false, groupBy, null, 0, 0);
            // Column 0: deliverypointnumber
            // Column 1: order type
            // Column 2: number of orders
            return dt;
        }

        /// <summary>
        /// Adds the order count.
        /// </summary>
        /// <param name="analytics">The analytics.</param>
        /// <param name="companyId">The company id.</param>
        private static void addOrderCount(Obymobi.Logic.Model.Analytics analytics, int companyId)
        {
            OrderCollection orders = new OrderCollection();
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderFields.Status == OrderStatus.Processed);

            PredicateExpression filterCompany = new PredicateExpression();
            filterCompany.Add(OrderFields.CompanyId == companyId);

            // Orders last 60 mins
            filter.Add(OrderFields.CreatedUTC >= DateTime.UtcNow.AddHours(-1));
            filter.Add(filterCompany);
            analytics.OrderCountHour = orders.GetDbCount(filter);

            // Orders today
            filter = new PredicateExpression();
            filter.Add(filterCompany);
            filter.Add(OrderFields.CreatedUTC >= Dionysos.DateTimeUtil.MakeBeginOfDay(DateTime.UtcNow));
            filter.Add(OrderFields.CreatedUTC <= DateTime.UtcNow);
            analytics.OrderCount = orders.GetDbCount(filter);

            filter = new PredicateExpression();
            filter.Add(filterCompany);
            filter.Add(OrderFields.CreatedUTC >= Dionysos.DateTimeUtil.MakeBeginOfDay(DateTime.UtcNow.AddDays(-7)));
            filter.Add(OrderFields.CreatedUTC <= DateTime.UtcNow.AddDays(-7));
            analytics.OrderCountLastDay = orders.GetDbCount(filter);

            // Orders last 7 days
            filter = new PredicateExpression();
            filter.Add(filterCompany);
            filter.Add(OrderFields.CreatedUTC >= Dionysos.DateTimeUtil.MakeBeginOfDay(DateTime.UtcNow.AddDays(-7)));
            filter.Add(OrderFields.CreatedUTC <= DateTime.UtcNow);
            analytics.OrderCountWeek = orders.GetDbCount(filter);

            filter = new PredicateExpression();
            filter.Add(filterCompany);
            filter.Add(OrderFields.CreatedUTC >= Dionysos.DateTimeUtil.MakeBeginOfDay(DateTime.UtcNow.AddDays(-14)));
            filter.Add(OrderFields.CreatedUTC <= DateTime.UtcNow.AddDays(-7));
            analytics.OrderCountLastWeek = orders.GetDbCount(filter);

            // Orders last 30 days
            filter = new PredicateExpression();
            filter.Add(filterCompany);
            filter.Add(OrderFields.CreatedUTC >= Dionysos.DateTimeUtil.MakeBeginOfDay(DateTime.UtcNow.AddDays(-30)));
            filter.Add(OrderFields.CreatedUTC <= DateTime.UtcNow);
            analytics.OrderCountMonth = orders.GetDbCount(filter);

            filter = new PredicateExpression();
            filter.Add(filterCompany);
            filter.Add(OrderFields.CreatedUTC >= Dionysos.DateTimeUtil.MakeBeginOfDay(DateTime.UtcNow.AddDays(-60)));
            filter.Add(OrderFields.CreatedUTC <= DateTime.UtcNow.AddDays(-30));
            analytics.OrderCountLastMonth = orders.GetDbCount(filter);
        }
    }
}