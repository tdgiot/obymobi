﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Api.Logic.EntityConverters;

namespace Obymobi.Api.Logic.HelperClasses
{
    public static class UIScheduleHelper
    {
        public static UIScheduleItem[] GetUIScheduleDeliverypointgroup(int deliverypointgroupId)
        { 
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointgroupFields.DeliverypointgroupId == deliverypointgroupId);

            RelationCollection relations = new RelationCollection();
            relations.Add(UIScheduleEntityBase.Relations.DeliverypointgroupEntityUsingUIScheduleId, JoinHint.Inner);

            PrefetchPath prefetch = new PrefetchPath(EntityType.UIScheduleEntity);
            prefetch.Add(UIScheduleEntityBase.PrefetchPathUIScheduleItemCollection).SubPath.Add(UIScheduleItemEntityBase.PrefetchPathUIScheduleItemOccurrenceCollection);

            UIScheduleCollection uiScheduleCollection = new UIScheduleCollection();
            uiScheduleCollection.GetMulti(filter, 0, null, relations, prefetch);

            UIScheduleItem[] uiScheduleItems = new UIScheduleItem[] { };
            if (uiScheduleCollection.Count > 0)
            {
                UIScheduleEntity uiSchedule = uiScheduleCollection[0];
                uiScheduleItems = new UIScheduleItemEntityConverter().ConvertEntityCollectionToModelArray(uiSchedule.UIScheduleItemCollection);
            }

            return uiScheduleItems;
        }
    }
}
