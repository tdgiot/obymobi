﻿using System;
using System.Threading.Tasks;
using Dionysos.Data.LLBLGen;
using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class AvailabilityHelper
    {
        public static AvailabilityEntity UpdateAvailability(Availability updatedAvailability)
        {
            AvailabilityEntity updatedAvailabilityEntity = null;

            PrefetchPath prefetch = new PrefetchPath(EntityType.AvailabilityEntity);
            prefetch.Add(AvailabilityEntityBase.PrefetchPathCustomTextCollection);

            bool refetchEntity = false;

            Transaction transaction = new Transaction(System.Data.IsolationLevel.ReadUncommitted, "AvailabilityUpdater");
            try
            {
                AvailabilityEntity availabilityEntity = new AvailabilityEntity(updatedAvailability.AvailabilityId, prefetch);
                if (!availabilityEntity.IsNew)
                {
                    availabilityEntity.AddToTransaction(transaction);
                    availabilityEntity.Name = updatedAvailability.Name;
                    availabilityEntity.Status = updatedAvailability.Status;
                    availabilityEntity.Save();

                    foreach (CustomText customText in updatedAvailability.CustomTexts)
                    {
                        CustomTextEntity customTextEntity = Obymobi.Logic.HelperClasses.CustomTextHelper.AddOrUpdateEntity(CustomTextType.AvailabilityStatus,
                                                                                               customText.CultureCode,
                                                                                               customText.Text,
                                                                                               CustomTextFields.AvailabilityId,
                                                                                               updatedAvailability.AvailabilityId,
                                                                                               transaction);

                        if (customTextEntity.IsNew || customTextEntity.IsDirty || customTextEntity.IsDeleted)
                            refetchEntity = true;
                    }

                    updatedAvailabilityEntity = availabilityEntity;
                }

                transaction.Commit();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
            finally
            {
                transaction.Dispose();
            }

            if (updatedAvailabilityEntity != null && refetchEntity)
            {
                updatedAvailabilityEntity = new AvailabilityEntity(updatedAvailabilityEntity.AvailabilityId, prefetch);
            }

            return updatedAvailabilityEntity;
        }

        /// <summary>
        /// Pushes the updated availabilities to the company.
        /// </summary>
        /// <param name="availabilities">The availabilities to push.</param>
        public static void PushAvailabilityToCompany(AvailabilityEntity availabilityEntity)
        {
            Availability model = new AvailabilityEntityConverter().ConvertEntityToModel(availabilityEntity);

            // Get terminals
            PredicateExpression terminalFilter = new PredicateExpression(TerminalFields.CompanyId == availabilityEntity.CompanyId);
            IncludeFieldsList terminalIncludes = new IncludeFieldsList(TerminalFields.TerminalId, TerminalFields.CompanyId);
            TerminalCollection terminals = EntityCollection.GetMulti<TerminalCollection>(terminalFilter, null, null, null, terminalIncludes);

            // Get the clients
            DateTime currentTimeUtcNow = DateTime.UtcNow;
            DateTime clientLastRequest = currentTimeUtcNow.AddMinutes(-3);

            PredicateExpression clientsFilter = new PredicateExpression(UIWidgetAvailabilityFields.AvailabilityId == availabilityEntity.AvailabilityId);
            if (!TestUtil.IsPcFloris)
            {
                // FO-TEMP
                clientsFilter.Add(DeviceFields.LastRequestUTC > clientLastRequest);
            }
            IncludeFieldsList clientIncludes = new IncludeFieldsList(ClientFields.ClientId);
            RelationCollection clientRelations = new RelationCollection();
            clientRelations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);
            clientRelations.Add(ClientEntityBase.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId);
            clientRelations.Add(DeliverypointgroupEntityBase.Relations.UIModeEntityUsingUIModeId);
            clientRelations.Add(UIModeEntityBase.Relations.UITabEntityUsingUIModeId);
            clientRelations.Add(UITabEntityBase.Relations.UIWidgetEntityUsingUITabId);
            clientRelations.Add(UIWidgetEntityBase.Relations.UIWidgetAvailabilityEntityUsingUIWidgetId);
            ClientCollection clients = EntityCollection.GetMulti<ClientCollection>(clientsFilter, null, clientRelations, null, clientIncludes);

            Task.Factory.StartNew(() =>
                                  {
                                      string modelJson = model.ToJson();
                                      foreach (TerminalEntity terminal in terminals)
                                      {
                                          try
                                          {
                                              CometHelper.PushModelToTerminal(terminal.TerminalId, ModelType.Availability, modelJson);
                                          }
                                          catch (Exception)
                                          {
                                              // ignored, too bad
                                          }

                                      }

                                      foreach (ClientEntity client in clients)
                                      {
                                          try
                                          {
                                              CometHelper.PushModelToClient(client.ClientId, ModelType.Availability, modelJson);
                                          }
                                          catch (Exception)
                                          {
                                              // ignored, too bad
                                          }
                                      }
                                  });
        }
    }
}