﻿using System;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class ClientEntertainmentHelper
    {
        public static void SaveClientEntertainment(ClientEntertainment[] clientEntertainments, ref ObyTypedResult<ClientEntertainment> obyresult)
        {
            bool cleared = false;
            ClientEntertainmentCollection clientEntertainmentCollection = new ClientEntertainmentCollection();
            
            for (int i = 0; i < clientEntertainments.Length; i++)
            {
                ClientEntertainment clientEntertainment = clientEntertainments[i];

                if (!cleared)
                {
                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(ClientEntertainmentFields.ClientId == clientEntertainment.ClientId);

                    LLBLGenEntityCollectionUtil.DeleteMultiValidated(EntityType.ClientEntertainmentEntity.ToString(), filter);
                    clientEntertainmentCollection.Clear();

                    cleared = true;
                }

                ClientEntertainmentEntity clientEntertainmentEntity = new ClientEntertainmentEntity();
                clientEntertainmentEntity.ClientId = clientEntertainment.ClientId;
                clientEntertainmentEntity.EntertainmentId = clientEntertainment.EntertainmentId;
                clientEntertainmentCollection.Add(clientEntertainmentEntity);
            }
            try
            {
                int savedCount = clientEntertainmentCollection.SaveMulti(true);
                if (savedCount != clientEntertainmentCollection.Count)
                {
                    throw new ObymobiException(ClientEntertainmentSaveResult.EntitySaveRecursiveFalse);
                }
            }
            catch (Exception ex)
            {
                throw new ObymobiException(ClientEntertainmentSaveResult.EntitySaveRecursiveException, "Error: {0}\r\nStacktrace: {1}", ex.Message, ex.StackTrace);
            }
        }
    }
}
