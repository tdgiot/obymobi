﻿using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class AnnouncementHelper
    {
        /// <summary>
        /// Gets the announcements for the specified deliverypointgroup.
        /// </summary>
        /// <param name="deliverypointgroupId">The id of the deliverypointgroup to get the announcements for</param>
        /// <returns>An array of <see cref="Announcement"/> instances</returns>
        public static Announcement[] GetAnnouncementsForDeliverypointgroup(int deliverypointgroupId)
        {
            PredicateExpression filter = new PredicateExpression(DeliverypointgroupAnnouncementFields.DeliverypointgroupId == deliverypointgroupId);
            RelationCollection relations = new RelationCollection(AnnouncementEntityBase.Relations.DeliverypointgroupAnnouncementEntityUsingAnnouncementId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.AnnouncementEntity);
            prefetch.Add(AnnouncementEntityBase.PrefetchPathCustomTextCollection);

            AnnouncementCollection announcementCollection = new AnnouncementCollection();
            announcementCollection.GetMulti(filter, 0, null, relations, prefetch);

            return new AnnouncementEntityConverter().ConvertEntityCollectionToModelArray(announcementCollection);
        }
    }
}
