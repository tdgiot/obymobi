﻿using System;
using System.Linq;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Constants;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Text;

namespace Obymobi.Api.Logic.HelperClasses
{
    public static class MessageHelper
    {
        public static Message[] GetMessages(int customerId, int clientId, int deliverypointId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(MessageRecipientFields.Queued == false);

            // Customer (CustomerId == customerId)
            PredicateExpression customerFilter = new PredicateExpression();
            if (customerId > 0)
            {
                customerFilter.Add(MessageRecipientFields.CustomerId == customerId);
            }
            if (clientId > 0)
            {
                customerFilter.Add(MessageRecipientFields.ClientId == clientId);
            }
            if (deliverypointId > 0)
            {
                customerFilter.AddWithOr(MessageRecipientFields.DeliverypointId == deliverypointId);
            }
            filter.Add(customerFilter);

            try
            {
                // Remove recipients that are older than 5 minutes
                PredicateExpression cleanupFilter = new PredicateExpression();
                cleanupFilter.Add(MessageRecipientFields.SentUTC < DateTime.UtcNow.Subtract(new TimeSpan(0, 0, 0, 0, ObymobiIntervals.MESSAGEHELPER_DELETEMESSAGE_THRESHOLD)));

                // MessageEntity does not have a validator, just do DeleteMulti
                MessageRecipientCollection collection = new MessageRecipientCollection();
                collection.DeleteMulti(cleanupFilter);
            }
            catch (Exception)
            {
                // ignored
                // For now we stop caring if this fails
            }

            PrefetchPath prefetch = new PrefetchPath(EntityType.MessageRecipientEntity);
            prefetch.Add(MessageRecipientEntityBase.PrefetchPathMessageEntity).SubPath.Add(MessageEntityBase.PrefetchPathProductCategoryEntity);

            MessageRecipientCollection messageRecipientCollection = new MessageRecipientCollection();
            messageRecipientCollection.GetMulti(filter, prefetch);

            // Initialize the array
            List<Message> messages = new List<Message>();
            foreach (MessageRecipientEntity messageRecipientEntity in messageRecipientCollection)
            {
                Message message = Obymobi.Logic.HelperClasses.MessageHelper.CreateMessageModelFromEntity(messageRecipientEntity.MessageEntity);
                message = Obymobi.Logic.HelperClasses.MessageHelper.SetMessageRecipientInfo(message, messageRecipientEntity);
                messages.Add(message);
            }

            return messages.ToArray();
        }

        public static void RemoveMessageRecipientById(int messageRecipientId, DialogCloseResult closeResult)
        {
            MessageRecipientEntity recipient = new MessageRecipientEntity(messageRecipientId);
            if (!recipient.IsNew)
            {
                int receiverId = recipient.CustomerId ?? recipient.ClientId ?? recipient.DeliverypointId ?? 0;

                MessageEntity message = new MessageEntity();
                if (closeResult == DialogCloseResult.OkButton)
                {
                    message.Fields[(int)MessageFieldIndex.RecipientsOkResultCount].ExpressionToApply = (MessageFields.RecipientsOkResultCount + 1);
                    message.Fields[(int)MessageFieldIndex.RecipientsOkResultAsString].ExpressionToApply = new DbFunctionCall("({0} + {1})", new object[] { string.Format("{0},", receiverId), MessageFields.RecipientsOkResultAsString });
                }
                else if (closeResult == DialogCloseResult.NoButton)
                {
                    message.Fields[(int)MessageFieldIndex.RecipientsNoResultCount].ExpressionToApply = (MessageFields.RecipientsNoResultCount + 1);
                    message.Fields[(int)MessageFieldIndex.RecipientsNoResultAsString].ExpressionToApply = new DbFunctionCall("({0} + {1})", new object[] { string.Format("{0},", receiverId), MessageFields.RecipientsNoResultAsString });
                }
                else if (closeResult == DialogCloseResult.YesButton)
                {
                    message.Fields[(int)MessageFieldIndex.RecipientsYesResultCount].ExpressionToApply = (MessageFields.RecipientsYesResultCount + 1);
                    message.Fields[(int)MessageFieldIndex.RecipientsYesResultAsString].ExpressionToApply = new DbFunctionCall("({0} + {1})", new object[] { string.Format("{0},", receiverId), MessageFields.RecipientsYesResultAsString });
                }
                else if (closeResult == DialogCloseResult.TimeOut)
                {
                    message.Fields[(int)MessageFieldIndex.RecipientsTimeOutResultCount].ExpressionToApply = (MessageFields.RecipientsTimeOutResultCount + 1);
                    message.Fields[(int)MessageFieldIndex.RecipientsTimeOutResultAsString].ExpressionToApply = new DbFunctionCall("({0} + {1})", new object[] { string.Format("{0},", receiverId), MessageFields.RecipientsTimeOutResultAsString });
                }
                else if (closeResult == DialogCloseResult.ClearManually)
                {
                    message.Fields[(int)MessageFieldIndex.RecipientsClearManuallyResultCount].ExpressionToApply = (MessageFields.RecipientsClearManuallyResultCount + 1);
                    message.Fields[(int)MessageFieldIndex.RecipientsClearManuallyResultAsString].ExpressionToApply = new DbFunctionCall("({0} + {1})", new object[] { string.Format("{0},", receiverId), MessageFields.RecipientsClearManuallyResultAsString });
                }

                MessageCollection messages = new MessageCollection();
                messages.UpdateMulti(message, new PredicateExpression(MessageFields.MessageId == recipient.MessageId));
            }

            // Remove recipient
            MessageRecipientCollection recipients = new MessageRecipientCollection();
            recipients.DeleteMulti(new PredicateExpression(MessageRecipientFields.MessageRecipientId == messageRecipientId));
        }

        public static MessageRecipientCollection CreateMessageRecipientsForMessagegroups(string messagegroupIdsAsString, int clientId, int companyId, List<Obymobi.Logic.Model.Action> actions)
        {
            MessageRecipientCollection recipients = new MessageRecipientCollection();

            if (messagegroupIdsAsString.Length > 0)
            {
                List<int> messagegroupIds = new List<int>();

                string[] messagegroupIdStringValues = messagegroupIdsAsString.Split(',');
                for (int i = 0; i < messagegroupIdStringValues.Length; i++)
                {
                    int messagegroupId;
                    if (int.TryParse(messagegroupIdStringValues[i], out messagegroupId) && messagegroupId > 0)
                    {
                        messagegroupIds.Add(messagegroupId);
                    }
                }

                if (messagegroupIds.Count > 0)
                {
                    DeliverypointCollection deliverypoints = Obymobi.Logic.HelperClasses.DeliverypointHelper.GetDeliverypointsLinkedToOnlineClientsForCompany(companyId);
                    DateTime localDateTime = DateTime.UtcNow.UtcToLocalTime(Obymobi.Logic.HelperClasses.CompanyHelper.GetCompanyTimeZone(companyId));

                    PredicateExpression filter = new PredicateExpression();
                    filter.Add(MessagegroupFields.MessagegroupId == messagegroupIds);

                    IncludeFieldsList deliverypointFields = new IncludeFieldsList();
                    deliverypointFields.Add(DeliverypointFields.DeliverypointId);
                    deliverypointFields.Add(DeliverypointFields.DeliverypointgroupId);

                    PrefetchPath prefetch = new PrefetchPath(EntityType.MessagegroupEntity);
                    prefetch.Add(MessagegroupEntityBase.PrefetchPathDeliverypointCollectionViaMessagegroupDeliverypoint, deliverypointFields);

                    MessagegroupCollection messagegroupEntities = new MessagegroupCollection();
                    messagegroupEntities.GetMulti(filter, prefetch);

                    foreach (MessagegroupEntity messagegroup in messagegroupEntities)
                    {
                        if (messagegroup.Type == MessagegroupType.Custom)
                        {
                            foreach (DeliverypointEntity deliverypoint in messagegroup.DeliverypointCollectionViaMessagegroupDeliverypoint)
                            {
                                Obymobi.Logic.Model.Action action = actions.FirstOrDefault(x => x.DeliverypointgroupId == deliverypoint.DeliverypointgroupId);
                                recipients.Add(Obymobi.Logic.HelperClasses.MessageHelper.CreateMessageRecipientEntity(0, clientId, deliverypoint.DeliverypointId, action));
                            }
                        }
                        else
                        {
                            Messagegroup[] models = PmsMessagegroupHelper.GetPmsMessagegroups(companyId, messagegroup.Type, localDateTime, messagegroup.GroupName);
                            if (models.Length > 0)
                            {
                                List<string> deliverypointNumbers = new List<string>();
                                foreach (int deliverypointNumberAsInt in models[0].DeliverypointNumbers)
                                {
                                    deliverypointNumbers.Add(deliverypointNumberAsInt.ToString());
                                }
                                recipients.AddRange(MessageHelper.CreateMessageRecipientsForDeliverypoints(deliverypoints, deliverypointNumbers, new List<int>(), clientId, actions));
                            }
                        }
                    }
                }
            }

            return recipients;
        }

        public static MessageRecipientCollection CreateMessageRecipientsForDeliverypoints(string deliverypointNumbersAsString, string deliverypointgroupIdsAsString, int clientId, int companyId, List<Obymobi.Logic.Model.Action> actions)
        {
            DeliverypointCollection deliverypoints = Obymobi.Logic.HelperClasses.DeliverypointHelper.GetDeliverypointsLinkedToOnlineClientsForCompany(companyId);

            List<string> deliverypointNumbers = new List<string>();
            List<int> deliverypointgroupIds = new List<int>();

            if (!deliverypointNumbersAsString.IsNullOrWhiteSpace())
            {
                foreach (string deliverypointNumber in deliverypointNumbersAsString.Split(','))
                {
                    deliverypointNumbers.Add(deliverypointNumber);
                }
            }

            if (!deliverypointgroupIdsAsString.IsNullOrWhiteSpace())
            {
                foreach (string deliverypointgroupIdStr in deliverypointgroupIdsAsString.Split(','))
                {
                    int numberInt;
                    if (int.TryParse(deliverypointgroupIdStr, out numberInt) && numberInt > 0)
                    {
                        deliverypointgroupIds.Add(numberInt);
                    }
                }
            }

            return MessageHelper.CreateMessageRecipientsForDeliverypoints(deliverypoints, deliverypointNumbers, deliverypointgroupIds, clientId, actions);
        }

        private static MessageRecipientCollection CreateMessageRecipientsForDeliverypoints(DeliverypointCollection allDeliverypoints, List<string> deliverypointNumbers, List<int> deliverypointgroupIds, int clientId, List<Obymobi.Logic.Model.Action> actions)
        {
            EntityView<DeliverypointEntity> deliverypointView = allDeliverypoints.DefaultView;

            PredicateExpression deliverypointFilter = new PredicateExpression();

            if (deliverypointNumbers.Count > 0)
            {
                PredicateExpression deliverypointNumberFilter = new PredicateExpression();
                deliverypointNumberFilter.Add(DeliverypointFields.Number == deliverypointNumbers);

                deliverypointFilter.Add(deliverypointNumberFilter);
            }

            if (deliverypointgroupIds.Count > 0)
            {
                PredicateExpression deliverypointgroupFilter = new PredicateExpression();
                deliverypointgroupFilter.Add(DeliverypointFields.DeliverypointgroupId == deliverypointgroupIds);

                deliverypointFilter.Add(deliverypointgroupFilter);
            }

            deliverypointView.Filter = deliverypointFilter;

            MessageRecipientCollection recipients = new MessageRecipientCollection();
            if (deliverypointView.Count > 0)
            {
                foreach (DeliverypointEntity deliverypoint in deliverypointView)
                {
                    Obymobi.Logic.Model.Action action = actions.FirstOrDefault(x => x.DeliverypointgroupId == deliverypoint.DeliverypointgroupId);
                    recipients.Add(Obymobi.Logic.HelperClasses.MessageHelper.CreateMessageRecipientEntity(0, clientId, deliverypoint.DeliverypointId, action));
                }
            }
            return recipients;
        }

        public static string GetAccessCodesAsStringForMobileClients(int companyId)
        {
            StringBuilder tagBuilder = new StringBuilder();

            // Get all access codes that contain this terminal's company
            PredicateExpression filter = new PredicateExpression();
            filter.Add(new FieldCompareSetPredicate(AccessCodeFields.AccessCodeId, AccessCodeCompanyFields.AccessCodeId, SetOperator.In, AccessCodeCompanyFields.CompanyId == companyId));

            AccessCodeCollection accessCodes = new AccessCodeCollection();
            accessCodes.GetMulti(filter);

            // Only continue if this terminal's company is in one or more terminals
            if (accessCodes.Count > 0)
            {
                // Build a tag expression                
                foreach (AccessCodeEntity accessCode in accessCodes)
                {
                    if (tagBuilder.Length > 0)
                    {
                        tagBuilder.Append(" || ");
                    }
                    tagBuilder.Append(accessCode.Code);
                }
            }
            return tagBuilder.ToString();
        }

        public static MessageEntity CreateMessageEntity(int orderId, int companyId, string title, string message, int duration, int mediaId, int categoryId, int entertainmentId, int productId, bool urgent, int messageLayoutType)
        {
            MessageEntity entity = new MessageEntity();
            entity.CompanyId = companyId;
            entity.Title = title;
            entity.Message = message;
            entity.Duration = duration;
            entity.MediaId = mediaId;
            entity.CategoryId = categoryId;
            entity.EntertainmentId = entertainmentId;
            entity.ProductId = productId;
            entity.MessageButtonType = (categoryId == 0 && entertainmentId == 0 && productId == 0) ? (int)MessageButtonType.Ok : (int)MessageButtonType.YesNo;
            entity.Urgent = urgent;
            entity.MessageLayoutType = messageLayoutType.ToEnum<MessageLayoutType>();
            entity.UpdatedBy = 0;
            entity.CreatedBy = 0;
            entity.RecipientsOkResultAsString = " ";
            entity.RecipientsYesResultAsString = " ";
            entity.RecipientsNoResultAsString = " ";
            entity.RecipientsTimeOutResultAsString = " ";
            entity.RecipientsClearManuallyResultAsString = " ";
            entity.Queued = true;

            if (orderId > 0)
            {
                OrderEntity order = new OrderEntity(orderId);
                if (order.IsNew)
                {
                    throw new ObymobiException(GenericResult.Failure, "Order with id {0} does not exists.", orderId);
                }
                else
                {
                    entity.OrderId = orderId;
                }
            }

            return entity;
        }
    }
}