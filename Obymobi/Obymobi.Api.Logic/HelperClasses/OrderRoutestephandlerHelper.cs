﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class OrderRoutestephandlerHelper
    {
        public static OrderRoutestephandlerEntity GetOrderRoutestephandlerByGuid(string guid)
        {
            PredicateExpression filter = new PredicateExpression(OrderRoutestephandlerFields.Guid == guid);

            OrderRoutestephandlerCollection orderRoutestephandlerCollection = new OrderRoutestephandlerCollection();
            orderRoutestephandlerCollection.GetMulti(filter);

            if (orderRoutestephandlerCollection.Count > 0)
                return orderRoutestephandlerCollection[0];
            else
                return null;
        }
    }
}
