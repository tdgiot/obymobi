﻿using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class PmsMessagegroupHelper
    {
        public static Messagegroup[] GetPmsMessagegroups(int companyId, MessagegroupType type, DateTime date, string groupName)
        {
            List<Messagegroup> messagegroups = new List<Messagegroup>();

            IncludeFieldsList includes = new IncludeFieldsList(GuestInformationFields.DeliverypointNumber);
            includes.Add(GuestInformationFields.CheckInUTC);
            includes.Add(GuestInformationFields.CheckOutUTC);
            includes.Add(GuestInformationFields.GroupName);

            GuestInformationCollection guestInformations = Obymobi.Logic.HelperClasses.GuestInformationHelper.GetGuestInformationsForMessagegroup(companyId, type, date, groupName, includes);
            messagegroups.Add(PmsMessagegroupHelper.ConvertGuestInformationsToMessagegroup("GuestInformations", type, groupName, guestInformations));

            return messagegroups.ToArray();
        }

        private static Messagegroup ConvertGuestInformationsToMessagegroup(string name, MessagegroupType type, string groupName, GuestInformationCollection guestInformations)
        {
            Messagegroup group = new Messagegroup();
            group.Name = name;
            group.Type = (int)type;
            group.GroupName = groupName;

            List<int> deliverypointNumbers = new List<int>();
            foreach (GuestInformationEntity guestInformation in guestInformations)
            {
                int deliverypointNumber;
                if (int.TryParse(guestInformation.DeliverypointNumber, out deliverypointNumber))
                {
                    deliverypointNumbers.Add(deliverypointNumber);
                }
            }
            group.DeliverypointNumbers = deliverypointNumbers.ToArray();
            return group;
        }       
    }
}
