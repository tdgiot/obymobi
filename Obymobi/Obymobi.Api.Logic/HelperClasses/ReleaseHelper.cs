﻿using System;
using Dionysos;
using Obymobi.Constants;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Logic.Model;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class ReleaseHelper
    {
        /// <summary>
        /// Sets the update status of a device
        /// </summary>
        /// <param name="companyId">The id of the company.</param>
        /// <param name="updateStatus">The update status of the device.</param>
        /// <param name="customMessage">A custom message for the log.</param>
        /// <param name="device">The device to set the update status for.</param>
        /// <param name="releaseId">The id of the release.</param>
        /// <returns>True if setting the update status succeeded, False if not.</returns>
        public static bool SetUpdateStatus(int companyId, int updateStatus, string customMessage, DeviceEntity deviceEntity, int releaseId)
        {
            // Check if a company was specified
            if (companyId == 0)
                throw new ObymobiException(Obymobi.Logic.HelperClasses.ReleaseHelper.ReleaseHelperErrors.NoCompanyIdSpecified);

            // Check if a release was specified
            if (releaseId == 0)
                throw new ObymobiException(Obymobi.Logic.HelperClasses.ReleaseHelper.ReleaseHelperErrors.NoReleaseIdSpecified);

            ReleaseEntity releaseEntity = new ReleaseEntity();
            releaseEntity.FetchUsingPK(releaseId, null, null, new ExcludeFieldsList { ReleaseFields.File });

            UpdateStatus status = (UpdateStatus)updateStatus;

            ClientEntity client = deviceEntity.ClientEntitySingle;
            TerminalEntity terminal = deviceEntity.TerminalEntitySingle;

            #region Log writing

            string message = string.Empty;
            int type = -1;

            switch (status)
            {
                case UpdateStatus.DownloadStarted:

                    if (client != null)                                 // Client
                        type = (int)ClientLogType.DownloadStarted;
                    else if (terminal != null)                          // Terminal
                        type = (int)TerminalLogType.DownloadStarted;

                    message = string.Format("Download started for release with id '{0}', application '{1}' and version '{2}'.", releaseId, releaseEntity.ApplicationName, releaseEntity.Version);

                    break;

                case UpdateStatus.DownloadCompleted:

                    if (client != null)                                 // Client
                        type = (int)ClientLogType.DownloadCompleted;
                    else if (terminal != null)                          // Terminal
                        type = (int)TerminalLogType.DownloadCompleted;

                    message = string.Format("Download completed for release with id '{0}', application '{1}' and version '{2}'.", releaseId, releaseEntity.ApplicationName, releaseEntity.Version);

                    break;

                case UpdateStatus.DownloadFailed:

                    if (client != null)                                 // Client
                        type = (int)ClientLogType.DownloadFailed;
                    else if (terminal != null)                          // Terminal
                        type = (int)TerminalLogType.DownloadFailed;

                    message = string.Format("Download failed for release with id '{0}', application '{1}' and version '{2}'.", releaseId, releaseEntity.ApplicationName, releaseEntity.Version);

                    break;

                case UpdateStatus.InstallationStarted:

                    if (client != null)                                 // Client
                        type = (int)ClientLogType.InstallationStarted;
                    else if (terminal != null)                          // Terminal
                        type = (int)TerminalLogType.InstallationStarted;

                    message = string.Format("Installation started for release with id '{0}', application '{1}' and version '{2}'.", releaseId, releaseEntity.ApplicationName, releaseEntity.Version);

                    break;

                case UpdateStatus.InstallationCompleted:

                    if (client != null)                                 // Client
                        type = (int)ClientLogType.InstallationCompleted;
                    else if (terminal != null)                          // Terminal
                        type = (int)TerminalLogType.InstallationCompleted;

                    message = string.Format("Installation completed for release with id '{0}', application '{1}' and version '{2}'.", releaseId, releaseEntity.ApplicationName, releaseEntity.Version);

                    break;

                case UpdateStatus.InstallationFailed:

                    if (client != null)                                 // Client
                        type = (int)ClientLogType.InstallationFailed;
                    else if (terminal != null)                          // Terminal
                        type = (int)TerminalLogType.InstallationFailed;

                    message = string.Format("Installation failed for release with id '{0}', application '{1}' and version '{2}'.", releaseId, releaseEntity.ApplicationName, releaseEntity.Version);

                    break;
            }

            if (!customMessage.IsNullOrWhiteSpace())
                message += " Message: " + customMessage;

            if (client != null)              // Client
            {
                ClientLogEntity clientLog = new ClientLogEntity();
                clientLog.ClientId = client.ClientId;
                clientLog.Type = type;
                clientLog.Message = message;
                clientLog.Save();
            }
            else if (terminal != null)       // Terminal
            {
                TerminalLogEntity terminalLog = new TerminalLogEntity();
                terminalLog.TerminalId = terminal.TerminalId;
                terminalLog.Type = type;
                terminalLog.Message = message;
                terminalLog.Save();
            }

            #endregion

            #region Update file setting

            if (client != null)
            {
                string applicationCode = releaseEntity.ApplicationEntity.Code;

                switch (status)
                {
                    case UpdateStatus.DownloadStarted:
                    case UpdateStatus.DownloadFailed:
                    case UpdateStatus.InstallationCompleted:
                        if (applicationCode.Equals(ApplicationCode.Emenu))
                            deviceEntity.UpdateEmenuDownloaded = false;
                        else if (applicationCode.Equals(ApplicationCode.Agent))
                            deviceEntity.UpdateAgentDownloaded = false;
                        else if (applicationCode.Equals(ApplicationCode.SupportTools))
                            deviceEntity.UpdateSupportToolsDownloaded = false;
                        else if (applicationCode.StartsWith("CraveOS", StringComparison.InvariantCultureIgnoreCase))
                            deviceEntity.UpdateOSDownloaded = false;
                        break;
                    case UpdateStatus.DownloadCompleted:
                        if (applicationCode.Equals(ApplicationCode.Emenu))
                            client.DeviceEntity.UpdateEmenuDownloaded = true;
                        else if (applicationCode.Equals(ApplicationCode.Agent))
                            client.DeviceEntity.UpdateAgentDownloaded = true;
                        else if (applicationCode.Equals(ApplicationCode.SupportTools))
                            client.DeviceEntity.UpdateSupportToolsDownloaded = true;
                        else if (applicationCode.StartsWith("CraveOS", StringComparison.InvariantCultureIgnoreCase))
                            client.DeviceEntity.UpdateOSDownloaded = true;
                        client.DeviceEntity.Save();
                        break;
                }
            }
            else if (terminal != null)
            {
                string applicationCode = releaseEntity.ApplicationEntity.Code;

                switch (status)
                {
                    case UpdateStatus.DownloadStarted:
                    case UpdateStatus.DownloadFailed:
                    case UpdateStatus.InstallationCompleted:
                        if (applicationCode.Equals(ApplicationCode.Console))
                            deviceEntity.UpdateConsoleDownloaded = false;
                        else if (applicationCode.Equals(ApplicationCode.Agent))
                            deviceEntity.UpdateAgentDownloaded = false;
                        else if (applicationCode.Equals(ApplicationCode.SupportTools))
                            deviceEntity.UpdateSupportToolsDownloaded = false;
                        else if (applicationCode.StartsWith("CraveOS", StringComparison.InvariantCultureIgnoreCase))
                            deviceEntity.UpdateOSDownloaded = false;
                        break;
                    case UpdateStatus.DownloadCompleted:
                        if (applicationCode.Equals(ApplicationCode.Console))
                            deviceEntity.UpdateConsoleDownloaded = true;
                        else if (applicationCode.Equals(ApplicationCode.Agent))
                            deviceEntity.UpdateAgentDownloaded = true;
                        else if (applicationCode.Equals(ApplicationCode.SupportTools))
                            deviceEntity.UpdateSupportToolsDownloaded = true;
                        else if (applicationCode.StartsWith("CraveOS", StringComparison.InvariantCultureIgnoreCase))
                            deviceEntity.UpdateOSDownloaded = true;
                        break;
                }
            }

            #endregion

            // Update the device's update status
            deviceEntity.UpdateStatusChangedUTC = DateTime.UtcNow;
            deviceEntity.UpdateStatus = (int)status;

            return deviceEntity.Save();
        }

        /// <summary>
        /// Gets the latest release for the specified application code and company id
        /// </summary>
        /// <param name="applicationCode">The code of the application.</param>
        /// <param name="companyId">The id of the company.</param>
        /// <returns>A <see cref="Release"/> instance cont</returns>
        public static Release GetRelease(string applicationCode, int companyId)
        {
            return GetRelease(applicationCode, companyId, true);
        }

        /// <summary>
        /// Gets the latest release for the specified application code and company id
        /// </summary>
        /// <param name="applicationCode">The code of the application.</param>
        /// <param name="companyId">The id of the company.</param>
        /// <param name="checkAmountOfDownloads">Flag which indicates whether the amount of current downloads needs to be checked.</param>
        /// <returns>A <see cref="Release"/> instance cont</returns>
        public static Release GetRelease(string applicationCode, int companyId, bool checkAmountOfDownloads)
        {
            Release release = null;

            if (companyId == 0)
                throw new ObymobiException(Obymobi.Logic.HelperClasses.ReleaseHelper.ReleaseHelperErrors.NoCompanyIdSpecified);

            if (checkAmountOfDownloads)
            {
                CompanyEntity company = new CompanyEntity(companyId);

                // Check whether the download limit is reached
                if (Obymobi.Logic.HelperClasses.ReleaseHelper.GetAmountOfDownloadingDevices(companyId) >= company.MaxDownloadConnections)
                    throw new ObymobiException(Obymobi.Logic.HelperClasses.ReleaseHelper.ReleaseHelperErrors.MaximumDownloadConnectionsReached);
            }

            // Get the company release
            ReleaseEntity companyRelease = Obymobi.Logic.HelperClasses.ReleaseHelper.GetCompanyRelease(applicationCode, companyId);

            // Get the stable release
            ReleaseEntity stableRelease = Obymobi.Logic.HelperClasses.ReleaseHelper.GetStableReleaseByCode(applicationCode);

            if (stableRelease == null)
            {
                // No stable release set for the specified application
                throw new ObymobiException(Obymobi.Logic.HelperClasses.ReleaseHelper.ReleaseHelperErrors.NoStableReleaseSetForApplication, "ApplicationCode: {0}", applicationCode);
            }
            else
            {
                // Create the object for the release to download
                string remarks = string.Empty;

                if (companyRelease != null)
                {
                    int companyVersion;
                    if (int.TryParse(companyRelease.Version, out companyVersion))
                    {
                        release = new Release();
                        release.ReleaseId = companyRelease.ReleaseId;
                        release.Version = companyRelease.Version;
                        release.Filename = companyRelease.Filename;
                        release.Crc32 = companyRelease.Crc32;
                    }
                    else
                    {
                        // The version of the company release could not be parsed to an int
                        // so just take the stable release
                        remarks = string.Format("Company release was set but the version '{0}' could not be parsed to an int. Returning stable release.", companyRelease.Version);
                    }
                }
                else
                {
                    // No company release was set,
                    // so just take the stable release
                    remarks = "No company release was set. Returning stable release.";
                }

                if (release == null || !remarks.IsNullOrWhiteSpace()) // Stable release
                {
                    release = new Release();
                    release.ReleaseId = stableRelease.ReleaseId;
                    release.Version = stableRelease.Version;
                    release.Filename = stableRelease.Filename;
                    release.Crc32 = stableRelease.Crc32;
                    release.Remarks = remarks;
                }
            }

            return release;
        }

        public static Release GetRelease(int releaseId, int companyId, bool checkAmountOfDownloads = true)
        {
            if (checkAmountOfDownloads)
            {
                CompanyEntity company = new CompanyEntity(companyId);

                // Check whether the download limit is reached
                if (Obymobi.Logic.HelperClasses.ReleaseHelper.GetAmountOfDownloadingDevices(companyId) >= company.MaxDownloadConnections)
                    throw new ObymobiException(Obymobi.Logic.HelperClasses.ReleaseHelper.ReleaseHelperErrors.MaximumDownloadConnectionsReached);
            }

            Release release = null;

            ReleaseEntity entity = new ReleaseEntity();
            entity.FetchUsingPK(releaseId, null, null, new ExcludeFieldsList { ReleaseFields.File });
            if (!entity.IsNew)
            {
                release = new Release();
                release.ReleaseId = entity.ReleaseId;
                release.Version = entity.Version;
                release.Filename = entity.Filename;
                release.Crc32 = entity.Crc32;
                release.Remarks = (entity.Intermediate) ? "Intermediate Update" : "Full";
            }

            if (release == null)
                throw new ObymobiException(Obymobi.Logic.HelperClasses.ReleaseHelper.ReleaseHelperErrors.ReleaseDoesNotExistsOnDisk);

            return release;
        }

        /// <summary>
        /// Gets a list of CraveOS updates since currentVersion parameter for a particular device
        /// </summary>
        /// <param name="deviceModel"></param>
        /// <param name="currentVersion"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public static ReleaseCollection GetCraveOSReleasesForDevice(DeviceModel deviceModel, string currentVersion, int? companyId = null)
        {
            var releaseCollection = new ReleaseCollection();
            string applicationCode = Obymobi.Logic.HelperClasses.ReleaseHelper.GetApplicationCodeForDeviceModel(deviceModel);

            if (!applicationCode.IsNullOrWhiteSpace())
            {
                var filter = new PredicateExpression();
                filter.Add(ApplicationFields.Code == applicationCode);
                filter.Add(ReleaseFields.Version > currentVersion);
                filter.Add(ReleaseFields.Intermediate == false);
                filter.Add(ReleaseFields.Filename != DBNull.Value);

                if (companyId.HasValue)
                {
                    string companyVersion = Obymobi.Logic.HelperClasses.ReleaseHelper.GetVersionForCompany(applicationCode, companyId.Value);
                    if (companyVersion.Equals("0"))
                    {
                        throw new ObymobiException(Obymobi.Logic.HelperClasses.ReleaseHelper.ReleaseHelperErrors.NoStableReleaseSetForApplication, "ApplicationCode: {0}", applicationCode);
                    }

                    filter.Add(ReleaseFields.Version <= companyVersion);
                }

                ExcludeFieldsList excludedFields = new ExcludeFieldsList(ReleaseFields.File);
                RelationCollection relations = new RelationCollection(ReleaseEntityBase.Relations.ApplicationEntityUsingApplicationId);
                SortExpression sort = new SortExpression(ReleaseFields.Version | SortOperator.Descending);

                releaseCollection.GetMulti(filter, 0, sort, relations, null, excludedFields, 0, 0);
            }

            return releaseCollection;
        }
    }
}
