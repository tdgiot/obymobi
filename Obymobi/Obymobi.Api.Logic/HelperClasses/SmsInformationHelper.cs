﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public static class SmsInformationHelper
    {
        public enum SmsInformationError
        {
            NoDefaultSmsInformationConfigured
        }

        public static SmsKeywordEntity GetKeywordEntityForKeyword(string keyword, bool isRetry = false)
        {
            // Trim any whitespace from start and end
            keyword = keyword.Trim();

            SmsKeywordEntity returnEntity;
            SmsKeywordCollection smsKeywordCollection = null;
            if (!keyword.IsNullOrWhiteSpace())
            {
                // Get keyword entity from database
                var filter = new PredicateExpression(SmsKeywordFields.Keyword == keyword);
                smsKeywordCollection = new SmsKeywordCollection();
                smsKeywordCollection.GetMulti(filter);
            }

            if (smsKeywordCollection == null || smsKeywordCollection.Count == 0)
            {
                if (!isRetry && !keyword.IsNullOrWhiteSpace() && keyword.Contains(" "))
                {
                    // No keyword found with original keyword input. Keyword contains a space
                    // so we split the keyword and try again with the first part of the input.
                    var keywordSplit = keyword.Split(' ')[0];
                    returnEntity = GetKeywordEntityForKeyword(keywordSplit, true);
                }
                else
                {
                    // No keyword found, create dummy SmsKeywordEntity and add default sms information
                    var tmp = new SmsKeywordEntity {SmsInformationEntity = GetDefaultInformationEntity()};
                    returnEntity = tmp;
                }
            }
            else
            {
                returnEntity = smsKeywordCollection[0];
            }

            return returnEntity;
        }

        public static SmsInformationEntity GetDefaultInformationEntity()
        {
            var filter = new PredicateExpression(SmsInformationFields.IsDefault == true);
            
            var smsInformationCollection = new SmsInformationCollection();
            smsInformationCollection.GetMulti(filter);

            if (smsInformationCollection.Count == 0)
            {
                throw new ObymobiException(SmsInformationError.NoDefaultSmsInformationConfigured, "No default Sms Information setup for this environment. FIX IT!");
            }

            return smsInformationCollection[0];
        }
    }
}
