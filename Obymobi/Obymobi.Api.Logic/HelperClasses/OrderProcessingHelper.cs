﻿using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class OrderProcessingHelper
    {
        /// <summary>
        /// Creates an order for the EmailDocument routestephandler. This order will be processed by the OrderProcessingHelper and will email a pdf file.
        /// </summary>
        /// <returns></returns>
        public static Order CreateEmailDocumentOrder(ClientEntity client, string documentTitle, byte[] file)
        {
            if (!client.DeliverypointId.HasValue)
                return null;

            Order order = null;

            var product = OrderProcessingHelper.CreateEmailDocumentProduct(client.CompanyId, documentTitle);
            if (product != null)
            {
                order = new Order();
                order.CompanyId = client.CompanyId;
                order.DeliverypointId = client.DeliverypointId.Value;
                order.AgeVerificationType = 100;
                order.Type = (int)OrderType.Document;
                order.File = file;

                var orderitem = new Orderitem();
                orderitem.ProductId = product.ProductId;
                orderitem.ProductName = product.Name;
                orderitem.Quantity = 1;
                orderitem.ProductPriceIn = 0;
                orderitem.VatPercentage = 0;

                var orderitems = new Orderitem[1];
                orderitems[0] = orderitem;

                order.Orderitems = orderitems;
            }
            return order;
        }

        /// <summary>
        /// Creates a product for a Document type order. This product will contain the title of the document which is emailed.
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="documentTitle"></param>
        /// <returns></returns>
        private static ProductEntity CreateEmailDocumentProduct(int companyId, string documentTitle)
        {
            var product = new ProductEntity();
            product.CompanyId = companyId;
            product.Name = documentTitle;
            product.Type = (int)ProductType.Document;
            product.SubType = (int)ProductSubType.Inherit;
            product.VattariffId = 1;
            product.Save();

            return product;
        }
    }
}
