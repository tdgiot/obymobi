﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class CommonDataHelper
    {
        public static long GetLastModifiedTicks()
        {
            long toReturn = 0;

            List<DateTime> lastModifiedDateTimes = new List<DateTime>();

            // VenueCategories
            VenueCategoryCollection venueCategoryCollection = new VenueCategoryCollection();
            object lastModifiedVenueCategory = venueCategoryCollection.GetScalar(VenueCategoryFieldIndex.LastModifiedUTC, AggregateFunction.Max);
            if (lastModifiedVenueCategory != DBNull.Value && lastModifiedVenueCategory != null)
                lastModifiedDateTimes.Add((DateTime)lastModifiedVenueCategory);

            // Amenities
            AmenityCollection amenityCollection = new AmenityCollection();
            object lastModifiedAmenity = amenityCollection.GetScalar(AmenityFieldIndex.LastModifiedUTC, AggregateFunction.Max);
            if (lastModifiedAmenity != DBNull.Value && lastModifiedAmenity != null)
                lastModifiedDateTimes.Add((DateTime)lastModifiedAmenity);

            // ActionButtons
            ActionButtonCollection actionButtonCollection = new ActionButtonCollection();
            object lastModifiedActionButton = actionButtonCollection.GetScalar(ActionButtonFieldIndex.LastModifiedUTC, AggregateFunction.Max);
            if (lastModifiedActionButton != DBNull.Value && lastModifiedActionButton != null)
                lastModifiedDateTimes.Add((DateTime)lastModifiedActionButton);

            if (lastModifiedDateTimes.Count > 0)
                toReturn = lastModifiedDateTimes.Max().Ticks;

            return toReturn;
        }

        public static VenueCategoryCollection GetVenueCategories()
        {
            PrefetchPath venueCategoryPrefetch = new PrefetchPath(EntityType.VenueCategoryEntity);
            venueCategoryPrefetch.Add(VenueCategoryEntityBase.PrefetchPathCustomTextCollection);

            VenueCategoryCollection venueCategoryCollection = new VenueCategoryCollection();
            venueCategoryCollection.GetMulti(null, venueCategoryPrefetch);

            return venueCategoryCollection;
        }

        public static AmenityCollection GetAmenities()
        {
            PrefetchPath amenityPrefetch = new PrefetchPath(EntityType.AmenityEntity);
            amenityPrefetch.Add(AmenityEntityBase.PrefetchPathCustomTextCollection);

            AmenityCollection amenityCollection = new AmenityCollection();
            amenityCollection.GetMulti(null, amenityPrefetch);

            return amenityCollection;
        }

        public static ActionButtonCollection GetActionButtons()
        {
            PrefetchPath actionButtonPrefetch = new PrefetchPath(EntityType.ActionButtonEntity);
            actionButtonPrefetch.Add(ActionButtonEntityBase.PrefetchPathCustomTextCollection);

            ActionButtonCollection actionButtonCollection = new ActionButtonCollection();
            actionButtonCollection.GetMulti(null, actionButtonPrefetch);

            return actionButtonCollection;
        }
    }
}
