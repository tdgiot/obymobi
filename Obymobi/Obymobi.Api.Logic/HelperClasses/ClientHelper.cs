﻿using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class ClientHelper
    {
        public static Client GetSetClientAndDevice(int clientId, int deviceType, int? deviceModel, int companyId, int deliverypointgroupId, int deliverypointId, string macAddress, string applicationVersion, string osVersion)
        {
            Client client = null;

            if (!macAddress.IsNullOrWhiteSpace()) // Get the client using the macAddress
            {
                // Create the prefetch for the clients
                PrefetchPath prefetch = new PrefetchPath(EntityType.DeviceEntity);

                IPrefetchPathElement prefetchClient = prefetch.Add(DeviceEntityBase.PrefetchPathClientCollection);
                prefetchClient.SubPath.Add(ClientEntityBase.PrefetchPathCompanyEntity);
                IPrefetchPathElement prefetchDeliverypointgroup = prefetchClient.SubPath.Add(ClientEntityBase.PrefetchPathDeliverypointgroupEntity);
                prefetchDeliverypointgroup.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathCustomTextCollection);
                prefetchDeliverypointgroup.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathCompanyEntity).SubPath.Add(CompanyEntityBase.PrefetchPathCustomTextCollection);
                prefetchDeliverypointgroup.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathMediaCollection).SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);

                // Get the device for the macAddress
                DeviceEntity deviceEntity = Obymobi.Logic.HelperClasses.DeviceHelper.GetDeviceEntityByIdentifier(macAddress, true, prefetch, DeviceType.Unknown);

                bool foundClient = false;

                // Check if this device isn;'t connected to a Terminal, is so, unlink it.
                if (deviceEntity.TerminalEntitySingle != null)
                {
                    TerminalEntity terminal = deviceEntity.TerminalEntitySingle;

                    // Is now connected as a Terminal, remove that relationship & Record to TerminalLog                    
                    TerminalLogEntity log = Obymobi.Logic.HelperClasses.TerminalHelper.CreateLogForDeviceChange(terminal);
                    log.Log = "Removed because Device called ClientHelper.GetSetClientDevice and will be connected to a Client of Company: '{0}'".FormatSafe(companyId);
                    log.Save();

                    terminal.DeviceId = null;
                    terminal.ValidatorOverwriteDontLogDeviceChange = true;
                    terminal.Save();
                }

                // Check if the device is linked to a ClientEntity, and if so if it's linked to THIS clientId 
                ClientEntity clientEntity = null;
                if (deviceEntity.ClientEntitySingle != null)
                {
                    // Device is already connected to a client, check if it belongs to THIS client & company.
                    clientEntity = deviceEntity.ClientEntitySingle;
                    if (clientId > 0 && clientEntity.ClientId != clientId)
                    {
                        // Delete the client
                        clientEntity.ValidatorOverwriteDontLogDeviceChange = true;
                        clientEntity.Delete();
                    }
                    else if (clientEntity.CompanyId != companyId)
                    {
                        // Delete the client
                        clientEntity.ValidatorOverwriteDontLogDeviceChange = true;
                        clientEntity.Delete();
                    }
                    else
                    {
                        // It's a good Client!
                        foundClient = true;
                    }
                }

                if (!foundClient)
                {
                    // No client was found for this company
                    // Create a new client
                    clientEntity = Obymobi.Logic.HelperClasses.ClientHelper.CreateClientEntity(string.Empty, companyId, deliverypointgroupId, string.Empty, string.Empty, string.Empty);

                    // Connect the device to the client
                    clientEntity.DeviceId = deviceEntity.DeviceId;

                    // Set the deliverypointId on the client because if deliverypointgroupId and the deliverypointId mismatch in the clientValidator,
                    // an exception will be thrown and the client won't be created. Otherwise we end up with a lot of floating clients.
                    if (deliverypointId > 0)
                    {
                        clientEntity.DeliverypointId = deliverypointId;
                    }

                    ClientLogEntity log = Obymobi.Logic.HelperClasses.ClientHelper.CreateLogForDeviceChange(clientEntity);
                    log.Message += "\r\nDevice linked to Client by ClientHelper.GetSetClientAndDevice";
                    clientEntity.Save();
                    if (!log.ClientId.HasValue || log.ClientId == 0)
                        log.ClientId = clientEntity.ClientId;
                    log.Save();

                    clientEntity.ValidatorOverwriteDontLogDeviceChange = true;
                }
                else if (clientEntity.LastApiVersion != 1)
                {
                    // Client switched from other API
                    ClientLogEntity log = new ClientLogEntity();
                    log.TypeEnum = ClientLogType.ServiceVersionSwitched;
                    log.ClientId = clientEntity.ClientId;
                    log.Message = string.Format("Switched to API version 1 (from {0})", clientEntity.LastApiVersion);
                    log.Save();

                    clientEntity.LastApiVersion = 1;
                }

                // Update versions
                if (!applicationVersion.IsNullOrWhiteSpace())
                    deviceEntity.ApplicationVersion = applicationVersion;

                if (!osVersion.IsNullOrWhiteSpace())
                    deviceEntity.OsVersion = osVersion;

                // GK To make it possible to change the deliverpoint group of a Client using the CMS 
                // the code below is changed. Whenever it calls and DOESN'T have a DeliverypointId on the device, but does in the CMS, we change the Deliverypointgroup
                // to the one that belongs to that deliverypoint.

                if (deliverypointId <= 0 && clientEntity.DeliverypointId.HasValue)
                {
                    // Tablet didn't have DeliverypointId, one is configured in the CMS, don't update anything keep it this way.                     
                }
                else
                {
                    // Check if an other deliverypointgroup is chosen for this client
                    if (deliverypointgroupId > 0 &&
                        (!clientEntity.DeliverypointGroupId.HasValue ||
                        clientEntity.DeliverypointGroupId.Value != deliverypointgroupId))
                    {
                        clientEntity.DeliverypointGroupId = deliverypointgroupId;
                    }

                    // Check if an other deliverypoint is chosen for this client
                    if (deliverypointId > 0 &&
                        (!clientEntity.DeliverypointId.HasValue ||
                        clientEntity.DeliverypointId.Value != deliverypointId))
                    {
                        clientEntity.DeliverypointId = deliverypointId;
                    }
                }

                if (clientEntity.IsDirty)
                {
                    deviceEntity.LastRequestUTC = DateTime.UtcNow;
                }

                // Save is ok, won't save when not dirty
                clientEntity.Save();

                // Save device specifications
                if (deviceEntity.Type != (DeviceType)deviceType)
                {
                    deviceEntity.Type = (DeviceType)deviceType;
                }
                if (deviceModel.HasValue && (!deviceEntity.DeviceModel.HasValue || deviceEntity.DeviceModel != (DeviceModel)deviceModel))
                {
                    deviceEntity.DeviceModel = (DeviceModel)deviceModel;
                }

                // Won't save if not a dirty boy
                deviceEntity.Save();

                client = new ClientEntityConverter().ConvertEntityToModel(clientEntity);
            }
            else if (clientId > 0) // Fallback for backwards compatibility: Get the client using the client id
            {
                // Get the client entity
                ClientEntity clientEntity = Obymobi.Logic.HelperClasses.ClientHelper.GetClientEntityByClientId(clientId);
                if (clientEntity != null)
                {
                    // Create the client model
                    client = new ClientEntityConverter().ConvertEntityToModel(clientEntity);
                }
            }
            else
                throw new ObymobiException(GetClientResult.NoClientIdOrIdentifierSpecified, "ClientId '{0}' macAddress '{1}'", clientId, macAddress);

            return client;
        }

        public static Client[] GetClientsForCompany(int companyId, int? terminalId)
        {
            return new ClientEntityConverter().ConvertEntityCollectionToModelArray(ClientHelper.GetClientEntitiesForCompany(companyId, terminalId, true));
        }

        public static ClientCollection GetClientEntitiesForCompany(int companyId, int? terminalId, bool usePrefetch)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ClientFields.CompanyId == companyId);

            RelationCollection joins = new RelationCollection();

            PrefetchPath prefetch = new PrefetchPath(EntityType.ClientEntity);
            if (usePrefetch)
            {
                prefetch.Add(ClientEntityBase.PrefetchPathCompanyEntity);
                prefetch.Add(ClientEntityBase.PrefetchPathDeviceEntity);
                prefetch.Add(ClientEntityBase.PrefetchPathDeliverypointEntity);

                IPrefetchPathElement prefetchDeliverypointgroup = prefetch.Add(ClientEntityBase.PrefetchPathDeliverypointgroupEntity);
                prefetchDeliverypointgroup.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathCustomTextCollection);
                prefetchDeliverypointgroup.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathCompanyEntity).SubPath.Add(CompanyEntityBase.PrefetchPathCustomTextCollection);
                IPrefetchPathElement prefetchDeliverypointgroupMedia = prefetchDeliverypointgroup.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathMediaCollection);
                prefetchDeliverypointgroupMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
                prefetchDeliverypointgroupMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);
            }

            ClientCollection clientCollection = new ClientCollection();
            clientCollection.GetMulti(filter, 0, null, joins, prefetch);

            return clientCollection;
        }
    }
}
