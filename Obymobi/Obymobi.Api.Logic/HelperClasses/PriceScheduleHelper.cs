﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class PriceScheduleHelper
    {
        public static PriceScheduleEntity GetPriceScheduleEntity(int priceScheduleId)
        {
            PredicateExpression filter = new PredicateExpression(PriceScheduleFields.PriceScheduleId == priceScheduleId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.PriceScheduleEntity);
            IPrefetchPathElement prefetchPriceScheduleItems = prefetch.Add(PriceScheduleEntityBase.PrefetchPathPriceScheduleItemCollection);
            IPrefetchPathElement prefetchPriceScheduleItemsOccurrences = prefetchPriceScheduleItems.SubPath.Add(PriceScheduleItemEntityBase.PrefetchPathPriceScheduleItemOccurrenceCollection);
            IPrefetchPathElement prefetchPriceScheduleItemsPriceLevels = prefetchPriceScheduleItems.SubPath.Add(PriceScheduleItemEntityBase.PrefetchPathPriceLevelEntity);
            IPrefetchPathElement prefetchPriceScheduleItemsPriceLevelsItems = prefetchPriceScheduleItemsPriceLevels.SubPath.Add(PriceLevelEntityBase.PrefetchPathPriceLevelItemCollection);

            PriceScheduleCollection priceSchedules = new PriceScheduleCollection();
            priceSchedules.GetMulti(filter, prefetch);

            if (priceSchedules.Count > 0)
            {
                return priceSchedules[0];
            }
            else
            {
                return null;
            }
        }
    }
}
