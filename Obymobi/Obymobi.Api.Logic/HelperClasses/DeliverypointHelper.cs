﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Api.Logic.EntityConverters;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class DeliverypointHelper
    {
        public static Deliverypoint[] GetDeliverypoints(int companyId, int? terminalId, bool addPosData, int deliverypointgroupId)
        {
            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointFields.CompanyId == companyId);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(DeliverypointFields.Number, SortOperator.Ascending));

            PrefetchPath prefetch = new PrefetchPath(EntityType.DeliverypointEntity);
            prefetch.Add(DeliverypointEntityBase.PrefetchPathDeviceEntity);

            if (addPosData)
                prefetch.Add(DeliverypointEntityBase.PrefetchPathPosdeliverypointEntity);

            RelationCollection relations = new RelationCollection();

            if (deliverypointgroupId > 0)
            {
                // Add DPG relation for filter
                relations.Add(DeliverypointgroupEntityBase.Relations.DeliverypointEntityUsingDeliverypointgroupId);

                filter.Add(DeliverypointgroupFields.DeliverypointgroupId == deliverypointgroupId);
            }
            else
            {
                relations.Add(DeliverypointEntityBase.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);
                filter.Add(DeliverypointgroupFields.Active == true);
            }

            // Create and initialize a DeliverypointCollection instance
            // and retrieve the items using the filter
            DeliverypointCollection deliverypointCollection = new DeliverypointCollection();
            deliverypointCollection.GetMulti(filter, 0, sort, relations, prefetch);

            // Sort the string-type "number" field as integer-type | Might not be an integer :S
            int dummy;
            var deliverypointsWithIntegerNumber = deliverypointCollection.Where(d => int.TryParse(d.Number, out dummy));
            var deliverypointsWithNonIntegerNumber = deliverypointCollection.Where(d => !int.TryParse(d.Number, out dummy));

            var sortedDeliverypointCollection = deliverypointsWithIntegerNumber.OrderBy(d => Convert.ToInt32(d.Number));
            var completeList = sortedDeliverypointCollection.ToList();

            completeList.AddRange(deliverypointsWithNonIntegerNumber);

            DeliverypointEntityConverter deliverypointEntityConverter = new DeliverypointEntityConverter(addPosData);

            // Initialize the array
            List<Deliverypoint> deliverypointModels = new List<Deliverypoint>();
            foreach (DeliverypointEntity deliverypointEntity in completeList)
            {
                deliverypointModels.Add(deliverypointEntityConverter.ConvertEntityToModel(deliverypointEntity));
            }

            return deliverypointModels.ToArray();
        }
    }
}
