﻿using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class FolioItemHelper
    {
        public static void UpdateFolioItemEntityFromModel(FolioItemEntity entity, FolioItem model)
        {
            entity.Code = model.Code;
            entity.Description = model.Description;
            entity.PriceIn = model.PriceIn;
            entity.CurrencyCode = model.CurrencyCode;
            entity.ReferenceId = model.ReferenceId;
        }
    }
}
