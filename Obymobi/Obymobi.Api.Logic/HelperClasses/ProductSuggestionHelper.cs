﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data.HelperClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Data;
using Obymobi.Enums;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class ProductSuggestionHelper
    {
        public static ProductSuggestion CreateProductSuggestionModelFromEntity(ProductSuggestionEntity productSuggestionEntity, int? menuId)
        {
            ProductSuggestion productSuggestion = null;

            if (productSuggestionEntity.SuggestedProductId.HasValue)
            {
                CategoryCollection categoryCollection = productSuggestionEntity.SuggestedProductEntity.CategoryCollectionViaProductCategory;

                foreach (CategoryEntity categoryEntity in productSuggestionEntity.SuggestedProductEntity.CategoryCollectionViaProductCategory)
                {
                    if (!menuId.HasValue || (categoryEntity.MenuId.HasValue && categoryEntity.MenuId.Value == menuId.Value))
                    {
                        productSuggestion = new ProductSuggestion();
                        productSuggestion.ProductSuggestionId = productSuggestionEntity.ProductSuggestionId;
                        productSuggestion.ProductId = productSuggestionEntity.ProductId;
                        productSuggestion.SuggestedProductId = productSuggestionEntity.SuggestedProductId.Value;
                        productSuggestion.Direct = productSuggestionEntity.Direct;
                        productSuggestion.Checkout = productSuggestionEntity.Checkout;
                        productSuggestion.CategoryId = categoryEntity.CategoryId;
                        break;
                    }
                }
            }

            return productSuggestion;
        }

		public static ProductSuggestion CreateProductSuggestionModelFromEntity(ProductCategoryEntity pc,  CategorySuggestionEntity categorySuggestionEntity)
        {
            int suggestedProductId = categorySuggestionEntity.ProductCategoryId.HasValue ? categorySuggestionEntity.ProductCategoryEntity.ProductId : -1;
            if (suggestedProductId < 0)
            {
                // For backwards compatibility
                suggestedProductId = categorySuggestionEntity.ProductId.HasValue ? categorySuggestionEntity.ProductId.Value : -1;
            }

            ProductSuggestion productSuggestion = new ProductSuggestion();
            productSuggestion.ProductSuggestionId = categorySuggestionEntity.CategorySuggestionId;
            productSuggestion.ProductId = pc.ProductId;
            productSuggestion.SuggestedProductId = suggestedProductId;
            productSuggestion.Direct = false;
            productSuggestion.Checkout = categorySuggestionEntity.Checkout;
            productSuggestion.CategoryId = categorySuggestionEntity.ProductCategoryId.HasValue ? categorySuggestionEntity.ProductCategoryEntity.CategoryId : pc.CategoryId;

            return productSuggestion;
		}

        public static ProductSuggestion CreateProductSuggestionModelFromEntity(ProductCategorySuggestionEntity productCategorySuggestionEntity)
        {
            ProductSuggestion productSuggestion = new ProductSuggestion();
            productSuggestion.ProductSuggestionId = productCategorySuggestionEntity.ProductCategorySuggestionId;
            productSuggestion.ProductId = productCategorySuggestionEntity.ProductCategoryEntity.ProductId;
            productSuggestion.SuggestedProductId = productCategorySuggestionEntity.SuggestedProductCategoryEntity.ProductId;
            productSuggestion.CategoryId = productCategorySuggestionEntity.SuggestedProductCategoryEntity.CategoryId;
            productSuggestion.Direct = false;
            productSuggestion.Checkout = productCategorySuggestionEntity.Checkout;

            return productSuggestion;
        }
    }
}
