﻿using System.Collections.Generic;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data.CollectionClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class AttachmentHelper
    {
        private static readonly AttachmentEntityConverter AttachmentEntityConverter = new AttachmentEntityConverter();

        public static List<Attachment> CreateAttachmentModelsForProduct(ProductEntity product)
        {
            List<Attachment> attachments = new List<Attachment>();

            // First add generic product alterations (if any)
            if (product.IsLinkedToGeneric && product.GenericproductEntity != null)
            {
                attachments.AddRange(AttachmentHelper.CreateAttachmentModels(product.GenericproductEntity.AttachmentCollection));
            }

            // Add product attachments
            attachments.AddRange(AttachmentHelper.CreateAttachmentModels(product.AttachmentCollection));

            return attachments;
        }

        public static List<Attachment> CreateAttachmentModels(AttachmentCollection attachmentCollection)
        {
            List<Attachment> attachments = new List<Attachment>();

            foreach (AttachmentEntity attachmentEntity in attachmentCollection)
            {
                if (attachmentEntity.HasDocuments() || !attachmentEntity.Url.IsNullOrWhiteSpace())
                {
                    attachments.Add(AttachmentHelper.AttachmentEntityConverter.ConvertEntityToModel(attachmentEntity));
                }
            }

            return attachments;
        }
    }
}