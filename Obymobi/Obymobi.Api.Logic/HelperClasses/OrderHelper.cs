﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class OrderHelper
    {
        public static OrderEntity GetOrderByGuid(string guid, bool prefetchOrderItems)
        {
            var filter = new PredicateExpression();
            filter.Add(OrderFields.Guid == guid);

            var filterOrderTypes = new PredicateExpression();
            filterOrderTypes.Add(OrderFields.Type == (int)OrderType.Standard);
            filterOrderTypes.AddWithOr(OrderFields.Type == (int)OrderType.RequestForService);
            filterOrderTypes.AddWithOr(OrderFields.Type == (int)OrderType.RequestForBatteryCharge);
            filterOrderTypes.AddWithOr(OrderFields.Type == (int)OrderType.RequestForPrint);
            filterOrderTypes.AddWithOr(OrderFields.Type == (int)OrderType.RequestForWakeUp);
            filter.Add(filterOrderTypes);

            // Prefetching
            var path = new PrefetchPath(EntityType.OrderEntity);
            if (prefetchOrderItems)
                path.Add(OrderEntity.PrefetchPathOrderitemCollection).SubPath.Add(OrderitemEntity.PrefetchPathProductEntity);

            var orders = new OrderCollection();
            orders.GetMulti(filter, 1, null, null, path);

            OrderEntity ret = null;
            if (orders.Count == 1)
                ret = orders[0];

            return ret;
        }

        public static Order[] GetOrderHistory(int terminalId)
        {
            // Filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(OrderRoutestephandlerHistoryFields.TerminalId == terminalId);

            // Relations
            RelationCollection relations = new RelationCollection();
            relations.Add(OrderEntity.Relations.OrderRoutestephandlerHistoryEntityUsingOrderId);

            // Sort
            SortExpression sort = new SortExpression();
            sort.Add(OrderFields.ProcessedUTC | SortOperator.Descending);

            // Prefetch
            PrefetchPath prefetch = new PrefetchPath(EntityType.OrderEntity);
            prefetch.Add(OrderEntity.PrefetchPathCompanyEntity);
            prefetch.Add(OrderEntity.PrefetchPathCustomerEntity);
            prefetch.Add(OrderEntity.PrefetchPathDeliverypointEntity);
            prefetch.Add(OrderEntity.PrefetchPathOrderRoutestephandlerCollection);
            IPrefetchPathElement orderitemPath = prefetch.Add(OrderEntity.PrefetchPathOrderitemCollection);
            orderitemPath.SubPath.Add(OrderitemEntity.PrefetchPathOrderitemAlterationitemCollection).SubPath.Add(OrderitemAlterationitemEntity.PrefetchPathAlterationitemEntity).SubPath.Add(AlterationitemEntity.PrefetchPathAlterationEntity).SubPath.Add(AlterationEntity.PrefetchPathPosAlterationEntity);

            // Get the orders 
            OrderCollection orderCollection = new OrderCollection();
            orderCollection.GetMulti(filter, 25, sort, relations, prefetch);

            // Convert the entities to models
            List<Order> orders = new List<Order>();
            foreach (OrderEntity order in orderCollection)
            {
                orders.Add(Obymobi.Logic.HelperClasses.OrderHelper.CreateOrderModelFromEntity(order, false, false));
            }

            // Return an array of order models
            return orders.ToArray();
        }

        public static int[] GetLastProcessedOrderIds(int? customerId, int? clientId, int maxMinutesSinceLastOrder)
        {
            int? companyId = null;
            if (customerId.HasValue)
            {
                var customer = new CustomerEntity(customerId.Value);
            }

            IncludeFieldsList fields = new IncludeFieldsList();
            fields.Add(OrderFields.OrderId);

            OrderCollection orders = GetLastOrders(companyId, customerId, clientId, (int)OrderStatus.Processed, maxMinutesSinceLastOrder, false, false, fields);
            var query = from o in orders select o.OrderId;
            return query.ToArray();
        }

        public static OrderCollection GetLastOrders(int? companyId, int? customerId, int? clientId, int? status, int maxMinutesSinceLastOrder, bool prefetchOrderItems, bool alwaysGetLastOrders, ExcludeIncludeFieldsList fields)
        {
            PredicateExpression filter = new PredicateExpression();
            if (maxMinutesSinceLastOrder != -1)
            {
                filter.Add(OrderFields.CreatedUTC >= DateTime.UtcNow.AddMinutes(maxMinutesSinceLastOrder * -1));
            }

            TerminalType handlingMethod = TerminalType.OnSiteServer;
            if (companyId.HasValue)
            {
                Terminal terminal = TerminalHelper.GetTerminalArrayByCompanyId(companyId.Value)[0];
                handlingMethod = terminal.HandlingMethod.ToEnum<TerminalType>();
            }
            else if (clientId.HasValue)
            {
                PredicateExpression terminalFilter = new PredicateExpression();
                terminalFilter.Add(ClientFields.ClientId == clientId.Value);
            }

            if (status.HasValue && handlingMethod == TerminalType.OnSiteServer)
                filter.Add(OrderFields.Status == status.Value);
            else if (handlingMethod != TerminalType.OnSiteServer)
            {
                // GK Before Order Status refactoring:
                // filter.Add(OrderFields.Status >= (int)OrderStatus.QueuedOnTerminal);
                // filter.AddWithAnd(OrderFields.Status < OrderStatus.Over9000);

                filter.Add(OrderFields.Status >= OrderStatus.InRoute);
                filter.Add(OrderFields.Status < OrderStatus.Unprocessable);
            }

            if (companyId.HasValue)
                filter.Add(OrderFields.CompanyId == companyId.Value);

            if (customerId.HasValue)
                filter.Add(OrderFields.CustomerId == customerId.Value);

            if (clientId.HasValue)
                filter.Add(OrderFields.ClientId == clientId.Value);

            PredicateExpression filterOrderTypes = new PredicateExpression();
            filterOrderTypes.Add(OrderFields.Type == (int)OrderType.Standard);
            filterOrderTypes.AddWithOr(OrderFields.Type == (int)OrderType.RequestForService);
            filterOrderTypes.AddWithOr(OrderFields.Type == (int)OrderType.RequestForCheckout);
            filter.Add(filterOrderTypes);

            SortExpression sort = new SortExpression();
            sort.Add(OrderFields.OrderId | SortOperator.Descending);

            // Prefetching
            PrefetchPath path = new PrefetchPath(EntityType.OrderEntity);
            if (prefetchOrderItems)
                path.Add(OrderEntity.PrefetchPathOrderitemCollection).SubPath.Add(OrderitemEntity.PrefetchPathProductEntity);

            OrderCollection orders = new OrderCollection();
            orders.GetMulti(filter, 0, sort, null, path, fields, 0, 0);

            if (orders.Count < 10 && alwaysGetLastOrders)
                orders.GetMulti(filter, 10, sort, null, path, fields, 0, 0);

            return orders;
        }
    }
}
