﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class PointOfInterestHelper
    {
        public static List<int> GetPointOfInterestIdListForObymobi()
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PointOfInterestFields.Visible == true);

            IncludeFieldsList fields = new IncludeFieldsList(PointOfInterestFields.PointOfInterestId);

            PointOfInterestCollection pois = new PointOfInterestCollection();
            pois.GetMulti(filter, 0, null, null, null, fields, 0, 0);            

            return pois.Select(x => x.PointOfInterestId).ToList();
        }

        public static List<int> GetPointOfInterestIdListUsingAccessCodes(List<int> accessCodeIds)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(AccessCodeFields.AccessCodeId == accessCodeIds);

            RelationCollection relations = new RelationCollection();
            relations.Add(PointOfInterestEntity.Relations.AccessCodePointOfInterestEntityUsingPointOfInterestId);
            relations.Add(AccessCodePointOfInterestEntity.Relations.AccessCodeEntityUsingAccessCodeId);

            IncludeFieldsList fields = new IncludeFieldsList(PointOfInterestFields.PointOfInterestId);

            PointOfInterestCollection pois = new PointOfInterestCollection();
            pois.GetMulti(filter, 0, null, relations, null, fields, 0, 0);

            return pois.Select(x => x.PointOfInterestId).ToList();
        }

        public static List<int> GetPointOfInterestIdList(List<int> accessCodeIds, AccessCodeType? accessCodeType)
        {
            List<int> poiIds = new List<int>();

            if (!accessCodeType.HasValue)
            {
                // Access code type could not be determined
                // because no access codes were supplied
                poiIds = GetPointOfInterestIdListForObymobi();
            }
            else if (accessCodeType.Value == AccessCodeType.ShowOnly)
            {
                // Access code is of type ShowOnly, which means we are only
                // going to retrieve the POIs specified by the access codes
                poiIds = GetPointOfInterestIdListUsingAccessCodes(accessCodeIds);
            }
            else // Access code is of type Show, combine the results
            {
                poiIds = GetPointOfInterestIdListForObymobi();
                poiIds = poiIds.Concat(GetPointOfInterestIdListUsingAccessCodes(accessCodeIds)).ToList();
            }

            return poiIds;
        }

        public static PointOfInterestCollection GetPointOfInterestCollection(List<int> poiIds)
        {
            PointOfInterestCollection pois = new PointOfInterestCollection();

            if (poiIds != null && poiIds.Count > 0)
            {
                // First, create and initialize a filter
                PredicateExpression filter = new PredicateExpression();
                filter.Add(PointOfInterestFields.PointOfInterestId == poiIds);

                var prefetch = PointOfInterestHelper.GetPrefetchPathForMobileWebservice();
                pois.GetMulti(filter, 0, null, null, prefetch);
            }

            return pois;
        }

        public static PrefetchPath GetPrefetchPathForMobileWebservice()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.PointOfInterestEntity);
                     
            // Media
            prefetch.Add(PointOfInterestEntity.PrefetchPathMediaCollection).SubPath.Add(MediaEntity.PrefetchPathMediaRatioTypeMediaCollection);

            // VenueCategories
            prefetch.Add(PointOfInterestEntity.PrefetchPathPointOfInterestVenueCategoryCollection).SubPath.Add(PointOfInterestVenueCategoryEntity.PrefetchPathVenueCategoryEntity).SubPath.Add(VenueCategoryEntity.PrefetchPathCustomTextCollection);

            // Amenities
            prefetch.Add(PointOfInterestEntity.PrefetchPathPointOfInterestAmenityCollection).SubPath.Add(PointOfInterestAmenityEntity.PrefetchPathAmenityEntity).SubPath.Add(AmenityEntity.PrefetchPathCustomTextCollection);

            // Business Hours
            prefetch.Add(PointOfInterestEntity.PrefetchPathBusinesshoursCollection);

            // Country
            prefetch.Add(PointOfInterestEntity.PrefetchPathCountryEntity);

            // Action Button
            prefetch.Add(PointOfInterestEntity.PrefetchPathActionButtonEntity).SubPath.Add(ActionButtonEntity.PrefetchPathCustomTextCollection);

            // UI Tab
            var uiModeSubPath = UIModeEntity.PrefetchPathUITabCollection;
            uiModeSubPath.SubPath.Add(UITabEntity.PrefetchPathCustomTextCollection);

            // UI modes
            prefetch.Add(PointOfInterestEntity.PrefetchPathUIModeCollection).SubPath.Add(uiModeSubPath);

            // Custom texts
            prefetch.Add(PointOfInterestEntity.PrefetchPathCustomTextCollection);

            return prefetch;
        }

        public static long GetLastModifiedTicks(List<int> poiIds)
        {            
            long toReturn = 0;

            PredicateExpression filter = new PredicateExpression();

            if (poiIds != null && poiIds.Count > 0)
                filter.Add(PointOfInterestFields.PointOfInterestId == poiIds);

            PointOfInterestCollection pois = new PointOfInterestCollection();
            object result = pois.GetScalar(PointOfInterestFieldIndex.LastModifiedUTC, null, AggregateFunction.Max, filter);
            if (result != DBNull.Value && result != null)
            {
                toReturn = ((DateTime)result).Ticks;
            }

            return toReturn;
        }
    }
}
