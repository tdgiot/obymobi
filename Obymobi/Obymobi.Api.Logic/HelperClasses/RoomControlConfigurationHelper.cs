﻿using System;
using System.Collections;
using System.Collections.Generic;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using Obymobi.Api.Logic.EntityConverters;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class RoomControlConfigurationHelper
    {
        public static RoomControlConfiguration[] GetRoomControlConfiguration(int roomControlConfigurationId)
        {
            PredicateExpression filter = new PredicateExpression(RoomControlConfigurationFields.RoomControlConfigurationId == roomControlConfigurationId);
            filter.Add(RoomControlAreaFields.Visible == true);
            filter.Add(RoomControlSectionFields.Visible == true);            

            RelationCollection relations = new RelationCollection(RoomControlConfigurationEntityBase.Relations.RoomControlAreaEntityUsingRoomControlConfigurationId);
            relations.Add(RoomControlAreaEntityBase.Relations.RoomControlSectionEntityUsingRoomControlAreaId);            

            PrefetchPath prefetch = new PrefetchPath(EntityType.RoomControlConfigurationEntity);
            IPrefetchPathElement prefetchRoomControlConfigurationArea = prefetch.Add(RoomControlConfigurationEntityBase.PrefetchPathRoomControlAreaCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaCustomTexts = prefetchRoomControlConfigurationArea.SubPath.Add(RoomControlAreaEntityBase.PrefetchPathCustomTextCollection);            

            IPrefetchPathElement prefetchRoomControlConfigurationAreaSection = prefetchRoomControlConfigurationArea.SubPath.Add(RoomControlAreaEntityBase.PrefetchPathRoomControlSectionCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionCustomTexts = prefetchRoomControlConfigurationAreaSection.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathCustomTextCollection);            
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionMedia = prefetchRoomControlConfigurationAreaSection.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionMediaMediaRatioType = prefetchRoomControlConfigurationAreaSectionMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionMediaCulture = prefetchRoomControlConfigurationAreaSectionMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);            

            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionWidget = prefetchRoomControlConfigurationAreaSection.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathRoomControlWidgetCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionWidgetCustomTexts = prefetchRoomControlConfigurationAreaSectionWidget.SubPath.Add(RoomControlWidgetEntityBase.PrefetchPathCustomTextCollection);            

            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionComponent = prefetchRoomControlConfigurationAreaSection.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathRoomControlComponentCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionComponentCustomTexts = prefetchRoomControlConfigurationAreaSectionComponent.SubPath.Add(RoomControlComponentEntityBase.PrefetchPathCustomTextCollection);            

            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItem = prefetchRoomControlConfigurationAreaSection.SubPath.Add(RoomControlSectionEntityBase.PrefetchPathRoomControlSectionItemCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItemCustomTexts = prefetchRoomControlConfigurationAreaSectionItem.SubPath.Add(RoomControlSectionItemEntityBase.PrefetchPathCustomTextCollection);            
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItemMedia = prefetchRoomControlConfigurationAreaSectionItem.SubPath.Add(RoomControlSectionItemEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItemMediaMediaRatioType = prefetchRoomControlConfigurationAreaSectionItemMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItemMediaCulture = prefetchRoomControlConfigurationAreaSectionItemMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);            

            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItemWidget = prefetchRoomControlConfigurationAreaSectionItem.SubPath.Add(RoomControlSectionItemEntityBase.PrefetchPathRoomControlWidgetCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItemWidgetCustomTexts = prefetchRoomControlConfigurationAreaSectionItemWidget.SubPath.Add(RoomControlWidgetEntityBase.PrefetchPathCustomTextCollection);            

            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItemStationList = prefetchRoomControlConfigurationAreaSectionItem.SubPath.Add(RoomControlSectionItemEntityBase.PrefetchPathStationListEntity);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItemStationListStation = prefetchRoomControlConfigurationAreaSectionItemStationList.SubPath.Add(StationListEntityBase.PrefetchPathStationCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItemStationListStationCustomTexts = prefetchRoomControlConfigurationAreaSectionItemStationListStation.SubPath.Add(StationEntityBase.PrefetchPathCustomTextCollection);            

            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItemStationListStationMedia = prefetchRoomControlConfigurationAreaSectionItemStationListStation.SubPath.Add(StationEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItemStationListStationMediaMediaRatioType = prefetchRoomControlConfigurationAreaSectionItemStationListStationMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            IPrefetchPathElement prefetchRoomControlConfigurationAreaSectionItemStationListStationMediaCulture = prefetchRoomControlConfigurationAreaSectionItemStationListStationMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);            

            var sort = new SortExpression(new SortClause(RoomControlAreaFields.SortOrder, SortOperator.Ascending));
            sort.Add(new SortClause(RoomControlSectionFields.SortOrder, SortOperator.Ascending));            

            var roomControlConfigurationCollection = new RoomControlConfigurationCollection();
            roomControlConfigurationCollection.GetMulti(filter, 0, sort, relations, prefetch);

            List<RoomControlConfiguration> configurations = new List<RoomControlConfiguration>();

            if (roomControlConfigurationCollection.Count > 0)
                configurations.Add(new RoomControlConfigurationEntityConverter().ConvertEntityToModel(roomControlConfigurationCollection[0]));

            return configurations.ToArray();
        }
    }
}