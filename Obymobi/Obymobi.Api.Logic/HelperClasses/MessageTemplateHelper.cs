﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class MessageTemplateHelper
    {
        public static MessageTemplate[] GetMessageTemplatesForCompany(int? companyId, MessageTemplateCollection messageTemplateCollection = null)
        {
            if (messageTemplateCollection == null)
            {
                PredicateExpression filter = new PredicateExpression();
                filter.Add(MessageTemplateFields.CompanyId == DBNull.Value);

                if (companyId.HasValue)
                {
                    filter.AddWithOr(MessageTemplateFields.CompanyId == companyId.Value);
                }

                messageTemplateCollection = new MessageTemplateCollection();
                messageTemplateCollection.GetMulti(filter);
            }

            List<MessageTemplate> messageTemplates = new List<MessageTemplate>();
            foreach (MessageTemplateEntity messageTemplateEntity in messageTemplateCollection)
            {
                MessageTemplate messageTemplate = Obymobi.Logic.HelperClasses.MessageTemplateHelper.CreateMessageTemplateModelFromEntity(messageTemplateEntity);
                messageTemplates.Add(messageTemplate);
            }

            return messageTemplates.ToArray();
        }
    }
}
