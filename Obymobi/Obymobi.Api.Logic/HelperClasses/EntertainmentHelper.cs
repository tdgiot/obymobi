﻿using System;
using System.Collections.Generic;
using System.Linq;
using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class EntertainmentHelper
    {
        public static EntertainmentCollection GetEntertainment(int companyId, EntertainmentType type)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(EntertainmentFields.EntertainmentType == type);

            PredicateExpression subFilter = new PredicateExpression();
            subFilter.Add(EntertainmentFields.CompanyId == companyId);
            subFilter.AddWithOr(EntertainmentFields.CompanyId == DBNull.Value);

            filter.Add(subFilter);

            SortExpression sort = new SortExpression();
            sort.Add(new SortClause(EntertainmentFields.Name, SortOperator.Ascending));

            EntertainmentCollection entertainment = new EntertainmentCollection();
            entertainment.GetMulti(filter, 0, sort);

            return entertainment;
        }

        public static Entertainment[] GetEntertainment(int companyId, int deliverypointgroupId, TerminalEntity terminalEntity, DeviceType deviceType = DeviceType.Unknown)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(EntertainmentFields.Visible == true);

            RelationCollection relations = new RelationCollection();

            // Building the prefetch tree:

            // Entertainment
            var prefetch = new PrefetchPath(EntityType.EntertainmentEntity);
            // Entertainment > Entertainmenturl
            IPrefetchPathElement prefetchEntertainmentEntertainmentUrl = prefetch.Add(EntertainmentEntityBase.PrefetchPathEntertainmenturlEntity);
            // Entertainment > EntertainmenturlCollection
            IPrefetchPathElement prefetchEntertainmentEntertainmentUrlCollection = prefetch.Add(EntertainmentEntityBase.PrefetchPathEntertainmenturlCollection);
            // Entertainment > DeliverypointgroupEntertainmentCollection
            IPrefetchPathElement prefetchEntertainmentDeliverypointgroupEntertainmentCollection = prefetch.Add(EntertainmentEntityBase.PrefetchPathDeliverypointgroupEntertainmentCollection);
            // Entertainment > EntertainmentFile
            IPrefetchPathElement prefetchEntertainmentEntertainmentFile = prefetch.Add(EntertainmentEntityBase.PrefetchPathEntertainmentFileEntity);
            // Entertainment > EntertainmentDependency 
            IPrefetchPathElement prefetchEntertainmentEntertainmentDependency = prefetch.Add(EntertainmentEntityBase.PrefetchPathEntertainmentDependencyCollection);
            // Entertainment > EntertainmentDependency > DependentEntertainment
            IPrefetchPathElement prefetchEntertainmentEntertainmentDependencyDependentEntertainment = prefetchEntertainmentEntertainmentDependency.SubPath.Add(EntertainmentDependencyEntityBase.PrefetchPathDependentEntertainmentEntity);
            // Entertainment > AdvertisementTagEntertainmentCollection
            IPrefetchPathElement prefetchEntertainmentAdvertisementTagEntertainmentCollection = prefetch.Add(EntertainmentEntityBase.PrefetchPathAdvertisementTagEntertainmentCollection);
            // Entertainment > Media
            IPrefetchPathElement prefetchEntertainmentMedia = prefetch.Add(EntertainmentEntityBase.PrefetchPathMediaCollection);
            // Entertainment > Media > MediaLanguage
            IPrefetchPathElement prefetchEntertainmentMediaMediaCulture = prefetchEntertainmentMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);
            // Entertainment > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchEntertainmentMediaMediaRatioTypeMedia = prefetchEntertainmentMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);

            if (deviceType != DeviceType.Unknown)
            {
                MediaType[] mediaTypes = MediaRatioTypes.GetMediaTypeArrayForDeviceTypes(deviceType);
                prefetchEntertainmentMediaMediaRatioTypeMedia.Filter = new PredicateExpression(MediaRatioTypeMediaFields.MediaType == mediaTypes);
            }

            SortExpression sort = new SortExpression();

            if (deliverypointgroupId <= 0 && terminalEntity != null && terminalEntity.TerminalType == (int)TerminalType.OnSiteServer)
            {
                if (terminalEntity.CompanyEntity.DeliverypointgroupCollection.Count > 0)
                    deliverypointgroupId = terminalEntity.CompanyEntity.DeliverypointgroupCollection[0].DeliverypointgroupId;
            }

            if (deliverypointgroupId > 0)
            {
                // Add Deliverypointgroup filter
                filter.Add(DeliverypointgroupEntertainmentFields.DeliverypointgroupId == deliverypointgroupId);
                relations.Add(EntertainmentEntityBase.Relations.DeliverypointgroupEntertainmentEntityUsingEntertainmentId);
                sort.Add(new SortClause(DeliverypointgroupEntertainmentFields.SortOrder, SortOperator.Ascending));
            }
            else if (terminalEntity != null && terminalEntity.TerminalId > 0)
            {
                // Add the terminal filter
                filter.Add(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, terminalEntity.TerminalId, "T1"));
                filter.AddWithOr(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, terminalEntity.TerminalId, "T2"));
                filter.AddWithOr(new FieldCompareValuePredicate(TerminalFields.TerminalId, ComparisonOperator.Equal, terminalEntity.TerminalId, "T3"));

                relations.Add(EntertainmentEntityBase.Relations.TerminalEntityUsingBrowser1, "T1", JoinHint.Left);
                relations.Add(EntertainmentEntityBase.Relations.TerminalEntityUsingBrowser2, "T2", JoinHint.Left);
                relations.Add(EntertainmentEntityBase.Relations.TerminalEntityUsingCmsPage, "T3", JoinHint.Left);
            }

            sort.Add(new SortClause(EntertainmentFields.Name, SortOperator.Ascending));

            // Create and initialize a EntertainmentCollection instance
            // and retrieve the items using the filter
            EntertainmentCollection entertainmentCollection = new EntertainmentCollection();
            entertainmentCollection.GetMulti(filter, 0, sort, relations, prefetch);

            EntertainmentEntityConverter entertainmentEntityConverter = new EntertainmentEntityConverter(deliverypointgroupId);
            List<Entertainment> entertainment = entertainmentEntityConverter.ConvertEntityCollectionToModelArray(entertainmentCollection).ToList();

            // Get entertainments that are linked to tabs
            filter = new PredicateExpression();

            relations = new RelationCollection();
            relations.Add(EntertainmentEntityBase.Relations.UITabEntityUsingEntertainmentId);
            relations.Add(UITabEntityBase.Relations.UIModeEntityUsingUIModeId);

            if (deliverypointgroupId > 0)
            {
                filter.Add(DeliverypointgroupFields.DeliverypointgroupId == deliverypointgroupId);
                relations.Add(UIModeEntityBase.Relations.DeliverypointgroupEntityUsingUIModeId);
            }
            else if (terminalEntity != null && terminalEntity.TerminalId > 0)
            {
                filter.Add(TerminalFields.TerminalId == terminalEntity.TerminalId);
                relations.Add(UIModeEntityBase.Relations.TerminalEntityUsingUIModeId);
            }

            entertainmentCollection.GetMulti(filter, 0, null, relations, prefetch);

            foreach (EntertainmentEntity entertainmentEntity in entertainmentCollection)
            {
                bool exists = false;
                foreach (Entertainment e in entertainment)
                {
                    if (e.EntertainmentId == entertainmentEntity.EntertainmentId)
                    {
                        exists = true;
                        break;
                    }
                }

                if (!exists)
                {
                    entertainment.Add(entertainmentEntityConverter.ConvertEntityToModel(entertainmentEntity));
                }
            }

            return entertainment.ToArray();
        }
    }
}
