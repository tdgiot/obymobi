﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Comet;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public static class UIWidgetHelper
    {
        public const int CATEGORY_ID_ADDITION = 100000000;

        public static UIWidgetEntity UpdateUIWidget(UIWidget updatedWidget)
        {
            UIWidgetEntity uiWidgetEntity = UIWidgetHelper.GetPrefetchedUIWidgetEntity(updatedWidget.UIWidgetId);
            if (!uiWidgetEntity.IsNew && uiWidgetEntity.ParentCompanyId.HasValue)
            {
                uiWidgetEntity.Name = updatedWidget.Name;
                uiWidgetEntity.Caption = updatedWidget.Caption;                
                uiWidgetEntity.FieldValue1 = updatedWidget.FieldValue1;
                uiWidgetEntity.FieldValue2 = updatedWidget.FieldValue2;
                uiWidgetEntity.FieldValue3 = updatedWidget.FieldValue3;
                uiWidgetEntity.FieldValue4 = updatedWidget.FieldValue4;
                uiWidgetEntity.FieldValue5 = updatedWidget.FieldValue5;

                CustomTextCollection updatedCustomTexts = new CustomTextCollection();
                foreach (CustomText updatedCustomText in updatedWidget.CustomTexts)
                {
                    CustomTextEntity customTextEntity = uiWidgetEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == updatedCustomText.CultureCode && x.Type == updatedCustomText.Type.ToEnum<CustomTextType>());
                    if (customTextEntity == null && !updatedCustomText.Text.IsNullOrWhiteSpace())
                    {
                        customTextEntity = uiWidgetEntity.CustomTextCollection.AddNew();
                        customTextEntity.UIWidgetId = uiWidgetEntity.UIWidgetId;
                        customTextEntity.CultureCode = updatedCustomText.CultureCode;
                        customTextEntity.Type = updatedCustomText.Type.ToEnum<CustomTextType>();
                        uiWidgetEntity.CustomTextCollection.Add(customTextEntity);
                    }

                    if (customTextEntity != null && !updatedCustomText.Text.IsNullOrWhiteSpace())
                    {
                        // Update
                        customTextEntity.Text = updatedCustomText.Text;
                        updatedCustomTexts.Add(customTextEntity);
                    }
                }

                // Remove the custom text that weren't update
                CustomTextCollection oldCustomTexts = uiWidgetEntity.CustomTextCollection.Where(x => !updatedCustomTexts.Contains(x)).ToEntityCollection<CustomTextCollection>();
                if (oldCustomTexts.Count > 0)
                {
                    uiWidgetEntity.CustomTextCollection.RemoveRange(oldCustomTexts);
                    oldCustomTexts.DeleteMulti();
                }

                // Update timers
                UIWidgetTimerCollection updatedTimers = new UIWidgetTimerCollection();
                foreach (UIWidgetTimer updatedTimer in updatedWidget.UIWidgetTimers)
                {
                    UIWidgetTimerEntity uiWidgetTimerEntity = null;
                    if (updatedTimer.UIWidgetTimerId > 0)
                    {
                        uiWidgetTimerEntity = uiWidgetEntity.UIWidgetTimerCollection.SingleOrDefault(x => x.UIWidgetTimerId == updatedTimer.UIWidgetTimerId);
                    }

                    if (uiWidgetTimerEntity == null)
                    {
                        uiWidgetTimerEntity = new UIWidgetTimerEntity();
                        uiWidgetTimerEntity.UIWidgetId = uiWidgetEntity.UIWidgetId;
                        uiWidgetEntity.UIWidgetTimerCollection.Add(uiWidgetTimerEntity);                        
                    }

                    try
                    {
                        if (!updatedTimer.CountToDate.IsNullOrWhiteSpace())
                        {
                            uiWidgetTimerEntity.CountToDate = DateTime.ParseExact(updatedTimer.CountToDate, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            uiWidgetTimerEntity.CountToDate = null;
                        }

                        uiWidgetTimerEntity.CountToTime = DateTime.ParseExact(updatedTimer.CountToTime, "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                        updatedTimers.Add(uiWidgetTimerEntity);
                    }
                    catch (Exception)
                    {
                        // Too bad
                    }
                }

                // Remove the old timers 
                UIWidgetTimerCollection oldTimers = uiWidgetEntity.UIWidgetTimerCollection.Where(x => !updatedTimers.Contains(x)).ToEntityCollection<UIWidgetTimerCollection>();
                if (oldTimers.Count > 0)
                {
                    uiWidgetEntity.UIWidgetTimerCollection.RemoveRange(oldTimers);
                    oldTimers.DeleteMulti();
                }
                uiWidgetEntity.Save(true);
                return uiWidgetEntity;
            }
            return null;
        }

        /// <summary>
        /// Pushes an UI widget model to terminals and clients which are using the widgets.
        /// </summary>
        /// <param name="uiWidgetEntity"></param>
        public static void PushUIWidgetEntityToCompany(UIWidgetEntity uiWidgetEntity)
        {
            UIWidget model = new UIWidgetEntityConverter().ConvertEntityToModel(uiWidgetEntity);

            TerminalCollection terminals = new TerminalCollection();
            ClientCollection clients = new ClientCollection();

            // Get terminals
            if (uiWidgetEntity.ParentCompanyId.HasValue)
            {
                PredicateExpression terminalFilter = new PredicateExpression(TerminalFields.CompanyId == uiWidgetEntity.ParentCompanyId.Value);
                IncludeFieldsList terminalIncludes = new IncludeFieldsList(TerminalFields.TerminalId, TerminalFields.CompanyId);                
                terminals = EntityCollection.GetMulti<TerminalCollection>(terminalFilter, null, null, null, terminalIncludes);                
            }

            // Get clients
            DateTime currentTimeUtcNow = DateTime.UtcNow;
            DateTime clientLastRequest = currentTimeUtcNow.AddMinutes(-3);

            PredicateExpression clientsFilter = new PredicateExpression(UIWidgetFields.UIWidgetId == uiWidgetEntity.UIWidgetId);
            if (!TestUtil.IsPcFloris)
            {
                // FO-TEMP
                clientsFilter.Add(DeviceFields.LastRequestUTC > clientLastRequest);
            }
            IncludeFieldsList clientIncludes = new IncludeFieldsList(ClientFields.ClientId);
            RelationCollection clientRelations = new RelationCollection();
            clientRelations.Add(ClientEntityBase.Relations.DeviceEntityUsingDeviceId);
            clientRelations.Add(ClientEntityBase.Relations.DeliverypointgroupEntityUsingDeliverypointGroupId);
            clientRelations.Add(DeliverypointgroupEntityBase.Relations.UIModeEntityUsingUIModeId);
            clientRelations.Add(UIModeEntityBase.Relations.UITabEntityUsingUIModeId);
            clientRelations.Add(UITabEntityBase.Relations.UIWidgetEntityUsingUITabId);
            clients = EntityCollection.GetMulti<ClientCollection>(clientsFilter, null, clientRelations, null, clientIncludes);

            Task.Factory.StartNew(() =>
                                  {
                                      string modelJson = model.ToJson();
                                      foreach (TerminalEntity terminal in terminals)
                                      {
                                          try
                                          {
                                              CometHelper.PushModelToTerminal(terminal.TerminalId, ModelType.UIWidget, modelJson);
                                          }
                                          catch (Exception)
                                          {
                                              // ignored, too bad
                                          }

                                      }

                                      foreach (ClientEntity client in clients)
                                      {
                                          try
                                          {
                                              CometHelper.PushModelToClient(client.ClientId, ModelType.UIWidget, modelJson);
                                          }
                                          catch (Exception)
                                          {
                                              // ignored, too bad
                                          }
                                      }
                                  });
        }

        /// <summary>
        /// Retrieves a prefetched UI widget from the database.
        /// </summary>
        /// <param name="uiWidgetId">The id of the UI widget to retrieve.</param>
        /// <returns></returns>
        public static UIWidgetEntity GetPrefetchedUIWidgetEntity(int uiWidgetId)
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.UIWidgetEntity);
            IPrefetchPathElement prefetchUIWidgetCustomTexts = prefetch.Add(UIWidgetEntityBase.PrefetchPathCustomTextCollection);
            IPrefetchPathElement prefetchUIWidgetTimers = prefetch.Add(UIWidgetEntityBase.PrefetchPathUIWidgetTimerCollection);
            IPrefetchPathElement prefetchUIWidgetUIWidgetAvailability = prefetch.Add(UIWidgetEntityBase.PrefetchPathUIWidgetAvailabilityCollection);
            IPrefetchPathElement prefetchUIWidgetMedia = prefetch.Add(UIWidgetEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchUIWidgetMediaMediaRatioTypeMedia = prefetchUIWidgetMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            IPrefetchPathElement prefetchUIWidgetMediaMediaCulture = prefetchUIWidgetMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);                        

            return new UIWidgetEntity(uiWidgetId, prefetch);
        }
    }
}