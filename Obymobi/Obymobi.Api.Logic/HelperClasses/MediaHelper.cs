﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class MediaHelper
    {
        public const string GenericFilesSubPath = "generic";
        public const string BrandFilesSubPath = "brand";

        public static string GetEntitySubDirectoryName(MediaRatioTypeMediaEntity mediaRatio)
        {
            return GetEntitySubDirectoryName(mediaRatio.MediaRatioType.DeviceType, mediaRatio);
        }

        /// <summary>
        /// Gets the name of the subdirectory for the related entity of the media entity
        /// </summary>
        /// <param name="deviceType">The type of device</param>
        /// <param name="mediaRatio">The <see cref="MediaRatioTypeMediaEntity"/> instance to get the subdirectory name for</param>
        /// <returns>A <see cref="System.String"/> instance containing the name of the subdirectory</returns>
        public static string GetEntitySubDirectoryName(DeviceType deviceType, MediaRatioTypeMediaEntity mediaRatio)
        {
            string dir = string.Empty;

            List<DeviceType> craveDeviceTypes = new List<DeviceType>();
            craveDeviceTypes.Add(DeviceType.Archos70);
            craveDeviceTypes.Add(DeviceType.SamsungP5110);
            craveDeviceTypes.Add(DeviceType.AndroidTvBox);
            craveDeviceTypes.Add(DeviceType.PhoneSmall);
            craveDeviceTypes.Add(DeviceType.PhoneNormal);
            craveDeviceTypes.Add(DeviceType.PhoneLarge);
            craveDeviceTypes.Add(DeviceType.PhoneXLarge);
            craveDeviceTypes.Add(DeviceType.TabletSmall);
            craveDeviceTypes.Add(DeviceType.TabletNormal);
            craveDeviceTypes.Add(DeviceType.TabletLarge);
            craveDeviceTypes.Add(DeviceType.TabletXLarge);
            craveDeviceTypes.Add(DeviceType.iPhone);
            craveDeviceTypes.Add(DeviceType.iPhoneRetina);
            craveDeviceTypes.Add(DeviceType.iPhoneRetinaHD);
            craveDeviceTypes.Add(DeviceType.iPhoneRetinaHDPlus);
            craveDeviceTypes.Add(DeviceType.iPad);
            craveDeviceTypes.Add(DeviceType.iPadRetina);
            craveDeviceTypes.Add(DeviceType.Unknown); // Needed for Attachments

            if (deviceType == DeviceType.EM200)
            {
                // Related entity
                if (mediaRatio.MediaEntity.ProductId.HasValue || mediaRatio.MediaEntity.GenericproductId.HasValue)
                    dir = "products";
                else if (mediaRatio.MediaEntity.CategoryId.HasValue || mediaRatio.MediaEntity.GenericcategoryId.HasValue)
                    dir = "categories";
                else if (mediaRatio.MediaEntity.EntertainmentId.HasValue)
                    dir = "entertainment";
                else if (mediaRatio.MediaEntity.AdvertisementId.HasValue)
                    dir = "advertisements";
                else if (mediaRatio.MediaEntity.CompanyId.HasValue)
                    dir = "eye-catchers";
                else if (mediaRatio.MediaEntity.DeliverypointgroupId.HasValue)
                    dir = "deliverypointgroups";
                else if (mediaRatio.MediaEntity.SurveyId.HasValue || mediaRatio.MediaEntity.SurveyPageId.HasValue)
                    dir = "surveys";
                else
                    throw new ObymobiException(MediaSaveResult.MissingOrUnsupportedRelatedEntity, "Can't determine SubDirectoryName");
            }
            else if (craveDeviceTypes.Contains(deviceType))
            {
                // Related entity
                if (mediaRatio.MediaEntity.ProductId.HasValue || mediaRatio.MediaEntity.GenericproductId.HasValue)
                    dir = "products";
                else if (mediaRatio.MediaEntity.CategoryId.HasValue || mediaRatio.MediaEntity.GenericcategoryId.HasValue)
                    dir = "categories";
                else if (mediaRatio.MediaEntity.CompanyId.HasValue)
                    dir = "eye-catchers";
                else if (mediaRatio.MediaEntity.EntertainmentId.HasValue)
                    dir = "entertainment";
                else if (mediaRatio.MediaEntity.AlterationId.HasValue)
                    dir = "alterations";
                else if (mediaRatio.MediaEntity.DeliverypointgroupId.HasValue)
                    dir = "deliverypointgroups";
                else if (mediaRatio.MediaEntity.SurveyId.HasValue || mediaRatio.MediaEntity.SurveyPageId.HasValue)
                    dir = "surveys";
                else if (mediaRatio.MediaEntity.AdvertisementId.HasValue)
                    dir = "advertisements";
                else if (mediaRatio.MediaEntity.PointOfInterestId.HasValue)
                    dir = "pointsofinterest";
                else if (mediaRatio.MediaEntity.PageElementId.HasValue || mediaRatio.MediaEntity.PageTemplateElementId.HasValue)
                    dir = "pageelement";
                else if (mediaRatio.MediaEntity.PageId.HasValue || mediaRatio.MediaEntity.PageTemplateId.HasValue)
                    dir = "page";
                else if (mediaRatio.MediaEntity.SiteId.HasValue)
                    dir = "site";
                else if (mediaRatio.MediaEntity.RelatedEntityType == EntityType.AttachmentEntity)
                    dir = "attachments";
                else if (mediaRatio.MediaEntity.RoutestephandlerId.HasValue)
                    dir = "routestephandlers";
                else if (mediaRatio.MediaEntity.UIWidgetId.HasValue)
                    dir = "widgets";
                else if (mediaRatio.MediaEntity.UIThemeId.HasValue)
                    dir = "themes";
                else if (mediaRatio.MediaEntity.RoomControlSectionId.HasValue ||
                         mediaRatio.MediaEntity.RoomControlSectionItemId.HasValue ||
                         mediaRatio.MediaEntity.StationId.HasValue)
                    dir = "roomcontrol";
                else if (mediaRatio.MediaEntity.UIFooterItemId.HasValue)
                    dir = "footerbar";
                else if (mediaRatio.MediaEntity.ProductgroupId.HasValue)
                    dir = "productgroups";
                else
                    throw new ObymobiException(MediaSaveResult.MissingOrUnsupportedRelatedEntity, "Can't determine SubDirectoryName");
            }

            return dir;
        }

        public static string GetMediaRatioTypeMediaPath(MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity, MediaRatioTypeMediaEntity.FileNameType fileNameType = MediaRatioTypeMediaEntity.FileNameType.Cdn)
        {
            string toReturn = string.Empty;

            List<string> pathComponents = new List<string>();

            if (mediaRatioTypeMediaEntity.MediaEntity.IsGeneric)
            {
                pathComponents.Add(MediaHelper.GenericFilesSubPath);
            }
            else if (mediaRatioTypeMediaEntity.MediaEntity.GetRelatedBrandId().HasValue)
            {
                pathComponents.Add(MediaHelper.BrandFilesSubPath);
                pathComponents.Add(mediaRatioTypeMediaEntity.MediaEntity.GetRelatedBrandId().ToString());
            }
            else
            {
                pathComponents.Add(mediaRatioTypeMediaEntity.MediaEntity.GetRelatedCompanyId().ToString());
            }

            if (mediaRatioTypeMediaEntity.MediaRatioType != null)
            {
                // Add DeviceType                       
                pathComponents.Add(mediaRatioTypeMediaEntity.MediaRatioType.DeviceType.ToString());

                // Entity Type Name            
                pathComponents.Add(MediaHelper.GetEntitySubDirectoryName(mediaRatioTypeMediaEntity));
            }

            // Get the FileNameFormat
            pathComponents.Add(mediaRatioTypeMediaEntity.GetFileName(fileNameType));

            toReturn = string.Join("\\", pathComponents);

            return toReturn;
        } 
    }
}
