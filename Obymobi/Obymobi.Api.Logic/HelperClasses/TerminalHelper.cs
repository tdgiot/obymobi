﻿using Dionysos;
using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class TerminalHelper
    {
        public static Terminal GetSetTerminalAndDevice(string macAddress, int terminalId, int deviceType, string applicationVersion, string osVersion)
        {
            Terminal terminal = null;

            if (macAddress.IsNullOrWhiteSpace())
            {
                throw new ObymobiException(Obymobi.Logic.HelperClasses.TerminalHelper.TerminalHelperResult.MissingParameters, "macAddress");
            }
            if (terminalId <= 0)
            {
                throw new ObymobiException(Obymobi.Logic.HelperClasses.TerminalHelper.TerminalHelperResult.MissingParameters, "terminalId");
            }

            // Get / create a device for the macAddress
            DeviceEntity deviceEntity = Obymobi.Logic.HelperClasses.DeviceHelper.GetDeviceEntityByIdentifier(macAddress, true);

            // Get terminal and check if it exists.
            TerminalEntity terminalEntity = new TerminalEntity(terminalId);
            if (terminalEntity.IsNew)
            {
                throw new ObymobiException(Obymobi.Logic.HelperClasses.TerminalHelper.TerminalHelperResult.TerminalIdDoesNotExist, "Id: '{0}'", terminalId);
            }

            if (terminalEntity.DeviceId == deviceEntity.DeviceId)
            {
                // We're good, it's all configured - Update the app ^& os version
                if (!osVersion.IsNullOrWhiteSpace())
                {
                    terminalEntity.DeviceEntity.OsVersion = osVersion;
                }

                if (!applicationVersion.IsNullOrWhiteSpace())
                {
                    terminalEntity.DeviceEntity.ApplicationVersion = applicationVersion;
                }

                terminalEntity.Save();
            }
            else if (terminalEntity.DeviceId != null && terminalEntity.DeviceId != deviceEntity.DeviceId && terminalEntity.DeviceEntity.Identifier.StartsWith("FAKE")) 
            {
                terminalEntity.DeviceId = null;
                terminalEntity.Save();

                return TerminalHelper.GetSetTerminalAndDevice(macAddress, terminalId, deviceType, applicationVersion, osVersion);
            }
            else if (terminalEntity.DeviceId != null && terminalEntity.DeviceId != deviceEntity.DeviceId)
            {
                // We can only link a device if the Terminal wasn't linked yet. To change devices you have to manually
                // unlink the devices in the CMS.
                throw new ObymobiException(Obymobi.Logic.HelperClasses.TerminalHelper.TerminalHelperResult.TerminalAlreadyLinkedToAnotherDevice, "Terminal '{0}' is already linked to device '{1}'. (Unlink via CMS to change)", terminalId, deviceEntity.DeviceId);
            }
            else
            {
                // If the device was used for another terminal or client before, remove it from that one.
                if (deviceEntity.ClientEntitySingle != null)
                {
                    ClientEntity previouslyLinkedClient = deviceEntity.ClientEntitySingle;
                    deviceEntity.ClientEntitySingle.DeviceId = null;

                    ClientLogEntity log = Obymobi.Logic.HelperClasses.ClientHelper.CreateLogForDeviceChange(previouslyLinkedClient);
                    log.Message = "Device has been removed from Client '{0}' because it started up as Terminal '{1}'".FormatSafe(previouslyLinkedClient.ClientId, terminalId);

                    previouslyLinkedClient.ValidatorOverwriteDontLogDeviceChange = true;
                    previouslyLinkedClient.Save();

                    log.Save();
                }
                else if (deviceEntity.TerminalEntitySingle != null)
                {
                    TerminalEntity previouslyLinkedTerminal = deviceEntity.TerminalEntitySingle;
                    deviceEntity.TerminalEntitySingle.DeviceId = null;

                    TerminalLogEntity log = Obymobi.Logic.HelperClasses.TerminalHelper.CreateLogForDeviceChange(previouslyLinkedTerminal);
                    log.Log = "Device has been removed from Terminal '{0}' because it started up as Terminal '{1}'".FormatSafe(previouslyLinkedTerminal.TerminalId, terminalId);

                    previouslyLinkedTerminal.ValidatorOverwriteDontLogDeviceChange = true;
                    previouslyLinkedTerminal.Save();

                    // Save now, to be sure is actually happened.                
                    log.Save();
                }

                terminalEntity.DeviceId = deviceEntity.DeviceId;

                TerminalLogEntity log2 = Obymobi.Logic.HelperClasses.TerminalHelper.CreateLogForDeviceChange(terminalEntity);
                log2.Log = "Changed by TerminalHelper.GetSetTerminalAndDevice";

                if (!osVersion.IsNullOrWhiteSpace())
                {
                    terminalEntity.DeviceEntity.OsVersion = osVersion;
                }

                if (!applicationVersion.IsNullOrWhiteSpace())
                {
                    terminalEntity.DeviceEntity.ApplicationVersion = applicationVersion;
                }

                terminalEntity.ValidatorOverwriteDontLogDeviceChange = true;
                terminalEntity.Save();

                // Save now, to be sure is actually happened.                
                log2.Save();
            }

            terminal = new TerminalEntityConverter().ConvertEntityToModel(terminalEntity);

            if (deviceEntity.Type != (DeviceType)deviceType)
            {
                deviceEntity.Type = (DeviceType)deviceType;
                deviceEntity.Save();
            }

            return terminal;
        }

        public static void FillTerminalAnnouncementActions(Terminal terminal)
        {
            List<AnnouncementAction> announcementActions = new List<AnnouncementAction>();

            RelationCollection relations = new RelationCollection();
            relations.Add(EntertainmentEntity.Relations.DeliverypointgroupEntertainmentEntityUsingEntertainmentId);
            relations.Add(DeliverypointgroupEntertainmentEntity.Relations.DeliverypointgroupEntityUsingDeliverypointgroupId);

            PredicateExpression filter = new PredicateExpression();
            filter.Add(DeliverypointgroupFields.CompanyId == terminal.CompanyId);
            filter.Add(EntertainmentFields.AnnouncementAction == true);

            PrefetchPath prefetch = new PrefetchPath(EntityType.EntertainmentEntity);
            prefetch.Add(EntertainmentEntityBase.PrefetchPathDeliverypointgroupEntertainmentCollection);

            EntertainmentCollection entertainmentCollection = new EntertainmentCollection();
            entertainmentCollection.GetMulti(filter, relations);

            if (entertainmentCollection.Count > 0)
            {
                foreach (EntertainmentEntity entertainmentEntity in entertainmentCollection)
                {
                    AnnouncementAction announcementAction = new AnnouncementAction();
                    announcementAction.Type = (int)AnnouncementActionType.Entertainment;
                    announcementAction.EntertainmentId = entertainmentEntity.EntertainmentId;
                    announcementAction.Name = entertainmentEntity.Name;
                    if (entertainmentEntity.DeliverypointgroupEntertainmentCollection.Any())
                        announcementAction.DeliverypointgroupIds = entertainmentEntity.DeliverypointgroupEntertainmentCollection.Select(x => x.DeliverypointgroupId).Distinct().ToArray();
                    announcementActions.Add(announcementAction);
                }
            }

            Dictionary<int, int[]> menuIdWithAttachedDeliverypointgroupIds = Obymobi.Logic.HelperClasses.MenuHelper.GetMenuIdWithAttachedDeliverypointgroupIds(terminal.CompanyId);

            filter = new PredicateExpression();
            filter.Add(CategoryFields.CompanyId == terminal.CompanyId);
            filter.Add(CategoryFields.AnnouncementAction == true);
            filter.Add(CategoryFields.Visible == true);

            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter);

            if (categoryCollection.Count > 0)
            {
                foreach (CategoryEntity categoryEntity in categoryCollection)
                {
                    AnnouncementAction announcementAction = new AnnouncementAction();
                    announcementAction.Type = (int)AnnouncementActionType.Category;
                    announcementAction.CategoryId = categoryEntity.CategoryId;
                    announcementAction.Name = categoryEntity.Name;

                    int[] deliverypointgroupIds = null;
                    if (categoryEntity.MenuId.HasValue && menuIdWithAttachedDeliverypointgroupIds[categoryEntity.MenuId.Value].Length > 0)
                        deliverypointgroupIds = menuIdWithAttachedDeliverypointgroupIds[categoryEntity.MenuId.Value];
                    announcementAction.DeliverypointgroupIds = deliverypointgroupIds;

                    announcementActions.Add(announcementAction);
                }
            }

            filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == terminal.CompanyId);
            filter.Add(ProductFields.AnnouncementAction == true);
            filter.Add(ProductFields.VisibilityType != VisibilityType.Never);

            prefetch = new PrefetchPath(EntityType.ProductEntity);
            prefetch.Add(ProductEntityBase.PrefetchPathProductCategoryCollection).SubPath.Add(ProductCategoryEntityBase.PrefetchPathCategoryEntity);

            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter);

            if (productCollection.Count > 0)
            {
                foreach (ProductEntity productEntity in productCollection)
                {
                    List<int> deliverypointgroupIds = new List<int>();

                    int[] menuIds = productEntity.ProductCategoryCollection.Select(x => x.CategoryEntity.MenuId ?? 0).Distinct().ToArray();
                    foreach (int menuId in menuIds)
                    {
                        if (menuId > 0)
                        {
                            deliverypointgroupIds.AddRange(menuIdWithAttachedDeliverypointgroupIds[menuId]);
                        }
                    }

                    AnnouncementAction announcementAction = new AnnouncementAction();
                    announcementAction.Type = (int)AnnouncementActionType.Product;
                    announcementAction.ProductId = productEntity.ProductId;
                    announcementAction.Name = productEntity.Name;

                    int[] distinctDeliverypointgroupIds = deliverypointgroupIds.Distinct().ToArray();
                    if (distinctDeliverypointgroupIds.Length > 0)
                        announcementAction.DeliverypointgroupIds = distinctDeliverypointgroupIds;

                    announcementActions.Add(announcementAction);
                }
            }

            terminal.AnnouncementActions = announcementActions.OrderBy(x => x.Name).ToArray();
        }        

        public static void FillTerminalMessagegroups(Terminal terminal)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(MessagegroupFields.CompanyId == terminal.CompanyId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.MessagegroupEntity);
            prefetch.Add(MessagegroupEntityBase.PrefetchPathMessagegroupDeliverypointCollection).SubPath.Add(MessagegroupDeliverypointEntityBase.PrefetchPathDeliverypointEntity);

            SortExpression sort = new SortExpression(new SortClause(MessagegroupFields.Name, SortOperator.Ascending));

            MessagegroupCollection messagegroupCollection = new MessagegroupCollection();
            messagegroupCollection.GetMulti(filter, 0, sort, null, prefetch);

            terminal.Messagegroups = new MessagegroupEntityConverter().ConvertEntityCollectionToModelArray(messagegroupCollection);
        }

        public static Terminal[] GetTerminalArrayByCompanyId(int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(TerminalFields.CompanyId == companyId);

            return TerminalHelper.RetrieveAndCreateArrayFromPredicateExpression(filter);
        }

        private static Terminal[] RetrieveAndCreateArrayFromPredicateExpression(PredicateExpression filter)
        {
            TerminalCollection terminalCollection = Obymobi.Logic.HelperClasses.TerminalHelper.GetTerminalCollection(filter);
            return new TerminalEntityConverter().ConvertEntityCollectionToModelArray(terminalCollection);
        }        

        public static TerminalEntity GetPmsTerminal(int companyId) 
        {
            PredicateExpression filter = new PredicateExpression(TerminalFields.CompanyId == companyId);
            filter.Add(TerminalFields.HandlingMethod == TerminalType.OnSiteServer);
            filter.Add(TerminalFields.PMSConnectorType > 0);

            IncludeFieldsList companyIncludes = new IncludeFieldsList(CompanyFields.TimeZoneOlsonId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.TerminalEntity);
            prefetch.Add(TerminalEntityBase.PrefetchPathCompanyEntity).ExcludedIncludedFields = companyIncludes;

            TerminalCollection terminals = new TerminalCollection();
            terminals.GetMulti(filter, prefetch);

            if (terminals.Count > 0)
            {
                return terminals[0];
            }
            return null;
        }

        public static void UpdateTerminalOperationMode(int terminalId, TerminalOperationMode mode)
        {
            TerminalEntity terminalEntity = null;

            // Create and initialize a filter
            PredicateExpression filter = new PredicateExpression { TerminalFields.TerminalId == terminalId };

            // Get a TerminalCollection instance for the speciefied filter
            TerminalCollection terminalCollection = Obymobi.Logic.HelperClasses.TerminalHelper.GetTerminalCollection(filter);
            if (terminalCollection.Count == 0)
            {
                throw new ObymobiException(UpdateLastTerminalRequestResult.TerminalUnknown, "TerminalId: {0}", terminalId);
            }
            if (terminalCollection.Count > 1)
            {
                throw new ObymobiException(UpdateLastTerminalRequestResult.MultipleTerminalsFound, "TerminalId: {0}", terminalId);
            }

            terminalEntity = terminalCollection[0];

            if (terminalEntity != null)
            {
                terminalEntity.OperationMode = (int)mode;
                terminalEntity.Save();
            }
        }
    }
}
