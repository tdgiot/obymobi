﻿using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class CategoryHelper
    {
        public const int CATEGORY_ID_ADDITION = 100000000;

        public static int GetTypeFromParent(CategoryEntity category)
        {
            int type = 0;

            if (category.Type > 0)
                type = category.Type;
            else if (category.ParentCategoryId.HasValue)
                type = GetTypeFromParent(category.ParentCategoryEntity);

            return type;
        }

        public static int GetTypeFromParent(int? parentCategoryId, CategoryCollection allCategories)
        {
            int type = 0;

            CategoryEntity categoryEntity = null;

            if (parentCategoryId.HasValue && allCategories.Any(x => x.CategoryId == parentCategoryId.Value))
                categoryEntity = allCategories.FirstOrDefault(x => x.CategoryId == parentCategoryId.Value);

            if (categoryEntity != null)
            {
                if (categoryEntity.Type > 0)
                    type = categoryEntity.Type;
                else if (categoryEntity.ParentCategoryId.HasValue)
                    type = GetTypeFromParent(categoryEntity.ParentCategoryId, allCategories);
            }

            return type;
        }

        public static int GetViewLayoutTypeFromParent(CategoryEntity category)
        {
            int type = (int)ViewLayoutType.VerticalWideImage;

            if (category == null)
                return type;

            if (category.ViewLayoutType > 0)
                type = (int)category.ViewLayoutType;
            else if (category.ParentCategoryId.HasValue)
                type = GetViewLayoutTypeFromParent(category.ParentCategoryEntity);

            return type;
        }

        public static int GetViewLayoutTypeFromParent(int? parentCategoryId, CategoryCollection allCategories)
        {
            int type = (int)ViewLayoutType.VerticalWideImage;

            CategoryEntity categoryEntity = null;

            if (parentCategoryId.HasValue && allCategories.Any(x => x.CategoryId == parentCategoryId.Value))
                categoryEntity = allCategories.FirstOrDefault(x => x.CategoryId == parentCategoryId.Value);

            if (categoryEntity != null)
            {
                if (categoryEntity.ViewLayoutType > 0)
                    type = (int)categoryEntity.ViewLayoutType;
                else if (categoryEntity.ParentCategoryId.HasValue)
                    type = GetViewLayoutTypeFromParent(categoryEntity.ParentCategoryId, allCategories);
            }

            return type;
        }

        public static int GetDeliveryLocationTypeFromParent(CategoryEntity category)
        {
            int type = (int)DeliveryLocationType.Room;

            if (category == null)
                return type;

            if (category.DeliveryLocationType > 0)
                type = (int)category.DeliveryLocationType;
            else if (category.ParentCategoryId.HasValue)
                type = GetDeliveryLocationTypeFromParent(category.ParentCategoryEntity);

            return type;
        }

        public static int GetDeliveryLocationTypeFromParent(int? parentCategoryId, CategoryCollection allCategories)
        {
            int type = (int)DeliveryLocationType.Room;

            CategoryEntity categoryEntity = null;

            if (parentCategoryId.HasValue && allCategories.Any(x => x.CategoryId == parentCategoryId.Value))
                categoryEntity = allCategories.FirstOrDefault(x => x.CategoryId == parentCategoryId.Value);

            if (categoryEntity != null)
            {
                if (categoryEntity.DeliveryLocationType > 0)
                    type = (int)categoryEntity.DeliveryLocationType;
                else if (categoryEntity.ParentCategoryId.HasValue)
                    type = GetDeliveryLocationTypeFromParent(categoryEntity.ParentCategoryId, allCategories);
            }

            return type;
        }

        public static bool? HasGeofencedParent(int? parentCategoryId, CategoryCollection allCategories)
        {
            bool? geofenced = null;

            CategoryEntity categoryEntity = null;

            if (parentCategoryId.HasValue && allCategories.Any(x => x.CategoryId == parentCategoryId.Value))
                categoryEntity = allCategories.FirstOrDefault(x => x.CategoryId == parentCategoryId.Value);

            if (categoryEntity != null)
            {
                if (categoryEntity.Geofencing.HasValue)
                    geofenced = categoryEntity.Geofencing.Value;
                else if (categoryEntity.ParentCategoryId.HasValue)
                    geofenced = HasGeofencedParent(categoryEntity.ParentCategoryId, allCategories);
            }

            return geofenced;
        }

        public static bool? HasAllowFreeTextParent(CategoryEntity category)
        {
            bool? allowFreeText = null;

            if (category.AllowFreeText.HasValue)
                allowFreeText = category.AllowFreeText.Value;
            else if (category.ParentCategoryId.HasValue)
                allowFreeText = HasAllowFreeTextParent(category.ParentCategoryEntity);

            return allowFreeText;
        }

        public static bool? HasAllowFreeTextParent(int? parentCategoryId, CategoryCollection allCategories)
        {
            bool? allowFreeText = null;

            CategoryEntity categoryEntity = null;

            if (parentCategoryId.HasValue && allCategories.Any(x => x.CategoryId == parentCategoryId.Value))
                categoryEntity = allCategories.FirstOrDefault(x => x.CategoryId == parentCategoryId.Value);

            if (categoryEntity != null)
            {
                if (categoryEntity.AllowFreeText.HasValue)
                    allowFreeText = categoryEntity.AllowFreeText.Value;
                else if (categoryEntity.ParentCategoryId.HasValue)
                    allowFreeText = HasAllowFreeTextParent(categoryEntity.ParentCategoryId, allCategories);
            }

            return allowFreeText;
        }

        public static int GetScheduleIdFromParent(CategoryEntity category)
        {
            int scheduleId = 0;

            if (category.ScheduleId.HasValue)
                scheduleId = category.ScheduleId.Value;
            else if (category.ParentCategoryId.HasValue)
                scheduleId = GetScheduleIdFromParent(category.ParentCategoryEntity);

            return scheduleId;
        }

        public static Category[] GetCategories(int companyId, int? deliverypointgroupId, bool excludeEmptyCategories, bool addPosData, DeviceType? deviceType)
        {
            // Get the media types for the current device type
            MediaType[] mediaTypes = null;
            if (deviceType.HasValue)
                mediaTypes = MediaRatioTypes.GetMediaTypeArrayForDeviceTypes(deviceType.Value);

            Category[] categoryArray = null;
            RelationCollection joins = new RelationCollection();

            // Category
            PrefetchPath prefetchCategory = new PrefetchPath(EntityType.CategoryEntity);

            // Category > AdvertisementTagCategory
            var prefetchCategoryAdvertisementTagCategory = prefetchCategory.Add(CategoryEntity.PrefetchPathAdvertisementTagCategoryCollection);

            // Category > Child categories
            var prefetchCategoryChildCategory = prefetchCategory.Add(CategoryEntity.PrefetchPathChildCategoryCollection);

            // Category > Media
            // Category > Media > MediaRatioType
            var prefetchCategoryMedia = prefetchCategory.Add(CategoryEntity.PrefetchPathMediaCollection);
            var prefetchCategoryMediaMediaRatioTypeMedia = prefetchCategoryMedia.SubPath.Add(MediaEntity.PrefetchPathMediaRatioTypeMediaCollection);
            if (mediaTypes != null)
                prefetchCategoryMediaMediaRatioTypeMedia.Filter = new PredicateExpression(MediaRatioTypeMediaFields.MediaType == mediaTypes);

            // Category > Media > MediaCulture
            var prefetchCategoryMediaMediaCulture = prefetchCategoryMedia.SubPath.Add(MediaEntity.PrefetchPathMediaCultureCollection);

            // Category > ProductCategory
            var prefetchCategoryProductCategory = prefetchCategory.Add(CategoryEntity.PrefetchPathProductCategoryCollection);

            // Category > CustomText
            var prefetchCategoryCustomText = prefetchCategory.Add(CategoryEntityBase.PrefetchPathCustomTextCollection);

            // Category > Company
            // Category > Company > CustomTexts
            var prefetchCategoryCompany = prefetchCategory.Add(CategoryEntity.PrefetchPathCompanyEntity);
            var prefetchCategoryCompanyCustomTexts = prefetchCategoryCompany.SubPath.Add(CompanyEntity.PrefetchPathCustomTextCollection);

            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CategoryFields.CompanyId == companyId);

            // Add the menu ID to the filter
            if (deliverypointgroupId.HasValue)
            {
                DeliverypointgroupEntity deliverypointgroup = new DeliverypointgroupEntity(deliverypointgroupId.Value);
                filter.Add(CategoryFields.MenuId == deliverypointgroup.MenuId);
            }

            // Only get visible categories
            filter.Add(CategoryFields.VisibilityType != VisibilityType.Never);

            // Create join to exclude empty categories
            if (excludeEmptyCategories)
            {
                PredicateExpression filterExcludeEmpties = new PredicateExpression();
                joins.Add(CategoryEntity.Relations.ProductCategoryEntityUsingCategoryId, JoinHint.Left);
                filterExcludeEmpties.AddWithOr(new FieldCompareNullPredicate(ProductCategoryFields.ProductCategoryId, true));
                joins.Add(CategoryEntity.Relations.CategoryEntityUsingParentCategoryId, "ChildCategory", JoinHint.Left);
                filterExcludeEmpties.AddWithOr(new FieldCompareNullPredicate(CategoryFields.CategoryId, "ChildCategory", true));
                filter.Add(filterExcludeEmpties);
            }

            // Sort on Sort Order
            SortExpression sort = new SortExpression(CategoryFields.SortOrder | SortOperator.Ascending);

            // Create and initialize a CategoryCollection instance
            // and retrieve the items using the filter
            CategoryCollection categoryCollection = new CategoryCollection();
            categoryCollection.GetMulti(filter, 0, sort, joins, prefetchCategory);

            CategoryEntityConverter categoryEntityConverter = new CategoryEntityConverter(addPosData);

            // Initialize the array
            categoryArray = new Category[categoryCollection.Count];
            for (int i = 0; i < categoryCollection.Count; i++)
            {
                CategoryEntity categoryEntity = categoryCollection[i];
                Category category = categoryEntityConverter.ConvertEntityToModel(categoryEntity);

                // Get the type
                if (categoryEntity.Type > 0)
                    category.Type = categoryEntity.Type;
                else
                    category.Type = GetTypeFromParent(categoryEntity.ParentCategoryId, categoryCollection);

                // Get the view layout type
                if (categoryEntity.ViewLayoutType > 0)
                    category.ViewLayoutType = (int)categoryEntity.ViewLayoutType;
                else
                    category.ViewLayoutType = GetViewLayoutTypeFromParent(categoryEntity.ParentCategoryId, categoryCollection);

                // Get the delivery location type
                if (categoryEntity.DeliveryLocationType > 0)
                    category.DeliveryLocationType = (int)categoryEntity.DeliveryLocationType;
                else
                    category.DeliveryLocationType = GetDeliveryLocationTypeFromParent(categoryEntity.ParentCategoryId, categoryCollection);

                categoryArray[i] = category;
            }

            return categoryArray;
        }
    }
}
