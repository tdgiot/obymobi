﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Text;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class PosalterationHelper
    {
        public static void CleanUp(int companyId, ref StringBuilder cleanupReport, out long batchId)
        {
            cleanupReport.AppendLine("*** START: Delete Posalteration entities ***");
            batchId = -1;

            // Prepare filter for company
            PredicateExpression filter = new PredicateExpression();
            filter.Add(PosalterationFields.CompanyId == companyId);

            // Create collection and use for Scalar MAX
            PosalterationCollection pas = new PosalterationCollection();
            object maxBatchId = pas.GetScalar(Data.PosalterationFieldIndex.SynchronisationBatchId, null, AggregateFunction.Max, filter);

            // Only proceed when we have an actual maxBatchId
            if (maxBatchId != DBNull.Value && maxBatchId is long)
            {
                // Retrieve all order posalterations & delete them
                cleanupReport.AppendFormatLine("BatchId: {0}", maxBatchId);
                batchId = (long)maxBatchId;
                // Prepare filter for older posalterations
                filter = new PredicateExpression();
                filter.Add(PosalterationFields.CompanyId == companyId);
                filter.Add(PosalterationFields.SynchronisationBatchId < maxBatchId);

                pas.GetMulti(filter);

                // Delete per piece
                foreach (var pa in pas)
                {
                    cleanupReport.AppendFormatLine("ExternalId: {0}, Name: {1}",
                        pa.ExternalId, pa.Name);
                    pa.Delete();
                }

                cleanupReport.AppendFormatLine("{0} entities deleted", pas.Count);
            }
            else
                cleanupReport.AppendLine("No valid batch found");

            cleanupReport.AppendLine("*** FINISHED: Delete Posalteration entities ***");
        }
    }
}
