﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class AlterationHelper
    {
        /// <summary>
        /// Returns a list of alteration models of the alterationEntity itself, and any child alterations
        /// </summary>
        public static List<Alteration> CreateAlterationWithChildrenFromEntity(AlterationEntity alterationEntity, bool addPosData, CompanyEntity companyEntity)
        {
            AlterationEntityConverter alterationEntityConverter = new AlterationEntityConverter(addPosData, companyEntity);

            List<Alteration> alterations = new List<Alteration>();
            alterations.Add(alterationEntityConverter.ConvertEntityToModel(alterationEntity));

            if (alterationEntity.AlterationCollection.Count > 0)
            {
                foreach (AlterationEntity childAlterationEntity in alterationEntity.AlterationCollection.OrderBy(a => a.SortOrder))
                {
                    alterations.AddRange(AlterationHelper.CreateAlterationWithChildrenFromEntity(childAlterationEntity, addPosData, companyEntity));
                }
            }

            return alterations;
        }
    }
}
