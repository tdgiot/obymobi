﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class MapHelper
    {
        public static MapEntity GetPrefetchedMapEntity(int mapId)
        {
            PredicateExpression filter = new PredicateExpression(MapFields.MapId == mapId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.MapEntity);
            IPrefetchPathElement prefetchMapPointOfInterests = prefetch.Add(MapEntityBase.PrefetchPathMapPointOfInterestCollection);
            IPrefetchPathElement prefetchPointOfInterest = prefetchMapPointOfInterests.SubPath.Add(MapPointOfInterestEntityBase.PrefetchPathPointOfInterestEntity);
            IPrefetchPathElement prefetchCustomText = prefetchPointOfInterest.SubPath.Add(PointOfInterestEntityBase.PrefetchPathCustomTextCollection);
            IPrefetchPathElement prefetchMedia = prefetchPointOfInterest.SubPath.Add(PointOfInterestEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchMediaRatioTypeMedia = prefetchMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            IPrefetchPathElement prefetchPointOfInterestVenueCategories = prefetchPointOfInterest.SubPath.Add(PointOfInterestEntityBase.PrefetchPathPointOfInterestVenueCategoryCollection);
            IPrefetchPathElement prefetchVenueCategory = prefetchPointOfInterestVenueCategories.SubPath.Add(PointOfInterestVenueCategoryEntityBase.PrefetchPathVenueCategoryEntity);
            IPrefetchPathElement prefetchBusinesshours = prefetchPointOfInterest.SubPath.Add(PointOfInterestEntityBase.PrefetchPathBusinesshoursCollection);

            MapCollection maps = new MapCollection();
            maps.GetMulti(filter, prefetch);

            return maps.Count > 0 ? maps[0] : null;
        }
    }
}
