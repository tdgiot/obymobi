﻿using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class CompanyHelper
    {
        public static string GetGoogleAnalyticsPropertyId(int companyId)
        {
            IncludeFieldsList includeFieldsCompany = new IncludeFieldsList(CompanyFields.GoogleAnalyticsId);
            CompanyEntity companyEntity = new CompanyEntity();
            if (companyEntity.FetchUsingPK(companyId, null, null, includeFieldsCompany))
            {
                return companyEntity.GoogleAnalyticsId;
            }
            else
                return string.Empty;
        }

        public static Company[] GetCompanyArrayByUsernameAndPassword(string username, string password)
        {
            // Encrypt the password if its saved encrypted for the companyOwner
            if (Obymobi.Logic.HelperClasses.CompanyOwnerHelper.IsCompanyOwnerPasswordEncrypted(username))
                password = Dionysos.Security.Cryptography.Cryptographer.EncryptStringUsingRijndael(password);

            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyOwnerFields.Username == username);
            filter.Add(CompanyOwnerFields.Password == password);
            filter.Add(CompanyFields.CompanyOwnerId == CompanyOwnerFields.CompanyOwnerId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);
            prefetch.Add(CompanyEntityBase.PrefetchPathCurrencyEntity); // Currency
            prefetch.Add(CompanyEntityBase.PrefetchPathCountryEntity); // Country
            prefetch.Add(CompanyEntityBase.PrefetchPathTimeZoneEntity); // TimeZone
            prefetch.Add(CompanyEntityBase.PrefetchPathCustomTextCollection); // CustomText
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyCurrencyCollection); // CompanyCurrencies
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyCultureCollection); // CompanyCultures

            // Media
            IPrefetchPathElement prefetchCompanyMedia = prefetch.Add(CompanyEntityBase.PrefetchPathMediaCollection);
            prefetchCompanyMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchCompanyMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);

            RelationCollection relations = new RelationCollection();
            relations.Add(CompanyEntityBase.Relations.CompanyOwnerEntityUsingCompanyOwnerId);

            return CompanyHelper.RetrieveAndCreateArrayFromPredicateExpression(filter, relations, prefetch);
        }

        public static Company[] RetrieveAndCreateArrayFromPredicateExpression(PredicateExpression filter, RelationCollection relations, PrefetchPath prefetch)
        {
            // Create and initialize a CompanyCollection instance
            // and retrieve the items using the filter
            CompanyCollection companyCollection = Obymobi.Logic.HelperClasses.CompanyHelper.GetCompanyCollection(filter, relations, prefetch);
            return new CompanyEntityConverter().ConvertEntityCollectionToModelArray(companyCollection);
        }

        public static CompanyEntity GetCompanyEntityByCompanyIdForMobileWebservice(int companyId)
        {
            CompanyEntity companyEntity = null;

            PrefetchPath path = CompanyHelper.GetPrefetchPathForMobileWebservice();

            PredicateExpression filter = new PredicateExpression();
            filter.Add(CompanyFields.CompanyId == companyId);

            CompanyCollection companyCollection = new CompanyCollection();
            companyCollection.GetMulti(filter, path);

            if (companyCollection.Count == 0)
                throw new ObymobiException(GetCompanyResult.CompanyIdUnknown, "CompanyId '{0}'", companyId);
            else if (companyCollection.Count == 1)
                companyEntity = companyCollection[0];
            else if (companyCollection.Count > 1)
                throw new ObymobiException(GetCompanyResult.MultipleCompaniesFoundForCompanyId, "CompanyId '{0}'", companyId);

            return companyEntity;
        }

        public static CompanyDirectoryEntry[] GetCompanyDirectoryEntryArray(int customerId)
        {
            List<int> companyIds = new List<int>();
            CompanyCollection companies = CompanyHelper.GetCompanyCollectionForObymobi(companyIds);
            return CompanyHelper.ConvertEntityCollectionToCompanyDirectoryModelArray(companies, (customerId > 0));
        }

        private static CompanyDirectoryEntry[] ConvertEntityCollectionToCompanyDirectoryModelArray(CompanyCollection companies, bool prefetchedWithCustomerSpecificFavorites)
        {
            // Initialize the array
            CompanyDirectoryEntry[] companyDirectoryArray = new CompanyDirectoryEntry[companies.Count];
            for (int i = 0; i < companies.Count; i++)
            {
                CompanyEntity companyEntity = companies[i];
                CompanyDirectoryEntry entry = new CompanyDirectoryEntry();
                entry.CompanyId = companyEntity.CompanyId;
                entry.Name = companyEntity.Name;
                entry.Longitude = companyEntity.Longitude;
                entry.Latitude = companyEntity.Latitude;
                entry.MenuDataLastModifiedTicks = companyEntity.MenuDataLastModifiedUTC.Ticks;
                entry.MenuMediaLastModifiedTicks = companyEntity.MenuMediaLastModifiedUTC.Ticks;
                entry.CompanyDataLastModifiedTicks = companyEntity.CompanyDataLastModifiedUTC.Ticks;
                entry.CompanyMediaLastModifiedTicks = companyEntity.CompanyMediaLastModifiedUTC.Ticks;

                DeliverypointgroupDirectoryEntry[] deliverypointgroupDirectoryArray = new DeliverypointgroupDirectoryEntry[companyEntity.DeliverypointgroupCollection.Count];
                for (int j = 0; j < companyEntity.DeliverypointgroupCollection.Count; j++)
                {
                    DeliverypointgroupEntity deliverypointgroupEntity = companyEntity.DeliverypointgroupCollection[j];
                    DeliverypointgroupDirectoryEntry deliverypointgroupEntry = new DeliverypointgroupDirectoryEntry();
                    deliverypointgroupEntry.DeliverypointgroupId = deliverypointgroupEntity.DeliverypointgroupId;
                    deliverypointgroupEntry.Name = deliverypointgroupEntity.Name;
                    deliverypointgroupDirectoryArray[j] = deliverypointgroupEntry;
                }
                entry.Deliverypointgroups = deliverypointgroupDirectoryArray;

                companyDirectoryArray[i] = entry;
            }

            return companyDirectoryArray;
        }

        public static CompanyCollection GetCompanyCollectionForObymobi(List<int> companyIds)
        {
            CompanyCollection companies = new CompanyCollection();

            if (companyIds != null && companyIds.Count > 0)
            {
                // First, create and initialize a filter
                PredicateExpression filter = new PredicateExpression();
                filter.Add(CompanyFields.CompanyId == companyIds);

                var prefetch = CompanyHelper.GetPrefetchPathForMobileWebservice();
                companies.GetMulti(filter, 0, null, null, prefetch);
            }

            return companies;
        }

        private static PrefetchPath GetPrefetchPathForMobileWebservice()
        {
            PrefetchPath prefetch = new PrefetchPath(EntityType.CompanyEntity);

            // Company currencies
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyCurrencyCollection);

            // Company cultures
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyCultureCollection);

            // CustomTexts
            prefetch.Add(CompanyEntityBase.PrefetchPathCustomTextCollection);

            // Country
            prefetch.Add(CompanyEntityBase.PrefetchPathCountryEntity);

            // Media
            prefetch.Add(CompanyEntityBase.PrefetchPathMediaCollection).SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);

            // DPG
            IPrefetchPathElement dpgPath = CompanyEntityBase.PrefetchPathDeliverypointgroupCollection;
            dpgPath.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathTabletUIModeEntity).SubPath.Add(UIModeEntityBase.PrefetchPathUITabCollection).SubPath.Add(UITabEntityBase.PrefetchPathCustomTextCollection);
            dpgPath.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathMobileUIModeEntity).SubPath.Add(UIModeEntityBase.PrefetchPathUITabCollection).SubPath.Add(UITabEntityBase.PrefetchPathCustomTextCollection);
            dpgPath.SubPath.Add(DeliverypointgroupEntityBase.PrefetchPathCustomTextCollection);
            prefetch.Add(dpgPath);

            // VenueCategories
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyVenueCategoryCollection).SubPath.Add(CompanyVenueCategoryEntityBase.PrefetchPathVenueCategoryEntity).SubPath.Add(VenueCategoryEntityBase.PrefetchPathCustomTextCollection);

            // Company Amenities
            prefetch.Add(CompanyEntityBase.PrefetchPathCompanyAmenityCollection).SubPath.Add(CompanyAmenityEntityBase.PrefetchPathAmenityEntity).SubPath.Add(AmenityEntityBase.PrefetchPathCustomTextCollection);

            // Business Hours
            prefetch.Add(CompanyEntityBase.PrefetchPathBusinesshoursCollection);

            // Action Button
            prefetch.Add(CompanyEntityBase.PrefetchPathActionButtonEntity).SubPath.Add(ActionButtonEntityBase.PrefetchPathCustomTextCollection);

            return prefetch;
        }
    }
}
