﻿using System.Collections.Generic;
using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    /// <summary>
    /// AdvertisementHelper class
    /// </summary>
    public static class AdvertisementHelper
    {
        public static Advertisement[] GetAdvertisementsForDeliverypointgroup(int deliverypointgroupId, int companyId)
        { 
            // Create a filter
            var filter = new PredicateExpression();
            filter.Add(AdvertisementFields.Visible == true);
            filter.Add(DeliverypointgroupAdvertisementFields.DeliverypointgroupId == deliverypointgroupId);

            // Create the relations
            var relations = new RelationCollection();
            relations.Add(AdvertisementEntityBase.Relations.DeliverypointgroupAdvertisementEntityUsingAdvertisementId);

            // Prefetch
            var prefetch = new PrefetchPath(EntityType.AdvertisementEntity);
            IPrefetchPathElement prefetchAdvertisementTags = prefetch.Add(AdvertisementEntityBase.PrefetchPathAdvertisementTagAdvertisementCollection);            
            IPrefetchPathElement prefetchMedia = prefetch.Add(AdvertisementEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchMediaRatioTypeMedia = prefetchMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            IPrefetchPathElement prefetchMediaMediaCulture = prefetchMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);
            IPrefetchPathElement prefetchCustomTexts = prefetch.Add(AdvertisementEntityBase.PrefetchPathCustomTextCollection);

            // Get the advertisements
            var advertisementCollection = new AdvertisementCollection();
            advertisementCollection.GetMulti(filter, 0, null, relations, prefetch);

            EntityView<ProductEntity> productsView = null;
            if (companyId > 0)
            {
                // Create a filter for retrieving the active products
                PredicateExpression productsFilter = new PredicateExpression();
                productsFilter.Add(ProductFields.VisibilityType != VisibilityType.Never);       // Product should be visible
                productsFilter.Add(ProductFields.CompanyId == companyId);                       // Product should belong to the specified company

                // The join to get linked to category products only
                RelationCollection joins = new RelationCollection();
                joins.Add(ProductEntityBase.Relations.ProductCategoryEntityUsingProductId, JoinHint.Inner);

                // Get the products
                ProductCollection products = new ProductCollection();
                products.GetMulti(productsFilter, 0, null, joins);

                productsView = products.DefaultView;
            }
            
            // Initialize the array
            List<Advertisement> advertisements = new List<Advertisement>();
            for (int i = 0; i < advertisementCollection.Count; i++)
            {
                AdvertisementEntity advertisementEntity = advertisementCollection[i];

                // Create an advertisement model
                Advertisement advertisement = new AdvertisementEntityConverter().ConvertEntityToModel(advertisementEntity);

                // Set the product id based on the generic product id
                if (productsView != null && advertisement.ProductId == -1 && advertisementEntity.GenericproductId.HasValue)
                {
                    PredicateExpression genericproductFilter = new PredicateExpression();
                    genericproductFilter.Add(ProductFields.GenericproductId == advertisementEntity.GenericproductId.Value);

                    productsView.Filter = genericproductFilter;

                    if (productsView.Count == 1)
                        advertisement.ProductId = productsView[0].ProductId;   
                }

                // Otouch - Check whether we have a ProductId
                // if not, the advertisement is not exported
                // because it cannot be linked to a product
                // Crave - Advertisements per deliverypointgroup
                if ((companyId > 0 && advertisement.ProductId > 0) || deliverypointgroupId > 0)
                {
                    // Add the advertisement to the list
                    advertisements.Add(advertisement);
                }
            }

            // Return the advertisements
            return advertisements.ToArray();
        }
    }
}
