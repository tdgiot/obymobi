﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class BusinesshoursHelper
    {
        public enum BusinesshoursHelperResult
        {
            InvalidParameters = 200,
        }

        public static string ConvertBusinesshoursToIntermediateFormat(BusinesshoursCollection businesshoursCollection)
        {
            StringBuilder intermediate = new StringBuilder();

            if (businesshoursCollection != null && businesshoursCollection.Count > 0)
            {
                Businesshours[] businesshours = GetBusinesshours(businesshoursCollection);

                // {Thursday,12:00-14:00|19:00-23:00};{Friday,12:00-14:00|19:00-23:00}
                Dictionary<DayOfWeek, List<TimeRange>> timesPerDay = new Dictionary<DayOfWeek, List<TimeRange>>();

                foreach (Businesshours businesshour in businesshours)
                {
                    DayOfWeek currentDayOfWeek = (DayOfWeek)businesshour.OpeningDay;

                    int startHour = -1;
                    int startMinute = -1;
                    int endHour = -1;
                    int endMinute = -1;

                    if (!int.TryParse(businesshour.OpeningTime.ToString("0000").Substring(0, 2), out startHour))
                    {
                        // Parsing of hour failed
                    }
                    else if (!int.TryParse(businesshour.OpeningTime.ToString("0000").Substring(2, 2), out startMinute))
                    {
                        // Parsing of minute failed
                    }
                    if (!int.TryParse(businesshour.ClosingTime.ToString("0000").Substring(0, 2), out endHour))
                    {
                        // Parsing of hour failed
                    }
                    else if (!int.TryParse(businesshour.ClosingTime.ToString("0000").Substring(2, 2), out endMinute))
                    {
                        // Parsing of minute failed
                    }
                    else
                    {
                        Time start = new Time(startHour, startMinute);
                        Time end = new Time(endHour, endMinute);

                        TimeRange range = new TimeRange(start, end);

                        if (!timesPerDay.ContainsKey(currentDayOfWeek))
                        {
                            timesPerDay.Add(currentDayOfWeek, new List<TimeRange>() { range });
                        }
                        else
                        {
                            timesPerDay[currentDayOfWeek].Add(range);
                        }
                    }
                }

                if (timesPerDay.Count > 0)
                {
                    foreach (DayOfWeek dayOfWeek in timesPerDay.Keys)
                    {
                        if (intermediate.Length > 0)
                            intermediate.Append(";");

                        intermediate.Append("{");
                        intermediate.Append(dayOfWeek.ToString());
                        intermediate.Append(",");

                        List<TimeRange> ranges = timesPerDay[dayOfWeek];

                        for (int i = 0; i < ranges.Count; i++)
                        {
                            TimeRange range = ranges[i];

                            if (i > 0)
                                intermediate.Append("|");

                            intermediate.AppendFormat("{0}-{1}", range.Start, range.End);
                        }

                        intermediate.Append("}");
                    }
                }
            }

            return intermediate.ToString();
        }

        public static Businesshours[] GetBusinesshours(BusinesshoursCollection businesshoursCollection = null)
        {
            List<Businesshours> businesshours = new List<Businesshours>();

            bool first = true;
            bool open = true;
            Businesshours currentHours = null;
            foreach (BusinesshoursEntity businesshoursEntity in businesshoursCollection)
            {
                int day = int.Parse(businesshoursEntity.DayOfWeekAndTime.Substring(0, 1));
                int time = int.Parse(businesshoursEntity.DayOfWeekAndTime.Substring(1, 4));

                if (first && businesshoursEntity.Opening)
                {
                    currentHours = new Businesshours();
                    currentHours.OpeningDay = day;
                    currentHours.OpeningTime = time;

                    first = false;
                }
                else if (!first && open && !businesshoursEntity.Opening)
                {
                    currentHours.ClosingDay = day;
                    currentHours.ClosingTime = time;
                    businesshours.Add(currentHours);

                    open = false;
                }
                else if (!first && !open && businesshoursEntity.Opening)
                {
                    currentHours = new Businesshours();
                    currentHours.OpeningDay = day;
                    currentHours.OpeningTime = time;

                    open = true;
                }
            }
            if (open && !first)
            {
                foreach (BusinesshoursEntity businesshoursEntity in businesshoursCollection)
                {
                    int day = int.Parse(businesshoursEntity.DayOfWeekAndTime.Substring(0, 1));
                    int time = int.Parse(businesshoursEntity.DayOfWeekAndTime.Substring(1, 4));

                    if (!businesshoursEntity.Opening)
                    {
                        currentHours.ClosingDay = day;
                        currentHours.ClosingTime = time;

                        businesshours.Add(currentHours);
                        break;
                    }
                }
            }

            return businesshours.ToArray();
        }

        public static Businesshours[] GetBusinesshours(int? companyId, int? pointOfInterestId, BusinesshoursCollection businesshoursCollection = null)
        {
            if (companyId.HasValue && pointOfInterestId.HasValue && businesshoursCollection != null)
                throw new ObymobiException(BusinesshoursHelperResult.InvalidParameters, "You must supply a CompanyId OR a Point Of Interest Id OR a BusinessHoursCollection, not all.");
            else if (!companyId.HasValue && !pointOfInterestId.HasValue && businesshoursCollection == null)
                throw new ObymobiException(BusinesshoursHelperResult.InvalidParameters, "You must supply a CompanyId or a Point Of Interest Id OR a BusinessHoursCollection, not nothing");

            List<Businesshours> businesshours = new List<Businesshours>();

            if (businesshoursCollection == null)
            {
                PredicateExpression filter = new PredicateExpression();
                if (companyId.HasValue)
                    filter.Add(BusinesshoursFields.CompanyId == companyId);
                else
                    filter.Add(BusinesshoursFields.PointOfInterestId == pointOfInterestId);

                SortExpression sort = new SortExpression();
                sort.Add(new SortClause(BusinesshoursFields.DayOfWeekAndTime, SortOperator.Ascending));

                businesshoursCollection = new BusinesshoursCollection();
                businesshoursCollection.GetMulti(filter, 0, sort);
            }

            bool first = true;
            bool open = true;
            Businesshours currentHours = null;
            foreach (BusinesshoursEntity businesshoursEntity in businesshoursCollection)
            {
                int day = int.Parse(businesshoursEntity.DayOfWeekAndTime.Substring(0, 1));
                int time = int.Parse(businesshoursEntity.DayOfWeekAndTime.Substring(1, 4));

                if (first && businesshoursEntity.Opening)
                {
                    currentHours = new Businesshours();
                    currentHours.OpeningDay = day;
                    currentHours.OpeningTime = time;

                    first = false;
                }
                else if (!first && open && !businesshoursEntity.Opening)
                {
                    currentHours.ClosingDay = day;
                    currentHours.ClosingTime = time;
                    businesshours.Add(currentHours);

                    open = false;
                }
                else if (!first && !open && businesshoursEntity.Opening)
                {
                    currentHours = new Businesshours();
                    currentHours.OpeningDay = day;
                    currentHours.OpeningTime = time;

                    open = true;
                }
            }
            if (open && !first)
            {
                foreach (BusinesshoursEntity businesshoursEntity in businesshoursCollection)
                {
                    int day = int.Parse(businesshoursEntity.DayOfWeekAndTime.Substring(0, 1));
                    int time = int.Parse(businesshoursEntity.DayOfWeekAndTime.Substring(1, 4));

                    if (!businesshoursEntity.Opening)
                    {
                        currentHours.ClosingDay = day;
                        currentHours.ClosingTime = time;

                        businesshours.Add(currentHours);
                        break;
                    }
                }
            }

            return businesshours.ToArray();
        }
    }
}
