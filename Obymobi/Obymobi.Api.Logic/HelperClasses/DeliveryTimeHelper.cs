﻿using System.Collections.Generic;
using Dionysos.Data.LLBLGen;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public static class DeliveryTimeHelper
    {
        public static DeliveryTime GetDeliveryTimeForDeliverypointgroup(int deliverypointgroupId)
        {
            if (deliverypointgroupId > 0)
            {
                PredicateExpression filter = new PredicateExpression(DeliverypointgroupFields.DeliverypointgroupId == deliverypointgroupId);
                DeliverypointgroupCollection deliverypointgroupCollection = EntityCollection.GetMulti<DeliverypointgroupCollection>(filter, null, DeliverypointgroupFields.EstimatedDeliveryTime);
                if (deliverypointgroupCollection.Count > 0)
                {
                    return Obymobi.Logic.HelperClasses.DeliveryTimeHelper.CreateDeliveryTimeModelFromDeliverypointgroupEntity(deliverypointgroupCollection[0]);
                }
            }
            return null;
        }
    }
}
