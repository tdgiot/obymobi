﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using SD.LLBLGen.Pro.QuerySpec;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class ProductHelper
    {
        private static readonly CustomTextType[] ProductCustomTextTypes = new[]  { 
                                                                                 CustomTextType.ProductName, 
                                                                                 CustomTextType.ProductDescription, 
                                                                                 CustomTextType.ProductOrderProcessedTitle, 
                                                                                 CustomTextType.ProductOrderProcessedText,
                                                                                 CustomTextType.ProductOrderConfirmationTitle,
                                                                                 CustomTextType.ProductOrderConfirmationText,
                                                                                 CustomTextType.ProductButtonText,
                                                                                 CustomTextType.ProductCustomizeButtonText
                                                                             };

        private static readonly CustomTextType[] GenericProductCustomTextTypes = new[]
                                                                             {
                                                                                 CustomTextType.GenericproductName, 
                                                                                 CustomTextType.GenericproductDescription
                                                                             };

        public static void AddAlterationsFromCategory(List<Alteration> alterations, CategoryEntity category, bool addPosData)
        {
            if (category.CategoryAlterationCollection.Count > 0) // TODO Optimize: This is not prefetched
            {
                CategoryAlterationCollection categoryAlterations = category.CategoryAlterationCollection;
                for (int i = 0; i < categoryAlterations.Count; i++)
                {
                    if (categoryAlterations[i].Version > 1)
                        continue;

                    Alteration alteration = new AlterationEntityConverter(addPosData, categoryAlterations[i].AlterationEntity.CompanyEntity).ConvertEntityToModel(categoryAlterations[i].AlterationEntity);
                    if ((alteration.Type == (int)AlterationType.Options && alteration.Alterationoptions != null && alteration.Alterationoptions.Length > 0) || alteration.Type != (int)AlterationType.Options)
                        alterations.Add(alteration);
                }
            }
            else if (category.ParentCategoryId.HasValue)
            {
                ProductHelper.AddAlterationsFromCategory(alterations, category.ParentCategoryEntity, addPosData); // TODO Optimize: This is not prefetched
            }
        }

        public static Product[] GetProducts(int companyId, int? deliverypointgroupId, bool getProductForEachProductCategoryEntity, bool addPosData, DeviceType? deviceType)
        {
            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.VisibilityType != VisibilityType.Never);
            filter.Add(ProductFields.CompanyId == companyId);
            filter.Add(ProductFields.Type == ProductType.Product);
            filter.Add(ProductCategoryFields.ProductCategoryId != DBNull.Value);

            CompanyEntity company = new CompanyEntity(companyId);
            int? menuId = null;

            if (deliverypointgroupId.HasValue)
            {
                DeliverypointgroupEntity deliverypointgroup = new DeliverypointgroupEntity(deliverypointgroupId.Value);
                menuId = deliverypointgroup.MenuId;

                filter.Add(CategoryFields.MenuId == menuId.Value);
            }

            // Check whether the company uses a POS
            // if so, only get the products which are connected to a PosProduct
            POSConnectorType posConnectorType = company.GetPOSConnectorType();
            if (posConnectorType != POSConnectorType.Unknown && !company.IncludeMenuItemsNotExternallyLinked)
            {
                PredicateExpression posFilter = new PredicateExpression();
                posFilter.Add(ProductFields.PosproductId != DBNull.Value);
                filter.Add(posFilter);
            }

            // Get the media types for the current device type
            PredicateExpression mediaTypeFilter = new PredicateExpression();
            
            if (deviceType.HasValue)
            {
                MediaType[] mediaTypes = MediaRatioTypes.GetMediaTypeArrayForDeviceTypes(deviceType.Value);
                if (mediaTypes.Length > 0)
                {
                    mediaTypeFilter.Add(MediaRatioTypeMediaFields.MediaType == mediaTypes);
                }
            }

            SortExpression sortCategorySuggestions = new SortExpression(CategorySuggestionFields.SortOrder | SortOperator.Ascending);
            SortExpression sortCategoryAlterations = new SortExpression(CategoryAlterationFields.SortOrder | SortOperator.Ascending);

            // Product
            // Product > ProductCategory
            // Product > ProductCategory > Category
            PrefetchPath prefetchProduct = new PrefetchPath(EntityType.ProductEntity);
            IPrefetchPathElement prefetchProductProductCategory = prefetchProduct.Add(ProductEntity.PrefetchPathProductCategoryCollection);

            // Product > ProductCategory > ProductCategorySuggestion
            // Product > ProductCategory > ProductCategorySuggestion > ProductCategory
            // Product > ProductCategory > ProductCategorySuggestion > ProductCategory > Product
            SortExpression sortProductCategorySuggestions = new SortExpression(ProductCategorySuggestionFields.SortOrder | SortOperator.Ascending);
            IPrefetchPathElement prefetchProductCategoryProductCategorySuggestion = prefetchProductProductCategory.SubPath.Add(ProductCategoryEntity.PrefetchPathProductCategorySuggestionCollection, 0, null, null, sortProductCategorySuggestions);
            IPrefetchPathElement prefetchProductCategoryProductCategorySuggestionProductCategory = prefetchProductCategoryProductCategorySuggestion.SubPath.Add(ProductCategorySuggestionEntity.PrefetchPathSuggestedProductCategoryEntity);
            IPrefetchPathElement prefetchProductCategoryProductCategorySuggestionProductCategoryProduct = prefetchProductCategoryProductCategorySuggestionProductCategory.SubPath.Add(ProductCategoryEntity.PrefetchPathProductEntity);

            List<IPrefetchPathElement> categoryPaths = new List<IPrefetchPathElement>();
            for (int i = 0; i < 5; i++)
            {
                IPrefetchPathElement prefetchCategory = null;

                if (i == 0)
                    prefetchCategory = ProductCategoryEntity.PrefetchPathCategoryEntity;
                else
                    prefetchCategory = CategoryEntity.PrefetchPathParentCategoryEntity;

                // Product > ProductCategory > Category > CategorySuggestion
                // Product > ProductCategory > Category > CategorySuggestion > Product
                // Product > ProductCategory > Category > CategorySuggestion > ProductCategory
                // Product > ProductCategory > Category > CategorySuggestion > ProductCategory > Product
                IPrefetchPathElement prefetchProductProductCategoryCategoryCategorySuggestion = prefetchCategory.SubPath.Add(CategoryEntity.PrefetchPathCategorySuggestionCollection, 0, null, null, sortCategorySuggestions);
                IPrefetchPathElement prefetchProductProductCategoryCategoryCategorySuggestionProduct = prefetchProductProductCategoryCategoryCategorySuggestion.SubPath.Add(CategorySuggestionEntity.PrefetchPathProductEntity);
                IPrefetchPathElement prefetchProductProductCategoryCategoryCategorySuggestionProductCategory = prefetchProductProductCategoryCategoryCategorySuggestion.SubPath.Add(CategorySuggestionEntityBase.PrefetchPathProductCategoryEntity);
                IPrefetchPathElement prefetchProductProductCategoryCategoryCategorySuggestionProductCategoryProduct = prefetchProductProductCategoryCategoryCategorySuggestionProductCategory.SubPath.Add(ProductCategoryEntityBase.PrefetchPathProductEntity);

                // Product > ProductCategory > Category > CategoryAlteration
                // Product > ProductCategory > Category > CategoryAlteration > Alteration
                // Product > ProductCategory > Category > CategoryAlteration > Alteration > CustomTexts
                // Product > ProductCategory > Category > CategoryAlteration > Alteration > AlterationItem
                // Product > ProductCategory > Category > CategoryAlteration > Alteration > AlterationItem > Alterationoption
                // Product > ProductCategory > Category > CategoryAlteration > Alteration > AlterationItem > Alterationoption > CustomTexts
                IPrefetchPathElement prefetchProductProductCategoryCategoryCategoryAlteration = prefetchCategory.SubPath.Add(CategoryEntity.PrefetchPathCategoryAlterationCollection, 0, null, null, sortCategoryAlterations);
                IPrefetchPathElement prefetchProductProductCategoryCategoryCategoryAlterationAlteration = prefetchProductProductCategoryCategoryCategoryAlteration.SubPath.Add(CategoryAlterationEntity.PrefetchPathAlterationEntity);
                IPrefetchPathElement prefetchProductProductCategoryCategoryCategoryAlterationAlterationCustomTexts = prefetchProductProductCategoryCategoryCategoryAlterationAlteration.SubPath.Add(AlterationEntity.PrefetchPathCustomTextCollection);
                IPrefetchPathElement prefetchProductProductCategoryCategoryCategoryAlterationAlterationAlterationitem = prefetchProductProductCategoryCategoryCategoryAlterationAlteration.SubPath.Add(AlterationEntity.PrefetchPathAlterationitemCollection);
                IPrefetchPathElement prefetchProductProductCategoryCategoryCategoryAlterationAlterationAlterationitemAlterationoption = prefetchProductProductCategoryCategoryCategoryAlterationAlterationAlterationitem.SubPath.Add(AlterationitemEntity.PrefetchPathAlterationoptionEntity);
                IPrefetchPathElement prefetchProductProductCategoryCategoryCategoryAlterationAlterationAlterationitemAlterationoptionCustomTexts = prefetchProductProductCategoryCategoryCategoryAlterationAlterationAlterationitemAlterationoption.SubPath.Add(AlterationoptionEntity.PrefetchPathCustomTextCollection);

                // Product > ProductCategory > Category > OrderHour
                prefetchCategory.SubPath.Add(CategoryEntity.PrefetchPathOrderHourCollection);

                // Product > ProductCategory > Category > Media
                // Product > ProductCategory > Category > Media > MediaRatioTypeMedia
                IPrefetchPathElement prefetchProductProductCategoryCategoryMedia = prefetchCategory.SubPath.Add(CategoryEntity.PrefetchPathMediaCollection);
                IPrefetchPathElement prefetchProductProductCategoryCategoryMediaMediaRatioTypeMedia = prefetchProductProductCategoryCategoryMedia.SubPath.Add(MediaEntity.PrefetchPathMediaRatioTypeMediaCollection);
                prefetchProductProductCategoryCategoryMediaMediaRatioTypeMedia.Filter = mediaTypeFilter;

                // Product > ProductCategory > Category > Media > MediaCulture
                IPrefetchPathElement prefetchProductProductCategoryCategoryMediaMediaCulture = prefetchProductProductCategoryCategoryMedia.SubPath.Add(MediaEntity.PrefetchPathMediaCultureCollection);

                // Product > ProductCategory > Category > CustomText
                IPrefetchPathElement prefetchProductProductCategoryCategoryCustomText = prefetchCategory.SubPath.Add(CategoryEntityBase.PrefetchPathCustomTextCollection);

                categoryPaths.Add(prefetchCategory);
            }

            prefetchProductProductCategory.SubPath.Add(categoryPaths[0]).SubPath.Add(categoryPaths[1]).SubPath.Add(categoryPaths[2]).SubPath.Add(categoryPaths[3]).SubPath.Add(categoryPaths[4]);

            SortExpression sortProductSuggestions = new SortExpression(ProductSuggestionFields.SortOrder | SortOperator.Ascending);


            // Product > AdvertisementTagProduct
            prefetchProduct.Add(ProductEntityBase.PrefetchPathAdvertisementTagProductCollection);

            // Product > Rating
            prefetchProduct.Add(ProductEntityBase.PrefetchPathRatingCollection);

            // Product > Media
            // Product > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchProductMedia = prefetchProduct.Add(ProductEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchProductMediaMediaRatioTypeMedia = prefetchProductMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchProductMediaMediaRatioTypeMedia.Filter = mediaTypeFilter;

            // Product > Media > MediaCulture
            IPrefetchPathElement prefetchProductMediaMediaLanguage = prefetchProductMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);

            // Product > Attachment
            // Product > Attachment > CustomTexts
            // Product > Attachment > Media
            // Product > Attachment > Media > MediaRatioTypeMedia
            PredicateExpression attachmentFilter = new PredicateExpression(AttachmentFields.Type.In(EmenuSupported.AttachmentTypes));
            IPrefetchPathElement prefetchProductAttachment = prefetchProduct.Add(ProductEntityBase.PrefetchPathAttachmentCollection, 0, attachmentFilter);
            IPrefetchPathElement prefetchProductAttachmentCustomTexts = prefetchProductAttachment.SubPath.Add(AttachmentEntityBase.PrefetchPathCustomTextCollection);
            IPrefetchPathElement prefetchProductAttachmentMedia = prefetchProductAttachment.SubPath.Add(AttachmentEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchProductAttachmentMediaMediaRatioTypeMedia = prefetchProductAttachmentMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchProductAttachmentMediaMediaRatioTypeMedia.Filter = mediaTypeFilter;

            // Product > Vattariff
            prefetchProduct.Add(ProductEntityBase.PrefetchPathVattariffEntity);

            // Product > ProductSuggestion
            // Product > ProductSuggestion > Product
            // Product > ProductSuggestion > Product > CategoryCollectionViaProductCategory
            // Product > ProductSuggestion > ProductCategory
            // Product > ProductSuggestion > ProductCategory > Product
            IPrefetchPathElement prefetchProductProductSuggestion = prefetchProduct.Add(ProductEntityBase.PrefetchPathProductSuggestionCollection, 0, null, null, sortProductSuggestions);
            IPrefetchPathElement prefetchProductProductSuggestionProduct = prefetchProductProductSuggestion.SubPath.Add(ProductSuggestionEntityBase.PrefetchPathSuggestedProductEntity);
            IPrefetchPathElement prefetchProductProductSuggestionProductCategoryCollectionViaProductCategory = prefetchProductProductSuggestionProduct.SubPath.Add(ProductEntityBase.PrefetchPathCategoryCollectionViaProductCategory);

            // Prefetch Product Alterations
            ProductHelper.PrefetchProductAlterations(prefetchProduct, mediaTypeFilter);

            // Product > CustomText
            prefetchProduct.Add(ProductEntityBase.PrefetchPathCustomTextCollection);

            // Product > OrderHour
            prefetchProduct.Add(ProductEntityBase.PrefetchPathOrderHourCollection);

            // Product > Company
            // Product > Company > CustomTexts
            IPrefetchPathElement prefetchProductCompany = prefetchProduct.Add(ProductEntityBase.PrefetchPathCompanyEntity);
            prefetchProductCompany.SubPath.Add(CompanyEntityBase.PrefetchPathCustomTextCollection);

            // Product > Genericproduct
            IPrefetchPathElement prefetchProductGenericproduct = ProductEntityBase.PrefetchPathGenericproductEntity;

            // Product > Genericproduct > Media
            // Product > Genericproduct > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchProductGenericproductMedia = prefetchProductGenericproduct.SubPath.Add(GenericproductEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchProductGenericproductMediaMediaRatioTypeMedia = prefetchProductGenericproductMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchProductGenericproductMediaMediaRatioTypeMedia.Filter = mediaTypeFilter;

            // Product > Genericproduct > Media > MediaCulture            
            prefetchProductGenericproductMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);

            // Product > Genericproduct > CustomTexts
            prefetchProductGenericproduct.SubPath.Add(GenericproductEntityBase.PrefetchPathCustomTextCollection);

            // Product > Genericproduct > AdvertisementTagGenericproduct
            prefetchProductGenericproduct.SubPath.Add(GenericproductEntityBase.PrefetchPathAdvertisementTagGenericproductCollection);

            // Product > Genericalteration > Genericalterationoption
            prefetchProductGenericproduct.SubPath.Add(GenericproductEntityBase.PrefetchPathGenericalterationCollectionViaGenericproductGenericalteration).SubPath.Add(GenericalterationEntityBase.PrefetchPathGenericalterationoptionCollectionViaGenericalterationitem);
            prefetchProduct.Add(prefetchProductGenericproduct);

            // Add prefetches for linked brand products
            ProductHelper.PrefetchLinkedBrandProduct(prefetchProduct, mediaTypeFilter);

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductEntityBase.Relations.ProductCategoryEntityUsingProductId, JoinHint.Left);

            if (deliverypointgroupId.HasValue)
            {
                relations.Add(ProductCategoryEntityBase.Relations.CategoryEntityUsingCategoryId, JoinHint.Left);
            }

            // Create and initialize a ProductCollection instance
            // and retrieve the items using the filter
            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter, 0, null, relations, prefetchProduct);

            ProductEntityConverter productEntityConverter = new ProductEntityConverter(getProductForEachProductCategoryEntity, addPosData, menuId);

            // Initialize the array
            List<Product> products = new List<Product>();
            for (int i = 0; i < productCollection.Count; i++)
            {
                ProductEntity productEntity = productCollection[i];
                products.AddRange(productEntityConverter.ConvertEntityToModel(productEntity));
            }

            IOrderedEnumerable<Product> output = products.OrderBy(p => p.SortOrder);

            return output.ToArray();
        }

        public static Product[] GetAlterationProducts(int companyId, bool addPosData, Product[] productsToExclude, DeviceType? deviceType)
        {
            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == companyId);
            filter.Add(ProductFields.Type == ProductType.Product);
            filter.Add(ProductFields.VisibilityType != VisibilityType.Never);

            // Check whether the company uses a POS
            // if so, only get the products which are connected to a PosProduct
            CompanyEntity company = new CompanyEntity(companyId);
            if (company.GetPOSConnectorType() != POSConnectorType.Unknown && !company.IncludeMenuItemsNotExternallyLinked)
            {
                PredicateExpression posFilter = new PredicateExpression();
                posFilter.Add(ProductFields.PosproductId != DBNull.Value);
                filter.Add(posFilter);
            }

            // Sort on SortOrder
            SortExpression sort = new SortExpression(ProductFields.SortOrder | SortOperator.Ascending);

            RelationCollection relations = new RelationCollection();
            relations.Add(ProductEntityBase.Relations.AlterationProductEntityUsingProductId);
            relations.Add(AlterationProductEntityBase.Relations.AlterationEntityUsingAlterationId);

            // Get the media types for the current device type
            MediaType[] mediaTypes = null;
            if (deviceType.HasValue)
                mediaTypes = MediaRatioTypes.GetMediaTypeArrayForDeviceTypes(deviceType.Value);

            // Product
            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductEntity);

            // Product > Company
            // Product > Company > CustomText
            IPrefetchPathElement prefetchProductCompany = prefetch.Add(ProductEntityBase.PrefetchPathCompanyEntity, new IncludeFieldsList(CompanyFields.CompanyId));
            IPrefetchPathElement prefetchProductCompanyCustomText = prefetchProductCompany.SubPath.Add(CompanyEntityBase.PrefetchPathCustomTextCollection);

            // Product > Media
            // Product > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchProductMedia = prefetch.Add(ProductEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchProductMediaMediaRatioTypeMedia = prefetchProductMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            if (mediaTypes != null)
                prefetchProductMediaMediaRatioTypeMedia.Filter = new PredicateExpression(MediaRatioTypeMediaFields.MediaType == mediaTypes);

            // Product > Media > MediaLanguage
            IPrefetchPathElement prefetchProductMediaMediaCulture = prefetchProductMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);

            SortExpression sortProductAlterations = new SortExpression(ProductAlterationFields.SortOrder | SortOperator.Ascending);

            // Product > ProductAlteration
            // Product > ProductAlteration > Alteration
            IPrefetchPathElement prefetchProductProductAlteration = prefetch.Add(ProductEntityBase.PrefetchPathProductAlterationCollection, 0, null, null, sortProductAlterations);
            IPrefetchPathElement prefetchProductProductAlterationAlteration = prefetchProductProductAlteration.SubPath.Add(ProductAlterationEntityBase.PrefetchPathAlterationEntity);

            // Product > ProductAlteration > Alteration > Media
            // Product > ProductAlteration > Alteration > Media > MediaCulture            
            // Product > ProductAlteration > Alteration > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchProductProductAlterationAlterationMedia = prefetchProductProductAlterationAlteration.SubPath.Add(AlterationEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchProductProductAlterationAlterationMediaMediaCulture = prefetchProductProductAlterationAlterationMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);
            IPrefetchPathElement prefetchProductProductAlterationAlterationMediaMediaRatioTypeMedia = prefetchProductProductAlterationAlterationMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            if (mediaTypes != null)
            {
                prefetchProductProductAlterationAlterationMediaMediaRatioTypeMedia.Filter = new PredicateExpression(MediaRatioTypeMediaFields.MediaType == mediaTypes);
            }

            // Product > ProductAlteration > Alteration > CustomText
            IPrefetchPathElement prefetchProductProductAlterationAlterationCustomText = prefetchProductProductAlterationAlteration.SubPath.Add(AlterationEntityBase.PrefetchPathCustomTextCollection);

            // Product > ProductAlteration > Alteration > Alterationitem
            // Product > ProductAlteration > Alteration > Alterationitem > Alterationoption
            // Product > ProductAlteration > Alteration > Alterationitem > Alterationoption > CustomText
            IPrefetchPathElement prefetchProductProductAlterationAlterationAlterationitem = prefetchProductProductAlterationAlteration.SubPath.Add(AlterationEntityBase.PrefetchPathAlterationitemCollection);
            IPrefetchPathElement prefetchProductProductAlterationAlterationAlterationitemAlterationoption = prefetchProductProductAlterationAlterationAlterationitem.SubPath.Add(AlterationitemEntityBase.PrefetchPathAlterationoptionEntity);
            IPrefetchPathElement prefetchProductProductAlterationAlterationAlterationitemAlterationoptionAlterationoptionLanguage = prefetchProductProductAlterationAlterationAlterationitemAlterationoption.SubPath.Add(AlterationoptionEntityBase.PrefetchPathCustomTextCollection);

            List<IPrefetchPathElement> alterationPaths = new List<IPrefetchPathElement>();
            for (int i = 0; i < 4; i++)
            {
                IPrefetchPathElement alterationPath = AlterationEntityBase.PrefetchPathAlterationCollection;

                // Alteration > CustomText
                // Alteration > Alterationitem
                // Alteration > Alterationitem > Alterationoption
                // Alteration > Alterationitem > Alterationoption > CustomText
                IPrefetchPathElement alterationAlterationCustomText = alterationPath.SubPath.Add(AlterationEntityBase.PrefetchPathCustomTextCollection);
                IPrefetchPathElement alterationAlterationitem = alterationPath.SubPath.Add(AlterationEntityBase.PrefetchPathAlterationitemCollection);
                IPrefetchPathElement alterationAlterationitemAlterationoption = alterationAlterationitem.SubPath.Add(AlterationitemEntityBase.PrefetchPathAlterationoptionEntity);
                IPrefetchPathElement alterationAlterationitemAlterationoptionCustomText = alterationAlterationitemAlterationoption.SubPath.Add(AlterationoptionEntityBase.PrefetchPathCustomTextCollection);

                // Alteration > AlterationProduct
                // Alteration > AlterationProduct > Product
                IPrefetchPathElement alterationAlterationProduct = alterationPath.SubPath.Add(AlterationEntityBase.PrefetchPathAlterationProductCollection);
                IPrefetchPathElement alterationAlterationProductProduct = alterationAlterationProduct.SubPath.Add(AlterationProductEntityBase.PrefetchPathProductEntity);

                // Alteration > Media
                // Alteration > Media > MediaCulture                
                // Alteration > Media > MediaRatioTypeMedia
                IPrefetchPathElement alterationMedia = alterationPath.SubPath.Add(AlterationEntityBase.PrefetchPathMediaCollection);
                IPrefetchPathElement alterationMediaMediaLanguage = alterationMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);
                IPrefetchPathElement alterationMediaMediaRatioTypeMedia = alterationMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
                if (mediaTypes != null)
                {
                    alterationMediaMediaRatioTypeMedia.Filter = new PredicateExpression(MediaRatioTypeMediaFields.MediaType == mediaTypes);
                }

                alterationPaths.Add(alterationPath);
            }
            prefetchProductProductAlterationAlteration.SubPath.Add(alterationPaths[0]).SubPath.Add(alterationPaths[1]).SubPath.Add(alterationPaths[2]).SubPath.Add(alterationPaths[3]);

            // Product > CustomText
            prefetch.Add(ProductEntityBase.PrefetchPathCustomTextCollection);

            // Product > GenericProduct > CustomText
            prefetch.Add(ProductEntityBase.PrefetchPathGenericproductEntity).SubPath.Add(GenericproductEntityBase.PrefetchPathCustomTextCollection);

            // Create and initialize a ProductCollection instance
            // and retrieve the items using the filter
            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter, 0, sort, relations, prefetch);

            // Initialize the array
            List<Product> products = new List<Product>();
            for (int i = 0; i < productCollection.Count; i++)
            {
                if (productsToExclude.Any(x => x.ProductId == productCollection[i].ProductId))
                    continue;

                ProductEntity productEntity = productCollection[i];

                Product product = new Product();
                product.ProductId = productEntity.ProductId;
                product.GenericproductId = productEntity.GenericproductId ?? -1;
                product.BrandProductId = productEntity.BrandProductId ?? -1;
                product.Name = productEntity.Name;
                product.Description = productEntity.Description;
                product.Type = productEntity.Type;
                product.CategoryId = -1;
                product.PriceIn = productEntity.PriceIn ?? 0;
                product.TextColor = productEntity.TextColor;
                product.BackgroundColor = productEntity.BackgroundColor;
                product.DisplayOnHomepage = (productEntity.DisplayOnHomepage.HasValue ? productEntity.DisplayOnHomepage.Value : false);

                if (addPosData && productEntity.PosproductId.HasValue)
                    product.Posproduct = Obymobi.Logic.HelperClasses.PosproductHelper.CreatePosproductModelFromEntity(productEntity.PosproductEntity, false);

                // Alterations
                List<Alteration> alterations = new List<Alteration>();
                if (productEntity.ProductAlterationCollection.Count > 0)
                {
                    // Get alterations from product
                    List<ProductAlterationEntity> productAlterations = productEntity.ProductAlterationCollection.OrderBy(pa => pa.SortOrder).ToList();
                    for (int j = 0; j < productAlterations.Count; j++)
                    {
                        if (productAlterations[j].Version > 1)
                            continue;

                        List<Alteration> alterationsWithChildren = AlterationHelper.CreateAlterationWithChildrenFromEntity(productAlterations[j].AlterationEntity, addPosData, productEntity.CompanyEntity);
                        foreach (Alteration alteration in alterationsWithChildren)
                        {
                            if ((alteration.Type == (int)AlterationType.Options && alteration.Alterationoptions != null && alteration.Alterationoptions.Length > 0) || alteration.Type != (int)AlterationType.Options)
                            {
                                alterations.Add(alteration);
                            }
                        }
                    }
                }
                product.Alterations = alterations.ToArray();

                // Custom texts
                product.CustomTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(productEntity.CustomTextCollection);

                // Media
                if (productEntity.MediaCollection.Count > 0)
                {
                    product.Media = Obymobi.Logic.HelperClasses.MediaHelper.CreateMediaModelsFromEntityCollection(productEntity.MediaCollection).ToArray();
                }

                products.Add(product);
            }

            // Convert to Array
            Product[] productArray = new Product[products.Count];
            products.CopyTo(productArray);

            return productArray;
        }

        public static Product[] GetServiceProducts(int companyId, int? deliverypointgroupId, bool addPosData)
        {
            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == companyId);
            filter.Add(ProductFields.Type == ProductType.Service);
            filter.Add(ProductFields.VisibilityType != VisibilityType.Never);

            // Check whether the company uses a POS
            // if so, only get the products which are connected to a PosProduct
            CompanyEntity company = new CompanyEntity(companyId);
            if (company.GetPOSConnectorType() != POSConnectorType.Unknown && !company.IncludeMenuItemsNotExternallyLinked)
            {
                PredicateExpression posFilter = new PredicateExpression();
                posFilter.Add(ProductFields.PosproductId != DBNull.Value);
                filter.Add(posFilter);
            }

            // Sort on SortOrder
            SortExpression sort = new SortExpression(ProductFields.SortOrder | SortOperator.Ascending);

            // Create the prefetch
            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductEntity);
            prefetch.Add(ProductEntityBase.PrefetchPathCustomTextCollection);
            prefetch.Add(ProductEntityBase.PrefetchPathCompanyEntity).SubPath.Add(CompanyEntityBase.PrefetchPathCustomTextCollection);

            RelationCollection relations = null;

            // Get the service requests for this deliverypointgroup in particular
            if (deliverypointgroupId.HasValue)
            {
                relations = new RelationCollection();
                relations.Add(ProductEntityBase.Relations.DeliverypointgroupProductEntityUsingProductId);

                // Filter on type
                filter.Add(DeliverypointgroupProductFields.Type == ProductType.Service);
                filter.Add(DeliverypointgroupProductFields.DeliverypointgroupId == deliverypointgroupId.Value);
            }

            // Create and initialize a ProductCollection instance
            // and retrieve the items using the filter
            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter, 0, sort, relations, prefetch);

            ProductEntityConverter productEntityConverter = new ProductEntityConverter(false, addPosData);

            // Initialize the array
            List<Product> products = new List<Product>();
            for (int i = 0; i < productCollection.Count; i++)
            {
                ProductEntity productEntity = productCollection[i];

                if (addPosData)
                {
                    products.AddRange(productEntityConverter.ConvertEntityToModel(productEntity));
                }
                else
                {
                    Product product = new Product();
                    product.ProductId = productEntity.ProductId;
                    product.GenericproductId = productEntity.GenericproductId ?? -1;
                    product.BrandProductId = productEntity.BrandProductId ?? -1;
                    product.Name = productEntity.Name;
                    product.Description = productEntity.Description;
                    product.Type = productEntity.Type;
                    product.CategoryId = -1;
                    product.PriceIn = productEntity.PriceIn ?? 0;
                    product.TextColor = productEntity.TextColor;
                    product.BackgroundColor = productEntity.BackgroundColor;
                    product.DisplayOnHomepage = productEntity.DisplayOnHomepage ?? false;
                    product.GenericproductId = productEntity.GenericproductId ?? -1;
                    product.BrandProductId = productEntity.BrandProductId ?? -1;

                    // Custom texts
                    product.CustomTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(productEntity.CustomTextCollection);

                    products.Add(product);
                }
            }

            // Convert to Array
            Product[] productArray = new Product[products.Count];
            products.CopyTo(productArray);

            return productArray;
        }

        public static Product[] GetPaymentmethodProducts(int companyId, bool addPosData)
        {
            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == companyId);
            filter.Add(ProductFields.Type == ProductType.Paymentmethod);
            filter.Add(ProductFields.VisibilityType != VisibilityType.Never);

            // Check whether the company uses a POS
            // if so, only get the products which are connected to a PosProduct
            CompanyEntity company = new CompanyEntity(companyId);
            if (company.GetPOSConnectorType() != POSConnectorType.Unknown && !company.IncludeMenuItemsNotExternallyLinked)
            {
                PredicateExpression posFilter = new PredicateExpression();
                posFilter.Add(ProductFields.PosproductId != DBNull.Value);
                filter.Add(posFilter);
            }

            // Sort on SortOrder
            SortExpression sort = new SortExpression(ProductFields.SortOrder | SortOperator.Ascending);

            PrefetchPath prefetch = new PrefetchPath(EntityType.ProductEntity);
            prefetch.Add(ProductEntityBase.PrefetchPathCustomTextCollection);

            // Create and initialize a ProductCollection instance and retrieve the items using the filter
            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter, 0, sort, null, prefetch);

            ProductEntityConverter productEntityConverter = new ProductEntityConverter(false, addPosData);

            // Initialize the array
            List<Product> products = new List<Product>();
            for (int i = 0; i < productCollection.Count; i++)
            {
                ProductEntity productEntity = productCollection[i];

                if (addPosData)
                {
                    products.AddRange(productEntityConverter.ConvertEntityToModel(productEntity));
                }
                else
                {
                    Product product = new Product();
                    product.ProductId = productEntity.ProductId;
                    product.GenericproductId = productEntity.GenericproductId ?? -1;
                    product.BrandProductId = productEntity.BrandProductId ?? -1;
                    product.Name = productEntity.Name;
                    product.Description = productEntity.Description;
                    product.Type = productEntity.Type;
                    product.CategoryId = -1;
                    product.PriceIn = productEntity.PriceIn ?? 0;
                    product.TextColor = productEntity.TextColor;
                    product.BackgroundColor = productEntity.BackgroundColor;
                    product.DisplayOnHomepage = productEntity.DisplayOnHomepage ?? false;

                    // Custom texts
                    product.CustomTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(productEntity.CustomTextCollection);

                    products.Add(product);
                }
            }

            // Convert to Array
            Product[] productArray = new Product[products.Count];
            products.CopyTo(productArray);

            return productArray;
        }

        public static Product[] GetDeliverypointProducts(int companyId, bool addPosData)
        {
            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == companyId);
            filter.Add(ProductFields.Type == ProductType.Deliverypoint);
            filter.Add(ProductFields.VisibilityType != VisibilityType.Never);

            // Check whether the company uses a POS
            // if so, only get the products which are connected to a PosProduct
            CompanyEntity company = new CompanyEntity(companyId);
            if (company.GetPOSConnectorType() != POSConnectorType.Unknown && !company.IncludeMenuItemsNotExternallyLinked)
            {
                PredicateExpression posFilter = new PredicateExpression();
                posFilter.Add(ProductFields.PosproductId != DBNull.Value);
                filter.Add(posFilter);
            }

            // Sort on SortOrder
            SortExpression sort = new SortExpression(ProductFields.SortOrder | SortOperator.Ascending);

            // Create and initialize a ProductCollection instance and retrieve the items using the filter
            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter, 0, sort);

            // Initialize the array
            List<Product> products = new List<Product>();
            for (int i = 0; i < productCollection.Count; i++)
            {
                ProductEntity productEntity = productCollection[i];

                Product product = new Product();
                product.ProductId = productEntity.ProductId;
                product.GenericproductId = productEntity.GenericproductId ?? -1;
                product.BrandProductId = productEntity.BrandProductId ?? -1;
                product.Name = productEntity.Name;
                product.Description = productEntity.Description;
                product.Type = productEntity.Type;
                product.CategoryId = -1;
                product.PriceIn = productEntity.PriceIn ?? 0;
                product.TextColor = productEntity.TextColor;
                product.BackgroundColor = productEntity.BackgroundColor;
                product.DisplayOnHomepage = (productEntity.DisplayOnHomepage ?? false);

                if (addPosData && productEntity.PosproductId.HasValue)
                {
                    product.Posproduct = Obymobi.Logic.HelperClasses.PosproductHelper.CreatePosproductModelFromEntity(productEntity.PosproductEntity, false);
                }

                products.Add(product);
            }

            // Convert to Array
            Product[] productArray = new Product[products.Count];
            products.CopyTo(productArray);

            return productArray;
        }

        public static Product[] GetTabletProducts(int companyId, bool addPosData)
        {
            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == companyId);
            filter.Add(ProductFields.Type == ProductType.Tablet);
            filter.Add(ProductFields.VisibilityType != VisibilityType.Never);

            // Check whether the company uses a POS
            // if so, only get the products which are connected to a PosProduct
            CompanyEntity company = new CompanyEntity(companyId);
            if (company.GetPOSConnectorType() != POSConnectorType.Unknown && !company.IncludeMenuItemsNotExternallyLinked)
            {
                PredicateExpression posFilter = new PredicateExpression();
                posFilter.Add(ProductFields.PosproductId != DBNull.Value);
                filter.Add(posFilter);
            }

            // Sort on SortOrder
            SortExpression sort = new SortExpression(ProductFields.SortOrder | SortOperator.Ascending);

            // Create and initialize a ProductCollection instance and retrieve the items using the filter
            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter, 0, sort);

            // Initialize the array
            List<Product> products = new List<Product>();
            for (int i = 0; i < productCollection.Count; i++)
            {
                ProductEntity productEntity = productCollection[i];

                Product product = new Product();
                product.ProductId = productEntity.ProductId;
                product.GenericproductId = productEntity.GenericproductId ?? -1;
                product.BrandProductId = productEntity.BrandProductId ?? -1;
                product.Name = productEntity.Name;
                product.Description = productEntity.Description;
                product.Type = productEntity.Type;
                product.CategoryId = -1;
                product.PriceIn = productEntity.PriceIn ?? 0;
                product.TextColor = productEntity.TextColor;
                product.BackgroundColor = productEntity.BackgroundColor;
                product.DisplayOnHomepage = productEntity.DisplayOnHomepage ?? false;

                if (addPosData && productEntity.PosproductId.HasValue)
                {
                    product.Posproduct = Obymobi.Logic.HelperClasses.PosproductHelper.CreatePosproductModelFromEntity(productEntity.PosproductEntity, false);
                }

                products.Add(product);
            }

            // Convert to Array
            Product[] productArray = new Product[products.Count];
            products.CopyTo(productArray);

            return productArray;
        }

        public static Product[] GetPosProducts(int companyId, bool addPosData, string revenueCenter = null)
        {
            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();
            filter.Add(ProductFields.CompanyId == companyId);
            filter.Add(ProductFields.Type == ProductType.Product);

            filter.Add(new FieldCompareSetPredicate(ProductFields.PosproductId, PosproductFields.PosproductId, SetOperator.In, new FieldCompareSetPredicate(PosproductFields.PosproductId, AlterationoptionFields.PosproductId, SetOperator.In, null)));

            if (!string.IsNullOrWhiteSpace(revenueCenter))
            {
                filter.Add(PosproductFields.RevenueCenter == revenueCenter);
            }

            // Check whether the company uses a POS
            // if so, only get the products which are connected to a PosProduct
            CompanyEntity company = new CompanyEntity(companyId);
            POSConnectorType posConnectorType = company.GetPOSConnectorType();
            if (posConnectorType != POSConnectorType.Unknown)
                filter.Add(ProductFields.PosproductId != DBNull.Value);

            // Create prefetch path for Categories
            PrefetchPath path = new PrefetchPath(EntityType.ProductEntity);

            SortExpression sortCategorySuggestions = new SortExpression(CategorySuggestionFields.SortOrder | SortOperator.Ascending);
            SortExpression sortCategoryAlterations = new SortExpression(CategoryAlterationFields.SortOrder | SortOperator.Ascending);

            IPrefetchPathElement categoryPath = ProductCategoryEntityBase.PrefetchPathCategoryEntity;
            categoryPath.SubPath.Add(CategoryEntityBase.PrefetchPathCategorySuggestionCollection, 0, null, null, sortCategorySuggestions).SubPath.Add(CategorySuggestionEntityBase.PrefetchPathProductEntity);
            categoryPath.SubPath.Add(CategoryEntityBase.PrefetchPathCategoryAlterationCollection, 0, null, null, sortCategoryAlterations).SubPath.Add(CategoryAlterationEntityBase.PrefetchPathAlterationEntity).SubPath.Add(AlterationEntityBase.PrefetchPathAlterationitemCollection).SubPath.Add(AlterationitemEntityBase.PrefetchPathAlterationoptionEntity);
            categoryPath.SubPath.Add(CategoryEntityBase.PrefetchPathOrderHourCollection);

            path.Add(ProductEntityBase.PrefetchPathProductCategoryCollection).SubPath.Add(categoryPath);

            SortExpression sortProductSuggestions = new SortExpression(ProductSuggestionFields.SortOrder | SortOperator.Ascending);
            SortExpression sortProductAlterations = new SortExpression(ProductAlterationFields.SortOrder | SortOperator.Ascending);

            path.Add(ProductEntityBase.PrefetchPathAdvertisementTagProductCollection);
            path.Add(ProductEntityBase.PrefetchPathRatingCollection);
            path.Add(ProductEntityBase.PrefetchPathMediaCollection).SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            path.Add(ProductEntityBase.PrefetchPathVattariffEntity);
            path.Add(ProductEntityBase.PrefetchPathProductSuggestionCollection, 0, null, null, sortProductSuggestions).SubPath.Add(ProductSuggestionEntityBase.PrefetchPathSuggestedProductEntity);
            path.Add(ProductEntityBase.PrefetchPathProductAlterationCollection, 0, null, null, sortProductAlterations).SubPath.Add(ProductAlterationEntityBase.PrefetchPathAlterationEntity).SubPath.Add(AlterationEntityBase.PrefetchPathAlterationitemCollection).SubPath.Add(AlterationitemEntityBase.PrefetchPathAlterationoptionEntity);
            path.Add(ProductEntityBase.PrefetchPathGenericproductEntity);

            // Create and initialize a ProductCollection instance and retrieve the items using the filter
            ProductCollection productCollection = new ProductCollection();
            productCollection.GetMulti(filter, path);

            // Initialize the array
            List<Product> products = new List<Product>();
            for (int i = 0; i < productCollection.Count; i++)
            {
                ProductEntity productEntity = productCollection[i];
                products.AddRange(new ProductEntityConverter(false, addPosData).ConvertEntityToModel(productEntity));
            }

            IOrderedEnumerable<Product> output = products.OrderBy(p => p.SortOrder);

            return output.ToArray();
        }

        private static void PrefetchLinkedBrandProduct(IPrefetchPath prefetchProduct, PredicateExpression mediaTypeFilter)
        {
            PredicateExpression attachmentFilter = new PredicateExpression(AttachmentFields.Type.In(EmenuSupported.AttachmentTypes));

            // Product > ProductAttachment
            IPrefetchPathElement prefetchProductAttachment = prefetchProduct.Add(ProductEntityBase.PrefetchPathProductAttachmentCollection);
            // Product > ProductAttachment > Attachment
            IPrefetchPathElement prefetchAttachment = prefetchProductAttachment.SubPath.Add(ProductAttachmentEntityBase.PrefetchPathAttachmentEntity, 0, attachmentFilter);
            // Product > ProductAttachment > Attachment > CustomText
            prefetchAttachment.SubPath.Add(AttachmentEntityBase.PrefetchPathCustomTextCollection);
            // Product > ProductAttachment > Attachment > Media
            IPrefetchPathElement prefetchAttachmentMedia = prefetchAttachment.SubPath.Add(AttachmentEntityBase.PrefetchPathMediaCollection);
            // Product > ProductAttachment > Attachment > Media > MediaCulture
            IPrefetchPathElement prefetchAttachmentMediaCulture = prefetchAttachmentMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);
            // Product > ProductAttachment > Attachment > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchAttachmentMediaRatioTypeMedia = prefetchAttachmentMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchAttachmentMediaRatioTypeMedia.Filter = mediaTypeFilter;

            // Product > MediaRelation
            IPrefetchPathElement prefetchProductMediaRelation = prefetchProduct.Add(ProductEntityBase.PrefetchPathMediaRelationshipCollection);
            // Product > MediaRelation > Media
            IPrefetchPathElement prefetchProductMedia = prefetchProductMediaRelation.SubPath.Add(MediaRelationshipEntityBase.PrefetchPathMediaEntity);
            // Product > MediaRelation > Media > MediaCulture
            prefetchProductMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);
            // Product > MediaRelation > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchProductMediaRatioTypeMedia = prefetchProductMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchProductMediaRatioTypeMedia.Filter = mediaTypeFilter;
        }

        private static void PrefetchProductAlterations(IPrefetchPath prefetchProduct, PredicateExpression mediaTypeFilter)
        {
            SortExpression sortProductAlterations = new SortExpression(ProductAlterationFields.SortOrder | SortOperator.Ascending);

            // Product > ProductAlteration
            // Product > ProductAlteration > Alteration
            // Product > ProductAlteration > Alteration > AlterationProduct
            // Product > ProductAlteration > Alteration > AlterationProduct > Product
            // Product > ProductAlteration > Alteration > CustomTexts
            // Product > ProductAlteration > Alteration > Alterationitem
            // Product > ProductAlteration > Alteration > Alterationitem > Alterationoption
            // Product > ProductAlteration > Alteration > Alterationitem > Alterationoption > CustomTexts
            IPrefetchPathElement prefetchProductProductAlteration = prefetchProduct.Add(ProductEntityBase.PrefetchPathProductAlterationCollection, 0, null, null, sortProductAlterations);
            IPrefetchPathElement prefetchProductProductAlterationAlteration = prefetchProductProductAlteration.SubPath.Add(ProductAlterationEntityBase.PrefetchPathAlterationEntity);
            IPrefetchPathElement prefetchProductProductAlterationAlterationAlterationProduct = prefetchProductProductAlterationAlteration.SubPath.Add(AlterationEntityBase.PrefetchPathAlterationProductCollection);
            IPrefetchPathElement prefetchProductProductAlterationAlterationAlterationProductProduct = prefetchProductProductAlterationAlterationAlterationProduct.SubPath.Add(AlterationProductEntityBase.PrefetchPathProductEntity);
            IPrefetchPathElement prefetchProductProductAlterationAlterationAlterationLCustomTexts = prefetchProductProductAlterationAlteration.SubPath.Add(AlterationEntityBase.PrefetchPathCustomTextCollection);
            IPrefetchPathElement prefetchProductProductAlterationAlterationAlterationitem = prefetchProductProductAlterationAlteration.SubPath.Add(AlterationEntityBase.PrefetchPathAlterationitemCollection);
            IPrefetchPathElement prefetchProductProductAlterationAlterationAlterationitemAlterationoption = prefetchProductProductAlterationAlterationAlterationitem.SubPath.Add(AlterationitemEntityBase.PrefetchPathAlterationoptionEntity);
            IPrefetchPathElement prefetchProductProductAlterationAlterationAlterationitemAlterationoptionCustomTexts = prefetchProductProductAlterationAlterationAlterationitemAlterationoption.SubPath.Add(AlterationoptionEntityBase.PrefetchPathCustomTextCollection);

            List<IPrefetchPathElement> alterationPaths = new List<IPrefetchPathElement>();
            for (int i = 0; i < 4; i++)
            {
                IPrefetchPathElement alterationPath = AlterationEntityBase.PrefetchPathAlterationCollection;

                // Alteration > CustomTexts
                IPrefetchPathElement alterationCustomText = alterationPath.SubPath.Add(AlterationEntityBase.PrefetchPathCustomTextCollection);

                // Alteration > Alterationitem
                // Alteration > Alterationitem > Alterationoption
                // Alteration > Alterationitem > Alterationoption > CustomTexts
                IPrefetchPathElement alterationAlterationitem = alterationPath.SubPath.Add(AlterationEntityBase.PrefetchPathAlterationitemCollection);
                IPrefetchPathElement alterationAlterationitemAlterationoption = alterationAlterationitem.SubPath.Add(AlterationitemEntityBase.PrefetchPathAlterationoptionEntity);
                IPrefetchPathElement alterationAlterationitemAlterationoptionCustomTexts = alterationAlterationitemAlterationoption.SubPath.Add(AlterationoptionEntityBase.PrefetchPathCustomTextCollection);

                // Alteration > AlterationProduct
                // Alteration > AlterationProduct > Product
                IPrefetchPathElement alterationAlterationProduct = alterationPath.SubPath.Add(AlterationEntityBase.PrefetchPathAlterationProductCollection);
                IPrefetchPathElement alterationAlterationProductProduct = alterationAlterationProduct.SubPath.Add(AlterationProductEntityBase.PrefetchPathProductEntity);

                // Alteration > Media
                // Alteration > Media > MediaRatioTypeMedia
                IPrefetchPathElement alterationMedia = alterationPath.SubPath.Add(AlterationEntityBase.PrefetchPathMediaCollection);
                IPrefetchPathElement alterationMediaMediaRatioTypeMedia = alterationMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
                alterationMediaMediaRatioTypeMedia.Filter = mediaTypeFilter;

                alterationPaths.Add(alterationPath);
            }
            prefetchProductProductAlterationAlteration.SubPath.Add(alterationPaths[0]).SubPath.Add(alterationPaths[1]).SubPath.Add(alterationPaths[2]).SubPath.Add(alterationPaths[3]);

            // Product > ProductAlteration > Alteration > Media
            // Product > ProductAlteration > Alteration > Media > MediaRatioTypeMedia
            IPrefetchPathElement prefetchProductProductAlterationAlterationMedia = prefetchProductProductAlterationAlteration.SubPath.Add(AlterationEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchProductProductAlterationAlterationMediaMediaRatioTypeMedia = prefetchProductProductAlterationAlterationMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchProductProductAlterationAlterationMediaMediaRatioTypeMedia.Filter = mediaTypeFilter;

            // Product > ProductAlteration > Alteration > Media > MediaCulture
            prefetchProductProductAlterationAlterationMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);
        }

        public static CustomText[] CreateCustomTextModelsForProductEntity(ProductEntity productEntity, ProductCategoryEntity productCategoryEntity = null)
        {
            List<CustomText> customTexts = new List<CustomText>();
            foreach (CompanyCultureEntity companyCulture in productEntity.CompanyEntity.CompanyCultureCollection)
            {
                for (int i = 0; i < ProductHelper.ProductCustomTextTypes.Length; i++)
                {
                    CustomText customTextModel = null;

                    // Get custom text from product
                    CustomTextEntity customTextEntity = productEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCulture.CultureCode && x.Type == ProductHelper.ProductCustomTextTypes[i]);
                    if (customTextEntity != null)
                    {
                        customTextModel = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertEntityToModel(customTextEntity);
                    }

                    if (customTextModel == null)
                    {
                        // Get custom text from category
                        if (productCategoryEntity != null && (ProductHelper.ProductCustomTextTypes[i] == CustomTextType.ProductButtonText || ProductHelper.ProductCustomTextTypes[i] == CustomTextType.ProductCustomizeButtonText))
                        {
                            CustomTextType categoryType = ProductHelper.ProductCustomTextTypes[i] == CustomTextType.ProductButtonText ? CustomTextType.CategoryButtonText : CustomTextType.CategoryCustomizeButtonText;

                            customTextEntity = productCategoryEntity.CategoryEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCulture.CultureCode && x.Type == categoryType);
                            if (customTextEntity != null)
                            {
                                // Get from custom text of parent category
                                customTextModel = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertEntityToModel(customTextEntity);
                                customTextModel.ForeignKey = productEntity.ProductId;
                                customTextModel.Type = (int)ProductHelper.ProductCustomTextTypes[i];
                            }
                            else if ((ProductHelper.ProductCustomTextTypes[i] == CustomTextType.ProductButtonText && !productCategoryEntity.CategoryEntity.ButtonText.IsNullOrWhiteSpace()) ||
                                     (ProductHelper.ProductCustomTextTypes[i] == CustomTextType.ProductCustomizeButtonText && !productCategoryEntity.CategoryEntity.CustomizeButtonText.IsNullOrWhiteSpace()))
                            {
                                // Get text directly from parent category
                                customTextModel = new CustomText();
                                customTextModel.CultureCode = companyCulture.CultureCode;
                                customTextModel.ForeignKey = productEntity.ProductId;
                                customTextModel.Type = (int)ProductHelper.ProductCustomTextTypes[i];

                                if (ProductHelper.ProductCustomTextTypes[i] == CustomTextType.ProductButtonText)
                                {
                                    customTextModel.Text = productCategoryEntity.CategoryEntity.ButtonText;
                                }
                                else if (ProductHelper.ProductCustomTextTypes[i] == CustomTextType.ProductCustomizeButtonText)
                                {
                                    customTextModel.Text = productCategoryEntity.CategoryEntity.CustomizeButtonText;
                                }
                            }
                        }
                    }

                    if (customTextModel == null &&
                        productEntity.GenericproductId.HasValue &&
                        ProductHelper.GenericProductCustomTextTypes.Length > i &&
                        (ProductHelper.GenericProductCustomTextTypes[i] == CustomTextType.GenericproductName && productEntity.Name.IsNullOrWhiteSpace() ||
                         ProductHelper.GenericProductCustomTextTypes[i] == CustomTextType.GenericproductDescription && productEntity.Description.IsNullOrWhiteSpace()))
                    {
                        // Get custom text from generic product
                        customTextEntity = productEntity.GenericproductEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCulture.CultureCode && x.Type == ProductHelper.GenericProductCustomTextTypes[i]);
                        if (customTextEntity != null)
                        {
                            // Get from custom text of generic product
                            customTextModel = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertEntityToModel(customTextEntity);
                            customTextModel.ForeignKey = productEntity.ProductId;
                            customTextModel.Type = (int)ProductHelper.ProductCustomTextTypes[i];
                        }
                        else
                        {
                            // Get text directly from generic product
                            customTextModel = new CustomText();
                            customTextModel.CultureCode = companyCulture.CultureCode;
                            customTextModel.ForeignKey = productEntity.ProductId;
                            customTextModel.Type = (int)ProductHelper.ProductCustomTextTypes[i];

                            if (ProductHelper.GenericProductCustomTextTypes[i] == CustomTextType.GenericproductName)
                            {
                                customTextModel.Text = productEntity.GenericproductEntity.Name;
                            }
                            else if (ProductHelper.GenericProductCustomTextTypes[i] == CustomTextType.GenericproductDescription)
                            {
                                customTextModel.Text = productEntity.GenericproductEntity.Description;
                            }
                        }
                    }

                    if (customTextModel != null)
                    {
                        customTexts.Add(customTextModel);
                    }
                }
            }
            return customTexts.ToArray();
        }
    }
}
