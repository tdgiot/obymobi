﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class AccessCodeHelper
    {
        public static void GetAccessCodeIdsAndTypeFromCommaSeparatedString(string accessCodeIds, out List<int> accessCodeIdList, out AccessCodeType? type)
        {
            accessCodeIdList = GetAccessCodeIdsFromCommaSeparatedString(accessCodeIds);
            type = null;

            if (accessCodeIdList != null && accessCodeIdList.Count > 0)
            {
                // Get the access codes based on the specified IDs
                AccessCodeCollection accessCodeCollection = AccessCodeHelper.GetAccessCodeCollectionFromIds(accessCodeIdList.ToArray());
                if (accessCodeCollection.Count > 0)
                {
                    // Validate the access codes
                    if (accessCodeCollection.Any(x => x.Type == AccessCodeType.ShowOnly))
                    {
                        type = AccessCodeType.ShowOnly; // If there is any ShowOnly access code, set that as the type
                    }
                    else
                    {
                        type = AccessCodeType.Show; // If there are no ShowOnly access codes, set Show as type
                    }
                }
            }
        }

        public static AccessCodeCollection GetAccessCodeCollectionFromIds(params int[] accessCodeIds)
        {
            AccessCodeCollection accessCodeCollection = new AccessCodeCollection();

            if (accessCodeIds != null && accessCodeIds.Length > 0)
                accessCodeCollection.GetMulti(AccessCodeFields.AccessCodeId == accessCodeIds);

            return accessCodeCollection;
        }

        public static AccessCodeCollection GetAccessCodeCollectionFromCommaSeparatedString(string accessCodeIds)
        {
            int[] accessCodeIdArray = GetAccessCodeIdsFromCommaSeparatedString(accessCodeIds).ToArray();
            return GetAccessCodeCollectionFromIds(accessCodeIdArray);
        }

        public static DateTime? GetMaxLastModifiedFromCommaSeparatedString(string accessCodeIds)
        {
            DateTime? maxLastModified = null;

            // Parse the array of access code ids
            int[] accessCodeIdArray = GetAccessCodeIdsFromCommaSeparatedString(accessCodeIds).ToArray();

            if (accessCodeIdArray != null && accessCodeIdArray.Length > 0)
            {
                PredicateExpression filter = new PredicateExpression(AccessCodeFields.AccessCodeId == accessCodeIdArray);
                object obj = new AccessCodeCollection().GetScalar(AccessCodeFieldIndex.LastModifiedUTC, null, SD.LLBLGen.Pro.ORMSupportClasses.AggregateFunction.Max, filter);
                if (obj != null)
                {
                    try
                    { maxLastModified = (DateTime)obj; }
                    catch
                    { maxLastModified = null; }
                }
            }

            return maxLastModified;
        }


        public static List<int> GetAccessCodeIdsFromCommaSeparatedString(string accessCodeIds)
        {
            List<int> accessCodeIdList = new List<int>();

            if (!accessCodeIds.IsNullOrWhiteSpace())
            {
                string[] temp = accessCodeIds.Split(",", StringSplitOptions.RemoveEmptyEntries);
                
                foreach (var str in temp)
                {
                    int id;
                    if (int.TryParse(str, out id))
                        accessCodeIdList.Add(id);
                }
            }

            return accessCodeIdList;
        }

        public static AccessCodeCollection GetAccessCodeCollectionUsingCode(string code)
        {
            AccessCodeCollection accessCodeCollection = new AccessCodeCollection();

            if (!code.IsNullOrWhiteSpace())
                accessCodeCollection.GetMulti(AccessCodeFields.Code == code);

            return accessCodeCollection;
        }

        public static Dictionary<int, bool> GetHeroFlags(List<int> accessCodeIdList)
        {
            AccessCodeCollection accessCodeCollection = AccessCodeHelper.GetAccessCodeCollectionFromIds(accessCodeIdList.ToArray());

            Dictionary<int, bool> heroFlags = new Dictionary<int, bool>();

            foreach (AccessCodeEntity accessCodeEntity in accessCodeCollection)
            {
                // Companies per access code
                foreach (AccessCodeCompanyEntity accessCodeCompanyEntity in accessCodeEntity.AccessCodeCompanyCollection)
                {
                    if (!heroFlags.ContainsKey(accessCodeCompanyEntity.CompanyId))
                    {
                        // Add the IsHero flag to the dictionary if there was no entry for the current company yet
                        heroFlags.Add(accessCodeCompanyEntity.CompanyId, accessCodeCompanyEntity.IsHero);
                    }
                    else if (!heroFlags[accessCodeCompanyEntity.CompanyId] && accessCodeCompanyEntity.IsHero)
                    {
                        // Update the existing entry in the dictionary
                        heroFlags[accessCodeCompanyEntity.CompanyId] = true;
                    }
                }

                // Points of interest per access code
                foreach (AccessCodePointOfInterestEntity accessCodePointOfInterestEntity in accessCodeEntity.AccessCodePointOfInterestCollection)
                {
                    int id = accessCodePointOfInterestEntity.PointOfInterestId * -1;

                    if (!heroFlags.ContainsKey(id))
                    {
                        // Add the IsHero flag to the dictionary if there was no entry for the current company yet
                        heroFlags.Add(id, accessCodePointOfInterestEntity.IsHero);
                    }
                    else if (!heroFlags[id] && accessCodePointOfInterestEntity.IsHero)
                    {
                        // Update the existing entry in the dictionary
                        heroFlags[id] = true;
                    }
                }
            }

            return heroFlags;
        }
    }
}
