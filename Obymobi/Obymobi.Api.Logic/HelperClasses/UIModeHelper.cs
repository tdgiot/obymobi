﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Api.Logic.EntityConverters;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class UIModeHelper
    {
        public static UIMode[] GetUIModes(int companyId)
        {
            // First, create and initialize a filter
            PredicateExpression filter = new PredicateExpression();

            // Create prefetch path
            PrefetchPath prefetchPath = new PrefetchPath(EntityType.UIModeEntity);
            IPrefetchPathElement prefetchUITab = prefetchPath.Add(UIModeEntityBase.PrefetchPathUITabCollection);
            IPrefetchPathElement prefetchUITabCustomTexts = prefetchUITab.SubPath.Add(UITabEntityBase.PrefetchPathCustomTextCollection);

            IPrefetchPathElement prefetchUITabUIWidgets = prefetchUITab.SubPath.Add(UITabEntityBase.PrefetchPathUIWidgetCollection);
            IPrefetchPathElement prefetchUITabUIWidgetsCustomTexts = prefetchUITabUIWidgets.SubPath.Add(UIWidgetEntityBase.PrefetchPathCustomTextCollection);
            IPrefetchPathElement prefetchUITabUIWidgetsTimers = prefetchUITabUIWidgets.SubPath.Add(UIWidgetEntityBase.PrefetchPathUIWidgetTimerCollection);
            IPrefetchPathElement prefetchUITabUIWidgetsUIWidgetAvailability = prefetchUITabUIWidgets.SubPath.Add(UIWidgetEntityBase.PrefetchPathUIWidgetAvailabilityCollection);            
            IPrefetchPathElement prefetchUITabUIWidgetsMedia = prefetchUITabUIWidgets.SubPath.Add(UIWidgetEntityBase.PrefetchPathMediaCollection);
            IPrefetchPathElement prefetchUITabUIWidgetsMediaMediaRatioTypeMedia = prefetchUITabUIWidgetsMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            IPrefetchPathElement prefetchUITabUIWidgetsMediaMediaCulture = prefetchUITabUIWidgetsMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);
            
            IPrefetchPathElement prefetchUIFooterItem = prefetchPath.Add(UIModeEntityBase.PrefetchPathUIFooterItemCollection);
            IPrefetchPathElement prefetchUIFooterItemCustomTexts = prefetchUIFooterItem.SubPath.Add(UIFooterItemEntityBase.PrefetchPathCustomTextCollection);
            IPrefetchPathElement prefetchUIFooterItemMedia = prefetchUIFooterItem.SubPath.Add(UIFooterItemEntityBase.PrefetchPathMediaCollection);
            prefetchUIFooterItemMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);
            prefetchUIFooterItemMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaCultureCollection);

            // Add company filter
            filter.Add(UIModeFields.CompanyId == companyId);

            // Get the collection
            UIModeCollection uiModeCollection = new UIModeCollection();
            uiModeCollection.GetMulti(filter, prefetchPath);

            return new UIModeEntityConverter().ConvertEntityCollectionToModelArray(uiModeCollection);
        }
    }
}