﻿using System;
using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System.Linq;
using Dionysos;
using Obymobi.Enums;

namespace Obymobi.Api.Logic.HelperClasses
{
    public static class GuestInformationHelper
    {
        public static void SaveGuestInformations(int companyId, GuestInformation[] models)
        {
            string[] deliverypointNumbers = models.Select(x => x.DeliverypointNumber).ToArray();
            GuestInformationCollection existingGuestInformations = GuestInformationHelper.GetGuestInformationsByDeliverypointNumbers(deliverypointNumbers, companyId);
            TimeZoneInfo companyTimeZoneInfo = Obymobi.Logic.HelperClasses.CompanyHelper.GetCompanyTimeZone(companyId);

            GuestInformationCollection guestInformationToUpdate = new GuestInformationCollection();
            GuestInformationCollection guestInformationToDelete = new GuestInformationCollection();

            foreach (GuestInformation model in models)
            {
                if (guestInformationToUpdate.Any(x => x.DeliverypointNumber == model.DeliverypointNumber))
                {
                    continue;
                }

                GuestInformationEntity guestInformationEntity = existingGuestInformations.FirstOrDefault(x => x.DeliverypointNumber.Equals(model.DeliverypointNumber, StringComparison.InvariantCultureIgnoreCase));

                if (guestInformationEntity == null)
                {
                    GuestInformationHelper.CreateGuestInformationEntityFromModel(model, companyId, companyTimeZoneInfo, ref guestInformationToUpdate, ref guestInformationToDelete);
                }
                else
                {
                    GuestInformationHelper.UpdateGuestInformationEntityFromModel(guestInformationEntity, model, companyId, companyTimeZoneInfo, ref guestInformationToUpdate, ref guestInformationToDelete);
                }
            }

            if (guestInformationToUpdate.Count > 0)
            {
                guestInformationToUpdate.SaveMulti();
            }

            if (guestInformationToDelete.Count > 0)
            {
                guestInformationToDelete.DeleteMulti();
            }
        }

        private static GuestInformationCollection GetGuestInformationsByDeliverypointNumbers(string[] deliverypointNumbers, int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(GuestInformationFields.DeliverypointNumber == deliverypointNumbers);
            filter.Add(GuestInformationFields.CompanyId == companyId);

            GuestInformationCollection guestInformations = new GuestInformationCollection();
            guestInformations.GetMulti(filter);

            return guestInformations;
        }

        private static void CreateGuestInformationEntityFromModel(GuestInformation model, int companyId, TimeZoneInfo companyTimeZoneInfo, ref GuestInformationCollection toUpdate, ref GuestInformationCollection toDelete)
        {
            GuestInformationHelper.UpdateGuestInformationEntityFromModel(new GuestInformationEntity(), model, companyId, companyTimeZoneInfo, ref toUpdate, ref toDelete);
        }

        private static void UpdateGuestInformationEntityFromModel(GuestInformationEntity entity, GuestInformation model, int companyId, TimeZoneInfo companyTimeZoneInfo, ref GuestInformationCollection toUpdate, ref GuestInformationCollection toDelete)
        {
            entity.CompanyId = companyId;
            entity.ExternalGuestId = model.GuestId;
            entity.DeliverypointNumber = model.DeliverypointNumber;
            entity.ReservationNumber = model.AccountNumber;
            entity.Vip = model.Vip;
            entity.Occupied = model.Occupied;
            entity.GroupName = model.GroupName;
            entity.LanguageCode = model.LanguageCode;
            entity.DoNotDisturb = model.DoNotDisturb;
            entity.MessageWaiting = model.VoicemailMessage;
            entity.ViewBill = model.AllowViewFolio;
            entity.ExpressCheckOut = model.AllowExpressCheckout;
            entity.CheckInUTC = TimeZoneInfo.ConvertTimeToUtc(model.Arrival, companyTimeZoneInfo);
            entity.CheckOutUTC = TimeZoneInfo.ConvertTimeToUtc(model.Departure, companyTimeZoneInfo);

            if (entity.Occupied)
            {
                toUpdate.Add(entity);
            }
            else
            {
                toDelete.Add(entity);
            }
        }        
    }
}