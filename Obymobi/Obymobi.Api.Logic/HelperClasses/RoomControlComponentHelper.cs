﻿using System.Collections.Generic;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class RoomControlComponentHelper
    {
        public static RoomControlComponentCollection GetAllRoomControlComponentsInConfigurationWithComponentType(int roomControlConfigurationId, RoomControlComponentType type)
        {
            var filter = new PredicateExpression(RoomControlAreaFields.RoomControlConfigurationId == roomControlConfigurationId);
            filter.Add(RoomControlComponentFields.Type == type);

            var relations = new RelationCollection(RoomControlComponentEntityBase.Relations.RoomControlSectionEntityUsingRoomControlSectionId);
            relations.Add(RoomControlSectionEntityBase.Relations.RoomControlAreaEntityUsingRoomControlAreaId);

            var sort = new SortExpression(new SortClause(RoomControlComponentFields.Name, SortOperator.Ascending));

            var roomControlComponentCollection = new RoomControlComponentCollection();
            roomControlComponentCollection.GetMulti(filter, 0, sort, relations);

            return roomControlComponentCollection;
        }
    }
}