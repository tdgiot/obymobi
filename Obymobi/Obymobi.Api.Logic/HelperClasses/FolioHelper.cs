﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace Obymobi.Api.Logic.HelperClasses
{
    public static class FolioHelper
    {
        public static void SaveFolios(int companyId, Folio[] models)
        {
            string[] deliverypointNumbers = models.Select(x => x.DeliverypointNumber).ToArray();
            GuestInformationCollection existingGuestInformations = FolioHelper.GetGuestInformations(deliverypointNumbers, companyId);

            FolioCollection foliosToUpdate = new FolioCollection();

            foreach (Folio model in models)
            {
                GuestInformationEntity guestInformation = existingGuestInformations.SingleOrDefault(x => x.DeliverypointNumber.Equals(model.DeliverypointNumber, StringComparison.InvariantCultureIgnoreCase));

                if (guestInformation != null)
                {
                    FolioEntity entity = guestInformation.FolioCollection.Count > 0 ? guestInformation.FolioCollection[0] : new FolioEntity();
                    FolioHelper.UpdateFolioEntityFromModel(entity, model, guestInformation.GuestInformationId, ref foliosToUpdate);
                }                
            }

            if (foliosToUpdate.Count > 0)
            {
                foliosToUpdate.SaveMulti();
            }
        }

        private static void UpdateFolioEntityFromModel(FolioEntity entity, Folio model, int guestInformationId, ref FolioCollection foliosToUpdate)
        {
            entity.GuestInformationId = guestInformationId;
            entity.Balance = model.Balance;

            if (model.FolioItems != null)
            {
                foreach (FolioItem folioItemModel in model.FolioItems)
                {
                    FolioItemEntity folioItemEntity = entity.FolioItemCollection.SingleOrDefault(x => x.Code.Equals(folioItemModel.Code, StringComparison.InvariantCultureIgnoreCase));

                    if (folioItemEntity == null)
                    {
                        folioItemEntity = new FolioItemEntity();
                        entity.FolioItemCollection.Add(folioItemEntity);
                    }

                    FolioItemHelper.UpdateFolioItemEntityFromModel(folioItemEntity, folioItemModel);
                }
            }

            foliosToUpdate.Add(entity);
        }

        private static GuestInformationCollection GetGuestInformations(string[] deliverypointNumbers, int companyId)
        {
            PredicateExpression filter = new PredicateExpression();
            filter.Add(GuestInformationFields.DeliverypointNumber == deliverypointNumbers);
            filter.Add(GuestInformationFields.CompanyId == companyId);

            PrefetchPath prefetch = new PrefetchPath(EntityType.GuestInformationEntity);
            prefetch.Add(GuestInformationEntityBase.PrefetchPathFolioCollection).SubPath.Add(FolioEntityBase.PrefetchPathFolioItemCollection);

            GuestInformationCollection guestInformations = new GuestInformationCollection();
            guestInformations.GetMulti(filter, prefetch);

            return guestInformations;
        }
    }
}
