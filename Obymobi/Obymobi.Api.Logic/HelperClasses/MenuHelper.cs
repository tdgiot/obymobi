﻿using Dionysos.Data.LLBLGen;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Obymobi.Api.Logic.HelperClasses
{
    public class MenuHelper
    {
        public static ProductmenuItem[] GetMenu(int companyId, int? deliverypointgroupId, bool useCache, DeviceType? deviceType, Obymobi.Logic.HelperClasses.CacheHelper.CacheParameters cacheParams, bool isOtoucho = false)
        {
            ProductmenuItem[] productmenuItems = null;

            // Get the deliverypointgroup
            if (deliverypointgroupId.HasValue)
            {
                DeliverypointgroupEntity deliverypointgroup = new DeliverypointgroupEntity(deliverypointgroupId.Value);
                if (deliverypointgroup.IsNew)
                    throw new ObymobiException(Obymobi.Logic.HelperClasses.MenuHelper.MenuHelperErrors.DeliverypointgroupDoesNotExist, "Deliverypointgroup with id '{0}' does not exist!", deliverypointgroupId);

                if (!deliverypointgroup.MenuId.HasValue)
                    throw new ObymobiException(Obymobi.Logic.HelperClasses.MenuHelper.MenuHelperErrors.DeliverypointgroupHasNoMenu, "Deliverypointgroup with id '{0}' has no menu linked to it!", deliverypointgroupId);
            }

            if (useCache && !Obymobi.Logic.HelperClasses.CacheHelper.TryGetValue(cacheParams, out productmenuItems, isOtoucho))
            {
                productmenuItems = null;
            }

            if (productmenuItems == null)
            {
                Product[] products = ProductHelper.GetProducts(companyId, deliverypointgroupId, true, false, deviceType);
                Category[] categories = CategoryHelper.GetCategories(companyId, deliverypointgroupId, true, false, deviceType);
                Product[] serviceProducts = ProductHelper.GetServiceProducts(companyId, deliverypointgroupId, false);
                Product[] paymentmethodProducts = ProductHelper.GetPaymentmethodProducts(companyId, false);
                Product[] deliverypointProducts = ProductHelper.GetDeliverypointProducts(companyId, false);
                Product[] tabletProducts = ProductHelper.GetTabletProducts(companyId, false);
                Product[] alterationProducts = ProductHelper.GetAlterationProducts(companyId, false, products, deviceType);

                productmenuItems = new ProductmenuItem[products.Length + categories.Length + serviceProducts.Length + paymentmethodProducts.Length + deliverypointProducts.Length + tabletProducts.Length + alterationProducts.Length];

                int categoryIdAddition = 100000000;
                int counter = 0;

                // Add the categories to the menu item list
                MenuHelper.GetCategoryProductmenuItems(productmenuItems, categories, ref counter, categoryIdAddition);

                // Add the products to the menu item list
                counter = MenuHelper.GetProductProductmenuItems(productmenuItems, products, counter, categoryIdAddition);

                // Add the service products to the menu item list
                counter = MenuHelper.GetServiceProductmenuItems(productmenuItems, serviceProducts, counter);

                // Add the paymentmethod products to the menu item list
                counter = MenuHelper.GetPaymentmethodProductmenuItems(productmenuItems, paymentmethodProducts, counter);

                // Add the deliverypoint products to the menu item list
                counter = MenuHelper.GetDeliverypointProductmenuItems(productmenuItems, deliverypointProducts, counter);

                // Add the tablet products to the menu item list
                counter = MenuHelper.GetTabletProductmenuItems(productmenuItems, tabletProducts, counter);

                // Add the alteration products to the menu item list
                counter = MenuHelper.GetAlterationProductmenuItems(productmenuItems, alterationProducts, counter);

                // Write the output to the cache
                if (useCache)
                {
                    Obymobi.Logic.HelperClasses.CacheHelper.CacheObject(cacheParams, productmenuItems);
                }
            }

            return productmenuItems;
        }

        public static void GetCategoryProductmenuItems(ProductmenuItem[] productmenuItems, Category[] categories, ref int counter, int categoryIdAddition)
        {
            for (int i = 0; i < categories.Length; i++)
            {
                Category category = categories[i];

                ProductmenuItem productmenuItem = new ProductmenuItem();
                productmenuItem.Id = category.CategoryId + categoryIdAddition;
                productmenuItem.Name = category.Name;
                productmenuItem.SortOrder = category.SortOrder;
                if (category.ParentCategoryId > 0)
                {
                    productmenuItem.ParentProductmenuItemId = category.ParentCategoryId + categoryIdAddition;
                }
                else
                    productmenuItem.ParentProductmenuItemId = -1;

                productmenuItem.ItemType = 100;
                productmenuItem.IsFavorite = 0;
                productmenuItem.Media = category.Media;
                productmenuItem.AdvertisementTags = category.AdvertisementTags;
                productmenuItem.Rateable = category.Rateable;
                productmenuItem.Type = category.Type;
                productmenuItem.AnnouncementAction = category.AnnouncementAction;
                productmenuItem.ViewLayoutType = category.ViewLayoutType;
                productmenuItem.Description = category.Description;
                productmenuItem.DeliveryLocationType = category.DeliveryLocationType;
                productmenuItem.VisibilityType = category.VisibilityType;
                productmenuItem.ScheduleId = category.ScheduleId;
                productmenuItem.CustomTexts = category.CustomTexts;

                bool hidePrices = category.HidePrices;
                if (!category.HidePrices && category.ParentCategoryId > 0)
                {
                    Category parentCategory = categories.FirstOrDefault(c => c.CategoryId == category.ParentCategoryId);
                    while (parentCategory != null && !parentCategory.HidePrices && parentCategory.ParentCategoryId > 0)
                    {
                        parentCategory = categories.FirstOrDefault(c => c.CategoryId == parentCategory.ParentCategoryId);
                    }
                    hidePrices = (parentCategory != null && parentCategory.HidePrices);
                }
                productmenuItem.HidePrices = hidePrices;

                productmenuItems[counter] = productmenuItem;
                counter++;
            }
        }

        public static int GetProductProductmenuItems(ProductmenuItem[] productmenuItems, Product[] products, int counter, int categoryIdAddition)
        {
            for (int i = 0; i < products.Length; i++)
            {
                Product product = products[i];

                ProductmenuItem productmenuItem = new ProductmenuItem();
                productmenuItem.Id = product.ProductId;
                productmenuItem.Name = product.Name;
                productmenuItem.SortOrder = product.SortOrder;
                productmenuItem.PriceIn = product.PriceIn;

                if (product.CategoryId > 0)
                    productmenuItem.ParentProductmenuItemId = product.CategoryId + categoryIdAddition;
                else
                    productmenuItem.ParentProductmenuItemId = -1;

                productmenuItem.Alterations = product.Alterations;
                productmenuItem.ProductSuggestions = product.ProductSuggestions;
                productmenuItem.ItemType = (int)ProductType.Product;
                productmenuItem.IsFavorite = 0;
                productmenuItem.BackgroundColor = product.BackgroundColor;
                productmenuItem.Description = product.Description;
                productmenuItem.TextColor = product.TextColor;
                productmenuItem.DisplayOnHomepage = product.DisplayOnHomepage;
                productmenuItem.Rateable = product.Rateable;
                productmenuItem.Media = product.Media;
                productmenuItem.Attachments = product.Attachments;
                productmenuItem.AdvertisementTags = product.AdvertisementTags;
                productmenuItem.Ratings = product.Ratings;
                productmenuItem.Type = product.Type;
                productmenuItem.SubType = product.SubType;
                productmenuItem.AllowFreeText = product.AllowFreeText;
                productmenuItem.Color = product.Color;
                productmenuItem.ButtonText = product.ButtonText;
                productmenuItem.WebTypeSmartphoneUrl = product.WebTypeSmartphoneUrl;
                productmenuItem.WebTypeTabletUrl = product.WebTypeTabletUrl;
                productmenuItem.ScheduleId = product.ScheduleId;
                productmenuItem.ViewLayoutType = product.ViewLayoutType;
                productmenuItem.DeliveryLocationType = product.DeliveryLocationType;
                productmenuItem.HidePrices = product.HidePrice;
                productmenuItem.VisibilityType = product.VisibilityType;
                productmenuItem.CustomizeButtonText = product.CustomizeButtonText;
                productmenuItem.CustomTexts = product.CustomTexts;
                productmenuItem.GenericproductId = product.GenericproductId;
                productmenuItem.BrandProductId = product.BrandProductId;

                // Add the categoryIdAddition for the categoryIds of the productSuggestions to determine which category a suggestion belongs to on the IRT
                foreach (ProductSuggestion productSuggestion in product.ProductSuggestions)
                {
                    if (productSuggestion.CategoryId > 0)
                        productSuggestion.CategoryId = productSuggestion.CategoryId + categoryIdAddition;
                    else
                        productSuggestion.CategoryId = -1;
                }

                productmenuItems[counter] = productmenuItem;
                counter++;
            }
            return counter;
        }

        private static int GetServiceProductmenuItems(ProductmenuItem[] productmenuItems, Product[] serviceProducts, int counter)
        {
            for (int i = 0; i < serviceProducts.Length; i++)
            {
                Product product = serviceProducts[i];

                ProductmenuItem productmenuItem = new ProductmenuItem();
                productmenuItem.Id = product.ProductId;
                productmenuItem.Name = product.Name;
                productmenuItem.PriceIn = product.PriceIn;
                productmenuItem.ParentProductmenuItemId = -1;
                productmenuItem.ItemType = (int)ProductType.Service;
                productmenuItem.IsFavorite = 0;
                productmenuItem.BackgroundColor = product.BackgroundColor;
                productmenuItem.Description = product.Description;
                productmenuItem.TextColor = product.TextColor;
                productmenuItem.DisplayOnHomepage = product.DisplayOnHomepage;
                productmenuItem.Rateable = false;
                productmenuItem.Type = 0;
                productmenuItem.Color = product.Color;
                productmenuItem.VisibilityType = product.VisibilityType;
                productmenuItem.CustomTexts = product.CustomTexts;
                productmenuItem.GenericproductId = product.GenericproductId;
                productmenuItem.BrandProductId = product.BrandProductId;

                productmenuItems[counter] = productmenuItem;
                counter++;
            }
            return counter;
        }

        private static int GetPaymentmethodProductmenuItems(ProductmenuItem[] productmenuItems, Product[] paymentmethodProducts, int counter)
        {
            for (int i = 0; i < paymentmethodProducts.Length; i++)
            {
                Product product = paymentmethodProducts[i];

                ProductmenuItem productmenuItem = new ProductmenuItem();
                productmenuItem.Id = product.ProductId;
                productmenuItem.Name = product.Name;
                productmenuItem.PriceIn = product.PriceIn;
                productmenuItem.ParentProductmenuItemId = -1;
                productmenuItem.ItemType = (int)ProductType.Paymentmethod;
                productmenuItem.IsFavorite = 0;
                productmenuItem.BackgroundColor = product.BackgroundColor;
                productmenuItem.Description = product.Description;
                productmenuItem.TextColor = product.TextColor;
                productmenuItem.DisplayOnHomepage = product.DisplayOnHomepage;
                productmenuItem.Rateable = false;
                productmenuItem.Type = 0;
                productmenuItem.VisibilityType = product.VisibilityType;
                productmenuItem.CustomTexts = product.CustomTexts;

                productmenuItems[counter] = productmenuItem;
                counter++;
            }
            return counter;
        }

        private static int GetDeliverypointProductmenuItems(ProductmenuItem[] productmenuItems, Product[] deliverypointProducts, int counter)
        {
            for (int i = 0; i < deliverypointProducts.Length; i++)
            {
                Product product = deliverypointProducts[i];

                ProductmenuItem productmenuItem = new ProductmenuItem();
                productmenuItem.Id = product.ProductId;
                productmenuItem.Name = product.Name;
                productmenuItem.PriceIn = product.PriceIn;
                productmenuItem.ParentProductmenuItemId = -1;
                productmenuItem.ItemType = (int)ProductType.Deliverypoint;
                productmenuItem.IsFavorite = 0;
                productmenuItem.BackgroundColor = product.BackgroundColor;
                productmenuItem.Description = product.Description;
                productmenuItem.TextColor = product.TextColor;
                productmenuItem.DisplayOnHomepage = product.DisplayOnHomepage;
                productmenuItem.Rateable = false;
                productmenuItem.Type = 0;
                productmenuItem.VisibilityType = product.VisibilityType;

                productmenuItems[counter] = productmenuItem;
                counter++;
            }
            return counter;
        }

        private static int GetTabletProductmenuItems(ProductmenuItem[] productmenuItems, Product[] tabletProducts, int counter)
        {
            for (int i = 0; i < tabletProducts.Length; i++)
            {
                Product product = tabletProducts[i];

                ProductmenuItem productmenuItem = new ProductmenuItem();
                productmenuItem.Id = product.ProductId;
                productmenuItem.Name = product.Name;
                productmenuItem.PriceIn = product.PriceIn;
                productmenuItem.ParentProductmenuItemId = -1;
                productmenuItem.ItemType = (int)ProductType.Tablet;
                productmenuItem.IsFavorite = 0;
                productmenuItem.BackgroundColor = product.BackgroundColor;
                productmenuItem.Description = product.Description;
                productmenuItem.TextColor = product.TextColor;
                productmenuItem.DisplayOnHomepage = product.DisplayOnHomepage;
                productmenuItem.Rateable = false;
                productmenuItem.Type = 0;
                productmenuItem.VisibilityType = product.VisibilityType;

                productmenuItems[counter] = productmenuItem;
                counter++;
            }
            return counter;
        }

        private static int GetAlterationProductmenuItems(ProductmenuItem[] productmenuItems, Product[] alterationProducts, int counter)
        {
            for (int i = 0; i < alterationProducts.Length; i++)
            {
                Product product = alterationProducts[i];

                ProductmenuItem productmenuItem = new ProductmenuItem();
                productmenuItem.Id = product.ProductId;
                productmenuItem.Name = product.Name;
                productmenuItem.PriceIn = product.PriceIn;
                productmenuItem.ParentProductmenuItemId = -1;
                productmenuItem.ItemType = (int)ProductType.Product;
                productmenuItem.IsFavorite = 0;
                productmenuItem.BackgroundColor = product.BackgroundColor;
                productmenuItem.Description = product.Description;
                productmenuItem.TextColor = product.TextColor;
                productmenuItem.DisplayOnHomepage = product.DisplayOnHomepage;
                productmenuItem.Rateable = false;
                productmenuItem.Type = 0;
                productmenuItem.VisibilityType = product.VisibilityType;
                productmenuItem.Media = product.Media;
                productmenuItem.Alterations = product.Alterations;
                productmenuItem.CustomTexts = product.CustomTexts;
                productmenuItem.GenericproductId = product.GenericproductId;
                productmenuItem.BrandProductId = product.BrandProductId;

                productmenuItems[counter] = productmenuItem;
                counter++;
            }
            return counter;
        }

        public static MenuEntity GetMenu(int menuId, bool loadInvisibleProductsAndCategories = false)
        {
            MenuEntity menuEntity = null;

            PredicateExpression filter = new PredicateExpression();
            filter.Add(MenuFields.MenuId == menuId);
            MenuCollection menuCollection = new MenuCollection();

            try
            {
                PrefetchPath menuPath = new PrefetchPath(EntityType.MenuEntity);

                // The categories are nested, so we also need to prefetch multiple levels deep based on a Category
                // below we prepare 7 of those prefetch paths, which are mostly equal to the above (difference is starting point, Category instead of Menu)
                List<IPrefetchPathElement> categoryPaths = new List<IPrefetchPathElement>();
                for (int i = 0; i < 8; i++)
                {
                    // Reusing stuff (paths) seems to fuck shit up, therefore this verbose stuff.
                    // Get the starting point from a Category
                    IPrefetchPathElement categoryPath;
                    if (i == 0)
                        categoryPath = MenuEntityBase.PrefetchPathCategoryCollection;
                    else
                        categoryPath = CategoryEntityBase.PrefetchPathChildCategoryCollection;

                    // Product > Genericproduct
                    // Product > Genericproduct > Media
                    // Product > Genericproduct > MediaRatioTypeMedia
                    var prefetchProductGenericproduct = ProductEntityBase.PrefetchPathGenericproductEntity;
                    var prefetchProductGenericproductMedia = prefetchProductGenericproduct.SubPath.Add(GenericproductEntityBase.PrefetchPathMediaCollection);
                    var prefetchProductGenericproductMediaMediaRatioTypeMedia = prefetchProductGenericproductMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);

                    // ProductAlteration > Alteration
                    // ProductAlteration > Alteration > Company
                    var prefetchProductAlterationAlteration = ProductAlterationEntityBase.PrefetchPathAlterationEntity;
                    var prefetchProductAlterationAlterationCompany = prefetchProductAlterationAlteration.SubPath.Add(AlterationEntityBase.PrefetchPathCompanyEntity);

                    // ProductAlteration > Alteration > CustomTexts                    
                    var prefetchProductAlterationAlterationAlterationLanguage = prefetchProductAlterationAlteration.SubPath.Add(AlterationEntityBase.PrefetchPathCustomTextCollection);

                    // ProductAlteration > Alteration > Alterationitem
                    // ProductAlteration > Alteration > Alterationitem > Alterationoption
                    // ProductAlteration > Alteration > Alterationitem > Alterationoption > CustomTexts                    
                    var prefetchProductAlterationAlterationAlterationitem = prefetchProductAlterationAlteration.SubPath.Add(AlterationEntityBase.PrefetchPathAlterationitemCollection);
                    var prefetchProductAlterationAlterationAlterationitemAlterationoption = prefetchProductAlterationAlterationAlterationitem.SubPath.Add(AlterationitemEntityBase.PrefetchPathAlterationoptionEntity);
                    var prefetchProductAlterationAlterationAlterationitemAlterationoptionAlterationoptionLanguage = prefetchProductAlterationAlterationAlterationitemAlterationoption.SubPath.Add(AlterationoptionEntityBase.PrefetchPathCustomTextCollection);

                    // ProductCategory > Product
                    // ProductCategory > Product > Genericproduct
                    var prefetchProductCategoryProduct = ProductCategoryEntityBase.PrefetchPathProductEntity;
                    var prefetchProductCategoryProductGenericproduct = prefetchProductCategoryProduct.SubPath.Add(prefetchProductGenericproduct);

                    // ProductCategory > Product > ProductAlteration
                    // ProductCategory > Product > ProductAlteration > Alteration
                    var prefetchProductCategoryProductProductAlteration = prefetchProductCategoryProduct.SubPath.Add(ProductEntityBase.PrefetchPathProductAlterationCollection);
                    var prefetchProductCategoryProductProductAlterationAlteration = prefetchProductCategoryProductProductAlteration.SubPath.Add(prefetchProductAlterationAlteration);

                    // ProductCategory > Product > ProductSuggestion
                    var prefetchProductCategoryProductProductSuggestion = prefetchProductCategoryProduct.SubPath.Add(ProductEntityBase.PrefetchPathProductSuggestionCollection);

                    // ProductCategory > Product > OrderHour
                    var prefetchProductCategoryProductOrderHour = prefetchProductCategoryProduct.SubPath.Add(ProductEntityBase.PrefetchPathOrderHourCollection);

                    // ProductCategory > Product > CustomText
                    var prefetchProductCategoryProductCustomText = prefetchProductCategoryProduct.SubPath.Add(ProductEntityBase.PrefetchPathCustomTextCollection);

                    // Category > Media
                    // Category > Media > MediaRatioTypeMedia
                    var prefetchCategoryMedia = categoryPath.SubPath.Add(CategoryEntityBase.PrefetchPathMediaCollection);
                    var prefetchCategoryMediaMediaRatioTypeMedia = prefetchCategoryMedia.SubPath.Add(MediaEntityBase.PrefetchPathMediaRatioTypeMediaCollection);

                    // Category > CustomText
                    var prefetchCategoryCustomText = categoryPath.SubPath.Add(CategoryEntityBase.PrefetchPathCustomTextCollection);

                    // Category > ProductCategory
                    // Category > ProductCategory > Product
                    var prefetchCategoryProductCategory = categoryPath.SubPath.Add(CategoryEntityBase.PrefetchPathProductCategoryCollection);
                    var prefetchCategoryProductCategoryProduct = prefetchCategoryProductCategory.SubPath.Add(prefetchProductCategoryProduct);

                    // Category > OrderHour
                    var prefetchCategoryOrderHour = categoryPath.SubPath.Add(CategoryEntityBase.PrefetchPathOrderHourCollection);

                    // Category > CategorySuggestion
                    var prefetchCategoryCategorySuggestion = categoryPath.SubPath.Add(CategoryEntityBase.PrefetchPathCategorySuggestionCollection);

                    // PILS LOW PRIO! Research why we can't add this prefetch (then we get no good results)
                    //subCategoryPath.SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity).SubPath.Add(CategoryEntity.PrefetchPathParentCategoryEntity);

                    categoryPaths.Add(categoryPath);
                }

                // Categories + Subcategories
                menuPath.Add(categoryPaths[0]).SubPath.Add(categoryPaths[1]).SubPath.Add(categoryPaths[2]).SubPath.Add(categoryPaths[3]).SubPath.Add(categoryPaths[4]).SubPath.Add(categoryPaths[5]).SubPath.Add(categoryPaths[6]).SubPath.Add(categoryPaths[7]);
                //menuPath.Add(categoryPath);                

                menuCollection.GetMulti(filter, menuPath);
            }
            catch (Exception ex)
            {
                if (TestUtil.IsPcDeveloper)
                {
                    Exception understandableError;

                    if (LLBLGenUtil.TryGetUnderstandableErrorForPrefetchError<EntityType>(ex, out understandableError))
                    {
                        // Don't ignore this, it's a 50% performance loss if you do!!!
                        throw understandableError;
                    }
                    else
                        throw;
                }
                else
                    menuCollection.GetMulti(filter);
            }

            if (menuCollection.Count > 0)
                menuEntity = menuCollection[0];

            if (!loadInvisibleProductsAndCategories)
            {
                // GK Design Choice: Do it in memory, because I thought SQL query might get quite a bit slower
                // with so many call with filters on non-indexed fields. I might be wrong, so if you have 
                // performance issues, that's something to look into.
                MenuHelper.RemoveInvisibleProductsAndCategories(menuEntity.CategoryCollection);
            }

            return menuEntity;
        }

        private static void RemoveInvisibleProductsAndCategories(CategoryCollection categories)
        {
            for (int iCategory = categories.Count - 1; iCategory >= 0; iCategory--)
            {
                CategoryEntity category = categories[iCategory];

                if (category.VisibilityType == VisibilityType.Never)
                    categories.RemoveAt(iCategory);
                else
                {
                    // Check it's childeren.
                    if (category.ChildCategoryCollection.Count > 0)
                        MenuHelper.RemoveInvisibleProductsAndCategories(category.ChildCategoryCollection);

                    // Validate products in this category
                    if (category.ProductCategoryCollection.Count > 0)
                    {
                        for (int iProduct = category.ProductCategoryCollection.Count - 1; iProduct >= 0; iProduct--)
                        {
                            ProductCategoryEntity productCategory = category.ProductCategoryCollection[iProduct];
                            if (productCategory.ProductEntity.VisibilityType == VisibilityType.Never)
                            {
                                category.ProductCategoryCollection.RemoveAt(iProduct);
                            }
                        }
                    }
                }
            }
        }
    }
}
