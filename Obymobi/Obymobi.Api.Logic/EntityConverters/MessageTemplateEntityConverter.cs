﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class MessageTemplateEntityConverter : EntityConverterBase<MessageTemplateEntity, MessageTemplate>
    {
        public override MessageTemplate ConvertEntityToModel(MessageTemplateEntity entity)
        {
            MessageTemplate model = new MessageTemplate();
            model.MessageTemplateId = entity.MessageTemplateId;
            model.Name = entity.Name;
            model.Title = entity.Title;
            model.Message = entity.Message;
            model.Duration = entity.Duration;
            model.MediaId = entity.MediaId.HasValue ? entity.MediaId.Value : 0;
            model.CategoryId = entity.CategoryId.HasValue ? entity.CategoryId.Value : 0;
            model.EntertainmentId = entity.EntertainmentId.HasValue ? entity.EntertainmentId.Value : 0;
            model.ProductId = entity.ProductId.HasValue ? entity.ProductId.Value : 0;
            model.MessageLayoutType = (int)entity.MessageLayoutType;

            return model;
        }
    }
}
