﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class UIFooterItemEntityConverter : EntityConverterBase<UIFooterItemEntity, UIFooterItem>
    {
        public override UIFooterItem ConvertEntityToModel(UIFooterItemEntity entity)
        {
            UIFooterItem model = new UIFooterItem();
            model.UIFooterItemId = entity.UIFooterItemId;
            model.Name = entity.Name;
            model.Position = entity.Position;
            model.Type = entity.Type;
            model.SortOrder = entity.SortOrder;
            model.ActionIntent = entity.ActionIntent.GetValueOrDefault(-1);
            model.Visible = entity.Visible;

            // Custom texts
            model.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToModelArray(entity.CustomTextCollection);

            // Media
            List<Obymobi.Logic.Model.Media> media = new List<Obymobi.Logic.Model.Media>();
            foreach (MediaEntity mediaEntity in entity.MediaCollection)
            {
                media.AddRange(new MediaEntityConverter().ConvertEntityToModel(mediaEntity, "UIMODES_UIFOOTERITEMS", "UIMODES_UIFOOTERITEMS_MEDIA"));
            }
            model.Media = media.ToArray();

            return model;          
        }
    }
}
