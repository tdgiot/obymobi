﻿using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class CompanyCultureEntityConverter : EntityConverterBase<CompanyCultureEntity, CompanyCulture>
    {
        public override CompanyCulture ConvertEntityToModel(CompanyCultureEntity entity)
        {
            CompanyCulture model = new CompanyCulture();
            model.CompanyCultureId = entity.CompanyCultureId;
            model.CultureCode = entity.CultureCode;

            if (!entity.CultureCode.IsNullOrWhiteSpace())
            {
                model.Name = Culture.Mappings[entity.CultureCode].Language.Name;
            }

            return model;
        }

        public override CompanyCulture[] ConvertEntityCollectionToModelArray(IEnumerable<CompanyCultureEntity> entityCollection)
        {
            List<CompanyCulture> toReturn = new List<CompanyCulture>();
            foreach (CompanyCultureEntity companyCultureEntity in entityCollection)
            {
                toReturn.Add(this.ConvertEntityToModel(companyCultureEntity));
            }
            return toReturn.OrderBy(x => x.Name).ToArray();
        }
    }
}
