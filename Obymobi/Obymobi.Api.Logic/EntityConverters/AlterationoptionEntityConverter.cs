﻿using Dionysos;
using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using System.Collections.Generic;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class AlterationoptionEntityConverter : EntityConverterBase<AlterationoptionEntity, AlterationitemCollection, Alterationoption>
    {
        private bool addPosData;
        private CompanyEntity companyEntity;

        public AlterationoptionEntityConverter(bool addPosData, CompanyEntity companyEntity)
        {
            this.addPosData = addPosData;
            this.companyEntity = companyEntity;
        }

        public override Alterationoption ConvertEntityToModel(AlterationoptionEntity entity)
        {
            Alterationoption model = new Alterationoption
            {
                AlterationoptionId = entity.AlterationoptionId,
                Name = entity.FriendlyName.IsNullOrWhiteSpace() ? entity.Name : entity.FriendlyName,
                PriceIn = entity.PriceIn ?? 0,
                Description = entity.Description,
                IsProductRelated = entity.IsProductRelated,
                CustomTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection)
            };

            if (entity.PosproductId.HasValue)
            {
                model.PosproductId = entity.PosproductId.Value;
            }

            if (addPosData && entity.PosalterationoptionId.HasValue)
            {
                model.Posalterationoption = Obymobi.Logic.HelperClasses.PosalterationoptionHelper.CreatePosalterationoptionModelFromEntity(entity.PosalterationoptionEntity);
            }

            return model;
        }

        public override Alterationoption[] ConvertEntityCollectionToModelArray(AlterationitemCollection entityCollection)
        {
            List<Alterationoption> alterationoptions = new List<Alterationoption>();

            entityCollection.Sort((int)AlterationitemFieldIndex.SortOrder, System.ComponentModel.ListSortDirection.Ascending);
            for (int i = 0; i < entityCollection.Count; i++)
            {
                AlterationoptionEntity alterationoption = entityCollection[i].AlterationoptionEntity;
                alterationoptions.Add(this.ConvertEntityToModel(entityCollection[i].AlterationoptionEntity));
            }

            return alterationoptions.ToArray();
        }
    }
}
