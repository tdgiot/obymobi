﻿using Obymobi.Logic.Model.Mobile;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public abstract class EntityConverterBaseWithParent<EntityType, ModelType> 
        where EntityType : IEntity 
    {
        #region Entity to model conversion

        public abstract ModelType ConvertEntityToModel(EntityType entity, string parent = "", string mediaParent = "");

        public virtual ModelType[] ConvertEntityCollectionToModelArray(IEnumerable<EntityType> entityCollection, string parent = "", string mediaParent = "")
        {
            List<ModelType> modelList = new List<ModelType>();

            if (entityCollection != null && entityCollection.Any())
            {
                foreach (EntityType entity in entityCollection)
                {
                    if (entity != null)
                    {
                        ModelType model = this.ConvertEntityToModel(entity, parent, mediaParent);
                        if (model != null)
                            modelList.Add(model);
                    }
                }
            }

            return modelList.ToArray();
        }

        #endregion
    }
}
