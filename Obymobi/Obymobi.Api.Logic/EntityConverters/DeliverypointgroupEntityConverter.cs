﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class DeliverypointgroupEntityConverter : EntityConverterBase<DeliverypointgroupEntity, Deliverypointgroup>
    {
        public override Deliverypointgroup ConvertEntityToModel(DeliverypointgroupEntity entity)
        {
            Deliverypointgroup model = new Deliverypointgroup();
            model.DeliverypointgroupId = entity.DeliverypointgroupId;
            model.CompanyId = entity.CompanyId;
            model.Name = entity.Name;
            model.OrderHistoryDialogEnabled = entity.OrderHistoryDialogEnabled;
            model.PmsIntegration = entity.PmsIntegration;
            model.PmsLockClientWhenNotCheckedIn = entity.PmsLockClientWhenNotCheckedIn;
            model.PmsAllowShowBill = entity.PmsAllowShowBill;
            model.PmsAllowExpressCheckout = entity.PmsAllowExpressCheckout;
            model.PmsAllowShowGuestName = entity.PmsAllowShowGuestName;
            model.DeliverypointCaption = entity.DeliverypointCaption;
            model.UseHardKeyboard = (int)entity.UseHardKeyboard;
            model.UIModeId = entity.UIModeId.GetValueOrDefault(0);
            model.ApiVersion = entity.ApiVersion;

            return model;
        }
    }
}
