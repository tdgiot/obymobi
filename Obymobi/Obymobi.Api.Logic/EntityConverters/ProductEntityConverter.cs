using Dionysos;
using Dionysos.Data.LLBLGen;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using System.Collections.Generic;
using System.Linq;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class ProductEntityConverter : EntityConverterBase<ProductEntity, List<Product>>
    {
        private readonly bool createInstanceForEachProductCategoryEntity;
        private bool addPosData;
        private int? menuId = null;

        public ProductEntityConverter(bool createInstanceForEachProductCategoryEntity, bool addPosData, int? menuId = null)
        {
            this.createInstanceForEachProductCategoryEntity = createInstanceForEachProductCategoryEntity;
            this.addPosData = addPosData;
            this.menuId = menuId;
        }

        public override List<Product> ConvertEntityToModel(ProductEntity entity)
        {
            List<Product> products = new List<Product>();

            AlterationEntityConverter alterationEntityConverter = new AlterationEntityConverter(this.addPosData, entity.CompanyEntity);

            // Add multiple instances if required
            if (createInstanceForEachProductCategoryEntity)
            {
                // Create an instance for each ProductCategory entity (each category it belongs to)
                IOrderedEnumerable<ProductCategoryEntity> sorted = entity.ProductCategoryCollection.OrderBy(pc => pc.SortOrder);
                foreach (ProductCategoryEntity productCategoryEntity in sorted)
                {
                    if (!menuId.HasValue || productCategoryEntity.CategoryEntity.MenuId == menuId.Value)
                    {
                        Product product = new Product();
                        product.ProductId = entity.ProductId;
                        product.Name = entity.Name;
                        product.Description = entity.Description;
                        product.Type = entity.Type;
                        product.SubType = entity.SubType;
                        product.TextColor = entity.TextColor;
                        product.BackgroundColor = entity.BackgroundColor;
                        product.WebTypeSmartphoneUrl = entity.WebTypeSmartphoneUrl;
                        product.WebTypeTabletUrl = entity.WebTypeTabletUrl;
                        product.HidePrice = entity.HidePrice;
                        product.PriceIn = entity.PriceIn ?? 0;
                        product.VisibilityType = (int)entity.VisibilityType;
                        product.GenericproductId = entity.GenericproductId ?? -1;
                        product.BrandProductId = entity.BrandProductId ?? -1;

                        if (entity.GenericproductId.HasValue)
                        {
                            if (entity.IsLinkedToGeneric || !entity.OverrideSubType)
                            {
                                // Use subtype from generic product if product is linked to brand product or not overriding subtype
                                product.SubType = entity.GenericproductEntity.SubType;
                            }

                            bool useDefault = true;
                            bool nameEmpty = string.IsNullOrEmpty(entity.Name) || entity.IsLinkedToGeneric; // Use generic product name if linked
                            bool useGenericproductDescription = !entity.ManualDescriptionEnabled;

                            // Otoucho compatibility
                            if (entity.CompanyEntity.SystemType == SystemType.Otoucho && (nameEmpty || useGenericproductDescription))
                            {
                                // Walk through the translations of the generic product
                                Dictionary<CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(entity.GenericproductEntity.CustomTextCollection, Obymobi.Culture.Dutch_Netherlands.Code);

                                if (nameEmpty)
                                {
                                    product.Name = customTexts.GetValueOrDefault(CustomTextType.ProductName);
                                    nameEmpty = false;
                                }

                                if (useGenericproductDescription)
                                    product.Description = customTexts.GetValueOrDefault(CustomTextType.ProductDescription);

                                useDefault = false;
                            }

                            if (useDefault)
                            {
                                if (nameEmpty)
                                    product.Name = entity.GenericproductEntity.Name;

                                if (useGenericproductDescription)
                                    product.Description = entity.GenericproductEntity.Description;
                            }

                            if (!entity.PriceIn.HasValue)
                            {
                                product.PriceIn = entity.GenericproductEntity.PriceIn ?? 0;
                            }

                            if (string.IsNullOrEmpty(entity.TextColor))
                            {
                                product.TextColor = entity.GenericproductEntity.TextColor;
                            }
                            if (string.IsNullOrEmpty(entity.BackgroundColor))
                            {
                                product.BackgroundColor = entity.GenericproductEntity.BackgroundColor;
                            }

                            if (entity.WebTypeSmartphoneUrl.IsNullOrWhiteSpace())
                            {
                                product.WebTypeSmartphoneUrl = entity.GenericproductEntity.WebTypeSmartphoneUrl;
                            }
                            if (entity.WebTypeTabletUrl.IsNullOrWhiteSpace())
                            {
                                product.WebTypeTabletUrl = entity.GenericproductEntity.WebTypeTabletUrl;
                            }
                        }

                        product.CategoryId = productCategoryEntity.CategoryId;
                        product.SortOrder = productCategoryEntity.SortOrder + (10000 * productCategoryEntity.CategoryEntity.SortOrder);
                        product.DisplayOnHomepage = (entity.DisplayOnHomepage.HasValue && entity.DisplayOnHomepage.Value);

                        // AllowFreeText
                        if (entity.AllowFreeText.HasValue)
                            product.AllowFreeText = entity.AllowFreeText.Value;
                        else
                            product.AllowFreeText = (CategoryHelper.HasAllowFreeTextParent(productCategoryEntity.CategoryEntity) ?? false); // TODO Optimize: This is not prefetched

                        // Schedule
                        if (entity.ScheduleId.HasValue)
                            product.ScheduleId = entity.ScheduleId.Value;
                        else
                            product.ScheduleId = CategoryHelper.GetScheduleIdFromParent(productCategoryEntity.CategoryEntity);

                        product.Type = CategoryHelper.GetTypeFromParent(productCategoryEntity.CategoryEntity); // TODO Optimize: This is not prefetched

                        if (entity.Color > 0)
                            product.Color = entity.Color;
                        else
                            product.Color = Obymobi.Logic.HelperClasses.CategoryHelper.GetColorFromParent(productCategoryEntity.CategoryEntity);

                        // View layout type
                        if (entity.GenericproductId.HasValue && entity.GenericproductEntity.ViewLayoutType > 0)
                            product.ViewLayoutType = entity.GenericproductEntity.ViewLayoutType;
                        else if (entity.ViewLayoutType > 0)
                            product.ViewLayoutType = (int)entity.ViewLayoutType;
                        else
                            product.ViewLayoutType = CategoryHelper.GetViewLayoutTypeFromParent(productCategoryEntity.CategoryEntity);

                        // Delivery location
                        if (entity.DeliveryLocationType > 0)
                            product.DeliveryLocationType = (int)entity.DeliveryLocationType;
                        else
                            product.DeliveryLocationType = CategoryHelper.GetDeliveryLocationTypeFromParent(productCategoryEntity.CategoryEntity);

                        // Product button
                        if (!entity.ButtonText.IsNullOrWhiteSpace())
                            product.ButtonText = entity.ButtonText;
                        else
                            product.ButtonText = Obymobi.Logic.HelperClasses.CategoryHelper.GetButtonTextFromParent(productCategoryEntity.CategoryEntity);

                        // Customize button
                        if (!entity.CustomizeButtonText.IsNullOrWhiteSpace())
                            product.CustomizeButtonText = entity.CustomizeButtonText;
                        else
                            product.CustomizeButtonText = Obymobi.Logic.HelperClasses.CategoryHelper.GetCustomizeButtonTextFromParent(productCategoryEntity.CategoryEntity);

                        // Alterations > Based on product, otherwise category
                        List<Alteration> alterations = new List<Alteration>();
                        if (entity.ProductAlterationCollection.Count > 0)
                        {
                            // Get alterations from product
                            List<ProductAlterationEntity> productAlterations = entity.ProductAlterationCollection.OrderBy(pa => pa.SortOrder).ToList();
                            for (int i = 0; i < productAlterations.Count; i++)
                            {
                                if (productAlterations[i].Version > 1)
                                    continue;

                                List<Alteration> alterationsWithChildren = AlterationHelper.CreateAlterationWithChildrenFromEntity(productAlterations[i].AlterationEntity, addPosData, entity.CompanyEntity);
                                foreach (Alteration alteration in alterationsWithChildren)
                                {
                                    if ((alteration.Type == (int)AlterationType.Options && alteration.Alterationoptions != null && alteration.Alterationoptions.Length > 0) || alteration.Type != (int)AlterationType.Options)
                                    {
                                        alterations.Add(alteration);
                                    }
                                }
                            }
                        }
                        else
                        {
                            // Get alterations from category
                            ProductHelper.AddAlterationsFromCategory(alterations, productCategoryEntity.CategoryEntity, addPosData);
                        }
                        product.Alterations = alterations.ToArray();

                        // Product suggestions > Based on product, otherwise category
                        List<ProductSuggestion> productSuggestions = new List<ProductSuggestion>();
                        if (productCategoryEntity.ProductCategorySuggestionCollection.Count > 0)
                        {
                            // Retrieve from product								
                            for (int i = 0; i < productCategoryEntity.ProductCategorySuggestionCollection.Count; i++)
                            {
                                ProductSuggestion productSuggestion = ProductSuggestionHelper.CreateProductSuggestionModelFromEntity(productCategoryEntity.ProductCategorySuggestionCollection[i]);
                                if (productSuggestion != null)
                                {
                                    productSuggestions.Add(productSuggestion);
                                }
                            }
                        }

                        // Always try to populate all buttons in Otoucho:
                        if (productSuggestions.Count < 4)
                        {
                            // Retrieve from category						
                            CategoryEntity category = productCategoryEntity.CategoryEntity;
                            for (int i = 0; i < category.CategorySuggestionCollection.Count; i++) // TODO Optimize: This is not prefetched
                            {
                                ProductSuggestion productSuggestion = ProductSuggestionHelper.CreateProductSuggestionModelFromEntity(productCategoryEntity, category.CategorySuggestionCollection[i]);
                                if (productSuggestion != null)
                                {
                                    productSuggestions.Add(productSuggestion);

                                    if (productSuggestions.Count >= 4)
                                        break;
                                }
                            }
                        }

                        // Always try to populate all buttons in Otoucho:
                        if (productSuggestions.Count < 4 && productCategoryEntity.CategoryEntity.ParentCategoryId.HasValue)
                        {
                            // Retrieve from category						
                            CategoryEntity category = productCategoryEntity.CategoryEntity.ParentCategoryEntity;
                            for (int i = 0; i < category.CategorySuggestionCollection.Count; i++)
                            {
                                ProductSuggestion productSuggestion = ProductSuggestionHelper.CreateProductSuggestionModelFromEntity(productCategoryEntity, category.CategorySuggestionCollection[i]);
                                if (productSuggestion != null)
                                {
                                    productSuggestions.Add(productSuggestion);

                                    if (productSuggestions.Count >= 4)
                                        break;
                                }
                            }
                        }

                        product.ProductSuggestions = productSuggestions.ToArray();
                        product.Media = ProductEntityConverter.CreateMediaModelsForProductEntity(entity).ToArray();
                        product.Attachments = ProductEntityConverter.CreateAttachmentModelsForProductEntity(entity).ToArray();
                        product.CustomTexts = ProductHelper.CreateCustomTextModelsForProductEntity(entity, productCategoryEntity);

                        products.Add(product);
                    }
                }
            }

            // Check if we have added any, if none it's not in any category and will be created without a parent productmenuitem
            if (products.Count == 0)
            {
                Product product = new Product();
                product.ProductId = entity.ProductId;
                product.Name = entity.Name;
                product.Description = entity.Description;
                product.Type = entity.Type;
                product.CategoryId = -1;
                product.TextColor = entity.TextColor;
                product.BackgroundColor = entity.BackgroundColor;
                product.AllowFreeText = entity.AllowFreeText.HasValue && entity.AllowFreeText.Value;
                product.Color = entity.Color;
                product.ButtonText = entity.ButtonText;
                product.WebTypeSmartphoneUrl = entity.WebTypeSmartphoneUrl;
                product.WebTypeTabletUrl = entity.WebTypeTabletUrl;
                product.HidePrice = entity.HidePrice;
                product.VisibilityType = (int)entity.VisibilityType;
                product.CustomizeButtonText = entity.CustomizeButtonText;

                if (entity.GenericproductId.HasValue)
                {
                    bool nameEmpty = string.IsNullOrEmpty(entity.Name);
                    bool useGenericproductDescription = !entity.ManualDescriptionEnabled;

                    // Otoucho compatibility
                    if (entity.CompanyEntity.SystemType == SystemType.Otoucho && (nameEmpty || useGenericproductDescription))
                    {
                        CustomTextEntity genericproductName = entity.GenericproductEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == "nl-NL" && x.Type == CustomTextType.GenericproductName);
                        CustomTextEntity genericproductDescription = entity.GenericproductEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == "nl-NL" && x.Type == CustomTextType.GenericproductDescription);

                        if (genericproductName != null && nameEmpty)
                        {
                            product.Name = genericproductName.Text;
                        }

                        if (genericproductDescription != null && useGenericproductDescription)
                        {
                            product.Description = genericproductDescription.Text;
                        }
                    }

                    if (product.Name.IsNullOrWhiteSpace())
                    {
                        product.Name = entity.GenericproductEntity.Name;
                    }

                    if (product.Description.IsNullOrWhiteSpace() && useGenericproductDescription)
                    {
                        product.Description = entity.GenericproductEntity.Description;
                    }

                    if (string.IsNullOrEmpty(entity.TextColor))
                        product.TextColor = entity.GenericproductEntity.TextColor;
                    if (string.IsNullOrEmpty(entity.BackgroundColor))
                        product.BackgroundColor = entity.GenericproductEntity.BackgroundColor;

                }

                product.PriceIn = entity.PriceIn ?? 0;
                product.DisplayOnHomepage = (entity.DisplayOnHomepage.HasValue && entity.DisplayOnHomepage.Value);
                product.Rateable = false;
                product.Type = 0;

                // Alterations > Based on product, otherwise category
                List<Alteration> alterations = new List<Alteration>();
                if (entity.ProductAlterationCollection.Count > 0)
                {
                    // Get alterations from product
                    for (int i = 0; i < entity.ProductAlterationCollection.Count; i++)
                    {
                        if (entity.ProductAlterationCollection[i].Version > 1)
                            continue;

                        Alteration alteration = alterationEntityConverter.ConvertEntityToModel(entity.ProductAlterationCollection[i].AlterationEntity);
                        if ((alteration.Type == (int)AlterationType.Options && alteration.Alterationoptions != null && alteration.Alterationoptions.Length > 0) || alteration.Type != (int)AlterationType.Options)
                            alterations.Add(alteration);
                    }
                }
                product.Alterations = alterations.ToArray();

                // Product suggestions > Based on product, otherwise category
                List<ProductSuggestion> productSuggestions = new List<ProductSuggestion>();
                if (entity.ProductSuggestionCollection.Count > 0)
                {
                    // Retrieve from product								
                    for (int i = 0; i < entity.ProductSuggestionCollection.Count; i++)
                    {
                        ProductSuggestion productSuggestion = ProductSuggestionHelper.CreateProductSuggestionModelFromEntity(entity.ProductSuggestionCollection[i], menuId);
                        if (productSuggestion != null)
                            productSuggestions.Add(productSuggestion);
                    }
                }

                product.ProductSuggestions = productSuggestions.ToArray();

                if (addPosData && entity.PosproductId.HasValue)
                {
                    product.Posproduct = Obymobi.Logic.HelperClasses.PosproductHelper.CreatePosproductModelFromEntity(entity.PosproductEntity, false);
                }

                product.Media = ProductEntityConverter.CreateMediaModelsForProductEntity(entity).ToArray();
                product.Attachments = ProductEntityConverter.CreateAttachmentModelsForProductEntity(entity).ToArray();
                product.CustomTexts = ProductHelper.CreateCustomTextModelsForProductEntity(entity);

                products.Add(product);
            }

            return products;
        }

        private static IEnumerable<Attachment> CreateAttachmentModelsForProductEntity(ProductEntity entity)
        {
            List<Attachment> models = new List<Attachment>();

            if (entity.BrandProductId.HasValue && entity.InheritAttachmentsFromBrand)
            {
                models.AddRange(AttachmentHelper.CreateAttachmentModels(entity.ProductAttachmentCollection.Select(x => x.AttachmentEntity).ToEntityCollection<AttachmentCollection>()));
            }
            else
            {
                models.AddRange(AttachmentHelper.CreateAttachmentModelsForProduct(entity));
            }

            return models;
        }

        private static IEnumerable<Media> CreateMediaModelsForProductEntity(ProductEntity entity)
        {
            MediaCollection mediaCollection = new MediaCollection();

            if (entity.BrandProductId.HasValue && entity.InheritMedia)
            {
                mediaCollection.AddRange(entity.MediaRelationshipCollection.Select(x => x.MediaEntity));
            }
            else if (entity.MediaCollection.Count > 0)
            {
                mediaCollection.AddRange(entity.MediaCollection);
            }
            else if (entity.GenericproductId.HasValue && entity.GenericproductEntity.MediaCollection.Count > 0)
            {
                mediaCollection.AddRange(entity.GenericproductEntity.MediaCollection.Where(x => !x.FromSupplier));
            }

            return new MediaEntityConverter().ConvertEntityCollectionToModels(mediaCollection);
        }
    }
}
