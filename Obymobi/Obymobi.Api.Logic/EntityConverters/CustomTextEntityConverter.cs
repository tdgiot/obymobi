﻿using MarkdownSharp;
using MarkdownSharp.Extensions;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class CustomTextEntityConverter : EntityConverterBaseWithParent<CustomTextEntity, CustomText>
    {
        public override CustomText ConvertEntityToModel(CustomTextEntity entity, string parent = "", string mediaParent = "")
        {
            CustomText model = new CustomText();
            model.CustomTextId = entity.CustomTextId;
            model.CultureCode = entity.CultureCode;
            model.Type = (int)entity.Type;
            model.ForeignKey = entity.RelatedEntityId;
            model.Parent = parent;

            Culture culture;
            if (Obymobi.Culture.Mappings.TryGetValue(entity.CultureCode, out culture))
            {
                model.LanguageCode = culture.Language.CodeAlpha2.ToUpperInvariant();
            }

            if (!this.ShouldApplyMarkDown(entity))
            {
                model.Text = entity.Text;
            }
            else
            {
                model.Text = this.ApplyMarkDown(entity);
            }

            return model;
        }

        private bool ShouldApplyMarkDown(CustomTextEntity entity)
        {
            return entity.AlterationId.HasValue && entity.Type == CustomTextType.AlterationDescription && entity.AlterationEntity.Type == AlterationType.Instruction;
        }

        private string ApplyMarkDown(CustomTextEntity entity)
        {
            Markdown markdown = MarkdownHelper.GetDefaultInstance();
            string toReturn = markdown.Transform(entity.Text);

            if (toReturn.EndsWith("\n"))
                toReturn = toReturn.Substring(0, toReturn.Length - 1);

            return toReturn;
        }
    }
}
