﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class EntertainmenturlEntityConverter : EntityConverterBase<EntertainmenturlEntity, Entertainmenturl>
    {
        public override Entertainmenturl ConvertEntityToModel(EntertainmenturlEntity entity)
        {
            Entertainmenturl model = new Entertainmenturl
            {
                EntertainmenturlId = entity.EntertainmenturlId,
                EntertainmentId = entity.EntertainmentId,
                Url = entity.Url,
                AccessFullDomain = entity.AccessFullDomain,
                InitialZoom = entity.InitialZoom.HasValue ? entity.InitialZoom.Value : 0,
            };

            return model;
        }
    }
}
