﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class PriceLevelItemEntityConverter : EntityConverterBase<PriceLevelItemEntity, PriceLevelItem>
    {
        public override PriceLevelItem ConvertEntityToModel(PriceLevelItemEntity entity)
        {
            PriceLevelItem model = new PriceLevelItem
            {
                PriceLevelItemId = entity.PriceLevelItemId,
                Price = entity.Price,
                ProductId = entity.ProductId ?? -1,
                AlterationoptionId = entity.AlterationoptionId ?? -1
            };

            return model;
        }
    }
}
