﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class MessagegroupEntityConverter : EntityConverterBase<MessagegroupEntity,  Messagegroup>
    {
        public override Messagegroup ConvertEntityToModel(MessagegroupEntity entity)
        {
            Messagegroup model = new Messagegroup
            {
                MessagegroupId = entity.MessagegroupId,
                Name = entity.Name,
                Type = (int)entity.Type,
                GroupName = entity.GroupName
            };

            List<int> deliverypointNumbers = new List<int>();
            foreach (MessagegroupDeliverypointEntity messagegroupDeliverypoint in entity.MessagegroupDeliverypointCollection)
            {
                int deliverypointNumber;
                if (int.TryParse(messagegroupDeliverypoint.DeliverypointEntity.Number, out deliverypointNumber))
                {
                    deliverypointNumbers.Add(deliverypointNumber);
                }
            }
            model.DeliverypointNumbers = deliverypointNumbers.ToArray();

            return model;
        }
    }
}
