﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class EntertainmentEntityConverter : EntityConverterBase<EntertainmentEntity, Entertainment>
    {
        private int deliverypointgroupId;

        public EntertainmentEntityConverter(int deliverypointgroupId)
        {
            this.deliverypointgroupId = deliverypointgroupId;
        }

        public override Entertainment ConvertEntityToModel(EntertainmentEntity entity)
        {
            Entertainment model = new Entertainment
            {
                EntertainmentId = entity.EntertainmentId,
                CompanyId = -1,
                Name = entity.Name,
                PackageName = entity.PackageName,
                ClassName = entity.ClassName,
                TextColor = entity.TextColor,
                BackgroundColor = entity.BackgroundColor,
                SurveyName = entity.SurveyName,
                FormName = entity.FormName,
                Description = entity.Description,
                BackButton = entity.BackButton,
                AnnouncementAction = entity.AnnouncementAction,
                PreventCaching = entity.PreventCaching,
                PostfixWithTimestamp = entity.PostfixWithTimestamp,
                MenuContainer = entity.MenuContainer,
                NavigationBar = entity.NavigationBar,
                NavigateButton = entity.NavigateButton,
                HomeButton = entity.HomeButton,
                TitleAsHeader = entity.TitleAsHeader,
                RestrictedAccess = entity.RestrictedAccess,
                EntertainmentType = (int)entity.EntertainmentType,
                AppDataClearInterval = (int)entity.AppDataClearInterval,
                AppCloseInterval = (int)entity.AppCloseInterval,
                SiteId = (entity.SiteId.HasValue ? entity.SiteId.Value : -1)
            };

            if (entity.EntertainmentcategoryId.HasValue)
                model.EntertainmentcategoryId = entity.EntertainmentcategoryId.Value;

            if (entity.EntertainmenturlEntity != null)
                model.DefaultEntertainmenturl = entity.EntertainmenturlEntity.Url;

            model.SocialmediaType = entity.SocialmediaType.HasValue ? entity.SocialmediaType.Value : 0;

            // Media
            List<Obymobi.Logic.Model.Media> media = new List<Obymobi.Logic.Model.Media>();
            for (int i = 0; i < entity.MediaCollection.Count; i++)
            {
                media.AddRange(Obymobi.Logic.HelperClasses.MediaHelper.CreateMediaModelFromEntity(entity.MediaCollection[i]));
            }
            model.Media = media.ToArray();

            // Entertainment urls
            model.Entertainmenturls = new EntertainmenturlEntityConverter().ConvertEntityCollectionToModelArray(entity.EntertainmenturlCollection);

            if (this.deliverypointgroupId > 0)
            {
                foreach (DeliverypointgroupEntertainmentEntity dpgEntertainment in entity.DeliverypointgroupEntertainmentCollection)
                {
                    if (dpgEntertainment.DeliverypointgroupId == this.deliverypointgroupId)
                    {
                        model.SortOrder = dpgEntertainment.SortOrder;
                        break;
                    }
                }
            }

            if (entity.EntertainmentFileId.HasValue)
            {
                model.Filename = entity.EntertainmentFileEntity.Filename;
            }
            model.FileVersion = entity.EntertainmentFileVersion;

            return model;
        }
    }
}
