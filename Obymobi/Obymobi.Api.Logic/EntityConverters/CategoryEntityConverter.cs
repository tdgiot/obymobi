﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class CategoryEntityConverter : EntityConverterBase<CategoryEntity, Category>
    {
        private readonly bool addPosData;

        public CategoryEntityConverter(bool addPosData)
        {
            this.addPosData = addPosData;
        }

        public override Category ConvertEntityToModel(CategoryEntity entity)
        {
            Category model = new Category
            {
                CategoryId = entity.CategoryId,
                ParentCategoryId = entity.ParentCategoryId.HasValue ? entity.ParentCategoryId.Value : -1,
                GenericcategoryId = entity.GenericcategoryId.HasValue ? entity.GenericcategoryId.Value : -1,
                Name = entity.Name,
                SortOrder = entity.SortOrder,
                AnnouncementAction = entity.AnnouncementAction,
                HidePrices = entity.HidePrices,
                Description = entity.Description,
                VisibilityType = (int)entity.VisibilityType,
                ScheduleId = entity.ScheduleId ?? 0
            };

            // Media
            if (entity.MediaCollection.Count > 0)
            {
                List<Obymobi.Logic.Model.Media> media = new List<Obymobi.Logic.Model.Media>();
                for (int i = 0; i < entity.MediaCollection.Count; i++)
                {
                    media.AddRange(Obymobi.Logic.HelperClasses.MediaHelper.CreateMediaModelFromEntity(entity.MediaCollection[i]));
                }
                model.Media = media.ToArray();
            }
            else if (entity.GenericcategoryId.HasValue)
            {
                List<Obymobi.Logic.Model.Media> media = new List<Obymobi.Logic.Model.Media>();
                for (int i = 0; i < entity.GenericcategoryEntity.MediaCollection.Count; i++)
                {
                    media.AddRange(Obymobi.Logic.HelperClasses.MediaHelper.CreateMediaModelFromEntity(entity.GenericcategoryEntity.MediaCollection[i]));
                }
                model.Media = media.ToArray();
            }

            // Products
            List<int> containedProductIds = new List<int>();
            foreach (var productCategory in entity.ProductCategoryCollection)
            {
                containedProductIds.Add(productCategory.ProductId);
            }
            model.ContainedProductIds = containedProductIds.ToArray();

            // Custom texts
            model.CustomTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection);

            // Related Product
            if (entity.ProductId.HasValue)
            {
                model.ProductId = entity.ProductId.Value;
            }

            if (this.addPosData && entity.PoscategoryId.HasValue)
            {
                model.Poscategory = Obymobi.Logic.HelperClasses.PoscategoryHelper.CreatePoscategoryModelFromEntity(entity);
            }

            return model;
        }
    }
}
