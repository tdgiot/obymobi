﻿using Obymobi.Logic.Model.Mobile;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public abstract class MobileEntityConverterBase<EntityType, EntityCollectionType, MobileModelType, ModelType> where EntityType : IEntity where EntityCollectionType : IEntityCollection where MobileModelType : ModelBase where ModelType : Obymobi.Logic.Model.ModelBase
    {
        #region Entity to mobile model conversion

        public abstract MobileModelType ConvertEntityToMobileModel(EntityType entity);

        public virtual MobileModelType[] ConvertEntityCollectionToMobileModelArray(EntityCollectionType entityCollection)
        {
            List<MobileModelType> modelList = new List<MobileModelType>();

            if (entityCollection != null && entityCollection.Count > 0)
            {
                foreach (EntityType entity in entityCollection)
                {
                    if (entity != null)
                    {
                        MobileModelType model = this.ConvertEntityToMobileModel(entity);
                        if (model != null)
                            modelList.Add(model);
                    }
                }
            }

            return modelList.ToArray();
        }

        #endregion

        #region Mobile model to model conversion

        public abstract ModelType ConvertMobileModelToModel(MobileModelType mobileModel);

        public virtual ModelType[] ConvertMobileModelArrayToModelArray(MobileModelType[] mobileModels)
        {
            List<ModelType> modelList = new List<ModelType>();

            if (mobileModels != null && mobileModels.Length > 0)
            {
                foreach (MobileModelType mobileModel in mobileModels)
                {
                    if (mobileModel != null)
                    {
                        ModelType model = this.ConvertMobileModelToModel(mobileModel);
                        if (model != null)
                            modelList.Add(model);
                    }
                }
            }

            return modelList.ToArray();
        }

        #endregion
    }
}
