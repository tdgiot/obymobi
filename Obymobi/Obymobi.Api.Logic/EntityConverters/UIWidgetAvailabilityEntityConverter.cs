﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class UIWidgetAvailabilityEntityConverter : EntityConverterBase<UIWidgetAvailabilityEntity, UIWidgetAvailability>
    {
        public override UIWidgetAvailability ConvertEntityToModel(UIWidgetAvailabilityEntity entity)
        {
            UIWidgetAvailability model = new UIWidgetAvailability();
            model.UIWidgetAvailabilityId = entity.UIWidgetAvailabilityId;
            model.UIWidgetId = entity.UIWidgetId;
            model.AvailabilityId = entity.AvailabilityId;

            return model;
        }
    }
}
