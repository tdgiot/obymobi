﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class StationListEntityConverter : EntityConverterBase<StationListEntity, StationList>
    {
        private readonly StationEntityConverter stationEntityConverter = new StationEntityConverter();

        public override StationList ConvertEntityToModel(StationListEntity entity)
        {
            StationList model = new StationList
            {
                StationListId = entity.StationListId,
                Name = entity.Name
            };

            // Stations
            List<Station> stations = new List<Station>();
            if (entity.StationCollection.Count > 0)
            {
                foreach (StationEntity stationEntity in entity.StationCollection.OrderBy(s => s.SortOrder))
                {
                    stations.Add(this.stationEntityConverter.ConvertEntityToModel(stationEntity));
                }
            }
            model.Stations = stations.ToArray();

            return model;
        }
    }
}
