﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class AdvertisementEntityConverter : EntityConverterBase<AdvertisementEntity, Advertisement>
    {
        public override Advertisement ConvertEntityToModel(AdvertisementEntity entity)
        {
            Advertisement model = new Advertisement();
            model.AdvertisementId = entity.AdvertisementId;
            model.CompanyId = (entity.CompanyId.HasValue ? entity.CompanyId.Value : -1);
            model.Name = entity.Name;
            model.ProductId = (entity.ProductId.HasValue ? entity.ProductId.Value : -1);
            model.EntertainmentId = (entity.EntertainmentId.HasValue ? entity.EntertainmentId.Value : -1);
            model.Description = entity.Description;
            model.ActionCategoryId = (entity.ActionCategoryId.HasValue ? entity.ActionCategoryId.Value : -1);
            model.ActionEntertainmentId = (entity.ActionEntertainmentId.HasValue ? entity.ActionEntertainmentId.Value : -1);
            model.ActionEntertainmentCategoryId = (entity.ActionEntertainmentCategoryId.HasValue ? entity.ActionEntertainmentCategoryId.Value : -1);
            model.ActionUrl = entity.ActionUrl;
            model.ActionSiteId = (entity.ActionSiteId.HasValue ? entity.ActionSiteId.Value : -1);
            model.ActionPageId = (entity.ActionPageId.HasValue ? entity.ActionPageId.Value : -1);

            // Custom texts
            model.CustomTexts = CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection);

            // Media
            var media = new List<Obymobi.Logic.Model.Media>();
            for (int i = 0; i < entity.MediaCollection.Count; i++)
            {
                media.AddRange(MediaHelper.CreateMediaModelFromEntity(entity.MediaCollection[i]));
            }
            model.Media = media.ToArray();

            return model;
        }
    }
}
