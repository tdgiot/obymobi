﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class MediaCultureEntityConverter : EntityConverterBaseWithParent<MediaCultureEntity, MediaCulture>
    {
        public override MediaCulture ConvertEntityToModel(MediaCultureEntity entity, string parent = "", string mediaParent = "")
        {
            MediaCulture model = new MediaCulture();
            model.MediaCultureId = entity.MediaCultureId;
            model.MediaId = entity.MediaId;
            model.CultureCode = entity.CultureCode;
            model.Parent = parent;

            return model;
        }
    }
}
