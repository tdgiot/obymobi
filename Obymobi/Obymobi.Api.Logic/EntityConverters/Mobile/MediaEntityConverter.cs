﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class MediaEntityConverter : MobileEntityConverterBase<MediaEntity, MediaCollection, Obymobi.Logic.Model.Mobile.Media, Obymobi.Logic.Model.Media>
    {
        #region Fields

        private bool minified = false;
        private DeviceType deviceType = DeviceType.Unknown;
        private string parent = string.Empty;

        #endregion

        #region Constructors

        public MediaEntityConverter(bool minified, DeviceType deviceType, string parent = "")
        {
            this.minified = minified;
            this.deviceType = deviceType;
            this.parent = parent;
        }

        #endregion

        #region Methods

        public override Obymobi.Logic.Model.Mobile.Media ConvertEntityToMobileModel(MediaEntity entity)
        {
            throw new NotImplementedException();
        }

        public override Obymobi.Logic.Model.Mobile.Media[] ConvertEntityCollectionToMobileModelArray(MediaCollection entityCollection)
        {
            List<Obymobi.Logic.Model.Mobile.Media> modelList = new List<Obymobi.Logic.Model.Mobile.Media>();

            if (entityCollection != null && entityCollection.Count > 0)
            {
                MediaRatioTypeMediaEntityConverter mediaRatioTypeMediaEntityConverter = null;

                foreach (MediaEntity entity in entityCollection)
                {                    
                    if (entity != null)
                    {
                        mediaRatioTypeMediaEntityConverter = new MediaRatioTypeMediaEntityConverter(this.minified, entity, this.deviceType, this.parent);
                        modelList.AddRange(mediaRatioTypeMediaEntityConverter.ConvertEntityCollectionToMobileModelArray(entity.MediaRatioTypeMediaCollection));
                    }
                }
            }

            return modelList.ToArray();
        }

        //public static void CreateMediaDirectories(int companyId, DeviceType deviceType, params MediaType[] mediaTypes)
        //{
        //    // /api/Media
        //    string mediaPath = HttpContext.Current.Server.MapPath("~/Media");

        //    // /api/Media/199/iPhone
        //    string mediaDeviceTypePath = Path.Combine(mediaPath, companyId.ToString(), deviceType.ToString());

        //    string mediaDeviceTypeMediaTypePath = string.Empty;
        //    foreach (MediaType mediaType in mediaTypes)
        //    {
        //        // /api/Media/199/iPhone/ProductButton640x960
        //        mediaDeviceTypeMediaTypePath = Path.Combine(mediaDeviceTypePath, mediaType.ToString());

        //        if (!Directory.Exists(mediaDeviceTypeMediaTypePath))
        //            Directory.CreateDirectory(mediaDeviceTypeMediaTypePath);                
        //    }
        //}

        public override Obymobi.Logic.Model.Media ConvertMobileModelToModel(Obymobi.Logic.Model.Mobile.Media mobileModel)
        {
            var mediaRatioTypeMediaEntityConverter = new MediaRatioTypeMediaEntityConverter(this.minified, null, this.deviceType, this.parent);
            return mediaRatioTypeMediaEntityConverter.ConvertMobileModelToModel(mobileModel);
        }

        #endregion
    }
}
