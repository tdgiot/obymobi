﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class ProductSuggestionEntityConverter : MobileEntityConverterBase<ProductSuggestionEntity, ProductSuggestionCollection, ProductSuggestion, Obymobi.Logic.Model.ProductSuggestion>
    {
        public override ProductSuggestion ConvertEntityToMobileModel(ProductSuggestionEntity entity)
        {
            ProductSuggestion productSuggestion = new ProductSuggestion();
            productSuggestion.ProductId = entity.ProductId;
            productSuggestion.SuggestedProductId = entity.SuggestedProductId.HasValue ? entity.SuggestedProductId.Value : -1;
            productSuggestion.SortOrder = entity.SortOrder;
            productSuggestion.Checkout = entity.Checkout;

            return productSuggestion;
        }

        public override Obymobi.Logic.Model.ProductSuggestion ConvertMobileModelToModel(ProductSuggestion mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
