﻿using System;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using Dionysos;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class CompanyCurrencyEntityConverter : MobileEntityConverterBase<CompanyCurrencyEntity, CompanyCurrencyCollection, CompanyCurrency, Obymobi.Logic.Model.CompanyCurrency>
    {
        public override CompanyCurrency ConvertEntityToMobileModel(CompanyCurrencyEntity entity)
        {
            CompanyCurrency companyCurrency = new CompanyCurrency();
            companyCurrency.CompanyCurrencyId = entity.CompanyCurrencyId;
            companyCurrency.CurrencyCode = entity.CurrencyCode;

            if (!entity.CurrencyCode.IsNullOrWhiteSpace())
            {
                companyCurrency.Symbol = Currency.Mappings[entity.CurrencyCode].Symbol;
                companyCurrency.Name = Currency.Mappings[entity.CurrencyCode].Name;
            }

            companyCurrency.ExchangeRate = entity.ExchangeRate;            

            return companyCurrency;
        }

        public override Obymobi.Logic.Model.CompanyCurrency ConvertMobileModelToModel(CompanyCurrency mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
