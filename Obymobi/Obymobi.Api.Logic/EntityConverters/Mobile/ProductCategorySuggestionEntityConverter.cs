﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class ProductCategorySuggestionEntityConverter : MobileEntityConverterBase<ProductCategorySuggestionEntity, ProductCategorySuggestionCollection, ProductSuggestion, Obymobi.Logic.Model.ProductSuggestion>
    {
        public override ProductSuggestion ConvertEntityToMobileModel(ProductCategorySuggestionEntity entity)
        {
            ProductSuggestion productSuggestion = new ProductSuggestion();
            productSuggestion.ProductId = entity.ProductCategoryEntity.ProductId;
            productSuggestion.SuggestedProductId = entity.SuggestedProductCategoryEntity.ProductId;
            productSuggestion.SortOrder = entity.SortOrder ?? 0;
            productSuggestion.Checkout = entity.Checkout;

            return productSuggestion;
        }

        public override Obymobi.Logic.Model.ProductSuggestion ConvertMobileModelToModel(ProductSuggestion mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
