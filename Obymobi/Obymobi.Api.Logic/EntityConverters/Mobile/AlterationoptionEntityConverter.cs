﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class AlterationoptionEntityConverter : MobileEntityConverterBase<AlterationitemEntity, AlterationitemCollection, Alterationoption, Obymobi.Logic.Model.Alterationoption>
    {
        #region Fields

        private CompanyEntity companyEntity = null;

        #endregion

        #region Constructors

        public AlterationoptionEntityConverter(CompanyEntity companyEntity)
        {
            this.companyEntity = companyEntity;
        }

        #endregion

        #region Methods

        public override Alterationoption ConvertEntityToMobileModel(AlterationitemEntity entity)
        {
            AlterationoptionEntity alterationoptionEntity = entity.AlterationoptionEntity;

            Alterationoption alterationoption = new Alterationoption();
            alterationoption.AlterationoptionId = entity.AlterationoptionId;
            alterationoption.Name = alterationoptionEntity.FriendlyName.IsNullOrWhiteSpace() ? alterationoptionEntity.Name : alterationoptionEntity.FriendlyName;
            alterationoption.PriceIn = alterationoptionEntity.PriceIn ?? 0;
            alterationoption.Description = alterationoptionEntity.Description;

            // Custom texts
            alterationoption.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.AlterationoptionEntity.CustomTextCollection);

            return alterationoption;
        }

        public override Obymobi.Logic.Model.Alterationoption ConvertMobileModelToModel(Alterationoption mobileModel)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
