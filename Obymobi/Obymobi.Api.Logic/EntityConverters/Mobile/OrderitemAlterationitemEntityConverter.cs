﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class OrderitemAlterationitemEntityConverter : MobileEntityConverterBase<OrderitemAlterationitemEntity, OrderitemAlterationitemCollection, Obymobi.Logic.Model.Mobile.Alterationitem, Obymobi.Logic.Model.Alterationitem>
    {
        public override Obymobi.Logic.Model.Mobile.Alterationitem ConvertEntityToMobileModel(OrderitemAlterationitemEntity entity)
        {
            Obymobi.Logic.Model.Mobile.Alterationitem mobileModel = new Obymobi.Logic.Model.Mobile.Alterationitem();
            if (entity.AlterationitemId.HasValue)
            {
                mobileModel.AlterationitemId = entity.AlterationitemId.Value;
                mobileModel.AlterationId = entity.AlterationitemEntity.AlterationId;
                mobileModel.AlterationoptionId = entity.AlterationitemEntity.AlterationoptionId;
                mobileModel.SelectedOnDefault = entity.AlterationitemEntity.SelectedOnDefault;
                mobileModel.SortOrder = entity.AlterationitemEntity.SortOrder;
            }
            mobileModel.Guid = entity.Guid;
            mobileModel.AlterationName = entity.AlterationName;
            mobileModel.AlterationType = entity.AlterationType;
            mobileModel.AlterationoptionName = entity.AlterationoptionName;
            mobileModel.AlterationoptionPriceIn = entity.AlterationoptionPriceIn;

            if (entity.Time.HasValue)
                mobileModel.Time = entity.Time.Value;

            mobileModel.Value = entity.Value;

            return mobileModel;
        }

        public override Obymobi.Logic.Model.Alterationitem ConvertMobileModelToModel(Obymobi.Logic.Model.Mobile.Alterationitem mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
