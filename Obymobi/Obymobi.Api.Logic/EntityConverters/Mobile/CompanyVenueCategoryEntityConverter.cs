﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class CompanyVenueCategoryEntityConverter : MobileEntityConverterBase<CompanyVenueCategoryEntity, CompanyVenueCategoryCollection, VenueCategory, Obymobi.Logic.Model.VenueCategory>
    {
        public override VenueCategory ConvertEntityToMobileModel(CompanyVenueCategoryEntity entity)
        {
            VenueCategoryEntity venueCategoryEntity = entity.VenueCategoryEntity;

            VenueCategory venueCategory = new VenueCategory();
            venueCategory.VenueCategoryId = venueCategoryEntity.VenueCategoryId;
            //venueCategory.Name = venueCategoryEntity.Name;

            return venueCategory;
        }

        public override Obymobi.Logic.Model.VenueCategory ConvertMobileModelToModel(VenueCategory mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
