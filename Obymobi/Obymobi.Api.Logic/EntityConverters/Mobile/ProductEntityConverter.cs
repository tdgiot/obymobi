﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model.Mobile;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Api.Logic.HelperClasses;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class ProductEntityConverter : MobileEntityConverterBase<ProductEntity, ProductCollection, Product, Obymobi.Logic.Model.Product>
    {
        #region Fields

        private static readonly CustomTextType[] ProductCustomTextTypes = new[]
                                                          {
                                                              CustomTextType.ProductName, 
                                                              CustomTextType.ProductDescription, 
                                                              CustomTextType.ProductButtonText
                                                          };

        private static readonly CustomTextType[] GenericProductCustomTextTypes = new[]
                                                                 {
                                                                     CustomTextType.GenericproductName, 
                                                                     CustomTextType.GenericproductDescription
                                                                 };

        private CategoryEntity categoryEntity = null;
        private DeviceType deviceType = DeviceType.Unknown;

        #endregion

        #region Constructors

        public ProductEntityConverter(CategoryEntity categoryEntity, DeviceType deviceType)
        {
            this.categoryEntity = categoryEntity;
            this.deviceType = deviceType;
        }

        #endregion

        #region Methods

        public override Product ConvertEntityToMobileModel(ProductEntity entity)
        {
            Product product = new Product();
            product.ProductId = entity.ProductId;
            product.Name = entity.Name;
            product.Description = entity.Description;
            product.Type = entity.Type;
            product.PriceIn = entity.PriceIn ?? 0;
            product.ButtonText = entity.ButtonText;
            product.WebTypeSmartphoneUrl = entity.WebTypeSmartphoneUrl;
            product.WebTypeTabletUrl = entity.WebTypeTabletUrl;
            product.ScheduleId = entity.ScheduleId ?? 0;

            if (entity.GenericproductId.HasValue)
            {
                bool useDefault = true;
                bool nameEmpty = string.IsNullOrEmpty(entity.Name);
                bool useGenericproductDescription = !entity.ManualDescriptionEnabled;

                if (useDefault)
                {
                    if (nameEmpty)
                        product.Name = entity.GenericproductEntity.Name;

                    if (useGenericproductDescription)
                        product.Description = entity.GenericproductEntity.Description;
                }
            }

            product.CategoryId = this.categoryEntity.CategoryId;
            product.SortOrder = entity.SortOrder + (10000 * this.categoryEntity.SortOrder);
            product.Type = CategoryHelper.GetTypeFromParent(this.categoryEntity);
            if (product.Type == 0)
                product.Type = (int)ProductSubType.Normal;

            product.SubType = entity.SubType;

            if (entity.Geofencing.HasValue)
                product.Geofencing = entity.Geofencing.Value;
            else
                product.Geofencing = (Obymobi.Logic.HelperClasses.CategoryHelper.HasGeofencedParent(this.categoryEntity) ?? true);

            if (entity.AllowFreeText.HasValue)
                product.AllowFreeText = entity.AllowFreeText.Value;
            else
                product.AllowFreeText = (CategoryHelper.HasAllowFreeTextParent(this.categoryEntity) ?? false);

            product.HidePrice = entity.HidePrice;

            // Alterations > Based on product, otherwise category
            List<Alteration> alterations = new List<Alteration>();
            if (entity.ProductAlterationCollection.Count > 0)
            {
                entity.ProductAlterationCollection.Sort((int)ProductAlterationFieldIndex.SortOrder, System.ComponentModel.ListSortDirection.Ascending);

                for (int i = 0; i < entity.ProductAlterationCollection.Count; i++)
                {
                    if (entity.ProductAlterationCollection[i].Version > 1)
                        continue;

                    List<Alteration> alterationsWithChildren = new AlterationEntityConverter(this.categoryEntity, this.deviceType).ConvertEntityToMobileModelWithChildren(entity.ProductAlterationCollection[i].AlterationEntity);
                    foreach (Alteration alteration in alterationsWithChildren)
                    {
                        if ((alteration.Type == (int)AlterationType.Options && alteration.Alterationoptions != null && alteration.Alterationoptions.Length > 0) || alteration.Type != (int)AlterationType.Options)
                        {
                            alterations.Add(alteration);
                        }
                    }
                }
                product.Alterations = alterations.ToArray();
            }
            else
            {
                // TODO Get alterations from category
                //ProductHelper.AddAlterationsFromCategory(alterations, productCategoryEntity.CategoryEntity, posConnectorType, false);
                //product.Alterations = alterations.ToArray();
            }

            // Media
            if (entity.GenericproductId.HasValue)
                product.Media = new MediaEntityConverter(false, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.GenericproductEntity.MediaCollection);
            else if (entity.MediaCollection.Count > 0)
                product.Media = new MediaEntityConverter(false, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.MediaCollection);

            // Product suggestions
            product.ProductSuggestions = new ProductSuggestionEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.ProductSuggestionCollection);

            // Product Attachments
            product.Attachments = new AttachmentEntityConverter(this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.AttachmentCollection);

            // Custom texts
            if (!entity.GenericproductId.HasValue)
            {
                product.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CustomTextCollection);
            }
            else
            {
                List<CustomText> customTexts = new List<CustomText>();               
                CustomTextEntityConverter converter = new CustomTextEntityConverter();
                foreach (CompanyCultureEntity companyCulture in entity.CompanyEntity.CompanyCultureCollection)
                {
                    if (companyCulture.CultureCode.Equals(entity.CompanyEntity.CultureCode, StringComparison.InvariantCultureIgnoreCase))
                        continue;

                    for (int i = 0; i < ProductEntityConverter.ProductCustomTextTypes.Length; i++)
                    {
                        // Get custom text from product
                        CustomTextEntity customTextEntity = entity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCulture.CultureCode && x.Type == ProductEntityConverter.ProductCustomTextTypes[i]);

                        if (entity.GenericproductId.HasValue && ProductEntityConverter.GenericProductCustomTextTypes.Length > i &&
                            (ProductEntityConverter.GenericProductCustomTextTypes[i] == CustomTextType.GenericproductName && entity.Name.IsNullOrWhiteSpace() ||
                            ProductEntityConverter.GenericProductCustomTextTypes[i] == CustomTextType.GenericproductDescription && entity.Description.IsNullOrWhiteSpace()))
                        {
                            // Get custom text from generic product
                            customTextEntity = entity.GenericproductEntity.CustomTextCollection.SingleOrDefault(x => x.CultureCode == companyCulture.CultureCode && x.Type == ProductEntityConverter.GenericProductCustomTextTypes[i]);
                            if (customTextEntity != null)
                            {
                                // Get from custom text of generic product
                                customTextEntity.GenericproductId = null;
                                customTextEntity.ProductId = entity.ProductId;
                                customTextEntity.Type = ProductEntityConverter.ProductCustomTextTypes[i];
                            }
                            else
                            {
                                // Get text directly from generic product
                                customTextEntity = new CustomTextEntity();
                                customTextEntity.CultureCode = companyCulture.CultureCode;
                                customTextEntity.ProductId = entity.ProductId;
                                customTextEntity.Type = ProductEntityConverter.ProductCustomTextTypes[i];

                                if (ProductEntityConverter.GenericProductCustomTextTypes[i] == CustomTextType.GenericproductName)
                                {
                                    customTextEntity.Text = entity.GenericproductEntity.Name;
                                }
                                else if (ProductEntityConverter.GenericProductCustomTextTypes[i] == CustomTextType.GenericproductDescription)
                                {
                                    customTextEntity.Text = entity.GenericproductEntity.Description;
                                }
                            }
                        }

                        if (customTextEntity != null)
                        {
                            customTexts.Add(converter.ConvertEntityToMobileModel(customTextEntity));
                        }                        
                    }
                }

                product.CustomTexts = customTexts.ToArray();                
            }

            return product;
        }

        public override Product[] ConvertEntityCollectionToMobileModelArray(ProductCollection entityCollection)
        {
            List<Product> modelList = new List<Product>();

            if (entityCollection != null && entityCollection.Count > 0)
            {
                entityCollection.Sort((int)ProductFieldIndex.SortOrder, System.ComponentModel.ListSortDirection.Ascending);

                foreach (ProductEntity entity in entityCollection)
                {
                    if (entity != null)
                    {
                        Product model = this.ConvertEntityToMobileModel(entity);
                        if (model != null)
                            modelList.Add(model);
                    }
                }
            }

            return modelList.ToArray();
        }

        public override Obymobi.Logic.Model.Product ConvertMobileModelToModel(Product mobileModel)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
