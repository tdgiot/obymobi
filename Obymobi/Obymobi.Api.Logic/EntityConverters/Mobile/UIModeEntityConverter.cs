﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class UIModeEntityConverter : MobileEntityConverterBase<UIModeEntity, UIModeCollection, UIMode, Obymobi.Logic.Model.UIMode>
    {
        public override UIMode ConvertEntityToMobileModel(UIModeEntity entity)
        {
            UIMode uiMode = new UIMode();
            uiMode.UIModeId = entity.UIModeId;
            uiMode.Name = entity.Name;

            var sortedTabs = entity.UITabCollection.OrderBy(x => x.SortOrder).ToList();

            UITabCollection tabs = new UITabCollection();
            tabs.AddRange(sortedTabs);

            uiMode.UITabs = new UITabEntityConverter().ConvertEntityCollectionToMobileModelArray(tabs);

            return uiMode;            
        }

        public override Obymobi.Logic.Model.UIMode ConvertMobileModelToModel(UIMode mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
