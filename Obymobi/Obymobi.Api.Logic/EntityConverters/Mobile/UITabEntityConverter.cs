﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class UITabEntityConverter : MobileEntityConverterBase<UITabEntity, UITabCollection, UITab, Obymobi.Logic.Model.UITab>
    {
        public override UITab ConvertEntityToMobileModel(UITabEntity entity)
        {
            UITab uiTab = new UITab();
            uiTab.UITabId = entity.UITabId;
            uiTab.Caption = entity.Caption;
            uiTab.Type = entity.Type;
            uiTab.CategoryId = entity.CategoryId.HasValue ? entity.CategoryId.Value : 0;
            uiTab.EntertainmentId = entity.EntertainmentId.HasValue ? entity.EntertainmentId.Value : 0;
            uiTab.URL = entity.URL;
            uiTab.Zoom = entity.Zoom;
            uiTab.Visible = entity.Visible;
            uiTab.SortOrder = entity.SortOrder;
            uiTab.SiteId = entity.SiteId ?? 0;

            // Custom texts
            uiTab.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CustomTextCollection);

            return uiTab;
        }

        public override Obymobi.Logic.Model.UITab ConvertMobileModelToModel(UITab mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
