﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System.Linq;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class SiteEntityConverter : MobileEntityConverterBase<SiteEntity, SiteCollection, Obymobi.Logic.Model.Mobile.Site, Obymobi.Logic.Model.Site>
    {
        #region Fields

        private DeviceType deviceType = DeviceType.Unknown;

        #endregion

        #region Constructors

        public SiteEntityConverter(DeviceType deviceType)
        {
            this.deviceType = deviceType;
        }

        #endregion

        #region Methods

        public override Obymobi.Logic.Model.Mobile.Site ConvertEntityToMobileModel(SiteEntity entity)
        {
            Obymobi.Logic.Model.Mobile.Site site = new Obymobi.Logic.Model.Mobile.Site();
            site.SiteId = entity.SiteId;
            site.Version = entity.Version;
            site.Name = entity.Name;
            site.SiteType = entity.SiteType;
            site.LastModifiedTicks = entity.LastModifiedUTC.Ticks;
            
            // Pages
            site.Pages = new PageEntityConverter(this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.PageCollection);

            // Media
            site.Media = new MediaEntityConverter(false, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.MediaCollection);

            // Custom texts
            site.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CustomTextCollection);

            return site;
        }

        public override Obymobi.Logic.Model.Site ConvertMobileModelToModel(Obymobi.Logic.Model.Mobile.Site mobileModel)
        {
            var site = new Obymobi.Logic.Model.Site();
            site.SiteId = mobileModel.SiteId;
            site.Version = mobileModel.Version;
            site.Name = mobileModel.Name;
            site.SiteType = mobileModel.SiteType;
            site.LastModifiedTicks = mobileModel.LastModifiedTicks;
            
            var pageConverter = new PageEntityConverter(this.deviceType);
            site.Pages = mobileModel.Pages.Select(pageConverter.ConvertMobileModelToModel).ToArray();

            var mediaConverter = new MediaEntityConverter(false, this.deviceType);
            site.Media = mobileModel.Media.Select(mediaConverter.ConvertMobileModelToModel).ToArray();

            var customTextConverter = new CustomTextEntityConverter();
            site.CustomTexts = mobileModel.CustomTexts.Select(customTextConverter.ConvertMobileModelToModel).ToArray();

            return site;
        }

        #endregion

        public Obymobi.Logic.Model.Site ConvertEntityToModel(SiteEntity entity)
        {
            var mobileSite = ConvertEntityToMobileModel(entity);
            return ConvertMobileModelToModel(mobileSite);
        }
    }
}
