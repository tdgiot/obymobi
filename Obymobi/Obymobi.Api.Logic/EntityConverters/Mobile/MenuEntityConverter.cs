﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class MenuEntityConverter : MobileEntityConverterBase<MenuEntity, MenuCollection, Obymobi.Logic.Model.Mobile.Menu, Obymobi.Logic.Model.ModelBase> // ModelBase because there is no normal model for Menu
    {
        #region Fields

        private DeviceType deviceType = DeviceType.Unknown;

        #endregion

        #region Constructors

        public MenuEntityConverter(DeviceType deviceType)
        {
            this.deviceType = deviceType;
        }

        #endregion

        #region Methods

        public override Obymobi.Logic.Model.Mobile.Menu ConvertEntityToMobileModel(MenuEntity entity)
        {
            Obymobi.Logic.Model.Mobile.Menu menu = new Obymobi.Logic.Model.Mobile.Menu();
            menu.MenuId = entity.MenuId;
            menu.Name = entity.Name;

            // Root categories
            menu.Categories = new CategoryEntityConverter(true, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.CategoryCollection);

            return menu;
        }

        public override Obymobi.Logic.Model.ModelBase ConvertMobileModelToModel(Obymobi.Logic.Model.Mobile.Menu mobileModel)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
