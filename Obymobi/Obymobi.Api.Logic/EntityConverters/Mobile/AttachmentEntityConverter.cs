﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class AttachmentEntityConverter : MobileEntityConverterBase<AttachmentEntity, AttachmentCollection, Attachment, Obymobi.Logic.Model.Attachment>
    {
        private DeviceType deviceType = DeviceType.Unknown;

        public AttachmentEntityConverter(DeviceType deviceType)
        {
            this.deviceType = deviceType;
        }

        public override Attachment ConvertEntityToMobileModel(AttachmentEntity paEntity)
        {
            Attachment pa = new Attachment();
            pa.AttachmentId = paEntity.AttachmentId;
            pa.Name = paEntity.Name;
            pa.ProductId = paEntity.ProductId ?? -1;
            pa.PageId = paEntity.PageId ?? -1;
            pa.Type = (int)paEntity.Type;
            pa.Url = paEntity.Url;
            pa.AllowAllDomains = paEntity.AllowAllDomains;
            pa.AllowedDomains = paEntity.AllowedDomains;

            // Media
            pa.Media = new MediaEntityConverter(false, this.deviceType, "PAGES_ATTACHMENTS").ConvertEntityCollectionToMobileModelArray(paEntity.MediaCollection);

            // Custom texts
            pa.CustomTexts = new CustomTextEntityConverter("PAGES_ATTACHMENTS").ConvertEntityCollectionToMobileModelArray(paEntity.CustomTextCollection);
            
            return pa;
        }

        public override Attachment[] ConvertEntityCollectionToMobileModelArray(AttachmentCollection entityCollection)
        {
            List<Attachment> toReturn = new List<Attachment>();

            foreach (var entity in entityCollection)
            {
                if (entity.HasDocuments() || !entity.Url.IsNullOrWhiteSpace())
                {
                    toReturn.Add(this.ConvertEntityToMobileModel(entity));
                }                
            }                

            return toReturn.ToArray();
        }

        public override Obymobi.Logic.Model.Attachment ConvertMobileModelToModel(Attachment mobileModel)
        {
            var model = new Obymobi.Logic.Model.Attachment();
            model.AttachmentId = mobileModel.AttachmentId;
            model.Name = mobileModel.Name;
            model.PageId = mobileModel.PageId;
            model.ProductId = mobileModel.ProductId;
            model.Type = mobileModel.Type;
            model.Url = mobileModel.Url;
            model.AllowAllDomains = mobileModel.AllowAllDomains;
            model.AllowedDomains = mobileModel.AllowedDomains;

            var mediaConverter = new MediaEntityConverter(false, this.deviceType);
            model.Media = mobileModel.Media.Select(mediaConverter.ConvertMobileModelToModel).ToArray();

            var customTextConverter = new CustomTextEntityConverter("PAGES_ATTACHMENTS");
            model.CustomTexts = mobileModel.CustomTexts.Select(customTextConverter.ConvertMobileModelToModel).ToArray();

            return model;
        }
    }
}
