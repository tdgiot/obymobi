﻿using Dionysos;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class MediaRatioTypeMediaEntityConverter : MobileEntityConverterBase<MediaRatioTypeMediaEntity, MediaRatioTypeMediaCollection, Obymobi.Logic.Model.Mobile.Media, Obymobi.Logic.Model.Media>
    {
        #region Fields

        private bool minified = false;
        private MediaEntity mediaEntity = null;
        private DeviceType deviceType = DeviceType.Unknown;
        private string parent;

        #endregion

        #region Constructors

        public MediaRatioTypeMediaEntityConverter(bool minified, MediaEntity mediaEntity, DeviceType deviceType, string parent = "")
        {
            this.minified = minified;
            this.mediaEntity = mediaEntity;
            this.deviceType = deviceType;
            this.parent = parent;
        }

        #endregion

        #region Methods

        public override Obymobi.Logic.Model.Mobile.Media ConvertEntityToMobileModel(MediaRatioTypeMediaEntity entity)
        {
            Obymobi.Logic.Model.Mobile.Media media = new Obymobi.Logic.Model.Mobile.Media();
            media.MediaId = this.mediaEntity.MediaId;
            media.MediaType = entity.MediaType.GetValueOrDefault(0);
            media.Parent = this.parent;

            // PILS Check if this works for generic products etc for mobile            
            media.CdnPath = MediaHelper.GetMediaRatioTypeMediaPath(entity, MediaRatioTypeMediaEntity.FileNameType.Cdn);

            if (!this.minified)
            {
                media.ActionProductId = mediaEntity.ActionProductId.HasValue ? mediaEntity.ActionProductId.Value : 0;
                media.ActionCategoryId = mediaEntity.ActionCategoryId.HasValue ? mediaEntity.ActionCategoryId.Value : 0;
                media.ActionEntertainmentId = mediaEntity.ActionEntertainmentId.HasValue ? mediaEntity.ActionEntertainmentId.Value : 0;
                media.ActionEntertainmentcategoryId = mediaEntity.ActionEntertainmentcategoryId.HasValue ? mediaEntity.ActionEntertainmentcategoryId.Value : 0;
                media.ActionUrl = mediaEntity.ActionUrl;
                media.ActionSiteId = mediaEntity.ActionSiteId.HasValue ? mediaEntity.ActionSiteId.Value : 0;
                media.ActionPageId = mediaEntity.ActionPageId.HasValue ? mediaEntity.ActionPageId.Value : 0;
            }

            media.SizeMode = (int)mediaEntity.SizeMode;
            media.ZoomLevel = mediaEntity.ZoomLevel;
            
            string mediaParent = this.parent.IsNullOrWhiteSpace() ? "" : string.Format("{0}_MEDIA", this.parent);
            media.MediaCultures = new MediaCultureEntityConverter(this.deviceType, mediaParent).ConvertEntityCollectionToMobileModelArray(this.mediaEntity.MediaCultureCollection);
            media.AgnosticMediaId = this.mediaEntity.AgnosticMediaId.GetValueOrDefault(0);
            media.RelatedCompanyId = mediaEntity.RelatedCompanyId.HasValue ? mediaEntity.RelatedCompanyId.Value : 0;
            media.RelatedBrandId = mediaEntity.ProductId.HasValue ? mediaEntity.ProductEntity.BrandId.GetValueOrDefault(0) : 0;

            return media;
        }

        public override Obymobi.Logic.Model.Mobile.Media[] ConvertEntityCollectionToMobileModelArray(MediaRatioTypeMediaCollection entityCollection)
        {
            List<Obymobi.Logic.Model.Mobile.Media> modelList = new List<Obymobi.Logic.Model.Mobile.Media>();

            if (entityCollection != null && entityCollection.Count > 0)
            {
                var mediaTypesForDevice = MediaRatioTypes.GetMediaTypeListForDeviceTypes(this.deviceType);
                foreach (MediaRatioTypeMediaEntity entity in entityCollection)
                {
                    if (entity != null && mediaTypesForDevice.Contains(entity.MediaTypeAsEnum))
                    {
                        Obymobi.Logic.Model.Mobile.Media model = this.ConvertEntityToMobileModel(entity);
                        if (model != null)
                            modelList.Add(model);
                    }
                }
            }

            return modelList.ToArray();
        }

        public override Obymobi.Logic.Model.Media ConvertMobileModelToModel(Obymobi.Logic.Model.Mobile.Media mobileModel)
        {
            var media = new Obymobi.Logic.Model.Media();
            media.MediaId = mobileModel.MediaId;
            media.MediaType = mobileModel.MediaType;
            media.Parent = mobileModel.Parent;

            media.ActionProductId = mobileModel.ActionProductId;
            media.ActionCategoryId = mobileModel.ActionCategoryId;
            media.ActionEntertainmentId = mobileModel.ActionEntertainmentId;
            media.ActionEntertainmentcategoryId = mobileModel.ActionEntertainmentcategoryId;
            media.ActionUrl = mobileModel.ActionUrl;
            media.ActionSiteId = mobileModel.ActionSiteId;
            media.ActionPageId = mobileModel.ActionPageId;

            media.CdnFilePathRelativeToMediaPath = mobileModel.CdnPath;

            media.SizeMode = mobileModel.SizeMode;
            media.ZoomLevel = mobileModel.ZoomLevel;

            string mediaParent = mobileModel.Parent.IsNullOrWhiteSpace() ? "" : string.Format("{0}_MEDIA", mobileModel.Parent);
            media.MediaCultures = mobileModel.MediaCultures.Select(new MediaCultureEntityConverter(this.deviceType, mediaParent).ConvertMobileModelToModel).ToArray();
            media.AgnosticMediaId = mobileModel.AgnosticMediaId;

            media.RelatedCompanyId = mobileModel.RelatedCompanyId;
            media.RelatedBrandId = mobileModel.RelatedBrandId;

            return media;
        }

        #endregion
    }
}
