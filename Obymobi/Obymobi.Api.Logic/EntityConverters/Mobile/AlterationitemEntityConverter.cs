﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class AlterationitemEntityConverter : MobileEntityConverterBase<AlterationitemEntity, AlterationitemCollection, Alterationitem, Obymobi.Logic.Model.Alterationitem>
    {
        public override Alterationitem ConvertEntityToMobileModel(AlterationitemEntity entity)
        {
            throw new NotImplementedException();
        }

        public override Obymobi.Logic.Model.Alterationitem ConvertMobileModelToModel(Alterationitem mobileModel)
        {
            Obymobi.Logic.Model.Alterationitem model = new Obymobi.Logic.Model.Alterationitem();
            model.AlterationitemId = mobileModel.AlterationitemId;
            model.AlterationId = mobileModel.AlterationId;
            model.Guid = mobileModel.Guid;
            model.AlterationName = mobileModel.AlterationName;
            model.AlterationType = mobileModel.AlterationType;
            model.AlterationoptionId = mobileModel.AlterationoptionId;
            model.AlterationoptionName = mobileModel.AlterationoptionName;
            model.AlterationoptionPriceIn = mobileModel.AlterationoptionPriceIn;
            model.SelectedOnDefault = mobileModel.SelectedOnDefault;
            model.SortOrder = mobileModel.SortOrder;
            model.Time = mobileModel.Time.ToString("dd-MM-yyyy HHmmss");      
            model.Value = mobileModel.Value;

            return model;
        }
    }
}
