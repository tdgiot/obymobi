﻿using Obymobi.Data;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using MarkdownSharp;
using Obymobi.Enums;
using MarkdownSharp.Extensions;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class AlterationEntityConverter : MobileEntityConverterBase<AlterationEntity, AlterationCollection, Alteration, Obymobi.Logic.Model.Alteration>
    {
        private CategoryEntity categoryEntity = null;
        private DeviceType deviceType = DeviceType.Unknown;

        public AlterationEntityConverter()
        {

        }

        public AlterationEntityConverter(CategoryEntity categoryEntity, DeviceType deviceType)
        {
            this.categoryEntity = categoryEntity;
            this.deviceType = deviceType;
        }

        public List<Alteration> ConvertEntityToMobileModelWithChildren(AlterationEntity alterationEntity)
        {
            List<Alteration> alterations = new List<Alteration>();
            alterations.Add(this.ConvertEntityToMobileModel(alterationEntity));

            if (alterationEntity.AlterationCollection.Count > 0)
            {
                alterationEntity.AlterationCollection.Sort((int)AlterationFieldIndex.SortOrder, System.ComponentModel.ListSortDirection.Ascending);
                foreach (AlterationEntity childAlterationEntity in alterationEntity.AlterationCollection)
                {
                    alterations.AddRange(this.ConvertEntityToMobileModelWithChildren(childAlterationEntity));
                }
            }

            return alterations;
        }

        public override Alteration ConvertEntityToMobileModel(AlterationEntity entity)
        {
            Alteration alteration = new Alteration();
            alteration.AlterationId = entity.AlterationId;
            alteration.Name = entity.FriendlyName.IsNullOrWhiteSpace() ? entity.Name : entity.FriendlyName;
            alteration.Type = (int)entity.Type;
            alteration.MinOptions = entity.MinOptions;
            alteration.MaxOptions = entity.MaxOptions;
            if (entity.StartTime.HasValue)
                alteration.StartTime = entity.StartTime.Value.ToString("dd-MM-yyyy HH:mm:ss");
            if (entity.EndTime.HasValue)
                alteration.EndTime = entity.EndTime.Value.ToString("dd-MM-yyyy HH:mm:ss");
            alteration.MinLeadMinutes = entity.MinLeadMinutes;
            alteration.MaxLeadHours = entity.MaxLeadHours;
            alteration.IntervalMinutes = entity.IntervalMinutes;
            alteration.ShowDatePicker = entity.ShowDatePicker;
            alteration.OrderLevelEnabled = entity.OrderLevelEnabled;
            alteration.ParentAlterationId = entity.ParentAlterationId ?? -1;
            alteration.Visible = entity.Visible;

            if (entity.Type != AlterationType.Instruction)
            {
                alteration.Description = entity.Description;
            }
            else
            {
                // Since Markdown is not thread safe and apparently creation cost is low, we spawn one (https://code.google.com/p/markdownsharp/issues/detail?id=48)
                Markdown markdown = MarkdownHelper.GetDefaultInstance();
                alteration.Description = markdown.Transform(entity.Description);

                if (alteration.Description.EndsWith("\n"))
                    alteration.Description = alteration.Description.Substring(0, alteration.Description.Length - 1);
            }            

            // GK Quick fix, TODO: Multiple
            if (entity.AlterationitemCollection.Any(ai => ai.SelectedOnDefault == true))
                alteration.DefaultAlterationoptionId = entity.AlterationitemCollection.First(ai => ai.SelectedOnDefault == true).AlterationoptionId;
            else
                alteration.DefaultAlterationoptionId = -1;

            if (entity.CompanyId.HasValue)
            {
                // Sort the alteration options according to the sort order
                entity.AlterationitemCollection.Sort((int)AlterationitemFieldIndex.SortOrder, System.ComponentModel.ListSortDirection.Ascending);

                // Alteration options
                alteration.Alterationoptions = new AlterationoptionEntityConverter(entity.CompanyEntity).ConvertEntityCollectionToMobileModelArray(entity.AlterationitemCollection);
            }            

            // Alteration products
            if (entity.AlterationProductCollection.Count > 0)
            {
                entity.AlterationProductCollection.Sort((int)AlterationProductFieldIndex.SortOrder, System.ComponentModel.ListSortDirection.Ascending);

                List<Product> products = new List<Product>();
                for (int i = 0; i < entity.AlterationProductCollection.Count; i++)
                {
                    if (entity.AlterationProductCollection[i].ProductEntity.Visible)
                    {
                        Product product = new ProductEntityConverter(this.categoryEntity, this.deviceType).ConvertEntityToMobileModel(entity.AlterationProductCollection[i].ProductEntity);
                        product.SortOrder = entity.AlterationProductCollection[i].SortOrder;

                        products.Add(product);
                    }                    
                }
                alteration.Products = products.ToArray();                
            }

            // Alteration media
            if (entity.MediaCollection.Count > 0)
            {
                alteration.Media = new MediaEntityConverter(false, this.deviceType).ConvertEntityCollectionToMobileModelArray(entity.MediaCollection);
            }

            // Custom texts
            alteration.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.CustomTextCollection);
            
            return alteration;
        }

        public override Obymobi.Logic.Model.Alteration ConvertMobileModelToModel(Alteration mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
