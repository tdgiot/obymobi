﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Obymobi.Data;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class ProductAlterationEntityConverter : MobileEntityConverterBase<ProductAlterationEntity, ProductAlterationCollection, Alteration, Obymobi.Logic.Model.Alteration>
    {
        public override Alteration ConvertEntityToMobileModel(ProductAlterationEntity entity)
        {
            return new AlterationEntityConverter().ConvertEntityToMobileModel(entity.AlterationEntity);
        }

        public override Alteration[] ConvertEntityCollectionToMobileModelArray(ProductAlterationCollection entityCollection)
        {
            List<Alteration> alterations = new List<Alteration>();

            foreach (ProductAlterationEntity productAlterationEntity in entityCollection)
            {
                Alteration alteration = this.ConvertEntityToMobileModel(productAlterationEntity);
                if ((alteration.Type == (int)AlterationType.Options && alteration.Alterationoptions != null && alteration.Alterationoptions.Length > 0) || alteration.Type != (int)AlterationType.Options)
                {
                    alterations.Add(alteration);                    
                }                
            }

            return alterations.ToArray();
        }

        public override Obymobi.Logic.Model.Alteration ConvertMobileModelToModel(Alteration mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
