﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class DeliverypointEntityConverter : MobileEntityConverterBase<DeliverypointEntity, DeliverypointCollection, Deliverypoint, Obymobi.Logic.Model.Deliverypoint>
    {
        public override Deliverypoint ConvertEntityToMobileModel(DeliverypointEntity entity)
        {
            Deliverypoint deliverypoint = new Deliverypoint();
            deliverypoint.DeliverypointId = entity.DeliverypointId;
            deliverypoint.CompanyId = entity.DeliverypointgroupEntity.CompanyId;
            deliverypoint.Name = entity.Name;
            deliverypoint.DeliverypointgroupId = entity.DeliverypointgroupId;

            int number = 0;
            if (int.TryParse(entity.Number, out number))
                deliverypoint.Number = number;

            return deliverypoint;            
        }

        public override Obymobi.Logic.Model.Deliverypoint ConvertMobileModelToModel(Deliverypoint mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
