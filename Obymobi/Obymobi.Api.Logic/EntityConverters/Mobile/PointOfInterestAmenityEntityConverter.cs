﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model.Mobile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class PointOfInterestAmenityEntityConverter : MobileEntityConverterBase<PointOfInterestAmenityEntity, PointOfInterestAmenityCollection, Amenity, Obymobi.Logic.Model.Amenity>
    {
        public override Amenity ConvertEntityToMobileModel(PointOfInterestAmenityEntity entity)
        {
            AmenityEntity amenityEntity = entity.AmenityEntity;

            Amenity amenity = new Amenity();
            amenity.AmenityId = amenityEntity.AmenityId;
            //amenity.Name = amenityEntity.Name;

            // Amenity languages
            //amenity.AmenityLanguages = new AmenityLanguageEntityConverter().ConvertEntityCollectionToMobileModelArray(amenityEntity.AmenityLanguageCollection);

            return amenity;
        }

        public override Obymobi.Logic.Model.Amenity ConvertMobileModelToModel(Amenity mobileModel)
        {
            throw new NotImplementedException();
        }
    }
}
