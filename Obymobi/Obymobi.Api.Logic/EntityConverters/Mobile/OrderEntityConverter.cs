﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Obymobi.Api.Logic.EntityConverters.Mobile
{
    public class OrderEntityConverter : MobileEntityConverterBase<OrderEntity, OrderCollection, Obymobi.Logic.Model.Mobile.Order, Obymobi.Logic.Model.Order>
    {
        public override Obymobi.Logic.Model.Mobile.Order ConvertEntityToMobileModel(OrderEntity entity)
        {
            Obymobi.Logic.Model.Mobile.Order mobileModel = new Obymobi.Logic.Model.Mobile.Order();
            mobileModel.OrderId = entity.OrderId;
            mobileModel.Guid = entity.Guid;
            mobileModel.CustomerId = entity.CustomerId ?? 0;
            mobileModel.CompanyId = entity.CompanyId;
            mobileModel.DeliverypointId = entity.DeliverypointId.HasValue ? entity.DeliverypointId.Value : -1;
            mobileModel.DeliverypointNumber = Convert.ToInt32(entity.DeliverypointNumber);
            mobileModel.DeliverypointName = entity.DeliverypointName;
            mobileModel.Status = entity.Status;
            mobileModel.StatusText = entity.StatusText;
            mobileModel.ErrorCode = entity.ErrorCode;
            mobileModel.Notes = entity.Notes;
            mobileModel.CustomerPhonenumber = entity.CustomerPhonenumber;
            mobileModel.Type = entity.Type;
            mobileModel.Created = entity.CreatedUTC.HasValue ? entity.CreatedUTC.Value.UtcToLocalTime().ToString() : "01-01-01";
            mobileModel.CreatedAsDateTime = entity.CreatedUTC.HasValue ? entity.CreatedUTC.Value.UtcToLocalTime() : DateTime.MinValue;
            mobileModel.Email = entity.Email;

            // Orderitems
            List<Obymobi.Logic.Model.Mobile.Orderitem> orderitems = new List<Obymobi.Logic.Model.Mobile.Orderitem>();
            
            // Master orderitems
            orderitems.AddRange(new OrderitemEntityConverter().ConvertEntityCollectionToMobileModelArray(entity.OrderitemCollection));
            
            // Child orderitems
            if (entity.OrderCollection.Count > 0)
            {
                foreach (OrderEntity childOrder in entity.OrderCollection)
                {
                    if (childOrder.OrderitemCollection.Count > 0)
                    {
                        orderitems.AddRange(new OrderitemEntityConverter().ConvertEntityCollectionToMobileModelArray(childOrder.OrderitemCollection));
                    }
                }
            }

            mobileModel.Orderitems = orderitems.ToArray();

            return mobileModel;
        }

        public override Obymobi.Logic.Model.Order ConvertMobileModelToModel(Obymobi.Logic.Model.Mobile.Order mobileModel)
        {
            Obymobi.Logic.Model.Order model = new Obymobi.Logic.Model.Order();
            model.OrderId = mobileModel.OrderId;
            model.Guid = mobileModel.Guid;
            model.CustomerId = mobileModel.CustomerId;
            model.CompanyId = mobileModel.CompanyId;
            model.DeliverypointId = mobileModel.DeliverypointId;
            model.DeliverypointNumber = mobileModel.DeliverypointNumber;
            model.DeliverypointName = mobileModel.DeliverypointName;
            model.Status = mobileModel.Status;
            model.StatusText = mobileModel.StatusText;
            model.ErrorCode = mobileModel.ErrorCode;
            model.Notes = mobileModel.Notes;
            model.CustomerNameFull = mobileModel.CustomerNameFull;
            model.CustomerPhonenumber = mobileModel.CustomerPhonenumber;
            model.Type = mobileModel.Type;
            model.Created = mobileModel.Created;
            model.MobileOrder = true;
            model.Email = mobileModel.Email;

            // Orderitems
            model.Orderitems = new OrderitemEntityConverter().ConvertMobileModelArrayToModelArray(mobileModel.Orderitems);

            return model;
        }
    }
}
