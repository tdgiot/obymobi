﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class PointOfInterestSynopsisEntityConverter : EntityConverterBase<PointOfInterestEntity, PointOfInterestSynopsis>
    {
        private readonly PointOfInterestVenueCategoryEntityConverter pointOfInterestVenueCategoryEntityConverter = new PointOfInterestVenueCategoryEntityConverter();

        public override PointOfInterestSynopsis ConvertEntityToModel(PointOfInterestEntity entity)
        {
            PointOfInterestSynopsis model = new PointOfInterestSynopsis
            {
                PointOfInterestId = entity.PointOfInterestId,
                Name = entity.Name,
                DescriptionSingleLine = entity.DescriptionSingleLine,
                Latitude = entity.Latitude,
                Longitude = entity.Longitude,
                Addressline1 = entity.Addressline1,
                Zipcode = entity.Zipcode,
                City = entity.City,
                Floor = entity.Floor,
                DisplayDistance = entity.DisplayDistance
            };

            model.VenueCategories = this.pointOfInterestVenueCategoryEntityConverter.ConvertEntityCollectionToModelArray(entity.PointOfInterestVenueCategoryCollection);
            model.CustomTexts = entity.CustomTextCollection.Select(x => Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertEntityToModel(x, "POINTOFINTERESTSYNOPSES")).ToArray();
            model.BusinesshoursIntermediate = BusinesshoursHelper.ConvertBusinesshoursToIntermediateFormat(entity.BusinesshoursCollection);

            Enums.MediaType[] allowedTypes = new Enums.MediaType[] { Enums.MediaType.MapMenuItem1280x800, Enums.MediaType.MapMarkerPointOfInterest1280x800 };

            List<Media> mediaModels = new List<Media>();
            foreach (MediaEntity mediaEntity in entity.MediaCollection)
            {
                List<MediaRatioTypeMediaEntity> ratios = mediaEntity.MediaRatioTypeMediaCollection.Where(x => allowedTypes.Contains(x.MediaTypeAsEnum)).ToList();
                foreach (MediaRatioTypeMediaEntity ratioEntity in ratios)
                {
                    Media media = Obymobi.Logic.HelperClasses.MediaHelper.CreateMediaModelFromMediaRatioTypeMedia(mediaEntity, ratioEntity, "", "");
                    if (media != null)
                    {
                        media.GenericFile = true;
                        mediaModels.Add(media);
                    }
                }
            }
            model.Media = mediaModels.ToArray();

            return model;
        }
    }
}
