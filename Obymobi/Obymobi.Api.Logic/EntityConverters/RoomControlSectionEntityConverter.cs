﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class RoomControlSectionEntityConverter : EntityConverterBase<RoomControlSectionEntity, RoomControlSection>
    {
        private readonly RoomControlSectionItemEntityConverter roomControlSectionItemEntityConverter;
        private readonly RoomControlComponentEntityConverter roomControlComponentEntityConverter;
        private readonly RoomControlWidgetEntityConverter roomControlWidgetEntityConverter;

        private List<int> infraredConfigurationIds;

        public RoomControlSectionEntityConverter(List<int> infraredConfigurationIds)
        {
            this.infraredConfigurationIds = infraredConfigurationIds;

            this.roomControlSectionItemEntityConverter = new RoomControlSectionItemEntityConverter(this.infraredConfigurationIds);
            this.roomControlComponentEntityConverter = new RoomControlComponentEntityConverter(this.infraredConfigurationIds);
            this.roomControlWidgetEntityConverter = new RoomControlWidgetEntityConverter(this.infraredConfigurationIds, "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS", "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS_ROOMCONTROLWIDGETS");
        }

        public override RoomControlSection ConvertEntityToModel(RoomControlSectionEntity entity)
        {
            RoomControlSection model = new RoomControlSection
            {
                RoomControlSectionId = entity.RoomControlSectionId,
                Name = entity.Name,
                Type = (int)entity.Type,
                FieldValue1 = entity.FieldValue1,
                FieldValue2 = entity.FieldValue2,
                FieldValue3 = entity.FieldValue3,
                FieldValue4 = entity.FieldValue4,
                FieldValue5 = entity.FieldValue5,
                Visible = entity.Visible
            };

            model.CustomTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection, "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS");

            // Room control section items
            List<RoomControlSectionItem> roomControlSectionItems = new List<RoomControlSectionItem>();
            if (entity.RoomControlSectionItemCollection.Count > 0)
            {
                List<RoomControlSectionItemEntity> sectionItems = entity.RoomControlSectionItemCollection.Where(si => si.Visible).OrderBy(si => si.SortOrder).ToList();
                foreach (RoomControlSectionItemEntity roomControlSectionItemEntity in sectionItems)
                {
                    roomControlSectionItems.Add(this.roomControlSectionItemEntityConverter.ConvertEntityToModel(roomControlSectionItemEntity));
                }
            }
            model.RoomControlSectionItems = roomControlSectionItems.ToArray();

            // Room control components
            List<RoomControlComponent> roomControlComponents = new List<RoomControlComponent>();
            if (entity.RoomControlComponentCollection.Count > 0)
            {
                List<RoomControlComponentEntity> components = entity.RoomControlComponentCollection.Where(c => c.Visible).OrderBy(c => c.SortOrder).ToList();
                foreach (RoomControlComponentEntity roomControlComponentEntity in components)
                {
                    roomControlComponents.Add(this.roomControlComponentEntityConverter.ConvertEntityToModel(roomControlComponentEntity));
                }
            }
            model.RoomControlComponents = roomControlComponents.ToArray();

            // Room control widgets
            List<RoomControlWidget> roomControlWidgets = new List<RoomControlWidget>();
            if (entity.RoomControlWidgetCollection.Count > 0)
            {
                List<RoomControlWidgetEntity> widgets = entity.RoomControlWidgetCollection.Where(c => c.Visible).OrderBy(c => c.SortOrder).ToList();
                foreach (RoomControlWidgetEntity roomControlWidgetEntity in widgets)
                {
                    roomControlWidgets.Add(this.roomControlWidgetEntityConverter.ConvertEntityToModel(roomControlWidgetEntity));
                }
            }
            model.RoomControlWidgets = roomControlWidgets.ToArray();

            // Media
            List<Media> media = new List<Media>();
            foreach (MediaEntity mediaEntity in entity.MediaCollection)
            {
                media.AddRange(MediaHelper.CreateMediaModelFromEntity(mediaEntity, "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS", "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS_MEDIA"));
            }
            model.Media = media.ToArray();

            return model;
        }
    }
}
