﻿using Obymobi.Api.Logic.Repositories;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class TerminalEntityConverter : EntityConverterBase<TerminalEntity, Terminal>
    {
        public override Terminal ConvertEntityToModel(TerminalEntity entity)
        {
            Terminal model = new Terminal();
            model.TerminalId = entity.TerminalId;
            model.Name = entity.Name;
            model.CompanyId = entity.CompanyId;
            model.CompanyName = entity.CompanyEntity.Name;
            model.LastRequest = entity.DeviceId.HasValue && entity.DeviceEntity.LastRequestUTC.HasValue ? entity.DeviceEntity.LastRequestUTC.Value : DateTime.MinValue;
            model.LastRequestNotifiedBySMS = entity.DeviceId.HasValue && entity.DeviceEntity.LastRequestNotifiedBySmsUTC.HasValue ? entity.DeviceEntity.LastRequestNotifiedBySmsUTC.Value : DateTime.MinValue;
            model.LastStatus = entity.LastStatus ?? 0;
            model.LastStatusText = entity.LastStatusText;
            model.LastStatusMessage = entity.LastStatusMessage;
            model.TeamviewerId = entity.TeamviewerId;
            model.RequestInterval = entity.RequestInterval;
            model.DiagnoseInterval = entity.DiagnoseInterval;
            model.PrinterName = entity.PrinterName;
            model.MaxDaysLogHistory = entity.MaxDaysLogHistory;
            model.POSConnectorType = entity.POSConnectorType;
            model.PMSConnectorType = entity.PMSConnectorType;
            model.SynchronizeWithPOSOnStart = entity.SynchronizeWithPOSOnStart;
            model.SynchronizeWithPMSOnStart = entity.SynchronizeWithPMSOnStart;
            model.MaxStatusReports = entity.MaxStatusReports;
            model.Active = entity.Active;
            model.UseMonitoring = (entity.CompanyEntity.UseMonitoring && entity.UseMonitoring);
            model.ReceiptTypes = entity.ReceiptTypes;
            model.LanguageCode = entity.LanguageCode;
            model.HandlingMethod = entity.HandlingMethod;
            model.MaxProcessTime = entity.MaxProcessTime;
            model.NotificationForNewOrder = entity.NotificationForNewOrder;
            model.NotificationForOverdueOrder = entity.NotificationForOverdueOrder;
            model.DeliverypointgroupId = entity.DeliverypointgroupId ?? 0;
            model.PosValue1 = entity.PosValue1;
            model.PosValue2 = entity.PosValue2;
            model.PosValue3 = entity.PosValue3;
            model.PosValue4 = entity.PosValue4;
            model.PosValue5 = entity.PosValue5;
            model.PosValue6 = entity.PosValue6;
            model.PosValue7 = entity.PosValue7;
            model.PosValue8 = entity.PosValue8;
            model.PosValue9 = entity.PosValue9;
            model.PosValue10 = entity.PosValue10;
            model.PosValue11 = entity.PosValue11;
            model.PosValue12 = entity.PosValue12;
            model.PosValue13 = entity.PosValue13;
            model.PosValue14 = entity.PosValue14;
            model.PosValue15 = entity.PosValue15;
            model.PosValue16 = entity.PosValue16;
            model.PosValue17 = entity.PosValue17;
            model.PosValue18 = entity.PosValue18;
            model.PosValue19 = entity.PosValue19;
            model.PosValue20 = entity.PosValue20;
            model.SupportpoolId = entity.CompanyEntity.SupportpoolId ?? 0;
            model.AnnouncementDuration = entity.AnnouncementDuration;
            model.ForwardToTerminalId = entity.ForwardToTerminalId ?? 0;
            model.Browser1EntertainmentId = entity.Browser1 ?? 0;
            model.Browser2EntertainmentId = entity.Browser2 ?? 0;
            model.CmsPageEntertainmentId = entity.CmsPage ?? 0;
            model.SurveyResultNotifications = entity.SurveyResultNotifications;
            model.PrintingEnabled = entity.PrintingEnabled;
            model.LastCommunicationMethod = entity.DeviceId.HasValue ? entity.DeviceEntity.CommunicationMethod : 0;
            model.LinkedToDevice = entity.DeviceId.HasValue ? entity.DeviceEntity.Identifier : string.Empty;
            model.UIModeId = entity.UIModeId ?? 0;
            model.HasMasterTab = entity.MasterTab;
            model.ResetTimeout = (entity.ResetTimeout > 0 ? entity.ResetTimeout : 5);
            model.UseHardKeyboard = (int)entity.UseHardKeyboard;
            model.MaxVolume = entity.MaxVolume;
            model.PmsTerminalOfflineNotificationEnabled = entity.PmsTerminalOfflineNotificationEnabled;

            // Get the company for the pincodes
            CompanyEntity company = entity.CompanyEntity;
            if (company != null)
            {
                model.Pincode = company.Pincode;
                model.PincodeGM = company.PincodeGM;
                model.PincodeSU = company.PincodeSU;

                if (model.PmsTerminalOfflineNotificationEnabled)
                {
                    TerminalEntity pmsTerminal = company.TerminalCollection.FirstOrDefault(x => x.HandlingMethod == (int)TerminalType.OnSiteServer && x.PMSConnectorType > 0);
                    if (pmsTerminal != null)
                    {
                        model.PmsActive = true;
                        model.PmsTerminalOnline = pmsTerminal.IsOnline;
                        model.LastPmsSync = Obymobi.Logic.HelperClasses.TerminalHelper.GetLastSyncFromTerminal(pmsTerminal);
                    }
                }
            }

            MessageTemplateCollection messageTemplateCollection = new MessageTemplateRepository().GetMessageTemplatesForTerminal(entity.TerminalId);
            if (messageTemplateCollection.Any())
                model.MessageTemplates = new MessageTemplateEntityConverter().ConvertEntityCollectionToModelArray(messageTemplateCollection);

            return model;
        }
    }
}
