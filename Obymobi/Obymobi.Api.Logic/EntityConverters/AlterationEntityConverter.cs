﻿using System.Collections.Generic;
using System.Linq;
using Dionysos;
using MarkdownSharp;
using MarkdownSharp.Extensions;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class AlterationEntityConverter : EntityConverterBase<AlterationEntity, Alteration>
    {
        private readonly bool addPosData;
        private readonly CompanyEntity companyEntity;

        private readonly AlterationoptionEntityConverter alterationoptionEntityConverter;
        private readonly AlterationProductEntityConverter alterationProductEntityConverter;

        public AlterationEntityConverter(bool addPosData, CompanyEntity companyEntity)
        {
            this.addPosData = addPosData;
            this.companyEntity = companyEntity;

            this.alterationoptionEntityConverter = new AlterationoptionEntityConverter(this.addPosData, this.companyEntity);
            this.alterationProductEntityConverter = new AlterationProductEntityConverter();
        }

        public override Alteration ConvertEntityToModel(AlterationEntity entity)
        {
            Alteration model = new Alteration();
            model.AlterationId = entity.AlterationId;
            model.Name = entity.FriendlyName.IsNullOrWhiteSpace() ? entity.Name : entity.FriendlyName;
            model.Type = (int)entity.Type;
            model.MinOptions = entity.MinOptions;
            model.MaxOptions = entity.MaxOptions;
            if (entity.StartTime.HasValue)
            {
                model.StartTime = entity.StartTime.Value.ToString("dd-MM-yyyy HH:mm:ss");
            }
            if (entity.EndTime.HasValue)
            {
                model.EndTime = entity.EndTime.Value.ToString("dd-MM-yyyy HH:mm:ss");
            }
            model.MinLeadMinutes = entity.MinLeadMinutes;
            model.MaxLeadHours = entity.MaxLeadHours;
            model.IntervalMinutes = entity.IntervalMinutes;
            model.ShowDatePicker = entity.ShowDatePicker;
            model.Value = entity.Value;
            model.OrderLevelEnabled = entity.OrderLevelEnabled;
            model.SortOrder = entity.SortOrder;
            model.ParentAlterationId = entity.ParentAlterationId ?? -1;
            model.LayoutType = (int)AlterationLayoutType.ImageAndTextOnTop;
            model.Visible = entity.Visible;            

            if (entity.Type != AlterationType.Instruction)
            {
                model.Description = entity.Description;
            }
            else
            {
                // Since Markdown is not thread safe and apparently creation cost is low, we spawn one (https://code.google.com/p/markdownsharp/issues/detail?id=48)
                Markdown markdown = MarkdownHelper.GetDefaultInstance();
                model.Description = markdown.Transform(entity.Description);

                if (model.Description.EndsWith("\n"))
                    model.Description = model.Description.Substring(0, model.Description.Length - 1);
            }
            
            if (companyEntity.SystemType == SystemType.Crave)
            {
                List<Obymobi.Logic.Model.Media> media = new List<Obymobi.Logic.Model.Media>();
                foreach (MediaEntity t in entity.MediaCollection)
                {
                    media.AddRange(new MediaEntityConverter().ConvertEntityToModel(t, "ALTERATIONS", "ALTERATIONS_MEDIA"));
                }
                model.Media = media.ToArray();
            }

            // Default alteration option ids
            if (entity.AlterationitemCollection.Any(ai => ai.SelectedOnDefault == true))
                model.DefaultAlterationoptionIds = entity.AlterationitemCollection.Where(ai => ai.SelectedOnDefault == true).Select(ai => ai.AlterationoptionId).ToArray();

            // Alteration options
            model.Alterationoptions = this.alterationoptionEntityConverter.ConvertEntityCollectionToModelArray(entity.AlterationitemCollection);
            
            // Alteration products
            model.AlterationProducts = this.alterationProductEntityConverter.ConvertEntityCollectionToModelArray(entity.AlterationProductCollection);
            
            if (addPosData && entity.PosalterationId.HasValue)
                model.Posalteration = Obymobi.Logic.HelperClasses.PosalterationHelper.CreatePosalterationModelFromEntity(entity.PosAlterationEntity, true);

            // Custom texts
            model.CustomTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection);

            return model;
        }
    }
}
