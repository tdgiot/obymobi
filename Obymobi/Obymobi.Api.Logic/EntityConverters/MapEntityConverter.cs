﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class MapEntityConverter : EntityConverterBase<MapEntity, Map>
    {
        private readonly PointOfInterestSynopsisEntityConverter pointOfInterestSynopsisEntityConverter = new PointOfInterestSynopsisEntityConverter();

        public override Map ConvertEntityToModel(MapEntity entity)
        {
            Map model = new Map
            {
                MapId = entity.MapId,
                Name = entity.Name,
                MapType = (int)entity.MapType,
                MyLocationEnabled = entity.MyLocationEnabled,
                ZoomControlsEnabled = entity.ZoomControlsEnabled,
                IndoorEnabled = entity.IndoorEnabled,
                ZoomLevel = entity.ZoomLevel,
                MapProvider = (int)entity.MapProvider
            };

            model.PointOfInterestSynopses = entity.MapPointOfInterestCollection.Select(x => this.pointOfInterestSynopsisEntityConverter.ConvertEntityToModel(x.PointOfInterestEntity)).ToArray();

            return model;
        }
    }
}
