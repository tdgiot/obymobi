﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class BusinesshoursEntityConverter : EntityConverterBase<BusinesshoursEntity, Businesshours>
    {
        public override Businesshours ConvertEntityToModel(BusinesshoursEntity entity)
        {
            return null;
        }

        public override Businesshours[] ConvertEntityCollectionToModelArray(IEnumerable<BusinesshoursEntity> entityCollection)
        {
            List<Businesshours> businesshours = new List<Businesshours>();

            bool first = true;
            bool open = true;
            Businesshours currentHours = null;
            foreach (BusinesshoursEntity businesshoursEntity in entityCollection)
            {
                int day = int.Parse(businesshoursEntity.DayOfWeekAndTime.Substring(0, 1));
                int time = int.Parse(businesshoursEntity.DayOfWeekAndTime.Substring(1, 4));

                if (first && businesshoursEntity.Opening)
                {
                    currentHours = new Businesshours();
                    currentHours.OpeningDay = day;
                    currentHours.OpeningTime = time;

                    first = false;
                }
                else if (!first && open && !businesshoursEntity.Opening)
                {
                    currentHours.ClosingDay = day;
                    currentHours.ClosingTime = time;
                    businesshours.Add(currentHours);

                    open = false;
                }
                else if (!first && !open && businesshoursEntity.Opening)
                {
                    currentHours = new Businesshours();
                    currentHours.OpeningDay = day;
                    currentHours.OpeningTime = time;

                    open = true;
                }
            }
            if (open && !first)
            {
                foreach (BusinesshoursEntity businesshoursEntity in entityCollection)
                {
                    int day = int.Parse(businesshoursEntity.DayOfWeekAndTime.Substring(0, 1));
                    int time = int.Parse(businesshoursEntity.DayOfWeekAndTime.Substring(1, 4));

                    if (!businesshoursEntity.Opening)
                    {
                        currentHours.ClosingDay = day;
                        currentHours.ClosingTime = time;

                        businesshours.Add(currentHours);
                        break;
                    }
                }
            }

            return businesshours.ToArray();
        }
    }
}
