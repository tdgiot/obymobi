﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class CompanyAmenityEntityConverter : EntityConverterBase<CompanyAmenityEntity, Amenity>
    {
        public override Amenity ConvertEntityToModel(CompanyAmenityEntity entity)
        {
            return new AmenityEntityConverter().ConvertEntityToModel(entity.AmenityEntity);
        }
    }
}
