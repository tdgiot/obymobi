﻿using System;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class AnnouncementEntityConverter : EntityConverterBase<AnnouncementEntity, Announcement>
    {
        public override Announcement ConvertEntityToModel(AnnouncementEntity entity)
        {
            Announcement model = new Announcement
            {
                AnnouncementId = entity.AnnouncementId,
                Title = entity.Title,
                Text = entity.Text,
                TimeToShow = entity.TimeToShow.HasValue ? entity.TimeToShow.Value : DateTime.MinValue,
                DateToShow = entity.DateToShow.HasValue ? entity.DateToShow.Value : DateTime.MinValue,
                Recurring = entity.Recurring.HasValue ? entity.Recurring.Value : false,
                RecurringPeriod = entity.RecurringPeriod.HasValue ? (int)entity.RecurringPeriod.Value : -1,
                RecurringBegin = entity.RecurringBeginDate.HasValue && entity.RecurringBeginTime.HasValue ? DateTimeUtil.CombineDateAndTime(entity.RecurringBeginTime.Value, entity.RecurringBeginDate.Value) : DateTime.MinValue,
                RecurringEnd = entity.RecurringEndDate.HasValue && entity.RecurringEndTime.HasValue ? DateTimeUtil.CombineDateAndTime(entity.RecurringEndTime.Value, entity.RecurringEndDate.Value) : DateTime.MinValue,
                RecurringAmount = entity.RecurringAmount.HasValue ? entity.RecurringAmount.Value : 0,
                RecurringMinutes = entity.RecurringMinutes.HasValue ? entity.RecurringMinutes.Value : 0,
                DialogType = entity.DialogType.HasValue ? entity.DialogType.Value : 0,
                OnYes = entity.OnYes.HasValue ? entity.OnYes.Value : 0,
                OnNo = entity.OnNo.HasValue ? entity.OnNo.Value : 0,
                OnYesCategory = entity.OnYesCategory.HasValue ? entity.OnYesCategory.Value : 0,
                OnNoCategory = entity.OnNoCategory.HasValue ? entity.OnNoCategory.Value : 0,
                OnYesEntertainmentCategory = entity.OnYesEntertainmentCategory.HasValue ? entity.OnYesEntertainmentCategory.Value : 0,
                OnNoEntertainmentCategory = entity.OnNoEntertainmentCategory.HasValue ? entity.OnNoEntertainmentCategory.Value : 0,
                OnYesEntertainment = entity.OnYesEntertainment.HasValue ? entity.OnYesEntertainment.Value : 0,
                OnYesProduct = entity.OnYesProduct.HasValue ? entity.OnYesProduct.Value : 0,
                MediaId = entity.MediaId.HasValue ? entity.MediaId.Value : 0,
                Duration = entity.Duration,
                CustomTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection)
            };

            return model;
        }
    }
}
