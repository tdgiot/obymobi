﻿using System;
using System.Collections.Generic;
using Dionysos;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Logic.Analytics;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class ClientEntityConverter : EntityConverterBase<ClientEntity, ClientCollection, Client>
    {
        private readonly Dictionary<int, Entertainmentcategory[]> entertainmentcategoriesCache = null;
        private readonly Dictionary<int, DeliverypointgroupEntity> deliverypointgroupCache = null;
        private readonly Media[] mediaCache = null;

        private readonly UIThemeEntityConverter uiThemeEntityConverter = new UIThemeEntityConverter();

        public ClientEntityConverter()
        {
        }

        public ClientEntityConverter(Dictionary<int, Entertainmentcategory[]> entertainmentcategoriesCache, Dictionary<int, DeliverypointgroupEntity> deliverypointgroupCache, Media[] mediaCache)
        {
            this.entertainmentcategoriesCache = entertainmentcategoriesCache;
            this.deliverypointgroupCache = deliverypointgroupCache;
            this.mediaCache = mediaCache;
        }

        public override Client ConvertEntityToModel(ClientEntity entity)
        {
            Client model = new Client();
            model.ClientId = entity.ClientId;
            model.CompanyId = entity.CompanyId;
            model.CompanyOwnerUsername = entity.CompanyEntity.CompanyOwnerEntity.Username;
            model.CompanyOwnerPassword = entity.CompanyEntity.CompanyOwnerEntity.Password;
            model.CompanyName = entity.CompanyEntity.Name;
            model.LastRequestNotifiedBySMSUTC = entity.DeviceEntity.LastRequestNotifiedBySmsUTC.GetValueOrDefault(DateTime.MinValue);
            model.LastStatus = (entity.LastStatus.HasValue ? entity.LastStatus.Value : 0);
            model.LastStatusText = entity.LastStatusText;
            model.OperationMode = entity.OperationMode;
            model.LastCommunicationMethod = entity.DeviceEntity.CommunicationMethod;
            if (entity.DeliverypointId.HasValue)
            {
                model.DeliverypointId = entity.DeliverypointEntity.DeliverypointId;
                model.DeliverypointNumber = entity.DeliverypointEntity.Number;
            }
            else
            {
                model.DeliverypointId = 0;
                model.DeliverypointNumber = "0";
            }

            if (entity.DeviceId.HasValue && entity.DeviceId.Value > 0)
            {
                DeviceEntity deviceEntity = entity.DeviceEntity;
                model.MacAddress = deviceEntity.Identifier;
                model.LastRequestUTC = deviceEntity.LastRequestUTC.GetValueOrDefault(DateTime.MinValue);
            }

            model.UpdateEmenuDownloaded = entity.DeviceEntity.UpdateEmenuDownloaded;
            model.UpdateAgentDownloaded = entity.DeviceEntity.UpdateAgentDownloaded;
            model.UpdateSupportToolsDownloaded = entity.DeviceEntity.UpdateSupportToolsDownloaded;
            model.DefaultRoomControlAreaName = entity.RoomControlAreaId.HasValue ? entity.RoomControlAreaEntity.NameSystem : string.Empty;

            // Temp
            //client.RelatedCustomerFullname = string.Format("Client {0}", client.ClientId);
            model.RelatedCustomerFullname = entity.MacAddress;
            //client.MacAddress = clientEntity.MacAddress;

            // Get the deliverypointgroup to get the settings
            DeliverypointgroupEntity deliverypointgroup = null;
            if (entity.DeliverypointGroupId.HasValue && (deliverypointgroupCache == null || !deliverypointgroupCache.TryGetValue(entity.DeliverypointGroupId.Value, out deliverypointgroup)))
            {
                deliverypointgroup = entity.DeliverypointgroupEntity;

                if (deliverypointgroupCache != null && deliverypointgroup != null)
                {
                    deliverypointgroupCache.Add(entity.DeliverypointGroupId.Value, deliverypointgroup);
                }
            }

            model.LogToFile = entity.LogToFile;

            if (deliverypointgroup != null)
            {
                Dictionary<CustomTextType, string> customTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ToCultureCodeSpecificDictionary(deliverypointgroup.CustomTextCollection, entity.CompanyEntity.CultureCode);

                model.DeliverypointgroupId = deliverypointgroup.DeliverypointgroupId;
                model.PingOfLifeInterval = 30; // PingOfLifeInterval needs to be placed as constant in Otoucho
                model.Locale = deliverypointgroup.Locale;
                model.Pincode = deliverypointgroup.Pincode;
                model.PincodeSU = deliverypointgroup.PincodeSU;
                model.PincodeGM = deliverypointgroup.PincodeGM;
                model.UseManualDeliverypoint = deliverypointgroup.UseManualDeliverypoint;
                model.UseManualDeliverypointEncryption = deliverypointgroup.UseManualDeliverypointEncryption;
                model.EncryptionSalt = deliverypointgroup.EncryptionSalt;
                model.HideDeliverypointNumbers = deliverypointgroup.HideDeliverypointNumber;
                model.HidePrices = deliverypointgroup.HidePrices;
                model.ClearSessionOnTimeout = deliverypointgroup.ClearSessionOnTimeout;
                model.ResetTimeout = (deliverypointgroup.ResetTimeout > 0 ? deliverypointgroup.ResetTimeout : 5);
                model.ScreenTimeoutInteval = deliverypointgroup.ScreenTimeoutInterval;
                model.ReorderNotificationEnabled = deliverypointgroup.ReorderNotificationEnabled;
                model.ReorderNotificationInterval = deliverypointgroup.ReorderNotificationInterval;
                model.DeviceActivationRequired = false;
                model.DefaultProductFullView = deliverypointgroup.DefaultProductFullView;
                model.MarketingSurveyUrl = deliverypointgroup.MarketingSurveyUrl;
                model.HotelUrl1 = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupHotelUrl1);
                model.HotelUrl1Caption = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupHotelUrl1Caption);
                model.HotelUrl1Zoom = deliverypointgroup.HotelUrl1Zoom ?? 0;
                model.HotelUrl2 = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupHotelUrl2);
                model.HotelUrl2Caption = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupHotelUrl2Caption);
                model.HotelUrl2Zoom = deliverypointgroup.HotelUrl2Zoom ?? 0;
                model.HotelUrl3 = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupHotelUrl3);
                model.HotelUrl3Caption = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupHotelUrl3Caption);
                model.HotelUrl3Zoom = deliverypointgroup.HotelUrl3Zoom ?? 0;
                model.ReorderNotificationAnnouncementId = deliverypointgroup.ReorderNotificationAnnouncementId ?? 0;
                model.DimLevelDull = deliverypointgroup.DimLevelDull;
                model.DimLevelMedium = deliverypointgroup.DimLevelMedium > 0 ? deliverypointgroup.DimLevelMedium : 30;
                model.DimLevelBright = deliverypointgroup.DimLevelBright > 0 ? deliverypointgroup.DimLevelBright : 100;
                model.DockedDimLevelDull = deliverypointgroup.DockedDimLevelDull;
                model.DockedDimLevelMedium = deliverypointgroup.DockedDimLevelMedium > 0 ? deliverypointgroup.DockedDimLevelMedium : 50;
                model.DockedDimLevelBright = deliverypointgroup.DockedDimLevelBright > 0 ? deliverypointgroup.DockedDimLevelBright : 100;
                model.PowerSaveTimeout = deliverypointgroup.PowerSaveTimeout > 0 ? deliverypointgroup.PowerSaveTimeout : 5;
                model.PowerSaveLevel = deliverypointgroup.PowerSaveLevel > 0 ? deliverypointgroup.PowerSaveLevel : 30;
                model.OutOfChargeLevel = deliverypointgroup.OutOfChargeLevel > 0 ? deliverypointgroup.OutOfChargeLevel : 10;
                model.OutOfChargeTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOutOfChargeTitle);
                model.OutOfChargeMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOutOfChargeText);
                model.OrderProcessedTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderProcessedTitle);
                model.OrderProcessedMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderProcessedText);
                model.OrderFailedTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderFailedTitle);
                model.OrderFailedMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderFailedText);
                model.OrderSavingTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderSavingTitle);
                model.OrderSavingMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderSavingText);
                model.OrderCompletedTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderCompletedTitle);
                model.OrderCompletedMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderCompletedText);
                model.OrderCompletedEnabled = deliverypointgroup.OrderCompletedEnabled;
                model.ServiceProcessedTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupServiceProcessedTitle);
                model.ServiceProcessedMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupServiceProcessedText);
                model.ServiceSavingTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupServiceSavingTitle);
                model.ServiceSavingMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupServiceSavingText);
                model.RatingProcessedTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupRatingProcessedTitle);
                model.RatingProcessedMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupRatingProcessedText);
                model.RatingSavingTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupRatingSavingTitle);
                model.RatingSavingMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupRatingSavingText);
                model.DailyOrderReset = !string.IsNullOrEmpty(deliverypointgroup.DailyOrderReset) ? deliverypointgroup.DailyOrderReset : "1100";
                model.SleepTime = deliverypointgroup.SleepTime;
                model.WakeUpTime = deliverypointgroup.WakeUpTime;
                model.OrderStatusOnthecaseEnabled = deliverypointgroup.OrderProcessingNotificationEnabled;
                model.OrderStatusOnthecaseTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderProcessingNotificationTitle);
                model.OrderStatusOnthecaseMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderProcessingNotificationText);
                model.OrderStatusCompleteEnabled = deliverypointgroup.OrderProcessedNotificationEnabled;
                model.OrderStatusCompleteTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderProcessedNotificationTitle);
                model.OrderStatusCompleteMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderProcessedNotificationText);
                model.ServiceRequestStatusOnTheCaseTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupServiceProcessingNotificationTitle);
                model.ServiceRequestStatusOnTheCaseMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupServiceProcessingNotificationText);
                model.ServiceRequestStatusCompleteTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupServiceProcessedNotificationTitle);
                model.ServiceRequestStatusCompleteMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupServiceProcessedNotificationText);
                model.FreeformMessageTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupFreeformMessageTitle);
                model.WifiSecurityMethod = deliverypointgroup.WifiSecurityMethod;
                model.WifiSsid = deliverypointgroup.WifiSsid;
                model.WifiPassword = deliverypointgroup.WifiPassword;
                model.WifiEapMode = deliverypointgroup.WifiEapMode;
                model.WifiPhase2Authentication = deliverypointgroup.WifiPhase2Authentication;
                model.WifiIdentity = deliverypointgroup.WifiIdentity;
                model.WifiAnonymousIdentity = deliverypointgroup.WifiAnonymousIdentify;
                model.HomepageSlideshowInterval = deliverypointgroup.HomepageSlideshowInterval;
                model.DeliverypointCaption = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupDeliverypointCaption);
                model.OrderingNotAvailableMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupOrderingNotAvailableText);
                model.UIModeId = deliverypointgroup.UIModeId ?? 0;
                model.PrintingEnabled = deliverypointgroup.GooglePrinterId.Length > 0;
                model.IsChargerRemovedDialogEnabled = deliverypointgroup.IsChargerRemovedDialogEnabled;
                model.ChargerRemovedDialogTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupChargerRemovedTitle);
                model.ChargerRemovedDialogText = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupChargerRemovedText);
                model.IsChargerRemovedReminderDialogEnabled = deliverypointgroup.IsChargerRemovedReminderDialogEnabled;
                model.ChargerRemovedReminderDialogTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupChargerRemovedReminderTitle);
                model.ChargerRemovedReminderDialogText = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupChargerRemovedReminderText);
                model.PmsDeviceLockedTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupPmsDeviceLockedTitle);
                model.PmsDeviceLockedText = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupPmsDeviceLockedText);
                model.PmsCheckinFailedTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupPmsCheckinFailedTitle);
                model.PmsCheckinFailedText = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupPmsCheckinFailedText);
                model.PmsRestartTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupPmsRestartTitle);
                model.PmsRestartText = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupPmsRestartText);
                model.PmsWelcomeTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupPmsWelcomeTitle);
                model.PmsWelcomeText = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupPmsWelcomeText);
                model.PmsCheckoutApproveText = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupPmsCheckoutApproveText);
                model.PmsWelcomeTimeoutMinutes = deliverypointgroup.PmsWelcomeTimeoutMinutes;
                model.PmsCheckoutCompleteTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupPmsCheckoutCompleteTitle);
                model.PmsCheckoutCompleteText = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupPmsCheckoutCompleteText);
                model.PmsCheckoutCompleteTimeoutMinutes = deliverypointgroup.PmsCheckoutCompleteTimeoutMinutes;
                model.ScreenOffMode = deliverypointgroup.ScreenOffMode;
                model.PowerButtonHardBehaviour = deliverypointgroup.PowerButtonHardBehaviour;
                model.PowerButtonSoftBehaviour = deliverypointgroup.PowerButtonSoftBehaviour;
                model.RoomserviceCharge = deliverypointgroup.RoomserviceCharge ?? 0;
                model.GooglePrinterId = deliverypointgroup.GooglePrinterId;
                model.EmailDocumentEnabled = deliverypointgroup.EmailDocumentRouteId.HasValue;
                model.ScreensaverMode = (int)deliverypointgroup.ScreensaverMode;
                model.TurnOffPrivacyTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupTurnOffPrivacyTitle);
                model.TurnOffPrivacyText = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupTurnOffPrivacyText);
                model.ItemCurrentlyUnavailableText = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupItemCurrentlyUnavailableText);
                model.ServiceFailedTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupServiceFailedTitle);
                model.ServiceFailedMessage = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupServiceFailedText);
                model.AlarmSetWhileNotChargingTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingTitle);
                model.AlarmSetWhileNotChargingText = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupAlarmSetWhileNotChargingText);
                model.DeliveryLocationMismatchTitle = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupDeliveryLocationMismatchTitle);
                model.DeliveryLocationMismatchText = customTexts.GetValueOrDefault(CustomTextType.DeliverypointgroupDeliveryLocationMismatchText);
                model.PriceScheduleId = deliverypointgroup.PriceScheduleId ?? 0;
                model.IsOrderitemAddedDialogEnabled = deliverypointgroup.IsOrderitemAddedDialogEnabled;
                model.IsClearBasketDialogEnabled = deliverypointgroup.IsClearBasketDialogEnabled;
                model.BrowserAgeVerificationEnabled = deliverypointgroup.BrowserAgeVerificationEnabled;
                model.BrowserAgeVerificationLayout = (int)deliverypointgroup.BrowserAgeVerificationLayout;
                model.RestartApplicationDialogEnabled = deliverypointgroup.RestartApplicationDialogEnabled;
                model.RebootDeviceDialogEnabled = deliverypointgroup.RebootDeviceDialogEnabled;
                model.RestartTimeoutSeconds = deliverypointgroup.RestartTimeoutSeconds;
                model.CraveAnalytics = deliverypointgroup.CraveAnalytics;

                // Custom texts
                model.CustomTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(deliverypointgroup.CustomTextCollection);

                // Media
                if (deliverypointgroup.CompanyEntity.SystemType == SystemType.Crave)
                {
                    if (this.mediaCache == null)
                    {
                        List<Media> media = new List<Media>();
                        deliverypointgroup.MediaCollection.Sort(MediaFields.SortOrder.FieldIndex, System.ComponentModel.ListSortDirection.Ascending);
                        foreach (MediaEntity t in deliverypointgroup.MediaCollection)
                        {
                            media.AddRange(Obymobi.Logic.HelperClasses.MediaHelper.CreateMediaModelFromEntity(t));
                        }

                        model.Media = media.ToArray();
                    }
                    else
                    {
                        model.Media = this.mediaCache;
                    }
                }

                // Entertainment categories
                Entertainmentcategory[] entertainmentcategories;
                if (entertainmentcategoriesCache == null || entertainmentcategoriesCache.TryGetValue(deliverypointgroup.DeliverypointgroupId, out entertainmentcategories))
                {
                    entertainmentcategories = Obymobi.Logic.HelperClasses.EntertainmentHelper.GetEntertainmentcategories(0, deliverypointgroup.DeliverypointgroupId);

                    if (entertainmentcategoriesCache != null)
                        entertainmentcategoriesCache.Add(deliverypointgroup.DeliverypointgroupId, entertainmentcategories);
                }
                model.Entertainmentcategories = entertainmentcategories;

                // Themes
                List<UITheme> themes = new List<UITheme>();
                if (!deliverypointgroup.UIThemeEntity.IsNew)
                {
                    themes.Add(this.uiThemeEntityConverter.ConvertEntityToModel(deliverypointgroup.UIThemeEntity));
                }
                model.UIThemes = themes.ToArray();

                model.CraveAnalyticsSas = string.Empty;
                model.CraveAnalyticsUrl = string.Empty;
            }

            return model;
        }

        public override Client[] ConvertEntityCollectionToModelArray(ClientCollection entityCollection)
        {
            Dictionary<int, Entertainmentcategory[]> entertainmentcategoriesCache = new Dictionary<int, Entertainmentcategory[]>();
            Dictionary<int, DeliverypointgroupEntity> deliverypointgroupCache = new Dictionary<int, DeliverypointgroupEntity>();
            Media[] mediaCache = new Media[0];

            ClientEntityConverter clientEntityConverter = new ClientEntityConverter(entertainmentcategoriesCache, deliverypointgroupCache, mediaCache);

            Client[] clientArray = new Client[entityCollection.Count];
            for (int i = 0; i < entityCollection.Count; i++)
            {
                ClientEntity clientEntity = entityCollection[i];
                clientArray[i] = clientEntityConverter.ConvertEntityToModel(clientEntity); // Use cache
            }

            return clientArray;
        }
    }
}
