﻿using System.Collections.Generic;
using System.Linq;
using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class AlterationProductEntityConverter : EntityConverterBase<AlterationProductEntity, AlterationProductCollection, AlterationProduct>
    {
        public override AlterationProduct ConvertEntityToModel(AlterationProductEntity entity)
        {
            if (entity.ProductEntity.VisibilityType == VisibilityType.Never)
            {
                // Linked product isn't visible
                return null;
            }
                
            return new AlterationProduct
            {
                AlterationProductId = entity.AlterationProductId,
                AlterationId = entity.AlterationId,
                ProductId = entity.ProductId,
                Visible = entity.Visible,
                SortOrder = entity.SortOrder
            };
        }

        public override AlterationProduct[] ConvertEntityCollectionToModelArray(AlterationProductCollection entityCollection)
        {
            List<AlterationProduct> alterationProducts = new List<AlterationProduct>();

            if (entityCollection.Count > 0)
            {
                List<AlterationProductEntity> visibleAlterationProducts = entityCollection.Where(a => a.Visible).OrderBy(a => a.SortOrder).ToList();
                foreach (AlterationProductEntity alterationProductEntity in visibleAlterationProducts)
                {
                    AlterationProduct alterationProduct = this.ConvertEntityToModel(alterationProductEntity);
                    if (alterationProduct != null)
                    {
                        alterationProducts.Add(alterationProduct);
                    }
                }
            }
            return alterationProducts.ToArray();
        }
    }
}
