﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class CloudStorageAccountEntityConverter : EntityConverterBase<CloudStorageAccountEntity, CloudStorageAccount>
    {
        public override CloudStorageAccount ConvertEntityToModel(CloudStorageAccountEntity entity)
        {
            CloudStorageAccount model = new CloudStorageAccount();
            model.CloudStorageAccountId = entity.CloudStorageAccountId;
            model.Type = (int)entity.Type;
            model.AccountName = entity.AccountName;
            model.AccountPassword = entity.AccountPassword;

            return model;
        }
    }
}
