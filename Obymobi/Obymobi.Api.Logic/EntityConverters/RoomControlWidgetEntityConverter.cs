﻿using System.Collections.Generic;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class RoomControlWidgetEntityConverter : EntityConverterBase<RoomControlWidgetEntity, RoomControlWidget>
    {
        private readonly string parent;
        private readonly string customTextParent;
        private List<int> infraredConfigurationIds;

        public RoomControlWidgetEntityConverter(List<int> infraredConfigurationIds, string parent, string customTextParent)
        {
            this.infraredConfigurationIds = infraredConfigurationIds;
            this.parent = parent;
            this.customTextParent = customTextParent;
        }

        public override RoomControlWidget ConvertEntityToModel(RoomControlWidgetEntity entity)
        {
            RoomControlWidget model = new RoomControlWidget
            {
                RoomControlWidgetId = entity.RoomControlWidgetId,
                Caption = entity.Caption,
                Type = (int)entity.Type,
                SortOrder = entity.SortOrder,
                RoomControlComponentId1 = entity.RoomControlComponentId1 ?? -1,
                RoomControlComponentId2 = entity.RoomControlComponentId2 ?? -1,
                RoomControlComponentId3 = entity.RoomControlComponentId3 ?? -1,
                RoomControlComponentId4 = entity.RoomControlComponentId4 ?? -1,
                RoomControlComponentId5 = entity.RoomControlComponentId5 ?? -1,
                RoomControlComponentId6 = entity.RoomControlComponentId6 ?? -1,
                RoomControlComponentId7 = entity.RoomControlComponentId7 ?? -1,
                RoomControlComponentId8 = entity.RoomControlComponentId8 ?? -1,
                RoomControlComponentId9 = entity.RoomControlComponentId9 ?? -1,
                RoomControlComponentId10 = entity.RoomControlComponentId10 ?? -1,
                Visible = entity.Visible,
                FieldValue1 = entity.FieldValue1,
                FieldValue2 = entity.FieldValue2,
                FieldValue3 = entity.FieldValue3,
                FieldValue4 = entity.FieldValue4,
                FieldValue5 = entity.FieldValue5,
                FieldValue6 = entity.FieldValue6,
                FieldValue7 = entity.FieldValue7,
                FieldValue8 = entity.FieldValue8,
                FieldValue9 = entity.FieldValue9,
                FieldValue10 = entity.FieldValue10,
                Parent = this.parent,
            };

            if (entity.InfraredConfigurationId.HasValue)
            {
                int infraredConfigurationId = entity.InfraredConfigurationId.Value;

                model.InfraredConfigurationId = infraredConfigurationId;
                if (!this.infraredConfigurationIds.Contains(infraredConfigurationId))
                    this.infraredConfigurationIds.Add(infraredConfigurationId);
            }
            else
            {
                model.InfraredConfigurationId = -1;
            }

            model.CustomTexts = CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection, this.customTextParent);

            return model;
        }
    }
}
