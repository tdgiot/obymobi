﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class UIModeEntityConverter : EntityConverterBase<UIModeEntity, UIMode>
    {
        public override UIMode ConvertEntityToModel(UIModeEntity entity)
        {
            UIMode model = new UIMode();
            model.UIModeId = entity.UIModeId;
            model.Name = entity.Name;
            model.ShowServiceOptions = entity.ShowServiceOptions;
            model.ShowRequestBill = entity.ShowRequestBill;
            model.ShowDeliverypoint = entity.ShowDeliverypoint;
            model.ShowClock = entity.ShowClock;
            model.ShowBattery = entity.ShowBattery;
            model.ShowBrightness = entity.ShowBrightness;
            model.ShowFullscreenEyecatcher = entity.ShowFullscreenEyecatcher;
            model.ShowFooterLogo = entity.ShowFooterLogo;
            model.Type = (int)entity.Type;
            model.DefaultUITabId = entity.DefaultUITabId ?? 0;

            List<UITab> uiTabs = new List<UITab>();
            foreach (UITabEntity uiTabEntity in entity.UITabCollection)
            {
                uiTabs.Add(new UITabEntityConverter().ConvertEntityToModel(uiTabEntity));
            }
            model.UITabs = uiTabs.OrderBy(order => order.SortOrder).ToArray();

            List<UIFooterItem> uiFooterItems = new List<UIFooterItem>();
            foreach (UIFooterItemEntity uiFooterItemEntity in entity.UIFooterItemCollection.Where(x => x.Visible))
            {
                uiFooterItems.Add(new UIFooterItemEntityConverter().ConvertEntityToModel(uiFooterItemEntity));
            }
            model.UIFooterItems = uiFooterItems.OrderBy(o => o.SortOrder).ToArray();

            return model;
        }
    }
}
