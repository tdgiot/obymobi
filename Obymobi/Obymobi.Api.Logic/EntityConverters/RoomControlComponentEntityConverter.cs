﻿using System.Collections.Generic;
using Dionysos;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class RoomControlComponentEntityConverter : EntityConverterBase<RoomControlComponentEntity, RoomControlComponent>
    {
        private List<int> infraredConfigurationIds;

        public RoomControlComponentEntityConverter(List<int> infraredConfigurationIds)
        {
            this.infraredConfigurationIds = infraredConfigurationIds;
        }

        public override RoomControlComponent ConvertEntityToModel(RoomControlComponentEntity entity)
        {
            RoomControlComponent model = new RoomControlComponent
            {
                RoomControlComponentId = entity.RoomControlComponentId,
                Name = entity.Name,
                NameSystem = entity.NameSystem,
                Type = (int)entity.Type,
                SortOrder = entity.SortOrder,
                Visible = entity.Visible,
                FieldValue1 = entity.FieldValue1,
                FieldValue2 = entity.FieldValue2,
                FieldValue3 = entity.FieldValue3,
                FieldValue4 = entity.FieldValue4,
                FieldValue5 = entity.FieldValue5,
                ControllerId = entity.ControllerId.IsNullOrWhiteSpace() ? "" : entity.ControllerId,
            };

            if (entity.InfraredConfigurationId.HasValue)
            {
                int infraredConfigurationId = entity.InfraredConfigurationId.Value;

                model.InfraredConfigurationId = infraredConfigurationId;
                if (!this.infraredConfigurationIds.Contains(infraredConfigurationId))
                    this.infraredConfigurationIds.Add(infraredConfigurationId);
            }
            else
            {
                model.InfraredConfigurationId = -1;
            }

            model.CustomTexts = CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection, "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS_ROOMCONTROLCOMPONENTS");

            return model;
        }
    }
}
