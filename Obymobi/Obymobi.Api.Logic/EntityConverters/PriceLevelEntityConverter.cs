﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class PriceLevelEntityConverter : EntityConverterBase<PriceLevelEntity, PriceLevel>
    {
        private readonly PriceLevelItemEntityConverter priceLevelItemEntityConverter = new PriceLevelItemEntityConverter();

        public override PriceLevel ConvertEntityToModel(PriceLevelEntity entity)
        {
            PriceLevel model = new PriceLevel
            {
                PriceLevelId = entity.PriceLevelId,
                Name = entity.Name
            };

            model.PriceLevelItems = this.priceLevelItemEntityConverter.ConvertEntityCollectionToModelArray(entity.PriceLevelItemCollection);

            return model;
        }
    }
}
