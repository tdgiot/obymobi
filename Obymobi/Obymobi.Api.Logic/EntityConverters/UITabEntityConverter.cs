﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class UITabEntityConverter : EntityConverterBase<UITabEntity, UITab>
    {
        public override UITab ConvertEntityToModel(UITabEntity entity)
        {
            UITab model = new UITab();
            model.UITabId = entity.UITabId;
            model.Caption = entity.Caption;
            model.Type = entity.Type;
            model.CategoryId = entity.CategoryId ?? 0;
            model.EntertainmentId = entity.EntertainmentId ?? 0;
            model.SiteId = entity.SiteId ?? 0;
            model.URL = entity.URL;
            model.Zoom = entity.Zoom;
            model.Width = entity.Width;
            model.SortOrder = entity.SortOrder;
            model.Visible = entity.Visible;
            model.RestrictedAccess = entity.RestrictedAccess;
            model.AllCategoryVisible = entity.AllCategoryVisible;
            model.ShowPmsMessagegroups = entity.ShowPmsMessagegroups;
            model.MapId = entity.MapId ?? 0;

            // UIWidgets
            List<UIWidget> uiWidgets = new List<UIWidget>();
            foreach (UIWidgetEntity uiWidgetEntity in entity.UIWidgetCollection)
            {
                if (uiWidgetEntity.IsVisible)
                {
                    uiWidgets.Add(new UIWidgetEntityConverter().ConvertEntityToModel(uiWidgetEntity));
                }
            }
            model.UIWidgets = uiWidgets.OrderBy(widget => widget.SortOrder).ToArray();

            // Custom texts
            model.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToModelArray(entity.CustomTextCollection);

            return model;          
        }
    }
}
