﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class RoomControlConfigurationEntityConverter : EntityConverterBase<RoomControlConfigurationEntity, RoomControlConfiguration>
    {
        private readonly RoomControlAreaEntityConverter roomControlAreaEntityConverter;
        private List<int> infraredConfigurationIds;

        public RoomControlConfigurationEntityConverter()
        {
            this.infraredConfigurationIds = new List<int>();
            this.roomControlAreaEntityConverter = new RoomControlAreaEntityConverter(this.infraredConfigurationIds);
        }

        public override RoomControlConfiguration ConvertEntityToModel(RoomControlConfigurationEntity entity)
        {
            RoomControlConfiguration model = new RoomControlConfiguration
            {
                RoomControlConfigurationId = entity.RoomControlConfigurationId,
                Name = entity.Name
            };

            // Room control areas
            List<RoomControlArea> roomControlAreas = new List<RoomControlArea>();
            if (entity.RoomControlAreaCollection.Count > 0)
            {
                // FO: Funky oneliner, needs to be replaced in the future when there's more time
                List<RoomControlAreaEntity> areas = entity.RoomControlAreaCollection.Where(a => a.Visible).OrderBy(a => a.SortOrder).ToList();
                foreach (RoomControlAreaEntity roomControlAreaEntity in areas)
                {
                    roomControlAreas.Add(roomControlAreaEntityConverter.ConvertEntityToModel(roomControlAreaEntity));
                }
            }
            model.RoomControlAreas = roomControlAreas.ToArray();

            // Infrared configurations
            model.InfraredConfigurations = InfraredConfigurationHelper.GetInfraredConfigurations(this.infraredConfigurationIds);

            return model;
        }
    }
}
