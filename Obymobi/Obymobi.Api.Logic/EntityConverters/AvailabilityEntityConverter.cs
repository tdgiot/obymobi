﻿using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class AvailabilityEntityConverter : EntityConverterBase<AvailabilityEntity, Availability>
    {
        public override Availability ConvertEntityToModel(AvailabilityEntity entity)
        {
            Availability model = new Availability();
            model.AvailabilityId = entity.AvailabilityId;
            model.Name = entity.Name;
            model.Status = entity.Status;
            model.ActionEntertainmentId = entity.ActionEntertainmentId ?? 0;
            model.ActionProductId = entity.ActionProductCategoryId.HasValue ? entity.ProductCategoryEntity.ProductId : 0;
            model.ActionCategoryId = entity.ActionProductCategoryId.HasValue ? (entity.ProductCategoryEntity.CategoryId + CategoryHelper.CATEGORY_ID_ADDITION) : 0;
            if (entity.ActionCategoryId.HasValue)
            {
                model.ActionCategoryId = entity.ActionCategoryId.Value + CategoryHelper.CATEGORY_ID_ADDITION;
            }
            model.ActionSiteId = entity.ActionSiteId ?? 0;
            model.ActionPageId = entity.ActionPageId ?? 0;
            model.Url = entity.Url;
            model.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToModelArray(entity.CustomTextCollection);

            return model;
        }
    }
}
