﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class CompanyCurrencyEntityConverter : EntityConverterBase<CompanyCurrencyEntity, CompanyCurrency>
    {
        public override CompanyCurrency ConvertEntityToModel(CompanyCurrencyEntity entity)
        {
            CompanyCurrency model = new CompanyCurrency();
            model.CompanyCurrencyId = entity.CompanyCurrencyId;
            model.CurrencyCode = entity.CurrencyCode;

            if (!entity.OverwriteSymbol.IsNullOrWhiteSpace())
            {
                model.Symbol = entity.OverwriteSymbol;
            }

            if (!entity.CurrencyCode.IsNullOrWhiteSpace())
            {
                Currency currency = Currency.Mappings[entity.CurrencyCode];

                if (model.Symbol.IsNullOrWhiteSpace())
                {
                    model.Symbol = currency.Symbol;
                }

                model.Name = currency.Name;
            }

            model.ExchangeRate = entity.ExchangeRate;

            return model;
        }
    }
}
