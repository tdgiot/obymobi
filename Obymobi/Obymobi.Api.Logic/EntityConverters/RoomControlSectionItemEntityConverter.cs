﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Enums;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class RoomControlSectionItemEntityConverter : EntityConverterBase<RoomControlSectionItemEntity, RoomControlSectionItem>
    {
        private readonly StationListEntityConverter stationListEntityConverter = new StationListEntityConverter();
        private readonly RoomControlWidgetEntityConverter roomControlWidgetEntityConverter;

        private List<int> infraredConfigurationIds;

        public RoomControlSectionItemEntityConverter(List<int> infraredConfigurationIds)
        {
            this.infraredConfigurationIds = infraredConfigurationIds;
            this.roomControlWidgetEntityConverter = new RoomControlWidgetEntityConverter(infraredConfigurationIds, "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS_ROOMCONTROLSECTIONITEMS", "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS_ROOMCONTROLSECTIONITEMS_ROOMCONTROLWIDGETS");
        }

        public override RoomControlSectionItem ConvertEntityToModel(RoomControlSectionItemEntity entity)
        {
            RoomControlSectionItem model = new RoomControlSectionItem
            {
                RoomControlSectionItemId = entity.RoomControlSectionItemId,
                Name = entity.Name,
                Type = (int)entity.Type,
                FieldValue1 = entity.FieldValue1,
                FieldValue2 = entity.FieldValue2,
                FieldValue3 = entity.FieldValue3,
                FieldValue4 = entity.FieldValue4,
                FieldValue5 = entity.FieldValue5,
                Visible = entity.Visible,
            };

            if (entity.InfraredConfigurationId.HasValue)
            {
                int infraredConfigurationId = entity.InfraredConfigurationId.Value;

                model.InfraredConfigurationId = infraredConfigurationId;
                if (!this.infraredConfigurationIds.Contains(infraredConfigurationId))
                    this.infraredConfigurationIds.Add(infraredConfigurationId);
            }
            else
            {
                model.InfraredConfigurationId = -1;
            }

            model.CustomTexts = CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection, "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS_ROOMCONTROLSECTIONITEMS");

            // Station list
            if (entity.Type == RoomControlSectionItemType.StationListing)
            {
                List<StationList> stationLists = new List<StationList>();
                if (entity.StationListId.HasValue)
                {
                    stationLists.Add(this.stationListEntityConverter.ConvertEntityToModel(entity.StationListEntity));
                }
                model.StationLists = stationLists.ToArray();
            }

            // Room control widgets
            List<RoomControlWidget> roomControlWidgets = new List<RoomControlWidget>();
            if (entity.RoomControlWidgetCollection.Count > 0)
            {
                List<RoomControlWidgetEntity> widgets = entity.RoomControlWidgetCollection.Where(c => c.Visible).OrderBy(c => c.SortOrder).ToList();
                foreach (RoomControlWidgetEntity roomControlWidgetEntity in widgets)
                {
                    roomControlWidgets.Add(this.roomControlWidgetEntityConverter.ConvertEntityToModel(roomControlWidgetEntity));
                }
            }
            model.RoomControlWidgets = roomControlWidgets.ToArray();

            // Media
            List<Media> media = new List<Media>();
            foreach (MediaEntity mediaEntity in entity.MediaCollection)
            {
                media.AddRange(MediaHelper.CreateMediaModelFromEntity(mediaEntity, "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS_ROOMCONTROLSECTIONITEMS", "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS_ROOMCONTROLSECTIONITEMS_MEDIA"));
            }
            model.Media = media.ToArray();

            return model;
        }
    }
}
