﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class PriceScheduleEntityConverter : EntityConverterBase<PriceScheduleEntity, PriceSchedule>
    {
        private readonly PriceScheduleItemEntityConverter priceScheduleItemEntityConverter = new PriceScheduleItemEntityConverter();
        private readonly PriceLevelEntityConverter priceLevelEntityConverter = new PriceLevelEntityConverter();

        public override PriceSchedule ConvertEntityToModel(PriceScheduleEntity entity)
        {
            PriceSchedule model = new PriceSchedule
            {
                PriceScheduleId = entity.PriceScheduleId,
                Name = entity.Name
            };

            // Key: PriceLevelId, Value: PriceLevelEntity
            Dictionary<int, PriceLevelEntity> priceLevelEntities = new Dictionary<int, PriceLevelEntity>();

            // PriceScheduleItems
            List<PriceScheduleItem> priceScheduleItems = new List<PriceScheduleItem>();
            foreach (PriceScheduleItemEntity priceScheduleItemEntity in entity.PriceScheduleItemCollection)
            {
                priceScheduleItems.Add(this.priceScheduleItemEntityConverter.ConvertEntityToModel(priceScheduleItemEntity));

                if (!priceLevelEntities.ContainsKey(priceScheduleItemEntity.PriceLevelId))
                {
                    priceLevelEntities.Add(priceScheduleItemEntity.PriceLevelId, priceScheduleItemEntity.PriceLevelEntity);
                }
            }
            model.PriceScheduleItems = priceScheduleItems.ToArray();

            // PriceLevels
            List<PriceLevel> priceLevels = new List<PriceLevel>();
            foreach (PriceLevelEntity priceLevelEntity in priceLevelEntities.Values)
            {
                priceLevels.Add(this.priceLevelEntityConverter.ConvertEntityToModel(priceLevelEntity));
            }
            model.PriceLevels = priceLevels.ToArray();

            return model;
        }
    }
}
