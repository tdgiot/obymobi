﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class UIThemeColorEntityConverter : EntityConverterBase<UIThemeColorEntity, UIThemeColor>
    {
        public override UIThemeColor ConvertEntityToModel(UIThemeColorEntity entity)
        {
            UIThemeColor model = new UIThemeColor
            {
                UIThemeColorId = entity.UIThemeId,
                UIThemeId = entity.UIThemeId,
                Type = (int)entity.Type,
                Color = entity.Color
            };

            return model;
        }
    }
}
