﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class StationEntityConverter : EntityConverterBase<StationEntity, Station>
    {
        public override Station ConvertEntityToModel(StationEntity entity)
        {
            Station model = new Station
            {
                StationId = entity.StationId,
                Caption = entity.Name,
                Scene = entity.Scene,
                SuccessMessage = entity.SuccessMessage,
                Description = entity.Description,
                Url = entity.Url,
                Channel = entity.Channel
            };

            model.CustomTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection, "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS_ROOMCONTROLSECTIONITEMS_STATIONLISTS_STATIONS");

            // Media
            List<Obymobi.Logic.Model.Media> media = new List<Obymobi.Logic.Model.Media>();
            foreach (MediaEntity mediaEntity in entity.MediaCollection)
            {
                media.AddRange(Obymobi.Logic.HelperClasses.MediaHelper.CreateMediaModelFromEntity(mediaEntity, "ROOMCONTROLAREAS_ROOMCONTROLSECTIONS_ROOMCONTROLSECTIONITEMS_STATIONLISTS_STATIONS", "UIMODES_UITABS_ROOMCONTROLSECTIONS_ROOMCONTROLSECTIONITEMS_STATIONLISTS_STATIONS_MEDIA"));
            }
            model.Media = media.ToArray();

            return model;
        }
    }
}
