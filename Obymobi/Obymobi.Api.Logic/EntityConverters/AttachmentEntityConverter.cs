﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class AttachmentEntityConverter : EntityConverterBase<AttachmentEntity, Attachment>
    {
        private readonly MediaEntityConverter mediaEntityConverter = new MediaEntityConverter();

        public override Attachment ConvertEntityToModel(AttachmentEntity entity)
        {
            Attachment model = new Attachment
            {
                AttachmentId = entity.AttachmentId,
                ProductId = entity.ProductId ?? -1,
                PageId = entity.PageId ?? -1,
                Name = entity.Name,
                Type = (int)entity.Type,
                Url = entity.Url,
                AllowAllDomains = entity.AllowAllDomains,
                AllowedDomains = entity.AllowedDomains,
                CustomTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection)
            };

            // Media
            List<Media> media = new List<Media>();
            foreach (MediaEntity mediaEntity in entity.MediaCollection)
            {
                media.AddRange(this.mediaEntityConverter.ConvertEntityToModel(mediaEntity, "ATTACHMENTS", "ATTACHMENTS_MEDIA"));
            }
            model.Media = media.ToArray();

            return model;
        }
    }
}
