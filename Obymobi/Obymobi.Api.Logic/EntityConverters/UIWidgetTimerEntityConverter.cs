﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class UIWidgetTimerEntityConverter : EntityConverterBase<UIWidgetTimerEntity, UIWidgetTimer>
    {
        public override UIWidgetTimer ConvertEntityToModel(UIWidgetTimerEntity entity)
        {
            UIWidgetTimer model = new UIWidgetTimer
            {
                UIWidgetTimerId = entity.UIWidgetTimerId,
                CountToTime = entity.CountToTime.ToString("dd-MM-yyyy HH:mm:ss"),
                CountToDate = entity.CountToDate.HasValue ? entity.CountToDate.Value.ToString("dd-MM-yyyy HH:mm:ss") : string.Empty
            };

            return model;
        }
    }
}
