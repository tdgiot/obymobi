﻿using Dionysos;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class MediaEntityConverter : EntityConverterBaseWithParent<MediaEntity, List<Media>>
    {
        public IEnumerable<Media> ConvertEntityCollectionToModels(MediaCollection mediaCollection, string parent = "", string mediaParent = "")
        {
            List<Media> mediaList = new List<Media>();

            foreach (MediaEntity mediaEntity in mediaCollection)
            {
                mediaList.AddRange(this.ConvertEntityToModel(mediaEntity, parent, mediaParent));
            }

            return mediaList;
        }

        public override List<Media> ConvertEntityToModel(MediaEntity mediaEntity, string parent = "", string mediaParent = "")
        {
            List<Media> mediaList = new List<Media>();

            foreach (MediaRatioTypeMediaEntity mediaRatio in mediaEntity.MediaRatioTypeMediaCollection)
            {
                Media media = this.CreateMediaModelFromMediaRatioTypeMedia(mediaEntity, mediaRatio, parent, mediaParent);
                if (media != null)
                {
                    media.GenericFile = mediaEntity.IsGeneric;
                    mediaList.Add(media);
                }
            }

            return mediaList;
        }

        public Media CreateMediaModelFromMediaRatioTypeMedia(MediaEntity mediaEntity, MediaRatioTypeMediaEntity mediaRatioTypeMediaEntity, string parent = "", string mediaParent = "")
        {
            var media = new Media();
            media.MediaId = mediaEntity.MediaId;
            media.Name = mediaEntity.Name;
            media.Parent = parent;

            string fileName = mediaRatioTypeMediaEntity.FileNameOnCdn;

            // Don't include images that don't have a filename.
            if (fileName.IsNullOrWhiteSpace())
                return null;

            media.MediaCultures = new MediaCultureEntityConverter().ConvertEntityCollectionToModelArray(mediaEntity.MediaCultureCollection, mediaParent.IsNullOrWhiteSpace() ? "" : mediaParent);
            media.AgnosticMediaId = mediaEntity.AgnosticMediaId.GetValueOrDefault(0);

            // GK Seemed double code
            string subDirectoryName = mediaRatioTypeMediaEntity.MediaRatioType != null ? MediaHelper.GetEntitySubDirectoryName(mediaRatioTypeMediaEntity) : string.Empty;
            media.FilePathRelativeToMediaPath = Path.Combine(subDirectoryName, fileName);
            media.MediaType = mediaRatioTypeMediaEntity.MediaType.GetValueOrDefault(0);
            media.ActionProductId = mediaEntity.ActionProductId.HasValue ? mediaEntity.ActionProductId.Value : 0;
            media.ActionCategoryId = mediaEntity.ActionCategoryId.HasValue ? mediaEntity.ActionCategoryId.Value : 0;
            media.ActionEntertainmentId = mediaEntity.ActionEntertainmentId.HasValue ? mediaEntity.ActionEntertainmentId.Value : 0;
            media.ActionEntertainmentcategoryId = mediaEntity.ActionEntertainmentcategoryId.HasValue ? mediaEntity.ActionEntertainmentcategoryId.Value : 0;
            media.ActionSiteId = mediaEntity.ActionSiteId.HasValue ? mediaEntity.ActionSiteId.Value : 0;
            media.ActionPageId = mediaEntity.ActionPageId.HasValue ? mediaEntity.ActionPageId.Value : 0;
            media.ActionUrl = mediaEntity.ActionUrl;
            media.SizeMode = (int)mediaEntity.SizeMode;
            media.ZoomLevel = mediaEntity.ZoomLevel;
            media.RelatedCompanyId = mediaEntity.RelatedCompanyId.HasValue ? mediaEntity.RelatedCompanyId.Value : 0;
            media.RelatedBrandId = mediaEntity.ProductId.HasValue ? mediaEntity.ProductEntity.BrandId.GetValueOrDefault(0) : 0;

            if (mediaRatioTypeMediaEntity.MediaRatioType != null) // Null happens when the MediaRatioType no longer exists.
                media.CdnFilePathRelativeToMediaPath = MediaHelper.GetMediaRatioTypeMediaPath(mediaRatioTypeMediaEntity);

            return media;
        }
    }
}
