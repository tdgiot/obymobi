﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class AmenityEntityConverter : EntityConverterBase<AmenityEntity, CompanyAmenityCollection, Amenity>
    {
        public override Amenity ConvertEntityToModel(AmenityEntity entity)
        {
            Amenity model = new Amenity();
            model.AmenityId = entity.AmenityId;
            model.Name = entity.Name;

            return model;
        }

        public override Amenity[] ConvertEntityCollectionToModelArray(CompanyAmenityCollection entityCollection)
        {
            List<Amenity> list = new List<Amenity>();

            foreach (CompanyAmenityEntity entity in entityCollection)
            {
                list.Add(this.ConvertEntityToModel(entity.AmenityEntity));
            }

            return list.ToArray();
        }
    }
}
