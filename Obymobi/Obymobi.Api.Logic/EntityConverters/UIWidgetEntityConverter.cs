﻿using Dionysos;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Obymobi.Api.Logic.HelperClasses;
using Obymobi.Api.Logic.Repositories;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class UIWidgetEntityConverter : EntityConverterBase<UIWidgetEntity, UIWidget>
    {
        private readonly UIWidgetTimerEntityConverter uiWidgetTimerEntityConverter = new UIWidgetTimerEntityConverter();
        private readonly UIWidgetAvailabilityEntityConverter uiWidgetAvailabilityEntityConverter = new UIWidgetAvailabilityEntityConverter();

        public override UIWidget ConvertEntityToModel(UIWidgetEntity entity)
        {
            UIWidget model = new UIWidget();
            model.UIWidgetId = entity.UIWidgetId;
            model.UITabId = entity.UITabId;
            model.Type = (int)entity.Type;
            model.Name = entity.Name;
            model.Caption = entity.Caption;
            model.SortOrder = entity.SortOrder;
            model.AdvertisementId = entity.AdvertisementId ?? 0;
            model.ProductId = entity.ProductCategoryId.HasValue ? entity.ProductCategoryEntity.ProductId : 0;
            model.CategoryId = entity.ProductCategoryId.HasValue ? (entity.ProductCategoryEntity.CategoryId + CategoryHelper.CATEGORY_ID_ADDITION) : 0;
            if (entity.CategoryId.HasValue)
            {
                model.CategoryId = entity.CategoryId.Value + CategoryHelper.CATEGORY_ID_ADDITION;
            }
            model.EntertainmentId = entity.EntertainmentId ?? 0;
            model.EntertainmentcategoryId = entity.EntertainmentcategoryId ?? 0;
            model.SiteId = entity.SiteId ?? 0;
            model.PageId = entity.PageId ?? 0;
            model.UITabType = entity.UITabType.HasValue ? (int)entity.UITabType.Value : 0;
            model.Url = entity.Url;
            model.RoomControlSectionType = entity.RoomControlType ?? 0;
            model.FieldValue1 = entity.FieldValue1;
            model.FieldValue2 = entity.FieldValue2;
            model.FieldValue3 = entity.FieldValue3;
            model.FieldValue4 = entity.FieldValue4;
            model.FieldValue5 = entity.FieldValue5;
            model.MessageLayoutType = (int)entity.MessageLayoutType;

            // Custom texts
            model.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToModelArray(entity.CustomTextCollection);

            // Timers
            model.UIWidgetTimers = this.uiWidgetTimerEntityConverter.ConvertEntityCollectionToModelArray(entity.UIWidgetTimerCollection);

            // Availabilities
            model.UIWidgetAvailabilitys = this.uiWidgetAvailabilityEntityConverter.ConvertEntityCollectionToModelArray(entity.UIWidgetAvailabilityCollection.OrderBy(x => x.SortOrder));

            // Media
            List<Media> media = new List<Media>();
            foreach (MediaEntity mediaEntity in entity.MediaCollection)
            {
                media.AddRange(new MediaEntityConverter().ConvertEntityToModel(mediaEntity, "UIMODES_UITABS_UIWIDGETS", "UIMODES_UITABS_UIWIDGETS_MEDIA"));
            }
            model.Media = media.ToArray();
            model.UIWidgetLanguages = null;

            return model;
        }
    }
}
