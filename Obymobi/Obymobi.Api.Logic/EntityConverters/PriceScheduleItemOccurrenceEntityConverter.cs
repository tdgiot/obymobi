﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class PriceScheduleItemOccurrenceEntityConverter : EntityConverterBase<PriceScheduleItemOccurrenceEntity, PriceScheduleItemOccurrence>
    {
        public override PriceScheduleItemOccurrence ConvertEntityToModel(PriceScheduleItemOccurrenceEntity entity)
        {
            PriceScheduleItemOccurrence model = new PriceScheduleItemOccurrence
            {
                PriceScheduleItemOccurrenceId = entity.PriceScheduleItemOccurrenceId,
                StartTime = entity.StartTime.HasValue ? entity.StartTime.Value.ToString("dd-MM-yyyy HH:mm:ss") : string.Empty,
                EndTime = entity.EndTime.HasValue ? entity.EndTime.Value.ToString("dd-MM-yyyy HH:mm:ss") : string.Empty,
                Recurring = entity.Recurring,
                RecurrenceType = entity.RecurrenceType,
                RecurrenceRange = entity.RecurrenceRange,
                RecurrenceStart = entity.RecurrenceStart.HasValue ? entity.RecurrenceStart.Value.ToString("dd-MM-yyyy HH:mm:ss") : string.Empty,
                RecurrenceEnd = entity.RecurrenceEnd.HasValue ? entity.RecurrenceEnd.Value.ToString("dd-MM-yyyy HH:mm:ss") : string.Empty,
                RecurrenceOccurenceCount = entity.RecurrenceOccurrenceCount,
                RecurrencePeriodicity = entity.RecurrencePeriodicity,
                RecurrenceDayNumber = entity.RecurrenceDayNumber,
                RecurrenceWeekDays = entity.RecurrenceWeekDays,
                RecurrenceWeekOfMonth = entity.RecurrenceWeekOfMonth,
                RecurrenceMonth = entity.RecurrenceMonth,
            };

            return model;
        }
    }
}
