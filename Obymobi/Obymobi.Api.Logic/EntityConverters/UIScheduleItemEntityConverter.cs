﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class UIScheduleItemEntityConverter : EntityConverterBase<UIScheduleItemEntity, UIScheduleItem>
    {
        private readonly UIScheduleItemOccurrenceEntityConverter uiScheduleItemOccurrenceEntityConverter = new UIScheduleItemOccurrenceEntityConverter();

        public override UIScheduleItem ConvertEntityToModel(UIScheduleItemEntity entity)
        {
            UIScheduleItem model = new UIScheduleItem
            {
                UIScheduleItemId = entity.UIScheduleItemId,
                UIWidgetId = entity.UIWidgetId ?? 0,
                MediaId = entity.MediaId ?? 0,
            };

            model.UIScheduleItemOccurrences = this.uiScheduleItemOccurrenceEntityConverter.ConvertEntityCollectionToModelArray(entity.UIScheduleItemOccurrenceCollection);

            return model;
        }
    }
}
