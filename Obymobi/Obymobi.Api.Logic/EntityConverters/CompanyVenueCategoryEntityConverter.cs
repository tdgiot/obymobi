﻿using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class CompanyVenueCategoryEntityConverter : EntityConverterBase<CompanyVenueCategoryEntity, VenueCategory>
    {
        private readonly CustomTextEntityConverter customTextEntityConverter = new CustomTextEntityConverter();

        public override VenueCategory ConvertEntityToModel(CompanyVenueCategoryEntity companyVenueCategoryEntity)
        {
            VenueCategoryEntity entity = companyVenueCategoryEntity.VenueCategoryEntity;

            VenueCategory model = new VenueCategory
            {
                VenueCategoryId = entity.VenueCategoryId,
                Name = entity.Name,
                MarkerIcon = (int)entity.MarkerIcon
            };

            model.CustomTexts = this.customTextEntityConverter.ConvertEntityCollectionToModelArray(entity.CustomTextCollection);

            return model;
        }
    }
}
