﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class PriceScheduleItemEntityConverter : EntityConverterBase<PriceScheduleItemEntity, PriceScheduleItem>
    {
        private readonly PriceScheduleItemOccurrenceEntityConverter priceScheduleItemOccurrenceEntityConverter = new PriceScheduleItemOccurrenceEntityConverter();

        public override PriceScheduleItem ConvertEntityToModel(PriceScheduleItemEntity entity)
        {
            PriceScheduleItem model = new PriceScheduleItem
            {
                PriceScheduleItemId = entity.PriceScheduleItemId,
                PriceLevelId = entity.PriceLevelId
            };

            model.PriceScheduleItemOccurrences = this.priceScheduleItemOccurrenceEntityConverter.ConvertEntityCollectionToModelArray(entity.PriceScheduleItemOccurrenceCollection);

            return model;
        }
    }
}
