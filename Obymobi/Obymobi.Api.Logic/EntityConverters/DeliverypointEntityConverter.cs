﻿using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class DeliverypointEntityConverter : EntityConverterBase<DeliverypointEntity, Deliverypoint>
    {
        private readonly bool addPosData;

        public DeliverypointEntityConverter(bool addPosData)
        {
            this.addPosData = addPosData;
        }

        public override Deliverypoint ConvertEntityToModel(DeliverypointEntity entity)
        {
            Deliverypoint model = new Deliverypoint();
            model.DeliverypointId = entity.DeliverypointId;
            model.CompanyId = entity.CompanyId;
            model.Name = entity.Name;
            model.DeliverypointgroupId = entity.DeliverypointgroupId;
            model.GooglePrinterId = entity.GooglePrinterId;
            model.PrintFromMacAddress = entity.DeviceId.HasValue ? entity.DeviceEntity.Identifier : string.Empty;
            model.EnableAnalytics = entity.EnableAnalytics;
            model.RoomControllerType = entity.RoomControllerType.HasValue ? (int)entity.RoomControllerType.Value : -1;
            model.RoomControllerIp = entity.RoomControllerIp;
            model.RoomControlConfigurationId = entity.RoomControlConfigurationId ?? -1;
            model.RoomControllerModbusSlaveId = entity.RoomControllerSlaveId ?? -1;

            int number;
            if (int.TryParse(entity.Number, out number))
                model.Number = number;

            if (this.addPosData && entity.PosdeliverypointId.HasValue)
            {
                model.Posdeliverypoint = PosdeliverypointHelper.CreatePosalterationitemModelFromEntity(entity.PosdeliverypointEntity);
            }

            return model;
        }
    }
}
