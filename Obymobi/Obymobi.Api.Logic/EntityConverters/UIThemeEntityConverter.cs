﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class UIThemeEntityConverter : EntityConverterBase<UIThemeEntity, UITheme>
    {
        private readonly UIThemeColorEntityConverter uiThemeColorEntityConverter = new UIThemeColorEntityConverter();
        private readonly UIThemeTextSizeEntityConverter uiThemeTextSizeEntityConverter = new UIThemeTextSizeEntityConverter();

        public override UITheme ConvertEntityToModel(UIThemeEntity entity)
        {
            UITheme model = new UITheme
            {
                UIThemeId = entity.UIThemeId
            };

            // Colors
            model.UIThemeColors = this.uiThemeColorEntityConverter.ConvertEntityCollectionToModelArray(entity.UIThemeColorCollection);

            // Text sizes
            model.UIThemeTextSizes = this.uiThemeTextSizeEntityConverter.ConvertEntityCollectionToModelArray(entity.UIThemeTextSizeCollection);

            // Media
            var media = new List<Media>();
            foreach (MediaEntity mediaEntity in entity.MediaCollection)
            {
                media.AddRange(Obymobi.Logic.HelperClasses.MediaHelper.CreateMediaModelFromEntity(mediaEntity, "UITHEMES", "UITHEMES_MEDIA"));
            }
            model.Media = media.ToArray();

            return model;
        }
    }
}
