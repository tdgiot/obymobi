﻿using System.Collections.Generic;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class UIThemeTextSizeEntityConverter : EntityConverterBase<UIThemeTextSizeEntity, UIThemeTextSize>
    {
        public override UIThemeTextSize ConvertEntityToModel(UIThemeTextSizeEntity entity)
        {
            UIThemeTextSize model = new UIThemeTextSize
            {
                UIThemeTextSizeId = entity.UIThemeTextSizeId,
                UIThemeId = entity.UIThemeId,
                Type = (int)entity.Type,
                TextSize = entity.TextSize
            };

            return model;
        }
    }
}
