﻿using Dionysos;
using Obymobi.Api.Logic.Repositories;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Enums;
using Obymobi.Logic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class CompanyEntityConverter : EntityConverterBase<CompanyEntity, Company>
    {
        public override Company ConvertEntityToModel(CompanyEntity entity)
        {
            Company model = new Company();
            model.CompanyId = entity.CompanyId;
            model.Currency = entity.CurrencyCode;

            if (!entity.CurrencyCode.IsNullOrWhiteSpace())
            {
                model.CurrencySymbol = Currency.Mappings[entity.CurrencyCode].Symbol;
            }

            Country country;
            try
            {
                country = (Country)entity.CountryCode;
            }
            catch (Exception)
            {
                country = null;
            }

            model.Name = entity.Name;
            model.Code = entity.Code;
            model.Addressline1 = entity.Addressline1;
            model.Addressline2 = entity.Addressline2;
            model.Addressline3 = entity.Addressline3;
            model.Zipcode = entity.Zipcode;
            model.City = entity.City;
            model.Country = country != null ? country.Name : entity.CountryEntity.Name;
            model.CountryCode = country != null ? country.CodeAlpha2 : entity.CountryEntity.Code;
            model.CountryCodeAlpha3 = country != null ? country.CodeAlpha3 : entity.CountryEntity.CodeAlpha3;
            model.Telephone = entity.Telephone;
            model.Fax = entity.Fax;
            model.Website = entity.Website;
            model.Email = entity.Email;
            model.Description = entity.Description;
            model.DescriptionSingleLine = entity.DescriptionSingleLine;
            model.GoogleAnalyticsId = entity.GoogleAnalyticsId;
            model.Latitude = entity.Latitude;
            model.Longitude = entity.Longitude;
            model.FacebookPlaceId = entity.FacebookPlaceId;
            model.FoursquareVenueId = entity.FoursquareVenueId;
            model.CompanyDataLastModifiedTicks = entity.CompanyDataLastModifiedUTC.Ticks;
            model.CompanyMediaLastModifiedTicks = entity.CompanyMediaLastModifiedUTC.Ticks;
            model.MenuDataLastModifiedTicks = entity.MenuDataLastModifiedUTC.Ticks;
            model.MenuMediaLastModifiedTicks = entity.MenuMediaLastModifiedUTC.Ticks;
            model.PosIntegrationInformationLastModifiedTicks = entity.PosIntegrationInfoLastModifiedUTC.Ticks;
            model.DeliverypointDataLastModifiedTicks = entity.DeliverypointDataLastModifiedUTC.Ticks;
            model.SurveyDataLastModifiedTicks = entity.SurveyDataLastModifiedUTC.Ticks;
            model.SurveyMediaLastModifiedTicks = entity.SurveyMediaLastModifiedUTC.Ticks;
            model.AnnouncementDataLastModifiedTicks = entity.AnnouncementDataLastModifiedUTC.Ticks;
            model.AnnouncementMediaLastModifiedTicks = entity.AnnouncementMediaLastModifiedUTC.Ticks;
            model.EntertainmentDataLastModifiedTicks = entity.EntertainmentDataLastModifiedUTC.Ticks;
            model.EntertainmentMediaLastModifiedTicks = entity.EntertainmentMediaLastModifiedUTC.Ticks;
            model.AdvertisementDataLastModifiedTicks = entity.AdvertisementDataLastModifiedUTC.Ticks;
            model.AdvertisementMediaLastModifiedTicks = entity.AdvertisementMediaLastModifiedUTC.Ticks;
            model.SupportpoolId = entity.SupportpoolId.GetValueOrDefault(0);
            model.FreeText = entity.AllowFreeText;
            model.UseBillButton = entity.UseBillButton;
            model.Salt = entity.Salt;
            model.SaltPms = entity.SaltPms;
            model.DeviceRebootMethod = entity.DeviceRebootMethod;
            model.ShowCurrencySymbol = entity.ShowCurrencySymbol;
            model.GeoFencingEnabled = entity.GeoFencingEnabled;
            model.GeoFencingRadius = entity.GeoFencingRadius;
            model.CometHandlerType = entity.CometHandlerType;
            model.SystemPassword = entity.SystemPassword;
            model.TemperatureUnit = (int)entity.TemperatureUnit;
            model.ClockMode = (int)entity.ClockMode;
            model.WeatherClockWidgetMode = (int)entity.WeatherClockWidgetMode;
            model.AlterationDialogMode = entity.AlterationDialogMode == AlterationDialogMode.v3 ? (int)AlterationDialogMode.v2 : (int)entity.AlterationDialogMode;
            model.PriceFormatType = entity.PriceFormatType;
            model.CultureCode = entity.CultureCode;
            model.TimeZone = entity.TimeZoneOlsonId;
            model.LanguageCode = entity.CultureCode.IsNullOrWhiteSpace() ? "nl" : Obymobi.Culture.Mappings[entity.CultureCode].Language.CodeAlpha2.ToLowerInvariant();
            model.ApiVersion = entity.ApiVersion;

            // Media
            List<Media> media = new List<Media>();
            for (int i = 0; i < entity.MediaCollection.Count; i++)
            {
                media.AddRange(new MediaEntityConverter().ConvertEntityToModel(entity.MediaCollection[i], string.Empty));
            }
            model.Media = media.ToArray();

            // Deliverypoint groups
            List<Deliverypointgroup> deliverypointgroups = new List<Deliverypointgroup>();
            foreach (DeliverypointgroupEntity deliverypointgroupEntity in entity.DeliverypointgroupCollection)
            {
                if (deliverypointgroupEntity.Active)
                {
                    deliverypointgroups.Add(new DeliverypointgroupEntityConverter().ConvertEntityToModel(deliverypointgroupEntity));
                }
            }
            model.Deliverypointgroups = deliverypointgroups.ToArray();

            // Terminals
            List<Terminal> terminals = new List<Terminal>();
            foreach (TerminalEntity terminalEntity in entity.TerminalCollection)
            {
                terminals.Add(new TerminalEntityConverter().ConvertEntityToModel(terminalEntity));
            }
            model.Terminals = terminals.ToArray();

            // Business hours
            model.Businesshours = new BusinesshoursEntityConverter().ConvertEntityCollectionToModelArray(entity.BusinesshoursCollection);

            // Message templates
            model.MessageTemplates = new MessageTemplateEntityConverter().ConvertEntityCollectionToModelArray(entity.MessageTemplateCollection);

            // UI Modes
            UIModeCollection uiModeCollection = new UIModeRepository().GetUIModeCollection(entity.CompanyId);
            if (uiModeCollection.Any())
                model.UIModes = new UIModeEntityConverter().ConvertEntityCollectionToModelArray(uiModeCollection);

            // Categories
            model.VenueCategories = new CompanyVenueCategoryEntityConverter().ConvertEntityCollectionToModelArray(entity.CompanyVenueCategoryCollection);

            // Amenity
            model.Amenities = new CompanyAmenityEntityConverter().ConvertEntityCollectionToModelArray(entity.CompanyAmenityCollection);

            // Schedules
            model.Schedules = new ScheduleEntityConverter().ConvertEntityCollectionToModelArray(entity.ScheduleCollection);

            // Availabilities
            model.Availabilitys = new AvailabilityEntityConverter().ConvertEntityCollectionToModelArray(entity.AvailabilityCollection);

            // Company currencies
            model.CompanyCurrencys = new CompanyCurrencyEntityConverter().ConvertEntityCollectionToModelArray(entity.CompanyCurrencyCollection).OrderBy(x => x.Name).ToArray();

            // Company cultures
            model.CompanyCultures = new CompanyCultureEntityConverter().ConvertEntityCollectionToModelArray(entity.CompanyCultureCollection).OrderBy(x => x.Name).ToArray();

            // Custom texts
            model.CustomTexts = new CustomTextEntityConverter().ConvertEntityCollectionToModelArray(entity.CustomTextCollection);

            // Set terminal handling method in company model
            // Only for sending this information to the Emenu client
            if (entity.TerminalCollection.Count > 0)
            {
                model.HandlingMethod = entity.TerminalCollection[0].HandlingMethod;
            }

            // Cloud storage accounts
            model.CloudStorageAccounts = new CloudStorageAccountEntityConverter().ConvertEntityCollectionToModelArray(entity.CloudStorageAccountCollection);

            return model;
        }
    }
}
