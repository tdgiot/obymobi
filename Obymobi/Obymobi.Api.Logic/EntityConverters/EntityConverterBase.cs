﻿using Obymobi.Logic.Model.Mobile;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Api.Logic.EntityConverters
{
    public abstract class EntityConverterBase<EntityType, ModelType> 
        where EntityType : IEntity 
    {
        #region Entity to model conversion

        public abstract ModelType ConvertEntityToModel(EntityType entity);

        public virtual ModelType[] ConvertEntityCollectionToModelArray(IEnumerable<EntityType> entityCollection)
        {
            List<ModelType> modelList = new List<ModelType>();

            if (entityCollection != null && entityCollection.Any())
            {
                foreach (EntityType entity in entityCollection)
                {
                    if (entity != null)
                    {
                        ModelType model = this.ConvertEntityToModel(entity);
                        if (model != null)
                            modelList.Add(model);
                    }
                }
            }

            return modelList.ToArray();
        }

        #endregion
    }

    public abstract class EntityConverterBase<EntityType, EntityCollectionType, ModelType>
        where EntityType : IEntity
        where EntityCollectionType : IEntityCollection
    {
        #region Entity to model conversion

        public abstract ModelType ConvertEntityToModel(EntityType entity);

        public virtual ModelType[] ConvertEntityCollectionToModelArray(EntityCollectionType entityCollection)
        {
            List<ModelType> modelList = new List<ModelType>();

            if (entityCollection != null && entityCollection.Count > 0)
            {
                foreach (EntityType entity in entityCollection)
                {
                    if (entity != null)
                    {
                        ModelType model = this.ConvertEntityToModel(entity);
                        if (model != null)
                            modelList.Add(model);
                    }
                }
            }

            return modelList.ToArray();
        }

        #endregion
    }
}
