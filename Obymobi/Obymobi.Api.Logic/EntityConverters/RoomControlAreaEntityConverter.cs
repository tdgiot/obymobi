﻿using System.Collections.Generic;
using System.Linq;
using Obymobi.Data.EntityClasses;
using Obymobi.Logic.Model;

namespace Obymobi.Api.Logic.EntityConverters
{
    public class RoomControlAreaEntityConverter : EntityConverterBase<RoomControlAreaEntity, RoomControlArea>
    {
        private readonly RoomControlSectionEntityConverter roomControlSectionEntityConverter;
        private List<int> infraredConfigurationIds;

        public RoomControlAreaEntityConverter(List<int> infraredConfigurationIds)
        {
            this.infraredConfigurationIds = infraredConfigurationIds;
            this.roomControlSectionEntityConverter = new RoomControlSectionEntityConverter(this.infraredConfigurationIds);
        }

        public override RoomControlArea ConvertEntityToModel(RoomControlAreaEntity entity)
        {
            RoomControlArea model = new RoomControlArea
            {
                RoomControlAreaId = entity.RoomControlAreaId,
                Name = entity.Name,
                NameSystem = entity.NameSystem,
                SortOrder = entity.SortOrder,
                Visible = entity.Visible,
                Type = (int)entity.Type,
                IsNameSystemBaseId = entity.IsNameSystemBaseId
            };

            model.CustomTexts = Obymobi.Logic.HelperClasses.CustomTextHelper.ConvertCustomTextCollectionToArray(entity.CustomTextCollection, "ROOMCONTROLAREAS");

            // Room control sections
            List<RoomControlSection> roomControlSections = new List<RoomControlSection>();
            if (entity.RoomControlSectionCollection.Count > 0)
            {
                List<RoomControlSectionEntity> sections = entity.RoomControlSectionCollection.Where(s => s.Visible).OrderBy(s => s.SortOrder).ToList();
                foreach (RoomControlSectionEntity roomControlSectionEntity in sections)
                {
                    roomControlSections.Add(this.roomControlSectionEntityConverter.ConvertEntityToModel(roomControlSectionEntity));
                }
            }
            model.RoomControlSections = roomControlSections.ToArray();

            return model;
        }
    }
}
