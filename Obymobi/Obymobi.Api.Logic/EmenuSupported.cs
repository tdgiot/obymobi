﻿using Obymobi.Enums;

namespace Obymobi.Api.Logic
{
    /// <summary>
    /// Class contains enum values supported by the emenu.
    /// </summary>
    public static class EmenuSupported
    {
        /// <summary>
        /// Attachment types supported by the emenu.
        /// </summary>
        public static AttachmentType[] AttachmentTypes = { AttachmentType.NotSet, AttachmentType.Document, AttachmentType.Website };
    }
}
