﻿using System;
using Newtonsoft.Json;
using NUnit.Framework;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Bridges.RefinedAbstractions;
using Obymobi.Web.Analytics.Factories;
using Obymobi.Web.Analytics.Requests;
using Obymobi.Web.Analytics.Requests.RequestBase;

namespace Obymobi.Cms.WebApplication.Tests
{
	[TestFixture]
	public class ReportRefinedAbstractionTest
	{
		[TestCase(typeof(InventoryReportRequest), AnalyticsReportType.Inventory)]
		[TestCase(typeof(ProductReportRequest), AnalyticsReportType.ProductSales)]
		[TestCase(typeof(TransactionsAppLessRequest), AnalyticsReportType.TransactionsAppLess)]
		public void CreateReportRequest_WithSpecifiedReportType_CreatesExpectedRequestType(System.Type expectedRequestType, AnalyticsReportType reportType)
		{
			// Arrange
			ReportRequestBridgeFactory factory = new ReportRequestBridgeFactory();
			ReportRefinedAbstraction sut = factory.GetReportRequestRefinedAbstraction(reportType);

			// Act
			IReportRequest request = sut.CreateReportRequest();

			// Assert
			Assert.IsInstanceOf(expectedRequestType, request);
		}

		[TestCase(typeof(InventoryReportRequest), AnalyticsReportType.Inventory)]
		[TestCase(typeof(ProductReportRequest), AnalyticsReportType.ProductSales)]
		[TestCase(typeof(TransactionsAppLessRequest), AnalyticsReportType.TransactionsAppLess)]
		public void CreateReportRequest_WithSerialisedObjectAndSpecifiedReportType_CreatesExpectedRequestType(System.Type expectedRequestType, AnalyticsReportType reportType)
		{
			// Arrange
			ReportRequestBridgeFactory factory = new ReportRequestBridgeFactory();
			ReportRefinedAbstraction sut = factory.GetReportRequestRefinedAbstraction(reportType);
			ReportRequest reportRequest = (ReportRequest)Activator.CreateInstance(expectedRequestType);
			reportRequest.CompanyId = 550;

			// Act
			IReportRequest request = sut.CreateReportRequest(JsonConvert.SerializeObject(reportRequest));

			// Assert
			Assert.AreEqual(reportRequest.CompanyId, request.CompanyId);
		}
	}
}
