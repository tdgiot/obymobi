﻿using NUnit.Framework;
using Obymobi.Enums;
using Obymobi.Web.Analytics.Bridges.RefinedAbstractions;
using Obymobi.Web.Analytics.Factories;

namespace Obymobi.Cms.WebApplication.Tests
{
	[TestFixture]
	public class SheetFilterRefinedAbstractionTest
	{
		[TestCase(typeof(InventoryReportSheetFactory), AnalyticsReportType.Inventory)]
		[TestCase(typeof(ProductReportSheetFactory), AnalyticsReportType.ProductSales)]
		[TestCase(typeof(TransactionReportSheetFactory), AnalyticsReportType.TransactionsAppLess)]
		public void CreateSheetFilterFactory_WithSpecifiedReportType_CreatesExpectedRequestType(System.Type expectedFactoryType, AnalyticsReportType reportType)
		{
			// Arrange
			ReportRequestBridgeFactory factory = new ReportRequestBridgeFactory();
			SheetFilterRefinedAbstraction sut = factory.GetSheetFilterRefinedAbstraction(reportType);

			// Act
			ISheetFilterFactory result = sut.CreateSheetFilterFactory();

			// Assert
			Assert.IsInstanceOf(expectedFactoryType, result);
		}
	}
}
