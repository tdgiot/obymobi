﻿using System;
using System.Collections.Generic;
using System.Text;
using Dionysos.Configuration;

namespace Obymobi.Integrations.PMS.Configuration
{
    /// <summary>
    /// Configuration class for PMS
    /// </summary>
    public class PmsConfigurationInfo : ConfigurationItemCollection
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the Obymobi.Logic.POS.POSConfigurationInfo class
        /// </summary>
        public PmsConfigurationInfo()
        {
            string sectionName = "PMS";

            // Comtrol
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.ComtrolHostAddress, "Host address (ip) of the Comtrol interface", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.ComtrolPort, "Port of the Comtrol interface", "25253", typeof(string)));

            // Tigertms
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.TigertmsWebserviceToCallToUrl, "Url of the Tigertms Webservice", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.TigertmsWebserviceToBeCalledOnUrl, "Url on which Tigertms can call the OSS", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.TigertmsWebserviceUserkey, "User key to supply with every request message", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.TigertmsWebserviceTestMode, "Runs in test mode", "false", typeof(bool)));

            // Innsist
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.InnsistWebserviceUrl, "Url of the Innsist Webservice", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.InnsistWebserviceToBeCalledOnUrl, "Url on which Innsist can call the OSS", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.InnsistUsername, "Innist PMS username", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.InnsistPassword, "Innsist PMS password", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.InnsistFromSystemId, "Innsist From system id", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.InnsistToSystemId, "Innsists To system id", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.InnsistWakeUpCode, "Code for settings wake up calls", "", typeof(string)));
            this.Add(new ConfigurationItem(sectionName, PmsConfigurationConstants.InnsistPosId, "Pos Id", "", typeof(string)));
        }

        #endregion
    }
}
