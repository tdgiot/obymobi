﻿
namespace Obymobi.Integrations.PMS.Configuration
{
    /// <summary>
    /// Configuration class which contains constants for the names of configuration items
    /// </summary>
    public class PmsConfigurationConstants
    {
        #region Comtrol

        public const string ComtrolHostAddress = "ComtrolHostAddress";

        public const string ComtrolPort = "ComtrolPort";

        #endregion

        #region TigerTMS

        /// <summary>
        /// Url of the TigertmsWebserviceToCallToUrl
        /// </summary>
        public const string TigertmsWebserviceToCallToUrl = "TigertmsWebserviceToCallToUrl";

        /// <summary>
        /// Webservice Userkey
        /// </summary>
        public const string TigertmsWebserviceUserkey = "TigertmsWebserviceUserkey";

        /// <summary>
        /// Url of the TigertmsWebserviceToBeCalledOnUrl
        /// </summary>
        public const string TigertmsWebserviceToBeCalledOnUrl = "TigertmsWebserviceToBeCalledOnUrl";

        /// <summary>
        /// Url of the TigertmsWebserviceTestMode
        /// </summary>
        public const string TigertmsWebserviceTestMode = "TigertmsWebserviceTestMode";

        #endregion

        #region Innsist

        /// <summary>
        /// Crave > PMS webservice URL
        /// </summary>
        public const string InnsistWebserviceUrl = "InnsistWebserviceUrl";

        /// <summary>
        /// PMS > Crave webservice URL
        /// </summary>
        public const string InnsistWebserviceToBeCalledOnUrl = "InnsistWebserviceToBeCalledOnUrl";

        /// <summary>
        /// Username used to authenticate with PMS
        /// </summary>
        public const string InnsistUsername = "InnsistUsername";

        /// <summary>
        /// Password used to authenticate with PMS
        /// </summary>
        public const string InnsistPassword = "InnsistPassword";

        public const string InnsistFromSystemId = "InnsistFromSystemId";
        public const string InnsistToSystemId = "InnsistToSystemId";
        public const string InnsistWakeUpCode = "InnsistWakeUpCode";
        public const string InnsistPosId = "InnsistPosId";

        #endregion
    }
}
