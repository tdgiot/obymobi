﻿namespace Obymobi.Integrations.PMS
{
    /// <summary>
    /// RoomState enums
    /// </summary>
    public enum RoomState : int
    {
        /// <summary>
        /// Unknown
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Room is cleaned
        /// </summary>
        RoomCleaned = 1,
        /// <summary>
        /// Cleaning is required
        /// </summary>
        CleaningRequired = 2,
        /// <summary>
        /// Cleaning is in progress
        /// </summary>
        CleaningInProgress = 3,
        /// <summary>
        /// Inspection is required
        /// </summary>
        InspectionRequired = 4,
        /// <summary>
        /// Maintenance is required
        /// </summary>
        MaintenanceRequired = 5,
        /// <summary>
        /// Out of order
        /// </summary>
        OutOfOrder = 6,
        /// <summary>
        /// Pick up laundry
        /// </summary>
        PickUpLaundry = 7,
        /// <summary>
        /// Inspection passed
        /// </summary>
        PassedInspection = 8,
        /// <summary>
        /// Inspection failed
        /// </summary>
        FailedInspection = 9,
        /// <summary>
        /// Cleaning is skipped
        /// </summary>
        CleaningSkipped = 10,
        /// <summary>
        /// Room is cleaned and occupied
        /// </summary>
        RoomCleanedAndOccupied = 11,
        /// <summary>
        /// Room is cleaned and vacant
        /// </summary>
        RoomCleanedAndVacant = 12,
        /// <summary>
        /// Cleaning is required and occupied
        /// </summary>
        CleaningRequiredAndOccupied = 13,
        /// <summary>
        /// Cleaning is required and vacant
        /// </summary>
        CleaningRequiredAndVacant = 14
    }
}
