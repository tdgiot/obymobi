﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

namespace Obymobi.Integrations.PMS.FlatWsdl
{
    public class Soap11ConformantAttribute : Attribute, IServiceBehavior
    {
        #region Methods

        public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            bool useSoap11 = true;

            //We remove SOAP12 namespace only if all the non-mex bindings use SOAP11.
            foreach (ServiceEndpoint endPoint in serviceHostBase.Description.Endpoints)
            {
                if (endPoint.Contract.ContractType != typeof(IMetadataExchange))
                {
                    useSoap11 &= endPoint.Binding.MessageVersion.Envelope == EnvelopeVersion.Soap11;
                }
            }

            if (useSoap11)
            {
                ServiceMetadataBehavior behavior = serviceHostBase.Description.Behaviors.Find<ServiceMetadataBehavior>();
                if (behavior != null)
                {
                    behavior.MetadataExporter = new Soap11ConformantWsdlExporter();
                }
            }
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }

        #endregion
    }
}