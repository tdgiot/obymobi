﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Description;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using ServiceDescription = System.Web.Services.Description.ServiceDescription;

namespace Obymobi.Integrations.PMS.FlatWsdl
{
    public class Soap11ConformantWsdlExporter : WsdlExporter
    {
        public override MetadataSet GetGeneratedMetadata()
        {
            MetadataSet metadataSet = base.GetGeneratedMetadata();

            List<MetadataSection> wsMetadataSections = new List<MetadataSection>(10);

            foreach (MetadataSection section in metadataSet.MetadataSections)
            {
                //Remove any Microsoft specific metadata section for interop purpose.
                if (!section.Identifier.Contains("microsoft"))
                {
                    wsMetadataSections.Add(section);

                    ServiceDescription description = section.Metadata as ServiceDescription;

                    if (description != null)
                    {
                        XmlQualifiedName[] namespaces = description.Namespaces.ToArray();

                        XmlSerializerNamespaces newNamespaces = new XmlSerializerNamespaces();

                        foreach (XmlQualifiedName ns in namespaces)
                        {
                            //Remove soap12 and microsoft namespaces since it will fail some non-Microsoft SOAP client.

                            if (ns.Name.ToLower() != "soap12" && !ns.Namespace.Contains("microsoft"))
                            {
                                newNamespaces.Add(ns.Name, ns.Namespace);
                            }
                        }

                        foreach (XmlSchema schema in description.Types.Schemas)
                        {
                            List<XmlSchemaImport> imports = new List<XmlSchemaImport>();

                            foreach (XmlSchemaObject include in schema.Includes)
                            {
                                XmlSchemaImport import = include as XmlSchemaImport;

                                if (import != null && import.Namespace.Contains("microsoft"))
                                {
                                    imports.Add(import);
                                }
                            }

                            foreach (XmlSchemaImport import in imports)
                            {
                                schema.Includes.Remove(import);
                            }
                        }

                        description.Namespaces = newNamespaces;
                    }
                }
            }

            return new MetadataSet(wsMetadataSections);
        }
    }

    public class Soap11ConformantServiceHostFactory : ServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            ServiceHost serviceHost = new ServiceHost(serviceType, baseAddresses);

            bool useSoap11 = true;

            //We remove SOAP12 namespace only if all the non-mex bindings use SOAP11.
            foreach (ServiceEndpoint endPoint in serviceHost.Description.Endpoints)
            {
                if (endPoint.Contract.ContractType != typeof(IMetadataExchange))
                {
                    useSoap11 &= endPoint.Binding.MessageVersion.Envelope == EnvelopeVersion.Soap11;
                }
            }

            if (useSoap11)
            {
                ServiceMetadataBehavior behavior = serviceHost.Description.Behaviors.Find<ServiceMetadataBehavior>();

                if (behavior != null)
                {
                    behavior.MetadataExporter = new Soap11ConformantWsdlExporter();
                }
            }

            return serviceHost;
        }
    }
}