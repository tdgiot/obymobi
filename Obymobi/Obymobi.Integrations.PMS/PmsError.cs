﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Integrations.PMS
{
    public enum PmsError : int
    {
        None = 0,
        UnspecifiedError = 10,
        NoPmsConfigured = 15,
        InitializationError = 20,
        NonImplementedPmsJobType = 30,
        ConfigurationError = 200,
        ConnectivityError = 201
    }
}
