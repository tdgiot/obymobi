﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Dionysos;
using Obymobi.Logic.HelperClasses;
using Obymobi.Logic.Model;
using Obymobi.Integrations.PMS.Interfaces;

namespace Obymobi.Integrations.PMS
{
    /*
     *
     * To decide: Is Roomnumber enough?
     * Do we implement guests per room?
     * CheckedInRoom or RoomCheckedIn or CheckedIn
     *
    */


    /// <summary>
    /// Base class for a PMS Connector
    /// The PMS Connector does two-way traffic, it can call the PMS to request stuff
    /// and can be called by the PMS to be notified of stuff.
    /// </summary>
    public abstract class PmsConnector : LoggingClassBase
    {
        public enum PmsConnectorResult : int
        {
            NullResponseFromWebservice = 200
        }

        #region Generic methods

        bool eventsHooked = false;

        public PmsConnector(LogHandler logHandler = null)
        {
            this.receivedMessages.CollectionChanged += this.receivedMessages_CollectionChanged;
            this.eventsHooked = true;

            this.Logged += logHandler;
        }

        public override string LogPrefix()
        {
            return "PmsConnector";
        }

        public void Initialize()
        {
            InitializePmsReceiver();
        }

        #endregion

        #region Receive from Pms

        /// <summary>
        /// Checked-In logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="guestInformation">The guest information.</param>
        public delegate void CheckInHandler(PmsConnector sender, GuestInformation guestInformation);
        /// <summary>
        /// Occurs when [checked in].
        /// </summary>
        public event CheckInHandler CheckedIn;

        /// <summary>
        /// This message should be received when the guest checks into a previously checked-out room.
        /// </summary>
        /// <param name="roomInformation"></param>
        protected virtual void OnCheckedIn(GuestInformation roomInformation)
        {
            // Put in local variable to be safe in multithreaded environment
            CheckInHandler listener = this.CheckedIn;
            if (listener != null)
            {
                listener(this, roomInformation);
            }            
        }

        /// <summary>
        /// Checked out logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        public delegate void CheckOutHandler(PmsConnector sender, GuestInformation guestInformation);
        /// <summary>
        /// Occurs when [checked out].
        /// </summary>
        public event CheckOutHandler CheckedOut;

        /// <summary>
        /// This message should be received when the guest checks out of a room (not moving rooms).
        /// </summary>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        protected virtual void OnCheckedOut(GuestInformation guestInformation)
        {
            CheckOutHandler listener = this.CheckedOut;
            if (listener != null)
            {
                listener(this, guestInformation);
            }
        }

        /// <summary>
        /// Moved Room logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="fromDeliverypointNumber">From deliverypoint number.</param>
        /// <param name="fromStationNumber">From station number.</param>
        /// <param name="toDeliverypointNumber">To deliverypoint number.</param>
        /// <param name="toStationNumber">To station number.</param>
        public delegate void MoveHandler(PmsConnector sender, string fromDeliverypointNumber, string fromStationNumber, string toDeliverypointNumber, string toStationNumber);
        /// <summary>
        /// Occurs when [moved].
        /// </summary>
        public event MoveHandler Moved;

        /// <summary>
        /// This message should be received when a credit limit is set for a room
        /// </summary>
        /// <param name="fromDeliverypointNumber">From deliverypoint number.</param>
        /// <param name="fromStationNumber">From station number.</param>
        /// <param name="toDeliverypointNumber">To deliverypoint number.</param>
        /// <param name="toStationNumber">To station number.</param>
        protected virtual void OnMoved(string fromDeliverypointNumber, string fromStationNumber, string toDeliverypointNumber, string toStationNumber)
        {
            MoveHandler listener = this.Moved;
            if (listener != null)
            {
                listener(this, fromDeliverypointNumber, fromStationNumber, toDeliverypointNumber, toStationNumber);
            }            
        }

        /// <summary>
        /// Guest information logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="guestInformation">The guest information.</param>
        public delegate void GuestInformationUpdateHandler(PmsConnector sender, GuestInformation guestInformation);
        /// <summary>
        /// Occurs when [guest information updated].
        /// </summary>
        public event GuestInformationUpdateHandler GuestInformationUpdated;

        /// <summary>
        /// This message should be received when information about the room is updated (moving rooms have seperate methods)
        /// </summary>
        /// <param name="guestInformation"></param>
        protected virtual void OnGuestInformationUpdated(GuestInformation guestInformation)
        {
            GuestInformationUpdateHandler listener = this.GuestInformationUpdated;
            if (listener != null)
            {
                listener(this, guestInformation);
            }            
        }

        /// <summary>
        /// Credit limit logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="limit">The limit.</param>
        public delegate void CreditLimitUpdateHandler(PmsConnector sender, string deliverypointNumber, decimal limit);
        /// <summary>
        /// Occurs when [credit limit updated].
        /// </summary>
        public event CreditLimitUpdateHandler CreditLimitUpdated;

        /// <summary>
        /// This message should be received when a credit limit is set for a room
        /// </summary>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="limit">The limit.</param>
        protected virtual void OnCreditLimitUpdated(string deliverypointNumber, decimal limit)
        {
            CreditLimitUpdateHandler listener = this.CreditLimitUpdated;
            if (listener != null)
            {
                listener(this, deliverypointNumber, limit);
            }            
        }

        /// <summary>
        /// Do not disturb handler
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="state">The state.</param>
        public delegate void DoNotDisturbUpdateHandler(PmsConnector sender, string deliverypointNumber, DoNotDisturbState state);
        /// <summary>
        /// Occurs when [do not disturb state updated].
        /// </summary>
        public event DoNotDisturbUpdateHandler DoNotDisturbStateUpdated;

        /// <summary>
        /// This message should be received when a credit limit is set for a room
        /// </summary>
        /// <param name="deliverypointNumber"></param>
        /// <param name="stationNumber"></param>
        /// <param name="state"></param>
        protected virtual void OnDoNotDisturbUpdated(string deliverypointNumber, string stationNumber, DoNotDisturbState state)
        {
            DoNotDisturbUpdateHandler listener = this.DoNotDisturbStateUpdated;
            if (listener != null)
            {
                listener(this, deliverypointNumber, state);
            }            
        }

        /// <summary>
        /// Room state logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="state">The state.</param>
        public delegate void RoomStateUpdateHandler(PmsConnector sender, string deliverypointNumber, RoomState state);
        /// <summary>
        /// Occurs when [room state updated].
        /// </summary>
        public event RoomStateUpdateHandler RoomStateUpdated;

        /// <summary>
        /// This message should be received when a credit limit is set for a room
        /// </summary>
        /// <param name="deliverypointNumber"></param>
        /// <param name="state"></param>
        protected virtual void OnRoomStateUpdate(string deliverypointNumber, RoomState state)
        {
            RoomStateUpdateHandler listener = this.RoomStateUpdated;
            if (listener != null)
            {
                listener(this, deliverypointNumber, state);
            }            
        }

        /// <summary>
        /// Set wake up logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="guestInformation">The wake up status.</param>
        public delegate void WakeUpSetHandler(PmsConnector sender, WakeUpStatus wakeUpStatus);
        /// <summary>
        /// Occurs when [wakeup status is set].
        /// </summary>
        public event WakeUpSetHandler WakeUpSet;

        /// <summary>
        /// This message should be received when information about the wake-up status is set
        /// </summary>
        /// <param name="guestInformation"></param>
        protected virtual void OnWakeUpSet(WakeUpStatus wakeUpStatus)
        {
            WakeUpSetHandler listener = this.WakeUpSet;
            if (listener != null)
            {
                listener(this, wakeUpStatus);
            }            
        }

        /// <summary>
        /// Clear wake up logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="guestInformation">The wake up status.</param>
        public delegate void WakeUpClearHandler(PmsConnector sender, WakeUpStatus wakeUpStatus);
        /// <summary>
        /// Occurs when [wakeup status is cleared].
        /// </summary>
        public event WakeUpClearHandler WakeUpCleared;

        /// <summary>
        /// This message should be received when information about the wake-up status is cleared
        /// </summary>
        /// <param name="guestInformation"></param>
        protected virtual void OnWakeUpCleared(WakeUpStatus wakeUpStatus)
        {
            WakeUpClearHandler listener = this.WakeUpCleared;
            if (listener != null)
            {
                listener(this, wakeUpStatus);
            }            
        }

        /// <summary>
        /// Folio logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="folio">Folio model.</param>
        public delegate void FolioUpdateHandler(PmsConnector sender, Folio folio);
        /// <summary>
        /// Occurs when [folio updated].
        /// </summary>
        public event FolioUpdateHandler FolioUpdated;

        /// <summary>
        /// This message should be received when folio for a room is updated
        /// </summary>
        /// <param name="folio"></param>
        protected virtual void OnFolioUpdated(Folio folio)
        {
            FolioUpdateHandler listener = this.FolioUpdated;
            if (listener != null)
            {
                listener(this, folio);
            }            
        }

        /// <summary>
        /// RoomData logic
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="roomData">RoomData model.</param>
        public delegate void RoomDataUpdateHandler(PmsConnector sender, RoomData roomData);
        /// <summary>
        /// Occurs when [roomData updated].
        /// </summary>
        public event RoomDataUpdateHandler RoomDataUpdated;

        /// <summary>
        /// This message should be received when roomdata for a room is updated
        /// </summary>
        /// <param name="roomData"></param>
        protected virtual void OnRoomDataUpdated(RoomData roomData)
        {
            RoomDataUpdateHandler listener = this.RoomDataUpdated;
            if (listener != null)
            {
                listener(this, roomData);
            }            
        }

        /// <summary>
        /// Guest information edited
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="roomData">RoomData model.</param>
        public delegate void GuestInformationEditedHandler(PmsConnector sender, GuestInformation guestInformation);
        /// <summary>
        /// Occurs when [GuestInformation is edited].
        /// </summary>
        public event GuestInformationEditedHandler GuestInformationEdited;

        /// <summary>
        /// This message should be received when guest information for a room is updated 
        /// </summary>
        /// <param name="roomData"></param>
        protected virtual void OnGuestInformationEdited(GuestInformation guestInformation)
        {
            GuestInformationEditedHandler listener = this.GuestInformationEdited;
            if (listener != null)
            {
                listener(this, guestInformation);
            }            
        }

        #endregion

        #region Push to PMS

        /// <summary>
        /// Indicates a request to have the PMS check a room out. The balance should match the PMS' record for
        /// the account / room number specified. The PMS must initiate an actual Check Out Room (after this
        /// message's required Express Checkout response) if this Express Checkout Request is accepted.
        /// </summary>
        /// <param name="checkoutInfo">The checkout info.</param>
        /// <returns>
        /// Boolean if the checkout succeeded
        /// </returns>
        public abstract bool Checkout(Checkout checkoutInfo);

        /// <summary>
        /// Get the Room Information for a Room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>The Room Information for the Room</returns>
        public abstract List<GuestInformation> GetGuestInformation(string deliverypointNumber);

        /// <summary>
        /// Get the Folio for the Room detailing all charges and credits booked for the Room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>The Folio for the room</returns>
        public abstract List<Folio> GetFolio(string deliverypointNumber);

        /// <summary>
        /// Get the balance of the room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>The balance of the room</returns>
        public abstract decimal GetBalance(string deliverypointNumber);

        /// <summary>
        /// Get the state of the room
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <returns>The room state</returns>
        public abstract RoomState GetRoomStatus(string deliverypointNumber);

        /// <summary>
        /// Add one or more FolioItems to the Folio
        /// </summary>
        /// <param name="deliverypointNumber">The deliverypoint number.</param>
        /// <param name="folioItems">Items to be added</param>
        /// <returns>
        /// True of false indicating if the item could be added
        /// </returns>
        public abstract bool AddToFolio(string deliverypointNumber, List<FolioItem> folioItems);

        /// <summary>
        /// Set the Room State
        /// </summary>
        /// <param name="deliverypointNumber">The DeliverypointNumber, i.e. RoomNumber</param>
        /// <param name="state">State of the room</param>
        /// <returns>True of false indicating if the state could be updated</returns>
        public abstract bool UpdateRoomState(string deliverypointNumber, RoomState state);

        /// <summary>
        /// Sets the wakeup status of a customer
        /// </summary>
        /// <param name="wakeUpStatus"></param>
        /// <returns></returns>
        public abstract bool SetWakeUpStatus(WakeUpStatus wakeUpStatus);

        #endregion

        #region Synchronisation of Webservice Calls

        object receivedMessagesLock = new object();
        protected int webserviceResponseMessageTimeout = 35000;
        private ConcurrentDictionary<string, EventWaitHandle> waitHandles = new ConcurrentDictionary<string, EventWaitHandle>();
        protected ObservableCollection<IPmsMessage> receivedMessages = new ObservableCollection<IPmsMessage>();

        protected IPmsMessage SendMessageToPmsSynchronously(IPmsMessage requestMessage, bool throwExceptionOnNull)
        {
            if (!this.eventsHooked)
                throw new TechnicalException("CollectionChanged event was not assigned!");

            string responseMessageSignature = requestMessage.GetResponseMessageSignature();
            // Submit message to webservice (in that call handle errors and stuff)

            // Check if we already received the response (almost impossible, but who knows
            IPmsMessage messageToReturn;
            EventWaitHandle handle = new EventWaitHandle(false, EventResetMode.AutoReset);
            try
            {
                // Call the webservice
                this.SendMessageToPms(requestMessage);

                // Try to create the wait handle
                if (!this.waitHandles.TryAdd(responseMessageSignature, handle))
                {
                    this.LogWarning("Multiple the calls for the same message signature at same time is not supported.", requestMessage.GetMessageSignature());
                }

                // Check just to be sure that we didn't get the response already
                bool timedOut = false;
                if (!this.receivedMessages.Any(kv => kv.GetMessageSignature() == responseMessageSignature))
                {
                    this.LogVerbose("Create wait handle to wait for message: '{0}', times out after {1}ms", requestMessage.MessageDescription, this.webserviceResponseMessageTimeout);

                    // Wait for the signal, timeout after the timeout interval
                    Stopwatch stopwatchCall = new Stopwatch();
                    stopwatchCall.Start();
                    if (!handle.WaitOne(this.webserviceResponseMessageTimeout))
                    {
                        stopwatchCall.Stop();
                        this.LogWarning("Wait handle timed out: '{0}' - Waited: {1}ms", requestMessage.MessageDescription, stopwatchCall.ElapsedMilliseconds);
                        timedOut = true;
                    }
                    else
                    {
                        stopwatchCall.Stop();
                        this.LogVerbose("Response of webservice in {0}ms", stopwatchCall.ElapsedMilliseconds);
                    }
                }

                lock (this.receivedMessagesLock)
                {
                    // Check if we received the message (even when we timed out we check
                    messageToReturn = this.receivedMessages.FirstOrDefault(kv => kv.GetMessageSignature() == responseMessageSignature);

                    if (messageToReturn == null)
                    {
                        if (!timedOut)
                            this.LogError("Wait handle was signalled but response '{0}' message not found for request: '{1}' (Unexpected behaviour - 'This should never happen').",
                                responseMessageSignature, requestMessage.MessageDescription);
                        else
                            this.LogError("Wait handle was timedout, also message '{0}' not found for request: '{1}'.", responseMessageSignature,
                                requestMessage.MessageDescription);
                    }
                    else
                    {
                        // Remove the received message from the queue - it will be handled by the caller of this method
                        this.receivedMessages.Remove(messageToReturn);
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError("An uncatched exception occured when interacting with webservice: {0}", ex.GetAllMessages());
                this.LogError("{0}", ex.GetAllMessages(true));

                throw;
            }
            finally
            {
                // Clean up the handle
                if (this.waitHandles.TryGetValue(responseMessageSignature, out handle))
                {
                    if (handle != null)
                        handle.Dispose();
                }

                this.waitHandles.TryRemove(responseMessageSignature, out handle);
            }

            if (throwExceptionOnNull && messageToReturn == null)
            {
                try
                {
                    string messageXml = XmlHelper.Serialize(requestMessage);
                    throw new ObymobiException(PmsConnectorResult.NullResponseFromWebservice, "Null response for message: '{0}'\r\n{1}",
                        requestMessage.MessageDescription, messageXml);
                }
                catch
                {
                    throw new ObymobiException(PmsConnectorResult.NullResponseFromWebservice, "Null response for message: '{0}'", requestMessage.MessageDescription);
                }
            }

            return messageToReturn;
        }

        void receivedMessages_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            // We're only interested in ADD, since that means a new message has arrived.
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                if (e.NewItems.Count > 0)
                {
                    foreach (var item in e.NewItems)
                    {
                        IPmsMessage message = null;
                        try
                        {
                            message = (IPmsMessage)item;

                            // Log the Message Signature
                            try
                            {
                                this.LogDebug("Received a message: {0}", message.MessageDescription);
                            }
                            catch (Exception ex)
                            {
                                this.LogWarning(ex.Message);
                            }

                            EventWaitHandle handle;
                            if (this.waitHandles.TryGetValue(message.GetMessageSignature(), out handle))
                            {
                                // If we are waiting for this message it's part of the
                                // SendMessageToWebserviceSynchronously design, so handle internally
                                handle.Set();
                            }
                            else
                            {
                                // Some messages can be sent without us asking for it, of those we implemented: Checkout and Checkin,
                                // which are invoked when the PMS get's a guest checked in or out.
                                lock (this.receivedMessagesLock)
                                {
                                    this.receivedMessages.Remove(message);
                                }

                                // Notify connector of received message
                                this.OnPmsMessageReceived(message);
                            }
                        }
                        catch (Exception ex)
                        {
                            if (message != null)
                            {
                                try
                                {
                                    string messageXml = XmlHelper.Serialize(message);
                                    this.LogError("An exception occured while trying to handle '{0}'-message: {1}\r\n{2}\r\n{3}", message.GetType().Name, ex.Message, message.MessageDescription, messageXml);
                                }
                                catch
                                {
                                    this.LogError("An exception occured while trying to handle '{0}'-message: {1}\r\n{2}", message.GetType().Name, ex.Message, message.MessageDescription);
                                }
                            }
                            else
                                this.LogError("Message could not be cased to a valid typed MessageBase object: {0}", ex.Message);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Initialize the webservice / WCF / socket / etc that receives the
        /// push messages from the PMS
        /// </summary>
        protected abstract void InitializePmsReceiver();

        /// <summary>
        /// Calls the webservice.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        protected abstract string SendMessageToPms(IPmsMessage message);

        /// <summary>
        /// Pms Message Received
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="guestInformation">The guest information.</param>
        public delegate void PmsMessageReceivedHandler(PmsConnector sender, IPmsMessage guestInformation);

        /// <summary>
        /// Occurs when a PMS message was received.
        /// </summary>
        public event PmsMessageReceivedHandler PmsMessageReceived;

        /// <summary>
        /// This message should be received when the guest checks into a previously checked-out room.
        /// </summary>
        /// <param name="roomInformation"></param>
        protected virtual void OnPmsMessageReceived(IPmsMessage message)
        {
            // Put in local variable to be safe in multithreaded environment
            PmsMessageReceivedHandler listener = this.PmsMessageReceived;
            if (listener != null)
            {
                listener(this, message);
            }            
        }

        /// <summary>
        /// Closes the connection to the pms system
        /// </summary>
        public abstract void StopReceiving();

        #endregion
    }
}
