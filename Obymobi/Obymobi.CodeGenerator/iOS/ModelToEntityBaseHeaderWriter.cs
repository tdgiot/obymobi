﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.iOS
{
    public class ModelToEntityBaseHeaderWriter
    {
        private Object model;
        private Type modelType;
        private StreamWriter stream;
        private PropertyInfo[] fields;

        public ModelToEntityBaseHeaderWriter(Object model, StreamWriter stream)
        {
            this.model = model;
            this.modelType = model.GetType();
            this.stream = stream;

            PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
            List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
            for (int j = 0; j < propertyInfo.Length; j++)
            {
                PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForMobile
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
                    propertyInfoList.Add(property);
            }

            this.fields = propertyInfoList.ToArray();
        }

        public void WriteEntity()
        {
            this.WriteIntroduction();
            this.WriteFields();
            this.WriteClassStart();
            this.WriteProperties();
            //this.WriteXmlLogic();
            //this.WriteCloneLogic();
        }

        private void WriteIntroduction()
        {
            WriteToStream("//");
            WriteToStream("// {0}EntityBase.h", this.modelType.Name);
            WriteToStream("// Generated entity base class header for {0}", this.modelType.FullName);
            WriteToStream("//");
            WriteToStream("//  Copyright (c) 2012 Crave Interactive Ltd. All rights reserved.");
            WriteToStream("//");
            WriteToStream("");
            WriteToStream("#import <Foundation/Foundation.h>");
            WriteToStream("#import <CoreData/CoreData.h>");
            WriteToStream("");

            string classImport = string.Empty;
            foreach (var field in this.fields)
            {
                if (!iOSHelper.IsPrimitiveDataType(field.PropertyType))
                {
                    string entityName = iOSHelper.GetiOSDataType(field.PropertyType).Replace("Collection", string.Empty);

                    if (!classImport.IsNullOrWhiteSpace())
                        classImport += ", ";

                    classImport += string.Format("{0}Entity, {0}Collection", entityName);
                }
            }

            if (!classImport.IsNullOrWhiteSpace())
            {
                WriteToStream("@class {0};", classImport);
                WriteToStream("");
            }

            WriteToStream("@interface {0}EntityBase : NSManagedObject", this.modelType.Name);
            WriteToStream("");
        }

        private void WriteFields()
        {
            foreach (var field in this.fields)
            {
                string datatype = iOSHelper.GetiOSDataType(field.PropertyType);
                if (iOSHelper.IsPrimitiveDataType(field.PropertyType))
                    WriteToStream("@property (nonatomic, retain) {0} * {1};", datatype, field.Name.TurnFirstToLower(false));
                else
                    WriteToStream("@property (nonatomic, retain) {0} *{1};", datatype, field.Name.TurnFirstToLower(false));
            }

            WriteToStream("");
            WriteToStream("@end");
            WriteToStream("");
        }

        private void WriteClassStart()
        {
            WriteToStream("@interface {0}EntityBase (CoreDataGeneratedAccessors)", this.modelType.Name);
            WriteToStream("");
        }

        private void WriteProperties()
        {
            foreach (var field in fields)
            {
                if (!iOSHelper.IsPrimitiveDataType(field.PropertyType))
                {
                    string entityName = iOSHelper.GetiOSDataType(field.PropertyType).Replace("Collection", string.Empty);

                    WriteToStream("- (void)add{0}Entity:({0}Entity *)value;", entityName);
                    WriteToStream("- (void)remove{0}Entity:({0}Entity *)value;", entityName);
                    WriteToStream("- (void)add{0}Collection:({0}Collection *)values;", entityName);
                    WriteToStream("- (void)remove{0}Collection:({0}Collection *)values;", entityName);
                    WriteToStream("");
                }
            }

            WriteToStream("@end");
        }

        private void WriteXmlLogic()
        {
            WriteToStream("\t// ===========================================================");
            WriteToStream("\t// Methods");
            WriteToStream("\t// ===========================================================");
            WriteToStream("");

            #region ToXml
            WriteToStream("\tpublic String toXml()");
            WriteToStream("\t{{");
            WriteToStream("\t\tString xml = \"\";");
            WriteToStream("");
            WriteToStream("\t\txml += XmlHelper.createXmlOpeningTag(\"{0}\");", this.modelType.Name);
            WriteToStream("");

            // Write fields
            foreach (var field in fields)
            {
                if(!field.PropertyType.BaseType.Name.Equals("Array"))
                    WriteToStream("\t\txml += XmlHelper.createXmlElement(\"{0}\", this.get{0}());", field.Name);
            }
            foreach (var field in fields)
            {
                if (field.PropertyType.BaseType.Name.Equals("Array"))
                    WriteToStream("\t\txml += XmlHelper.createXmlElement(\"{0}\", this.get{0}());", field.Name);
            }

            WriteToStream("");
            WriteToStream("\t\txml += XmlHelper.createXmlClosingTag(\"{0}\");", this.modelType.Name);
            WriteToStream("");
            WriteToStream("\t\treturn xml;");
            WriteToStream("\t}}");
            #endregion
                        
            WriteToStream("");

            #region ToJson
            WriteToStream("\tpublic String toJson()");
            WriteToStream("\t{{");
            WriteToStream("\t\tString json = \"\";");
            WriteToStream("");
            WriteToStream("\t\tjson += JsonHelper.createJsonOpeningTag();");
            WriteToStream("");

            bool last = false;

            // Write fields
            for (int i = 0; i < this.fields.Length; i++)
            {
                PropertyInfo field = this.fields[i];
                last = (i == (this.fields.Length - 1));
                //if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
                WriteToStream("\t\tjson += JsonHelper.createJsonElement(\"{0}\", this.get{2}(), {1});", field.Name.TurnFirstToLower(false), last.ToString().ToLower(), field.Name);
            }

            WriteToStream("");
            WriteToStream("\t\tjson += JsonHelper.createJsonClosingTag();");
            WriteToStream("");
            WriteToStream("\t\treturn json;");
            WriteToStream("\t}}");
            #endregion

            WriteToStream("");
        }

        public void WriteCloneLogic()
        {
            WriteToStream("\t\t// ------------------  CLONE LOGIC ------------------");
            WriteToStream("");

            WriteToStream("\t\tpublic {0}Entity clone()", this.modelType.Name);
            WriteToStream("\t\t{{");
            WriteToStream("\t\t	{0}Entity clone = new {0}Entity();", this.modelType.Name);

            // Clone fields
            foreach (var field in fields)
            {
                //if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
                string typeName = iOSHelper.GetiOSDataType(field.PropertyType);
                if (typeName.Contains("Collection"))
                {
                    string entityBaseType = typeName.Replace("Collection", "EntityBase");
                    string entityType = typeName.Replace("Collection", "Entity");

                    // Clone Collection
                    WriteToStream("\t		for(Object source : this.get{0}())", field.Name);
                    WriteToStream("\t		{{");
                    //WriteToStream("\t		    @SuppressWarnings(\"unused\")");
                    WriteToStream("\t		    {0} source{0} = ({0})source;", entityType);
                    WriteToStream("\t		    {0} clone{0} = source{0}.clone();", entityType);
                    WriteToStream("\t		    clone.get{0}().add(clone{1});", field.Name, entityType);
                    WriteToStream("\t		}}");
                }
                else
                {
                    WriteToStream("\t		clone.set{0}(this.get{0}());", field.Name);
                }
            }

            WriteToStream("\t		return clone;");

            WriteToStream("\t\t}}");
            WriteToStream("");

        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
