﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.iOS
{
    public class ModelToEntityWriter
    {
        private Object model;
        private Type modelType;
        private StreamWriter stream;
        private PropertyInfo[] fields;

        public ModelToEntityWriter(Object model, StreamWriter stream)
        {
            this.model = model;
            this.modelType = model.GetType();
            this.stream = stream;

            PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
            List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
            for (int j = 0; j < propertyInfo.Length; j++)
            {
                PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForMobile
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
                    propertyInfoList.Add(property);
            }

            this.fields = propertyInfoList.ToArray();
        }

        public void WriteEntity()
        {
            this.WriteIntroduction();
        }

        private void WriteIntroduction()
        {
            WriteToStream("//");
            WriteToStream("// {0}Entity.m", this.modelType.Name);
            WriteToStream("// Generated entity class implementation for {0}", this.modelType.FullName);
            WriteToStream("//");
            WriteToStream("//  Copyright (c) 2012 Crave Interactive Ltd. All rights reserved.");
            WriteToStream("//");
            WriteToStream("");
            WriteToStream("@implementation {0}Entity", this.modelType.Name);
            WriteToStream("@end");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
