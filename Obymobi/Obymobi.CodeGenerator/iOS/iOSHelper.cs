﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.CodeGenerator.iOS
{
    public class iOSHelper
    {
        public static string GetiOSDataType(Type dotNetType)
        {
            string type = string.Empty;
            switch (dotNetType.ToString())
            {
                case "System.Boolean":
                    type = "BOOL";
                    break;
                case "System.String":
                    type = "NSString";
                    break;
                case "System.Int32":
                case "System.UInt32":
                case "System.Int16":
                case "System.Int64":
                case "System.Decimal":
                case "System.Double":
                case "System.Single":
                    type = "NSNumber";
                    break;
                case "System.Int32[]":
                    type = "NSMutableArray";
                    break;
                case "System.DateTime":
                case "System.Nullable`1[System.DateTime]":
                    type = "NSDate";
                    break;
            }

            if (type == string.Empty)
            {
                type = string.Format("{0}Collection", dotNetType.Name.Replace("[]", string.Empty));
            }

            return type;
        }

        public static bool IsPrimitiveDataType(Type dotNetType)
        {
            bool isPrimitive = true;

            string dataType = GetiOSDataType(dotNetType);

            if (dataType != "BOOL" && dataType != "NSString" && dataType != "NSNumber" &&
                dataType != "NSDate" && dataType != "NSMutableArray")
                isPrimitive = false;

            return isPrimitive;
        }
    }
}
