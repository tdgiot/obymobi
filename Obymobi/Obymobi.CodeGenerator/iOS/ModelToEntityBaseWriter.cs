﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.iOS
{
    public class ModelToEntityBaseWriter
    {
        private Object model;
        private Type modelType;
        private StreamWriter stream;
        private PropertyInfo[] fields;

        public ModelToEntityBaseWriter(Object model, StreamWriter stream)
        {
            this.model = model;
            this.modelType = model.GetType();
            this.stream = stream;

            PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
            List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
            for (int j = 0; j < propertyInfo.Length; j++)
            {
                PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForMobile
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
                    propertyInfoList.Add(property);
            }

            this.fields = propertyInfoList.ToArray();
        }

        public void WriteEntity()
        {
            this.WriteIntroduction();
            this.WriteFields();
        }

        private void WriteIntroduction()
        {
            WriteToStream("//");
            WriteToStream("// {0}EntityBase.m", this.modelType.Name);
            WriteToStream("// Generated entity base class implementation for {0}", this.modelType.FullName);
            WriteToStream("//");
            WriteToStream("//  Copyright (c) 2012 Crave Interactive Ltd. All rights reserved.");
            WriteToStream("//");
            WriteToStream("");

            string classImport = string.Empty;
            foreach (var field in this.fields)
            {
                if (!iOSHelper.IsPrimitiveDataType(field.PropertyType))
                {
                    string entityName = iOSHelper.GetiOSDataType(field.PropertyType).Replace("Collection", string.Empty);

                    WriteToStream("#import \"{0}Entity.h\"", entityName);
                    WriteToStream("#import \"{0}Collection.h\"", entityName);
                }
            }

            WriteToStream("");
            WriteToStream("");
            WriteToStream("@implementation {0}EntityBase", this.modelType.Name);
            WriteToStream("");
        }

        private void WriteFields()
        {
            foreach (var field in this.fields)
            {
                WriteToStream("@dynamic {0};", field.Name.TurnFirstToLower(false));
            }

            WriteToStream("");
            WriteToStream("@end");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
