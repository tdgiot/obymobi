﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using Obymobi.Attributes;
using Dionysos.Diagnostics;

namespace Obymobi.CodeGenerator.iOS
{
    public class iOSGenerator
    {
        #region Fields

        private string modelNamespace = string.Empty;
        private Assembly modelAssemlby = null;
        private string codePath = string.Empty;

        #endregion

        #region Methods

        public iOSGenerator(string codePath, Type typeOfOneModelClass)
        {
            this.codePath = codePath;

            this.modelNamespace = typeOfOneModelClass.Namespace;
            this.modelAssemlby = Assembly.GetAssembly(typeOfOneModelClass);
        }

        public void Generate()
        {
            Debug.WriteLine("iOSGenerator.Generate - Start");

            // Get all classes to write
            List<Type> models = this.GetAllModels();

            foreach (var modelClassType in models)
            {
                Debug.WriteLine(string.Format("iOSGenerator.Generate - Model: {0}", modelClassType.ToString()));
                if (modelClassType.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
                {
                    this.WriteEntityBaseHeaderFile(modelClassType);
                    this.WriteEntityBaseFile(modelClassType);
                    this.WriteEntityHeaderFile(modelClassType);
                    this.WriteEntityFile(modelClassType);
                    //this.WriteEntityCollectionFile(modelClassType);
                }
            }

            List<MethodInfo> methods = this.GetAllWebserviceMethods();

            foreach (MethodInfo method in methods)
            {
                if (method.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
                {
					Debug.WriteLine(string.Format("iOSGenerator.Generate - WriteWebserviceHandler: {0}", method.ToString()));
                    //this.WriteWebserviceHandler(method);
                    //this.WriteWebserviceEvent(method);
                    //this.WriteWebserviceResultEvent(method);
                    //this.WriteWebserviceHandler(method);
                }
            }

            Debug.WriteLine("iOSGenerator.Generate - Finish");
        }

        private void WriteEntityBaseHeaderFile(Type modelType)
        {
            Object modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

            // Init the file
            string entityBaseClassDirectory = Path.Combine(this.codePath, "data\\entitybaseclasses");
            if (!Directory.Exists(entityBaseClassDirectory))
                Directory.CreateDirectory(entityBaseClassDirectory);
            StreamWriter writer = new StreamWriter(Path.Combine(entityBaseClassDirectory, modelType.Name + "EntityBase.h"));

            // Write contents
            ModelToEntityBaseHeaderWriter entityWriter = new ModelToEntityBaseHeaderWriter(modelInstance, writer);
            entityWriter.WriteEntity();

            // Close
            writer.Flush();
            writer.Close();
        }

        private void WriteEntityBaseFile(Type modelType)
        {
            Object modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

            // Init the file
            string entityBaseClassDirectory = Path.Combine(this.codePath, "data\\entitybaseclasses");
            if (!Directory.Exists(entityBaseClassDirectory))
                Directory.CreateDirectory(entityBaseClassDirectory);
            StreamWriter writer = new StreamWriter(Path.Combine(entityBaseClassDirectory, modelType.Name + "EntityBase.m"));

            // Write contents
            ModelToEntityBaseWriter entityWriter = new ModelToEntityBaseWriter(modelInstance, writer);
            entityWriter.WriteEntity();

            // Close
            writer.Flush();
            writer.Close();
        }

        private void WriteEntityHeaderFile(Type modelType)
        {
            Object modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

            // Init the file
            string entityBaseClassDirectory = Path.Combine(this.codePath, "data\\entityclasses");
            if (!Directory.Exists(entityBaseClassDirectory))
                Directory.CreateDirectory(entityBaseClassDirectory);

            string fileName = Path.Combine(entityBaseClassDirectory, modelType.Name + "Entity.h");
            if (!File.Exists(fileName))
            {
                StreamWriter writer = new StreamWriter(fileName);

                // Write contents
                ModelToEntityHeaderWriter entityWriter = new ModelToEntityHeaderWriter(modelInstance, writer);
                entityWriter.WriteEntity();

                // Close
                writer.Flush();
                writer.Close();
            }
        }

        private void WriteEntityFile(Type modelType)
        {
            Object modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

            // Init the file
            string entityBaseClassDirectory = Path.Combine(this.codePath, "data\\entityclasses");
            if (!Directory.Exists(entityBaseClassDirectory))
                Directory.CreateDirectory(entityBaseClassDirectory);

            string fileName = Path.Combine(entityBaseClassDirectory, modelType.Name + "Entity.m");
            if (!File.Exists(fileName))
            {
                StreamWriter writer = new StreamWriter(fileName);

                // Write contents
                ModelToEntityWriter entityWriter = new ModelToEntityWriter(modelInstance, writer);
                entityWriter.WriteEntity();

                // Close
                writer.Flush();
                writer.Close();
            }
        }

        private void WriteEntityCollectionFile(Type modelType)
        {
            Object modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

            // Init the file
            string collectionBaseClassDirectory = Path.Combine(this.codePath, "data\\collectionclasses");
            if (!Directory.Exists(collectionBaseClassDirectory))
                Directory.CreateDirectory(collectionBaseClassDirectory);

            string filePath = Path.Combine(collectionBaseClassDirectory, modelType.Name + "Collection.java");

            if (!File.Exists(filePath))
            {
                StreamWriter writer = new StreamWriter(filePath);

                // Write contents
                ModelToCollectionWriter entityWriter = new ModelToCollectionWriter(modelInstance, writer);
                entityWriter.WriteCollection();

                // Close
                writer.Flush();
                writer.Close();
            }
        }

        //private void WriteWebserviceMap(List<MethodInfo> methods)
        //{
        //    //@"D:\Development\Flex\SjokieDokieAlpha01\src\maps\WebserviceMap.mxml"
        //    StreamWriter writer = new StreamWriter(Path.Combine(this.codePath, @"maps\WebserviceMap.mxml"));

        //    // Write contents
        //    WebserviceMapWriter webserviceMapWriter = new WebserviceMapWriter(writer, methods);
        //    webserviceMapWriter.Write();

        //    // Close
        //    writer.Flush();
        //    writer.Close();
        //}

        //private void WriteLocalserviceMap(List<MethodInfo> methods)
        //{
        //    //@"D:\Development\Flex\SjokieDokieAlpha01\src\maps\WebserviceMap.mxml"
        //    StreamWriter writer = new StreamWriter(Path.Combine(this.codePath, @"maps\LocalserviceMap.mxml"));

        //    // Write contents
        //    LocalserviceMapWriter localserviceMapWriter = new LocalserviceMapWriter(writer, methods);
        //    localserviceMapWriter.Write();

        //    // Close
        //    writer.Flush();
        //    writer.Close();
        //}

        //private void WriteWebserviceEvent(MethodInfo method)
        //{
        //    // Init the file
        //    string eventsDirectory = Path.Combine(this.codePath, @"net\obymobi\events");
        //    if (!Directory.Exists(eventsDirectory))
        //        Directory.CreateDirectory(eventsDirectory);

        //    StreamWriter writer = new StreamWriter(Path.Combine(eventsDirectory, method.Name + "Event.as"));

        //    // Write contents
        //    WebserviceEventWriter webserviceEventWriter = new WebserviceEventWriter(writer, method);
        //    webserviceEventWriter.Write();

        //    // Close
        //    writer.Flush();
        //    writer.Close();
        //}

        //private void WriteWebserviceResultEvent(MethodInfo method)
        //{
        //    // Init the file
        //    string eventsDirectory = Path.Combine(this.codePath, @"net\obymobi\events");
        //    if (!Directory.Exists(eventsDirectory))
        //        Directory.CreateDirectory(eventsDirectory);

        //    StreamWriter writer = new StreamWriter(Path.Combine(eventsDirectory, method.Name + "ResultEvent.as"));

        //    // Write contents
        //    WebserviceEventResultWriter webserviceEventResultWriter = new WebserviceEventResultWriter(writer, method);
        //    webserviceEventResultWriter.Write();

        //    // Close
        //    writer.Flush();
        //    writer.Close();
        //}

        private void WriteWebserviceHandler(MethodInfo method)
        {
            // Init the file
            string handlerDirectory = Path.Combine(this.codePath, "net\\handlers");
            if (!Directory.Exists(handlerDirectory))
                Directory.CreateDirectory(handlerDirectory);

            StreamWriter writer = new StreamWriter(Path.Combine(handlerDirectory, method.Name + "Handler.java"));

            // Write contents
            //if (method.GetCustomAttributes(typeof(ClientLocalStorage), false).Length <= 0)
            //{
            WebserviceHandlerWriter webserviceHandlerWriter = new WebserviceHandlerWriter(writer, method, this.modelAssemlby);
            webserviceHandlerWriter.Write();
            //}

            // Close
            writer.Flush();
            writer.Close();
        }

        //private void WriteWebserviceConstants(List<MethodInfo> methods)
        //{
        //    StreamWriter writer = new StreamWriter(Path.Combine(this.codePath, @"net\obymobi\WebserviceEvents.as"));

        //    // Write contents
        //    WebserviceEventsWriter webserviceEventsWriter = new WebserviceEventsWriter(writer, methods);
        //    webserviceEventsWriter.Write();

        //    // Close
        //    writer.Flush();
        //    writer.Close();
        //}

        //private void WriteLocalserviceConstants(List<MethodInfo> methods)
        //{
        //    StreamWriter writer = new StreamWriter(Path.Combine(this.codePath, @"net\obymobi\LocalserviceEvent.as"));

        //    // Write contents
        //    LocalserviceEventsWriter localserviceEventsWriter = new LocalserviceEventsWriter(writer, methods);
        //    localserviceEventsWriter.Write();

        //    // Close
        //    writer.Flush();
        //    writer.Close();
        //}

        /// <summary>
        /// Method to populate a list with all the class
        /// in the namespace provided by the user
        /// </summary>
        /// <param name="nameSpace">The namespace the user wants searched</param>
        /// <returns></returns>
        private List<Type> GetAllModels()
        {
            List<Type> models = new List<Type>();
            foreach (Type type in this.modelAssemlby.GetTypes())
            {
                if (type.Namespace == this.modelNamespace)
                    models.Add(type);
            }
            return models;

            #region Stolen from:
            /*
			//create an Assembly and use its GetExecutingAssembly Method
			//http://msdn2.microsoft.com/en-us/library/system.reflection.assembly.getexecutingassembly.aspx
			Assembly asm = Assembly.GetAssembly(typeof(Obymobi.Logic.Obyresult));
			//create a list for the namespaces
			List<string> namespaceList = new List<string>();
			//create a list that will hold all the classes
			//the suplied namespace is executing
			List<string> returnList = new List<string>();
			//loop through all the "Types" in the Assembly using
			//the GetType method:
			//http://msdn2.microsoft.com/en-us/library/system.reflection.assembly.gettypes.aspx
			foreach (Type type in asm.GetTypes())
			{
				if (type.Namespace == this.modelNamespace)
					namespaceList.Add(type.Name);
			}
			//now loop through all the classes returned above and add
			//them to our classesName list
			foreach (String className in namespaceList)
				returnList.Add(className);
			//return the list
			return returnList;
			 */
            #endregion
        }

        /// <summary>
        /// Method to populate a list with all the webservice methods
        /// </summary>
        /// <returns></returns>
        private List<MethodInfo> GetAllWebserviceMethods()
        {
            Assembly assembly = Assembly.Load("Obymobi.Logic");
            Type type = assembly.GetType("Obymobi.Logic.Interfaces.IWebserviceHandler");
            MethodInfo[] methodInfo = type.GetMethods();
            List<MethodInfo> methodInfoList = new List<MethodInfo>();
            for (int j = 0; j < methodInfo.Length; j++)
            {
                MethodInfo method = methodInfo[j];
                if (method.Name != "IsDatabaseAlive" && method.Name != "IsWebserviceAlive" && method.Name != "RequestToReprintConfirmation" && method.Name != "ConfirmCode")
                    methodInfoList.Add(method);
            }

            return methodInfoList;
        }

        #endregion
    }
}
