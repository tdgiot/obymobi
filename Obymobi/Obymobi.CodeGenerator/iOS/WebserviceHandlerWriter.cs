﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.iOS
{
    public class WebserviceHandlerWriter
    {
        private StreamWriter stream;
        private MethodInfo method;
        private Type type;
        private string typeName;
        private Assembly modelAssembly;
        private PropertyInfo[] fields;
        private PropertyInfo[] fieldsNonPrimitive;
        private object model;
        private int index = 0;

        private PropertyInfo[] propertyInfo;
        private Dictionary<string, PropertyInfo> doubleProperties = new Dictionary<string, PropertyInfo>();
        private List<string> doublPropertiesHandled = new List<string>(); 

        public WebserviceHandlerWriter(StreamWriter stream, MethodInfo method, Assembly modelAssembly)
        {
            this.stream = stream;
            this.method = method;
            this.type = (this.method.ReturnType.GetGenericArguments().Length > 0 ? this.method.ReturnType.GetGenericArguments()[0] : this.method.ReturnType);
            this.typeName = this.type.Name;
            this.modelAssembly = modelAssembly;

            this.model = Dionysos.InstanceFactory.CreateInstance(this.modelAssembly, type);

            propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(this.model);
            List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
            List<PropertyInfo> nonPrimitivePropertyInfoList = new List<PropertyInfo>();

            for (int j = 0; j < propertyInfo.Length; j++)
            {
                PropertyInfo property = propertyInfo[j];
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
                {
                    propertyInfoList.Add(property);

                    if (!iOSHelper.IsPrimitiveDataType(property.PropertyType))
                        nonPrimitivePropertyInfoList.Add(property);
                }
            }

            this.fields = propertyInfoList.ToArray();
            this.fieldsNonPrimitive = nonPrimitivePropertyInfoList.ToArray();

            CheckForDoubleProperties(propertyInfo);
        }

        private string findPrefix = "";
        private void CheckForDoubleProperties(PropertyInfo[] baseInfo)
        {
            foreach (PropertyInfo property in baseInfo)
            {
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                {
                    if (!iOSHelper.IsPrimitiveDataType(property.PropertyType))
                    {
                        findPrefix += property.Name + "_";
                        currentPrefix = "";
                        if (CheckForDoubleProperties(propertyInfo, property))
                        {
                            // Create key
                            string name = property.PropertyType.Name.Replace("[]", string.Empty);
                            string elementName = findPrefix.Replace(property.Name + "_", name.TurnFirstToLower(false));
                            this.doubleProperties.Add(elementName, property);
                        }
                        
                        string modelName = property.PropertyType.Name.Replace("[]", string.Empty);
                        PropertyInfo[] fields = this.GetRelatedFields(modelName);
                        CheckForDoubleProperties(fields);

                        findPrefix = findPrefix.Replace(property.Name + "_", string.Empty);
                    }
                }
            }
        }

        private string currentPrefix = "";
        private bool CheckForDoubleProperties(PropertyInfo[] parent, PropertyInfo lookingFor)
        {
            bool foundDouble = false;
            foreach (PropertyInfo property in parent)
            {
                if (!iOSHelper.IsPrimitiveDataType(property.PropertyType))
                {
                    currentPrefix += property.Name + "_";
                    if ((!property.Equals(lookingFor) || currentPrefix != findPrefix) && property.Name.Equals(lookingFor.Name) )
                    {
                        foundDouble = true;
                        break;
                    }
                    
                    string modelName = property.PropertyType.Name.Replace("[]", string.Empty);
                    PropertyInfo[] fields = this.GetRelatedFields(modelName);
                    if (CheckForDoubleProperties(fields, lookingFor))
                    {
                        foundDouble = true;
                        break;
                    }
                    currentPrefix = currentPrefix.Replace(property.Name + "_", string.Empty);
                }
            }
            
            return foundDouble;
        }

        public void Write()
        {
            this.WriteIntroduction();
            this.WriteFields();
            this.WriteMethods();
            this.WriteStartElementMethod();
            this.WriteEndElementMethod();
            this.WriteCharactersMethod();
            this.WriteProperties();
            this.WriteEnum();
            this.WriteClassEnd();
        }

        private void WriteIntroduction()
        {
            this.WriteToStream("package net.craveinteractive.android.net.handlers;");
            this.WriteToStream("");
            this.WriteToStream("import net.craveinteractive.android.data.entityclasses.*;");
            this.WriteToStream("import net.craveinteractive.android.data.collectionclasses.*;");
            this.WriteToStream("import net.craveinteractive.android.net.supportclasses.ObyTypedResult;");
            this.WriteToStream("");
            this.WriteToStream("import org.xml.sax.Attributes;");
            this.WriteToStream("import org.xml.sax.SAXException;");
            //this.WriteToStream("import org.xml.sax.helpers.DefaultHandler;");
            this.WriteToStream("");
            this.WriteToStream("public class {0}Handler extends WebserviceContentHandler", this.method.Name);
            this.WriteToStream("{{");
        }

        private void WriteFields()
        {
            this.WriteToStream("\t// ===========================================================");
            this.WriteToStream("\t// Fields");
            this.WriteToStream("\t// ===========================================================");
            this.WriteToStream("");

            this.WriteToStream("\tprivate Element currentElement = Element.{0};", this.typeName.ToUpper());
            this.WriteToStream("");

            this.WriteToStream("\tprivate StringBuilder buffer = null;");
            this.WriteToStream("\tprivate String attributeParent = \"\";");
            this.WriteToStream("");

            this.WriteToStream("\tprivate ObyTypedResult result = null;");
            this.WriteToStream("\tprivate boolean in_ResultType = false;");
            this.WriteToStream("\tprivate boolean in_ResultMessage = false;");
            this.WriteToStream("\tprivate boolean in_ResultCode = false;");
            this.WriteToStream("");
            this.WriteToStream("\tprivate {0}Collection {1}Collection = null;", this.typeName, this.typeName.TurnFirstToLower(false));
            this.WriteToStream("\tprivate {0}Entity {1}Entity = null;", this.typeName, this.typeName.TurnFirstToLower(false));
            this.WriteToStream("");

            foreach (var field in this.fieldsNonPrimitive)
            {
                string typeName = field.PropertyType.Name.Replace("[]", string.Empty);
                this.WriteToStream("\tprivate {0}Entity {1}Entity = null;", typeName, typeName.TurnFirstToLower(false));

                this.WriteRelatedMembers(field);
            }

            this.WriteToStream("");

            this.WriteToStream("\t// {0} flags", this.typeName);
            // GK Seems unnecessary:
            // this.WriteToStream("\tprivate boolean in_{0} = false;", this.typeName);

            List<PropertyInfo> nonPrimitiveFields = new List<PropertyInfo>();

            foreach (var field in this.fields)
            {
                if (iOSHelper.IsPrimitiveDataType(field.PropertyType))
                    this.WriteToStream("\tprivate boolean in_{0} = false;", field.Name);
                else
                    nonPrimitiveFields.Add(field);
            }

            if (nonPrimitiveFields.Count > 0)
            {
                foreach (var field in nonPrimitiveFields)
                {
                    this.WriteRelatedFields(field);
                }
            }

            this.WriteToStream("");
        }

        private string relatedMemberPrefix = "";
        private void WriteRelatedMembers(PropertyInfo field)
        {
            relatedMemberPrefix += field.Name + "_";
            PropertyInfo[] fields = this.GetRelatedFields(field.PropertyType.Name.Replace("[]", string.Empty));
            foreach (var propertyInfo in fields)
            {
                if (!iOSHelper.IsPrimitiveDataType(propertyInfo.PropertyType) && propertyInfo.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                {
                    string typeName = propertyInfo.PropertyType.Name.Replace("[]", string.Empty);
                    this.WriteToStream("\tprivate {0}Entity {1}{2}Entity = null;", typeName, relatedMemberPrefix.TurnFirstToLower(false), typeName.TurnFirstToLower(false));

                    this.WriteRelatedMembers(propertyInfo);
                }
            }
            relatedMemberPrefix = relatedMemberPrefix.Replace(field.Name + "_", string.Empty);
        }

        private string relatedFieldPrefix = "";
        private void WriteRelatedFields(PropertyInfo field)
        {
            string modelName = field.PropertyType.Name.Replace("[]", string.Empty);
            PropertyInfo[] fields = this.GetRelatedFields(modelName);

            this.WriteToStream("");
            this.WriteToStream("\t// {0} flags", modelName);

            List<PropertyInfo> nonPrimitiveFields = new List<PropertyInfo>();

            for (int j = 0; j < fields.Length; j++)
            {
                PropertyInfo propertyInfo = fields[j];
                if (propertyInfo.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                {
                    if (iOSHelper.IsPrimitiveDataType(propertyInfo.PropertyType))
                        this.WriteToStream("\tprivate boolean in_{0}{1}_{2} = false;", relatedFieldPrefix, field.Name, propertyInfo.Name);
                    else
                        nonPrimitiveFields.Add(propertyInfo);
                }
            }

            relatedFieldPrefix += field.Name + "_";
            foreach (var propertyInfo in nonPrimitiveFields)
            {
                this.WriteRelatedFields(propertyInfo);
            }
            relatedFieldPrefix = relatedFieldPrefix.Replace(field.Name + "_", string.Empty);
        }

        private void WriteMethods()
        {
            this.WriteToStream("\t// ===========================================================");
            this.WriteToStream("\t// Methods");
            this.WriteToStream("\t// ===========================================================");
            this.WriteToStream("");
            this.WriteToStream("\t@Override");
            this.WriteToStream("\tpublic void startDocument() throws SAXException");
            this.WriteToStream("\t{{");
            this.WriteToStream("\t\tthis.result = new ObyTypedResult();");
            this.WriteToStream("\t\tthis.{0}Collection = new {1}Collection();", this.typeName.TurnFirstToLower(false), this.typeName);
            this.WriteToStream("\t}}");
            this.WriteToStream("");
            this.WriteToStream("\t@Override");
            this.WriteToStream("\tpublic void endDocument() throws SAXException");
            this.WriteToStream("\t{{");
            this.WriteToStream("\t\tthis.result.setModelCollection(this.{0}Collection);", this.typeName.TurnFirstToLower(false));
            this.WriteToStream("\t}}");
            this.WriteToStream("");
        }

        private void WriteStartElementMethod()
        {
            this.WriteToStream("\t@Override");
            this.WriteToStream("\tpublic void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException");
            this.WriteToStream("\t{{");
            this.WriteToStream("\t\tthis.buffer = new StringBuilder();");
            this.WriteToStream("");
            this.WriteToStream("\t\tif (localName.equals(\"ResultType\"))");
            this.WriteToStream("\t\t\tthis.in_ResultType = true;");
            this.WriteToStream("\t\telse if (localName.equals(\"ResultMessage\"))");
            this.WriteToStream("\t\t\tthis.in_ResultMessage = true;");
            this.WriteToStream("\t\telse if (localName.equals(\"ResultCode\"))");
            this.WriteToStream("\t\t\tthis.in_ResultCode = true;");
            this.WriteToStream("");
            this.WriteToStream("\t\tif (localName.equals(\"{0}\"))", this.typeName);
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\tthis.currentElement = Element.{0};", this.typeName.ToUpper());
            this.WriteToStream("\t\t\tthis.{0}Entity = new {1}Entity();", this.typeName.TurnFirstToLower(false), this.typeName);
            this.WriteToStream("\t\t}}");
            for (int i = 0; i < this.fields.Length; i++)
            {
                PropertyInfo propertyInfo = this.fields[i];

                if (!iOSHelper.IsPrimitiveDataType(propertyInfo.PropertyType))
                    this.WriteRelatedStartMethod(propertyInfo);
            }
            this.WriteToStream("");

            this.WriteToStream("\t\tif (this.currentElement == Element.{0})", this.typeName.ToUpper());
            this.WriteToStream("\t\t{{");

            List<PropertyInfo> nonPrimitiveFields = new List<PropertyInfo>();

            for (int i = 0; i < this.fields.Length; i++)
            {
                PropertyInfo field = this.fields[i];

                if (!iOSHelper.IsPrimitiveDataType(field.PropertyType))
                    nonPrimitiveFields.Add(field);
                else
                {
                    this.WriteToStream("\t\t\t{0}if (localName.equals(\"{1}\"))", (i == 0 ? "" : "else "), field.Name);
                    this.WriteToStream("\t\t\t\tthis.in_{0} = true;", field.Name);
                }
            }
            this.WriteToStream("\t\t}}");

            foreach (var field in nonPrimitiveFields)
            {
                this.WriteRelatedStartMethodFields(field);
            }

            this.WriteToStream("\t}}");
            this.WriteToStream("");
        }

        private string relatedStartMethodFieldsPrefix = "";
        private void WriteRelatedStartMethodFields(PropertyInfo field)
        {
            string modelName = field.PropertyType.Name.Replace("[]", string.Empty);
            PropertyInfo[] fields = this.GetRelatedFields(modelName);

            this.WriteToStream("\t\telse if (this.currentElement == Element.{0}{1})", relatedStartMethodFieldsPrefix.ToUpper(), modelName.ToUpper());
            this.WriteToStream("\t\t{{");

            List<PropertyInfo> nonPrimitiveFields = new List<PropertyInfo>();

            for (int i = 0; i < fields.Length; i++)
            {
                PropertyInfo propertyInfo = fields[i];
                if (propertyInfo.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                {
                    if (!iOSHelper.IsPrimitiveDataType(propertyInfo.PropertyType))
                        nonPrimitiveFields.Add(propertyInfo);
                    else
                    {
                        this.WriteToStream("\t\t\t{0}if (localName.equals(\"{1}\"))", (i == 0 ? "" : "else "), propertyInfo.Name);
                        this.WriteToStream("\t\t\t\tthis.in_{0}{1}_{2} = true;", relatedStartMethodFieldsPrefix, field.Name, propertyInfo.Name);
                    }
                }
            }

            this.WriteToStream("\t\t}}");

            relatedStartMethodFieldsPrefix += field.Name + "_";
            foreach (var propertyInfo in nonPrimitiveFields)
            {
                this.WriteRelatedStartMethodFields(propertyInfo);
            }
            relatedStartMethodFieldsPrefix = relatedStartMethodFieldsPrefix.ReplaceLastOccurrence(field.Name + "_", string.Empty);
        }

        private string relatedStartMethodPrefix = "";
        private void WriteRelatedStartMethod(PropertyInfo field)
        {
            string modelName = field.PropertyType.Name.Replace("[]", string.Empty);
            PropertyInfo[] fields = this.GetRelatedFields(modelName);

            if (!this.doublPropertiesHandled.Contains(modelName))
            {
                this.WriteToStream("\t\telse if (localName.equals(\"{0}\"))", modelName);
                this.WriteToStream("\t\t{{");

                // Check if we have more properties with this name
                var dictionary = GetDoubleProperties(field);
                if (dictionary.Count > 0)
                {
                    this.WriteToStream("\t\t\tif (atts.getLength() > 0)");
                    this.WriteToStream("\t\t\t{{");
                    this.WriteToStream("\t\t\t\tthis.attributeParent = atts.getValue(0);");
                    this.WriteToStream("\t\t\t}}");
                    this.WriteToStream("\t\t\telse");
                    this.WriteToStream("\t\t\t{{");
                    this.WriteToStream("\t\t\t\tthis.attributeParent = \"\";");
                    this.WriteToStream("\t\t\t}}");
                    bool isFirst = true;
                    foreach (KeyValuePair<string, PropertyInfo> doubleValue in dictionary)
                    {
                        this.doublPropertiesHandled.Add(modelName);
                        string parentKey = doubleValue.Key.ToUpper();
                        parentKey = parentKey.ReplaceLastOccurrence(modelName.ToUpper(), string.Empty);
                        parentKey = parentKey.ReplaceRight("_", string.Empty);
                        this.WriteToStream("\t\t\t{0}if (this.attributeParent.equals(\"{1}\"))", (isFirst ? "" : "else "), parentKey);
                        this.WriteToStream("\t\t\t{{");
                        this.WriteToStream("\t\t\t\tthis.currentElement = Element.{0};", doubleValue.Key.ToUpper());
                        this.WriteToStream("\t\t\t\tthis.{0}Entity = new {1}Entity();", doubleValue.Key.TurnFirstToLower(false), modelName);
                        this.WriteToStream("\t\t\t}}");

                        if (isFirst)
                            isFirst = false;
                    }
                }
                else
                {
                    this.WriteToStream("\t\t\tthis.currentElement = Element.{0}{1};", relatedStartMethodPrefix.ToUpper(), modelName.ToUpper());
                    this.WriteToStream("\t\t\tthis.{0}{1}Entity = new {2}Entity();", relatedStartMethodPrefix.TurnFirstToLower(false), modelName.TurnFirstToLower(false), modelName);
                }

                this.WriteToStream("\t\t}}");
            }

            relatedStartMethodPrefix += field.Name + "_";
            foreach (PropertyInfo propertyInfo in fields)
            {
                if (!iOSHelper.IsPrimitiveDataType(propertyInfo.PropertyType) && propertyInfo.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                    this.WriteRelatedStartMethod(propertyInfo);
            }
            relatedStartMethodPrefix = relatedStartMethodPrefix.ReplaceLastOccurrence(field.Name + "_", string.Empty);
        }

        private void WriteEndElementMethod()
        {
            this.WriteToStream("\t@Override");
            this.WriteToStream("\tpublic void endElement(String namespaceURI, String localName, String qName) throws SAXException");
            this.WriteToStream("\t{{");
            this.WriteToStream("\t\tthis.buffer = null;");
            this.WriteToStream("");
            this.WriteToStream("\t\tif (localName.equals(\"ResultType\"))");
            this.WriteToStream("\t\t\tthis.in_ResultType = false;");
            this.WriteToStream("\t\telse if (localName.equals(\"ResultMessage\"))");
            this.WriteToStream("\t\t\tthis.in_ResultMessage = false;");
            this.WriteToStream("\t\telse if (localName.equals(\"ResultCode\"))");
            this.WriteToStream("\t\t\tthis.in_ResultCode = false;");
            this.WriteToStream("");
            this.WriteToStream("\t\tif (localName.equals(\"{0}\"))", this.typeName);
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\tthis.currentElement = Element.{0};", this.typeName.ToUpper());
            this.WriteToStream("\t\t\tthis.{0}Collection.add(this.{0}Entity);", this.typeName.TurnFirstToLower(false));
            this.WriteToStream("\t\t}}");
            for (int i = 0; i < this.fields.Length; i++)
            {
                PropertyInfo propertyInfo = this.fields[i];

                if (!iOSHelper.IsPrimitiveDataType(propertyInfo.PropertyType))
                    this.WriteRelatedEndMethod(propertyInfo, this.typeName);
            }
            this.WriteToStream("");

            this.WriteToStream("\t\tif (this.currentElement == Element.{0})", this.typeName.ToUpper());
            this.WriteToStream("\t\t{{");

            List<PropertyInfo> nonPrimitiveFields = new List<PropertyInfo>();

            for (int i = 0; i < this.fields.Length; i++)
            {
                PropertyInfo field = this.fields[i];

                if (!iOSHelper.IsPrimitiveDataType(field.PropertyType))
                    nonPrimitiveFields.Add(field);
                else
                {
                    this.WriteToStream("\t\t\t{0}if (localName.equals(\"{1}\"))", (i == 0 ? "" : "else "), field.Name);
                    this.WriteToStream("\t\t\t\tthis.in_{0} = false;", field.Name);
                }
            }
            this.WriteToStream("\t\t}}");

            foreach (var field in nonPrimitiveFields)
            {
                this.WriteRelatedEndMethodFields(field);
            }

            this.WriteToStream("\t}}");
            this.WriteToStream("");
        }

        private string relatedEndMethodFieldsPrefix = "";
        private void WriteRelatedEndMethodFields(PropertyInfo field)
        {
            string modelName = field.PropertyType.Name.Replace("[]", string.Empty);
            PropertyInfo[] fields = this.GetRelatedFields(modelName);

            this.WriteToStream("\t\telse if (this.currentElement == Element.{0}{1})", relatedEndMethodFieldsPrefix.ToUpper(), modelName.ToUpper());
            this.WriteToStream("\t\t{{");

            List<PropertyInfo> nonPrimitiveFields = new List<PropertyInfo>();

            for (int i = 0; i < fields.Length; i++)
            {
                PropertyInfo propertyInfo = fields[i];

                if (propertyInfo.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                {
                    if (!iOSHelper.IsPrimitiveDataType(propertyInfo.PropertyType))
                        nonPrimitiveFields.Add(propertyInfo);
                    else
                    {
                        this.WriteToStream("\t\t\t{0}if (localName.equals(\"{1}\"))", (i == 0 ? "" : "else "), propertyInfo.Name);
                        this.WriteToStream("\t\t\t\tthis.in_{0}{1}_{2} = false;", relatedEndMethodFieldsPrefix, field.Name, propertyInfo.Name);
                    }
                }
            }

            this.WriteToStream("\t\t}}");

            relatedEndMethodFieldsPrefix += field.Name + "_";
            foreach (var propertyInfo in nonPrimitiveFields)
            {
                this.WriteRelatedEndMethodFields(propertyInfo);
            }
            relatedEndMethodFieldsPrefix = relatedEndMethodFieldsPrefix.ReplaceLastOccurrence(field.Name + "_", string.Empty);
        }

        private string relatedEndMethodPrefix = "";
        private void WriteRelatedEndMethod(PropertyInfo field, string parent)
        {
            string modelName = field.PropertyType.Name.Replace("[]", string.Empty);
            PropertyInfo[] fields = this.GetRelatedFields(modelName);


            if (!this.doublPropertiesHandled.Contains(modelName + "_end"))
            {
                this.WriteToStream("\t\telse if (localName.equals(\"{0}\"))", modelName);
                this.WriteToStream("\t\t{{");

                // Check if we have more properties with this name
                var dictionary = GetDoubleProperties(field);
                if (dictionary.Count > 0)
                {
                    bool isFirst = true;
                    foreach (KeyValuePair<string, PropertyInfo> doubleValue in dictionary)
                    {
                        this.doublPropertiesHandled.Add(modelName + "_end");
                        string parentKey = doubleValue.Key.ToUpper();
                        parentKey = parentKey.ReplaceLastOccurrence(modelName.ToUpper(), string.Empty);
                        parentKey = parentKey.ReplaceRight("_", string.Empty);

                        parent = doubleValue.Value.ReflectedType.Name.Replace("[]", string.Empty);
                        this.WriteToStream("\t\t\t{0}if (this.attributeParent.equals(\"{1}\"))", (isFirst ? "" : "else "), parentKey);
                        this.WriteToStream("\t\t\t{{");
                        this.WriteToStream("\t\t\t\tthis.currentElement = Element.{0};", doubleValue.Key.ToUpper());
                        this.WriteToStream("\t\t\t\tthis.{0}Entity.get{1}().add(this.{2}Entity);", parent.TurnFirstToLower(false), field.Name, doubleValue.Key.TurnFirstToLower(false));
                        this.WriteToStream("\t\t\t}}");

                        if (isFirst)
                            isFirst = false;
                    }
                }
                else
                {
                    this.WriteToStream("\t\t\tthis.currentElement = Element.{0}{1};", relatedEndMethodPrefix.ToUpper(), modelName.ToUpper());
                    this.WriteToStream("\t\t\tthis.{0}Entity.get{1}().add(this.{2}{3}Entity);", parent.TurnFirstToLower(false), field.Name, relatedEndMethodPrefix.TurnFirstToLower(false), modelName.TurnFirstToLower(false));
                }

                this.WriteToStream("\t\t}}");
            }

            string prefixWithoutSelf = relatedEndMethodPrefix;
            relatedEndMethodPrefix += field.Name + "_";
            foreach (PropertyInfo propertyInfo in fields)
            {
                if (!iOSHelper.IsPrimitiveDataType(propertyInfo.PropertyType) && propertyInfo.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                    this.WriteRelatedEndMethod(propertyInfo, prefixWithoutSelf + field.PropertyType.Name.Replace("[]", string.Empty).TurnFirstToLower(false));
            }
            relatedEndMethodPrefix = relatedEndMethodPrefix.ReplaceLastOccurrence(field.Name + "_", string.Empty);
        }

        private void WriteCharactersMethod()
        {
            this.WriteToStream("\t@Override");
            this.WriteToStream("\tpublic void characters(char ch[], int start, int length)");
            this.WriteToStream("\t{{");
            this.WriteToStream("\t\tif (this.buffer != null)");
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\tfor (int i=start; i<start+length; i++) {{");
            this.WriteToStream("\t\t\t\tthis.buffer.append(ch[i]);");
            this.WriteToStream("\t\t\t}}");
            this.WriteToStream("\t\t}}");
            this.WriteToStream("");
            this.WriteToStream("\t\tString value = \"\";");
            this.WriteToStream("\t\tif (this.buffer != null)");
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\tvalue = this.buffer.toString().trim();");
            this.WriteToStream("\t\t\tif (value != \"\")");
            this.WriteToStream("\t\t\t{{");
            this.WriteToStream("\t\t\t\ttry");
            this.WriteToStream("\t\t\t\t{{");
            this.WriteToStream("\t\t\t\t\tif (this.in_ResultType)");
            this.WriteToStream("\t\t\t\t\t\tthis.result.setResultType(value);");
            this.WriteToStream("\t\t\t\t\telse if (this.in_ResultMessage)");
            this.WriteToStream("\t\t\t\t\t\tthis.result.setResultMessage(value);");
            this.WriteToStream("\t\t\t\t\telse if (this.in_ResultCode)");
            this.WriteToStream("\t\t\t\t\t\tthis.result.setResultCode(Integer.parseInt(value));");
            this.WriteToStream("");
            this.WriteToStream("\t\t\t\t\tif (this.currentElement == Element.{0} && this.{1}Entity != null)", this.typeName.ToUpper(), this.typeName.TurnFirstToLower(false));
            this.WriteToStream("\t\t\t\t\t{{");

            List<PropertyInfo> nonPrimitiveFields = new List<PropertyInfo>();

            for (int i = 0; i < this.fields.Length; i++)
            {
                PropertyInfo propertyInfo = this.fields[i];

                if (!iOSHelper.IsPrimitiveDataType(propertyInfo.PropertyType))
                    nonPrimitiveFields.Add(propertyInfo);
                else
                {
                    string fieldName = propertyInfo.Name;
                    string typeName = iOSHelper.GetiOSDataType(propertyInfo.PropertyType);

                    switch (typeName)
                    {
                        case "boolean":
                            this.WriteToStream("\t\t\t\t\t\t{1}if (this.in_{0})", fieldName, (i == 0 ? "" : "else "));
                            this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(Boolean.parseBoolean(value));", this.typeName.TurnFirstToLower(false), fieldName);
                            break;
                        case "String":
                            this.WriteToStream("\t\t\t\t\t\t{1}if (this.in_{0})", fieldName, (i == 0 ? "" : "else "));
                            this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(value);", this.typeName.TurnFirstToLower(false), fieldName);
                            break;
                        case "int":
                            this.WriteToStream("\t\t\t\t\t\t{1}if (this.in_{0})", fieldName, (i == 0 ? "" : "else "));
                            this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(Integer.parseInt(value));", this.typeName.TurnFirstToLower(false), fieldName);
                            break;
                        case "short":
                            this.WriteToStream("\t\t\t\t\t\t{1}if (this.in_{0})", fieldName, (i == 0 ? "" : "else "));
                            this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(Short.parseShort(value));", this.typeName.TurnFirstToLower(false), fieldName);
                            break;
                        case "long":
                            this.WriteToStream("\t\t\t\t\t\t{1}if (this.in_{0})", fieldName, (i == 0 ? "" : "else "));
                            this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(Long.parseLong(value));", this.typeName.TurnFirstToLower(false), fieldName);
                            break;
                        case "double":
                            this.WriteToStream("\t\t\t\t\t\t{1}if (this.in_{0})", fieldName, (i == 0 ? "" : "else "));
                            this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(Double.parseDouble(value));", this.typeName.TurnFirstToLower(false), fieldName);
                            break;
                        case "float":
                            this.WriteToStream("\t\t\t\t\t\t{1}if (this.in_{0})", fieldName, (i == 0 ? "" : "else "));
                            this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(Float.parseFloat(value));", this.typeName.TurnFirstToLower(false), fieldName);
                            break;
                        case "Vector<Integer>":
                            this.WriteToStream("\t\t\t\t\t\t{1}if (this.in_{0})", fieldName, (i == 0 ? "" : "else "));
                            this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.get{1}().add(Integer.parseInt(value));", this.typeName.TurnFirstToLower(false), fieldName);
                            break;
                        case "Date":
                            this.WriteToStream("\t\t\t\t\t\t{1}if (this.in_{0})", fieldName, (i == 0 ? "" : "else "));
                            //this.WriteToStream("\t\t\t\t\t\t\tString pattern = DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.getPattern();");
                            //this.WriteToStream("\t\t\t\t\t\t\tDate d = DateUtils.parseDate(value, new String[] { pattern });");
                            //this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(d);", this.typeName.TurnFirstToLower(false), fieldName);
                            this.WriteToStream("\t\t\t\t\t\t\t{{");
                            this.WriteToStream("\t\t\t\t\t\t\t\tjava.text.SimpleDateFormat parser = new java.text.SimpleDateFormat(\"yyyy-MM-d'T'HH:mm:ss\");");
                            this.WriteToStream("\t\t\t\t\t\t\t\tthis.{0}Entity.set{1}(parser.parse(value));", this.typeName.TurnFirstToLower(false), fieldName);
                            this.WriteToStream("\t\t\t\t\t\t\t}}");
                            break;
                    }
                }
            }
            this.WriteToStream("\t\t\t\t\t}}");

            foreach (var field in nonPrimitiveFields)
            {
                this.WriteRelatedCharactersMethod(field);
            }

            this.WriteToStream("\t\t\t\t}}");
            this.WriteToStream("\t\t\t\tcatch (Exception ex)");
            this.WriteToStream("\t\t\t\t{{");
            this.WriteToStream("\t\t\t\t\tSystem.out.println(\"Exception \" + ex.getMessage() + \" on value:\" + value);");
            this.WriteToStream("\t\t\t\t}}");
            this.WriteToStream("\t\t\t}}");
            this.WriteToStream("\t\t}}");
            this.WriteToStream("\t}}");
            this.WriteToStream("");
        }

        private string relatedCharactersMethodPrefix = "";
        private void WriteRelatedCharactersMethod(PropertyInfo field)
        {
            string modelName = field.PropertyType.Name.Replace("[]", string.Empty);
            PropertyInfo[] fields = this.GetRelatedFields(modelName);

            string varName = relatedCharactersMethodPrefix.TurnFirstToLower(false) + modelName.TurnFirstToLower(false);
            this.WriteToStream("\t\t\t\t\telse if (this.currentElement == Element.{0}{1} && this.{2}Entity != null)", relatedCharactersMethodPrefix.ToUpper(), modelName.ToUpper(), varName);
            this.WriteToStream("\t\t\t\t\t{{");

            List<PropertyInfo> nonPrimitiveFields = new List<PropertyInfo>();

            for (int i = 0; i < fields.Length; i++)
            {
                PropertyInfo propertyInfo = fields[i];
                if (propertyInfo.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                {
                    if (!iOSHelper.IsPrimitiveDataType(propertyInfo.PropertyType))
                        nonPrimitiveFields.Add(propertyInfo);
                    else
                    {
                        string fieldName = propertyInfo.Name;
                        string typeName = iOSHelper.GetiOSDataType(propertyInfo.PropertyType);

                        switch (typeName)
                        {
                            case "boolean":
                                this.WriteToStream("\t\t\t\t\t\t{0}if (this.in_{1}{2}_{3})", (i == 0 ? "" : "else "), relatedCharactersMethodPrefix, field.Name, fieldName);
                                this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(Boolean.parseBoolean(value));", varName, fieldName);
                                break;
                            case "String":
                                this.WriteToStream("\t\t\t\t\t\t{0}if (this.in_{1}{2}_{3})", (i == 0 ? "" : "else "), relatedCharactersMethodPrefix, field.Name, fieldName);
                                this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(value);", varName, fieldName);
                                break;
                            case "int":
                                this.WriteToStream("\t\t\t\t\t\t{0}if (this.in_{1}{2}_{3})", (i == 0 ? "" : "else "), relatedCharactersMethodPrefix, field.Name, fieldName);
                                this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(Integer.parseInt(value));", varName, fieldName);
                                break;
                            case "short":
                                this.WriteToStream("\t\t\t\t\t\t{0}if (this.in_{1}{2}_{3})", (i == 0 ? "" : "else "), relatedCharactersMethodPrefix, field.Name, fieldName);
                                this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(Short.parseShort(value));", varName, fieldName);
                                break;
                            case "long":
                                this.WriteToStream("\t\t\t\t\t\t{0}if (this.in_{1}{2}_{3})", (i == 0 ? "" : "else "), relatedCharactersMethodPrefix, field.Name, fieldName);
                                this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(Long.parseLong(value));", varName, fieldName);
                                break;
                            case "double":
                                this.WriteToStream("\t\t\t\t\t\t{0}if (this.in_{1}{2}_{3})", (i == 0 ? "" : "else "), relatedCharactersMethodPrefix, field.Name, fieldName);
                                this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(Double.parseDouble(value));", varName, fieldName);
                                break;
                            case "float":
                                this.WriteToStream("\t\t\t\t\t\t{0}if (this.in_{1}{2}_{3})", (i == 0 ? "" : "else "), relatedCharactersMethodPrefix, field.Name, fieldName);
                                this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(Float.parseFloat(value));", varName, fieldName);
                                break;
                            case "date":
                                this.WriteToStream("\t\t\t\t\t\t{0}if (this.in_{1}{2}_{3})", (i == 0 ? "" : "else "), relatedCharactersMethodPrefix, field.Name, fieldName);
                                this.WriteToStream("\t\t\t\t\t\t\tString pattern = DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.getPattern();");
                                this.WriteToStream("\t\t\t\t\t\t\tDate d = DateUtils.parseDate(value, new String[] { pattern });");
                                this.WriteToStream("\t\t\t\t\t\t\tthis.{0}Entity.set{1}(d);", modelName.TurnFirstToLower(false), fieldName);
                                break;
                        }
                    }
                }
            }
            this.WriteToStream("\t\t\t\t\t}}");

            relatedCharactersMethodPrefix += field.Name + "_";
            foreach (var field2 in nonPrimitiveFields)
            {
                this.WriteRelatedCharactersMethod(field2);
            }
            relatedCharactersMethodPrefix = relatedCharactersMethodPrefix.Replace(field.Name + "_", string.Empty);

        }

        private void WriteProperties()
        {
            this.WriteToStream("\t// ===========================================================");
            this.WriteToStream("\t// Properties");
            this.WriteToStream("\t// ===========================================================");
            this.WriteToStream("");
            this.WriteToStream("\tpublic ObyTypedResult getObyTypedResult()");
            this.WriteToStream("\t{{");
            this.WriteToStream("\t\treturn this.result;");
            this.WriteToStream("\t}}");
            this.WriteToStream("");
        }

        private PropertyInfo[] GetRelatedFields(string modelName)
        {
            PropertyInfo[] fields = new PropertyInfo[0];
            Type type = this.modelAssembly.GetType(string.Format("Obymobi.Logic.Model.{0}", modelName));
            if (type != null)
            {
                object model = Dionysos.InstanceFactory.CreateInstance(this.modelAssembly, type);

                fields = Dionysos.Reflection.Member.GetPropertyInfo(model);
            }
            return fields;
        }

        private string GetTabs(int level)
        {
            string tabs = "\t\t\t\t\t\t";
            for (int i = 0; i < level; i++)
            {
                tabs += "\t";
            }
            return tabs;
        }

        private char GetIndex(int level)
        {
            char index = 'j';
            index += (char)this.index;
            return index;
        }

        private Dictionary<string, PropertyInfo> GetDoubleProperties(PropertyInfo propertyInfo)
        {
            return doubleProperties.Where(doubleProperty => doubleProperty.Value.Name == propertyInfo.Name).ToDictionary(doubleProperty => doubleProperty.Key, doubleProperty => doubleProperty.Value);
        }

        private void WriteClassEnd()
        {
            WriteToStream("}}");
        }

        private void WriteEnum()
        {
            this.WriteToStream("\t// ===========================================================");
            this.WriteToStream("\t// Enum");
            this.WriteToStream("\t// ===========================================================");
            this.WriteToStream("");
            this.WriteToStream("\tprivate enum Element");
            this.WriteToStream("\t{{");
            this.WriteToStream("\t\t{0},", this.typeName.ToUpper());

            foreach (var field in this.fields)
            {
                if (!iOSHelper.IsPrimitiveDataType(field.PropertyType))
                {
                    this.WriteToStream("\t\t{0},", field.PropertyType.Name.Replace("[]", "").ToUpper());
                    this.WriteRelatedEnum(field);
                }
            }

            this.WriteToStream("\t}}");
            this.WriteToStream("");
        }

        private string relatedEnumPrefix = "";
        private void WriteRelatedEnum(PropertyInfo field)
        {
            PropertyInfo[] fields = this.GetRelatedFields(field.PropertyType.Name.Replace("[]", ""));
            string fieldName = field.PropertyType.Name.Replace("[]", "").ToUpper();
            relatedEnumPrefix += field.Name.ToUpper() + "_";
            foreach (var propertyInfo in fields)
            {
                if (!iOSHelper.IsPrimitiveDataType(propertyInfo.PropertyType) && propertyInfo.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                {
                    this.WriteToStream("\t\t{0}{1},", relatedEnumPrefix, propertyInfo.PropertyType.Name.Replace("[]", "").ToUpper());
                    this.WriteRelatedEnum(propertyInfo);
                }
            }
            relatedEnumPrefix = relatedEnumPrefix.ReplaceLastOccurrence(field.Name.ToUpper() + "_", string.Empty);
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
