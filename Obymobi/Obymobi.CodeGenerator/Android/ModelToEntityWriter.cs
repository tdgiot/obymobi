﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Android
{
    public class ModelToEntityWriter
    {
        private Object model;
        private Type modelType;
        private StreamWriter stream;
        private PropertyInfo[] fields;

        public ModelToEntityWriter(Object model, StreamWriter stream)
        {
            this.model = model;
            this.modelType = model.GetType();
            this.stream = stream;

            PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
            List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
            for (int j = 0; j < propertyInfo.Length; j++)
            {
                PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForAndroid
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                {
                    propertyInfoList.Add(property);
                }
            }

            this.fields = propertyInfoList.ToArray();
        }

        public void WriteEntity()
        {
            this.WriteIntroduction();
            this.WriteClassStart();
            this.WriteClassEnd();
        }

        private void WriteIntroduction()
        {
            WriteToStream("/*");
            WriteToStream(" * {0}Entity.as", this.modelType.Name);
            WriteToStream(" * Generated entity class for {0}", this.modelType.FullName);
            WriteToStream(" */");
            WriteToStream("");
			WriteToStream("package {0}.data.entityclasses;", AndroidGenerator.BaseNamespace);
            WriteToStream("");
			WriteToStream("import {0}.data.entitybaseclasses.{1}EntityBase;", AndroidGenerator.BaseNamespace, this.modelType.Name);
            WriteToStream("");
        }

        private void WriteClassStart()
        {
            if (this.modelType.GetProperty("Media") != null && this.modelType.GetProperty("Media").PropertyType.ToString().Equals("Obymobi.Logic.Model.Media[]"))
                WriteToStream("public class {0}Entity extends {0}EntityBase implements net.craveinteractive.cravebase.data.IMediaContainingEntity", this.modelType.Name);
            else
                WriteToStream("public class {0}Entity extends {0}EntityBase", this.modelType.Name);
            WriteToStream("{{");
            WriteToStream("\t// ------------------  CONSTRUCTORS ------------------");
            WriteToStream("\t/**");
            WriteToStream("\t* Initialize an instance of the {0}Entity type", this.modelType.Name);
            WriteToStream("\t*/");
            WriteToStream("\tpublic {0}Entity()", this.modelType.Name);
            WriteToStream("\t{{");
            WriteToStream("\t}}");

            if (this.modelType.GetProperty("Media") != null && this.modelType.GetProperty("Media").PropertyType.ToString().Equals("Obymobi.Logic.Model.Media[]"))
            {
                WriteToStream("");
                WriteToStream("\tpublic int getMediaRelatedEntityId()");
                WriteToStream("\t{{");
                WriteToStream("\t\treturn NO ID, NO COMPILE;");
                WriteToStream("\t}}");
            }
        }

        private void WriteClassEnd()
        {
            WriteToStream("}}");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
