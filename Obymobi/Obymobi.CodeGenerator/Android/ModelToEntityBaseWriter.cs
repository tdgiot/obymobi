﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Android
{
    public class ModelToEntityBaseWriter
    {
        private Object model;
        private Type modelType;
        private StreamWriter stream;
        private PropertyInfo[] fields;

        public ModelToEntityBaseWriter(Object model, StreamWriter stream)
        {
            this.model = model;
            this.modelType = model.GetType();
            this.stream = stream;

            PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
            List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
            for (int j = 0; j < propertyInfo.Length; j++)
            {
                PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForAndroid
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                    propertyInfoList.Add(property);
            }

            this.fields = propertyInfoList.ToArray();
        }

        public void WriteEntity()
        {
            this.WriteIntroduction();
            this.WriteClassStart();
            this.WriteConstructor();
            this.WriteFields();
            this.WriteProperties();
            //this.WriteFromAndToDataStreamLogic();
            this.WriteXmlLogic();
            this.WriteCloneLogic();
            //this.WriteGenericPropertyAccessor();
            //this.WriteAdditionalLogic();
            this.WriteClassEnd();
        }

        private void WriteIntroduction()
        {
            WriteToStream("/*");
            WriteToStream(" * {0}EntityBase.as", this.modelType.Name);
            WriteToStream(" * Generated entity base class for {0}", this.modelType.FullName);
            WriteToStream(" */");
            WriteToStream("");
			WriteToStream("package {0}.data.entitybaseclasses;", AndroidGenerator.BaseNamespace);
            WriteToStream("");

            bool dateImported = false;
            bool vectorImported = false;

            foreach (var field in fields)
            {
                string datatype = AndroidHelper.GetAndroidDataType(field.PropertyType);
                if (datatype == "Date" && !dateImported)
                {
                    WriteToStream("import java.util.Date;");
                    dateImported = true;
                }
                else if (datatype == "Vector<Integer>" && !vectorImported)
                {
                    WriteToStream("import java.util.Vector;");
                    vectorImported = true;
                }
                else if (field.Name == "ProcessedOrderIds" && !vectorImported)
                {
                    WriteToStream("import java.util.Vector;");
                    vectorImported = true;
                }

                if(dateImported && vectorImported)
                    break;
            }

            if (modelType.GetCustomAttributes(typeof(CodeGeneratorSerializable), false).Length > 0)
            {
                WriteToStream("import java.io.Serializable;");
            }

            if (this.fields.Any(f => AndroidHelper.GetAndroidDataType(f.PropertyType).Contains("Collection")))
				WriteToStream("import {0}.data.collectionclasses.*;", AndroidGenerator.BaseNamespace);

			WriteToStream("import {0}.data.supportclasses.XmlHelper;", AndroidGenerator.BaseNamespace);
			WriteToStream("import {0}.data.supportclasses.JsonHelper;", AndroidGenerator.BaseNamespace);
			WriteToStream("import {0}.data.entityclasses.*;", AndroidGenerator.BaseNamespace);
            WriteToStream("");
        }

        private void WriteFields()
        {
            WriteToStream("\t// ===========================================================");
            WriteToStream("\t// Fields");
            WriteToStream("\t// ===========================================================");
            WriteToStream("");

            foreach (var field in fields)
            {
                string datatype = AndroidHelper.GetAndroidDataType(field.PropertyType);
                if (datatype == "boolean")
                    WriteToStream("\tprivate {0} {1} = false;", datatype, field.Name.TurnFirstTwoToLower(false));
                else if (datatype == "String")
                    WriteToStream("\tprivate {0} {1} = \"\";", datatype, field.Name.TurnFirstTwoToLower(false));
                else if (datatype == "int" || datatype == "short" || datatype == "long" || datatype == "float" || datatype == "double")
                    WriteToStream("\tprivate {0} {1} = 0;", datatype, field.Name.TurnFirstTwoToLower(false));
                else if (datatype == "Vector<Integer>")
                    WriteToStream("\tprivate {0} {1} =  new {0}();", datatype, field.Name.TurnFirstTwoToLower(false));
                else
                {
                    WriteToStream("\tprivate {0} {1} = new {0}();", datatype, field.Name.TurnFirstTwoToLower(false));
                }
            }

            WriteToStream("");
        }

        private void WriteClassStart()
        {
            if (modelType.GetCustomAttributes(typeof(CodeGeneratorSerializable), false).Length > 0)
            {
                WriteToStream("public class {0}EntityBase implements Serializable", this.modelType.Name);
            }
            else
            {
                WriteToStream("public class {0}EntityBase", this.modelType.Name);
            }
            WriteToStream("{{");
        }

        private void WriteConstructor()
        {
            WriteToStream("\t// ===========================================================");
            WriteToStream("\t// Constructors");
            WriteToStream("\t// ===========================================================");
            WriteToStream("\t/**");
            WriteToStream("\t* Initializes an instance of the {0} type class", this.modelType.Name);
            WriteToStream("\t*/");
            WriteToStream("\tpublic {0}EntityBase()", this.modelType.Name);
            WriteToStream("\t{{");
            WriteToStream("\t}}");
            WriteToStream("");
        }

        private void WriteProperties()
        {
            WriteToStream("\t// ===========================================================");
            WriteToStream("\t// Properties");
            WriteToStream("\t// ===========================================================");
            WriteToStream("");
            foreach (var field in fields)
            {
                string typeName = AndroidHelper.GetAndroidDataType(field.PropertyType);

                //if (field.Name.Equals("ProcessedOrderIds"))
                //{
                //    // The Setter
                //    WriteToStream("\tpublic void setProcessedOrderIds(Vector<Integer> value)");
                //    WriteToStream("\t{{");
                //    WriteToStream("\t\tthis.processedOrderIds = value;", field.Name.TurnFirstToLower(false));
                //    WriteToStream("\t}}");

                //    WriteToStream("");

                //    // The Getter
                //    WriteToStream("\tpublic Vector<Integer> getProcessedOrderIds()");
                //    WriteToStream("\t{{");
                //    WriteToStream("\t\treturn this.processedOrderIds;");
                //    WriteToStream("\t}}");
                //}
                //else
                //{
                if (typeName.Length > 0)
                {
                    // The Setter
                    WriteToStream("\tpublic void set{0}({1} value)", field.Name, typeName);
                    WriteToStream("\t{{");
                    WriteToStream("\t\tthis.{0} = value;", field.Name.TurnFirstTwoToLower(false));
                    WriteToStream("\t}}");

                    WriteToStream("");

                    // The Getter
                    WriteToStream("\tpublic {1} get{0}()", field.Name, typeName);
                    WriteToStream("\t{{");
                    WriteToStream("\t\treturn this.{0};", field.Name.TurnFirstTwoToLower(false));
                    WriteToStream("\t}}");
                }

                WriteToStream("");
            }
        }

        private void WriteXmlLogic()
        {
            WriteToStream("\t// ===========================================================");
            WriteToStream("\t// Methods");
            WriteToStream("\t// ===========================================================");
            WriteToStream("");

            #region ToXml
            WriteToStream("\tpublic String toXml()");
            WriteToStream("\t{{");
            WriteToStream("\t\tString xml = \"\";");
            WriteToStream("");
            WriteToStream("\t\txml += XmlHelper.createXmlOpeningTag(\"{0}\");", this.modelType.Name);
            WriteToStream("");

            // Write fields
            foreach (var field in fields)
            {
                if(!field.PropertyType.BaseType.Name.Equals("Array"))
                    WriteToStream("\t\txml += XmlHelper.createXmlElement(\"{0}\", this.get{0}());", field.Name);
            }
            foreach (var field in fields)
            {
                if (field.PropertyType.BaseType.Name.Equals("Array"))
                    WriteToStream("\t\txml += XmlHelper.createXmlElement(\"{0}\", this.get{0}());", field.Name);
            }

            WriteToStream("");
            WriteToStream("\t\txml += XmlHelper.createXmlClosingTag(\"{0}\");", this.modelType.Name);
            WriteToStream("");
            WriteToStream("\t\treturn xml;");
            WriteToStream("\t}}");
            #endregion
                        
            WriteToStream("");

            #region ToJson
            WriteToStream("\tpublic String toJson()");
            WriteToStream("\t{{");
            WriteToStream("\t\tString json = \"\";");
            WriteToStream("");
            WriteToStream("\t\tjson += JsonHelper.createJsonOpeningTag();");
            WriteToStream("");

            bool last = false;

            // Write fields
            for (int i = 0; i < this.fields.Length; i++)
            {
                PropertyInfo field = this.fields[i];
                last = (i == (this.fields.Length - 1));
                //if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
                WriteToStream("\t\tjson += JsonHelper.createJsonElement(\"{0}\", this.get{2}(), {1});", field.Name.TurnFirstToLower(false), last.ToString().ToLower(), field.Name);
            }

            WriteToStream("");
            WriteToStream("\t\tjson += JsonHelper.createJsonClosingTag();");
            WriteToStream("");
            WriteToStream("\t\treturn json;");
            WriteToStream("\t}}");
            #endregion

            WriteToStream("");
        }

        public void WriteCloneLogic()
        {
            WriteToStream("\t\t// ------------------  CLONE LOGIC ------------------");
            WriteToStream("");

            WriteToStream("\t\tpublic {0}Entity clone()", this.modelType.Name);
            WriteToStream("\t\t{{");
            WriteToStream("\t\t	{0}Entity clone = new {0}Entity();", this.modelType.Name);

            // Clone fields
            foreach (var field in fields)
            {
                //if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
                string typeName = AndroidHelper.GetAndroidDataType(field.PropertyType);
                if (typeName.Contains("Collection"))
                {
                    string entityBaseType = typeName.Replace("Collection", "EntityBase");
                    string entityType = typeName.Replace("Collection", "Entity");

                    // Clone Collection
                    WriteToStream("\t		for(Object source : this.get{0}())", field.Name);
                    WriteToStream("\t		{{");
                    //WriteToStream("\t		    @SuppressWarnings(\"unused\")");
                    WriteToStream("\t		    {0} source{0} = ({0})source;", entityType);
                    WriteToStream("\t		    {0} clone{0} = source{0}.clone();", entityType);
                    WriteToStream("\t		    clone.get{0}().add(clone{1});", field.Name, entityType);
                    WriteToStream("\t		}}");
                }
                else
                {
                    WriteToStream("\t		clone.set{0}(this.get{0}());", field.Name);
                }
            }

            WriteToStream("\t		return clone;");

            WriteToStream("\t\t}}");
            WriteToStream("");

        }

        //public void WriteGenericPropertyAccessor()
        //{
        //    WriteToStream("	// Generic Property Accessor");
        //    WriteToStream("	// <editor-fold>");
        //    WriteToStream("");

        //    #region SetPropery for values of type Object
        //    WriteToStream("	public void setProperty(String propertyName, Object value)");
        //    WriteToStream("	{{");
        //    WriteToStream("");

        //    bool firstIfClause = true;
        //    foreach (var field in fields)
        //    {
        //        if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
        //        {
        //            if (firstIfClause)
        //            {
        //                WriteToStream("		if(propertyName.compareTo(\"{0}\") == 0)", field.Name);
        //                firstIfClause = false;
        //            }
        //            else
        //                WriteToStream("		else if(propertyName.compareTo(\"{0}\") == 0)", field.Name);

        //            if (field.PropertyType.ToString() == typeof(Int32).ToString())
        //            {
        //                WriteToStream("			this.{0} = Integer.parseInt(value.toString());", field.Name.TurnFirstToLower(false));
        //            }
        //            else if (field.PropertyType.ToString() == typeof(String).ToString())
        //            {
        //                WriteToStream("			this.{0} = value.toString();", field.Name.TurnFirstToLower(false));
        //            }
        //            else
        //            {
        //                // Nohting.
        //            }
        //        }
        //    }

        //    // End switch & function
        //    WriteToStream("	}}");
        //    WriteToStream("");
        //    #endregion

        //    #region SetPropery for values of type int
        //    WriteToStream("	public void setProperty(String propertyName, int value)");
        //    WriteToStream("	{{");
        //    WriteToStream("");
        //    firstIfClause = true;
        //    foreach (var field in fields)
        //    {
        //        if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
        //        {
        //            if (firstIfClause)
        //            {
        //                WriteToStream("		if(propertyName.compareTo(\"{0}\") == 0)", field.Name);
        //                firstIfClause = false;
        //            }
        //            else
        //                WriteToStream("		else if(propertyName.compareTo(\"{0}\") == 0)", field.Name);

        //            if (field.PropertyType.ToString() == typeof(Int32).ToString())
        //            {
        //                WriteToStream("			this.{0} = value;", field.Name.TurnFirstToLower(false));
        //            }
        //            else if (field.PropertyType.ToString() == typeof(String).ToString())
        //            {
        //                WriteToStream("			this.{0} = Integer.toString(value);", field.Name.TurnFirstToLower(false));
        //            }
        //        }
        //    }

        //    // End switch & function
        //    WriteToStream("	}}");
        //    #endregion

        //    WriteToStream("");
        //    WriteToStream("	// </editor-fold>");
        //}

        //private void WriteAdditionalLogic()
        //{
        //    WriteToStream("	// Entity Base logic");
        //    WriteToStream("	// <editor-fold>");
        //    WriteToStream("");

        //    // Get Entity Instance
        //    WriteToStream("	public IEntity getNewEmptyInstance()");
        //    WriteToStream("	{{");
        //    WriteToStream("		return new {0}Entity();", this.modelType.Name);
        //    WriteToStream("	}}");
        //    WriteToStream("");

        //    // Get CollectionInstance
        //    WriteToStream("	public EntityCollectionBase getNewEmptyEntityCollection()");
        //    WriteToStream("	{{");
        //    WriteToStream("		return new {0}Collection();", this.modelType.Name);
        //    WriteToStream("	}}");
        //    WriteToStream("");

        //    // Unique EntityName
        //    WriteToStream("	public String uniqueEntityName()");
        //    WriteToStream("	{{");
        //    WriteToStream("		return \"{0}Entity\";", this.modelType.Name);
        //    WriteToStream("	}}");
        //    WriteToStream("");

        //    // Abstract methods for PK fields
        //    WriteToStream("	public abstract int getPkFieldValue();");
        //    WriteToStream("	public abstract void setPkFieldValue(int pkValue);");

        //    WriteToStream("");
        //    WriteToStream("	// </editor-fold>");

        //    // Clone object
        //}

        private void WriteClassEnd()
        {
            WriteToStream("}}");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
