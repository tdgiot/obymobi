﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.CodeGenerator.Android
{
    public class AndroidHelper
    {
        public static string GetAndroidDataType(Type dotNetType)
        {
            string type = string.Empty;
            switch (dotNetType.ToString())
            {
                case "System.Boolean":
                case "System.Nullable`1[System.Boolean]":
                    type = "boolean";
                    break;
                case "System.String":
                case "System.Nullable`1[System.String]":
                    type = "String";
                    break;
                case "System.Int32":
                case "System.UInt32":
                case "System.Nullable`1[System.Int32]":
                    type = "int";
                    break;
                case "System.Int16":
                    type = "short";
                    break;
                case "System.Int32[]":
                    type = "Vector<Integer>";
                    break;
                case "System.Int64":
                    type = "long";
                    break;
                case "System.Int64[]":
                    type = "Vector<Long>";
                    break;
                case "System.Decimal":
                case "System.Double":
                case "System.Nullable`1[System.Double]":
                    type = "double";
                    break;
                case "System.Single":
                    type = "float";
                    break;
                case "System.DateTime":
                case "System.Nullable`1[System.DateTime]":
                    type = "Date";
                    break;
            }

            if (type == string.Empty)
            {
                type = string.Format("{0}Collection", dotNetType.Name.Replace("[]", string.Empty));
            }

            return type;
        }

        public static bool IsPrimitiveDataType(Type dotNetType)
        {
            bool isPrimitive = true;

            string dataType = GetAndroidDataType(dotNetType);

            if (dataType != "boolean" && dataType != "String" && dataType != "int" &&
                dataType != "short" && dataType != "long" && dataType != "double" &&
                dataType != "float" && dataType != "Date" && dataType != "Vector<Integer>" &&
                dataType != "Vector<Long>")
                isPrimitive = false;

            return isPrimitive;
        }
    }
}
