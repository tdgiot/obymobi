﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using Obymobi.Attributes;
using Dionysos.Diagnostics;

namespace Obymobi.CodeGenerator.Android
{
    public class AndroidGenerator
    {
        #region Fields

		public static string BaseNamespace = "net.craveinteractive.cravebase";
        public static string PhilotesNamespace = "net.craveinteractive.philotes";

        private readonly string modelNamespace = string.Empty;
        private readonly Assembly modelAssemlby;
        private readonly string codePath = string.Empty;

	    private readonly List<string> excludedWebserviceMethod = new List<string>
		    {
			    "IsDatabaseAlive",
			    "IsWebserviceAlive",
			    "RequestToReprintConfirmation",
			    "ConfirmCode"
		    };

        #endregion

        #region Methods

        public AndroidGenerator(string codePath, Type typeOfOneModelClass)
        {
            this.codePath = codePath;

            this.modelNamespace = typeOfOneModelClass.Namespace;
            this.modelAssemlby = Assembly.GetAssembly(typeOfOneModelClass);
        }

        public void Generate()
        {
            Debug.WriteLine("AndroidGenerator.Generate - Start");

			Debug.WriteLine("AndroidGenerator.Generate - Analytics");

			AnalyticsWriter aw = new AnalyticsWriter(this.codePath);
			aw.Write();

			/*if (TestUtil.IsPcGabriel || TestUtil.IsPcBattleStationErik)
				codePath = @"D:\Development\Android\Philotes\src\net\craveinteractive\philotes";
			else if(TestUtil.IsPcBattleStationDanny)
				codePath = @"D:\Development\Test\android";*/

			// Get all classes to write
            IEnumerable<Type> models = this.GetAllModels();

            foreach (var modelClassType in models)
            {
                Debug.WriteLine(string.Format("AndroidGenerator.Generate - Model: {0}", modelClassType));
                if (modelClassType.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                {
                    this.WriteEntityBaseFile(modelClassType);
                    this.WriteEntityFile(modelClassType);
                    this.WriteEntityCollectionFile(modelClassType);
                }
            }

            IEnumerable<MethodInfo> methods = this.GetAllWebserviceMethods();
            //this.WriteWebserviceMap(methods);
            //this.WriteWebserviceConstants(methods);
            //this.WriteLocalserviceMap(methods);
            //this.WriteLocalserviceConstants(methods);

            foreach (var method in methods)
            {
                if (method.GetCustomAttributes(typeof(IncludeInCodeGeneratorForAndroid), false).Length > 0)
                {
					Debug.WriteLine(string.Format("AndroidGenerator.Generate - WriteWebserviceHandler: {0}", method));
                    this.WriteWebserviceHandler(method);
                    //this.WriteWebserviceEvent(method);
                    //this.WriteWebserviceResultEvent(method);
                    //this.WriteWebserviceHandler(method);
                }
            }

			Debug.WriteLine("AndroidGenerator.Generate - Finish");
        }

		private void WriteAnalyticsFiles()
		{
			AnalyticsWriter aw = new AnalyticsWriter(this.codePath);
			
		}

        private void WriteEntityBaseFile(Type modelType)
        {
            Object modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

            // Init the file
            string entityBaseClassDirectory = Path.Combine(this.codePath, "data\\entitybaseclasses");
            if (!Directory.Exists(entityBaseClassDirectory))
                Directory.CreateDirectory(entityBaseClassDirectory);
            var writer = new StreamWriter(Path.Combine(entityBaseClassDirectory, modelType.Name + "EntityBase.java"));

            // Write contents
            var entityWriter = new ModelToEntityBaseWriter(modelInstance, writer);
            entityWriter.WriteEntity();

            // Close
            writer.Flush();
            writer.Close();
        }

        private void WriteEntityFile(Type modelType)
        {
            var modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

            // Init the file
            var entityBaseClassDirectory = Path.Combine(this.codePath, "data\\entityclasses");
            if (!Directory.Exists(entityBaseClassDirectory))
                Directory.CreateDirectory(entityBaseClassDirectory);

            var fileName = Path.Combine(entityBaseClassDirectory, modelType.Name + "Entity.java");
            if (!File.Exists(fileName))
            {
                var writer = new StreamWriter(fileName);

                // Write contents
                var entityWriter = new ModelToEntityWriter(modelInstance, writer);
                entityWriter.WriteEntity();

                // Close
                writer.Flush();
                writer.Close();
            }
        }

        private void WriteEntityCollectionFile(Type modelType)
        {
            var modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

            // Init the file
            var collectionBaseClassDirectory = Path.Combine(this.codePath, "data\\collectionclasses");
            if (!Directory.Exists(collectionBaseClassDirectory))
                Directory.CreateDirectory(collectionBaseClassDirectory);

            var filePath = Path.Combine(collectionBaseClassDirectory, modelType.Name + "Collection.java");

            if (!File.Exists(filePath))
            {
                var writer = new StreamWriter(filePath);

                // Write contents
                var entityWriter = new ModelToCollectionWriter(modelInstance, writer);
                entityWriter.WriteCollection();

                // Close
                writer.Flush();
                writer.Close();
            }
        }

        //private void WriteWebserviceMap(List<MethodInfo> methods)
        //{
        //    //@"D:\Development\Flex\SjokieDokieAlpha01\src\maps\WebserviceMap.mxml"
        //    StreamWriter writer = new StreamWriter(Path.Combine(this.codePath, @"maps\WebserviceMap.mxml"));

        //    // Write contents
        //    WebserviceMapWriter webserviceMapWriter = new WebserviceMapWriter(writer, methods);
        //    webserviceMapWriter.Write();

        //    // Close
        //    writer.Flush();
        //    writer.Close();
        //}

        //private void WriteLocalserviceMap(List<MethodInfo> methods)
        //{
        //    //@"D:\Development\Flex\SjokieDokieAlpha01\src\maps\WebserviceMap.mxml"
        //    StreamWriter writer = new StreamWriter(Path.Combine(this.codePath, @"maps\LocalserviceMap.mxml"));

        //    // Write contents
        //    LocalserviceMapWriter localserviceMapWriter = new LocalserviceMapWriter(writer, methods);
        //    localserviceMapWriter.Write();

        //    // Close
        //    writer.Flush();
        //    writer.Close();
        //}

        //private void WriteWebserviceEvent(MethodInfo method)
        //{
        //    // Init the file
        //    string eventsDirectory = Path.Combine(this.codePath, @"net\obymobi\events");
        //    if (!Directory.Exists(eventsDirectory))
        //        Directory.CreateDirectory(eventsDirectory);

        //    StreamWriter writer = new StreamWriter(Path.Combine(eventsDirectory, method.Name + "Event.as"));

        //    // Write contents
        //    WebserviceEventWriter webserviceEventWriter = new WebserviceEventWriter(writer, method);
        //    webserviceEventWriter.Write();

        //    // Close
        //    writer.Flush();
        //    writer.Close();
        //}

        //private void WriteWebserviceResultEvent(MethodInfo method)
        //{
        //    // Init the file
        //    string eventsDirectory = Path.Combine(this.codePath, @"net\obymobi\events");
        //    if (!Directory.Exists(eventsDirectory))
        //        Directory.CreateDirectory(eventsDirectory);

        //    StreamWriter writer = new StreamWriter(Path.Combine(eventsDirectory, method.Name + "ResultEvent.as"));

        //    // Write contents
        //    WebserviceEventResultWriter webserviceEventResultWriter = new WebserviceEventResultWriter(writer, method);
        //    webserviceEventResultWriter.Write();

        //    // Close
        //    writer.Flush();
        //    writer.Close();
        //}

        private void WriteWebserviceHandler(MethodInfo method)
        {
            // Init the file
            var handlerDirectory = Path.Combine(this.codePath, "net\\handlers");
            if (!Directory.Exists(handlerDirectory))
                Directory.CreateDirectory(handlerDirectory);

	        var filepath = Path.Combine(handlerDirectory, method.Name + "Handler.java");
			var writer = new StreamWriter(filepath);

            // Write contents
            var webserviceHandlerWriter = new WebserviceHandlerWriter(writer, method, this.modelAssemlby);
            webserviceHandlerWriter.Write();

			// Close
			writer.Flush();
	        writer.Close();

	        if (webserviceHandlerWriter.AttributeParentNotUsed)
	        {
				string fullText = File.ReadAllText(filepath);
				File.WriteAllText(filepath, fullText.Replace("\tprivate String attributeParent = \"\";\r\n", ""));
	        }
        }

        //private void WriteWebserviceConstants(List<MethodInfo> methods)
        //{
        //    StreamWriter writer = new StreamWriter(Path.Combine(this.codePath, @"net\obymobi\WebserviceEvents.as"));

        //    // Write contents
        //    WebserviceEventsWriter webserviceEventsWriter = new WebserviceEventsWriter(writer, methods);
        //    webserviceEventsWriter.Write();

        //    // Close
        //    writer.Flush();
        //    writer.Close();
        //}

        //private void WriteLocalserviceConstants(List<MethodInfo> methods)
        //{
        //    StreamWriter writer = new StreamWriter(Path.Combine(this.codePath, @"net\obymobi\LocalserviceEvent.as"));

        //    // Write contents
        //    LocalserviceEventsWriter localserviceEventsWriter = new LocalserviceEventsWriter(writer, methods);
        //    localserviceEventsWriter.Write();

        //    // Close
        //    writer.Flush();
        //    writer.Close();
        //}

        /// <summary>
        /// Method to populate a list with all the class
        /// in the namespace provided by the user
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Type> GetAllModels()
        {
            var models = new List<Type>();
            foreach (var type in this.modelAssemlby.GetTypes())
            {
                if (type.Namespace == this.modelNamespace)
                    models.Add(type);
            }
            return models;

            #region Stolen from:
            /*
			//create an Assembly and use its GetExecutingAssembly Method
			//http://msdn2.microsoft.com/en-us/library/system.reflection.assembly.getexecutingassembly.aspx
			Assembly asm = Assembly.GetAssembly(typeof(Obymobi.Logic.Obyresult));
			//create a list for the namespaces
			List<string> namespaceList = new List<string>();
			//create a list that will hold all the classes
			//the suplied namespace is executing
			List<string> returnList = new List<string>();
			//loop through all the "Types" in the Assembly using
			//the GetType method:
			//http://msdn2.microsoft.com/en-us/library/system.reflection.assembly.gettypes.aspx
			foreach (Type type in asm.GetTypes())
			{
				if (type.Namespace == this.modelNamespace)
					namespaceList.Add(type.Name);
			}
			//now loop through all the classes returned above and add
			//them to our classesName list
			foreach (String className in namespaceList)
				returnList.Add(className);
			//return the list
			return returnList;
			 */
            #endregion
        }

        /// <summary>
        /// Method to populate a list with all the webservice methods
        /// </summary>
        /// <returns></returns>
        private IEnumerable<MethodInfo> GetAllWebserviceMethods()
        {
            var assembly = Assembly.Load("Obymobi.Logic");
			var type = assembly.GetType("IWebserviceHandler");
            var methodInfo = type.GetMethods();
            var methodInfoList = new List<MethodInfo>();
            foreach (var method in methodInfo)
            {
	            if (!this.excludedWebserviceMethod.Contains(method.Name))
	            {
		            methodInfoList.Add(method);
	            }
            }

            return methodInfoList;
        }

        #endregion
    }
}
