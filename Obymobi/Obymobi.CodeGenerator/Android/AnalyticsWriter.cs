﻿using Obymobi.Analytics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dionysos;
using System.IO;
using Obymobi.Enums;
using System.Reflection;

namespace Obymobi.CodeGenerator.Android
{
	public class AnalyticsWriter
	{
		private string basePath;
		public AnalyticsWriter(string basePath)
		{
			this.basePath = Path.Combine(basePath, "logic\\googleanalytics\\");
		}


		public void Write()
		{
			this.WriteHitBase();
			this.WriteDotNetEnum();
            this.WriteCreateTable();
		}
		private void WriteDotNetEnum()
		{
			string completePath = @"D:\Development\dotnet\Obymobi\Obymobi.Analytics.Google\CustomDimensionDefinition.cs";
			if(!File.Exists(completePath))
				throw new Exception("The code path is incorrect, as CustomDimensionDefinition.cs does not yet exist: " + completePath);

			StringBuilder sb = new StringBuilder();
			List<int> definedCustomDimensions = new List<int>();
			sb.AppendFormatLine("using System;");
			sb.AppendFormatLine("using System.Collections.Generic;");
			sb.AppendFormatLine("using System.Linq;");
			sb.AppendFormatLine("using System.Text;");
			sb.AppendFormatLine("using System.Threading.Tasks;");
			sb.AppendFormatLine("");
			sb.AppendFormatLine("/**");
			sb.AppendFormatLine("* WARNING: GENERATED CODE");
			sb.AppendFormatLine("* Changes to this class are futile - the full file will be replaced when code is generated");
			sb.AppendFormatLine("* To extend this class do so in the HitBase-class and re-generate the code");
			sb.AppendFormatLine("* RTFM: https://goo.gl/qKrdQX ");
			sb.AppendFormatLine("*/");
			sb.AppendFormatLine("");
			sb.AppendFormatLine("namespace Obymobi.Analytics.Google");
			sb.AppendFormatLine("{");
			sb.AppendFormatLine("    public enum CustomDimensionDefinition : int");
			sb.AppendFormatLine("    {");
			sb.AppendFormatLine("        Unknown = 0,");
			Type hitBaseType = typeof(HitBase);
			foreach (var field in hitBaseType.GetFields())
			{
				string googleParameter = string.Empty;
				var attributes = field.GetCustomAttributes(typeof(HitParameterAttribute), false);
				if (attributes != null && attributes.Length > 0)
				{
					googleParameter = ((HitParameterAttribute)attributes[0]).GoogleParameterName;
				}

				if (googleParameter.Length <= 2 || !googleParameter.StartsWith("cd"))
					continue;

				int customDimensionNo = Convert.ToInt32(googleParameter.Substring(2));

				sb.AppendFormatLine("        {0} = {1},", field.Name, customDimensionNo);

				if (definedCustomDimensions.Contains(customDimensionNo))
					throw new Exception("The Custom Dimension No was used more than once: " + customDimensionNo);

				definedCustomDimensions.Add(customDimensionNo);
			}

			if(definedCustomDimensions.Max() != definedCustomDimensions.Count)
				throw new Exception("The Custom Dimension are not defined sequential without any gaps");

			sb.AppendFormatLine("        /*");
			sb.AppendFormatLine("        * WARNING: GENERATED CODE");
			sb.AppendFormatLine("        * Changes to this class are futile - the full file will be replaced when code is generated");
			sb.AppendFormatLine("        * To extend this class do so in the HitBase-class and re-generate the code");
			sb.AppendFormatLine("        * RTFM: https://goo.gl/qKrdQX ");
			sb.AppendFormatLine("        */");
			sb.AppendFormatLine("    }");
			sb.AppendFormatLine("}");

			File.WriteAllText(completePath, sb.ToString());
		}

        private void WriteCreateTable()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormatLine("CREATE TABLE [dbo].[Hit](");
            sb.AppendFormatLine("[HitId] [int] IDENTITY(1,1) NOT NULL,");

            Type hitBaseType = typeof(HitBase);

            var fields = hitBaseType.GetFields();
            for(int iField = 0; iField < fields.Length; iField++)
            {
                FieldInfo field = fields[iField];
                string name = field.Name;
                string type;

                int length = 2048; // Chosen to not use MAX as that won't be included in ColumnStore Index
                var attributes = field.GetCustomAttributes(typeof(HitParameterAttribute), false);
                if (attributes != null && attributes.Length > 0)
                {
                    if (((HitParameterAttribute)attributes[0]).Length > 0)
                        length = ((HitParameterAttribute)attributes[0]).Length;
                }

                if (field.FieldType == typeof(string))
                {                    
                    type = string.Format("[nvarchar]({0})", length);
                }
                else if (field.FieldType == typeof(int))
                    type = "[int]";
                else if (field.FieldType == typeof(bool))
                    type = "[bit]";
                else if (field.FieldType == typeof(Double))
                    type = "[money]";
                else if (field.FieldType.IsEnum)
                {
                    type = "[nvarchar](64)";
                }
                else
                    throw new NotImplementedException("No implementation for Type: " + field.FieldType.FullName);

                // No longer needed: We always have 'Created' as last fiel
                //if(iField + 1 < fields.Length)
                //    sb.AppendFormatLine("[{0}] {1} NULL, ", name, type);
                //else
                //    sb.AppendFormatLine("[{0}] {1} NULL ", name, type);
                sb.AppendFormatLine("[{0}] {1} NULL, ", name, type);
            }

            sb.AppendFormatLine("[Created] [datetime] NULL");
            sb.AppendFormatLine("CONSTRAINT [PK_Hit] PRIMARY KEY CLUSTERED ");
            sb.AppendFormatLine("( ");
            sb.AppendFormatLine("[HitId] ASC ");
            sb.AppendFormatLine(")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ");
            sb.AppendFormatLine(") ON [PRIMARY] ");
            sb.AppendFormatLine("");
            sb.AppendFormatLine("ALTER TABLE [dbo].[Hit] ADD CONSTRAINT [DF_Hit_Created]  DEFAULT (getutcdate()) FOR [Created]");
            sb.AppendFormatLine("");

            File.WriteAllText("Create-Hit-Table.sql", sb.ToString());
        }

		private void WriteHitBase()
		{
			string completePath = Path.Combine(this.basePath, "HitBase.java");
			if (!File.Exists(completePath))
				throw new Exception("The code path is incorrect, as HitBase.java does not yet exist: " + completePath);

			Type hitBaseType = typeof(HitBase);

			StringBuilder sb = new StringBuilder();
			sb.AppendFormatLine("package net.craveinteractive.cravebase.logic.googleanalytics;");
			sb.AppendFormatLine("/**");
			sb.AppendFormatLine("* WARNING: GENERATED CODE");
			sb.AppendFormatLine("* Changes to this class are futile - the full file will be replaced when code is generated");
			sb.AppendFormatLine("* To extend this class do so in the .NET Project and re-generate the code");
			sb.AppendFormatLine("*/");
            sb.AppendFormatLine("");
            sb.AppendFormatLine("import com.google.gson.annotations.SerializedName;");
            sb.AppendFormatLine("");
            sb.AppendFormatLine("");
            sb.AppendFormatLine("public class HitBase {");

			foreach (var field in hitBaseType.GetFields())
			{
				string name = field.Name;
				string type;

				if (field.FieldType == typeof(string))
					type = "String";
				else if (field.FieldType == typeof(int))
					type = "Integer";
				else if (field.FieldType == typeof(bool))
					type = "boolean";
				else if (field.FieldType == typeof(Double))
					type = "double";
				else if (field.FieldType.IsEnum)
				{
					type = field.FieldType.Name; // Intended for enums
					this.WriteEnum(field.FieldType);
				}
				else
					throw new NotImplementedException("No implementation for Type: " + field.FieldType.FullName);

				string googleParameter = "";
				var attributes = field.GetCustomAttributes(typeof(HitParameterAttribute), false);
				if (attributes != null && attributes.Length > 0)
				{
					googleParameter = ((HitParameterAttribute)attributes[0]).GoogleParameterName;
				}
				else
					throw new InvalidTypeException("You've not set a Google Parameter for: " + name);

				sb.AppendFormatLine("    @HitAttributeAnnotation(googleParameterName = \"{0}\")", googleParameter);
                // Hint for Json conversion by Gson (https://github.com/google/gson/blob/master/UserGuide.md#json-field-naming-support)
                sb.AppendFormatLine("    @SerializedName(\"{0}\")", googleParameter); 
                sb.AppendFormatLine("    public {0} {1};", type, name);
			}

			sb.AppendFormatLine("    /*");
			sb.AppendFormatLine("    * WARNING: GENERATED CODE");
			sb.AppendFormatLine("    * Changes to this class are futile - the full file will be replaced when code is generated");
			sb.AppendFormatLine("    * To extend this class do so in the .NET Project and re-generate the code");
			sb.AppendFormatLine("    * RTFM: https://goo.gl/qKrdQX ");
			sb.AppendFormatLine("    */");

			sb.AppendFormatLine("}");

			File.WriteAllText(Path.Combine(this.basePath, "HitBase.java"), sb.ToString());
		}

		private void WriteEnum(Type enumType)
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendFormatLine("package net.craveinteractive.cravebase.logic.googleanalytics;");
			sb.AppendFormatLine("/**");
			sb.AppendFormatLine("* WARNING: GENERATED CODE");
			sb.AppendFormatLine("* Changes to this class are futile - the full file will be replaced when code is generated");
			sb.AppendFormatLine("* To extend this class do so in the .NET Project and re-generate the code");
			sb.AppendFormatLine("*/");
			sb.AppendFormatLine("public enum {0} {{", enumType.Name);

			var fie1dz = enumType.GetFields(); // You can hate me later :P
			for (int i = 0; i < fie1dz.Length; i++)
			{
				var member = fie1dz[i];

				if (!member.FieldType.Equals(enumType))
					continue;

				string stringValue;
				var attributes = member.GetCustomAttributes(typeof(AnalyticsEnumStringValueAttribute), false);
                if (attributes != null && attributes.Length > 0)
                {
                    stringValue = ((AnalyticsEnumStringValueAttribute)attributes[0]).StringValue;
                }
                // For non Analytics specific Enums you can use an exception
                else if (enumType.Equals(typeof(OrderType)))
                {
                    stringValue = member.ToString();
                }
                else
                {
                    throw new InvalidTypeException("StringValue is required for enum values used in the HitBase class");
                }

				string lineEnding = (i < fie1dz.Length - 1) ? "," : ";";
				sb.AppendFormatLine("    {0}(\"{1}\"){2}", member.Name.ToUpper(), stringValue, lineEnding);
			}

			sb.AppendFormatLine("");
			sb.AppendFormatLine("    /*");
			sb.AppendFormatLine("    * WARNING: GENERATED CODE");
			sb.AppendFormatLine("    * Changes to this class are futile - the full file will be replaced when code is generated");
			sb.AppendFormatLine("    * To extend this class do so in the .NET Project and re-generate the code");
			sb.AppendFormatLine("    * RTFM: https://goo.gl/qKrdQX ");
			sb.AppendFormatLine("    */");
			sb.AppendFormatLine("");
			sb.AppendFormatLine("    private final String mText;");
			sb.AppendFormatLine("");
			sb.AppendFormatLine("    {0}(final String text)", enumType.Name);
			sb.AppendFormatLine("    {");
			sb.AppendFormatLine("        this.mText = text;");
			sb.AppendFormatLine("    }");
			sb.AppendFormatLine("");
			sb.AppendFormatLine("    @Override");
			sb.AppendFormatLine("    public String toString() {");
			sb.AppendFormatLine("        return this.mText;");
			sb.AppendFormatLine("    }");
			sb.AppendFormatLine("");
			sb.AppendFormatLine("}");

			File.WriteAllText(Path.Combine(this.basePath, enumType.Name + ".java"), sb.ToString());

		}
	}
}

