﻿using Obymobi.Integrations.POS;
using System;
using System.IO;
using System.Windows.Forms;

namespace Obymobi.CodeGenerator
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

            // Set the configuration provider
            Dionysos.Configuration.XmlConfigurationProvider provider = new Dionysos.Configuration.XmlConfigurationProvider();
            provider.ManualConfigFilePath = Path.Combine(Directory.GetParent(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location)).FullName, "Obymobi.config");

            Dionysos.Global.ConfigurationProvider = provider;

            // Set the application information
            Dionysos.Global.ApplicationInfo = new Dionysos.Windows.Forms.ApplicationInfo();
            Dionysos.Global.ApplicationInfo.ApplicationName = "Obymobi.CodeGenerator";
            Dionysos.Global.ApplicationInfo.BasePath = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            // Set the configuration definitions
            Dionysos.Global.ConfigurationInfo.Add(new POSConfigurationInfo());

			Application.Run(new MainForm());
		}
	}
}
