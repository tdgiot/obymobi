﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Dionysos;

namespace Obymobi.CodeGenerator.J2ME
{
	public class ModelToCollectionWriter
	{
		private Object model;
		private Type modelType;
		private StreamWriter stream;

		public ModelToCollectionWriter(Object model, StreamWriter stream)
		{
			this.model = model;
			this.modelType = model.GetType();
			this.stream = stream;
		}

		public void WriteCollection()
		{ 
			this.WriteIntroduction();
			this.WriteClassStart();
			this.WriteIEntityCollectionImplementation();
			this.WriteClassEnd();
		}

		private void WriteIntroduction()
		{
			WriteToStream("/*");
			WriteToStream(" * Generated collection class for {0}", this.modelType.FullName);
			WriteToStream(" */");
			WriteToStream("");
			WriteToStream("package obymobi.data.collectionbaseclasses;");
			WriteToStream("");
			WriteToStream("import java.util.Vector;");
			WriteToStream("import obymobi.data.entityclasses.{0}Entity;", this.modelType.Name);			
			WriteToStream("import obymobi.data.IEntity;");
			WriteToStream("import obymobi.data.IEntityCollection;");
			WriteToStream("import obymobi.data.EntityCollectionBase;");
			WriteToStream("import obymobi.networking.XmlHelper;");
			WriteToStream("");
		}

		private void WriteClassStart()
		{
			WriteToStream("public class {0}CollectionBase extends EntityCollectionBase", this.modelType.Name);
			WriteToStream("{{");
			WriteToStream("");
		}

		private void WriteIEntityCollectionImplementation()
		{
			WriteToStream("	// Entity Base logic");
			WriteToStream("	// <editor-fold>");
			WriteToStream("");

			WriteToStream("	public {0}Entity elementAtTyped(int index)", this.modelType.Name);
			WriteToStream("	{{");
			WriteToStream("		return ({0}Entity)super.elementAt(index);", this.modelType.Name);
			WriteToStream("	}}");
			WriteToStream("");

			// Get Entity Instance
			WriteToStream("	public IEntity getNewEmptyEntityInstance()");
			WriteToStream("	{{");
			WriteToStream("		return new {0}Entity();", this.modelType.Name);
			WriteToStream("	}}");
			WriteToStream("");

			// Constant for EntityName
			WriteToStream("	public String getModelName()");
			WriteToStream("	{{");
			WriteToStream("		return \"{0}\";", this.modelType.Name);
			WriteToStream("	}}");
			WriteToStream("");
			WriteToStream("");

			// Collection to XML
			WriteToStream("	public String toXml()");			
			WriteToStream("	{{");			
			WriteToStream("		String xml = \"\";");			
			WriteToStream("		xml += XmlHelper.createXmlOpeningTag(\"ArrayOf\" + this.getModelName());");			
			WriteToStream("		for (int i = 0; i < this.size(); i++)");			
			WriteToStream("		{{");			
			WriteToStream("			{0}Entity {1}Entity = this.elementAtTyped(i);", this.modelType.Name.TurnFirstToUpper(false), this.modelType.Name.TurnFirstToLower(false));
			WriteToStream("			xml += {0}Entity.writeXml();", this.modelType.Name.TurnFirstToLower(false));			
			WriteToStream("		}}");			
			WriteToStream("		xml += XmlHelper.createXmlClosingTag(\"ArrayOf\" + this.getModelName());");			
			WriteToStream("		return xml;");			
			WriteToStream("	}}");
			
		}

		private void WriteClassEnd()
		{
			WriteToStream("");
			WriteToStream("	}}");
		}

		private void WriteToStream(string message, params object[] args)
		{
			this.stream.WriteLine(string.Format(message, args));
		}
	}
}
