﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using Dionysos;

namespace Obymobi.CodeGenerator.J2ME
{
	public class PolishLocalizationHelper
	{
		private List<String> keysFromSourceCode = new List<string>();

		public PolishLocalizationHelper(string sourceCodePath, string localizationFilesPath)
		{
			// First get the key from the source
			this.RetrieveTranslationKeyFromSourceFiles(sourceCodePath);

			// Now update each language resource file
			this.ProcessLocalizationFile(localizationFilesPath);
			
		}

		private class LocalizationElement: IComparable
		{
			public LocalizationElement(string key, string value)
			{
				this.Key = key;
				this.Value = value;
				this.Found = false;
			}

			public string Key { get; set; }
			public string Value { get; set; }
			public bool Found { get; set; }

			#region IComparable Members

			public int CompareTo(object obj)
			{
				return this.Key.CompareTo(((LocalizationElement)obj).Key);
			}

			#endregion
		}

		private void ProcessLocalizationFile(string localizationFilesPath)
		{
			Regex resourceFileParser = new Regex("(?<Key>.+)=(?<Value>.+)");			
			List<string> localizationFiles = new List<string>();
			loadFiles(localizationFilesPath, "messages*.txt", ref localizationFiles);

			foreach (var localizationFile in localizationFiles)
			{
				List<LocalizationElement> localizationElements = new List<LocalizationElement>();
				string fileContents = File.ReadAllText(localizationFile);
				MatchCollection matches = resourceFileParser.Matches(fileContents);
				foreach (Match match in matches)
				{
					if (match.Groups["Key"] != null &&
						match.Groups["Value"] != null)
					{ 
						// Remove pound (#) signs
						string keyName = match.Groups["Key"].Value.Replace("#", string.Empty);
						localizationElements.Add(new LocalizationElement(keyName, match.Groups["Value"].Value));
					}
				}

				// We now have the current contents
				foreach (string key in this.keysFromSourceCode)
				{
					bool found = false;
					foreach (var localiElement in localizationElements)
					{
						if (localiElement.Key == key)
						{
							localiElement.Found = true;
							found = true;
							break;
						}
					}

					if (!found)
					{
						var item = new LocalizationElement(key, "TODO");
						item.Found = true;
						localizationElements.Add(item);
					}
				}

				localizationElements.Sort();

				// Write the new file
				string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(localizationFile);
				string fileBackup = localizationFile.Replace(fileNameWithoutExtension, fileNameWithoutExtension + DateTimeUtil.DateTimeToSimpleDateTimeStamp(DateTime.Now));
				//File.Copy(localizationFile, fileBackup);

				File.Delete(localizationFile);
				StreamWriter writer = new StreamWriter(localizationFile);				

				// Write contents
				string last = string.Empty;

				foreach (var localiElement in localizationElements)
				{
					string current = localiElement.Key.GetAllBeforeFirstOccurenceOf(".", true);
					if (current != last && current.Length > 0)
					{
						writer.WriteLine(" ");
						writer.WriteLine(string.Format("#{0}", current));
						last = current;
					}

					string value = localiElement.Value.Replace("\r", string.Empty);
					value = value.Replace("\n", string.Empty);					

					if (localiElement.Found)
					{
						writer.WriteLine("{0}={1}", localiElement.Key, value);
					}
					else
					{
						writer.WriteLine("#{0}={1}", localiElement.Key, value);
					}
				}	

				// Close
				writer.Flush();
				writer.Close();			

			}
		}

		private void RetrieveTranslationKeyFromSourceFiles(string sourceCodePath)		
		{
			// Retrieve srouce
			List<string> sourceFiles = new List<string>();
			loadFiles(sourceCodePath, "*.java", ref sourceFiles);

			foreach (var sourceFilePath in sourceFiles)
			{
				string fileContents = File.ReadAllText(sourceFilePath);
				Regex sourceFileParser = new Regex("Locale.get\\(\"(?<Key>.+?)\"");
				MatchCollection matches = sourceFileParser.Matches(fileContents);
				foreach (Match match in matches)
				{
					if (match.Groups["Key"] != null)
					{
						string matchText = match.Groups["Key"].Value;
						if (matchText.Contains("\""))
						{
							matchText = matchText.GetAllBeforeFirstOccurenceOf('\"', true);
						}

						if (matchText.Length > 0)
							this.keysFromSourceCode.Add(matchText);
					}
				}
			}

			this.keysFromSourceCode.Sort();
		}

		public static void loadFiles(string directoryPath, string fileFilter, ref List<string> fileList)
		{
			String[] files;
			if(fileFilter.Length>0)
				files = Directory.GetFiles(directoryPath, fileFilter);
			else
				files = Directory.GetFiles(directoryPath);

			String[] subdirectories = Directory.GetDirectories(directoryPath);
			Array.Sort(files);
			Array.Sort(subdirectories);

			// Get the folders (recursion)
			foreach (var subdirPath in subdirectories)
			{
				loadFiles(subdirPath, fileFilter, ref fileList);
			}

			// Get the files
			foreach (var path in files)
			{
				if (path.ToLower().Contains(".svn"))
					break;

				string pathToLower = path.ToLower();
				if (fileFilter.Length > 0 && pathToLower.Contains(fileFilter.ToLower()))
				{
					fileList.Add(path);
				}
				else
				{
					fileList.Add(path);
				}
			}
		}
	}
}
