﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.J2ME
{
	public class ModelToEntityWriter
	{
		private Object model;
		private Type modelType;
		private StreamWriter stream;
		private PropertyInfo[] fields;

		public ModelToEntityWriter(Object model, StreamWriter stream)
		{
			this.model = model;
			this.modelType = model.GetType();
			this.stream = stream;			
			
			PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
			List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
			for (int j = 0; j < propertyInfo.Length; j++)
			{
				PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForMobile
				if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
				{
					propertyInfoList.Add(property);
				}
			}

			this.fields = propertyInfoList.ToArray();
		}

		public void WriteEntity()
		{
			this.WriteIntroduction();
			this.WriteClassStart();
			this.WriteFields();
			this.WriteProperties();
			this.WriteFromAndToDataStreamLogic();
			this.WriteXmlLogic();
			this.WriteGenericPropertyAccessor();
			this.WriteAdditionalLogic();
			this.WriteClassEnd();
		}

		private void WriteIntroduction()
		{
			WriteToStream("/*");
			WriteToStream(" * Generated entity class for {0}", this.modelType.FullName);
			WriteToStream(" */");
			WriteToStream("");
			WriteToStream("package obymobi.data.entitybaseclasses;");
			WriteToStream("");
			WriteToStream("import java.io.DataInputStream;");
			WriteToStream("import java.io.DataOutputStream;");
			WriteToStream("import obymobi.data.*;");
			WriteToStream("import obymobi.data.collectionclasses.{0}Collection;", this.modelType.Name);
			WriteToStream("import obymobi.data.entityclasses.{0}Entity;", this.modelType.Name);
			WriteToStream("import obymobi.networking.XmlHelper;");
			WriteToStream("");
			WriteToStream("");
		}

		private void WriteClassStart()
		{
			WriteToStream("public abstract class {0}EntityBase extends EntityBase", this.modelType.Name);
			WriteToStream("{{");
			WriteToStream("");
		}

		private void WriteFields()
		{
			WriteToStream("	// Fields");
			WriteToStream("	// <editor-fold>");
			WriteToStream("");

			foreach (var field in fields)
			{
				if (field.PropertyType.ToString() == typeof(Int32).ToString())
				{
					WriteToStream("	private int {0} = -1;", field.Name.TurnFirstToLower(false));
				}
				else if (field.PropertyType.ToString() == typeof(String).ToString())
				{
					WriteToStream("	private String {0} = \"\";", field.Name.TurnFirstToLower(false));
				}
			}
			WriteToStream("");
			WriteToStream("	// </editor-fold>");
		}

		private void WriteProperties()
		{
			WriteToStream("	// Properties");
			WriteToStream("	// <editor-fold>");
			WriteToStream("");			
			foreach (var field in fields)
			{
				string typeName = "";
				if (field.PropertyType.ToString() == typeof(Int32).ToString())
				{
					typeName = "int";
				}
				else if (field.PropertyType.ToString() == typeof(String).ToString())
				{
					typeName = "String";
				}

				if (typeName.Length > 0)
				{
					// The Setter
					WriteToStream("	public void set{0}({1} value)", field.Name, typeName);
					WriteToStream("	{{");
					WriteToStream("		this.{0} = value;", field.Name.TurnFirstToLower(false));
					WriteToStream("	}}");

					WriteToStream("");

					// The Getter
					WriteToStream("	public {1} get{0}()", field.Name, typeName);
					WriteToStream("	{{");
					WriteToStream("		return this.{0};", field.Name.TurnFirstToLower(false));
					WriteToStream("	}}");
				}
				WriteToStream("");
			}
			WriteToStream("");

			// Constant for EntityName
			WriteToStream("	public String getModelName()");
			WriteToStream("	{{");
			WriteToStream("		return \"{0}\";", this.modelType.Name);
			WriteToStream("	}}");
			WriteToStream("");
			WriteToStream("");

			WriteToStream("	// </editor-fold>");
		}

		private void WriteFromAndToDataStreamLogic()
		{
			WriteToStream("	// From and To DataStream conversion");
			WriteToStream("	// <editor-fold>");
			WriteToStream("");

			// From DataStream
			WriteToStream("	public void fromDataStream(DataInputStream inputStream)");
			WriteToStream("	{{");
			WriteToStream("		try");
			WriteToStream("		{{");

			// Per field			
			foreach (var field in fields)
			{
				string conversionMethodName = "";
				if (field.PropertyType.ToString() == typeof(Int32).ToString())
				{
					conversionMethodName = "readInt";
				}
				else if (field.PropertyType.ToString() == typeof(String).ToString())
				{
					conversionMethodName = "readUTF";
				}

				if (conversionMethodName.Length > 0)
					WriteToStream("			this.set{0}(inputStream.{1}());", field.Name, conversionMethodName);
			}


			WriteToStream("		}}");
			WriteToStream("		catch (Exception ex)");
			WriteToStream("		{{");
			WriteToStream("			throw new obymobi.logic.helperclasses.GenericRuntimeException(ex);");
			WriteToStream("		}}");
			WriteToStream("	}}");

			// To DataStream
			WriteToStream("	public void toDataStream(DataOutputStream outputStream)");
			WriteToStream("	{{");
			WriteToStream("		try");
			WriteToStream("		{{");
			WriteToStream("");

			// Per field			
			foreach (var field in fields)
			{
				string conversionMethodName = "";
				if (field.PropertyType.ToString() == typeof(Int32).ToString())
				{
					conversionMethodName = "writeInt";
				}
				else if (field.PropertyType.ToString() == typeof(String).ToString())
				{
					conversionMethodName = "writeUTF";
				}

				if (conversionMethodName.Length > 0)
					WriteToStream("			outputStream.{0}(this.get{1}());", conversionMethodName, field.Name);
			}


			WriteToStream("		}}");
			WriteToStream("		catch (Exception ex)");
			WriteToStream("		{{");
			WriteToStream("			throw new obymobi.logic.helperclasses.GenericRuntimeException(ex);");
			WriteToStream("		}}");
			WriteToStream("	}}");
			WriteToStream("");

			// To DataStream

			WriteToStream("");
			WriteToStream("	// </editor-fold>");
		}

		private void WriteXmlLogic()
		{
			WriteToStream("	// Xml logic");
			WriteToStream("	// <editor-fold>");
			WriteToStream("");

			#region ToXml
			WriteToStream("	public String writeXml()");
			WriteToStream(" {{");
			WriteToStream("		String xml = \"\";");
			WriteToStream("");
			WriteToStream("		xml += XmlHelper.createXmlOpeningTag(\"{0}\");", this.modelType.Name);
			WriteToStream("");

			// Write fields
			foreach (var field in fields)
			{
				if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
					WriteToStream("		xml += XmlHelper.createXmlElement(\"{0}\", get{0}());", field.Name);
			}

			WriteToStream("");
			WriteToStream("		xml += XmlHelper.createXmlClosingTag(\"{0}\");", this.modelType.Name);
			WriteToStream("");
			WriteToStream("		return xml;");
			#endregion

			WriteToStream("	}}");
			WriteToStream("	// </editor-fold>");
			WriteToStream("");
		}

		public void WriteGenericPropertyAccessor()
		{
			WriteToStream("	// Generic Property Accessor");
			WriteToStream("	// <editor-fold>");
			WriteToStream("");

			#region SetPropery for values of type Object
			WriteToStream("	public void setProperty(String propertyName, Object value)");
			WriteToStream("	{{");
			WriteToStream("");

			bool firstIfClause = true;
			foreach (var field in fields)
			{
				if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
				{
					if (firstIfClause)
					{
						WriteToStream("		if(propertyName.compareTo(\"{0}\") == 0)", field.Name);
						firstIfClause = false;
					}
					else
						WriteToStream("		else if(propertyName.compareTo(\"{0}\") == 0)", field.Name);

					if (field.PropertyType.ToString() == typeof(Int32).ToString())
					{
						WriteToStream("			this.{0} = Integer.parseInt(value.toString());", field.Name.TurnFirstToLower(false));
					}
					else if (field.PropertyType.ToString() == typeof(String).ToString())
					{
						WriteToStream("			this.{0} = value.toString();", field.Name.TurnFirstToLower(false));
					}
					else
					{
						// Nohting.
					}
				}
			}

			// End switch & function
			WriteToStream("	}}");
			WriteToStream("");
			#endregion

			#region SetPropery for values of type int
			WriteToStream("	public void setProperty(String propertyName, int value)");
			WriteToStream("	{{");
			WriteToStream("");
			firstIfClause = true;
			foreach (var field in fields)
			{
				if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
				{
					if (firstIfClause)
					{
						WriteToStream("		if(propertyName.compareTo(\"{0}\") == 0)", field.Name);
						firstIfClause = false;
					}
					else
						WriteToStream("		else if(propertyName.compareTo(\"{0}\") == 0)", field.Name);

					if (field.PropertyType.ToString() == typeof(Int32).ToString())
					{
						WriteToStream("			this.{0} = value;", field.Name.TurnFirstToLower(false));
					}
					else if (field.PropertyType.ToString() == typeof(String).ToString())
					{
						WriteToStream("			this.{0} = Integer.toString(value);", field.Name.TurnFirstToLower(false));
					}
				}
			}

			// End switch & function
			WriteToStream("	}}");
			#endregion

			WriteToStream("");
			WriteToStream("	// </editor-fold>");
		}

		private void WriteAdditionalLogic()
		{
			WriteToStream("	// Entity Base logic");
			WriteToStream("	// <editor-fold>");
			WriteToStream("");

			// Get Entity Instance
			WriteToStream("	public IEntity getNewEmptyInstance()");
			WriteToStream("	{{");
			WriteToStream("		return new {0}Entity();", this.modelType.Name);
			WriteToStream("	}}");
			WriteToStream("");

			// Get CollectionInstance
			WriteToStream("	public EntityCollectionBase getNewEmptyEntityCollection()");
			WriteToStream("	{{");
			WriteToStream("		return new {0}Collection();", this.modelType.Name);
			WriteToStream("	}}");
			WriteToStream("");

			// Unique EntityName
			WriteToStream("	public String uniqueEntityName()");
			WriteToStream("	{{");
			WriteToStream("		return \"{0}Entity\";", this.modelType.Name);
			WriteToStream("	}}");
			WriteToStream("");

			// Abstract methods for PK fields
			WriteToStream("	public abstract int getPkFieldValue();");
			WriteToStream("	public abstract void setPkFieldValue(int pkValue);");

			WriteToStream("");
			WriteToStream("	// </editor-fold>");

			// Clone object
		}

		private void WriteClassEnd()
		{
			WriteToStream("}}");
		}

		private void WriteToStream(string message, params object[] args)
		{
			this.stream.WriteLine(string.Format(message, args));
		}
	}
}
