﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.J2ME
{
	public class Generator
	{
		#region Fields

		private string modelNamespace = string.Empty;
		private Assembly modelAssemlby = null;
		private string codePath = string.Empty;

		#endregion

		#region Methods

		public Generator(string codePath, Type typeOfOneModelClass)
		{
			this.codePath = codePath;
			this.modelNamespace = typeOfOneModelClass.Namespace;
			this.modelAssemlby = Assembly.GetAssembly(typeOfOneModelClass);
		}

		public void Generate()
		{
			// Get all classes to write
			List<Type> models = this.GetAllModels();
			
			foreach (var modelClassType in models)
			{
				if (modelClassType.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
				{
					this.WriteEntityBaseFile(modelClassType);
					this.WriteEntityCollectionFile(modelClassType);
				}
			}
		}

		private void WriteEntityBaseFile(Type modelType)
		{
			Object modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

			// Init the file
			string entityBaseClassDirectory = Path.Combine(this.codePath, "entitybaseclasses");
			if (!Directory.Exists(entityBaseClassDirectory))
				Directory.CreateDirectory(entityBaseClassDirectory);
			StreamWriter writer = new StreamWriter(Path.Combine(entityBaseClassDirectory, modelType.Name + "EntityBase.java"));

			// Write contents
			ModelToEntityWriter entityWriter = new ModelToEntityWriter(modelInstance, writer);
			entityWriter.WriteEntity();

			// Close
			writer.Flush();
			writer.Close();			
		}		

		private void WriteEntityCollectionFile(Type modelType)
		{
			Object modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

			// Init the file
			string collectionBaseClassDirectory = Path.Combine(this.codePath, "collectionbaseclasses");
			if (!Directory.Exists(collectionBaseClassDirectory))
				Directory.CreateDirectory(collectionBaseClassDirectory);

			StreamWriter writer = new StreamWriter(Path.Combine(collectionBaseClassDirectory, modelType.Name + "CollectionBase.java"));

			// Write contents
			ModelToCollectionWriter entityWriter = new ModelToCollectionWriter(modelInstance, writer);
			entityWriter.WriteCollection();

			// Close
			writer.Flush();
			writer.Close();			
		}

		/// <summary>
		/// Method to populate a list with all the class
		/// in the namespace provided by the user
		/// </summary>
		/// <param name="nameSpace">The namespace the user wants searched</param>
		/// <returns></returns>
		private List<Type> GetAllModels()
		{			
			List<Type> models = new List<Type>();
			foreach (Type type in this.modelAssemlby.GetTypes())
			{
				if (type.Namespace == this.modelNamespace)
					models.Add(type);
			}
			return models;

			#region Stolen from:
			/*
			//create an Assembly and use its GetExecutingAssembly Method
			//http://msdn2.microsoft.com/en-us/library/system.reflection.assembly.getexecutingassembly.aspx
			Assembly asm = Assembly.GetAssembly(typeof(Obymobi.Logic.Obyresult));
			//create a list for the namespaces
			List<string> namespaceList = new List<string>();
			//create a list that will hold all the classes
			//the suplied namespace is executing
			List<string> returnList = new List<string>();
			//loop through all the "Types" in the Assembly using
			//the GetType method:
			//http://msdn2.microsoft.com/en-us/library/system.reflection.assembly.gettypes.aspx
			foreach (Type type in asm.GetTypes())
			{
				if (type.Namespace == this.modelNamespace)
					namespaceList.Add(type.Name);
			}
			//now loop through all the classes returned above and add
			//them to our classesName list
			foreach (String className in namespaceList)
				returnList.Add(className);
			//return the list
			return returnList;
			 */
			#endregion
		}

		#endregion

		#region Event Handlers
		#endregion

		#region Properties	

		#endregion
		
	}
}
