﻿namespace Obymobi.CodeGenerator
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.btGenerate = new System.Windows.Forms.Button();
            this.btLocalization = new System.Windows.Forms.Button();
            this.btCommentTracing = new System.Windows.Forms.Button();
            this.btUncommentTracing = new System.Windows.Forms.Button();
            this.btGenerateiPhoneCode = new System.Windows.Forms.Button();
            this.btnGenerateFlexCode = new System.Windows.Forms.Button();
            this.btGenerateAndroid = new System.Windows.Forms.Button();
            this.btGenerateXamarin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btGenerate
            // 
            this.btGenerate.BackColor = System.Drawing.Color.Red;
            this.btGenerate.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btGenerate.ForeColor = System.Drawing.Color.White;
            this.btGenerate.Location = new System.Drawing.Point(0, 0);
            this.btGenerate.Name = "btGenerate";
            this.btGenerate.Size = new System.Drawing.Size(301, 178);
            this.btGenerate.TabIndex = 0;
            this.btGenerate.Text = "Generate J2ME code!";
            this.btGenerate.UseVisualStyleBackColor = false;
            this.btGenerate.Click += new System.EventHandler(this.btGenerate_Click);
            // 
            // btLocalization
            // 
            this.btLocalization.BackColor = System.Drawing.Color.Red;
            this.btLocalization.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLocalization.ForeColor = System.Drawing.Color.White;
            this.btLocalization.Location = new System.Drawing.Point(0, 184);
            this.btLocalization.Name = "btLocalization";
            this.btLocalization.Size = new System.Drawing.Size(571, 185);
            this.btLocalization.TabIndex = 1;
            this.btLocalization.Text = "Update localization";
            this.btLocalization.UseVisualStyleBackColor = false;
            // 
            // btCommentTracing
            // 
            this.btCommentTracing.BackColor = System.Drawing.Color.Red;
            this.btCommentTracing.Font = new System.Drawing.Font("Microsoft Sans Serif", 75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCommentTracing.ForeColor = System.Drawing.Color.White;
            this.btCommentTracing.Location = new System.Drawing.Point(0, 369);
            this.btCommentTracing.Name = "btCommentTracing";
            this.btCommentTracing.Size = new System.Drawing.Size(571, 254);
            this.btCommentTracing.TabIndex = 2;
            this.btCommentTracing.Text = "Comment Tracing";
            this.btCommentTracing.UseVisualStyleBackColor = false;
            // 
            // btUncommentTracing
            // 
            this.btUncommentTracing.BackColor = System.Drawing.Color.Red;
            this.btUncommentTracing.Font = new System.Drawing.Font("Microsoft Sans Serif", 75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btUncommentTracing.ForeColor = System.Drawing.Color.White;
            this.btUncommentTracing.Location = new System.Drawing.Point(577, 369);
            this.btUncommentTracing.Name = "btUncommentTracing";
            this.btUncommentTracing.Size = new System.Drawing.Size(600, 254);
            this.btUncommentTracing.TabIndex = 4;
            this.btUncommentTracing.Text = "Uncomment Tracing";
            this.btUncommentTracing.UseVisualStyleBackColor = false;
            // 
            // btGenerateiPhoneCode
            // 
            this.btGenerateiPhoneCode.BackColor = System.Drawing.Color.Red;
            this.btGenerateiPhoneCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btGenerateiPhoneCode.ForeColor = System.Drawing.Color.White;
            this.btGenerateiPhoneCode.Location = new System.Drawing.Point(307, 0);
            this.btGenerateiPhoneCode.Name = "btGenerateiPhoneCode";
            this.btGenerateiPhoneCode.Size = new System.Drawing.Size(234, 178);
            this.btGenerateiPhoneCode.TabIndex = 5;
            this.btGenerateiPhoneCode.Text = "Generate iPhone code!";
            this.btGenerateiPhoneCode.UseVisualStyleBackColor = false;
            this.btGenerateiPhoneCode.Click += new System.EventHandler(this.btGenerateiPhoneCode_Click);
            // 
            // btnGenerateFlexCode
            // 
            this.btnGenerateFlexCode.BackColor = System.Drawing.Color.Red;
            this.btnGenerateFlexCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenerateFlexCode.ForeColor = System.Drawing.Color.White;
            this.btnGenerateFlexCode.Location = new System.Drawing.Point(547, 0);
            this.btnGenerateFlexCode.Name = "btnGenerateFlexCode";
            this.btnGenerateFlexCode.Size = new System.Drawing.Size(339, 178);
            this.btnGenerateFlexCode.TabIndex = 6;
            this.btnGenerateFlexCode.Text = "Generate Flex code!";
            this.btnGenerateFlexCode.UseVisualStyleBackColor = false;
            this.btnGenerateFlexCode.Click += new System.EventHandler(this.btnGenerateFlexCode_Click);
            // 
            // btGenerateAndroid
            // 
            this.btGenerateAndroid.BackColor = System.Drawing.Color.Red;
            this.btGenerateAndroid.Font = new System.Drawing.Font("Microsoft Sans Serif", 35F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btGenerateAndroid.ForeColor = System.Drawing.Color.White;
            this.btGenerateAndroid.Location = new System.Drawing.Point(892, 0);
            this.btGenerateAndroid.Name = "btGenerateAndroid";
            this.btGenerateAndroid.Size = new System.Drawing.Size(285, 178);
            this.btGenerateAndroid.TabIndex = 7;
            this.btGenerateAndroid.Text = "Generate Android code!";
            this.btGenerateAndroid.UseVisualStyleBackColor = false;
            this.btGenerateAndroid.Click += new System.EventHandler(this.btGenerateAndroid_Click);
            // 
            // btGenerateXamarin
            // 
            this.btGenerateXamarin.BackColor = System.Drawing.Color.Red;
            this.btGenerateXamarin.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btGenerateXamarin.ForeColor = System.Drawing.Color.White;
            this.btGenerateXamarin.Location = new System.Drawing.Point(577, 184);
            this.btGenerateXamarin.Name = "btGenerateXamarin";
            this.btGenerateXamarin.Size = new System.Drawing.Size(597, 185);
            this.btGenerateXamarin.TabIndex = 8;
            this.btGenerateXamarin.Text = "Generate Xamarin code!";
            this.btGenerateXamarin.UseVisualStyleBackColor = false;
            this.btGenerateXamarin.Click += new System.EventHandler(this.btGenerateXamarin_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1177, 631);
            this.Controls.Add(this.btGenerateXamarin);
            this.Controls.Add(this.btGenerateAndroid);
            this.Controls.Add(this.btnGenerateFlexCode);
            this.Controls.Add(this.btGenerateiPhoneCode);
            this.Controls.Add(this.btUncommentTracing);
            this.Controls.Add(this.btCommentTracing);
            this.Controls.Add(this.btLocalization);
            this.Controls.Add(this.btGenerate);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btGenerate;
		private System.Windows.Forms.Button btLocalization;
		private System.Windows.Forms.Button btCommentTracing;
		private System.Windows.Forms.Button btUncommentTracing;
		private System.Windows.Forms.Button btGenerateiPhoneCode;
        private System.Windows.Forms.Button btnGenerateFlexCode;
        private System.Windows.Forms.Button btGenerateAndroid;
        private System.Windows.Forms.Button btGenerateXamarin;
	}
}