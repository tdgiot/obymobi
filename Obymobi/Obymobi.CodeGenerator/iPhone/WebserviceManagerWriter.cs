﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.iPhone
{
    public class WebserviceManagerWriter
    {
        private StreamWriter streamForClassFile;
        private StreamWriter streamForHeaderFile;
        private List<MethodInfo> methods;
        private List<Type> nonStandardTypes;

        public WebserviceManagerWriter(List<MethodInfo> methods, StreamWriter streamClass, StreamWriter streamHeader)
        {
            this.streamForClassFile = streamClass;
            this.streamForHeaderFile = streamHeader;
            this.methods = methods;
            this.nonStandardTypes = new List<Type>();
        }

        public void Write()
        {
            this.GetNonStandardTypes();

            this.WriteIntroduction();
            this.WriteImports();
            this.WriteMethods();
            this.WriteClassEnd();
        }

        private void GetNonStandardTypes()
        {
            foreach (MethodInfo methodInfo in this.methods)
            {
                Type type = (methodInfo.ReturnType.GetGenericArguments().Length > 0 ? methodInfo.ReturnType.GetGenericArguments()[0] : methodInfo.ReturnType);
                if (type.ToString() != typeof(Int32).ToString() && type.ToString() != typeof(String).ToString() && !this.nonStandardTypes.Contains(type))
                    this.nonStandardTypes.Add(type);
            }
        }

        private void WriteIntroduction()
        {
            // Header
            WriteToHStream("@interface WebserviceManager : NSObject {{");
            WriteToHStream("}}");
            WriteToHStream("");

            // Class
            WriteToMStream("#import \"WebserviceManager.h\"");
            WriteToMStream("#import \"XmlToObjectParser.h\"");
            WriteToMStream("#import \"ObyResult.h\"");
            WriteToMStream("#import \"HtmlEntitiesConverter.h\"");
            WriteToMStream("");
            WriteToMStream("");
            WriteToMStream("@implementation WebserviceManager");
            WriteToMStream("");
        }

        private void WriteImports()
        {
        }

        private void WriteMethods()
        {
            foreach (MethodInfo methodInfo in this.methods)
            {
                Type type = (methodInfo.ReturnType.GetGenericArguments().Length > 0 ? methodInfo.ReturnType.GetGenericArguments()[0] : methodInfo.ReturnType);

                // Header
                WriteToHStream("-(NSArray*){0}{1};", methodInfo.Name, this.GetParameterString(methodInfo));

                // Class
                WriteToMStream("-(NSArray*){0}{1} {{", methodInfo.Name, this.GetParameterString(methodInfo));
                WriteToMStream("");
                WriteToMStream("\t// Create and initialize the url string");
                WriteToMStream("\t// TODO hardcoded API url, needs to be a variable");
                WriteToMStream("\tNSString *serviceUrl = [[NSString alloc] initWithFormat:@\"http://dev.crave-emenu.com/api/4.0/CraveService.asmx/{0}?{1}\", {2}];", methodInfo.Name, this.GetQueryString(methodInfo), this.GetFormatString(methodInfo));
                WriteToMStream("\tNSLog(serviceUrl);");
                WriteToMStream("");
                WriteToMStream("\t// Create and initialize the URL instance");
                WriteToMStream("\tNSURL* URL=[[NSURL alloc] initWithString:serviceUrl];");
                WriteToMStream("\t");
                WriteToMStream("\t// Create and initialize an XMLToObjectParser instance");
                WriteToMStream("\t// and parse the XML from the specified url");
                WriteToMStream("\tXMLToObjectParser *resultParser = [[XMLToObjectParser alloc] parseXMLAtURL:URL toObject:@\"Obyresult\" parseError:nil];");
                WriteToMStream("\t");
                WriteToMStream("\t// Get the Obyresult instance from the parsed XML");
                WriteToMStream("\tObyresult* result = (Obyresult *)[[resultParser items] objectAtIndex:0];");
                WriteToMStream("");
                WriteToMStream("\t// Create and initialize an XMLToObjectParser instance");
                WriteToMStream("\t// and parse the XML from the Obyresult");
                WriteToMStream("\tXMLToObjectParser *companyParser = [[XMLToObjectParser alloc] parseXMLFromString:result.data useXmlElement:@\"{0}\" toObject:@\"{0}Entity\" parseError:nil];", type.Name);
                WriteToMStream("");
                WriteToMStream("\t[serviceUrl release];");
                WriteToMStream("\t[URL release];");
                WriteToMStream("\t[resultParser release];");
                WriteToMStream("");
                WriteToMStream("\treturn [companyParser items];");
                WriteToMStream("}}");
                WriteToMStream("");
            }
        }

        //private void WriteMethods()
        //{
        //    for (int i = 0; i < this.methods.Count; i++)
        //    {
        //        MethodInfo methodInfo = this.methods[i];

        //        if (methodInfo.GetCustomAttributes(typeof(ClientLocalStorage), false).Length > 0)
        //        {
        //            WriteToStream("\t<EventHandlers type=\"{{WebserviceEvents.{0}}}\" debug=\"true\">", methodInfo.Name);
        //            WriteToStream("\t\t<MethodInvoker generator=\"{{{0}Handler}}\" method=\"save{0}\" arguments=\"{{[event]}}\" />", methodInfo.Name);
        //            WriteToStream("\t</EventHandlers>");
        //        }
        //        else
        //        {
        //            WriteToStream("\t<EventHandlers type=\"{{WebserviceEvents.{0}}}\" debug=\"true\">", methodInfo.Name);
        //            if (methodInfo.GetParameters().Length == 0)
        //                WriteToStream("\t\t<WebServiceInvoker instance=\"{{this.webservice}}\" method=\"{{WebserviceEvents.{0}}}\" >", methodInfo.Name);
        //            else
        //                WriteToStream("\t\t<WebServiceInvoker instance=\"{{this.webservice}}\" method=\"{{WebserviceEvents.{0}}}\" arguments=\"{{[{1}]}}\" >", methodInfo.Name, this.GetParameterString(methodInfo));
        //            WriteToStream("\t\t\t<resultHandlers>");
        //            WriteToStream("\t\t\t\t<MethodInvoker generator=\"{{{0}Handler}}\" method=\"getObyTypedResult\" arguments=\"{{resultObject}}\" />", methodInfo.Name);
        //            WriteToStream("\t\t\t\t<EventAnnouncer generator=\"{{{0}ResultEvent}}\" type=\"{{WebserviceEvents.{0}Result}}\" >", methodInfo.Name);
        //            WriteToStream("\t\t\t\t\t<Properties Result=\"{{lastReturn}}\" />");
        //            WriteToStream("\t\t\t\t</EventAnnouncer>");
        //            WriteToStream("\t\t\t</resultHandlers>");
        //            WriteToStream("\t\t\t<faultHandlers>");
        //            WriteToStream("\t\t\t\t<InlineInvoker method=\"ApplicationEvent.AnnounceWebserviceCallFailed\" arguments=\"{{ [ '{0}', fault ] }}\" />", methodInfo.Name);
        //            WriteToStream("\t\t\t</faultHandlers>");
        //            WriteToStream("\t\t</WebServiceInvoker>");
        //            WriteToStream("\t</EventHandlers>");

        //        }

        //        if (i != (this.methods.Count - 1))
        //            WriteToStream("");
        //    }
        //}

        private string GetParameterString(MethodInfo method)
        {
            string parameterString = string.Empty;
            ParameterInfo[] parameters = method.GetParameters();

            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo parameter = parameters[i];
                parameterString += string.Format(":({0}){1}", this.GetParameterType(parameter), parameter.Name);
            }

            return parameterString;
        }

        private string GetParameterType(ParameterInfo parameter)
        {
            string type = string.Empty;

            if (parameter.ParameterType.ToString() == typeof(Int32).ToString())
            {
                // Header
                type = "NSNumber*";
            }
            else if (parameter.ParameterType.ToString() == typeof(String).ToString())
            {
                // Header
                type = "NSString*";
            }

            return type;
        }

        private string GetQueryString(MethodInfo method)
        {
            string parameterString = string.Empty;
            ParameterInfo[] parameters = method.GetParameters();

            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo parameter = parameters[i];
                parameterString += string.Format("{0}=%@", parameter.Name);

                if (i != (parameters.Length - 1))
                    parameterString += "&";
            }

            return parameterString;
        }

        private string GetFormatString(MethodInfo method)
        {
            string parameterString = string.Empty;
            ParameterInfo[] parameters = method.GetParameters();

            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo parameter = parameters[i];
                parameterString += parameter.Name;

                if (i != (parameters.Length - 1))
                    parameterString += ", ";
            }

            return parameterString;
        }

        private void WriteClassEnd()
        {
            // Header
            WriteToHStream("");
            WriteToHStream("@end");

            // Class
            WriteToMStream("");
            WriteToMStream("@end");
        }

        private void WriteToMStream(string message, params object[] args)
        {
            this.streamForClassFile.WriteLine(string.Format(message, args));
        }

        private void WriteToHStream(string message, params object[] args)
        {
            this.streamForHeaderFile.WriteLine(string.Format(message, args));
        }
    }
}
