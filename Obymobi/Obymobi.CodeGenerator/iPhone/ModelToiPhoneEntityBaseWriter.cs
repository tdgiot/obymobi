﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.iPhone
{
	public class ModelToiPhoneEntityBaseWriter
	{
		private Object model;
		private Type modelType;
		private StreamWriter streamForClassFile;
		private StreamWriter streamForHeaderFile;
		private PropertyInfo[] fields;
		private PropertyInfo primaryKeyField = null;

		public ModelToiPhoneEntityBaseWriter(Object model, StreamWriter streamClass, StreamWriter streamHeader)
		{
			this.model = model;
			this.modelType = model.GetType();
			this.streamForClassFile = streamClass;
			this.streamForHeaderFile = streamHeader;

			PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
			List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
			for (int j = 0; j < propertyInfo.Length; j++)
			{
				PropertyInfo property = propertyInfo[j];

				if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
				{
					propertyInfoList.Add(property);
				}

				if (property.GetCustomAttributes(typeof(PrimaryKeyFieldOfModel), false).Length > 0)
				{
					if (this.primaryKeyField == null)
						this.primaryKeyField = property;
					else
						throw new Exception("No PK field found for Model: " + this.modelType.Name);
				}
			}

			this.fields = propertyInfoList.ToArray();
		}

		public void WriteEntity()
		{
			this.WriteIntroduction();
			this.WriteClassStart();
			this.WriteFields();
			this.WriteProperties();
			//this.WriteSqlRetrieveLogic();
			//this.WriteSqlSaveLogic();
			//this.WriteSqlDeleteLogic();
			//this.WriteFromAndToDataStreamLogic();
			//this.WriteXmlLogic();
			//this.WriteGenericPropertyAccessor();
			//this.WriteAdditionalLogic();
			this.WriteClassEnd();
		}

		public string GetCreateTableQuery()
		{
			string createQuery = string.Format("CREATE TABLE tbl{0} (", this.modelType.Name);
			string columns = "";
			foreach (var field in fields)
			{
				string typeName = "";
				if (field.PropertyType.ToString() == typeof(Int32).ToString())
				{
					typeName = "INTEGER";
				}
				else if (field.PropertyType.ToString() == typeof(String).ToString())
				{
					typeName = "TEXT";
				}

				
				if (typeName.Length > 0)
				{
					columns = StringUtil.CombineWithSeperator(",", columns, string.Format("{0} {1}", field.Name, typeName));
				}

				
			}
			createQuery += columns;
			createQuery += ");";

			return createQuery;
		}

		private void WriteIntroduction()
		{
			// Header
			WriteToHStream("#import <Foundation/Foundation.h>");
			//WriteToHStream("#import <sqlite3.h>");

            for (int i = 0; i < this.fields.Length; i++)
            {
                PropertyInfo field = this.fields[i];
                if (field.PropertyType.ToString() != typeof(Int32).ToString() && field.PropertyType.ToString() != typeof(String).ToString() && field.PropertyType.ToString() != typeof(Boolean).ToString())
                {
                    Type type = (field.PropertyType.GetGenericArguments().Length > 0 ? field.PropertyType.GetGenericArguments()[0] : field.PropertyType);
                    WriteToHStream("#import \"{0}.h\"", type.Name.Replace("[]", "Collection"));
                }                
            }

			WriteToHStream("");
			WriteToHStream("");

			// Class
			WriteToMStream("#import \"{0}EntityBase.h\"", this.modelType.Name);
			//WriteToMStream("#import <sqlite3.h>");
			//WriteToMStream("#import \"FMDatabase.h\"");
			//WriteToMStream("#import \"FMResultSet.h\"");
			//WriteToMStream("#import \"DbUtil.h\"");
			WriteToMStream("");
			WriteToMStream("");
		}

		private void WriteClassStart()
		{
			// Header
			WriteToHStream("@interface {0}EntityBase : NSObject {{", this.modelType.Name);
			WriteToHStream("");

			// Class
			WriteToMStream("@implementation {0}EntityBase", this.modelType.Name);
			WriteToMStream("");
		}

		private void WriteFields()
		{
			foreach (var field in fields)
			{
				if (field.PropertyType.ToString() == typeof(Int32).ToString())
				{
					// Header
					WriteToHStream("NSNumber *{0};", field.Name);

				}
                else if (field.PropertyType.ToString() == typeof(String).ToString())
                {
                    // Header
                    WriteToHStream("NSString *{0};", field.Name);
                }
                else if (field.PropertyType.ToString() == typeof(Boolean).ToString())
                {
                    // Header
                    WriteToHStream("BOOL {0};", field.Name);
                }
                else
                {
                    Type type = (field.PropertyType.GetGenericArguments().Length > 0 ? field.PropertyType.GetGenericArguments()[0] : field.PropertyType);
                    WriteToHStream("{0} *{1};", type.Name.Replace("[]", "Collection"), field.Name);
                }
			}

			// Header
			WriteToHStream("");
			WriteToHStream("}}");
			WriteToHStream("");

			// Class
			WriteToMStream("");
			WriteToMStream("");

		}

		private void WriteSqlRetrieveLogic()
		{
			// Header
            if (this.primaryKeyField == null)
            {
                // Dont do shit
            }
			else if(this.primaryKeyField.PropertyType.ToString() == typeof(String).ToString())
			{
				WriteToHStream("- (id)initWithPrimaryKey:(NSString*)pk;");
				WriteToMStream("- (id)initWithPrimaryKey:(NSString*)pk {{");
			}
			else if(this.primaryKeyField.PropertyType.ToString() == typeof(Int32).ToString())
			{
				WriteToHStream("- (id)initWithPrimaryKey:(NSNumber*)pk;");
				WriteToMStream("- (id)initWithPrimaryKey:(NSNumber*)pk {{");
			}
									
			WriteToMStream("FMDatabase * db = [DbUtil GetDatabase];");
			WriteToMStream("//NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];");
			WriteToMStream("FMResultSet *rs = [db executeQuery:@\"select * from tbl{0} where {1} = ?\", pk];", this.modelType.Name, this.primaryKeyField.Name);
			WriteToMStream("BOOL retrieved = NO;");
			WriteToMStream("while ([rs next]) {{");
			WriteToMStream("	// just print out what we've got in a number of formats.");
			WriteToMStream("	if(retrieved) {{");
			WriteToMStream("		NSAssert1(0, @\"Multiple records found for PK: %@ of {0}\", pk);", this.modelType.Name);
			WriteToMStream("	}} else {{");
			WriteToMStream("		[rs kvcMagic:self];");
			WriteToMStream("	}}");
			WriteToMStream("}}");
			WriteToMStream("");
			WriteToMStream("// close the result set.");
			WriteToMStream("// it'll also close when it's dealloc'd, but we're closing the database before");
			WriteToMStream("// the autorelease pool closes, so sqlite will complain about it.");
			WriteToMStream("//[rs close];");
			WriteToMStream("//[pool release];");
			WriteToMStream("return self;");
			WriteToMStream("}}");
		}

		private void WriteSqlSaveLogic()
		{
			// Header
			WriteToHStream("- (BOOL)save;");

			// Class
			WriteToMStream("- (BOOL)save {{");
			WriteToMStream("	FMDatabase * db = [DbUtil GetDatabase];");
			WriteToMStream("");
			WriteToMStream("	BOOL update = NO;");
			WriteToMStream("	BOOL success = YES;");
			WriteToMStream("	// Check if a entity with the PK exists");
			WriteToMStream("");
			WriteToMStream("	if(self.{0} != nil && self.{0} >= 0)", this.primaryKeyField.Name);
			WriteToMStream("	{{");
			WriteToMStream("		{0}EntityBase * existing = [[{0}EntityBase alloc] initWithPrimaryKey:self.{1}];", this.modelType.Name, this.primaryKeyField.Name);
			WriteToMStream("										 ");
			WriteToMStream("		if(existing.{0} != nil && existing.{0} > 0)", this.primaryKeyField.Name);
			WriteToMStream("		{{");
			WriteToMStream("			update = YES;");
			WriteToMStream("		}}");
			WriteToMStream("		");
			WriteToMStream("		[existing release];");
			WriteToMStream("	}}		");
			WriteToMStream("	");
			WriteToMStream("	if(update)");
			WriteToMStream("	{{");
			WriteToMStream("		// UPDATE");

			string actualValues = "";
			foreach (var field in this.fields)
			{
				actualValues = StringUtil.CombineWithSeperator(", ", actualValues, string.Format("self.{0}",field.Name));
			}	

			// Generate UPDATE query
			string updateSetPart = "";
			foreach (var field in this.fields)
			{
				updateSetPart = StringUtil.CombineWithSeperator(", ", updateSetPart, string.Format("{0} = ?", field.Name));
			}
			WriteToMStream("		[db executeUpdate:@\"update tbl{0} set {1} where {2} = ?\", {3}, self.{2}];", this.modelType.Name, updateSetPart, this.primaryKeyField.Name, actualValues);

			WriteToMStream("	}}");
			WriteToMStream("	else");
			WriteToMStream("	{{");
			WriteToMStream("		// INSERT");

			// Generate INSERT query			
			string insertColumnsPart = "";
			foreach (var field in this.fields)
			{				
				insertColumnsPart = StringUtil.CombineWithSeperator(", ", insertColumnsPart, field.Name);
			}

			string insertValuesPart = "";
			foreach (var field in this.fields)
			{
				insertValuesPart = StringUtil.CombineWithSeperator(", ", insertValuesPart, "?");
			}

			WriteToMStream("		[db executeUpdate:@\"insert into tbl{0} ({1}) values ({2})\", {3}];", this.modelType.Name, insertColumnsPart, insertValuesPart, actualValues);

			WriteToMStream("	}}");
			WriteToMStream("	");
			WriteToMStream("	if ([db hadError]) {{");
			WriteToMStream("		NSLog(@\"Err %d: %@\", [db lastErrorCode], [db lastErrorMessage]);");
			WriteToMStream("			success = NO;");
			WriteToMStream("	}}	");
			WriteToMStream("		");
			WriteToMStream("	return success;");
			WriteToMStream("}}");
		}

		private void WriteSqlDeleteLogic()
		{
			// Header
			WriteToHStream("- (BOOL)delete;");

			// Class
			WriteToMStream("- (BOOL)delete {{");			
			WriteToMStream("	");
			WriteToMStream("	FMDatabase * db = [DbUtil GetDatabase];");
			WriteToMStream("	BOOL found = NO;");
			WriteToMStream("	BOOL success = NO;");
			WriteToMStream("	");
			WriteToMStream("	// Check if a entity with the PK exists");
			WriteToMStream("");
			WriteToMStream("	if(self.{0} != nil && self.{0} >= 0)", this.primaryKeyField.Name);
			WriteToMStream("	{{");
			WriteToMStream("		{0}EntityBase * existing = [[{0}EntityBase alloc] initWithPrimaryKey:self.{1}];", this.modelType.Name, this.primaryKeyField.Name);
			WriteToMStream("		");
			WriteToMStream("		if(existing.{0} != nil && existing.{0} > 0)", this.primaryKeyField.Name);
			WriteToMStream("		{{");
			WriteToMStream("			found = YES;");
			WriteToMStream("		}}");
			WriteToMStream("		");
			WriteToMStream("		[existing release];");
			WriteToMStream("	}}	");
			WriteToMStream("	");
			WriteToMStream("	if(found)");
			WriteToMStream("	{{");
			WriteToMStream("		[db executeUpdate:@\"DELETE FROM tbl{0} WHERE {1} = ?\"];", this.modelType.Name, this.primaryKeyField.Name);
			WriteToMStream("	");
			WriteToMStream("		if ([db hadError]) {{");
			WriteToMStream("			NSLog(@\"Error on delete {0} %d: %@\", [db lastErrorCode], [db lastErrorMessage]);", this.modelType.Name);
			WriteToMStream("			success = NO;");
			WriteToMStream("		}}		");
			WriteToMStream("	}}");
			WriteToMStream("	");
			WriteToMStream("	return success;	");
			WriteToMStream("}}");
		}

		private void WriteProperties()
		{
			foreach (var field in fields)
			{
				string typeName = "";
				if (field.PropertyType.ToString() == typeof(Int32).ToString())
				{
					typeName = "NSNumber";
				}
                else if (field.PropertyType.ToString() == typeof(String).ToString())
                {
                    typeName = "NSString";
                }
                else if (field.PropertyType.ToString() == typeof(Boolean).ToString())
                {
                    typeName = "BOOL";
                }
                else
                {
                    Type type = (field.PropertyType.GetGenericArguments().Length > 0 ? field.PropertyType.GetGenericArguments()[0] : field.PropertyType);
                    typeName = type.Name.Replace("[]", "Collection");
                }

				if (typeName.Length > 0)
				{
                    if (typeName == "BOOL")
                        WriteToHStream("@property {0} {1};", typeName, field.Name);
                    else
                        WriteToHStream("@property(nonatomic,retain) {0} *{1};", typeName, field.Name);

					// Class
					WriteToMStream("@synthesize {0};", field.Name);
				}
			}

			// Header
			WriteToHStream("");
			WriteToHStream("");

			// Class
			WriteToMStream("");
			WriteToMStream("");
		}

		private void WriteClassEnd()
		{
			// Header
			WriteToHStream("");
			WriteToHStream("@end");

			// Class
			WriteToMStream("");
			WriteToMStream("@end");
		}

		private void WriteToMStream(string message, params object[] args)
		{
			this.streamForClassFile.WriteLine(string.Format(message, args));
		}

		private void WriteToHStream(string message, params object[] args)
		{
			this.streamForHeaderFile.WriteLine(string.Format(message, args));
		}
	}
}
