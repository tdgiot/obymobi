﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using Obymobi.Attributes;
using Obymobi.CodeGenerator.iPhone;

namespace Obymobi.CodeGenerator
{
	public class iPhoneGenerator
	{
		#region Fields

		private string modelNamespace = string.Empty;
		private Assembly modelAssemlby = null;
		private string codePath = string.Empty;
		private List<string> createTableQueries = new List<string>(); 

		#endregion

		#region Methods

		public iPhoneGenerator(string codePath, Type typeOfOneModelClass)
		{
			this.codePath = codePath;
			this.modelNamespace = typeOfOneModelClass.Namespace;
			this.modelAssemlby = Assembly.GetAssembly(typeOfOneModelClass);
		}

		public void Generate()
		{
			// Get all classes to write
			List<Type> models = this.GetAllModels();

            string output = string.Empty;

            foreach (var modelClassType in models)
            {
                if (modelClassType.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
                {
                    this.WriteEntityBaseFile(modelClassType);
                    this.WriteEntityFile(modelClassType);
                    this.WriteEntityCollectionFile(modelClassType);

                    output += modelClassType.Name + "|";

                    Object modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelClassType);

                    PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(modelInstance);
                    List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
                    for (int j = 0; j < propertyInfo.Length; j++)
                    {
                        PropertyInfo property = propertyInfo[j];

                        if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
                        {
                            output += property.Name + "|";

                            propertyInfoList.Add(property);
                        }
                    }

                    output += "\r\n";
                }
            }

            List<MethodInfo> methods = this.GetAllWebserviceMethods();
            this.WriteWebserviceManager(methods);

            //this.GenerateDbUtilHeader();
            //this.GenerateDbUtilClass();

            //this.GenerateCreateTableStatementsHeader();
            //this.GenerateCreateTableStatementsClass();
		}

        private void GenerateDbUtilHeader()
        {
            // Save Create Queries			
            StreamWriter stream = new StreamWriter(Path.Combine(Path.Combine(this.codePath, "Data"), "DbUtil.h"));

            stream.WriteLine("//");
            stream.WriteLine("//  DbUtil.h");
            stream.WriteLine("//  Crave Mobile for iOS");
            stream.WriteLine("//");
            stream.WriteLine("//  Created by Crave on {0}/{1}/{2}.", DateTime.Now.Day.ToString("00"), DateTime.Now.Month.ToString("00"), DateTime.Now.Year.ToString());
            stream.WriteLine("//  Copyright {0} Crave. All rights reserved.", DateTime.Now.Year);
            stream.WriteLine("//");
            stream.WriteLine("");
            stream.WriteLine("#import <Foundation/Foundation.h>");
            stream.WriteLine("#import \"FMDatabase.h\"");
            stream.WriteLine("");
            stream.WriteLine("@interface DbUtil : NSObject {");
            stream.WriteLine("}");
            stream.WriteLine("");
            stream.WriteLine("+(void)CreateDatabaseIfNeeded;");
            stream.WriteLine("+(FMDatabase*)GetDatabase;");
            stream.WriteLine("");
            stream.WriteLine("@end");

            stream.Flush();
            stream.Close();
        }

        private void GenerateDbUtilClass()
        {
            // Save Create Queries			
            StreamWriter stream = new StreamWriter(Path.Combine(Path.Combine(this.codePath, "Data"), "DbUtil.m"));

            stream.WriteLine("//");
            stream.WriteLine("//  DbUtil.m");
            stream.WriteLine("//  Crave Mobile for iOS");
            stream.WriteLine("//");
            stream.WriteLine("//  Created by Crave on {0}/{1}/{2}.", DateTime.Now.Day.ToString("00"), DateTime.Now.Month.ToString("00"), DateTime.Now.Year.ToString());
            stream.WriteLine("//  Copyright {0} Crave. All rights reserved.", DateTime.Now.Year);
            stream.WriteLine("//");
            stream.WriteLine("");
            stream.WriteLine("#import \"DbUtil.h\"");
            stream.WriteLine("#import \"FMDatabase.h\"");
            stream.WriteLine("#import \"CreateTableStatements.h\"");
            stream.WriteLine("");
            stream.WriteLine("@implementation DbUtil");
            stream.WriteLine("");
            stream.WriteLine("+(NSString*)GetDbPath {");
            stream.WriteLine("	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);");
            stream.WriteLine("	NSString *documentsDirectory = [paths objectAtIndex:0];");
            stream.WriteLine("	return [documentsDirectory stringByAppendingPathComponent:@\"oby.mobi\"];");
            stream.WriteLine("}");
            stream.WriteLine("");
            stream.WriteLine("+(void)CreateDatabaseIfNeeded {");
            stream.WriteLine("");
            stream.WriteLine("    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];");
            stream.WriteLine("");
            stream.WriteLine("    // Check for existing DB, if exists, return.");
            stream.WriteLine("	NSString * dbPath = [DbUtil GetDbPath];");
            stream.WriteLine("");
            stream.WriteLine("	NSFileManager *fileManager = [[NSFileManager defaultManager] autorelease];");
            stream.WriteLine("	BOOL success = [fileManager fileExistsAtPath:dbPath];");
            stream.WriteLine("	if(success) return;");
            stream.WriteLine("");
            stream.WriteLine("    // Not found, we need to create");
            stream.WriteLine("    FMDatabase* db = [FMDatabase databaseWithPath:dbPath];");
            stream.WriteLine("    if (![db open]) {");
            stream.WriteLine("        NSAssert1(0, @\"Could not open db %s.\", dbPath);");
            stream.WriteLine("        [pool release];");
            stream.WriteLine("        return;");
            stream.WriteLine("    }");
            stream.WriteLine("");
            stream.WriteLine("	// Opened, create tables");
            stream.WriteLine("	NSMutableArray * createStatements = [CreateTableStatements GetStatements];");
            stream.WriteLine("	for(NSString *statement in createStatements)");
            stream.WriteLine("	{");
            stream.WriteLine("		[db executeUpdate:statement];");
            stream.WriteLine("		if ([db hadError]) {");
            stream.WriteLine("	        NSAssert1(0, @\"Could not create table %s.\", statement);");
            stream.WriteLine("			return;");
            stream.WriteLine("		}");
            stream.WriteLine("	}");
            stream.WriteLine("");
            stream.WriteLine("	[pool drain];");
            stream.WriteLine("}");
            stream.WriteLine("");
            stream.WriteLine("+(FMDatabase*)GetDatabase {");
            stream.WriteLine("	NSString * dbPath = [DbUtil GetDbPath];");
            stream.WriteLine("    FMDatabase* db = [FMDatabase databaseWithPath:dbPath];");
            stream.WriteLine("    if (![db open]) {");
            stream.WriteLine("        NSAssert1(0, @\"Could not open db %@.\", dbPath);");
            stream.WriteLine("        return nil;");
            stream.WriteLine("    }");
            stream.WriteLine("	return db;");
            stream.WriteLine("}");
            stream.WriteLine("");
            stream.WriteLine("@end");

            stream.Flush();
            stream.Close();
        }

        private void GenerateCreateTableStatementsHeader()
        {
            // Save Create Queries			
            StreamWriter stream = new StreamWriter(Path.Combine(Path.Combine(this.codePath, "Data"), "CreateTableStatements.h"));

            stream.WriteLine("//");
            stream.WriteLine("//  CreateTableStatements.h");
            stream.WriteLine("//  Crave Mobile for iOS");
            stream.WriteLine("//");
            stream.WriteLine("//  Created by Crave on {0}/{1}/{2}.", DateTime.Now.Day.ToString("00"), DateTime.Now.Month.ToString("00"), DateTime.Now.Year.ToString());
            stream.WriteLine("//  Copyright {0} Crave. All rights reserved.", DateTime.Now.Year);
            stream.WriteLine("//");
            stream.WriteLine("");
            stream.WriteLine("#import <Foundation/Foundation.h>");
            stream.WriteLine("");
            stream.WriteLine("");
            stream.WriteLine("@interface CreateTableStatements : NSObject {");
            stream.WriteLine("}");
            stream.WriteLine("");
            stream.WriteLine("+(NSMutableArray*)GetStatements;");
            stream.WriteLine("");
            stream.WriteLine("@end");

            stream.Flush();
            stream.Close();
        }

		private void GenerateCreateTableStatementsClass()
		{
			// Save Create Queries			
            StreamWriter stream = new StreamWriter(Path.Combine(Path.Combine(this.codePath, "Data"), "CreateTableStatements.m"));

			stream.WriteLine("//");
			stream.WriteLine("//  CreateTableStatements.m");
			stream.WriteLine("//  Crave Mobile for iOS");
			stream.WriteLine("//");
            stream.WriteLine("//  Created by Crave on {0}/{1}/{2}.", DateTime.Now.Day.ToString("00"), DateTime.Now.Month.ToString("00"), DateTime.Now.Year.ToString());
			stream.WriteLine("//  Copyright {0} Crave. All rights reserved.", DateTime.Now.Year);
			stream.WriteLine("//");
            stream.WriteLine("");
			stream.WriteLine("#import \"CreateTableStatements.h\"");
            stream.WriteLine("");
            stream.WriteLine("");
			stream.WriteLine("@implementation CreateTableStatements");
            stream.WriteLine("");
			stream.WriteLine("+(NSMutableArray*)GetStatements{");
			stream.WriteLine("				");
			stream.WriteLine("	NSMutableArray *mutable = [[[NSMutableArray alloc] init] autorelease];");
			
			// Add queries
			foreach(var statement in this.createTableQueries)
			{
				stream.WriteLine(string.Format("	[mutable addObject: @\"{0}\"];", statement));
			}

			stream.WriteLine("	return mutable;");
			stream.WriteLine("}");
            stream.WriteLine("");
			stream.WriteLine("@end");

			stream.Flush();
			stream.Close();
		}

		private void WriteEntityBaseFile(Type modelType)
		{			
			Object modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

			// Init the file
            string entityBaseClassDirectory = Path.Combine(Path.Combine(this.codePath, "Data"), "EntityBaseClasses");
			if (!Directory.Exists(entityBaseClassDirectory))
				Directory.CreateDirectory(entityBaseClassDirectory);
			StreamWriter writerH = new StreamWriter(Path.Combine(entityBaseClassDirectory, modelType.Name + "EntityBase.h"));
			StreamWriter writerM = new StreamWriter(Path.Combine(entityBaseClassDirectory, modelType.Name + "EntityBase.m"));

			// Write contents
			ModelToiPhoneEntityBaseWriter entityWriter = new ModelToiPhoneEntityBaseWriter(modelInstance, writerM, writerH);
			entityWriter.WriteEntity();
			this.createTableQueries.Add(entityWriter.GetCreateTableQuery());

			// Close
			writerH.Flush();
			writerH.Close();

			writerM.Flush();
			writerM.Close();			
		}

        private void WriteEntityFile(Type modelType)
        {
            Object modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

            // Init the file
            string entityClassDirectory = Path.Combine(Path.Combine(this.codePath, "Data"), "EntityClasses");
            if (!Directory.Exists(entityClassDirectory))
                Directory.CreateDirectory(entityClassDirectory);
            StreamWriter writerH = new StreamWriter(Path.Combine(entityClassDirectory, modelType.Name + "Entity.h"));
            StreamWriter writerM = new StreamWriter(Path.Combine(entityClassDirectory, modelType.Name + "Entity.m"));

            // Write contents
            ModelToiPhoneEntityWriter entityWriter = new ModelToiPhoneEntityWriter(modelInstance, writerM, writerH);
            entityWriter.WriteEntity();
            //this.createTableQueries.Add(entityWriter.GetCreateTableQuery());

            // Close
            writerH.Flush();
            writerH.Close();

            writerM.Flush();
            writerM.Close();
        }

        private void WriteEntityCollectionFile(Type modelType)
        {
            Object modelInstance = Dionysos.InstanceFactory.CreateInstance(this.modelAssemlby, modelType);

            // Init the file
            string collectionBaseClassDirectory = Path.Combine(Path.Combine(this.codePath, "Data"), "CollectionClasses");
            if (!Directory.Exists(collectionBaseClassDirectory))
                Directory.CreateDirectory(collectionBaseClassDirectory);

            StreamWriter writerH = new StreamWriter(Path.Combine(collectionBaseClassDirectory, modelType.Name + "Collection.h"));
            StreamWriter writerM = new StreamWriter(Path.Combine(collectionBaseClassDirectory, modelType.Name + "Collection.m"));

            // Write contents
            ModelToiPhoneCollectionWriter entityWriter = new ModelToiPhoneCollectionWriter(modelInstance, writerM, writerH);
            entityWriter.WriteCollection();

            // Close
            writerH.Flush();
            writerH.Close();

            writerM.Flush();
            writerM.Close();
        }

        private void WriteWebserviceManager(List<MethodInfo> methods)
        {
            // Init the file
            string collectionBaseClassDirectory = Path.Combine(this.codePath, "Net");
            if (!Directory.Exists(collectionBaseClassDirectory))
                Directory.CreateDirectory(collectionBaseClassDirectory);

            StreamWriter writerH = new StreamWriter(Path.Combine(collectionBaseClassDirectory, "WebserviceManager.h"));
            StreamWriter writerM = new StreamWriter(Path.Combine(collectionBaseClassDirectory, "WebserviceManager.m"));

            // Write contents
            WebserviceManagerWriter webserviceManagerWriter = new WebserviceManagerWriter(methods, writerM, writerH);
            webserviceManagerWriter.Write();

            // Close
            writerH.Flush();
            writerH.Close();

            writerM.Flush();
            writerM.Close();
        }

		/// <summary>
		/// Method to populate a list with all the class
		/// in the namespace provided by the user
		/// </summary>
		/// <param name="nameSpace">The namespace the user wants searched</param>
		/// <returns></returns>
		private List<Type> GetAllModels()
		{			
			List<Type> models = new List<Type>();
			foreach (Type type in this.modelAssemlby.GetTypes())
			{
				if (type.Namespace == this.modelNamespace)
					models.Add(type);
			}
			return models;

			#region Stolen from:
			/*
			//create an Assembly and use its GetExecutingAssembly Method
			//http://msdn2.microsoft.com/en-us/library/system.reflection.assembly.getexecutingassembly.aspx
			Assembly asm = Assembly.GetAssembly(typeof(Obymobi.Logic.Obyresult));
			//create a list for the namespaces
			List<string> namespaceList = new List<string>();
			//create a list that will hold all the classes
			//the suplied namespace is executing
			List<string> returnList = new List<string>();
			//loop through all the "Types" in the Assembly using
			//the GetType method:
			//http://msdn2.microsoft.com/en-us/library/system.reflection.assembly.gettypes.aspx
			foreach (Type type in asm.GetTypes())
			{
				if (type.Namespace == this.modelNamespace)
					namespaceList.Add(type.Name);
			}
			//now loop through all the classes returned above and add
			//them to our classesName list
			foreach (String className in namespaceList)
				returnList.Add(className);
			//return the list
			return returnList;
			 */
			#endregion
		}

        /// <summary>
        /// Method to populate a list with all the webservice methods
        /// </summary>
        /// <returns></returns>
        private List<MethodInfo> GetAllWebserviceMethods()
        {
            Assembly assembly = Assembly.Load("Obymobi.Logic");
            Type type = assembly.GetType("Obymobi.Logic.Interfaces.IWebserviceHandler");
            MethodInfo[] methodInfo = type.GetMethods();
            List<MethodInfo> methodInfoList = new List<MethodInfo>();
            for (int j = 0; j < methodInfo.Length; j++)
            {
                MethodInfo method = methodInfo[j];
                if (method.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
                    methodInfoList.Add(method);
            }

            return methodInfoList;
        }

		#endregion

		#region Event Handlers
		#endregion

		#region Properties	

		#endregion
		
	}
}
