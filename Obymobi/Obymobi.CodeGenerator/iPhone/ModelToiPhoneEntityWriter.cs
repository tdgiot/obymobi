﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.iPhone
{
	public class ModelToiPhoneEntityWriter
	{ 
		private Object model;
		private Type modelType;
		private StreamWriter streamForClassFile;
		private StreamWriter streamForHeaderFile;
		private PropertyInfo[] fields;
		private PropertyInfo primaryKeyField = null;

		public ModelToiPhoneEntityWriter(Object model, StreamWriter streamClass, StreamWriter streamHeader)
		{
			this.model = model;
			this.modelType = model.GetType();
			this.streamForClassFile = streamClass;
			this.streamForHeaderFile = streamHeader;

			PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
			List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
			for (int j = 0; j < propertyInfo.Length; j++)
			{
				PropertyInfo property = propertyInfo[j];

                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0 &&
					(property.PropertyType.ToString() == typeof(String).ToString() || property.PropertyType.ToString() == typeof(Int32).ToString()))
				{
					propertyInfoList.Add(property);
				}

				if (property.GetCustomAttributes(typeof(PrimaryKeyFieldOfModel), false).Length > 0)
				{
					if (this.primaryKeyField == null)
						this.primaryKeyField = property;
					else
						throw new Exception("No PK field found for Model: " + this.modelType.Name);
				}
			}

			this.fields = propertyInfoList.ToArray();
		}

		public void WriteEntity()
		{
			this.WriteIntroduction();
			this.WriteClassStart();
			this.WriteClassEnd();
		}

		private void WriteIntroduction()
		{
			// Header
			WriteToHStream("#import <Foundation/Foundation.h>");
			WriteToHStream("#import \"{0}EntityBase.h\"", this.modelType.Name);
			WriteToHStream("");
			WriteToHStream("");

			// Class
    		WriteToMStream("#import \"{0}Entity.h\"", this.modelType.Name);
            //WriteToMStream("#import <sqlite3.h>");
            //WriteToMStream("#import \"FMDatabase.h\"");
            //WriteToMStream("#import \"FMResultSet.h\"");
            //WriteToMStream("#import \"DbUtil.h\"");
			WriteToMStream("");
			WriteToMStream("");
		}

		private void WriteClassStart()
		{
			// Header
			WriteToHStream("@interface {0}Entity : {0}EntityBase {{", this.modelType.Name);
            WriteToHStream("}}");

			// Class
			WriteToMStream("@implementation {0}Entity", this.modelType.Name);
		}

		private void WriteClassEnd()
		{
			// Header
			WriteToHStream("");
			WriteToHStream("@end");

			// Class
			WriteToMStream("");
			WriteToMStream("@end");
		}

		private void WriteToMStream(string message, params object[] args)
		{
			this.streamForClassFile.WriteLine(string.Format(message, args));
		}

		private void WriteToHStream(string message, params object[] args)
		{
			this.streamForHeaderFile.WriteLine(string.Format(message, args));
		}
	}
}
