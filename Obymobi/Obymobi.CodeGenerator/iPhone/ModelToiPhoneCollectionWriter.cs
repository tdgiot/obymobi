﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.iPhone
{
	public class ModelToiPhoneCollectionWriter
	{
		private Object model;
		private Type modelType;
		private StreamWriter streamForClassFile;
		private StreamWriter streamForHeaderFile;

		public ModelToiPhoneCollectionWriter(Object model, StreamWriter streamClass, StreamWriter streamHeader)
		{
			this.model = model;
			this.modelType = model.GetType();
			this.streamForClassFile = streamClass;
			this.streamForHeaderFile = streamHeader;
		}

		public void WriteCollection()
		{
			this.WriteIntroduction();
			this.WriteClassStart();
            this.WriteClassBody();
			this.WriteClassEnd();
		}

		private void WriteIntroduction()
		{
			// Header
			WriteToHStream("//");
			WriteToHStream("// {0}Collection.h", this.modelType.Name);
            WriteToHStream("//  Obymobi for iPhone");
            WriteToHStream("//");
            WriteToHStream("//  Created by Grenos on {0}/{1}/{2}.", DateTime.Now.Day.ToString("00"), DateTime.Now.Month.ToString("00"), DateTime.Now.Year.ToString());
            WriteToHStream("//  Copyright {0} Grenos. All rights reserved.", DateTime.Now.Year);
            WriteToHStream("//");
			WriteToHStream("");
			WriteToHStream("");
            WriteToHStream("#import \"{0}Entity.h\"", this.modelType.Name);
            WriteToHStream("");
            WriteToHStream("");

			// Class
			WriteToMStream("//");
			WriteToMStream("// {0}Collection.m", this.modelType.Name);
            WriteToMStream("//  Obymobi for iPhone");
            WriteToMStream("//");
            WriteToMStream("//  Created by Grenos on {0}/{1}/{2}.", DateTime.Now.Day.ToString("00"), DateTime.Now.Month.ToString("00"), DateTime.Now.Year.ToString());
            WriteToMStream("//  Copyright {0} Grenos. All rights reserved.", DateTime.Now.Year);
            WriteToMStream("//");
			WriteToMStream("");
			WriteToMStream("");
            WriteToMStream("#import \"{0}Entity.h\"", this.modelType.Name);
			WriteToMStream("#import \"{0}Collection.h\"", this.modelType.Name);
			WriteToMStream("");
			WriteToMStream("");
		}

		private void WriteClassStart()
		{
			// Header
			WriteToHStream("@interface {0}Collection : NSObject {{", this.modelType.Name);
            WriteToHStream("\tNSMutableArray *Items;");
            WriteToHStream("}}");
            WriteToHStream("");
            WriteToHStream("@property(nonatomic,retain) NSMutableArray *Items;");
            WriteToHStream("");

			// Class
			WriteToMStream("@implementation {0}Collection", this.modelType.Name);
            WriteToMStream("@synthesize Items;");
            WriteToMStream("");
		}

        private void WriteClassBody()
        {
            // Header
            WriteToHStream("- (id)init;");
            WriteToHStream("- (NSInteger)count;");
            WriteToHStream("- (void)add{0}Entity:({0}Entity*){1}Entity;", this.modelType.Name, this.modelType.Name.TurnFirstToLower(false));
            WriteToHStream("- ({0}Entity*)get{0}EntityAt:(NSInteger)index;", this.modelType.Name);

            // Class
            WriteToMStream("-(id)init {{");
            WriteToMStream("\tself.Items = [[NSMutableArray alloc] init];");
            WriteToMStream("\treturn [super init];");
            WriteToMStream("}}");
            WriteToMStream("");
            WriteToMStream("- (NSInteger)count {{");
            WriteToMStream("\treturn [self.Items count];");
            WriteToMStream("}}");
            WriteToMStream("");
            WriteToMStream("- (void)add{0}Entity:({0}Entity*){1}Entity {{", this.modelType.Name, this.modelType.Name.TurnFirstToLower(false));
            WriteToMStream("\t[self.Items addObject:{0}Entity];", this.modelType.Name.TurnFirstToLower(false));
            WriteToMStream("}}");
            WriteToMStream("");
            WriteToMStream("- ({0}Entity*)get{0}EntityAt:(NSInteger)index {{", this.modelType.Name);
            WriteToMStream("\t{0}Entity *{1} = nil;", this.modelType.Name, this.modelType.Name.TurnFirstToLower(false));
            WriteToMStream("\tif ([self count] >= (index+1))");
            WriteToMStream("\t\t{0} = [self.Items objectAtIndex:index];", this.modelType.Name.TurnFirstToLower(false));
            WriteToMStream("\treturn {0};", this.modelType.Name.TurnFirstToLower(false));
            WriteToMStream("}}");
        }

		private void WriteClassEnd()
		{
			// Header
			WriteToHStream("");
			WriteToHStream("@end");

			// Class
			WriteToMStream("");
			WriteToMStream("@end");
		}

		private void WriteToMStream(string message, params object[] args)
		{
			this.streamForClassFile.WriteLine(string.Format(message, args));
		}

		private void WriteToHStream(string message, params object[] args)
		{
			this.streamForHeaderFile.WriteLine(string.Format(message, args));
		}
	}
}
