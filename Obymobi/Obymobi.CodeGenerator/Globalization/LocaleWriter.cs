﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Obymobi.CodeGenerator
{
    public class LocaleFileWriter
    {
        private StreamWriter stream;
        private Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> dictionary;
        private int max;
        private string[] lines;

        public LocaleFileWriter(StreamWriter stream, Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> dictionary, int max, string[] lines)
        {
            this.stream = stream;
            this.dictionary = dictionary;
            this.max = max;
            this.lines = lines;
        }

        public void Write()
        {
            foreach (string key in this.dictionary.Keys)
            {
                List<GlobalizationItem> items = this.dictionary[key];
                this.WriteToStream("# {0}", key);

                foreach (GlobalizationItem item in items)
                {
                    string tabs = string.Empty;
                    int length = key.Length + item.VariableName.Length;
                    if (length < max)
                    {
                        int diff = max - length;
                        for (int i = 0; i < diff; i++)
                        {
                            tabs += " ";
                        }
                    }

                    string value = item.Value;

                    //foreach (string line in this.lines)
                    //{
                    //    if (line.Contains(string.Format("{0}.{1}", key, item.VariableName)))
                    //    {
                    //        value = line.Substring(line.IndexOf("=")+2);
                    //    }
                    //}

                    this.WriteToStream("{0}.{1}{2}= {3}", key, item.VariableName, tabs, value);    
                }

                this.WriteToStream("");
            }
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
