﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Dionysos;

namespace Obymobi.CodeGenerator
{
    public class LocalizationConstantsWriter
    {
        private StreamWriter stream;
        private Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> dictionary;

        public LocalizationConstantsWriter(StreamWriter stream, Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> dictionary)
        {
            this.stream = stream;
            this.dictionary = dictionary;
        }

        public void Write()
        {
            this.WriteIntroduction();
            this.WriteBody();
            this.WriteEnd();
        }

        public void WriteIntroduction()
        {
            WriteToStream("package com.logic.localization");
            WriteToStream("{{");
	        WriteToStream("\tpublic class LocalizationConstants");
	        WriteToStream("\t{{");
            WriteToStream("\t\tprivate static var localizationConstants:LocalizationConstants;");
        }
            
        public void WriteBody()
        {
            foreach (string key in this.dictionary.Keys)
	        {
    		    List<GlobalizationItem> items = this.dictionary[key];
                WriteToStream("\t\t/* {0} */", key);

                foreach (GlobalizationItem item in items)
	            {
                    WriteToStream("\t\t[Bindable]");
		            WriteToStream("\t\tpublic var {0}:String = \"{1}\";", item.VariableName, item.Value);
            	}

                WriteToStream("");
	        }
        }

        public void WriteEnd()
        {
		    WriteToStream("\t\tpublic static function getInstance() : LocalizationConstants");
		    WriteToStream("\t\t{{");
			WriteToStream("\t\tif ( localizationConstants == null )");
			WriteToStream("\t\t\tlocalizationConstants = new LocalizationConstants( arguments.callee );");
			WriteToStream("\t\t\treturn localizationConstants;");
		    WriteToStream("\t\t}}");
		    WriteToStream("");
		    WriteToStream("\t\t//NOTE: AS3 does not allow for private or protected constructors");
		    WriteToStream("\t\tpublic function LocalizationConstants( caller : Function = null )");
		    WriteToStream("\t\t{{");
			WriteToStream("\t\tif( caller != LocalizationConstants.getInstance )");
		    WriteToStream("\t\t\tthrow new Error (\"Singleton is a singleton class, use getInstance() instead\");");
            WriteToStream("");
			WriteToStream("\t\tif ( LocalizationConstants.localizationConstants != null )");
			WriteToStream("\t\t\tthrow new Error( \"Only one Singleton instance should be instantiated\" );");
		    WriteToStream("\t\t}}");
	        WriteToStream("\t}}");
            WriteToStream("}}");
        }

		private void WriteToStream(string message, params object[] args)
		{
			this.stream.WriteLine(string.Format(message, args));
		}
    }
}
