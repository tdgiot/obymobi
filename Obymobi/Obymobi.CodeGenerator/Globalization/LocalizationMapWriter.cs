﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Dionysos;

namespace Obymobi.CodeGenerator
{
    public class LocalizationMapWriter
    {
        private StreamWriter stream;
        private Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> dictionary;

        public LocalizationMapWriter(StreamWriter stream, Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> dictionary)
        {
            this.stream = stream;
            this.dictionary = dictionary;
        }

        public void Write()
        {
            this.WriteIntroduction();
            this.WriteBody();
            this.WriteEnd();
        }

        public void WriteIntroduction()
        {
            WriteToStream("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            WriteToStream("<LocaleMap 	xmlns=\"http://com.asfusion.mate/l10n\" xmlns:mx=\"http://www.adobe.com/2006/mxml\" >");
	        WriteToStream("\t<mx:Script>");
		    WriteToStream("\t\t<![CDATA[");
            WriteToStream("\t\t\timport com.logic.localization.LocalizationConstants;");
            WriteToStream("\t\t\timport com.ui.components.*;");
			WriteToStream("\t\t\timport com.ui.views.base.*;");
			WriteToStream("\t\t\timport com.ui.views.modals.*;");
			WriteToStream("\t\t\timport com.data.obymobi.entityclasses.*;");
			WriteToStream("\t\t\timport com.logic.managers.*;");
			WriteToStream("\t\t\timport com.ui.views.*;");
            WriteToStream("");
            WriteToStream("\t\t\t[Bindable]");
			WriteToStream("\t\t\tprivate var localizationConstants:LocalizationConstants = LocalizationConstants.getInstance();");
            WriteToStream("\t\t]]>");
	        WriteToStream("\t</mx:Script>");
            WriteToStream("");
        }
            
        public void WriteBody()
        {
            foreach (string key in this.dictionary.Keys)
	        {
    		    List<GlobalizationItem> items = this.dictionary[key];
                WriteToStream("\t<SmartResourceInjector bundleName=\"otoucho\" target=\"{{localizationConstants}}\" >", key);

                foreach (GlobalizationItem item in items)
	            {
		            WriteToStream("\t\t<ResourceProxy property=\"{0}\" 	key=\"{1}.{0}\" />", item.VariableName, key);
            	}

                WriteToStream("\t</SmartResourceInjector>");
                WriteToStream("");
	        }
        }

        public void WriteEnd()
        {
            WriteToStream("</LocaleMap>");
        }

		private void WriteToStream(string message, params object[] args)
		{
			this.stream.WriteLine(string.Format(message, args));
		}
    }
}
