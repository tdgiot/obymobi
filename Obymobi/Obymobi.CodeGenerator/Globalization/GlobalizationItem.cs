﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.CodeGenerator
{
    public class GlobalizationItem
    {
        public GlobalizationItem(string variableName, string value)
        {
            this.VariableName = variableName;
            this.Value = value;
        }

        public string VariableName { get; set; }

        public string Value { get; set; }
    }
}
