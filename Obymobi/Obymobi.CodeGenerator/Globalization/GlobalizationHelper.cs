﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace Obymobi.CodeGenerator
{
    public class GlobalizationHelper
    {
        public static List<string> GetFiles(string path)
        {
            List<string> toReturn = new List<string>();

            string[] files = Directory.GetFiles(path, "*.*", SearchOption.AllDirectories);
            foreach (string file in files)
            {
                if (file.EndsWith(".mxml", StringComparison.InvariantCultureIgnoreCase) ||
                    file.EndsWith(".as", StringComparison.InvariantCultureIgnoreCase))
                {
                    toReturn.Add(file);
                }
            }

            return toReturn;
        }

        public static Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> GetLocalizationDictionary()
        {
            Regex regex = new Regex("public var text.+:String = \".+\";");

            Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> dictionary = new Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>>();

            List<string> variableNames = new List<string>();

            List<string> files = GlobalizationHelper.GetFiles(@"D:\Development\Flex\Otoucho\OtouchO.Application\src\com");
            foreach (string filePath in files)
            {
                string fileContent = File.ReadAllText(filePath);
                string fileName = Path.GetFileNameWithoutExtension(filePath);

                List<GlobalizationItem> globalizationItems = new List<GlobalizationItem>();

                MatchCollection matches = regex.Matches(fileContent);
                foreach (Match match in matches)
                {
                    string value = match.Value;
                    value = value.Replace("public var ", string.Empty);

                    string variableName = value.Substring(0, value.IndexOf(":"));
                    string variableValue = value.Substring(value.IndexOf("\""));
                    variableValue = variableValue.Replace(";", string.Empty);
                    variableValue = variableValue.Replace("\"", string.Empty);

                    if (!variableNames.Contains(variableName))
                    {
                        variableNames.Add(variableName);

                        GlobalizationItem item = new GlobalizationItem(variableName, variableValue);
                        globalizationItems.Add(item);
                    }
                }

                if (globalizationItems.Count > 0)
                    dictionary.Add(fileName, globalizationItems);
            }

            return dictionary;
        }

        public static void WriteLocalizationMap()
        {
            Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> dictionary = GetLocalizationDictionary();

            StreamWriter stream = new StreamWriter(@"D:\Development\Flex\Otoucho\OtouchO.Application\src\com\maps\LocalizationMap.mxml", false, Encoding.UTF8);

            LocalizationMapWriter mapWriter = new LocalizationMapWriter(stream, dictionary);
            mapWriter.Write();

            stream.Flush();
            stream.Close();
        }

        public static void WriteLocalizationConstants()
        {
            Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> dictionary = GetLocalizationDictionary();

            StreamWriter stream = new StreamWriter(@"D:\Development\Flex\Otoucho\OtouchO.Application\src\com\logic\localization\LocalizationConstants.as", false, Encoding.UTF8);

            LocalizationConstantsWriter mapWriter = new LocalizationConstantsWriter(stream, dictionary);
            mapWriter.Write();

            stream.Flush();
            stream.Close();
        }

        public static void WriteLocaleFile()
        {
            Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> dictionary = GetLocalizationDictionary();

            int max = 0;
            foreach (string key in dictionary.Keys)
            {
                List<GlobalizationItem> items = dictionary[key];
                foreach (GlobalizationItem item in items)
                {
                    if ((key.Length + item.VariableName.Length) > max)
                        max = key.Length + item.VariableName.Length;
                }
            }

            string[] lines = File.ReadAllLines(@"D:\Development\Flex\Otoucho\OtouchO.Application\locale\en_US\otoucho.properties");

            StreamWriter stream = new StreamWriter(@"D:\Development\Flex\Otoucho\OtouchO.Application\locale\nl_NL\otoucho.properties", false, Encoding.UTF8);

            LocaleFileWriter localeWriter = new LocaleFileWriter(stream, dictionary, max, lines);
            localeWriter.Write();

            stream.Flush();
            stream.Close();
        }

        public static Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> LoadLocaleFile(string path)
        {
            Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> dictionary = new Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>>();

            if (File.Exists(path))
            {
                string[] fileLines = File.ReadAllLines(path);

                List<GlobalizationItem> items = null;
                string key = string.Empty;

                foreach (string line in fileLines)
                {
                    if (line.StartsWith("# "))
                    {
                        items = new List<GlobalizationItem>();
                        key = line.Substring(2);
                    }
                    else if (string.IsNullOrEmpty(line))
                    {
                        dictionary.Add(key, items);
                    }
                    else
                    {
                        string value = line;
                        value = value.Replace(string.Format("{0}.", key), string.Empty);

                        string[] parts = value.Split('=');
                        if (parts.Length == 2)
                        {
                            string variableName = parts[0].Trim();
                            string variableValue = parts[1].Trim();

                            GlobalizationItem item = new GlobalizationItem(variableName, variableValue);
                            items.Add(item);
                        }
                    }
                }
            }

            return dictionary;
        }

        public static void ReplaceLanguage(string localeFilePath)
        {
            Dionysos.Collections.OrderedDictionary<string, List<GlobalizationItem>> dictionary = LoadLocaleFile(localeFilePath);

            Regex regex = new Regex("public var text.+:String = \".+\";");

            List<string> files = GlobalizationHelper.GetFiles(@"D:\Development\Flex\Otoucho\OtouchO.Application\src\com");
            foreach (string filePath in files)
            {
                string fileContent = File.ReadAllText(filePath);
                string fileName = Path.GetFileNameWithoutExtension(filePath);

                foreach (string key in dictionary.Keys)
                {
                    if (key == fileName)
                    {
                        List<GlobalizationItem> items = dictionary[key];

                        MatchCollection matches = regex.Matches(fileContent);
                        foreach (Match match in matches)
                        {
                            string value = match.Value;
                            value = value.Replace("public var ", string.Empty);
                            string variableName = value.Substring(0, value.IndexOf(":"));

                            foreach (GlobalizationItem item in items)
                            {
                                if (item.VariableName == variableName)
                                {
                                    fileContent = fileContent.Replace(match.Value, string.Format("public var {0}:String = \"{1}\";", item.VariableName, item.Value)); 
                                    break;
                                }
                            }
                        }

                        break;
                    }
                }

                File.WriteAllText(filePath, fileContent);
            }
        }
    }
}
