﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using Obymobi.CodeGenerator.J2ME;
using Obymobi.CodeGenerator.Flex;
using Obymobi.CodeGenerator.Android;
using Obymobi.CodeGenerator.iOS;
using Obymobi.CodeGenerator.Xamarin;

namespace Obymobi.CodeGenerator
{
    public partial class MainForm : System.Windows.Forms.Form
    {
        //private VectronConnector vectronConnector = new VectronConnector();

        public MainForm()
        {
            InitializeComponent();
            this.btGenerate.Click += new EventHandler(btGenerate_Click);
            this.btLocalization.Click += new EventHandler(btLocalization_Click);
            this.btUncommentTracing.Click += new EventHandler(btUncommentTracing_Click);
            this.btCommentTracing.Click += new EventHandler(btCommentTracing_Click);
        }

        void btCommentTracing_Click(object sender, EventArgs e)
        {
            this.CommentAllTracing();
        }

        void btUncommentTracing_Click(object sender, EventArgs e)
        {
            this.UncommentAllTracing();
        }

        void btLocalization_Click(object sender, EventArgs e)
        {
            #region Old crap
            //Posorder posorder = new Posorder();
            //posorder.PosdeliverypointExternalId = "1";

            //Posorderitem posorderitem = new Posorderitem();
            //posorderitem.PosproductExternalId = "1";
            //posorderitem.Quantity = 1;

            //Posorderitem posorderitem2 = new Posorderitem();
            //posorderitem2.PosproductExternalId = "2";
            //posorderitem2.Quantity = 2;

            //posorder.Posorderitems = new Posorderitem[] { posorderitem, posorderitem2 };

            //VectronConnector vectronConnector = new VectronConnector();
            //vectronConnector.SaveOrder(posorder);
            //vectronConnector = null;

            //PolishLocalizationHelper p = new PolishLocalizationHelper(@"D:\Development\Java\obymobi\Obymobi.StandardApplication\src\",
            //                                                            @"D:\Development\Java\obymobi\Obymobi.StandardApplication\resources\base\");
            #endregion

            #region Index
            //Obymobi.Logic.POS.Aloha.AlohaConnector aloha = new Obymobi.Logic.POS.Aloha.AlohaConnector();
            ////Poscategory[] categories = aloha.GetPoscategories();
            ////Posproduct[] products = aloha.GetPosproducts();
            ////Posdeliverypoint[] deliverypoints = aloha.GetPosdeliverypoints();
            ////Posdeliverypointgroup[] deliverypointgroups = aloha.GetPosdeliverypointgroups();
            //if (aloha.Login(98, 103))
            //{
            //    if (aloha.ClockIn(98, 10))
            //    {
            //        if (aloha.SaveOrder(null))
            //        {
            //            if (aloha.ClockOut(98, 0))
            //            {
            //                if (aloha.Logout(98))
            //                {
            //                    MessageBox.Show("Logout failed");
            //                }
            //            }
            //            else
            //            {
            //                MessageBox.Show("ClockOut failed");
            //            }
            //        }
            //        else
            //        {
            //            MessageBox.Show("SaveOrder failed");
            //        }
            //    }
            //    else
            //    {
            //        MessageBox.Show("Clockin failed");
            //    }
            //}
            //else
            //{
            //    MessageBox.Show("Login failed");
            //}
            #endregion

            //GlobalizationHelper.WriteLocalizationMap();
            //GlobalizationHelper.WriteLocalizationConstants();
            GlobalizationHelper.WriteLocaleFile();

            //GlobalizationHelper.ReplaceLanguage(@"D:\Development\Flex\Otoucho\OtouchO.Application\locale\en_US\otoucho.properties");
        }

        void btGenerate_Click(object sender, EventArgs e)
        {
            Generator gen = new Generator(@"D:\Development\Java\obymobi\Obymobi.Base\src\obymobi\data\", typeof(Obymobi.Logic.Model.Category));
            gen.Generate();
            MessageBox.Show("DONE!");
        }

        private void CommentAllTracing()
        {
            // Retrieve srouce
            List<string> sourceFiles = new List<string>();
            PolishLocalizationHelper.loadFiles(@"D:\Development\Java\obymobi\Obymobi.StandardApplication\src\", "*.java", ref sourceFiles);
            PolishLocalizationHelper.loadFiles(@"D:\Development\Java\obymobi\Obymobi.Base\src\", "*.java", ref sourceFiles);

            int i = 0;
            foreach (var sourceFilePath in sourceFiles)
            {
                string fileContents = File.ReadAllText(sourceFilePath);
                System.Diagnostics.Debug.WriteLine("CommentAllTracing - " + sourceFilePath + string.Format("{0}/{1}", i, sourceFiles.Count));
                Regex regexFullName = new Regex(" obymobi.Trace.Write");
                Regex regexShortCut = new Regex(" Trace.Write");

                fileContents = regexFullName.Replace(fileContents, " //obymobi.Trace.Write");
                fileContents = regexShortCut.Replace(fileContents, " //Trace.Write");

                File.WriteAllText(sourceFilePath, fileContents);
                i++;
            }
            MessageBox.Show("DONE!");
        }

        private void UncommentAllTracing()
        {
            // Retrieve srouce
            List<string> sourceFiles = new List<string>();
            PolishLocalizationHelper.loadFiles(@"D:\Development\Java\obymobi\Obymobi.StandardApplication\src\", "*.java", ref sourceFiles);
            PolishLocalizationHelper.loadFiles(@"D:\Development\Java\obymobi\Obymobi.Base\src\", "*.java", ref sourceFiles);

            int i = 0;
            foreach (var sourceFilePath in sourceFiles)
            {
                string fileContents = File.ReadAllText(sourceFilePath);
                System.Diagnostics.Debug.WriteLine("UncommentAllTracing - " + sourceFilePath + string.Format("{0}/{1}", i, sourceFiles.Count));
                Regex regexFullName = new Regex(" //obymobi.Trace.Write");
                Regex regexShortCut = new Regex(" //Trace.Write");

                fileContents = regexFullName.Replace(fileContents, " obymobi.Trace.Write");
                fileContents = regexShortCut.Replace(fileContents, " Trace.Write");

                File.WriteAllText(sourceFilePath, fileContents);
                i++;
            }
            MessageBox.Show("DONE!");
        }

        private void btGenerateiPhoneCode_Click(object sender, EventArgs e)
        {
            iOSGenerator gen = new iOSGenerator(@"D:\Development\iOS\Obymobi\Classes\", typeof(Obymobi.Logic.Model.Category));
            gen.Generate();
            MessageBox.Show("DONE!");
        }

        private void btnGenerateFlexCode_Click(object sender, EventArgs e)
        {
            FlexGenerator gen = new FlexGenerator(@"D:\Development\Flex\OtouchO\OtouchO.Application\src\com\", typeof(Obymobi.Logic.Model.Category));
            gen.Generate();

            MessageBox.Show("DONE!");
        }

        private void btGenerateAndroid_Click(object sender, EventArgs e)
        {
			AndroidGenerator gen = new AndroidGenerator(@"D:\Development\Android\CraveBase\src\net\craveinteractive\cravebase", typeof(Obymobi.Logic.Model.Category));
            gen.Generate();

            MessageBox.Show("ARIBA!");
        }

        private void btGenerateXamarin_Click(object sender, EventArgs e)
        {
            XamarinGenerator gen = new XamarinGenerator(@"D:\Development\Xamarin.new_api\Obymobi.Mobile", typeof(Obymobi.Logic.Model.Mobile.Category));
            gen.Generate();

            MessageBox.Show("OH JA JOH!");
        }

    }
}
