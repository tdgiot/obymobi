﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Xamarin
{
    public class ModelToEntityWriter
    {
        private Object model;
        private Type modelType;
        private StreamWriter stream;
        private PropertyInfo[] fields;

        public ModelToEntityWriter(Object model, StreamWriter stream)
        {
            this.model = model;
            this.modelType = model.GetType();
            this.stream = stream;

            PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
            List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
            for (int j = 0; j < propertyInfo.Length; j++)
            {
                PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForXamarin
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForXamarin), false).Length > 0)
                {
                    propertyInfoList.Add(property);
                }
            }

            this.fields = propertyInfoList.ToArray();
        }

        public void WriteEntity()
        {
            this.WriteIntroduction();
            this.WriteClassEnd();
        }

        private void WriteIntroduction()
        {
            WriteToStream("using System;");
            WriteToStream("using {0}.Data.EntityBaseClasses;", XamarinGenerator.BaseNamespace);
            WriteToStream("using {0}.Data.CollectionClasses;", XamarinGenerator.BaseNamespace);
            WriteToStream("");
            WriteToStream("namespace {0}.Data.EntityClasses", XamarinGenerator.BaseNamespace);
            WriteToStream("{{");
            WriteToStream("\t[Serializable]");
            WriteToStream("\tpublic class {0}Entity : {0}EntityBase", this.modelType.Name);
            WriteToStream("\t{{");
            WriteToStream("\t\t#region Constructors");
            WriteToStream("");
            WriteToStream("\t\tpublic {0}Entity()", this.modelType.Name);
            WriteToStream("\t\t{{");
            WriteToStream("\t\t}}");
            WriteToStream("");
            WriteToStream("\t\t#endregion");
        }

        private void WriteClassEnd()
        {
            WriteToStream("\t}}");
            WriteToStream("}}");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
