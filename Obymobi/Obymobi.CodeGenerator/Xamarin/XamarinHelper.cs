﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.CodeGenerator.Xamarin
{
    public class XamarinHelper
    {
        public static string GetDataType(Type dotNetType)
        {
            string type = string.Empty;

            string dotNetTypeString = dotNetType.ToString();
            bool nullable = false;
            if(dotNetTypeString.StartsWith("System.Nullable`1"))
            {
                nullable = true;
                dotNetTypeString = dotNetTypeString.Replace("System.Nullable`1[", string.Empty);
                dotNetTypeString = dotNetTypeString.Substring(0, dotNetTypeString.Length - 1);
            }

            switch (dotNetTypeString)
            {
                case "System.Boolean":
                    type = "bool";
                    break;
                case "System.String":
                    type = "string";
                    break;
                case "System.Int32":
                case "System.UInt32":
                    type = "int";
                    break;
                case "System.Int16":
                    type = "short";
                    break;
                case "System.Int32[]":
                    type = "int[]";
                    break;
                case "System.Int64":
                    type = "long";
                    break;
                case "System.Decimal":
                    type = "decimal";
                    break;
                case "System.Double":
                    type = "double";
                    break;
                case "System.Single":
                    type = "float";
                    break;
                case "System.DateTime":
                    type = "DateTime";
                    break;              
            }

            if (nullable)
                type += "?";

            if (type == string.Empty)
            {
                if (dotNetType.Name.Contains("[]"))
                    type = string.Format("{0}Collection", dotNetType.Name.Replace("[]", string.Empty));
                else
                    type = string.Format("{0}Entity", dotNetType.Name);
            }

            return type;
        }

        public static bool IsPrimitiveDataType(Type dotNetType)
        {
            bool isPrimitive = true;

            string dataType = GetDataType(dotNetType);

            if (dataType.Contains("?"))
                dataType = dataType.Substring(0, dataType.Length - 1);

            if (dataType != "bool" && dataType != "string" && dataType != "int" &&
                dataType != "short" && dataType != "long" && dataType != "double" && dataType != "double?" && dataType != "decimal" &&
                dataType != "float" && dataType != "DateTime" && dataType != "DateTime?" && dataType != "int[]")
                isPrimitive = false;

            return isPrimitive;
        }
    }
}
