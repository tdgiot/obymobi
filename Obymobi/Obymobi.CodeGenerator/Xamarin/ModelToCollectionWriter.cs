﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Dionysos;
using System.Reflection;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Xamarin
{
    public class ModelToCollectionWriter
    {
        private Object model;
        private Type modelType;
        private StreamWriter stream;
        private List<PropertyInfo> fkFields = new List<PropertyInfo>();
        private PropertyInfo pkField = null;

        public ModelToCollectionWriter(Object model, StreamWriter stream)
        {
            this.model = model;
            this.modelType = model.GetType();

            PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
            for (int j = 0; j < propertyInfo.Length; j++)
            {
                PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForXamarin
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForXamarin), false).Length > 0)
                {
                    if (property.GetCustomAttributes(typeof(ForeignKeyFieldOfModel), false).Length > 0)
                        this.fkFields.Add(property);
                    else if (property.GetCustomAttributes(typeof(PrimaryKeyFieldOfModel), false).Length > 0)
                    {
                        if (this.pkField != null)
                            throw new Dionysos.TechnicalException("Multiple PK fields found on Model {0}", this.modelType.FullName);
                        else
                            this.pkField = property;
                    }
                }
            }

            this.stream = stream;
        }

        public void WriteCollection()
        {
            this.WriteIntroduction();
            //this.WriteIEntityCollectionImplementation();
            this.WriteClassEnd();
        }

        private void WriteIntroduction()
        {
            WriteToStream("using System;");
            WriteToStream("using Obymobi.Mobile.Data.SupportClasses;");
            WriteToStream("using Obymobi.Mobile.Data.EntityClasses;");
            WriteToStream("");
            WriteToStream("namespace {0}.Data.CollectionClasses", XamarinGenerator.BaseNamespace);
            WriteToStream("{{");
            WriteToStream("\t[Serializable]");
            WriteToStream("\tpublic class {0}Collection : EntityCollection<{0}Entity>", this.modelType.Name);
            WriteToStream("\t{{");
            WriteToStream("\t\t#region Constructors");
            WriteToStream("");
            WriteToStream("\t\tpublic {0}Collection()", this.modelType.Name);
            WriteToStream("\t\t{{");
            WriteToStream("\t\t}}");
            WriteToStream("");
            WriteToStream("\t\t#endregion");
        }

        private void WriteIEntityCollectionImplementation()
        {
            WriteToStream("\t// ------------------  METHODS ------------------");
            WriteToStream("");

            // Set
            WriteToStream("\tpublic {0}Entity set(int index, {1}Entity item)", this.modelType.Name, this.modelType.Name);
            WriteToStream("\t{{");
            WriteToStream("\t\treturn super.set(index, item);");
            WriteToStream("\t}}");
            WriteToStream("");

            // Add
            WriteToStream("\tpublic boolean add({0}Entity item)", this.modelType.Name);
            WriteToStream("\t{{");
            WriteToStream("\t\treturn super.add(item);");
            WriteToStream("\t}}");

            // Get
            WriteToStream("\tpublic {0}Entity get(int index)", this.modelType.Name);
            WriteToStream("\t{{");
            WriteToStream("\t\treturn super.get(index);", this.modelType.Name);
            WriteToStream("\t}}");

            // IndexOf
            WriteToStream("\tpublic int indexOf({0}Entity item)", this.modelType.Name);
            WriteToStream("\t{{");
            WriteToStream("\t\treturn super.indexOf(item);");
            WriteToStream("\t}}");

            //// Constant for EntityName
            WriteToStream("\t\tpublic String getModelName()");
            WriteToStream("\t\t{{");
            WriteToStream("\t\t\treturn \"{0}\";", this.modelType.Name);
            WriteToStream("\t\t}}");
            WriteToStream("");

            // Collection to XML
            WriteToStream("\t@Override");
            WriteToStream("\tpublic String toXml()");
            WriteToStream("\t{{");
            WriteToStream("\t\tString xml = \"\";");
            //WriteToStream("\t\t\txml += xmlhelper.createXmlOpeningTag(\"ArrayOf\" + this.getModelName());");
            WriteToStream("\t\tfor (int i = 0; i < this.size(); i++)");
            WriteToStream("\t\t{{");
            WriteToStream("\t\t\t{0}Entity {1}Entity = this.get(i);", this.modelType.Name.TurnFirstToUpper(false), this.modelType.Name.TurnFirstToLower(false), this.modelType.Name);
            WriteToStream("\t\t\txml += {0}Entity.toXml();", this.modelType.Name.TurnFirstToLower(false));
            WriteToStream("\t\t}}");
            //WriteToStream("\t\t\txml += xmlhelper.createXmlClosingTag(\"ArrayOf\" + this.getModelName());");
            WriteToStream("\t\treturn xml;");
            WriteToStream("\t}}");
            WriteToStream("");

            // Collection to Json
            WriteToStream("\t@Override");
            WriteToStream("\tpublic String toJson()");
            WriteToStream("\t{{");
            WriteToStream("\t\tString json = \"\";");
            WriteToStream("\t\tfor (int i = 0; i < this.size(); i++)");
            WriteToStream("\t\t{{");
            WriteToStream("\t\t\t{0}Entity {1}Entity = this.get(i);", this.modelType.Name.TurnFirstToUpper(false), this.modelType.Name.TurnFirstToLower(false), this.modelType.Name);
            WriteToStream("\t\t\tjson += {0}Entity.toJson();", this.modelType.Name.TurnFirstToLower(false));
            WriteToStream("\t\t}}");
            WriteToStream("\t\treturn json;");
            WriteToStream("\t}}");
            WriteToStream("");

            //WriteToStream("\t\tpublic override function toString():String");
            //WriteToStream("\t\t{{");
            //WriteToStream("\t\t\treturn toXml();");
            //WriteToStream("\t\t}}");
        }

        private void WriteClassEnd()
        {
            WriteToStream("\t}}");
            WriteToStream("}}");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
