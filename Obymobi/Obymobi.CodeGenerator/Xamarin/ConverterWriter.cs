﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Xamarin
{
    public class ConverterWriter
    {
        private Object model;
        private Type modelType;
        private StreamWriter stream;
        private PropertyInfo[] fields;

        public ConverterWriter(Object model, StreamWriter stream)
        {
            this.model = model;
            this.modelType = model.GetType();
            this.stream = stream;

            PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
            List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
            for (int j = 0; j < propertyInfo.Length; j++)
            {
                PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForXamarin
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForXamarin), false).Length > 0)
                    propertyInfoList.Add(property);
            }

            this.fields = propertyInfoList.ToArray();
        }

        public void WriteEntity()
        {
            this.WriteIntroduction();
            this.WriteMethods();
            //this.WriteFromAndToDataStreamLogic();
            //this.WriteXmlLogic();
            //this.WriteCloneLogic();
            //this.WriteGenericPropertyAccessor();
            //this.WriteAdditionalLogic();
            this.WriteClassEnd();
        }

        private void WriteIntroduction()
        {
            WriteToStream("using {0}.Data.EntityClasses;", XamarinGenerator.BaseNamespace);
            WriteToStream("using {0}.Data.CollectionClasses;", XamarinGenerator.BaseNamespace);
            WriteToStream("using {0}.Net.SupportClasses;", XamarinGenerator.BaseNamespace);
            WriteToStream("");
            WriteToStream("namespace {0}.Net.ConverterClasses", XamarinGenerator.BaseNamespace);
            WriteToStream("{{");
            WriteToStream("\tpublic class {0}Converter : ModelConverterBase<MobileService.{0}, {0}Entity, {0}Collection>", this.modelType.Name);
            WriteToStream("\t{{");
        }

        private void WriteMethods()
        {
            WriteToStream("\t\tpublic override {0}Entity ConvertWebserviceModelToEntity(MobileService.{0} model)", this.modelType.Name);
            WriteToStream("\t\t{{");
            WriteToStream("\t\t\t{0}Entity {1}Entity = new {0}Entity();", this.modelType.Name, this.modelType.Name.TurnFirstToLower(false));

            foreach (var field in fields)
            {
                if (XamarinHelper.IsPrimitiveDataType(field.PropertyType))
                    WriteToStream("\t\t\t{0}Entity.{1} = model.{1};", this.modelType.Name.TurnFirstToLower(false), field.Name);
                else if (field.PropertyType.ToString().Contains("[]"))
                    WriteToStream("\t\t\t{0}Entity.{1} = new {2}Converter().ConvertWebserviceArrayToEntityCollection(model.{1});", this.modelType.Name.TurnFirstToLower(false), field.Name, XamarinHelper.GetDataType(field.PropertyType).Replace("Collection", string.Empty));
                else
                {
                    WriteToStream("\t\t\tif (model.{0} != null)", field.Name);
                    WriteToStream("\t\t\t\t{0}Entity.{1} = new {2}Converter().ConvertWebserviceModelToEntity(model.{1});", this.modelType.Name.TurnFirstToLower(false), field.Name, XamarinHelper.GetDataType(field.PropertyType).Replace("Entity", string.Empty));
                }
            }

            WriteToStream("");
            WriteToStream("\t\t\treturn {0}Entity;", this.modelType.Name.TurnFirstToLower(false));
            WriteToStream("\t\t}}");

            WriteToStream("");

            WriteToStream("\t\tpublic override MobileService.{0} ConvertEntityToWebserviceModel({0}Entity entity)", this.modelType.Name);
            WriteToStream("\t\t{{");
            WriteToStream("\t\t\tMobileService.{0} model = new MobileService.{0}();", this.modelType.Name);

            foreach (var field in fields)
            {
                if (XamarinHelper.IsPrimitiveDataType(field.PropertyType))
                    WriteToStream("\t\t\tmodel.{0} = entity.{0};", field.Name);
                else if (field.PropertyType.ToString().Contains("[]"))
                    WriteToStream("\t\t\tmodel.{0} = new {1}Converter().ConvertEntityCollectionToWebserviceArray(entity.{0});", field.Name, XamarinHelper.GetDataType(field.PropertyType).Replace("Collection", string.Empty));
                else
                {
                    WriteToStream("\t\t\tif (entity.{0} != null)", field.Name);
                    WriteToStream("\t\t\t\tmodel.{0} = new {1}Converter().ConvertEntityToWebserviceModel(entity.{0});", field.Name, XamarinHelper.GetDataType(field.PropertyType).Replace("Entity", string.Empty));
                }
            }

            WriteToStream("");
            WriteToStream("\t\t\treturn model;");
            WriteToStream("\t\t}}");
            WriteToStream("");
        }

        //private void WriteXmlLogic()
        //{
        //    WriteToStream("\t// ===========================================================");
        //    WriteToStream("\t// Methods");
        //    WriteToStream("\t// ===========================================================");
        //    WriteToStream("");

        //    #region ToXml
        //    WriteToStream("\tpublic String toXml()");
        //    WriteToStream("\t{{");
        //    WriteToStream("\t\tString xml = \"\";");
        //    WriteToStream("");
        //    WriteToStream("\t\txml += XmlHelper.createXmlOpeningTag(\"{0}\");", this.modelType.Name);
        //    WriteToStream("");

        //    // Write fields
        //    foreach (var field in fields)
        //    {
        //        if(!field.PropertyType.BaseType.Name.Equals("Array"))
        //            WriteToStream("\t\txml += XmlHelper.createXmlElement(\"{0}\", this.get{0}());", field.Name);
        //    }
        //    foreach (var field in fields)
        //    {
        //        if (field.PropertyType.BaseType.Name.Equals("Array"))
        //            WriteToStream("\t\txml += XmlHelper.createXmlElement(\"{0}\", this.get{0}());", field.Name);
        //    }

        //    WriteToStream("");
        //    WriteToStream("\t\txml += XmlHelper.createXmlClosingTag(\"{0}\");", this.modelType.Name);
        //    WriteToStream("");
        //    WriteToStream("\t\treturn xml;");
        //    WriteToStream("\t}}");
        //    #endregion
                        
        //    WriteToStream("");

        //    #region ToJson
        //    WriteToStream("\tpublic String toJson()");
        //    WriteToStream("\t{{");
        //    WriteToStream("\t\tString json = \"\";");
        //    WriteToStream("");
        //    WriteToStream("\t\tjson += JsonHelper.createJsonOpeningTag();");
        //    WriteToStream("");

        //    bool last = false;

        //    // Write fields
        //    for (int i = 0; i < this.fields.Length; i++)
        //    {
        //        PropertyInfo field = this.fields[i];
        //        last = (i == (this.fields.Length - 1));
        //        //if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
        //        WriteToStream("\t\tjson += JsonHelper.createJsonElement(\"{0}\", this.get{2}(), {1});", field.Name.TurnFirstToLower(false), last.ToString().ToLower(), field.Name);
        //    }

        //    WriteToStream("");
        //    WriteToStream("\t\tjson += JsonHelper.createJsonClosingTag();");
        //    WriteToStream("");
        //    WriteToStream("\t\treturn json;");
        //    WriteToStream("\t}}");
        //    #endregion

        //    WriteToStream("");
        //}

        //public void WriteCloneLogic()
        //{
        //    WriteToStream("\t\t// ------------------  CLONE LOGIC ------------------");
        //    WriteToStream("");

        //    WriteToStream("\t\tpublic {0}Entity clone()", this.modelType.Name);
        //    WriteToStream("\t\t{{");
        //    WriteToStream("\t\t	{0}Entity clone = new {0}Entity();", this.modelType.Name);

        //    // Clone fields
        //    foreach (var field in fields)
        //    {
        //        //if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
        //        string typeName = AndroidHelper.GetAndroidDataType(field.PropertyType);
        //        if (typeName.Contains("Collection"))
        //        {
        //            string entityBaseType = typeName.Replace("Collection", "EntityBase");
        //            string entityType = typeName.Replace("Collection", "Entity");

        //            // Clone Collection
        //            WriteToStream("\t		for(Object source : this.get{0}())", field.Name);
        //            WriteToStream("\t		{{");
        //            //WriteToStream("\t		    @SuppressWarnings(\"unused\")");
        //            WriteToStream("\t		    {0} source{0} = ({0})source;", entityType);
        //            WriteToStream("\t		    {0} clone{0} = source{0}.clone();", entityType);
        //            WriteToStream("\t		    clone.get{0}().add(clone{1});", field.Name, entityType);
        //            WriteToStream("\t		}}");
        //        }
        //        else
        //        {
        //            WriteToStream("\t		clone.set{0}(this.get{0}());", field.Name);
        //        }
        //    }

        //    WriteToStream("\t		return clone;");

        //    WriteToStream("\t\t}}");
        //    WriteToStream("");

        //}

        //public void WriteGenericPropertyAccessor()
        //{
        //    WriteToStream("	// Generic Property Accessor");
        //    WriteToStream("	// <editor-fold>");
        //    WriteToStream("");

        //    #region SetPropery for values of type Object
        //    WriteToStream("	public void setProperty(String propertyName, Object value)");
        //    WriteToStream("	{{");
        //    WriteToStream("");

        //    bool firstIfClause = true;
        //    foreach (var field in fields)
        //    {
        //        if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
        //        {
        //            if (firstIfClause)
        //            {
        //                WriteToStream("		if(propertyName.compareTo(\"{0}\") == 0)", field.Name);
        //                firstIfClause = false;
        //            }
        //            else
        //                WriteToStream("		else if(propertyName.compareTo(\"{0}\") == 0)", field.Name);

        //            if (field.PropertyType.ToString() == typeof(Int32).ToString())
        //            {
        //                WriteToStream("			this.{0} = Integer.parseInt(value.toString());", field.Name.TurnFirstToLower(false));
        //            }
        //            else if (field.PropertyType.ToString() == typeof(String).ToString())
        //            {
        //                WriteToStream("			this.{0} = value.toString();", field.Name.TurnFirstToLower(false));
        //            }
        //            else
        //            {
        //                // Nohting.
        //            }
        //        }
        //    }

        //    // End switch & function
        //    WriteToStream("	}}");
        //    WriteToStream("");
        //    #endregion

        //    #region SetPropery for values of type int
        //    WriteToStream("	public void setProperty(String propertyName, int value)");
        //    WriteToStream("	{{");
        //    WriteToStream("");
        //    firstIfClause = true;
        //    foreach (var field in fields)
        //    {
        //        if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
        //        {
        //            if (firstIfClause)
        //            {
        //                WriteToStream("		if(propertyName.compareTo(\"{0}\") == 0)", field.Name);
        //                firstIfClause = false;
        //            }
        //            else
        //                WriteToStream("		else if(propertyName.compareTo(\"{0}\") == 0)", field.Name);

        //            if (field.PropertyType.ToString() == typeof(Int32).ToString())
        //            {
        //                WriteToStream("			this.{0} = value;", field.Name.TurnFirstToLower(false));
        //            }
        //            else if (field.PropertyType.ToString() == typeof(String).ToString())
        //            {
        //                WriteToStream("			this.{0} = Integer.toString(value);", field.Name.TurnFirstToLower(false));
        //            }
        //        }
        //    }

        //    // End switch & function
        //    WriteToStream("	}}");
        //    #endregion

        //    WriteToStream("");
        //    WriteToStream("	// </editor-fold>");
        //}

        //private void WriteAdditionalLogic()
        //{
        //    WriteToStream("	// Entity Base logic");
        //    WriteToStream("	// <editor-fold>");
        //    WriteToStream("");

        //    // Get Entity Instance
        //    WriteToStream("	public IEntity getNewEmptyInstance()");
        //    WriteToStream("	{{");
        //    WriteToStream("		return new {0}Entity();", this.modelType.Name);
        //    WriteToStream("	}}");
        //    WriteToStream("");

        //    // Get CollectionInstance
        //    WriteToStream("	public EntityCollectionBase getNewEmptyEntityCollection()");
        //    WriteToStream("	{{");
        //    WriteToStream("		return new {0}Collection();", this.modelType.Name);
        //    WriteToStream("	}}");
        //    WriteToStream("");

        //    // Unique EntityName
        //    WriteToStream("	public String uniqueEntityName()");
        //    WriteToStream("	{{");
        //    WriteToStream("		return \"{0}Entity\";", this.modelType.Name);
        //    WriteToStream("	}}");
        //    WriteToStream("");

        //    // Abstract methods for PK fields
        //    WriteToStream("	public abstract int getPkFieldValue();");
        //    WriteToStream("	public abstract void setPkFieldValue(int pkValue);");

        //    WriteToStream("");
        //    WriteToStream("	// </editor-fold>");

        //    // Clone object
        //}

        private void WriteClassEnd()
        {
            WriteToStream("\t}}");
            WriteToStream("}}");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
