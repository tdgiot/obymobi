﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Flex
{
	public class ModelToSQLiteTableWriter
	{
		private Object model;
		private Type modelType;
		private StreamWriter stream;
		private PropertyInfo[] fields;

		public ModelToSQLiteTableWriter(Object model, StreamWriter stream)
		{
			this.model = model;
			this.modelType = model.GetType();
			this.stream = stream;			
			
			PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
			List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
			for (int j = 0; j < propertyInfo.Length; j++)
			{
				PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForMobile
				if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForMobile), false).Length > 0)
				{
					propertyInfoList.Add(property);
				}
			}

			this.fields = propertyInfoList.ToArray();
		}

		public void WriteEntity()
		{
			this.WriteIntroduction();
			this.WriteClassStart();
            this.WriteFields();
            this.WriteProperties();
			this.WriteClassEnd();
		}

		private void WriteIntroduction()
		{
			WriteToStream("/*");
            WriteToStream(" * {0}EntityBase.as", this.modelType.Name);
			WriteToStream(" * Generated entity base class for {0}", this.modelType.FullName);
			WriteToStream(" */");
			WriteToStream("");
            WriteToStream("package com.data.obymobi.entitybaseclasses");
			WriteToStream("{{");
            WriteToStream("\timport com.logic.helpers.xmlhelper;");
			WriteToStream("\timport com.data.obymobi.collectionclasses.*;");//import flash.events.EventDispatcher;
			WriteToStream("\timport flash.events.EventDispatcher;");
            WriteToStream("");            	
		}

		private void WriteClassStart()
		{
			WriteToStream("\tpublic class {0}EntityBase extends EventDispatcher", this.modelType.Name);
            WriteToStream("\t{{");
            WriteToStream("\t\t// ------------------  CONSTRUCTORS ------------------");
            WriteToStream("\t\t/**");
            WriteToStream("\t\t* Constructor, initializes the type class");
            WriteToStream("\t\t*/");
			WriteToStream("\t\tpublic function {0}EntityBase()", this.modelType.Name);
            WriteToStream("\t\t{{");
            WriteToStream("\t\t}}");
            WriteToStream("");
		}

		private void WriteFields()
		{
            WriteToStream("\t\t// ------------------  FIELDS ------------------");
            WriteToStream("");

			foreach (var field in fields)
			{
                string datatype = FlexHelper.GetFlexDataType(field.PropertyType);
                if (datatype == "Boolean")
            		WriteToStream("\t\tprivate var _{0}:{1} = false;", field.Name.TurnFirstToLower(false), datatype);
                else if (datatype == "String")
                    WriteToStream("\t\tprivate var _{0}:{1} = \"\";", field.Name.TurnFirstToLower(false), datatype);
                else if (datatype == "int" || datatype == "Number")
                    WriteToStream("\t\tprivate var _{0}:{1} = 0;", field.Name.TurnFirstToLower(false), datatype);
                else
                {
                    WriteToStream("\t\tprivate var _{0}:{1} = new {1}(true);", field.Name.TurnFirstToLower(false), datatype);
                }
			}

            WriteToStream("");
		}

		private void WriteProperties()
		{
            WriteToStream("\t\t// ------------------  PROPERTIES ------------------");
			WriteToStream("");			
			foreach (var field in fields)
			{
				string typeName = FlexHelper.GetFlexDataType(field.PropertyType);

				if (typeName.Length > 0)
				{
					// The Setter
					WriteToStream("\t\t[Bindable]");
					WriteToStream("\t\tpublic function set {0}(value:{1}):void", field.Name, typeName);
					WriteToStream("\t\t{{");
					WriteToStream("\t\t\tthis._{0} = value;", field.Name.TurnFirstToLower(false));
					WriteToStream("\t\t}}");

					WriteToStream("");

					// The Getter
					WriteToStream("\t\tpublic function get {0}():{1}", field.Name, typeName);
					WriteToStream("\t\t{{");
					WriteToStream("\t\t\treturn this._{0};", field.Name.TurnFirstToLower(false));
					WriteToStream("\t\t}}");
				}
				WriteToStream("");
			}
			WriteToStream("");
		}

		private void WriteClassEnd()
		{
			WriteToStream("\t}}");
            WriteToStream("}}");
		}

		private void WriteToStream(string message, params object[] args)
		{
			this.stream.WriteLine(string.Format(message, args));
		}
	}
}
