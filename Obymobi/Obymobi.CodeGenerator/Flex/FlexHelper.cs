﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.CodeGenerator.Flex
{
    public class FlexHelper
    {
        public static string GetFlexDataType(Type dotNetType)
        {
            string type = string.Empty;

            switch (dotNetType.ToString())
            {
                case "System.Boolean":
                    type = "Boolean";
                    break;
                case "System.String":
                    type = "String";
                    break;
                case "System.Int32":
                    type = "int";
                    break;
                case "System.Int16":
                case "System.Int64":
                case "System.Decimal":
                case "System.Double":
                case "System.Single":
                    type = "Number";
                    break;
            }

            if (type == string.Empty)
            {
                type = string.Format("{0}Collection", dotNetType.Name.Replace("[]", string.Empty));
            }

            if (type == "DateTimeCollection")
                type = "Date";
            else if (type == "Int32Collection")
                type = "String";

            return type;
        }
    }
}
