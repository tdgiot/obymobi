﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Flex
{
    public class WebserviceHandlerWriterClientLocalStorage
    {
        private StreamWriter stream;
        private MethodInfo method;
        private Type type;
        private Assembly modelAssembly;
        private PropertyInfo[] fields;
        private object model;
        private ParameterInfo[] parameters;

        public WebserviceHandlerWriterClientLocalStorage(StreamWriter stream, MethodInfo method, Assembly modelAssembly)
        {
            this.stream = stream;
            this.method = method;
            this.type = (this.method.ReturnType.GetGenericArguments().Length > 0 ? this.method.ReturnType.GetGenericArguments()[0] : this.method.ReturnType);
            this.modelAssembly = modelAssembly;

            this.model = Dionysos.InstanceFactory.CreateInstance(this.modelAssembly, type);

            PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(this.model);
            List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
            for (int j = 0; j < propertyInfo.Length; j++)
            {
                PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForFlex
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForFlex), false).Length > 0)
                {
                    propertyInfoList.Add(property);
                }
            }

            this.fields = propertyInfoList.ToArray();
            this.parameters = this.method.GetParameters();
        }

        public void Write()
        {
            this.WriteIntroduction();
            this.WriteMethods();
            this.WriteClassEnd();
        }

        private void WriteIntroduction()
        {
            this.WriteToStream("package com.net.obymobi.handlers");
            this.WriteToStream("{{");
            this.WriteToStream("\timport com.data.obymobi.collectionclasses.*;");
            this.WriteToStream("\timport com.data.obymobi.entityclasses.*;");
            this.WriteToStream("\timport com.logic.Global;");
            this.WriteToStream("\timport com.logic.events.*;");
            this.WriteToStream("\timport com.logic.helpers.FileHelper;");
            this.WriteToStream("\timport com.net.obymobi.WebserviceEvents;");
            this.WriteToStream("\timport com.net.obymobi.events.*;");
            this.WriteToStream("");
            this.WriteToStream("\timport flash.events.Event;");
            this.WriteToStream("\timport flash.events.HTTPStatusEvent;");
            this.WriteToStream("\timport flash.events.IEventDispatcher;");
            this.WriteToStream("\timport flash.events.IOErrorEvent;");
            this.WriteToStream("\timport flash.events.ProgressEvent;");
            this.WriteToStream("\timport flash.events.SecurityErrorEvent;");
            this.WriteToStream("\timport flash.filesystem.File;");
            this.WriteToStream("\timport flash.net.URLLoader;");
            this.WriteToStream("\timport flash.net.URLLoaderDataFormat;");
            this.WriteToStream("\timport flash.net.URLRequest;");
            this.WriteToStream("");
            this.WriteToStream("\tpublic class {0}Handler", this.method.Name);
            this.WriteToStream("\t{{");
            this.WriteToStream("\t\t// ------------------  FIELDS ------------------");
            this.WriteToStream("");
            this.WriteToStream("\t\tprivate var _loader:URLLoader;");
            this.WriteToStream("");
            this.WriteToStream("\t\t// ------------------  CONSTRUCTORS ------------------");
            this.WriteToStream("");
            this.WriteToStream("\t\tpublic function {0}Handler()", this.method.Name);
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t}}");
            this.WriteToStream("");
        }

        private void WriteMethods()
        {
            this.WriteToStream("\t\tpublic function save{0}(event:{0}Event):void", this.method.Name);
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\tif(Global.communicationIsOk && !Global.disconnectedMode)");
            this.WriteToStream("\t\t\t{{");
			this.WriteToStream("\t\t\t\tvar url:String = Global.currentWebserviceUrl + \"/\" + Global.API_VERSION + \"/CraveService.asmx/{0}\"", this.method.Name);
            this.WriteToStream("\t\t\t\turl += \"?\";");
            for (int i = 0; i < this.parameters.Length; i++)
            {
                this.WriteToStream("\t\t\t\turl += \"{0}=\" + event.{0}{1};", this.parameters[i].Name, (i != (this.parameters.Length-1) ? " + \"&\"" : ""));
            }
            this.WriteToStream("");
            this.WriteToStream("\t\t\t\tthis._loader = new URLLoader();");
            this.WriteToStream("\t\t\t\tthis._loader.dataFormat = URLLoaderDataFormat.TEXT;");
            this.WriteToStream("\t\t\t\tconfigureListeners(this._loader);");
            this.WriteToStream("");
            this.WriteToStream("\t\t\t\tvar request:URLRequest = new URLRequest(url);");
            this.WriteToStream("\t\t\t\ttry");
            this.WriteToStream("\t\t\t\t{{");
            this.WriteToStream("\t\t\t\t\tthis._loader.load(request);");
            this.WriteToStream("\t\t\t\t}}");
            this.WriteToStream("\t\t\t\tcatch (error:Error)");
            this.WriteToStream("\t\t\t\t{{");
            this.WriteToStream("\t\t\t\t\tApplicationEvent.AnnounceWebserviceCallError(\"{0} - Error\", error);", this.method.Name);
            this.WriteToStream("\t\t\t\t}}");
            this.WriteToStream("\t\t\t}}");
            this.WriteToStream("\t\t\telse");
            this.WriteToStream("\t\t\t{{");
            this.WriteToStream("\t\t\t\ttrace(\"{0}Handler.save{0}: Offline mode\");", this.method.Name);
            this.WriteToStream("\t\t\t\tvar ev:{0}ResultEvent = new {0}ResultEvent(WebserviceEvents.{0}Result);", this.method.Name);
            this.WriteToStream("\t\t\t\tGlobal.dispatcher.dispatchEvent(ev);");
            this.WriteToStream("\t\t\t}}");
            this.WriteToStream("\t\t}}");
            this.WriteToStream("");
            this.WriteToStream("\t\tprivate function configureListeners(dispatcher:IEventDispatcher):void");
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\tdispatcher.addEventListener(Event.COMPLETE, completeHandler);");
            this.WriteToStream("\t\t\tdispatcher.addEventListener(Event.OPEN, openHandler);");
            this.WriteToStream("\t\t\tdispatcher.addEventListener(ProgressEvent.PROGRESS, progressHandler);");
            this.WriteToStream("\t\t\tdispatcher.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);");
            this.WriteToStream("\t\t\tdispatcher.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);");
            this.WriteToStream("\t\t\tdispatcher.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);");
            this.WriteToStream("\t\t}}");
            this.WriteToStream("");
            this.WriteToStream("\t\tprivate function openHandler(event:Event):void");
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\ttrace(\"{0}Handler.openHandler: \" + event);", this.method.Name);
            this.WriteToStream("\t\t}}");
            this.WriteToStream("");
            this.WriteToStream("\t\tprivate function progressHandler(event:ProgressEvent):void");
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\tLoadingViewEvent.AnnounceUpdateProgressTwoConfig(0, event.bytesTotal, event.bytesLoaded, Global.dispatcher);");
            this.WriteToStream("\t\t}}");
            this.WriteToStream("");
            this.WriteToStream("\t\tprivate function securityErrorHandler(event:SecurityErrorEvent):void");
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\tApplicationEvent.AnnounceWebserviceCallSecurityError(\"{0} - SecurityError\", event);", this.method.Name);
            this.WriteToStream("\t\t}}");
            this.WriteToStream("");
            this.WriteToStream("\t\tprivate function httpStatusHandler(event:HTTPStatusEvent):void");
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\ttrace(\"{0}Handler.httpStatusHandler: \" + event);", this.method.Name);
            this.WriteToStream("\t\t}}");
            this.WriteToStream("");
            this.WriteToStream("\t\tprivate function ioErrorHandler(event:IOErrorEvent):void");
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\tApplicationEvent.AnnounceWebserviceCallIOError(\"{0} - IOError\", this._loader, event);", this.method.Name);
            this.WriteToStream("\t\t}}");
            this.WriteToStream("");
            this.WriteToStream("\t\tprivate function completeHandler(event:Event):void");
            this.WriteToStream("\t\t{{");

			this.WriteToStream("// Verify Status");
			this.WriteToStream("var dataAsString:String = this._loader.data.toString();");
			this.WriteToStream("");
			this.WriteToStream("if(dataAsString.indexOf(\"<ResultMessage>\") > 0)");
			this.WriteToStream("{{");
			this.WriteToStream("	// Valid reponse, verify it's result");
			this.WriteToStream("	var resultMessage:String = dataAsString.substring(dataAsString.indexOf(\"<ResultMessage>\"), dataAsString.indexOf(\"</ResultMessage>\"));");
			this.WriteToStream("	resultMessage = resultMessage.substring(\"<ResultMessage>\".length);");
			this.WriteToStream("	var resultCode:String = dataAsString.substring(dataAsString.indexOf(\"<ResultCode>\"), dataAsString.indexOf(\"</ResultCode>\"));");
			this.WriteToStream("	resultCode = resultCode.substring(\"<ResultCode>\".length);");
			this.WriteToStream("	");
			this.WriteToStream("	if(resultCode == \"100\")");
			this.WriteToStream("	{{");
			this.WriteToStream("		// Create a File instance for the media zip");
			this.WriteToStream("		var outFileZip:File = Global.customerDataPath;");
			this.WriteToStream("		outFileZip = outFileZip.resolvePath(\"{0}.xml\");", this.method.Name);
			this.WriteToStream("");
			this.WriteToStream("		// Write the media zip to disk");
			this.WriteToStream("		FileHelper.writeStringToDisk(this._loader.data, outFileZip);");
			this.WriteToStream("");
			this.WriteToStream("		var ev:{0}ResultEvent = new {0}ResultEvent(WebserviceEvents.{0}Result);", this.method.Name);
			this.WriteToStream("		Global.dispatcher.dispatchEvent(ev);				");
			this.WriteToStream("	}}				");
			this.WriteToStream("	else");
			this.WriteToStream("	{{");
			this.WriteToStream("		ApplicationEvent.AnnounceWebserviceCallCustomerError(\"{0}\", resultMessage, resultCode);", this.method.Name);
			this.WriteToStream("	}}											");
			this.WriteToStream("}}");
			this.WriteToStream("else");
			this.WriteToStream("{{");
			this.WriteToStream("	// Invalid response");
			this.WriteToStream("	ApplicationEvent.AnnounceWebserviceCallCustomerError(\"{0}\", \"Incorrect result format: \" + dataAsString, \"-1\");", this.method.Name);
			this.WriteToStream("}}");
            this.WriteToStream("\t\t}}");
        }

        //private void WriteMethods()
        //{
        //    this.WriteToStream("\t\t// ------------------  METHODS ------------------");
        //    this.WriteToStream("");
        //    this.WriteToStream("\t\tpublic function getObyTypedResult(resultObject:Object):ObyTypedResult", this.type.Name);
        //    this.WriteToStream("\t\t{{");
        //    this.WriteToStream("\t\t\ttrace(\"{0}Handler.getObyTypedResult\");", this.method.Name);
        //    this.WriteToStream("");
        //    this.WriteToStream("\t\t\tvar result:ObyTypedResult = new ObyTypedResult();");
        //    this.WriteToStream("\t\t\tresult.resultType = resultObject.ResultType;");
        //    this.WriteToStream("\t\t\tresult.resultMessage = resultObject.ResultMessage;");
        //    this.WriteToStream("\t\t\tresult.resultCode = resultObject.ResultCode;");
        //    this.WriteToStream("");
        //    this.WriteToStream("\t\t\tvar {0}Collection:{1}Collection = new {1}Collection();", StringUtil.TurnFirstToLower(this.type.Name, false), this.type.Name);
        //    this.WriteToStream("");
        //    this.WriteToStream("\t\t\tif (resultObject != null && resultObject.ModelCollection.length > 0)");
        //    this.WriteToStream("\t\t\t{{");
        //    this.WriteToStream("\t\t\t\tfor (var i:int=0; i < resultObject.ModelCollection.length; i++)");
        //    this.WriteToStream("\t\t\t\t{{");
        //    this.WriteToStream("\t\t\t\t\tvar {0}:Object = resultObject.ModelCollection[i];", StringUtil.TurnFirstToLower(this.type.Name, false));
        //    this.WriteToStream("\t\t\t\t\tif ({0} != null)", StringUtil.TurnFirstToLower(this.type.Name, false));
        //    this.WriteToStream("\t\t\t\t\t{{");
        //    this.WriteToStream("\t\t\t\t\t\tvar {0}Entity:{1}Entity = new {1}Entity();", StringUtil.TurnFirstToLower(this.type.Name, false), this.type.Name);

        //    for (int i = 0; i < this.fields.Length; i++)
        //    {
        //        PropertyInfo property = this.fields[i];

        //        // Check whether the field is a collection
        //        if (property.PropertyType.IsArray)
        //        {
        //            this.WriteRelatedCollection(property, 0);
        //        }
        //        else
        //        {
        //            this.WriteToStream("\t\t\t\t\t\t{0}Entity.{1} = {0}.{1};", StringUtil.TurnFirstToLower(this.type.Name, false), property.Name);
        //        }
        //    }

        //    this.WriteToStream("");			
        //    this.WriteToStream("\t\t\t\t\t\t{0}Collection.add{1}Entity({0}Entity);", StringUtil.TurnFirstToLower(this.type.Name, false), this.type.Name);
        //    this.WriteToStream("\t\t\t\t\t}}");
        //    this.WriteToStream("\t\t\t\t}}");
        //    this.WriteToStream("\t\t\t}}");
        //    this.WriteToStream("");
        //    this.WriteToStream("\t\t\tresult.entityCollection = {0}Collection;", StringUtil.TurnFirstToLower(this.type.Name, false));
        //    this.WriteToStream("");
        //    this.WriteToStream("\t\t\treturn result;");
        //    this.WriteToStream("\t\t}}");
        //}

        //private void WriteRelatedCollection(PropertyInfo property, int level)
        //{
        //    this.index++;

        //    string typeName = property.PropertyType.FullName.Replace("[]", string.Empty);
        //    Type type = this.modelAssembly.GetType(typeName);

        //    WriteToStream("");
        //    WriteToStream("{0}if ({2}.hasOwnProperty(\"{1}\"))", this.GetTabs(level), property.Name, StringUtil.TurnFirstToLower(property.DeclaringType.Name, false));
        //    WriteToStream("{0}{{", this.GetTabs(level));
        //    WriteToStream("{2}\tfor(var {3}:int=0; {3} < {0}.{1}.length; {3}++)", StringUtil.TurnFirstToLower(property.DeclaringType.Name, false), property.Name, this.GetTabs(level), this.GetIndex(level));
        //    WriteToStream("{0}\t{{", this.GetTabs(level));
        //    WriteToStream("{3}\t\tvar {0}:Object = {1}.{2}[{4}];", StringUtil.TurnFirstToLower(type.Name, false), StringUtil.TurnFirstToLower(property.DeclaringType.Name, false), property.Name, this.GetTabs(level), this.GetIndex(level));
        //    WriteToStream("");
        //    WriteToStream("{2}\t\tvar {0}Entity:{1}Entity = new {1}Entity();", StringUtil.TurnFirstToLower(type.Name, false), type.Name, this.GetTabs(level));

        //    object model = Dionysos.InstanceFactory.CreateInstance(this.modelAssembly, type);

        //    PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
        //    List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
        //    for (int j = 0; j < propertyInfo.Length; j++)
        //    {
        //        PropertyInfo property2 = propertyInfo[j];

        //        // Check whether the property has a ExcludeFromCodeGeneratorToMobile
        //        if (property2.GetCustomAttributes(typeof(ExcludeFromCodeGeneratorToMobile), false).Length <= 0)
        //        {
        //            propertyInfoList.Add(property2);
        //        }
        //    }

        //    PropertyInfo[] fields = propertyInfoList.ToArray();

        //    for (int i = 0; i < fields.Length; i++)
        //    {
        //        PropertyInfo property2 = fields[i];

        //        // Check whether the field is a collection
        //        if (property2.PropertyType.IsArray)
        //        {
        //            level++;
        //            this.WriteRelatedCollection(property2, level);
        //        }
        //        else
        //        {
        //            this.WriteToStream("{2}\t\t{0}Entity.{1} = {0}.{1};", StringUtil.TurnFirstToLower(type.Name, false), property2.Name, this.GetTabs(level));
        //        }
        //    }

        //    WriteToStream("");
        //    WriteToStream("{0}\t\t{1}Entity.{2}.add{3}Entity({4}Entity);", this.GetTabs(level), StringUtil.TurnFirstToLower(property.DeclaringType.Name, false), property.Name, type.Name, StringUtil.TurnFirstToLower(type.Name, false));
        //    WriteToStream("{0}\t}}", this.GetTabs(level));
        //    WriteToStream("{0}}}", this.GetTabs(level));
        //    WriteToStream("");
        //}

        //private string GetTabs(int level)
        //{
        //    string tabs = "\t\t\t\t\t\t";
        //    for (int i = 0; i < level; i++)
        //    {
        //        tabs += "\t";
        //    }
        //    return tabs;
        //}

        //private char GetIndex(int level)
        //{
        //    char index = 'j';
        //    index += (char)this.index;
        //    return index;
        //}

        private void WriteClassEnd()
        {
            WriteToStream("\t}}");
            WriteToStream("}}");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
