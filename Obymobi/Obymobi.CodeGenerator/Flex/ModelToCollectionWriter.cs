﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Dionysos;
using System.Reflection;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Flex
{
	public class ModelToCollectionWriter
	{
		private Object model;
		private Type modelType;
		private StreamWriter stream;		
		private List<PropertyInfo> fkFields = new List<PropertyInfo>();
		private PropertyInfo pkField = null;

		public ModelToCollectionWriter(Object model, StreamWriter stream)
		{
			this.model = model;
			this.modelType = model.GetType();

			PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);			
			for (int j = 0; j < propertyInfo.Length; j++)
			{
				PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForFlex
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForFlex), false).Length == 0)
				{
					// Exclude
				}
				else if (property.GetCustomAttributes(typeof(ForeignKeyFieldOfModel), false).Length > 0)
					this.fkFields.Add(property);
				else if (property.GetCustomAttributes(typeof(PrimaryKeyFieldOfModel), false).Length > 0)
				{
					if (this.pkField != null)
						throw new Dionysos.TechnicalException("Multiple PK fields found on Model {0}", this.modelType.FullName);
					else
						this.pkField = property;
				}			
			}

			this.stream = stream;
		}

		public void WriteCollection()
		{ 
			this.WriteIntroduction();
			this.WriteClassStart();
			this.WriteIEntityCollectionImplementation();

            if (!this.modelType.Name.Contains("SimpleWebserviceResult"))
			    this.WriteHashmapCode();

			this.WriteClassEnd();
		}

		private void WriteIntroduction()
		{
            WriteToStream("/*");
            WriteToStream(" * {0}Collection.as", this.modelType.Name);
            WriteToStream(" * Generated entity collection class for {0}", this.modelType.FullName);
            WriteToStream(" */");
            WriteToStream("");
            WriteToStream("package com.data.obymobi.collectionclasses");
            WriteToStream("{{");
            //WriteToStream("\timport mx.collections.ArrayCollection;");
	        //WriteToStream("\timport mx.collections.IList;");
            //WriteToStream("\timport mx.collections.ICollectionView;");
			WriteToStream("\timport flash.utils.Dictionary;");
            WriteToStream("\timport com.data.obymobi.supportclasses.EntityCollection;");
            //WriteToStream("\timport com.logic.helpers.xmlhelper;");
            WriteToStream("\timport com.data.obymobi.entityclasses.{0}Entity;", this.modelType.Name);
            WriteToStream("");
		}

		private void WriteClassStart()
		{
			WriteToStream("\tpublic class {0}Collection extends EntityCollection", this.modelType.Name);
			WriteToStream("\t{{");
            WriteToStream("\t\t// ------------------  CONSTRUCTORS ------------------");
            WriteToStream("\t\t/**");
            WriteToStream("\t\t* Constructor - initializes the collection");
            WriteToStream("\t\t*/");
			WriteToStream("\t\tpublic function {0}Collection(maintainHashmaps:Boolean)", this.modelType.Name);
            WriteToStream("\t\t{{");
			WriteToStream("\t\t\tsuper(maintainHashmaps);");
            WriteToStream("\t\t}}");
            WriteToStream("");
		}

		private void WriteIEntityCollectionImplementation()
		{
            WriteToStream("\t\t// ------------------  METHODS ------------------");
			WriteToStream("");

		    WriteToStream("\t\tpublic function add{0}EntityAt(item:{0}Entity,index:int):void", this.modelType.Name);
		    WriteToStream("\t\t{{");
			WriteToStream("\t\t\taddItemAt(item,index);");
		    WriteToStream("\t\t}}");
            WriteToStream("");

		    WriteToStream("\t\tpublic function add{0}Entity(item:{0}Entity):void", this.modelType.Name);
		    WriteToStream("\t\t{{");
			WriteToStream("\t\t\taddItem(item);");
		    WriteToStream("\t\t}}");
            WriteToStream("");

		    WriteToStream("\t\tpublic function get{0}EntityAt(index:int):{0}Entity", this.modelType.Name); 
		    WriteToStream("\t\t{{");
			WriteToStream("\t\t\treturn getItemAt(index) as {0}Entity;", this.modelType.Name);
		    WriteToStream("\t\t}}");
            WriteToStream("");

		    WriteToStream("\t\tpublic function get{0}EntityIndex(item:{0}Entity):int", this.modelType.Name);
		    WriteToStream("\t\t{{");
			WriteToStream("\t\t\treturn getItemIndex(item);");
		    WriteToStream("\t\t}}");
            WriteToStream("");

		    WriteToStream("\t\tpublic function set{0}EntityAt(item:{0}Entity,index:int):void", this.modelType.Name);
		    WriteToStream("\t\t{{");
			WriteToStream("\t\t\tsetItemAt(item,index);");
            WriteToStream("\t\t}}");
            WriteToStream("");

            //// Get Entity Instance
            //WriteToStream("	public IEntity getNewEmptyEntityInstance()");
            //WriteToStream("	{{");
            //WriteToStream("		return new {0}Entity();", this.modelType.Name);
            //WriteToStream("	}}");
            //WriteToStream("");

            // Constant for EntityName
            WriteToStream("\t\tpublic function getModelName():String");
            WriteToStream("\t\t{{");
            WriteToStream("\t\t\treturn \"{0}\";", this.modelType.Name);
            WriteToStream("\t\t}}");
            WriteToStream("");

			// Collection to XML
			WriteToStream("\t\tpublic override function toXml():String");			
			WriteToStream("\t\t{{");			
			WriteToStream("\t\t\tvar xml:String = \"\";");
			//WriteToStream("\t\t\txml += xmlhelper.createXmlOpeningTag(\"ArrayOf\" + this.getModelName());");			
			WriteToStream("\t\t\tfor (var i:int; i < this.length; i++)");			
			WriteToStream("\t\t\t{{");
            WriteToStream("\t\t\t\tvar {1}Entity:{0}Entity = this.get{2}EntityAt(i);", this.modelType.Name.TurnFirstToUpper(false), this.modelType.Name.TurnFirstToLower(false), this.modelType.Name);
			WriteToStream("\t\t\t\txml += {0}Entity.toXml();", this.modelType.Name.TurnFirstToLower(false));			
			WriteToStream("\t\t\t}}");			
			//WriteToStream("\t\t\txml += xmlhelper.createXmlClosingTag(\"ArrayOf\" + this.getModelName());");			
			WriteToStream("\t\t\treturn xml;");			
			WriteToStream("\t\t}}");
            WriteToStream("");

            WriteToStream("\t\tpublic override function toString():String");
            WriteToStream("\t\t{{");
            WriteToStream("\t\t\treturn toXml();");
            WriteToStream("\t\t}}");
		}

		private void WriteHashmapCode()
		{ 
			// Private Dictionaries
			WriteToStream("");
			WriteToStream("\t\t// ------------------  Hashmap Code ------------------");		
			WriteToStream("\t\tinternal var pkFieldHashmap:Dictionary  = new Dictionary(true);");
			WriteToStream("\t\tinternal var fkFieldsHashmap:Dictionary = new Dictionary(true);");
			WriteToStream("");

			// PK Field Retrieval Handler
			WriteToStream("\t\tpublic function get{0}By{1}({2}:int):{3}Entity", this.modelType.Name.TurnFirstToUpper(false), this.pkField.Name, this.pkField.Name.TurnFirstToLower(false), this.modelType.Name);		
			WriteToStream("\t\t{{");
			WriteToStream("\t\t\treturn this.pkFieldHashmap[{0}] as {1}Entity;", this.pkField.Name.TurnFirstToLower(false), this.modelType.Name);
			WriteToStream("\t\t}}");
			WriteToStream("");

			// Update PK Hashmap
			WriteToStream("\t\tprotected override function updateHashmaps(item:Object, add:Boolean):void");
			WriteToStream("\t\t{{");
			WriteToStream("\t\t\tvar itemTyped:{0}Entity = item as {0}Entity;", this.modelType.Name);
			WriteToStream("\t\t\t");
			WriteToStream("\t\t\tif(add)							");
			WriteToStream("\t\t\t\tthis.pkFieldHashmap[itemTyped.{0}] = itemTyped;			", this.pkField.Name);
			WriteToStream("\t\t\telse			");
			WriteToStream("\t\t\t\tdelete this.pkFieldHashmap[itemTyped.{0}];			", this.pkField.Name);
			WriteToStream("\t\t\t");
			WriteToStream("\t\t\t// Per FK Field");
			foreach(var fkField in this.fkFields)
			{
				WriteToStream("\t\t\tthis.updateForeignKeyHashmap(\"{0}\", itemTyped.{0}, itemTyped, add);	", fkField.Name);
			}
			WriteToStream("\t\t}}");
			WriteToStream("");

			// Helper: retrieveForeignKeyCollection
			WriteToStream("\t\tprotected function retrieveForeignKeyCollection(foreignKeyField:String, foreignKeyValue:*):{0}Collection", this.modelType.Name);
			WriteToStream("\t\t{{");
			WriteToStream("\t\t\tif(this.fkFieldsHashmap[foreignKeyField][foreignKeyValue] == null)");			
			WriteToStream("\t\t\t\tthis.fkFieldsHashmap[foreignKeyField][foreignKeyValue] = new {0}Collection(false);", this.modelType.Name);
			WriteToStream("\t\t\t");	
			WriteToStream("\t\t\treturn this.fkFieldsHashmap[foreignKeyField][foreignKeyValue] as {0}Collection;", this.modelType.Name);
			WriteToStream("\t\t}}");
			WriteToStream("");

			// Helper: updateForeignKeyHashmap
			WriteToStream("\t\tprotected function updateForeignKeyHashmap(foreignKeyField:String, foreignKeyValue:*, item:{0}Entity, add:Boolean):void", this.modelType.Name);
			WriteToStream("\t\t{{");
			WriteToStream("\t\t\tif(this.fkFieldsHashmap[foreignKeyField][foreignKeyValue] == null)");			
			WriteToStream("\t\t\t\tthis.fkFieldsHashmap[foreignKeyField][foreignKeyValue] = new {0}Collection(false);", this.modelType.Name);	
			WriteToStream("\t\t\t");	
			WriteToStream("\t\t\tvar collection:{0}Collection = this.fkFieldsHashmap[foreignKeyField][foreignKeyValue] as {0}Collection;", this.modelType.Name);
			WriteToStream("\t\t\t");	
			WriteToStream("\t\t\tif(add)");
			WriteToStream("\t\t\t\tcollection.addItem(item);");
			WriteToStream("\t\t\telse");
			WriteToStream("\t\t\t\tcollection.removeItemAt(collection.getItemIndex(item));");
			WriteToStream("\t\t}}");
			WriteToStream("");

			// Helper: initialize Hashmaps
			WriteToStream("\t\tprotected override function initializeHashmaps():void");
			WriteToStream("\t\t{{");
			WriteToStream("\t\t\tthis.pkFieldHashmap = new Dictionary(true);");
			WriteToStream("\t\t\t");	
			WriteToStream("\t\t\t// Initialize the Sub-Dictionaries for each foreign key field");
			WriteToStream("\t\t\tthis.fkFieldsHashmap = new Dictionary(true);");
			WriteToStream("\t\t\t");	
			WriteToStream("\t\t\t// Per FK Field");
			foreach(var fkField in this.fkFields)
			{
				WriteToStream("\t\t\tthis.fkFieldsHashmap[\"{0}\"] = new Dictionary(true);", fkField.Name);
			}			
			WriteToStream("\t\t}}");
			WriteToStream("");

			// FK Field Retrieval Handlers
			foreach (var fkField in this.fkFields)
			{
				WriteToStream("\t\tpublic function get{0}sBy{1}({2}:int):{0}Collection", this.modelType.Name, fkField.Name, fkField.Name.TurnFirstToLower(false));
				WriteToStream("\t\t{{");
				WriteToStream("\t\t\treturn this.retrieveForeignKeyCollection(\"{0}\", {1});", fkField.Name, fkField.Name.TurnFirstToLower(false));
				WriteToStream("\t\t}}");
			}
		}

		private void WriteClassEnd()
		{
			WriteToStream("");
			WriteToStream("\t}}");
            WriteToStream("}}");
		}

		private void WriteToStream(string message, params object[] args)
		{
			this.stream.WriteLine(string.Format(message, args));
		}
	}
}
