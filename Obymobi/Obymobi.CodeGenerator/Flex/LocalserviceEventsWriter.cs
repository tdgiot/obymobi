﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Flex
{
    public class LocalserviceEventsWriter
    {
        private StreamWriter stream;
        private List<MethodInfo> methods;

        public LocalserviceEventsWriter(StreamWriter stream, List<MethodInfo> methods)
        {
            this.stream = stream;
            this.methods = methods;
        }

        public void Write()
        {
            this.WriteToStream("package com.net.obymobi");
            this.WriteToStream("{{");
	        this.WriteToStream("\timport com.data.obymobi.collectionclasses.*;");
            this.WriteToStream("");
            this.WriteToStream("\timport flash.events.Event;");
	        this.WriteToStream("\timport flash.events.IEventDispatcher;");
	        this.WriteToStream("");
	        this.WriteToStream("\tpublic class LocalserviceEvent extends Event");
            this.WriteToStream("\t{{");
            this.WriteToStream("\t\t/*-.........................................Constants..........................................*/");
            this.WriteToStream("");
            this.WriteToStream("\t\tpublic static const eventTypePrefix:String 			= \"LocalserviceEvent.\"");

            for (int i = 0; i < this.methods.Count; i++)
            {
                MemberInfo method = this.methods[i];
                if (method.GetCustomAttributes(typeof(IncludeInCodeGeneratorForFlex), false).Length > 0 && method.GetCustomAttributes(typeof(ClientLocalStorageFlex), false).Length > 0)
                {
                    this.WriteToStream("\t\tpublic static const PARSE_{0}:String				= eventTypePrefix + \"PARSE_{0}\";", method.Name.ToUpper());
                }
            }

            this.WriteToStream("");
		    this.WriteToStream("\t\t/*-.........................................Constructor..........................................*/");	
		    this.WriteToStream("\t\tpublic function LocalserviceEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false)");
		    this.WriteToStream("\t\t{{");
			this.WriteToStream("\t\t\tsuper(type, bubbles, cancelable);");
		    this.WriteToStream("\t\t}}");	
            this.WriteToStream("");		
		    this.WriteToStream("\t\t/*-.........................................Methods..........................................*/");
		    this.WriteToStream("\t\tpublic static function Announce(type:String, dispatcher:IEventDispatcher):void");
		    this.WriteToStream("\t\t{{");
			this.WriteToStream("\t\t\tvar event:com.net.obymobi.LocalserviceEvent = new com.net.obymobi.LocalserviceEvent(type);");
			this.WriteToStream("\t\t\tdispatcher.dispatchEvent(event);");
            this.WriteToStream("\t\t}}");
	        this.WriteToStream("\t}}");
            this.WriteToStream("}}");

        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
