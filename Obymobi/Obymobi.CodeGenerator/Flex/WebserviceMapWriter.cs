﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Flex
{
    public class WebserviceMapWriter
    {
        private StreamWriter stream;
        private List<MethodInfo> methods;

        public WebserviceMapWriter(StreamWriter stream, List<MethodInfo> methods)
        {
            this.stream = stream;
            this.methods = methods;
        }

        public void Write()
        {
            this.WriteIntroduction();
            this.WriteMethods();
            this.WriteClassEnd();
        }

        private void WriteIntroduction()
        {
            WriteToStream("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            WriteToStream("<EventMap xmlns:mx=\"http://www.adobe.com/2006/mxml\" xmlns=\"http://mate.asfusion.com/\">");
	        WriteToStream("");
	        WriteToStream("\t<mx:Script>");
		    WriteToStream("\t\t<![CDATA[");
			WriteToStream("\t\t\timport mx.rpc.soap.SOAPFault;");
			WriteToStream("\t\t\timport com.logic.events.ApplicationEvent;");
			WriteToStream("\t\t\timport mx.rpc.events.FaultEvent;");
            WriteToStream("\t\t\timport mx.events.FlexEvent;");
            WriteToStream("\t\t\timport com.net.obymobi.*;");
			WriteToStream("\t\t\timport com.net.obymobi.events.*;");
			WriteToStream("\t\t\timport com.net.obymobi.handlers.*;");
			WriteToStream("");
			WriteToStream("\t\t\t[Bindable]");
			WriteToStream("\t\t\tpublic var webserviceUrl:String = \"http://localhost/obymobi/ObymobiTypedWebservice.asmx?WSDL\";");		   			

			WriteToStream("\t\t\tprivate function webserviceInitFault(event:FaultEvent):void");
			WriteToStream("\t\t\t\t{{								");
			WriteToStream("\t\t\t\t\tif(event.fault is mx.rpc.soap.SOAPFault)");
			WriteToStream("\t\t\t\t\t{{");
			WriteToStream("\t\t\t\t\t\t// Nothing, method error Handled by Fault Handler");
			WriteToStream("\t\t\t\t\t}}");
			WriteToStream("\t\t\t\t\telse");
			WriteToStream("\t\t\t\t\t{{");
			WriteToStream("\t\t\t\t\t\tApplicationEvent.AnnounceWebserviceInitCallFailed(event);");
			WriteToStream("\t\t\t\t\t}}");
			WriteToStream("\t\t\t\t}}");

			WriteToStream("\t\t]]>");

	        WriteToStream("\t</mx:Script>");
	        WriteToStream("");			
	        WriteToStream("\t<mx:WebService id=\"webservice\" wsdl=\"{{this.webserviceUrl}}\" fault=\"webserviceInitFault(event)\" />");
            WriteToStream("");

            WriteToStream("\t<!-- Start of Webservices Map -->");
            WriteToStream("");
        }

        private void WriteMethods()
        {
            for (int i = 0; i < this.methods.Count; i++)
            {
                MethodInfo methodInfo = this.methods[i];

                if (methodInfo.GetCustomAttributes(typeof(IncludeInCodeGeneratorForFlex), false).Length > 0)
                {
                    if (methodInfo.GetCustomAttributes(typeof(ClientLocalStorageFlex), false).Length > 0)
                    {
                        WriteToStream("\t<EventHandlers type=\"{{WebserviceEvents.{0}}}\" debug=\"true\">", methodInfo.Name);
                        WriteToStream("\t\t<MethodInvoker generator=\"{{{0}Handler}}\" method=\"save{0}\" arguments=\"{{[event]}}\" />", methodInfo.Name);
                        WriteToStream("\t</EventHandlers>");
                    }
                    else
                    {
                        WriteToStream("\t<EventHandlers type=\"{{WebserviceEvents.{0}}}\" debug=\"true\">", methodInfo.Name);
                        if (methodInfo.GetParameters().Length == 0)
                            WriteToStream("\t\t<WebServiceInvoker instance=\"{{this.webservice}}\" method=\"{{WebserviceEvents.{0}}}\" >", methodInfo.Name);
                        else
                            WriteToStream("\t\t<WebServiceInvoker instance=\"{{this.webservice}}\" method=\"{{WebserviceEvents.{0}}}\" arguments=\"{{[{1}]}}\" >", methodInfo.Name, this.GetParameterString(methodInfo));
                        WriteToStream("\t\t\t<resultHandlers>");
                        WriteToStream("\t\t\t\t<MethodInvoker generator=\"{{{0}Handler}}\" method=\"getObyTypedResult\" arguments=\"{{resultObject}}\" />", methodInfo.Name);
                        WriteToStream("\t\t\t\t<EventAnnouncer generator=\"{{{0}ResultEvent}}\" type=\"{{WebserviceEvents.{0}Result}}\" >", methodInfo.Name);
                        WriteToStream("\t\t\t\t\t<Properties Result=\"{{lastReturn}}\" />");
                        WriteToStream("\t\t\t\t</EventAnnouncer>");
                        WriteToStream("\t\t\t</resultHandlers>");
                        WriteToStream("\t\t\t<faultHandlers>");
                        WriteToStream("\t\t\t\t<InlineInvoker method=\"ApplicationEvent.AnnounceWebserviceCallFailed\" arguments=\"{{ [ '{0}', fault ] }}\" />", methodInfo.Name);
                        WriteToStream("\t\t\t</faultHandlers>");
                        WriteToStream("\t\t</WebServiceInvoker>");
                        WriteToStream("\t</EventHandlers>");

                    }

                    if (i != (this.methods.Count - 1))
                        WriteToStream("");
                }
            }
        }

        private string GetParameterString(MethodInfo method)
        {
            string parameterString = string.Empty;
            ParameterInfo[] parameters = method.GetParameters();

            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo parameter = parameters[i];
                parameterString += string.Format("event.{0}", parameter.Name);

                if (i != (parameters.Length - 1))
                    parameterString += ", ";
            }

            return parameterString;
        }

        private void WriteClassEnd()
        {
            WriteToStream("");
            WriteToStream("\t<!-- End of Webservices Map -->");
            WriteToStream("");
            WriteToStream("</EventMap>");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
