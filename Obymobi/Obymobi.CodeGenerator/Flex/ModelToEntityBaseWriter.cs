﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Flex
{
	public class ModelToEntityBaseWriter
	{
		private Object model;
		private Type modelType;
		private StreamWriter stream;
		private PropertyInfo[] fields;

		public ModelToEntityBaseWriter(Object model, StreamWriter stream)
		{
			this.model = model;
			this.modelType = model.GetType();
			this.stream = stream;			
			
			PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
			List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
			for (int j = 0; j < propertyInfo.Length; j++)
			{
				PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForFlex
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForFlex), false).Length > 0)
				    propertyInfoList.Add(property);
			}

			this.fields = propertyInfoList.ToArray();
		}

		public void WriteEntity()
		{
			this.WriteIntroduction();
			this.WriteClassStart();
            this.WriteFields();
            this.WriteProperties();
            //this.WriteFromAndToDataStreamLogic();
            this.WriteXmlLogic();
			this.WriteCloneLogic();
            //this.WriteGenericPropertyAccessor();
            //this.WriteAdditionalLogic();
			this.WriteClassEnd();
		}

		private void WriteIntroduction()
		{
			WriteToStream("/*");
            WriteToStream(" * {0}EntityBase.as", this.modelType.Name);
			WriteToStream(" * Generated entity base class for {0}", this.modelType.FullName);
			WriteToStream(" */");
			WriteToStream("");
            WriteToStream("package com.data.obymobi.entitybaseclasses");
			WriteToStream("{{");
            WriteToStream("\timport com.logic.helpers.xmlhelper;");
			WriteToStream("\timport com.data.obymobi.collectionclasses.*;");//import flash.events.EventDispatcher;
			WriteToStream("\timport com.data.obymobi.entitybaseclasses.*;");//import flash.events.EventDispatcher;
			WriteToStream("\timport com.data.obymobi.entityclasses.*;");//import flash.events.EventDispatcher;
			WriteToStream("\timport flash.events.EventDispatcher;");
            WriteToStream("");            	
		}

		private void WriteClassStart()
		{
			WriteToStream("\tpublic class {0}EntityBase extends EventDispatcher", this.modelType.Name);
            WriteToStream("\t{{");
            WriteToStream("\t\t// ------------------  CONSTRUCTORS ------------------");
            WriteToStream("\t\t/**");
            WriteToStream("\t\t* Constructor, initializes the type class");
            WriteToStream("\t\t*/");
			WriteToStream("\t\tpublic function {0}EntityBase()", this.modelType.Name);
            WriteToStream("\t\t{{");
            WriteToStream("\t\t}}");
            WriteToStream("");
		}

		private void WriteFields()
		{
            WriteToStream("\t\t// ------------------  FIELDS ------------------");
            WriteToStream("");

			foreach (var field in fields)
			{
                string datatype = FlexHelper.GetFlexDataType(field.PropertyType);
                if (datatype == "Boolean")
            		WriteToStream("\t\tprivate var _{0}:{1} = false;", field.Name.TurnFirstToLower(false), datatype);
                else if (datatype == "String")
                    WriteToStream("\t\tprivate var _{0}:{1} = \"\";", field.Name.TurnFirstToLower(false), datatype);
                else if (datatype == "int" || datatype == "Number")
                    WriteToStream("\t\tprivate var _{0}:{1} = 0;", field.Name.TurnFirstToLower(false), datatype);
                else if (datatype == "Date")
                    WriteToStream("\t\tprivate var _{0}:{1};", field.Name.TurnFirstToLower(false), datatype);
                else
                {
                    WriteToStream("\t\tprivate var _{0}:{1} = new {1}(true);", field.Name.TurnFirstToLower(false), datatype);
                }
			}

            WriteToStream("");
		}

		private void WriteProperties()
		{
            WriteToStream("\t\t// ------------------  PROPERTIES ------------------");
			WriteToStream("");			
			foreach (var field in fields)
			{
				string typeName = FlexHelper.GetFlexDataType(field.PropertyType);

				if (typeName.Length > 0)
				{
					// The Setter
					WriteToStream("\t\t[Bindable]");
					WriteToStream("\t\tpublic function set {0}(value:{1}):void", field.Name, typeName);
					WriteToStream("\t\t{{");
					WriteToStream("\t\t\tthis._{0} = value;", field.Name.TurnFirstToLower(false));
					WriteToStream("\t\t}}");

					WriteToStream("");

					// The Getter
					WriteToStream("\t\tpublic function get {0}():{1}", field.Name, typeName);
					WriteToStream("\t\t{{");
					WriteToStream("\t\t\treturn this._{0};", field.Name.TurnFirstToLower(false));
					WriteToStream("\t\t}}");
				}
				WriteToStream("");
			}
			WriteToStream("");

            //// Constant for EntityName
            //WriteToStream("	public String getModelName()");
            //WriteToStream("	{{");
            //WriteToStream("		return \"{0}\";", this.modelType.Name);
            //WriteToStream("	}}");
            //WriteToStream("");
            //WriteToStream("");
		}

		private void WriteFromAndToDataStreamLogic()
		{
			WriteToStream("	// From and To DataStream conversion");
			WriteToStream("	// <editor-fold>");
			WriteToStream("");

			// From DataStream
			WriteToStream("	public void fromDataStream(DataInputStream inputStream)");
			WriteToStream("	{{");
			WriteToStream("		try");
			WriteToStream("		{{");

			// Per field			
			foreach (var field in fields)
			{
				string conversionMethodName = "";
				if (field.PropertyType.ToString() == typeof(Int32).ToString())
				{
					conversionMethodName = "readInt";
				}
				else if (field.PropertyType.ToString() == typeof(String).ToString())
				{
					conversionMethodName = "readUTF";
				}

				if (conversionMethodName.Length > 0)
					WriteToStream("			this.set{0}(inputStream.{1}());", field.Name, conversionMethodName);
			}


			WriteToStream("		}}");
			WriteToStream("		catch (Exception ex)");
			WriteToStream("		{{");
			WriteToStream("			throw new obymobi.logic.helperclasses.GenericRuntimeException(ex);");
			WriteToStream("		}}");
			WriteToStream("	}}");

			// To DataStream
			WriteToStream("	public void toDataStream(DataOutputStream outputStream)");
			WriteToStream("	{{");
			WriteToStream("		try");
			WriteToStream("		{{");
			WriteToStream("");

			// Per field			
			foreach (var field in fields)
			{
				string conversionMethodName = "";
				if (field.PropertyType.ToString() == typeof(Int32).ToString())
				{
					conversionMethodName = "writeInt";
				}
				else if (field.PropertyType.ToString() == typeof(String).ToString())
				{
					conversionMethodName = "writeUTF";
				}

				if (conversionMethodName.Length > 0)
					WriteToStream("			outputStream.{0}(this.get{1}());", conversionMethodName, field.Name);
			}


			WriteToStream("		}}");
			WriteToStream("		catch (Exception ex)");
			WriteToStream("		{{");
			WriteToStream("			throw new obymobi.logic.helperclasses.GenericRuntimeException(ex);");
			WriteToStream("		}}");
			WriteToStream("	}}");
			WriteToStream("");

			// To DataStream

			WriteToStream("");
			WriteToStream("	// </editor-fold>");
		}

		private void WriteXmlLogic()
		{
            WriteToStream("\t\t// ------------------  XML LOGIC ------------------");
            WriteToStream("");	

			#region ToXml
			WriteToStream("\t\tpublic function toXml():String");
			WriteToStream("\t\t{{");
			WriteToStream("\t\t\tvar xml:String = \"\";");
			WriteToStream("");
			WriteToStream("\t\t\txml += xmlhelper.createXmlOpeningTag(\"{0}\");", this.modelType.Name);
			WriteToStream("");

			// Write fields
			foreach (var field in fields)
			{
				//if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
					WriteToStream("\t\t\txml += xmlhelper.createXmlElement(\"{0}\", {0});", field.Name);
			}

			WriteToStream("");
			WriteToStream("\t\t\txml += xmlhelper.createXmlClosingTag(\"{0}\");", this.modelType.Name);
			WriteToStream("");
			WriteToStream("\t\t\treturn xml;");
			#endregion

			WriteToStream("\t\t}}");
			WriteToStream("");
		}

		public void WriteCloneLogic()
		{
			WriteToStream("\t\t// ------------------  CLONE LOGIC ------------------");
			WriteToStream("");

			#region ToXml
			WriteToStream("\t\tpublic function Clone():{0}Entity", this.modelType.Name);
			WriteToStream("\t\t{{");
			WriteToStream("\t\t	var clone:{0}Entity = new {0}Entity();", this.modelType.Name);		

			// Clone fields
			foreach (var field in fields)
			{
				//if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
				string typeName = FlexHelper.GetFlexDataType(field.PropertyType);
				if(typeName.Contains("Collection"))
				{
					string entityBaseType = typeName.Replace("Collection", "EntityBase");
					string entityType = typeName.Replace("Collection", "Entity");
					// Clone Collection
					WriteToStream("\t		for each(var sourceItem{0}:{0} in this.{1})", entityType, field.Name);
					WriteToStream("\t		{{");
					WriteToStream("\t			var cloneItem{0}:{0} = sourceItem{0}.Clone();", entityType);
					WriteToStream("\t			clone.{0}.addItem(cloneItem{1});", field.Name, entityType);
					WriteToStream("\t		}}");
				}
				else
				{
					WriteToStream("\t		clone.{0} = this.{0};", field.Name);
				}
			}			

			WriteToStream("\t		return clone;");

			#endregion

			WriteToStream("\t\t}}");
			WriteToStream("");

		}

		public void WriteGenericPropertyAccessor()
		{
			WriteToStream("	// Generic Property Accessor");
			WriteToStream("	// <editor-fold>");
			WriteToStream("");

			#region SetPropery for values of type Object
			WriteToStream("	public void setProperty(String propertyName, Object value)");
			WriteToStream("	{{");
			WriteToStream("");

			bool firstIfClause = true;
			foreach (var field in fields)
			{
				if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
				{
					if (firstIfClause)
					{
						WriteToStream("		if(propertyName.compareTo(\"{0}\") == 0)", field.Name);
						firstIfClause = false;
					}
					else
						WriteToStream("		else if(propertyName.compareTo(\"{0}\") == 0)", field.Name);

					if (field.PropertyType.ToString() == typeof(Int32).ToString())
					{
						WriteToStream("			this.{0} = Integer.parseInt(value.toString());", field.Name.TurnFirstToLower(false));
					}
					else if (field.PropertyType.ToString() == typeof(String).ToString())
					{
						WriteToStream("			this.{0} = value.toString();", field.Name.TurnFirstToLower(false));
					}
					else
					{
						// Nohting.
					}
				}
			}

			// End switch & function
			WriteToStream("	}}");
			WriteToStream("");
			#endregion

			#region SetPropery for values of type int
			WriteToStream("	public void setProperty(String propertyName, int value)");
			WriteToStream("	{{");
			WriteToStream("");
			firstIfClause = true;
			foreach (var field in fields)
			{
				if (field.PropertyType.ToString() == typeof(Int32).ToString() || field.PropertyType.ToString() == typeof(String).ToString())
				{
					if (firstIfClause)
					{
						WriteToStream("		if(propertyName.compareTo(\"{0}\") == 0)", field.Name);
						firstIfClause = false;
					}
					else
						WriteToStream("		else if(propertyName.compareTo(\"{0}\") == 0)", field.Name);

					if (field.PropertyType.ToString() == typeof(Int32).ToString())
					{
						WriteToStream("			this.{0} = value;", field.Name.TurnFirstToLower(false));
					}
					else if (field.PropertyType.ToString() == typeof(String).ToString())
					{
						WriteToStream("			this.{0} = Integer.toString(value);", field.Name.TurnFirstToLower(false));
					}
				}
			}

			// End switch & function
			WriteToStream("	}}");
			#endregion

			WriteToStream("");
			WriteToStream("	// </editor-fold>");
		}

		private void WriteAdditionalLogic()
		{
			WriteToStream("	// Entity Base logic");
			WriteToStream("	// <editor-fold>");
			WriteToStream("");

			// Get Entity Instance
			WriteToStream("	public IEntity getNewEmptyInstance()");
			WriteToStream("	{{");
			WriteToStream("		return new {0}Entity();", this.modelType.Name);
			WriteToStream("	}}");
			WriteToStream("");

			// Get CollectionInstance
			WriteToStream("	public EntityCollectionBase getNewEmptyEntityCollection()");
			WriteToStream("	{{");
			WriteToStream("		return new {0}Collection();", this.modelType.Name);
			WriteToStream("	}}");
			WriteToStream("");

			// Unique EntityName
			WriteToStream("	public String uniqueEntityName()");
			WriteToStream("	{{");
			WriteToStream("		return \"{0}Entity\";", this.modelType.Name);
			WriteToStream("	}}");
			WriteToStream("");

			// Abstract methods for PK fields
			WriteToStream("	public abstract int getPkFieldValue();");
			WriteToStream("	public abstract void setPkFieldValue(int pkValue);");

			WriteToStream("");
			WriteToStream("	// </editor-fold>");

			// Clone object
		}

		private void WriteClassEnd()
		{
			WriteToStream("\t}}");
            WriteToStream("}}");
		}

		private void WriteToStream(string message, params object[] args)
		{
			this.stream.WriteLine(string.Format(message, args));
		}
	}
}
