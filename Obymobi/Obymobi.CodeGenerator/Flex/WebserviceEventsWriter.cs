﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Flex
{
    public class WebserviceEventsWriter
    {
        private StreamWriter stream;
        private List<MethodInfo> methods;

        public WebserviceEventsWriter(StreamWriter stream, List<MethodInfo> methods)
        {
            this.stream = stream;
            this.methods = methods;
        }

        public void Write()
        {
            this.WriteToStream("package com.net.obymobi");
            this.WriteToStream("{{");
            this.WriteToStream("\tpublic class WebserviceEvents");
            this.WriteToStream("\t{{");

            for (int i = 0; i < this.methods.Count; i++)
            {
                MemberInfo method = this.methods[i];
                if (method.GetCustomAttributes(typeof(IncludeInCodeGeneratorForFlex), false).Length > 0)
                {
                    this.WriteToStream("\t\tpublic static const {0}:String = \"{0}\";", method.Name);
                }
            }
            
            this.WriteToStream("");
            
            for (int i = 0; i < this.methods.Count; i++)
            {
                MemberInfo method = this.methods[i];
                if (method.GetCustomAttributes(typeof(IncludeInCodeGeneratorForFlex), false).Length > 0)
                {
                    this.WriteToStream("\t\tpublic static const {0}Result:String = \"{0}Result\";", method.Name);
                }
            }

            this.WriteToStream("\t}}");
            this.WriteToStream("}}");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
