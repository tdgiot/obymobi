﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Flex
{
    public class LocalserviceMapWriter
    {
        private StreamWriter stream;
        private List<MethodInfo> methods;

        public LocalserviceMapWriter(StreamWriter stream, List<MethodInfo> methods)
        {
            this.stream = stream;
            this.methods = methods;
        }

        public void Write()
        {
            this.WriteIntroduction();
            this.WriteMethods();
            this.WriteClassEnd();
        }

        private void WriteIntroduction()
        {
            WriteToStream("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            WriteToStream("<EventMap xmlns:mx=\"http://www.adobe.com/2006/mxml\" xmlns=\"http://mate.asfusion.com/\">");
            WriteToStream("");
	        WriteToStream("\t<mx:Script>");
		    WriteToStream("\t\t<![CDATA[");
            WriteToStream("\t\t\timport com.logic.events.LocalserviceEvent;");
			WriteToStream("\t\t\timport com.logic.Global;");
			WriteToStream("\t\t\timport com.logic.helpers.*;");
			WriteToStream("\t\t\timport com.net.obymobi.handlers.*;");
            WriteToStream("\t\t\timport com.logic.events.DataEvent;");
            WriteToStream("");
		    WriteToStream("\t\t]]>");
	        WriteToStream("\t</mx:Script>");
            WriteToStream("");
            WriteToStream("\t<!-- Start of Local services Map -->");
            WriteToStream("");
        }

        private void WriteMethods()
        {
            for (int i = 0; i < this.methods.Count; i++)
            {
                MethodInfo methodInfo = this.methods[i];

                if (methodInfo.GetCustomAttributes(typeof(IncludeInCodeGeneratorForFlex), false).Length > 0 && methodInfo.GetCustomAttributes(typeof(ClientLocalStorageFlex), false).Length > 0)
                {
	                WriteToStream("\t<EventHandlers type=\"{{LocalserviceEvent.PARSE_{0}}}\" debug=\"true\">", methodInfo.Name.ToUpper());
                    WriteToStream("\t\t<HTTPServiceInvoker instance=\"{{{0}Service}}\">", methodInfo.Name);
                    WriteToStream("\t\t\t<resultHandlers>");
                    WriteToStream("\t\t\t\t<MethodInvoker generator=\"{{{0}Helper}}\" method=\"loadXML\" arguments=\"{{resultObject}}\" />", methodInfo.Name);
				    WriteToStream("\t\t\t\t<EventAnnouncer generator=\"{{com.logic.events.DataEvent}}\" type=\"{{com.logic.events.DataEvent.PARSED_{0}}}\" >", methodInfo.Name.ToUpper());
					WriteToStream("\t\t\t\t\t<Properties Result=\"{{lastReturn}}\" />");
				    WriteToStream("\t\t\t\t</EventAnnouncer>");
                    WriteToStream("\t\t\t</resultHandlers>");
                    WriteToStream("\t\t</HTTPServiceInvoker>");
                    WriteToStream("\t</EventHandlers>");

                    if (i != (this.methods.Count - 1))
                        WriteToStream("");
                }
            }
        }

        private string GetParameterString(MethodInfo method)
        {
            string parameterString = string.Empty;
            ParameterInfo[] parameters = method.GetParameters();

            for (int i = 0; i < parameters.Length; i++)
            {
                ParameterInfo parameter = parameters[i];
                parameterString += string.Format("event.{0}", parameter.Name);

                if (i != (parameters.Length - 1))
                    parameterString += ", ";
            }

            return parameterString;
        }

        private void WriteClassEnd()
        {
            for (int i = 0; i < this.methods.Count; i++)
            {
                MethodInfo methodInfo = this.methods[i];

                if (methodInfo.GetCustomAttributes(typeof(ClientLocalStorageFlex), false).Length > 0)
                {
                    this.WriteToStream("\t<mx:HTTPService id=\"{0}Service\" url=\"assets/data/{0}.xml\" resultFormat=\"e4x\" />", methodInfo.Name);
                }
            }

            WriteToStream("");
            WriteToStream("\t<!-- End of Local service Map -->");
            WriteToStream("");
            WriteToStream("</EventMap>");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
