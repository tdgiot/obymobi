﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Flex
{
    public class ModelToEntityWriter
    {
        private Object model;
        private Type modelType;
        private StreamWriter stream;
        private PropertyInfo[] fields;

        public ModelToEntityWriter(Object model, StreamWriter stream)
        {
            this.model = model;
            this.modelType = model.GetType();
            this.stream = stream;

            PropertyInfo[] propertyInfo = Dionysos.Reflection.Member.GetPropertyInfo(model);
            List<PropertyInfo> propertyInfoList = new List<PropertyInfo>();
            for (int j = 0; j < propertyInfo.Length; j++)
            {
                PropertyInfo property = propertyInfo[j];

                // Check whether the property has a IncludeInCodeGeneratorForFlex
                if (property.GetCustomAttributes(typeof(IncludeInCodeGeneratorForFlex), false).Length > 0)
                {
                    propertyInfoList.Add(property);
                }
            }

            this.fields = propertyInfoList.ToArray();
        }

        public void WriteEntity()
        {
            this.WriteIntroduction();
            this.WriteClassStart();
            this.WriteClassEnd();
        }

        private void WriteIntroduction()
        {
            WriteToStream("/*");
            WriteToStream(" * {0}Entity.as", this.modelType.Name);
            WriteToStream(" * Generated entity class for {0}", this.modelType.FullName);
            WriteToStream(" */");
            WriteToStream("");
            WriteToStream("package com.data.obymobi.entityclasses");
            WriteToStream("{{");
            WriteToStream("\timport com.data.obymobi.entitybaseclasses.{0}EntityBase;", this.modelType.Name);
            WriteToStream("");
        }

        private void WriteClassStart()
        {
            WriteToStream("\tpublic class {0}Entity extends {0}EntityBase", this.modelType.Name);
            WriteToStream("\t{{");
            WriteToStream("\t\t// ------------------  CONSTRUCTORS ------------------");
            WriteToStream("\t\t/**");
            WriteToStream("\t\t* Constructor, initializes the type class");
            WriteToStream("\t\t*/");
            WriteToStream("\t\tpublic function {0}Entity()", this.modelType.Name);
            WriteToStream("\t\t{{");
            WriteToStream("\t\t}}");
        }

        private void WriteClassEnd()
        {
            WriteToStream("\t}}");
            WriteToStream("}}");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
