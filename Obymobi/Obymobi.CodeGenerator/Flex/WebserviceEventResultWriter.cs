﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Flex
{
    public class WebserviceEventResultWriter
    {
        private StreamWriter stream;
        private MethodInfo method;
        private ParameterInfo[] parameters;
        private Type type;

        public WebserviceEventResultWriter(StreamWriter stream, MethodInfo method)
        {
            this.stream = stream;
            this.method = method;
            this.parameters = this.method.GetParameters();
            this.type = (this.method.ReturnType.GetGenericArguments().Length > 0 ? this.method.ReturnType.GetGenericArguments()[0] : this.method.ReturnType);
        }

        public void Write()
        {
            this.WriteToStream("package com.net.obymobi.events");
            this.WriteToStream("{{");
	        this.WriteToStream("\timport com.data.obymobi.collectionclasses.*;");
	        this.WriteToStream("\timport com.logic.obymobi.ObyTypedResult;");
	        this.WriteToStream("");
	        this.WriteToStream("\timport flash.events.Event;");
            this.WriteToStream("");
	        this.WriteToStream("\tpublic class {0}ResultEvent extends Event", this.method.Name);
	        this.WriteToStream("\t{{");
		    this.WriteToStream("\t\t// ------------------  CONSTRUCTORS ------------------");
            this.WriteToStream("");
            this.WriteToStream("\t\tpublic function {0}ResultEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false)", this.method.Name);
		    this.WriteToStream("\t\t{{");
			this.WriteToStream("\t\t\tsuper(type, bubbles, cancelable);");
            this.WriteToStream("\t\t}}");
            this.WriteToStream("");
		    this.WriteToStream("\t\t// ------------------  FIELDS ------------------");
            this.WriteToStream("");
            this.WriteToStream("\t\tprivate var _obyTypedResult:ObyTypedResult;");
            this.WriteToStream("\t\tprivate var _{0}Collection:{1}Collection;", StringUtil.TurnFirstToLower(this.type.Name, false), this.type.Name);
            this.WriteToStream("");
		    this.WriteToStream("\t\t// ------------------  PROPERTIES ------------------");
		    this.WriteToStream("");
            this.WriteToStream("\t\tpublic function get Result():ObyTypedResult");
		    this.WriteToStream("\t\t{{");
			this.WriteToStream("\t\t\treturn this._obyTypedResult;");
		    this.WriteToStream("\t\t}}");
            this.WriteToStream("");
            this.WriteToStream("\t\tpublic function set Result(value:ObyTypedResult):void");
		    this.WriteToStream("\t\t{{");
			this.WriteToStream("\t\t\tthis._obyTypedResult = value;");
		    this.WriteToStream("\t\t}}");
            this.WriteToStream("");
            this.WriteToStream("\t\tpublic function get EntityCollection():{0}Collection", this.type.Name);
		    this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\tthis._{0}Collection = this._obyTypedResult.entityCollection as {1}Collection;", StringUtil.TurnFirstToLower(this.type.Name, false), this.type.Name);
            this.WriteToStream("\t\t\treturn this._{0}Collection;", StringUtil.TurnFirstToLower(this.type.Name, false));
            this.WriteToStream("\t\t}}");
            this.WriteToStream("");
            this.WriteToStream("\t\tpublic function set EntityCollection(value:{0}Collection):void", this.type.Name);
		    this.WriteToStream("\t\t{{");
			this.WriteToStream("\t\t\tthis._{0}Collection = value;", StringUtil.TurnFirstToLower(this.type.Name, false));
            this.WriteToStream("\t\t}}");
            this.WriteToStream("\t}}");
            this.WriteToStream("}}");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
