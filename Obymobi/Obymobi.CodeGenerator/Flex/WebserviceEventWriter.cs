﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Dionysos;
using Obymobi.Attributes;

namespace Obymobi.CodeGenerator.Flex
{
    public class WebserviceEventWriter
    {
        private StreamWriter stream;
        private MethodInfo method;
        private ParameterInfo[] parameters;

        public WebserviceEventWriter(StreamWriter stream, MethodInfo method)
        {
            this.stream = stream;
            this.method = method;
            this.parameters = this.method.GetParameters();
        }

        public void Write()
        {
            this.WriteIntroduction();
            this.WriteConstants();
            this.WriteFields();
            this.WriteMethods();
            this.WriteClassEnd();
        }

        private void WriteIntroduction()
        {
            WriteToStream("package com.net.obymobi.events");
            WriteToStream("{{");
	        WriteToStream("\timport flash.events.Event;");
            WriteToStream("\timport flash.events.IEventDispatcher;");
	        WriteToStream("");
            WriteToStream("\tpublic class {0}Event extends Event", this.method.Name);
            WriteToStream("\t{{");
        }

        private void WriteConstants()
        {
            this.WriteToStream("\t\t// ------------------  CONSTANTS ------------------");
            this.WriteToStream("");
            this.WriteToStream("\t\tpublic static const MethodName:String = \"{0}\";", this.method.Name);
            this.WriteToStream("");
        }

        private void WriteFields()
        {
            this.WriteToStream("\t\t// ------------------  FIELDS ------------------");
            this.WriteToStream("");

            for (int i = 0; i < this.parameters.Length; i++)
            {
                ParameterInfo parameter = this.parameters[i];
                this.WriteToStream("\t\tpublic var {0}:{1};", parameter.Name, FlexHelper.GetFlexDataType(parameter.ParameterType));
            }
            this.WriteToStream("");
        }

        private void WriteMethods()
        {
            this.WriteToStream("\t\t// ------------------  METHODS ------------------");
            this.WriteToStream("");
            this.WriteToStream("\t\tpublic function {0}Event(type:String, bubbles:Boolean=true, cancelable:Boolean=false)", this.method.Name);
		    this.WriteToStream("\t\t{{");
			this.WriteToStream("\t\t\tsuper(type, bubbles, cancelable);");
		    this.WriteToStream("\t\t}}");
            
            this.WriteToStream("");

            string parameterString = "dispatcher:IEventDispatcher";
            for (int i = 0; i < this.parameters.Length; i++)
            {
                ParameterInfo parameter = this.parameters[i];
                parameterString += string.Format(", {0}:{1}", parameter.Name, FlexHelper.GetFlexDataType(parameter.ParameterType));
            }

            this.WriteToStream("\t\tpublic static function Announce({0}):void", parameterString);
            this.WriteToStream("\t\t{{");
            this.WriteToStream("\t\t\tvar event:{0}Event = new {0}Event(MethodName);", this.method.Name);
            for (int i = 0; i < this.parameters.Length; i++)
            {
                ParameterInfo parameter = this.parameters[i];
                this.WriteToStream("\t\t\tevent.{0} = {0}", parameter.Name);
            }

            this.WriteToStream("");
            this.WriteToStream("\t\t\tdispatcher.dispatchEvent(event);");
            this.WriteToStream("\t\t}}");
        }

        private void WriteClassEnd()
        {
            WriteToStream("\t}}");
            WriteToStream("}}");
        }

        private void WriteToStream(string message, params object[] args)
        {
            this.stream.WriteLine(string.Format(message, args));
        }
    }
}
