﻿using System;
using System.Collections.Generic;
using System.Linq;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Dionysos.Data.Extensions;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;
using Obymobi.Enums;
using Obymobi.Interfaces;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class UpdateAlterationoptionWithProductUseCase : UseCaseBase<UpdateAlterationoptionWithProductRequest, SimpleResponse>
    {
        private readonly UpdateAlterationoptionFieldsWithProductUseCase updateAlterationoptionFieldsWithProductUseCase;
        
        private static readonly Dictionary<CustomTextType, CustomTextType> CustomTextMapping = new Dictionary<CustomTextType, CustomTextType>
        {
            { CustomTextType.ProductName, CustomTextType.AlterationoptionName }
        };

        public UpdateAlterationoptionWithProductUseCase()
        {
            this.updateAlterationoptionFieldsWithProductUseCase = new UpdateAlterationoptionFieldsWithProductUseCase();
        }
        
        public override SimpleResponse Execute(UpdateAlterationoptionWithProductRequest request)
        {
            AlterationoptionEntity alterationoption = request.Alterationoption;
            ProductEntity product = request.Product;

            if (alterationoption.IsNew || product.IsNew)
            {
                return new SimpleResponse{ Success = false, Message = "Unknown alterationoption or product" };
            }

            this.UpdateAlterationoptionFields(alterationoption, product);
            this.UpdateCustomTexts(alterationoption, product);
            this.UpdateTags(alterationoption, product);
            
            return new SimpleResponse { Success = true };
        }

        private void UpdateAlterationoptionFields(AlterationoptionEntity alterationoption, ProductEntity product)
        {
            this.updateAlterationoptionFieldsWithProductUseCase.Execute(new UpdateAlterationoptionFieldsWithProductRequest
            {
                Alterationoption = alterationoption,
                Product = product
            });
        }

        private void UpdateTags(AlterationoptionEntity alterationoption, ProductEntity product)
        {
            if (!alterationoption.ProductId.HasValue && alterationoption.IsChanged(AlterationoptionFields.ProductId))
            {
                    AlterationoptionTagCollection tagCollection = alterationoption.AlterationoptionTagCollection;
                    ICollection<ITag> productTags = product.GetTags();

                    ICollection<int> tagIdsToRemove = productTags.Select(tag => tag.TagId).ToList();

                    AlterationoptionTagCollection trackerCollection = new AlterationoptionTagCollection();
                    tagCollection.RemovedEntitiesTracker = trackerCollection;

                    foreach (int tagId in tagIdsToRemove)
                    {
                        AlterationoptionTagEntity tagEntity = tagCollection.FirstOrDefault(x => x.TagId == tagId);
                        if (tagEntity == null || tagEntity.IsNew) continue;

                        tagCollection.Remove(tagEntity);
                    }

                    trackerCollection.AddToTransaction(alterationoption);
                    trackerCollection.DeleteMulti();
            }
            else
            {
                ICollection<ITag> alterationOptionTags = alterationoption.GetTags();
                ICollection<ITag> productTags = product.GetTags();

                foreach (ITag productTag in productTags)
                {
                    if (alterationOptionTags.Any(t => t.Name.Equals(productTag.Name, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        continue;
                    }

                    AlterationoptionTagEntity newTag = new AlterationoptionTagEntity();
                    newTag.AlterationoptionId = alterationoption.AlterationoptionId;
                    newTag.TagId = productTag.TagId;

                    newTag.AddToTransaction(alterationoption);
                    newTag.Save();
                }
            }
        }

        private void UpdateCustomTexts(AlterationoptionEntity alterationoption, ProductEntity product)
        {
            if (!alterationoption.ProductId.HasValue)
            {
                return;
            }

            foreach (CustomTextEntity productCustomText in product.CustomTextCollection)
            {
                if (CustomTextMapping.TryGetValue(productCustomText.Type, out CustomTextType alterationoptionCustomTextType))
                {
                    CustomTextEntity alterationoptionCustomText = alterationoption.CustomTextCollection.FirstOrDefault(x => x.Type == alterationoptionCustomTextType && x.CultureCode == productCustomText.CultureCode);
                    if (alterationoptionCustomText == null)
                    {
                        alterationoption.CustomTextCollection.Add(new CustomTextEntity
                        {
                            CultureCode = productCustomText.CultureCode,
                            Type = alterationoptionCustomTextType,
                            Text = productCustomText.Text
                        });
                    }
                    else if (!alterationoptionCustomText.Text.Equals(productCustomText.Text, StringComparison.InvariantCultureIgnoreCase))
                    {
                        alterationoptionCustomText.Text = productCustomText.Text;
                    }
                }
            }

            alterationoption.CustomTextCollection.AddToTransaction(alterationoption);
            alterationoption.CustomTextCollection.SaveMulti();
        }
    }
}
