﻿using System;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class UpdateAlterationoptionFieldsWithProductUseCase : UseCaseBase<UpdateAlterationoptionFieldsWithProductRequest, SimpleResponse>
    {
        public override SimpleResponse Execute(UpdateAlterationoptionFieldsWithProductRequest request)
        {
            AlterationoptionEntity alterationoption = request.Alterationoption;
            ProductEntity product = request.Product;

            alterationoption.Name = alterationoption.InheritName ? product.Name : alterationoption.Name;
            if (!alterationoption.ExternalProductId.HasValue)
            {
                alterationoption.PriceIn = alterationoption.InheritPrice ? product.PriceIn : alterationoption.PriceIn;
            }
            alterationoption.TaxTariffId = alterationoption.InheritTaxTariff ? product.TaxTariffId : alterationoption.TaxTariffId;
            alterationoption.IsAlcoholic = alterationoption.InheritIsAlcoholic ? product.IsAlcoholic : alterationoption.IsAlcoholic;
            
            return new SimpleResponse { Success = true };
        }
    }
}
