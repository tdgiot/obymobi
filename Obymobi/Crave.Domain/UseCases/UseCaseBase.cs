﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public abstract class UseCaseBase<TRequest, TResponse>
    {
        /// <summary>
        /// Execute use case logic with <see cref="T:TRequest" /> as input parameter.
        /// Returns <see cref="T:TResponse" /> object once the execution completed.
        /// </summary>
        /// <param name="request">The <see cref="T:TRequest" /> used as input parameter</param>
        /// <returns>Result of the use case of type <typeparamref name="TResponse"/></returns>
        public abstract TResponse Execute(TRequest request);
    }
}
