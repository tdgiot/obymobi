﻿using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases.Requests
{
    public class UpdateAlterationoptionFieldsWithExternalProductRequest
    {
        public AlterationoptionEntity Alterationoption { get; set; }

        public ExternalProductEntity ExternalProduct { get; set; }
    }
}
