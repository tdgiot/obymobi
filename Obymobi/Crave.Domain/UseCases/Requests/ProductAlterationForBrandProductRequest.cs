﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Domain.UseCases.Requests
{
    public class ProductAlterationForBrandProductRequest
    {
        public int BrandProductId { get; set; }
        public ProductAlterationEntity ProductAlteration { get; set; }
    }
}
