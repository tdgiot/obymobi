﻿using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases.Requests
{
    public class RemoveBrandProductFromProductRequest
    {
        public ProductEntity Product { get; set; }
    }
}
