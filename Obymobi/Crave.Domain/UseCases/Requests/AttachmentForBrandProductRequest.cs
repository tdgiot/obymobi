﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Domain.UseCases.Requests
{
    public class AttachmentForBrandProductRequest
    {
        public int BrandProductId { get; set; }
        public AttachmentEntity Attachment { get; set; }
    }
}
