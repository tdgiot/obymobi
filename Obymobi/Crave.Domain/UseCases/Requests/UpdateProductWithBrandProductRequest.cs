﻿using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases.Requests
{
    public class UpdateProductWithBrandProductRequest
    {
        public ProductEntity Product { get; set; }
        public ProductEntity BrandProduct { get; set; }
    }
}
