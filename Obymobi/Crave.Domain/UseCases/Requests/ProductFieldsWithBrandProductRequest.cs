﻿using Obymobi.Data.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Crave.Domain.UseCases.Requests
{
    public class ProductFieldsWithBrandProductRequest
    {
        public ProductEntity Product { get; set; }
        public ProductEntity BrandProduct { get; set; }
    }
}
