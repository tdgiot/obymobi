﻿using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases.Requests
{
    public class UpdateAlterationoptionWithProductRequest
    {
        public AlterationoptionEntity Alterationoption { get; set; }

        public ProductEntity Product { get; set; }
    }
}
