﻿using System;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class UpdateProductFieldsWithBrandProductUseCase : UseCaseBase<ProductFieldsWithBrandProductRequest, SimpleResponse>
    {
        public override SimpleResponse Execute(ProductFieldsWithBrandProductRequest request)
        {
            ProductEntity product = request.Product;
            ProductEntity brandProduct = request.BrandProduct;

            product.Name = product.InheritName ? brandProduct.Name : product.Name;
            product.SubType = brandProduct.SubType;
            product.PriceIn = product.InheritPrice ? brandProduct.PriceIn : product.PriceIn;
            product.ButtonText = product.InheritButtonText ? brandProduct.ButtonText : product.ButtonText;
            product.CustomizeButtonText = product.InheritCustomizeButtonText ? brandProduct.CustomizeButtonText : product.CustomizeButtonText;
            product.WebTypeTabletUrl = product.InheritWebTypeTabletUrl ? brandProduct.WebTypeTabletUrl : product.WebTypeTabletUrl;
            product.WebTypeSmartphoneUrl = product.InheritWebTypeSmartphoneUrl ? brandProduct.WebTypeSmartphoneUrl : product.WebTypeSmartphoneUrl;
            product.Description = product.InheritDescription ? brandProduct.Description : product.Description;
            product.Color = product.InheritColor ? brandProduct.Color : product.Color;
            product.ViewLayoutType = brandProduct.ViewLayoutType;
            product.DeliveryLocationType = brandProduct.DeliveryLocationType;
            
            return new SimpleResponse { Success = true };
        }        
    }
}
