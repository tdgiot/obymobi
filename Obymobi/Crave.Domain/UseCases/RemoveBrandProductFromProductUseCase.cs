﻿using System;
using System.Linq;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class RemoveBrandProductFromProductUseCase : UseCaseBase<RemoveBrandProductFromProductRequest, SimpleResponse>
    {
        public override SimpleResponse Execute(RemoveBrandProductFromProductRequest request)
        {
            ProductEntity product = request.Product;

            if (product.IsNew)
            {
                return new SimpleResponse{ Success = false, Message = "Unknown product" };
            }

            this.RemoveProductAlterations(product);
            this.RemoveProductAttachments(product);
            this.RemoveMediaRelationships(product);
            
            return new SimpleResponse { Success = true };
        }

        private void RemoveProductAlterations(ProductEntity product)
        {
            if (!product.InheritAlterationsFromBrand)
                return;

            product.ProductAlterationCollection.AddToTransaction(product);
            product.ProductAlterationCollection.DeleteMulti();
        }

        private void RemoveProductAttachments(ProductEntity product)
        {
            if (!product.InheritAttachmentsFromBrand)
                return;

            product.ProductAttachmentCollection.AddToTransaction(product);
            product.ProductAttachmentCollection.DeleteMulti();
        }

        private void RemoveMediaRelationships(ProductEntity product)
        {
            if (!product.InheritMedia)
                return;

            product.MediaRelationshipCollection.AddToTransaction(product);
            product.MediaRelationshipCollection.DeleteMulti();
        }
    }
}
