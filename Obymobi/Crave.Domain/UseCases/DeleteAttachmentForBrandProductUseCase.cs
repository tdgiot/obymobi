﻿using System;
using System.Linq;
using Crave.Data.DataSources;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class DeleteAttachmentForBrandProductUseCase : UseCaseBase<AttachmentForBrandProductRequest, SimpleResponse>
    {
        private readonly LLBLGenProductDataSource dataSource;

        public DeleteAttachmentForBrandProductUseCase()
        {
            this.dataSource = new LLBLGenProductDataSource();
        }

        public override SimpleResponse Execute(AttachmentForBrandProductRequest request)
        {
            AttachmentEntity brandAttachment = request.Attachment;

            ProductCollection productCollection = this.dataSource.GetProductsByBrandProductId(request.BrandProductId);            

            foreach (ProductEntity product in productCollection)
            {
                if (!product.InheritAttachmentsFromBrand)
                    continue;

                ProductAttachmentEntity productAttachment = product.ProductAttachmentCollection.FirstOrDefault(x => x.AttachmentId == brandAttachment.AttachmentId);
                if (productAttachment != null)
                {
                    productAttachment.AddToTransaction(brandAttachment);
                    productAttachment.Delete();
                }
            }
            
            return new SimpleResponse { Success = true };
        }        
    }
}
