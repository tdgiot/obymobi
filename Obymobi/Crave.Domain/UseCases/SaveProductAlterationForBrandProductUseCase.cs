﻿using System;
using System.Linq;
using Crave.Data.DataSources;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class SaveProductAlterationForBrandProductUseCase : UseCaseBase<ProductAlterationForBrandProductRequest, SimpleResponse>
    {
        private readonly LLBLGenProductDataSource dataSource;

        public SaveProductAlterationForBrandProductUseCase()
        {
            this.dataSource = new LLBLGenProductDataSource();
        }

        public override SimpleResponse Execute(ProductAlterationForBrandProductRequest request)
        {
            ProductAlterationEntity brandProductAlteration = request.ProductAlteration;

            ProductCollection productCollection = this.dataSource.GetProductsByBrandProductId(request.BrandProductId);

            foreach (ProductEntity product in productCollection)
            {
                if (!product.InheritAlterationsFromBrand)
                    continue;

                ProductAlterationEntity productAlteration = product.ProductAlterationCollection.FirstOrDefault(x => x.SortOrder == brandProductAlteration.SortOrder);
                if (productAlteration == null)
                {
                    productAlteration = new ProductAlterationEntity
                    {
                        ProductId = product.ProductId,
                        AlterationId = brandProductAlteration.AlterationId,
                        Version = brandProductAlteration.Version,                        
                        SortOrder = brandProductAlteration.SortOrder
                    };
                    productAlteration.AddToTransaction(brandProductAlteration);
                    productAlteration.Save();
                }
                else if (productAlteration.AlterationId != brandProductAlteration.AlterationId)
                {
                    productAlteration.AlterationId = brandProductAlteration.AlterationId;
                    productAlteration.AddToTransaction(brandProductAlteration);
                    productAlteration.Save();
                }
            }
            
            return new SimpleResponse { Success = true };
        }        
    }
}
