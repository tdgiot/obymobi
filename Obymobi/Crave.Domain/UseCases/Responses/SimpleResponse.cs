﻿namespace Crave.Domain.UseCases.Responses
{
    public class SimpleResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}
