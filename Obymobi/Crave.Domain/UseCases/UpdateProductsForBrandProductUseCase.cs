﻿using System;
using Crave.Data.DataSources;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class UpdateProductsForBrandProductUseCase : UseCaseBase<UpdateProductsForBrandProductRequest, SimpleResponse>
    {
        private readonly UpdateProductFieldsWithBrandProductUseCase updateProductFieldsWithBrandProductUseCase;
        private readonly LLBLGenProductDataSource dataSource;

        public UpdateProductsForBrandProductUseCase()
        {
            this.updateProductFieldsWithBrandProductUseCase = new UpdateProductFieldsWithBrandProductUseCase();
            this.dataSource = new LLBLGenProductDataSource();
        }

        public override SimpleResponse Execute(UpdateProductsForBrandProductRequest request)
        {
            ProductEntity brandProduct = request.BrandProduct;

            ProductCollection productCollection = this.dataSource.GetProductsByBrandProductId(request.BrandProduct.ProductId);

            foreach (ProductEntity product in productCollection)
            {
                this.updateProductFieldsWithBrandProductUseCase.Execute(new ProductFieldsWithBrandProductRequest
                {
                    Product = product,
                    BrandProduct = brandProduct
                });

                product.ValidatorPreventBrandProductUpdate = true;
                product.AddToTransaction(brandProduct);
                product.Save();
            }
            
            return new SimpleResponse { Success = true };
        }        
    }
}
