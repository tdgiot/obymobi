﻿using System;
using System.Linq;
using Crave.Data.DataSources;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class SaveAttachmentForBrandProductUseCase : UseCaseBase<AttachmentForBrandProductRequest, SimpleResponse>
    {
        private readonly LLBLGenProductDataSource dataSource;

        public SaveAttachmentForBrandProductUseCase()
        {
            this.dataSource = new LLBLGenProductDataSource();
        }

        public override SimpleResponse Execute(AttachmentForBrandProductRequest request)
        {
            AttachmentEntity brandAttachment = request.Attachment;

            ProductCollection productCollection = this.dataSource.GetProductsByBrandProductId(request.BrandProductId);

            foreach (ProductEntity product in productCollection)
            {
                if (!product.InheritAttachmentsFromBrand)
                    continue;

                ProductAttachmentEntity productAttachment = product.ProductAttachmentCollection.FirstOrDefault(x => x.AttachmentId == brandAttachment.AttachmentId);
                if (productAttachment == null)
                {
                    productAttachment = new ProductAttachmentEntity
                    {
                        ProductId = product.ProductId,
                        AttachmentId = brandAttachment.AttachmentId
                    };
                    productAttachment.AddToTransaction(brandAttachment);
                    productAttachment.Save();
                }
            }
            
            return new SimpleResponse { Success = true };
        }        
    }
}
