﻿using System;
using System.Linq;
using Crave.Data.DataSources;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class DeleteProductAlterationForBrandProductUseCase : UseCaseBase<ProductAlterationForBrandProductRequest, SimpleResponse>
    {
        private readonly LLBLGenProductDataSource dataSource;

        public DeleteProductAlterationForBrandProductUseCase()
        {
            this.dataSource = new LLBLGenProductDataSource();
        }

        public override SimpleResponse Execute(ProductAlterationForBrandProductRequest request)
        {
            ProductAlterationEntity brandProductAlteration = request.ProductAlteration;

            ProductCollection productCollection = this.dataSource.GetProductsByBrandProductId(request.BrandProductId);            

            foreach (ProductEntity product in productCollection)
            {
                if (!product.InheritAlterationsFromBrand)
                    continue;

                ProductAlterationEntity productAlteration = product.ProductAlterationCollection.FirstOrDefault(x => x.AlterationId == brandProductAlteration.AlterationId);
                if (productAlteration != null)
                {
                    productAlteration.AddToTransaction(brandProductAlteration);
                    productAlteration.Delete();
                }
            }
            
            return new SimpleResponse { Success = true };
        }        
    }
}
