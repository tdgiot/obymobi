﻿using System;
using System.Linq;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;
using Obymobi.Data.HelperClasses;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class UpdateProductWithBrandProductUseCase : UseCaseBase<UpdateProductWithBrandProductRequest, SimpleResponse>
    {
        private readonly UpdateProductFieldsWithBrandProductUseCase updateProductFieldsWithBrandProductUseCase;

        public UpdateProductWithBrandProductUseCase()
        {
            this.updateProductFieldsWithBrandProductUseCase = new UpdateProductFieldsWithBrandProductUseCase();
        }

        public override SimpleResponse Execute(UpdateProductWithBrandProductRequest request)
        {
            ProductEntity product = request.Product;
            ProductEntity brandProduct = request.BrandProduct;

            if (product.IsNew || brandProduct.IsNew)
            {
                return new SimpleResponse{ Success = false, Message = "Unknown product or brand product" };
            }

            this.UpdateProductFields(product, brandProduct);
            this.UpdateProductAlterations(product, brandProduct);
            this.UpdateProductAttachments(product, brandProduct);
            this.UpdateMediaRelationships(product, brandProduct);
            this.UpdateCustomTexts(product, brandProduct);
            
            return new SimpleResponse { Success = true };
        }

        private void UpdateProductFields(ProductEntity product, ProductEntity brandProduct)
        {
            this.updateProductFieldsWithBrandProductUseCase.Execute(new ProductFieldsWithBrandProductRequest
            {
                Product = product,
                BrandProduct = brandProduct
            });            
        }

        private void UpdateProductAlterations(ProductEntity product, ProductEntity brandProduct)
        {
            ProductAlterationCollection productAlterationsToDelete = new ProductAlterationCollection();
            ProductAlterationCollection productAlterationsToSave = new ProductAlterationCollection();

            if (product.InheritAlterationsFromBrand)
            {
                productAlterationsToDelete.AddRange(product.ProductAlterationCollection
                                                           .Where(x => !brandProduct.ProductAlterationCollection.Select(y => y.AlterationId).Contains(x.AlterationId)));

                productAlterationsToSave.AddRange(brandProduct.ProductAlterationCollection
                                                              .Where(x => !product.ProductAlterationCollection.Select(y => y.AlterationId).Contains(x.AlterationId))
                                                              .Select(z => new ProductAlterationEntity { AlterationId = z.AlterationId, Version = z.Version, SortOrder = z.SortOrder, ProductId = product.ProductId }));
            }
            else if (product.Fields[ProductFields.InheritAlterationsFromBrand.Name].IsChanged)
            {
                productAlterationsToDelete.AddRange(product.ProductAlterationCollection);
            }

            productAlterationsToDelete.AddToTransaction(product);
            productAlterationsToDelete.DeleteMulti();

            productAlterationsToSave.AddToTransaction(product);
            productAlterationsToSave.SaveMulti();
        }

        private void UpdateProductAttachments(ProductEntity product, ProductEntity brandProduct)
        {
            ProductAttachmentCollection productAttachmentsToDelete = new ProductAttachmentCollection();
            ProductAttachmentCollection productAttachmentsToSave = new ProductAttachmentCollection();

            if (product.InheritAttachmentsFromBrand)
            {
                productAttachmentsToDelete.AddRange(product.ProductAttachmentCollection
                                                           .Where(x => !brandProduct.AttachmentCollection.Select(y => y.AttachmentId).Contains(x.AttachmentId)));

                productAttachmentsToSave.AddRange(brandProduct.AttachmentCollection
                                                              .Where(x => !product.ProductAttachmentCollection.Select(y => y.AttachmentId).Contains(x.AttachmentId))
                                                              .Select(z => new ProductAttachmentEntity { AttachmentId = z.AttachmentId, ProductId = product.ProductId }));
            }
            else if (product.Fields[ProductFields.InheritAttachmentsFromBrand.Name].IsChanged)
            {
                productAttachmentsToDelete.AddRange(product.ProductAttachmentCollection);
            }

            productAttachmentsToDelete.AddToTransaction(product);
            productAttachmentsToDelete.DeleteMulti();

            productAttachmentsToSave.AddToTransaction(product);
            productAttachmentsToSave.SaveMulti();
        }

        private void UpdateMediaRelationships(ProductEntity product, ProductEntity brandProduct)
        {
            MediaRelationshipCollection mediaToDelete = new MediaRelationshipCollection();
            MediaRelationshipCollection mediaToSave = new MediaRelationshipCollection();

            if (product.InheritMedia)
            {
                mediaToDelete.AddRange(product.MediaRelationshipCollection
                                              .Where(x => !brandProduct.MediaCollection.Select(y => y.MediaId).Contains(x.MediaId)));

                mediaToSave.AddRange(brandProduct.MediaCollection
                                                 .Where(x => !product.MediaRelationshipCollection.Select(y => y.MediaId).Contains(x.MediaId))
                                                 .Select(z => new MediaRelationshipEntity { MediaId = z.MediaId, ProductId = product.ProductId }));
            }
            else if (product.Fields[ProductFields.InheritMedia.Name].IsChanged)
            {
                mediaToDelete.AddRange(product.MediaRelationshipCollection);
            }

            mediaToDelete.AddToTransaction(product);
            mediaToDelete.DeleteMulti();

            mediaToSave.AddToTransaction(product);
            mediaToSave.SaveMulti();
        }

        private void UpdateCustomTexts(ProductEntity product, ProductEntity brandProduct)
        {
            foreach (CustomTextEntity brandCustomText in brandProduct.CustomTextCollection)
            {
                CustomTextEntity productCustomText = product.CustomTextCollection.FirstOrDefault(x => x.CultureCode == brandCustomText.CultureCode && x.Type == brandCustomText.Type);
                if (productCustomText == null)
                {
                    product.CustomTextCollection.Add(new CustomTextEntity
                    {
                        CultureCode = brandCustomText.CultureCode,
                        Type = brandCustomText.Type,
                        Text = brandCustomText.Text
                    }); 
                }
                else if (!productCustomText.Text.Equals(brandCustomText.Text, StringComparison.InvariantCultureIgnoreCase))
                {
                    productCustomText.Text = brandCustomText.Text;                    
                }
            }

            product.CustomTextCollection.AddToTransaction(product);
            product.CustomTextCollection.SaveMulti();
        }
    }
}
