﻿using System;
using System.Linq;
using Crave.Data.DataSources;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class DeleteMediaForBrandProductUseCase : UseCaseBase<MediaForBrandProductRequest, SimpleResponse>
    {
        private readonly LLBLGenProductDataSource dataSource;

        public DeleteMediaForBrandProductUseCase()
        {
            this.dataSource = new LLBLGenProductDataSource();
        }

        public override SimpleResponse Execute(MediaForBrandProductRequest request)
        {
            MediaEntity brandMedia = request.Media;

            ProductCollection productCollection = this.dataSource.GetProductsByBrandProductId(request.BrandProductId);

            foreach (ProductEntity product in productCollection)
            {
                if (!product.InheritMedia)
                    continue;

                MediaRelationshipEntity mediaRelationship = product.MediaRelationshipCollection.FirstOrDefault(x => x.MediaId == brandMedia.MediaId);
                if (mediaRelationship != null)
                {
                    mediaRelationship.AddToTransaction(brandMedia);
                    mediaRelationship.Delete();
                }
            }
            
            return new SimpleResponse { Success = true };
        }        
    }
}
