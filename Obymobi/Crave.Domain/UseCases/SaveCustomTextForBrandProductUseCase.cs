﻿using System;
using System.Linq;
using Crave.Data.DataSources;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class SaveCustomTextForBrandProductUseCase : UseCaseBase<CustomTextForBrandProductRequest, SimpleResponse>
    {
        private readonly LLBLGenProductDataSource dataSource;

        public SaveCustomTextForBrandProductUseCase()
        {
            this.dataSource = new LLBLGenProductDataSource();
        }

        public override SimpleResponse Execute(CustomTextForBrandProductRequest request)
        {
            CustomTextEntity brandProductCustomText = request.CustomText;

            ProductCollection productCollection = this.dataSource.GetProductsByBrandProductId(request.BrandProductId);

            foreach (ProductEntity product in productCollection)
            {
                CustomTextEntity productCustomText = product.CustomTextCollection.FirstOrDefault(x => x.Type == brandProductCustomText.Type && x.CultureCode.Equals(brandProductCustomText.CultureCode, StringComparison.InvariantCultureIgnoreCase));
                if (productCustomText == null)
                {
                    productCustomText = new CustomTextEntity
                    {
                        ProductId = product.ProductId,
                        CultureCode = brandProductCustomText.CultureCode,
                        Type = brandProductCustomText.Type
                    };
                }

                productCustomText.Text = brandProductCustomText.Text;
                productCustomText.AddToTransaction(brandProductCustomText);
                productCustomText.Save();
            }
            
            return new SimpleResponse { Success = true };
        }        
    }
}
