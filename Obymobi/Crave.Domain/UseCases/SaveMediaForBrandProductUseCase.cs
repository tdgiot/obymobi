﻿using System;
using System.Linq;
using Crave.Data.DataSources;
using Crave.Domain.UseCases.Requests;
using Crave.Domain.UseCases.Responses;
using Obymobi.Data.CollectionClasses;
using Obymobi.Data.EntityClasses;

namespace Crave.Domain.UseCases
{
    [Serializable]
    public class SaveMediaForBrandProductUseCase : UseCaseBase<MediaForBrandProductRequest, SimpleResponse>
    {
        private readonly LLBLGenProductDataSource dataSource;

        public SaveMediaForBrandProductUseCase()
        {
            this.dataSource = new LLBLGenProductDataSource();
        }

        public override SimpleResponse Execute(MediaForBrandProductRequest request)
        {
            MediaEntity brandMedia = request.Media;

            ProductCollection productCollection = this.dataSource.GetProductsByBrandProductId(request.BrandProductId);

            foreach (ProductEntity product in productCollection)
            {
                if (!product.InheritMedia)
                    continue;

                MediaRelationshipEntity productMediaRelation = product.MediaRelationshipCollection.FirstOrDefault(x => x.MediaId == brandMedia.MediaId);
                if (productMediaRelation == null)
                {
                    productMediaRelation = new MediaRelationshipEntity
                    {
                        ProductId = product.ProductId,
                        MediaId = brandMedia.MediaId
                    };
                    productMediaRelation.AddToTransaction(brandMedia);
                    productMediaRelation.Save();
                }
            }
            
            return new SimpleResponse { Success = true };
        }        
    }
}
