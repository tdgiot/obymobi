﻿using System.Text;

namespace Obymobi.Printing.PrintLayouts
{
    public class RequestTimeoutFailurePrintLayout : PrintLayout
    {
        #region Constructors

        /// <summary>
        /// Constructs an instance of the ObymobiRequestService.Printing.RequestTimeoutFailurePrintLayout type
        /// </summary>
        public RequestTimeoutFailurePrintLayout(int widthInCharacters)
        {
            this.WidthInCharacters = widthInCharacters;
        }

        #endregion

        #region Methods

        public override string RenderOutput()
        {
            var markupBuilder = new StringBuilder();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine("{Warning}");
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            this.Markup = markupBuilder.ToString();

            this.SetText("{Warning}", "FOUTMELDING: De Obymobi webservice kon al lange tijd niet bereikt worden. Controleer internetverbinding.");

            return this.Markup;
        }

        #endregion
    }
}
