﻿using System.Text;
using Obymobi.Logic.Model;
using Obymobi.Printing.Fields;

namespace Obymobi.Printing.PrintLayouts
{
    public class CloseTransactionPrintLayout : PrintLayout
    {
        #region Fields

    	#endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ObymobiRequestService.Printing.CloseTransactionPrintLayout type
        /// </summary>
        public CloseTransactionPrintLayout(Order order, int widthInCharacters)
        {
            this.DataSource = order;
            this.WidthInCharacters = widthInCharacters;

            // Initialize the markup
            this.InitializeMarkup();
        }

        #endregion

        #region Methods

        private void InitializeMarkup()
        {
            var markupBuilder = new StringBuilder();

            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(GeneralFields.CompanyName);
            markupBuilder.AppendLine(GeneralFields.CompanyAddressline1);
            markupBuilder.AppendLine(GeneralFields.CompanyAddressline2);
            markupBuilder.AppendLine(GeneralFields.CompanyAddressline3);
            markupBuilder.AppendLine(GeneralFields.CompanyTelephone);
            markupBuilder.AppendLine(GeneralFields.CompanyFax);
            markupBuilder.AppendLine(GeneralFields.CompanyWebsite);
            markupBuilder.AppendLine(GeneralFields.CompanyEmail);
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(ReceiptFields.OrderId);
            markupBuilder.AppendLine(GeneralFields.CustomerNameFull);
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(GeneralFields.Line);
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(GeneralFields.HelpText);
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(TransactionFields.ConfirmationCode);
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(GeneralFields.Line);
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(this.CenterString("U heeft besteld via:"));
            markupBuilder.AppendLine();

            this.Markup = markupBuilder.ToString();
        }

        public override string RenderOutput()
        {
            this.SetText(GeneralFields.CompanyName, (PrintManager.CompanyName.Length > 0 ? this.CenterString(PrintManager.CompanyName) : string.Empty));
            this.SetText(GeneralFields.CompanyAddressline1, (PrintManager.CompanyAddressline1.Length > 0 ? this.CenterString(PrintManager.CompanyAddressline1) : string.Empty));
            this.SetText(GeneralFields.CompanyAddressline2, (PrintManager.CompanyAddressline2.Length > 0 ? this.CenterString(PrintManager.CompanyAddressline2) : string.Empty));
            this.SetText(GeneralFields.CompanyAddressline3, (PrintManager.CompanyAddressline3.Length > 0 ? this.CenterString(PrintManager.CompanyAddressline3) : string.Empty));
            this.SetText(GeneralFields.CompanyTelephone, (PrintManager.CompanyTelephone.Length > 0 ? this.CenterString(PrintManager.CompanyTelephone) : string.Empty));
            this.SetText(GeneralFields.CompanyFax, (PrintManager.CompanyFax.Length > 0 ? this.CenterString(PrintManager.CompanyFax) : string.Empty));
            this.SetText(GeneralFields.CompanyWebsite, (PrintManager.CompanyWebsite.Length > 0 ? this.CenterString(PrintManager.CompanyWebsite) : string.Empty));
            this.SetText(GeneralFields.CompanyEmail, (PrintManager.CompanyEmail.Length > 0 ? this.CenterString(PrintManager.CompanyEmail) : string.Empty));

            this.SetText(ReceiptFields.OrderId, string.Format("{0}{1}", "Bestelling:".PadRight(18), this.DataSource.OrderId));
            this.SetText(GeneralFields.CustomerNameFull, string.Format("{0}{1}", "Klant:".PadRight(18), this.DataSource.CustomerNameFull));

            this.SetText(GeneralFields.HelpText, "U dient de betaling van uw bestelling(en) te bevestigen door middel van een confirmatiecode. Deze code dient u in te voeren op uw telefoon. De code wordt hieronder weergegeven.");

            this.SetText(TransactionFields.ConfirmationCode, ("Confirmatiecode:".PadRight(this.WidthInCharacters - 5) + this.DataSource.ConfirmationCode));

            return this.Markup;
        }

        #endregion

        #region Properties

    	/// <summary>
    	/// Gets or sets the order for this receipt
    	/// </summary>
    	public Order DataSource { get; set; }

    	#endregion
    }
}
