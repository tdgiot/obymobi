﻿namespace Obymobi.Printing.PrintLayouts
{
    public abstract class PrintLayout
    {
        #region Fields

        private string markup = string.Empty;
        private int widthInCharacters = -1;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ObymobiRequestService.Printing.PrintLayout type
        /// </summary>
        protected PrintLayout()
        {
        }

        /// <summary>
        /// Constructs an instance of the ObymobiRequestService.Printing.PrintLayout type using the specified markup
        /// </summary>
        /// <param name="markup"></param>
        protected PrintLayout(string markup)
        {
            this.markup = markup;
        }

        #endregion

        #region Methods

        public abstract string RenderOutput();

        public void SetText(string field, string text)
        {
            if (text.Length <= this.WidthInCharacters)
                this.Markup = this.Markup.Replace(field, text);
            else
            {
                string[] words = text.Split(' ');
                string sentence = string.Empty;
                string temp = string.Empty;
                foreach (string t in words)
                {
                	if ((sentence.Length + t.Length + 1) > this.WidthInCharacters)
                	{
                		temp += this.CenterString(sentence) + "\r\n";
                		sentence = string.Empty;
                	}

                	sentence += t + " ";
                }

                if (sentence.Length > 0)
                    temp += this.CenterString(sentence);

                this.Markup = this.Markup.Replace(field, temp);
            }
        }

        public string CenterString(string stringToCenter)
        {
            int difference = this.WidthInCharacters - stringToCenter.Length;
            difference = difference / 2;

            return stringToCenter.PadLeft(stringToCenter.Length + difference);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the markup for this printlayout instance
        /// </summary>
        public string Markup
        {
            get
            {
                return this.markup;
            }
            set
            {
                this.markup = value;
            }
        }

        /// <summary>
        /// Gets the rendered output for the printlayout
        /// </summary>
        public string RenderedOutput
        {
            get
            {
                return this.RenderOutput();
            }
        }

        /// <summary>
        /// Gets or sets the width in characters of the receipt
        /// </summary>
        public int WidthInCharacters
        {
            get
            {
                return this.widthInCharacters;
            }
            set
            {
                this.widthInCharacters = value;
            }
        }

        #endregion
    }
}
