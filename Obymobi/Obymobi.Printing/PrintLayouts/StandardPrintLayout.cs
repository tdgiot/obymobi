﻿using System.Text;
using Obymobi.Logic.Model;
using Obymobi.Printing.Fields;

namespace Obymobi.Printing.PrintLayouts
{
    public class StandardPrintLayout : PrintLayout
    {
        #region Fields

    	#endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the ObymobiRequestService.Printing.StandardPrintLayout type
        /// </summary>
        public StandardPrintLayout(Order order, int widthInCharacters)
        {
            this.DataSource = order;
            this.WidthInCharacters = widthInCharacters;

            // Initialize the markup
            this.InitializeMarkup();
        }

        #endregion

        #region Methods

        private void InitializeMarkup()
        {
            var markupBuilder = new StringBuilder();

            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(GeneralFields.CompanyName);
            markupBuilder.AppendLine(GeneralFields.CompanyAddressline1);
            markupBuilder.AppendLine(GeneralFields.CompanyAddressline2);
            markupBuilder.AppendLine(GeneralFields.CompanyAddressline3);
            markupBuilder.AppendLine(GeneralFields.CompanyTelephone);
            markupBuilder.AppendLine(GeneralFields.CompanyFax);
            markupBuilder.AppendLine(GeneralFields.CompanyWebsite);
            markupBuilder.AppendLine(GeneralFields.CompanyEmail);
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(ReceiptFields.OrderId);
            markupBuilder.AppendLine(GeneralFields.CustomerNameFull);
            markupBuilder.AppendLine(ReceiptFields.DeliverypointNumber);
            markupBuilder.AppendLine(ReceiptFields.PaymentmethodName);
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(GeneralFields.Line);
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(ReceiptFields.Orderitems);
            markupBuilder.AppendLine(GeneralFields.Line);
            markupBuilder.AppendLine();
            //markupBuilder.AppendLine(ReceiptFields.PriceExTotal);
            //markupBuilder.AppendLine(ReceiptFields.PriceVatTotal);
            markupBuilder.AppendLine(ReceiptFields.PriceInTotal);
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(ReceiptFields.Notes);
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine();
            markupBuilder.AppendLine(this.CenterString("U heeft besteld via:"));
            markupBuilder.AppendLine();

            this.Markup = markupBuilder.ToString();
        }

        public override string RenderOutput()
        {
            this.SetText(GeneralFields.CompanyName, (PrintManager.CompanyName.Length > 0 ? this.CenterString(PrintManager.CompanyName) : string.Empty));
            this.SetText(GeneralFields.CompanyAddressline1, (PrintManager.CompanyAddressline1.Length > 0 ? this.CenterString(PrintManager.CompanyAddressline1) : string.Empty));
            this.SetText(GeneralFields.CompanyAddressline2, (PrintManager.CompanyAddressline2.Length > 0 ? this.CenterString(PrintManager.CompanyAddressline2) : string.Empty));
            this.SetText(GeneralFields.CompanyAddressline3, (PrintManager.CompanyAddressline3.Length > 0 ? this.CenterString(PrintManager.CompanyAddressline3) : string.Empty));
            this.SetText(GeneralFields.CompanyTelephone, (PrintManager.CompanyTelephone.Length > 0 ? this.CenterString(PrintManager.CompanyTelephone) : string.Empty));
            this.SetText(GeneralFields.CompanyFax, (PrintManager.CompanyFax.Length > 0 ? this.CenterString(PrintManager.CompanyFax) : string.Empty));
            this.SetText(GeneralFields.CompanyWebsite, (PrintManager.CompanyWebsite.Length > 0 ? this.CenterString(PrintManager.CompanyWebsite) : string.Empty));
            this.SetText(GeneralFields.CompanyEmail, (PrintManager.CompanyEmail.Length > 0 ? this.CenterString(PrintManager.CompanyEmail) : string.Empty));

            this.SetText(ReceiptFields.OrderId, string.Format("{0}{1}", "Bestelling:".PadRight(18), this.DataSource.OrderId));
            this.SetText(GeneralFields.CustomerNameFull, string.Format("{0}{1}", "Klant:".PadRight(18), this.DataSource.CustomerNameFull));
            this.SetText(ReceiptFields.DeliverypointNumber, string.Format("{0}{1}", "Tafel:".PadRight(18), this.DataSource.DeliverypointNumber));
            this.SetText(ReceiptFields.PaymentmethodName, string.Format("{0}{1}", "Betalingswijze:".PadRight(18), this.DataSource.PaymentmethodName));

            string orderitems = string.Empty;
            orderitems += string.Format("{0}{1}{2}{3}\r\n", "Aant", " Omschrijving".PadRight(23), "Prijs".PadLeft(8), "Totaal".PadLeft(9));
            orderitems += "\r\n\r\n";

            for (int i = 0; i < this.DataSource.Orderitems.Length; i++)
            {
                Orderitem orderitem = this.DataSource.Orderitems[i];
                orderitems += string.Format("{0} {1}€ {2} € {3}\r\n", orderitem.Quantity.ToString().PadLeft(4), orderitem.ProductName.PadRight(22), orderitem.PriceInPerPiece.ToString("F").PadLeft(6), orderitem.PriceIn.ToString("F").PadLeft(6));
            }
            this.Markup = this.Markup.Replace(ReceiptFields.Orderitems, orderitems);

            //this.SetText(ReceiptFields.PriceExTotal, string.Format("{0}€ {1}", "Subtotaal".PadRight(36), this.DataSource.PriceExTotalAsDecimal.ToString("F").PadLeft(6)));
            //this.SetText(ReceiptFields.PriceVatTotal, string.Format("{0}€ {1}", "BTW".PadRight(36), this.DataSource.PriceVatTotalAsDecimal.ToString("F").PadLeft(6)));
            this.SetText(ReceiptFields.PriceInTotal, string.Format("{0}€ {1}", "TOTAAL".PadRight(36), this.DataSource.PriceIn.ToString("F").PadLeft(6)));

            this.SetText(ReceiptFields.Notes, this.DataSource.Notes);

            return this.Markup;
        }

        #endregion

        #region Properties

    	/// <summary>
    	/// Gets or sets the order for this receipt
    	/// </summary>
    	public Order DataSource { get; set; }

    	#endregion
    }
}
