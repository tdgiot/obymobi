﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Management;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Dionysos;
using Obymobi.Logic.Model;
using Obymobi.Printing.PrintLayouts;
using Obymobi.Printing.Reports;
using PrintLayout = Obymobi.Printing.PrintLayouts.PrintLayout;

namespace Obymobi.Printing
{
	public class PrintManager
	{
		#region Fields

		static PrintManager instance;

		private string companyName = string.Empty;
		private string companyAddressline1 = string.Empty;
		private string companyAddressline2 = string.Empty;
		private string companyAddressline3 = string.Empty;
		private string companyTelephone = string.Empty;
		private string companyFax = string.Empty;
		private string companyWebsite = string.Empty;
		private string companyEmail = string.Empty;
		private string companyLogoFile = string.Empty;
		private string receiptTypesToPrint = string.Empty;

		private Font printFont;
		private StringReader stringReader;
		private PrintLayout printLayout;

		#endregion

		#region Constructors

		private PrintManager()
		{
		}

		/// <summary>
		/// Constructs an instance of the PrintManager class if the instance has not been initialized yet
		/// </summary>
		public static PrintManager GetSingleton()
		{
			return instance ?? (instance = new PrintManager());
		}

		#endregion

		#region Methods

		/// <summary>
		/// 
		/// </summary>
		/// <param name="receiptTypes"> </param>
		/// <param name="name"></param>
		/// <param name="addressline1"></param>
		/// <param name="addressline2"></param>
		/// <param name="addressline3"></param>
		/// <param name="telephone"></param>
		/// <param name="fax"></param>
		/// <param name="website"></param>
		/// <param name="email"></param>
		/// <param name="logoFile"></param>
		public void Init(string receiptTypes, string name, string addressline1, string addressline2, string addressline3, string telephone, string fax, string website, string email, string logoFile)
		{
			//// Initialize the static members
			//PrintFont = new Font("Courier", 10);

			receiptTypesToPrint = receiptTypes;
			CompanyName = name;
			CompanyAddressline1 = addressline1;
			CompanyAddressline2 = addressline2;
			CompanyAddressline3 = addressline3;
			CompanyTelephone = telephone;
			CompanyFax = fax;
			CompanyWebsite = website;
			CompanyEmail = email;
			CompanyLogoFile = logoFile;
		}

		public bool IsPrinterOnline(string printerName)
		{
			var searcher = new ManagementObjectSearcher(string.Format("SELECT * from Win32_Printer WHERE Name LIKE '%{0}'", printerName));
			ManagementObjectCollection coll = searcher.Get();

			foreach (ManagementObject printer in coll)
			{
				if (printer.Properties["PrinterStatus"].Value.ToString() != "1")
				{
					return true;
				}
			}

			return false;
		}

		public void Print(Order order, string[] printerNames)
		{
            var orders = new Order[] { order };

			foreach (string currentPrinter in printerNames)
			{
				try
				{
					if (ReceiptTypesToPrint.Contains("EndUser"))
						PrintEndUserReport(orders, currentPrinter);

					if (ReceiptTypesToPrint.Contains("InternalUse"))
						PrintInternalUseReport(orders, currentPrinter);
				}
				catch (Exception)
				{
					throw new FunctionalException("Printing of Order {0} on Printer {1} failed.", order.OrderId, currentPrinter);
				}
			}
		}

		public void PrintReport(XtraReport rep, string thePrinterName)
		{
			rep.CreateDocument();
			rep.PrintingSystem.ShowMarginsWarning = false;

			BrickEnumerator be = rep.PrintingSystem.Pages[0].GetEnumerator();
			float b = 0f;

			while (be.MoveNext())
			{
				var vb = (VisualBrick)be.CurrentBrick;
				if ((b < vb.Rect.Bottom))
				{
					b = vb.Rect.Bottom;
				}
			}

			if (rep.ReportUnit == ReportUnit.HundredthsOfAnInch)
			{
				b = XRConvert.Convert(b, 300, 100);
			}

			rep.PaperKind = PaperKind.Custom;
			rep.PageHeight = (int)b + 1 + rep.Margins.Top + rep.Margins.Bottom;
			rep.CreateDocument();

			if (string.IsNullOrEmpty(thePrinterName))
			{
				var myPrintTool = new ReportPrintTool(rep);
				myPrintTool.Print(thePrinterName);
			}
			else
			{
				try
				{
					var myPrintTool = new ReportPrintTool(rep);
					myPrintTool.Print(thePrinterName);
				}
				catch (Exception)
				{
					try
					{
						var myPrintTool = new ReportPrintTool(rep);
						myPrintTool.Print(thePrinterName);
					}
					catch (Exception ex2)
					{
						throw ex2;
					}
				}
			}
		}

		private void PrintEndUserReport(Order[] orders, string printerName)
		{
			var report = new OpenTransactionReport();
			report.DataSource = orders;
			report.CreateDocument();

			BrickEnumerator be = report.PrintingSystem.Pages[0].GetEnumerator();
			float b = 0;
			while (be.MoveNext())
			{
				var vb = be.CurrentBrick as VisualBrick;
				if (vb != null && b < vb.Rect.Bottom)
					b = vb.Rect.Bottom;
			}
			if (report.ReportUnit == ReportUnit.HundredthsOfAnInch)
				b = XRConvert.Convert(b, 300, 100);

			b = b / 1.8f;

			report.PaperKind = PaperKind.Custom;
			report.PageHeight = (int)b + 1 + report.Margins.Top + report.Margins.Bottom;
			report.CreateDocument();
			report.PrintingSystem.ShowMarginsWarning = false;

			var myPrintTool = new ReportPrintTool(report);
			myPrintTool.Print(printerName);
		}

		private void PrintInternalUseReport(Order[] orders, string printerName)
		{
			var report = new InternalUseReceiptReport();
			report.DataSource = orders;
			report.CreateDocument();

			BrickEnumerator be = report.PrintingSystem.Pages[0].GetEnumerator();
			float b = 0;
			while (be.MoveNext())
			{
				/*report.lblNo.Text += " - " + b.ToString() ;*/
				var vb = be.CurrentBrick as VisualBrick;
				if (vb != null && b < vb.Rect.Bottom)
					b = vb.Rect.Bottom;
			}
			if (report.ReportUnit == ReportUnit.HundredthsOfAnInch)
				b = XRConvert.Convert(b, 300, 100);

			//b = ((b * 3f) / 1.7f + 200) * .65f;
			//report.lblNo.Text += " - " + b.ToString();
			report.PaperKind = PaperKind.Custom;
			report.PageHeight = (int)b + 1 + report.Margins.Top + report.Margins.Bottom;
			//report.lblNo.Text += "PH " + report.PageHeight;
			report.CreateDocument();
			report.PrintingSystem.ShowMarginsWarning = false;

			if (report.Pages.Count > 1)
				report.Pages.RemoveAt(1);

			var myPrintTool = new ReportPrintTool(report);
			myPrintTool.Print(printerName);
		}

		public void Print(PrintLayout printLayout, string[] printerNames)
		{
			instance.printLayout = printLayout;

			foreach (var printer in printerNames)
			{
				var pd = new PrintDocument();
				pd.PrinterSettings.PrinterName = printer;
				pd.PrintPage += (pd_PrintPage);
				pd.Print();
			}
		}
		#endregion

		#region Properties

		public static string ReceiptTypesToPrint
		{
			get
			{
				return instance.receiptTypesToPrint;
			}
			set
			{
				instance.receiptTypesToPrint = value;
			}
		}

		/// <summary>
		/// Gets or sets the font to use when printing
		/// </summary>
		public static Font PrintFont
		{
			get
			{
				return instance.printFont;
			}
			set
			{
				instance.printFont = value;
			}
		}

		/// <summary>
		/// Gets the name of the company
		/// </summary>
		public static string CompanyName
		{
			get
			{
				return instance.companyName;
			}
			set
			{
				instance.companyName = value;
			}
		}

		/// <summary>
		/// Gets the address line 1 of the company
		/// </summary>
		public static string CompanyAddressline1
		{
			get
			{
				return instance.companyAddressline1;
			}
			set
			{
				instance.companyAddressline1 = value;
			}
		}

		/// <summary>
		/// Gets the address line 2 of the company
		/// </summary>
		public static string CompanyAddressline2
		{
			get
			{
				return instance.companyAddressline2;
			}
			set
			{
				instance.companyAddressline2 = value;
			}
		}

		/// <summary>
		/// Gets the address line 3 of the company
		/// </summary>
		public static string CompanyAddressline3
		{
			get
			{
				return instance.companyAddressline3;
			}
			set
			{
				instance.companyAddressline3 = value;
			}
		}

		/// <summary>
		/// Gets the telephone number of the company
		/// </summary>
		public static string CompanyTelephone
		{
			get
			{
				return instance.companyTelephone;
			}
			set
			{
				instance.companyTelephone = value;
			}
		}

		/// <summary>
		/// Gets the fax number of the company
		/// </summary>
		public static string CompanyFax
		{
			get
			{
				return instance.companyFax;
			}
			set
			{
				instance.companyFax = value;
			}
		}

		/// <summary>
		/// Gets the website address of the company
		/// </summary>
		public static string CompanyWebsite
		{
			get
			{
				return instance.companyWebsite;
			}
			set
			{
				instance.companyWebsite = value;
			}
		}

		/// <summary>
		/// Gets the email address of the company
		/// </summary>
		public static string CompanyEmail
		{
			get
			{
				return instance.companyEmail;
			}
			set
			{
				instance.companyEmail = value;
			}
		}

		/// <summary>
		/// Gets the file path of the company logo
		/// </summary>
		public static string CompanyLogoFile
		{
			get
			{
				return instance.companyLogoFile;
			}
			set
			{
				instance.companyLogoFile = value;
			}
		}

		#endregion

		#region Event handlers

		void pd_PrintPage(object sender, PrintPageEventArgs ev)
		{
			float yPos;
			int count = 0;
			const float leftMargin = 10;  //ev.MarginBounds.Left;
			const float topMargin = 10; // ev.MarginBounds.Top;
			string line = null;
			PrintFont = new Font("Lucida Console", 8);

			// Calculate the number of lines per page.
			float linesPerPage = ev.MarginBounds.Height / PrintFont.GetHeight(ev.Graphics);

			if (File.Exists(CompanyLogoFile))
			{
				yPos = topMargin + (count * PrintFont.GetHeight(ev.Graphics));
				ev.Graphics.DrawImage(Image.FromFile(CompanyLogoFile), leftMargin + 75, yPos);
				count++;
			}

			instance.stringReader = new StringReader(instance.printLayout.RenderedOutput);

			// Print each line of the file.
			while (count < linesPerPage && ((line = instance.stringReader.ReadLine()) != null))
			{
				yPos = topMargin + (count * PrintFont.GetHeight(ev.Graphics));
				ev.Graphics.DrawString(line, PrintFont, Brushes.Black, leftMargin, yPos, new StringFormat());
				count++;
			}

			yPos = topMargin + (count * PrintFont.GetHeight(ev.Graphics));
			const string obymobiLogo = "obymobi.png";
			if (File.Exists(obymobiLogo))
			{
				ev.Graphics.DrawImage(Image.FromFile(obymobiLogo), leftMargin + 75, yPos);
			}

			// If more lines exist, print another page.
			if (line != null)
				ev.HasMorePages = true;
			else
				ev.HasMorePages = false;
		}

		#endregion
	}
}
