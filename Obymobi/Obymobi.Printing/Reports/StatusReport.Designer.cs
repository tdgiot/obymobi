namespace Obymobi.Printing.Reports
{
	partial class StatusReport
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lblSolution = new DevExpress.XtraReports.UI.XRLabel();
            this.imgObymobi = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblStateUsername = new DevExpress.XtraReports.UI.XRLabel();
            this.lblStatusCheckDate = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblStatus = new DevExpress.XtraReports.UI.XRLabel();
            this.lblStatePrinter = new DevExpress.XtraReports.UI.XRLabel();
            this.lblStateObymobiDatabase = new DevExpress.XtraReports.UI.XRLabel();
            this.lblStateObymobiWebservice = new DevExpress.XtraReports.UI.XRLabel();
            this.lblStateInternet = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblTerminal = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.drStatusReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.drStatusReportContent = new DevExpress.XtraReports.UI.DetailBand();
            this.drClosingReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.topMarginBand1 = new DevExpress.XtraReports.UI.TopMarginBand();
            this.bottomMarginBand1 = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Dpi = 254F;
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 150, 254F);
            this.Detail.StylePriority.UsePadding = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSolution
            // 
            this.lblSolution.Dpi = 254F;
            this.lblSolution.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSolution.LocationFloat = new DevExpress.Utils.PointFloat(21F, 519F);
            this.lblSolution.Multiline = true;
            this.lblSolution.Name = "lblSolution";
            this.lblSolution.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblSolution.SizeF = new System.Drawing.SizeF(680F, 21F);
            this.lblSolution.StylePriority.UseFont = false;
            this.lblSolution.StylePriority.UseTextAlignment = false;
            this.lblSolution.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // imgObymobi
            // 
            this.imgObymobi.Dpi = 254F;
            this.imgObymobi.LocationFloat = new DevExpress.Utils.PointFloat(0F, 558F);
            this.imgObymobi.Name = "imgObymobi";
            this.imgObymobi.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 254F);
            this.imgObymobi.SizeF = new System.Drawing.SizeF(720F, 254F);
            this.imgObymobi.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            this.imgObymobi.StylePriority.UsePadding = false;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Dpi = 254F;
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(0F, 347F);
            this.xrLabel14.Multiline = true;
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(550F, 42F);
            this.xrLabel14.Text = "Logingegevens";
            // 
            // lblStateUsername
            // 
            this.lblStateUsername.Dpi = 254F;
            this.lblStateUsername.LocationFloat = new DevExpress.Utils.PointFloat(569F, 347F);
            this.lblStateUsername.Multiline = true;
            this.lblStateUsername.Name = "lblStateUsername";
            this.lblStateUsername.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblStateUsername.SizeF = new System.Drawing.SizeF(148F, 42F);
            this.lblStateUsername.Text = "Mislukt";
            // 
            // lblStatusCheckDate
            // 
            this.lblStatusCheckDate.Dpi = 254F;
            this.lblStatusCheckDate.LocationFloat = new DevExpress.Utils.PointFloat(378F, 410F);
            this.lblStatusCheckDate.Multiline = true;
            this.lblStatusCheckDate.Name = "lblStatusCheckDate";
            this.lblStatusCheckDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblStatusCheckDate.SizeF = new System.Drawing.SizeF(336F, 42F);
            this.lblStatusCheckDate.Text = "In orde";
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Dpi = 254F;
            this.xrPageInfo1.Format = "{0:G}";
            this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(378F, 452F);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.xrPageInfo1.SizeF = new System.Drawing.SizeF(339F, 42F);
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Dpi = 254F;
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 452F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(357F, 42F);
            this.xrLabel7.Text = "Status afgedrukt";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Dpi = 254F;
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 410F);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(370F, 42F);
            this.xrLabel1.Text = "Status gecontroleerd";
            // 
            // lblStatus
            // 
            this.lblStatus.CanGrow = false;
            this.lblStatus.Dpi = 254F;
            this.lblStatus.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblStatus.SizeF = new System.Drawing.SizeF(720F, 127F);
            this.lblStatus.StyleName = "largeBoldText";
            this.lblStatus.StylePriority.UseTextAlignment = false;
            this.lblStatus.Text = "NIET GEREED!";
            this.lblStatus.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblStatePrinter
            // 
            this.lblStatePrinter.Dpi = 254F;
            this.lblStatePrinter.LocationFloat = new DevExpress.Utils.PointFloat(569F, 302F);
            this.lblStatePrinter.Multiline = true;
            this.lblStatePrinter.Name = "lblStatePrinter";
            this.lblStatePrinter.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblStatePrinter.SizeF = new System.Drawing.SizeF(148F, 42F);
            this.lblStatePrinter.Text = "OK";
            // 
            // lblStateObymobiDatabase
            // 
            this.lblStateObymobiDatabase.Dpi = 254F;
            this.lblStateObymobiDatabase.LocationFloat = new DevExpress.Utils.PointFloat(569F, 257F);
            this.lblStateObymobiDatabase.Multiline = true;
            this.lblStateObymobiDatabase.Name = "lblStateObymobiDatabase";
            this.lblStateObymobiDatabase.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblStateObymobiDatabase.SizeF = new System.Drawing.SizeF(148F, 42F);
            this.lblStateObymobiDatabase.Text = "Mislukt";
            // 
            // lblStateObymobiWebservice
            // 
            this.lblStateObymobiWebservice.Dpi = 254F;
            this.lblStateObymobiWebservice.LocationFloat = new DevExpress.Utils.PointFloat(569F, 214F);
            this.lblStateObymobiWebservice.Multiline = true;
            this.lblStateObymobiWebservice.Name = "lblStateObymobiWebservice";
            this.lblStateObymobiWebservice.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblStateObymobiWebservice.SizeF = new System.Drawing.SizeF(148F, 42F);
            this.lblStateObymobiWebservice.Text = "Mislukt";
            // 
            // lblStateInternet
            // 
            this.lblStateInternet.Dpi = 254F;
            this.lblStateInternet.LocationFloat = new DevExpress.Utils.PointFloat(569F, 169F);
            this.lblStateInternet.Multiline = true;
            this.lblStateInternet.Name = "lblStateInternet";
            this.lblStateInternet.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblStateInternet.SizeF = new System.Drawing.SizeF(148F, 42F);
            this.lblStateInternet.Text = "Mislukt";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Dpi = 254F;
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 302F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(550F, 42F);
            this.xrLabel6.Text = "Afdrukken van bestellingen";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Dpi = 254F;
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 257F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(550F, 42F);
            this.xrLabel5.Text = "Gegevensoverdracht Obymobi";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Dpi = 254F;
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 214F);
            this.xrLabel4.Multiline = true;
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(550F, 42F);
            this.xrLabel4.Text = "Verbinding met Obymobi";
            // 
            // xrLabel3
            // 
            this.xrLabel3.Dpi = 254F;
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 169F);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(550F, 42F);
            this.xrLabel3.Text = "Verbinding met internet";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTerminal,
            this.xrLabel2});
            this.ReportHeader.Dpi = 254F;
            this.ReportHeader.HeightF = 265F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblTerminal
            // 
            this.lblTerminal.CanGrow = false;
            this.lblTerminal.Dpi = 254F;
            this.lblTerminal.Font = new System.Drawing.Font("Lucida Console", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTerminal.LocationFloat = new DevExpress.Utils.PointFloat(0F, 127F);
            this.lblTerminal.Name = "lblTerminal";
            this.lblTerminal.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.lblTerminal.SizeF = new System.Drawing.SizeF(720F, 127F);
            this.lblTerminal.StyleName = "largeBoldText";
            this.lblTerminal.StylePriority.UseFont = false;
            this.lblTerminal.StylePriority.UseTextAlignment = false;
            this.lblTerminal.Text = "Terminal";
            this.lblTerminal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.CanGrow = false;
            this.xrLabel2.Dpi = 254F;
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(720F, 127F);
            this.xrLabel2.StyleName = "largeBoldText";
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Status rapport";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Dpi = 254F;
            this.ReportFooter.HeightF = 58F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrControlStyle2
            // 
            this.xrControlStyle2.Name = "xrControlStyle2";
            // 
            // drStatusReport
            // 
            this.drStatusReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.drStatusReportContent});
            this.drStatusReport.Dpi = 254F;
            this.drStatusReport.Level = 0;
            this.drStatusReport.Name = "drStatusReport";
            // 
            // drStatusReportContent
            // 
            this.drStatusReportContent.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblSolution,
            this.imgObymobi,
            this.xrLabel14,
            this.lblStateUsername,
            this.lblStatusCheckDate,
            this.xrPageInfo1,
            this.xrLabel7,
            this.xrLabel1,
            this.xrLabel3,
            this.lblStatePrinter,
            this.lblStateObymobiDatabase,
            this.lblStateObymobiWebservice,
            this.lblStateInternet,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.lblStatus});
            this.drStatusReportContent.Dpi = 254F;
            this.drStatusReportContent.HeightF = 812F;
            this.drStatusReportContent.Name = "drStatusReportContent";
            // 
            // drClosingReport
            // 
            this.drClosingReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2});
            this.drClosingReport.Dpi = 254F;
            this.drClosingReport.Level = 1;
            this.drClosingReport.Name = "drClosingReport";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel9,
            this.xrLabel8});
            this.Detail2.Dpi = 254F;
            this.Detail2.HeightF = 331F;
            this.Detail2.Name = "Detail2";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Dpi = 254F;
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(3F, 203F);
            this.xrLabel9.Multiline = true;
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(701F, 127F);
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Deze terminal is vanaf nu uitgeschakeld. De gasten kunnen nu niet meer met Obymob" +
                "i bij u bestellen.";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel8
            // 
            this.xrLabel8.CanGrow = false;
            this.xrLabel8.Dpi = 254F;
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 254F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(720F, 127F);
            this.xrLabel8.StyleName = "largeBoldText";
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "UITGESCHAKELD";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // topMarginBand1
            // 
            this.topMarginBand1.Dpi = 254F;
            this.topMarginBand1.HeightF = 0F;
            this.topMarginBand1.Name = "topMarginBand1";
            // 
            // bottomMarginBand1
            // 
            this.bottomMarginBand1.Dpi = 254F;
            this.bottomMarginBand1.HeightF = 0F;
            this.bottomMarginBand1.Name = "bottomMarginBand1";
            // 
            // StatusReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportHeader,
            this.ReportFooter,
            this.drStatusReport,
            this.drClosingReport,
            this.topMarginBand1,
            this.bottomMarginBand1});
            this.Dpi = 254F;
            this.Font = new System.Drawing.Font("Lucida Console", 8.25F);
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 0, 0);
            this.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 100, 254F);
            this.PageHeight = 2500;
            this.PageWidth = 722;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            this.ReportUnit = DevExpress.XtraReports.UI.ReportUnit.TenthsOfAMillimeter;
            this.SnapToGrid = false;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle2});
            this.StyleSheetPath = "D:\\Development\\dotnet\\Obymobi\\ObymobiRequestService.Printing\\Reports\\DefaultStyle" +
                "Sheet.repss";
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
		private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
		private DevExpress.XtraReports.UI.XRLabel xrLabel3;
		private DevExpress.XtraReports.UI.XRLabel xrLabel2;
		private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle2;
		private DevExpress.XtraReports.UI.XRLabel lblStateInternet;
		private DevExpress.XtraReports.UI.XRLabel xrLabel6;
		private DevExpress.XtraReports.UI.XRLabel xrLabel5;
		private DevExpress.XtraReports.UI.XRLabel xrLabel4;
		private DevExpress.XtraReports.UI.XRLabel lblStatePrinter;
		private DevExpress.XtraReports.UI.XRLabel lblStateObymobiDatabase;
		private DevExpress.XtraReports.UI.XRLabel lblStateObymobiWebservice;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRLabel lblStatus;
		private DevExpress.XtraReports.UI.XRLabel xrLabel7;
		private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblStatusCheckDate;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel lblStateUsername;
        private DevExpress.XtraReports.UI.XRPictureBox imgObymobi;
        private DevExpress.XtraReports.UI.XRLabel lblTerminal;
        private DevExpress.XtraReports.UI.XRLabel lblSolution;
		private DevExpress.XtraReports.UI.DetailReportBand drStatusReport;
		private DevExpress.XtraReports.UI.DetailBand drStatusReportContent;
		private DevExpress.XtraReports.UI.DetailReportBand drClosingReport;
		private DevExpress.XtraReports.UI.DetailBand Detail2;
		private DevExpress.XtraReports.UI.XRLabel xrLabel9;
		private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.TopMarginBand topMarginBand1;
        private DevExpress.XtraReports.UI.BottomMarginBand bottomMarginBand1;
	}
}
