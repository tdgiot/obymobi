using Obymobi.Logic.Model;
using System.Windows.Forms;
using System.IO;
using Dionysos;
using Obymobi.Printing.Properties;

namespace Obymobi.Printing.Reports
{
    public partial class OpenTransactionReport : DevExpress.XtraReports.UI.XtraReport
    {
        public OpenTransactionReport()
        {
            InitializeComponent();

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("nl-NL");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("nl-NL");

            string appPath = Application.StartupPath;

            this.StyleSheetPath = Path.Combine(appPath, "DefaultStyleSheet.repss");

            this.imgCompanyLogo.ImageUrl = Path.Combine(appPath, "logo.jpg");
            //this.imgObymobi.Image = Resources.obymobi;
            //this.imgObymobi2.Image = Resources.obymobi;
            this.lblCompanyInfo1.Text = StringUtil.CombineWithSpace(PrintManager.CompanyAddressline1);
            this.lblCompanyInfo2.Text = PrintManager.CompanyTelephone;
            this.lblCompanyInfo3.Text = PrintManager.CompanyWebsite;
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.lblWelcomeText.Text = "Je dient je eerste bestelling van vandaag bij '{0}' te bevestigen met de code die hieronder wordt vermeld. Je kunt deze code op je telefoon invoeren via de functie ‘Bevestigingscode’ in het startscherm.";
            var orders = (Order[])this.DataSource;
            if (orders.Length >= this.CurrentRowIndex + 1)
            {
                Order order = orders[this.CurrentRowIndex];

                this.lblWelcomeText.Text = string.Format(this.lblWelcomeText.Text, order.CompanyName);

                if (order.Type == 100)
                {
                    // Confirmation
                    this.drConfirmationOrder.Visible = true;
                    this.drOrderitems.Visible = false;
                    this.rf.Visible = false;
                }
                else
                {
                    // Order items
                    this.drConfirmationOrder.Visible = false;
                    this.drOrderitems.Visible = true;
                    this.rf.Visible = true;
                }
            }
        }

        //public OpenTransactionReport(Order order)
        //{
        //    this.InitializeComponent();
        //}
    }
}
