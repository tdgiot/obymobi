using System.IO;
using System.Windows.Forms;
using Obymobi.Logic.Model;
using System.Collections.Generic;

namespace Obymobi.Printing.Reports
{
    public partial class InternalUseReceiptReport : DevExpress.XtraReports.UI.XtraReport
    {
        public InternalUseReceiptReport()
        {
            InitializeComponent();

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("nl-NL");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("nl-NL");

            string appPath = Application.StartupPath;

            this.StyleSheetPath = Path.Combine(appPath, "DefaultStyleSheet.repss");
        }

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
            Order[] orders = (Order[])this.DataSource;
			if (orders.Length >= this.CurrentRowIndex + 1)
			{
				Order order = orders[this.CurrentRowIndex];
    			this.drAgeCheck.Visible = false;
			}
		}

		private void Detail3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			
		}		

		private void InternalUseReceiptReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
		
		}
    }
}
