using System.IO;
using Obymobi.Logic.Status;
using Obymobi.Printing.Properties;

namespace Obymobi.Printing.Reports
{
    public partial class StatusReport : DevExpress.XtraReports.UI.XtraReport
    {
        /*public StatusReport(XTerminalStatus status, bool closingReport)
        {
            InitializeComponent();

            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("nl-NL");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("nl-NL");

            this.StyleSheetPath = Path.Combine("", "DefaultStyleSheet.repss");

            if (status.TerminalId > 0)
                this.lblTerminal.Text = "Terminal " + status.TerminalId.ToString();

            if (closingReport)
            {
                this.drClosingReport.Visible = true;
                this.drStatusReport.Visible = false;
            }
            else
            {
                this.drClosingReport.Visible = false;
                this.drStatusReport.Visible = true;

                const string okText = "OK";
                const string failureText = "MISLUKT";
                const string invalidText = "ONGELDIG";

                // Data
                this.lblStatusCheckDate.Text = status.Created.ToString("G");

                // Set terminal steps
                if (status.Internet)
                    this.lblStateInternet.Text = okText;
                else
                {
                    this.lblStateInternet.Text = failureText;
                    this.lblSolution.Text = "Er is een probleem geconstateerd met de verbinding met internet. Controleer de internetverbinding.";
                }

                if (status.Webservice)
                    this.lblStateObymobiWebservice.Text = okText;
                else
                {
                    this.lblStateObymobiWebservice.Text = failureText;
                    this.lblSolution.Text = "Er is een probleem geconstateerd met de verbinding met Obymobi. Raadpleeg de Service meldingen pagina op www.obymobi.com.";
                }

                if (status.Database)
                    this.lblStateObymobiDatabase.Text = okText;
                else
                {
                    this.lblStateObymobiDatabase.Text = failureText;
                    this.lblSolution.Text = "Er is een probleem geconstateerd met de gegevensoverdracht met Obymobi. Raadpleeg de Service meldingen pagina op www.obymobi.com.";
                }

                // Set configuration steps
                if (status.UsernameSpecified && status.PasswordSpecified && status.CompanyIdSpecified && status.TerminalIdSpecified && status.PrinternameSpecified && (status.ConfigurationValid.HasValue && status.ConfigurationValid.Value))
                    this.lblStateUsername.Text = okText;
                else
                {
                    this.lblStateUsername.Text = invalidText;
                    this.lblSolution.Text = "Er is een probleem geconstateerd met de logingegevens van Obymobi. Neem contact op met Obymobi om de logingegevens in te stellen.";
                }

                // Set main title
                const string headerOkText = "OK";
                const string headerFailureText = "NIET GEREED!";
                this.lblStatus.Text = status.Ok ? headerOkText : headerFailureText;

                // Set image
                this.imgObymobi.Image = Resources.obymobi;
            }
        }*/
    }
}
