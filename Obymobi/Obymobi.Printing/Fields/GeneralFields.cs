﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Printing.Fields
{
    public class GeneralFields
    {
        public const string Line =      "---------------------------------------";
        public const string DoubleLine= "=======================================";

        public const string CompanyName = "{CompanyName}";
        public const string CompanyAddressline1 = "{CompanyAddressline1}";
        public const string CompanyAddressline2 = "{CompanyAddressline2}";
        public const string CompanyAddressline3 = "{CompanyAddressline3}";
        public const string CompanyTelephone = "{CompanyTelephone}";
        public const string CompanyFax = "{CompanyFax}";
        public const string CompanyWebsite = "{CompanyWebsite}";
        public const string CompanyEmail = "{CompanyEmail}";

        public const string CustomerNameFull = "{CustomerNameFull}";

        public const string HelpText = "{HelpText}";
    }
}
