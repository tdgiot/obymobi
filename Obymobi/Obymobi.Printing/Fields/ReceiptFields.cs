﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Obymobi.Printing.Fields
{
    public class ReceiptFields
    {
        public const string OrderId = "{OrderId}";
        public const string DeliverypointNumber = "{DeliverypointNumber}";
        public const string PaymentmethodName = "{PaymentmethodName}";

        public const string Orderitems = "{Orderitems}";
        public const string Quantity = "{Quantity}";
        public const string ProductName = "{ProductName}";
        public const string PriceIn = "{PriceIn}";
        public const string PriceInSubTotal = "{PriceInSubTotal}";

        public const string PriceExTotal = "{PriceExTotal}";
        public const string PriceVatTotal = "{PriceVatTotal}";
        public const string PriceInTotal = "{PriceInTotal}";

        public const string Notes = "{Notes}";
    }
}
