﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using CraveBuilder.Configuration;

namespace CraveBuilder
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);

            Application_Start();

            System.Windows.Forms.Application.Run(new FormMain());
        }

        private static void Application_Start()
        {
            // Set the configuration provider
            Dionysos.Configuration.XmlConfigurationProvider provider = new Dionysos.Configuration.XmlConfigurationProvider();
            provider.ManualConfigFilePath = Path.Combine(System.Windows.Forms.Application.StartupPath, "CraveBuilder.config");

            Dionysos.Global.ConfigurationProvider = provider;

            // Set the application information
            Dionysos.Global.ApplicationInfo = new Dionysos.Windows.Forms.ApplicationInfo();
            Dionysos.Global.ApplicationInfo.ApplicationName = "CraveBuilder";
            Dionysos.Global.ApplicationInfo.ApplicationVersion = "1.2014052000";
            Dionysos.Global.ApplicationInfo.BasePath = System.Windows.Forms.Application.StartupPath;

            // Set the configuration definitions
            Dionysos.Global.ConfigurationInfo.Add(new CraveBuilderConfigInfo());
        }
    }
}
