﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CraveBuilder
{
	public partial class FormAndroid : Form
	{
		[DllImport("User32.dll", CharSet = CharSet.Auto, EntryPoint = "SendMessage")]
		static extern IntPtr SendMessage(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

		const int WM_VSCROLL = 277;
		const int SB_BOTTOM = 7;

		#region Fields

		private string androidSourceDir = "D:/Development/Android";

		private readonly AndroidBuilder builder;

		private string selectedApplication = "CraveDemo";

		#endregion

		public FormAndroid()
		{
			InitializeComponent();

			// Enable these styles to reduce flicker of output textbox
			this.SetStyle(ControlStyles.UserPaint, true);
			this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
			this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

			this.builder = new AndroidBuilder(this.tbOutput);
			this.builder.BuildCompletedEvent += builder_BuildCompletedEvent;

            // Get the available folders
            var androidDirectories = Directory.GetDirectories("d:\\development\\", "android*").ToList();
            if (androidDirectories.Count == 0)
            {
                MessageBox.Show("Android Folders", "No Android folders found in D:\\Development");
                Close();

                return;
            }

            androidSourceDir = androidDirectories[0];
            this.cbAndroidFolder.DataSource = androidDirectories;      

			this.cmbApplication.SelectedIndex = 0;
		}

		#region Builder Events

		void builder_BuildCompletedEvent()
		{
			this.Invoke((MethodInvoker)(() =>
				{
					this.emenuInputFields.Enabled = true;
					this.btnBuild.Text = @"Start Build";
				}));

			if (this.builder.IsBuildComplete)
			{
				MessageBox.Show(@"ARIBA!");
			}
		}

		#endregion

		#region Methods

		void ReadVersionsFromFile()
		{
			// Emenu stuff
			string manifestPath = string.Format("{0}/{1}", androidSourceDir, selectedApplication);
			int version = ReadManifest(manifestPath);
			this.tbAPKVersion.Text = (version == 0) ? "" : (version + 1).ToString(CultureInfo.InvariantCulture);

			string devSettingsPath = string.Format("{0}/{1}", androidSourceDir, "CraveBase/src/net/craveinteractive/cravebase/logic");
			this.tbAPIVersion.Text = ReadApiVersion(devSettingsPath);
		}

		int ReadManifest(string manifestPath)
		{
			string manifest = File.ReadAllText(string.Format("{0}/AndroidManifest.xml", manifestPath));
			int versionCodeIndex = manifest.IndexOf("versionCode=\"", StringComparison.Ordinal) + 13;

			int versionOut;
			if (!int.TryParse(manifest.Substring(versionCodeIndex, 8), out versionOut))
			{
				versionOut = 0;
			}
			return versionOut;
		}

		string ReadApiVersion(string developerSettingsPath)
		{
			string devSettings = File.ReadAllText(string.Format("{0}/DeveloperSettings.java", developerSettingsPath));
			int apiVersionIndex = devSettings.IndexOf("API_VERSION = \"", StringComparison.Ordinal) + 15;
			int apiVersionEnd = devSettings.IndexOf('"', apiVersionIndex);

			return devSettings.Substring(apiVersionIndex, apiVersionEnd - apiVersionIndex);
		}

		bool ValidateInputFields()
		{
			string apkVersion = this.tbAPKVersion.Text;
			string apiVersion = this.tbAPIVersion.Text;

			// Check if API version is a x.x number
			bool apiVersionCorrect = false;
			int tmp;
			if (apiVersion.Contains("."))
			{
				string[] apiSplit = apiVersion.Split('.');
				foreach (string apiPart in apiSplit)
				{
					apiVersionCorrect = int.TryParse(apiPart, out tmp);
				}
			}

			bool apkVersionCorrect = int.TryParse(apkVersion, out tmp);

			string errorMessage = "Build could not be started:\n";
			if (!apiVersionCorrect)
			{
				errorMessage += " - API version does not have the correct format\n";
			}
			if (!apkVersionCorrect)
			{
				errorMessage += " - APK version is not a number";
			}

			if (!apiVersionCorrect || !apkVersionCorrect)
			{
				MessageBox.Show(errorMessage, @"Build Error");
				return false;
			}

			return true;
		}

		#endregion

		#region Event Handlers

		private void btnBuild_Click(object sender, EventArgs e)
		{
			if (this.builder.IsBuilding)
			{
				DialogResult result = MessageBox.Show(@"Are you sure you want to stop the build process?", @"Stop Build?", MessageBoxButtons.YesNo);
				if (result.Equals(DialogResult.Yes))
				{
					this.builder.StopBuild();
				}
			}
			else
			{
                this.builder.AndroidBaseDir = androidSourceDir;

				if (ValidateInputFields())
				{
					this.emenuInputFields.Enabled = false;
					this.btnBuild.Text = @"Stop Build";
					this.tbOutput.Clear();

					switch (cmbApplication.SelectedIndex)
					{
						case 0:
							this.builder.BuildCraveEmenu(this.tbAPIVersion.Text, this.tbAPKVersion.Text, this.cbDropbox.Checked);
							break;
						case 1:
							this.builder.BuildCraveAgent(this.tbAPIVersion.Text, this.tbAPKVersion.Text, this.cbDropbox.Checked);
							break;
						case 2:
							this.builder.BuildCraveSupportTools(this.tbAPIVersion.Text, this.tbAPKVersion.Text, this.cbDropbox.Checked);
							break;
					}
				}
			}
		}

		private void tbOutput_TextChanged(object sender, EventArgs e)
		{
			var ptrWparam = new IntPtr(SB_BOTTOM);
			var ptrLparam = new IntPtr(0);
			SendMessage(this.tbOutput.Handle, WM_VSCROLL, ptrWparam, ptrLparam);
		}

		private void FormAndroid_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (this.builder.IsBuilding)
			{
				DialogResult result = MessageBox.Show(@"The Android builder is still running. Are you sure you want to close this dialog? This will stop the build.", @"Close Form?", MessageBoxButtons.YesNo);
				if (result.Equals(DialogResult.Yes))
				{
					this.builder.StopBuild();
				}
				else
				{
					e.Cancel = true;
				}
			}
		}

		private void tbAPKVersion_KeyPress(object sender, KeyPressEventArgs e)
		{
			e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
		}

		private void cmbApplication_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (cmbApplication.SelectedIndex)
			{
				case 0:
					selectedApplication = "CraveDemo";
					break;
				case 1:
					selectedApplication = "CraveAgent";
					break;
				case 2:
					selectedApplication = "CraveSupportTools";
					break;
			}

			ReadVersionsFromFile();
		}

        private void cbAndroidFolder_SelectedIndexChanged(object sender, EventArgs e)
        {
            androidSourceDir = this.cbAndroidFolder.Text;

            ReadVersionsFromFile();
        }	

		#endregion

	}
}
